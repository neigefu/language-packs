<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>DetailMessageWindow</name>
    <message>
        <location filename="../view/detail_message_window.cpp" line="10"/>
        <source>Message Notification</source>
        <translation>Хабар хабарландыруы</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="14"/>
        <source>title</source>
        <translation>тақырыбы</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="15"/>
        <source>content</source>
        <translation>мазмұн</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="16"/>
        <source>sender</source>
        <translation>жіберуші</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="17"/>
        <source>time</source>
        <translation>уақыт</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="521"/>
        <source>jump to</source>
        <translation>секіру</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="523"/>
        <source>pages</source>
        <translation>.pages</translation>
    </message>
</context>
<context>
    <name>HistoryLogWindow</name>
    <message>
        <location filename="../view/history_log_window.cpp" line="13"/>
        <location filename="../view/history_log_window.cpp" line="22"/>
        <source>Log List</source>
        <translation>Журнал тізімі</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Time</source>
        <translation>Уақыт</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>User</source>
        <translation>Пайдаланушы</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Log Type</source>
        <translation>Журнал түрі</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Content</source>
        <translation>Мазмұн</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>No log information found!</source>
        <translation>Журнал мәліметі табылмады!</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>warning</source>
        <translation>Ескерту</translation>
    </message>
</context>
<context>
    <name>HistoryMessageWindow</name>
    <message>
        <location filename="../view/history_message_window.cpp" line="14"/>
        <location filename="../view/history_message_window.cpp" line="23"/>
        <source>History Messages</source>
        <translation>Журнал хабарлары</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>State</source>
        <translation>Мемлекет</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Time</source>
        <translation>Уақыт</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Sender</source>
        <translation>Жіберуші</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Importance</source>
        <translation>Маңыздылығы</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Title</source>
        <translation>Тақырып</translation>
    </message>
</context>
<context>
    <name>InspectWindow</name>
    <message>
        <location filename="../view/inspect_window.cpp" line="14"/>
        <location filename="../view/inspect_window.cpp" line="23"/>
        <location filename="../view/inspect_window.cpp" line="161"/>
        <location filename="../view/inspect_window.cpp" line="276"/>
        <source>Complication Inspect</source>
        <translation>Асқынуды тексеру</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="116"/>
        <source>Repair</source>
        <translation>Жөндеу</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="127"/>
        <location filename="../view/inspect_window.cpp" line="133"/>
        <source>Repair tip</source>
        <translation>Жөндеу бойынша кеңес</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="134"/>
        <location filename="../view/inspect_window.cpp" line="164"/>
        <location filename="../view/inspect_window.cpp" line="281"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="171"/>
        <location filename="../view/inspect_window.cpp" line="237"/>
        <source>Repairing...</source>
        <translation>Жөндеу...</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="305"/>
        <source>     Inspection passed</source>
        <translation>     Тексеруден өтті</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="55"/>
        <location filename="../view/inspect_window.cpp" line="352"/>
        <source>One key repair</source>
        <translation>Бір кілтті жөндеу</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="162"/>
        <source>Please wait for the current repair to complete before performing the repair operation!</source>
        <translation>Жөндеу жұмыстарын орындау алдында ағымдағы жөндеудің аяқталғанын күтуіңізді сұраймын!</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="277"/>
        <source>The current device has not detected integrity detection rules. Please contact the server to issue them!</source>
        <translation>Қолданыстағы құрылғыда тұтастықты анықтау ережелері анықталмаған. Оларды беру үшін серверге хабарласыңыз!</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Items</source>
        <translation>Элементтер</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Baseline</source>
        <translation>Негізгі сызық</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Result</source>
        <translation>Нәтиже</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Operate</source>
        <translation>Жұмыс істеу</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="356"/>
        <source>Check Now</source>
        <translation>Қазір тексеру</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <source>total</source>
        <translation>барлығы</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <location filename="../view/item_wid/paginationwid.cpp" line="43"/>
        <source>pages</source>
        <translation>.pages</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="40"/>
        <source>jump to</source>
        <translation>секіру</translation>
    </message>
</context>
<context>
    <name>PolicyWindow</name>
    <message>
        <location filename="../view/policy_window.cpp" line="19"/>
        <location filename="../view/policy_window.cpp" line="24"/>
        <source>Policy info</source>
        <translation>Саясат туралы ақпарат</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="45"/>
        <location filename="../view/policy_window.cpp" line="427"/>
        <source>     Policy Version: Latest</source>
        <translation>     Саясат нұсқасы: Соңғы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Policy</source>
        <translation>Саясат</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Description</source>
        <translation>Сипаттама</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Version</source>
        <translation>Нұсқасы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Operate</source>
        <translation>Жұмыс істеу</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="96"/>
        <source>One key update</source>
        <translation>Бір кілт жаңартуы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="113"/>
        <source>Loading...</source>
        <translation>Жүктеу...</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="172"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <location filename="../view/policy_window.cpp" line="304"/>
        <location filename="../view/policy_window.cpp" line="313"/>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>warning</source>
        <translation>Ескерту</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <source>Password cannot be empty!</source>
        <translation>Құпия сөз бос бола алмайды!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="191"/>
        <location filename="../view/policy_window.cpp" line="267"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="193"/>
        <location filename="../view/policy_window.cpp" line="269"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="212"/>
        <location filename="../view/policy_window.cpp" line="218"/>
        <source>Lift strategy</source>
        <translation>Лифт стратегиясы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="225"/>
        <source>verification code</source>
        <translation>тексеру коды</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="252"/>
        <source>Please send the code to the administrator to obtain the release code</source>
        <translation>Шығарылым кодын алу үшін кодты әкімшіге жіберуіңізді сұраймын</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="256"/>
        <source>release code</source>
        <translation>шығарылым коды</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="260"/>
        <source>Please enter the release code</source>
        <translation>Шығарылым кодын енгізуіңізді сұраймын</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="304"/>
        <source>The verification code cannot be empty!</source>
        <translation>Тексеру коды бос бола алмайды!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="313"/>
        <source>Please enter the correct verification code!</source>
        <translation>Дұрыс тексеру кодын енгізуіңізді сұраймын!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>Policy update failed</source>
        <translation>Саясат жаңартуы жаңылысы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="647"/>
        <source>error</source>
        <translation>қате</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="429"/>
        <source>     Policy Version: Not Latest</source>
        <translation>     Саясат нұсқасы: Соңғы емес</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="164"/>
        <source>User password</source>
        <translation>Пайдаланушы құпиясөзі</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="495"/>
        <source>Policy cancel</source>
        <translation>Саясаттан бас тарту</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="524"/>
        <source>Wait updates</source>
        <translation>Жаңартуларды күту</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="527"/>
        <source>latest</source>
        <translation>соңғы</translation>
    </message>
</context>
<context>
    <name>ScriptsWarningWindow</name>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="7"/>
        <source>Script Run Warning</source>
        <translation>Скрипт іске қосу ескертуі</translation>
    </message>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="8"/>
        <source>The script is running, please do not shut down or restart!</source>
        <translation>Сценарий іске қосылады, өшірмеуіңізді немесе қайта іске қосуыңызды өтінемін!</translation>
    </message>
</context>
<context>
    <name>SettingWindow</name>
    <message>
        <location filename="../view/setting_window.cpp" line="17"/>
        <source>App install:</source>
        <translation>Бағдарламаны орнату:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="18"/>
        <source>App run：</source>
        <translation>Бағдарламаны іске қосу:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="25"/>
        <source>Firewall:</source>
        <translation>Брандмауэр:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="26"/>
        <source>Hotspot:</source>
        <translation>Ошақ:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="27"/>
        <source>Network Isolation:</source>
        <translation>Желілік оқшаулау:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="34"/>
        <source>USB:</source>
        <translation>USB:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="35"/>
        <source>Mobile:</source>
        <translation>Ұялы телефон:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="36"/>
        <source>CD-ROM:</source>
        <translation>CD-ROM:</translation>
    </message>
</context>
<context>
    <name>ShutdownAndReboot</name>
    <message>
        <location filename="../view/shutdown_window.cpp" line="15"/>
        <source>Shutdown</source>
        <translation>Өшіру</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="17"/>
        <source>Restart</source>
        <translation>Қайта іске қосу</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="22"/>
        <location filename="../view/shutdown_window.cpp" line="26"/>
        <source>warning</source>
        <translation>Ескерту</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="23"/>
        <source>? After 60 seconds, the computer will automatically</source>
        <translation>? 60 секундтан кейін компьютер автоматты түрде жұмыс істейтін болады</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="27"/>
        <source>After 60 seconds, the computer will automatically</source>
        <translation>60 секундтан кейін компьютер автоматты түрде жұмыс істейтін болады</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="33"/>
        <source>Execute immediately</source>
        <translation>Бірден орындау</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="36"/>
        <source>Execute in 5 minutes</source>
        <translation>5 минутта орындау</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="39"/>
        <source>Remind me in an hour</source>
        <translation>Бір сағаттан кейін еске салыңыз</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="69"/>
        <source>? After </source>
        <translation>? Кейін </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="70"/>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>s, the computer will automatically </source>
        <translation>s, компьютер автоматты түрде болады </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>After </source>
        <translation>Кейін </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="102"/>
        <source>in process of</source>
        <translation>технологиялық процесте</translation>
    </message>
</context>
<context>
    <name>Trayicon_left_widget</name>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="61"/>
        <source>kcm client</source>
        <translation>kcm клиенті</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="63"/>
        <source>scene</source>
        <translation>көрініс</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="92"/>
        <source>policy update</source>
        <translation>саясатты жаңарту</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="136"/>
        <source>connection is normal</source>
        <translation>қосылым қалыпты</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="218"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="220"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="340"/>
        <source>Connection is normal</source>
        <translation>Қосылым қалыпты</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="343"/>
        <source>Connection is abnormal</source>
        <translation>Қосылым аномальды</translation>
    </message>
</context>
<context>
    <name>Trayicon_widget</name>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="23"/>
        <location filename="../view/trayicon_widget.cpp" line="193"/>
        <source>kcm client</source>
        <translation>kcm клиенті</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="131"/>
        <source>Complication inspect</source>
        <translation>Асқынуды тексеру</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="133"/>
        <source>Policy setting</source>
        <translation>Саясат параметрі</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="135"/>
        <source>Policy info</source>
        <translation>Саясат туралы ақпарат</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="137"/>
        <source>Log list</source>
        <translation>Журнал тізімі</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="139"/>
        <source>History Messages</source>
        <translation>Журнал хабарлары</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="141"/>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="143"/>
        <source>Exit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="200"/>
        <source>Version:</source>
        <translation>Нұсқасы:</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="202"/>
        <source>kcm client is a software that enables policy download, execution, and data reporting with the kylin terminal control console. Provide system management, log reporting, and other functions for user terminals</source>
        <translation>kcm клиенті — Kylin терминалды басқару консолімен саясатты жүктеуге, орындауға және деректерді есептеуге мүмкіндік беретін бағдарламалық жасақтама. Пайдаланушылар терминалдары үшін жүйені басқаруды, журнал бойынша есеп беруді және басқа да функцияларды қамтамасыз ету</translation>
    </message>
</context>
<context>
    <name>UpdateRequest</name>
    <message>
        <location filename="../view/update_request_window.cpp" line="7"/>
        <source>Upgrade Request</source>
        <translation>Сұрауды жаңарту</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="8"/>
        <location filename="../view/update_request_window.cpp" line="54"/>
        <source>The server request </source>
        <translation>Сервер сұрауы </translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="9"/>
        <source> upgrade. Please choose whether to agree or not. (If not selected after 30 seconds, the decision on whether to agree to install will be based on the configuration)</source>
        <translation> жаңарту. Келісуді немесе келіспеуді таңдауыңызды сұраймын.(Егер 30 секундтан кейін таңдалмаса, орнатуға келісудің орындылығы туралы шешім конфигурацияға негізделеді)</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="15"/>
        <source>Agree</source>
        <translation>Келiсiм беру</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="17"/>
        <source>Refuse</source>
        <translation>Бас тарту</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="55"/>
        <source> upgrade. Please choose whether to agree or not. (After ）</source>
        <translation> жаңарту. Келісе ме, жоқ па, соны таңдауыңызды сұраймын. (Кейін)</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="57"/>
        <source>s,if not selected, it will be determined whether to agree to install according to the configuration</source>
        <translation>s, егер таңдалмаса, конфигурациясына сәйкес орнатуға келісетіні анықталатын болады</translation>
    </message>
</context>
<context>
    <name>UpdateWindow</name>
    <message>
        <location filename="../view/update_window.cpp" line="7"/>
        <source>System notifications</source>
        <translation>Жүйелік хабарландырулар</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="25"/>
        <source>The system is currently undergoing a unified upgrade or installation of components. You can use it normally, but please do not shut down, restart, or log out</source>
        <translation>Қазiргi уақытта жүйеде құрамдас бөлiктердi бiрыңғай жаңарту немесе орнату жүзеге асырылады. Оны әдетте пайдалануға болады, бірақ өшірілмеуіңізді, қайта іске қосуыңызды немесе журналға кірмеуіңізді сұраймыз</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="29"/>
        <source>Getting system apt cache data</source>
        <translation>Жүйелік апт кэш деректерін алу</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="66"/>
        <source>Opening system apt cache</source>
        <translation>Жүйенің апт кэшін ашу</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="78"/>
        <source>System upgrade failed, After </source>
        <translation>Жүйені жаңарту жаңылысы, кейін </translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="79"/>
        <source>s,close the window</source>
        <translation>s, терезені жабу</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="82"/>
        <source>Updating apt cache: apt update</source>
        <translation>Apt кэшін жаңарту: apt жаңартуы</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="83"/>
        <source>speed：</source>
        <translation>жылдамдық:</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="85"/>
        <source>Performing download and installation</source>
        <translation>Жүктеу және орнатуды орындау</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="95"/>
        <source>Download completed! About to automatically close.</source>
        <translation>Жүктеу аяқталды! Автоматты түрде жабу туралы.</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="120"/>
        <source>System upgrade failed,after</source>
        <translation>Жүйені жаңарту сәтсіз аяқталды, кейін</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="121"/>
        <source>s,close the window！</source>
        <translation>s, терезені жабыңыз!</translation>
    </message>
</context>
<context>
    <name>User_information_table</name>
    <message>
        <location filename="../view/user_information_table.cpp" line="75"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="78"/>
        <source>Login Name</source>
        <translation>Кіру атауы</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="81"/>
        <source>Host Name</source>
        <translation>Хост атауы</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="84"/>
        <source>Terminal ORG</source>
        <translation>ORG терминалы</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="87"/>
        <source>Local User</source>
        <translation>Жергілікті пайдаланушы</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="91"/>
        <source>Policy Time</source>
        <translation>Саясат уақыты</translation>
    </message>
</context>
</TS>
