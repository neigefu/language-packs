<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>DetailMessageWindow</name>
    <message>
        <location filename="../view/detail_message_window.cpp" line="10"/>
        <source>Message Notification</source>
        <translation>ཆ་འཕྲིན་བརྡ་ཁྱབ།</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="14"/>
        <source>title</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="15"/>
        <source>content</source>
        <translation>ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="16"/>
        <source>sender</source>
        <translation>སྐུར་མཁན།</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="17"/>
        <source>time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="521"/>
        <source>jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="523"/>
        <source>pages</source>
        <translation>.pages</translation>
    </message>
</context>
<context>
    <name>HistoryLogWindow</name>
    <message>
        <location filename="../view/history_log_window.cpp" line="13"/>
        <location filename="../view/history_log_window.cpp" line="22"/>
        <source>Log List</source>
        <translation>ཉིན་ཐོའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Log Type</source>
        <translation>ཤིང་གི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Content</source>
        <translation>ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>No log information found!</source>
        <translation>ཉིན་ཐོ་འགོད་པའི་ཆ་འཕྲིན་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
</context>
<context>
    <name>HistoryMessageWindow</name>
    <message>
        <location filename="../view/history_message_window.cpp" line="14"/>
        <location filename="../view/history_message_window.cpp" line="23"/>
        <source>History Messages</source>
        <translation>ལོ་རྒྱུས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>State</source>
        <translation>རྒྱལ་ཁབ།</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Sender</source>
        <translation>སྐུར་མཁན།</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Importance</source>
        <translation>གལ་ཆེའི་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Title</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
</context>
<context>
    <name>InspectWindow</name>
    <message>
        <location filename="../view/inspect_window.cpp" line="14"/>
        <location filename="../view/inspect_window.cpp" line="23"/>
        <location filename="../view/inspect_window.cpp" line="161"/>
        <location filename="../view/inspect_window.cpp" line="276"/>
        <source>Complication Inspect</source>
        <translation>རྙོག་འཛིང་ཆེ་བའི་ཞིབ་བཤེར</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="116"/>
        <source>Repair</source>
        <translation>ཞིག་གསོ་</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="127"/>
        <location filename="../view/inspect_window.cpp" line="133"/>
        <source>Repair tip</source>
        <translation>ཞིག་གསོའི་མན་ངག</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="134"/>
        <location filename="../view/inspect_window.cpp" line="164"/>
        <location filename="../view/inspect_window.cpp" line="281"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="171"/>
        <location filename="../view/inspect_window.cpp" line="237"/>
        <source>Repairing...</source>
        <translation>ཞིག་གསོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="305"/>
        <source>     Inspection passed</source>
        <translation>     ཞིབ་བཤེར་བྱས་ནས་གྲོས་འཆམ་བྱུང</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="55"/>
        <location filename="../view/inspect_window.cpp" line="352"/>
        <source>One key repair</source>
        <translation>ལྡེ་མིག་ཞིག་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="162"/>
        <source>Please wait for the current repair to complete before performing the repair operation!</source>
        <translation>ཞིག་གསོའི་ལས་ཀ་མ་བྱས་གོང་ལ་མིག་སྔར་ཞིག་གསོ་བྱས་ཚར་རྗེས་ད་གཟོད་ལེགས་འགྲུབ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="277"/>
        <source>The current device has not detected integrity detection rules. Please contact the server to issue them!</source>
        <translation>ད་ལྟའི་སྒྲིག་ཆས་ཀྱིས་ད་དུང་ཡིད་རྟོན་ཞིབ་དཔྱད་ཚད་ལེན་གྱི་སྒྲིག་སྲོལ་ལ་ཞིབ་དཔྱད་ཚད་ ཁྱེད་ཀྱིས་ཞབས་ཞུའི་ཡོ་བྱད་ལ་འབྲེལ་གཏུག་བྱས་ནས་ཁོ་ཚོར་འགྲེམས་</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Items</source>
        <translation>རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Baseline</source>
        <translation>གཞི་རྩའི་ཚད་གཞི།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Result</source>
        <translation>མཇུག་འབྲས།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Operate</source>
        <translation>གཉེར་སྐྱོང་།</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="356"/>
        <source>Check Now</source>
        <translation>ད་ལྟ་ཞིབ་བཤེར་བྱ་</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <source>total</source>
        <translation>ཁྱོན་བསྡོམས</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <location filename="../view/item_wid/paginationwid.cpp" line="43"/>
        <source>pages</source>
        <translation>.pages</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="40"/>
        <source>jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>PolicyWindow</name>
    <message>
        <location filename="../view/policy_window.cpp" line="19"/>
        <location filename="../view/policy_window.cpp" line="24"/>
        <source>Policy info</source>
        <translation>སྲིད་ཇུས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="45"/>
        <location filename="../view/policy_window.cpp" line="427"/>
        <source>     Policy Version: Latest</source>
        <translation>     སྲིད་ཇུས་ཀྱི་པར་གཞི། ཆེས་གསར་བ།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Policy</source>
        <translation>སྲིད་ཇུས།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Description</source>
        <translation>གསལ་བཤད།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Version</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Operate</source>
        <translation>གཉེར་སྐྱོང་།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="96"/>
        <source>One key update</source>
        <translation>འགག་རྩའི་གསར་སྒྱུར་ཞིག</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="113"/>
        <source>Loading...</source>
        <translation>དངོས་ཟོག་དབོར་འདྲེན་བྱེད་པ...</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="172"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <location filename="../view/policy_window.cpp" line="304"/>
        <location filename="../view/policy_window.cpp" line="313"/>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <source>Password cannot be empty!</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="191"/>
        <location filename="../view/policy_window.cpp" line="267"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="193"/>
        <location filename="../view/policy_window.cpp" line="269"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="212"/>
        <location filename="../view/policy_window.cpp" line="218"/>
        <source>Lift strategy</source>
        <translation>ཡར་བཀྱགས་ནས་འཐབ་ཇུས</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="225"/>
        <source>verification code</source>
        <translation>ཞིབ་བཤེར་གྱི་ཚབ་རྟགས།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="252"/>
        <source>Please send the code to the administrator to obtain the release code</source>
        <translation>ཚབ་རྟགས་དེ་དོ་དམ་པར་བསྐུར་ནས་ཁྱབ་བསྒྲགས་བྱས་པའི་ཨང་གྲངས་ལེན་རོགས།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="256"/>
        <source>release code</source>
        <translation>ཁྱབ་བསྒྲགས་བྱས་པའི་ཚབ་རྟགས</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="260"/>
        <source>Please enter the release code</source>
        <translation>ཁྱོད་ཀྱིས་ཁྱབ་བསྒྲགས་བྱས་པའི་ཚབ་རྟགས་ནང་འཇུག་</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="304"/>
        <source>The verification code cannot be empty!</source>
        <translation>ཞིབ་བཤེར་གྱི་ཚབ་རྟགས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="313"/>
        <source>Please enter the correct verification code!</source>
        <translation>ཡང་དག་པའི་ཞིབ་བཤེར་ཨང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>Policy update failed</source>
        <translation>སྲིད་ཇུས་གསར་སྒྱུར་ལ་ཕམ་ཉེས་བྱུང</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="647"/>
        <source>error</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="429"/>
        <source>     Policy Version: Not Latest</source>
        <translation>     སྲིད་ཇུས་ཀྱི་པར་གཞི། ཆེས་གསར་བ་མིན།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="164"/>
        <source>User password</source>
        <translation>སྤྱོད་མཁན་གྱི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="495"/>
        <source>Policy cancel</source>
        <translation>སྲིད་ཇུས་མེད་པར་བཟོ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="524"/>
        <source>Wait updates</source>
        <translation>སྒུག་ནས་གནས་ཚུལ་གསར་བར</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="527"/>
        <source>latest</source>
        <translation>ཆེས་གསར་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>ScriptsWarningWindow</name>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="7"/>
        <source>Script Run Warning</source>
        <translation>འཁྲབ་གཞུང་འཁོར་སྐྱོད་ཀྱི་ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="8"/>
        <source>The script is running, please do not shut down or restart!</source>
        <translation>འཁྲབ་གཞུང་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད་པས་སྒོ་རྒྱག་པའམ་ཡང་ན་བསྐྱར་དུ་འགོ་འཛུགས་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>SettingWindow</name>
    <message>
        <location filename="../view/setting_window.cpp" line="17"/>
        <source>App install:</source>
        <translation>ཉེར་སྤྱོད་སྒྲིག་སྦྱོར་བྱས་པ་གཤམ་</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="18"/>
        <source>App run：</source>
        <translation>ཉེར་སྤྱོད་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="25"/>
        <source>Firewall:</source>
        <translation>མེ་འགོག་གྱང་ནི།</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="26"/>
        <source>Hotspot:</source>
        <translation>ཚ་བ་ཆེ་བ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="27"/>
        <source>Network Isolation:</source>
        <translation>དྲ་རྒྱ་ཁེར་རྐྱང་དུ་ལུས་པ</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="34"/>
        <source>USB:</source>
        <translation>USB:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="35"/>
        <source>Mobile:</source>
        <translation>སྒུལ་བདེའི་རང་བཞིན་</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="36"/>
        <source>CD-ROM:</source>
        <translation>CD-ROM:</translation>
    </message>
</context>
<context>
    <name>ShutdownAndReboot</name>
    <message>
        <location filename="../view/shutdown_window.cpp" line="15"/>
        <source>Shutdown</source>
        <translation>ལས་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="17"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས་</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="22"/>
        <location filename="../view/shutdown_window.cpp" line="26"/>
        <source>warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="23"/>
        <source>? After 60 seconds, the computer will automatically</source>
        <translation>? སྐར་ཆ་60འགོར་རྗེས་གློག་ཀླད་ཀྱིས་རང་འགུལ་གྱིས་གློག་ཀླད་གཏོང་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="27"/>
        <source>After 60 seconds, the computer will automatically</source>
        <translation>སྐར་ཆ་60འགོར་རྗེས་གློག་ཀླད་ཀྱིས་རང་འགུལ་གྱིས་གློག་ཀླད་གཏོང་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="33"/>
        <source>Execute immediately</source>
        <translation>འཕྲལ་མར་ལག་བསྟར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="36"/>
        <source>Execute in 5 minutes</source>
        <translation>སྐར་མ་5ཡི་ནང་དུ་ལག་བསྟར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="39"/>
        <source>Remind me in an hour</source>
        <translation>དུས་ཚོད་གཅིག་གི་ནང་དུ་ང་ལ་དྲན་སྐུལ་</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="69"/>
        <source>? After </source>
        <translation>? རྗེས་སུ། </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="70"/>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>s, the computer will automatically </source>
        <translation>s,རྩིས་འཁོར་གྱིས་རང་འགུལ་གྱིས་རྩིས་འཁོར་གཏོང་རྒྱུ་རེད། </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>After </source>
        <translation>རྗེས་སུ། </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="102"/>
        <source>in process of</source>
        <translation>བརྒྱུད་རིམ་ཁྲོད།</translation>
    </message>
</context>
<context>
    <name>Trayicon_left_widget</name>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="61"/>
        <source>kcm client</source>
        <translation>kcm མངགས་བཅོལ་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="63"/>
        <source>scene</source>
        <translation>ཡུལ་དངོས།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="92"/>
        <source>policy update</source>
        <translation>སྲིད་ཇུས་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="136"/>
        <source>connection is normal</source>
        <translation>འབྲེལ་མཐུད་ནི་རྒྱུན་ལྡན་ཡིན།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="218"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="220"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="340"/>
        <source>Connection is normal</source>
        <translation>འབྲེལ་མཐུད་ནི་རྒྱུན་ལྡན་ཡིན།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="343"/>
        <source>Connection is abnormal</source>
        <translation>འབྲེལ་མཐུད་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
</context>
<context>
    <name>Trayicon_widget</name>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="23"/>
        <location filename="../view/trayicon_widget.cpp" line="193"/>
        <source>kcm client</source>
        <translation>kcm མངགས་བཅོལ་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="131"/>
        <source>Complication inspect</source>
        <translation>རྙོག་འཛིང་ཆེ་བའི་ཞིབ་བཤེར</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="133"/>
        <source>Policy setting</source>
        <translation>སྲིད་ཇུས་གཏན་འབེབས།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="135"/>
        <source>Policy info</source>
        <translation>སྲིད་ཇུས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="137"/>
        <source>Log list</source>
        <translation>ཉིན་ཐོའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="139"/>
        <source>History Messages</source>
        <translation>ལོ་རྒྱུས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="141"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="143"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="200"/>
        <source>Version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="202"/>
        <source>kcm client is a software that enables policy download, execution, and data reporting with the kylin terminal control console. Provide system management, log reporting, and other functions for user terminals</source>
        <translation>kcm མངགས་བཅོལ་བྱེད་མཁན་ནི་སྲིད་ཇུས་ཕབ་ལེན་དང་། ལག་བསྟར། གཞི་གྲངས་སྙན་ཞུ་བཅས་བྱེད་ཐུབ་པའི་མཉེན་ཆས་ཤིག་ཡིན། སྤྱོད་མཁན་མཐའ་སྣེར་མ་ལག་དོ་དམ་དང་། ཐོ་འགོད་སྙན་ཞུ། དེ་བཞིན་འགན་ནུས་གཞན་དག་བཅས་འདོན་སྤྲོད་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>UpdateRequest</name>
    <message>
        <location filename="../view/update_request_window.cpp" line="7"/>
        <source>Upgrade Request</source>
        <translation>རིམ་པ་སྤར་བའི་བླང་བྱ</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="8"/>
        <location filename="../view/update_request_window.cpp" line="54"/>
        <source>The server request </source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ཀྱི་བླང་བྱ། </translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="9"/>
        <source> upgrade. Please choose whether to agree or not. (If not selected after 30 seconds, the decision on whether to agree to install will be based on the configuration)</source>
        <translation> རིམ་པ་སྤར་བ་རེད། འཐད་པ་ཡིན་མིན་གདམ་གསེས་བྱེད་རོགས། (གལ་ཏེ་སྐར་ཆ་30ཡི་རྗེས་སུ་བདམས་ཐོན་མ་བྱུང་ན། སྒྲིག་སྦྱོར་བྱ་རྒྱུར་མོས་མཐུན་བྱ་རྒྱུའི་ཐག་བཅད་དེ་བཀོད་སྒྲིག་བྱས་པའི་རྨང་གཞིའི་ཐོག་གཏན་འབེབས་བྱ་རྒྱུ། )</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="15"/>
        <source>Agree</source>
        <translation>ཁ་ཆད་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="17"/>
        <source>Refuse</source>
        <translation>དང་ལེན་མི་བྱེད་</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="55"/>
        <source> upgrade. Please choose whether to agree or not. (After ）</source>
        <translation> རིམ་པ་སྤར་བ་རེད། འཐད་པ་ཡིན་མིན་གདམ་ག་གནང་རོགས། (རྗེས་སུ། )</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="57"/>
        <source>s,if not selected, it will be determined whether to agree to install according to the configuration</source>
        <translation>s,གལ་ཏེ་གདམ་གསེས་མ་བྱས་ན། བཀོད་སྒྲིག་ལྟར་སྒྲིག་སྦྱོར་བྱ་རྒྱུར་མོས་མཐུན་ཡོད་མེད་གཏན་འཁེལ་བྱ་རྒྱུ།</translation>
    </message>
</context>
<context>
    <name>UpdateWindow</name>
    <message>
        <location filename="../view/update_window.cpp" line="7"/>
        <source>System notifications</source>
        <translation>མ་ལག་གི་བརྡ་ཐོ།</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="25"/>
        <source>The system is currently undergoing a unified upgrade or installation of components. You can use it normally, but please do not shut down, restart, or log out</source>
        <translation>མ་ལག་འདི་མིག་སྔར་གཅིག་གྱུར་གྱིས་ལྷུ་ལག་རིམ་སྤར་དང་སྒྲིག་སྦྱོར་བྱེད་བཞིན་ཡོད། ཁྱེད་ཚོས་རྒྱུན་ལྡན་ལྟར་བཀོལ་སྤྱོད་བྱས་ཆོགའོན་ཀྱང་སྒོ་རྒྱག་པ་དང་། བསྐྱར་དུ་འགོ་འཛུགས་པ། ཐོ་འགོད་བཅས་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="29"/>
        <source>Getting system apt cache data</source>
        <translation>མ་ལག་གི་མྱུར་ཚད་མགྱོགས་སུ་གཏོང་བའི་གཞི་གྲངས</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="66"/>
        <source>Opening system apt cache</source>
        <translation>སྒོ་འབྱེད་མ་ལག་གི་མྱུར་ཚད་མགྱོགས་སུ་</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="78"/>
        <source>System upgrade failed, After </source>
        <translation>མ་ལག་རིམ་སྤར་ལ་ཕམ་ཉེས་བྱུང་རྗེས། </translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="79"/>
        <source>s,close the window</source>
        <translation>s,སྒེའུ་ཁུང་གི་སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="82"/>
        <source>Updating apt cache: apt update</source>
        <translation>apt cache: apt update</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="83"/>
        <source>speed：</source>
        <translation>མྱུར་ཚད་ནི།</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="85"/>
        <source>Performing download and installation</source>
        <translation>ཕབ་ལེན་དང་སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="95"/>
        <source>Download completed! About to automatically close.</source>
        <translation>ཕབ་ལེན་ལེགས་འགྲུབ་བྱུང་སོང་། རང་འགུལ་གྱིས་སྒོ་རྒྱག་གྲབས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="120"/>
        <source>System upgrade failed,after</source>
        <translation>མ་ལག་རིམ་སྤར་བྱེད་པར་ཕམ་ཉེས་བྱུང་རྗེས།</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="121"/>
        <source>s,close the window！</source>
        <translation>s,སྒེའུ་ཁུང་གི་སྒོ་རྒྱག་དགོས།</translation>
    </message>
</context>
<context>
    <name>User_information_table</name>
    <message>
        <location filename="../view/user_information_table.cpp" line="75"/>
        <source>IP</source>
        <translation>ཤེས་བྱའི་ཐོན་དངོས</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="78"/>
        <source>Login Name</source>
        <translation>ཐོ་འགོད་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="81"/>
        <source>Host Name</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="84"/>
        <source>Terminal ORG</source>
        <translation>མཐའ་སྣེའི་ORG</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="87"/>
        <source>Local User</source>
        <translation>ས་གནས་དེ་གའི་སྤྱོད་མཁན</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="91"/>
        <source>Policy Time</source>
        <translation>སྲིད་ཇུས་ཀྱི་དུས་ཚོད།</translation>
    </message>
</context>
</TS>
