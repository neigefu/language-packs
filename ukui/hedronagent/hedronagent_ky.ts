<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>DetailMessageWindow</name>
    <message>
        <location filename="../view/detail_message_window.cpp" line="10"/>
        <source>Message Notification</source>
        <translation>Билдирүү билдирүү</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="14"/>
        <source>title</source>
        <translation>аталышы</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="15"/>
        <source>content</source>
        <translation>мазмуну</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="16"/>
        <source>sender</source>
        <translation>жөнөтүүчү</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="17"/>
        <source>time</source>
        <translation>убакыт</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="521"/>
        <source>jump to</source>
        <translation>секирүү</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="523"/>
        <source>pages</source>
        <translation>.pages</translation>
    </message>
</context>
<context>
    <name>HistoryLogWindow</name>
    <message>
        <location filename="../view/history_log_window.cpp" line="13"/>
        <location filename="../view/history_log_window.cpp" line="22"/>
        <source>Log List</source>
        <translation>Кирүү тизмеси</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Time</source>
        <translation>Убакыт</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>User</source>
        <translation>Колдонуучу</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Log Type</source>
        <translation>Кирүү түрү</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Content</source>
        <translation>Мазмуну</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>No log information found!</source>
        <translation>Эч кандай журнал маалымат табылган жок!</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>warning</source>
        <translation>эскертүү</translation>
    </message>
</context>
<context>
    <name>HistoryMessageWindow</name>
    <message>
        <location filename="../view/history_message_window.cpp" line="14"/>
        <location filename="../view/history_message_window.cpp" line="23"/>
        <source>History Messages</source>
        <translation>Тарых билдирүүлөрү</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>State</source>
        <translation>Мамлекеттик</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Time</source>
        <translation>Убакыт</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Sender</source>
        <translation>Жөнөтүүчү</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Importance</source>
        <translation>Маанилүүлүгү</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Title</source>
        <translation>Аталышы</translation>
    </message>
</context>
<context>
    <name>InspectWindow</name>
    <message>
        <location filename="../view/inspect_window.cpp" line="14"/>
        <location filename="../view/inspect_window.cpp" line="23"/>
        <location filename="../view/inspect_window.cpp" line="161"/>
        <location filename="../view/inspect_window.cpp" line="276"/>
        <source>Complication Inspect</source>
        <translation>Асқынуу текшерүү</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="116"/>
        <source>Repair</source>
        <translation>Оңдоп-түз</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="127"/>
        <location filename="../view/inspect_window.cpp" line="133"/>
        <source>Repair tip</source>
        <translation>Оңдоп-түзөө кеңеши</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="134"/>
        <location filename="../view/inspect_window.cpp" line="164"/>
        <location filename="../view/inspect_window.cpp" line="281"/>
        <source>OK</source>
        <translation>МАКУЛ</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="171"/>
        <location filename="../view/inspect_window.cpp" line="237"/>
        <source>Repairing...</source>
        <translation>Оңдоп-түзөө...</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="305"/>
        <source>     Inspection passed</source>
        <translation>     Текшерүүдөн өттү</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="55"/>
        <location filename="../view/inspect_window.cpp" line="352"/>
        <source>One key repair</source>
        <translation>Бир негизги оңдоо</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="162"/>
        <source>Please wait for the current repair to complete before performing the repair operation!</source>
        <translation>Сураныч, оңдоп-түзөө операциясын аткарар алдында учурдагы оңдоп-түзөөнүн аякташын күткүлө!</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="277"/>
        <source>The current device has not detected integrity detection rules. Please contact the server to issue them!</source>
        <translation>Учурдагы аппарат бүтүндүгүн аныктоо эрежелерин аныктаган жок. Аларды чыгаруу үчүн серверге кайрылыңыздар!</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Items</source>
        <translation>Элементтер</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Baseline</source>
        <translation>Базалык</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Result</source>
        <translation>Натыйжасы</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Operate</source>
        <translation>Иштөө</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="356"/>
        <source>Check Now</source>
        <translation>Азыр текшерүү</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <source>total</source>
        <translation>бардыгы</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <location filename="../view/item_wid/paginationwid.cpp" line="43"/>
        <source>pages</source>
        <translation>.pages</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="40"/>
        <source>jump to</source>
        <translation>секирүү</translation>
    </message>
</context>
<context>
    <name>PolicyWindow</name>
    <message>
        <location filename="../view/policy_window.cpp" line="19"/>
        <location filename="../view/policy_window.cpp" line="24"/>
        <source>Policy info</source>
        <translation>Саясат маалыматы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="45"/>
        <location filename="../view/policy_window.cpp" line="427"/>
        <source>     Policy Version: Latest</source>
        <translation>     Саясат версиясы: Акыркы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Policy</source>
        <translation>Саясат</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Description</source>
        <translation>Сүрөттөлүшү</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Version</source>
        <translation>Версиясы</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Operate</source>
        <translation>Иштөө</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="96"/>
        <source>One key update</source>
        <translation>Негизги жаңыртуулардын бири</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="113"/>
        <source>Loading...</source>
        <translation>Жүктөө...</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="172"/>
        <source>Password</source>
        <translation>Сырсөз</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <location filename="../view/policy_window.cpp" line="304"/>
        <location filename="../view/policy_window.cpp" line="313"/>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>warning</source>
        <translation>эскертүү</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <source>Password cannot be empty!</source>
        <translation>Сырсөз бош болушу мүмкүн эмес!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="191"/>
        <location filename="../view/policy_window.cpp" line="267"/>
        <source>OK</source>
        <translation>МАКУЛ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="193"/>
        <location filename="../view/policy_window.cpp" line="269"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="212"/>
        <location filename="../view/policy_window.cpp" line="218"/>
        <source>Lift strategy</source>
        <translation>Стратегияны көтөрүү</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="225"/>
        <source>verification code</source>
        <translation>Текшерүү коду</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="252"/>
        <source>Please send the code to the administrator to obtain the release code</source>
        <translation>Чыгаруу кодун алуу үчүн кодду администраторго жөнөтүңүз</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="256"/>
        <source>release code</source>
        <translation>кодду чыгаруу</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="260"/>
        <source>Please enter the release code</source>
        <translation>Чыгаруу кодун киргизиңиз</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="304"/>
        <source>The verification code cannot be empty!</source>
        <translation>Текшерүү коду бош болушу мүмкүн эмес!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="313"/>
        <source>Please enter the correct verification code!</source>
        <translation>Туура текшерүү кодун киргизиңиз!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>Policy update failed</source>
        <translation>Саясатты жаңылоо ишке ашпады</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="647"/>
        <source>error</source>
        <translation>ката</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="429"/>
        <source>     Policy Version: Not Latest</source>
        <translation>     Саясат версиясы: Акыркы эмес</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="164"/>
        <source>User password</source>
        <translation>Колдонуучу сырсөз</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="495"/>
        <source>Policy cancel</source>
        <translation>Саясат жокко чыгарылган</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="524"/>
        <source>Wait updates</source>
        <translation>Жаңыртууларды күтүү</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="527"/>
        <source>latest</source>
        <translation>акыркы</translation>
    </message>
</context>
<context>
    <name>ScriptsWarningWindow</name>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="7"/>
        <source>Script Run Warning</source>
        <translation>Эскертүү скрипт</translation>
    </message>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="8"/>
        <source>The script is running, please do not shut down or restart!</source>
        <translation>Скрипт иштеп жатат, сураныч, өчүрүп же кайра иштетпейт!</translation>
    </message>
</context>
<context>
    <name>SettingWindow</name>
    <message>
        <location filename="../view/setting_window.cpp" line="17"/>
        <source>App install:</source>
        <translation>Тиркемени орнотуу:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="18"/>
        <source>App run：</source>
        <translation>Тиркемени иштетүү:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="25"/>
        <source>Firewall:</source>
        <translation>Брандмауэр:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="26"/>
        <source>Hotspot:</source>
        <translation>Хотспот:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="27"/>
        <source>Network Isolation:</source>
        <translation>Тармактык изоляция:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="34"/>
        <source>USB:</source>
        <translation>USB:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="35"/>
        <source>Mobile:</source>
        <translation>Мобилдик:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="36"/>
        <source>CD-ROM:</source>
        <translation>CD-ROM:</translation>
    </message>
</context>
<context>
    <name>ShutdownAndReboot</name>
    <message>
        <location filename="../view/shutdown_window.cpp" line="15"/>
        <source>Shutdown</source>
        <translation>Өчүрүү</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="17"/>
        <source>Restart</source>
        <translation>Кайра иштетүү</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="22"/>
        <location filename="../view/shutdown_window.cpp" line="26"/>
        <source>warning</source>
        <translation>эскертүү</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="23"/>
        <source>? After 60 seconds, the computer will automatically</source>
        <translation>? 60 секуд өткөндөн кийин, компьютер автоматтык түрдө болот</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="27"/>
        <source>After 60 seconds, the computer will automatically</source>
        <translation>60 секуд өткөндөн кийин, компьютер автоматтык түрдө болот</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="33"/>
        <source>Execute immediately</source>
        <translation>Дароо аткаруу</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="36"/>
        <source>Execute in 5 minutes</source>
        <translation>5 мүнөттө аткаруу</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="39"/>
        <source>Remind me in an hour</source>
        <translation>Бир сааттын ичинде эсиме сал</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="69"/>
        <source>? After </source>
        <translation>? Кийин </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="70"/>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>s, the computer will automatically </source>
        <translation>с, компьютер автоматтык түрдө болот </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>After </source>
        <translation>Кийин </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="102"/>
        <source>in process of</source>
        <translation>процесстин жүрүшүндө</translation>
    </message>
</context>
<context>
    <name>Trayicon_left_widget</name>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="61"/>
        <source>kcm client</source>
        <translation>км кардар</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="63"/>
        <source>scene</source>
        <translation>сахна</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="92"/>
        <source>policy update</source>
        <translation>саясат жаңыртуу</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="136"/>
        <source>connection is normal</source>
        <translation>туташтыруу нормалдуу</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="218"/>
        <source>OK</source>
        <translation>МАКУЛ</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="220"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="340"/>
        <source>Connection is normal</source>
        <translation>Туташуу нормалдуу</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="343"/>
        <source>Connection is abnormal</source>
        <translation>Туташуу аномалдуу</translation>
    </message>
</context>
<context>
    <name>Trayicon_widget</name>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="23"/>
        <location filename="../view/trayicon_widget.cpp" line="193"/>
        <source>kcm client</source>
        <translation>км кардар</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="131"/>
        <source>Complication inspect</source>
        <translation>Асқынуу текшерүү</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="133"/>
        <source>Policy setting</source>
        <translation>Саясат орнотуу</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="135"/>
        <source>Policy info</source>
        <translation>Саясат маалыматы</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="137"/>
        <source>Log list</source>
        <translation>Кирүү тизмеси</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="139"/>
        <source>History Messages</source>
        <translation>Тарых билдирүүлөрү</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="141"/>
        <source>About</source>
        <translation>Жөнүндө</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="143"/>
        <source>Exit</source>
        <translation>Чыгуу</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="200"/>
        <source>Version:</source>
        <translation>Версиясы:</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="202"/>
        <source>kcm client is a software that enables policy download, execution, and data reporting with the kylin terminal control console. Provide system management, log reporting, and other functions for user terminals</source>
        <translation>kcm кардар - бул килин терминалын башкаруу консолу менен саясат жүктөп, аткарууга жана маалыматтарды билдирүүгө мүмкүндүк берген программалык камсыздоо. Системаны башкарууну, журналдын отчетторун жана колдонуучу терминалдар үчүн башка функцияларды камсыз кылуу</translation>
    </message>
</context>
<context>
    <name>UpdateRequest</name>
    <message>
        <location filename="../view/update_request_window.cpp" line="7"/>
        <source>Upgrade Request</source>
        <translation>Жогорулатуу өтүнүчү</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="8"/>
        <location filename="../view/update_request_window.cpp" line="54"/>
        <source>The server request </source>
        <translation>Сервердин өтүнүчү </translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="9"/>
        <source> upgrade. Please choose whether to agree or not. (If not selected after 30 seconds, the decision on whether to agree to install will be based on the configuration)</source>
        <translation> жогорулатуу. Сураныч, макулбу же жокпу, тандап алыңыз. (Эгерде 30 секундадан кийин тандалбаса, орнотууга макул болуу жөнүндө чечим конфигурацияга негизделет)</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="15"/>
        <source>Agree</source>
        <translation>Макул болгула</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="17"/>
        <source>Refuse</source>
        <translation>Баш тартуу</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="55"/>
        <source> upgrade. Please choose whether to agree or not. (After ）</source>
        <translation> жогорулатуу. Макул болууну же макул болбоону тандаңыз. (Кийин )</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="57"/>
        <source>s,if not selected, it will be determined whether to agree to install according to the configuration</source>
        <translation>с,тандалбаса, конфигурацияга ылайык орнотууга макул болуу аныкталат</translation>
    </message>
</context>
<context>
    <name>UpdateWindow</name>
    <message>
        <location filename="../view/update_window.cpp" line="7"/>
        <source>System notifications</source>
        <translation>Системалык билдирүүлөр</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="25"/>
        <source>The system is currently undergoing a unified upgrade or installation of components. You can use it normally, but please do not shut down, restart, or log out</source>
        <translation>Система азыркы учурда компоненттерди бирдиктүү жогорулатуу же орнотуудан өтүүдө. Сиз аны нормалдуу колдоно аласыз, бирок, сураныч, өчүрүп, кайра иштетүү, же кирүү эмес,</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="29"/>
        <source>Getting system apt cache data</source>
        <translation>Система апт кэш маалыматтарын алуу</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="66"/>
        <source>Opening system apt cache</source>
        <translation>Ачуу системасы апт кэш</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="78"/>
        <source>System upgrade failed, After </source>
        <translation>Системаны жогорулатуу ишке ашпады, кийин </translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="79"/>
        <source>s,close the window</source>
        <translation>с,терезени жабуу</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="82"/>
        <source>Updating apt cache: apt update</source>
        <translation>Апт кэшти жаңылоо: апт жаңыртуу</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="83"/>
        <source>speed：</source>
        <translation>ылдамдыгы:</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="85"/>
        <source>Performing download and installation</source>
        <translation>Жүктөө жана орнотуу аткаруу</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="95"/>
        <source>Download completed! About to automatically close.</source>
        <translation>Жүктөп алуу аяктады! Автоматтык түрдө жабуу жөнүндө.</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="120"/>
        <source>System upgrade failed,after</source>
        <translation>Системаны жогорулатуу ишке ашпады,кийин</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="121"/>
        <source>s,close the window！</source>
        <translation>с,терезени жабуу!</translation>
    </message>
</context>
<context>
    <name>User_information_table</name>
    <message>
        <location filename="../view/user_information_table.cpp" line="75"/>
        <source>IP</source>
        <translation>ИП</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="78"/>
        <source>Login Name</source>
        <translation>Кирүү аты</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="81"/>
        <source>Host Name</source>
        <translation>Хост аты</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="84"/>
        <source>Terminal ORG</source>
        <translation>Орг терминалы</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="87"/>
        <source>Local User</source>
        <translation>Жергиликтүү колдонуучу</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="91"/>
        <source>Policy Time</source>
        <translation>Саясат убактысы</translation>
    </message>
</context>
</TS>
