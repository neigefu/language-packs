<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>DetailMessageWindow</name>
    <message>
        <location filename="../view/detail_message_window.cpp" line="10"/>
        <source>Message Notification</source>
        <translation>消息通知</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="14"/>
        <source>title</source>
        <translation>標題</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="15"/>
        <source>content</source>
        <translation>內容</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="16"/>
        <source>sender</source>
        <translation>寄件者</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="17"/>
        <source>time</source>
        <translation>時間</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="521"/>
        <source>jump to</source>
        <translation>跳轉到</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="523"/>
        <source>pages</source>
        <translation>。.pages</translation>
    </message>
</context>
<context>
    <name>HistoryLogWindow</name>
    <message>
        <location filename="../view/history_log_window.cpp" line="13"/>
        <location filename="../view/history_log_window.cpp" line="22"/>
        <source>Log List</source>
        <translation>日誌清單</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Time</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>User</source>
        <translation>使用者</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Log Type</source>
        <translation>日誌類型</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Content</source>
        <translation>內容</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>No log information found!</source>
        <translation>未找到日誌資訊！</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
</context>
<context>
    <name>HistoryMessageWindow</name>
    <message>
        <location filename="../view/history_message_window.cpp" line="14"/>
        <location filename="../view/history_message_window.cpp" line="23"/>
        <source>History Messages</source>
        <translation>歷史消息</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>State</source>
        <translation>州</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Time</source>
        <translation>時間</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Sender</source>
        <translation>寄件者</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Importance</source>
        <translation>重要性</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Title</source>
        <translation>標題</translation>
    </message>
</context>
<context>
    <name>InspectWindow</name>
    <message>
        <location filename="../view/inspect_window.cpp" line="14"/>
        <location filename="../view/inspect_window.cpp" line="23"/>
        <location filename="../view/inspect_window.cpp" line="161"/>
        <location filename="../view/inspect_window.cpp" line="276"/>
        <source>Complication Inspect</source>
        <translation>併發症檢查</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="116"/>
        <source>Repair</source>
        <translation>修</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="127"/>
        <location filename="../view/inspect_window.cpp" line="133"/>
        <source>Repair tip</source>
        <translation>維修提示</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="134"/>
        <location filename="../view/inspect_window.cpp" line="164"/>
        <location filename="../view/inspect_window.cpp" line="281"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="171"/>
        <location filename="../view/inspect_window.cpp" line="237"/>
        <source>Repairing...</source>
        <translation>修復。。。</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="305"/>
        <source>     Inspection passed</source>
        <translation>     檢查通過</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="55"/>
        <location filename="../view/inspect_window.cpp" line="352"/>
        <source>One key repair</source>
        <translation>一鍵修復</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="162"/>
        <source>Please wait for the current repair to complete before performing the repair operation!</source>
        <translation>請等待當前修復完成，然後再執行修復操作！</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="277"/>
        <source>The current device has not detected integrity detection rules. Please contact the server to issue them!</source>
        <translation>當前設備未檢測到完整性檢測規則。請聯繫伺服器發出它們！</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Items</source>
        <translation>專案</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Baseline</source>
        <translation>基線</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Result</source>
        <translation>結果</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="356"/>
        <source>Check Now</source>
        <translation>立即檢查</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <source>total</source>
        <translation>總</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <location filename="../view/item_wid/paginationwid.cpp" line="43"/>
        <source>pages</source>
        <translation>。.pages</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="40"/>
        <source>jump to</source>
        <translation>跳轉到</translation>
    </message>
</context>
<context>
    <name>PolicyWindow</name>
    <message>
        <location filename="../view/policy_window.cpp" line="19"/>
        <location filename="../view/policy_window.cpp" line="24"/>
        <source>Policy info</source>
        <translation>政策資訊</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="45"/>
        <location filename="../view/policy_window.cpp" line="427"/>
        <source>     Policy Version: Latest</source>
        <translation>     策略版本：最新</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Policy</source>
        <translation>政策</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="96"/>
        <source>One key update</source>
        <translation>一鍵更新</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="113"/>
        <source>Loading...</source>
        <translation>裝載。。。</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="172"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <location filename="../view/policy_window.cpp" line="304"/>
        <location filename="../view/policy_window.cpp" line="313"/>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <source>Password cannot be empty!</source>
        <translation>密碼不能為空！</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="191"/>
        <location filename="../view/policy_window.cpp" line="267"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="193"/>
        <location filename="../view/policy_window.cpp" line="269"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="212"/>
        <location filename="../view/policy_window.cpp" line="218"/>
        <source>Lift strategy</source>
        <translation>提升策略</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="225"/>
        <source>verification code</source>
        <translation>驗證碼</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="252"/>
        <source>Please send the code to the administrator to obtain the release code</source>
        <translation>請將代碼發送給管理員以獲取發佈代碼</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="256"/>
        <source>release code</source>
        <translation>發佈代碼</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="260"/>
        <source>Please enter the release code</source>
        <translation>請輸入發佈代碼</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="304"/>
        <source>The verification code cannot be empty!</source>
        <translation>驗證碼不能為空！</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="313"/>
        <source>Please enter the correct verification code!</source>
        <translation>請輸入正確的驗證碼！</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>Policy update failed</source>
        <translation>策略更新失敗</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="647"/>
        <source>error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="429"/>
        <source>     Policy Version: Not Latest</source>
        <translation>     策略版本：不是最新版本</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="164"/>
        <source>User password</source>
        <translation>用戶密碼</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="495"/>
        <source>Policy cancel</source>
        <translation>政策取消</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="524"/>
        <source>Wait updates</source>
        <translation>等待更新</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="527"/>
        <source>latest</source>
        <translation>最近的</translation>
    </message>
</context>
<context>
    <name>ScriptsWarningWindow</name>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="7"/>
        <source>Script Run Warning</source>
        <translation>腳本運行警告</translation>
    </message>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="8"/>
        <source>The script is running, please do not shut down or restart!</source>
        <translation>腳本正在運行，請不要關閉或重新啟動！</translation>
    </message>
</context>
<context>
    <name>SettingWindow</name>
    <message>
        <location filename="../view/setting_window.cpp" line="17"/>
        <source>App install:</source>
        <translation>套用安裝：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="18"/>
        <source>App run：</source>
        <translation>套用執行：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="25"/>
        <source>Firewall:</source>
        <translation>防火牆：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="26"/>
        <source>Hotspot:</source>
        <translation>熱點：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="27"/>
        <source>Network Isolation:</source>
        <translation>網路隔離：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="34"/>
        <source>USB:</source>
        <translation>介面：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="35"/>
        <source>Mobile:</source>
        <translation>移動：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="36"/>
        <source>CD-ROM:</source>
        <translation>光碟：</translation>
    </message>
</context>
<context>
    <name>ShutdownAndReboot</name>
    <message>
        <location filename="../view/shutdown_window.cpp" line="15"/>
        <source>Shutdown</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="17"/>
        <source>Restart</source>
        <translation>重新啟動</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="22"/>
        <location filename="../view/shutdown_window.cpp" line="26"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="23"/>
        <source>? After 60 seconds, the computer will automatically</source>
        <translation>?60秒后，計算機將自動</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="27"/>
        <source>After 60 seconds, the computer will automatically</source>
        <translation>60秒后，計算機將自動</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="33"/>
        <source>Execute immediately</source>
        <translation>立即執行</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="36"/>
        <source>Execute in 5 minutes</source>
        <translation>5 分鐘內執行</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="39"/>
        <source>Remind me in an hour</source>
        <translation>一小時後提醒我</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="69"/>
        <source>? After </source>
        <translation>?後 </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="70"/>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>s, the computer will automatically </source>
        <translation>s，電腦會自動 </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>After </source>
        <translation>後 </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="102"/>
        <source>in process of</source>
        <translation>在進行中</translation>
    </message>
</context>
<context>
    <name>Trayicon_left_widget</name>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="61"/>
        <source>kcm client</source>
        <translation>KCM 用戶端</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="63"/>
        <source>scene</source>
        <translation>現場</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="92"/>
        <source>policy update</source>
        <translation>政策更新</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="136"/>
        <source>connection is normal</source>
        <translation>連接正常</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="218"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="220"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="340"/>
        <source>Connection is normal</source>
        <translation>連接正常</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="343"/>
        <source>Connection is abnormal</source>
        <translation>連接異常</translation>
    </message>
</context>
<context>
    <name>Trayicon_widget</name>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="23"/>
        <location filename="../view/trayicon_widget.cpp" line="193"/>
        <source>kcm client</source>
        <translation>KCM 用戶端</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="131"/>
        <source>Complication inspect</source>
        <translation>併發症檢查</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="133"/>
        <source>Policy setting</source>
        <translation>策略設置</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="135"/>
        <source>Policy info</source>
        <translation>政策資訊</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="137"/>
        <source>Log list</source>
        <translation>日誌清單</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="139"/>
        <source>History Messages</source>
        <translation>歷史消息</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="141"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="143"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="200"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="202"/>
        <source>kcm client is a software that enables policy download, execution, and data reporting with the kylin terminal control console. Provide system management, log reporting, and other functions for user terminals</source>
        <translation>KCM用戶端是一款通過麒麟終端控制台實現策略下載、執行和數據上報的軟體。為使用者終端提供系統管理、日誌上報等功能</translation>
    </message>
</context>
<context>
    <name>UpdateRequest</name>
    <message>
        <location filename="../view/update_request_window.cpp" line="7"/>
        <source>Upgrade Request</source>
        <translation>升級請求</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="8"/>
        <location filename="../view/update_request_window.cpp" line="54"/>
        <source>The server request </source>
        <translation>伺服器請求 </translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="9"/>
        <source> upgrade. Please choose whether to agree or not. (If not selected after 30 seconds, the decision on whether to agree to install will be based on the configuration)</source>
        <translation> 升級。請選擇是否同意。 （如果 30 秒後未選擇，則是否同意安裝的決定將取決於設定）</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="15"/>
        <source>Agree</source>
        <translation>同意</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="17"/>
        <source>Refuse</source>
        <translation>拒絕</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="55"/>
        <source> upgrade. Please choose whether to agree or not. (After ）</source>
        <translation> 升級。請選擇是否同意。</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="57"/>
        <source>s,if not selected, it will be determined whether to agree to install according to the configuration</source>
        <translation>s，如果未選擇，將根據配置決定是否同意安裝</translation>
    </message>
</context>
<context>
    <name>UpdateWindow</name>
    <message>
        <location filename="../view/update_window.cpp" line="7"/>
        <source>System notifications</source>
        <translation>系統通知</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="25"/>
        <source>The system is currently undergoing a unified upgrade or installation of components. You can use it normally, but please do not shut down, restart, or log out</source>
        <translation>系統目前正在進行元件的統一升級或安裝。您可以正常使用，但請不要關機、重啟或註銷</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="29"/>
        <source>Getting system apt cache data</source>
        <translation>獲取系統快取數據</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="66"/>
        <source>Opening system apt cache</source>
        <translation>打開系統快取</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="78"/>
        <source>System upgrade failed, After </source>
        <translation>系統升級失敗，之後 </translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="79"/>
        <source>s,close the window</source>
        <translation>s，關閉視窗</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="82"/>
        <source>Updating apt cache: apt update</source>
        <translation>更新apt緩存：apt更新</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="83"/>
        <source>speed：</source>
        <translation>速度：</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="85"/>
        <source>Performing download and installation</source>
        <translation>執行下載和安裝</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="95"/>
        <source>Download completed! About to automatically close.</source>
        <translation>下載完成！即將自動關閉。</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="120"/>
        <source>System upgrade failed,after</source>
        <translation>系統升級失敗，之後</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="121"/>
        <source>s,close the window！</source>
        <translation>s，關上窗戶！</translation>
    </message>
</context>
<context>
    <name>User_information_table</name>
    <message>
        <location filename="../view/user_information_table.cpp" line="75"/>
        <source>IP</source>
        <translation>智慧財產權</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="78"/>
        <source>Login Name</source>
        <translation>登錄名</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="81"/>
        <source>Host Name</source>
        <translation>主機名</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="84"/>
        <source>Terminal ORG</source>
        <translation>終端組織</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="87"/>
        <source>Local User</source>
        <translation>本地使用者</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="91"/>
        <source>Policy Time</source>
        <translation>策略時間</translation>
    </message>
</context>
</TS>
