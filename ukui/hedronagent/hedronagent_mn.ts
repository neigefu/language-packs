<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>DetailMessageWindow</name>
    <message>
        <location filename="../view/detail_message_window.cpp" line="10"/>
        <source>Message Notification</source>
        <translation>ᠮᠡᠳᠡᠭᠡ ᠮᠡᠳᠡᠭᠳᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="14"/>
        <source>title</source>
        <translation>ᠲᠸᠺᠰᠲ</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="15"/>
        <source>content</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="16"/>
        <source>sender</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠥᠮᠥᠨ ᠢ ᠶᠠᠪᠤᠭᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="17"/>
        <source>time</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="521"/>
        <source>jump to</source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠦᠰᠦᠷᠦᠬᠡᠳ</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="523"/>
        <source>pages</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>HistoryLogWindow</name>
    <message>
        <location filename="../view/history_log_window.cpp" line="13"/>
        <location filename="../view/history_log_window.cpp" line="22"/>
        <source>Log List</source>
        <translation>ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠦᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠳᠦ ᠵᠢᠭᠰᠠᠭᠠᠭᠳᠠ</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Time</source>
        <translation>ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Log Type</source>
        <translation>ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Content</source>
        <translation>ᠯᠠᠪᠯᠠᠬᠤ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>No log information found!</source>
        <translation>ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠯ ᠦᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠶᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>HistoryMessageWindow</name>
    <message>
        <location filename="../view/history_message_window.cpp" line="14"/>
        <location filename="../view/history_message_window.cpp" line="23"/>
        <source>History Messages</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>State</source>
        <translation>ᠵᠧᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Time</source>
        <translation>ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Sender</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠥᠮᠥᠨ ᠢ ᠶᠠᠪᠤᠭᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Importance</source>
        <translation>ᠴᠢᠬᠤᠯᠠ ᠴᠢᠨᠠᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Title</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>InspectWindow</name>
    <message>
        <location filename="../view/inspect_window.cpp" line="14"/>
        <location filename="../view/inspect_window.cpp" line="23"/>
        <location filename="../view/inspect_window.cpp" line="161"/>
        <location filename="../view/inspect_window.cpp" line="276"/>
        <source>Complication Inspect</source>
        <translation>ᠪᠤᠳᠤᠯᠢᠶᠠᠨᠲᠠᠢ ᠴᠢᠳᠠᠮᠵᠢ ᠶᠢᠨ ᠪᠠᠶᠢᠴᠠᠭᠠᠯᠲᠠ᠃</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="116"/>
        <source>Repair</source>
        <translation>ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠰᠦᠢᠳᠦᠯ᠎ᠢ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="127"/>
        <location filename="../view/inspect_window.cpp" line="133"/>
        <source>Repair tip</source>
        <translation>ᠪᠢᠴᠢᠬᠠᠨ ᠨᠠᠭᠠᠭᠴᠢ ᠶᠢ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="134"/>
        <location filename="../view/inspect_window.cpp" line="164"/>
        <location filename="../view/inspect_window.cpp" line="281"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="171"/>
        <location filename="../view/inspect_window.cpp" line="237"/>
        <source>Repairing...</source>
        <translation>ᠶᠠᠭ ᠤᠳᠤ ᠵᠠᠰᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="305"/>
        <source>     Inspection passed</source>
        <translation>     ᠰᠢᠯᠭᠠᠨ ᠪᠠᠶᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠲᠤᠯᠠᠭᠳᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="55"/>
        <location filename="../view/inspect_window.cpp" line="352"/>
        <source>One key repair</source>
        <translation>ᠨᠢᠭᠡ ᠲᠠᠷᠤᠭᠤᠯ ᠢ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="162"/>
        <source>Please wait for the current repair to complete before performing the repair operation!</source>
        <translation>ᠣᠳᠣ ᠶᠢᠨ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠᠵᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠨ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠᠬᠤ ᠠᠵᠢᠯ ᠬᠢᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="277"/>
        <source>The current device has not detected integrity detection rules. Please contact the server to issue them!</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢ ᠪᠦᠷᠢᠨ ᠪᠦᠲᠦᠨ ᠴᠢᠨᠠᠷ ᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠳᠦᠷᠢᠮ ᠳᠦ ᠬᠦᠷᠦᠭᠰᠡᠨ ᠦᠭᠡᠶ ᠃ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Items</source>
        <translation>ᠲᠦᠷᠦᠯ ᠵᠦᠢᠯ᠄</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Baseline</source>
        <translation>ᠰᠠᠭᠤᠷᠢ ᠱᠤᠭᠤᠮ</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Result</source>
        <translation>ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠃</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Operate</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="356"/>
        <source>Check Now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠲᠦᠷᠭᠡᠨ ᠪᠠᠢ᠌ᠴᠠᠭᠠᠵᠤ ᠦᠵᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <source>total</source>
        <translation>ᠨᠡᠢᠲᠡ</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <location filename="../view/item_wid/paginationwid.cpp" line="43"/>
        <source>pages</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="40"/>
        <source>jump to</source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠦᠰᠦᠷᠦᠬᠡᠳ</translation>
    </message>
</context>
<context>
    <name>PolicyWindow</name>
    <message>
        <location filename="../view/policy_window.cpp" line="19"/>
        <location filename="../view/policy_window.cpp" line="24"/>
        <source>Policy info</source>
        <translation>ᠲᠥᠷᠥ ᠶᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="45"/>
        <location filename="../view/policy_window.cpp" line="427"/>
        <source>     Policy Version: Latest</source>
        <translation>     ᠠᠷᠭ᠎ᠠ ᠪᠤᠲᠤᠯᠭ᠎ᠠ ᠶᠢᠨ ᠬᠡᠪᠯᠡᠯ  ᠬᠠᠮᠤᠭ ᠰᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Policy</source>
        <translation>ᠭᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Description</source>
        <translation>ᠳᠦᠷᠰᠦᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Operate</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="96"/>
        <source>One key update</source>
        <translation>ᠨᠢᠭᠡ ᠲᠠᠷᠤᠭᠤᠯ ᠰᠢᠨᠡᠳᠭᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="113"/>
        <source>Loading...</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠃...</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="172"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <location filename="../view/policy_window.cpp" line="304"/>
        <location filename="../view/policy_window.cpp" line="313"/>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <source>Password cannot be empty!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="191"/>
        <location filename="../view/policy_window.cpp" line="267"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="193"/>
        <location filename="../view/policy_window.cpp" line="269"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="212"/>
        <location filename="../view/policy_window.cpp" line="218"/>
        <source>Lift strategy</source>
        <translation>ᠠᠷᠭ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠪᠠᠨ ᠳᠡᠭᠡᠭᠰᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="225"/>
        <source>verification code</source>
        <translation>ᠤᠬᠤᠷ ᠮᠡᠳᠡᠭᠡᠨ᠎ᠦ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="252"/>
        <source>Please send the code to the administrator to obtain the release code</source>
        <translation>ᠣᠷᠣᠯᠠᠬᠤ ᠨᠣᠮᠧᠷ ᠢ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠳᠤ ᠬᠦᠷᠭᠡᠵᠦ ᠨᠡᠶᠢᠲᠡᠯᠡᠭᠰᠡᠨ ᠺᠣᠳ᠋ ᠢ ᠣᠯᠵᠤ ᠥᠭ᠍ᠭᠥᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="256"/>
        <source>release code</source>
        <translation>ᠺᠣᠳ᠋ ᠨᠡᠶᠢᠲᠡᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="260"/>
        <source>Please enter the release code</source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠦᠨ ᠺᠣᠳ᠋ ᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠢᠷᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="304"/>
        <source>The verification code cannot be empty!</source>
        <translation>ᠨᠤᠮᠸᠷ ᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠤᠯᠭᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠭᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="313"/>
        <source>Please enter the correct verification code!</source>
        <translation>ᠵᠥᠪ ᠰᠢᠯᠭᠠᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠨᠣᠮᠧᠷ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠪᠣᠯᠪᠠᠤ !</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>Policy update failed</source>
        <translation>ᠠᠷᠭ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯᠲᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="647"/>
        <source>error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="429"/>
        <source>     Policy Version: Not Latest</source>
        <translation>     ᠪᠠᠶᠢᠯᠳᠤᠬᠤ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢᠨ ᠬᠡᠪᠯᠡᠯ ᠄ ᠬᠠᠮᠤᠭ ᠰᠢᠨ᠎ᠡ ᠪᠢᠰᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="164"/>
        <source>User password</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="495"/>
        <source>Policy cancel</source>
        <translation>ᠲᠥᠷᠥ ᠶᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠪᠠ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="524"/>
        <source>Wait updates</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠯᠲᠡ ᠶᠢ ᠬᠦᠯᠢᠶᠡᠵᠦ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="527"/>
        <source>latest</source>
        <translation>ᠣᠷᠴᠢᠮ ᠤᠨ ᠬᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>ScriptsWarningWindow</name>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="7"/>
        <source>Script Run Warning</source>
        <translation>ᠬᠥᠯ ᠳᠡᠪᠲᠡᠷ ᠦᠨ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠳᠤ ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭ᠍ᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="8"/>
        <source>The script is running, please do not shut down or restart!</source>
        <translation>ᠬᠥᠯ ᠦᠨ ᠳᠡᠪᠲᠡᠷ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠂ ᠪᠢᠲᠡᠬᠡᠢ ᠪᠠᠢ᠌ᠭᠤᠯᠭ᠎ᠠ ᠪᠤᠶᠤ ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠦᠭᠡᠷᠡᠢ !</translation>
    </message>
</context>
<context>
    <name>SettingWindow</name>
    <message>
        <location filename="../view/setting_window.cpp" line="17"/>
        <source>App install:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤᠭᠰᠠᠷᠠᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="18"/>
        <source>App run：</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠃:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="25"/>
        <source>Firewall:</source>
        <translation>ᠭᠠᠯ ᠰᠡᠷᠬᠡᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠮ:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="26"/>
        <source>Hotspot:</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="27"/>
        <source>Network Isolation:</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠲᠤᠰᠠᠭᠠᠷᠯᠠᠬᠤ :</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="34"/>
        <source>USB:</source>
        <translation>USB ᠵᠠᠯᠭᠠᠯᠲᠠ :</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="35"/>
        <source>Mobile:</source>
        <translation>ᠭᠠᠷ ᠤᠲᠠᠰᠤ:</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="36"/>
        <source>CD-ROM:</source>
        <translation>ᠭᠡᠷᠡᠯᠢᠭ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ:</translation>
    </message>
</context>
<context>
    <name>ShutdownAndReboot</name>
    <message>
        <location filename="../view/shutdown_window.cpp" line="15"/>
        <source>Shutdown</source>
        <translation>ᠪᠠᠶᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ ᠶᠢᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="17"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="22"/>
        <location filename="../view/shutdown_window.cpp" line="26"/>
        <source>warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="23"/>
        <source>? After 60 seconds, the computer will automatically</source>
        <translation>? ᠵᠢᠷᠠᠨ ᠰᠧᠺᠦᠢᠨᠳ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠺᠣᠮᠫᠢᠦᠢᠲ᠋ᠧᠷ ᠠᠦᠢᠲ᠋ᠣᠴᠢᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="27"/>
        <source>After 60 seconds, the computer will automatically</source>
        <translation>ᠵᠢᠷᠠᠨ ᠰᠧᠺᠦᠢᠨᠳ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠺᠣᠮᠫᠢᠦᠢᠲ᠋ᠧᠷ ᠠᠦᠢᠲ᠋ᠣᠴᠢᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="33"/>
        <source>Execute immediately</source>
        <translation>ᠳᠠᠷᠤᠶᠢᠬᠠᠨ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="36"/>
        <source>Execute in 5 minutes</source>
        <translation>5 ᠮᠢᠨᠦ᠋ᠲ ᠦᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠭᠦᠢᠴᠡᠳᠬᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="39"/>
        <source>Remind me in an hour</source>
        <translation>ᠨᠢᠭᠡ ᠴᠠᠭ ᠤᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠠᠳᠠᠳᠤ ᠰᠠᠨᠠᠭᠤᠯᠤᠶ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="69"/>
        <source>? After </source>
        <translation>? ᠰᠡᠭᠦᠯᠡᠷ ᠃ </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="70"/>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>s, the computer will automatically </source>
        <translation>s ᠂ ᠺᠣᠮᠫᠢᠦᠢᠲ᠋ᠧᠷ ᠠᠦᠢᠲ᠋ᠣᠴᠢᠯᠠᠨ᠎ᠠ ᠃ </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>After </source>
        <translation>ᠰᠠᠭᠤᠷᠢ ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="102"/>
        <source>in process of</source>
        <translation>ᠶᠠᠭ ᠶᠠᠪᠤᠭᠳᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>Trayicon_left_widget</name>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="61"/>
        <source>kcm client</source>
        <translation>KCM ᠵᠣᠴᠢᠨ ᠡᠷᠦᠬᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="63"/>
        <source>scene</source>
        <translation>ᠲᠤᠬᠠᠢ ᠶᠢᠨ ᠲᠠᠯᠠᠪᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="92"/>
        <source>policy update</source>
        <translation>ᠲᠥᠷᠥ ᠶᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯᠲᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="136"/>
        <source>connection is normal</source>
        <translation>ᠬᠣᠯᠪᠣᠯᠲᠠ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠲᠠᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="218"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="220"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="340"/>
        <source>Connection is normal</source>
        <translation>ᠬᠣᠯᠪᠣᠯᠲᠠ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠲᠠᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="343"/>
        <source>Connection is abnormal</source>
        <translation>ᠬᠣᠯᠪᠣᠯᠲᠠ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>Trayicon_widget</name>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="23"/>
        <location filename="../view/trayicon_widget.cpp" line="193"/>
        <source>kcm client</source>
        <translation>KCM ᠵᠣᠴᠢᠨ ᠡᠷᠦᠬᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="131"/>
        <source>Complication inspect</source>
        <translation>ᠡᠪᠡᠳᠴᠢᠲᠡᠨ ᠦ ᠪᠠᠶᠢᠴᠠᠭᠠᠯᠲᠠ ᠬᠢᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="133"/>
        <source>Policy setting</source>
        <translation>ᠠᠷᠭ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="135"/>
        <source>Policy info</source>
        <translation>ᠲᠥᠷᠥ ᠶᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="137"/>
        <source>Log list</source>
        <translation>ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠦᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠳᠦ ᠵᠢᠭᠰᠠᠭᠠᠭᠳᠠ</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="139"/>
        <source>History Messages</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="141"/>
        <source>About</source>
        <translation>ᠪᠠᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="143"/>
        <source>Exit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="200"/>
        <source>Version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="202"/>
        <source>kcm client is a software that enables policy download, execution, and data reporting with the kylin terminal control console. Provide system management, log reporting, and other functions for user terminals</source>
        <translation>ᠺᠴᠮ Clieent ᠪᠣᠯ ᠳᠡᠮᠵᠢᠬᠦ ᠠᠷᠭ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠶᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠂ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠬᠦ ᠪᠠ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢ ᠳᠡᠭᠡᠭᠰᠢ ᠮᠡᠳᠡᠭᠦᠯᠬᠦ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠂ ᠺᠣᠯᠳ᠋ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠡᠵᠡᠮᠳᠡᠯ ᠦᠨ ᠲᠠᠶᠢᠰᠠ ᠪᠠᠷ ᠳᠠᠮᠵᠢᠨ ᠶᠠᠪᠤᠭᠳᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠦᠵᠦᠭᠦᠷ ᠲᠦ ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠂ ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ ᠳᠡᠭᠡᠭᠰᠢ ᠮᠡᠳᠡᠭᠦᠯᠬᠦ ᠵᠡᠷᠭᠡ ᠴᠢᠳᠠᠮᠵᠢ ᠬᠠᠩᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>UpdateRequest</name>
    <message>
        <location filename="../view/update_request_window.cpp" line="7"/>
        <source>Upgrade Request</source>
        <translation>ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠦᠯᠬᠦ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="8"/>
        <location filename="../view/update_request_window.cpp" line="54"/>
        <source>The server request </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ ᠃ </translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="9"/>
        <source> upgrade. Please choose whether to agree or not. (If not selected after 30 seconds, the decision on whether to agree to install will be based on the configuration)</source>
        <translation> ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠦᠯᠦᠨ᠎ᠡ ᠃ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ ᠶᠢ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ ( ᠬᠡᠷᠪᠡ 30 ᠰᠧᠺᠦᠢᠨᠳ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠤᠩᠭ᠋ᠤᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠪᠣᠯ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠶᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ ᠶᠢ ᠰᠤᠩᠭ᠋ᠤᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ )</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="15"/>
        <source>Agree</source>
        <translation>ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="17"/>
        <source>Refuse</source>
        <translation>ᠲᠡᠪᠴᠢᠬᠦ ᠡᠴᠡ ᠲᠠᠲᠠᠭᠠᠯᠵᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="55"/>
        <source> upgrade. Please choose whether to agree or not. (After ）</source>
        <translation> ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠦᠯᠦᠨ᠎ᠡ ᠃ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ ᠶᠢ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="57"/>
        <source>s,if not selected, it will be determined whether to agree to install according to the configuration</source>
        <translation>s ᠂ ᠬᠡᠷᠪᠡ ᠰᠤᠩᠭ᠋ᠤᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠪᠣᠯ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠶᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ ᠶᠢ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>UpdateWindow</name>
    <message>
        <location filename="../view/update_window.cpp" line="7"/>
        <source>System notifications</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="25"/>
        <source>The system is currently undergoing a unified upgrade or installation of components. You can use it normally, but please do not shut down, restart, or log out</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠣᠳᠣ ᠶᠠᠭ ᠲᠣᠨᠣᠭᠯᠠᠯ ᠤᠨ ᠨᠢᠭᠡᠳᠦᠯᠲᠡᠶ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠪᠤᠶᠤ ᠤᠭᠰᠠᠷᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃ ᠲᠠ ᠲᠡᠭᠦᠨ ᠢ ᠬᠡᠪ ᠦᠨ ᠶᠣᠰᠣᠭᠠᠷ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ᠂ ᠭᠡᠪᠡᠴᠦ ᠪᠢᠲᠡᠭᠡᠢ ᠪᠠᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ ᠪᠠᠨ ᠬᠠᠭᠠᠵᠤ᠂ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠪᠤᠶᠤ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠭᠠᠷᠠᠢ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="29"/>
        <source>Getting system apt cache data</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ apt ᠶᠢᠨ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢ ᠬᠣᠶᠢᠰᠢᠯᠠᠭᠤᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="66"/>
        <source>Opening system apt cache</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠬᠣᠶᠢᠰᠢᠯᠠᠭᠤᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="78"/>
        <source>System upgrade failed, After </source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠃ </translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="79"/>
        <source>s,close the window</source>
        <translation>ᠴᠣᠩᠬᠣ ᠶᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="82"/>
        <source>Updating apt cache: apt update</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ apt ᠠᠯᠭᠤᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠳᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠄ apt update ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="83"/>
        <source>speed：</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ :</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="85"/>
        <source>Performing download and installation</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="95"/>
        <source>Download completed! About to automatically close.</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠬᠠᠨ᠎ᠠ ! ᠤᠳᠠᠯ ᠦᠭᠡᠢ ᠠᠦᠢᠲ᠋ᠣ᠋ ᠬᠠᠭᠠᠬᠤ ᠭᠡᠵᠦ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠃.</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="120"/>
        <source>System upgrade failed,after</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="121"/>
        <source>s,close the window！</source>
        <translation>ᠴᠣᠩᠬᠣ ᠶᠢ ᠬᠠᠭᠠᠬᠤ!</translation>
    </message>
</context>
<context>
    <name>User_information_table</name>
    <message>
        <location filename="../view/user_information_table.cpp" line="75"/>
        <source>IP</source>
        <translation>ᠮᠡᠳᠡᠯᠭᠡ ᠶᠢᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ ᠦ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="78"/>
        <source>Login Name</source>
        <translation>ᠨᠡᠷᠡᠰ ᠲᠦ ᠲᠡᠮᠳᠡᠭᠯᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="81"/>
        <source>Host Name</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠱᠢᠨ᠎ᠤ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="84"/>
        <source>Terminal ORG</source>
        <translation>ᠦᠵᠦᠭᠦᠷ ᠦᠨ ORG ᠃</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="87"/>
        <source>Local User</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="91"/>
        <source>Policy Time</source>
        <translation>ᠳᠠᠭᠠᠳᠬᠠᠯ ᠤᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢᠨ ᠴᠠᠭ ᠃</translation>
    </message>
</context>
</TS>
