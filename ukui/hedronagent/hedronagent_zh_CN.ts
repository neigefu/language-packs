<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DetailMessageWindow</name>
    <message>
        <location filename="../view/detail_message_window.cpp" line="10"/>
        <source>Message Notification</source>
        <translation>消息通知</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="14"/>
        <source>title</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="15"/>
        <source>content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="16"/>
        <source>sender</source>
        <translation>发件人</translation>
    </message>
    <message>
        <location filename="../view/detail_message_window.cpp" line="17"/>
        <source>time</source>
        <translation>时间</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="521"/>
        <source>jump to</source>
        <translation>跳至</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="523"/>
        <source>pages</source>
        <translation>页</translation>
    </message>
</context>
<context>
    <name>HistoryLogWindow</name>
    <message>
        <location filename="../view/history_log_window.cpp" line="13"/>
        <location filename="../view/history_log_window.cpp" line="22"/>
        <source>Log List</source>
        <translation>日志列表</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Log Type</source>
        <translation>日志类型</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="99"/>
        <source>Content</source>
        <translation>内容</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>No log information found!</source>
        <translation>未找到日志信息！</translation>
    </message>
    <message>
        <location filename="../view/history_log_window.cpp" line="161"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
</context>
<context>
    <name>HistoryMessageWindow</name>
    <message>
        <location filename="../view/history_message_window.cpp" line="14"/>
        <location filename="../view/history_message_window.cpp" line="23"/>
        <source>History Messages</source>
        <translation>历史消息</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>State</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="51"/>
        <source>Sender</source>
        <translation>发件人</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Importance</source>
        <translation>重要程度</translation>
    </message>
    <message>
        <location filename="../view/history_message_window.cpp" line="52"/>
        <source>Title</source>
        <translation>标题</translation>
    </message>
</context>
<context>
    <name>InspectWindow</name>
    <message>
        <location filename="../view/inspect_window.cpp" line="14"/>
        <location filename="../view/inspect_window.cpp" line="23"/>
        <location filename="../view/inspect_window.cpp" line="161"/>
        <location filename="../view/inspect_window.cpp" line="276"/>
        <source>Complication Inspect</source>
        <translation>合规性检查</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="116"/>
        <source>Repair</source>
        <translation>修复</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="127"/>
        <location filename="../view/inspect_window.cpp" line="133"/>
        <source>Repair tip</source>
        <translation>修复提示</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="134"/>
        <location filename="../view/inspect_window.cpp" line="164"/>
        <location filename="../view/inspect_window.cpp" line="281"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="171"/>
        <location filename="../view/inspect_window.cpp" line="237"/>
        <source>Repairing...</source>
        <translation>修复中...</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="305"/>
        <source>     Inspection passed</source>
        <translation>     检查通过</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="55"/>
        <location filename="../view/inspect_window.cpp" line="352"/>
        <source>One key repair</source>
        <translation>一键修复</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="162"/>
        <source>Please wait for the current repair to complete before performing the repair operation!</source>
        <translation>请等待当前修复结束后再执行修复操作！</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="277"/>
        <source>The current device has not detected integrity detection rules. Please contact the server to issue them!</source>
        <translation>当前设备未检测到完整性检测规则，请联系服务端进行下发！</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Items</source>
        <translation>检查项</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Baseline</source>
        <translation>基线要求</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Result</source>
        <translation>检查结果</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="320"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../view/inspect_window.cpp" line="356"/>
        <source>Check Now</source>
        <translation>立即检查</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <source>total</source>
        <translation>共</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="16"/>
        <location filename="../view/item_wid/paginationwid.cpp" line="43"/>
        <source>pages</source>
        <translation>页</translation>
    </message>
    <message>
        <location filename="../view/item_wid/paginationwid.cpp" line="40"/>
        <source>jump to</source>
        <translation>跳至</translation>
    </message>
</context>
<context>
    <name>PolicyWindow</name>
    <message>
        <location filename="../view/policy_window.cpp" line="19"/>
        <location filename="../view/policy_window.cpp" line="24"/>
        <source>Policy info</source>
        <translation>策略详情</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="45"/>
        <location filename="../view/policy_window.cpp" line="427"/>
        <source>     Policy Version: Latest</source>
        <translation>     策略版本:最新</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Policy</source>
        <translation>策略</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Description</source>
        <translation>策略说明</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Version</source>
        <translation>策略版本</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="60"/>
        <source>Operate</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="96"/>
        <source>One key update</source>
        <translation>一键更新</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="113"/>
        <source>Loading...</source>
        <translation>加载中...</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="172"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <location filename="../view/policy_window.cpp" line="304"/>
        <location filename="../view/policy_window.cpp" line="313"/>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="181"/>
        <source>Password cannot be empty!</source>
        <translation>密码不能为空！</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="191"/>
        <location filename="../view/policy_window.cpp" line="267"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="193"/>
        <location filename="../view/policy_window.cpp" line="269"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="212"/>
        <location filename="../view/policy_window.cpp" line="218"/>
        <source>Lift strategy</source>
        <translation>解除策略</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="225"/>
        <source>verification code</source>
        <translation>验证码</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="252"/>
        <source>Please send the code to the administrator to obtain the release code</source>
        <translation>请将验证码发送管理员以获取解除码</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="256"/>
        <source>release code</source>
        <translation>解除码</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="260"/>
        <source>Please enter the release code</source>
        <translation>请输入解除码</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="304"/>
        <source>The verification code cannot be empty!</source>
        <translation>验证码不能为空！</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="313"/>
        <source>Please enter the correct verification code!</source>
        <translation>请输入正确的验证码！</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="361"/>
        <source>Policy update failed</source>
        <translation>策略更新失败</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="647"/>
        <source>error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="429"/>
        <source>     Policy Version: Not Latest</source>
        <translation>     策略版本：非最新</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="164"/>
        <source>User password</source>
        <translation>用户密码</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="495"/>
        <source>Policy cancel</source>
        <translation>策略取消</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="524"/>
        <source>Wait updates</source>
        <translation>待更新</translation>
    </message>
    <message>
        <location filename="../view/policy_window.cpp" line="527"/>
        <source>latest</source>
        <translation>最新</translation>
    </message>
</context>
<context>
    <name>ScriptsWarningWindow</name>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="7"/>
        <source>Script Run Warning</source>
        <translation>脚本运行警告</translation>
    </message>
    <message>
        <location filename="../view/scripts_warning_window.cpp" line="8"/>
        <source>The script is running, please do not shut down or restart!</source>
        <translation>脚本正在运行，请勿关机重启！</translation>
    </message>
</context>
<context>
    <name>SettingWindow</name>
    <message>
        <location filename="../view/setting_window.cpp" line="17"/>
        <source>App install:</source>
        <translation>软件安装：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="18"/>
        <source>App run：</source>
        <translation>软件运行：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="25"/>
        <source>Firewall:</source>
        <translation>防火墙：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="26"/>
        <source>Hotspot:</source>
        <translation>热点：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="27"/>
        <source>Network Isolation:</source>
        <translation>内外网隔离：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="34"/>
        <source>USB:</source>
        <translation>USB存储设备：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="35"/>
        <source>Mobile:</source>
        <translation>手机：</translation>
    </message>
    <message>
        <location filename="../view/setting_window.cpp" line="36"/>
        <source>CD-ROM:</source>
        <translation>光驱：</translation>
    </message>
</context>
<context>
    <name>ShutdownAndReboot</name>
    <message>
        <location filename="../view/shutdown_window.cpp" line="15"/>
        <source>Shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="17"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="22"/>
        <location filename="../view/shutdown_window.cpp" line="26"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="23"/>
        <source>? After 60 seconds, the computer will automatically</source>
        <translation>电脑在60s后将自动</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="27"/>
        <source>After 60 seconds, the computer will automatically</source>
        <translation>电脑在60s后将自动</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="33"/>
        <source>Execute immediately</source>
        <translation>立即执行</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="36"/>
        <source>Execute in 5 minutes</source>
        <translation>5分钟后执行</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="39"/>
        <source>Remind me in an hour</source>
        <translation>一小时后提醒我</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="69"/>
        <source>? After </source>
        <translation>? 电脑在</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="70"/>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>s, the computer will automatically </source>
        <translation>s后将自动</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="72"/>
        <source>After </source>
        <translation>电脑在</translation>
    </message>
    <message>
        <location filename="../view/shutdown_window.cpp" line="102"/>
        <source>in process of</source>
        <translation>正在</translation>
    </message>
</context>
<context>
    <name>Trayicon_left_widget</name>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="61"/>
        <source>kcm client</source>
        <translation>麒麟天域客户端</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="63"/>
        <source>scene</source>
        <translation>场景</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="92"/>
        <source>policy update</source>
        <translation>策略更新</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="136"/>
        <source>connection is normal</source>
        <translation>与管控平台通信正常</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="218"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="220"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="340"/>
        <source>Connection is normal</source>
        <translation>与管控平台通信正常</translation>
    </message>
    <message>
        <location filename="../view/trayicon_left_widget.cpp" line="343"/>
        <source>Connection is abnormal</source>
        <translation>与管控平台通信异常</translation>
    </message>
</context>
<context>
    <name>Trayicon_widget</name>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="23"/>
        <location filename="../view/trayicon_widget.cpp" line="193"/>
        <source>kcm client</source>
        <translation>麒麟天域客户端</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="131"/>
        <source>Complication inspect</source>
        <translation>完整性检测</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="133"/>
        <source>Policy setting</source>
        <translation>策略设置</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="135"/>
        <source>Policy info</source>
        <translation>历史消息</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="137"/>
        <source>Log list</source>
        <translation>日志列表</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="139"/>
        <source>History Messages</source>
        <translation>历史消息</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="141"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="143"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="200"/>
        <source>Version:</source>
        <translation>版本号:</translation>
    </message>
    <message>
        <location filename="../view/trayicon_widget.cpp" line="202"/>
        <source>kcm client is a software that enables policy download, execution, and data reporting with the kylin terminal control console. Provide system management, log reporting, and other functions for user terminals</source>
        <translation>麒麟天域客户端是一款与麒麟终端管控台进行策略下载、执行、数据上报的客户软件。为用户终端提供系统管理、日志上报等功能。</translation>
    </message>
</context>
<context>
    <name>UpdateRequest</name>
    <message>
        <location filename="../view/update_request_window.cpp" line="7"/>
        <source>Upgrade Request</source>
        <translation>升级请求</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="8"/>
        <location filename="../view/update_request_window.cpp" line="54"/>
        <source>The server request </source>
        <translation>服务端请求为</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="9"/>
        <source> upgrade. Please choose whether to agree or not. (If not selected after 30 seconds, the decision on whether to agree to install will be based on the configuration)</source>
        <translation>进行升级，请选择是否同意。（30s后未选择将按照配置决定是否同意安装）</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="15"/>
        <source>Agree</source>
        <translation>同意升级</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="17"/>
        <source>Refuse</source>
        <translation>拒绝升级</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="55"/>
        <source> upgrade. Please choose whether to agree or not. (After ）</source>
        <translation>进行升级，请选择是否同意。（</translation>
    </message>
    <message>
        <location filename="../view/update_request_window.cpp" line="57"/>
        <source>s,if not selected, it will be determined whether to agree to install according to the configuration</source>
        <translation>s后未选择将按照配置决定是否同意安装）</translation>
    </message>
</context>
<context>
    <name>UpdateWindow</name>
    <message>
        <location filename="../view/update_window.cpp" line="7"/>
        <source>System notifications</source>
        <translation>系统通知</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="25"/>
        <source>The system is currently undergoing a unified upgrade or installation of components. You can use it normally, but please do not shut down, restart, or log out</source>
        <translation>系统正在统一升级或安装组件，您可以正常使用，但请勿关机、重启、注销</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="29"/>
        <source>Getting system apt cache data</source>
        <translation>正在获取系统apt缓存数据</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="66"/>
        <source>Opening system apt cache</source>
        <translation>正在打开系统apt缓存</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="78"/>
        <source>System upgrade failed, After </source>
        <translation>系统升级失败，将在</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="79"/>
        <source>s,close the window</source>
        <translation>秒后关闭窗口！</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="82"/>
        <source>Updating apt cache: apt update</source>
        <translation>正在更新apt缓存：apt update</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="83"/>
        <source>speed：</source>
        <translation>速度：</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="85"/>
        <source>Performing download and installation</source>
        <translation>正在执行下载和安装</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="95"/>
        <source>Download completed! About to automatically close.</source>
        <translation>下载完成！即将自动关闭。</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="120"/>
        <source>System upgrade failed,after</source>
        <translation>系统升级失败，将在</translation>
    </message>
    <message>
        <location filename="../view/update_window.cpp" line="121"/>
        <source>s,close the window！</source>
        <translation>秒后关闭窗口！</translation>
    </message>
</context>
<context>
    <name>User_information_table</name>
    <message>
        <location filename="../view/user_information_table.cpp" line="75"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="78"/>
        <source>Login Name</source>
        <translation>登录名</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="81"/>
        <source>Host Name</source>
        <translation>主机名</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="84"/>
        <source>Terminal ORG</source>
        <translation>终端组织</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="87"/>
        <source>Local User</source>
        <translation>本地用户</translation>
    </message>
    <message>
        <location filename="../view/user_information_table.cpp" line="91"/>
        <source>Policy Time</source>
        <translation>策略时间</translation>
    </message>
</context>
</TS>
