<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>AllowLocation</name>
    <message>
        <source>OK</source>
        <translation type="vanished">ЖАҚСЫ</translation>
    </message>
    <message>
        <source>Whether to enable application locating?</source>
        <translation>Қолданбаның орналасуын қосу керек пе?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CollectCity</name>
    <message>
        <source>None</source>
        <translation>Ешқайсысы</translation>
    </message>
    <message>
        <source>Collect City</source>
        <translation>Қаланы жинау</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <source>Seek failed!</source>
        <translation>Іздеу жаңылысы!</translation>
    </message>
</context>
<context>
    <name>DataParser</name>
    <message>
        <source>Data is empty</source>
        <translation>Деректер бос</translation>
    </message>
    <message>
        <source>hourly != 24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>hourly key is wrong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hourly is not array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hourly is not exits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorReact</name>
    <message>
        <source>Abnormal access address!</source>
        <translation>Аномальды қатынас мекенжайы!</translation>
    </message>
    <message>
        <source>Network error!</source>
        <translation>Желі қатесі!</translation>
    </message>
    <message>
        <source>Network not connected!</source>
        <translation>Желі қосылмаған!</translation>
    </message>
    <message>
        <source>Data parsing error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Network Timeout!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Seek failed!</source>
        <translation type="unfinished">Іздеу жаңылысы!</translation>
    </message>
</context>
<context>
    <name>HotCity</name>
    <message>
        <source>Hot City</source>
        <translation>Хот-Сити</translation>
    </message>
</context>
<context>
    <name>PromptWidget</name>
    <message>
        <source>Retry</source>
        <translation>Ретри</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Weather</source>
        <translation>Ауа райы</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <source>Search</source>
        <translation type="vanished">Іздеу</translation>
    </message>
</context>
<context>
    <name>SearchCityLIst</name>
    <message>
        <source>there is no search city!</source>
        <translation>іздеу қаласы жоқ!</translation>
    </message>
</context>
<context>
    <name>ViewVar</name>
    <message>
        <source>Lasa</source>
        <translation>Ласа</translation>
    </message>
    <message>
        <source>Wuxi</source>
        <translation type="vanished">Вуси</translation>
    </message>
    <message>
        <source>Xian</source>
        <translation>Сянь</translation>
    </message>
    <message>
        <source>Jinan</source>
        <translation type="vanished">Цзинань</translation>
    </message>
    <message>
        <source>Macao</source>
        <translation>Макао</translation>
    </message>
    <message>
        <source>Wuhan</source>
        <translation>Ухань</translation>
    </message>
    <message>
        <source>Lijiang</source>
        <translation type="vanished">Лицзян</translation>
    </message>
    <message>
        <source>Nanjing</source>
        <translation>Нанкин</translation>
    </message>
    <message>
        <source>Taibei</source>
        <translation>Тайбэй</translation>
    </message>
    <message>
        <source>Suzhou</source>
        <translation>Сучжоу</translation>
    </message>
    <message>
        <source>Hongkong</source>
        <translation>Хонконг</translation>
    </message>
    <message>
        <source>Chongqing</source>
        <translation>Чунцин</translation>
    </message>
    <message>
        <source>Qingdao</source>
        <translation type="vanished">Циндао</translation>
    </message>
    <message>
        <source>Hangzhou</source>
        <translation>Ханчжоу</translation>
    </message>
    <message>
        <source>Guangzhou</source>
        <translation>Гуанчжоу</translation>
    </message>
    <message>
        <source>Beijing</source>
        <translation>Бейжің</translation>
    </message>
    <message>
        <source>Chengdu</source>
        <translation>Чэнду</translation>
    </message>
    <message>
        <source>Tianjin</source>
        <translation>Тяньцзинь</translation>
    </message>
    <message>
        <source>Zhengzhou</source>
        <translation>Чжэнчжоу</translation>
    </message>
    <message>
        <source>Shanghai</source>
        <translation>Шанхай</translation>
    </message>
    <message>
        <source>Changsha</source>
        <translation>Чанша</translation>
    </message>
    <message>
        <source>Shenzhen</source>
        <translation>Шэньчжэнь</translation>
    </message>
</context>
<context>
    <name>air</name>
    <message>
        <source>Air</source>
        <translation>Әуе</translation>
    </message>
    <message>
        <source>N/A</source>
        <translation>Н/А</translation>
    </message>
</context>
<context>
    <name>city</name>
    <message>
        <source>【Change】</source>
        <translation>【Өзгерту】</translation>
    </message>
</context>
<context>
    <name>clothe</name>
    <message>
        <source>Clothe</source>
        <translation>Шұға</translation>
    </message>
</context>
<context>
    <name>flu</name>
    <message>
        <source>Flu</source>
        <translation>Тұмау</translation>
    </message>
</context>
<context>
    <name>horscreen</name>
    <message>
        <source>Exit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <source>【Retract】</source>
        <translation>【Retract】</translation>
    </message>
    <message>
        <source>full srceen</source>
        <translation>толық шолпы</translation>
    </message>
    <message>
        <source>Open Weather</source>
        <translation>Ашық ауа райы</translation>
    </message>
    <message>
        <source>【Change】</source>
        <translation>【Өзгерту】</translation>
    </message>
    <message>
        <source>recovery</source>
        <translation>қалпына келтіру</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation>Ауа райы</translation>
    </message>
    <message>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation type="obsolete">Ауа райы - ауа райы туралы ақпарат құралы, ол ауа-райы жағдайы мен тұрмыс индексін шолуға болады.</translation>
    </message>
    <message>
        <source>kylin-weather</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About to switch to default City</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>no</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Опциялар</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">жабу</translation>
    </message>
    <message>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation>Ауа райы - ауа райы туралы ақпарат құралы, ол ауа-райы жағдайы мен тұрмыс индексін шолуға болады.</translation>
    </message>
    <message>
        <source>Service &amp; Support Team: </source>
        <translation>Қызмет және қолдау тобы: </translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Орналасуы</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Нұсқасы: </translation>
    </message>
    <message>
        <source>Weather</source>
        <translation>Ауа райы</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>sport</name>
    <message>
        <source>Sport</source>
        <translation>Спорт</translation>
    </message>
</context>
<context>
    <name>title</name>
    <message>
        <source>Minimize</source>
        <translation>кішірейту</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>жабу</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation>Ауа райы</translation>
    </message>
    <message>
        <source>full screen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ultravioletrays</name>
    <message>
        <source>UV</source>
        <translation>УК</translation>
    </message>
</context>
<context>
    <name>washcar</name>
    <message>
        <source>Wash Car</source>
        <translation>Вашингтон</translation>
    </message>
</context>
<context>
    <name>weekweather</name>
    <message>
        <source>Today</source>
        <translation>Бүгін</translation>
    </message>
    <message>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Monday</source>
        <translation>Дүйсенбі</translation>
    </message>
    <message>
        <source>Tuesday</source>
        <translation>Сейсенбі</translation>
    </message>
    <message>
        <source>Wednesday</source>
        <translation>Сәрсенбі</translation>
    </message>
    <message>
        <source>Thursday</source>
        <translation>Бейсенбі</translation>
    </message>
    <message>
        <source>Friday</source>
        <translation>Жұма</translation>
    </message>
    <message>
        <source>Saturday</source>
        <translation>Сенбі</translation>
    </message>
    <message>
        <source>Sunday</source>
        <translation>Жексенбі</translation>
    </message>
    <message>
        <source>N/A</source>
        <translation>Н/А</translation>
    </message>
</context>
</TS>
