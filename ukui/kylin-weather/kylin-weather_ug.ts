<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>AllowLocation</name>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="56"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">ماقۇل</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="28"/>
        <source>Whether to enable application locating?</source>
        <translation>قوللىنىشچان ئورۇننى قوزغىتىش كېرەكمۇ يوق؟</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>CollectCity</name>
    <message>
        <location filename="../view/search/collectcity.cpp" line="16"/>
        <source>None</source>
        <translation>يوق</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="10"/>
        <source>Collect City</source>
        <translation>شەھەر توپلاش</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../controller/core/core.cpp" line="232"/>
        <location filename="../controller/core/core.cpp" line="237"/>
        <source>Seek failed!</source>
        <translation>ئىنتىلىش مەغلۇپ بولدى!</translation>
    </message>
</context>
<context>
    <name>DataParser</name>
    <message>
        <location filename="../model/dataparser.cpp" line="28"/>
        <location filename="../model/dataparser.cpp" line="47"/>
        <location filename="../model/dataparser.cpp" line="57"/>
        <location filename="../model/dataparser.cpp" line="125"/>
        <location filename="../model/dataparser.cpp" line="175"/>
        <location filename="../model/dataparser.cpp" line="208"/>
        <location filename="../model/dataparser.cpp" line="263"/>
        <source>Data is empty</source>
        <translation>سانلىق مەلۇمات بوش</translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="278"/>
        <source>hourly != 24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="286"/>
        <source>hourly key is wrong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="306"/>
        <source>Hourly is not array</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="313"/>
        <source>Hourly is not exits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorReact</name>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="25"/>
        <source>Abnormal access address!</source>
        <translation>نورمالسىز زىيارەت ئادرىسى!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="19"/>
        <source>Network error!</source>
        <translation>تور خاتالىقى!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="16"/>
        <source>Network not connected!</source>
        <translation>تور ئۇلانمىدى!</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="31"/>
        <source>Data parsing error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="49"/>
        <source>Network Timeout!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="55"/>
        <source>Seek failed!</source>
        <translation type="unfinished">ئىنتىلىش مەغلۇپ بولدى!</translation>
    </message>
</context>
<context>
    <name>HotCity</name>
    <message>
        <location filename="../view/search/hotcity.cpp" line="9"/>
        <source>Hot City</source>
        <translation>قىزىق شەھەر</translation>
    </message>
</context>
<context>
    <name>PromptWidget</name>
    <message>
        <location filename="../view/horscreen/promptwidget.cpp" line="21"/>
        <source>Retry</source>
        <translation>قايتا قايتا تىرشىش</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="81"/>
        <source>Weather</source>
        <translation>ھاۋارايى</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <source>Search</source>
        <translation type="vanished">ئىزدە</translation>
    </message>
</context>
<context>
    <name>SearchCityLIst</name>
    <message>
        <location filename="../view/search/searchcitylist.cpp" line="46"/>
        <location filename="../view/search/searchcitylist.cpp" line="105"/>
        <source>there is no search city!</source>
        <translation>ئىزدەش شەھىرى يوق!</translation>
    </message>
</context>
<context>
    <name>ViewVar</name>
    <message>
        <location filename="../global/viewvar.cpp" line="140"/>
        <source>Lasa</source>
        <translation>لاسا</translation>
    </message>
    <message>
        <source>Wuxi</source>
        <translation type="vanished">ۋۇشى</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="133"/>
        <source>Xian</source>
        <translation>شين</translation>
    </message>
    <message>
        <source>Jinan</source>
        <translation type="vanished">جىنەن</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="139"/>
        <source>Macao</source>
        <translation>ئاۋمېن</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="128"/>
        <source>Wuhan</source>
        <translation>ۋۇخەن شەھىرى</translation>
    </message>
    <message>
        <source>Lijiang</source>
        <translation type="vanished">لىجياڭ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="132"/>
        <source>Nanjing</source>
        <translation>نەنجىڭ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="137"/>
        <source>Taibei</source>
        <translation>تەيبېي</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="131"/>
        <source>Suzhou</source>
        <translation>سۇجۇ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="138"/>
        <source>Hongkong</source>
        <translation>خوڭكوڭ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="129"/>
        <source>Chongqing</source>
        <translation>چۇڭچىڭ</translation>
    </message>
    <message>
        <source>Qingdao</source>
        <translation type="vanished">چىڭداۋ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="135"/>
        <source>Hangzhou</source>
        <translation>خاڭجۇ ئوبلاستى</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="125"/>
        <source>Guangzhou</source>
        <translation>گۇاڭجۇ ئوبلاستى</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="123"/>
        <source>Beijing</source>
        <translation>بېيجىڭ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="127"/>
        <source>Chengdu</source>
        <translation>چېڭدۇ</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="130"/>
        <source>Tianjin</source>
        <translation>تيەنجىن</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="136"/>
        <source>Zhengzhou</source>
        <translation>جېڭجۇ شەھىرى</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="124"/>
        <source>Shanghai</source>
        <translation>شاڭخەي</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="134"/>
        <source>Changsha</source>
        <translation>چاڭشا ئوبلاستى</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="126"/>
        <source>Shenzhen</source>
        <translation>شېنجېن شەھىرى</translation>
    </message>
</context>
<context>
    <name>air</name>
    <message>
        <location filename="../view/horscreen/air.cpp" line="16"/>
        <source>Air</source>
        <translation>ھاۋا</translation>
    </message>
    <message>
        <location filename="../view/horscreen/air.cpp" line="18"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
</context>
<context>
    <name>city</name>
    <message>
        <location filename="../view/horscreen/city.cpp" line="71"/>
        <source>【Change】</source>
        <translation>«ئۆزگەرتىش»</translation>
    </message>
</context>
<context>
    <name>clothe</name>
    <message>
        <location filename="../view/horscreen/clothe.cpp" line="17"/>
        <source>Clothe</source>
        <translation>رەخت</translation>
    </message>
</context>
<context>
    <name>flu</name>
    <message>
        <location filename="../view/horscreen/flu.cpp" line="17"/>
        <source>Flu</source>
        <translation>تارقىلىشچان زۇكام</translation>
    </message>
</context>
<context>
    <name>horscreen</name>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="377"/>
        <source>Exit</source>
        <translation>چىقىش ئېغىزى</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="168"/>
        <source>【Retract】</source>
        <translation>«چېكىنىش»</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="247"/>
        <source>full srceen</source>
        <translation>تولۇق چەرچەن</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="376"/>
        <source>Open Weather</source>
        <translation>ئوچۇق ھاۋارايى</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="172"/>
        <location filename="../view/horscreen/horscreen.cpp" line="565"/>
        <location filename="../view/horscreen/horscreen.cpp" line="579"/>
        <source>【Change】</source>
        <translation>«ئۆزگەرتىش»</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="252"/>
        <source>recovery</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="33"/>
        <location filename="../view/horscreen/horscreen.cpp" line="366"/>
        <source>Weather</source>
        <translation>ھاۋارايى</translation>
    </message>
    <message>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation type="obsolete">ھاۋارايى بىر خىل ھاۋارايى ئۇچۇر قورالى، ئۇ ھاۋارايى شارائىتى ۋە تۇرمۇش كۆرسەتكۈچىنى كۆزدىن كەچۈرەلەيدۇ.</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="296"/>
        <location filename="../view/horscreen/horscreen.cpp" line="298"/>
        <source>kylin-weather</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="299"/>
        <source>About to switch to default City</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="59"/>
        <location filename="../view/horscreen/menumodule.cpp" line="105"/>
        <source>no</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="56"/>
        <location filename="../view/horscreen/menumodule.cpp" line="100"/>
        <source>yes</source>
        <translation>ھەئە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="49"/>
        <location filename="../view/horscreen/menumodule.cpp" line="98"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="78"/>
        <location filename="../view/horscreen/menumodule.cpp" line="94"/>
        <source>Quit</source>
        <translation>چېكىنىش</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="219"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="34"/>
        <source>Options</source>
        <translation>تاللاش تۈرى</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="74"/>
        <location filename="../view/horscreen/menumodule.cpp" line="96"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">يېپىش</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="151"/>
        <location filename="../view/horscreen/menumodule.cpp" line="166"/>
        <location filename="../view/horscreen/menumodule.cpp" line="197"/>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation>ھاۋارايى بىر خىل ھاۋارايى ئۇچۇر قورالى، ئۇ ھاۋارايى شارائىتى ۋە تۇرمۇش كۆرسەتكۈچىنى كۆزدىن كەچۈرەلەيدۇ.</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="26"/>
        <location filename="../view/horscreen/menumodule.cpp" line="139"/>
        <location filename="../view/horscreen/menumodule.cpp" line="155"/>
        <source>Service &amp; Support Team: </source>
        <translation>مۇلازىمەت &gt; قوللاش ئەترىتى: </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="54"/>
        <source>Location</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="30"/>
        <location filename="../view/horscreen/menumodule.cpp" line="196"/>
        <location filename="../view/horscreen/menumodule.cpp" line="258"/>
        <source>Version: </source>
        <translation>نەشرى: </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="196"/>
        <location filename="../view/horscreen/menumodule.cpp" line="212"/>
        <location filename="../view/horscreen/menumodule.cpp" line="230"/>
        <source>Weather</source>
        <translation>ھاۋارايى</translation>
    </message>
</context>
<context>
    <name>sport</name>
    <message>
        <location filename="../view/horscreen/sport.cpp" line="17"/>
        <source>Sport</source>
        <translation>تەنتەربىيە</translation>
    </message>
</context>
<context>
    <name>title</name>
    <message>
        <location filename="../view/horscreen/title.cpp" line="33"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="54"/>
        <source>Close</source>
        <translation>يېپىش</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="19"/>
        <source>Weather</source>
        <translation>ھاۋارايى</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="45"/>
        <source>full screen</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ultravioletrays</name>
    <message>
        <location filename="../view/horscreen/ultravioletrays.cpp" line="17"/>
        <source>UV</source>
        <translation>UV</translation>
    </message>
</context>
<context>
    <name>washcar</name>
    <message>
        <location filename="../view/horscreen/washcar.cpp" line="17"/>
        <source>Wash Car</source>
        <translation>يۇيۇش ماشىنىسى</translation>
    </message>
</context>
<context>
    <name>weekweather</name>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="51"/>
        <source>Today</source>
        <translation>تارىخ-بۈگۈن</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="270"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="270"/>
        <source> </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="289"/>
        <source>Monday</source>
        <translation>دۈشەنبە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="292"/>
        <source>Tuesday</source>
        <translation>سەيشەنبە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="295"/>
        <source>Wednesday</source>
        <translation>چارشەنبە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="298"/>
        <source>Thursday</source>
        <translation>پەيشەنبە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="301"/>
        <source>Friday</source>
        <translation>جۈمە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="304"/>
        <source>Saturday</source>
        <translation>شەنبە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="307"/>
        <source>Sunday</source>
        <translation>يەكشەنبە</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="310"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
</context>
</TS>
