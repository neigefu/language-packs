<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>AddCityAction</name>
    <message>
        <source>kylin-weather</source>
        <translation type="obsolete">天气</translation>
    </message>
</context>
<context>
    <name>Air</name>
    <message>
        <source>Air</source>
        <translation type="vanished">空气指数</translation>
    </message>
</context>
<context>
    <name>AllowLocation</name>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="24"/>
        <source>Whether to enable application locating?</source>
        <translation>是否啟用應用程式定位？</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="48"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="52"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>ChangeCityWidget</name>
    <message>
        <location filename="../view/horscreen/changecitywidget.cpp" line="137"/>
        <source>Add favorite cities to reach the upper limit !</source>
        <translation>添加都市已達到上限！</translation>
    </message>
</context>
<context>
    <name>CityCollectionWidget</name>
    <message>
        <source>Kylin Weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Collections</source>
        <translation type="vanished">收藏城市</translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="vanished">当前城市</translation>
    </message>
    <message>
        <source>Current Network Exception, Please Check Network Settings</source>
        <translation type="vanished">当前网络异常，请检查网络设置</translation>
    </message>
</context>
<context>
    <name>Clothe</name>
    <message>
        <source>Clothe</source>
        <translation type="vanished">穿衣指数</translation>
    </message>
</context>
<context>
    <name>CollectCity</name>
    <message>
        <location filename="../view/search/collectcity.cpp" line="34"/>
        <source>Collect City</source>
        <translation>收集城市</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="38"/>
        <location filename="../view/search/collectcity.cpp" line="122"/>
        <location filename="../view/search/collectcity.cpp" line="149"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="40"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="123"/>
        <location filename="../view/search/collectcity.cpp" line="150"/>
        <source>Collection City</source>
        <translation>收藏都市</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="144"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="145"/>
        <source>City Management</source>
        <translation>城市管理</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../controller/core/core.cpp" line="448"/>
        <location filename="../controller/core/core.cpp" line="454"/>
        <source>Seek failed!</source>
        <translation>尋找失敗！</translation>
    </message>
    <message>
        <location filename="../controller/core/core.cpp" line="782"/>
        <source>Collection City</source>
        <translation>收藏都市</translation>
    </message>
</context>
<context>
    <name>DataParser</name>
    <message>
        <location filename="../model/dataparser.cpp" line="28"/>
        <location filename="../model/dataparser.cpp" line="85"/>
        <location filename="../model/dataparser.cpp" line="95"/>
        <location filename="../model/dataparser.cpp" line="106"/>
        <location filename="../model/dataparser.cpp" line="184"/>
        <location filename="../model/dataparser.cpp" line="243"/>
        <location filename="../model/dataparser.cpp" line="290"/>
        <location filename="../model/dataparser.cpp" line="323"/>
        <location filename="../model/dataparser.cpp" line="378"/>
        <source>Data is empty</source>
        <translation>數據為空</translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="394"/>
        <source>hourly != 24</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="402"/>
        <source>hourly key is wrong</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ErrorReact</name>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="16"/>
        <source>Network not connected!</source>
        <translation>網路未連接！</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="19"/>
        <source>Network error!</source>
        <translation>網路錯誤！</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="25"/>
        <source>Abnormal access address!</source>
        <translation>訪問地址異常！</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="31"/>
        <source>Data parsing error!</source>
        <translation>數據解析錯誤！</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="49"/>
        <source>Network Timeout!</source>
        <translation>網路超時！</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="55"/>
        <source>Seek failed!</source>
        <translation>尋找失敗！</translation>
    </message>
    <message>
        <source>seek failed!</source>
        <translation type="vanished">定位失败！</translation>
    </message>
</context>
<context>
    <name>Flu</name>
    <message>
        <source>Flu</source>
        <translation type="vanished">感冒指数</translation>
    </message>
</context>
<context>
    <name>HotCity</name>
    <message>
        <location filename="../view/search/hotcity.cpp" line="9"/>
        <source>Hot City</source>
        <translation>熱門城市</translation>
    </message>
</context>
<context>
    <name>Information</name>
    <message>
        <source>Today</source>
        <translation type="vanished">今天</translation>
    </message>
</context>
<context>
    <name>LeftUpSearchBox</name>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Open Kylin Weather</source>
        <translation type="vanished">打开麒麟天气</translation>
    </message>
    <message>
        <source>Add City</source>
        <translation type="vanished">增加城市</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Open Weather</source>
        <translation type="vanished">打开天气</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Network not connected</source>
        <translation type="vanished">网络未连接</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Incorrect access address</source>
        <translation type="vanished">访问地址异常</translation>
    </message>
    <message>
        <source>Network error code:%1</source>
        <translation type="vanished">网络错误代码：%1</translation>
    </message>
    <message>
        <source>Kylin Weather</source>
        <translation type="obsolete">麒麟天气</translation>
    </message>
</context>
<context>
    <name>PromptWidget</name>
    <message>
        <location filename="../view/horscreen/promptwidget.cpp" line="20"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <source>retry</source>
        <translation type="vanished">重试</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>SearchCityLIst</name>
    <message>
        <location filename="../view/search/searchcitylist.cpp" line="46"/>
        <location filename="../view/search/searchcitylist.cpp" line="108"/>
        <source>there is no search city!</source>
        <translation>沒有搜索城市！</translation>
    </message>
</context>
<context>
    <name>SearchView</name>
    <message>
        <location filename="../view/search/searchview.cpp" line="184"/>
        <source>Add favorite cities to reach the upper limit !</source>
        <translation>添加都市已達到上限！</translation>
    </message>
</context>
<context>
    <name>Sport</name>
    <message>
        <source>Sport</source>
        <translation type="vanished">运动指数</translation>
    </message>
</context>
<context>
    <name>TitleSearchCityView</name>
    <message>
        <location filename="../view/search/titlesearchcityview.cpp" line="38"/>
        <location filename="../view/search/titlesearchcityview.cpp" line="76"/>
        <location filename="../view/search/titlesearchcityview.cpp" line="141"/>
        <source>there is no search city!</source>
        <translation>沒有搜索城市！</translation>
    </message>
</context>
<context>
    <name>ViewVar</name>
    <message>
        <location filename="../global/viewvar.cpp" line="145"/>
        <source>Beijing</source>
        <translation>北京</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="146"/>
        <source>Shanghai</source>
        <translation>上海</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="147"/>
        <source>Guangzhou</source>
        <translation>廣州</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="148"/>
        <source>Shenzhen</source>
        <translation>深圳</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="149"/>
        <source>Chengdu</source>
        <translation>成都</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="150"/>
        <source>Wuhan</source>
        <translation>武漢</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="151"/>
        <source>Chongqing</source>
        <translation>重慶</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="157"/>
        <source>Hangzhou</source>
        <translation>杭州</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="154"/>
        <source>Nanjing</source>
        <translation>南京</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="159"/>
        <source>Taibei</source>
        <translation>臺北</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="153"/>
        <source>Suzhou</source>
        <translation>蘇州</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="152"/>
        <source>Tianjin</source>
        <translation>天津</translation>
    </message>
    <message>
        <source>Qingdao</source>
        <translation type="vanished">青島</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="156"/>
        <source>Changsha</source>
        <translation>長沙</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="158"/>
        <source>Zhengzhou</source>
        <translation>鄭州</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="155"/>
        <source>Xian</source>
        <translation>西安</translation>
    </message>
    <message>
        <source>Wuxi</source>
        <translation type="vanished">無錫</translation>
    </message>
    <message>
        <source>Jinan</source>
        <translation type="vanished">濟南</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="160"/>
        <source>Hongkong</source>
        <translation>香港</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="161"/>
        <source>Macao</source>
        <translation>澳門</translation>
    </message>
    <message>
        <source>Aomen</source>
        <translation type="vanished">澳门</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="162"/>
        <source>Lasa</source>
        <translation>拉薩</translation>
    </message>
    <message>
        <source>Lijiang</source>
        <translation type="vanished">麗江</translation>
    </message>
</context>
<context>
    <name>WashCar</name>
    <message>
        <source>Wash Car</source>
        <translation type="vanished">洗车指数</translation>
    </message>
</context>
<context>
    <name>air</name>
    <message>
        <location filename="../view/horscreen/air.cpp" line="16"/>
        <source>Air</source>
        <translation>空氣</translation>
    </message>
    <message>
        <location filename="../view/horscreen/air.cpp" line="18"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
</context>
<context>
    <name>city</name>
    <message>
        <source>Switch</source>
        <translation type="vanished">切换</translation>
    </message>
    <message>
        <source>【City】</source>
        <translation type="vanished">【城市】</translation>
    </message>
    <message>
        <source>【Change】</source>
        <translation type="vanished">【變更】</translation>
    </message>
</context>
<context>
    <name>clothe</name>
    <message>
        <location filename="../view/horscreen/clothe.cpp" line="17"/>
        <source>Clothe</source>
        <translation>衣服</translation>
    </message>
</context>
<context>
    <name>flu</name>
    <message>
        <location filename="../view/horscreen/flu.cpp" line="17"/>
        <source>Flu</source>
        <translation>流感</translation>
    </message>
</context>
<context>
    <name>horscreen</name>
    <message>
        <source>Retract</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <source>【Retract】</source>
        <translation type="vanished">【收回】</translation>
    </message>
    <message>
        <source>【City】</source>
        <translation type="vanished">【城市】</translation>
    </message>
    <message>
        <source>【Change】</source>
        <translation type="vanished">【變更】</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="362"/>
        <source>full srceen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="367"/>
        <source>recovery</source>
        <translation>恢復</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="32"/>
        <location filename="../view/horscreen/horscreen.cpp" line="481"/>
        <source>Weather</source>
        <translation>天氣</translation>
    </message>
    <message>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation type="obsolete">天氣是一個天氣資訊工具，它可以瀏覽天氣狀況和生活索引。</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="411"/>
        <location filename="../view/horscreen/horscreen.cpp" line="413"/>
        <source>kylin-weather</source>
        <translation>麒麟天氣</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="414"/>
        <source>About to switch to default City</source>
        <translation>即將切換到預設城市</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="488"/>
        <source>Open Weather</source>
        <translation>打開天氣</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="489"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Switch</source>
        <translation type="vanished">切换</translation>
    </message>
</context>
<context>
    <name>information</name>
    <message>
        <location filename="../view/horscreen/information.cpp" line="26"/>
        <source>Hourly Weather</source>
        <translation>時刻天氣</translation>
    </message>
    <message>
        <location filename="../view/horscreen/information.cpp" line="27"/>
        <source>Daily Wather</source>
        <translation>每日天氣</translation>
    </message>
    <message>
        <location filename="../view/horscreen/information.cpp" line="28"/>
        <source>Life Index</source>
        <translation>生活指數</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>KylinWeather</source>
        <translation type="vanished">Weather</translation>
    </message>
    <message>
        <source>show kylin-weather test</source>
        <translation type="vanished">show kylin-weather test</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队: </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="26"/>
        <location filename="../view/horscreen/menumodule.cpp" line="141"/>
        <location filename="../view/horscreen/menumodule.cpp" line="157"/>
        <source>Service &amp; Support Team: </source>
        <translation>服務與支持團隊： </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="34"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="49"/>
        <location filename="../view/horscreen/menumodule.cpp" line="98"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="54"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="56"/>
        <location filename="../view/horscreen/menumodule.cpp" line="100"/>
        <source>yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="59"/>
        <location filename="../view/horscreen/menumodule.cpp" line="105"/>
        <source>no</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="74"/>
        <location filename="../view/horscreen/menumodule.cpp" line="96"/>
        <source>About</source>
        <translation>关於</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="78"/>
        <location filename="../view/horscreen/menumodule.cpp" line="94"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="153"/>
        <location filename="../view/horscreen/menumodule.cpp" line="168"/>
        <location filename="../view/horscreen/menumodule.cpp" line="199"/>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation>天氣是一個天氣資訊工具，它可以瀏覽天氣狀況和生活索引。</translation>
    </message>
    <message>
        <source>Weather is a city weather information viewing tool, you can browse the future weather conditions and living index, simple and convenient operation, accurately understand the weather changes.</source>
        <translation type="vanished">天气是一款城市气象信息查看工具，可以快速浏览城市未来的天气情况及生活指数，操作简单便捷，准确了解天气变化。</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="198"/>
        <location filename="../view/horscreen/menumodule.cpp" line="214"/>
        <location filename="../view/horscreen/menumodule.cpp" line="232"/>
        <source>Weather</source>
        <translation>天氣</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="221"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="30"/>
        <location filename="../view/horscreen/menumodule.cpp" line="198"/>
        <location filename="../view/horscreen/menumodule.cpp" line="260"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>indicator china weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Indicator China Weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Support: support@kylinos.cn</source>
        <translation type="vanished">支持：support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>sport</name>
    <message>
        <location filename="../view/horscreen/sport.cpp" line="17"/>
        <source>Sport</source>
        <translation>運動</translation>
    </message>
</context>
<context>
    <name>title</name>
    <message>
        <source>kylin-weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="18"/>
        <source>Weather</source>
        <translation>天氣</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="26"/>
        <source>Search City/Region</source>
        <translation>蒐索都市/地區</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="40"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="51"/>
        <source>full screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="60"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>ultravioletrays</name>
    <message>
        <location filename="../view/horscreen/ultravioletrays.cpp" line="17"/>
        <source>UV</source>
        <translation>紫外線</translation>
    </message>
</context>
<context>
    <name>washcar</name>
    <message>
        <location filename="../view/horscreen/washcar.cpp" line="17"/>
        <source>Wash Car</source>
        <translation>洗車</translation>
    </message>
</context>
<context>
    <name>weekweather</name>
    <message>
        <source>今天</source>
        <translation type="vanished">今天</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="280"/>
        <source>-</source>
        <translation>月</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="280"/>
        <source> </source>
        <translation>日</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="59"/>
        <source>Today</source>
        <translation>今天</translation>
    </message>
</context>
</TS>
