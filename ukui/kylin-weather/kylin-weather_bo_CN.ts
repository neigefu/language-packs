<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AddCityAction</name>
    <message>
        <source>kylin-weather</source>
        <translation type="obsolete">天气</translation>
    </message>
</context>
<context>
    <name>Air</name>
    <message>
        <source>Air</source>
        <translation type="vanished">空气指数</translation>
    </message>
</context>
<context>
    <name>AllowLocation</name>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="28"/>
        <source>Whether to enable application locating?</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་གྱི་གནས་གཏན་འཁེལ་བྱེད་ཐུབ་མིན་ལ་ཐུག་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/allowlocation.cpp" line="56"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>CityCollectionWidget</name>
    <message>
        <source>Kylin Weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Collections</source>
        <translation type="vanished">收藏城市</translation>
    </message>
    <message>
        <source>Local</source>
        <translation type="vanished">当前城市</translation>
    </message>
    <message>
        <source>Current Network Exception, Please Check Network Settings</source>
        <translation type="vanished">当前网络异常，请检查网络设置</translation>
    </message>
</context>
<context>
    <name>Clothe</name>
    <message>
        <source>Clothe</source>
        <translation type="vanished">穿衣指数</translation>
    </message>
</context>
<context>
    <name>CollectCity</name>
    <message>
        <location filename="../view/search/collectcity.cpp" line="10"/>
        <source>Collect City</source>
        <translation>གྲོང་ཁྱེར་འཚོལ་སྡུད་</translation>
    </message>
    <message>
        <location filename="../view/search/collectcity.cpp" line="16"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../controller/core/core.cpp" line="232"/>
        <location filename="../controller/core/core.cpp" line="237"/>
        <source>Seek failed!</source>
        <translation>ཕམ་ཉེས་བྱུང་བར་བརྩོན་ལེན་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>DataParser</name>
    <message>
        <location filename="../model/dataparser.cpp" line="28"/>
        <location filename="../model/dataparser.cpp" line="47"/>
        <location filename="../model/dataparser.cpp" line="57"/>
        <location filename="../model/dataparser.cpp" line="125"/>
        <location filename="../model/dataparser.cpp" line="175"/>
        <location filename="../model/dataparser.cpp" line="208"/>
        <location filename="../model/dataparser.cpp" line="263"/>
        <source>Data is empty</source>
        <translation>གཞི་གྲངས་ནི་སྟོང་བ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="278"/>
        <source>hourly != 24</source>
        <translation>ཆུ་ཚོད་རེ། != 24</translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="286"/>
        <source>hourly key is wrong</source>
        <translation>ཆུ་ཚོད་ལྡེ་མིག་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="306"/>
        <source>Hourly is not array</source>
        <translation>ཆུ་ཚོད་ནི་གྲངས་ཚོ་།</translation>
    </message>
    <message>
        <location filename="../model/dataparser.cpp" line="313"/>
        <source>Hourly is not exits</source>
        <translation>ཆུ་ཚོད་རེར་ཕྱིར་མི་གཏོང་།</translation>
    </message>
</context>
<context>
    <name>ErrorReact</name>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="16"/>
        <source>Network not connected!</source>
        <translation>དྲ་རྒྱ་དང་འབྲེལ་མཐུད་མི་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="19"/>
        <source>Network error!</source>
        <translation>དྲ་རྒྱའི་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="25"/>
        <source>Abnormal access address!</source>
        <translation>རྒྱུན་ལྡན་མིན་པའི་ཆོག་འཐུས་ས་གནས</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="31"/>
        <source>Data parsing error!</source>
        <translation>གཞི་གྲངས་ལ་དབྱེ་ཞིབ་བྱེད་པའི་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="49"/>
        <source>Network Timeout!</source>
        <translation>དྲ་རྒྱའི་དུས་ཚོད་ཕྱིར་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../controller/core/errorreact.cpp" line="55"/>
        <source>Seek failed!</source>
        <translation>ཕམ་ཉེས་བྱུང་བར་བརྩོན་ལེན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>seek failed!</source>
        <translation type="vanished">定位失败！</translation>
    </message>
</context>
<context>
    <name>Flu</name>
    <message>
        <source>Flu</source>
        <translation type="vanished">感冒指数</translation>
    </message>
</context>
<context>
    <name>HotCity</name>
    <message>
        <location filename="../view/search/hotcity.cpp" line="9"/>
        <source>Hot City</source>
        <translation>ཚ་ཚ་འུར་འུར་གྱི</translation>
    </message>
</context>
<context>
    <name>Information</name>
    <message>
        <source>Today</source>
        <translation type="vanished">今天</translation>
    </message>
</context>
<context>
    <name>LeftUpSearchBox</name>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Open Kylin Weather</source>
        <translation type="vanished">打开麒麟天气</translation>
    </message>
    <message>
        <source>Add City</source>
        <translation type="vanished">增加城市</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Open Weather</source>
        <translation type="vanished">打开天气</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Network not connected</source>
        <translation type="vanished">网络未连接</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Incorrect access address</source>
        <translation type="vanished">访问地址异常</translation>
    </message>
    <message>
        <source>Network error code:%1</source>
        <translation type="vanished">网络错误代码：%1</translation>
    </message>
    <message>
        <source>Kylin Weather</source>
        <translation type="obsolete">麒麟天气</translation>
    </message>
</context>
<context>
    <name>PromptWidget</name>
    <message>
        <location filename="../view/horscreen/promptwidget.cpp" line="21"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>retry</source>
        <translation type="vanished">重试</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="81"/>
        <source>Weather</source>
        <translation>གནམ་གཤིས་</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <source>Search</source>
        <translation type="vanished">འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>SearchCityLIst</name>
    <message>
        <location filename="../view/search/searchcitylist.cpp" line="46"/>
        <location filename="../view/search/searchcitylist.cpp" line="105"/>
        <source>there is no search city!</source>
        <translation>དེ་རུ་འཚོལ་ཞིབ་གྲོང་ཁྱེར་མེད།</translation>
    </message>
</context>
<context>
    <name>Sport</name>
    <message>
        <source>Sport</source>
        <translation type="vanished">运动指数</translation>
    </message>
</context>
<context>
    <name>ViewVar</name>
    <message>
        <location filename="../global/viewvar.cpp" line="123"/>
        <source>Beijing</source>
        <translation>པེ་ཅིན་</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="124"/>
        <source>Shanghai</source>
        <translation>ཧྲང་ཧའེ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="125"/>
        <source>Guangzhou</source>
        <translation>ཀོང་ཀྲོའུ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="126"/>
        <source>Shenzhen</source>
        <translation>ཧྲེན་ཀྲེན།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="127"/>
        <source>Chengdu</source>
        <translation>ཁྲེང་ཏུའུ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="128"/>
        <source>Wuhan</source>
        <translation>ཝུའུ་ཧན།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="129"/>
        <source>Chongqing</source>
        <translation>ཁྲུང་ཆིང་།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="135"/>
        <source>Hangzhou</source>
        <translation>ཧང་ཀྲོའུ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="132"/>
        <source>Nanjing</source>
        <translation>ནན་ཅིན་</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="137"/>
        <source>Taibei</source>
        <translation>ཐའེ་པེ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="131"/>
        <source>Suzhou</source>
        <translation>སུའུ་ཀྲོའུ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="130"/>
        <source>Tianjin</source>
        <translation>ཐེན་ཅིན་གྲོང་ཁྱེར།</translation>
    </message>
    <message>
        <source>Qingdao</source>
        <translation type="vanished">ཆིང་ཏའོ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="134"/>
        <source>Changsha</source>
        <translation>ཁྲང་ཧྲ་</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="136"/>
        <source>Zhengzhou</source>
        <translation>ཀྲེང་ཀྲོའུ།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="133"/>
        <source>Xian</source>
        <translation>ཞན་ཨན།</translation>
    </message>
    <message>
        <source>Wuxi</source>
        <translation type="vanished">ཝུའུ་ཞི།</translation>
    </message>
    <message>
        <source>Jinan</source>
        <translation type="vanished">ཅི་ནན།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="138"/>
        <source>Hongkong</source>
        <translation>ཞང་ཀང་།</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="139"/>
        <source>Macao</source>
        <translation>ཨའོ་མོན།</translation>
    </message>
    <message>
        <source>Aomen</source>
        <translation type="vanished">澳门</translation>
    </message>
    <message>
        <location filename="../global/viewvar.cpp" line="140"/>
        <source>Lasa</source>
        <translation>ལྷ་ས།</translation>
    </message>
    <message>
        <source>Lijiang</source>
        <translation type="vanished">ལི་ཅང་།</translation>
    </message>
</context>
<context>
    <name>WashCar</name>
    <message>
        <source>Wash Car</source>
        <translation type="vanished">洗车指数</translation>
    </message>
</context>
<context>
    <name>air</name>
    <message>
        <location filename="../view/horscreen/air.cpp" line="16"/>
        <source>Air</source>
        <translation>མཁའ་དབུགས།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/air.cpp" line="18"/>
        <source>N/A</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>city</name>
    <message>
        <source>Switch</source>
        <translation type="vanished">切换</translation>
    </message>
    <message>
        <source>【City】</source>
        <translation type="vanished">【城市】</translation>
    </message>
    <message>
        <location filename="../view/horscreen/city.cpp" line="71"/>
        <source>【Change】</source>
        <translation>【འགྱུར་ལྡོག】</translation>
    </message>
</context>
<context>
    <name>clothe</name>
    <message>
        <location filename="../view/horscreen/clothe.cpp" line="17"/>
        <source>Clothe</source>
        <translation>རས་ཆ།</translation>
    </message>
</context>
<context>
    <name>flu</name>
    <message>
        <location filename="../view/horscreen/flu.cpp" line="17"/>
        <source>Flu</source>
        <translation>ཆམ་རིམས་</translation>
    </message>
</context>
<context>
    <name>horscreen</name>
    <message>
        <source>Retract</source>
        <translation type="vanished">还原</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="168"/>
        <source>【Retract】</source>
        <translation>【ཕྱིར་འཐེན་】</translation>
    </message>
    <message>
        <source>【City】</source>
        <translation type="vanished">【城市】</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="172"/>
        <location filename="../view/horscreen/horscreen.cpp" line="565"/>
        <location filename="../view/horscreen/horscreen.cpp" line="579"/>
        <source>【Change】</source>
        <translation>【འགྱུར་ལྡོག】</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="247"/>
        <source>full srceen</source>
        <translation>ཕྱོགས་ཡོངས་ནས་ངོ་རྒོལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="252"/>
        <source>recovery</source>
        <translation>སླར་གསོ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="33"/>
        <location filename="../view/horscreen/horscreen.cpp" line="366"/>
        <source>Weather</source>
        <translation>གནམ་གཤིས་</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation type="vanished">གནམ་གཤིས་ནི་གནམ་གཤིས་ཆ་འཕྲིན་གྱི་ཡོ་བྱད་ཅིག་ཡིན་པས་གནམ་གཤིས་ཀྱི་གནས་ཚུལ་དང་འཚོ་བའི་སྟོན་གྲངས་ལ་ལྟ་ཞིབ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="296"/>
        <location filename="../view/horscreen/horscreen.cpp" line="298"/>
        <source>kylin-weather</source>
        <translation>ཅིན་ལིན་གྱི་གནམ་གཤིས་</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="299"/>
        <source>About to switch to default City</source>
        <translation>མི་རིང་བར་བརྗེ་ནས་སོར་བཞག་གྲོང་ཁྱེར་།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="376"/>
        <source>Open Weather</source>
        <translation>སྒོ་འབྱེད་པའི་གནམ་གཤིས།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/horscreen.cpp" line="377"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Switch</source>
        <translation type="vanished">切换</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>KylinWeather</source>
        <translation type="vanished">Weather</translation>
    </message>
    <message>
        <source>show kylin-weather test</source>
        <translation type="vanished">show kylin-weather test</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队: </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="26"/>
        <location filename="../view/horscreen/menumodule.cpp" line="139"/>
        <location filename="../view/horscreen/menumodule.cpp" line="155"/>
        <source>Service &amp; Support Team: </source>
        <translation>ཞབས་ཞུ་དང་རོགས་སྐྱོར་རུ་ཁག་སྟེ། </translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="34"/>
        <source>Options</source>
        <translation>འདེམས་ཚན་</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="49"/>
        <location filename="../view/horscreen/menumodule.cpp" line="98"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="54"/>
        <source>Location</source>
        <translation>གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="56"/>
        <location filename="../view/horscreen/menumodule.cpp" line="100"/>
        <source>yes</source>
        <translation>ཡེ་ཡ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="59"/>
        <location filename="../view/horscreen/menumodule.cpp" line="105"/>
        <source>no</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="74"/>
        <location filename="../view/horscreen/menumodule.cpp" line="96"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="78"/>
        <location filename="../view/horscreen/menumodule.cpp" line="94"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="151"/>
        <location filename="../view/horscreen/menumodule.cpp" line="166"/>
        <location filename="../view/horscreen/menumodule.cpp" line="197"/>
        <source>Weather is a weather information tool, it can browse the weather conditions and living index.</source>
        <translation>གནམ་གཤིས་ནི་གནམ་གཤིས་ཆ་འཕྲིན་གྱི་ཡོ་བྱད་ཅིག་ཡིན་པས་གནམ་གཤིས་ཀྱི་གནས་ཚུལ་དང་འཚོ་བའི་སྟོན་གྲངས་ལ་ལྟ་ཞིབ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="219"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weather is a city weather information viewing tool, you can browse the future weather conditions and living index, simple and convenient operation, accurately understand the weather changes.</source>
        <translation type="vanished">天气是一款城市气象信息查看工具，可以快速浏览城市未来的天气情况及生活指数，操作简单便捷，准确了解天气变化。</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="196"/>
        <location filename="../view/horscreen/menumodule.cpp" line="212"/>
        <location filename="../view/horscreen/menumodule.cpp" line="230"/>
        <source>Weather</source>
        <translation>གནམ་གཤིས་</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/menumodule.cpp" line="30"/>
        <location filename="../view/horscreen/menumodule.cpp" line="196"/>
        <location filename="../view/horscreen/menumodule.cpp" line="258"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>indicator china weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Indicator China Weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <source>Support: support@kylinos.cn</source>
        <translation type="vanished">支持：support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>sport</name>
    <message>
        <location filename="../view/horscreen/sport.cpp" line="17"/>
        <source>Sport</source>
        <translation>ལུས་རྩལ།</translation>
    </message>
</context>
<context>
    <name>title</name>
    <message>
        <source>kylin-weather</source>
        <translation type="vanished">麒麟天气</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="19"/>
        <source>Weather</source>
        <translation>གནམ་གཤིས་</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="33"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="45"/>
        <source>full screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../view/horscreen/title.cpp" line="54"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>ultravioletrays</name>
    <message>
        <location filename="../view/horscreen/ultravioletrays.cpp" line="17"/>
        <source>UV</source>
        <translation>སྨུག་ཕྱིའི་ཐིག་གི་སྟོན་གྲངས་</translation>
    </message>
</context>
<context>
    <name>washcar</name>
    <message>
        <location filename="../view/horscreen/washcar.cpp" line="17"/>
        <source>Wash Car</source>
        <translation>རླངས་འཁོར་འཁྲུད་པ།</translation>
    </message>
</context>
<context>
    <name>weekweather</name>
    <message>
        <source>今天</source>
        <translation type="vanished">今天</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="51"/>
        <source>Today</source>
        <translation>དེ་རིང་།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="270"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="270"/>
        <source> </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="289"/>
        <source>Monday</source>
        <translation>གཟའ་ཟླ་བ་</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="292"/>
        <source>Tuesday</source>
        <translation>གཟའ་མིག་དམར་</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="295"/>
        <source>Wednesday</source>
        <translation>གཟའ་ལྷག་པ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="298"/>
        <source>Thursday</source>
        <translation>གཟའ་ཕུར་བུ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="301"/>
        <source>Friday</source>
        <translation>གཟའ་པ་སངས་</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="304"/>
        <source>Saturday</source>
        <translation>གཟའ་སྤེན་པ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="307"/>
        <source>Sunday</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
    <message>
        <location filename="../view/horscreen/weekweather.cpp" line="310"/>
        <source>N/A</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
