<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>ᠹᠤᠷᠮᠠᠲ</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="28"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>ᠲᠠᠨ ᠤᠤ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠨᠢᠭᠡᠨ ᠭᠠᠷᠴᠠᠭ ᠪᠤᠶᠤ ᠡᠷᠬᠡ ᠦᠬᠡᠢ᠂ ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="31"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="26"/>
        <source>Are you sure to start crushing?</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="32"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="39"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="47"/>
        <source>cancle</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>Kylin Shred Manager</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <source>Shred Manager</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="303"/>
        <source>No file selected to be shredded</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="155"/>
        <source>Shred File</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="37"/>
        <source>FileShredder</source>
        <translation>ᠹᠠᠢᠯ ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="154"/>
        <location filename="../shreddialog.cpp" line="199"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>ᠹᠠᠢᠯ ᠪᠦᠷᠢᠮᠦᠰᠦᠨ ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠳᠠᠵᠤ᠂ ᠳᠠᠬᠢᠨ ᠰᠡᠷᠬᠦᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="156"/>
        <source>Deselect</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠵᠤ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="157"/>
        <source>Add File</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="158"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation>ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ: ᠹᠠᠢᠯ ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠪᠤᠯᠭᠤᠮᠵᠢᠳᠠᠢ ᠠᠵᠢᠯᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Note: The file shredding process cannot be cancelled, please operate with caution!</source>
        <translation type="vanished">注意：文件粉碎操作不可取消，请谨慎操作！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="231"/>
        <source>File Shredding ...</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠤᠨᠳᠠᠭᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="288"/>
        <source>Please select the file to shred!</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ.!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="293"/>
        <source>You did not select a file with permissions!</source>
        <translation>ᠲᠠᠨ ᠤᠤ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠨᠢ ᠨᠢᠭᠡᠨ ᠡᠷᠬᠡ ᠲᠠᠢ ᠹᠠᠢᠯ ᠪᠢᠰᠢ!</translation>
    </message>
    <message>
        <source>Shattering...</source>
        <translation type="vanished">粉碎中...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>Select file</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>All Files(*)</source>
        <translation>ᠪᠦᠬᠦ ᠹᠠᠢᠯ (*)</translation>
    </message>
    <message>
        <source>Select file!</source>
        <translation type="vanished">请选择文件！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="253"/>
        <source>Shred successfully!</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠤᠨᠳᠠᠭᠯᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="266"/>
        <source>Shred failed!</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠤᠨᠳᠠᠭᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="50"/>
        <source>Shred Manager</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="55"/>
        <source>Delete files makes it unable to recover</source>
        <translation>ᠨᠤᠨᠳᠠᠭᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
</context>
</TS>
