<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printer</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printer-backend</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་རྒྱབ་རྟེན་</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation>ཁ་སྣོན་བྱས་པའི་ཁ་སྣོན་བྱས་པའི་ཁ་སྣོན་བྱས་པའི་</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="136"/>
        <source>Add Printer</source>
        <translation>དཔར་ཆས་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="148"/>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="149"/>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="161"/>
        <source>Device List</source>
        <translation>སྒྲིག་ཆས་ཀྱི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>Protocol</source>
        <translation>གྲོས་ཆོད།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="193"/>
        <source>Address</source>
        <translation>སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="199"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <source>socket</source>
        <translation type="vanished">བསྒར་ཁུང་།</translation>
    </message>
    <message>
        <source>ipp</source>
        <translation type="vanished">ipp</translation>
    </message>
    <message>
        <source>http</source>
        <translation type="vanished">http</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="213"/>
        <source>name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="214"/>
        <source>location</source>
        <translation>གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="215"/>
        <source>driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="321"/>
        <source>Success</source>
        <translation>ལེགས་འགྲུབ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="322"/>
        <source>Fail</source>
        <translation>ཕམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="220"/>
        <source>forward</source>
        <translation>མདུན་དུ་སྐྱོད་པ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>ཁ་སྣོན་བྱས་པའི་དཔར་ཆས་ལ་ཕམ་ཁ་བྱུང་བ་སྟེ། PPDབདམས་མེད་</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>དཔར་ཆས་ཁ་སྣོན་བྱས་ནས་ཕམ་སོང་། ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="304"/>
        <location filename="../ui/add_printer_window.cpp" line="314"/>
        <location filename="../ui/add_printer_window.cpp" line="333"/>
        <location filename="../ui/add_printer_window.cpp" line="433"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <location filename="../ui/add_printer_window.cpp" line="561"/>
        <location filename="../ui/add_printer_window.cpp" line="603"/>
        <location filename="../ui/add_printer_window.cpp" line="648"/>
        <location filename="../ui/add_printer_window.cpp" line="687"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>དཔར་ཆས་ལེགས་འགྲུབ་བྱུང་ན་དཔར་ཆས་ལ་ཚོད་ལྟའི་ཤོག་ལྷེ་ཞིག་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="313"/>
        <source>Print test page</source>
        <translation>པར་སྐྲུན་ཚོད་ལྟའི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <source>Is the test page printed successfully?</source>
        <translation>ཚད་ལེན་ཚོད་ལྟའི་ཤོག་ལྷེ་དེ་ལེགས་འགྲུབ་བྱུང་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>ཚོད་ལྟའི་པར་སྐྲུན་ལ་ཕམ་ཉེས་བྱུང་། ཁྱོད་ཀྱིས་དཔར་ཆས་ཀྱི་ཁ་ལོ་བ་ཞིག་བརྗེ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <source>Automatic driver configuration failed, please select the driver manually.</source>
        <translation>རང་འགུལ་གྱིས་བཀོད་སྒྲིག་ལ་བརྟེན་ནས་ཕམ་ཁ་བྱུང་བར་སྐུལ་འདེད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="578"/>
        <source>Searching for driver...</source>
        <translation>འཚོལ་ཞིབ་ལ་བརྟེན་ནས་སྐུལ་འདེད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <source>Failed to search for driver. Do you want to use local driver?</source>
        <translation>འཚོལ་ཞིབ་ལ་བརྟེན་ནས་ཕམ་ཁ་བྱུང་ན་ས་གནས་དེ་གའི་སྐུལ་འདེད་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="620"/>
        <source>It is detected that the driver comes from the Internet. Do you want to continue the installation?</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་པའི་གོ་རིམ་ནི་དྲ་སྦྲེལ་ལས་བྱུང་བ་ཡིན་པས། མུ་མཐུད་དུ་སྒྲིག་སྦྱོར་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="625"/>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="639"/>
        <source>The device driver is being adapted.</source>
        <translation>སྒྲིག་ཆས་ཀྱི་བཟོ་དབྱིབས་འདིས་སྐུལ་འདེད་འོས་འཚམ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="642"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>ཚོང་ལས་གྲོས་མཐུན་གྱི་ཚོད་འཛིན་ཐེབས་པའི་དབང་གིས་སྒྲིག་ཆས་བཟོ་མཁན་དང་འབྲེལ་བ་བྱས་ནས་སྒྲིག་ཆས་ཀྱི་བཟོ་དབྱིབས་དེ་ཐོབ་པར་བྱེད་པའི་གོ་རིམ་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="659"/>
        <source>Installing printer driver...</source>
        <translation>སྐུལ་འདེད་བྱེད་བཞིན་པའི་སྒང་ཡིན་པས་ཅུང་ཙམ་འགོར་རྗེས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <source>Driver installation failed. Do you want to use local driver?</source>
        <translation>སྒྲིག་སྦྱོར་ལ་ཕམ་ཉེས་བྱུང་ན་ས་གནས་དེ་གའི་སྐུལ་འདེད་བཀོལ་སྤྱོད་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <source>Change Driver</source>
        <translation type="vanished">ཁ་ལོ་བ་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="604"/>
        <location filename="../ui/add_printer_window.cpp" line="626"/>
        <location filename="../ui/add_printer_window.cpp" line="688"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="362"/>
        <source>Searching printers...</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་འཚོལ་ཞིབ་བྱེད་པ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Can not find this Printer!</source>
        <translation>དཔར་ཆས་འདི་རྙེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="432"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <location filename="../ui/add_printer_window.cpp" line="622"/>
        <location filename="../ui/add_printer_window.cpp" line="645"/>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <location filename="../ui/add_printer_window.cpp" line="747"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>Searching printer driver...</source>
        <translation type="vanished">དཔར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཁ་ལོ་བ་འཚོལ་ཞིབ</translation>
    </message>
    <message>
        <source>Install driver package automatically failed,continue?</source>
        <translation type="vanished">ཁ་ལོ་བའི་ཐུམ་སྒྲིལ་རང་འགུལ་གྱིས་ཕམ་ཉེས་བྱུང་ནས་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="749"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/auto_search_result_model.cpp" line="49"/>
        <location filename="../ui/auto_search_result_model.cpp" line="51"/>
        <source>network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
</context>
<context>
    <name>BaseInfo</name>
    <message>
        <location filename="../common/base_info.cpp" line="64"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="137"/>
        <source>Choose PPD</source>
        <translation>PPDབདམས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>PPDདཔེ་མཛོད་ཁང་ནས་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>ས་གནས་དེ་གའི་PPDབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>ཁྱེད་ཀྱིས་བདེ་མོ་མཛོད་ཀྱི་ཁུག་མ་ཞིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>Deb File(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>གདམ་གསེས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>འཚོལ་ཞིབ་ཁ་ལོ་བ་ ...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ནས་ཕམ་ཉེས་བྱུང་ན་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="75"/>
        <source>Common configuration</source>
        <translation>ཐུན་མོང་དུ་སྤྱོད་པའི་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="150"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>ཁ་ན་api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="180"/>
        <source>Advanced Features</source>
        <translation>ནུས་པ་མཐོན་པོ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="30"/>
        <source>Configure</source>
        <translation>བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="31"/>
        <source>Service address:</source>
        <translation>ཞབས་ཞུའི་ས་གནས་ནི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="32"/>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="34"/>
        <source>Enable</source>
        <translation>སྤྱོད་འགོ་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="33"/>
        <source>This printer can be used to convert print files to PDF format</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་འདི་བཀོལ་ནས་ཡིག་ཆ་དེ་PDFཡི་རྣམ་པར་བསྒྱུར་ཆོག</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="35"/>
        <source>This printer can be used to convert print files to BRF format</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་འདི་བཀོལ་ནས་ཡིག་ཆ་དེ་BRFཡི་རྣམ་པར་བསྒྱུར་ཆོག</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="36"/>
        <source>Displays all serial printers</source>
        <translation>ལྐོག་དཔར་འཕྲུལ་ཆས་ཡོད་ཚད་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="38"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Config init failed</source>
        <translation>ཁུང་ཙིའི་ནང་དུ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="53"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translation>གྲལ་བཤེར་བྱེད་པར་ཚོད་ལྟ་བྱེད་པའི་ཆ་འཕྲིན་སོར་འཇོག་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>CustomAboutDialog</name>
    <message>
        <location filename="../ui/custom_ui/custom_about_dialog.cpp" line="12"/>
        <source>Version: </source>
        <translation>པར་གཞི།</translation>
    </message>
</context>
<context>
    <name>CustomDoublelineCheckBox</name>
    <message>
        <location filename="../ui/custom_ui/custom_doubleline_checkbox.ui" line="14"/>
        <source>Form</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཁ་ལོ་བར་བཟོ་བཅོས་རྒྱག</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation>PPDལ་བཟོ་བཅོས་བརྒྱབ་ནས་ཕམ་ཁ་བྱུང་བ་སྟེ། PPDབདམས་ཐོན་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <location filename="../ui/device_list_button.cpp" line="32"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="35"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="38"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="41"/>
        <source>Unknow</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="62"/>
        <location filename="../ui/device_list_button.cpp" line="264"/>
        <location filename="../ui/device_list_button.cpp" line="296"/>
        <source>Default</source>
        <translation>ཁ་ཆད་དང་འགལ་</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="133"/>
        <location filename="../ui/device_list_button.cpp" line="163"/>
        <source>Set Default</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་གནས</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="135"/>
        <location filename="../ui/device_list_button.cpp" line="166"/>
        <source>Enabled</source>
        <translation>ནུས་པ་ཐོན་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="139"/>
        <location filename="../ui/device_list_button.cpp" line="169"/>
        <source>Shared</source>
        <translation>ཐུན་མོང་དུ་ལོངས་</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../backend/device_map.cpp" line="292"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="294"/>
        <source>Printers</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="296"/>
        <source>plug-in:</source>
        <translation>plug-in:</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="298"/>
        <source>unplugged:</source>
        <translation>ཁ་གསལ་མེད་པ་སྟེ།</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="96"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation>&quot;+&quot;མཐེབ་གཅུས་བརྒྱབ་ནས་དཔར་ཆས་ཤིག་ཁ་སྣོན་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར། </translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>ལས་ཀ་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>གསར་སྐྲུན་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ་རེད།</translation>
    </message>
</context>
<context>
    <name>InkInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/ink_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="50"/>
        <source>Test Page</source>
        <translation>ཚོད་ལྟའི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="53"/>
        <source>untitled</source>
        <translation>ཡིག་རྨོངས་སེལ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="64"/>
        <source>unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="70"/>
        <source>Printing page %1</source>
        <translation>ཤོག་ངོས་1པར་རྒྱག་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="261"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="412"/>
        <source>Cancel print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་མེད་པར</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="415"/>
        <source>Delete print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="418"/>
        <source>Hold print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་ཉར་འཛིན</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="421"/>
        <source>Release print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་འགྲེམས་སྤེལ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="424"/>
        <source>Reprint</source>
        <translation>བསྐྱར་དུ་བྱུགས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="427"/>
        <source>Job properties</source>
        <translation>ལས་འགན་གྱི་ངོ་བོ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <source>Set error: Job status has been updated!</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བ་སྟེ། ལས་གནས་ཀྱི་གནས་ཚུལ་གསར་སྒྱུར་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="447"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="453"/>
        <source>Sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Cannot move job to itself!</source>
        <translation>ལས་ཀ་རང་ཉིད་ལ་སྤོ་སྒྱུར་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="108"/>
        <source>Cancel print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་མེད་པར</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="110"/>
        <source>Delete print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="112"/>
        <source>Hold print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་ཉར་འཛིན</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="114"/>
        <source>Release print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་འགྲེམས་སྤེལ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="116"/>
        <source>Reprint</source>
        <translation>བསྐྱར་དུ་ཐེལ་ཙེ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="118"/>
        <source>Use other printer...</source>
        <translation>དཔར་ཆས་གཞན་དག་བཀོལ་སྤྱོད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="120"/>
        <source>Job properties</source>
        <translation>ལས་འགན་གྱི་ངོ་བོ།</translation>
    </message>
</context>
<context>
    <name>JobPropertiesWindow</name>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="15"/>
        <source>Job properties</source>
        <translation>ལས་འགན་གྱི་ངོ་བོ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="35"/>
        <source>name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="36"/>
        <source>value</source>
        <translation>རིན་ཐང་།</translation>
    </message>
</context>
<context>
    <name>LaunchPrinter</name>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>Is the test page printed successfully?</source>
        <translation>ཡིག་དཔར་ཚོད་ལྟའི་ཤོག་ངོས་ལ་ལེགས་འགྲུབ་བྱུང་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>%1 print result confirmation</source>
        <translation>1.ཡིག་དཔར་བྱས་པའི་མཇུག་འབྲས་ལ་ངོས་འཛིན་ཁས་ལེན་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="94"/>
        <source>Yes</source>
        <translation>དེ་ནི་རེད།</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="96"/>
        <source>No</source>
        <translation>དེ་ལྟར་མ་བྱས་</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="47"/>
        <source>Device List</source>
        <translation>སྒྲིག་ཆས་ཀྱི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="362"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="212"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་&quot;%1&quot;བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="216"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="217"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="360"/>
        <source>Set Default Failed!</source>
        <translation>སྔོན་ལ་ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="364"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="45"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="47"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="71"/>
        <source>Property</source>
        <translation>རྒྱུ་ནོར།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="78"/>
        <source>Job List</source>
        <translation>ལས་གནས་ཀྱི་མིང་ཐོ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="85"/>
        <source>PrintTest</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="90"/>
        <source>Troubleshooting</source>
        <translation>བར་ཆད་བྱུང་རིགས་གྲལ་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="190"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="192"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="194"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="196"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="198"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="200"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="205"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="229"/>
        <source>Are you sure to rename &quot;%1&quot; ?</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་མིང་བསྒྱུར་དགོས་པ་ཡིན་ན &quot;%1&quot; ?</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="233"/>
        <source>Rename</source>
        <translation>བསྐྱར་དུ་མིང་བཏགས་པ་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="234"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="230"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="231"/>
        <source>This action will delete the job queue too!</source>
        <translation>བྱ་སྤྱོད་འདིས་ཀྱང་ལས་གནས་ཀྱི་གྲལ་བསྒྲིགས་ནས་བསུབ་རྒྱུ་རེད།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="91"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="225"/>
        <source>Delete Failed!</source>
        <translation>ཕམ་ཁ་བྱུང་བ་དེ་བསུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="227"/>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="229"/>
        <location filename="../ui/mainwindow.cpp" line="269"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>དཔར་ཆས་ཁ་སྣོན་བྱས་ནས་ཕམ་སོང་། ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Try to connect the Printer...</source>
        <translation>ཐབས་བརྒྱ་ཇུས་སྟོང་གིས་དཔར་ཆས་སྦྲེལ་མཐུད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation>ཁ་ཕྱེས་པའི་དཔར་ཆས་ལ་སྐྱོན་ཤོར་བ་དང་། ཏེ་ཨར་དང་དཔར་ཆས་བསྣན་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="333"/>
        <source>Sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="37"/>
        <location filename="../ui/menumodule.cpp" line="69"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="39"/>
        <location filename="../ui/menumodule.cpp" line="66"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="41"/>
        <location filename="../ui/menumodule.cpp" line="72"/>
        <source>Configure</source>
        <translation>བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="43"/>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="52"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation>པར་སྐྲུན་ཚོད་ལྟའི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation>ལྟ་ཀློག་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation>ལག་ཤེས་སྒྲིག་སྦྱོར།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ......</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation>སྒྲིག་སྦྱོར་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ཀྱང་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་པར་འདེབས་འཕྲུལ་འཁོར</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation>ཚད་ལེན་ཚོད་ལྟའི་ཤོག་ལྷེ་དེ་ལེགས་འགྲུབ་བྱུང་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>ཚོད་ལྟའི་པར་སྐྲུན་ལ་ཕམ་ཉེས་བྱུང་། ཁྱོད་ཀྱིས་དཔར་ཆས་ཀྱི་ཁ་ལོ་བ་ཞིག་བརྗེ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation>ཁ་ལོ་བ་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation>འགོ་བརྩམས་མ་ཐུབ་ན་ ཡང་བསྐྱར་དཔར་ཆས་ཁ་སྣོན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>PopWindowManager</name>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Do you want to add a printer?</source>
        <translation>དཔར་ཆས་ཁ་སྣོན་བྱས་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>The printer was detected:%1</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་ནས་དཔར་ཆས་ཀྱི་བརྒྱ་ཆ་1.1ཟིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="167"/>
        <source>The device driver is being adapted.</source>
        <translation>སྒྲིག་ཆས་ཀྱི་བཟོ་དབྱིབས་འདིས་སྐུལ་འདེད་འོས་འཚམ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="170"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>ཚོང་ལས་གྲོས་མཐུན་གྱི་ཚོད་འཛིན་ཐེབས་པའི་དབང་གིས་སྒྲིག་ཆས་བཟོ་མཁན་དང་འབྲེལ་བ་བྱས་ནས་སྒྲིག་ཆས་ཀྱི་བཟོ་དབྱིབས་དེ་ཐོབ་པར་བྱེད་པའི་གོ་རིམ་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="172"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="223"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="237"/>
        <source>%1 installation failed</source>
        <translation>1.སྒྲིག་སྦྱོར་ལེགས་འགྲུབ་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>%1 successful installation</source>
        <translation>1.སྒྲིག་སྦྱོར་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="222"/>
        <source>No printer driver for %1.</source>
        <translation>1%ཡི་དཔར་ཆས་ཀྱི་སྐུལ་འདེད་གོ་རིམ་མེད།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>Print test page</source>
        <translation>ཡིག་དཔར་ཚོད་ལྟ་ཤོག་ལྷེ་དཔར་བ།</translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation>རྒྱུ་ནོར་གྱི་མིང་ཐོའི་རླུང་ཕྱོགས་</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="11"/>
        <source>Refresh</source>
        <translation>གསར་དུ་བཀྲུ་བ།</translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="139"/>
        <source>Model:</source>
        <translation>དཔེ་དབྱིབས་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="140"/>
        <source>Status:</source>
        <translation>གོ་གནས་ནི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="141"/>
        <source>Location:</source>
        <translation>གནས་ཡུལ་ནི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="142"/>
        <source>Driver:</source>
        <translation>ཁ་ལོ་བ་ནི།</translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="23"/>
        <source>base</source>
        <translation>རྟེན་གཞི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="36"/>
        <source>Ink</source>
        <translation>སྣག་ཚ་དང་སྣག་ཚ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>advanced</source>
        <translation>སྔོན་ཐོན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="58"/>
        <source>PrinterProperty</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="95"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="163"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="327"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="96"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="166"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="331"/>
        <source>Location</source>
        <translation>གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="97"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="170"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="335"/>
        <source>Status</source>
        <translation>གནས་ཚུལ་གྱི་གནས་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="98"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="190"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="339"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="380"/>
        <source>Driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="101"/>
        <source>Modify</source>
        <translation>བཟོ་བཅོས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="103"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="193"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="343"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="127"/>
        <source> (level: %1%)</source>
        <translation> (ལྷག་གྲངས། )བརྒྱ་ཆ་1ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="172"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="174"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="176"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="178"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="180"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="182"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="187"/>
        <source>Unknow</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="396"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="412"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="423"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <source>Error happened, all options restored!.</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བས་བསལ་འདེམས་ཚང་མ་སླར་གསོ་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="232"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="269"/>
        <source>Conflict with &quot;%1&quot;</source>
        <translation>&quot;བརྒྱ་ཆ་1&quot;དང་འགལ་བ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="395"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ནང་དུ་&quot;/\&quot;#&quot;དང་འཕྲིན་ཡིག་0ལྷག་ཙམ་ཡོད་པ་དེ་འཕྲིན་ཡིག་128ལས་ཉུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="398"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="414"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="425"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="411"/>
        <source>Exist Same Name Printer!</source>
        <translation>མིང་གཅིག་མཚུངས་ཡིན་པའི་དཔར་ཆས་ཡོད་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="422"/>
        <source>Printer Name Illegal!</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ཁྲིམས་དང་མི་མཐུན</translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="226"/>
        <source>Unknow</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="32"/>
        <source>id</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="33"/>
        <source>user</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="34"/>
        <source>title</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="35"/>
        <source>printer name</source>
        <translation>དཔར་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="36"/>
        <source>size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="37"/>
        <source>create time</source>
        <translation>དུས་ཚོད་གསར་སྐྲུན་བྱེད་</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="38"/>
        <source>job state</source>
        <translation>ལས་གནས་ཀྱི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="44"/>
        <source>Job is waiting to be printed</source>
        <translation>ལས་ཀ་ནི་པར་སྐྲུན་བྱེད་པར་སྒུག་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="45"/>
        <source>Job is held for printing</source>
        <translation>པར་སྐྲུན་ལས་རིགས་ཀྱི་ལས་འགན་འཁུར་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="46"/>
        <source>Job is currently printing</source>
        <translation>མིག་སྔར་པར་སྐྲུན་བྱེད་བཞིན་པའི་ལས་གནས།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="47"/>
        <source>Job has been stopped</source>
        <translation>ལས་ཀ་བྱེད་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="48"/>
        <source>Job has been canceled</source>
        <translation>ལས་ཀ་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="49"/>
        <source>Job has aborted due to error</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བའི་རྐྱེན་གྱིས་ལས་ཀ་ཤོར་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="50"/>
        <source>Job has completed successfully</source>
        <translation>ལས་ཀ་བདེ་བླག་ངང་ལེགས་གྲུབ་བྱུང་ཡོད།</translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation>མིང་ལ་པར་རྒྱག་མཁན་གྱི་ནད་རྟགས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་།</translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation>PppdDialogབདམས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="203"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="247"/>
        <source>Apply</source>
        <translation>རེ་ཞུ་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="23"/>
        <source>Select Driver</source>
        <translation>ཁ་ལོ་བ་གདམ་གསེས་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="56"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="85"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="266"/>
        <source>driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="57"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="71"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="232"/>
        <source>vendor</source>
        <translation>ཚོང་པ་ཉི་ཚེ་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="195"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation>དཔེ་ཆ་ཀློག་པའི་ཁ་ལོ་བ་རྣམས་ཀྱིས་རེ་སྒུག་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="319"/>
        <source>Please Choose Model And Vender!</source>
        <translation>ཁྱོད་ཀྱིས་དཔེ་དབྱིབས་དང་འཁོན་ལན་བས་ཁྱོད་ལ་གདམ་ག་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="320"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="322"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>SmbAuthInfoDialog</name>
    <message>
        <location filename="../ui/smb_auth_info_dialog.ui" line="26"/>
        <source>SmbAuthInfoDialog</source>
        <translation>SmbAuthInfoDialog</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="13"/>
        <source>Authentication</source>
        <translation>ཐོབ་ཐང་དཔང་དངོས་བདེན་པ་ཡིན་པའི་ར</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="14"/>
        <source>Authentication is required to connect to the printer &quot;%1&quot;</source>
        <translation>&quot;1&quot;པར་འདེབས་འཕྲུལ་ཆས་བརྒྱ་ཆ་1ལ་འབྲེལ་མཐུད་བྱེད་པར་བདེན་དཔང་ར་སྤྲོད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="15"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="16"/>
        <source>Password</source>
        <translation>གསང་བའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="17"/>
        <source>Workgroup</source>
        <translation>ས་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="18"/>
        <source>Accept</source>
        <translation>གཏན་འཁེལ་བྱ།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="19"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="37"/>
        <source>Authentication in progress. Please wait...</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་བཞིན་ཡོད་པས་ཅུང་ཙམ་སྒུགས་དང་།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="58"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="59"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ།</translation>
    </message>
</context>
<context>
    <name>SmbClientHelper</name>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="175"/>
        <source>Cannot connect to %1!</source>
        <translation>བརྒྱ་ཆ་1འཐུད་ཐབས་མེད།</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="180"/>
        <location filename="../device_manager/smbclient_helper.cpp" line="187"/>
        <source>Wrong user name or password!</source>
        <translation>ནོར་འཁྲུལ་གྱི་སྤྱོད་མཁན་གྱི་མིང་ངམ་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="192"/>
        <source>The samba server shared printer list is empty</source>
        <translation>sambཞབས་ཞུའི་ཡོ་བྱད་མཉམ་སྤྱོད་ཀྱིས་པར་འདེབས་འཕྲུལ་ཆས་ཀྱི་རེའུ་མིག་དེ་སྟོང་པར་བརྩི་བ།</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="36"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
</context>
<context>
    <name>TroubleshootingDialog</name>
    <message>
        <location filename="../ui/troubleshooting_dialog.ui" line="14"/>
        <source>TroubleshootingDialog</source>
        <translation>TroubleshootingDialog</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="20"/>
        <source>Troubleshooting</source>
        <translation>བར་ཆད་བྱུང་རིགས་གྲལ་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="21"/>
        <source>Accept</source>
        <translation>གཏན་འཁེལ་བྱ།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="23"/>
        <source>Repair</source>
        <translation>ཉམས་གསོ་མཐེབ་བཀྱག་ཅིག</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="72"/>
        <source>Check whether the CUPS service is enabled</source>
        <translation>CUPSཞབས་ཞུ་བྱེད་འགོ་ཚུགས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="82"/>
        <source>CUPS service is enabled</source>
        <translation>CUPSཞབས་ཞུ་འགོ་ཚུགས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="85"/>
        <source>CUPS service is not enabled</source>
        <translation>CUPSཞབས་ཞུ་འགོ་ཚུགས་མེད།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="99"/>
        <source>Check CUPS user access rights</source>
        <translation>CUPSལ་སྤྱོད་མཁན་གྱི་འཚམས་འདྲིའི་དབང་ཚད་ལ་ཞིབ་བཤེར་བྱས་</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="110"/>
        <source>CUPS users can access normally</source>
        <translation>CUPSབེད་སྤྱོད་མཁན་གྱིས་རྒྱུན་ལྡན་གྱི་འཚམས་འདྲི་བྱས་ཆོག།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="113"/>
        <source>CUPS user restricted access rights</source>
        <translation>CUPSལ་སྤྱོད་མཁན་གྱི་འཚམས་འདྲིའི་དབང་ཚད་ལ་ཚོད་འཛིན་ཐེབས།།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="126"/>
        <source>Check whether the printer connection is normal</source>
        <translation>བསྐྱར་དཔར་འཕྲུལ་ཆས་རྒྱུན་ལྡན་ཡིན་མིན་ལ་ཞིབ་བཤེར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="145"/>
        <source>Printer connected</source>
        <translation>བསྐྱར་དཔར་འཕྲུལ་ཆས་འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="148"/>
        <source>Printer disconnected</source>
        <translation>པར་རྒྱག་འཕྲུལ་ཆས་ཀྱིས་འབྲེལ་མཐུད་བྱེད་མཚམས་ཆད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="178"/>
        <source>Check whether the printer protocol port configuration is enabled</source>
        <translation>བསྐྱར་དཔར་འཕྲུལ་ཆས་ཀྱི་གྲོས་མཐུན་བཀོད་སྒྲིག་བྱེད་འགོ་ཚུགས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="186"/>
        <source>Printer protocol port configuration is enabled</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་ཀྱི་གྲོས་མཐུན་བཀོད་སྒྲིག་བྱེད་འགོ་ཚུགས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="189"/>
        <source>Printer protocol port is not configured to be enabled</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་ཀྱི་གྲོས་མཐུན་བཀོད་སྒྲིག་བྱེད་འགོ་ཚུགས་མེད།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།ནོར་འཁྲུལ་</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Automatic repair failed and manual troubleshooting is required.</source>
        <translation>རང་འགུལ་གྱིས་ཉམས་གསོ་བྱས་ནས་ཕམ་ཉེས་བྱུང་ན་ལག་པ་འགུལ་ནས་སྐྱོན་ཆ་སེལ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="200"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ།</translation>
    </message>
</context>
<context>
    <name>TroubleshootingInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/troubleshooting_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1499"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="342"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1501"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="344"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1503"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="438"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ནང་དུ་&quot;/\&quot;#&quot;དང་འཕྲིན་ཡིག་0ལྷག་ཙམ་ཡོད་པ་དེ་འཕྲིན་ཡིག་128ལས་ཉུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="455"/>
        <source>Exist Same Name Printer!</source>
        <translation>མིང་གཅིག་མཚུངས་ཡིན་པའི་དཔར་ཆས་ཡོད་</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="458"/>
        <source>Printer name is not case sensitive!</source>
        <translation>དཔར་ཆས་ཀྱི་མིང་ལ་དཔེ་མཚོན་གྱི་ཚོར་བ་རྣོན་པོ་མེད།</translation>
    </message>
</context>
<context>
    <name>WaitingDialog</name>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="32"/>
        <source>Widget</source>
        <translation>ཚོད་འཛིན་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="87"/>
        <source>TextLabel</source>
        <translation>དཔེ་དེབ་ཀྱི་འཆིང་རྒྱ།</translation>
    </message>
</context>
</TS>
