<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printer-backend</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤᠨ ᠠᠷᠤ ᠳᠠᠪᠴᠠᠩ ᠳᠤ ᠬᠢ ᠠᠬᠢᠴᠠ</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="136"/>
        <source>Add Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="148"/>
        <source>Auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="149"/>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>Manual</source>
        <translation>ᠭᠠᠷ ᠢᠶᠠᠷ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="161"/>
        <source>Device List</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>Protocol</source>
        <translation>ᠭᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="193"/>
        <source>Address</source>
        <translation>ᠬᠠᠶ᠋ᠢᠭ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="199"/>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <source>ipp</source>
        <translation type="vanished">IPP</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="213"/>
        <source>name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="214"/>
        <source>location</source>
        <translation>ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="215"/>
        <source>driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="321"/>
        <source>Success</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="322"/>
        <source>Fail</source>
        <translation>ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="220"/>
        <source>forward</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠨᠡᠮᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ: ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠨᠡᠮᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="304"/>
        <location filename="../ui/add_printer_window.cpp" line="314"/>
        <location filename="../ui/add_printer_window.cpp" line="333"/>
        <location filename="../ui/add_printer_window.cpp" line="433"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <location filename="../ui/add_printer_window.cpp" line="561"/>
        <location filename="../ui/add_printer_window.cpp" line="603"/>
        <location filename="../ui/add_printer_window.cpp" line="648"/>
        <location filename="../ui/add_printer_window.cpp" line="687"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Hint</source>
        <translation>ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>ᠨᠡᠮᠡᠵᠤ ᠴᠢᠳᠠᠪᠠ ᠂ ᠳᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠤᠤ？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="313"/>
        <source>Print test page</source>
        <translation>ᠳᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <source>Is the test page printed successfully?</source>
        <translation>ᠲᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢ ᠫᠷᠢᠨᠲᠸ᠋ᠷᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>ᠲᠤᠷᠰᠢᠵᠤ ᠫᠷᠢᠨᠲᠸ᠋ᠷᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠂ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠦᠨ ᠳᠦᠰᠦᠯ ᠢ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Change Driver</source>
        <translation type="vanished">ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠵᠠᠰᠠᠵᠤ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="604"/>
        <location filename="../ui/add_printer_window.cpp" line="626"/>
        <location filename="../ui/add_printer_window.cpp" line="688"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="362"/>
        <source>Searching printers...</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠶᠠᠭ ᠬᠠᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Can not find this Printer!</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ!</translation>
    </message>
    <message>
        <source>Searching printer driver...</source>
        <translation type="vanished">ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤᠨ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠶᠠᠭ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Install driver package automatically failed,continue?</source>
        <translation type="vanished">ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠲᠤ᠌ ᠨᠠᠷᠢᠯᠢᠭ ᠤᠨᠤᠪᠴᠢᠳᠠᠢ ᠠᠪᠴᠠᠯᠳᠤᠭᠰᠠᠨ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ᠂ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠲᠤ᠌ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ᠋ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠤᠭᠰᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="432"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <location filename="../ui/add_printer_window.cpp" line="622"/>
        <location filename="../ui/add_printer_window.cpp" line="645"/>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <location filename="../ui/add_printer_window.cpp" line="747"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <source>Automatic driver configuration failed, please select the driver manually.</source>
        <translation>ᠠᠦᠢᠲ᠋ᠣ᠋ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠭᠠᠷ ᠢᠶᠠᠷ ᠢᠶᠠᠨ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠶᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="578"/>
        <source>Searching for driver...</source>
        <translation>ᠶᠠᠭ ᠡᠷᠢᠵᠦ ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠤᠯᠭᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠁</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <source>Failed to search for driver. Do you want to use local driver?</source>
        <translation>ᠡᠷᠢᠬᠦ ᠪᠡᠷ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠨᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="620"/>
        <source>It is detected that the driver comes from the Internet. Do you want to continue the installation?</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷᠲᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠨᠢ ᠢᠨᠲ᠋ᠸᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠠ ᠡᠴᠡ ᠢᠷᠡᠯᠲᠡ ᠲᠠᠢ ᠂ ᠦᠷᠭᠦᠯᠵᠢᠯᠠᠨ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠤᠤ ソ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="625"/>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="639"/>
        <source>The device driver is being adapted.</source>
        <translation>ᠲᠤᠰ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠪᠡᠷ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠳᠤ ᠲᠣᠬᠢᠷᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="642"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ ᠤ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠦᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠠᠴᠠ ᠪᠣᠯᠵᠤ ᠂ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠦᠢᠯᠡᠳᠬᠦ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠴᠢᠨ ᠲᠠᠶ ᠬᠠᠷᠢᠯᠴᠠᠵᠤ ᠲᠤᠰ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠣᠯᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="659"/>
        <source>Installing printer driver...</source>
        <translation>ᠶᠠᠭ ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠤᠭᠰᠠᠷᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ᠂ ᠵᠢᠭᠠᠬᠠᠨ ᠠᠷᠤ ᠁</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <source>Driver installation failed. Do you want to use local driver?</source>
        <translation>ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠨᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="749"/>
        <source>Close</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation>addPrinterWindow</translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/auto_search_result_model.cpp" line="49"/>
        <location filename="../ui/auto_search_result_model.cpp" line="51"/>
        <source>network</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>BaseInfo</name>
    <message>
        <location filename="../common/base_info.cpp" line="64"/>
        <source>Printer</source>
        <translation>ᠬᠡᠪᠯᠡᠬᠦ ᠮᠠᠱᠢᠨ</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>ᠱᠤᠰᠢᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="137"/>
        <source>Choose PPD</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠬᠦᠮᠦᠷᠭᠡ ᠡᠴᠡ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>ᠳᠤᠰ ᠭᠠᠵᠠᠷ ᠲᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>deb ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠶᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠫᠷᠦᠭᠷᠡᠮ ᠢ ᠶᠠᠭ ᠬᠠᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠶᠢ ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠂ ᠳᠠᠬᠢᠵᠤ ᠲᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="75"/>
        <source>Common configuration</source>
        <translation>ᠨᠡᠶᠢᠳᠡᠮ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="150"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>ᠠᠩᠬᠠᠨ ᠤ ᠬᠠᠶ᠋ᠢᠭ:api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="180"/>
        <source>Advanced Features</source>
        <translation>ᠳᠡᠭᠡᠳᠦ ᠲᠠᠰ ᠤᠨ ᠴᠢᠳᠠᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="38"/>
        <source>Ok</source>
        <translation>ᠨᠣᠲ᠋ᠠᠯᠠᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="30"/>
        <source>Configure</source>
        <translatorcomment>配置</translatorcomment>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="31"/>
        <source>Service address:</source>
        <translatorcomment>服务器地址：</translatorcomment>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠦᠨ ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="32"/>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="34"/>
        <source>Enable</source>
        <translation>ᠠᠰᠢᠭᠯᠠᠯᠲᠠ ᠳᠤ ᠣᠷᠣᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="33"/>
        <source>This printer can be used to convert print files to PDF format</source>
        <translation>ᠲᠤᠰ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ PDF ᠬᠡᠯᠪᠡᠷᠢ ᠪᠣᠯᠭᠠᠨ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="35"/>
        <source>This printer can be used to convert print files to BRF format</source>
        <translation>ᠲᠤᠰ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ BRF ᠬᠡᠯᠪᠡᠷᠢ ᠪᠣᠯᠭᠠᠨ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="36"/>
        <source>Displays all serial printers</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠰᠥᠯᠪᠡᠭᠡᠯᠵᠢᠨ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Config init failed</source>
        <translatorcomment>设置初始化失败！</translatorcomment>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="53"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translatorcomment>保留调试信息用于故障排查</translatorcomment>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠦᠯᠡᠳᠡᠬᠡᠵᠤ ᠬᠡᠮᠳᠦᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>CustomAboutDialog</name>
    <message>
        <location filename="../ui/custom_ui/custom_about_dialog.cpp" line="12"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠄ </translation>
    </message>
</context>
<context>
    <name>CustomDoublelineCheckBox</name>
    <message>
        <location filename="../ui/custom_ui/custom_doubleline_checkbox.ui" line="14"/>
        <source>Form</source>
        <translation>ui ᠭᠠᠷᠴᠠᠭ ᠃</translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤᠨ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠵᠠᠰᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ: ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <location filename="../ui/device_list_button.cpp" line="32"/>
        <source>Idle</source>
        <translation>ᠰᠡᠯᠡᠭᠦᠦ ᠰᠡᠯᠡᠭᠦᠦ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="35"/>
        <source>Printing</source>
        <translation>ᠶᠠᠭᠠᠷᠠᠤ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="38"/>
        <source>Stopped</source>
        <translation>ᠵᠣᠭᠰᠣᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="41"/>
        <source>Unknow</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="62"/>
        <location filename="../ui/device_list_button.cpp" line="264"/>
        <location filename="../ui/device_list_button.cpp" line="296"/>
        <source>Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="133"/>
        <location filename="../ui/device_list_button.cpp" line="163"/>
        <source>Set Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="135"/>
        <location filename="../ui/device_list_button.cpp" line="166"/>
        <source>Enabled</source>
        <translatorcomment>启用</translatorcomment>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="139"/>
        <location filename="../ui/device_list_button.cpp" line="169"/>
        <source>Shared</source>
        <translatorcomment>共享</translatorcomment>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../backend/device_map.cpp" line="292"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="294"/>
        <source>Printers</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="296"/>
        <source>plug-in:</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="298"/>
        <source>unplugged:</source>
        <translation>ᠰᠤᠭᠤᠯᠵᠤ ᠠᠪᠬᠤ:</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="96"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation>&quot;+&quot; ᠳᠠᠷᠤᠭᠤᠯ ᠢ᠋ ᠳᠠᠷᠤᠵᠤ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ᠋ ᠨᠡᠮᠡᠭᠡᠷᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ </translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>ᠡᠬᠦᠷᠬᠡ:</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠪᠠ！</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ！</translation>
    </message>
</context>
<context>
    <name>InkInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/ink_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ui ᠭᠠᠷᠴᠠᠭ ᠃</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="50"/>
        <source>Test Page</source>
        <translation>ᠲᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="64"/>
        <source>unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="70"/>
        <source>Printing page %1</source>
        <translation>ᠶᠠᠭ 1 ᠳᠤᠭᠠᠷ ᠨᠢᠭᠤᠷ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="53"/>
        <source>untitled</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="261"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="412"/>
        <source>Cancel print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="415"/>
        <source>Delete print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="418"/>
        <source>Hold print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠲᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="421"/>
        <source>Release print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="424"/>
        <source>Reprint</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="427"/>
        <source>Job properties</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <source>Set error: Job status has been updated!</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠪᠤᠷᠤᠭᠤ: ᠡᠩᠨᠡᠭᠡ ᠶᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="447"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="453"/>
        <source>Sure</source>
        <translation>ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Cannot move job to itself!</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠲᠤ᠌ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="108"/>
        <source>Cancel print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="110"/>
        <source>Delete print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="112"/>
        <source>Hold print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠲᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="114"/>
        <source>Release print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="116"/>
        <source>Reprint</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="118"/>
        <source>Use other printer...</source>
        <translation>ᠪᠤᠰᠤᠳ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ...</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="120"/>
        <source>Job properties</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠃</translation>
    </message>
</context>
<context>
    <name>JobPropertiesWindow</name>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="15"/>
        <source>Job properties</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="35"/>
        <source>name</source>
        <translation>ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="36"/>
        <source>value</source>
        <translation>ᠦᠨ᠎ᠡ ᠨᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>LaunchPrinter</name>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>Is the test page printed successfully?</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢᠨ ᠨᠢᠭᠤᠷ ᠢ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠨᠢ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠤᠭᠰᠠᠨ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>%1 print result confirmation</source>
        <translation>1 ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="94"/>
        <source>Yes</source>
        <translation>ᠲᠡᠢ᠌ᠮᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="96"/>
        <source>No</source>
        <translation>ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="47"/>
        <source>Device List</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="216"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="362"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="212"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ%1 ᠵᠢ/ ᠢ᠋ ᠯᠠᠪᠳᠠᠢ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="217"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="360"/>
        <source>Set Default Failed!</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="364"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="45"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="47"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="71"/>
        <source>Property</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="78"/>
        <source>Job List</source>
        <translation>ᠫᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ ᠡᠩᠨᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="85"/>
        <source>PrintTest</source>
        <translation>ᠳᠤᠷᠰᠢᠨ ᠫᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="229"/>
        <source>Are you sure to rename &quot;%1&quot; ?</source>
        <translation>ᠲᠠ % 1&quot;᠎ᠶ᠋ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ᠎ᠪᠡᠷ ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="233"/>
        <source>Rename</source>
        <translation>ᠬᠦᠨᠳᠦ ᠨᠡᠷᠡᠶᠢᠳᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="190"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="192"/>
        <source>Idle</source>
        <translation>ᠰᠤᠯᠠ ᠰᠡᠯᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="90"/>
        <source>Troubleshooting</source>
        <translation>ᠭᠡᠮ ᠢ ᠵᠢᠭᠰᠠᠭᠠᠨ ᠪᠠᠶᠢᠴᠠᠭᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="194"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="196"/>
        <source>Printing</source>
        <translation>ᠶᠠᠭᠠᠷᠠᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="198"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="200"/>
        <source>Stopped</source>
        <translation>ᠵᠤᠭᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="205"/>
        <source>Unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="230"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="231"/>
        <source>This action will delete the job queue too!</source>
        <translation>ᠡᠨᠡᠬᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠡᠩᠨᠡᠭᠡ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="234"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="91"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="225"/>
        <source>Delete Failed!</source>
        <translation>ᠬᠠᠰᠤᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="227"/>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="229"/>
        <location filename="../ui/mainwindow.cpp" line="269"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠨᠡᠮᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="333"/>
        <source>Sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Error</source>
        <translation>ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Try to connect the Printer...</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠪᠡᠷ ᠤᠷᠤᠯᠳᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠨᠡᠮᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="37"/>
        <location filename="../ui/menumodule.cpp" line="69"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="39"/>
        <location filename="../ui/menumodule.cpp" line="66"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="41"/>
        <location filename="../ui/menumodule.cpp" line="72"/>
        <source>Configure</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="43"/>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="52"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation>ᠫᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ ᠳᠤᠷᠰᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ᠋ ᠭᠠᠷ ᠵᠢᠡᠷ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ......</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation>ᠤᠭᠰᠠᠷᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation>ᠫᠷᠢᠨᠲᠸᠷ ᠢ᠋ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠪᠠ:</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠳᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠪᠤᠯᠤᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation>ᠦᠬᠡᠢ/ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation>ᠳᠡᠢᠮᠤ/ ᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation>ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>ᠲᠤᠷᠰᠢᠵᠤ ᠫᠷᠢᠨᠲᠸᠷᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠤ᠋ᠨ ᠳᠦᠰᠦᠯ ᠢ᠋ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠫᠠᠷᠢᠨᠲᠸᠷ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠨᠡᠮᠡᠬᠡᠳ ᠳᠠᠬᠢᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠦᠵᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation>ᠨᠣᠲ᠋ᠠᠯᠠᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>PopWindowManager</name>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Do you want to add a printer?</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠨᠡᠮᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>The printer was detected:%1</source>
        <translation>ᠪᠠᠶᠢᠴᠠᠭᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠡᠴᠡ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠯᠲᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="167"/>
        <source>The device driver is being adapted.</source>
        <translation>ᠲᠤᠰ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠪᠡᠷ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠳᠤ ᠲᠣᠬᠢᠷᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="170"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ ᠤ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠦᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠠᠴᠠ ᠪᠣᠯᠵᠤ ᠂ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠦᠢᠯᠡᠳᠬᠦ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠴᠢᠨ ᠲᠠᠶ ᠬᠠᠷᠢᠯᠴᠠᠵᠤ ᠲᠤᠰ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠣᠯᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="172"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="223"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="237"/>
        <source>%1 installation failed</source>
        <translation>1 ᠤᠭᠰᠠᠷᠠᠵᠤ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>%1 successful installation</source>
        <translation>1 ᠤᠭᠰᠠᠷᠠᠵᠤ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠪᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="222"/>
        <source>No printer driver for %1.</source>
        <translation>1 ᠶᠢᠨ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠢᠶᠠᠷ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠪᠠᠶᠢᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>Print test page</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢᠨ ᠨᠢᠭᠤᠷ ᠢ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="11"/>
        <source>Refresh</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation>propertyListWindow</translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="139"/>
        <source>Model:</source>
        <translation>ᠨᠤᠮᠸᠷ:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="140"/>
        <source>Status:</source>
        <translation>ᠪᠠᠢᠳᠠᠯ ᠳᠦᠯᠦᠪ:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="141"/>
        <source>Location:</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="142"/>
        <source>Driver:</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ:</translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="396"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="412"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="423"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="23"/>
        <source>base</source>
        <translation>ᠰᠠᠭᠤᠷᠢ ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="36"/>
        <source>Ink</source>
        <translation>ᠪᠤᠳᠤᠭᠲᠤ ᠤᠰᠤ ᠪᠤᠳᠤᠭ ᠤᠨ ᠨᠤᠨᠲᠠᠭ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>advanced</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="58"/>
        <source>PrinterProperty</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠤ᠋ᠨ ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="95"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="163"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="327"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="96"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="166"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="331"/>
        <source>Location</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="97"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="170"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="335"/>
        <source>Status</source>
        <translation>ᠳᠦᠯᠦᠪ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="98"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="190"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="339"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="380"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="101"/>
        <source>Modify</source>
        <translation>ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="103"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="193"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="343"/>
        <source>URI</source>
        <translation>uri</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="127"/>
        <source> (level: %1%)</source>
        <translation> ( ᠦᠯᠡᠳᠡᠪᠦᠷᠢ ᠶᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠄ 1% )</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="172"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="174"/>
        <source>Idle</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠰᠡᠯᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="176"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="178"/>
        <source>Printing</source>
        <translation>ᠶᠠᠭᠠᠷᠠᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="180"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="182"/>
        <source>Stopped</source>
        <translation>ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <source>Error happened, all options restored!.</source>
        <translation>ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ᠂ ᠪᠦᠬᠦ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ!.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="232"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="269"/>
        <source>Conflict with &quot;%1&quot;</source>
        <translation>《1》 ᠯᠦᠭᠡ ᠮᠥᠷᠭᠥᠯᠳᠦᠭᠡᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="395"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳ᠋ᠤ᠌ &apos;/\&apos;&quot;?#&apos; ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ 128 ᠠᠪᠢᠶ᠎ᠠ ᠡᠴᠡ ᠴᠦᠬᠡᠨ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="398"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="414"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="425"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="411"/>
        <source>Exist Same Name Printer!</source>
        <translation>ᠠᠳᠠᠯᠢ ᠨᠡᠷ᠎ᠡ ᠲᠠᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="422"/>
        <source>Printer Name Illegal!</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="187"/>
        <source>Unknow</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Idle</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠰᠡᠯᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Printing</source>
        <translation>ᠶᠠᠭᠠᠷᠠᠤ</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Stopped</source>
        <translation>ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="226"/>
        <source>Unknow</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="32"/>
        <source>id</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="33"/>
        <source>user</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="34"/>
        <source>title</source>
        <translation>ᠲᠸᠺᠰᠲ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="35"/>
        <source>printer name</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="36"/>
        <source>size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="37"/>
        <source>create time</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="38"/>
        <source>job state</source>
        <translation>ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠪᠠᠢᠳᠠᠯ ᠳᠦᠯᠦᠪ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="44"/>
        <source>Job is waiting to be printed</source>
        <translation>ᠡᠩᠨᠡᠬᠡᠨ ᠳ᠋ᠤ᠌</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="45"/>
        <source>Job is held for printing</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ ᠵᠢ ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="46"/>
        <source>Job is currently printing</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="47"/>
        <source>Job has been stopped</source>
        <translation>ᠡᠬᠦᠷᠬᠡ ᠵᠤᠭᠰᠤᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="48"/>
        <source>Job has been canceled</source>
        <translation>ᠡᠬᠦᠷᠬᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="49"/>
        <source>Job has aborted due to error</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="50"/>
        <source>Job has completed successfully</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠤ᠋ᠨ ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢᠨ ᠴᠤᠩᠬᠤ ᠵᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation>ᠨᠣᠲ᠋ᠠᠯᠠᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="23"/>
        <source>Select Driver</source>
        <translation>ᠬᠥᠳᠡᠯᠭᠡᠬᠦ ᠬᠦᠴᠦ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="56"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="85"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="266"/>
        <source>driver</source>
        <translation>ᠬᠥᠳᠡᠯᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="57"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="71"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="232"/>
        <source>vendor</source>
        <translation>ᠨᠡᠷᠡᠲᠦ ᠲᠡᠮᠳᠡᠭᠲᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="195"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ᠋ ᠤᠩᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="319"/>
        <source>Please Choose Model And Vender!</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢ ᠪᠤᠯᠤᠨ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="320"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="322"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation>ui ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="203"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="247"/>
        <source>Apply</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>SmbAuthInfoDialog</name>
    <message>
        <location filename="../ui/smb_auth_info_dialog.ui" line="26"/>
        <source>SmbAuthInfoDialog</source>
        <translation>SmbAuthInfoDialog</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="13"/>
        <source>Authentication</source>
        <translation>ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠭᠠᠷᠤᠯ ᠢ ᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="14"/>
        <source>Authentication is required to connect to the printer &quot;%1&quot;</source>
        <translation>《 1 》 ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠢ ᠬᠣᠯᠪᠣᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠳᠤ ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠬᠢᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="15"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="16"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="17"/>
        <source>Workgroup</source>
        <translation>ᠣᠷᠣᠨ ᠪᠦᠰᠡ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="18"/>
        <source>Accept</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="19"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="37"/>
        <source>Authentication in progress. Please wait...</source>
        <translation>ᠶᠠᠭ ᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠂ ᠵᠢᠭᠠᠬᠠᠨ ᠬᠦᠯᠢᠶᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="58"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭ᠍ᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="59"/>
        <source>Confirm</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>SmbClientHelper</name>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="175"/>
        <source>Cannot connect to %1!</source>
        <translation>ᠬᠣᠯᠪᠣᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="180"/>
        <location filename="../device_manager/smbclient_helper.cpp" line="187"/>
        <source>Wrong user name or password!</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ !</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="192"/>
        <source>The samba server shared printer list is empty</source>
        <translation>sammba ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠦᠨ ᠬᠠᠮᠲᠤᠪᠠᠷ ᠬᠦᠷᠲᠡᠬᠦ ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠤ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠃</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="36"/>
        <source>Printer</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ</translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TroubleshootingDialog</name>
    <message>
        <location filename="../ui/troubleshooting_dialog.ui" line="14"/>
        <source>TroubleshootingDialog</source>
        <translation>TroubleshootingDialog</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="20"/>
        <source>Troubleshooting</source>
        <translation>ᠭᠡᠮ ᠢ ᠵᠢᠭᠰᠠᠭᠠᠨ ᠪᠠᠶᠢᠴᠠᠭᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="21"/>
        <source>Accept</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="23"/>
        <source>Repair</source>
        <translation>ᠨᠢᠭᠡ ᠲᠠᠷᠤᠭᠤᠯ ᠢ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="72"/>
        <source>Check whether the CUPS service is enabled</source>
        <translation>CUPS ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠨᠡᠭᠡᠭᠡᠭᠰᠡᠨ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="82"/>
        <source>CUPS service is enabled</source>
        <translation>CUPS ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠡᠬᠢᠯᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="85"/>
        <source>CUPS service is not enabled</source>
        <translation>CUPS ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠨᠡᠭᠡᠭᠡᠭᠰᠡᠨ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="99"/>
        <source>Check CUPS user access rights</source>
        <translation>CUPS ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="110"/>
        <source>CUPS users can access normally</source>
        <translation>CUPS ᠬᠡᠪ ᠦᠨ ᠶᠣᠰᠣᠭᠠᠷ ᠠᠶᠢᠯᠴᠢᠯᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="113"/>
        <source>CUPS user restricted access rights</source>
        <translation>CUPS ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠭᠳᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="126"/>
        <source>Check whether the printer connection is normal</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠬᠡᠪ ᠦᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠲᠠᠢ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠢ᠌ᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="145"/>
        <source>Printer connected</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="148"/>
        <source>Printer disconnected</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="178"/>
        <source>Check whether the printer protocol port configuration is enabled</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠤ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠦᠨ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠡᠬᠢᠯᠡᠭᠰᠡᠨ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="186"/>
        <source>Printer protocol port configuration is enabled</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠤ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠦᠨ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠨᠢᠭᠡᠨᠲᠡ ᠡᠬᠢᠯᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="189"/>
        <source>Printer protocol port is not configured to be enabled</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠤ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠦᠨ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠡᠬᠢᠯᠡᠭᠡᠳᠦᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Automatic repair failed and manual troubleshooting is required.</source>
        <translation>ᠠᠦᠢᠲ᠋ᠣ᠋ ᠵᠠᠰᠠᠨ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠᠵᠤ ᠢᠯᠠᠭᠳᠠᠬᠤ ᠳᠤ ᠭᠠᠷ ᠢᠶᠠᠷ ᠢᠶᠠᠨ ᠭᠡᠮ ᠢ ᠠᠷᠢᠯᠭᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="200"/>
        <source>Confirm</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>TroubleshootingInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/troubleshooting_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ui ᠭᠠᠷᠴᠠᠭ ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1499"/>
        <source>Idle</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠰᠡᠯᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="342"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1501"/>
        <source>Printing</source>
        <translation>ᠶᠠᠭᠠᠷᠠᠤ</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="344"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1503"/>
        <source>Stopped</source>
        <translation>ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="438"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳ᠋ᠤ᠌ &apos;/\&apos;&quot;?#&apos; ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ 128 ᠠᠪᠢᠶ᠎ᠠ ᠡᠴᠡ ᠴᠦᠬᠡᠨ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="455"/>
        <source>Exist Same Name Printer!</source>
        <translation>ᠠᠳᠠᠯᠢ ᠨᠡᠷ᠎ᠡ ᠲᠠᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="458"/>
        <source>Printer name is not case sensitive!</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠢᠴᠢᠯᠬᠡ ᠬᠤᠪᠢᠶᠠᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>WaitingDialog</name>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="32"/>
        <source>Widget</source>
        <translation>ᠵᠢᠵᠢᠭ ᠲᠤᠨᠤᠭᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="87"/>
        <source>TextLabel</source>
        <translation>ᠲᠧᠺᠰᠲ ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
</context>
</TS>
