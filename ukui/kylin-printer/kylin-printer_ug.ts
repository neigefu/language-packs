<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printer-backend</source>
        <translation>پرىنتېرلاش-ئارقا قوشۇمچىسى</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation>AddPrinterWindow</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="136"/>
        <source>Add Printer</source>
        <translation>پرىنتېرلاش خەت چوڭ-كىچىكلىكىنى قوشۇش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="148"/>
        <source>Auto</source>
        <translation>ئاپتۇماتىك</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="149"/>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>Manual</source>
        <translation>قوللانما</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="161"/>
        <source>Device List</source>
        <translation>ئۈسكۈنە تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>Protocol</source>
        <translation>كېلىشىم</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="193"/>
        <source>Address</source>
        <translation>ئادرېس</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="199"/>
        <source>Search</source>
        <translation>ئىزدە</translation>
    </message>
    <message>
        <source>socket</source>
        <translation type="vanished">socket</translation>
    </message>
    <message>
        <source>ipp</source>
        <translation type="vanished">ipp</translation>
    </message>
    <message>
        <source>http</source>
        <translation type="vanished">http</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="213"/>
        <source>name</source>
        <translation>ئىسىم</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="214"/>
        <source>location</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="215"/>
        <source>driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="220"/>
        <source>forward</source>
        <translation>ئالدىغا</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>پرىنتېرنى قوشۇش مەغلۇپ بولدى: PPD تاللانمىدى!</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>پرىنتېرنى قوشۇۋالسىڭىز مەغلۇپ بولىدۇ، بىر مەزگىل ئۆتكەندىن كېيىن قايتا قايتا قىلىڭ.</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="304"/>
        <location filename="../ui/add_printer_window.cpp" line="314"/>
        <location filename="../ui/add_printer_window.cpp" line="333"/>
        <location filename="../ui/add_printer_window.cpp" line="433"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <location filename="../ui/add_printer_window.cpp" line="561"/>
        <location filename="../ui/add_printer_window.cpp" line="603"/>
        <location filename="../ui/add_printer_window.cpp" line="648"/>
        <location filename="../ui/add_printer_window.cpp" line="687"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Hint</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>پرىنتېرنى مۇۋەپپەقىيەتلىك قوشىمىز،پرىنتېردا سىناق بېتى بارمۇ؟</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="313"/>
        <source>Print test page</source>
        <translation>بېسىپ چىقىرىش سىنىقى بېتى</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="321"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="322"/>
        <source>Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <source>Automatic driver configuration failed, please select the driver manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="578"/>
        <source>Searching for driver...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <source>Failed to search for driver. Do you want to use local driver?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="620"/>
        <source>It is detected that the driver comes from the Internet. Do you want to continue the installation?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="625"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="639"/>
        <source>The device driver is being adapted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="642"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="659"/>
        <source>Installing printer driver...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <source>Driver installation failed. Do you want to use local driver?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <source>Is the test page printed successfully?</source>
        <translation>سىناق بېتى مۇۋەپپەقىيەتلىك بېسىلىپ بولدىمۇ؟</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>سىناق بېسىپ چىقىرىش مەغلۇپ بولدى. پرىنتېر شوپۇرىنى ئالماشتۇرامسىز؟</translation>
    </message>
    <message>
        <source>Change Driver</source>
        <translation type="vanished">قوزغاچىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="604"/>
        <location filename="../ui/add_printer_window.cpp" line="626"/>
        <location filename="../ui/add_printer_window.cpp" line="688"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="362"/>
        <source>Searching printers...</source>
        <translation>پرىنتېرلار ئاختۇرۇۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Can not find this Printer!</source>
        <translation>بۇ پرىنتېرنى تاپالمىدىم!</translation>
    </message>
    <message>
        <source>Searching printer driver...</source>
        <translation type="vanished">پرىنتېر شوپۇرى ئاختۇرۇۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="432"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <location filename="../ui/add_printer_window.cpp" line="622"/>
        <location filename="../ui/add_printer_window.cpp" line="645"/>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <location filename="../ui/add_printer_window.cpp" line="747"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <source>Install driver package automatically failed,continue?</source>
        <translation type="vanished">شوپۇرلۇق بوغچىسىنى قاچىلاش ئاپتوماتىك مەغلۇپ بولدى، داۋاملاشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="749"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/auto_search_result_model.cpp" line="49"/>
        <location filename="../ui/auto_search_result_model.cpp" line="51"/>
        <source>network</source>
        <translation>تور</translation>
    </message>
</context>
<context>
    <name>BaseInfo</name>
    <message>
        <location filename="../common/base_info.cpp" line="64"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="137"/>
        <source>Choose PPD</source>
        <translation>PPD نى تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>PPD ئامبىرىدىن تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>يەرلىك PPD قوشۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>Deb يۈرۈشلۈكىنى تاللاڭ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>Deb ھۆججىتى(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>ئىزدەۋاتقان شوپۇر...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>قاچىلاش بوغچىسى مەغلۇپ بولدى، قايتا قايتا قاچىلاڭ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="75"/>
        <source>Common configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="150"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>初始址:api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="180"/>
        <source>Advanced Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="30"/>
        <source>Configure</source>
        <translation>سەپلىمە</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="31"/>
        <source>Service address:</source>
        <translation>مۇلازىمەت ئادرېسى:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="32"/>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="34"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="33"/>
        <source>This printer can be used to convert print files to PDF format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="35"/>
        <source>This printer can be used to convert print files to BRF format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="36"/>
        <source>Displays all serial printers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="38"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Config init failed</source>
        <translation>سەپلىمە init مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="53"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translation>چاتاق يوقلاش ئۈچۈن دېففېكت ئۇچۇرىنى ساقلاپ قېلىش</translation>
    </message>
</context>
<context>
    <name>CustomAboutDialog</name>
    <message>
        <location filename="../ui/custom_ui/custom_about_dialog.cpp" line="12"/>
        <source>Version: </source>
        <translation type="unfinished">نەشرى: </translation>
    </message>
</context>
<context>
    <name>CustomDoublelineCheckBox</name>
    <message>
        <location filename="../ui/custom_ui/custom_doubleline_checkbox.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">جەدۋەل</translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>پرىنتېر قوزغاغۇچنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation>PPD نى ئۆزگەرتىش مەغلۇپ بولدى: PPD تاللانمىدى!</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <location filename="../ui/device_list_button.cpp" line="32"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="35"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="38"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="41"/>
        <source>Unknow</source>
        <translation>ئۇقۇشمىغانلار</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="62"/>
        <location filename="../ui/device_list_button.cpp" line="264"/>
        <location filename="../ui/device_list_button.cpp" line="296"/>
        <source>Default</source>
        <translation>كۆڭۈلدىكى سۆز</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="133"/>
        <location filename="../ui/device_list_button.cpp" line="163"/>
        <source>Set Default</source>
        <translation>كۆڭۈلدىكىنى بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="135"/>
        <location filename="../ui/device_list_button.cpp" line="166"/>
        <source>Enabled</source>
        <translation>قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="139"/>
        <location filename="../ui/device_list_button.cpp" line="169"/>
        <source>Shared</source>
        <translation>ئورتاق بەھرىلىنىش</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../backend/device_map.cpp" line="292"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="294"/>
        <source>Printers</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="296"/>
        <source>plug-in:</source>
        <translation>قىستۇرما:</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="298"/>
        <source>unplugged:</source>
        <translation>توك مەنبەسىدىن ئايرىلدى:</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="96"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation>پرىنتېر قوشۇش ئۈچۈن «+» كونۇپكىسىنى چېكىڭ.</translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>پرىنتېرلاش </translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>خىزمەت ئورنى:</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>يارىتىلغان!</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>تاماملاندى!</translation>
    </message>
</context>
<context>
    <name>InkInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/ink_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">جەدۋەل</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="50"/>
        <source>Test Page</source>
        <translation>سىنا بېتى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="53"/>
        <source>untitled</source>
        <translation>خەتسىز</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="64"/>
        <source>unknown</source>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="70"/>
        <source>Printing page %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="261"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="412"/>
        <source>Cancel print</source>
        <translation>بېسىپ چىقىرىشنى بىكار قىلىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="415"/>
        <source>Delete print</source>
        <translation>بېسىپ چىقىرىشنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="418"/>
        <source>Hold print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="421"/>
        <source>Release print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="424"/>
        <source>Reprint</source>
        <translation>قايتا قاز</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="427"/>
        <source>Job properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <source>Set error: Job status has been updated!</source>
        <translation>بەلگىلەش خاتالىقى: خىزمەت ھالىتى يېڭىلاندى!</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="447"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="453"/>
        <source>Sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Cannot move job to itself!</source>
        <translation>خىزمەتنى ئۆزىگە يۆتكىيەلمەيدۇ!</translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="108"/>
        <source>Cancel print</source>
        <translation>بېسىپ چىقىرىشنى بىكار قىلىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="110"/>
        <source>Delete print</source>
        <translation>بېسىپ چىقىرىشنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="112"/>
        <source>Hold print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="114"/>
        <source>Release print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="116"/>
        <source>Reprint</source>
        <translation type="unfinished">قايتا قاز</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="118"/>
        <source>Use other printer...</source>
        <translation>باشقا پرىنتېرنى ئىشلىتىڭ...</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="120"/>
        <source>Job properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JobPropertiesWindow</name>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="15"/>
        <source>Job properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="35"/>
        <source>name</source>
        <translation type="unfinished">ئىسىم</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="36"/>
        <source>value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LaunchPrinter</name>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>Is the test page printed successfully?</source>
        <translation type="unfinished">سىناق بېتى مۇۋەپپەقىيەتلىك بېسىلىپ بولدىمۇ؟</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>%1 print result confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="94"/>
        <source>Yes</source>
        <translation type="unfinished">شۇنداق</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="96"/>
        <source>No</source>
        <translation type="unfinished">ياق</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="47"/>
        <source>Device List</source>
        <translation>ئۈسكۈنە تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="212"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>«٪1»نى چوقۇم ئۆچۈرەمسىز؟</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="216"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="217"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="360"/>
        <source>Set Default Failed!</source>
        <translation>كۆڭۈلدىكىنى بەلگىلەش مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="362"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="364"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="45"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="47"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="71"/>
        <source>Property</source>
        <translation>مال-مۈلۈك</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="78"/>
        <source>Job List</source>
        <translation>خىزمەت تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="85"/>
        <source>PrintTest</source>
        <translation>PrintTest</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="90"/>
        <source>Troubleshooting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="190"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="192"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="194"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="196"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="198"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="200"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="205"/>
        <source>Unknown</source>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="229"/>
        <source>Are you sure to rename &quot;%1&quot; ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="233"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="234"/>
        <source>Cancel</source>
        <translation type="unfinished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="230"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="231"/>
        <source>This action will delete the job queue too!</source>
        <translation>بۇ ھەرىكەتمۇ خىزمەت قاتارىنى ئۆچۈرىۋېتىدۇ!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="91"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="225"/>
        <source>Delete Failed!</source>
        <translation>ئۆچۈرۈش مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="227"/>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="229"/>
        <location filename="../ui/mainwindow.cpp" line="269"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>پرىنتېرنى قوشۇۋالسىڭىز مەغلۇپ بولىدۇ، بىر مەزگىل ئۆتكەندىن كېيىن قايتا قايتا قىلىڭ.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Try to connect the Printer...</source>
        <translation>پرىنتېرنى ئۇلاشقا تىرىشىڭ...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation>پرىنتېرنى ئېچىش مەغلۇپ بولدى، Del دىن خەت باسقۇچىنى قوشۇۋېلىڭ، ئاندىن قايتا سىناپ بېقىڭ!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="333"/>
        <source>Sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="37"/>
        <location filename="../ui/menumodule.cpp" line="69"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="39"/>
        <location filename="../ui/menumodule.cpp" line="66"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="41"/>
        <location filename="../ui/menumodule.cpp" line="72"/>
        <source>Configure</source>
        <translation>سەپلىمە</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="43"/>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <source>Quit</source>
        <translation>چېكىنىش</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="52"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation>بېسىپ چىقىرىش سىناق بېتى</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation>ئۈسكۈنىنى كۆرۈش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation>قولدا قاچىلاش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation>قاچىلاش ......</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation>مۇۋەپپىقىيەتلىك قاچىلاش!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation>قاچىلاش مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation>پرىنتېر بايقالدى:</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation>سىناق بېتى مۇۋەپپەقىيەتلىك بېسىلىپ بولدىمۇ؟</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>سىناق بېسىپ چىقىرىش مەغلۇپ بولدى. پرىنتېر شوپۇرىنى ئالماشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation>قوزغاچىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation>باشلانمىدى، پرىنتېرنى يەنە بىر قېتىم قوشۇشنى سىناپ بېقىڭ!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
</context>
<context>
    <name>PopWindowManager</name>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Do you want to add a printer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>The printer was detected:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="167"/>
        <source>The device driver is being adapted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="170"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="172"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="223"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="237"/>
        <source>%1 installation failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>%1 successful installation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="222"/>
        <source>No printer driver for %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>Print test page</source>
        <translation type="unfinished">بېسىپ چىقىرىش سىنىقى بېتى</translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation>PropertyListWindow</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="11"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="139"/>
        <source>Model:</source>
        <translation>مودېل:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="140"/>
        <source>Status:</source>
        <translation>ئەھۋالى:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="141"/>
        <source>Location:</source>
        <translation>ئورنى:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="142"/>
        <source>Driver:</source>
        <translation>شوپۇر:</translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="23"/>
        <source>base</source>
        <translation>بازا</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="36"/>
        <source>Ink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>advanced</source>
        <translation>ئىلغار</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="58"/>
        <source>PrinterProperty</source>
        <translation>پرىنتېرلاشProperty</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="95"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="163"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="327"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="96"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="166"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="331"/>
        <source>Location</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="97"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="170"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="335"/>
        <source>Status</source>
        <translation>ھالەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="98"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="190"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="339"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="380"/>
        <source>Driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="101"/>
        <source>Modify</source>
        <translation>ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="103"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="193"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="343"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="127"/>
        <source> (level: %1%)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="172"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="174"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="176"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="178"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="180"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="182"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="187"/>
        <source>Unknow</source>
        <translation>ئۇقۇشمىغانلار</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="396"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="412"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="423"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <source>Error happened, all options restored!.</source>
        <translation>خاتالىق يۈز بەردى، بارلىق تاللاشلار ئەسلىگە كەلدى!.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="232"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="269"/>
        <source>Conflict with &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="395"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>پرىنتېر نامى &apos;/\&apos;?#&apos;, 0 دىن ئارتۇق ھەرىپى، 128 ھەرپتىن كىچىك بولالمايدۇ !</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="398"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="414"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="425"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="411"/>
        <source>Exist Same Name Printer!</source>
        <translation>مەۋجۇتلۇق ئوخشاش ئىسىمدىكى پرىنتېرلاش ماشىنىسى!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="422"/>
        <source>Printer Name Illegal!</source>
        <translation>پرىنتېرلاش نامى قانۇنسىز!</translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="226"/>
        <source>Unknow</source>
        <translation>ئۇقۇشمىغانلار</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="32"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="33"/>
        <source>user</source>
        <translation>ئىشلەتكۈچى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="34"/>
        <source>title</source>
        <translation>تېما</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="35"/>
        <source>printer name</source>
        <translation>پرىنتېرلاش نامى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="36"/>
        <source>size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="37"/>
        <source>create time</source>
        <translation>ۋاقىت يارىتىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="38"/>
        <source>job state</source>
        <translation>خىزمەت ھالىتى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="44"/>
        <source>Job is waiting to be printed</source>
        <translation>خىزمەت بىسىلىشنى كۈتمەكتە</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="45"/>
        <source>Job is held for printing</source>
        <translation>باسمىچىلىق خىزمىتى ئېلىپ بېرىلىدۇ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="46"/>
        <source>Job is currently printing</source>
        <translation>خىزمەت ھازىر باستۇرىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="47"/>
        <source>Job has been stopped</source>
        <translation>خىزمەت توختىتىلدى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="48"/>
        <source>Job has been canceled</source>
        <translation>خىزمەت ئەمەلدىن قالدۇرۇلدى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="49"/>
        <source>Job has aborted due to error</source>
        <translation>خاتالىق سەۋەبىدىن خىزمەتتىن توختىدى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="50"/>
        <source>Job has completed successfully</source>
        <translation>خىزمەت مۇۋەپپەقىيەتلىك تاماملاندى</translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation>RenamePrinterDialog</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation>پرىنتېرلاش نامى</translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation>SelectPpdDialog</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="203"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="247"/>
        <source>Apply</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="23"/>
        <source>Select Driver</source>
        <translation>شوپۇر تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="56"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="85"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="266"/>
        <source>driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="57"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="71"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="232"/>
        <source>vendor</source>
        <translation>تىخنىكچى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="195"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation>كۆرۈش شوپۇرلار،كۈتۈڭ...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="319"/>
        <source>Please Choose Model And Vender!</source>
        <translation>Model And Vender نى تاللاڭ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="320"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="322"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>SmbAuthInfoDialog</name>
    <message>
        <location filename="../ui/smb_auth_info_dialog.ui" line="26"/>
        <source>SmbAuthInfoDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="13"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="14"/>
        <source>Authentication is required to connect to the printer &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="15"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="16"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="17"/>
        <source>Workgroup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="18"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="19"/>
        <source>Cancel</source>
        <translation type="unfinished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="37"/>
        <source>Authentication in progress. Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="58"/>
        <source>Warning</source>
        <translation type="unfinished">دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="59"/>
        <source>Confirm</source>
        <translation type="unfinished">جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>SmbClientHelper</name>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="175"/>
        <source>Cannot connect to %1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="180"/>
        <location filename="../device_manager/smbclient_helper.cpp" line="187"/>
        <source>Wrong user name or password!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="192"/>
        <source>The samba server shared printer list is empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation>پرىنتېرلاش ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="36"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation>ئەڭ چوڭ چەككە</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation>چېكىنىش</translation>
    </message>
</context>
<context>
    <name>TroubleshootingDialog</name>
    <message>
        <location filename="../ui/troubleshooting_dialog.ui" line="14"/>
        <source>TroubleshootingDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="20"/>
        <source>Troubleshooting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="21"/>
        <source>Accept</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation type="unfinished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="23"/>
        <source>Repair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="72"/>
        <source>Check whether the CUPS service is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="82"/>
        <source>CUPS service is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="85"/>
        <source>CUPS service is not enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="99"/>
        <source>Check CUPS user access rights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="110"/>
        <source>CUPS users can access normally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="113"/>
        <source>CUPS user restricted access rights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="126"/>
        <source>Check whether the printer connection is normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="145"/>
        <source>Printer connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="148"/>
        <source>Printer disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="178"/>
        <source>Check whether the printer protocol port configuration is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="186"/>
        <source>Printer protocol port configuration is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="189"/>
        <source>Printer protocol port is not configured to be enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Error</source>
        <translation type="unfinished">خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Automatic repair failed and manual troubleshooting is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="200"/>
        <source>Confirm</source>
        <translation type="unfinished">جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>TroubleshootingInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/troubleshooting_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">جەدۋەل</translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1499"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="342"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1501"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="344"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1503"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="438"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>پرىنتېر نامى &apos;/\&apos;?#&apos;, 0 دىن ئارتۇق ھەرىپى، 128 ھەرپتىن كىچىك بولالمايدۇ !</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="455"/>
        <source>Exist Same Name Printer!</source>
        <translation>مەۋجۇتلۇق ئوخشاش ئىسىمدىكى پرىنتېرلاش ماشىنىسى!</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="458"/>
        <source>Printer name is not case sensitive!</source>
        <translation>پرىنتېرلاش نامى دىلرابا سەزگۈر ئەمەس!</translation>
    </message>
</context>
<context>
    <name>WaitingDialog</name>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="32"/>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="87"/>
        <source>TextLabel</source>
        <translation type="unfinished">TextLabel</translation>
    </message>
</context>
</TS>
