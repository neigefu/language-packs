<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printer-backend</source>
        <translation>印表機後台進程</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="136"/>
        <source>Add Printer</source>
        <translation>添加印表機</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="148"/>
        <source>Auto</source>
        <translation>自動查找</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="149"/>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>Manual</source>
        <translation>手動添加</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="161"/>
        <source>Device List</source>
        <translation>設備清單</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>Protocol</source>
        <translation>協定</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="193"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="199"/>
        <source>Search</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="213"/>
        <source>name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="214"/>
        <source>location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="215"/>
        <source>driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="321"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="322"/>
        <source>Fail</source>
        <translation>失敗</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="220"/>
        <source>forward</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>添加印表機失敗：沒有選擇驅動！</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>添加印表機失敗，請重試！</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="304"/>
        <location filename="../ui/add_printer_window.cpp" line="314"/>
        <location filename="../ui/add_printer_window.cpp" line="333"/>
        <location filename="../ui/add_printer_window.cpp" line="433"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <location filename="../ui/add_printer_window.cpp" line="561"/>
        <location filename="../ui/add_printer_window.cpp" line="603"/>
        <location filename="../ui/add_printer_window.cpp" line="648"/>
        <location filename="../ui/add_printer_window.cpp" line="687"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Hint</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>印表機安裝成功！</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="313"/>
        <source>Print test page</source>
        <translation>列印測試頁</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <source>Is the test page printed successfully?</source>
        <translation>列印測試是否成功？</translation>
    </message>
    <message>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation type="vanished">列印測試失敗，建議修改驅動后重試。</translation>
    </message>
    <message>
        <source>Change Driver</source>
        <translation type="vanished">修改驅動</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="604"/>
        <location filename="../ui/add_printer_window.cpp" line="626"/>
        <location filename="../ui/add_printer_window.cpp" line="688"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="362"/>
        <source>Searching printers...</source>
        <translation>正在搜索印表機......</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>印表測試失敗，請檢查印表機是否連接或修改驅動後重試。</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Can not find this Printer!</source>
        <translation>無法找到印表機</translation>
    </message>
    <message>
        <source>Searching printer driver...</source>
        <translation type="vanished">安裝印表機驅動......</translation>
    </message>
    <message>
        <source>Install driver package automatically failed,continue?</source>
        <translation type="vanished">伺服器無精準匹配驅動包，是否繼續本地驅動安裝？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="432"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <location filename="../ui/add_printer_window.cpp" line="622"/>
        <location filename="../ui/add_printer_window.cpp" line="645"/>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <location filename="../ui/add_printer_window.cpp" line="747"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <source>Automatic driver configuration failed, please select the driver manually.</source>
        <translation>自動配置驅動失敗，請手動選擇驅動。</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="578"/>
        <source>Searching for driver...</source>
        <translation>搜尋驅動...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <source>Failed to search for driver. Do you want to use local driver?</source>
        <translation>搜索驅動失敗，是否使用本地驅動？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="620"/>
        <source>It is detected that the driver comes from the Internet. Do you want to continue the installation?</source>
        <translation>檢測到驅動程式來源於互聯網，是否繼續安裝？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="625"/>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="639"/>
        <source>The device driver is being adapted.</source>
        <translation>該設備型號驅動適配中。</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="642"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>由於商業協定限制，請聯繫設備製造商以獲取該設備型號驅動程式。</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="659"/>
        <source>Installing printer driver...</source>
        <translation>安裝驅動，請稍後...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <source>Driver installation failed. Do you want to use local driver?</source>
        <translation>安裝驅動失敗，是否使用本地驅動？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="749"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/auto_search_result_model.cpp" line="49"/>
        <location filename="../ui/auto_search_result_model.cpp" line="51"/>
        <source>network</source>
        <translation>網路</translation>
    </message>
</context>
<context>
    <name>BaseInfo</name>
    <message>
        <location filename="../common/base_info.cpp" line="64"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>ui標籤</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="137"/>
        <source>Choose PPD</source>
        <translation>選擇驅動</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>從驅動庫中選擇</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>本地添加</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>請選擇deb驅動包</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>安裝套件（*.deb）</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>搜尋驅動程式...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>安裝驅動包失敗，請重試！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="75"/>
        <source>Common configuration</source>
        <translation>通用配置</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="150"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>初始位址：api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="180"/>
        <source>Advanced Features</source>
        <translation>高級功能</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="38"/>
        <source>Ok</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="30"/>
        <source>Configure</source>
        <translatorcomment>配置</translatorcomment>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="31"/>
        <source>Service address:</source>
        <translatorcomment>服务器地址：</translatorcomment>
        <translation>伺服器位址：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="32"/>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="34"/>
        <source>Enable</source>
        <translation>啟用</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="33"/>
        <source>This printer can be used to convert print files to PDF format</source>
        <translation>可使用該印表機將列印檔案轉換為 PDF 格式</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="35"/>
        <source>This printer can be used to convert print files to BRF format</source>
        <translation>可使用該印表機將列印檔轉換為 BRF 格式</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="36"/>
        <source>Displays all serial printers</source>
        <translation>顯示所有串口印表機</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Config init failed</source>
        <translatorcomment>设置初始化失败！</translatorcomment>
        <translation>設置初始化失敗！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="53"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translatorcomment>保留调试信息用于故障排查</translatorcomment>
        <translation>保留調試資訊用於故障排查</translation>
    </message>
</context>
<context>
    <name>CustomAboutDialog</name>
    <message>
        <location filename="../ui/custom_ui/custom_about_dialog.cpp" line="12"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
</context>
<context>
    <name>CustomDoublelineCheckBox</name>
    <message>
        <location filename="../ui/custom_ui/custom_doubleline_checkbox.ui" line="14"/>
        <source>Form</source>
        <translation>ui標題</translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>ui標題</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>修改印表機驅動</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation>修改驅動檔失敗：沒有選擇驅動檔</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <location filename="../ui/device_list_button.cpp" line="32"/>
        <source>Idle</source>
        <translation>空閒</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="35"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="38"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="41"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="62"/>
        <location filename="../ui/device_list_button.cpp" line="264"/>
        <location filename="../ui/device_list_button.cpp" line="296"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="133"/>
        <location filename="../ui/device_list_button.cpp" line="163"/>
        <source>Set Default</source>
        <translation>設為預設</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="135"/>
        <location filename="../ui/device_list_button.cpp" line="166"/>
        <source>Enabled</source>
        <translatorcomment>启用</translatorcomment>
        <translation>啟用</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="139"/>
        <location filename="../ui/device_list_button.cpp" line="169"/>
        <source>Shared</source>
        <translatorcomment>共享</translatorcomment>
        <translation>共用</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../backend/device_map.cpp" line="292"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="294"/>
        <source>Printers</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="296"/>
        <source>plug-in:</source>
        <translation>插入：</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="298"/>
        <source>unplugged:</source>
        <translation>拔出：</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="96"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation>請點擊按鈕“+”添加一個印表機。</translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>印表機 </translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>工作：</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>創建！</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>完成！</translation>
    </message>
</context>
<context>
    <name>InkInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/ink_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ui標題</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="50"/>
        <source>Test Page</source>
        <translation>測試頁</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="64"/>
        <source>unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="70"/>
        <source>Printing page %1</source>
        <translation>正在列印第%1頁</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="53"/>
        <source>untitled</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="261"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="412"/>
        <source>Cancel print</source>
        <translation>取消列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="415"/>
        <source>Delete print</source>
        <translation>刪除列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="418"/>
        <source>Hold print</source>
        <translation>暫停列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="421"/>
        <source>Release print</source>
        <translation>恢復列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="424"/>
        <source>Reprint</source>
        <translation>重新列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="427"/>
        <source>Job properties</source>
        <translation>任務屬性</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <source>Set error: Job status has been updated!</source>
        <translation>設置錯誤：佇列已更新！</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="447"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="453"/>
        <source>Sure</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Cannot move job to itself!</source>
        <translation>無法將任務移動到印表機本身！</translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="108"/>
        <source>Cancel print</source>
        <translation>取消列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="110"/>
        <source>Delete print</source>
        <translation>刪除列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="112"/>
        <source>Hold print</source>
        <translation>暫停列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="114"/>
        <source>Release print</source>
        <translation>恢復列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="116"/>
        <source>Reprint</source>
        <translation>重新列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="118"/>
        <source>Use other printer...</source>
        <translation>使用其他印表機...</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="120"/>
        <source>Job properties</source>
        <translation>任務屬性</translation>
    </message>
</context>
<context>
    <name>JobPropertiesWindow</name>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="15"/>
        <source>Job properties</source>
        <translation>任務屬性</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="35"/>
        <source>name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="36"/>
        <source>value</source>
        <translation>值</translation>
    </message>
</context>
<context>
    <name>LaunchPrinter</name>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>Is the test page printed successfully?</source>
        <translation>列印測試頁是否成功？</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>%1 print result confirmation</source>
        <translation>%1 列印結果確認</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="94"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="96"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="47"/>
        <source>Device List</source>
        <translation>設備清單</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="216"/>
        <source>Delete</source>
        <translation>拿掉</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="362"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="212"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>您確定要移除印表機%1 嗎？</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="217"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="360"/>
        <source>Set Default Failed!</source>
        <translation>設為默認失敗！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="364"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="45"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="47"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="71"/>
        <source>Property</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="78"/>
        <source>Job List</source>
        <translation>列印佇列</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="85"/>
        <source>PrintTest</source>
        <translation>列印測試</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="229"/>
        <source>Are you sure to rename &quot;%1&quot; ?</source>
        <translation>確定要重命名 「%1」 嗎？</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="233"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="190"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="192"/>
        <source>Idle</source>
        <translation>空閒</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="90"/>
        <source>Troubleshooting</source>
        <translation>故障排查</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="194"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="196"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="198"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="200"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="205"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="230"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="231"/>
        <source>This action will delete the job queue too!</source>
        <translation>重命名會清空該設備的列印佇列內容。</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="234"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation>ui標題</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="91"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="225"/>
        <source>Delete Failed!</source>
        <translation>刪除失敗！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="227"/>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="229"/>
        <location filename="../ui/mainwindow.cpp" line="269"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>添加印表機失敗，請重試！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="333"/>
        <source>Sure</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Try to connect the Printer...</source>
        <translation>正在嘗試連接印表機......</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation>啟動印表機失敗，請重新添加印表機！</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="37"/>
        <location filename="../ui/menumodule.cpp" line="69"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="39"/>
        <location filename="../ui/menumodule.cpp" line="66"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="41"/>
        <location filename="../ui/menumodule.cpp" line="72"/>
        <source>Configure</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="43"/>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="52"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation>ui標題</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation>列印測試</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation>查看設備</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation>手動安裝驅動</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation>安裝...</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation>安裝成功！</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation>安裝失敗！</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation>偵測到印表機：</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation>列印測試頁是否成功？</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation type="vanished">測試列印失敗，是否更改驅動方案？</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation>修改驅動</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>印表測試失敗，請檢查印表機是否連接或修改驅動後重試。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation>印表機啟動失敗，嘗試重新添加印表機再次打開！</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>PopWindowManager</name>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Do you want to add a printer?</source>
        <translation>是否添加印表機？</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>The printer was detected:%1</source>
        <translation>檢測到印表機：%1</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="167"/>
        <source>The device driver is being adapted.</source>
        <translation>該設備型號驅動適配中。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="170"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>由於商業協定限制，請聯繫設備製造商以獲取該設備型號驅動程式。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="172"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="223"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="237"/>
        <source>%1 installation failed</source>
        <translation>%1 安裝失敗</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>%1 successful installation</source>
        <translation>%1 安裝成功</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="222"/>
        <source>No printer driver for %1.</source>
        <translation>沒有 %1 的印表機驅動程式。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>Print test page</source>
        <translation>列印測試頁</translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="11"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="139"/>
        <source>Model:</source>
        <translation>型號：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="140"/>
        <source>Status:</source>
        <translation>狀態：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="141"/>
        <source>Location:</source>
        <translation>位置：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="142"/>
        <source>Driver:</source>
        <translation>驅動：</translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="396"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="412"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="423"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="23"/>
        <source>base</source>
        <translation>基礎屬性</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="36"/>
        <source>Ink</source>
        <translation>墨水/墨粉</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>advanced</source>
        <translation>高級選項</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="58"/>
        <source>PrinterProperty</source>
        <translation>印表機屬性</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="95"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="163"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="327"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="96"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="166"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="331"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="97"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="170"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="335"/>
        <source>Status</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="98"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="190"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="339"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="380"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="101"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="103"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="193"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="343"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="127"/>
        <source> (level: %1%)</source>
        <translation> （餘量： %1%）</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="172"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="174"/>
        <source>Idle</source>
        <translation>空閒</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="176"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="178"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="180"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="182"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <source>Error happened, all options restored!.</source>
        <translation>錯誤發生，所有選項重置！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="232"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="269"/>
        <source>Conflict with &quot;%1&quot;</source>
        <translation>與%1 設定衝突</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="395"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>印表機名稱不能包含『%1』，而且不能為空，且少於128個字母！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="398"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="414"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="425"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="411"/>
        <source>Exist Same Name Printer!</source>
        <translation>存在同名印表機！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="422"/>
        <source>Printer Name Illegal!</source>
        <translation>印表機名稱不合法！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="187"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Idle</source>
        <translation>空閒</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="226"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="32"/>
        <source>id</source>
        <translation>任務</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="33"/>
        <source>user</source>
        <translation>使用者</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="34"/>
        <source>title</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="35"/>
        <source>printer name</source>
        <translation>印表機名稱</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="36"/>
        <source>size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="37"/>
        <source>create time</source>
        <translation>創建時間</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="38"/>
        <source>job state</source>
        <translation>工作狀態</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="44"/>
        <source>Job is waiting to be printed</source>
        <translation>佇列中</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="45"/>
        <source>Job is held for printing</source>
        <translation>暫停列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="46"/>
        <source>Job is currently printing</source>
        <translation>正在列印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="47"/>
        <source>Job has been stopped</source>
        <translation>任務被停止</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="48"/>
        <source>Job has been canceled</source>
        <translation>任務取消</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="49"/>
        <source>Job has aborted due to error</source>
        <translation>任務錯誤</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="50"/>
        <source>Job has completed successfully</source>
        <translation>列印完成</translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation>重新命名印表機對話框</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation>印表機名稱</translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="23"/>
        <source>Select Driver</source>
        <translation>選擇驅動</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="56"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="85"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="266"/>
        <source>driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="57"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="71"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="232"/>
        <source>vendor</source>
        <translation>品牌</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="195"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation>讀取驅動檔，請稍後...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="319"/>
        <source>Please Choose Model And Vender!</source>
        <translation>請選擇廠商和型號！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="320"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="322"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation>ui標題</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="203"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="247"/>
        <source>Apply</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>SmbAuthInfoDialog</name>
    <message>
        <location filename="../ui/smb_auth_info_dialog.ui" line="26"/>
        <source>SmbAuthInfoDialog</source>
        <translation>SmbAuthInfoDialog</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="13"/>
        <source>Authentication</source>
        <translation>身份認證</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="14"/>
        <source>Authentication is required to connect to the printer &quot;%1&quot;</source>
        <translation>連接訪問「%1」 印表機需要認證</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="15"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="16"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="17"/>
        <source>Workgroup</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="18"/>
        <source>Accept</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="19"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="37"/>
        <source>Authentication in progress. Please wait...</source>
        <translation>正在認證，請稍等</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="58"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="59"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>SmbClientHelper</name>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="175"/>
        <source>Cannot connect to %1!</source>
        <translation>無法連線到 %1！</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="180"/>
        <location filename="../device_manager/smbclient_helper.cpp" line="187"/>
        <source>Wrong user name or password!</source>
        <translation>錯誤的使用者名或密碼！</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="192"/>
        <source>The samba server shared printer list is empty</source>
        <translation>samba伺服器共用印表機清單為空</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation>印表機資訊</translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="36"/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TroubleshootingDialog</name>
    <message>
        <location filename="../ui/troubleshooting_dialog.ui" line="14"/>
        <source>TroubleshootingDialog</source>
        <translation>TroubleshootingDialog</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="20"/>
        <source>Troubleshooting</source>
        <translation>故障排查</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="21"/>
        <source>Accept</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="23"/>
        <source>Repair</source>
        <translation>一鍵修復</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="72"/>
        <source>Check whether the CUPS service is enabled</source>
        <translation>檢查 CUPS 服務是否開啟</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="82"/>
        <source>CUPS service is enabled</source>
        <translation>CUPS 服務已開啟</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="85"/>
        <source>CUPS service is not enabled</source>
        <translation>CUPS 服務未開啟</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="99"/>
        <source>Check CUPS user access rights</source>
        <translation>檢查 CUPS 使用者存取許可權</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="110"/>
        <source>CUPS users can access normally</source>
        <translation>CUPS 用戶可正常訪問</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="113"/>
        <source>CUPS user restricted access rights</source>
        <translation>CUPS 使用者訪問許可權受限</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="126"/>
        <source>Check whether the printer connection is normal</source>
        <translation>檢查印表機連接是否正常</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="145"/>
        <source>Printer connected</source>
        <translation>印表機已連接</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="148"/>
        <source>Printer disconnected</source>
        <translation>印表機已斷開連接</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="178"/>
        <source>Check whether the printer protocol port configuration is enabled</source>
        <translation>檢查印表機協定埠配置是否開啟</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="186"/>
        <source>Printer protocol port configuration is enabled</source>
        <translation>印表機協定埠配置已開啟</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="189"/>
        <source>Printer protocol port is not configured to be enabled</source>
        <translation>印表機協定埠配置未開啟</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Automatic repair failed and manual troubleshooting is required.</source>
        <translation>自動修復失敗，需要手動排除故障。</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="200"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>TroubleshootingInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/troubleshooting_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ui標題</translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1499"/>
        <source>Idle</source>
        <translation>空閒</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="342"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1501"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="344"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1503"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="438"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>印表機名稱不能包含『%1』，而且不能為空，且少於128個字母！</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="455"/>
        <source>Exist Same Name Printer!</source>
        <translation>存在同名印表機！</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="458"/>
        <source>Printer name is not case sensitive!</source>
        <translation>印表機名稱不區分大小寫！</translation>
    </message>
</context>
<context>
    <name>WaitingDialog</name>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="32"/>
        <source>Widget</source>
        <translation>控件</translation>
    </message>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="87"/>
        <source>TextLabel</source>
        <translation>文字框</translation>
    </message>
</context>
</TS>
