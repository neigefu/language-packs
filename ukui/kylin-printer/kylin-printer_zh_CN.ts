<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printer-backend</source>
        <translation>打印机后台进程</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="136"/>
        <source>Add Printer</source>
        <translation>添加打印机</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="148"/>
        <source>Auto</source>
        <translation>自动查找</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="149"/>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>Manual</source>
        <translation>手动添加</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="161"/>
        <source>Device List</source>
        <translation>设备列表</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>Protocol</source>
        <translation>协议</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="193"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="199"/>
        <source>Search</source>
        <translation>查找</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="213"/>
        <source>name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="214"/>
        <source>location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="215"/>
        <source>driver</source>
        <translation>驱动</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="321"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="322"/>
        <source>Fail</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="578"/>
        <source>Searching for driver...</source>
        <translation>正在搜索驱动...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <source>Failed to search for driver. Do you want to use local driver?</source>
        <translation>搜索驱动失败，是否使用本地驱动?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="220"/>
        <source>forward</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="250"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>添加打印机失败：没有选择驱动！</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="301"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>添加打印机失败，请重试!</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="304"/>
        <location filename="../ui/add_printer_window.cpp" line="314"/>
        <location filename="../ui/add_printer_window.cpp" line="333"/>
        <location filename="../ui/add_printer_window.cpp" line="433"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <location filename="../ui/add_printer_window.cpp" line="561"/>
        <location filename="../ui/add_printer_window.cpp" line="603"/>
        <location filename="../ui/add_printer_window.cpp" line="648"/>
        <location filename="../ui/add_printer_window.cpp" line="687"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Hint</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="311"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>打印机安装成功！</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="313"/>
        <source>Print test page</source>
        <translation>打印测试页</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="319"/>
        <source>Is the test page printed successfully?</source>
        <translation>打印测试是否成功？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>打印测试失败，请检查打印机是否连接或修改驱动后重试。</translation>
    </message>
    <message>
        <source>Change Driver</source>
        <translation type="vanished">修改驱动</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="604"/>
        <location filename="../ui/add_printer_window.cpp" line="626"/>
        <location filename="../ui/add_printer_window.cpp" line="688"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="362"/>
        <source>Searching printers...</source>
        <translation>正在搜索打印机...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Can not find this Printer!</source>
        <translation>无法找到打印机</translation>
    </message>
    <message>
        <source>Searching printer driver...</source>
        <translation type="vanished">正在安装打印机驱动...</translation>
    </message>
    <message>
        <source>Install driver package automatically failed,continue?</source>
        <translation type="vanished">服务器无精准匹配驱动包，是否继续本地驱动安装?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="432"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <location filename="../ui/add_printer_window.cpp" line="601"/>
        <location filename="../ui/add_printer_window.cpp" line="622"/>
        <location filename="../ui/add_printer_window.cpp" line="645"/>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <location filename="../ui/add_printer_window.cpp" line="747"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="559"/>
        <source>Automatic driver configuration failed, please select the driver manually.</source>
        <translation>自动配置驱动失败，请手动选择驱动。</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="620"/>
        <source>It is detected that the driver comes from the Internet. Do you want to continue the installation?</source>
        <translation>检测到驱动程序来源于互联网，是否继续安装？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="625"/>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="639"/>
        <source>The device driver is being adapted.</source>
        <translation>该设备型号驱动适配中。</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="642"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>由于商业协议限制，请联系设备制造商以获取该设备型号驱动程序。</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="659"/>
        <source>Installing printer driver...</source>
        <translation>正在安装驱动，请稍后...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="685"/>
        <source>Driver installation failed. Do you want to use local driver?</source>
        <translation>安装驱动失败，是否使用本地驱动?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="749"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/auto_search_result_model.cpp" line="49"/>
        <location filename="../ui/auto_search_result_model.cpp" line="51"/>
        <source>network</source>
        <translation>网络</translation>
    </message>
</context>
<context>
    <name>BaseInfo</name>
    <message>
        <location filename="../common/base_info.cpp" line="64"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>ui标签</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="137"/>
        <source>Choose PPD</source>
        <translation>选择驱动</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>从驱动库中选择</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>本地添加</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>请选择deb驱动包</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>安装包(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>正在搜索驱动程序...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>安装驱动包失败，请重试！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="75"/>
        <source>Common configuration</source>
        <translation>通用配置</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="150"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>初始地址:api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="180"/>
        <source>Advanced Features</source>
        <translation>高级功能</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="38"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="30"/>
        <source>Configure</source>
        <translatorcomment>配置</translatorcomment>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="31"/>
        <source>Service address:</source>
        <translatorcomment>服务器地址：</translatorcomment>
        <translation>服务器地址：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="32"/>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="34"/>
        <source>Enable</source>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="33"/>
        <source>This printer can be used to convert print files to PDF format</source>
        <translation>可使用该打印机将打印文件转换为 PDF 格式</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="35"/>
        <source>This printer can be used to convert print files to BRF format</source>
        <translation>可使用该打印机将打印文件转换为 BRF 格式</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="36"/>
        <source>Displays all serial printers</source>
        <translation>显示所有串口打印机</translation>
    </message>
    <message>
        <source>Show all protocols</source>
        <translation type="vanished">显示所有协议</translation>
    </message>
    <message>
        <source>Show virtual protocols</source>
        <translation type="vanished">显示虚拟打印机</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="50"/>
        <source>Config init failed</source>
        <translatorcomment>设置初始化失败！</translatorcomment>
        <translation>设置初始化失败！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="53"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translatorcomment>保留调试信息用于故障排查</translatorcomment>
        <translation>保留调试信息用于故障排查</translation>
    </message>
</context>
<context>
    <name>CustomAboutDialog</name>
    <message>
        <location filename="../ui/custom_ui/custom_about_dialog.cpp" line="12"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
</context>
<context>
    <name>CustomDoublelineCheckBox</name>
    <message>
        <location filename="../ui/custom_ui/custom_doubleline_checkbox.ui" line="14"/>
        <source>Form</source>
        <translation>ui标题</translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>ui标题</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>驱动</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>修改打印机驱动</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation>修改驱动文件失败：没有选择驱动文件</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <location filename="../ui/device_list_button.cpp" line="32"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="35"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="38"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="41"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="62"/>
        <location filename="../ui/device_list_button.cpp" line="264"/>
        <location filename="../ui/device_list_button.cpp" line="296"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="133"/>
        <location filename="../ui/device_list_button.cpp" line="163"/>
        <source>Set Default</source>
        <translation>设为默认</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="135"/>
        <location filename="../ui/device_list_button.cpp" line="166"/>
        <source>Enabled</source>
        <translatorcomment>启用</translatorcomment>
        <translation>启用</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="139"/>
        <location filename="../ui/device_list_button.cpp" line="169"/>
        <source>Shared</source>
        <translatorcomment>共享</translatorcomment>
        <translation>共享</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../backend/device_map.cpp" line="292"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="294"/>
        <source>Printers</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="296"/>
        <source>plug-in:</source>
        <translation>插入：</translation>
    </message>
    <message>
        <location filename="../backend/device_map.cpp" line="298"/>
        <source>unplugged:</source>
        <translation>拔出：</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="96"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation>请点击按钮“+”添加一个打印机。</translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>任务：</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>创建！</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>完成！</translation>
    </message>
</context>
<context>
    <name>InkInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/ink_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ui标题</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="50"/>
        <source>Test Page</source>
        <translation>测试页</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="64"/>
        <source>unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="70"/>
        <source>Printing page %1</source>
        <translation>正在打印第%1页</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="53"/>
        <source>untitled</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="261"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="412"/>
        <source>Cancel print</source>
        <translation>取消打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="415"/>
        <source>Delete print</source>
        <translation>删除打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="418"/>
        <source>Hold print</source>
        <translation>暂停打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="421"/>
        <source>Release print</source>
        <translation>恢复打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="424"/>
        <source>Reprint</source>
        <translation>重新打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="427"/>
        <source>Job properties</source>
        <translation>任务属性</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="445"/>
        <source>Set error: Job status has been updated!</source>
        <translation>设置错误：队列已更新!</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="447"/>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="453"/>
        <source>Sure</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="451"/>
        <source>Cannot move job to itself!</source>
        <translation>无法将任务移动到打印机本身!</translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="108"/>
        <source>Cancel print</source>
        <translation>取消打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="110"/>
        <source>Delete print</source>
        <translation>删除打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="112"/>
        <source>Hold print</source>
        <translation>暂停打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="114"/>
        <source>Release print</source>
        <translation>恢复打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="116"/>
        <source>Reprint</source>
        <translation>重新打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="118"/>
        <source>Use other printer...</source>
        <translation>使用其他打印机...</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.cpp" line="120"/>
        <source>Job properties</source>
        <translation>任务属性</translation>
    </message>
</context>
<context>
    <name>JobPropertiesWindow</name>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="15"/>
        <source>Job properties</source>
        <translation>任务属性</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="35"/>
        <source>name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_properties_window.cpp" line="36"/>
        <source>value</source>
        <translation>值</translation>
    </message>
</context>
<context>
    <name>LaunchPrinter</name>
    <message>
        <source>Print test page</source>
        <translation type="obsolete">打印测试页</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>Is the test page printed successfully?</source>
        <translation>打印测试页是否成功？</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="92"/>
        <source>%1 print result confirmation</source>
        <translation>%1 打印结果确认</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="94"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../backend/launch_printer.cpp" line="96"/>
        <source>No</source>
        <translation>否</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="47"/>
        <source>Device List</source>
        <translation>设备列表</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="216"/>
        <source>Delete</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="362"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="212"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>您确定要移除打印机 “%1”吗?</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="217"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="360"/>
        <source>Set Default Failed!</source>
        <translation>设为默认失败！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="364"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="45"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="47"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="71"/>
        <source>Property</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="78"/>
        <source>Job List</source>
        <translation>打印队列</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="85"/>
        <source>PrintTest</source>
        <translation>打印测试</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="229"/>
        <source>Are you sure to rename &quot;%1&quot; ?</source>
        <translation>确定要重命名 “%1” 么？</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="233"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="190"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="192"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="90"/>
        <source>Troubleshooting</source>
        <translation>故障排查</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="194"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="196"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="198"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="200"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="205"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="230"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="231"/>
        <source>This action will delete the job queue too!</source>
        <translation>重命名会清空该设备的打印队列内容。</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="234"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation>ui标题</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="91"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="225"/>
        <source>Delete Failed!</source>
        <translation>删除失败！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="227"/>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="229"/>
        <location filename="../ui/mainwindow.cpp" line="269"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="267"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>添加打印机失败，请重试!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="333"/>
        <source>Sure</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Try to connect the Printer...</source>
        <translation>正在尝试连接打印机...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="331"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation>启动打印机失败，请重新添加打印机！</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="37"/>
        <location filename="../ui/menumodule.cpp" line="69"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="39"/>
        <location filename="../ui/menumodule.cpp" line="66"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="41"/>
        <location filename="../ui/menumodule.cpp" line="72"/>
        <source>Configure</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="43"/>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="52"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation>ui标题</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation>打印测试</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation>查看设备</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation>手动安装驱动</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation>正在安装...</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation>安装成功！</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation>安装失败！</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation>检测到打印机：</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation>打印测试页是否成功？</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Please check whether the printer is connected or modify the driver and try again.</source>
        <translation>打印测试失败，请检查打印机是否连接或修改驱动后重试。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation>修改驱动</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation>打印机启动失败,尝试重新添加打印机再次打开!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>PopWindowManager</name>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Do you want to add a printer?</source>
        <translation>是否添加打印机？</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>The printer was detected:%1</source>
        <translation>检测到打印机:%1</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="48"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="172"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="223"/>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="237"/>
        <source>%1 installation failed</source>
        <translation>%1 安装失败</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>%1 successful installation</source>
        <translation>%1 安装成功</translation>
    </message>
    <message>
        <source>Installing...</source>
        <translation type="vanished">正在安装...</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="167"/>
        <source>The device driver is being adapted.</source>
        <translation>该设备型号驱动适配中。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="170"/>
        <source>Due to commercial agreement restrictions, please contact the device manufacturer to obtain drivers for this device model.</source>
        <translation>由于商业协议限制，请联系设备制造商以获取该设备型号驱动程序。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="222"/>
        <source>No printer driver for %1.</source>
        <translation> 没有 %1 的打印机驱动程序。</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/pop_window_manager.cpp" line="247"/>
        <source>Print test page</source>
        <translation>打印测试页</translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="11"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="139"/>
        <source>Model:</source>
        <translation>型号：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="140"/>
        <source>Status:</source>
        <translation>状态：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="141"/>
        <source>Location:</source>
        <translation>位置：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="142"/>
        <source>Driver:</source>
        <translation>驱动：</translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="396"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="412"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="423"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="23"/>
        <source>base</source>
        <translation>基础属性</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="36"/>
        <source>Ink</source>
        <translation>墨水/墨粉</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>advanced</source>
        <translation>高级选项</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="58"/>
        <source>PrinterProperty</source>
        <translation>打印机属性</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="95"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="163"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="327"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="96"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="166"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="331"/>
        <source>Location</source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="97"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="170"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="335"/>
        <source>Status</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="98"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="190"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="339"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="380"/>
        <source>Driver</source>
        <translation>驱动</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="101"/>
        <source>Modify</source>
        <translation>修改</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="103"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="193"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="343"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="127"/>
        <source> (level: %1%)</source>
        <translation> (余量: %1%)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="172"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="174"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="176"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="178"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="180"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="182"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="230"/>
        <source>Error happened, all options restored!.</source>
        <translation>错误发生，所有选项重置！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="232"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="269"/>
        <source>Conflict with &quot;%1&quot;</source>
        <translation>与 “%1” 设置冲突</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="395"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>打印机名称不能包含‘%1’，而且不能为空，且少于128个字母！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="398"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="414"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="425"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="411"/>
        <source>Exist Same Name Printer!</source>
        <translation>存在同名打印机！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="422"/>
        <source>Printer Name Illegal!</source>
        <translation>打印机名称不合法！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="187"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="226"/>
        <source>Unknow</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="32"/>
        <source>id</source>
        <translation>任务</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="33"/>
        <source>user</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="34"/>
        <source>title</source>
        <translation>文档</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="35"/>
        <source>printer name</source>
        <translation>打印机名称</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="36"/>
        <source>size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="37"/>
        <source>create time</source>
        <translation>创建时间</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="38"/>
        <source>job state</source>
        <translation>工作状态</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="44"/>
        <source>Job is waiting to be printed</source>
        <translation>队列中</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="45"/>
        <source>Job is held for printing</source>
        <translation>暂停打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="46"/>
        <source>Job is currently printing</source>
        <translation>正在打印</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="47"/>
        <source>Job has been stopped</source>
        <translation>任务被停止</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="48"/>
        <source>Job has been canceled</source>
        <translation>任务取消</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="49"/>
        <source>Job has aborted due to error</source>
        <translation>任务错误</translation>
    </message>
    <message>
        <location filename="../ui/job_manager/job_manager_window.h" line="50"/>
        <source>Job has completed successfully</source>
        <translation>打印完成</translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation>重命名打印机对话框</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation>打印机名称</translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="23"/>
        <source>Select Driver</source>
        <translation>选择驱动</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="56"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="85"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="266"/>
        <source>driver</source>
        <translation>驱动</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="57"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="71"/>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="232"/>
        <source>vendor</source>
        <translation>品牌</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="195"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation>正在读取驱动文件，请稍后…</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="319"/>
        <source>Please Choose Model And Vender!</source>
        <translation>请选择厂商和型号！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="320"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="322"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation>ui标题</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="203"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="247"/>
        <source>Apply</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>SmbAuthInfoDialog</name>
    <message>
        <location filename="../ui/smb_auth_info_dialog.ui" line="26"/>
        <source>SmbAuthInfoDialog</source>
        <translation>SmbAuthInfoDialog</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="13"/>
        <source>Authentication</source>
        <translation>身份认证</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="14"/>
        <source>Authentication is required to connect to the printer &quot;%1&quot;</source>
        <translation>连接访问“%1”打印机需要认证</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="15"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="16"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="17"/>
        <source>Workgroup</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="18"/>
        <source>Accept</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="19"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="37"/>
        <source>Authentication in progress. Please wait...</source>
        <translation>正在认证，请稍等</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="58"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/smb_auth_info_dialog.cpp" line="59"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>SmbClientHelper</name>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="175"/>
        <source>Cannot connect to %1!</source>
        <translation>无法连接到 %1！</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="180"/>
        <location filename="../device_manager/smbclient_helper.cpp" line="187"/>
        <source>Wrong user name or password!</source>
        <translation>错误的用户名或密码！</translation>
    </message>
    <message>
        <location filename="../device_manager/smbclient_helper.cpp" line="192"/>
        <source>The samba server shared printer list is empty</source>
        <translation>samba服务器共享打印机列表为空</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation>打印机信息</translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="36"/>
        <source>Printer</source>
        <translation>打印机</translation>
    </message>
</context>
<context>
    <name>ToubleshootingDialog</name>
    <message>
        <source>Toubleshooting</source>
        <translation type="vanished">故障排查</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Repair</source>
        <translation type="vanished">一键修复</translation>
    </message>
    <message>
        <source>Check whether the CUPS service is enabled</source>
        <translation type="vanished">检查 CUPS 服务是否开启</translation>
    </message>
    <message>
        <source>CUPS service is enabled</source>
        <translation type="vanished">CUPS 服务已开启</translation>
    </message>
    <message>
        <source>CUPS service is not enabled</source>
        <translation type="vanished">CUPS 服务未开启</translation>
    </message>
    <message>
        <source>Check CUPS user access rights</source>
        <translation type="vanished">检查 CUPS 用户访问权限</translation>
    </message>
    <message>
        <source>CUPS users can access normally</source>
        <translation type="vanished">CUPS 用户可正常访问</translation>
    </message>
    <message>
        <source>CUPS user restricted access rights</source>
        <translation type="vanished">CUPS 用户访问权限受限</translation>
    </message>
    <message>
        <source>Check whether the printer connection is normal</source>
        <translation type="vanished">检查打印机连接是否正常</translation>
    </message>
    <message>
        <source>Idle</source>
        <translation type="vanished">空闲</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="vanished">忙碌</translation>
    </message>
    <message>
        <source>Printer connected</source>
        <translation type="vanished">打印机已连接</translation>
    </message>
    <message>
        <source>Printer disconnected</source>
        <translation type="vanished">打印机已断开连接</translation>
    </message>
    <message>
        <source>Check whether the printer protocol port configuration is enabled</source>
        <translation type="vanished">检查打印机协议端口配置是否开启</translation>
    </message>
    <message>
        <source>Printer protocol port configuration is enabled</source>
        <translation type="vanished">打印机协议端口配置已开启</translation>
    </message>
    <message>
        <source>Printer protocol port is not configured to be enabled</source>
        <translation type="vanished">打印机协议端口配置未开启</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Automatic repair failed and manual troubleshooting is required.</source>
        <translation type="vanished">自动修复失败，需要手动排除故障。</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">确定</translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TroubleshootingDialog</name>
    <message>
        <location filename="../ui/troubleshooting_dialog.ui" line="14"/>
        <source>TroubleshootingDialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="20"/>
        <source>Troubleshooting</source>
        <translation>故障排查</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="21"/>
        <source>Accept</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="22"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="23"/>
        <source>Repair</source>
        <translation>一键修复</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="72"/>
        <source>Check whether the CUPS service is enabled</source>
        <translation>检查 CUPS 服务是否开启</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="82"/>
        <source>CUPS service is enabled</source>
        <translation>CUPS 服务已开启</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="85"/>
        <source>CUPS service is not enabled</source>
        <translation>CUPS 服务未开启</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="99"/>
        <source>Check CUPS user access rights</source>
        <translation>检查 CUPS 用户访问权限</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="110"/>
        <source>CUPS users can access normally</source>
        <translation>CUPS 用户可正常访问</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="113"/>
        <source>CUPS user restricted access rights</source>
        <translation>CUPS 用户访问权限受限</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="126"/>
        <source>Check whether the printer connection is normal</source>
        <translation>检查打印机连接是否正常</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="145"/>
        <source>Printer connected</source>
        <translation>打印机已连接</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="148"/>
        <source>Printer disconnected</source>
        <translation>打印机已断开连接</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="178"/>
        <source>Check whether the printer protocol port configuration is enabled</source>
        <translation>检查打印机协议端口配置是否开启</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="186"/>
        <source>Printer protocol port configuration is enabled</source>
        <translation>打印机协议端口配置已开启</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="189"/>
        <source>Printer protocol port is not configured to be enabled</source>
        <translation>打印机协议端口配置未开启</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="197"/>
        <source>Automatic repair failed and manual troubleshooting is required.</source>
        <translation>自动修复失败，需要手动排除故障。</translation>
    </message>
    <message>
        <location filename="../ui/troubleshooting_dialog.cpp" line="200"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>TroubleshootingInfoWidget</name>
    <message>
        <location filename="../ui/new_property_window/troubleshooting_info_widget.ui" line="14"/>
        <source>Form</source>
        <translation>ui标题</translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1499"/>
        <source>Idle</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="342"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1501"/>
        <source>Printing</source>
        <translation>忙碌</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="344"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1503"/>
        <source>Stopped</source>
        <translation>停止</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="438"/>
        <source>Printer Name Cannot Contains &apos;%1&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>打印机名称不能包含‘%1’，而且不能为空，且少于128个字母！</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="455"/>
        <source>Exist Same Name Printer!</source>
        <translation>存在同名打印机！</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="458"/>
        <source>Printer name is not case sensitive!</source>
        <translation>打印机名称不区分大小写！</translation>
    </message>
</context>
<context>
    <name>WaitingDialog</name>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="32"/>
        <source>Widget</source>
        <translation>控件</translation>
    </message>
    <message>
        <location filename="../ui/custom_ui/waiting_dialog.ui" line="87"/>
        <source>TextLabel</source>
        <translation>文本框</translation>
    </message>
</context>
</TS>
