<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DictionaryForChinesePrinter</name>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="10"/>
        <source>letter</source>
        <translation>信纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="11"/>
        <source>Page Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="12"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="262"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="423"/>
        <source>Resolution</source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="13"/>
        <source>Paper Source</source>
        <translation>来源</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="14"/>
        <source>Source</source>
        <translation>来源</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="15"/>
        <source>PageType</source>
        <translation>纸张类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="16"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="437"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="574"/>
        <source>Media Type</source>
        <translation>纸张类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="17"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="439"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="18"/>
        <source>Direction</source>
        <translation>打印方向</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="19"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="158"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="501"/>
        <source>Duplex</source>
        <translation>双面打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="20"/>
        <source>Pages per Side</source>
        <translation>每面页数</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="21"/>
        <source>Ink Rank</source>
        <translation>墨水等级</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="22"/>
        <source>Output Order</source>
        <translation>输出顺序</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="23"/>
        <source>legal</source>
        <translation>法条</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="24"/>
        <source>statement</source>
        <translation>声明</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="25"/>
        <source>executive</source>
        <translation>行政文件</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="26"/>
        <source>monarch</source>
        <translation>单色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="27"/>
        <source>com10</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="28"/>
        <source>auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="29"/>
        <source>multi-purpose tray</source>
        <translation>多功能托盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="30"/>
        <source>drawer 1</source>
        <translation>纸盒1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="31"/>
        <source>drawer 2</source>
        <translation>纸盒2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="32"/>
        <source>plain paper</source>
        <translation>普通纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="33"/>
        <source>left</source>
        <translation>左侧</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="34"/>
        <source>right</source>
        <translation>右侧</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="35"/>
        <source>top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="36"/>
        <source>bindingedge</source>
        <translation>装订</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="37"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="467"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="598"/>
        <source>Output Mode</source>
        <translation>输出模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="38"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="468"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="600"/>
        <source>Grayscale</source>
        <translation>灰度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="39"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="595"/>
        <source>EconoMode</source>
        <translation>经济模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="40"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="551"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="596"/>
        <source>Off</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="41"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="592"/>
        <source>Media Source</source>
        <translation>纸张来源</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="42"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="594"/>
        <source>Tray1</source>
        <translation>纸盘1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="43"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="575"/>
        <source>Unspecified</source>
        <translation>未指定</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="44"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="577"/>
        <source>Light 60-74g</source>
        <translation>轻磅纸 60-74 g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="45"/>
        <source>Mid-Weight 96-110g</source>
        <translation>中等重量纸 96-110 g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="46"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="579"/>
        <source>Heavy 111-130g</source>
        <translation>重磅纸 111-130 g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="47"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="580"/>
        <source>Extra Heavy 131-175g</source>
        <translation>超重磅纸 131-175 g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="48"/>
        <source>Monochrome Laster Transparency</source>
        <translation>单色激光透明</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="49"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="441"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="582"/>
        <source>Labels</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="50"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="583"/>
        <source>Letterhead</source>
        <translation>信头纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="51"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="440"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="584"/>
        <source>Envelope</source>
        <translation>信封</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="52"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="585"/>
        <source>Preprinted</source>
        <translation>预打印纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="53"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="586"/>
        <source>Prepunched</source>
        <translation>预先打孔纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="54"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="587"/>
        <source>Colored</source>
        <translation>彩纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="55"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="438"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="588"/>
        <source>Bond</source>
        <translation>证券纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="56"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="445"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="589"/>
        <source>Recycled</source>
        <translation>再生纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="57"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="590"/>
        <source>Rough</source>
        <translation>粗糙纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="58"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="591"/>
        <source>Vellum</source>
        <translation>羊皮纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="59"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="602"/>
        <source>Print Quality</source>
        <translation>打印质量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="60"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="603"/>
        <source>FastRes 600</source>
        <translation>快速打印 600</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="61"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="466"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="556"/>
        <source>Media Size</source>
        <translation>纸张大小</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="62"/>
        <source>1SIDECOATED1</source>
        <translation>单面覆膜纸1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="63"/>
        <source>1SIDECOATED2</source>
        <translation>单面覆膜纸2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="64"/>
        <source>1Sided</source>
        <translation>单面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="65"/>
        <source>2up</source>
        <translation>二合一</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="66"/>
        <source>2Staples</source>
        <translation>2钉</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="67"/>
        <source>LeftDouble</source>
        <translation>2钉(左)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="68"/>
        <source>TopDouble</source>
        <translation>2钉(上)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="69"/>
        <source>2holes</source>
        <translation>2孔</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="70"/>
        <source>2SIDECOATED1</source>
        <translation>双面覆膜纸1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="71"/>
        <source>2SIDECOATED2</source>
        <translation>双面覆膜纸2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="72"/>
        <source>2Sided</source>
        <translation>双面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="73"/>
        <source>3holes</source>
        <translation>3孔</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="74"/>
        <source>4up</source>
        <translation>四合一</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="75"/>
        <source>4holes</source>
        <translation>4孔</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="76"/>
        <source>KMSectionManagement</source>
        <translation>账户跟踪</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="77"/>
        <source>AutoTrapping</source>
        <translation>自动获取</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="78"/>
        <source>BackCoverPage</source>
        <translation>封底</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="79"/>
        <source>BackCoverTray</source>
        <translation>封底纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="80"/>
        <source>KMCopySecurityBackgroundPattern</source>
        <translation>背景样式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="81"/>
        <source>BindEdge</source>
        <translation>装订边距设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="82"/>
        <source>Binding</source>
        <translation>装订位置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="83"/>
        <source>BLACK</source>
        <translation>黑</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="84"/>
        <source>Blank</source>
        <translation>空白纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="85"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="477"/>
        <source>Blue</source>
        <translation>蓝</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="86"/>
        <source>Booklet</source>
        <translation>小册子</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="87"/>
        <source>CENTERBOTTOM</source>
        <translation>底部</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="88"/>
        <source>LEFTBOTTOM</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="89"/>
        <source>RIGHTBOTTOM</source>
        <translation>右下</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="90"/>
        <source>BottomLeft</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="91"/>
        <source>KMBoxNumbe</source>
        <translation>BOX号</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="92"/>
        <source>BypassTray</source>
        <translation>手送纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="93"/>
        <source>Cas1</source>
        <translation>纸盘1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="94"/>
        <source>Cas2</source>
        <translation>纸盘2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="95"/>
        <source>Cas3</source>
        <translation>纸盘3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="96"/>
        <source>Cas4</source>
        <translation>纸盘4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="97"/>
        <source>Cas5</source>
        <translation>纸盘5</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="98"/>
        <source>CENTER</source>
        <translation>中央</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="99"/>
        <source>Stitch</source>
        <translation>中央装订&amp;折叠</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="100"/>
        <source>KMCopySecurityCharacters</source>
        <translation>字符</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="101"/>
        <source>CNAdvancedSmoothing</source>
        <translation>高级平滑</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="102"/>
        <source>CNColorHalftone</source>
        <translation>彩色半色调</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="103"/>
        <source>CNColorMode</source>
        <translation>彩色模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="104"/>
        <source>CNColorToUseWithBlack</source>
        <translation>彩色与黑色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="105"/>
        <source>CNCopySetNumbering</source>
        <translation>打印编号</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="106"/>
        <source>CNCreep</source>
        <translation>位移校正</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="107"/>
        <source>CNDisplacementCorrection</source>
        <translation>位移校正设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="108"/>
        <source>CNEnableTrustPrint</source>
        <translation>设备加密打印功能</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="109"/>
        <source>CNFinisher</source>
        <translation>输出选项</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="110"/>
        <source>CNHalftone</source>
        <translation>半色调</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="111"/>
        <source>CNImageRefinement</source>
        <translation>图像细化</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="112"/>
        <source>CNInterleafMediaType</source>
        <translation>插入纸张类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="113"/>
        <source>CNInterleafPrint</source>
        <translation>插入纸张打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="114"/>
        <source>CNInterleafSheet</source>
        <translation>插入纸张(透明胶片之间)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="115"/>
        <source>CNJobExecMode</source>
        <translation>输出打印数据模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="116"/>
        <source>CNLineControl</source>
        <translation>线条控制</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="117"/>
        <source>CNLineRefinement</source>
        <translation>线条细化</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="118"/>
        <source>CNMatchingMethod</source>
        <translation>匹配设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="119"/>
        <source>CNMonitorProfile</source>
        <translation>显示器匹配配置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="120"/>
        <source>CNMultiPunch</source>
        <translation>打孔类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="121"/>
        <source>CNNumberOfColors</source>
        <translation>颜色数量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="122"/>
        <source>CNOutputPartition</source>
        <translation>逐份排序方法</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="123"/>
        <source>CNPunch</source>
        <translation>打孔位置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="124"/>
        <source>CNPuncher</source>
        <translation>打孔设备</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="125"/>
        <source>CNSaddleStitch</source>
        <translation>鞍式装订</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="126"/>
        <source>CNSharpness</source>
        <translation>锐度范围</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="127"/>
        <source>CNShiftStartPrintPosition</source>
        <translation>移动位置开始打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="128"/>
        <source>CNTonerSaving</source>
        <translation>节省墨粉</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="129"/>
        <source>CNTrustPrint</source>
        <translation>使用设备的加密打印功能</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="130"/>
        <source>CNUseCSModeJobAccount</source>
        <translation>部门ID管理功能</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="131"/>
        <source>CNUseCSModeSecured</source>
        <translation>加密打印功能</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="132"/>
        <source>CNVfolding</source>
        <translation>折叠</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="133"/>
        <source>Collate</source>
        <translation>逐份打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="134"/>
        <source>GraphicColorMatching</source>
        <translation>配色(图表)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="135"/>
        <source>PhotoColorMatching</source>
        <translation>配色(照片)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="136"/>
        <source>TextColorMatching</source>
        <translation>配色(文本)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="137"/>
        <source>OriginalImageType</source>
        <translation>颜色设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="138"/>
        <source>Colorimetric</source>
        <translation>色差最小</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="139"/>
        <source>ColorTone</source>
        <translation>色调</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="140"/>
        <source>Combination</source>
        <translation>版面分页</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="141"/>
        <source>CopyGuard</source>
        <translation>复印防止</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="142"/>
        <source>CopyProtect</source>
        <translation>复印保护</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="143"/>
        <source>KMCopySecurityEnable</source>
        <translation>复印安全</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="144"/>
        <source>KMCopySecurityMode</source>
        <translation>复印安全模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="145"/>
        <source>CoverMode</source>
        <translation>封面模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="146"/>
        <source>UserCustomType1</source>
        <translation>自定义1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="147"/>
        <source>Cyan</source>
        <translation>青</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="148"/>
        <source>Dark</source>
        <translation>暗</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="149"/>
        <source>KMCopySecurityDateTime</source>
        <translation>日期和时间</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="150"/>
        <source>KMCopySecurityDateFormat</source>
        <translation>日期格式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="151"/>
        <source>KMCopySecurityDCNStart</source>
        <translation>打印起始页码</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="152"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="460"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="153"/>
        <source>KMDepCode</source>
        <translation>部门编号</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="154"/>
        <source>CNDetectPaperSize</source>
        <translation>检测纸张大小</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="155"/>
        <source>KMCopySecurityDCNumber</source>
        <translation>分布控制编码</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="156"/>
        <source>Document</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="157"/>
        <source>KMEncryption</source>
        <translation>驱动加密</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="159"/>
        <source>DuplexNoTumble</source>
        <translation>双面打印(长边装订)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="160"/>
        <source>DuplexTumble</source>
        <translation>双面打印(短边装订)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="161"/>
        <source>String4Pt</source>
        <translation>边缘增强</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="162"/>
        <source>EFFECT2</source>
        <translation>浮雕背景</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="163"/>
        <source>EFFECT1</source>
        <translation>浮雕文本</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="164"/>
        <source>KMEncPass</source>
        <translation>密码加密</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="165"/>
        <source>Env2</source>
        <translation>信封托盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="166"/>
        <source>TextGraphic</source>
        <translation>文本/图片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="167"/>
        <source>KMBoxFileName</source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="168"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="520"/>
        <source>Finisher</source>
        <translation>出纸装置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="169"/>
        <source>Finishing</source>
        <translation>偏移</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="170"/>
        <source>Fold</source>
        <translation>折纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="171"/>
        <source>COMPOSITION1</source>
        <translation>正面覆盖</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="172"/>
        <source>FrontCoverPage</source>
        <translation>封面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="173"/>
        <source>FrontCoverTray</source>
        <translation>封面纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="174"/>
        <source>FullColor</source>
        <translation>全彩</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="175"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="409"/>
        <source>General</source>
        <translation>基本功能</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="176"/>
        <source>GlossyMode</source>
        <translation>光泽模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="177"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="537"/>
        <source>Gradation</source>
        <translation>渐变</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="178"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="476"/>
        <source>Green</source>
        <translation>绿</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="179"/>
        <source>Group</source>
        <translation>不逐份</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="180"/>
        <source>HalfFold</source>
        <translation>折页</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="181"/>
        <source>PrinterHDD</source>
        <translation>硬盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="182"/>
        <source>ThickPaper</source>
        <translation>厚纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="183"/>
        <source>ThickPaperH</source>
        <translation>高级重磅纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="184"/>
        <source>HEAVY1</source>
        <translation>厚纸1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="185"/>
        <source>HEAVY2</source>
        <translation>厚纸2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="186"/>
        <source>HEAVY3</source>
        <translation>厚纸3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="187"/>
        <source>HEAVY4</source>
        <translation>厚纸4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="188"/>
        <source>Quality2</source>
        <translation>高质量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="189"/>
        <source>HighResolution</source>
        <translation>高分辨率</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="190"/>
        <source>Speed</source>
        <translation>高速度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="191"/>
        <source>FFPunch</source>
        <translation>打孔</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="192"/>
        <source>IDPrint</source>
        <translation>ID&amp;打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="193"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="426"/>
        <source>Quality</source>
        <translation>质量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="194"/>
        <source>FFOutputMode</source>
        <translation>图像质量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="195"/>
        <source>CNSuperSmooth</source>
        <translation>图像细化</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="196"/>
        <source>InstallableOptions</source>
        <translation>可安装选项</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="197"/>
        <source>HDD</source>
        <translation>已安装</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="198"/>
        <source>OHPOpTray</source>
        <translation>插页纸盒</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="199"/>
        <source>KMCopySecurityJobNumber</source>
        <translation>作业编号</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="200"/>
        <source>LARGE</source>
        <translation>大</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="201"/>
        <source>FFLayout</source>
        <translation>布局</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="202"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="203"/>
        <source>LeftBinding</source>
        <translation>左装订</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="204"/>
        <source>1Staple(Left)</source>
        <translation>左角</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="205"/>
        <source>LHEAD</source>
        <translation>专用信纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="206"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="444"/>
        <source>Light</source>
        <translation>亮</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="207"/>
        <source>Magenta</source>
        <translation>品红</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="208"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="209"/>
        <source>MediaType</source>
        <translation>纸张类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="210"/>
        <source>Medium</source>
        <translation>中等</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="211"/>
        <source>Mode1</source>
        <translation>模式1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="212"/>
        <source>Mode2</source>
        <translation>模式2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="213"/>
        <source>Mode3</source>
        <translation>模式3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="214"/>
        <source>Mode4</source>
        <translation>模式4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="215"/>
        <source>Monitor</source>
        <translation>感觉(匹配显示器颜色)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="216"/>
        <source>mono</source>
        <translation>黑白</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="217"/>
        <source>PrinterDefault</source>
        <translation>打印机默认</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="218"/>
        <source>offset</source>
        <translation>偏移</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="219"/>
        <source>OHP</source>
        <translation>透明胶片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="220"/>
        <source>CNOutputAdjustment</source>
        <translation>输出调整</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="221"/>
        <source>FFColorMode</source>
        <translation>输出颜色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="222"/>
        <source>KMOutputMethod</source>
        <translation>输出方式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="223"/>
        <source>OutputBin</source>
        <translation>输出纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="224"/>
        <source>PageRegion</source>
        <translation>原稿纸张尺寸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="225"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="527"/>
        <source>PageSize</source>
        <translation>输出纸张尺寸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="226"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="528"/>
        <source>InputSlot</source>
        <translation>纸张来源</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="227"/>
        <source>PaperSources</source>
        <translation>纸张来源</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="228"/>
        <source>KMAccPass</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="229"/>
        <source>PasswordCopy</source>
        <translation>密码复印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="230"/>
        <source>KMCopySecurityPass</source>
        <translation>密码复印-密码</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="231"/>
        <source>KMCopySecurityPatternAngle</source>
        <translation>角度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="232"/>
        <source>KMCopySecurityPatternColor</source>
        <translation>样式颜色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="233"/>
        <source>KMCopySecurityPatternContrast</source>
        <translation>对比度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="234"/>
        <source>KMCopySecurityPatternDensity</source>
        <translation>浓度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="235"/>
        <source>KMCopySecurityPatternEmboss</source>
        <translation>浮雕模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="236"/>
        <source>KMCopySecurityPatternOverwrite</source>
        <translation>样式覆盖</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="237"/>
        <source>KMCopySecurityPatternTextSize</source>
        <translation>文本大小</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="238"/>
        <source>Photo</source>
        <translation>照片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="239"/>
        <source>Photographic</source>
        <translation>感觉(摄影)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="240"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="448"/>
        <source>Plain</source>
        <translation>普通纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="241"/>
        <source>PlainPaper</source>
        <translation>普通纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="242"/>
        <source>PlainLPaper</source>
        <translation>薄纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="243"/>
        <source>Plain(2nd)</source>
        <translation>普通纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="244"/>
        <source>PlainPaper1</source>
        <translation>普通纸1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="245"/>
        <source>PlainPaper2</source>
        <translation>普通纸2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="246"/>
        <source>PlainPaperL</source>
        <translation>薄纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="247"/>
        <source>print</source>
        <translation>普通打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="248"/>
        <source>KMStampPNPrintPosition</source>
        <translation>打印位置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="249"/>
        <source>KMDuplex</source>
        <translation>打印类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="250"/>
        <source>ProofMode</source>
        <translation>校样打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="251"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="535"/>
        <source>Punch</source>
        <translation>打孔</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="252"/>
        <source>KOPunch</source>
        <translation>打孔</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="253"/>
        <source>PUNF2</source>
        <translation>2孔(法语)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="254"/>
        <source>PUNF4</source>
        <translation>4孔(法语)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="255"/>
        <source>PUNS4</source>
        <translation>4孔(瑞典语)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="256"/>
        <source>PUNU2</source>
        <translation>2孔(法语)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="257"/>
        <source>PUNU23</source>
        <translation>3孔</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="258"/>
        <source>PhotoPureBlack</source>
        <translation>纯黑(照片)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="259"/>
        <source>TextPureBlack</source>
        <translation>纯黑(文本)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="260"/>
        <source>Recycled(2nd)</source>
        <translation>再生纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="261"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="475"/>
        <source>Red</source>
        <translation>红</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="263"/>
        <source>RIGHTCENTER</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="264"/>
        <source>RightBinding</source>
        <translation>右装订</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="265"/>
        <source>1Staple(Right)</source>
        <translation>右角</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="266"/>
        <source>CNRotatePrint</source>
        <translation>旋转180°打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="267"/>
        <source>rotation</source>
        <translation>旋转</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="268"/>
        <source>KMSafeQUser</source>
        <translation>SafeQ用户</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="269"/>
        <source>Saturation</source>
        <translation>饱和</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="270"/>
        <source>BoxPrint</source>
        <translation>保存到用户BOX&amp;打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="271"/>
        <source>Box</source>
        <translation>保存到用户BOX</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="272"/>
        <source>GraphicScreen</source>
        <translation>屏幕(图表)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="273"/>
        <source>PhotoScreen</source>
        <translation>屏幕(照片)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="274"/>
        <source>TextScreen</source>
        <translation>屏幕(文本)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="275"/>
        <source>Secure</source>
        <translation>机密打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="276"/>
        <source>KMSecID</source>
        <translation>机密打印ID</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="277"/>
        <source>KMSecPass</source>
        <translation>机密打印密码</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="278"/>
        <source>secured</source>
        <translation>机密打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="279"/>
        <source>SelectColor</source>
        <translation>选择颜色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="280"/>
        <source>KMCopySecuritySerialNumber</source>
        <translation>序列号</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="281"/>
        <source>SideDeck</source>
        <translation>纸盘5</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="282"/>
        <source>SingleSidedOnly</source>
        <translation>仅单面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="283"/>
        <source>SMALL</source>
        <translation>小</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="284"/>
        <source>Smooth1</source>
        <translation>平滑1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="285"/>
        <source>Smooth2</source>
        <translation>平滑2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="286"/>
        <source>GraphicSmoothing</source>
        <translation>平滑(图表)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="287"/>
        <source>PhotoSmoothing</source>
        <translation>平滑(照片)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="288"/>
        <source>Special</source>
        <translation>特殊纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="289"/>
        <source>CNSpecialPrintMode</source>
        <translation>特殊打印模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="290"/>
        <source>StampRepeat</source>
        <translation>印记重复</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="291"/>
        <source>Standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="292"/>
        <source>FFStaple</source>
        <translation>装订</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="293"/>
        <source>StapleCollate</source>
        <translation>装订&amp;逐份</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="294"/>
        <source>StapleGroup</source>
        <translation>装订&amp;不逐份</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="295"/>
        <source>StapleLocation</source>
        <translation>装订位置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="296"/>
        <source>store</source>
        <translation>存储打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="297"/>
        <source>TAB</source>
        <translation>标签纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="298"/>
        <source>Ledger</source>
        <translation>小报</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="299"/>
        <source>Text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="300"/>
        <source>KMStampPNTextColor</source>
        <translation>文本颜色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="301"/>
        <source>KMCopySecurityTimeFormat</source>
        <translation>时间格式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="302"/>
        <source>CNTonerDensity</source>
        <translation>墨粉密度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="303"/>
        <source>TonerSave</source>
        <translation>节省墨粉</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="304"/>
        <source>Top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="305"/>
        <source>TopBinding</source>
        <translation>上装订</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="306"/>
        <source>UpperLeftSingle</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="307"/>
        <source>RIGHTTOP</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="308"/>
        <source>TopLeft</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="309"/>
        <source>TopRight</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="310"/>
        <source>TRACING</source>
        <translation>扫图纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="311"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="446"/>
        <source>Transparency</source>
        <translation>透明纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="312"/>
        <source>TransparencyInterleave</source>
        <translation>OHP 插页</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="313"/>
        <source>Tray2</source>
        <translation>纸盘2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="314"/>
        <source>Tray3</source>
        <translation>纸盘3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="315"/>
        <source>Tray4</source>
        <translation>纸盘4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="316"/>
        <source>TrayA</source>
        <translation>纸盘A</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="317"/>
        <source>TrayB</source>
        <translation>纸盘B</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="318"/>
        <source>TrayC</source>
        <translation>纸盘C</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="319"/>
        <source>TriFold</source>
        <translation>三折页</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="320"/>
        <source>TwoColors</source>
        <translation>双色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="321"/>
        <source>Printer</source>
        <translation>使用打印机默认设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="322"/>
        <source>CNSkipBlank</source>
        <translation>使用跳过空白页模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="323"/>
        <source>KMAuthentication</source>
        <translation>用户认证</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="324"/>
        <source>KMAuthUser</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="325"/>
        <source>Vivid</source>
        <translation>鲜艳</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="326"/>
        <source>Vividphoto</source>
        <translation>鲜艳照片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="327"/>
        <source>Yellow</source>
        <translation>黄</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="328"/>
        <source>KMDriverEncryption</source>
        <translation>打印驱动加密</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="329"/>
        <source>ColorModel</source>
        <translation>色彩模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="330"/>
        <source>CMY</source>
        <translation>彩色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="331"/>
        <source>Gray</source>
        <translation>黑白</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="332"/>
        <source>UTDuplex</source>
        <translation>双面打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="333"/>
        <source>0Front</source>
        <translation>近正面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="334"/>
        <source>1Both</source>
        <translation>双面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="335"/>
        <source>2Back</source>
        <translation>仅背面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="336"/>
        <source>CardFrontGroup</source>
        <translation>卡正面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="337"/>
        <source>CFColourFormat</source>
        <translation>卡正面颜色格式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="338"/>
        <source>Sharpness</source>
        <translation>锐度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="339"/>
        <source>0ResinQuality</source>
        <translation>高速度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="340"/>
        <source>1ResinQuality</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="341"/>
        <source>2ResinQuality</source>
        <translation>高质量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="342"/>
        <source>ColourCorrection</source>
        <translation>颜色校正</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="343"/>
        <source>0ColourCorrection</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="344"/>
        <source>1ColourCorrection</source>
        <translation>驱动管理</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="345"/>
        <source>2ColourCorrection</source>
        <translation>感觉</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="346"/>
        <source>3ColourCorrection</source>
        <translation>饱和度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="347"/>
        <source>4ColourCorrection</source>
        <translation>相对色度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="348"/>
        <source>5ColourCorrection</source>
        <translation>绝对色度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="349"/>
        <source>Postcard</source>
        <translation>明信片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="350"/>
        <source>DoublePostcardRotated</source>
        <translation>明信片2(双面)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="351"/>
        <source>EnvYou4</source>
        <translation>Y4信封</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="352"/>
        <source>BrMediaType</source>
        <translation>介质类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="353"/>
        <source>THIN</source>
        <translation>薄纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="354"/>
        <source>THICKERPAPER2</source>
        <translation>特厚纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="355"/>
        <source>ENV</source>
        <translation>信封</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="356"/>
        <source>ENVTHICK</source>
        <translation>厚信封</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="357"/>
        <source>ENVTHIN</source>
        <translation>薄信封</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="358"/>
        <source>GraphicsOptions</source>
        <translation>图形</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="359"/>
        <source>SpeedOptions</source>
        <translation>减速模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="360"/>
        <source>MediaOptions</source>
        <translation>介质类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="361"/>
        <source>PrnDef</source>
        <translation>自动介质选择</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="362"/>
        <source>Cardstock</source>
        <translation>卡片纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="363"/>
        <source>Highqlty</source>
        <translation>高质量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="364"/>
        <source>OptionTray</source>
        <translation>可选纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="365"/>
        <source>1Cassette</source>
        <translation>单纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="366"/>
        <source>LargeCapacityTray</source>
        <translation>大容量纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="367"/>
        <source>NotInstalled</source>
        <translation>未安装</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="368"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="526"/>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="369"/>
        <source>MultiTray</source>
        <translation>手送纸盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="370"/>
        <source>1Tray</source>
        <translation>纸盘1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="371"/>
        <source>2Tray</source>
        <translation>纸盘2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="372"/>
        <source>3Tray</source>
        <translation>纸盘3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="373"/>
        <source>4Tray</source>
        <translation>纸盘4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="374"/>
        <source>5Tray</source>
        <translation>纸盘5</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="375"/>
        <source>RIPaperPolicy</source>
        <translation>适合纸张</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="376"/>
        <source>CutSheetFeeder</source>
        <translation>进纸器</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="377"/>
        <source>Tractor</source>
        <translation>拖纸器</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="378"/>
        <source>RollPaper</source>
        <translation>卷纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="379"/>
        <source>EPSpeed</source>
        <translation>打印速度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="380"/>
        <source>High</source>
        <translation>高速</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="381"/>
        <source>Std</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="382"/>
        <source>EPPrintDirection</source>
        <translation>打印方向</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="383"/>
        <source>UniOff</source>
        <translation>双向</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="384"/>
        <source>UniOn</source>
        <translation>单向</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="385"/>
        <source>EPSvRP</source>
        <translation>节省卷纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="386"/>
        <source>EPTearOff</source>
        <translation>自动切纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="387"/>
        <source>EPOffsetHrz</source>
        <translation>水平偏移量</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="388"/>
        <source>EPTopMP</source>
        <translation>上边优先级</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="389"/>
        <source>Driver</source>
        <translation>驱动程序</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="390"/>
        <source>EPPackMode</source>
        <translation>打包模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="391"/>
        <source>RGB</source>
        <translation>混色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="392"/>
        <source>PrinterOptions</source>
        <translation>打印机选项</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="393"/>
        <source>MediaMethod</source>
        <translation>打印方式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="394"/>
        <source>PaperType</source>
        <translation>打印纸类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="395"/>
        <source>PrintSpeed</source>
        <translation>打印速度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="396"/>
        <source>PrintDarkness</source>
        <translation>浓度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="397"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="513"/>
        <source>None</source>
        <translation>使用打印机当前设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="398"/>
        <source>PostAction</source>
        <translation>打印后操作</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="399"/>
        <source>FowardOffset</source>
        <translation>打印完成后进纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="400"/>
        <source>SpecifiedPages</source>
        <translation>间隔份数</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="401"/>
        <source>effects</source>
        <translation>效果</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="402"/>
        <source>MirrorImage</source>
        <translation>镜像</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="403"/>
        <source>NegativeImage</source>
        <translation>负片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="404"/>
        <source>Orientation</source>
        <translation>打印方向</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="405"/>
        <source>Compression</source>
        <translation>压缩</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="406"/>
        <source>DataCompress</source>
        <translation>数据压缩</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="407"/>
        <source>ColorOption</source>
        <translation>颜色选项</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="408"/>
        <source>THICK</source>
        <translation>厚纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="410"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="605"/>
        <source>Draft</source>
        <translation>草稿</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="411"/>
        <source>Normal</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="412"/>
        <source>Normal Color</source>
        <translation>普通色彩</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="413"/>
        <source>Normal Grayscale</source>
        <translation>普通灰度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="414"/>
        <source>Draft Color</source>
        <translation>漂移彩色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="415"/>
        <source>Draft Grayscale</source>
        <translation>漂移灰度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="416"/>
        <source>Best</source>
        <translation>最佳</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="417"/>
        <source>High-Resolution Photo</source>
        <translation>高分辨率照片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="418"/>
        <source>Fast Draft</source>
        <translation>快速漂移</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="419"/>
        <source>Installed Cartridges</source>
        <translation>安装的墨盒</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="420"/>
        <source>Photo Only</source>
        <translation>仅相片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="421"/>
        <source>Black and TriColor</source>
        <translation>黑和三原色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="422"/>
        <source>Photo and TriColor</source>
        <translation>相片和三原色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="424"/>
        <source>Color Mode</source>
        <translation>彩色模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="425"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="599"/>
        <source>Black Only Grayscale</source>
        <translation>黑和灰度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="427"/>
        <source>High Resolution </source>
        <translation>打印机分辨率：</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="428"/>
        <source>Auto Source</source>
        <translation>自动源</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="429"/>
        <source>Manual Feed</source>
        <translation>手动送纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="430"/>
        <source>Middle Tray</source>
        <translation>中间托盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="431"/>
        <source>Upper or Only One InputSlot</source>
        <translation>上部或唯一插槽</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="432"/>
        <source>Multi-purpose Tray</source>
        <translation>多用途托盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="433"/>
        <source>Drawer 1 </source>
        <translation>抽屉1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="434"/>
        <source>Drawer 2 </source>
        <translation>抽屉2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="435"/>
        <source>Tray 1</source>
        <translation>托盘1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="436"/>
        <source>Auto Select</source>
        <translation>自动选择</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="442"/>
        <source>Standard Paper</source>
        <translation>标准纸张</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="443"/>
        <source>Heavy</source>
        <translation>重</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="447"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="576"/>
        <source>Plain Paper</source>
        <translation>普通纸张</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="449"/>
        <source>Transparency Film</source>
        <translation>透明胶卷</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="450"/>
        <source>CD or DVD Media</source>
        <translation>CD或DVD媒体</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="451"/>
        <source>Print Density</source>
        <translation>打印密度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="452"/>
        <source>Extra Light (1)</source>
        <translation>极轻(1)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="453"/>
        <source>Light (2)</source>
        <translation>轻(2)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="454"/>
        <source>Medium (3)</source>
        <translation>中等(3)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="455"/>
        <source>Dark (4)</source>
        <translation>暗(4)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="456"/>
        <source>Extra Dark (5)</source>
        <translation>极暗(5)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="457"/>
        <source>Copies</source>
        <translation>副本</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="458"/>
        <source>Adjustment</source>
        <translation>调整</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="459"/>
        <source>Halftone Algorithm</source>
        <translation>Halftone算法</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="461"/>
        <source>Miscellaneous</source>
        <translation>混合</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="462"/>
        <source>N-up Orientation</source>
        <translation>纵向</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="463"/>
        <source>N-up Printing</source>
        <translation>纵向打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="464"/>
        <source>Landscape</source>
        <translation>风景</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="465"/>
        <source>Seascape</source>
        <translation>海景</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="469"/>
        <source>Two-Sided</source>
        <translation>双面打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="470"/>
        <source>Print Settings</source>
        <translation>打印设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="471"/>
        <source>Color Settings</source>
        <translation>彩色设置</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="472"/>
        <source>Color Settings (Advanced)</source>
        <translation>彩色设置(高级)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="473"/>
        <source>Brightness</source>
        <translation>明亮</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="474"/>
        <source>Contrast</source>
        <translation>对比</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="478"/>
        <source>_Media Size</source>
        <translation>纸张大小</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="479"/>
        <source>_Grayscale</source>
        <translation>灰度</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="480"/>
        <source>_Brightness</source>
        <translation>明亮</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="481"/>
        <source>_Contrast</source>
        <translation>对比</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="482"/>
        <source>_Saturation</source>
        <translation>饱和</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="483"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="597"/>
        <source>On</source>
        <translation>开</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="484"/>
        <source>2-Sided Printing</source>
        <translation>双面打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="485"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="486"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="487"/>
        <source>Installable Options</source>
        <translation>可安装选项</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="488"/>
        <source>Duplexer Installed</source>
        <translation>双工</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="489"/>
        <source>Color Model</source>
        <translation>彩色模式</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="490"/>
        <source>Color Precision</source>
        <translation>彩色比例</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="491"/>
        <source>Resolution </source>
        <translation>分辨率</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="492"/>
        <source>Printer Features Common</source>
        <translation>打印机常见功能</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="493"/>
        <source>CD Hub Size</source>
        <translation>CD封面尺寸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="494"/>
        <source>Ink Type</source>
        <translation>墨水类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="495"/>
        <source>Toner Save </source>
        <translation>节省墨粉</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="496"/>
        <source>ON</source>
        <translation>启动</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="497"/>
        <source>Media Type </source>
        <translation>纸张类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="498"/>
        <source>Collate </source>
        <translation>自动分页</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="499"/>
        <source>Image Refinement </source>
        <translation>图像精化</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="500"/>
        <source>Halftones </source>
        <translation>半色调</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="502"/>
        <source>OFF</source>
        <translation>关</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="503"/>
        <source>ON (Long-edged Binding)</source>
        <translation>开(长边装订)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="504"/>
        <source>ON (Short-edged Binding)</source>
        <translation>开(短边装订)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="505"/>
        <source>Paper Size</source>
        <translation>纸张尺寸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="506"/>
        <source>Paper Type</source>
        <translation>纸张类型</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="507"/>
        <source>Thin</source>
        <translation>薄</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="508"/>
        <source>Thick</source>
        <translation>厚</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="509"/>
        <source>Thicker</source>
        <translation>更厚</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="510"/>
        <source>Edge Enhance</source>
        <translation>边缘增强</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="511"/>
        <source>Skip Blank Pages</source>
        <translation>跳过空白页</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="512"/>
        <source>Double-sided Printing</source>
        <translation>双边打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="514"/>
        <source>Reverse Duplex Printing</source>
        <translation>反向双面打印</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="515"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="518"/>
        <source>Long Edge</source>
        <translation>长边</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="516"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="519"/>
        <source>Short Edge</source>
        <translation>短边</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="517"/>
        <source>Two-sided</source>
        <translation>双面</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="521"/>
        <source>Option Tray</source>
        <translation>选件送纸器</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="522"/>
        <source>External Tray</source>
        <translation>外部送纸器</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="523"/>
        <source>Internal Tray 2</source>
        <translation>内部送纸器2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="524"/>
        <source>Internal Shift Tray</source>
        <translation>内部轮班托盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="525"/>
        <source>Not Installed</source>
        <translation>未安装</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="529"/>
        <source>Tray 2</source>
        <translation>托盘2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="530"/>
        <source>Tray 3</source>
        <translation>托盘3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="531"/>
        <source>Tray 4</source>
        <translation>托盘4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="532"/>
        <source>Bypass Tray</source>
        <translation>旁路托盘</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="533"/>
        <source>Destination</source>
        <translation>目的地</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="534"/>
        <source>Staple</source>
        <translation>书钉</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="536"/>
        <source>Toner Saving</source>
        <translation>节约墨粉</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="538"/>
        <source>Fast</source>
        <translation>快速</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="539"/>
        <source>Draft (Color cartridge)</source>
        <translation>草稿(彩色墨盒)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="540"/>
        <source>Normal (Color cartridge)</source>
        <translation>草稿(彩色墨盒)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="541"/>
        <source>Photo (on photo paper)</source>
        <translation>照片 (使用照片纸)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="542"/>
        <source>Photo (Color cartridge, on photo paper)</source>
        <translation>相片(彩色墨盒,相纸)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="543"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="573"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="544"/>
        <source>Letter</source>
        <translation>信纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="545"/>
        <source>Photo or 4x6 inch index card</source>
        <translation>照片或 4x6 英寸索引卡片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="546"/>
        <source>Photo or 5x7 inch index card</source>
        <translation>照片或 5x7 英寸索引卡片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="547"/>
        <source>Photo with tear-off tab</source>
        <translation>带易开标签的照片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="548"/>
        <source>3x5 inch index card</source>
        <translation>3x5 英寸索引卡片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="549"/>
        <source>5x8 inch index card</source>
        <translation>5x8 英寸索引卡片</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="550"/>
        <source>A6 with tear-off tab</source>
        <translation>带易开标签的 A6 纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="552"/>
        <source>300 dpi, Color, Color Cartr.</source>
        <translation>300 dpi, 彩色，彩色墨盒.</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="553"/>
        <source>300 dpi, Draft, Color, Color Cartr.</source>
        <translation>300 dpi, 草稿，彩色，彩色墨盒</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="554"/>
        <source>300 dpi, Draft, Grayscale, Black Cartr.</source>
        <translation>300 dpi, 草稿，灰度，黑色墨盒</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="555"/>
        <source>300 dpi, Grayscale, Black Cartr.</source>
        <translation>300 dpi, 灰度，黑色墨盒</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="557"/>
        <source>Executive 7.25x10.5in</source>
        <translation>行政用纸 7.25x10.5in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="558"/>
        <source>Letter 8.5x11in</source>
        <translation>美国信纸 8.5x11in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="559"/>
        <source>Legal 8.5x14in</source>
        <translation>美国法定用纸 8.5x14in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="560"/>
        <source>Postcard (JIS)</source>
        <translation>明信片 (JIS)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="561"/>
        <source>Double Postcard (JIS)</source>
        <translation>明信片双面 (JIS)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="562"/>
        <source>Monarch Envelope 3.875x7.5in</source>
        <translation>Monarch信封 3.875x7.5in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="563"/>
        <source>DL Envelope 110x220mm</source>
        <translation>DL信封 110x220mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="564"/>
        <source>#10 Envelope 4.12x9.5in</source>
        <translation>10号信封 4.12x9.5in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="565"/>
        <source>C5 Envelope 162x229mm</source>
        <translation>C5信封 162x229mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="566"/>
        <source>B5 Envelope 176x250mm</source>
        <translation>B5信封 176x250mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="567"/>
        <source>C6 Envelope 114x162mm</source>
        <translation>C6信封 114x162mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="568"/>
        <source>#3 Japanese Envelope 120x235mm</source>
        <translation>3号日本信封 120x235mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="569"/>
        <source>Statement 5.5x8.5in</source>
        <translation>结算单 5.5x8.5in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="570"/>
        <source>Photo 4x6in</source>
        <translation>照片 4x6in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="571"/>
        <source>Photo 5x7in</source>
        <translation>照片 5x7in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="572"/>
        <source>Photo 10x15cm</source>
        <translation>照片 10x15cm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="578"/>
        <source>Mid-Weight96-110g</source>
        <translation>中等重量光泽纸96-110g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="581"/>
        <source>Monochrome Laser Transparency</source>
        <translation>单色激光透明</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="593"/>
        <source>Manual Feeder</source>
        <translation>手动进纸</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="601"/>
        <source>Monochrome</source>
        <translation>单色</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="604"/>
        <source>FastRes 1200</source>
        <translation>快速打印 1200</translation>
    </message>
</context>
</TS>
