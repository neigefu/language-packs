<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>DictionaryForChinesePrinter</name>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="10"/>
        <source>letter</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="11"/>
        <source>Page Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="12"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="262"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="423"/>
        <source>Resolution</source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="13"/>
        <source>Paper Source</source>
        <translation>ᠢᠷᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="14"/>
        <source>Source</source>
        <translation>ᠢᠷᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="15"/>
        <source>PageType</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="16"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="437"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="574"/>
        <source>Media Type</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="17"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="439"/>
        <source>Color</source>
        <translation>ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="18"/>
        <source>Direction</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="19"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="158"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="501"/>
        <source>Duplex</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="20"/>
        <source>Pages per Side</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ ᠪᠦᠷᠢᠶᠢᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠲᠣᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="21"/>
        <source>Ink Rank</source>
        <translation>ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠳᠡᠰ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="22"/>
        <source>Output Order</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="23"/>
        <source>legal</source>
        <translation>ᠬᠠᠤᠯᠢ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="24"/>
        <source>statement</source>
        <translation>ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="25"/>
        <source>executive</source>
        <translation>ᠵᠠᠰᠠᠭ ᠵᠠᠬᠢᠷᠭᠠᠨ᠎ᠤ᠋ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="26"/>
        <source>monarch</source>
        <translation>ᠳᠠᠩ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="27"/>
        <source>com10</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠠᠮᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="28"/>
        <source>auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="29"/>
        <source>multi-purpose tray</source>
        <translation>ᠣᠯᠠᠨ ᠴᠢᠳᠠᠪᠤᠷᠢᠲᠤ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="30"/>
        <source>drawer 1</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠬᠠᠢᠷᠴᠠᠭ1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="31"/>
        <source>drawer 2</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠬᠠᠢᠷᠴᠠᠭ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="32"/>
        <source>plain paper</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="33"/>
        <source>left</source>
        <translation>ᠵᠡᠭᠦᠨ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="34"/>
        <source>right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="35"/>
        <source>top</source>
        <translation>ᠣᠷᠣᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="36"/>
        <source>bindingedge</source>
        <translation>ᠦᠳᠡᠬᠦ ᠂ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="37"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="467"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="598"/>
        <source>Output Mode</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="38"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="468"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="600"/>
        <source>Grayscale</source>
        <translation>ᠴᠠᠢᠷᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="39"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="595"/>
        <source>EconoMode</source>
        <translation>ᠠᠵᠤ ᠠᠬᠤᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠪ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="40"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="551"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="596"/>
        <source>Off</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="41"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="592"/>
        <source>Media Source</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠢᠷᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="42"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="594"/>
        <source>Tray1</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="43"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="575"/>
        <source>Unspecified</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="44"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="577"/>
        <source>Light 60-74g</source>
        <translation>ᠬᠥᠩᠭᠡᠨ ᠫᠦᠨᠳ᠋ ᠴᠠᠭᠠᠰᠤ 60 — 74g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="45"/>
        <source>Mid-Weight 96-110g</source>
        <translation>ᠳᠤᠮᠳᠠ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢᠨ ᠬᠦᠨᠳᠦ ᠴᠠᠭᠠᠰᠤ 96— 110g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="46"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="579"/>
        <source>Heavy 111-130g</source>
        <translation>ᠬᠦᠨᠳᠦ ᠹᠦᠨᠲ ᠴᠠᠭᠠᠰᠤ 11—130 g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="47"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="580"/>
        <source>Extra Heavy 131-175g</source>
        <translation>ᠬᠦᠨᠳᠦ᠎ᠡᠴᠡ ᠬᠡᠲᠦᠷᠡᠭᠰᠡᠨ ᠫᠦᠨᠳ᠋ ᠴᠠᠭᠠᠰᠤ 131—175 g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="48"/>
        <source>Monochrome Laster Transparency</source>
        <translation>ᠳᠠᠩ ᠥᠩᠭᠡᠲᠦ ᠯᠠᠢᠰᠧᠷ ᠲᠤᠩᠭᠠᠯᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="49"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="441"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="582"/>
        <source>Labels</source>
        <translation>ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="50"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="583"/>
        <source>Letterhead</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠣᠯᠣᠭᠠᠢ᠎ᠶ᠋ᠢᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="51"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="440"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="584"/>
        <source>Envelope</source>
        <translation>ᠲᠣᠭᠳᠣᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="52"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="585"/>
        <source>Preprinted</source>
        <translation>ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠬᠡᠪᠯᠡᠬᠦ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="53"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="586"/>
        <source>Prepunched</source>
        <translation>ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠴᠣᠭᠣᠯᠲᠤᠷ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="54"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="587"/>
        <source>Colored</source>
        <translation>ᠥᠩᠭᠡᠲᠦ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="55"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="438"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="588"/>
        <source>Bond</source>
        <translation>ᠲᠡᠮᠳᠡᠭᠲᠦ᠎ᠶ᠋ᠢᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="56"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="445"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="589"/>
        <source>Recycled</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠭᠦᠰᠬᠦ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="57"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="590"/>
        <source>Rough</source>
        <translation>ᠪᠦᠳᠦᠭᠦᠯᠢᠭ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="58"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="591"/>
        <source>Vellum</source>
        <translation>ᠬᠣᠨᠢᠨ ᠠᠷᠠᠰᠤ᠎ᠶ᠋ᠢᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="59"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="602"/>
        <source>Print Quality</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="60"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="603"/>
        <source>FastRes 600</source>
        <translation>60 ᠬᠥᠮᠦᠨ᠎ᠢ᠋ ᠲᠦᠷᠭᠡᠨ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="61"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="466"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="556"/>
        <source>Media Size</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="62"/>
        <source>1SIDECOATED1</source>
        <translation>ᠭᠠᠭᠴᠠ ᠲᠠᠯᠠᠲᠤ ᠪᠦᠷᠬᠦᠭᠦᠯ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="63"/>
        <source>1SIDECOATED2</source>
        <translation>ᠭᠠᠭᠴᠠ ᠲᠠᠯᠠᠲᠤ ᠪᠦᠷᠬᠦᠭᠦᠯ ᠴᠠᠭᠠᠰᠤᠨ ᠴᠠᠭ2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="64"/>
        <source>1Sided</source>
        <translation>ᠭᠠᠭᠴᠠ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="65"/>
        <source>2up</source>
        <translation>ᠬᠣᠶᠠᠷ ᠨᠢᠭᠡᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="66"/>
        <source>2Staples</source>
        <translation>2. ᠬᠠᠳᠠᠭᠠᠰᠤᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="67"/>
        <source>LeftDouble</source>
        <translation>2 ᠬᠠᠳᠠᠭᠠᠰᠤ (᠎ᠵᠡᠭᠦᠨ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="68"/>
        <source>TopDouble</source>
        <translation>2 ᠬᠠᠳᠠᠭᠠᠰᠤ2 (᠎ᠳᠡᠭᠡᠳᠦ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="69"/>
        <source>2holes</source>
        <translation>2.2 ᠰᠦᠪᠡ᠎ᠶ᠋ᠢᠨ ᠰᠦᠪᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="70"/>
        <source>2SIDECOATED1</source>
        <translation>ᠬᠣᠣᠰ ᠲᠠᠯᠠᠲᠤ ᠪᠦᠷᠬᠦᠭᠦᠯ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="71"/>
        <source>2SIDECOATED2</source>
        <translation>ᠬᠣᠣᠰ ᠲᠠᠯᠠᠲᠤ ᠪᠦᠷᠬᠦᠭᠦᠯ ᠴᠠᠭᠠᠰᠤ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="72"/>
        <source>2Sided</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="73"/>
        <source>3holes</source>
        <translation>3 ᠨᠦᠬᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="74"/>
        <source>4up</source>
        <translation>ᠳᠥᠷᠪᠡ᠎ᠶ᠋ᠢ ᠨᠢᠭᠡᠳᠬᠡᠨ ᠨᠢᠭᠡ᠎ᠶ᠋ᠢ ᠨᠡᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="75"/>
        <source>4holes</source>
        <translation>4.4ᠨᠦᠬᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="76"/>
        <source>KMSectionManagement</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠡᠷᠦᠬᠡ᠎ᠶ᠋ᠢ ᠮᠥᠷᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="77"/>
        <source>AutoTrapping</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ ᠣᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="78"/>
        <source>BackCoverPage</source>
        <translation>ᠰᠠᠭᠤᠷᠢ ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="79"/>
        <source>BackCoverTray</source>
        <translation>ᠰᠠᠭᠤᠷᠢ ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠬᠦ ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="80"/>
        <source>KMCopySecurityBackgroundPattern</source>
        <translation>ᠠᠷᠤ ᠠᠬᠤᠢ᠎ᠶ᠋ᠢᠨ ᠶᠠᠩᠵᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="81"/>
        <source>BindEdge</source>
        <translation>ᠬᠥᠪᠡᠭᠡ ᠵᠠᠬᠢᠶᠠᠨ᠎ᠤ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭ᠎ᠠ ᠤᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="82"/>
        <source>Binding</source>
        <translation>ᠲᠣᠪᠯᠠᠬᠤ ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="83"/>
        <source>BLACK</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠬᠠᠷᠠᠩᠭᠤᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="84"/>
        <source>Blank</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="85"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="477"/>
        <source>Blue</source>
        <translation>ᠬᠥᠬᠡ ᠬᠥᠬᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="86"/>
        <source>Booklet</source>
        <translation>ᠲᠣᠪᠬᠢᠮᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="87"/>
        <source>CENTERBOTTOM</source>
        <translation>ᠢᠷᠣᠭᠠᠷ᠎ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="88"/>
        <source>LEFTBOTTOM</source>
        <translation>ᠵᠡᠭᠦᠨ ᠳᠣᠣᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="89"/>
        <source>RIGHTBOTTOM</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠳᠣᠣᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="90"/>
        <source>BottomLeft</source>
        <translation>ᠵᠡᠭᠦᠨ ᠳᠣᠣᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="91"/>
        <source>KMBoxNumbe</source>
        <translation>BOX ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="92"/>
        <source>BypassTray</source>
        <translation>ᠭᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠬᠦᠷᠭᠡᠬᠦ ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="93"/>
        <source>Cas1</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="94"/>
        <source>Cas2</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="95"/>
        <source>Cas3</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="96"/>
        <source>Cas4</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="97"/>
        <source>Cas5</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ 5</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="98"/>
        <source>CENTER</source>
        <translation>ᠲᠥᠪ ᠬᠣᠷᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="99"/>
        <source>Stitch</source>
        <translation>ᠲᠥᠪ᠎ᠦ᠋ᠨ ᠴᠢᠮᠡᠭᠯᠡᠯ ᠬᠢᠬᠦ᠊ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="100"/>
        <source>KMCopySecurityCharacters</source>
        <translation>ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="101"/>
        <source>CNAdvancedSmoothing</source>
        <translation>ᠥᠨᠳᠥᠷ ᠲᠡᠪᠰᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="102"/>
        <source>CNColorHalftone</source>
        <translation>ᠥᠩᠭᠡ ᠬᠠᠭᠠᠰ ᠥᠩᠭᠡᠨ᠎ᠦ᠌ ᠵᠣᠬᠢᠴᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="103"/>
        <source>CNColorMode</source>
        <translation>ᠥᠩᠭᠡᠲᠦ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="104"/>
        <source>CNColorToUseWithBlack</source>
        <translation>ᠥᠩᠭᠡ ᠬᠢᠭᠡᠳ ᠬᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="105"/>
        <source>CNCopySetNumbering</source>
        <translation>ᠳᠠᠷᠤᠮᠯᠠᠬᠤ ᠳ᠋ᠤᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="106"/>
        <source>CNCreep</source>
        <translation>ᠪᠠᠢᠷᠢ ᠰᠢᠯᠵᠢᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠵᠠᠯᠠᠷᠠᠭᠤᠯᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="107"/>
        <source>CNDisplacementCorrection</source>
        <translation>ᠪᠠᠢᠷᠢ ᠰᠢᠯᠵᠢᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠵᠠᠯᠠᠷᠠᠭᠤᠯᠤᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="108"/>
        <source>CNEnableTrustPrint</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢᠭᠤᠴᠠᠯᠠᠨ ᠬᠡᠪᠯᠡᠬᠦ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="109"/>
        <source>CNFinisher</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="110"/>
        <source>CNHalftone</source>
        <translation>ᠬᠠᠭᠠᠰ ᠥᠩᠭᠡ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="111"/>
        <source>CNImageRefinement</source>
        <translation>ᠢᠮᠡᠬᠧ ᠵᠢᠷᠤᠭ᠎ᠢ᠋ ᠨᠠᠷᠢᠯᠢᠭᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="112"/>
        <source>CNInterleafMediaType</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="113"/>
        <source>CNInterleafPrint</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠬᠡᠪᠯᠡᠬᠦ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="114"/>
        <source>CNInterleafSheet</source>
        <translation>ᠴᠠᠭᠠᠰᠤ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ (᠎ᠲᠤᠩᠭᠠᠯᠠᠭ ᠹᠢᠯᠢᠮ᠎ᠦ᠋ᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="115"/>
        <source>CNJobExecMode</source>
        <translation>ᠬᠡᠪᠯᠡᠬᠦ ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠮᠣᠳᠧᠯ᠎ᠢ᠋ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="116"/>
        <source>CNLineControl</source>
        <translation>ᠵᠢᠷᠤᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠡᠵᠡᠮᠰᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="117"/>
        <source>CNLineRefinement</source>
        <translation>ᠵᠢᠷᠤᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠨᠠᠷᠢᠯᠢᠭᠵᠢᠭᠤᠯᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="118"/>
        <source>CNMatchingMethod</source>
        <translation>ᠠᠪᠴᠠᠯᠳᠤᠭᠤᠯᠬᠤ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="119"/>
        <source>CNMonitorProfile</source>
        <translation>ᠦᠵᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠠᠪᠤᠴᠠᠯᠳᠤᠯ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="120"/>
        <source>CNMultiPunch</source>
        <translation>ᠨᠦᠬᠡᠯᠡᠬᠦ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="121"/>
        <source>CNNumberOfColors</source>
        <translation>ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠲᠤᠭ ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="122"/>
        <source>CNOutputPartition</source>
        <translation>ᠬᠡᠰᠡᠭ ᠳᠠᠷᠠᠭᠠᠯᠠᠨ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="123"/>
        <source>CNPunch</source>
        <translation>ᠨᠦᠬᠡ ᠴᠣᠭᠣᠯᠬᠤ ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="124"/>
        <source>CNPuncher</source>
        <translation>ᠨᠦᠬᠡ ᠭᠠᠷᠭᠠᠬᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="125"/>
        <source>CNSaddleStitch</source>
        <translation>ᠡᠮᠡᠭᠡᠯ ᠦᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="126"/>
        <source>CNSharpness</source>
        <translation>ᠬᠤᠷᠴᠠ ᠬᠡᠪᠴᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="127"/>
        <source>CNShiftStartPrintPosition</source>
        <translation>ᠪᠠᠢᠷᠢ᠎ᠶ᠋ᠢ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ ᠡᠬᠢᠯᠡᠨ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="128"/>
        <source>CNTonerSaving</source>
        <translation>ᠪᠣᠳᠣᠭ ᠠᠷᠪᠢᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠠᠷᠪᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="129"/>
        <source>CNTrustPrint</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ ᠬᠡᠪᠯᠡᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="130"/>
        <source>CNUseCSModeJobAccount</source>
        <translation>ᠰᠠᠯᠠᠭ᠎ᠠ ᠮᠥᠴᠢᠷ᠎ᠦ᠋ᠨ ID ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="131"/>
        <source>CNUseCSModeSecured</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ ᠬᠡᠪᠯᠡᠬᠦ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="132"/>
        <source>CNVfolding</source>
        <translation>ᠡᠪᠬᠡᠬᠦ ᠂ ᠡᠪᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="133"/>
        <source>Collate</source>
        <translation>ᠬᠤᠪᠢ ᠳᠠᠷᠠᠭᠠᠯᠠᠨ ᠬᠡᠪᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="134"/>
        <source>GraphicColorMatching</source>
        <translation>ᠥᠩᠭᠡ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ (᠎ᠵᠢᠷᠤᠭ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="135"/>
        <source>PhotoColorMatching</source>
        <translation>ᠥᠩᠭᠡ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="136"/>
        <source>TextColorMatching</source>
        <translation>ᠥᠩᠭᠡ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ (᠎ᠲᠧᠺᠰᠲ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="137"/>
        <source>OriginalImageType</source>
        <translation>ᠥᠩᠭᠡᠶᠢᠨ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="138"/>
        <source>Colorimetric</source>
        <translation>ᠥᠩᠭᠡᠨ ᠵᠥᠷᠢᠶ᠎ᠡ ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="139"/>
        <source>ColorTone</source>
        <translation>ᠥᠩᠭᠡ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="140"/>
        <source>Combination</source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠨᠢᠭᠤᠷ ᠬᠤᠪᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="141"/>
        <source>CopyGuard</source>
        <translation>ᠳᠠᠷᠤᠮᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠰᠡᠷᠭᠡᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="142"/>
        <source>CopyProtect</source>
        <translation>ᠳᠠᠷᠤᠮᠯᠠᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="143"/>
        <source>KMCopySecurityEnable</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠭᠤᠷ᠎ᠤ᠋ᠨ ᠠᠶᠤᠯ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="144"/>
        <source>KMCopySecurityMode</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠭᠤᠷ᠎ᠤ᠋ᠨ ᠠᠶᠤᠯᠭᠦᠢ᠎ᠶ᠋ᠢᠨ ᠮᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="145"/>
        <source>CoverMode</source>
        <translation>ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="146"/>
        <source>UserCustomType1</source>
        <translation>ᠥᠪᠡᠰᠦᠪᠡᠨ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠬᠤ 1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="147"/>
        <source>Cyan</source>
        <translation>ᠵᠠᠯᠠᠭᠤᠴᠤᠳ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="148"/>
        <source>Dark</source>
        <translation>ᠬᠠᠷᠠᠩᠭᠤᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="149"/>
        <source>KMCopySecurityDateTime</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠣᠯᠤᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="150"/>
        <source>KMCopySecurityDateFormat</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="151"/>
        <source>KMCopySecurityDCNStart</source>
        <translation>ᠡᠬᠢᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="152"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="460"/>
        <source>Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠂ ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="153"/>
        <source>KMDepCode</source>
        <translation>ᠰᠠᠯᠠᠭ᠎ᠠ ᠮᠥᠴᠢᠷ᠎ᠦ᠋ᠨ ᠳ᠋ᠤᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="154"/>
        <source>CNDetectPaperSize</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="155"/>
        <source>KMCopySecurityDCNumber</source>
        <translation>ᠲᠠᠷᠬᠠᠯᠲᠠ ᠡᠵᠡᠮᠰᠢᠯᠲᠦ ᠳ᠋ᠤᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="156"/>
        <source>Document</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="157"/>
        <source>KMEncryption</source>
        <translation>ᠬᠥᠳᠡᠯᠭᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="159"/>
        <source>DuplexNoTumble</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="160"/>
        <source>DuplexTumble</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ (᠎ᠣᠬᠣᠷ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="161"/>
        <source>String4Pt</source>
        <translation>ᠬᠥᠪᠡᠭᠡ ᠵᠠᠬ᠎ᠠ ᠴᠢᠩᠭᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="162"/>
        <source>EFFECT2</source>
        <translation>ᠭᠦᠳᠦᠭᠦᠷ ᠰᠡᠢᠯᠦᠮᠡᠯ᠎ᠦ᠋ᠨ ᠠᠷᠤ ᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="163"/>
        <source>EFFECT1</source>
        <translation>ᠬᠥᠲᠥᠬᠡᠷ ᠰᠡᠢᠯᠦᠮᠡᠯ ᠪᠢᠴᠢᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="164"/>
        <source>KMEncPass</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="165"/>
        <source>Env2</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠳᠤᠭᠲᠤᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="166"/>
        <source>TextGraphic</source>
        <translation>ᠲᠧᠺᠰᠲ / ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="167"/>
        <source>KMBoxFileName</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="168"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="520"/>
        <source>Finisher</source>
        <translation>ᠴᠠᠭᠠᠰᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="169"/>
        <source>Finishing</source>
        <translation>ᠵᠠᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="170"/>
        <source>Fold</source>
        <translation>ᠴᠠᠭᠠᠰᠤ ᠨᠤᠭᠤᠯᠵᠤ ᠪᠣᠳᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="171"/>
        <source>COMPOSITION1</source>
        <translation>ᠵᠥᠪ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢ ᠪᠦᠷᠬᠦᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="172"/>
        <source>FrontCoverPage</source>
        <translation>ᠭᠠᠳᠠᠷ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="173"/>
        <source>FrontCoverTray</source>
        <translation>ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="174"/>
        <source>FullColor</source>
        <translation>ᠴᠢᠤᠸᠠᠨ ᠼᠠᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="175"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="409"/>
        <source>General</source>
        <translation>ᠦᠨᠳᠦᠰᠦᠨ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="176"/>
        <source>GlossyMode</source>
        <translation>ᠭᠡᠷᠡᠯ ᠭᠢᠯᠪᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠪ ᠮᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="177"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="537"/>
        <source>Gradation</source>
        <translation>ᠠᠯᠭᠤᠷ ᠬᠤᠪᠢᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="178"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="476"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="179"/>
        <source>Group</source>
        <translation>ᠬᠤᠪᠢ ᠬᠤᠪᠢᠶᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="180"/>
        <source>HalfFold</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ ᠂ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="181"/>
        <source>PrinterHDD</source>
        <translation>ᠬᠠᠲᠠᠭᠤ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="182"/>
        <source>ThickPaper</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="183"/>
        <source>ThickPaperH</source>
        <translation>ᠬᠦᠨᠳᠦ ᠳᠡᠰ᠎ᠦ᠋ᠨ ᠪᠣᠯᠣᠳ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="184"/>
        <source>HEAVY1</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠴᠠᠭ1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="185"/>
        <source>HEAVY2</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠴᠠᠭ2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="186"/>
        <source>HEAVY3</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠴᠠᠭ3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="187"/>
        <source>HEAVY4</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠴᠠᠭ4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="188"/>
        <source>Quality2</source>
        <translation>ᠴᠢᠨᠠᠷ ᠰᠠᠢᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="189"/>
        <source>HighResolution</source>
        <translation>ᠥᠨᠳᠥᠷ ᠢᠯᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="190"/>
        <source>Speed</source>
        <translation>ᠥᠨᠳᠥᠷ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="191"/>
        <source>FFPunch</source>
        <translation>ᠨᠦᠬᠡ ᠴᠣᠭᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="192"/>
        <source>IDPrint</source>
        <translation>ID&amp;ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="193"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="426"/>
        <source>Quality</source>
        <translation>ᠶᠠᠰᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="194"/>
        <source>FFOutputMode</source>
        <translation>ᠢᠮᠡᠭᠸ ᠵᠢᠷᠤᠭ᠎ᠤ᠋ᠨ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="195"/>
        <source>CNSuperSmooth</source>
        <translation>ᠢᠮᠡᠬᠧ ᠵᠢᠷᠤᠭ᠎ᠢ᠋ ᠨᠠᠷᠢᠯᠢᠭᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="196"/>
        <source>InstallableOptions</source>
        <translation>ᠰᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤᠢ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="197"/>
        <source>HDD</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠰᠠᠭᠤᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="198"/>
        <source>OHPOpTray</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤᠯᠢᠭ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="199"/>
        <source>KMCopySecurityJobNumber</source>
        <translation>ᠠᠵᠢᠯ᠎ᠤ᠋ᠨ ᠳ᠋ᠤᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="200"/>
        <source>LARGE</source>
        <translation>ᠲᠣᠮᠣ ᠲᠣᠮᠣ ᠲᠣᠮᠣ ᠲᠣᠮᠣ ᠲᠣᠮᠣ ᠲᠣᠮᠣ ᠲᠣᠮᠣ ᠡᠷᠦᠬᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="201"/>
        <source>FFLayout</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="202"/>
        <source>Left</source>
        <translation>ᠵᠡᠭᠦᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="203"/>
        <source>LeftBinding</source>
        <translation>ᠵᠡᠭᠦᠨ ᠬᠤᠪᠴᠠᠰᠤ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="204"/>
        <source>1Staple(Left)</source>
        <translation>ᠵᠡᠭᠦᠨ ᠥᠨᠴᠥᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="205"/>
        <source>LHEAD</source>
        <translation>ᠲᠤᠰᠬᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="206"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="444"/>
        <source>Light</source>
        <translation>ᠰᠠᠷᠠᠭᠤᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="207"/>
        <source>Magenta</source>
        <translation>ᠫᠢᠨ ᠬᠣᠩ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="208"/>
        <source>Manual</source>
        <translation>ᠭᠠᠷ ᠬᠥᠳᠡᠯᠦᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="209"/>
        <source>MediaType</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="210"/>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ ᠵᠡᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="211"/>
        <source>Mode1</source>
        <translation>ᠮᠣᠳᠧᠯ 1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="212"/>
        <source>Mode2</source>
        <translation>ᠫᠠᠲ᠋ᠧᠨ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="213"/>
        <source>Mode3</source>
        <translation>ᠮᠣᠳ᠋ 3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="214"/>
        <source>Mode4</source>
        <translation>ᠮᠣᠳᠧᠯ 4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="215"/>
        <source>Monitor</source>
        <translation>ᠰᠡᠷᠡᠬᠦᠢ (᠎ᠠᠪᠴᠠᠯᠳᠤᠩᠭᠤᠢ ᠦᠵᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠥᠩᠭᠡ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="216"/>
        <source>mono</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠴᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="217"/>
        <source>PrinterDefault</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="218"/>
        <source>offset</source>
        <translation>ᠵᠠᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="219"/>
        <source>OHP</source>
        <translation>ᠲᠤᠩᠭᠠᠯᠠᠭ ᠹᠢᠯᠢᠮ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="220"/>
        <source>CNOutputAdjustment</source>
        <translation>ᠭᠠᠷᠭᠠᠯᠲᠠ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="221"/>
        <source>FFColorMode</source>
        <translation>ᠥᠩᠭᠡ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="222"/>
        <source>KMOutputMethod</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="223"/>
        <source>OutputBin</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="224"/>
        <source>PageRegion</source>
        <translation>ᠡᠬᠡ ᠪᠢᠴᠢᠭ᠎ᠦ᠋ᠨ ᠴᠠᠭᠠᠰᠤ᠎ᠶ᠋ᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="225"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="527"/>
        <source>PageSize</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="226"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="528"/>
        <source>InputSlot</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠢᠷᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="227"/>
        <source>PaperSources</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠢᠷᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="228"/>
        <source>KMAccPass</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="229"/>
        <source>PasswordCopy</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="230"/>
        <source>KMCopySecurityPass</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠲᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="231"/>
        <source>KMCopySecurityPatternAngle</source>
        <translation>ᠥᠨᠴᠥᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="232"/>
        <source>KMCopySecurityPatternColor</source>
        <translation>ᠶᠠᠩᠵᠤ᠎ᠶ᠋ᠢᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="233"/>
        <source>KMCopySecurityPatternContrast</source>
        <translation>ᠲᠣᠳᠣᠴᠠ ᠂ ᠲᠣᠳᠣᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="234"/>
        <source>KMCopySecurityPatternDensity</source>
        <translation>ᠥᠳᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="235"/>
        <source>KMCopySecurityPatternEmboss</source>
        <translation>ᠬᠥᠲᠥᠬᠡᠷ ᠰᠡᠢᠯᠦᠮᠡᠯ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="236"/>
        <source>KMCopySecurityPatternOverwrite</source>
        <translation>ᠶᠠᠩᠵᠤ ᠪᠦᠷᠬᠦᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="237"/>
        <source>KMCopySecurityPatternTextSize</source>
        <translation>ᠲᠧᠺᠰᠲ᠎ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="238"/>
        <source>Photo</source>
        <translation>ᠰᠡᠭᠦᠳᠡᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="239"/>
        <source>Photographic</source>
        <translation>ᠰᠡᠷᠡᠬᠦᠢ (᠎ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="240"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="448"/>
        <source>Plain</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="241"/>
        <source>PlainPaper</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="242"/>
        <source>PlainLPaper</source>
        <translation>ᠨᠢᠮᠭᠡᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="243"/>
        <source>Plain(2nd)</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="244"/>
        <source>PlainPaper1</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠭ11</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="245"/>
        <source>PlainPaper2</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠭ2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="246"/>
        <source>PlainPaperL</source>
        <translation>ᠨᠢᠮᠭᠡᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="247"/>
        <source>print</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="248"/>
        <source>KMStampPNPrintPosition</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="249"/>
        <source>KMDuplex</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="250"/>
        <source>ProofMode</source>
        <translation>ᠬᠠᠷᠭᠤᠭᠤᠯᠬᠤ ᠶᠠᠩᠵᠤ ᠳᠠᠷᠤᠮᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="251"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="535"/>
        <source>Punch</source>
        <translation>ᠨᠦᠬᠡ ᠴᠣᠭᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="252"/>
        <source>KOPunch</source>
        <translation>ᠨᠦᠬᠡ ᠴᠣᠭᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="253"/>
        <source>PUNF2</source>
        <translation>2 ᠺᠦᠩ (᠎ᠹᠷᠠᠨᠼᠢ ᠬᠡᠯᠡ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="254"/>
        <source>PUNF4</source>
        <translation>4 ᠺᠦᠩ (᠎ᠹᠷᠠᠨᠼᠢ ᠬᠡᠯᠡ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="255"/>
        <source>PUNS4</source>
        <translation>4 ᠺᠦᠩ (᠎ᠰᠸᠧᠳᠧᠨ ᠬᠡᠯᠡ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="256"/>
        <source>PUNU2</source>
        <translation>2 ᠺᠦᠩ (᠎ᠹᠷᠠᠨᠼᠢ ᠬᠡᠯᠡ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="257"/>
        <source>PUNU23</source>
        <translation>3 ᠨᠦᠬᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="258"/>
        <source>PhotoPureBlack</source>
        <translation>ᠴᠣᠯ ᠬᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="259"/>
        <source>TextPureBlack</source>
        <translation>ᠴᠣᠯ ᠬᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="260"/>
        <source>Recycled(2nd)</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠭᠦᠰᠬᠦ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="261"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="475"/>
        <source>Red</source>
        <translation>ᠤᠯᠠᠭᠠᠨ ᠤᠯᠠᠭᠠᠨ ᠤᠯᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="263"/>
        <source>RIGHTCENTER</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="264"/>
        <source>RightBinding</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠬᠤᠪᠴᠠᠰᠤ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="265"/>
        <source>1Staple(Right)</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠥᠨᠴᠥᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="266"/>
        <source>CNRotatePrint</source>
        <translation>180°ᠡᠷᠭᠢᠵᠦ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="267"/>
        <source>rotation</source>
        <translation>ᠡᠷᠭᠢᠯᠳᠦᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="268"/>
        <source>KMSafeQUser</source>
        <translation>SafeQ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="269"/>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="270"/>
        <source>BoxPrint</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨBOXᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="271"/>
        <source>Box</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨBOXᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="272"/>
        <source>GraphicScreen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ (᠎ᠵᠢᠷᠤᠭ ᠬᠦᠰᠦᠨᠦᠭ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="273"/>
        <source>PhotoScreen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ (᠎ᠰᠡᠭᠦᠳᠡᠷ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="274"/>
        <source>TextScreen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ (᠎ᠲᠧᠺᠰᠲ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="275"/>
        <source>Secure</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠫᠷᠢᠨᠲ᠋ᠸᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="276"/>
        <source>KMSecID</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ID</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="277"/>
        <source>KMSecPass</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="278"/>
        <source>secured</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠫᠷᠢᠨᠲ᠋ᠸᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="279"/>
        <source>SelectColor</source>
        <translation>ᠥᠩᠭᠡ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="280"/>
        <source>KMCopySecuritySerialNumber</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="281"/>
        <source>SideDeck</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ 5</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="282"/>
        <source>SingleSidedOnly</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠭᠠᠭᠴᠠ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="283"/>
        <source>SMALL</source>
        <translation>ᠪᠠᠭ᠎ᠠ ᠪᠠᠭ᠎ᠠ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="284"/>
        <source>Smooth1</source>
        <translation>1. ᠭᠤᠯᠭᠤᠮᠠᠲᠠᠭᠠᠢ 1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="285"/>
        <source>Smooth2</source>
        <translation>ᠭᠤᠯᠭᠤᠭᠴᠢ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="286"/>
        <source>GraphicSmoothing</source>
        <translation>ᠭᠢᠯᠦᠭᠡᠷ (᠎ᠵᠢᠷᠤᠭ ᠬᠦᠰᠦᠨᠦᠭ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="287"/>
        <source>PhotoSmoothing</source>
        <translation>ᠭᠢᠯᠦᠭᠡᠷ (᠎ᠰᠡᠭᠦᠳᠡᠷ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="288"/>
        <source>Special</source>
        <translation>ᠣᠨᠴᠠᠭᠠᠢ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="289"/>
        <source>CNSpecialPrintMode</source>
        <translation>ᠣᠨᠴᠠᠭᠠᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠠᠩᠵᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="290"/>
        <source>StampRepeat</source>
        <translation>ᠲᠠᠮᠠᠭᠠᠯᠠᠯ ᠳᠠᠪᠲᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="291"/>
        <source>Standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="292"/>
        <source>FFStaple</source>
        <translation>ᠦᠳᠡᠬᠦ ᠂ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="293"/>
        <source>StapleCollate</source>
        <translation>ᠳᠡᠪᠲᠡᠷᠯᠡᠬᠦ ᠂ ᠠᠯᠬᠤᠮ ᠳᠠᠷᠠᠭ᠎ᠠ᠎ᠪᠠᠷ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="294"/>
        <source>StapleGroup</source>
        <translation>ᠲᠣᠭᠣᠨᠣ᠎ᠲᠠᠢ ᠬᠠᠪᠢᠳᠠᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="295"/>
        <source>StapleLocation</source>
        <translation>ᠲᠣᠪᠯᠠᠬᠤ ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="296"/>
        <source>store</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="297"/>
        <source>TAB</source>
        <translation>ᠱᠣᠰᠢᠭ᠎ᠠ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="298"/>
        <source>Ledger</source>
        <translation>ᠵᠢᠵᠢᠭ ᠰᠣᠨᠢᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="299"/>
        <source>Text</source>
        <translation>ᠲᠧᠺᠰᠲ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="300"/>
        <source>KMStampPNTextColor</source>
        <translation>ᠲᠧᠺᠰᠲ᠎ᠦ᠋ᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="301"/>
        <source>KMCopySecurityTimeFormat</source>
        <translation>ᠴᠠᠭ᠎ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="302"/>
        <source>CNTonerDensity</source>
        <translation>ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠲᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="303"/>
        <source>TonerSave</source>
        <translation>ᠪᠣᠳᠣᠭ ᠠᠷᠪᠢᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠠᠷᠪᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="304"/>
        <source>Top</source>
        <translation>ᠣᠷᠣᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="305"/>
        <source>TopBinding</source>
        <translation>ᠱᠠᠩ ᠵᠤᠸᠠᠩ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="306"/>
        <source>UpperLeftSingle</source>
        <translation>ᠵᠡᠭᠦᠨ ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="307"/>
        <source>RIGHTTOP</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="308"/>
        <source>TopLeft</source>
        <translation>ᠵᠡᠭᠦᠨ ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="309"/>
        <source>TopRight</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="310"/>
        <source>TRACING</source>
        <translation>ᠵᠢᠷᠤᠭ ᠠᠷᠢᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="311"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="446"/>
        <source>Transparency</source>
        <translation>ᠲᠤᠩᠭᠠᠯᠠᠭ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="312"/>
        <source>TransparencyInterleave</source>
        <translation>OHP ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="313"/>
        <source>Tray2</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="314"/>
        <source>Tray3</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="315"/>
        <source>Tray4</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="316"/>
        <source>TrayA</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="317"/>
        <source>TrayB</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ B</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="318"/>
        <source>TrayC</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠢᠯᠠ C</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="319"/>
        <source>TriFold</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠨᠤᠭᠤᠯᠭᠠᠲᠤ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="320"/>
        <source>TwoColors</source>
        <translation>ᠬᠣᠣᠰ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="321"/>
        <source>Printer</source>
        <translation>ᠬᠡᠪᠯᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠠᠶᠠᠳᠠᠯ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="322"/>
        <source>CNSkipBlank</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠮᠣᠳᠧᠯ᠎ᠢ᠋ ᠦᠰᠦᠷᠦᠨ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="323"/>
        <source>KMAuthentication</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="324"/>
        <source>KMAuthUser</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="325"/>
        <source>Vivid</source>
        <translation>ᠰᠣᠷᠭᠣᠭ ᠰᠠᠢᠬᠠᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="326"/>
        <source>Vividphoto</source>
        <translation>ᠰᠢᠶᠠᠩ ᠶᠠᠨ ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="327"/>
        <source>Yellow</source>
        <translation>ᠰᠢᠷ᠎ᠠ ᠰᠢᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="328"/>
        <source>KMDriverEncryption</source>
        <translation>ᠬᠡᠪᠯᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="329"/>
        <source>ColorModel</source>
        <translation>ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="330"/>
        <source>CMY</source>
        <translation>ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="331"/>
        <source>Gray</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠴᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="332"/>
        <source>UTDuplex</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="333"/>
        <source>0Front</source>
        <translation>ᠣᠢᠷᠠᠯᠴᠠᠭ᠎ᠠ ᠵᠥᠪ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="334"/>
        <source>1Both</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="335"/>
        <source>2Back</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠠᠷᠤ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="336"/>
        <source>CardFrontGroup</source>
        <translation>ᠺᠠᠷᠲ᠎ᠤ᠋ᠨ ᠡᠮᠦᠨ᠎ᠡ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="337"/>
        <source>CFColourFormat</source>
        <translation>ᠺᠠᠷᠲ᠎ᠤ᠋ᠨ ᠡᠮᠦᠨ᠎ᠡ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠹᠤᠷᠮᠠᠲ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="338"/>
        <source>Sharpness</source>
        <translation>ᠬᠤᠷᠴᠠ ᠰᠢᠷᠦᠭᠦᠨ᠎ᠢ᠋ᠶ᠋ᠡᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="339"/>
        <source>0ResinQuality</source>
        <translation>ᠥᠨᠳᠥᠷ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="340"/>
        <source>1ResinQuality</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠂ ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="341"/>
        <source>2ResinQuality</source>
        <translation>ᠴᠢᠨᠠᠷ ᠰᠠᠢᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="342"/>
        <source>ColourCorrection</source>
        <translation>ᠥᠩᠭᠡ ᠵᠠᠯᠠᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="343"/>
        <source>0ColourCorrection</source>
        <translation>ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="344"/>
        <source>1ColourCorrection</source>
        <translation>ᠬᠥᠳᠡᠯᠭᠡᠬᠦ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="345"/>
        <source>2ColourCorrection</source>
        <translation>ᠰᠡᠷᠡᠬᠦᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="346"/>
        <source>3ColourCorrection</source>
        <translation>ᠬᠠᠨᠤᠴᠠ᠎ᠶ᠋ᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="347"/>
        <source>4ColourCorrection</source>
        <translation>ᠬᠠᠷᠢᠴᠠᠩᠭᠤᠢ ᠥᠩᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="348"/>
        <source>5ColourCorrection</source>
        <translation>ᠬᠠᠷᠢᠴᠠᠯᠠᠰᠢᠭᠦᠢ ᠥᠩᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="349"/>
        <source>Postcard</source>
        <translation>ᠢᠯᠡ ᠵᠠᠬᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="350"/>
        <source>DoublePostcardRotated</source>
        <translation>ᠢᠯᠡ ᠵᠠᠬᠢᠳᠠᠯ 2 (᠎ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="351"/>
        <source>EnvYou4</source>
        <translation>Y4 ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠣᠭᠳᠣᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="352"/>
        <source>BrMediaType</source>
        <translation>ᠵᠠᠭᠤᠴᠢ᠎ᠶ᠋ᠢᠨ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="353"/>
        <source>THIN</source>
        <translation>ᠨᠢᠮᠭᠡᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="354"/>
        <source>THICKERPAPER2</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="355"/>
        <source>ENV</source>
        <translation>ᠲᠣᠭᠳᠣᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="356"/>
        <source>ENVTHICK</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠣᠭᠳᠣᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="357"/>
        <source>ENVTHIN</source>
        <translation>ᠨᠢᠮᠭᠡᠨ ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠣᠭᠳᠣᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="358"/>
        <source>GraphicsOptions</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="359"/>
        <source>SpeedOptions</source>
        <translation>ᠤᠳᠠᠭᠠᠰᠢᠷᠠᠬᠤ ᠮᠣᠳᠧᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="360"/>
        <source>MediaOptions</source>
        <translation>ᠵᠠᠭᠤᠴᠢ᠎ᠶ᠋ᠢᠨ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="361"/>
        <source>PrnDef</source>
        <translation>ᠠᠦᠲᠣᠴᠢᠯᠠᠭᠰᠠᠨ ᠣᠷᠴᠢᠨ᠎ᠤ᠋ ᠰᠢᠯᠢᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="362"/>
        <source>Cardstock</source>
        <translation>ᠺᠠᠷᠲ᠎ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="363"/>
        <source>Highqlty</source>
        <translation>ᠴᠢᠨᠠᠷ ᠰᠠᠢᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="364"/>
        <source>OptionTray</source>
        <translation>ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠬᠤ ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="365"/>
        <source>1Cassette</source>
        <translation>ᠳᠠᠩ ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="366"/>
        <source>LargeCapacityTray</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭᠲᠠᠭᠠᠮᠵᠢᠲᠤ ᠴᠠᠭᠠᠰᠤᠨ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="367"/>
        <source>NotInstalled</source>
        <translation>ᠰᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="368"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="526"/>
        <source>Installed</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠰᠠᠭᠤᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="369"/>
        <source>MultiTray</source>
        <translation>ᠭᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠬᠦᠷᠭᠡᠬᠦ ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="370"/>
        <source>1Tray</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="371"/>
        <source>2Tray</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="372"/>
        <source>3Tray</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="373"/>
        <source>4Tray</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠫᠠᠨᠰᠠ 4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="374"/>
        <source>5Tray</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠲᠠᠪᠠᠭ 5</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="375"/>
        <source>RIPaperPolicy</source>
        <translation>ᠴᠠᠭᠠᠰᠤ ᠲᠣᠬᠢᠷᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠲᠣᠬᠢᠷᠠᠮᠵᠢᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="376"/>
        <source>CutSheetFeeder</source>
        <translation>ᠴᠠᠭᠠᠰᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="377"/>
        <source>Tractor</source>
        <translation>ᠴᠠᠭᠠᠰᠤ ᠴᠢᠷᠦᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="378"/>
        <source>RollPaper</source>
        <translation>ᠣᠷᠢᠶᠠᠮᠠᠯ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="379"/>
        <source>EPSpeed</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="380"/>
        <source>High</source>
        <translation>ᠥᠨᠳᠥᠷ ᠬᠤᠷᠳᠤᠴᠠᠲᠤ ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="381"/>
        <source>Std</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="382"/>
        <source>EPPrintDirection</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="383"/>
        <source>UniOff</source>
        <translation>ᠬᠣᠣᠰ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="384"/>
        <source>UniOn</source>
        <translation>ᠳᠠᠩ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="385"/>
        <source>EPSvRP</source>
        <translation>ᠡᠪᠬᠡᠮᠡᠯ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="386"/>
        <source>EPTearOff</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ ᠴᠠᠭᠠᠰᠤ ᠬᠡᠷᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="387"/>
        <source>EPOffsetHrz</source>
        <translation>ᠬᠡᠪᠲᠡᠭᠡ ᠬᠠᠵᠠᠢᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="388"/>
        <source>EPTopMP</source>
        <translation>ᠳᠡᠭᠡᠳᠦ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠲᠡᠷᠢᠭᠦᠯᠡᠬᠦ ᠳᠡᠰ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="389"/>
        <source>Driver</source>
        <translation>ᠬᠥᠳᠡᠯᠭᠡᠬᠦ ᠫᠷᠦᠭᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="390"/>
        <source>EPPackMode</source>
        <translation>ᠪᠣᠭᠣᠳᠠᠯᠯᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="391"/>
        <source>RGB</source>
        <translation>ᠬᠣᠯᠢᠮᠠᠭ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="392"/>
        <source>PrinterOptions</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="393"/>
        <source>MediaMethod</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="394"/>
        <source>PaperType</source>
        <translation>ᠳᠠᠷᠤᠮᠯᠠᠬᠤ ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="395"/>
        <source>PrintSpeed</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="396"/>
        <source>PrintDarkness</source>
        <translation>ᠥᠳᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="397"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="513"/>
        <source>None</source>
        <translation>ᠣᠳᠣᠬᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="398"/>
        <source>PostAction</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠬᠣᠢᠨᠠᠬᠢ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="399"/>
        <source>FowardOffset</source>
        <translation>ᠬᠡᠪᠯᠡᠭᠦᠯᠦᠭᠡ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="400"/>
        <source>SpecifiedPages</source>
        <translation>ᠵᠠᠪᠰᠠᠷ᠎ᠤ᠋ᠨ ᠲᠣᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="401"/>
        <source>effects</source>
        <translation>ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="402"/>
        <source>MirrorImage</source>
        <translation>ᠲᠣᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="403"/>
        <source>NegativeImage</source>
        <translation>ᠰᠥᠭᠡᠷᠭᠦ ᠹᠢᠯᠢᠮ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="404"/>
        <source>Orientation</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="405"/>
        <source>Compression</source>
        <translation>ᠠᠪᠴᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="406"/>
        <source>DataCompress</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠠᠪᠴᠢᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="407"/>
        <source>ColorOption</source>
        <translation>ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="408"/>
        <source>THICK</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="410"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="605"/>
        <source>Draft</source>
        <translation>ᠡᠬᠡ ᠪᠢᠴᠢᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="411"/>
        <source>Normal</source>
        <translation>ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="412"/>
        <source>Normal Color</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="413"/>
        <source>Normal Grayscale</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠢᠷᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="414"/>
        <source>Draft Color</source>
        <translation>ᠬᠥᠪᠥᠮᠡᠯ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="415"/>
        <source>Draft Grayscale</source>
        <translation>ᠬᠥᠪᠬᠦ ᠴᠠᠢᠷᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="416"/>
        <source>Best</source>
        <translation>ᠬᠠᠮᠤᠭ ᠰᠠᠢᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="417"/>
        <source>High-Resolution Photo</source>
        <translation>ᠥᠨᠳᠥᠷ ᠢᠯᠭᠠᠮᠵᠢ ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="418"/>
        <source>Fast Draft</source>
        <translation>ᠲᠦᠷᠭᠡᠨ ᠬᠥᠪᠦᠨ ᠰᠢᠯᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="419"/>
        <source>Installed Cartridges</source>
        <translation>ᠰᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="420"/>
        <source>Photo Only</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠰᠡᠭᠦᠳᠡᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="421"/>
        <source>Black and TriColor</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠪᠠ ᠭᠤᠷᠪᠠᠨ ᠡᠬᠢᠲᠦ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="422"/>
        <source>Photo and TriColor</source>
        <translation>ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ ᠪᠠ ᠭᠤᠷᠪᠠᠨ ᠤᠤᠯ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="424"/>
        <source>Color Mode</source>
        <translation>ᠥᠩᠭᠡᠲᠦ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="425"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="599"/>
        <source>Black Only Grayscale</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠪᠠ ᠴᠠᠢᠷᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="427"/>
        <source>High Resolution </source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠢᠯᠭᠠᠮᠵᠢ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="428"/>
        <source>Auto Source</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ ᠡᠬᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="429"/>
        <source>Manual Feed</source>
        <translation>ᠭᠠᠷ ᠴᠠᠭᠠᠰᠤ ᠬᠦᠷᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="430"/>
        <source>Middle Tray</source>
        <translation>ᠳᠤᠮᠳᠠᠬᠢ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="431"/>
        <source>Upper or Only One InputSlot</source>
        <translation>ᠳᠡᠭᠡᠳᠦ ᠬᠡᠰᠡᠭ ᠪᠤᠶᠤ ᠴᠣᠷ᠎ᠤ᠋ᠨ ᠭᠠᠭᠴᠠ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠬᠣᠪᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="432"/>
        <source>Multi-purpose Tray</source>
        <translation>ᠣᠯᠠᠨ ᠬᠡᠷᠡᠭᠴᠡᠭᠡᠲᠦ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="433"/>
        <source>Drawer 1 </source>
        <translation>ᠰᠢᠷᠭᠤᠯ 1 </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="434"/>
        <source>Drawer 2 </source>
        <translation>ᠰᠢᠷᠭᠤᠯ 2 </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="435"/>
        <source>Tray 1</source>
        <translation>ᠲᠠᠪᠠᠭ᠎ᠤ᠋ᠨ 1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="436"/>
        <source>Auto Select</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ ᠰᠢᠯᠢᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="442"/>
        <source>Standard Paper</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="443"/>
        <source>Heavy</source>
        <translation>ᠴᠢᠬᠤᠯᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="447"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="576"/>
        <source>Plain Paper</source>
        <translation>ᠡᠩ᠎ᠦ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="449"/>
        <source>Transparency Film</source>
        <translation>ᠲᠤᠩᠭᠠᠯᠠᠭ ᠹᠢᠯᠢᠮ᠎ᠦ᠋ᠨ ᠣᠷᠢᠶᠠᠮᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="450"/>
        <source>CD or DVD Media</source>
        <translation>CD ᠪᠤᠶᠤ DVD ᠵᠠᠭᠤᠴᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="451"/>
        <source>Print Density</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠨᠢᠭᠲᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="452"/>
        <source>Extra Light (1)</source>
        <translation>ᠲᠤᠢᠯ᠎ᠤ᠋ᠨ ᠬᠥᠩᠭᠡᠨ (᠎1 )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="453"/>
        <source>Light (2)</source>
        <translation>ᠬᠥᠩᠭᠡᠨ (᠎2 )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="454"/>
        <source>Medium (3)</source>
        <translation>ᠳᠤᠮᠳᠠ ᠵᠡᠷᠭᠡ (᠎3 )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="455"/>
        <source>Dark (4)</source>
        <translation>ᠬᠠᠷᠠᠩᠭᠤᠢ (᠎4 )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="456"/>
        <source>Extra Dark (5)</source>
        <translation>ᠲᠤᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠷᠠᠩᠭᠤᠢ (᠎5 )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="457"/>
        <source>Copies</source>
        <translation>ᠬᠠᠭᠤᠯᠪᠤᠷᠢ ᠳᠡᠪᠲᠡᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="458"/>
        <source>Adjustment</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="459"/>
        <source>Halftone Algorithm</source>
        <translation>Halftone ᠪᠣᠳᠣᠬᠤ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="461"/>
        <source>Miscellaneous</source>
        <translation>ᠬᠣᠯᠢᠮᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="462"/>
        <source>N-up Orientation</source>
        <translation>ᠭᠤᠯᠳᠤ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="463"/>
        <source>N-up Printing</source>
        <translation>ᠭᠤᠯᠳᠤ ᠴᠢᠭᠯᠡᠯ᠎ᠢ᠋ᠶ᠋ᠡᠷ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="464"/>
        <source>Landscape</source>
        <translation>ᠦᠵᠡᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="465"/>
        <source>Seascape</source>
        <translation>ᠳᠠᠯᠠᠢ᠎ᠶ᠋ᠢᠨ ᠦᠵᠡᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="469"/>
        <source>Two-Sided</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="470"/>
        <source>Print Settings</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="471"/>
        <source>Color Settings</source>
        <translation>ᠥᠩᠭᠡᠲᠦ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="472"/>
        <source>Color Settings (Advanced)</source>
        <translation>ᠥᠩᠭᠡᠲᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭ᠎ᠠ (᠎ᠳᠡᠭᠡᠳᠦ ᠳᠡᠰ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="473"/>
        <source>Brightness</source>
        <translation>ᠰᠠᠷᠠᠭᠤᠯ ᠲᠤᠩᠭᠠᠯᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="474"/>
        <source>Contrast</source>
        <translation>ᠬᠠᠷᠢᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="478"/>
        <source>_Media Size</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="479"/>
        <source>_Grayscale</source>
        <translation>ᠴᠠᠢᠷᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="480"/>
        <source>_Brightness</source>
        <translation>ᠰᠠᠷᠠᠭᠤᠯ ᠲᠤᠩᠭᠠᠯᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="481"/>
        <source>_Contrast</source>
        <translation>ᠬᠠᠷᠢᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="482"/>
        <source>_Saturation</source>
        <translation>ᠴᠠᠳᠴᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="483"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="597"/>
        <source>On</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="484"/>
        <source>2-Sided Printing</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="485"/>
        <source>No</source>
        <translation>ᠡᠰᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="486"/>
        <source>Yes</source>
        <translation>ᠲᠡᠢᠮᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="487"/>
        <source>Installable Options</source>
        <translation>ᠰᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤᠢ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="488"/>
        <source>Duplexer Installed</source>
        <translation>ᠬᠣᠣᠰ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="489"/>
        <source>Color Model</source>
        <translation>ᠥᠩᠭᠡᠲᠦ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="490"/>
        <source>Color Precision</source>
        <translation>ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠷᠢᠴᠠᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="491"/>
        <source>Resolution </source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="492"/>
        <source>Printer Features Common</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠪᠠᠢᠩᠭᠤ ᠲᠣᠬᠢᠶᠠᠯᠳᠤᠳᠠᠭ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="493"/>
        <source>CD Hub Size</source>
        <translation>CD ᠭᠠᠳᠠᠷ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="494"/>
        <source>Ink Type</source>
        <translation>ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="495"/>
        <source>Toner Save </source>
        <translation>ᠪᠣᠳᠣᠭ ᠠᠷᠪᠢᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠠᠷᠪᠢᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="496"/>
        <source>ON</source>
        <translation>ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="497"/>
        <source>Media Type </source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="498"/>
        <source>Collate </source>
        <translation>ᠠᠦᠲᠣᠴᠢᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠤᠷ ᠬᠤᠪᠢᠶᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="499"/>
        <source>Image Refinement </source>
        <translation>ᠳᠦᠷᠰᠦ ᠨᠠᠷᠢᠯᠢᠭᠵᠢᠭᠤᠯᠬᠤ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="500"/>
        <source>Halftones </source>
        <translation>ᠬᠠᠭᠠᠰ ᠥᠩᠭᠡ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠬᠤ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="502"/>
        <source>OFF</source>
        <translation>ᠪᠣᠭᠣᠮᠲᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="503"/>
        <source>ON (Long-edged Binding)</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ (᠎ᠤᠷᠲᠤ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠵᠠᠬᠢᠶᠠᠯᠠᠭ᠎ᠠ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="504"/>
        <source>ON (Short-edged Binding)</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ (᠎ᠪᠣᠭᠣᠨᠢ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠵᠠᠬᠢᠶᠠᠯᠠᠭ᠎ᠠ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="505"/>
        <source>Paper Size</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="506"/>
        <source>Paper Type</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="507"/>
        <source>Thin</source>
        <translation>ᠨᠢᠮᠭᠡᠨ ᠲᠠᠴᠢᠷ ᠭᠠᠵᠠᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="508"/>
        <source>Thick</source>
        <translation>ᠵᠤᠵᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="509"/>
        <source>Thicker</source>
        <translation>ᠨᠡᠩ ᠵᠤᠵᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="510"/>
        <source>Edge Enhance</source>
        <translation>ᠬᠥᠪᠡᠭᠡ ᠵᠠᠬ᠎ᠠ ᠴᠢᠩᠭᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="511"/>
        <source>Skip Blank Pages</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠨᠢᠭᠤᠷ᠎ᠢ᠋ ᠦᠰᠦᠷᠦᠨ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="512"/>
        <source>Double-sided Printing</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="514"/>
        <source>Reverse Duplex Printing</source>
        <translation>ᠡᠰᠡᠷᠭᠦ ᠴᠢᠭᠯᠡᠯᠲᠦ ᠬᠣᠶᠠᠷ ᠲᠠᠯᠠᠲᠤ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="515"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="518"/>
        <source>Long Edge</source>
        <translation>ᠤᠷᠲᠤ ᠬᠥᠪᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="516"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="519"/>
        <source>Short Edge</source>
        <translation>ᠣᠢᠷ᠎ᠠ ᠵᠠᠬ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="517"/>
        <source>Two-sided</source>
        <translation>ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="521"/>
        <source>Option Tray</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠴᠠᠭᠠᠰᠤ ᠬᠦᠷᠭᠡᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="522"/>
        <source>External Tray</source>
        <translation>ᠭᠠᠳᠠᠭᠠᠳᠤ ᠴᠠᠭᠠᠰᠤ ᠬᠦᠷᠭᠡᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="523"/>
        <source>Internal Tray 2</source>
        <translation>ᠳᠣᠲᠣᠭᠠᠳᠤ ᠴᠠᠭᠠᠰᠤ ᠬᠦᠷᠭᠡᠬᠦᠷ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="524"/>
        <source>Internal Shift Tray</source>
        <translation>ᠳᠣᠲᠣᠭᠠᠳᠤ ᠡᠭᠡᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="525"/>
        <source>Not Installed</source>
        <translation>ᠰᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="529"/>
        <source>Tray 2</source>
        <translation>ᠲᠠᠪᠠᠭ 2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="530"/>
        <source>Tray 3</source>
        <translation>ᠲᠠᠪᠠᠭ᠎ᠤ᠋ᠨ 3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="531"/>
        <source>Tray 4</source>
        <translation>ᠲᠠᠪᠠᠭ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭ᠎ᠠ 4</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="532"/>
        <source>Bypass Tray</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠵᠠᠮ᠎ᠤ᠋ᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="533"/>
        <source>Destination</source>
        <translation>ᠵᠣᠷᠢᠭᠰᠠᠨ ᠭᠠᠵᠠᠷ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="534"/>
        <source>Staple</source>
        <translation>ᠪᠢᠴᠢᠭ᠎ᠦ᠋ᠨ ᠬᠠᠳᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="536"/>
        <source>Toner Saving</source>
        <translation>ᠪᠣᠳᠣᠭ ᠠᠷᠪᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="538"/>
        <source>Fast</source>
        <translation>ᠲᠦᠷᠭᠡᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="539"/>
        <source>Draft (Color cartridge)</source>
        <translation>ᠡᠬᠡ ᠨᠣᠣᠷᠠᠭ (᠎ᠥᠩᠭᠡᠲᠦ ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="540"/>
        <source>Normal (Color cartridge)</source>
        <translation>ᠡᠬᠡ ᠨᠣᠣᠷᠠᠭ (᠎ᠥᠩᠭᠡᠲᠦ ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="541"/>
        <source>Photo (on photo paper)</source>
        <translation>ᠰᠡᠭᠦᠳᠡᠷ (᠎ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ᠎ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="542"/>
        <source>Photo (Color cartridge, on photo paper)</source>
        <translation>ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ (᠎ᠥᠩᠭᠡᠲᠦ ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="543"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="573"/>
        <source>Custom</source>
        <translation>ᠥᠪᠡᠰᠦᠪᠡᠨ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="544"/>
        <source>Letter</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="545"/>
        <source>Photo or 4x6 inch index card</source>
        <translation>ᠰᠡᠭᠦᠳᠡᠷ ᠪᠤᠶᠤ 4x6 ᠢᠨᠴ᠎ᠦ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="546"/>
        <source>Photo or 5x7 inch index card</source>
        <translation>ᠰᠡᠭᠦᠳᠡᠷ ᠪᠤᠶᠤ 5x7 ᠢᠨᠴ᠎ᠦ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="547"/>
        <source>Photo with tear-off tab</source>
        <translation>ᠱᠤᠰᠢᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠠᠮᠠᠷᠬᠠᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="548"/>
        <source>3x5 inch index card</source>
        <translation>3x5 ᠢᠨᠴ᠎ᠦ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="549"/>
        <source>5x8 inch index card</source>
        <translation>5x8 ᠢᠨᠴ᠎ᠦ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="550"/>
        <source>A6 with tear-off tab</source>
        <translation>ᠱᠤᠰᠢᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠠᠮᠠᠷᠬᠠᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ A 6 ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="552"/>
        <source>300 dpi, Color, Color Cartr.</source>
        <translation>300dpi ᠂ ᠥᠩᠭᠡ ᠂ ᠥᠩᠭᠡᠲᠦ ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ . .</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="553"/>
        <source>300 dpi, Draft, Color, Color Cartr.</source>
        <translation>300dpi ᠡᠬᠡ ᠨᠣᠣᠷᠠᠭ ᠂ ᠥᠩᠭᠡ ᠂ ᠪᠤᠳᠤᠭᠲᠤ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="554"/>
        <source>300 dpi, Draft, Grayscale, Black Cartr.</source>
        <translation>300dpi ᠡᠬᠡ ᠨᠣᠣᠷᠠᠭ ᠂ ᠴᠠᠢᠷᠴᠠ ᠂ ᠬᠠᠷ᠎ᠠ ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="555"/>
        <source>300 dpi, Grayscale, Black Cartr.</source>
        <translation>300dpi ᠂ ᠴᠠᠢᠷᠴᠠ ᠂ ᠬᠠᠷ᠎ᠠ ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠪᠡᠬᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="557"/>
        <source>Executive 7.25x10.5in</source>
        <translation>ᠵᠠᠰᠠᠭ ᠵᠠᠬᠢᠷᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠴᠠᠭᠠᠰᠤ 7.25 x 10.5 in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="558"/>
        <source>Letter 8.5x11in</source>
        <translation>ᠠᠮᠧᠷᠢᠺᠠ᠎ᠶ᠋ᠢᠨ ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ 8.5x11in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="559"/>
        <source>Legal 8.5x14in</source>
        <translation>ᠠᠮᠧᠷᠢᠺᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠤᠯᠢ᠎ᠪᠠᠷ ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠴᠠᠭᠠᠰᠤ 8.5x14in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="560"/>
        <source>Postcard (JIS)</source>
        <translation>ᠢᠯᠡ ᠵᠠᠬᠢᠳᠠᠯ (᠎JIS )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="561"/>
        <source>Double Postcard (JIS)</source>
        <translation>ᠢᠯᠡ ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ᠎ᠶ᠋ᠢᠨ ᠬᠣᠶᠠᠷ ᠲᠠᠯ᠎ᠠ (᠎JIS )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="562"/>
        <source>Monarch Envelope 3.875x7.5in</source>
        <translation>Monarch ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠣᠭᠳᠣᠢ 3.875x7.5in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="563"/>
        <source>DL Envelope 110x220mm</source>
        <translation>DL ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠣᠭᠳᠣᠢ 110x220mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="564"/>
        <source>#10 Envelope 4.12x9.5in</source>
        <translation>10 ᠳ᠋ᠤᠭᠠᠷ ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠣᠭᠳᠣᠢ 4.12x9.5in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="565"/>
        <source>C5 Envelope 162x229mm</source>
        <translation>C5ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠳᠤᠭᠲᠤᠢ 162x229mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="566"/>
        <source>B5 Envelope 176x250mm</source>
        <translation>B5 ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠳᠤᠭᠲᠤᠢ 176x250 mm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="567"/>
        <source>C6 Envelope 114x162mm</source>
        <translation>C6 ᠵᠠᠬᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠳᠤᠭᠲᠤᠢ 114x162mml</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="568"/>
        <source>#3 Japanese Envelope 120x235mm</source>
        <translation>3 ᠶᠠᠫᠣᠨ᠎ᠤ᠋ ᠵᠠᠬᠢᠳᠠᠯ ᠲᠣᠭᠳᠣᠢ 120 x 235mm ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="569"/>
        <source>Statement 5.5x8.5in</source>
        <translation>ᠳ᠋ᠦᠩᠨᠡᠨ ᠪᠣᠳᠣᠬᠤ ᠳᠠᠩ 5.5 x8.5 in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="570"/>
        <source>Photo 4x6in</source>
        <translation>ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ 4x6in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="571"/>
        <source>Photo 5x7in</source>
        <translation>ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ 5x7in</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="572"/>
        <source>Photo 10x15cm</source>
        <translation>ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ 10x15cm</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="578"/>
        <source>Mid-Weight96-110g</source>
        <translation>ᠳᠤᠮᠳᠠ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢᠨ ᠬᠦᠨᠳᠦ ᠭᠢᠯᠠᠭᠠᠷ ᠥᠩᠭᠡᠲᠦ ᠴᠠᠭᠠᠰᠤ 96～110g</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="581"/>
        <source>Monochrome Laser Transparency</source>
        <translation>ᠳᠠᠩ ᠥᠩᠭᠡᠲᠦ ᠯᠠᠢᠰᠧᠷ ᠲᠤᠩᠭᠠᠯᠠᠭ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="593"/>
        <source>Manual Feeder</source>
        <translation>ᠭᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠴᠠᠭᠠᠰᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="601"/>
        <source>Monochrome</source>
        <translation>ᠳᠠᠩ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="604"/>
        <source>FastRes 1200</source>
        <translation>1200᠎ᠶ᠋ᠢ ᠲᠦᠷᠭᠡᠨ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
</TS>
