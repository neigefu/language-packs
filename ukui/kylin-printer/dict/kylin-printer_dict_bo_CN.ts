<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>DictionaryForChinesePrinter</name>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="10"/>
        <source>letter</source>
        <translation>ཡིག་ཤོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="11"/>
        <source>Page Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="12"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="262"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="423"/>
        <source>Resolution</source>
        <translation>དབྱེ་འབྱེད་བྱེད་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="13"/>
        <source>Paper Source</source>
        <translation>འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="14"/>
        <source>Source</source>
        <translation>འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="15"/>
        <source>PageType</source>
        <translation>ཤོག་བུའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="16"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="437"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="574"/>
        <source>Media Type</source>
        <translation>ཤོག་བུའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="17"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="439"/>
        <source>Color</source>
        <translation>ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="18"/>
        <source>Direction</source>
        <translation>དཔར་རྒྱག་པའི་ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="19"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="158"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="501"/>
        <source>Duplex</source>
        <translation>ངོས་གཉིས་དཔར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="20"/>
        <source>Pages per Side</source>
        <translation>ཤོག་གྲངས་རེ་རེ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="21"/>
        <source>Ink Rank</source>
        <translation>སྣག་ཚའི་རིམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="22"/>
        <source>Output Order</source>
        <translation>ཕྱིར་གཏོང་གི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="23"/>
        <source>legal</source>
        <translation>ཁྲིམས་ཀྱི་དོན་ཚན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="24"/>
        <source>statement</source>
        <translation>གསལ་བསྒྲགས།གསལ་བསྒྲགས་བཅས་བྱ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="25"/>
        <source>executive</source>
        <translation>སྲིད་འཛིན་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="26"/>
        <source>monarch</source>
        <translation>མདོག་རྐྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="27"/>
        <source>com10</source>
        <translation>ལྐོག་འབྲེལ་གྱི་མདོ་ཁ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="28"/>
        <source>auto</source>
        <translation>རང་འགུལ་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="29"/>
        <source>multi-purpose tray</source>
        <translation>ནུས་པ་མང་བའི་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="30"/>
        <source>drawer 1</source>
        <translation>ཤོག་བུའི་སྒམ་1།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="31"/>
        <source>drawer 2</source>
        <translation>ཤོག་སྒམ་2བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="32"/>
        <source>plain paper</source>
        <translation>ཤོག་བུ་དཀྱུས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="33"/>
        <source>left</source>
        <translation>གཡོན་ལོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="34"/>
        <source>right</source>
        <translation>གཡས་ལོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="35"/>
        <source>top</source>
        <translation>རྩེ་མོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="36"/>
        <source>bindingedge</source>
        <translation>རྒྱན་སྤྲས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="37"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="467"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="598"/>
        <source>Output Mode</source>
        <translation>ཕྱིར་གཏོང་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="38"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="468"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="600"/>
        <source>Grayscale</source>
        <translation>ཐལ་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="39"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="595"/>
        <source>EconoMode</source>
        <translation>དཔལ་འབྱོར་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="40"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="551"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="596"/>
        <source>Off</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="41"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="592"/>
        <source>Media Source</source>
        <translation>ཤོག་བུའི་འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="42"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="594"/>
        <source>Tray1</source>
        <translation>ཤོག་བུ་སྡེར་མ་1བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="43"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="575"/>
        <source>Unspecified</source>
        <translation>དམིགས་འཛུགས་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="44"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="577"/>
        <source>Light 60-74g</source>
        <translation>ཤོག་བུ་ཡང་མོ་60-74gཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="45"/>
        <source>Mid-Weight 96-110g</source>
        <translation>འབྲིང་རིམ་གྱི་ལྗིད་ཚད་ཤོག་བུ་96-110Gབཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="46"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="579"/>
        <source>Heavy 111-130g</source>
        <translation>ཤོག་བུ་ལྗིད་ཚད་111-130gཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="47"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="580"/>
        <source>Extra Heavy 131-175g</source>
        <translation>ལྗིད་ཚད་ལས་བརྒལ་བའི་ཤོག་བུ་131-175Gཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="48"/>
        <source>Monochrome Laster Transparency</source>
        <translation>ཁ་དོག་སྣ་རྐྱང་གི་སྐུལ་འོད་གསལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="49"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="441"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="582"/>
        <source>Labels</source>
        <translation>ཡིག་བྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="50"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="583"/>
        <source>Letterhead</source>
        <translation>ཡིག་ཐོག་ཤོག་བུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="51"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="440"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="584"/>
        <source>Envelope</source>
        <translation>ཡིག་ཤུབས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="52"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="585"/>
        <source>Preprinted</source>
        <translation>སྔོན་ཚུད་ནས་ཤོག་བུ་དཔར་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="53"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="586"/>
        <source>Prepunched</source>
        <translation>སྔོན་ཚུད་ནས་ཁུང་བུ་བརྟོལ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="54"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="587"/>
        <source>Colored</source>
        <translation>ཤོག་བུ་ཚོན་མདངས་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="55"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="438"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="588"/>
        <source>Bond</source>
        <translation>དངུལ་འཛིན་ཤོག་བུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="56"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="445"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="589"/>
        <source>Recycled</source>
        <translation>ཤོག་བུ་བསྐྱར་དུ་བཙེམ་པ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="57"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="590"/>
        <source>Rough</source>
        <translation>ཤོག་བུ་རྩུབ་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="58"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="591"/>
        <source>Vellum</source>
        <translation>ལུག་གི་ཤོག་བུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="59"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="602"/>
        <source>Print Quality</source>
        <translation>ཡིག་དཔར་རྒྱག་པའི་སྤུས་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="60"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="603"/>
        <source>FastRes 600</source>
        <translation>མགྱོགས་མྱུར་གྱིས་ཡིག་དཔར་600བཅས་བརྒྱབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="61"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="466"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="556"/>
        <source>Media Size</source>
        <translation>ཤོག་བུ་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="62"/>
        <source>1SIDECOATED1</source>
        <translation>ཤོག་བུ་ཁེབས་པའི་ཤོག་བུ་རྐྱང་པ་གཅིག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="63"/>
        <source>1SIDECOATED2</source>
        <translation>ཤོག་བུ་ཁེབས་པའི་ཤོག་བུ་རྐྱང་པ་གཉིས།།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="64"/>
        <source>1Sided</source>
        <translation>ཕྱོགས་གཅིག་ཁོ་ན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="65"/>
        <source>2up</source>
        <translation>གཉིས་ནས་གཅིག་ཏུ་བསྡུ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="66"/>
        <source>2Staples</source>
        <translation>གཟེར་མ་2</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="67"/>
        <source>LeftDouble</source>
        <translation>(གཡོན།)གཟེར་མ། (གཡོན།)།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="68"/>
        <source>TopDouble</source>
        <translation>(གོང་དུ་བཤད་པ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="69"/>
        <source>2holes</source>
        <translation>ཁུང་བུ་2བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="70"/>
        <source>2SIDECOATED1</source>
        <translation>ཤོག་བུ་ཉིས་བརྩེགས་གཅིག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="71"/>
        <source>2SIDECOATED2</source>
        <translation>ཤོག་བུ་ཉིས་བརྩེགས་གཉིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="72"/>
        <source>2Sided</source>
        <translation>ངོས་གཉིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="73"/>
        <source>3holes</source>
        <translation>ཁུང་བུ་3ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="74"/>
        <source>4up</source>
        <translation>བཞི་སྡེབ་གཅིག་ཏུ་བསྡུ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="75"/>
        <source>4holes</source>
        <translation>ཁུང་བུ་4བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="76"/>
        <source>KMSectionManagement</source>
        <translation>ཐོ་ཁོངས་རྗེས་སྙེག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="77"/>
        <source>AutoTrapping</source>
        <translation>རང་འགུལ་གྱིས་འཐོབ་ཐབས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="78"/>
        <source>BackCoverPage</source>
        <translation>ཁ་སྦྱར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="79"/>
        <source>BackCoverTray</source>
        <translation>ཤོག་བུ་སྦྱར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="80"/>
        <source>KMCopySecurityBackgroundPattern</source>
        <translation>རྒྱབ་ལྗོངས་ཀྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="81"/>
        <source>BindEdge</source>
        <translation>མཐའ་མཚམས་ཀྱི་སྒྲིག་ཆས་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="82"/>
        <source>Binding</source>
        <translation>སྡོད་གནས་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="83"/>
        <source>BLACK</source>
        <translation>ནག་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="84"/>
        <source>Blank</source>
        <translation>ཤོག་བུ་སྟོང་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="85"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="477"/>
        <source>Blue</source>
        <translation>སྔོན་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="86"/>
        <source>Booklet</source>
        <translation>དེབ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="87"/>
        <source>CENTERBOTTOM</source>
        <translation>མཐིལ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="88"/>
        <source>LEFTBOTTOM</source>
        <translation>གཡོན་ཕྱོགས་སུ་ལྷུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="89"/>
        <source>RIGHTBOTTOM</source>
        <translation>གཡས་ཕྱོགས་སུ་ཕྱིན་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="90"/>
        <source>BottomLeft</source>
        <translation>གཡོན་ཕྱོགས་སུ་ལྷུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="91"/>
        <source>KMBoxNumbe</source>
        <translation>BOXཡི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="92"/>
        <source>BypassTray</source>
        <translation>ལག་པས་ཤོག་བུ་སྡེར་མ་སྐྱེལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="93"/>
        <source>Cas1</source>
        <translation>ཤོག་བུ་སྡེར་མ་1བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="94"/>
        <source>Cas2</source>
        <translation>ཤོག་བུ་སྡེར་མ་2བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="95"/>
        <source>Cas3</source>
        <translation>ཤོག་བུ་སྡེར་མ་3བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="96"/>
        <source>Cas4</source>
        <translation>ཤོག་བུ་སྡེར་མ་4བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="97"/>
        <source>Cas5</source>
        <translation>ཤོག་བུ་སྡེར་མ་5བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="98"/>
        <source>CENTER</source>
        <translation>ཀྲུང་དབྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="99"/>
        <source>Stitch</source>
        <translation>ཀྲུང་དབྱང་གིས་བཏགས་ནས་ལྟེབ་བརྩེགས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="100"/>
        <source>KMCopySecurityCharacters</source>
        <translation>ཡི་གེའི་རྟགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="101"/>
        <source>CNAdvancedSmoothing</source>
        <translation>མཐོ་རིམ་གྱི་ངོས་འཇམ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="102"/>
        <source>CNColorHalftone</source>
        <translation>ཚོན་མདོག་ཕྱེད་ཙམ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="103"/>
        <source>CNColorMode</source>
        <translation>ཚོན་མདངས་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="104"/>
        <source>CNColorToUseWithBlack</source>
        <translation>ཚོན་མདོག་དང་ནག་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="105"/>
        <source>CNCopySetNumbering</source>
        <translation>ཨང་གྲངས་དཔར་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="106"/>
        <source>CNCreep</source>
        <translation>གནས་སྤོ་སློབ་གྲྭ་སྤོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="107"/>
        <source>CNDisplacementCorrection</source>
        <translation>གནས་སྤོས་སློབ་གྲྭ་འཛུགས་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="108"/>
        <source>CNEnableTrustPrint</source>
        <translation>སྒྲིག་ཆས་ཀྱི་གསང་རྒྱ་དཔར་བའི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="109"/>
        <source>CNFinisher</source>
        <translation>རྣམ་གྲངས་ཕྱིར་གཏོང་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="110"/>
        <source>CNHalftone</source>
        <translation>ཚོས་གཞི་ཕྱེད་ཙམ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="111"/>
        <source>CNImageRefinement</source>
        <translation>པར་རིས་ཞིབ་ཚགས་སུ་གྱུར་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="112"/>
        <source>CNInterleafMediaType</source>
        <translation>ཤོག་བུའི་རིགས་སུ་འཇུག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="113"/>
        <source>CNInterleafPrint</source>
        <translation>ཤོག་བུའི་ནང་དུ་བཀར་ནས་པར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="114"/>
        <source>CNInterleafSheet</source>
        <translation>ཤོག་བུ་(ཕྱི་གསལ་ནང་གསལ་གྱི་སྤྱིན་ཤོག་བར་དུ་བཅུག་པ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="115"/>
        <source>CNJobExecMode</source>
        <translation>ཕྱིར་གཏོང་གིས་གཞི་གྲངས་ཀྱི་དཔེ་དབྱིབས་དཔར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="116"/>
        <source>CNLineControl</source>
        <translation>སྐུད་ལམ་ཚོད་འཛིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="117"/>
        <source>CNLineRefinement</source>
        <translation>སྐུད་པ་ཕྲ་ཞིང་ཕྲ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="118"/>
        <source>CNMatchingMethod</source>
        <translation>ཆ་མཐུན་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="119"/>
        <source>CNMonitorProfile</source>
        <translation>བརྡ་སྟོན་ཡོ་བྱད་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="120"/>
        <source>CNMultiPunch</source>
        <translation>ཁུང་བུ་བརྟོལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="121"/>
        <source>CNNumberOfColors</source>
        <translation>ཁ་དོག་གི་གྲངས་འབོར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="122"/>
        <source>CNOutputPartition</source>
        <translation>གོ་རིམ་རེ་རེ་བཞིན་གོ་རིམ་སྒྲིག་སྟངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="123"/>
        <source>CNPunch</source>
        <translation>ཁུང་བུ་བརྟོལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="124"/>
        <source>CNPuncher</source>
        <translation>ཁུང་བུ་བརྟོལ་བའི་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="125"/>
        <source>CNSaddleStitch</source>
        <translation>སྒ་དབྱིབས་ཀྱི་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="126"/>
        <source>CNSharpness</source>
        <translation>ཁྱབ་ཁོངས་རྣོ་ངར་ཅན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="127"/>
        <source>CNShiftStartPrintPosition</source>
        <translation>སྒུལ་བདེའི་སྡོད་གནས་བསྐྱར་དཔར་བྱེད་འགོ་བརྩམས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="128"/>
        <source>CNTonerSaving</source>
        <translation>སྣག་ཚའི་ཕྱེ་གྲོན་ཆུང་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="129"/>
        <source>CNTrustPrint</source>
        <translation>སྒྲིག་ཆས་བཀོལ་སྤྱོད་བྱེད་པའི་གསང་རྒྱ་དཔར་བའི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="130"/>
        <source>CNUseCSModeJobAccount</source>
        <translation>སྡེ་ཁག་གི་IDགྱི་དོ་དམ་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="131"/>
        <source>CNUseCSModeSecured</source>
        <translation>ཚགས་དམ་པར་རྒྱག་པའི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="132"/>
        <source>CNVfolding</source>
        <translation>ལྟེབ་བརྩེགས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="133"/>
        <source>Collate</source>
        <translation>རེ་རེ་བཞིན་ཐེལ་ཙེ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="134"/>
        <source>GraphicColorMatching</source>
        <translation>ཚོན་མདོག(རེའུ་མིག)།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="135"/>
        <source>PhotoColorMatching</source>
        <translation>ཁ་དོག(འདྲ་པར། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="136"/>
        <source>TextColorMatching</source>
        <translation>ཁ་མདོག(ཡིག་སྨར། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="137"/>
        <source>OriginalImageType</source>
        <translation>ཁ་དོག་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="138"/>
        <source>Colorimetric</source>
        <translation>མདོག་གི་ཁྱད་པར་ཆུང་ཤོས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="139"/>
        <source>ColorTone</source>
        <translation>ཚོས་གཞི་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="140"/>
        <source>Combination</source>
        <translation>པར་ངོས་ཀྱི་ཤོག་ལྷེ་བགོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="141"/>
        <source>CopyGuard</source>
        <translation>བསྐྱར་དཔར་སྔོན་འགོག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="142"/>
        <source>CopyProtect</source>
        <translation>བསྐྱར་དཔར་སྲུང་སྐྱོང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="143"/>
        <source>KMCopySecurityEnable</source>
        <translation>བསྐྱར་དཔར་བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="144"/>
        <source>KMCopySecurityMode</source>
        <translation>བདེ་འཇགས་ཀྱི་རྣམ་པ་བསྐྱར་དཔར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="145"/>
        <source>CoverMode</source>
        <translation>མདུན་ཤོག་གི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="146"/>
        <source>UserCustomType1</source>
        <translation>རང་གི་མཚན་ཉིད་1བཅས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="147"/>
        <source>Cyan</source>
        <translation>ཆིན་ཆིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="148"/>
        <source>Dark</source>
        <translation>མུན་ནག་ཏུ་གྱུར</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="149"/>
        <source>KMCopySecurityDateTime</source>
        <translation>ཚེས་གྲངས་དང་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="150"/>
        <source>KMCopySecurityDateFormat</source>
        <translation>ཚེས་གྲངས་ཀྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="151"/>
        <source>KMCopySecurityDCNStart</source>
        <translation>ཤོག་གྲངས་དཔར་ནས་ཤོག་གྲངས་དཔར་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="152"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="460"/>
        <source>Default</source>
        <translation>ཁ་རོག་གེར་ཁས་ལེན</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="153"/>
        <source>KMDepCode</source>
        <translation>སྡེ་ཁག་གིས་ཨང་གྲངས་བསྒྲིག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="154"/>
        <source>CNDetectPaperSize</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་ཤོག་བུ་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="155"/>
        <source>KMCopySecurityDCNumber</source>
        <translation>ཁྱབ་སྟངས་ཚོད་འཛིན་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="156"/>
        <source>Document</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="157"/>
        <source>KMEncryption</source>
        <translation>ཚགས་དམ་དུ་གཏོང་བར་སྐུལ་འདེད་གཏོང་དགོས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="159"/>
        <source>DuplexNoTumble</source>
        <translation>ངོས་གཉིས་དཔར་རྒྱག་པ། (མཐའ་རྒྱན་རིང་པོ་རྒྱག་པ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="160"/>
        <source>DuplexTumble</source>
        <translation>ངོས་གཉིས་དཔར་རྒྱག་པ། (མཐའ་ཐུང་སྒྲིག་བཟོ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="161"/>
        <source>String4Pt</source>
        <translation>མཐའ་མཚམས་ཇེ་ཆེར་འགྲོ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="162"/>
        <source>EFFECT2</source>
        <translation>རྒྱབ་ལྗོངས་འཕྱོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="163"/>
        <source>EFFECT1</source>
        <translation>ཡིག་སྐོས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="164"/>
        <source>KMEncPass</source>
        <translation>གསང་བ་དང་གསང་བ་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="165"/>
        <source>Env2</source>
        <translation>ཡིག་སྐོགས་ཀྱི་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="166"/>
        <source>TextGraphic</source>
        <translation>དཔེ་དེབ་དང་པར་རིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="167"/>
        <source>KMBoxFileName</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="168"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="520"/>
        <source>Finisher</source>
        <translation>ཤོག་བུའི་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="169"/>
        <source>Finishing</source>
        <translation>ཕྱོགས་ཞེན་མེད་པར་སྤོ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="170"/>
        <source>Fold</source>
        <translation>ཤོག་བུ་ལྟེབ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="171"/>
        <source>COMPOSITION1</source>
        <translation>དྲང་ཕྱོགས་སུ་ཁེབས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="172"/>
        <source>FrontCoverPage</source>
        <translation>མདུན་ཤོག་གི་མདུན་ཤོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="173"/>
        <source>FrontCoverTray</source>
        <translation>ཤོག་བུ་སྦྱར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="174"/>
        <source>FullColor</source>
        <translation>ཚང་མ་ཚོན་མདངས་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="175"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="409"/>
        <source>General</source>
        <translation>གཞི་རྩའི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="176"/>
        <source>GlossyMode</source>
        <translation>བཀྲག་མདངས་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="177"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="537"/>
        <source>Gradation</source>
        <translation>རིམ་བཞིན་འགྱུར་ལྡོག་བྱུང་བ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="178"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="476"/>
        <source>Green</source>
        <translation>སྔོ་ལྗང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="179"/>
        <source>Group</source>
        <translation>སྐལ་བ་རེ་རེ་བཞིན་མི་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="180"/>
        <source>HalfFold</source>
        <translation>ཤོག་གྲངས་ལྟེབ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="181"/>
        <source>PrinterHDD</source>
        <translation>སྲ་སྡེར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="182"/>
        <source>ThickPaper</source>
        <translation>ཤོག་བུ་མཐུག་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="183"/>
        <source>ThickPaperH</source>
        <translation>མཐོ་རིམ་ཤོག་བུ་ལྗིད་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="184"/>
        <source>HEAVY1</source>
        <translation>ཤོག་བུ་མཐུག་པོ་1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="185"/>
        <source>HEAVY2</source>
        <translation>ཤོག་བུ་མཐུག་པོ་2བཅ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="186"/>
        <source>HEAVY3</source>
        <translation>ཤོག་བུ་མཐུག་པོ་3བཅ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="187"/>
        <source>HEAVY4</source>
        <translation>ཤོག་བུ་མཐུག་པོ་4བཅ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="188"/>
        <source>Quality2</source>
        <translation>སྤུས་ཚད་མཐོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="189"/>
        <source>HighResolution</source>
        <translation>དབྱེ་འབྱེད་བྱེད་ཚད་མཐོན་པོར་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="190"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད་མཐོན་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="191"/>
        <source>FFPunch</source>
        <translation>ཁུང་བུ་བརྟོལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="192"/>
        <source>IDPrint</source>
        <translation>ID〕ཐེལ་ཙེ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="193"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="426"/>
        <source>Quality</source>
        <translation>སྤུས་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="194"/>
        <source>FFOutputMode</source>
        <translation>པར་རིས་ཀྱི་སྤུས་ཀ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="195"/>
        <source>CNSuperSmooth</source>
        <translation>པར་རིས་ཞིབ་ཚགས་སུ་གྱུར་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="196"/>
        <source>InstallableOptions</source>
        <translation>རྣམ་གྲངས་སྒྲིག་སྦྱོར་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="197"/>
        <source>HDD</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ཟིན་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="198"/>
        <source>OHPOpTray</source>
        <translation>ཤོག་བུའི་སྒམ་འཛུགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="199"/>
        <source>KMCopySecurityJobNumber</source>
        <translation>ལས་སྒྲུབ་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="200"/>
        <source>LARGE</source>
        <translation>ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="201"/>
        <source>FFLayout</source>
        <translation>བཀོད་སྒྲིག་བྱེད་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="202"/>
        <source>Left</source>
        <translation>གཡོན་དུ་གཡོན་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="203"/>
        <source>LeftBinding</source>
        <translation>གཡོན་ཕྱོགས་སུ་དབོར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="204"/>
        <source>1Staple(Left)</source>
        <translation>གཡོན་ཟུར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="205"/>
        <source>LHEAD</source>
        <translation>ཆེད་སྤྱོད་ཡིད་རྟོན་ཤོག་བུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="206"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="444"/>
        <source>Light</source>
        <translation>དཀར་ཆ་དོད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="207"/>
        <source>Magenta</source>
        <translation>ཐོན་རྫས་དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="208"/>
        <source>Manual</source>
        <translation>ལག་པ་འགུལ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="209"/>
        <source>MediaType</source>
        <translation>ཤོག་བུའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="210"/>
        <source>Medium</source>
        <translation>འབྲིང་རིམ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="211"/>
        <source>Mode1</source>
        <translation>དཔེ་དབྱིབས་1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="212"/>
        <source>Mode2</source>
        <translation>དཔེ་དབྱིབས་2བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="213"/>
        <source>Mode3</source>
        <translation>མ་དཔེ་3</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="214"/>
        <source>Mode4</source>
        <translation>དཔེ་དབྱིབས་4བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="215"/>
        <source>Monitor</source>
        <translation>ཚོར་བ། (ཆ་མཐུན་བརྡ་སྟོན་ཡོ་བྱད་ཀྱི་ཁ་དོག)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="216"/>
        <source>mono</source>
        <translation>དཀར་ནག་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="217"/>
        <source>PrinterDefault</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་ཀྱིས་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="218"/>
        <source>offset</source>
        <translation>ཕྱོགས་ཞེན་མེད་པར་སྤོ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="219"/>
        <source>OHP</source>
        <translation>སྤྱིན་ཤོག་ཕྱི་གསལ་ནང་གསལ་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="220"/>
        <source>CNOutputAdjustment</source>
        <translation>ཕྱིར་གཏོང་ལེགས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="221"/>
        <source>FFColorMode</source>
        <translation>ཁ་དོག་ཕྱིར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="222"/>
        <source>KMOutputMethod</source>
        <translation>ཕྱིར་གཏོང་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="223"/>
        <source>OutputBin</source>
        <translation>ཤོག་བུ་ཕྱིར་གཏོང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="224"/>
        <source>PageRegion</source>
        <translation>མ་ཡིག་ཤོག་བུ་རིང་ཐུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="225"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="527"/>
        <source>PageSize</source>
        <translation>ཤོག་བུ་ཕྱིར་གཏོང་བྱེད་པའི་རིང་ཐུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="226"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="528"/>
        <source>InputSlot</source>
        <translation>ཤོག་བུའི་འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="227"/>
        <source>PaperSources</source>
        <translation>ཤོག་བུའི་འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="228"/>
        <source>KMAccPass</source>
        <translation>གསང་བའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="229"/>
        <source>PasswordCopy</source>
        <translation>གསང་བ་བསྐྱར་དཔར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="230"/>
        <source>KMCopySecurityPass</source>
        <translation>གསང་གྲངས་བསྐྱར་དཔར། གསང་བའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="231"/>
        <source>KMCopySecurityPatternAngle</source>
        <translation>ཟུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="232"/>
        <source>KMCopySecurityPatternColor</source>
        <translation>བཟོ་དབྱིབས་ཀྱི་ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="233"/>
        <source>KMCopySecurityPatternContrast</source>
        <translation>བསྡུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="234"/>
        <source>KMCopySecurityPatternDensity</source>
        <translation>གར་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="235"/>
        <source>KMCopySecurityPatternEmboss</source>
        <translation>འཇིམ་རྐོས་ཀྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="236"/>
        <source>KMCopySecurityPatternOverwrite</source>
        <translation>བཟོ་དབྱིབས་ཀྱིས་ཁེབས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="237"/>
        <source>KMCopySecurityPatternTextSize</source>
        <translation>ཡིག་ཆ་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="238"/>
        <source>Photo</source>
        <translation>འདྲ་པར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="239"/>
        <source>Photographic</source>
        <translation>ཚོར་བ། (པར་ལེན། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="240"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="448"/>
        <source>Plain</source>
        <translation>ཤོག་བུ་དཀྱུས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="241"/>
        <source>PlainPaper</source>
        <translation>ཤོག་བུ་དཀྱུས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="242"/>
        <source>PlainLPaper</source>
        <translation>ཤོག་བུ་སྲབ་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="243"/>
        <source>Plain(2nd)</source>
        <translation>ཤོག་བུ་དཀྱུས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="244"/>
        <source>PlainPaper1</source>
        <translation>ཤོག་བུ་དཀྱུས་མ་1</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="245"/>
        <source>PlainPaper2</source>
        <translation>ཤོག་བུ་དཀྱུས་མ་2ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="246"/>
        <source>PlainPaperL</source>
        <translation>ཤོག་བུ་སྲབ་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="247"/>
        <source>print</source>
        <translation>སྤྱིར་བཏང་དུ་ཐེལ་ཙེ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="248"/>
        <source>KMStampPNPrintPosition</source>
        <translation>སྡོད་གནས་དཔར་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="249"/>
        <source>KMDuplex</source>
        <translation>ཐམ་ག་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="250"/>
        <source>ProofMode</source>
        <translation>སློབ་གྲྭའི་དཔེ་དབྱིབས་ལ་ཐེལ་ཙེ་རྒྱག་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="251"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="535"/>
        <source>Punch</source>
        <translation>ཁུང་བུ་བརྟོལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="252"/>
        <source>KOPunch</source>
        <translation>ཁུང་བུ་བརྟོལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="253"/>
        <source>PUNF2</source>
        <translation>ཁུང་ཙི། (ཧྥ་རན་སིའི་སྐད། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="254"/>
        <source>PUNF4</source>
        <translation>ཁུང་ཙི། (ཧྥ་རན་སིའི་སྐད། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="255"/>
        <source>PUNS4</source>
        <translation>ཁུང་ཁུང་(སུའེ་ཏེན་གྱི་སྐད། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="256"/>
        <source>PUNU2</source>
        <translation>ཁུང་ཙི། (ཧྥ་རན་སིའི་སྐད། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="257"/>
        <source>PUNU23</source>
        <translation>ཁུང་བུ་3ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="258"/>
        <source>PhotoPureBlack</source>
        <translation>ནག་པོ། (འདྲ་པར། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="259"/>
        <source>TextPureBlack</source>
        <translation>ནག་པོ། (ཡིག་སྨར། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="260"/>
        <source>Recycled(2nd)</source>
        <translation>ཤོག་བུ་བསྐྱར་དུ་བཙེམ་པ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="261"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="475"/>
        <source>Red</source>
        <translation>དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="263"/>
        <source>RIGHTCENTER</source>
        <translation>གཡས་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="264"/>
        <source>RightBinding</source>
        <translation>གཡས་ཕྱོགས་སུ་དབོར་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="265"/>
        <source>1Staple(Right)</source>
        <translation>གཡས་ཟུར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="266"/>
        <source>CNRotatePrint</source>
        <translation>180ལ་འཁོར་སྐྱོད་བྱས་ནས་ཐེལ་ཙེ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="267"/>
        <source>rotation</source>
        <translation>འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="268"/>
        <source>KMSafeQUser</source>
        <translation>SafeQཡི་སྤྱོད་མཁན་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="269"/>
        <source>Saturation</source>
        <translation>གྲོད་པ་རྒྱགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="270"/>
        <source>BoxPrint</source>
        <translation>སྤྱོད་མཁན་BOXཡིས་དཔར་དུ་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="271"/>
        <source>Box</source>
        <translation>སྤྱོད་མཁན་BOXལ་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="272"/>
        <source>GraphicScreen</source>
        <translation>བརྙན་ཤེལ། (རེའུ་མིག)།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="273"/>
        <source>PhotoScreen</source>
        <translation>བརྙན་ཤེལ། (འདྲ་པར། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="274"/>
        <source>TextScreen</source>
        <translation>བརྙན་ཤེལ། (ཡིག་དེབ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="275"/>
        <source>Secure</source>
        <translation>གསང་བའི་ཐོག་ནས་ཐེལ་ཙེ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="276"/>
        <source>KMSecID</source>
        <translation>གསང་བའི་སྒོ་ནས་IDལ་དཔར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="277"/>
        <source>KMSecPass</source>
        <translation>གསང་བའི་ཨང་གྲངས་དཔར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="278"/>
        <source>secured</source>
        <translation>གསང་བའི་ཐོག་ནས་ཐེལ་ཙེ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="279"/>
        <source>SelectColor</source>
        <translation>ཁ་དོག་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="280"/>
        <source>KMCopySecuritySerialNumber</source>
        <translation>རིམ་པ་བསྒྲིགས་པའི་ཨང་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="281"/>
        <source>SideDeck</source>
        <translation>ཤོག་བུ་སྡེར་མ་5བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="282"/>
        <source>SingleSidedOnly</source>
        <translation>ཕྱོགས་གཅིག་ཁོ་ན་ལས་མེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="283"/>
        <source>SMALL</source>
        <translation>ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="284"/>
        <source>Smooth1</source>
        <translation>འདྲ་མཉམ་1།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="285"/>
        <source>Smooth2</source>
        <translation>འདྲ་མཉམ་2ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="286"/>
        <source>GraphicSmoothing</source>
        <translation>ངོས་འཇམ། (རེའུ་མིག)།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="287"/>
        <source>PhotoSmoothing</source>
        <translation>འདྲ་པར། (འདྲ་པར། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="288"/>
        <source>Special</source>
        <translation>ཤོག་བུ་དམིགས་བསལ་ཅན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="289"/>
        <source>CNSpecialPrintMode</source>
        <translation>དམིགས་བསལ་གྱི་པར་རྒྱག་སྟངས་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="290"/>
        <source>StampRepeat</source>
        <translation>བསྐྱར་དུ་བཀོད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="291"/>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="292"/>
        <source>FFStaple</source>
        <translation>རྒྱན་སྤྲས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="293"/>
        <source>StapleCollate</source>
        <translation>སྐལ་བ་རེ་རེ་བཞིན་བཏུ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="294"/>
        <source>StapleGroup</source>
        <translation>སྐལ་བ་རེ་རེ་བཞིན་སྒྲིག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="295"/>
        <source>StapleLocation</source>
        <translation>སྡོད་གནས་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="296"/>
        <source>store</source>
        <translation>གསོག་ཉར་ཐེལ་ཙེ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="297"/>
        <source>TAB</source>
        <translation>ཤོག་བྱང་གི་ཤོག་བྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="298"/>
        <source>Ledger</source>
        <translation>ཚགས་པར་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="299"/>
        <source>Text</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="300"/>
        <source>KMStampPNTextColor</source>
        <translation>ཡིག་སྨར་གྱི་ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="301"/>
        <source>KMCopySecurityTimeFormat</source>
        <translation>དུས་ཚོད་ཀྱི་རྣམ་གཞག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="302"/>
        <source>CNTonerDensity</source>
        <translation>སྣག་ཚ་སྟུག་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="303"/>
        <source>TonerSave</source>
        <translation>སྣག་ཚའི་ཕྱེ་གྲོན་ཆུང་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="304"/>
        <source>Top</source>
        <translation>རྩེ་མོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="305"/>
        <source>TopBinding</source>
        <translation>སྟོད་གོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="306"/>
        <source>UpperLeftSingle</source>
        <translation>གཡོན་ཕྱོགས་སུ་ལྷུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="307"/>
        <source>RIGHTTOP</source>
        <translation>གཡས་ཀྱི་གོང་ལ་ཟེར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="308"/>
        <source>TopLeft</source>
        <translation>གཡོན་ཕྱོགས་སུ་ལྷུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="309"/>
        <source>TopRight</source>
        <translation>གཡས་ཀྱི་གོང་ལ་ཟེར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="310"/>
        <source>TRACING</source>
        <translation>དཔེ་རིས་ཕྱགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="311"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="446"/>
        <source>Transparency</source>
        <translation>ཤོག་བུ་ཕྱི་གསལ་ནང་གསལ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="312"/>
        <source>TransparencyInterleave</source>
        <translation>OHPཡིས་ཤོག་གྲངས་བསྒྲིགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="313"/>
        <source>Tray2</source>
        <translation>ཤོག་བུ་སྡེར་མ་2བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="314"/>
        <source>Tray3</source>
        <translation>ཤོག་བུ་སྡེར་མ་3བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="315"/>
        <source>Tray4</source>
        <translation>ཤོག་བུ་སྡེར་མ་4བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="316"/>
        <source>TrayA</source>
        <translation>ཤོག་བུ་སྡེར་མ་Aརེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="317"/>
        <source>TrayB</source>
        <translation>ཤོག་བུ་སྡེར་མ་Bརེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="318"/>
        <source>TrayC</source>
        <translation>ཤོག་བུ་སྡེར་མ་Cརེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="319"/>
        <source>TriFold</source>
        <translation>ཤོག་གྲངས་ལྟེབ་མ་གསུམ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="320"/>
        <source>TwoColors</source>
        <translation>མདོག་གཉིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="321"/>
        <source>Printer</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་བཀོལ་ནས་ཁ་རོག་གེར་བཀོད་སྒྲིག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="322"/>
        <source>CNSkipBlank</source>
        <translation>ཤོག་ངོས་སྟོང་བ་ལས་བརྒལ་བའི་དཔེ་དབྱིབས་བཀོལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="323"/>
        <source>KMAuthentication</source>
        <translation>བེད་སྤྱོད་བྱེད་མཁན་གྱིས་བདེན་དཔང་ར་སྤྲོད</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="324"/>
        <source>KMAuthUser</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="325"/>
        <source>Vivid</source>
        <translation>བཀྲག་མདངས་བཀྲ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="326"/>
        <source>Vividphoto</source>
        <translation>འདྲ་པར་བཀྲག་མདངས་འཚེར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="327"/>
        <source>Yellow</source>
        <translation>སེར་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="328"/>
        <source>KMDriverEncryption</source>
        <translation>ཐམ་ག་བརྒྱབ་ནས་གསང་རྒྱ་བསྒྲུགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="329"/>
        <source>ColorModel</source>
        <translation>ཚོན་མདངས་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="330"/>
        <source>CMY</source>
        <translation>ཚོན་མདངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="331"/>
        <source>Gray</source>
        <translation>དཀར་ནག་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="332"/>
        <source>UTDuplex</source>
        <translation>ངོས་གཉིས་དཔར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="333"/>
        <source>0Front</source>
        <translation>དྲང་ཕྱོགས་དང་ཉེ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="334"/>
        <source>1Both</source>
        <translation>ངོས་གཉིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="335"/>
        <source>2Back</source>
        <translation>རྒྱབ་ངོས་ཁོ་ན་ལས་མེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="336"/>
        <source>CardFrontGroup</source>
        <translation>ཁ་དྲང་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="337"/>
        <source>CFColourFormat</source>
        <translation>ཁ་དྲང་ཕྱོགས་ཀྱི་ཁ་དོག་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="338"/>
        <source>Sharpness</source>
        <translation>རྣོ་ངར་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="339"/>
        <source>0ResinQuality</source>
        <translation>མྱུར་ཚད་མཐོན་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="340"/>
        <source>1ResinQuality</source>
        <translation>ཁ་རོག་གེར་ཁས་ལེན</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="341"/>
        <source>2ResinQuality</source>
        <translation>སྤུས་ཚད་མཐོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="342"/>
        <source>ColourCorrection</source>
        <translation>ཁ་དོག་ཡང་དག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="343"/>
        <source>0ColourCorrection</source>
        <translation>མེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="344"/>
        <source>1ColourCorrection</source>
        <translation>དོ་དམ་ལ་སྐུལ་འདེད་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="345"/>
        <source>2ColourCorrection</source>
        <translation>ཚོར་སྣང་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="346"/>
        <source>3ColourCorrection</source>
        <translation>ཚད་འགྲངས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="347"/>
        <source>4ColourCorrection</source>
        <translation>བལྟོས་བཅས་ཀྱི་ཚོས་གཞི།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="348"/>
        <source>5ColourCorrection</source>
        <translation>བལྟོས་མེད་ཀྱི་ཚོས་གཞི།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="349"/>
        <source>Postcard</source>
        <translation>ཡིག་ས མིང་འགོད་པའི་འཕྲིན་ཡིག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="350"/>
        <source>DoublePostcardRotated</source>
        <translation>ཡིག་སྐོན་མེད་པའི་འཕྲིན་ཡིག་2(ངོ་གཉིས་)།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="351"/>
        <source>EnvYou4</source>
        <translation>ཡིག་སྐོགས་སྦྱར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="352"/>
        <source>BrMediaType</source>
        <translation>ངོ་བོ་མཚམས་སྦྱོར་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="353"/>
        <source>THIN</source>
        <translation>ཤོག་བུ་སྲབ་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="354"/>
        <source>THICKERPAPER2</source>
        <translation>ཤོག་བུ་མཐུག་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="355"/>
        <source>ENV</source>
        <translation>ཡིག་ཤུབས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="356"/>
        <source>ENVTHICK</source>
        <translation>ཡིག་ཤུབས་མཐུག་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="357"/>
        <source>ENVTHIN</source>
        <translation>ཡིག་ཤུབས་སྲབ་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="358"/>
        <source>GraphicsOptions</source>
        <translation>དཔེ་རིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="359"/>
        <source>SpeedOptions</source>
        <translation>མྱུར་ཚད་ཆུང་དུ་གཏོང་བའི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="360"/>
        <source>MediaOptions</source>
        <translation>ངོ་བོ་མཚམས་སྦྱོར་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="361"/>
        <source>PrnDef</source>
        <translation>རང་འགུལ་གྱིས་སྨྱན་བྱེད་ངོ་སྤྲོད་བྱེད་པའི་གདམ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="362"/>
        <source>Cardstock</source>
        <translation>ཤོག་བྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="363"/>
        <source>Highqlty</source>
        <translation>སྤུས་ཚད་མཐོ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="364"/>
        <source>OptionTray</source>
        <translation>ཤོག་བུ་སྡེར་མ་བདམས་ཆོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="365"/>
        <source>1Cassette</source>
        <translation>ཤོག་བུ་རྐྱང་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="366"/>
        <source>LargeCapacityTray</source>
        <translation>ཤོང་ཚད་ཆེ་བའི་ཤོག་སྡེར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="367"/>
        <source>NotInstalled</source>
        <translation>སྒྲིག་སྦྱོར་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="368"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="526"/>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ཟིན་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="369"/>
        <source>MultiTray</source>
        <translation>ལག་པས་ཤོག་བུ་སྡེར་མ་སྐྱེལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="370"/>
        <source>1Tray</source>
        <translation>ཤོག་བུ་སྡེར་མ་1བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="371"/>
        <source>2Tray</source>
        <translation>ཤོག་བུ་སྡེར་མ་2བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="372"/>
        <source>3Tray</source>
        <translation>ཤོག་བུ་སྡེར་མ་3བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="373"/>
        <source>4Tray</source>
        <translation>ཤོག་བུ་སྡེར་མ་4བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="374"/>
        <source>5Tray</source>
        <translation>ཤོག་བུ་སྡེར་མ་5བཅས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="375"/>
        <source>RIPaperPolicy</source>
        <translation>ཤོག་བུ་དང་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="376"/>
        <source>CutSheetFeeder</source>
        <translation>ཤོག་བུའི་ཡོ་བྱད་ནང་འདྲེན་བྱེད</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="377"/>
        <source>Tractor</source>
        <translation>འདྲུད་ཤོག་གི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="378"/>
        <source>RollPaper</source>
        <translation>ཤོག་བུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="379"/>
        <source>EPSpeed</source>
        <translation>རྒྱ་གར་གྱི་མྱུར་ཚད་དཔར་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="380"/>
        <source>High</source>
        <translation>མྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="381"/>
        <source>Std</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="382"/>
        <source>EPPrintDirection</source>
        <translation>དཔར་རྒྱག་པའི་ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="383"/>
        <source>UniOff</source>
        <translation>ཕྱོགས་གཉིས་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="384"/>
        <source>UniOn</source>
        <translation>ཁ་ཕྱོགས་རྐྱང་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="385"/>
        <source>EPSvRP</source>
        <translation>ཤོག་བུ་གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="386"/>
        <source>EPTearOff</source>
        <translation>རང་འགུལ་གྱིས་ཤོག་བུ་གཏུབ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="387"/>
        <source>EPOffsetHrz</source>
        <translation>ཆུ་ཚད་ཕྱོགས་ཞེན་མེད་པར་འགྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="388"/>
        <source>EPTopMP</source>
        <translation>གོང་གི་རིམ་པ་སྔོན་ལ་སླེབས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="389"/>
        <source>Driver</source>
        <translation>སྐུལ་འདེད་ཀྱི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="390"/>
        <source>EPPackMode</source>
        <translation>ཐུམ་རྒྱག་སྟངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="391"/>
        <source>RGB</source>
        <translation>ཁ་དོག་འདྲེས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="392"/>
        <source>PrinterOptions</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་ཀྱིས་རྣམ་གྲངས་བདམས་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="393"/>
        <source>MediaMethod</source>
        <translation>ཡིག་དཔར་རྒྱག་སྟངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="394"/>
        <source>PaperType</source>
        <translation>ཤོག་བུའི་རིགས་དཔར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="395"/>
        <source>PrintSpeed</source>
        <translation>རྒྱ་གར་གྱི་མྱུར་ཚད་དཔར་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="396"/>
        <source>PrintDarkness</source>
        <translation>གར་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="397"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="513"/>
        <source>None</source>
        <translation>དཔར་ཆས་བཀོལ་སྤྱོད་བྱེད་པའི་མིག་སྔར་སྒྲིག་འགོད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="398"/>
        <source>PostAction</source>
        <translation>ཐམ་ག་བརྒྱབ་རྗེས་བཀོལ་སྤྱོད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="399"/>
        <source>FowardOffset</source>
        <translation>དཔར་བརྒྱབ་རྗེས་ཤོག་བུ་དཔར་ཚར་བ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="400"/>
        <source>SpecifiedPages</source>
        <translation>བར་གྱི་སྐལ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="401"/>
        <source>effects</source>
        <translation>ཕན་འབྲས་ཐོན་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="402"/>
        <source>MirrorImage</source>
        <translation>ཤེལ་གཟུགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="403"/>
        <source>NegativeImage</source>
        <translation>ཤུགས་རྐྱེན་ཐེབས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="404"/>
        <source>Orientation</source>
        <translation>དཔར་རྒྱག་པའི་ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="405"/>
        <source>Compression</source>
        <translation>ཉུང་འཕྲི་བཅས་བྱ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="406"/>
        <source>DataCompress</source>
        <translation>གཞི་གྲངས་ཉུང་འཕྲི།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="407"/>
        <source>ColorOption</source>
        <translation>ཁ་དོག་གི་རྣམ་གྲངས་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="408"/>
        <source>THICK</source>
        <translation>ཤོག་བུ་མཐུག་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="410"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="605"/>
        <source>Draft</source>
        <translation>ཟིན་བྲིས་མ་ཟིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="411"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན་རེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="412"/>
        <source>Normal Color</source>
        <translation>ཁ་དོག་དཀྱུས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="413"/>
        <source>Normal Grayscale</source>
        <translation>ཐལ་ཚད་དཀྱུས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="414"/>
        <source>Draft Color</source>
        <translation>ཚོན་མདངས་བཀྲ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="415"/>
        <source>Draft Grayscale</source>
        <translation>ཐལ་བ་གཡེང་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="416"/>
        <source>Best</source>
        <translation>ཆེས་ལེགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="417"/>
        <source>High-Resolution Photo</source>
        <translation>དབྱེ་འབྱེད་ཚད་མཐོན་པོའི་འདྲ་པར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="418"/>
        <source>Fast Draft</source>
        <translation>མགྱོགས་མྱུར་ངང་ཆུ་ལ་གཡེང་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="419"/>
        <source>Installed Cartridges</source>
        <translation>སྣག་ཚ་བླུགས་པའི་སྣག་ཚ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="420"/>
        <source>Photo Only</source>
        <translation>འདྲ་པར་ཁོ་ན་ལས་མེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="421"/>
        <source>Black and TriColor</source>
        <translation>ནག་པོ་དང་མ་མདོག་གསུམ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="422"/>
        <source>Photo and TriColor</source>
        <translation>འདྲ་པར་དང་མ་མདོག་གསུམ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="424"/>
        <source>Color Mode</source>
        <translation>ཚོན་མདངས་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="425"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="599"/>
        <source>Black Only Grayscale</source>
        <translation>ནག་པོ་དང་ཐལ་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="427"/>
        <source>High Resolution </source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་ཀྱི་དབྱེ་འབྱེད་ཚད་ནི། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="428"/>
        <source>Auto Source</source>
        <translation>རང་འགུལ་གྱི་འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="429"/>
        <source>Manual Feed</source>
        <translation>ལག་པས་ཤོག་བུ་སྐྱེལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="430"/>
        <source>Middle Tray</source>
        <translation>དཀྱིལ་གྱི་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="431"/>
        <source>Upper or Only One InputSlot</source>
        <translation>སྟོད་རྒྱུད་དམ་ཡང་ན་སྣོད་གཅིག་པུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="432"/>
        <source>Multi-purpose Tray</source>
        <translation>སྤྱོད་སྒོ་མང་བའི་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="433"/>
        <source>Drawer 1 </source>
        <translation>འཐེན་སྒམ་1བཅས་ཡོད། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="434"/>
        <source>Drawer 2 </source>
        <translation>འཐེན་སྒམ་2ཡོད། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="435"/>
        <source>Tray 1</source>
        <translation>སྡེར་མ་1བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="436"/>
        <source>Auto Select</source>
        <translation>རང་འགུལ་གྱིས་གདམ་ག་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="442"/>
        <source>Standard Paper</source>
        <translation>ཚད་ལྡན་ཤོག་བུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="443"/>
        <source>Heavy</source>
        <translation>ལྗིད་པོ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="447"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="576"/>
        <source>Plain Paper</source>
        <translation>ཤོག་བུ་དཀྱུས་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="449"/>
        <source>Transparency Film</source>
        <translation>སྤྱིན་ཤོག་ཕྱི་གསལ་ནང་གསལ་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="450"/>
        <source>CD or DVD Media</source>
        <translation>CDའམ་ཡང་ན་DVDགྱི་སྨྱན་སྦྱོར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="451"/>
        <source>Print Density</source>
        <translation>རྒྱ་གར་གྱི་ཚགས་དམ་པར་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="452"/>
        <source>Extra Light (1)</source>
        <translation>ཧ་ཅང་ཡང་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="453"/>
        <source>Light (2)</source>
        <translation>ཡང་བ། (2)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="454"/>
        <source>Medium (3)</source>
        <translation>འབྲིང་རིམ། (3)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="455"/>
        <source>Dark (4)</source>
        <translation>ལྐོག་ཏུ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="456"/>
        <source>Extra Dark (5)</source>
        <translation>ཧ་ཅང་མུན་ནག་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="457"/>
        <source>Copies</source>
        <translation>བུ་ཡིག་གི་བུ་ཡིག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="458"/>
        <source>Adjustment</source>
        <translation>ལེགས་སྒྲིག་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="459"/>
        <source>Halftone Algorithm</source>
        <translation>Halonneཡི་རྩིས་རྒྱག་ཐབས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="461"/>
        <source>Miscellaneous</source>
        <translation>མཉམ་བསྲེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="462"/>
        <source>N-up Orientation</source>
        <translation>གཞུང་ཕྱོགས་སུ་བསྐྱོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="463"/>
        <source>N-up Printing</source>
        <translation>གཞུང་ཕྱོགས་ནས་ཐེལ་ཙེ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="464"/>
        <source>Landscape</source>
        <translation>ཡུལ་ལྗོངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="465"/>
        <source>Seascape</source>
        <translation>རྒྱ་མཚོའི་ཡུལ་ལྗོངས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="469"/>
        <source>Two-Sided</source>
        <translation>ངོས་གཉིས་དཔར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="470"/>
        <source>Print Settings</source>
        <translation>ཐེལ་ཙེ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="471"/>
        <source>Color Settings</source>
        <translation>ཚོན་མདངས་རྣམ་པར་བཀྲ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="472"/>
        <source>Color Settings (Advanced)</source>
        <translation>ཚོན་མདངས་བཀོད་སྒྲིག(མཐོ་རིམ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="473"/>
        <source>Brightness</source>
        <translation>དཀར་ཆ་དོད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="474"/>
        <source>Contrast</source>
        <translation>བསྡུར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="478"/>
        <source>_Media Size</source>
        <translation>ཤོག་བུ་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="479"/>
        <source>_Grayscale</source>
        <translation>ཐལ་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="480"/>
        <source>_Brightness</source>
        <translation>དཀར་ཆ་དོད་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="481"/>
        <source>_Contrast</source>
        <translation>བསྡུར་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="482"/>
        <source>_Saturation</source>
        <translation>གྲོད་པ་རྒྱགས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="483"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="597"/>
        <source>On</source>
        <translation>འགོ་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="484"/>
        <source>2-Sided Printing</source>
        <translation>ངོས་གཉིས་དཔར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="485"/>
        <source>No</source>
        <translation>དེ་ལྟར་མ་བྱས་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="486"/>
        <source>Yes</source>
        <translation>དེ་ནི་རེད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="487"/>
        <source>Installable Options</source>
        <translation>རྣམ་གྲངས་སྒྲིག་སྦྱོར་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="488"/>
        <source>Duplexer Installed</source>
        <translation>བཟོ་པ་གཉིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="489"/>
        <source>Color Model</source>
        <translation>ཚོན་མདངས་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="490"/>
        <source>Color Precision</source>
        <translation>ཚོན་མདོག་གི་བསྡུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="491"/>
        <source>Resolution </source>
        <translation>དབྱེ་འབྱེད་བྱེད་ཚད། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="492"/>
        <source>Printer Features Common</source>
        <translation>པར་འདེབས་འཕྲུལ་ཆས་ཀྱི་རྒྱུན་མཐོང་གི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="493"/>
        <source>CD Hub Size</source>
        <translation>CDརིམ་པའི་རིང་ཐུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="494"/>
        <source>Ink Type</source>
        <translation>སྣག་ཚའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="495"/>
        <source>Toner Save </source>
        <translation>སྣག་ཚའི་ཕྱེ་གྲོན་ཆུང་བྱ་དགོས། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="496"/>
        <source>ON</source>
        <translation>བྱེད་སྒོ་སྤེལ་འགོ་འཛུགས་དགོས</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="497"/>
        <source>Media Type </source>
        <translation>ཤོག་བུའི་རིགས། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="498"/>
        <source>Collate </source>
        <translation>རང་འགུལ་གྱིས་ཤོག་ལྷེ་བགོ་བ། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="499"/>
        <source>Image Refinement </source>
        <translation>པར་རིས་ཞིབ་ཚགས་ཅན་ </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="500"/>
        <source>Halftones </source>
        <translation>ཚོས་གཞི་ཕྱེད་ཙམ། </translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="502"/>
        <source>OFF</source>
        <translation>འགག་སྒོ་བསྲུང་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="503"/>
        <source>ON (Long-edged Binding)</source>
        <translation>(མཐའ་རྒྱན་སྤྲས་པ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="504"/>
        <source>ON (Short-edged Binding)</source>
        <translation>(དུས་ཐུང་ནང་དུ་ཆ་སྒྲིག་པ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="505"/>
        <source>Paper Size</source>
        <translation>ཤོག་བུའི་རིང་ཐུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="506"/>
        <source>Paper Type</source>
        <translation>ཤོག་བུའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="507"/>
        <source>Thin</source>
        <translation>སྲབ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="508"/>
        <source>Thick</source>
        <translation>མཐུག་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="509"/>
        <source>Thicker</source>
        <translation>དེ་བས་ཀྱང་མཐུག་པོ</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="510"/>
        <source>Edge Enhance</source>
        <translation>མཐའ་མཚམས་ཇེ་ཆེར་འགྲོ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="511"/>
        <source>Skip Blank Pages</source>
        <translation>ཤོག་ལྷེ་སྟོང་པ་ལས་མཆོངས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="512"/>
        <source>Double-sided Printing</source>
        <translation>ཕྱོགས་གཉིས་ཀས་ཐེལ་ཙེ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="514"/>
        <source>Reverse Duplex Printing</source>
        <translation>ཕྱོགས་གཉིས་ལ་ལྡོག་ནས་ཐེལ་ཙེ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="515"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="518"/>
        <source>Long Edge</source>
        <translation>རིང་པོ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="516"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="519"/>
        <source>Short Edge</source>
        <translation>ཐུང་ཐུང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="517"/>
        <source>Two-sided</source>
        <translation>ངོས་གཉིས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="521"/>
        <source>Option Tray</source>
        <translation>ཤོག་བུ་སྐྱེལ་ཆས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="522"/>
        <source>External Tray</source>
        <translation>ཕྱི་རོལ་དུ་ཤོག་བུ་སྐྱེལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="523"/>
        <source>Internal Tray 2</source>
        <translation>ནང་ཁུལ་དུ་ཤོག་བུ་སྐྱེལ་ཆས་2ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="524"/>
        <source>Internal Shift Tray</source>
        <translation>ནང་གི་འཁོར་ལོ་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="525"/>
        <source>Not Installed</source>
        <translation>སྒྲིག་སྦྱོར་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="529"/>
        <source>Tray 2</source>
        <translation>སྡེར་མ་2བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="530"/>
        <source>Tray 3</source>
        <translation>སྡེར་མ་3བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="531"/>
        <source>Tray 4</source>
        <translation>སྡེར་མ་4བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="532"/>
        <source>Bypass Tray</source>
        <translation>ལམ་འགྲམ་གྱི་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="533"/>
        <source>Destination</source>
        <translation>དམིགས་ཡུལ་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="534"/>
        <source>Staple</source>
        <translation>དཔེ་ཆའི་གཟེར་མ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="536"/>
        <source>Toner Saving</source>
        <translation>སྣག་ཚའི་ཕྱེ་གྲོན་ཆུང་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="538"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="539"/>
        <source>Draft (Color cartridge)</source>
        <translation>ཟིན་བྲིས། (ཚོན་ལྡན་སྣག་ཚའི་སྒམ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="540"/>
        <source>Normal (Color cartridge)</source>
        <translation>ཟིན་བྲིས། (ཚོན་ལྡན་སྣག་ཚའི་སྒམ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="541"/>
        <source>Photo (on photo paper)</source>
        <translation>འདྲ་པར། (འདྲ་པར་ཤོག་བུ་བཀོལ་སྤྱོད་བྱེད་པ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="542"/>
        <source>Photo (Color cartridge, on photo paper)</source>
        <translation>འདྲ་པར། (ཚོན་ལྡན་སྣག་ཚའི་སྒམ། ཤོག་བུ། )</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="543"/>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="573"/>
        <source>Custom</source>
        <translation>རང་ཉིད་མཚན་ཉིད་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="544"/>
        <source>Letter</source>
        <translation>ཡིག་ཤོག</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="545"/>
        <source>Photo or 4x6 inch index card</source>
        <translation>འདྲ་པར་དང་དབྱིན་ཚུན་4x6ཅན་གྱི་འདྲ་པར་</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="546"/>
        <source>Photo or 5x7 inch index card</source>
        <translation>འདྲ་པར་དང་དབྱིན་ཚུན་5x7གྱི་འདྲ་པར་གྱི་འདྲ་པར།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="547"/>
        <source>Photo with tear-off tab</source>
        <translation>ཤོག་བྱང་འབྲི་སླ་བའི་འདྲ་པར་འཁྱེར་དགོས།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="548"/>
        <source>3x5 inch index card</source>
        <translation>དབྱིན་ཚུན་3x5ཅན་གྱི་འདྲེན་བྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="549"/>
        <source>5x8 inch index card</source>
        <translation>དབྱིན་ཚུན་5x8ཅན་གྱི་འདྲེན་བྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="550"/>
        <source>A6 with tear-off tab</source>
        <translation>ཤོག་བྱང་འབྲི་སླ་བའི་ཤོག་བུ་A6ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="552"/>
        <source>300 dpi, Color, Color Cartr.</source>
        <translation>ཚོན་ལྡན་སྣག་ཚ།ཚོན་ལྡན་སྣག་ཚ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="553"/>
        <source>300 dpi, Draft, Color, Color Cartr.</source>
        <translation>མ་ཟིན་3000ཡི་མ་ཟིན་དང་། ཚོན་མདངས་རྣམ་པར་བཀྲ་བ། ཚོན་ལྡན་སྣག་ཚའི་སྒམ་བཅས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="554"/>
        <source>300 dpi, Draft, Grayscale, Black Cartr.</source>
        <translation>ཟིན་བྲིས་མ་ཟིན་3000དང་། ཐལ་ཚད། སྣག་ཚ་ནག་པོ།སྣག་ཚ་ནག་པོ་བཅས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="555"/>
        <source>300 dpi, Grayscale, Black Cartr.</source>
        <translation>300piཡིན་པ་དང་། ཐལ་ཚད། སྣག་ཚ་ནག་པོ།སྣག་ཚ་ནག་པོ་བཅས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="557"/>
        <source>Executive 7.25x10.5in</source>
        <translation>སྲིད་འཛིན་གྱིས་ཤོག་བུ་7.2510.5inཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="558"/>
        <source>Letter 8.5x11in</source>
        <translation>ཨ་རིའི་འཕྲིན་ཡིག་8.5x113ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="559"/>
        <source>Legal 8.5x14in</source>
        <translation>ཨ་རིའི་ཁྲིམས་བཀོད་ཤོག་བུ་8.51414ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="560"/>
        <source>Postcard (JIS)</source>
        <translation>ཡིག་སྡོགས་མེད་པའི་འཕྲིན་ཡིག(JIS)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="561"/>
        <source>Double Postcard (JIS)</source>
        <translation>ཡིག་སྡོགས་མེད་པའི་འཕྲིན་ཡིག་ངོས་གཉིས། (JIS)</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="562"/>
        <source>Monarch Envelope 3.875x7.5in</source>
        <translation>Monrrchist.3.875x7.5inཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="563"/>
        <source>DL Envelope 110x220mm</source>
        <translation>DLཡི་ཡིག་ཤུབས་110x20mmmཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="564"/>
        <source>#10 Envelope 4.12x9.5in</source>
        <translation>འཕྲིན་ཡིག་ཨང་10པ། 4.12x9.5inབཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="565"/>
        <source>C5 Envelope 162x229mm</source>
        <translation>C5ཡི་ཡིག་ཤུབས་16229mmmཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="566"/>
        <source>B5 Envelope 176x250mm</source>
        <translation>B5ཡི་ཡིག་ཤུབས་176x250mmmཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="567"/>
        <source>C6 Envelope 114x162mm</source>
        <translation>C6གི་ཡིག་ཤུབས་114x162mmmཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="568"/>
        <source>#3 Japanese Envelope 120x235mm</source>
        <translation>ཚེས་3ཉིན་འཇར་པན་གྱི་ཡིག་ཤུབས་120x235mmmཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="569"/>
        <source>Statement 5.5x8.5in</source>
        <translation>རྩིས་ཐོ་5.5x8.5inབཅས་ཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="570"/>
        <source>Photo 4x6in</source>
        <translation>འདྲ་པར་4x6in།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="571"/>
        <source>Photo 5x7in</source>
        <translation>འདྲ་པར་5x7in།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="572"/>
        <source>Photo 10x15cm</source>
        <translation>འདྲ་པར་10x15cmཡོད།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="578"/>
        <source>Mid-Weight96-110g</source>
        <translation>ལྗིད་ཚད་འབྲིང་གྲས་ཀྱི་འོད་ཤོག་96-110Gཡིན།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="581"/>
        <source>Monochrome Laser Transparency</source>
        <translation>ཁ་དོག་སྣ་རྐྱང་གི་སྐུལ་འོད་གསལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="593"/>
        <source>Manual Feeder</source>
        <translation>ཤོག་བུའི་ནང་དུ་ལག་པ་འགུལ་བ།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="601"/>
        <source>Monochrome</source>
        <translation>མདོག་རྐྱང་།</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="604"/>
        <source>FastRes 1200</source>
        <translation>མགྱོགས་མྱུར་གྱིས་1200ལ་དཔར་བ།</translation>
    </message>
</context>
</TS>
