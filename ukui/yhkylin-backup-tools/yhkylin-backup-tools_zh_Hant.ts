<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>對話框</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="15"/>
        <source>Backup &amp; Restore</source>
        <translation>備份還原</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="16"/>
        <source>version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="18"/>
        <source>The backup tool is a tool that supports system backup and data backup. When the user data is damaged or the system is attacked, the tool can flexibly restore the status of the backup node. A lot of optimization and innovation have been carried out for domestic hardware and software platforms.</source>
        <translation>備份還原工具是一款支持系統備份還原和數據備份還原的工具，當用戶數據損壞或系統遭受攻擊時能夠通過該工具靈活的還原到備份節點的狀態。 針對國產軟硬體平台開展了大量的優化和創新。</translation>
    </message>
    <message>
        <source>Service &amp; Support: %1</source>
        <translation type="vanished">服务与支持团队：%1</translation>
    </message>
</context>
<context>
    <name>BackupListWidget</name>
    <message>
        <location filename="component/backuplistwidget.cpp" line="164"/>
        <source>File drag and drop area</source>
        <translation>拖放檔或資料夾識別路徑</translation>
    </message>
</context>
<context>
    <name>BackupPointListDialog</name>
    <message>
        <location filename="backuppointlistdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>對話框</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="54"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup Name</source>
        <translation>備份名稱</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>UUID</source>
        <translation>備份標識</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup Time</source>
        <translation>備份時間</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Position</source>
        <translation>備份位置</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="123"/>
        <source>No Backup</source>
        <translation>暫無備份</translation>
    </message>
</context>
<context>
    <name>BackupPositionSelectDialog</name>
    <message>
        <location filename="component/backuppositionselectdialog.cpp" line="8"/>
        <source>Please select a path</source>
        <translation>請選擇一個路徑</translation>
    </message>
</context>
<context>
    <name>DataBackup</name>
    <message>
        <location filename="module/databackup.cpp" line="96"/>
        <source>Data Backup</source>
        <translation>數據備份</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="117"/>
        <source>Only files in the /home, and /data directories can be backed up</source>
        <translation>僅支援備份/home、/data目錄下的檔</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="119"/>
        <source>Only files in the /home, and /data/usershare directories can be backed up</source>
        <translation>僅支援備份/home、/data/usershare目錄下的檔</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="130"/>
        <source>Multi-Spot</source>
        <translation>多點還原</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="137"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="144"/>
        <source>Protect Data</source>
        <translation>防止數據丟失</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="151"/>
        <source>Convenient</source>
        <translation>便捷快速</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="157"/>
        <source>Start Backup</source>
        <translation>開始備份</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="178"/>
        <source>Update Backup</source>
        <translation>備份更新</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="223"/>
        <source>Backup Management &gt;&gt;</source>
        <translation>備份管理 &gt;&gt;</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="304"/>
        <source>Please select backup position</source>
        <translation>請選擇備份位置</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="322"/>
        <source>Browse</source>
        <translation>流覽</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="471"/>
        <location filename="module/databackup.cpp" line="900"/>
        <source>local default path : </source>
        <translation>本地預設路徑： </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="474"/>
        <location filename="module/databackup.cpp" line="903"/>
        <source>removable devices path : </source>
        <translation>行動裝置： </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="334"/>
        <location filename="module/databackup.cpp" line="715"/>
        <source>Select backup data</source>
        <translation>選擇備份數據</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="362"/>
        <location filename="module/databackup.cpp" line="743"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="397"/>
        <location filename="module/databackup.cpp" line="778"/>
        <source>Select</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="602"/>
        <location filename="module/databackup.cpp" line="851"/>
        <source>Please select file to backup</source>
        <translation>請選擇備份檔</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="434"/>
        <location filename="module/databackup.cpp" line="814"/>
        <location filename="module/databackup.cpp" line="1067"/>
        <location filename="module/databackup.cpp" line="1499"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="355"/>
        <location filename="module/databackup.cpp" line="736"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="444"/>
        <location filename="module/databackup.cpp" line="824"/>
        <location filename="module/databackup.cpp" line="1080"/>
        <location filename="module/databackup.cpp" line="1514"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="484"/>
        <location filename="module/databackup.cpp" line="557"/>
        <location filename="module/databackup.cpp" line="895"/>
        <source>customize path : </source>
        <translation>自訂路徑： </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="687"/>
        <source>Default backup location</source>
        <translation>默認備份位置</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="967"/>
        <location filename="module/databackup.cpp" line="1368"/>
        <location filename="module/databackup.cpp" line="1648"/>
        <location filename="module/databackup.cpp" line="1930"/>
        <source>checking</source>
        <translation>環境檢測</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="970"/>
        <location filename="module/databackup.cpp" line="1371"/>
        <location filename="module/databackup.cpp" line="1651"/>
        <location filename="module/databackup.cpp" line="1933"/>
        <source>preparing</source>
        <translation>備份準備</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="973"/>
        <location filename="module/databackup.cpp" line="1374"/>
        <location filename="module/databackup.cpp" line="1654"/>
        <location filename="module/databackup.cpp" line="1936"/>
        <source>backuping</source>
        <translation>備份中</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="976"/>
        <location filename="module/databackup.cpp" line="1377"/>
        <location filename="module/databackup.cpp" line="1657"/>
        <location filename="module/databackup.cpp" line="1939"/>
        <source>finished</source>
        <translation>備份完成</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1091"/>
        <source>Recheck</source>
        <translation>重新檢測</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1122"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在檢測，請稍等...</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1128"/>
        <source>Do not perform other operations during backup to avoid data loss</source>
        <translation>備份過程中不要做其它操作，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1131"/>
        <source>Check whether the remaining capacity of the backup partition is sufficient</source>
        <translation>檢測備份位置空間是否充足···</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1134"/>
        <source>Check whether the remaining capacity of the removable device is sufficient</source>
        <translation>檢測行動裝置空間是否充足···</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1153"/>
        <source>Check success</source>
        <translation>檢測成功</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1155"/>
        <source>The storage for backup is enough</source>
        <translation>備份空間充足</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1160"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>請確保電腦已連接電源或電量超過60%</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1171"/>
        <source>Check failure</source>
        <translation>檢測失敗</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1284"/>
        <location filename="module/databackup.cpp" line="1813"/>
        <source>Program lock failed, please retry</source>
        <translation>程式鎖定失敗，請重試</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1286"/>
        <location filename="module/databackup.cpp" line="1815"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它備份/還原等任務在執行</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1290"/>
        <location filename="module/databackup.cpp" line="1819"/>
        <source>Unsupported task type</source>
        <translation>不支援的任務類型</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1292"/>
        <location filename="module/databackup.cpp" line="1821"/>
        <source>No processing logic was found in the service</source>
        <translation>沒有找到相應的處理邏輯</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1296"/>
        <location filename="module/databackup.cpp" line="1825"/>
        <source>Failed to mount the backup partition</source>
        <translation>備份分區掛載失敗</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1298"/>
        <location filename="module/databackup.cpp" line="1827"/>
        <source>Check whether there is a backup partition</source>
        <translation>檢查是否有備份分區</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1302"/>
        <source>The filesystem of device is vfat format</source>
        <translation>行動裝置的檔案系統是vfat格式</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1304"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>請換成ext3、ext4、ntfs等文件系統格式</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1308"/>
        <source>The device is read only</source>
        <translation>行動裝置是唯讀掛載的</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1310"/>
        <source>Please chmod to rw</source>
        <translation>請修改為讀寫模式</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1314"/>
        <location filename="module/databackup.cpp" line="1831"/>
        <source>The storage for backup is not enough</source>
        <translation>備份空間不足</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1316"/>
        <location filename="module/databackup.cpp" line="1833"/>
        <source>Retry after release space</source>
        <translation>建議釋放空間后重試</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1320"/>
        <location filename="module/databackup.cpp" line="1837"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它備份還原等操作正在執行</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1322"/>
        <location filename="module/databackup.cpp" line="1839"/>
        <source>Please try again later</source>
        <translation>請稍後重試</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1401"/>
        <source>Backup Name</source>
        <translation>備份名稱</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1452"/>
        <source>Maximum length reached</source>
        <translation>長度已達上限</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1462"/>
        <source>Unsupported symbol : </source>
        <translation>不支援符號： </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1456"/>
        <location filename="module/databackup.cpp" line="1470"/>
        <location filename="module/databackup.cpp" line="1529"/>
        <source>Name already exists</source>
        <translation>名稱已存在</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1714"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1737"/>
        <source>Do not use computer in case of data loss</source>
        <translation>請勿使用電腦，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1845"/>
        <source>Failed to create the backup point directory</source>
        <translation>創建備份目錄失敗</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1847"/>
        <source>Please check backup partition permissions</source>
        <translation>請檢查備份分區許可權</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1851"/>
        <source>The backup had been canceled</source>
        <translation>備份已取消</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1853"/>
        <source>Re-initiate the backup if necessary</source>
        <translation>如需要可重新進行備份</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1886"/>
        <source>An error occurred during backup</source>
        <translation>備份期間發生錯誤</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1888"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>錯誤資訊請參考日誌檔：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2021"/>
        <source>Home Page</source>
        <translation>返回首頁</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2028"/>
        <source>Retry</source>
        <translation>再試一次</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2053"/>
        <source>The backup is successful</source>
        <translation>備份成功</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2067"/>
        <source>The backup is failed</source>
        <translation>備份失敗</translation>
    </message>
</context>
<context>
    <name>DataRestore</name>
    <message>
        <location filename="module/datarestore.cpp" line="82"/>
        <source>Data Restore</source>
        <translation>數據還原</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="100"/>
        <source>Backed up first, then can be restored</source>
        <translation>必須先進行數據備份，否則無法進行數據還原操作</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="111"/>
        <source>Fast Recovery</source>
        <translation>快速恢復</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="118"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="125"/>
        <source>Protect Data</source>
        <translation>解決數據丟失</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="132"/>
        <source>Independent</source>
        <translation>自主操作</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="138"/>
        <source>Start Restore</source>
        <translation>開始還原</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="263"/>
        <location filename="module/datarestore.cpp" line="633"/>
        <location filename="module/datarestore.cpp" line="869"/>
        <source>checking</source>
        <translation>環境檢測</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="267"/>
        <location filename="module/datarestore.cpp" line="637"/>
        <location filename="module/datarestore.cpp" line="873"/>
        <source>restoring</source>
        <translation>還原中</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="271"/>
        <location filename="module/datarestore.cpp" line="641"/>
        <location filename="module/datarestore.cpp" line="877"/>
        <source>finished</source>
        <translation>還原完成</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="353"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="361"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="372"/>
        <source>Recheck</source>
        <translation>重新檢測</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="409"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在檢測，請稍等...</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="415"/>
        <source>Check whether the restore environment meets the requirements</source>
        <translation>檢查恢復環境是否符合要求</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="417"/>
        <source>Do not perform other operations during restore to avoid data loss</source>
        <translation>還原期間不要做其它操作，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="435"/>
        <source>Check success</source>
        <translation>檢測成功</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="437"/>
        <location filename="module/datarestore.cpp" line="681"/>
        <source>Do not use computer in case of data loss</source>
        <translation>請勿使用電腦，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="442"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>請確保電腦已連接電源或電量超過60%</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="453"/>
        <source>Check failure</source>
        <translation>檢測失敗</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="570"/>
        <location filename="module/datarestore.cpp" line="762"/>
        <source>Program lock failed, please retry</source>
        <translation>程式鎖定失敗，請重試</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="572"/>
        <location filename="module/datarestore.cpp" line="764"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它備份/還原等任務在執行</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="576"/>
        <location filename="module/datarestore.cpp" line="768"/>
        <source>Unsupported task type</source>
        <translation>不支援的任務類型</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="578"/>
        <location filename="module/datarestore.cpp" line="770"/>
        <source>No processing logic was found in the service</source>
        <translation>沒有找到相應的處理邏輯</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="582"/>
        <location filename="module/datarestore.cpp" line="774"/>
        <source>The .user.txt file does not exist</source>
        <translation>.user.txt檔不存在</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="584"/>
        <location filename="module/datarestore.cpp" line="590"/>
        <location filename="module/datarestore.cpp" line="596"/>
        <location filename="module/datarestore.cpp" line="776"/>
        <location filename="module/datarestore.cpp" line="782"/>
        <location filename="module/datarestore.cpp" line="788"/>
        <source>Backup points may be corrupted</source>
        <translation>備份點可能被損壞</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="588"/>
        <location filename="module/datarestore.cpp" line="780"/>
        <source>The .exclude.user.txt file does not exist</source>
        <translation>.exclude.user.txt檔不存在</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="594"/>
        <location filename="module/datarestore.cpp" line="786"/>
        <source>The backup point data directory does not exist</source>
        <translation>備份點資料目錄不存在</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="792"/>
        <source>Failed to rsync /boot/efi</source>
        <translation>同步/boot/efi失敗</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="794"/>
        <source>Check the mounting mode of the /boot/efi partition</source>
        <translation>請檢查/boot/efi分區掛載方式</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="798"/>
        <source>Failed to prepare the restore directory</source>
        <translation>還原目錄準備失敗</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="800"/>
        <source>Refer to log :/var/log/backup.log for more information</source>
        <translation>更多資訊請參考日誌/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="833"/>
        <source>An error occurred during restore</source>
        <translation>還原時發生錯誤</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="835"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>錯誤資訊請參考日誌檔：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="952"/>
        <source>Home Page</source>
        <translation>返回首頁</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="959"/>
        <source>Retry</source>
        <translation>再試一次</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="965"/>
        <source>Reboot System</source>
        <translation>重啟系統</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="998"/>
        <source>Successfully restoring the data</source>
        <translation>還原成功</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="1006"/>
        <source>The system needs to reboot. Otherwise, some tools cannot be used.</source>
        <translation>系統需要重啟，否則某些工具可能無法使用</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="1023"/>
        <source>Restoring the data failed</source>
        <translation>還原失敗</translation>
    </message>
</context>
<context>
    <name>DeleteBackupDialog</name>
    <message>
        <location filename="deletebackupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>對話框</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="42"/>
        <location filename="deletebackupdialog.cpp" line="43"/>
        <source>Please wait while data is being removed</source>
        <translation>正在刪除資料，請稍候</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="55"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="67"/>
        <source>Removing backup point...</source>
        <translation>刪除備份點...</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="84"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="130"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它備份還原等操作正在執行</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="171"/>
        <source>Program lock failed, please retry</source>
        <translation>程式鎖定失敗，請重試</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="175"/>
        <source>Unsupported task type</source>
        <translation>不支援的任務類型</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="205"/>
        <source>Deleted backup successfully.</source>
        <translation>刪除備份成功。</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="207"/>
        <source>Failed to delete backup.</source>
        <translation>刪除備份失敗。</translation>
    </message>
</context>
<context>
    <name>FuncTypeConverter</name>
    <message>
        <location filename="functypeconverter.cpp" line="7"/>
        <source>System Backup</source>
        <translation>系統備份</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="8"/>
        <source>System Recovery</source>
        <translation>系統還原</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="9"/>
        <source>Data Backup</source>
        <translation>數據備份</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="10"/>
        <source>Data Recovery</source>
        <translation>數據還原</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="11"/>
        <source>Log Records</source>
        <translation>操作紀錄</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="12"/>
        <source>Ghost Image</source>
        <translation>Ghost鏡像</translation>
    </message>
</context>
<context>
    <name>GhostImage</name>
    <message>
        <location filename="module/ghostimage.cpp" line="91"/>
        <source>Ghost Image</source>
        <translation>Ghost鏡像</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="112"/>
        <source>A ghost image file can only be created after backup system to local disk</source>
        <translation>必須先進行本地系統備份，否則無法創建鏡像檔</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="123"/>
        <source>Simple</source>
        <translation>操作簡單</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="130"/>
        <source>Fast</source>
        <translation>創建速度快</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="137"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="144"/>
        <source>Timesaving</source>
        <translation>節省時間</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="151"/>
        <source>Start Ghost</source>
        <translation>創建鏡像</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="232"/>
        <source>Please select storage location</source>
        <translation>請選擇儲存位置</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="259"/>
        <source>local default path : </source>
        <translation>本地預設路徑： </translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="261"/>
        <source>removable devices path : </source>
        <translation>行動裝置： </translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="279"/>
        <location filename="module/ghostimage.cpp" line="425"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="288"/>
        <location filename="module/ghostimage.cpp" line="433"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="335"/>
        <location filename="module/ghostimage.cpp" line="766"/>
        <location filename="module/ghostimage.cpp" line="1086"/>
        <source>checking</source>
        <translation>環境檢測</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="339"/>
        <location filename="module/ghostimage.cpp" line="770"/>
        <location filename="module/ghostimage.cpp" line="1090"/>
        <source>ghosting</source>
        <translation>創建中</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="343"/>
        <location filename="module/ghostimage.cpp" line="774"/>
        <location filename="module/ghostimage.cpp" line="1094"/>
        <source>finished</source>
        <translation>創建完成</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="444"/>
        <source>Recheck</source>
        <translation>重新檢測</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="481"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在檢測，請稍等...</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="487"/>
        <source>Check whether the conditions for creating an ghost image are met</source>
        <translation>檢測是否具備製作Ghost鏡像條件</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="489"/>
        <source>Do not perform other operations to avoid data loss</source>
        <translation>不要做其它操作，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="507"/>
        <source>Check success</source>
        <translation>檢測成功</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="509"/>
        <source>The storage space is enough</source>
        <translation>存儲空間充足</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="514"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>請確保電腦已連接電源或電量超過60%</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="525"/>
        <source>Check failure</source>
        <translation>檢測失敗</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="649"/>
        <location filename="module/ghostimage.cpp" line="950"/>
        <source>Program lock failed, please retry</source>
        <translation>程式鎖定失敗，請重試</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="651"/>
        <location filename="module/ghostimage.cpp" line="952"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它備份/還原等任務在執行</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="655"/>
        <location filename="module/ghostimage.cpp" line="956"/>
        <source>Unsupported task type</source>
        <translation>不支援的任務類型</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="657"/>
        <location filename="module/ghostimage.cpp" line="958"/>
        <source>No processing logic was found in the service</source>
        <translation>沒有找到相應的處理邏輯</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="661"/>
        <location filename="module/ghostimage.cpp" line="962"/>
        <source>Failed to mount the backup partition</source>
        <translation>備份分區掛載失敗</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="663"/>
        <location filename="module/ghostimage.cpp" line="964"/>
        <source>Check whether there is a backup partition</source>
        <translation>檢查是否有備份分區</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="667"/>
        <location filename="module/ghostimage.cpp" line="968"/>
        <source>The filesystem of device is vfat format</source>
        <translation>行動裝置的檔案系統是vfat格式</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="669"/>
        <location filename="module/ghostimage.cpp" line="970"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>請換成ext3、ext4、ntfs等文件系統格式</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="673"/>
        <location filename="module/ghostimage.cpp" line="974"/>
        <source>The device is read only</source>
        <translation>行動裝置是唯讀掛載的</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="675"/>
        <location filename="module/ghostimage.cpp" line="976"/>
        <source>Please chmod to rw</source>
        <translation>請修改為讀寫模式</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="679"/>
        <location filename="module/ghostimage.cpp" line="980"/>
        <source>The storage for ghost is not enough</source>
        <translation>Ghost存儲空間不足</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="681"/>
        <location filename="module/ghostimage.cpp" line="687"/>
        <location filename="module/ghostimage.cpp" line="982"/>
        <location filename="module/ghostimage.cpp" line="988"/>
        <source>Retry after release space</source>
        <translation>建議釋放空間后重試</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="685"/>
        <location filename="module/ghostimage.cpp" line="986"/>
        <source>There is not enough space for temporary .kyimg file</source>
        <translation>沒有足夠的空間存放臨時.kyimg檔</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="691"/>
        <location filename="module/ghostimage.cpp" line="992"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它備份還原等操作正在執行</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="693"/>
        <location filename="module/ghostimage.cpp" line="994"/>
        <source>Please try again later</source>
        <translation>請稍後重試</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="698"/>
        <location filename="module/ghostimage.cpp" line="999"/>
        <source>The backup node does not exist</source>
        <translation>相應的備份節點不存在</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="700"/>
        <location filename="module/ghostimage.cpp" line="1001"/>
        <source>Check whether the backup point has been deleted</source>
        <translation>請檢查備份點是否已經被刪除</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="835"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="866"/>
        <source>Do not use computer in case of data loss</source>
        <translation>請勿使用電腦，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1005"/>
        <source>The data is being compressed to the local disk, please wait patiently...</source>
        <translation>正壓縮資料到本地磁碟，請耐心等待...</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1010"/>
        <source>Transferring image file to mobile device, about to be completed...</source>
        <translation>傳輸image檔到行動裝置，即將完成...</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1015"/>
        <source>The image creation had been canceled</source>
        <translation>已取消製作Ghost鏡像</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1017"/>
        <source>Re-initiate the image creation if necessary</source>
        <translation>如需要可以重新進行Ghost鏡像製作</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1050"/>
        <source>An error occurred during make ghost image</source>
        <translation>製作Ghost鏡像時發生錯誤</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1052"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>錯誤資訊請參考日誌檔：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1169"/>
        <source>Home Page</source>
        <translation>返回首頁</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1176"/>
        <source>Retry</source>
        <translation>再試一次</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1209"/>
        <source>Ghost image creation is successful</source>
        <translation>創建成功</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1217"/>
        <source>You can view it in the directory : %1</source>
        <translation>您可以在「%1」 目錄下查看</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1231"/>
        <source>Ghost image creation is failed</source>
        <translation>創建失敗</translation>
    </message>
</context>
<context>
    <name>LeftsiderbarWidget</name>
    <message>
        <location filename="leftsiderbarwidget.cpp" line="56"/>
        <location filename="leftsiderbarwidget.cpp" line="57"/>
        <location filename="leftsiderbarwidget.cpp" line="58"/>
        <location filename="leftsiderbarwidget.cpp" line="161"/>
        <location filename="leftsiderbarwidget.cpp" line="162"/>
        <location filename="leftsiderbarwidget.cpp" line="163"/>
        <source>Backup &amp; Restore</source>
        <translation>備份還原</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <location filename="maindialog.cpp" line="128"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="129"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="130"/>
        <location filename="maindialog.cpp" line="286"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="131"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="175"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="179"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="181"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="281"/>
        <source>Normal</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="27"/>
        <source>Backup &amp; Restore</source>
        <translation>備份還原</translation>
    </message>
</context>
<context>
    <name>ManageBackupPointList</name>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="16"/>
        <location filename="module/managebackuppointlist.cpp" line="17"/>
        <source>System Backup Information</source>
        <translation>系統備份資訊清單</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="19"/>
        <location filename="module/managebackuppointlist.cpp" line="20"/>
        <source>Data Backup Information</source>
        <translation>數據備份資訊清單</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="32"/>
        <source>You can delete the backup that does not need, refer operation logs for more details</source>
        <translation>您可以刪除不需要的備份，更多細節請參考“操作日誌”</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="35"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="156"/>
        <source>backup finished</source>
        <translation>備份完成</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="158"/>
        <source>backup unfinished</source>
        <translation>備份未完成</translation>
    </message>
</context>
<context>
    <name>OperationLog</name>
    <message>
        <location filename="module/operationlog.cpp" line="45"/>
        <source>No operation log</source>
        <translation>無操作日誌</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>Backup Name</source>
        <translation>備份名稱</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>UUID</source>
        <translation>備份標識</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>Operation</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>Operation Time</source>
        <translation>操作時間</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="159"/>
        <source>new system backup</source>
        <translation>新建系統備份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="163"/>
        <source>udpate system backup</source>
        <translation>增量系統備份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="167"/>
        <source>new data backup</source>
        <translation>新建資料備份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="171"/>
        <source>update data backup</source>
        <translation>更新數據備份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="175"/>
        <source>restore system</source>
        <translation>系統還原</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="179"/>
        <source>restore retaining user data</source>
        <translation>保留用戶數據還原</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="183"/>
        <source>restore data</source>
        <translation>數據還原</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="187"/>
        <source>delete backup</source>
        <translation>刪除備份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="191"/>
        <source>make ghost image</source>
        <translation>製作ghost鏡像</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../backup-daemon/parsebackuplist.cpp" line="368"/>
        <source>factory backup</source>
        <translation>出廠備份</translation>
    </message>
    <message>
        <location filename="../common/utils.cpp" line="1277"/>
        <location filename="../common/utils.cpp" line="1289"/>
        <source>Factory Backup</source>
        <translation>出廠備份</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup State</source>
        <translation>備份狀態</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>PrefixPath</source>
        <translation>前綴路徑</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="328"/>
        <location filename="component/backuplistwidget.cpp" line="337"/>
        <location filename="component/backuplistwidget.cpp" line="360"/>
        <location filename="maindialog.cpp" line="370"/>
        <location filename="maindialog.cpp" line="386"/>
        <location filename="maindialog.cpp" line="410"/>
        <location filename="module/databackup.cpp" line="573"/>
        <location filename="module/databackup.cpp" line="591"/>
        <location filename="module/databackup.cpp" line="839"/>
        <location filename="module/datarestore.cpp" line="1114"/>
        <location filename="module/managebackuppointlist.cpp" line="51"/>
        <location filename="module/selectrestorepoint.cpp" line="51"/>
        <location filename="module/systembackup.cpp" line="411"/>
        <location filename="module/systemrestore.cpp" line="226"/>
        <source>Warning</source>
        <translation>備份還原</translation>
    </message>
    <message>
        <source>Path can not include symbols that such as : ``,$(),${},;,&amp;,|,etc.</source>
        <translation type="vanished">路径中不能包含：``、$()、${}、;、&amp;、|等特殊符号</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="330"/>
        <location filename="component/backuplistwidget.cpp" line="339"/>
        <location filename="component/backuplistwidget.cpp" line="362"/>
        <location filename="main.cpp" line="47"/>
        <location filename="maindialog.cpp" line="372"/>
        <location filename="maindialog.cpp" line="388"/>
        <location filename="maindialog.cpp" line="412"/>
        <location filename="module/databackup.cpp" line="507"/>
        <location filename="module/databackup.cpp" line="525"/>
        <location filename="module/databackup.cpp" line="533"/>
        <location filename="module/databackup.cpp" line="541"/>
        <location filename="module/databackup.cpp" line="575"/>
        <location filename="module/databackup.cpp" line="593"/>
        <location filename="module/databackup.cpp" line="841"/>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/datarestore.cpp" line="1116"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/managebackuppointlist.cpp" line="51"/>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <location filename="module/selectrestorepoint.cpp" line="51"/>
        <location filename="module/systembackup.cpp" line="346"/>
        <location filename="module/systembackup.cpp" line="364"/>
        <location filename="module/systembackup.cpp" line="372"/>
        <location filename="module/systembackup.cpp" line="380"/>
        <location filename="module/systembackup.cpp" line="413"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="329"/>
        <source>Path already exists : </source>
        <translation>路徑已經存在： </translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="338"/>
        <source>The file or directory does not exist : </source>
        <translation>檔或目錄不存在 </translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="361"/>
        <source>Only data that exists in the follow directorys can be selected: %1.
 Path:%2 is not in them.</source>
        <translation>只有後面目錄中的數據可以選擇：%1。
路徑：%2不在其中。</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <location filename="module/databackup.cpp" line="505"/>
        <location filename="module/databackup.cpp" line="523"/>
        <location filename="module/databackup.cpp" line="531"/>
        <location filename="module/databackup.cpp" line="539"/>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/datarestore.cpp" line="200"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <location filename="module/systembackup.cpp" line="344"/>
        <location filename="module/systembackup.cpp" line="362"/>
        <location filename="module/systembackup.cpp" line="370"/>
        <location filename="module/systembackup.cpp" line="378"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <source>Information</source>
        <translation>備份還原</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>Backup &amp; Restore</source>
        <translation>備份還原</translation>
    </message>
    <message>
        <source>This function can only be used by administrator.</source>
        <translation type="vanished">此功能只能由系统管理员使用。</translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Another user had opened kybackup, you can not start it again.</source>
        <translation>其他使用者已開啟備份還原工具，不能再開啟</translation>
    </message>
    <message>
        <source>kybackup</source>
        <translation type="vanished">备份还原</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="371"/>
        <source>An exception occurred when mounting backup partition.</source>
        <translation>掛載備份分區時發生錯誤。</translation>
    </message>
    <message>
        <source>Please check if the backup partition exists which can be created when you install the Operating System.</source>
        <translation type="vanished">请检查备份还原分区是否存在，在安装操作系统时必须创建备份还原分区。</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="387"/>
        <source>Failed to mount backup partition.</source>
        <translation>掛載備份分區失敗。</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="411"/>
        <location filename="module/datarestore.cpp" line="1115"/>
        <source>It&apos;s busy, please wait</source>
        <translation>系統正忙，請稍等</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="506"/>
        <location filename="module/systembackup.cpp" line="345"/>
        <source>Are you sure to continue customizing the path?
The custom path backup file is not protected, which may cause the backup file to be lost or damaged</source>
        <translation>確定自定義路徑？
自定義路徑備份檔不受保護，可能導致備份文件丟失或損壞</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="524"/>
        <location filename="module/databackup.cpp" line="532"/>
        <location filename="module/systembackup.cpp" line="363"/>
        <location filename="module/systembackup.cpp" line="371"/>
        <source>Cannot nest backups, please select another directory.</source>
        <translation>不能嵌套備份，請選擇其它目錄</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="540"/>
        <location filename="module/systembackup.cpp" line="379"/>
        <source>Mobile devices can only select the mount directory itself.</source>
        <translation>行動裝置只能選擇掛載目錄本身</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="574"/>
        <location filename="module/systembackup.cpp" line="412"/>
        <source>Please select backup position</source>
        <translation>請選擇備份位置</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="592"/>
        <location filename="module/databackup.cpp" line="840"/>
        <source>Please select a backup file or directory</source>
        <translation>請選擇備份檔案或目錄</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <source>Are you sure to cancel the operation？</source>
        <translation>確定取消當前操作？</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="507"/>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/datarestore.cpp" line="200"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <location filename="module/systembackup.cpp" line="346"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <location filename="module/systemrestore.cpp" line="228"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="200"/>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <location filename="module/systemrestore.cpp" line="228"/>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="200"/>
        <source>Contains the user&apos;s home directory, which need to reboot after restoration. Are you sure to continue?</source>
        <translation>包含使用者家目錄，還原完成後需要重啟系統。 是否繼續？</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="51"/>
        <location filename="module/selectrestorepoint.cpp" line="51"/>
        <source>Please select one backup to continue.</source>
        <translation>請選擇一個備份再繼續。</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <source>Are you sure to delete the backup ?</source>
        <translation>是否確定刪除此備份？</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="64"/>
        <location filename="module/managebackuppointlist.cpp" line="145"/>
        <location filename="module/selectrestorepoint.cpp" line="165"/>
        <source>Customize:</source>
        <translation>自訂位置：</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="149"/>
        <location filename="module/selectrestorepoint.cpp" line="172"/>
        <source>Udisk Device:</source>
        <translation>行動裝置：</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="65"/>
        <location filename="module/managebackuppointlist.cpp" line="151"/>
        <location filename="module/selectrestorepoint.cpp" line="174"/>
        <source>Local Disk:</source>
        <translation>本地磁碟：</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <source>Do you want to continue?</source>
        <translation>是否繼續？</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="170"/>
        <source>Other machine:</source>
        <translation>異機備份：</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="227"/>
        <source>Restore factory settings, your system user data will not be retained. Are you sure to continue?</source>
        <translation>恢復出廠設置，您的系統用戶數據都將會消失。 是否繼續？</translation>
    </message>
</context>
<context>
    <name>SelectRestorePoint</name>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="14"/>
        <location filename="module/selectrestorepoint.cpp" line="15"/>
        <source>System Backup Information</source>
        <translation>系統備份資訊清單</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="17"/>
        <location filename="module/selectrestorepoint.cpp" line="18"/>
        <source>Data Backup Information</source>
        <translation>數據備份資訊清單</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="31"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="65"/>
        <source>Other machine:</source>
        <translation>異機備份：</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="67"/>
        <source>Customize:</source>
        <translation>自訂位置：</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="69"/>
        <source>Udisk Device:</source>
        <translation>行動裝置：</translation>
    </message>
</context>
<context>
    <name>SystemBackup</name>
    <message>
        <location filename="module/systembackup.cpp" line="90"/>
        <source>System Backup</source>
        <translation>系統備份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="110"/>
        <source>Can be restored when files are damaged or lost</source>
        <translation>系統原始文件受損或丟失時可以進行還原</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="121"/>
        <source>Multi-Spot</source>
        <translation>多還原點</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="128"/>
        <source>Small Size</source>
        <translation>體積小</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="135"/>
        <source>Security</source>
        <translation>安全保障</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="142"/>
        <source>Simple</source>
        <translation>操作簡單</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="148"/>
        <source>Start Backup</source>
        <translation>開始備份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="165"/>
        <source>Backup Management &gt;&gt;</source>
        <translation>備份管理 &gt;&gt;</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="245"/>
        <source>Please select backup position</source>
        <translation>請選擇備份位置</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="263"/>
        <source>Browse</source>
        <translation>流覽</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="311"/>
        <source>local default path : </source>
        <translation>本地預設路徑： </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="314"/>
        <source>removable devices path : </source>
        <translation>行動裝置： </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="276"/>
        <location filename="module/systembackup.cpp" line="554"/>
        <location filename="module/systembackup.cpp" line="979"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="285"/>
        <location filename="module/systembackup.cpp" line="562"/>
        <location filename="module/systembackup.cpp" line="991"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="324"/>
        <location filename="module/systembackup.cpp" line="396"/>
        <source>customize path : </source>
        <translation>自訂路徑： </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="455"/>
        <location filename="module/systembackup.cpp" line="855"/>
        <location filename="module/systembackup.cpp" line="1131"/>
        <location filename="module/systembackup.cpp" line="1463"/>
        <source>checking</source>
        <translation>環境檢測</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="458"/>
        <location filename="module/systembackup.cpp" line="858"/>
        <location filename="module/systembackup.cpp" line="1134"/>
        <location filename="module/systembackup.cpp" line="1466"/>
        <source>preparing</source>
        <translation>備份準備</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="461"/>
        <location filename="module/systembackup.cpp" line="861"/>
        <location filename="module/systembackup.cpp" line="1137"/>
        <location filename="module/systembackup.cpp" line="1469"/>
        <source>backuping</source>
        <translation>備份中</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="464"/>
        <location filename="module/systembackup.cpp" line="864"/>
        <location filename="module/systembackup.cpp" line="1140"/>
        <location filename="module/systembackup.cpp" line="1472"/>
        <source>finished</source>
        <translation>備份完成</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="573"/>
        <source>Recheck</source>
        <translation>重新檢測</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="604"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在檢測，請稍等...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="610"/>
        <source>Do not perform other operations during backup to avoid data loss</source>
        <translation>備份期間不要做其它操作，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="613"/>
        <source>Check whether the remaining capacity of the backup partition is sufficient</source>
        <translation>檢測備份位置空間是否充足···</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="616"/>
        <source>Check whether the remaining capacity of the removable device is sufficient</source>
        <translation>檢測行動裝置空間是否充足···</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="635"/>
        <source>Check success</source>
        <translation>檢測成功</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="637"/>
        <source>The storage for backup is enough</source>
        <translation>備份空間充足</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="642"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>請確保電腦已連接電源或電量超過60%</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="653"/>
        <source>Check failure</source>
        <translation>檢測失敗</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="771"/>
        <location filename="module/systembackup.cpp" line="1315"/>
        <source>Program lock failed, please retry</source>
        <translation>程式鎖定失敗，請重試</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="773"/>
        <location filename="module/systembackup.cpp" line="1317"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它備份/還原等任務在執行</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="777"/>
        <location filename="module/systembackup.cpp" line="1321"/>
        <source>Unsupported task type</source>
        <translation>不支援的任務類型</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="779"/>
        <location filename="module/systembackup.cpp" line="1323"/>
        <source>No processing logic was found in the service</source>
        <translation>沒有找到相應的處理邏輯</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="783"/>
        <location filename="module/systembackup.cpp" line="1327"/>
        <source>Failed to mount the backup partition</source>
        <translation>備份分區掛載失敗</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="785"/>
        <location filename="module/systembackup.cpp" line="1329"/>
        <source>Check whether there is a backup partition</source>
        <translation>檢查是否有備份分區</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="789"/>
        <location filename="module/systembackup.cpp" line="1333"/>
        <source>The filesystem of device is vfat format</source>
        <translation>行動裝置的檔案系統是vfat格式</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="791"/>
        <location filename="module/systembackup.cpp" line="1335"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>請換成ext3、ext4、ntfs等文件系統格式</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="795"/>
        <location filename="module/systembackup.cpp" line="1339"/>
        <source>The device is read only</source>
        <translation>行動裝置是唯讀掛載的</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="797"/>
        <location filename="module/systembackup.cpp" line="1341"/>
        <source>Please chmod to rw</source>
        <translation>請修改為讀寫模式</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="801"/>
        <location filename="module/systembackup.cpp" line="1345"/>
        <source>The storage for backup is not enough</source>
        <translation>備份空間不足</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="803"/>
        <location filename="module/systembackup.cpp" line="1347"/>
        <source>Retry after release space</source>
        <translation>建議釋放空間后重試</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="807"/>
        <location filename="module/systembackup.cpp" line="1351"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它備份還原等操作正在執行</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="809"/>
        <location filename="module/systembackup.cpp" line="1353"/>
        <source>Please try again later</source>
        <translation>請稍後重試</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="888"/>
        <source>Backup Name</source>
        <translation>備份名稱</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="934"/>
        <source>Maximum length reached</source>
        <translation>長度已達上限</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="944"/>
        <source>Unsupported symbol : </source>
        <translation>不支援符號： </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="938"/>
        <location filename="module/systembackup.cpp" line="952"/>
        <location filename="module/systembackup.cpp" line="1006"/>
        <source>Name already exists</source>
        <translation>名稱已存在</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1071"/>
        <source>factory backup</source>
        <translation>出廠備份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1212"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1236"/>
        <source>Do not use computer in case of data loss</source>
        <translation>請勿使用電腦，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1359"/>
        <source>Failed to create the backup point directory</source>
        <translation>創建備份目錄失敗</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1361"/>
        <source>Please check backup partition permissions</source>
        <translation>請檢查備份分區許可權</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1365"/>
        <source>The system is being compressed to the local disk, please wait patiently...</source>
        <translation>正壓縮系統到本地磁碟，請耐心等待...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1373"/>
        <source>Transferring image file to mobile device, about to be completed...</source>
        <translation>傳輸image檔到行動裝置，即將完成...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1378"/>
        <source>The backup had been canceled</source>
        <translation>已取消備份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1380"/>
        <source>Re-initiate the backup if necessary</source>
        <translation>如需要可重新進行備份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1393"/>
        <location filename="module/systembackup.cpp" line="1419"/>
        <source>An error occurred during backup</source>
        <translation>備份時發生錯誤</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1395"/>
        <location filename="module/systembackup.cpp" line="1421"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>錯誤資訊請參考日誌檔：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1553"/>
        <source>Home Page</source>
        <translation>返回首頁</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1560"/>
        <source>Retry</source>
        <translation>再試一次</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1585"/>
        <source>The backup is successful</source>
        <translation>備份成功</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1599"/>
        <source>The backup is failed</source>
        <translation>備份失敗</translation>
    </message>
</context>
<context>
    <name>SystemRestore</name>
    <message>
        <location filename="module/systemrestore.cpp" line="82"/>
        <source>System Restore</source>
        <translation>系統還原</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="100"/>
        <source>You can restore the system to its previous state</source>
        <translation>在您遇到問題時可將系統還原到以前的狀態</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="111"/>
        <source>Simple</source>
        <translation>操作簡單</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="118"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="125"/>
        <source>Repair</source>
        <translation>修復系統損壞</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="132"/>
        <source>Independent</source>
        <translation>自主操作</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="138"/>
        <source>Start Restore</source>
        <translation>開始還原</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="150"/>
        <source>Factory Restore</source>
        <translation>出廠還原</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="155"/>
        <source>Retaining User Data</source>
        <translation>保留用戶數據</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="281"/>
        <location filename="module/systemrestore.cpp" line="659"/>
        <location filename="module/systemrestore.cpp" line="896"/>
        <source>checking</source>
        <translation>環境檢測</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="285"/>
        <location filename="module/systemrestore.cpp" line="663"/>
        <location filename="module/systemrestore.cpp" line="900"/>
        <source>restoring</source>
        <translation>還原中</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="289"/>
        <location filename="module/systemrestore.cpp" line="667"/>
        <location filename="module/systemrestore.cpp" line="904"/>
        <source>finished</source>
        <translation>還原完成</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="371"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="379"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="390"/>
        <source>Recheck</source>
        <translation>重新檢測</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="427"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在檢測，請稍等...</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="433"/>
        <source>Check whether the restore environment meets the requirements</source>
        <translation>檢查恢復環境是否符合要求</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="435"/>
        <source>Do not perform other operations during restore to avoid data loss</source>
        <translation>還原期間不要做其它操作，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="453"/>
        <source>Check success</source>
        <translation>檢測成功</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="455"/>
        <source>The system will reboot automatically after the restore is successful</source>
        <translation>還原成功後系統將自動重啟</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="460"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>請確保電腦已連接電源或電量超過60%</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="471"/>
        <source>Check failure</source>
        <translation>檢測失敗</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="590"/>
        <location filename="module/systemrestore.cpp" line="789"/>
        <source>Program lock failed, please retry</source>
        <translation>程式鎖定失敗，請重試</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="592"/>
        <location filename="module/systemrestore.cpp" line="791"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它備份/還原等任務在執行</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="596"/>
        <location filename="module/systemrestore.cpp" line="795"/>
        <source>Unsupported task type</source>
        <translation>不支援的任務類型</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="598"/>
        <location filename="module/systemrestore.cpp" line="797"/>
        <source>No processing logic was found in the service</source>
        <translation>沒有找到相應的處理邏輯</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="602"/>
        <location filename="module/systemrestore.cpp" line="801"/>
        <source>The .user.txt file does not exist</source>
        <translation>.user.txt檔不存在</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="604"/>
        <location filename="module/systemrestore.cpp" line="610"/>
        <location filename="module/systemrestore.cpp" line="616"/>
        <location filename="module/systemrestore.cpp" line="803"/>
        <location filename="module/systemrestore.cpp" line="809"/>
        <location filename="module/systemrestore.cpp" line="815"/>
        <source>Backup points may be corrupted</source>
        <translation>備份點可能被損壞</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="608"/>
        <location filename="module/systemrestore.cpp" line="807"/>
        <source>The .exclude.user.txt file does not exist</source>
        <translation>.exclude.user.txt檔不存在</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="614"/>
        <location filename="module/systemrestore.cpp" line="813"/>
        <source>The backup point data directory does not exist</source>
        <translation>備份點資料目錄不存在</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="620"/>
        <location filename="module/systemrestore.cpp" line="819"/>
        <source>Failed to rsync /boot/efi</source>
        <translation>同步/boot/efi失敗</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="622"/>
        <location filename="module/systemrestore.cpp" line="821"/>
        <source>Check the mounting mode of the /boot/efi partition</source>
        <translation>請檢查/boot/efi分區掛載方式</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="707"/>
        <source>Do not use computer in case of data loss</source>
        <translation>請勿使用電腦，以防數據丟失</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="825"/>
        <source>Failed to prepare the restore directory</source>
        <translation>還原目錄準備失敗</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="827"/>
        <source>Refer to log :/var/log/backup.log for more information</source>
        <translation>更多資訊請參考日誌/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="860"/>
        <source>An error occurred during restore</source>
        <translation>還原時發生錯誤</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="862"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>錯誤資訊請參考日誌檔：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="979"/>
        <source>Home Page</source>
        <translation>返回首頁</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="986"/>
        <source>Retry</source>
        <translation>再試一次</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1019"/>
        <source>Successfully restoring the system</source>
        <translation>系統還原成功</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1025"/>
        <source>The system will automatically reboot.</source>
        <translation>系統將自動重啟。</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1026"/>
        <source>Or not, please force shutdown and restart.</source>
        <translation>若未能自動重啟，請手動強制關機重啟。</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1033"/>
        <source>Restoring the system failed</source>
        <translation>系統還原失敗</translation>
    </message>
</context>
<context>
    <name>restore</name>
    <message>
        <location filename="main.cpp" line="129"/>
        <source>system restore</source>
        <translation>系統還原</translation>
    </message>
</context>
</TS>
