<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="15"/>
        <source>Backup &amp; Restore</source>
        <translation>རྗེས་གྲབས་དང་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="16"/>
        <source>version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="18"/>
        <source>The backup tool is a tool that supports system backup and data backup. When the user data is damaged or the system is attacked, the tool can flexibly restore the status of the backup node. A lot of optimization and innovation have been carried out for domestic hardware and software platforms.</source>
        <translation>གྲབས་ཉར་ལག་ཆ་སྤྱོད་མཁན་གྱི་གཞི་གྲངས་ལ་གཏོར་སྐྱོན་ཕོག་པའམ་ཡང་ན་མ་ལག་ལ་འཇབ་རྒོལ་བྱེད་སྐབས་ཡོ་བྱད་ཀྱིས་སྟབས་བསྟུན་གྱིས་རྗེས་གྲབས་དྲ་བའི་གནས་ཚུལ་སླར་གསོ་བྱེད་ཐུབ། རྒྱལ་ནང་གི་མཁྲེགས་ཆས་དང་མཉེན་ཆས་ཀྱི་སྟེགས་བུ་མང་པོ་ཞིག་ལེགས་སྒྱུར་དང་གསར་གཏོད་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>Service &amp; Support: %1</source>
        <translation type="vanished">ཞབས་ཞུ། རྒྱབ་སྐྱོར། %1</translation>
    </message>
</context>
<context>
    <name>BackupListWidget</name>
    <message>
        <location filename="component/backuplistwidget.cpp" line="164"/>
        <source>File drag and drop area</source>
        <translation>ཡིག་ཆ་འདྲུད་འཐེན་དང་མར་ལྷུང་བའི་ས་ཁོངས</translation>
    </message>
</context>
<context>
    <name>BackupPointListDialog</name>
    <message>
        <location filename="backuppointlistdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="54"/>
        <source>Close</source>
        <translation type="unfinished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup Name</source>
        <translation>རྗེས་གྲབས་མིང་།</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup Time</source>
        <translation>རྗེས་གྲབས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup Size</source>
        <translation>རྗེས་གྲབས་གཞི་ཁྱོན།</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Position</source>
        <translation>གོ་གནས།</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="123"/>
        <source>No Backup</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>BackupPositionSelectDialog</name>
    <message>
        <location filename="component/backuppositionselectdialog.cpp" line="8"/>
        <source>Please select a path</source>
        <translation>ཁྱོད་ཀྱིས་ལམ་ཕྲན་ཞིག་འདེམས་རོགས།</translation>
    </message>
</context>
<context>
    <name>DataBackup</name>
    <message>
        <location filename="module/databackup.cpp" line="96"/>
        <source>Data Backup</source>
        <translation>གཞི་གྲངས་གྲབས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="117"/>
        <source>Only files in the /home, and /data directories can be backed up</source>
        <translation>/home,/data དཀར་ཆག་ནང་གི་ཡིག་ཆ་མ་གཏོགས་རྒྱབ་སྐྱོར་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="119"/>
        <source>Only files in the /home, and /data/usershare directories can be backed up</source>
        <translation>/home,/data/usershare དཀར་ཆག་ནང་གི་ཡིག་ཆ་མ་གཏོགས་རྒྱབ་སྐྱོར་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="130"/>
        <source>Multi-Spot</source>
        <translation>ས་ཆ་མང་པོ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="137"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="144"/>
        <source>Protect Data</source>
        <translation>གཞི་གྲངས་ལ་སྲུང་སྐྱོབ་</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="151"/>
        <source>Convenient</source>
        <translation>སྟབས་བདེ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="157"/>
        <source>Start Backup</source>
        <translation>གསར་འཛུགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="178"/>
        <source>Update Backup</source>
        <translation>གསར་སྒྱུར་</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="223"/>
        <source>Backup Management &gt;&gt;</source>
        <translation>རྗེས་གྲབས་དོ་དམ་&gt;&gt;</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="304"/>
        <source>Please select backup position</source>
        <translation>རྗེས་གྲབས་ལས་གནས་གདམ་གསེས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="471"/>
        <location filename="module/databackup.cpp" line="900"/>
        <source>local default path : </source>
        <translation>ས་གནས་དེ་གའི་ཁ་ཆད་དང་འགལ་ </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="474"/>
        <location filename="module/databackup.cpp" line="903"/>
        <source>removable devices path : </source>
        <translation>གནས་སྤོ་ཐུབ་པའི་སྒྲིག་ཆས་ཀྱི་ལམ་བུ། </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="334"/>
        <location filename="module/databackup.cpp" line="715"/>
        <source>Select backup data</source>
        <translation>རྗེས་གྲབས་གཞི་གྲངས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="362"/>
        <location filename="module/databackup.cpp" line="743"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="397"/>
        <location filename="module/databackup.cpp" line="778"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="602"/>
        <location filename="module/databackup.cpp" line="851"/>
        <source>Please select file to backup</source>
        <translation>ཁྱོད་ཀྱིས་ཡིག་ཆ་བདམས་ནས་རྗེས་གྲབས་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="434"/>
        <location filename="module/databackup.cpp" line="814"/>
        <location filename="module/databackup.cpp" line="1067"/>
        <location filename="module/databackup.cpp" line="1499"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="322"/>
        <source>Browse</source>
        <translation>བལྟས་པ་ཙམ་གྱིས་</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="355"/>
        <location filename="module/databackup.cpp" line="736"/>
        <source>Clear</source>
        <translation>གསལ་པོར་བཤད་ན།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="444"/>
        <location filename="module/databackup.cpp" line="824"/>
        <location filename="module/databackup.cpp" line="1080"/>
        <location filename="module/databackup.cpp" line="1514"/>
        <source>Next</source>
        <translation>གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="484"/>
        <location filename="module/databackup.cpp" line="557"/>
        <location filename="module/databackup.cpp" line="895"/>
        <source>customize path : </source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་ལམ་བུ། </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="687"/>
        <source>Default backup location</source>
        <translation>སྔོན་ལ་རྗེས་གྲབས་གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="967"/>
        <location filename="module/databackup.cpp" line="1368"/>
        <location filename="module/databackup.cpp" line="1648"/>
        <location filename="module/databackup.cpp" line="1930"/>
        <source>checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="970"/>
        <location filename="module/databackup.cpp" line="1371"/>
        <location filename="module/databackup.cpp" line="1651"/>
        <location filename="module/databackup.cpp" line="1933"/>
        <source>preparing</source>
        <translation>གྲ་སྒྲིག་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="973"/>
        <location filename="module/databackup.cpp" line="1374"/>
        <location filename="module/databackup.cpp" line="1654"/>
        <location filename="module/databackup.cpp" line="1936"/>
        <source>backuping</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="976"/>
        <location filename="module/databackup.cpp" line="1377"/>
        <location filename="module/databackup.cpp" line="1657"/>
        <location filename="module/databackup.cpp" line="1939"/>
        <source>finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1091"/>
        <source>Recheck</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1122"/>
        <source>Checking, wait a moment ...</source>
        <translation>ཞིབ་བཤེར་བྱས་ནས་ཅུང་ཙམ་སྒུགས་དང་། ...</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1128"/>
        <source>Do not perform other operations during backup to avoid data loss</source>
        <translation>རྗེས་གྲབས་བྱེད་རིང་གཞི་གྲངས་བོར་བརླག་མི་ཡོང་བའི་ཆེད་དུ་ལས་ཀ་གཞན་དག་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1131"/>
        <source>Check whether the remaining capacity of the backup partition is sufficient</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་ཀྱི་ལྷག་མའི་ཤོང་ཚད་འདང་མིན་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1134"/>
        <source>Check whether the remaining capacity of the removable device is sufficient</source>
        <translation>གནས་སྤོ་ཐུབ་པའི་སྒྲིག་ཆས་ཀྱི་ལྷག་འཕྲོའི་ཤོང་ཚད་འདང་མིན་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1153"/>
        <source>Check success</source>
        <translation>ཞིབ་བཤེར་ལེགས་འགྲུབ་བྱུང་</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1155"/>
        <source>The storage for backup is enough</source>
        <translation>རྗེས་གྲབས་གསོག་འཇོག་བྱས་པ་འདང་ངེས་ཤིག་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1160"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>གློག་ཀླད་ནང་དུ་འཇུག་པའམ་ཡང་ན་གློག་གཡིས་ཀྱི་ཆུ་ཚད་བརྒྱ་ཆ་60ཡན་ཟིན་པར་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1171"/>
        <source>Check failure</source>
        <translation>ཞིབ་བཤེར་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1284"/>
        <location filename="module/databackup.cpp" line="1813"/>
        <source>Program lock failed, please retry</source>
        <translation>གོ་རིམ་གྱི་ཟྭ་ལ་སྐྱོན་ཤོར་བ་དང་། བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1286"/>
        <location filename="module/databackup.cpp" line="1815"/>
        <source>There may be other backups or restores being performed</source>
        <translation>ད་དུང་ལག་བསྟར་བྱེད་བཞིན་པའི་རྗེས་གྲབས་དཔུང་ཁག་གམ་ཡང་ན་སླར་གསོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1290"/>
        <location filename="module/databackup.cpp" line="1819"/>
        <source>Unsupported task type</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ལས་འགན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1292"/>
        <location filename="module/databackup.cpp" line="1821"/>
        <source>No processing logic was found in the service</source>
        <translation>ཞབས་ཞུའི་ཁྲོད་དུ་ཐག་གཅོད་བྱེད་པའི་གཏན་ཚིགས་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1296"/>
        <location filename="module/databackup.cpp" line="1825"/>
        <source>Failed to mount the backup partition</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་བྱས་ནས་སྒྲིག་སྦྱོར་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1298"/>
        <location filename="module/databackup.cpp" line="1827"/>
        <source>Check whether there is a backup partition</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1302"/>
        <source>The filesystem of device is vfat format</source>
        <translation>སྒྲིག་ཆས་ཀྱི་ཡིག་ཚགས་མ་ལག་ནི་vfatཡི་རྣམ་གཞག་ཡིན།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1304"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>ཡིག་ཚགས་མ་ལག་གི་རྣམ་གཞག་དེ་ext3、ext4འམ་ཡང་ན་ntfsལ་བསྒྱུར་རོགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1308"/>
        <source>The device is read only</source>
        <translation>སྒྲིག་ཆས་འདི་ཁོ་ནར་བལྟས་པ་ཙམ་ཡིན།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1310"/>
        <source>Please chmod to rw</source>
        <translation>ཁྱེད་ཀྱིས་ཆི་མོ་ཏི་ལ་གུས་ལུགས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1314"/>
        <location filename="module/databackup.cpp" line="1831"/>
        <source>The storage for backup is not enough</source>
        <translation>རྗེས་གྲབས་གསོག་འཇོག་བྱས་པ་མི་འདང་བ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1316"/>
        <location filename="module/databackup.cpp" line="1833"/>
        <source>Retry after release space</source>
        <translation>བར་སྟོང་འགྲེམ་སྤེལ་བྱས་རྗེས་ཡང་བསྐྱར་ཞིབ་བཤེར་བྱས</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1320"/>
        <location filename="module/databackup.cpp" line="1837"/>
        <source>Other backup or restore task is being performed</source>
        <translation>ད་དུང་རྗེས་གྲབས་ལས་འགན་གཞན་དག་གམ་ཡང་ན་སླར་གསོ་བྱེད་པའི་ལས་འགན</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1322"/>
        <location filename="module/databackup.cpp" line="1839"/>
        <source>Please try again later</source>
        <translation>ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1401"/>
        <source>Backup Name</source>
        <translation>རྗེས་གྲབས་མིང་།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1452"/>
        <source>Maximum length reached</source>
        <translation>རིང་ཚད་ཡས་མཐའི་ཚད་ལ་སླེབས་འདུག</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1462"/>
        <source>Unsupported symbol : </source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་རྟགས། </translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1456"/>
        <location filename="module/databackup.cpp" line="1470"/>
        <location filename="module/databackup.cpp" line="1529"/>
        <source>Name already exists</source>
        <translation>མིང་ཡོད་པ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1714"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1737"/>
        <source>Do not use computer in case of data loss</source>
        <translation>གཞི་གྲངས་བོར་བརླག་ཏུ་སོང་བའི་གནས་ཚུལ་འོག་རྩིས་འཁོར་བཀོལ་སྤྱོད་མི་</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1845"/>
        <source>Failed to create the backup point directory</source>
        <translation>རྗེས་གྲབས་ས་གནས་ཀྱི་དཀར་ཆག་གསར་སྐྲུན་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1847"/>
        <source>Please check backup partition permissions</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་ཀྱི་ཆོག་འཐུས་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1851"/>
        <source>The backup had been canceled</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག་དེ་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1853"/>
        <source>Re-initiate the backup if necessary</source>
        <translation>དགོས་ངེས་ཀྱི་སྐབས་སུ་ཡང་བསྐྱར་རྗེས་གྲབས་དཔུང་ཁག་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1886"/>
        <source>An error occurred during backup</source>
        <translation>རྗེས་གྲབས་བྱེད་རིང་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1888"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཆ་འཕྲིན་ཞེས་པ་ནི་ཐོ་འགོད་ཡིག་ཚགས་ལ་ཟེར.log</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2021"/>
        <source>Home Page</source>
        <translation>ཁྱིམ་ཚང་གི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2028"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2053"/>
        <source>The backup is successful</source>
        <translation>རྗེས་གྲབས་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="2067"/>
        <source>The backup is failed</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག་ལ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>DataRestore</name>
    <message>
        <location filename="module/datarestore.cpp" line="82"/>
        <source>Data Restore</source>
        <translation>གཞི་གྲངས་སླར་གསོ</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="100"/>
        <source>Backed up first, then can be restored</source>
        <translation>སྔོན་ལ་རྒྱབ་སྐྱོར་བྱས་ན་སླར་གསོ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="111"/>
        <source>Fast Recovery</source>
        <translation>མགྱོགས་མྱུར་སླར་གསོ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="118"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="125"/>
        <source>Protect Data</source>
        <translation>གཞི་གྲངས་ལ་སྲུང་སྐྱོབ་</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="132"/>
        <source>Independent</source>
        <translation>རང་རྐྱ་འཕེར་བའི</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="138"/>
        <source>Start Restore</source>
        <translation>འགོ་ཚུགས་པ་</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="263"/>
        <location filename="module/datarestore.cpp" line="633"/>
        <location filename="module/datarestore.cpp" line="869"/>
        <source>checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="267"/>
        <location filename="module/datarestore.cpp" line="637"/>
        <location filename="module/datarestore.cpp" line="873"/>
        <source>restoring</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="271"/>
        <location filename="module/datarestore.cpp" line="641"/>
        <location filename="module/datarestore.cpp" line="877"/>
        <source>finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="353"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="361"/>
        <source>Next</source>
        <translation>གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="372"/>
        <source>Recheck</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="409"/>
        <source>Checking, wait a moment ...</source>
        <translation>ཞིབ་བཤེར་བྱས་ནས་ཅུང་ཙམ་སྒུགས་དང་། ...</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="415"/>
        <source>Check whether the restore environment meets the requirements</source>
        <translation>སླར་གསོ་བྱེད་པའི་ཁོར་ཡུག་དེ་བླང་བྱ་དང་མཐུན་མིན་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="417"/>
        <source>Do not perform other operations during restore to avoid data loss</source>
        <translation>གཞི་གྲངས་བོར་བརླག་མི་ཡོང་བའི་ཆེད་དུ་སླར་གསོ་བྱེད་རིང་ལས་སྒོ་གཞན་དག་མི་སྒྲུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="435"/>
        <source>Check success</source>
        <translation>ཞིབ་བཤེར་ལེགས་འགྲུབ་བྱུང་</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="437"/>
        <location filename="module/datarestore.cpp" line="681"/>
        <source>Do not use computer in case of data loss</source>
        <translation>གཞི་གྲངས་བོར་བརླག་ཏུ་སོང་བའི་གནས་ཚུལ་འོག་རྩིས་འཁོར་བཀོལ་སྤྱོད་མི་</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="442"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>གློག་ཀླད་ནང་དུ་འཇུག་པའམ་ཡང་ན་གློག་གཡིས་ཀྱི་ཆུ་ཚད་བརྒྱ་ཆ་60ཡན་ཟིན་པར་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="453"/>
        <source>Check failure</source>
        <translation>ཞིབ་བཤེར་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="570"/>
        <location filename="module/datarestore.cpp" line="762"/>
        <source>Program lock failed, please retry</source>
        <translation>གོ་རིམ་གྱི་ཟྭ་ལ་སྐྱོན་ཤོར་བ་དང་། བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="572"/>
        <location filename="module/datarestore.cpp" line="764"/>
        <source>There may be other backups or restores being performed</source>
        <translation>ད་དུང་ལག་བསྟར་བྱེད་བཞིན་པའི་རྗེས་གྲབས་དཔུང་ཁག་གམ་ཡང་ན་སླར་གསོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="576"/>
        <location filename="module/datarestore.cpp" line="768"/>
        <source>Unsupported task type</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ལས་འགན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="578"/>
        <location filename="module/datarestore.cpp" line="770"/>
        <source>No processing logic was found in the service</source>
        <translation>ཞབས་ཞུའི་ཁྲོད་དུ་ཐག་གཅོད་བྱེད་པའི་གཏན་ཚིགས་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="582"/>
        <location filename="module/datarestore.cpp" line="774"/>
        <source>The .user.txt file does not exist</source>
        <translation>.user.txt ཡིག་ཆ་གནས་མེད།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="584"/>
        <location filename="module/datarestore.cpp" line="590"/>
        <location filename="module/datarestore.cpp" line="596"/>
        <location filename="module/datarestore.cpp" line="776"/>
        <location filename="module/datarestore.cpp" line="782"/>
        <location filename="module/datarestore.cpp" line="788"/>
        <source>Backup points may be corrupted</source>
        <translation>རྗེས་གྲབས་ས་ཚིགས་རུལ་སུངས་སུ་འགྱུར་སྲིད།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="588"/>
        <location filename="module/datarestore.cpp" line="780"/>
        <source>The .exclude.user.txt file does not exist</source>
        <translation>.exclude.user.txtཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="594"/>
        <location filename="module/datarestore.cpp" line="786"/>
        <source>The backup point data directory does not exist</source>
        <translation>རྗེས་གྲབས་ས་གནས་ཀྱི་གཞི་གྲངས་དཀར་ཆག་མེད།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="792"/>
        <source>Failed to rsync /boot/efi</source>
        <translation>rsync /boot/efiལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="794"/>
        <source>Check the mounting mode of the /boot/efi partition</source>
        <translation>/boot/efiས་ཁུལ་གྱི་སྒྲིག་སྦྱོར་བྱེད་སྟངས་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="798"/>
        <source>Failed to prepare the restore directory</source>
        <translation>སླར་གསོ་བྱས་པའི་དཀར་ཆག་གྲ་སྒྲིག་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="800"/>
        <source>Refer to log :/var/log/backup.log for more information</source>
        <translation>དེ་བས་མང་བའི་ཆ་འཕྲིན་རོགས་།དཔྱད་གཞིའི་ཉིན་ཐོ་།:/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="833"/>
        <source>An error occurred during restore</source>
        <translation>སླར་གསོ་བྱེད་རིང་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="835"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཆ་འཕྲིན་དཔྱད་གཞིའི་ཉིན་ཐོའི་ཡིག་ཆ་རོགས་།:/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="952"/>
        <source>Home Page</source>
        <translation>ཁྱིམ་ཚང་གི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="959"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="965"/>
        <source>Reboot System</source>
        <translation>བསྐྱར་དུ་མ་ལག་བསྐྱར་དུ་སྒྲིག་པ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="998"/>
        <source>Successfully restoring the data</source>
        <translation>གཞི་གྲངས་བདེ་བླག་ངང་སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="1006"/>
        <source>The system needs to reboot. Otherwise, some tools cannot be used.</source>
        <translation>མ་ལག་འདི་བསྐྱར་དུ་ཐོན་དགོས། དེ་འདྲ་མ་བྱས་ན་ཡོ་བྱད་ཁ་ཤས་བེད་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="1023"/>
        <source>Restoring the data failed</source>
        <translation>གཞི་གྲངས་སླར་གསོ་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>DeleteBackupDialog</name>
    <message>
        <location filename="deletebackupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="42"/>
        <location filename="deletebackupdialog.cpp" line="43"/>
        <source>Please wait while data is being removed</source>
        <translation>གཞི་གྲངས་མེད་པར་བཟོ་བཞིན་པའི་སྐབས་སུ་སྒུག་རོགས།</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="55"/>
        <source>Close</source>
        <translation type="unfinished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="67"/>
        <source>Removing backup point...</source>
        <translation>རྗེས་གྲབས་ས་ཆ་མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="84"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="130"/>
        <source>Other backup or restore task is being performed</source>
        <translation>ད་དུང་རྗེས་གྲབས་ལས་འགན་གཞན་དག་གམ་ཡང་ན་སླར་གསོ་བྱེད་པའི་ལས་འགན</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="171"/>
        <source>Program lock failed, please retry</source>
        <translation>གོ་རིམ་གྱི་ཟྭ་ལ་སྐྱོན་ཤོར་བ་དང་། བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="175"/>
        <source>Unsupported task type</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ལས་འགན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="205"/>
        <source>Deleted backup successfully.</source>
        <translation>བདེ་བླག་ངང་རྗེས་གྲབས་དཔུང་ཁག་བསུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="207"/>
        <source>Failed to delete backup.</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག་བསུབ་མ་ཐུབ་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>FuncTypeConverter</name>
    <message>
        <location filename="functypeconverter.cpp" line="7"/>
        <source>System Backup</source>
        <translation>མ་ལག་གི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="8"/>
        <source>System Recovery</source>
        <translation>མ་ལག་སླར་གསོ་</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="9"/>
        <source>Data Backup</source>
        <translation>གཞི་གྲངས་གྲབས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="10"/>
        <source>Data Recovery</source>
        <translation>གཞི་གྲངས་སླར་གསོ་</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="11"/>
        <source>Log Records</source>
        <translation>ཟིན་ཐོ་འགོད་པ།</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="12"/>
        <source>Ghost Image</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན།</translation>
    </message>
</context>
<context>
    <name>GhostImage</name>
    <message>
        <location filename="module/ghostimage.cpp" line="91"/>
        <source>Ghost Image</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="112"/>
        <source>A ghost image file can only be created after backup system to local disk</source>
        <translation>ས་གནས་དེ་གའི་རྒྱུད་ཁོངས་གྲབས་ཉར་གྲུབ་པའི་ཤེལ་བརྙན།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="123"/>
        <source>Simple</source>
        <translation>སྟབས་བདེ་</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="130"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="137"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="144"/>
        <source>Timesaving</source>
        <translation>དུས་ཚོད་ཀྱི་འཁོར་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="151"/>
        <source>Start Ghost</source>
        <translation>གསར་འཛུགས་</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="232"/>
        <source>Please select storage location</source>
        <translation>གསོག་ཉར་བྱེད་ས་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="259"/>
        <source>local default path : </source>
        <translation>ས་གནས་དེ་གའི་ཁ་ཆད་དང་འགལ་ </translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="261"/>
        <source>removable devices path : </source>
        <translation>གནས་སྤོ་ཐུབ་པའི་སྒྲིག་ཆས་ཀྱི་ལམ་བུ། </translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="279"/>
        <location filename="module/ghostimage.cpp" line="425"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="288"/>
        <location filename="module/ghostimage.cpp" line="433"/>
        <source>Next</source>
        <translation>གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="335"/>
        <location filename="module/ghostimage.cpp" line="766"/>
        <location filename="module/ghostimage.cpp" line="1086"/>
        <source>checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="339"/>
        <location filename="module/ghostimage.cpp" line="770"/>
        <location filename="module/ghostimage.cpp" line="1090"/>
        <source>ghosting</source>
        <translation>གདོན་འདྲེའི་གདོན་འདྲེ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="343"/>
        <location filename="module/ghostimage.cpp" line="774"/>
        <location filename="module/ghostimage.cpp" line="1094"/>
        <source>finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="444"/>
        <source>Recheck</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="481"/>
        <source>Checking, wait a moment ...</source>
        <translation>ཞིབ་བཤེར་བྱས་ནས་ཅུང་ཙམ་སྒུགས་དང་། ...</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="487"/>
        <source>Check whether the conditions for creating an ghost image are met</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན་གསར་སྐྲུན་བྱེད་པའི་ཆ་རྐྱེན་དང་མཐུན་མིན་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="489"/>
        <source>Do not perform other operations to avoid data loss</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན་གསར་སྐྲུན་བྱེད་རིང་གཞི་གྲངས་བོར་བརླག་མི་ཡོང་བའི་ཆེད་དུ་བཀོལ་སྤྱོད་གཞན་དག་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="507"/>
        <source>Check success</source>
        <translation>ཞིབ་བཤེར་ལེགས་འགྲུབ་བྱུང་</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="509"/>
        <source>The storage space is enough</source>
        <translation>གསོག་ཉར་གྱི་བར་སྟོང་འདང་ངེས་ཤིག་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="514"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>གློག་ཀླད་ནང་དུ་འཇུག་པའམ་ཡང་ན་གློག་གཡིས་ཀྱི་ཆུ་ཚད་བརྒྱ་ཆ་60ཡན་ཟིན་པར་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="525"/>
        <source>Check failure</source>
        <translation>ཞིབ་བཤེར་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="649"/>
        <location filename="module/ghostimage.cpp" line="950"/>
        <source>Program lock failed, please retry</source>
        <translation>གོ་རིམ་གྱི་ཟྭ་ལ་སྐྱོན་ཤོར་བ་དང་། བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="651"/>
        <location filename="module/ghostimage.cpp" line="952"/>
        <source>There may be other backups or restores being performed</source>
        <translation>ད་དུང་ལག་བསྟར་བྱེད་བཞིན་པའི་རྗེས་གྲབས་དཔུང་ཁག་གམ་ཡང་ན་སླར་གསོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="655"/>
        <location filename="module/ghostimage.cpp" line="956"/>
        <source>Unsupported task type</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ལས་འགན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="657"/>
        <location filename="module/ghostimage.cpp" line="958"/>
        <source>No processing logic was found in the service</source>
        <translation>ཞབས་ཞུའི་ཁྲོད་དུ་ཐག་གཅོད་བྱེད་པའི་གཏན་ཚིགས་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="661"/>
        <location filename="module/ghostimage.cpp" line="962"/>
        <source>Failed to mount the backup partition</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་བྱས་ནས་སྒྲིག་སྦྱོར་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="663"/>
        <location filename="module/ghostimage.cpp" line="964"/>
        <source>Check whether there is a backup partition</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="667"/>
        <location filename="module/ghostimage.cpp" line="968"/>
        <source>The filesystem of device is vfat format</source>
        <translation>སྒྲིག་ཆས་ཀྱི་ཡིག་ཚགས་མ་ལག་ནི་vfatཡི་རྣམ་གཞག་ཡིན།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="669"/>
        <location filename="module/ghostimage.cpp" line="970"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>ཡིག་ཚགས་མ་ལག་གི་རྣམ་གཞག་དེ་ext3、ext4འམ་ཡང་ན་ntfsལ་བསྒྱུར་རོགས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="673"/>
        <location filename="module/ghostimage.cpp" line="974"/>
        <source>The device is read only</source>
        <translation>སྒྲིག་ཆས་འདི་ཁོ་ནར་བལྟས་པ་ཙམ་ཡིན།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="675"/>
        <location filename="module/ghostimage.cpp" line="976"/>
        <source>Please chmod to rw</source>
        <translation>ཁྱེད་ཀྱིས་ཆི་མོ་ཏི་ལ་གུས་ལུགས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="679"/>
        <location filename="module/ghostimage.cpp" line="980"/>
        <source>The storage for ghost is not enough</source>
        <translation>གདོན་འདྲེའི་གསོག་ཉར་མི་འདང་བ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="681"/>
        <location filename="module/ghostimage.cpp" line="687"/>
        <location filename="module/ghostimage.cpp" line="982"/>
        <location filename="module/ghostimage.cpp" line="988"/>
        <source>Retry after release space</source>
        <translation>བར་སྟོང་འགྲེམ་སྤེལ་བྱས་རྗེས་ཡང་བསྐྱར་ཞིབ་བཤེར་བྱས</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="685"/>
        <location filename="module/ghostimage.cpp" line="986"/>
        <source>There is not enough space for temporary .kyimg file</source>
        <translation>གནས་སྐབས་ཀྱི་ཡིག་ཚགས་ཀྱི་བར་སྟོང་མི་འདང་བ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="691"/>
        <location filename="module/ghostimage.cpp" line="992"/>
        <source>Other backup or restore task is being performed</source>
        <translation>ད་དུང་རྗེས་གྲབས་ལས་འགན་གཞན་དག་གམ་ཡང་ན་སླར་གསོ་བྱེད་པའི་ལས་འགན</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="693"/>
        <location filename="module/ghostimage.cpp" line="994"/>
        <source>Please try again later</source>
        <translation>ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="698"/>
        <location filename="module/ghostimage.cpp" line="999"/>
        <source>The backup node does not exist</source>
        <translation>རྗེས་གྲབས་ཀྱི་གནས་ཚུལ་མི་གནས་པ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="700"/>
        <location filename="module/ghostimage.cpp" line="1001"/>
        <source>Check whether the backup point has been deleted</source>
        <translation>རྗེས་གྲབས་ས་ཚིགས་སུབ་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="835"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="866"/>
        <source>Do not use computer in case of data loss</source>
        <translation>གཞི་གྲངས་བོར་བརླག་ཏུ་སོང་བའི་གནས་ཚུལ་འོག་རྩིས་འཁོར་བཀོལ་སྤྱོད་མི་</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1005"/>
        <source>The data is being compressed to the local disk, please wait patiently...</source>
        <translation>གཞི་གྲངས་དེ་དག་ས་གནས་དེ་གའི་ཁབ་ལེན་དྲ་བར་གནོན་བཙིར་བྱེད་བཞིན་ཡོད་པས་ངང་རྒྱུད་རིང་པོས་སྒུག་རོགས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1010"/>
        <source>Transferring image file to mobile device, about to be completed...</source>
        <translation>པར་རིས་ཡིག་ཆ་དེ་སྒུལ་བདེའི་སྒྲིག་ཆས་སུ་སྤོ་སྒྱུར་བྱས་ནས་མི་རིང་བར་ལེགས་འགྲུབ་བྱ་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1015"/>
        <source>The image creation had been canceled</source>
        <translation>པར་རིས་གསར་རྩོམ་བྱེད་རྒྱུ་མེད་པར་བཟོས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1017"/>
        <source>Re-initiate the image creation if necessary</source>
        <translation>དགོས་ངེས་ཀྱི་སྐབས་སུ་ཡང་བསྐྱར་པར་རིས་གསར་རྩོམ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1050"/>
        <source>An error occurred during make ghost image</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན་བཟོ་བའི་བརྒྱུད་རིམ་ཁྲོད་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1052"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཆ་འཕྲིན་ཞེས་པ་ནི་ཐོ་འགོད་ཡིག་ཚགས་ལ་ཟེར.log</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1169"/>
        <source>Home Page</source>
        <translation>ཁྱིམ་ཚང་གི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1176"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1209"/>
        <source>Ghost image creation is successful</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན་གསར་རྩོམ་ལེགས་འགྲུབ་བྱུང</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1217"/>
        <source>You can view it in the directory : %1</source>
        <translation>ཁྱོད་ཀྱིས་དཀར་ཆག་ནང་དུ་ལྟ་ཞིབ་བྱས་ཆོག་སྟེ། %1</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1231"/>
        <source>Ghost image creation is failed</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན་གསར་རྩོམ་བྱེད་པར་ཕམ་ཉེས་</translation>
    </message>
</context>
<context>
    <name>LeftsiderbarWidget</name>
    <message>
        <location filename="leftsiderbarwidget.cpp" line="56"/>
        <location filename="leftsiderbarwidget.cpp" line="57"/>
        <location filename="leftsiderbarwidget.cpp" line="58"/>
        <location filename="leftsiderbarwidget.cpp" line="161"/>
        <location filename="leftsiderbarwidget.cpp" line="162"/>
        <location filename="leftsiderbarwidget.cpp" line="163"/>
        <source>Backup &amp; Restore</source>
        <translation>རྗེས་གྲབས་དང་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <location filename="maindialog.cpp" line="128"/>
        <source>Options</source>
        <translation>འདེམས་ཚན་</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="129"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="130"/>
        <location filename="maindialog.cpp" line="286"/>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="131"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="175"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="179"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="181"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="281"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན་གྱི་གནས་</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="27"/>
        <source>Backup &amp; Restore</source>
        <translation>རྗེས་གྲབས་དང་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>ManageBackupPointList</name>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="16"/>
        <location filename="module/managebackuppointlist.cpp" line="17"/>
        <source>System Backup Information</source>
        <translation>མ་ལག་གི་རྗེས་གྲབས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="19"/>
        <location filename="module/managebackuppointlist.cpp" line="20"/>
        <source>Data Backup Information</source>
        <translation>གཞི་གྲངས་རྗེས་གྲབས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="32"/>
        <source>You can delete the backup that does not need, refer operation logs for more details</source>
        <translation>ཁྱེད་ཚོས་མི་དགོས་པའི་རྗེས་གྲབས་དཔུང་ཁག་བསུབ་ཆོག་པ་དང་། བཀོལ་སྤྱོད་ཀྱི་ཟིན་ཐོ་དཔྱད་གཞིར་བཟུང་ནས་ཞིབ་ཕྲའི་གནས་ཚུལ་ཞིབ་ཕྲ་ཤེས་སུ་</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="35"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="156"/>
        <source>backup finished</source>
        <translation>རྗེས་གྲབས་ལས་དོན་མཇུག་འགྲིལ་བ།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="158"/>
        <source>backup unfinished</source>
        <translation>ལེགས་འགྲུབ་བྱུང་མེད་པའི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
</context>
<context>
    <name>OperationLog</name>
    <message>
        <location filename="module/operationlog.cpp" line="45"/>
        <source>No operation log</source>
        <translation>བཀོལ་སྤྱོད་ཀྱི་ཟིན་ཐོ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>Backup Name</source>
        <translation>རྗེས་གྲབས་མིང་།</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>UUID</source>
        <translation>UUID</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>Operation</source>
        <translation>བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="55"/>
        <source>Operation Time</source>
        <translation>འཁོར་སྐྱོད་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="159"/>
        <source>new system backup</source>
        <translation>མ་ལག་གསར་པའི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="163"/>
        <source>udpate system backup</source>
        <translation>udpateམ་ལག་གི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="167"/>
        <source>new data backup</source>
        <translation>གཞི་གྲངས་གསར་པའི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="171"/>
        <source>update data backup</source>
        <translation>གཞི་གྲངས་གསར་སྒྱུར་གྱི་རྗེས་གྲབས་</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="175"/>
        <source>restore system</source>
        <translation>མ་ལག་སླར་གསོ་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="179"/>
        <source>restore retaining user data</source>
        <translation>ཉར་ཚགས་བྱས་པའི་སྤྱོད་མཁན་གྱི་གཞི་གྲངས་</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="183"/>
        <source>restore data</source>
        <translation>གཞི་གྲངས་སླར་གསོ་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="187"/>
        <source>delete backup</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="191"/>
        <source>make ghost image</source>
        <translation>གདོན་འདྲེའི་གཟུགས་བརྙན་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../backup-daemon/parsebackuplist.cpp" line="368"/>
        <source>factory backup</source>
        <translation>བཟོ་གྲྭའི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="../common/utils.cpp" line="1277"/>
        <location filename="../common/utils.cpp" line="1289"/>
        <source>Factory Backup</source>
        <translation>བཟོ་གྲྭའི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>Backup State</source>
        <translation>རྗེས་གྲབས་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="64"/>
        <source>PrefixPath</source>
        <translation>སྔོན་འགོག་བྱེད་པའི་ལམ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="328"/>
        <location filename="component/backuplistwidget.cpp" line="337"/>
        <location filename="component/backuplistwidget.cpp" line="360"/>
        <location filename="maindialog.cpp" line="370"/>
        <location filename="maindialog.cpp" line="386"/>
        <location filename="maindialog.cpp" line="410"/>
        <location filename="module/databackup.cpp" line="573"/>
        <location filename="module/databackup.cpp" line="591"/>
        <location filename="module/databackup.cpp" line="839"/>
        <location filename="module/datarestore.cpp" line="1114"/>
        <location filename="module/managebackuppointlist.cpp" line="51"/>
        <location filename="module/selectrestorepoint.cpp" line="51"/>
        <location filename="module/systembackup.cpp" line="411"/>
        <location filename="module/systemrestore.cpp" line="226"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>Path can not include symbols that such as : ``,$(),${},;,&amp;,|,etc.</source>
        <translation type="vanished">འགྲོ་ལམ་ནང་དུ་&quot;$()&quot;དང་།${}&quot;སོགས་ཀྱི་མཚོན་རྟགས་ཚུད་མི་ཐུབ་པ |་དཔེར་ན། &quot;</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="330"/>
        <location filename="component/backuplistwidget.cpp" line="339"/>
        <location filename="component/backuplistwidget.cpp" line="362"/>
        <location filename="main.cpp" line="47"/>
        <location filename="maindialog.cpp" line="372"/>
        <location filename="maindialog.cpp" line="388"/>
        <location filename="maindialog.cpp" line="412"/>
        <location filename="module/databackup.cpp" line="507"/>
        <location filename="module/databackup.cpp" line="525"/>
        <location filename="module/databackup.cpp" line="533"/>
        <location filename="module/databackup.cpp" line="541"/>
        <location filename="module/databackup.cpp" line="575"/>
        <location filename="module/databackup.cpp" line="593"/>
        <location filename="module/databackup.cpp" line="841"/>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/datarestore.cpp" line="1116"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/managebackuppointlist.cpp" line="51"/>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <location filename="module/selectrestorepoint.cpp" line="51"/>
        <location filename="module/systembackup.cpp" line="346"/>
        <location filename="module/systembackup.cpp" line="364"/>
        <location filename="module/systembackup.cpp" line="372"/>
        <location filename="module/systembackup.cpp" line="380"/>
        <location filename="module/systembackup.cpp" line="413"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="329"/>
        <source>Path already exists : </source>
        <translation>འགྲོ་ལམ་ཡོད་པ་གཤམ་གསལ། </translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="338"/>
        <source>The file or directory does not exist : </source>
        <translation>ཡིག་ཆ་དང་དཀར་ཆག་མེད་པ ། </translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="361"/>
        <source>Only data that exists in the follow directorys can be selected: %1.
 Path:%2 is not in them.</source>
        <translation>གཤམ་གྱི་དཀར་ཆག་ནང་དུ་གནས་པའི་གཞི་གྲངས་ཁོ་ན་བདམས་ཆོག་པ་སྟེ། %1
 Path:%2ནི་ཁོ་ཚོའི་ནང་ན་མེད།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="45"/>
        <location filename="module/databackup.cpp" line="505"/>
        <location filename="module/databackup.cpp" line="523"/>
        <location filename="module/databackup.cpp" line="531"/>
        <location filename="module/databackup.cpp" line="539"/>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/datarestore.cpp" line="200"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <location filename="module/systembackup.cpp" line="344"/>
        <location filename="module/systembackup.cpp" line="362"/>
        <location filename="module/systembackup.cpp" line="370"/>
        <location filename="module/systembackup.cpp" line="378"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <source>Information</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="58"/>
        <source>Backup &amp; Restore</source>
        <translation type="unfinished">རྗེས་གྲབས་དང་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>This function can only be used by administrator.</source>
        <translation type="vanished">此功能只能由系统管理员使用。</translation>
    </message>
    <message>
        <location filename="main.cpp" line="46"/>
        <source>Another user had opened kybackup, you can not start it again.</source>
        <translation>སྤྱོད་མཁན་གཞན་ཞིག་གིས་ཁ་ཕྱེ་ཟིན་པས་ཁྱེད་ཀྱིས་ཡང་བསྐྱར་མགོ་རྩོམ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>kybackup</source>
        <translation type="vanished">ཅིན་པུའུ་ལུའུ་ཕུའུ།</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="371"/>
        <source>An exception occurred when mounting backup partition.</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་སྒྲིག་སྦྱོར་བྱེད་སྐབས་དམིགས་བསལ་གྱི་གནས་ཚུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>Please check if the backup partition exists which can be created when you install the Operating System.</source>
        <translation type="vanished">请检查备份还原分区是否存在，在安装操作系统时必须创建备份还原分区。</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="387"/>
        <source>Failed to mount backup partition.</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་བྱེད་མ་ཐུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="411"/>
        <location filename="module/datarestore.cpp" line="1115"/>
        <source>It&apos;s busy, please wait</source>
        <translation>བྲེལ་བ་ཧ་ཅང་ཆེ་བས་སྒུག་དང་།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="506"/>
        <location filename="module/systembackup.cpp" line="345"/>
        <source>Are you sure to continue customizing the path?
The custom path backup file is not protected, which may cause the backup file to be lost or damaged</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་མུ་མཐུད་དུ་ལམ་འདི་གཏན་འཁེལ་བྱེད་དགོས་སམ།
འགག་སྒོའི་ལམ་ཕྲན་གྱི་རྗེས་གྲབས་ཡིག་ཚགས་ལ་སྲུང་སྐྱོབ་མི་བྱེད་པར་རྗེས་གྲབས་ཡིག་ཆ་བོར་བརླག་ཏུ་སོང་བའམ་ཡང་ན་གཏོར་བརླག་ཐེབས་སྲིད།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="524"/>
        <location filename="module/databackup.cpp" line="532"/>
        <location filename="module/systembackup.cpp" line="363"/>
        <location filename="module/systembackup.cpp" line="371"/>
        <source>Cannot nest backups, please select another directory.</source>
        <translation>གྲབས་ཉར་ཆ་ཚང་འཛུད་མི་ཐུབ་།གདམ་ག་བྱེད་རོགས་།དཀར་ཆག་གཞན་དག་།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="540"/>
        <location filename="module/systembackup.cpp" line="379"/>
        <source>Mobile devices can only select the mount directory itself.</source>
        <translation>སྒུལ་བདེའི་སྒྲིག་ཆས་བདམས་ནས་བཀལ་རིགས་དཀར་ཆག་དེ་ཉིད་ལས་།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="574"/>
        <location filename="module/systembackup.cpp" line="412"/>
        <source>Please select backup position</source>
        <translation>རྗེས་གྲབས་ལས་གནས་གདམ་གསེས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="592"/>
        <location filename="module/databackup.cpp" line="840"/>
        <source>Please select a backup file or directory</source>
        <translation>ཁྱེད་ཀྱིས་རྗེས་གྲབས་ཡིག་ཆ་དང་དཀར་ཆག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <source>Are you sure to cancel the operation？</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་གཤག་བཅོས་དེ་མེད་པར་བཟོ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="507"/>
        <location filename="module/databackup.cpp" line="1758"/>
        <location filename="module/datarestore.cpp" line="200"/>
        <location filename="module/ghostimage.cpp" line="890"/>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <location filename="module/systembackup.cpp" line="346"/>
        <location filename="module/systembackup.cpp" line="1255"/>
        <location filename="module/systemrestore.cpp" line="228"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="200"/>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <location filename="module/systemrestore.cpp" line="228"/>
        <source>Continue</source>
        <translation>མུ་མཐུད་དུ་རྒྱུན་</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="200"/>
        <source>Contains the user&apos;s home directory, which need to reboot after restoration. Are you sure to continue?</source>
        <translation>དེའི་ནང་དུ་སྤྱོད་མཁན་གྱི་ཁྱིམ་ཚང་གི་དཀར་ཆག་འདུས་ཡོད་པས་སླར་གསོ་བྱས་རྗེས་བསྐྱར་དུ་འགོ་འཛུགས་དགོས། ཁྱོད་ཀྱིས་ད་དུང་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="51"/>
        <location filename="module/selectrestorepoint.cpp" line="51"/>
        <source>Please select one backup to continue.</source>
        <translation>མུ་མཐུད་དུ་རྗེས་གྲབས་དཔུང་ཁག་ཅིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="56"/>
        <source>Are you sure to delete the backup ?</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་རྗེས་གྲབས་དཔུང་ཁག་དེ་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="64"/>
        <location filename="module/managebackuppointlist.cpp" line="145"/>
        <location filename="module/selectrestorepoint.cpp" line="165"/>
        <source>Customize:</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ནི།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="149"/>
        <location filename="module/selectrestorepoint.cpp" line="172"/>
        <source>Udisk Device:</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་སྒྲིག་ཆས་ནི།</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="65"/>
        <location filename="module/managebackuppointlist.cpp" line="151"/>
        <location filename="module/selectrestorepoint.cpp" line="174"/>
        <source>Local Disk:</source>
        <translation>ས་གནས་ཀྱི་ཁབ་ལེན་འཁོར་ལོ་ནི།</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="57"/>
        <source>Do you want to continue?</source>
        <translation>ཁྱོད་ཀྱིས་ད་དུང་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་དགོས</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="170"/>
        <source>Other machine:</source>
        <translation>འཕྲུལ་འཁོར་གཞན་དག་སྟེ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="227"/>
        <source>Restore factory settings, your system user data will not be retained. Are you sure to continue?</source>
        <translation>བཟོ་གྲྭའི་སྒྲིག་བཀོད་སླར་གསོ་བྱས་ན་ཁྱེད་ཚོའི་མ་ལག་གི་སྤྱོད་མཁན་གྱི་གཞི་གྲངས་སོར་འཇོག་བྱེད་མི་ཐུབ། ཁྱོད་ཀྱིས་ད་དུང་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ</translation>
    </message>
</context>
<context>
    <name>SelectRestorePoint</name>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="14"/>
        <location filename="module/selectrestorepoint.cpp" line="15"/>
        <source>System Backup Information</source>
        <translation>མ་ལག་གི་རྗེས་གྲབས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="17"/>
        <location filename="module/selectrestorepoint.cpp" line="18"/>
        <source>Data Backup Information</source>
        <translation>གཞི་གྲངས་རྗེས་གྲབས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="31"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="65"/>
        <source>Other machine:</source>
        <translation>འཕྲུལ་འཁོར་གཞན་དག་སྟེ།</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="67"/>
        <source>Customize:</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ནི།</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="69"/>
        <source>Udisk Device:</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་སྒྲིག་ཆས་ནི།</translation>
    </message>
</context>
<context>
    <name>SystemBackup</name>
    <message>
        <location filename="module/systembackup.cpp" line="90"/>
        <source>System Backup</source>
        <translation>མ་ལག་གི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="110"/>
        <source>Can be restored when files are damaged or lost</source>
        <translation>ཡིག་ཚགས་ལ་གཏོར་སྐྱོན་ཕོག་པའམ་ཡང་ན་བོར་བརླག་ཏུ་སོང་བའི་སྐབས་སུ་སླར་གསོ་བྱས་</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="121"/>
        <source>Multi-Spot</source>
        <translation>ས་ཆ་མང་པོ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="128"/>
        <source>Small Size</source>
        <translation>གཞི་ཁྱོན་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="135"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="142"/>
        <source>Simple</source>
        <translation>སྟབས་བདེ་</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="148"/>
        <source>Start Backup</source>
        <translation>གསར་འཛུགས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="165"/>
        <source>Backup Management &gt;&gt;</source>
        <translation>རྗེས་གྲབས་དོ་དམ་&gt;&gt;</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="245"/>
        <source>Please select backup position</source>
        <translation>རྗེས་གྲབས་ལས་གནས་གདམ་གསེས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="311"/>
        <source>local default path : </source>
        <translation>ས་གནས་དེ་གའི་ཁ་ཆད་དང་འགལ་ </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="314"/>
        <source>removable devices path : </source>
        <translation>གནས་སྤོ་ཐུབ་པའི་སྒྲིག་ཆས་ཀྱི་ལམ་བུ། </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="276"/>
        <location filename="module/systembackup.cpp" line="554"/>
        <location filename="module/systembackup.cpp" line="979"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="263"/>
        <source>Browse</source>
        <translation>བལྟས་པ་ཙམ་གྱིས་</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="285"/>
        <location filename="module/systembackup.cpp" line="562"/>
        <location filename="module/systembackup.cpp" line="991"/>
        <source>Next</source>
        <translation>གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="324"/>
        <location filename="module/systembackup.cpp" line="396"/>
        <source>customize path : </source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་ལམ་བུ། </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="455"/>
        <location filename="module/systembackup.cpp" line="855"/>
        <location filename="module/systembackup.cpp" line="1131"/>
        <location filename="module/systembackup.cpp" line="1463"/>
        <source>checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="458"/>
        <location filename="module/systembackup.cpp" line="858"/>
        <location filename="module/systembackup.cpp" line="1134"/>
        <location filename="module/systembackup.cpp" line="1466"/>
        <source>preparing</source>
        <translation>གྲ་སྒྲིག་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="461"/>
        <location filename="module/systembackup.cpp" line="861"/>
        <location filename="module/systembackup.cpp" line="1137"/>
        <location filename="module/systembackup.cpp" line="1469"/>
        <source>backuping</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="464"/>
        <location filename="module/systembackup.cpp" line="864"/>
        <location filename="module/systembackup.cpp" line="1140"/>
        <location filename="module/systembackup.cpp" line="1472"/>
        <source>finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="573"/>
        <source>Recheck</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="604"/>
        <source>Checking, wait a moment ...</source>
        <translation>ཞིབ་བཤེར་བྱས་ནས་ཅུང་ཙམ་སྒུགས་དང་། ...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="610"/>
        <source>Do not perform other operations during backup to avoid data loss</source>
        <translation>རྗེས་གྲབས་བྱེད་རིང་གཞི་གྲངས་བོར་བརླག་མི་ཡོང་བའི་ཆེད་དུ་ལས་ཀ་གཞན་དག་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="613"/>
        <source>Check whether the remaining capacity of the backup partition is sufficient</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་ཀྱི་ལྷག་མའི་ཤོང་ཚད་འདང་མིན་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="616"/>
        <source>Check whether the remaining capacity of the removable device is sufficient</source>
        <translation>གནས་སྤོ་ཐུབ་པའི་སྒྲིག་ཆས་ཀྱི་ལྷག་འཕྲོའི་ཤོང་ཚད་འདང་མིན་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="635"/>
        <source>Check success</source>
        <translation>ཞིབ་བཤེར་ལེགས་འགྲུབ་བྱུང་</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="637"/>
        <source>The storage for backup is enough</source>
        <translation>རྗེས་གྲབས་གསོག་འཇོག་བྱས་པ་འདང་ངེས་ཤིག་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="642"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>གློག་ཀླད་ནང་དུ་འཇུག་པའམ་ཡང་ན་གློག་གཡིས་ཀྱི་ཆུ་ཚད་བརྒྱ་ཆ་60ཡན་ཟིན་པར་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="653"/>
        <source>Check failure</source>
        <translation>ཞིབ་བཤེར་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="771"/>
        <location filename="module/systembackup.cpp" line="1315"/>
        <source>Program lock failed, please retry</source>
        <translation>གོ་རིམ་གྱི་ཟྭ་ལ་སྐྱོན་ཤོར་བ་དང་། བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="773"/>
        <location filename="module/systembackup.cpp" line="1317"/>
        <source>There may be other backups or restores being performed</source>
        <translation>ད་དུང་ལག་བསྟར་བྱེད་བཞིན་པའི་རྗེས་གྲབས་དཔུང་ཁག་གམ་ཡང་ན་སླར་གསོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="777"/>
        <location filename="module/systembackup.cpp" line="1321"/>
        <source>Unsupported task type</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ལས་འགན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="779"/>
        <location filename="module/systembackup.cpp" line="1323"/>
        <source>No processing logic was found in the service</source>
        <translation>ཞབས་ཞུའི་ཁྲོད་དུ་ཐག་གཅོད་བྱེད་པའི་གཏན་ཚིགས་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="783"/>
        <location filename="module/systembackup.cpp" line="1327"/>
        <source>Failed to mount the backup partition</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་བྱས་ནས་སྒྲིག་སྦྱོར་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="785"/>
        <location filename="module/systembackup.cpp" line="1329"/>
        <source>Check whether there is a backup partition</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="789"/>
        <location filename="module/systembackup.cpp" line="1333"/>
        <source>The filesystem of device is vfat format</source>
        <translation>སྒྲིག་ཆས་ཀྱི་ཡིག་ཚགས་མ་ལག་ནི་vfatཡི་རྣམ་གཞག་ཡིན།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="791"/>
        <location filename="module/systembackup.cpp" line="1335"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>ཡིག་ཚགས་མ་ལག་གི་རྣམ་གཞག་དེ་ext3、ext4འམ་ཡང་ན་ntfsལ་བསྒྱུར་རོགས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="795"/>
        <location filename="module/systembackup.cpp" line="1339"/>
        <source>The device is read only</source>
        <translation>སྒྲིག་ཆས་འདི་ཁོ་ནར་བལྟས་པ་ཙམ་ཡིན།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="797"/>
        <location filename="module/systembackup.cpp" line="1341"/>
        <source>Please chmod to rw</source>
        <translation>ཁྱེད་ཀྱིས་ཆི་མོ་ཏི་ལ་གུས་ལུགས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="801"/>
        <location filename="module/systembackup.cpp" line="1345"/>
        <source>The storage for backup is not enough</source>
        <translation>རྗེས་གྲབས་གསོག་འཇོག་བྱས་པ་མི་འདང་བ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="803"/>
        <location filename="module/systembackup.cpp" line="1347"/>
        <source>Retry after release space</source>
        <translation>བར་སྟོང་འགྲེམ་སྤེལ་བྱས་རྗེས་ཡང་བསྐྱར་ཞིབ་བཤེར་བྱས</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="807"/>
        <location filename="module/systembackup.cpp" line="1351"/>
        <source>Other backup or restore task is being performed</source>
        <translation>ད་དུང་རྗེས་གྲབས་ལས་འགན་གཞན་དག་གམ་ཡང་ན་སླར་གསོ་བྱེད་པའི་ལས་འགན</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="809"/>
        <location filename="module/systembackup.cpp" line="1353"/>
        <source>Please try again later</source>
        <translation>ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="888"/>
        <source>Backup Name</source>
        <translation>རྗེས་གྲབས་མིང་།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="934"/>
        <source>Maximum length reached</source>
        <translation>རིང་ཚད་ཡས་མཐའི་ཚད་ལ་སླེབས་འདུག</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="944"/>
        <source>Unsupported symbol : </source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་རྟགས། </translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="938"/>
        <location filename="module/systembackup.cpp" line="952"/>
        <location filename="module/systembackup.cpp" line="1006"/>
        <source>Name already exists</source>
        <translation>མིང་ཡོད་པ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1071"/>
        <source>factory backup</source>
        <translation>བཟོ་གྲྭའི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1212"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1236"/>
        <source>Do not use computer in case of data loss</source>
        <translation>གཞི་གྲངས་བོར་བརླག་ཏུ་སོང་བའི་གནས་ཚུལ་འོག་རྩིས་འཁོར་བཀོལ་སྤྱོད་མི་</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1359"/>
        <source>Failed to create the backup point directory</source>
        <translation>རྗེས་གྲབས་ས་གནས་ཀྱི་དཀར་ཆག་གསར་སྐྲུན་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1361"/>
        <source>Please check backup partition permissions</source>
        <translation>རྗེས་གྲབས་ཁག་བགོས་ཀྱི་ཆོག་འཐུས་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1365"/>
        <source>The system is being compressed to the local disk, please wait patiently...</source>
        <translation>མ་ལག་དེ་ས་གནས་དེ་གའི་ཁབ་ལེན་དྲ་བར་གནོན་བཙིར་བྱེད་བཞིན་ཡོད་པས་ངང་རྒྱུད་རིང་པོས་སྒུག་རོགས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1373"/>
        <source>Transferring image file to mobile device, about to be completed...</source>
        <translation>པར་རིས་ཡིག་ཆ་དེ་སྒུལ་བདེའི་སྒྲིག་ཆས་སུ་སྤོ་སྒྱུར་བྱས་ནས་མི་རིང་བར་ལེགས་འགྲུབ་བྱ་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1378"/>
        <source>The backup had been canceled</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག་དེ་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1380"/>
        <source>Re-initiate the backup if necessary</source>
        <translation>དགོས་ངེས་ཀྱི་སྐབས་སུ་ཡང་བསྐྱར་རྗེས་གྲབས་དཔུང་ཁག་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1393"/>
        <location filename="module/systembackup.cpp" line="1419"/>
        <source>An error occurred during backup</source>
        <translation>རྗེས་གྲབས་བྱེད་རིང་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1395"/>
        <location filename="module/systembackup.cpp" line="1421"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཆ་འཕྲིན་ཞེས་པ་ནི་ཐོ་འགོད་ཡིག་ཚགས་ལ་ཟེར.log</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1553"/>
        <source>Home Page</source>
        <translation>ཁྱིམ་ཚང་གི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1560"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1585"/>
        <source>The backup is successful</source>
        <translation>རྗེས་གྲབས་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1599"/>
        <source>The backup is failed</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག་ལ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>SystemRestore</name>
    <message>
        <location filename="module/systemrestore.cpp" line="82"/>
        <source>System Restore</source>
        <translation>མ་ལག་སླར་གསོ</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="100"/>
        <source>You can restore the system to its previous state</source>
        <translation>ཁྱེད་ཚོས་མ་ལག་དེ་སྔོན་ཆད་ཀྱི་རྣམ་པ་སླར་གསོ་བྱས་ཆོག།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="111"/>
        <source>Simple</source>
        <translation>སྟབས་བདེ་</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="118"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="125"/>
        <source>Repair</source>
        <translation>ཞིག་གསོ་</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="132"/>
        <source>Independent</source>
        <translation>རང་རྐྱ་འཕེར་བའི</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="138"/>
        <source>Start Restore</source>
        <translation>འགོ་ཚུགས་པ་</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="150"/>
        <source>Factory Restore</source>
        <translation>བཟོ་གྲྭ་སླར་གསོ་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="155"/>
        <source>Retaining User Data</source>
        <translation>སྤྱོད་མཁན་གྱི་གཞི་གྲངས་སོར་འཇོག་བྱ</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="281"/>
        <location filename="module/systemrestore.cpp" line="659"/>
        <location filename="module/systemrestore.cpp" line="896"/>
        <source>checking</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="285"/>
        <location filename="module/systemrestore.cpp" line="663"/>
        <location filename="module/systemrestore.cpp" line="900"/>
        <source>restoring</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="289"/>
        <location filename="module/systemrestore.cpp" line="667"/>
        <location filename="module/systemrestore.cpp" line="904"/>
        <source>finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="371"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="379"/>
        <source>Next</source>
        <translation>གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="390"/>
        <source>Recheck</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="427"/>
        <source>Checking, wait a moment ...</source>
        <translation>ཞིབ་བཤེར་བྱས་ནས་ཅུང་ཙམ་སྒུགས་དང་། ...</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="433"/>
        <source>Check whether the restore environment meets the requirements</source>
        <translation>སླར་གསོ་བྱེད་པའི་ཁོར་ཡུག་དེ་བླང་བྱ་དང་མཐུན་མིན་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="435"/>
        <source>Do not perform other operations during restore to avoid data loss</source>
        <translation>གཞི་གྲངས་བོར་བརླག་མི་ཡོང་བའི་ཆེད་དུ་སླར་གསོ་བྱེད་རིང་ལས་སྒོ་གཞན་དག་མི་སྒྲུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="453"/>
        <source>Check success</source>
        <translation>ཞིབ་བཤེར་ལེགས་འགྲུབ་བྱུང་</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="455"/>
        <source>The system will reboot automatically after the restore is successful</source>
        <translation>སླར་གསོ་བྱས་རྗེས་མ་ལག་རང་འགུལ་གྱིས་བསྐྱར་དུ་ཐོན་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="460"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>གློག་ཀླད་ནང་དུ་འཇུག་པའམ་ཡང་ན་གློག་གཡིས་ཀྱི་ཆུ་ཚད་བརྒྱ་ཆ་60ཡན་ཟིན་པར་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="471"/>
        <source>Check failure</source>
        <translation>ཞིབ་བཤེར་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="590"/>
        <location filename="module/systemrestore.cpp" line="789"/>
        <source>Program lock failed, please retry</source>
        <translation>གོ་རིམ་གྱི་ཟྭ་ལ་སྐྱོན་ཤོར་བ་དང་། བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="592"/>
        <location filename="module/systemrestore.cpp" line="791"/>
        <source>There may be other backups or restores being performed</source>
        <translation>ད་དུང་ལག་བསྟར་བྱེད་བཞིན་པའི་རྗེས་གྲབས་དཔུང་ཁག་གམ་ཡང་ན་སླར་གསོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="596"/>
        <location filename="module/systemrestore.cpp" line="795"/>
        <source>Unsupported task type</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ལས་འགན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="598"/>
        <location filename="module/systemrestore.cpp" line="797"/>
        <source>No processing logic was found in the service</source>
        <translation>ཞབས་ཞུའི་ཁྲོད་དུ་ཐག་གཅོད་བྱེད་པའི་གཏན་ཚིགས་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="602"/>
        <location filename="module/systemrestore.cpp" line="801"/>
        <source>The .user.txt file does not exist</source>
        <translation>.user.txt ཡིག་ཆ་གནས་མེད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="604"/>
        <location filename="module/systemrestore.cpp" line="610"/>
        <location filename="module/systemrestore.cpp" line="616"/>
        <location filename="module/systemrestore.cpp" line="803"/>
        <location filename="module/systemrestore.cpp" line="809"/>
        <location filename="module/systemrestore.cpp" line="815"/>
        <source>Backup points may be corrupted</source>
        <translation>རྗེས་གྲབས་ས་ཚིགས་རུལ་སུངས་སུ་འགྱུར་སྲིད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="608"/>
        <location filename="module/systemrestore.cpp" line="807"/>
        <source>The .exclude.user.txt file does not exist</source>
        <translation>.exclude.user.txtཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="614"/>
        <location filename="module/systemrestore.cpp" line="813"/>
        <source>The backup point data directory does not exist</source>
        <translation>རྗེས་གྲབས་ས་གནས་ཀྱི་གཞི་གྲངས་དཀར་ཆག་མེད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="620"/>
        <location filename="module/systemrestore.cpp" line="819"/>
        <source>Failed to rsync /boot/efi</source>
        <translation>rsync /boot/efiལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="622"/>
        <location filename="module/systemrestore.cpp" line="821"/>
        <source>Check the mounting mode of the /boot/efi partition</source>
        <translation>/boot/efiས་ཁུལ་གྱི་སྒྲིག་སྦྱོར་བྱེད་སྟངས་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="707"/>
        <source>Do not use computer in case of data loss</source>
        <translation>གཞི་གྲངས་བོར་བརླག་ཏུ་སོང་བའི་གནས་ཚུལ་འོག་རྩིས་འཁོར་བཀོལ་སྤྱོད་མི་</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="825"/>
        <source>Failed to prepare the restore directory</source>
        <translation>སླར་གསོ་བྱས་པའི་དཀར་ཆག་གྲ་སྒྲིག་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="827"/>
        <source>Refer to log :/var/log/backup.log for more information</source>
        <translation>དཔྱད་གཞིའི་ཡིག་ཆ་དེ་བས་མང་བ་མཁོ་འདོན་བྱ་རྒྱུའི་རེ་བ་ཡོད.log་ཅེས་གསུངས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="860"/>
        <source>An error occurred during restore</source>
        <translation>སླར་གསོ་བྱེད་རིང་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="862"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཆ་འཕྲིན་ཞེས་པ་ནི་ཐོ་འགོད་ཡིག་ཚགས་ལ་ཟེར.log</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="979"/>
        <source>Home Page</source>
        <translation>ཁྱིམ་ཚང་གི་ཤོག་ལྷེ</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="986"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1019"/>
        <source>Successfully restoring the system</source>
        <translation>མ་ལག་བདེ་བླག་ངང་སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1025"/>
        <source>The system will automatically reboot.</source>
        <translation>མ་ལག་རང་འགུལ་གྱིས་བསྐྱར་དུ་འཁོར་སྐྱོད་བྱེད་སྲིད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1026"/>
        <source>Or not, please force shutdown and restart.</source>
        <translation>དེ་མིན་ལག་རྩལ་ཐོག་ལས་ཨུ་ཚུགས་བཀོལ་ནས་སླར་ལོག་འབད།</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="1033"/>
        <source>Restoring the system failed</source>
        <translation>མ་ལག་སླར་གསོ་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>restore</name>
    <message>
        <location filename="main.cpp" line="129"/>
        <source>system restore</source>
        <translation>མ་ལག་སླར་གསོ་བྱེད་པ</translation>
    </message>
</context>
</TS>
