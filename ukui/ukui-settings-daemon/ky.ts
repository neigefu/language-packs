<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky" sourcelanguage="en">
<context>
    <name>A11yKeyboardManager</name>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="491"/>
        <source>There was an error displaying help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="653"/>
        <source>Do you want to activate Slow Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="654"/>
        <source>Do you want to deactivate Slow Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="655"/>
        <source>You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="664"/>
        <source>Slow Keys Alert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="668"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="800"/>
        <source>Do_n&apos;t activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="668"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="800"/>
        <source>Do_n&apos;t deactivate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="670"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="802"/>
        <source>_Activate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="670"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="802"/>
        <source>_Deactivate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="672"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="803"/>
        <source>input-keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="782"/>
        <source>Do you want to activate Sticky Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="783"/>
        <source>Do you want to deactivate Sticky Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="785"/>
        <source>You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="787"/>
        <source>You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="796"/>
        <source>Sticky Keys Alert</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>A11yPreferencesDialog</name>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="35"/>
        <source>Use on-screen _keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="48"/>
        <source>Use screen _reader</source>
        <translatorcomment>使用屏幕阅读器</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="55"/>
        <source>Use screen _magnifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="62"/>
        <source>Enhance _contrast in colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="69"/>
        <source>Make _text larger and easier to read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="76"/>
        <source>Press keyboard shortcuts one key at a time (Sticky Keys)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="83"/>
        <source>Ignore duplicate keypresses (Bounce Keys)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="90"/>
        <source>Press and _hold keys to accept them (Slow Keys)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="106"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceWindow</name>
    <message>
        <location filename="../../../plugins/media-keys/devicewindow.ui" line="14"/>
        <source>DeviceWindow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../../../plugins/keyboard/keyboardwidget.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LdsmDialog</name>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.ui" line="14"/>
        <source>LdsmDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="92"/>
        <source>Low Disk Space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="120"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="125"/>
        <source>Empty Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="129"/>
        <source>Examine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="142"/>
        <source>The volume &quot;%1&quot; has only %s disk space remaining.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="145"/>
        <source>The computer has only %s disk space remaining.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="154"/>
        <source>You can free up disk space by emptying the Trash, removing unused programs or files, or moving files to another disk or partition.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="157"/>
        <source>You can free up disk space by removing unused programs or files, or by moving files to another disk or partition.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="161"/>
        <source>You can free up disk space by emptying the Trash, removing unused programs or files, or moving files to an external disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="164"/>
        <source>You can free up disk space by removing unused programs or files, or by moving files to an external disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="172"/>
        <source>Don&apos;t show any warnings again for this file system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="174"/>
        <source>Don&apos;t show any warnings again</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LdsmTrashEmpty</name>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="60"/>
        <source>Emptying the trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="76"/>
        <source>Empty all of the items from the trash?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="80"/>
        <source>If you choose to empty the trash, all items in it will be permanently lost.Please note that you can also delete them separately.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="83"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="85"/>
        <source>Empty Trash</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error while trying to run &quot;%1&quot;;  
 which is linked to the key &quot;%2&quot;</source>
        <translation type="vanished">试图运行&quot;%1&quot;时出错; 链接到键&quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../../../plugins/keybindings/keybindings-manager.cpp" line="403"/>
        <source>Error while trying to run &quot;%1&quot;;
 which is linked to the key &quot;%2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/keybindings/keybindings-manager.cpp" line="406"/>
        <source>Shortcut message box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/keybindings/keybindings-manager.cpp" line="409"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/keyboard/keyboard-xkb.cpp" line="167"/>
        <source>Error activating XKB configuration.
It can happen under various circumstances:
 • a bug in libxklavier library
 • a bug in X server (xkbcomp, xmodmap utilities)
 • X server with incompatible libxkbfile implementation

X server version data:
 %1 
 %2 
If you report this situation as a bug, please include:
 • The result of &lt;b&gt; xprop -root | grep XKB &lt;/b&gt;
 • The result of &lt;b&gt; gsettings list-keys org.mate.peripherals-keyboard-xkb.kbd &lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/keyboard/keyboard-xkb.cpp" line="177"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/keyboard/keyboard-xkb.cpp" line="178"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="574"/>
        <source>Do you want to activate Slow Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="575"/>
        <source>Do you want to deactivate Slow Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="577"/>
        <source>You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="708"/>
        <source>Do you want to activate Sticky Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="709"/>
        <source>Do you want to deactivate Sticky Keys?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="711"/>
        <source>You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="713"/>
        <source>You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The system detects that the HD device has been replaced.Do you need to switch to the recommended zoom (100%)? Click on the confirmation logout.</source>
        <translation type="vanished">系统检测到高清设备已被更换。您是否需要切换到建议的缩放比例（100%）？点击确认后会注销生效。</translation>
    </message>
    <message>
        <source>Scale tips</source>
        <translation type="vanished">缩放提示</translation>
    </message>
    <message>
        <source>Confirmation</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Does the system detect high clear equipment and whether to switch to recommended scaling (200%)? Click on the confirmation logout.</source>
        <translation type="vanished">系统检测到高清设备，您是否切换到建议的缩放（200%）？点击确认后会注销生效。</translation>
    </message>
</context>
<context>
    <name>VolumeWindow</name>
    <message>
        <location filename="../../../plugins/media-keys/volumewindow.ui" line="14"/>
        <source>VolumeWindow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
