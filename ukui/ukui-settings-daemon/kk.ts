<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk" sourcelanguage="en">
<context>
    <name>A11yKeyboardManager</name>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="491"/>
        <source>There was an error displaying help</source>
        <translation>Анықтаманы көрсететін қате пайда болды</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="653"/>
        <source>Do you want to activate Slow Keys?</source>
        <translation>Баяу пернелерді белсендіргіңіз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="654"/>
        <source>Do you want to deactivate Slow Keys?</source>
        <translation>Баяу пернелерді ажыратқыңыз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="655"/>
        <source>You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works.</source>
        <translation>Shift пернесін 8 секунд бойы ғана ұстап тұрдыңыз.  Бұл баяу пернелер тіркесімі мүмкіндігіне арналған пернелер тіркесімі, ол пернелер тіркесімінің жұмыс істеу тәсіліне әсер етеді.</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="664"/>
        <source>Slow Keys Alert</source>
        <translation>Баяу пернелер ескертуі</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="668"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="800"/>
        <source>Do_n&apos;t activate</source>
        <translation>Do_n белсендірмеу</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="668"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="800"/>
        <source>Do_n&apos;t deactivate</source>
        <translation>Do_n істен шықпайды</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="670"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="802"/>
        <source>_Activate</source>
        <translation>_Activate</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="670"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="802"/>
        <source>_Deactivate</source>
        <translation>_Deactivate</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="672"/>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="803"/>
        <source>input-keyboard</source>
        <translation>енгізу-пернетақта</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="782"/>
        <source>Do you want to activate Sticky Keys?</source>
        <translation>Жабысқақ пернелерді белсендіргіңіз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="783"/>
        <source>Do you want to deactivate Sticky Keys?</source>
        <translation>Жабысқақ пернелерді ажыратқыңыз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="785"/>
        <source>You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation>Shift пернесін қатарынан 5 рет басып тұрдыңыз.  Бұл пернелер тіркесімінің жұмыс істеу тәсіліне әсер ететін Жабысқақ пернелер тіркесімі.</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="787"/>
        <source>You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation>Екі кілтті бірден басып тұрсаңыз немесе Shift пернесін қатарынан 5 рет басыңыз.  Бұл пернетақтаның жұмыс істеу тәсіліне әсер ететін Жабысқақ пернелер мүмкіндігін өшіреді.</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="796"/>
        <source>Sticky Keys Alert</source>
        <translation>Жабысқақ пернелер ескертуі</translation>
    </message>
</context>
<context>
    <name>A11yPreferencesDialog</name>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="14"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="35"/>
        <source>Use on-screen _keyboard</source>
        <translation>Экрандағы _keyboard пайдалану</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="48"/>
        <source>Use screen _reader</source>
        <translatorcomment>使用屏幕阅读器</translatorcomment>
        <translation>Экран _reader пайдалану</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="55"/>
        <source>Use screen _magnifier</source>
        <translation>Экран _magnifier пайдалану</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="62"/>
        <source>Enhance _contrast in colors</source>
        <translation>Түстердегі _contrast күшейту</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="69"/>
        <source>Make _text larger and easier to read</source>
        <translation>_text үлкенірек және оқуды жеңілдету</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="76"/>
        <source>Press keyboard shortcuts one key at a time (Sticky Keys)</source>
        <translation>Пернелер тіркесімін бір уақытта басу (Жабысқақ пернелер)</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="83"/>
        <source>Ignore duplicate keypresses (Bounce Keys)</source>
        <translation>Қайталанатын пернелерді елемеу (Bounce пернелері)</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="90"/>
        <source>Press and _hold keys to accept them (Slow Keys)</source>
        <translation>Оларды қабылдау үшін пернелерді басу және _hold (Баяу пернелер)</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-preferences-dialog.ui" line="106"/>
        <source>close</source>
        <translation>жабу</translation>
    </message>
</context>
<context>
    <name>DeviceWindow</name>
    <message>
        <location filename="../../../plugins/media-keys/devicewindow.ui" line="14"/>
        <source>DeviceWindow</source>
        <translation>DeviceWideow</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../../../plugins/keyboard/keyboardwidget.ui" line="16"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
<context>
    <name>LdsmDialog</name>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.ui" line="14"/>
        <source>LdsmDialog</source>
        <translation>LdsmDialog</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="92"/>
        <source>Low Disk Space</source>
        <translation>Диск кеңістігі төмен</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="120"/>
        <source>Ignore</source>
        <translation>Елемеу</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="125"/>
        <source>Empty Trash</source>
        <translation>Себетті босату</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="129"/>
        <source>Examine</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="142"/>
        <source>The volume &quot;%1&quot; has only %s disk space remaining.</source>
        <translation>&quot;% 1&quot; көлемінде тек % дискідегі бос орын ғана қалды.</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="145"/>
        <source>The computer has only %s disk space remaining.</source>
        <translation>Компьютерде тек % дискідегі бос орын ғана қалды.</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="154"/>
        <source>You can free up disk space by emptying the Trash, removing unused programs or files, or moving files to another disk or partition.</source>
        <translation>Себетті босату, пайдаланылмаған бағдарламаларды немесе файлдарды жою немесе файлдарды басқа дискіге немесе қалқаға жылжыту арқылы дискідегі бос орынды босатуға болады.</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="157"/>
        <source>You can free up disk space by removing unused programs or files, or by moving files to another disk or partition.</source>
        <translation>Пайдаланылмаған бағдарламаларды немесе файлдарды жою арқылы немесе файлдарды басқа дискіге немесе қалқаға жылжыту арқылы дискі кеңістігін босатуға болады.</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="161"/>
        <source>You can free up disk space by emptying the Trash, removing unused programs or files, or moving files to an external disk.</source>
        <translation>Себетті босату, пайдаланылмаған бағдарламаларды немесе файлдарды жою немесе файлдарды сыртқы дискіге жылжыту арқылы дискідегі бос орынды босатуға болады.</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="164"/>
        <source>You can free up disk space by removing unused programs or files, or by moving files to an external disk.</source>
        <translation>Пайдаланылмаған бағдарламаларды немесе файлдарды жою арқылы немесе файлдарды сыртқы дискіге жылжыту арқылы дискі кеңістігін босатуға болады.</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="172"/>
        <source>Don&apos;t show any warnings again for this file system</source>
        <translation>Бұл файл жүйесі үшін ескертулерді қайта көрсетпеңіз</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/usd-ldsm-dialog.cpp" line="174"/>
        <source>Don&apos;t show any warnings again</source>
        <translation>Ескертулерді қайта көрсетпеңіз</translation>
    </message>
</context>
<context>
    <name>LdsmTrashEmpty</name>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.ui" line="14"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="60"/>
        <source>Emptying the trash</source>
        <translation>Себетті босату</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="76"/>
        <source>Empty all of the items from the trash?</source>
        <translation>Барлық элементтерді себеттен босатыңыз ба?</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="80"/>
        <source>If you choose to empty the trash, all items in it will be permanently lost.Please note that you can also delete them separately.</source>
        <translation>Егер себетті босатуды таңдасаңыз, онда барлық элементтер біржола жоғалады. Оларды бөлек жоюға болатынын ескеріңіз.</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="83"/>
        <source>cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../../../plugins/housekeeping/ldsm-trash-empty.cpp" line="85"/>
        <source>Empty Trash</source>
        <translation>Себетті босату</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Error while trying to run &quot;%1&quot;;  
 which is linked to the key &quot;%2&quot;</source>
        <translation type="vanished">试图运行&quot;%1&quot;时出错; 链接到键&quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../../../plugins/keybindings/keybindings-manager.cpp" line="403"/>
        <source>Error while trying to run &quot;%1&quot;;
 which is linked to the key &quot;%2&quot;</source>
        <translation>&quot;% 1&quot; дегенді іске қосу кезінде қате пайда болды;
 бұл &quot;% 2&quot; кілтімен байланысқан</translation>
    </message>
    <message>
        <location filename="../../../plugins/keybindings/keybindings-manager.cpp" line="406"/>
        <source>Shortcut message box</source>
        <translation>Пернелер тіркесімі</translation>
    </message>
    <message>
        <location filename="../../../plugins/keybindings/keybindings-manager.cpp" line="409"/>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location filename="../../../plugins/keyboard/keyboard-xkb.cpp" line="167"/>
        <source>Error activating XKB configuration.
It can happen under various circumstances:
 • a bug in libxklavier library
 • a bug in X server (xkbcomp, xmodmap utilities)
 • X server with incompatible libxkbfile implementation

X server version data:
 %1 
 %2 
If you report this situation as a bug, please include:
 • The result of &lt;b&gt; xprop -root | grep XKB &lt;/b&gt;
 • The result of &lt;b&gt; gsettings list-keys org.mate.peripherals-keyboard-xkb.kbd &lt;/b&gt;</source>
        <translation>XKB баптауларын белсендіру қатесі.
Ол әр түрлі жағдайларда орын алуы мүмкін:
 • libxklavier кітапханасындағы қате
 • X серверіндегі қате (xkbcomp, xmodmap утилиталары)
 • үйлеспейтін libxkbfile іске асыруы бар X сервері

X сервер нұсқасының деректері:
 %1 
 %2 
Егер бұл жағдайды қате ретінде хабарласаңыз, мынаны көрсетіңіз:
 • &lt;b&gt; xprop -root нәтижесі | grep XKB &lt;/b&gt;
 • &lt;b&gt; gsettings list-keys org.mate.peripherals-keyboard-xkb.kbd &lt;/b&gt; нәтижесі</translation>
    </message>
    <message>
        <location filename="../../../plugins/keyboard/keyboard-xkb.cpp" line="177"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location filename="../../../plugins/keyboard/keyboard-xkb.cpp" line="178"/>
        <source>Error</source>
        <translation>Қате</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="574"/>
        <source>Do you want to activate Slow Keys?</source>
        <translation>Баяу пернелерді белсендіргіңіз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="575"/>
        <source>Do you want to deactivate Slow Keys?</source>
        <translation>Баяу пернелерді ажыратқыңыз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="577"/>
        <source>You just held down the Shift key for 8 seconds.  This is the shortcut for the Slow Keys feature, which affects the way your keyboard works.</source>
        <translation>Shift пернесін 8 секунд бойы ғана ұстап тұрдыңыз.  Бұл баяу пернелер тіркесімі мүмкіндігіне арналған пернелер тіркесімі, ол пернелер тіркесімінің жұмыс істеу тәсіліне әсер етеді.</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="708"/>
        <source>Do you want to activate Sticky Keys?</source>
        <translation>Жабысқақ пернелерді белсендіргіңіз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="709"/>
        <source>Do you want to deactivate Sticky Keys?</source>
        <translation>Жабысқақ пернелерді ажыратқыңыз келе ме?</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="711"/>
        <source>You just pressed the Shift key 5 times in a row.  This is the shortcut for the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation>Shift пернесін қатарынан 5 рет басып тұрдыңыз.  Бұл пернелер тіркесімінің жұмыс істеу тәсіліне әсер ететін Жабысқақ пернелер тіркесімі.</translation>
    </message>
    <message>
        <location filename="../../../plugins/a11y-keyboard/a11y-keyboard-manager.cpp" line="713"/>
        <source>You just pressed two keys at once, or pressed the Shift key 5 times in a row.  This turns off the Sticky Keys feature, which affects the way your keyboard works.</source>
        <translation>Екі кілтті бірден басып тұрсаңыз немесе Shift пернесін қатарынан 5 рет басыңыз.  Бұл пернетақтаның жұмыс істеу тәсіліне әсер ететін Жабысқақ пернелер мүмкіндігін өшіреді.</translation>
    </message>
    <message>
        <source>The system detects that the HD device has been replaced.Do you need to switch to the recommended zoom (100%)? Click on the confirmation logout.</source>
        <translation type="vanished">系统检测到高清设备已被更换。您是否需要切换到建议的缩放比例（100%）？点击确认后会注销生效。</translation>
    </message>
    <message>
        <source>Scale tips</source>
        <translation type="vanished">缩放提示</translation>
    </message>
    <message>
        <source>Confirmation</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Does the system detect high clear equipment and whether to switch to recommended scaling (200%)? Click on the confirmation logout.</source>
        <translation type="vanished">系统检测到高清设备，您是否切换到建议的缩放（200%）？点击确认后会注销生效。</translation>
    </message>
</context>
<context>
    <name>VolumeWindow</name>
    <message>
        <location filename="../../../plugins/media-keys/volumewindow.ui" line="14"/>
        <source>VolumeWindow</source>
        <translation>VolodWindow</translation>
    </message>
</context>
</TS>
