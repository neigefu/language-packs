<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="147"/>
        <source>Content index incomplete.</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶᠢᠨ ᠬᠡᠯᠬᠢᠶᠡᠰᠦ ᠪᠦᠷᠢᠨ ᠪᠤᠰᠤ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-search-task.cpp" line="157"/>
        <source>Warning, Can not find home path.</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠦᠯ᠂ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppMatch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ᠎ᠶᠢᠨ ᠳᠦᠷᠰᠦᠯᠡᠯ ᠄</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchPlugin</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="31"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="245"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="32"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="246"/>
        <source>Add Shortcut to Desktop</source>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤᠨ ᠲᠦᠳᠡ ᠴᠦᠷᠬᠡ᠎ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ᠎ᠳᠤ ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="247"/>
        <source>Add Shortcut to Panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠬᠡ᠎ᠶᠢᠨ ᠲᠦᠲᠡ ᠴᠦᠷᠬᠡ᠎ᠳᠦ ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="34"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="248"/>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="64"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="69"/>
        <source>Applications Search</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="152"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="194"/>
        <source>Application Description:</source>
        <translation>应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="52"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="57"/>
        <source>Application search.</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠵᠢ ᠬᠠᠢᠬᠤ.</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::DirSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="243"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="386"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="244"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="387"/>
        <source>Open path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="245"/>
        <source>Copy Path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="264"/>
        <source>Dir Search</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="327"/>
        <source>Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="259"/>
        <source>Dir search.</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠬᠠᠢᠬᠤ.</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="vanished">目录</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="364"/>
        <source>Path</source>
        <translation>ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="376"/>
        <source>Last time modified</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="388"/>
        <source>Copy path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContengSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="429"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="648"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="430"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="649"/>
        <source>Open path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="431"/>
        <source>Copy Path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">文本内容搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="445"/>
        <source>File content search.</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶᠢ ᠬᠠᠢᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="450"/>
        <source>File content search</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="511"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="515"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="626"/>
        <source>Path</source>
        <translation>ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="638"/>
        <source>Last time modified</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="650"/>
        <source>Copy path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="62"/>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="87"/>
        <source>File Content</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="67"/>
        <source>File Content Search</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶᠢ ᠬᠠᠢᠬᠤ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="34"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="184"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="35"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="185"/>
        <source>Open path</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠪᠠᠢᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="36"/>
        <source>Copy Path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="55"/>
        <source>File Search</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="50"/>
        <source>File search.</source>
        <translation>ᠹᠠᠢᠯ ᠬᠠᠢᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="97"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="208"/>
        <source>Yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="99"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="210"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌ %1᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ᠎ᠶᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="125"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="162"/>
        <source>Path</source>
        <translation>ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="174"/>
        <source>Last time modified</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭ᠎ᠠ᠎ᠶᠢᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="186"/>
        <source>Copy path</source>
        <translation>ᠵᠠᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearch</name>
    <message>
        <source>From</source>
        <translation type="vanished">ᠢᠮᠸᠯ ᠢᠯᠡᠬᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>To</source>
        <translation type="vanished">ᠢᠮᠸᠯ ᠬᠤᠷᠢᠶᠠᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Cc</source>
        <translation type="vanished">ᠬᠠᠭᠤᠯᠵᠤ ᠬᠦᠷᠬᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearchPlugin</name>
    <message>
        <source>open</source>
        <translation type="obsolete">ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Mail Search</source>
        <translation type="vanished">ᠢᠮᠸᠯ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation type="vanished">ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearch</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="239"/>
        <source>Note Description:</source>
        <translatorcomment>便签内容：</translatorcomment>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠱᠢᠭ᠎ᠠ᠎ᠶᠢᠨ ᠠᠭᠤᠯᠭ᠎ᠠ:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearchPlugin</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="33"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="177"/>
        <source>Open</source>
        <translatorcomment>打开</translatorcomment>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="52"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="155"/>
        <source>Note Search</source>
        <translatorcomment>便签</translatorcomment>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠱᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="47"/>
        <source>Note Search.</source>
        <translatorcomment>便签.</translatorcomment>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠱᠢᠭ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="129"/>
        <source>Application</source>
        <translatorcomment>应用</translatorcomment>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchManager</name>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="68"/>
        <source>Path:</source>
        <translation>ᠵᠢᠮ:</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="69"/>
        <source>Modified time:</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ:</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchResultPropertyInfo</name>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="26"/>
        <source>file path</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="32"/>
        <source>file name</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="38"/>
        <source>file icon name</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="44"/>
        <source>modified time</source>
        <translation>ᠴᠠᠭ ᠢᠶᠠᠨ ᠵᠠᠰᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="50"/>
        <source>application desktop file path</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠤᠨ ᠰᠢᠷᠡᠭᠡᠨ ᠦ ᠨᠢᠭᠤᠷ ᠤᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="56"/>
        <source>application local name</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="62"/>
        <source>application icon name</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="68"/>
        <source>application description</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠲᠣᠳᠣᠷᠬᠠᠶᠢᠯᠠᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="74"/>
        <source>is online application</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠮᠥᠨ</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/search-result-property-info.cpp" line="80"/>
        <source>application package name</source>
        <translation>ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠤᠨ ᠪᠣᠭᠴᠣ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchTaskPluginManager</name>
    <message>
        <source>plugin type: %1, is disabled!</source>
        <translation type="vanished">ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ᠎ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ ᠄%1 ᠂ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>plugin type: %1, is not registered!</source>
        <translation type="vanished">ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ᠎ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ ᠄%1 ᠂ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsSearchPlugin</name>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="19"/>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="158"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="39"/>
        <source>Settings Search</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯ ᠬᠡᠱᠢᠬᠦᠨ</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="34"/>
        <source>Settings search.</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠬᠠᠢᠯᠲᠠ.</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="147"/>
        <source>Settings</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠵᠦᠢᠯ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/ukui-search-task.cpp" line="102"/>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation>ᠲᠤᠰ ᠡᠬᠦᠷᠭᠡuuid ᠨᠢ ᠪᠤᠷᠤᠭᠤ ᠡᠰᠡᠪᠡᠯ ᠨᠢᠭᠡ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTaskPrivate</name>
    <message>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation type="vanished">ᠲᠤᠰ ᠡᠬᠦᠷᠭᠡuuid ᠨᠢ ᠪᠤᠷᠤᠭᠤ ᠡᠰᠡᠪᠡᠯ ᠨᠢᠭᠡ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchPlugin</name>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="28"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="158"/>
        <source>Start browser search</source>
        <translation>ᠬᠠᠢᠭᠤᠷ᠎ᠤᠨ ᠬᠠᠢᠯᠲᠠ᠎ᠶᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="40"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="45"/>
        <source>Web Page</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
</context>
</TS>
