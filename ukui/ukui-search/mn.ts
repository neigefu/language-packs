<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <location filename="../../frontend/view/best-list-view.cpp" line="388"/>
        <source>Best Matches</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠰᠠᠢᠨ ᠢᠵᠢᠯᠢᠱᠢᠯ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="41"/>
        <source>ukui-search</source>
        <translation>ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="70"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="94"/>
        <source>Creating index can help you getting results quickly, whether to create or not?</source>
        <translation>ᠬᠡᠯᠬᠢᠶᠡᠰᠦ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠬᠠᠢᠯᠲᠠ᠎ᠶᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ᠎ᠢ ᠬᠤᠷᠳᠤᠨ ᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠂ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="105"/>
        <source>Don&apos;t remind</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="116"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="118"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FolderListItem</name>
    <message>
        <source>Delete the folder out of blacklist</source>
        <translation type="vanished">ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="71"/>
        <source>ukui-search</source>
        <translation>ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="79"/>
        <source>Global Search</source>
        <translation>ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <location filename="../../frontend/control/search-line-edit.cpp" line="57"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsWidget</name>
    <message>
        <source>ukui-search-settings</source>
        <translation type="vanished">ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Settings&lt;/h2&gt;</source>
        <translation type="vanished">&lt;h2&gt; ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ&lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Index State&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt; ᠬᠡᠯᠬᠢᠶᠡᠰᠦ᠎ᠶᠢᠨ ᠪᠠᠢᠳᠠᠯ&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>&lt;h3&gt;File Index Settings&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt; ᠹᠠᠢᠯ᠎ᠤᠨ ᠬᠡᠯᠬᠢᠶᠡᠰᠦ᠎ᠶᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation type="obsolete">ᠬᠠᠢᠯᠲᠠ᠎ᠪᠠᠷ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠴᠤᠮᠤᠭ᠎ᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ᠌ ᠦᠬᠡᠢ ᠂ ᠨᠡᠮᠡᠬᠦ᠌ ᠪᠤᠶᠤ ᠬᠠᠰᠤᠬᠤ᠎ᠪᠠᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠢᠷᠢ᠎ᠶᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Add ignored folders</source>
        <translation type="vanished">ᠴᠤᠮᠤᠭ᠎ᠢ ᠬᠠᠷ᠎ᠠ ᠳᠠᠩᠰᠠᠨ᠎ᠳᠤ ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Search Engine Settings&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt;ᠡᠷᠢᠯᠲᠡ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ᠎ᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Please select search engine you preferred.</source>
        <translation type="vanished">ᠢᠨᠲᠸᠷᠨ᠋ᠸᠲ᠎ᠦᠨ ᠡᠷᠢᠯᠳᠡ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ᠎ᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>baidu</source>
        <translation type="obsolete">ᠪᠠᠢ ᠳ᠋ᠦ᠋</translation>
    </message>
    <message>
        <source>sougou</source>
        <translation type="obsolete">ᠰᠸᠤ ᠭᠸᠦ</translation>
    </message>
    <message>
        <source>360</source>
        <translation type="obsolete">360</translation>
    </message>
    <message>
        <source>Whether to delete this directory?</source>
        <translation type="vanished">ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ᠎ᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Yes</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">No</translation>
    </message>
    <message>
        <source>Creating ...</source>
        <translation type="vanished">ᠶᠠᠭ ᠬᠡᠯᠬᠢᠶᠡᠰᠦᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ...</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="vanished">ᠬᠡᠯᠬᠢᠶᠡᠰᠦᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <source>Index Entry: %1</source>
        <translation type="vanished">ᠬᠡᠯᠬᠢᠶᠡᠰᠦ᠎ᠶᠢᠨ ᠵᠦᠢᠯ ᠄%1</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="obsolete">ᠴᠤᠮᠤᠭ</translation>
    </message>
    <message>
        <source>select blocked folder</source>
        <translation type="vanished">ᠬᠠᠯᠬᠠᠯᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">ᠪᠠᠢᠷᠢ ᠄ </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="obsolete">ᠹᠠᠢᠯ᠎ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠄ </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ ᠄ </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Choosen path is Empty!</source>
        <translation type="vanished">ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠵᠠᠮ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Choosen path is not in &quot;home&quot;!</source>
        <translation type="vanished">ᠲᠤᠰ ᠭᠠᠷᠴᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠴᠤᠮᠤᠭ᠎ᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Its&apos; parent folder has been blocked!</source>
        <translation type="vanished">ᠡᠬᠢ ᠴᠤᠮᠤᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠠᠯᠬᠠᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>Set blocked folder failed!</source>
        <translation type="vanished">ᠬᠠᠱᠢᠭᠳᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="107"/>
        <source>Quit ukui-search application</source>
        <translation>ᠬᠠᠢᠯᠲᠠ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ᠎ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="110"/>
        <source>Show main window</source>
        <translation>ᠭᠤᠤᠯ ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ᠎ᠶᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="113"/>
        <source>unregister a plugin with &lt;pluginName&gt;</source>
        <translation>&lt;pluginName&gt;ᠳᠠᠩᠰᠠᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠠᠭᠰᠠᠭᠠᠭᠰᠠᠨ ᠵᠦᠢᠯ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠶᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="116"/>
        <source>register a plugin with &lt;pluginName&gt;</source>
        <translation>ᠳᠠᠩᠰᠠᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ &lt;pluginName&gt;ᠬᠠᠭᠰᠠᠭᠠᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="119"/>
        <source>move &lt;pluginName&gt; to the target pos</source>
        <translation>ᠬᠠᠷᠠᠯᠲᠠ ᠳᠤ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ&lt;pluginName&gt;᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="122"/>
        <source>move plugin to &lt;index&gt;</source>
        <translation>ᠬᠠᠭᠰᠠᠭᠠᠭᠰᠠᠨ ᠲᠣᠨᠣᠭ ᠢ &lt;index&gt;ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchWidget</name>
    <message>
        <source>Web Page</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤᠨ ᠬᠠᠢᠯᠲᠠ</translation>
    </message>
</context>
<context>
    <name>search</name>
    <message>
        <location filename="../../frontend/search-app-widget-plugin/provider/data/search.qml" line="115"/>
        <source>search</source>
        <translation>ᠪᠦᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠬᠠᠢᠬᠤ</translation>
    </message>
</context>
</TS>
