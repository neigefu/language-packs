<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <location filename="../../frontend/view/best-list-view.cpp" line="388"/>
        <source>Best Matches</source>
        <translation>སྙོམས་སྒྲིག་ལེགས་ཤོས།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="41"/>
        <source>ukui-search</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="70"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="94"/>
        <source>Creating index can help you getting results quickly, whether to create or not?</source>
        <translation>དཀར་ཆག་གསར་བཟོ་བྱས་ཚེ་འཚོལ་བྱ་མྱུར་དུ་རྙེད་ཐུབ། གསར་བཟོ་བྱའམ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="105"/>
        <source>Don&apos;t remind</source>
        <translation>ད་ནས་གསལ་བརྡ་མི་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="116"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="118"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FolderListItem</name>
    <message>
        <source>Delete the folder out of blacklist</source>
        <translation type="vanished">མིང་ཐོ་ནག་པོའི་ནང་ནས་ཡིག་སྣོད་བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="71"/>
        <source>ukui-search</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="79"/>
        <source>Global Search</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <location filename="../../frontend/control/search-line-edit.cpp" line="57"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsWidget</name>
    <message>
        <source>ukui-search-settings</source>
        <translation type="vanished">འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <source>&lt;h2&gt;Settings&lt;/h2&gt;</source>
        <translation type="vanished">&lt;h2&gt; སྒྲིག་འགོད། &lt;/h2&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Index State&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt;དཀར་ཆག་གི་རྣམ་པ།&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>&lt;h3&gt;File Index Settings&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt;ཡིག་ཆའི་དཀར་ཆག་སྒྲིག་འགོད། &lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation type="vanished">གཤམ་གྱི་ཡིག་སྣོད་འཚོལ་བཤེར་མི་བྱེད། ཡིག་སྣོད་གསར་སྣོན་དང་གསུབ་འཕྲི་བྱས་ཚེ་ཡིག་ཆའི་དཀར་ཆག་སྒྲིག་འགོད་བྱ་ཐུབ།</translation>
    </message>
    <message>
        <source>Add ignored folders</source>
        <translation type="vanished">སྣང་མེད་དུ་བཞག་པའི་ཡིག་སྣོད་ཁ་སྣོན་</translation>
    </message>
    <message>
        <source>&lt;h3&gt;Search Engine Settings&lt;/h3&gt;</source>
        <translation type="vanished">&lt;h3&gt;འཚོལ་བཤེར་ཆས་སྒྲིག་འགོད།&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Please select search engine you preferred.</source>
        <translation type="vanished">ཁྱེད་རང་གིས་དགའ་པོ་བྱེད་པའི་འཚོལ་བཤེར་མ་ལག་འདེམས་</translation>
    </message>
    <message>
        <source>baidu</source>
        <translation type="vanished">པའེ་ཏུའུ།</translation>
    </message>
    <message>
        <source>sougou</source>
        <translation type="vanished">སོའོ་གོའུ།</translation>
    </message>
    <message>
        <source>360</source>
        <translation type="vanished">360</translation>
    </message>
    <message>
        <source>Whether to delete this directory?</source>
        <translation type="vanished">དཀར་ཆག་འདི་གསུབ་བམ།</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">རེད།</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">མིན།</translation>
    </message>
    <message>
        <source>Creating ...</source>
        <translation type="vanished">དཀར་ཆག་འདྲེན་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="vanished">བསྒྲུབས་ཚར།</translation>
    </message>
    <message>
        <source>Index Entry: %1</source>
        <translation type="vanished">གསལ་བྱང་ཚན་པ།:</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">དཀར་ཆག</translation>
    </message>
    <message>
        <source>select blocked folder</source>
        <translation type="vanished">བཀག་སྡོམ་བྱས་པའི་ཡིག་སྣོད་གདམ་གསེས</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">གོ་གནས་ནི། </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Choosen path is Empty!</source>
        <translation type="vanished">བདམས་ཟིན་པའི་ལམ་ཐིག་མི་འདུག</translation>
    </message>
    <message>
        <source>Choosen path is not in &quot;home&quot;!</source>
        <translation type="vanished">ཁྱིམ་གྱི་དཀར་ཆག་ནང་གི་ཡིག་སྣོད་འདེམ་རོགས།</translation>
    </message>
    <message>
        <source>Its&apos; parent folder has been blocked!</source>
        <translation type="vanished">རིམ་པ་གོང་མའི་ཡིག་སྣོད་གབ་ཟིན།</translation>
    </message>
    <message>
        <source>Set blocked folder failed!</source>
        <translation type="vanished">བཀག་སྡོམ་བྱས་པའི་ཡིག་སྣོད་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="107"/>
        <source>Quit ukui-search application</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་ལས་ཕྱིར་འཐེན་བྱ།</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="110"/>
        <source>Show main window</source>
        <translation>སྒེའུ་ཁུང་གཙོ་བོ་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="113"/>
        <source>unregister a plugin with &lt;pluginName&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="116"/>
        <source>register a plugin with &lt;pluginName&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="119"/>
        <source>move &lt;pluginName&gt; to the target pos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="122"/>
        <source>move plugin to &lt;index&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchWidget</name>
    <message>
        <source>Web Page</source>
        <translation type="vanished">དྲ་ངོས།</translation>
    </message>
</context>
<context>
    <name>search</name>
    <message>
        <location filename="../../frontend/search-app-widget-plugin/provider/data/search.qml" line="115"/>
        <source>search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
