<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <location filename="../../frontend/view/best-list-view.cpp" line="312"/>
        <source>Best Matches</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="41"/>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="70"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="94"/>
        <source>Creating index can help you getting results quickly, whether to create or not?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="105"/>
        <source>Don&apos;t remind</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="116"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="118"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FolderListItem</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="538"/>
        <source>Delete the folder out of blacklist</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="69"/>
        <source>ukui-search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="76"/>
        <source>Global Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <location filename="../../frontend/control/search-line-edit.cpp" line="56"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsWidget</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="35"/>
        <source>ukui-search-settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="81"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="503"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="108"/>
        <source>&lt;h2&gt;Settings&lt;/h2&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="113"/>
        <source>&lt;h3&gt;Index State&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="115"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="117"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="125"/>
        <source>&lt;h3&gt;File Index Settings&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="127"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="136"/>
        <source>Add ignored folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="157"/>
        <source>&lt;h3&gt;Search Engine Settings&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="159"/>
        <source>Please select search engine you preferred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="172"/>
        <source>baidu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="174"/>
        <source>sougou</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="176"/>
        <source>360</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <source>Whether to delete this directory?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="300"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="301"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="359"/>
        <source>Creating ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="362"/>
        <source>Done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="370"/>
        <source>Index Entry: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="416"/>
        <source>Directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="417"/>
        <source>select blocked folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="418"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="419"/>
        <source>Position: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="420"/>
        <source>FileName: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="421"/>
        <source>FileType: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="422"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="487"/>
        <source>Choosen path is Empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="491"/>
        <source>Choosen path is not in &quot;home&quot;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="495"/>
        <source>Its&apos; parent folder has been blocked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="499"/>
        <source>Set blocked folder failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="504"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="88"/>
        <source>Quit ukui-search application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="91"/>
        <source>Show main window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchWidget</name>
    <message>
        <location filename="../../frontend/view/web-search-view.cpp" line="152"/>
        <source>Web Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
