<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="32"/>
        <location filename="../search.cpp" line="200"/>
        <source>Search</source>
        <translation>ᠪᠦᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠬᠠᠢᠬᠤ</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="236"/>
        <source>Create index</source>
        <translation>ᠬᠡᠯᠬᠢᠶᠡᠰᠦ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="237"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ᠎ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠠᠢᠯᠲᠠ᠎ᠶᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ᠎ᠢ ᠬᠤᠷᠳᠤᠨ ᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="205"/>
        <source>Default web searching engine</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢᠨᠲᠸᠷᠨ᠋ᠸᠲ᠎ᠦᠨ ᠡᠷᠢᠯᠳᠡ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="209"/>
        <source>baidu</source>
        <translation>ᠪᠠᠢ ᠳ᠋ᠦ᠋</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="210"/>
        <source>sougou</source>
        <translation>ᠰᠸᠤ ᠭᠸᠦ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="211"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="345"/>
        <source>Block Folders</source>
        <translation>ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="350"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>ᠬᠠᠢᠯᠲᠠ᠎ᠪᠠᠷ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠴᠤᠮᠤᠭ᠎ᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ᠌ ᠦᠬᠡᠢ ᠂ ᠨᠡᠮᠡᠬᠦ᠌ ᠪᠤᠶᠤ ᠬᠠᠰᠤᠬᠤ᠎ᠪᠠᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠢᠷᠢ᠎ᠶᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="702"/>
        <location filename="../search.cpp" line="773"/>
        <source>delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="431"/>
        <location filename="../search.cpp" line="475"/>
        <source>Directories</source>
        <translation>ᠴᠤᠮᠤᠭ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="267"/>
        <source>File Content Search</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠡᠷᠢᠬᠦ</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="272"/>
        <source>Precise Search</source>
        <translation>ᠣᠨᠣᠪᠴᠢᠲᠠᠢ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="274"/>
        <source>show the results that exactly match the keyword</source>
        <translation>ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠲᠡᠢ ᠪᠦᠷᠢᠮᠦᠰᠦᠨ ᠲᠣᠬᠢᠷᠠᠯᠴᠠᠭᠰᠠᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="283"/>
        <source>Fuzzy Search</source>
        <translation>ᠪᠠᠯᠠᠷᠬᠠᠢ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="285"/>
        <source>show more results that match the keyword</source>
        <translation>ᠨᠡᠩ ᠠᠷᠪᠢᠨ ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠲᠡᠢ ᠲᠣᠬᠢᠷᠠᠯᠴᠠᠭᠰᠠᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="312"/>
        <source>Search Folders</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="318"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠭᠠᠷᠴᠠᠭ ᠢ ᠡᠷᠢᠨ᠎ᠡ ᠃ ᠲᠠ ᠭᠠᠷᠴᠠᠭ ᠢᠶᠠᠷ ᠳᠠᠮᠵᠢᠨ ᠡᠷᠢᠬᠦ ᠬᠡᠪᠴᠢᠶ᠎ᠡ ᠶᠢ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="432"/>
        <source>select blocked folder</source>
        <translation>ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="433"/>
        <location filename="../search.cpp" line="477"/>
        <source>Select</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="434"/>
        <location filename="../search.cpp" line="478"/>
        <source>Position: </source>
        <translation>ᠪᠠᠢᠷᠢ ᠄ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="435"/>
        <location filename="../search.cpp" line="479"/>
        <source>FileName: </source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠄ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="436"/>
        <location filename="../search.cpp" line="480"/>
        <source>FileType: </source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="437"/>
        <location filename="../search.cpp" line="481"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <location filename="../search.cpp" line="455"/>
        <location filename="../search.cpp" line="459"/>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="492"/>
        <location filename="../search.cpp" line="495"/>
        <location filename="../search.cpp" line="498"/>
        <location filename="../search.cpp" line="501"/>
        <location filename="../search.cpp" line="504"/>
        <location filename="../search.cpp" line="507"/>
        <location filename="../search.cpp" line="510"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠨᠡᠮᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠂ ᠲᠡᠭᠦᠨ ᠦ ᠡᠴᠢᠭᠡ ᠶᠢᠨ ᠭᠠᠷᠴᠠᠭ ᠨᠢᠭᠡᠨᠲᠡ ᠨᠡᠮᠡᠭ᠍ᠳᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="455"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="459"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠨᠢᠭᠡᠨᠲᠡ ᠨᠡᠮᠡᠭ᠍ᠳᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="507"/>
        <source>Add search folder failed, hidden　path is not supported!</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠬᠦ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠭᠠᠷᠴᠠᠭ ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="476"/>
        <source>select search folder</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠨᠢᠭᠡᠨᠲᠡ ᠰᠤᠩᠭ᠋ᠤᠬᠤ ᠵᠠᠮ ᠪᠤᠶᠤ ᠲᠡᠭᠦᠨ ᠦ ᠡᠴᠢᠭᠡ ᠶᠢᠨ ᠭᠠᠷᠴᠠᠭ ᠨᠡᠮᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="495"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="498"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠨ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠳᠠᠬᠢᠨ ᠳᠠᠪᠲᠠᠨ ᠠᠴᠢᠶᠠᠯᠠᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠨᠡᠮᠡᠵᠦ ᠂ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠨᠥᠭᠥᠭᠡ ᠨᠢᠭᠡ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠨᠡᠮᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="501"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠨᠥᠭᠥᠭᠡ ᠨᠢᠭᠡ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠢ ᠨᠡᠮᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="504"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>ᠨᠡᠮᠡᠵᠦ ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="510"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>ᠡᠷᠢᠬᠦ ᠭᠠᠷᠴᠠᠭ ᠢ ᠨᠡᠮᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠡᠨᠡ ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠵᠠᠮ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠵᠠᠮ ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ᠎ᠤᠨ ᠳᠤᠤᠷ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠡᠬᠢ ᠭᠠᠷᠴᠠᠭ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation type="vanished">ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠡᠨᠡ ᠴᠤᠮᠤᠭ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠴᠢᠬᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
</TS>
