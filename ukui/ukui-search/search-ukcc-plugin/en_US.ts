<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="32"/>
        <location filename="../search.cpp" line="200"/>
        <source>Search</source>
        <translation>Search</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="205"/>
        <source>Default web searching engine</source>
        <translation>Default web searching engine</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="209"/>
        <source>baidu</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="210"/>
        <source>sougou</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="211"/>
        <source>360</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="236"/>
        <source>Create index</source>
        <translation>Create index</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="237"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="267"/>
        <source>File Content Search</source>
        <translation>File Content Search</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="272"/>
        <source>Precise Search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="274"/>
        <source>show the results that exactly match the keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="283"/>
        <source>Fuzzy Search</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="285"/>
        <source>show more results that match the keyword</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="312"/>
        <source>Search Folders</source>
        <translation>Search Folders</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="318"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="345"/>
        <source>Block Folders</source>
        <translation>Block Folders</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="350"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="431"/>
        <location filename="../search.cpp" line="475"/>
        <source>Directories</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="432"/>
        <source>select blocked folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="433"/>
        <location filename="../search.cpp" line="477"/>
        <source>Select</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="434"/>
        <location filename="../search.cpp" line="478"/>
        <source>Position: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="435"/>
        <location filename="../search.cpp" line="479"/>
        <source>FileName: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="436"/>
        <location filename="../search.cpp" line="480"/>
        <source>FileType: </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="437"/>
        <location filename="../search.cpp" line="481"/>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <location filename="../search.cpp" line="455"/>
        <location filename="../search.cpp" line="459"/>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="492"/>
        <location filename="../search.cpp" line="495"/>
        <location filename="../search.cpp" line="498"/>
        <location filename="../search.cpp" line="501"/>
        <location filename="../search.cpp" line="504"/>
        <location filename="../search.cpp" line="507"/>
        <location filename="../search.cpp" line="510"/>
        <source>Warning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="455"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="459"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="476"/>
        <source>select search folder</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="495"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="498"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="501"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="504"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="507"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="510"/>
        <source>Add search folder failed, permission denied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="702"/>
        <location filename="../search.cpp" line="773"/>
        <source>delete</source>
        <translation></translation>
    </message>
</context>
</TS>
