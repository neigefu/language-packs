<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="32"/>
        <location filename="../search.cpp" line="200"/>
        <source>Search</source>
        <translation>全局搜索</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="236"/>
        <source>Create index</source>
        <translation>创建索引</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="237"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation>开启之后可以快速获取搜索结果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="205"/>
        <source>Default web searching engine</source>
        <translation>默认互联网搜索引擎</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="209"/>
        <source>baidu</source>
        <translation>百度</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="210"/>
        <source>sougou</source>
        <translation>搜狗</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="211"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="345"/>
        <source>Block Folders</source>
        <translation>排除的文件夹</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="350"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>搜索将不查看以下文件夹，通过添加和删除可以设置排除的文件夹位置</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="702"/>
        <location filename="../search.cpp" line="773"/>
        <source>delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="431"/>
        <location filename="../search.cpp" line="475"/>
        <source>Directories</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="267"/>
        <source>File Content Search</source>
        <translation>搜索文本内容</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="285"/>
        <source>show more results that match the keyword</source>
        <translation>显示更多与输入内容匹配的搜索结果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="283"/>
        <source>Fuzzy Search</source>
        <translation>模糊搜索</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="272"/>
        <source>Precise Search</source>
        <translation>精确搜索</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="274"/>
        <source>show the results that exactly match the keyword</source>
        <translation>仅显示与输入内容完全一致的搜索结果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="312"/>
        <source>Search Folders</source>
        <translation>搜索范围</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="318"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>以下文件的内容将出现在全局搜索的结果中</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="432"/>
        <source>select blocked folder</source>
        <translation>选择排除的文件夹</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="433"/>
        <location filename="../search.cpp" line="477"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="434"/>
        <location filename="../search.cpp" line="478"/>
        <source>Position: </source>
        <translation>位置</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="435"/>
        <location filename="../search.cpp" line="479"/>
        <source>FileName: </source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="436"/>
        <location filename="../search.cpp" line="480"/>
        <source>FileType: </source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="437"/>
        <location filename="../search.cpp" line="481"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <location filename="../search.cpp" line="455"/>
        <location filename="../search.cpp" line="459"/>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="492"/>
        <location filename="../search.cpp" line="495"/>
        <location filename="../search.cpp" line="498"/>
        <location filename="../search.cpp" line="501"/>
        <location filename="../search.cpp" line="504"/>
        <location filename="../search.cpp" line="507"/>
        <location filename="../search.cpp" line="510"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="507"/>
        <source>Add search folder failed, hidden&#x3000;path is not supported!</source>
        <translation>添加失败，不支持隐藏目录！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="510"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>添加失败，该目录无权限访问！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>添加失败，父目录已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="455"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>添加失败，要添加的路径不存在！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="459"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>添加失败，这个文件夹已经被添加过了！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="476"/>
        <source>select search folder</source>
        <translation>选择要搜索的文件夹</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="495"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>添加失败，暂不支持该目录！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="501"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>添加失败，文件夹位于重复挂载设备下，相同内容的文件夹已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>添加失败，该目录或其父目录已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="498"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>添加失败，文件夹位于重复挂载设备下，且该设备另一个挂载点的文件夹已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="504"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>添加失败，要添加的路径不存在！</translation>
    </message>
</context>
</TS>
