<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="32"/>
        <location filename="../search.cpp" line="200"/>
        <source>Search</source>
        <translation>全域搜索</translation>
        <extra-contents_path>/Search/Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="236"/>
        <source>Create index</source>
        <translation>創建索引</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="237"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation>開啟之後可以快速獲取搜尋結果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="205"/>
        <source>Default web searching engine</source>
        <translation>默認互聯網搜尋引擎</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="209"/>
        <source>baidu</source>
        <translation>百度</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="210"/>
        <source>sougou</source>
        <translation>搜狗</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="211"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="345"/>
        <source>Block Folders</source>
        <translation>排除的資料夾</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="350"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>搜索將不查看以下資料夾，通過添加和刪除可以設置排除的資料夾位置</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="702"/>
        <location filename="../search.cpp" line="773"/>
        <source>delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="431"/>
        <location filename="../search.cpp" line="475"/>
        <source>Directories</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="267"/>
        <source>File Content Search</source>
        <translation>搜索文字內容</translation>
        <extra-contents_path>/Search/File Content Search</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="285"/>
        <source>show more results that match the keyword</source>
        <translation>顯示更多與輸入內容匹配的搜尋結果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="283"/>
        <source>Fuzzy Search</source>
        <translation>模糊搜索</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="272"/>
        <source>Precise Search</source>
        <translation>精確搜索</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="274"/>
        <source>show the results that exactly match the keyword</source>
        <translation>僅顯示與輸入內容完全一致的搜尋結果</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="312"/>
        <source>Search Folders</source>
        <translation>搜索範圍</translation>
        <extra-contents_path>/Search/Search Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="318"/>
        <source>Following folders will be searched. You can set it by adding and removing folders.</source>
        <translation>以下文件的內容將出現在全域搜索的結果中</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="432"/>
        <source>select blocked folder</source>
        <translation>選擇排除的資料夾</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="433"/>
        <location filename="../search.cpp" line="477"/>
        <source>Select</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="434"/>
        <location filename="../search.cpp" line="478"/>
        <source>Position: </source>
        <translation>位置 </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="435"/>
        <location filename="../search.cpp" line="479"/>
        <source>FileName: </source>
        <translation>檔名 </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="436"/>
        <location filename="../search.cpp" line="480"/>
        <source>FileType: </source>
        <translation>類型 </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="437"/>
        <location filename="../search.cpp" line="481"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <location filename="../search.cpp" line="455"/>
        <location filename="../search.cpp" line="459"/>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="492"/>
        <location filename="../search.cpp" line="495"/>
        <location filename="../search.cpp" line="498"/>
        <location filename="../search.cpp" line="501"/>
        <location filename="../search.cpp" line="504"/>
        <location filename="../search.cpp" line="507"/>
        <location filename="../search.cpp" line="510"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="463"/>
        <location filename="../search.cpp" line="507"/>
        <source>Add search folder failed, hidden　path is not supported!</source>
        <translation>添加失敗，不支持隱藏目錄！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="510"/>
        <source>Add search folder failed, permission denied!</source>
        <translation>添加失敗，該目錄無許可權訪問！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="451"/>
        <source>Add blocked folder failed, its parent dir has been added!</source>
        <translation>添加失敗，父目錄已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="455"/>
        <source>Add blocked folder failed, choosen path is not exist!</source>
        <translation>添加失敗，要添加的路徑不存在！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="459"/>
        <source>Add blocked folder failed, it has already been blocked!</source>
        <translation>添加失敗，這個資料夾已經被添加過了！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="476"/>
        <source>select search folder</source>
        <translation>選擇要搜索的資料夾</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="495"/>
        <source>Add search folder failed, choosen path is not supported currently!</source>
        <translation>添加失敗，暫不支援該目錄！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="501"/>
        <source>Add search folder failed, another path which is in the same device has been added!</source>
        <translation>添加失敗，資料夾位於重複掛載設備下，相同內容的資料夾已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="492"/>
        <source>Add search folder failed, choosen path or its parent dir has been added!</source>
        <translation>添加失敗，該目錄或其父目錄已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="498"/>
        <source>Add search folder failed, choosen path is in repeat mounted devices and another path which is in the same device has been added!</source>
        <translation>添加失敗，資料夾位於重複掛載設備下，且該設備另一個掛載點的資料夾已被添加！</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="504"/>
        <source>Add search folder failed, choosen path is not exists!</source>
        <translation>添加失敗，要添加的路徑不存在！</translation>
    </message>
</context>
</TS>
