<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AndroidHomePage</name>
    <message>
        <location filename="../ui/filemanageview/androidhomepage.cpp" line="14"/>
        <source>Storage</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>AndroidItem</name>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="69"/>
        <source>Picture</source>
        <translation>ᠵᠢᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="72"/>
        <source>Video</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="75"/>
        <source>Music</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="78"/>
        <source>Document</source>
        <translation>ᠠᠯᠪᠠᠨ ᠪᠢᠴᠢᠭ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="81"/>
        <source>WeChat</source>
        <translation>ᠸᠢᠴᠠᠲ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="84"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="87"/>
        <source>Storage</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="100"/>
        <source>All File</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="102"/>
        <source>items</source>
        <translation>ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠃</translation>
    </message>
</context>
<context>
    <name>BaseDevice</name>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="404"/>
        <source>Control Devices Supported</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠶᠢ ᠳᠡᠮᠵᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="408"/>
        <source>Control device not supported</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ᠃</translation>
    </message>
</context>
<context>
    <name>ConnectInterface</name>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="19"/>
        <source>Back</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠢᠷᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="33"/>
        <source>Nearby device</source>
        <translation>ᠣᠶᠢᠷ᠎ᠠ ᠬᠠᠪᠢ ᠶᠢᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="34"/>
        <source>Connect Phone</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠤᠲᠠᠰᠤ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="35"/>
        <source>Device Code</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠣᠷᠣᠯᠠᠬᠤ ᠨᠣᠮᠧᠷ ᠃</translation>
    </message>
</context>
<context>
    <name>ConnectedWin</name>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="38"/>
        <source>CONNECTED</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="114"/>
        <location filename="../ui/view/connectedwin.cpp" line="122"/>
        <source>ComputerScreen</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠦᠨ ᠳᠡᠯᠪᠡᠷᠡᠯᠲᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="120"/>
        <source>MobileScreen</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠳᠡᠯᠪᠡᠷᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="175"/>
        <source>ExitScreen</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ ᠡᠴᠡ ᠤᠬᠤᠷᠢᠨ ᠭᠠᠷᠤᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>DeviceCodeWidget</name>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="28"/>
        <source>Please enter the connection code of the other device</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠣᠯᠪᠣᠯᠲᠠ ᠶᠢᠨ ᠺᠣᠳ᠋ ᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠢᠷᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="37"/>
        <source>The connection code of the device can be obtained on the homepage of the other party&apos;s &apos;multi terminal collaboration&apos; application</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠬᠣᠯᠪᠣᠯᠲᠠ ᠶᠢᠨ ᠨᠣᠮᠧᠷ ᠢ ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠦᠨ 《 ᠣᠯᠠᠨ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠬᠠᠮᠵᠢᠯᠴᠠᠭ᠎ᠠ 》 ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠶᠢᠨ ᠲᠦᠷᠦᠭᠦᠦ ᠨᠢᠭᠤᠷ ᠲᠤ ᠣᠯᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="65"/>
        <source>Connection</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>FileManageWin</name>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="25"/>
        <source>Go Back</source>
        <translation>ᠪᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="37"/>
        <source>Go Forward</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="57"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="406"/>
        <source>Search File</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠡᠷᠢᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="85"/>
        <source>Select</source>
        <translation>ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="106"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="626"/>
        <source>List Mode</source>
        <translation>ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠢ ᠵᠢᠭᠰᠠᠭᠠᠨ᠎</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="118"/>
        <source>Refresh</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="124"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="432"/>
        <source>Select File</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠰᠣᠩᠭᠣᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="133"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="212"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="646"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="650"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="658"/>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ ᠰᠤᠩᠭ᠋ᠤᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="142"/>
        <source>Finish</source>
        <translation>ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="210"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="647"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="649"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="657"/>
        <source>Deselect All</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠤᠯᠢ ᠶᠢ ᠦᠭᠡᠶᠢᠰᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="631"/>
        <source>Icon Mode</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤᠨ ᠲᠡᠮᠳᠡᠭ ᠦᠨ ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="91"/>
        <source>List of Files</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠳᠦ ᠵᠢ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="92"/>
        <source>Picture</source>
        <translation>ᠵᠢᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="93"/>
        <source>Video</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="94"/>
        <source>Music</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="95"/>
        <source>Doc</source>
        <translation>ᠡᠮᠴᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="96"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="97"/>
        <source>WeChat</source>
        <translation>ᠸᠢᠴᠠᠲ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="98"/>
        <source>Storage</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>FileView</name>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="132"/>
        <location filename="../ui/filemanageview/fileview.cpp" line="525"/>
        <source>Download</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="405"/>
        <source>Choose folder</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠠᠪᠤᠳᠠᠷ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="523"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../ui/view/homepage.cpp" line="42"/>
        <source>Multiterminal</source>
        <translation>ᠢᠯᠡᠭᠦᠦ ᠦᠵᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="48"/>
        <source>Cross equipment and cross system collaboration. It is more convenient to share resources and screens, and more convenient and efficient to work!</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠠᠯᠤᠰᠯᠠᠬᠤ ᠪᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠠᠯᠤᠰᠯᠠᠨ ᠬᠠᠮᠵᠢᠯᠴᠠᠬᠤ ᠬᠠᠮᠲᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠪᠠ ᠳᠡᠯᠪᠡᠴᠢᠬᠦ ᠨᠢ ᠨᠡᠩ ᠳᠥᠭᠥᠮ ᠂ ᠠᠵᠢᠯ ᠨᠢ ᠨᠡᠩ ᠳᠥᠭᠥᠮ ᠪᠥᠭᠡᠳ ᠥᠨᠳᠥᠷ ᠦᠷ᠎ᠡ ᠪᠦᠲᠦᠮᠵᠢᠲᠡᠶ !</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="55"/>
        <source>Device Code:</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠣᠷᠣᠯᠠᠬᠤ ᠨᠣᠮᠧᠷ ᠄</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="66"/>
        <source>Folder</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠠᠪᠤᠳᠠᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="70"/>
        <source>Share</source>
        <translation>ᠬᠠᠮᠲᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="74"/>
        <source>Cross</source>
        <translation>ᠭᠠᠲᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="77"/>
        <source>Connect Now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠲᠦᠷᠭᠡᠨ ᠬᠣᠯᠪᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="185"/>
        <source>No network</source>
        <translation>ᠲᠣᠣᠷ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="381"/>
        <location filename="../ui/mainwindow.cpp" line="1380"/>
        <source>kylin-connectivity</source>
        <translation>ᠣᠯᠠᠨ ᠦᠵᠦᠬᠦᠷ ᠲᠤ᠌ ᠬᠠᠮᠵᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="481"/>
        <source>Agreed to connect</source>
        <translation>ᠬᠣᠯᠪᠣᠬᠤ ᠶᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="483"/>
        <source>Peer has agreed</source>
        <translation>ᠬᠠᠮᠲᠤ ᠠᠵᠢᠯ ᠨᠢᠭᠡᠨᠲᠡ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="485"/>
        <source>Establishing connection, please wait...</source>
        <translation>ᠶᠠᠭ ᠬᠣᠯᠪᠣᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ᠂ ᠵᠢᠭᠠᠬᠠᠨ ᠬᠦᠯᠢᠶᠡᠵᠦ ᠁</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="489"/>
        <source>CANCEL</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="939"/>
        <source>Not currently connected, please connect</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠬᠣᠯᠪᠣᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠵᠠᠯᠭᠠᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="695"/>
        <location filename="../ui/mainwindow.cpp" line="1368"/>
        <source>file download failed</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1086"/>
        <source>Connection request received from&quot;</source>
        <translation> ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠦ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠤᠭᠰᠠᠨ ᠬᠤᠯᠪᠤᠭ᠎ᠠ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ  ᠡᠴᠡ ᠠᠪᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1086"/>
        <location filename="../ui/mainwindow.cpp" line="1408"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1087"/>
        <source>After consent, the other party can view and download all the files on the device, and can share the other party&apos;s desktop to this screen.</source>
        <translation>ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠨᠥᠭᠥᠭᠡ ᠲᠠᠯ᠎ᠠ ᠨᠢ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠠᠯᠢᠪᠠ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠦᠵᠡᠵᠦ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠨᠥᠭᠥᠭᠡ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ ᠰᠢᠷᠡᠭᠡᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠬᠠᠮᠲᠤᠪᠠᠷ ᠡᠨᠳᠡ ᠬᠦᠷᠲᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1093"/>
        <location filename="../ui/mainwindow.cpp" line="1414"/>
        <source>NO</source>
        <translation>ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1092"/>
        <location filename="../ui/mainwindow.cpp" line="1413"/>
        <source>YES</source>
        <translation>ᠲᠡᠢ᠌ᠮᠦ ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1350"/>
        <source>File open exception!</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1426"/>
        <source>The other party has refused your screen projection request!</source>
        <translation>ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠴᠢᠨᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢ ᠲᠡᠪᠴᠢᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1427"/>
        <source>Failed to cast the screen. Please contact the other party and try again.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢ ᠬᠠᠷᠪᠤᠵᠤ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃ ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠲᠡᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠳᠠᠬᠢᠨ ᠲᠤᠷᠰᠢᠵᠤ ᠦᠵᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1112"/>
        <location filename="../ui/mainwindow.cpp" line="1431"/>
        <source>RECONNECT</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1107"/>
        <source>The other party has rejected your connection request!</source>
        <translation>ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠲᠠᠨ ᠤ ᠬᠣᠯᠪᠣᠯᠲᠠ ᠶᠢᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢ ᠲᠡᠪᠴᠢᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1108"/>
        <source>Connection failed. Please contact the other party and try again.</source>
        <translation>ᠬᠣᠯᠪᠣᠭᠳᠠᠭᠰᠠᠨ ᠢᠯᠠᠭᠳᠠᠨ᠎ᠠ ᠃ ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠲᠡᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠳᠠᠬᠢᠨ ᠲᠤᠷᠰᠢᠵᠤ ᠦᠵᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1113"/>
        <location filename="../ui/mainwindow.cpp" line="1432"/>
        <location filename="../ui/mainwindow.cpp" line="1488"/>
        <location filename="../ui/mainwindow.cpp" line="1800"/>
        <source>CLOSE</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1211"/>
        <source>Please install kylin-assistant on the Android terminal!</source>
        <translation>ᠠᠨᠲᠧᠷᠦ᠋ᠰ ᠦᠨ ᠦᠵᠦᠭᠦᠷ ᠳᠡᠭᠡᠷ᠎ᠡ ᠾᠧ ᠯᠢᠨ ᠬᠠᠪᠰᠤᠷᠤᠭᠴᠢ ᠤᠭᠰᠠᠷᠠᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1221"/>
        <source>Please use the USB to connect your phone device!</source>
        <translation>USB ᠶᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠵᠦ ᠲᠠᠨ ᠤ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠬᠣᠯᠪᠣᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1232"/>
        <source>Connection error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠶᠢ ᠬᠣᠯᠪᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1234"/>
        <source>Connection timed out</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠦᠶ᠎ᠡ ᠡᠴᠡ ᠬᠡᠲᠦᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1991"/>
        <source>There is currently a connection in progress!</source>
        <translation>ᠣᠳᠣ ᠶᠠᠭ ᠬᠣᠯᠪᠣᠵᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1381"/>
        <source>Version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1263"/>
        <source>No Content</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1383"/>
        <source>Mobile Assistant is an interconnection tool of Android device and Kirin operating system, which supports Android file synchronization, file transfer, screen mirroring and other functions, which is simple and fast to operate</source>
        <translation>ᠭᠠᠷ ᠤᠲᠠᠰᠤᠨ ᠤ ᠬᠠᠪᠰᠤᠷᠤᠭᠴᠢ ᠪᠣᠯ ᠠᠨᠲᠧᠷᠯᠠᠶ ᠶᠢᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠪᠣᠯᠣᠨ ᠾᠧ ᠯᠢᠨ ᠦ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠢᠨᠲ᠋ᠧᠷᠨᠧᠲ᠌ ᠦᠨ ᠢᠨᠲ᠋ᠧᠷᠨᠧᠲ ᠂ ᠠᠨᠲᠧᠷᠦ᠋ ᠶᠢᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠂ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠂ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠰᠢᠯ ᠵᠡᠷᠭᠡ ᠴᠢᠳᠠᠪᠬᠢ ᠶᠢ ᠳᠡᠮᠵᠢᠵᠦ ᠂ ᠳᠥᠭᠥᠮ ᠲᠦᠷᠭᠡᠨ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1407"/>
        <source>Received screen projection request from &quot;</source>
        <translation>ᠢᠷᠡᠯᠲᠡ ᠶᠢ ᠣᠯᠵᠤ ᠠᠪᠤᠶ᠎ᠠ 》 ᠭᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1409"/>
        <source>After consent, the other party can share the device desktop to this screen.</source>
        <translation>ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠨᠥᠭᠥᠭᠡ ᠲᠠᠯ᠎ᠠ ᠨᠢ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠰᠢᠷᠡᠭᠡᠨ ᠦ ᠭᠠᠳᠠᠷᠭᠤ ᠶᠢ ᠡᠨᠡ ᠳᠡᠯᠭᠡᠷ ᠬᠦᠷᠲᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1446"/>
        <source>The other party agreed to your screen projection request!</source>
        <translation>ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠴᠢᠨᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠵᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1447"/>
        <source>The screen is being cast, please wait...</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ ᠶᠠᠭ ᠲᠤᠰᠴᠤ ᠪᠠᠢᠢᠨ᠎ᠠ᠂ ᠵᠢᠭᠠᠬᠠᠨ ᠬᠦᠯᠢᠶᠡᠵᠦ᠁</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1472"/>
        <location filename="../ui/mainwindow.cpp" line="1495"/>
        <source>End of screen projection</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ ᠶᠢᠨ ᠰᠡᠭᠦᠳᠡᠷ ᠲᠠᠭᠤᠰᠴᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1473"/>
        <location filename="../ui/mainwindow.cpp" line="1496"/>
        <source>The other party has finished the screen projection function.</source>
        <translation>ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠨᠢ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠰᠡᠭᠦᠳᠡᠷᠯᠡᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠪᠠᠨ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1569"/>
        <source>Search data loading failed!</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢ ᠡᠷᠢᠵᠦ ᠢᠯᠠᠭᠳᠠᠯ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1587"/>
        <location filename="../ui/mainwindow.cpp" line="1590"/>
        <source>Transmission interruption</source>
        <translation>ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠲᠠᠰᠤᠷᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1588"/>
        <source>The other party&apos;s device has insufficient local storage!</source>
        <translation>ᠨᠥᠭᠥᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠦᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1590"/>
        <source>Insufficient local storage space!</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠣᠷᠣᠨ ᠵᠠᠶ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1609"/>
        <source>No relevant results</source>
        <translation>ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠦᠭᠡᠢ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="2005"/>
        <source>The screen projection request has been sent to the connected device. Please click [Agree] in the opposite pop-up window</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ ᠪᠡᠷ ᠺᠢᠨᠣ᠋ ᠥᠭ᠍ᠬᠦ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠣᠯᠪᠣᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠬᠦᠷᠭᠡᠬᠦ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠵᠠᠢ ᠃ ᠡᠰᠡᠷᠭᠦ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ ᠪᠥᠮᠪᠥᠭᠡᠨ ᠴᠣᠩᠬᠣᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ 〔 ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ 〕</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="941"/>
        <location filename="../ui/mainwindow.cpp" line="1213"/>
        <location filename="../ui/mainwindow.cpp" line="1223"/>
        <location filename="../ui/mainwindow.cpp" line="1296"/>
        <location filename="../ui/mainwindow.cpp" line="1352"/>
        <location filename="../ui/mainwindow.cpp" line="1451"/>
        <location filename="../ui/mainwindow.cpp" line="1477"/>
        <location filename="../ui/mainwindow.cpp" line="1500"/>
        <location filename="../ui/mainwindow.cpp" line="1571"/>
        <location filename="../ui/mainwindow.cpp" line="1593"/>
        <location filename="../ui/mainwindow.cpp" line="1987"/>
        <location filename="../ui/mainwindow.cpp" line="1993"/>
        <location filename="../ui/mainwindow.cpp" line="2010"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1482"/>
        <location filename="../ui/mainwindow.cpp" line="1794"/>
        <source>Screen projection loading error</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠰᠡᠭᠦᠳᠡᠷ ᠲᠦ ᠪᠤᠷᠤᠭᠤ ᠨᠡᠮᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1484"/>
        <location filename="../ui/mainwindow.cpp" line="1796"/>
        <source>Please check whether to install the projection expansion package [kylin connectivity tools]</source>
        <translation>ᠾᠧ ᠯᠢᠨ ᠦ ᠵᠠᠯᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1784"/>
        <source>Uploaded to</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠳᠡᠭᠡᠭ᠍ᠰᠢ ᠳᠠᠮᠵᠢᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1755"/>
        <source>Downloaded to</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠬᠦᠷᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1981"/>
        <location filename="../ui/mainwindow.cpp" line="2003"/>
        <source>Request sent successfully!</source>
        <translation>ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠣᠭᠤᠯᠬᠤ ᠪᠠᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1982"/>
        <source>The connection request has been sent to the selected device. Please click [YES] in the opposite pop-up window</source>
        <translation>ᠬᠣᠯᠪᠣᠬᠤ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠬᠦᠷᠭᠡᠵᠦ ᠥᠭ᠍ᠬᠦ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠵᠠᠢ ᠃ ᠡᠰᠡᠷᠭᠦ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ ᠪᠥᠮᠪᠥᠭᠡ ᠴᠣᠩᠬᠣᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠳᠠᠩ ᠴᠣᠬᠢᠯᠭ᠎ᠠ 〔 ᠪᠣᠯ 〕</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../ui/basewidget/messagedialog.cpp" line="29"/>
        <source>kylin-connectivity</source>
        <translation>ᠣᠯᠠᠨ ᠦᠵᠦᠬᠦᠷ ᠲᠤ᠌ ᠬᠠᠮᠵᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/basewidget/messagedialog.cpp" line="40"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MobileConnectWin</name>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="24"/>
        <source>ScanCode</source>
        <translation>ᠨᠣᠮᠧᠷ ᠰᠢᠷᠦᠭᠦ</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="25"/>
        <source>USBConnect</source>
        <translation>USBᠵᠠᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="43"/>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="103"/>
        <source>Your phone model: </source>
        <translation>ᠲᠠᠨ ᠤ ᠭᠠᠷ ᠤᠲᠠᠰᠤᠨ ᠤ ᠬᠡᠯᠪᠡᠷᠢ </translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="49"/>
        <source>vivo</source>
        <translation>ᠪᠡᠶ᠎ᠡ ᠳᠣᠲᠣᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="50"/>
        <source>HUAWEI</source>
        <translation>ᠬᠤᠸᠠ ᠬᠤᠸᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="51"/>
        <source>Xiaomi</source>
        <translation>ᠨᠠᠷᠢᠮᠤ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="53"/>
        <source>OPPO</source>
        <translation>OPPO</translation>
    </message>
</context>
<context>
    <name>MobileQRcode</name>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="27"/>
        <source>No network</source>
        <translation>ᠲᠣᠣᠷ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="83"/>
        <source>Connect the mobile phone and computer to the same network,open the mobile phone app and scan the QR code.</source>
        <translation>ᠭᠠᠷ ᠤᠲᠠᠰᠤ ᠪᠣᠯᠣᠨ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠲᠣᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠦ ᠵᠠᠯᠭᠠᠵᠤ ᠂ ᠭᠠᠷ ᠤᠲᠠᠰᠤᠨ ᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠂ ᠬᠣᠶᠠᠷ ᠨᠣᠮᠧᠷ ᠢ ᠰᠢᠷᠪᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="86"/>
        <source>Use the mobile app to scan this code</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠡᠨᠡ ᠺᠣᠳ᠋ ᠢ ᠰᠢᠷᠪᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="45"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="56"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="95"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="101"/>
        <source>No app installed? Install Now</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠦᠭᠷᠠᠮ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠦᠦ ? ᠳᠠᠷᠤᠢ ᠲᠦᠷᠭᠡᠨ ᠤᠭᠰᠠᠷᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="50"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="61"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="171"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="176"/>
        <source>view supported phone types&gt;&gt;</source>
        <translation>ᠳᠡᠮᠵᠢᠯᠭᠡ ᠶᠢᠨ ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ &gt;&gt; ᠢ ᠦᠵᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="181"/>
        <source>Please scan this QR code with your mobile browser to download the app</source>
        <translation>ᠲᠠᠨ ᠤ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠦᠵᠡᠭᠦᠯᠦᠭᠦᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠡᠨᠡ ᠬᠣᠶᠠᠷ ᠨᠣᠮᠧᠷ ᠠᠴᠠ ᠳᠣᠷᠣᠭᠰᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ ᠰᠢᠷᠪᠢᠵᠦ ᠥᠭ᠍ᠭᠥᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>ScrollSettingWidget</name>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="22"/>
        <source>Mouse sensitivity</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨ᠎ᠠ ᠶᠢᠨ ᠬᠤᠷᠴᠠ ᠰᠡᠷᠭᠡᠭ ᠦᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="34"/>
        <source>slow</source>
        <translation>ᠠᠭᠠᠵᠢᠮ ᠢᠶᠠᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="35"/>
        <source>quick</source>
        <translation>ᠬᠤᠷᠳᠤᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="40"/>
        <source>ok</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="41"/>
        <source>cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>SearchDeviceWin</name>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="40"/>
        <source>Searching for nearby available devices...</source>
        <translation>ᠣᠶᠢᠷ᠎ᠠ ᠬᠠᠪᠢ ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠡᠷᠢᠵᠦ ᠁</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="76"/>
        <source>No network</source>
        <translation>ᠲᠣᠣᠷ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="178"/>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ ᠃</translation>
    </message>
</context>
<context>
    <name>SuspendTabBar</name>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="101"/>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="176"/>
        <source>Back to the desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠭᠤᠯᠢᠷ ᠲᠤ ᠪᠤᠴᠠᠶ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="105"/>
        <source>Resume screen projection</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ ᠶᠢ ᠰᠡᠷᠭᠦᠭᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="175"/>
        <source>Counter control (activate Ctrl+H, cancel Ctrl+G)</source>
        <translation>ᠲᠣᠭᠠᠴᠢᠯᠠᠭᠤᠷ ᠤᠨ ᠡᠵᠡᠮᠰᠢᠯ ( Ctrl+H ᠶᠢ ᠬᠥᠭᠵᠢᠭᠡᠨ ᠂ Ctrl+ G ᠶᠢ ᠦᠭᠡᠶᠢᠰᠬᠡᠨ᠎ᠡ )</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="177"/>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="214"/>
        <source>End screen projection</source>
        <translation>ᠺᠢᠨᠤ᠋ ᠶᠢᠨ ᠰᠡᠭᠦᠯ ᠦᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠵᠢᠷᠤᠭ ᠢ ᠰᠡᠭᠦᠳᠡᠷᠯᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="213"/>
        <source>End Counter Control (Ctrl+G)</source>
        <translation>ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠬᠡᠮᠵᠢᠭᠦᠷ ᠦᠨ ᠡᠵᠡᠮᠳᠡᠯ ( Ctrl+G )</translation>
    </message>
</context>
<context>
    <name>Titlebar</name>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="199"/>
        <source>Menu</source>
        <translation>ᠵᠠᠭᠤᠰᠢ ᠶᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="45"/>
        <source>kylin-connectivity</source>
        <translation>ᠣᠯᠠᠨ ᠦᠵᠦᠬᠦᠷ ᠲᠤ᠌ ᠬᠠᠮᠵᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="57"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠪᠠᠭ᠎ᠠ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="67"/>
        <location filename="../ui/view/titlebar.cpp" line="124"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠶᠡᠬᠡ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="77"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="128"/>
        <source>Reduction</source>
        <translation>ᠪᠠᠭᠠᠰᠴᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="216"/>
        <location filename="../ui/view/titlebar.cpp" line="256"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="217"/>
        <location filename="../ui/view/titlebar.cpp" line="258"/>
        <source>About</source>
        <translation>ᠪᠠᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="218"/>
        <location filename="../ui/view/titlebar.cpp" line="260"/>
        <source>Quit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>TransmissionDialog</name>
    <message>
        <location filename="../ui/basewidget/transmissiondialog.cpp" line="16"/>
        <source>Current progress</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ ᠠᠬᠢᠴᠠ ᠲᠠᠶ ᠪᠠᠶᠢᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>UsbConnectWin</name>
    <message>
        <location filename="../ui/connectinterface/usbconnectwin.cpp" line="71"/>
        <source>Connect now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠲᠦᠷᠭᠡᠨ ᠬᠣᠯᠪᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/usbconnectwin.cpp" line="116"/>
        <source>No tutorial</source>
        <translation>ᠲᠦᠷ ᠬᠢᠴᠢᠶᠡᠯ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>VideoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.cpp" line="776"/>
        <source>Control device not supported</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ᠃</translation>
    </message>
</context>
<context>
    <name>VideoTitle</name>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="66"/>
        <source>kylin-connectivity</source>
        <translation>ᠣᠯᠠᠨ ᠦᠵᠦᠬᠦᠷ ᠲᠤ᠌ ᠬᠠᠮᠵᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="145"/>
        <location filename="../projection/uibase/videotitle.cpp" line="183"/>
        <source>Hide Navigation Button</source>
        <translation>ᠵᠠᠮᠴᠢᠯᠠᠭᠴᠢ ᠶᠢᠨ ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠨᠢᠭᠤᠨ ᠳᠠᠯᠳᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="146"/>
        <source>Stay on top</source>
        <translation>ᠲᠡᠷᠢᠭᠦᠨ ᠦ ᠪᠠᠢ᠌ᠳᠠᠯ ᠢ ᠪᠠᠲᠤᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="147"/>
        <location filename="../projection/uibase/videotitle.cpp" line="209"/>
        <source>FullScreen</source>
        <translation>ᠪᠦᠬᠦ ᠳᠡᠯᠬᠡᠴᠡ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="148"/>
        <source>Mouse sensitivity</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨ᠎ᠠ ᠶᠢᠨ ᠬᠤᠷᠴᠠ ᠰᠡᠷᠭᠡᠭ ᠦᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="149"/>
        <source>Quit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="180"/>
        <source>Show Navigation Button</source>
        <translation>ᠵᠠᠮᠴᠢᠯᠠᠭᠴᠢ ᠶᠢᠨ ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="193"/>
        <source>Stay On Top</source>
        <translation>ᠲᠡᠷᠢᠭᠦᠨ ᠦ ᠪᠠᠢ᠌ᠳᠠᠯ ᠢ ᠪᠠᠲᠤᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="197"/>
        <source>Cancel Stay On Top</source>
        <translation>ᠣᠷᠣᠢ ᠳᠡᠭᠡᠷ᠎ᠡ ᠦᠯᠡᠳᠡᠭᠡᠬᠦ ᠶᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="213"/>
        <source>Cancel FullScreen</source>
        <translation>ᠪᠦᠬᠦ ᠳᠡᠯᠬᠡᠴᠡ ᠶᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>videoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.ui" line="17"/>
        <source>kylin-connectivity</source>
        <translation>ᠣᠯᠠᠨ ᠦᠵᠦᠬᠦᠷ ᠲᠤ᠌ ᠬᠠᠮᠵᠢᠯᠴᠠᠬᠤ</translation>
    </message>
</context>
</TS>
