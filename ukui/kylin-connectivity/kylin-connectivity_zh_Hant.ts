<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AndroidHomePage</name>
    <message>
        <location filename="../ui/filemanageview/androidhomepage.cpp" line="14"/>
        <source>Storage</source>
        <translation>存儲</translation>
    </message>
</context>
<context>
    <name>AndroidItem</name>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="69"/>
        <source>Picture</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="72"/>
        <source>Video</source>
        <translation>視頻</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="75"/>
        <source>Music</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="78"/>
        <source>Document</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="81"/>
        <source>WeChat</source>
        <translation>微信</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="84"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="87"/>
        <source>Storage</source>
        <translation>存儲</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="100"/>
        <source>All File</source>
        <translation>所有檔</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="102"/>
        <source>items</source>
        <translation>項</translation>
    </message>
</context>
<context>
    <name>BaseDevice</name>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="404"/>
        <source>Control Devices Supported</source>
        <translation>已支援反控設備</translation>
    </message>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="408"/>
        <source>Control device not supported</source>
        <translation>未支援反控設備</translation>
    </message>
</context>
<context>
    <name>ConnectInterface</name>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="19"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="33"/>
        <source>Nearby device</source>
        <translation>附近設備</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="34"/>
        <source>Connect Phone</source>
        <translation>連接手機</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="35"/>
        <source>Device Code</source>
        <translation>設備碼連接</translation>
    </message>
</context>
<context>
    <name>ConnectedWin</name>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="38"/>
        <source>CONNECTED</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="114"/>
        <location filename="../ui/view/connectedwin.cpp" line="122"/>
        <source>ComputerScreen</source>
        <translation>電腦投屏</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="120"/>
        <source>MobileScreen</source>
        <translation>手機投屏</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="175"/>
        <source>ExitScreen</source>
        <translation>退出投屏</translation>
    </message>
</context>
<context>
    <name>DeviceCodeWidget</name>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="28"/>
        <source>Please enter the connection code of the other device</source>
        <translation>請輸入對方設備的連接碼</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="37"/>
        <source>The connection code of the device can be obtained on the homepage of the other party&apos;s &apos;multi terminal collaboration&apos; application</source>
        <translation>設備的連接碼，可在對方“多端協同”應用的首頁中獲取</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="65"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
</context>
<context>
    <name>FileManageWin</name>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="25"/>
        <source>Go Back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="37"/>
        <source>Go Forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="57"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="406"/>
        <source>Search File</source>
        <translation>搜索檔</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="85"/>
        <source>Select</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="106"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="626"/>
        <source>List Mode</source>
        <translation>清單模式</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="118"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="124"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="432"/>
        <source>Select File</source>
        <translation>選擇檔案</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="133"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="212"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="646"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="650"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="658"/>
        <source>Select All</source>
        <translation>全選</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="142"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="210"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="647"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="649"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="657"/>
        <source>Deselect All</source>
        <translation>取消全選</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="631"/>
        <source>Icon Mode</source>
        <translation>圖示模式</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="91"/>
        <source>List of Files</source>
        <translation>檔案清單</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="92"/>
        <source>Picture</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="93"/>
        <source>Video</source>
        <translation>視頻</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="94"/>
        <source>Music</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="95"/>
        <source>Doc</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="96"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="97"/>
        <source>WeChat</source>
        <translation>微信</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="98"/>
        <source>Storage</source>
        <translation>存儲</translation>
    </message>
</context>
<context>
    <name>FileView</name>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="132"/>
        <location filename="../ui/filemanageview/fileview.cpp" line="525"/>
        <source>Download</source>
        <translation>下載至本地</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="405"/>
        <source>Choose folder</source>
        <translation>選擇資料夾</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="523"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../ui/view/homepage.cpp" line="42"/>
        <source>Multiterminal</source>
        <translation>多端協同</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="48"/>
        <source>Cross equipment and cross system collaboration. It is more convenient to share resources and screens, and more convenient and efficient to work!</source>
        <translation>跨設備、跨平臺協同，檔、螢幕實時共用，打造高效辦公方式！</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="55"/>
        <source>Device Code:</source>
        <translation>本機設備碼：</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="66"/>
        <source>Folder</source>
        <translation>檔共用</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="70"/>
        <source>Share</source>
        <translation>屏幕共用</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="74"/>
        <source>Cross</source>
        <translation>跨屏操控</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="77"/>
        <source>Connect Now</source>
        <translation>立即連接</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="185"/>
        <source>No network</source>
        <translation>無網路</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="381"/>
        <location filename="../ui/mainwindow.cpp" line="1380"/>
        <source>kylin-connectivity</source>
        <translation>多端協同</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="481"/>
        <source>Agreed to connect</source>
        <translation>同意連接</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="483"/>
        <source>Peer has agreed</source>
        <translation>對方已同意</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="485"/>
        <source>Establishing connection, please wait...</source>
        <translation>正在建立連接，請稍候</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="489"/>
        <source>CANCEL</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="939"/>
        <source>Not currently connected, please connect</source>
        <translation>當前未連接，請連接</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="695"/>
        <location filename="../ui/mainwindow.cpp" line="1368"/>
        <source>file download failed</source>
        <translation>檔案下載失敗</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1086"/>
        <source>Connection request received from&quot;</source>
        <translation>收到來自 ”</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1086"/>
        <location filename="../ui/mainwindow.cpp" line="1408"/>
        <source>&quot;</source>
        <translation>」 的請求</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1087"/>
        <source>After consent, the other party can view and download all the files on the device, and can share the other party&apos;s desktop to this screen.</source>
        <translation>同意后，對方可查看、下載本設備上的所有檔，以及可將對方桌面共用至本螢幕上。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1093"/>
        <location filename="../ui/mainwindow.cpp" line="1414"/>
        <source>NO</source>
        <translation>拒絕</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1092"/>
        <location filename="../ui/mainwindow.cpp" line="1413"/>
        <source>YES</source>
        <translation>同意</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1350"/>
        <source>File open exception!</source>
        <translation>檔打開異常！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1426"/>
        <source>The other party has refused your screen projection request!</source>
        <translation>對方拒絕了你的投屏請求！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1427"/>
        <source>Failed to cast the screen. Please contact the other party and try again.</source>
        <translation>投屏失敗，請聯繫對方后重新嘗試。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1112"/>
        <location filename="../ui/mainwindow.cpp" line="1431"/>
        <source>RECONNECT</source>
        <translation>重新連接</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1107"/>
        <source>The other party has rejected your connection request!</source>
        <translation>對方拒絕了您的連接請求！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1108"/>
        <source>Connection failed. Please contact the other party and try again.</source>
        <translation>連接失敗，請聯繫對方後重新嘗試。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1113"/>
        <location filename="../ui/mainwindow.cpp" line="1432"/>
        <location filename="../ui/mainwindow.cpp" line="1488"/>
        <location filename="../ui/mainwindow.cpp" line="1800"/>
        <source>CLOSE</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1211"/>
        <source>Please install kylin-assistant on the Android terminal!</source>
        <translation>請在手機端下載麒麟手機助手app！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1221"/>
        <source>Please use the USB to connect your phone device!</source>
        <translation>請使用USB連接您的手機設備！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1232"/>
        <source>Connection error</source>
        <translation>連接失敗</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1234"/>
        <source>Connection timed out</source>
        <translation>連接超時</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1991"/>
        <source>There is currently a connection in progress!</source>
        <translation>當前有連接正在進行！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1381"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1263"/>
        <source>No Content</source>
        <translation>無內容</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1383"/>
        <source>Mobile Assistant is an interconnection tool of Android device and Kirin operating system, which supports Android file synchronization, file transfer, screen mirroring and other functions, which is simple and fast to operate</source>
        <translation>多端協同是一款麒麟操作系統之間的互通互聯工具，同時也支援Android檔同步、檔傳輸、螢幕鏡像等功能，操作簡單快捷。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1407"/>
        <source>Received screen projection request from &quot;</source>
        <translation>收到來自 ”</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1409"/>
        <source>After consent, the other party can share the device desktop to this screen.</source>
        <translation>同意后，對方可將設備桌面共用至本螢幕上。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1446"/>
        <source>The other party agreed to your screen projection request!</source>
        <translation>對方同意了您的投屏請求！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1447"/>
        <source>The screen is being cast, please wait...</source>
        <translation>正在投屏，請稍等...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1472"/>
        <location filename="../ui/mainwindow.cpp" line="1495"/>
        <source>End of screen projection</source>
        <translation>投屏結束</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1473"/>
        <location filename="../ui/mainwindow.cpp" line="1496"/>
        <source>The other party has finished the screen projection function.</source>
        <translation>對方已結束了投屏功能。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1569"/>
        <source>Search data loading failed!</source>
        <translation>搜索數據載入失敗！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1587"/>
        <location filename="../ui/mainwindow.cpp" line="1590"/>
        <source>Transmission interruption</source>
        <translation>傳輸中斷</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1588"/>
        <source>The other party&apos;s device has insufficient local storage!</source>
        <translation>對方設備空間不足！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1590"/>
        <source>Insufficient local storage space!</source>
        <translation>本地設備空間不足！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1609"/>
        <source>No relevant results</source>
        <translation>無相關結果</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="2005"/>
        <source>The screen projection request has been sent to the connected device. Please click [Agree] in the opposite pop-up window</source>
        <translation>已將投屏請求發送至已連接設備，請在對端彈窗中點擊【同意】</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="941"/>
        <location filename="../ui/mainwindow.cpp" line="1213"/>
        <location filename="../ui/mainwindow.cpp" line="1223"/>
        <location filename="../ui/mainwindow.cpp" line="1296"/>
        <location filename="../ui/mainwindow.cpp" line="1352"/>
        <location filename="../ui/mainwindow.cpp" line="1451"/>
        <location filename="../ui/mainwindow.cpp" line="1477"/>
        <location filename="../ui/mainwindow.cpp" line="1500"/>
        <location filename="../ui/mainwindow.cpp" line="1571"/>
        <location filename="../ui/mainwindow.cpp" line="1593"/>
        <location filename="../ui/mainwindow.cpp" line="1987"/>
        <location filename="../ui/mainwindow.cpp" line="1993"/>
        <location filename="../ui/mainwindow.cpp" line="2010"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1482"/>
        <location filename="../ui/mainwindow.cpp" line="1794"/>
        <source>Screen projection loading error</source>
        <translation>投屏載入失敗</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1484"/>
        <location filename="../ui/mainwindow.cpp" line="1796"/>
        <source>Please check whether to install the projection expansion package [kylin connectivity tools]</source>
        <translation>請檢查是否安裝投屏擴展包[kylin-connectivity-tools]</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1784"/>
        <source>Uploaded to</source>
        <translation>上傳至</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1755"/>
        <source>Downloaded to</source>
        <translation>下載到</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1981"/>
        <location filename="../ui/mainwindow.cpp" line="2003"/>
        <source>Request sent successfully!</source>
        <translation>請求發送成功！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1982"/>
        <source>The connection request has been sent to the selected device. Please click [YES] in the opposite pop-up window</source>
        <translation>已將連接請求發送至所選設備，請在對端彈窗中點擊【同意】</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../ui/basewidget/messagedialog.cpp" line="29"/>
        <source>kylin-connectivity</source>
        <translation>多端協同</translation>
    </message>
    <message>
        <location filename="../ui/basewidget/messagedialog.cpp" line="40"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>MobileConnectWin</name>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="24"/>
        <source>ScanCode</source>
        <translation>掃碼連接</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="25"/>
        <source>USBConnect</source>
        <translation>USB連接</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="43"/>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="103"/>
        <source>Your phone model: </source>
        <translation>您的手機型號： </translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="49"/>
        <source>vivo</source>
        <translation>vivo</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="50"/>
        <source>HUAWEI</source>
        <translation>華為</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="51"/>
        <source>Xiaomi</source>
        <translation>小米</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="53"/>
        <source>OPPO</source>
        <translation>OPPO</translation>
    </message>
</context>
<context>
    <name>MobileQRcode</name>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="27"/>
        <source>No network</source>
        <translation>未連接網路</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="83"/>
        <source>Connect the mobile phone and computer to the same network,open the mobile phone app and scan the QR code.</source>
        <translation>將手機和電腦連接到同一網絡，打開手機應用程序並掃描二維碼。</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="86"/>
        <source>Use the mobile app to scan this code</source>
        <translation>使用手機端app掃描此碼</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="45"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="56"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="95"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="101"/>
        <source>No app installed? Install Now</source>
        <translation>未裝App？ 立即安裝</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="50"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="61"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="171"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="176"/>
        <source>view supported phone types&gt;&gt;</source>
        <translation>查看支援手機類型&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="181"/>
        <source>Please scan this QR code with your mobile browser to download the app</source>
        <translation>請使用手機瀏覽器掃描此二維碼以下載App</translation>
    </message>
</context>
<context>
    <name>ScrollSettingWidget</name>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="22"/>
        <source>Mouse sensitivity</source>
        <translation>滾輪靈敏度</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="34"/>
        <source>slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="35"/>
        <source>quick</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="40"/>
        <source>ok</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="41"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>SearchDeviceWin</name>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="40"/>
        <source>Searching for nearby available devices...</source>
        <translation>搜尋附近可用裝置...</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="76"/>
        <source>No network</source>
        <translation>未連接網路</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="178"/>
        <source>Search</source>
        <translation>重新搜索</translation>
    </message>
</context>
<context>
    <name>SuspendTabBar</name>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="101"/>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="176"/>
        <source>Back to the desktop</source>
        <translation>回到桌面</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="105"/>
        <source>Resume screen projection</source>
        <translation>恢復投屏</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="175"/>
        <source>Counter control (activate Ctrl+H, cancel Ctrl+G)</source>
        <translation>反控（啟動Ctrl+H，取消Ctrl+G）</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="177"/>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="214"/>
        <source>End screen projection</source>
        <translation>結束投屏</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="213"/>
        <source>End Counter Control (Ctrl+G)</source>
        <translation>結束反控（Ctrl+G）</translation>
    </message>
</context>
<context>
    <name>Titlebar</name>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="199"/>
        <source>Menu</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="45"/>
        <source>kylin-connectivity</source>
        <translation>多端協同</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="57"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="67"/>
        <location filename="../ui/view/titlebar.cpp" line="124"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="77"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="128"/>
        <source>Reduction</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="216"/>
        <location filename="../ui/view/titlebar.cpp" line="256"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="217"/>
        <location filename="../ui/view/titlebar.cpp" line="258"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="218"/>
        <location filename="../ui/view/titlebar.cpp" line="260"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TransmissionDialog</name>
    <message>
        <location filename="../ui/basewidget/transmissiondialog.cpp" line="16"/>
        <source>Current progress</source>
        <translation>當前進度</translation>
    </message>
</context>
<context>
    <name>UsbConnectWin</name>
    <message>
        <location filename="../ui/connectinterface/usbconnectwin.cpp" line="71"/>
        <source>Connect now</source>
        <translation>設置完成，立即連接</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/usbconnectwin.cpp" line="116"/>
        <source>No tutorial</source>
        <translation>暫無教程</translation>
    </message>
</context>
<context>
    <name>VideoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.cpp" line="776"/>
        <source>Control device not supported</source>
        <translation>當前連接不支援控制設備</translation>
    </message>
</context>
<context>
    <name>VideoTitle</name>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="66"/>
        <source>kylin-connectivity</source>
        <translation>多端協同</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="145"/>
        <location filename="../projection/uibase/videotitle.cpp" line="183"/>
        <source>Hide Navigation Button</source>
        <translation>隱藏導航欄</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="146"/>
        <source>Stay on top</source>
        <translation>置頂</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="147"/>
        <location filename="../projection/uibase/videotitle.cpp" line="209"/>
        <source>FullScreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="148"/>
        <source>Mouse sensitivity</source>
        <translation>滾輪靈敏度</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="149"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="180"/>
        <source>Show Navigation Button</source>
        <translation>顯示導航欄</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="193"/>
        <source>Stay On Top</source>
        <translation>置頂</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="197"/>
        <source>Cancel Stay On Top</source>
        <translation>取消置頂</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="213"/>
        <source>Cancel FullScreen</source>
        <translation>取消全屏</translation>
    </message>
</context>
<context>
    <name>videoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.ui" line="17"/>
        <source>kylin-connectivity</source>
        <translation>多端協同</translation>
    </message>
</context>
</TS>
