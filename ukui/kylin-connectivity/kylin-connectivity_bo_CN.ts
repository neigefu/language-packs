<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AndroidHomePage</name>
    <message>
        <location filename="../ui/filemanageview/androidhomepage.cpp" line="14"/>
        <source>Storage</source>
        <translation>གསོག་ཉར།</translation>
    </message>
</context>
<context>
    <name>AndroidItem</name>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="69"/>
        <source>Picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="72"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="75"/>
        <source>Music</source>
        <translation>རོལ་མོ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="78"/>
        <source>Document</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="81"/>
        <source>WeChat</source>
        <translation>འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="84"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="87"/>
        <source>Storage</source>
        <translation>གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="100"/>
        <source>All File</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/androiditem.cpp" line="102"/>
        <source>items</source>
        <translation>རྣམ་གྲངས།</translation>
    </message>
</context>
<context>
    <name>BaseDevice</name>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="404"/>
        <source>Control Devices Supported</source>
        <translation>རྒྱབ་སྐྱོར་བྱེད་པའི་ཚོད་འཛིན་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="408"/>
        <source>Control device not supported</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཚོད་འཛིན་སྒྲིག་ཆས།</translation>
    </message>
</context>
<context>
    <name>ConnectInterface</name>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="19"/>
        <source>Back</source>
        <translation>ཕྱིར་ལོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="33"/>
        <source>Nearby device</source>
        <translation>ཉེ་འགྲམ་གྱི་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="34"/>
        <source>Connect Phone</source>
        <translation>ཁ་པར་སྦྲེལ་མཐུད་བྱེད་</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/connectinterface.cpp" line="35"/>
        <source>Device Code</source>
        <translation>སྒྲིག་ཆས་ཀྱི་ཚབ་རྟགས།</translation>
    </message>
</context>
<context>
    <name>ConnectedWin</name>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="38"/>
        <source>CONNECTED</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="114"/>
        <location filename="../ui/view/connectedwin.cpp" line="122"/>
        <source>ComputerScreen</source>
        <translation>རྩིས་འཁོར་གྱི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="120"/>
        <source>MobileScreen</source>
        <translation>སྒུལ་བདེའི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedwin.cpp" line="175"/>
        <source>ExitScreen</source>
        <translation>ཕྱིར་གཏོང་བརྙན་ཤེལ།</translation>
    </message>
</context>
<context>
    <name>DeviceCodeWidget</name>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="28"/>
        <source>Please enter the connection code of the other device</source>
        <translation>སྒྲིག་ཆས་གཞན་ཞིག་གི་འབྲེལ་མཐུད་ཨང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="37"/>
        <source>The connection code of the device can be obtained on the homepage of the other party&apos;s &apos;multi terminal collaboration&apos; application</source>
        <translation>སྒྲིག་ཆས་འདིའི་འབྲེལ་མཐུད་ཨང་གྲངས་ནི་ཕྱོགས་ཅིག་ཤོས་ཀྱི་&quot;མཐའ་སྣེའི་མཉམ་ལས་&quot;རེ་ཞུའི་དྲ་ངོས་སུ་ཐོབ་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/devicecodewidget.cpp" line="65"/>
        <source>Connection</source>
        <translation>འབྲེལ་མཐུད་</translation>
    </message>
</context>
<context>
    <name>FileManageWin</name>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="25"/>
        <source>Go Back</source>
        <translation>ཕྱིར་རྒྱུགས།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="37"/>
        <source>Go Forward</source>
        <translation>མདུན་དུ་བསྐྱོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="57"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="406"/>
        <source>Search File</source>
        <translation>འཚོལ་བཤེར་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="85"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="106"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="626"/>
        <source>List Mode</source>
        <translation>མིང་ཐོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="118"/>
        <source>Refresh</source>
        <translation>དྭངས་ཤིང་གཙང་བ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="124"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="432"/>
        <source>Select File</source>
        <translation>ཡིག་ཆ་གདམ་གསེས་བྱེད་</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="133"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="212"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="646"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="650"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="658"/>
        <source>Select All</source>
        <translation>ཚང་མ་བདམས་པ་</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="142"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="210"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="647"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="649"/>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="657"/>
        <source>Deselect All</source>
        <translation>ཚང་མ་ཕྱིར་འབུད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.cpp" line="631"/>
        <source>Icon Mode</source>
        <translation>མཚོན་རྟགས་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="91"/>
        <source>List of Files</source>
        <translation>ཡིག་ཆའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="92"/>
        <source>Picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="93"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="94"/>
        <source>Music</source>
        <translation>རོལ་མོ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="95"/>
        <source>Doc</source>
        <translation>ཏོ་ཁི་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="96"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="97"/>
        <source>WeChat</source>
        <translation>འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/filemanagewin.h" line="98"/>
        <source>Storage</source>
        <translation>གསོག་ཉར།</translation>
    </message>
</context>
<context>
    <name>FileView</name>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="132"/>
        <location filename="../ui/filemanageview/fileview.cpp" line="525"/>
        <source>Download</source>
        <translation>ཕབ་ལེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="405"/>
        <source>Choose folder</source>
        <translation>ཡིག་སྣོད་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/filemanageview/fileview.cpp" line="523"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <location filename="../ui/view/homepage.cpp" line="42"/>
        <source>Multiterminal</source>
        <translation>གཏན་འཁེལ་མང་བ།</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="48"/>
        <source>Cross equipment and cross system collaboration. It is more convenient to share resources and screens, and more convenient and efficient to work!</source>
        <translation>སྒྲིག་ཆས་ལས་བརྒལ་བ་དང་མ་ལག་ལས་བརྒལ་བའི་མཉམ་ ཐོན་ཁུངས་དང་བརྙན་ཤེལ་མཉམ་སྤྱོད་བྱེད་པར་དེ་བས་སྟབས་བདེ་དང་ལས་ཀ་དེ་བས་སྟབས་བདེ་དང་ལས་ཆོད་ཆེ་རུ་ཕྱིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="55"/>
        <source>Device Code:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་ཚབ་རྟགས་ནི།</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="66"/>
        <source>Folder</source>
        <translation>ཡིག་སྣོད་</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="70"/>
        <source>Share</source>
        <translation>མ་རྐང་འཛིན་ཤོག</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="74"/>
        <source>Cross</source>
        <translation>རྒྱ་གྲམ་རྟགས་ཅན།</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="77"/>
        <source>Connect Now</source>
        <translation>ད་ལྟ་སྦྲེལ་མཐུད་བྱེད་</translation>
    </message>
    <message>
        <location filename="../ui/view/homepage.cpp" line="185"/>
        <source>No network</source>
        <translation>དྲ་རྒྱ་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="381"/>
        <location filename="../ui/mainwindow.cpp" line="1380"/>
        <source>kylin-connectivity</source>
        <translation>ཕན་ཚུན་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="481"/>
        <source>Agreed to connect</source>
        <translation>འབྲེལ་མཐུད་བྱ་རྒྱུར་མོས་མཐུན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="483"/>
        <source>Peer has agreed</source>
        <translation>ལས་རིགས་གཅིག་པའི་ཁ་ཆད་བཞག་ཟིན</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="485"/>
        <source>Establishing connection, please wait...</source>
        <translation>འབྲེལ་བ་བཙུགས་ནས་སྒུག་དང་།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="489"/>
        <source>CANCEL</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="939"/>
        <source>Not currently connected, please connect</source>
        <translation>མིག་སྔར་འབྲེལ་མཐུད་མ་བྱས་ན་འབྲེལ་མཐུད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="695"/>
        <location filename="../ui/mainwindow.cpp" line="1368"/>
        <source>file download failed</source>
        <translation>ཡིག་ཆ་ཕབ་ལེན་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1086"/>
        <source>Connection request received from&quot;</source>
        <translation>འབྲེལ་མཐུད་ཀྱི་བླང་བྱ་འབྱོར་བ་&quot;ཞེས་པར་ལེན་</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1086"/>
        <location filename="../ui/mainwindow.cpp" line="1408"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1087"/>
        <source>After consent, the other party can view and download all the files on the device, and can share the other party&apos;s desktop to this screen.</source>
        <translation>མོས་མཐུན་བྱུང་རྗེས་ཕྱོགས་ཅིག་ཤོས་ཀྱིས་སྒྲིག་ཆས་སྟེང་གི་ཡིག་ཆ་ཡོད་ཚད་ལ་ལྟ་ཞིབ་དང་ཕབ་ལེན་བྱས་ཆོག་པ་མ་ཟད། ཕྱོགས་ཅིག་ཤོས་ཀྱི་ཅོག་ཙེ་དེ་བརྙན་ཤེལ་འདིའི་སྟེང་མཉམ་སྤྱོད་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1093"/>
        <location filename="../ui/mainwindow.cpp" line="1414"/>
        <source>NO</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1092"/>
        <location filename="../ui/mainwindow.cpp" line="1413"/>
        <source>YES</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1350"/>
        <source>File open exception!</source>
        <translation>ཡིག་ཆ་ཁ་ཕྱེ་ནས་དམིགས་བསལ་དུ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1426"/>
        <source>The other party has refused your screen projection request!</source>
        <translation>ཕྱོགས་ཅིག་ཤོས་ཀྱིས་ཁྱེད་ཚོའི་བརྙན་ཤེལ་གྱི་རྣམ་གྲངས་བླང་བྱ་དང་ལེན་མ་བྱས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1427"/>
        <source>Failed to cast the screen. Please contact the other party and try again.</source>
        <translation>བརྙན་ཤེལ་ལ་སྐྱོན་ཤོར་བ་རེད། ཕྱོགས་ཅིག་ཤོས་ལ་འབྲེལ་གཏུག་བྱས་ནས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1112"/>
        <location filename="../ui/mainwindow.cpp" line="1431"/>
        <source>RECONNECT</source>
        <translation>བསྐྱར་དུ་འབྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1107"/>
        <source>The other party has rejected your connection request!</source>
        <translation>ཕྱོགས་ཅིག་ཤོས་ཀྱིས་ཁྱེད་ཚོའི་འབྲེལ་གཏུག་རེ་བ་དང་ལེན་མ་བྱས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1108"/>
        <source>Connection failed. Please contact the other party and try again.</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པ་རེད། ཕྱོགས་ཅིག་ཤོས་ལ་འབྲེལ་གཏུག་བྱས་ནས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1113"/>
        <location filename="../ui/mainwindow.cpp" line="1432"/>
        <location filename="../ui/mainwindow.cpp" line="1488"/>
        <location filename="../ui/mainwindow.cpp" line="1800"/>
        <source>CLOSE</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1211"/>
        <source>Please install kylin-assistant on the Android terminal!</source>
        <translation>ཨན་ཏུང་གི་མཐའ་སྣེའི་སྟེང་དུ་ཅིན་ལིན་གྱི་ལས་རོགས་སྒྲིག་སྦྱོར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1221"/>
        <source>Please use the USB to connect your phone device!</source>
        <translation>ཁྱེད་ཀྱིས་ཨ་སྒོར་བཀོལ་ནས་ཁྱེད་ཀྱི་ལག་ཐོགས་ཁ་པར་གྱི་སྒྲིག་ཆས་སྦྲེལ་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1232"/>
        <source>Connection error</source>
        <translation>འབྲེལ་མཐུད་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1234"/>
        <source>Connection timed out</source>
        <translation>དུས་ཐོག་ཏུ་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1991"/>
        <source>There is currently a connection in progress!</source>
        <translation>མིག་སྔར་ཡར་ཐོན་ཡོང་བར་འབྲེལ་བ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1381"/>
        <source>Version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1263"/>
        <source>No Content</source>
        <translation>ནང་དོན་མེད་པ་</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1383"/>
        <source>Mobile Assistant is an interconnection tool of Android device and Kirin operating system, which supports Android file synchronization, file transfer, screen mirroring and other functions, which is simple and fast to operate</source>
        <translation>སྒུལ་བདེའི་ལས་རོགས་ནི་ཨན་ཏུང་གི་སྒྲིག་ཆས་དང་ཁི་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་ཕན་ཚུན་འབྲེལ་མཐུད་བྱེད་པའི་ཡོ་བྱད་ཅིག་ཡིན་ཞིང་། དེས་ཨན་ཏུང་གི་ཡིག་ཆ་དུས་གཅིག་ཏུ་འགྱུར་བ་དང་། ཡིག་ཆ་སྤོ་སྒྱུར། བརྙན་ཤེལ་མེ་ལོང་སོགས་ཀྱི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1407"/>
        <source>Received screen projection request from &quot;</source>
        <translation>&quot;བརྙན་ཤེལ་གྱི་རྣམ་གྲངས་རེ་ཞུ་འབྱོར་བ་&quot;ཞེས་བཤད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1409"/>
        <source>After consent, the other party can share the device desktop to this screen.</source>
        <translation>མོས་མཐུན་བྱུང་རྗེས་ཕྱོགས་ཅིག་ཤོས་ཀྱིས་སྒྲིག་ཆས་ཀྱི་ཅོག་ཙེ་དེ་བརྙན་ཤེལ་འདིའི་སྟེང་མཉམ་སྤྱོད་བྱས་ཆོག།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1446"/>
        <source>The other party agreed to your screen projection request!</source>
        <translation>ཕྱོགས་ཅིག་ཤོས་ཀྱིས་ཁྱེད་ཚོའི་བརྙན་ཤེལ་གྱི་རྣམ་གྲངས་བླང་བྱར་མོས་མཐུན་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1447"/>
        <source>The screen is being cast, please wait...</source>
        <translation>བརྙན་ཤེལ་སྟོན་བཞིན་པའི་སྐབས་སུ་ཁྱེད་ཀྱིས་སྒུག་དང་།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1472"/>
        <location filename="../ui/mainwindow.cpp" line="1495"/>
        <source>End of screen projection</source>
        <translation>བརྙན་ཤེལ་གྱི་སྔོན་དཔག་མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1473"/>
        <location filename="../ui/mainwindow.cpp" line="1496"/>
        <source>The other party has finished the screen projection function.</source>
        <translation>ཕྱོགས་ཅིག་ཤོས་ཀྱིས་བརྙན་ཤེལ་གྱི་རྣམ་གྲངས་ཀྱི་ནུས་པ་ལེགས་འགྲུབ་བྱུང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1569"/>
        <source>Search data loading failed!</source>
        <translation>འཚོལ་ཞིབ་ཀྱི་གཞི་གྲངས་ནང་འཇུག་བྱེད་པར་ཕམ་ཉེས་བྱུང་</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1587"/>
        <location filename="../ui/mainwindow.cpp" line="1590"/>
        <source>Transmission interruption</source>
        <translation>བརྒྱུད་སྐྱེལ་མཚམས་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1588"/>
        <source>The other party&apos;s device has insufficient local storage!</source>
        <translation>ཕྱོགས་ཅིག་ཤོས་ཀྱི་སྒྲིག་ཆས་ལ་ས་གནས་དེ་གའི་གསོག་ཉར་མི་འདང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1590"/>
        <source>Insufficient local storage space!</source>
        <translation>ས་གནས་དེ་གའི་གསོག་ཉར་བྱེད་ས་མི་</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1609"/>
        <source>No relevant results</source>
        <translation>འབྲེལ་མེད་འབྲས་བུ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="2005"/>
        <source>The screen projection request has been sent to the connected device. Please click [Agree] in the opposite pop-up window</source>
        <translation>བརྙན་ཤེལ་གྱི་འཆར་འགོད་བླང་བྱ་དེ་འབྲེལ་མཐུད་སྒྲིག་ཆས་ལ་བསྐྱལ་ཟིན། ཁྱོད་ཀྱིས་ཁ་གཏད་ཀྱི་སྒེའུ་ཁུང་ནས་[འཐད་པ་བྱུང་]ཞེས་པའི་ཁ་པར་ཞིག་རྒྱག་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="941"/>
        <location filename="../ui/mainwindow.cpp" line="1213"/>
        <location filename="../ui/mainwindow.cpp" line="1223"/>
        <location filename="../ui/mainwindow.cpp" line="1296"/>
        <location filename="../ui/mainwindow.cpp" line="1352"/>
        <location filename="../ui/mainwindow.cpp" line="1451"/>
        <location filename="../ui/mainwindow.cpp" line="1477"/>
        <location filename="../ui/mainwindow.cpp" line="1500"/>
        <location filename="../ui/mainwindow.cpp" line="1571"/>
        <location filename="../ui/mainwindow.cpp" line="1593"/>
        <location filename="../ui/mainwindow.cpp" line="1987"/>
        <location filename="../ui/mainwindow.cpp" line="1993"/>
        <location filename="../ui/mainwindow.cpp" line="2010"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1482"/>
        <location filename="../ui/mainwindow.cpp" line="1794"/>
        <source>Screen projection loading error</source>
        <translation>བརྙན་ཤེལ་གྱི་ནང་དུ་བཅུག་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1484"/>
        <location filename="../ui/mainwindow.cpp" line="1796"/>
        <source>Please check whether to install the projection expansion package [kylin connectivity tools]</source>
        <translation>ཁྱེད་ཀྱིས་རྣམ་གྲངས་རྒྱ་སྐྱེད་གཏོང་བའི་ཇུས་གཞི་[kylinསྦྲེལ་མཐུད་ཡོ་བྱད་]སྒྲིག་སྦྱོར་བྱེད་མིན་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1784"/>
        <source>Uploaded to</source>
        <translation>གོང་དུ་སྤེལ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1755"/>
        <source>Downloaded to</source>
        <translation>ཕབ་ལེན་བྱས་ནས་ཕབ་ལེན་བྱས</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1981"/>
        <location filename="../ui/mainwindow.cpp" line="2003"/>
        <source>Request sent successfully!</source>
        <translation>རེ་འདུན་ཞུ་ཡིག་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1982"/>
        <source>The connection request has been sent to the selected device. Please click [YES] in the opposite pop-up window</source>
        <translation>འབྲེལ་མཐུད་ཀྱི་རེ་བ་དེ་བདམས་ཟིན་པའི་སྒྲིག་ཆས་ལ་བསྐྱལ་ཟིན། ཁྱོད་ཀྱིས་ཁ་གཏད་ཀྱི་སྒེའུ་ཁུང་ནས་[YES]ལ་ཞིབ་འཇུག་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../ui/basewidget/messagedialog.cpp" line="29"/>
        <source>kylin-connectivity</source>
        <translation>ཕན་ཚུན་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/basewidget/messagedialog.cpp" line="40"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>MobileConnectWin</name>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="24"/>
        <source>ScanCode</source>
        <translation>ཞིབ་བཤེར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="25"/>
        <source>USBConnect</source>
        <translation>USBསྦྲེལ་མཐུད</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="43"/>
        <location filename="../ui/connectinterface/mobileconnectwin.cpp" line="103"/>
        <source>Your phone model: </source>
        <translation>ཁྱེད་ཀྱི་ལག་ཁྱེར་ཁ་པར་གྱི་དཔེ་ </translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="49"/>
        <source>vivo</source>
        <translation>vivo</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="50"/>
        <source>HUAWEI</source>
        <translation>ཧྭ་ཝེ།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="51"/>
        <source>Xiaomi</source>
        <translation>ཞའོ་སྨི།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileconnectwin.h" line="53"/>
        <source>OPPO</source>
        <translation>OPPO</translation>
    </message>
</context>
<context>
    <name>MobileQRcode</name>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="27"/>
        <source>No network</source>
        <translation>དྲ་རྒྱ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="83"/>
        <source>Connect the mobile phone and computer to the same network,open the mobile phone app and scan the QR code.</source>
        <translation>ལག་ཐོགས་ཁ་པར་དང་རྩིས་འཁོར་དྲ་རྒྱ་གཅིག་ཏུ་སྦྲེལ་བ་དང་། ལག་ཐོགས་ཁ་པར་གྱི་ཉེར་སྤྱོད་ཁ་ཕྱེ་ནས་QRཡི་ཚབ་རྟགས་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="86"/>
        <source>Use the mobile app to scan this code</source>
        <translation>སྒུལ་བདེའི་ཉེར་སྤྱོད་གོ་རིམ་སྤྱད་དེ་ཚབ་རྟགས་འདི་ལ་ཞིབ་</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="45"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="56"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="95"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="101"/>
        <source>No app installed? Install Now</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་དམ། ད་ལྟ་སྒྲིག་སྦྱོར་བྱས་</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="50"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="61"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="171"/>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="176"/>
        <source>view supported phone types&gt;&gt;</source>
        <translation>རྒྱབ་སྐྱོར་བྱས་པའི་ཁ་པར་གྱི་རིགས་དབྱིབས་ལ་ལྟ&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/mobileqrcode.cpp" line="181"/>
        <source>Please scan this QR code with your mobile browser to download the app</source>
        <translation>ཁྱེད་ཀྱི་སྒུལ་བདེའི་བལྟ་ཆས་ཀྱིས་QRཡི་ཚབ་རྟགས་འདི་ལ་ཞིབ་བཤེར་བྱས་ནས་ཉེར་སྤྱོད་གོ་རིམ་ཕབ་ལེན</translation>
    </message>
</context>
<context>
    <name>ScrollSettingWidget</name>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="22"/>
        <source>Mouse sensitivity</source>
        <translation>བྱི་བའི་ཚོར་སྐྱེན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="34"/>
        <source>slow</source>
        <translation>དལ་མོ་ཡིན་པ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="35"/>
        <source>quick</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="40"/>
        <source>ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="41"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>SearchDeviceWin</name>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="40"/>
        <source>Searching for nearby available devices...</source>
        <translation>ཉེ་འགྲམ་དུ་སྤྱོད་ཆོག་པའི་སྒྲིག་ཆས་འཚོལ་ཞིབ་བྱ་རྒྱུ་</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="76"/>
        <source>No network</source>
        <translation>དྲ་རྒྱ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/searchdevicewin.cpp" line="178"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>SuspendTabBar</name>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="101"/>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="176"/>
        <source>Back to the desktop</source>
        <translation>ཡང་བསྐྱར་ཅོག་ཙེའི་སྟེང་དུ་ལོག</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="105"/>
        <source>Resume screen projection</source>
        <translation>བརྙན་ཤེལ་གྱི་རྣམ་གྲངས་སླར་གསོ</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="175"/>
        <source>Counter control (activate Ctrl+H, cancel Ctrl+G)</source>
        <translation>ལྡོག་ཕྱོགས་ཀྱི་ཚོད་འཛིན་(Ctrl+H,Ctrl+G)མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="177"/>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="214"/>
        <source>End screen projection</source>
        <translation>མཇུག་སྒྲིལ་བའི་བརྙན་ཤེལ་གྱི་</translation>
    </message>
    <message>
        <location filename="../pcscreenmanage/suspendtabbar.cpp" line="213"/>
        <source>End Counter Control (Ctrl+G)</source>
        <translation>མཇུག་སྒྲིལ་བའི་ཚོད་འཛིན་(Ctrl+G)</translation>
    </message>
</context>
<context>
    <name>Titlebar</name>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="199"/>
        <source>Menu</source>
        <translation>ཟས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="45"/>
        <source>kylin-connectivity</source>
        <translation>ཕན་ཚུན་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="57"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="67"/>
        <location filename="../ui/view/titlebar.cpp" line="124"/>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="77"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="128"/>
        <source>Reduction</source>
        <translation>ཉུང་འཕྲི།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="216"/>
        <location filename="../ui/view/titlebar.cpp" line="256"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="217"/>
        <location filename="../ui/view/titlebar.cpp" line="258"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="218"/>
        <location filename="../ui/view/titlebar.cpp" line="260"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
</context>
<context>
    <name>TransmissionDialog</name>
    <message>
        <location filename="../ui/basewidget/transmissiondialog.cpp" line="16"/>
        <source>Current progress</source>
        <translation>མིག་སྔའི་འཕེལ་རིམ།</translation>
    </message>
</context>
<context>
    <name>UsbConnectWin</name>
    <message>
        <location filename="../ui/connectinterface/usbconnectwin.cpp" line="71"/>
        <source>Connect now</source>
        <translation>ད་ལྟ་སྦྲེལ་མཐུད་བྱེད་</translation>
    </message>
    <message>
        <location filename="../ui/connectinterface/usbconnectwin.cpp" line="116"/>
        <source>No tutorial</source>
        <translation>ཟུར་ཁྲིད་མི་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>VideoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.cpp" line="776"/>
        <source>Control device not supported</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཚོད་འཛིན་སྒྲིག་ཆས།</translation>
    </message>
</context>
<context>
    <name>VideoTitle</name>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="66"/>
        <source>kylin-connectivity</source>
        <translation>ཕན་ཚུན་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="145"/>
        <location filename="../projection/uibase/videotitle.cpp" line="183"/>
        <source>Hide Navigation Button</source>
        <translation>ཕྱོགས་སྟོན་གྱི་སྒྲོག་གུ་སྦས་སྐུ</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="146"/>
        <source>Stay on top</source>
        <translation>མུ་མཐུད་དུ་གོང་ནས་འོག་</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="147"/>
        <location filename="../projection/uibase/videotitle.cpp" line="209"/>
        <source>FullScreen</source>
        <translation>བརྙན་ཤེལ་ཆ་ཚང་།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="148"/>
        <source>Mouse sensitivity</source>
        <translation>བྱི་བའི་ཚོར་སྐྱེན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="149"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="180"/>
        <source>Show Navigation Button</source>
        <translation>ཕྱོགས་སྟོན་གྱི་སྒྲོག་གུ་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="193"/>
        <source>Stay On Top</source>
        <translation>མུ་མཐུད་དུ་གོང་ནས་འོག་</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="197"/>
        <source>Cancel Stay On Top</source>
        <translation>མུ་མཐུད་དུ་གོང་ནས་འོག་བར་དུ</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="213"/>
        <source>Cancel FullScreen</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་བོ་མེད་པར་</translation>
    </message>
</context>
<context>
    <name>videoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.ui" line="17"/>
        <source>kylin-connectivity</source>
        <translation>ཕན་ཚུན་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
</context>
</TS>
