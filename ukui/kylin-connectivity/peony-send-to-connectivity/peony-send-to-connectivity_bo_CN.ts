<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::SendToKConnectivity</name>
    <message>
        <location filename="../plugin/peony-send-to-connectivity/send-to-connectivity.cpp" line="70"/>
        <source>send to connectivity</source>
        <translation>ཕན་ཚུན་འབྲེལ་མཐུད་བྱེད་པར་བསྐྱལ་</translation>
    </message>
    <message>
        <location filename="../plugin/peony-send-to-connectivity/send-to-connectivity.h" line="58"/>
        <source>Send to a connectivity</source>
        <translation>སྦྲེལ་མཐུད་ཀྱི་གནས་སུ་བསྐྱལ།</translation>
    </message>
</context>
</TS>
