<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="272"/>
        <source>File does not exist</source>
        <translation>文件不存在</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="243"/>
        <source>Failed to copy. Reason: %1</source>
        <translation>复制失败.原因：</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="265"/>
        <source>This is not a local file</source>
        <translation>非本机文件</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="277"/>
        <source>This is not a file</source>
        <translation>这不是文件</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="306"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="524"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="559"/>
        <source>Operation not supported</source>
        <translation>操作不支持</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="311"/>
        <source>MULT may not have started</source>
        <translation>多端协同可能尚未启动</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="358"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="67"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="71"/>
        <source>Multiterminal</source>
        <translation>多端协同</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="443"/>
        <source>Incorrect path</source>
        <translation>错误路径</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-register.h" line="51"/>
        <source>Default multiterminal vfs of peony</source>
        <translation>默认多端协同vfs</translation>
    </message>
</context>
</TS>
