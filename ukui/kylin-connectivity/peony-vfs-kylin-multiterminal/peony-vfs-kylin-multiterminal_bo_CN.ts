<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="272"/>
        <source>File does not exist</source>
        <translation>ཡིག་ཆ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="243"/>
        <source>Failed to copy. Reason: %1</source>
        <translation>འདྲ་བཤུས་བྱེད་མ་ཐུབ་པ་རེད། རྒྱུ་མཚན་ནི། %1</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="265"/>
        <source>This is not a local file</source>
        <translation>འདི་ནི་ས་གནས་དེ་གའི་ཡིག་ཆ་མིན།</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="277"/>
        <source>This is not a file</source>
        <translation>འདི་ནི་ཡིག་ཆ་ཞིག་མ་རེད།</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="306"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="524"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="559"/>
        <source>Operation not supported</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་བྱ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="311"/>
        <source>MULT may not have started</source>
        <translation>MULT འགོ་ཚུགས་མེད་པ་འདྲ།</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="358"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="67"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="71"/>
        <source>Multiterminal</source>
        <translation>གཏན་འཁེལ་མང་བ།</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="443"/>
        <source>Incorrect path</source>
        <translation>ཡང་དག་མིན་པའི་འགྲོ་ལམ</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-register.h" line="51"/>
        <source>Default multiterminal vfs of peony</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་སྣ་མང་རང་བཞིན་གྱི་མེ་ཏོག་གི་མེ་ཏོག</translation>
    </message>
</context>
</TS>
