<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="272"/>
        <source>File does not exist</source>
        <translation>ᠹᠠᠢᠯ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="243"/>
        <source>Failed to copy. Reason: %1</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠵᠣ ᠳᠡᠢᠯᠦᠭᠰᠡᠨ ᠦᠭᠡᠢ᠃ ᠰᠢᠯᠲᠠᠭᠠᠨ: %1</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="265"/>
        <source>This is not a local file</source>
        <translation>ᠡᠨᠡ ᠪᠣᠯ ᠲᠤᠰ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="277"/>
        <source>This is not a file</source>
        <translation>ᠡᠨᠡ ᠪᠣᠯ ᠹᠠᠢᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="306"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="524"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="559"/>
        <source>Operation not supported</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤ ᠶ᠋ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="311"/>
        <source>MULT may not have started</source>
        <translation>MULT ᠶ᠋ᠢ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠭᠡᠳᠦᠢ ᠪᠠᠢ᠌ᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="358"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="67"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="71"/>
        <source>Multiterminal</source>
        <translation>ᠤᠯᠠᠨ ᠦᠵᠦᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="443"/>
        <source>Incorrect path</source>
        <translation>ᠵᠢᠮ ᠪᠤᠷᠤᠭᠤᠳᠠᠵᠠᠢ</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-register.h" line="51"/>
        <source>Default multiterminal vfs of peony</source>
        <translation>peony ᠶ᠋ᠢᠨ vfs ᠲᠡᠬᠢ ᠠᠶᠠᠳᠠᠯ ᠤᠯᠠᠨ ᠦᠵᠦᠭᠦᠷ</translation>
    </message>
</context>
</TS>
