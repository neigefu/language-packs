<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="272"/>
        <source>File does not exist</source>
        <translation>檔不存在</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="243"/>
        <source>Failed to copy. Reason: %1</source>
        <translation>複製失敗。原因： %1</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="265"/>
        <source>This is not a local file</source>
        <translation>這不是本地檔</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="277"/>
        <source>This is not a file</source>
        <translation>這不是檔</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="306"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="524"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="559"/>
        <source>Operation not supported</source>
        <translation>不支援操作</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="311"/>
        <source>MULT may not have started</source>
        <translation>MULT 可能尚未啟動</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="358"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="67"/>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/peony-vfs-multiterminal.h" line="71"/>
        <source>Multiterminal</source>
        <translation>多終端</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-file.cpp" line="443"/>
        <source>Incorrect path</source>
        <translation>路徑不正確</translation>
    </message>
    <message>
        <location filename="../plugin/peony-vfs-kylin-multiterminal/multiterminal-vfs-register.h" line="51"/>
        <source>Default multiterminal vfs of peony</source>
        <translation>牡丹的預設多終端vfs</translation>
    </message>
</context>
</TS>
