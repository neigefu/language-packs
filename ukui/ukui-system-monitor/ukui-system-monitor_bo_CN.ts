<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BaseDetailViewWidget</name>
    <message>
        <location filename="../sysresource/basedetailviewwidget.cpp" line="31"/>
        <source>Hide Details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ་སྦས་སྐུང་།</translation>
    </message>
</context>
<context>
    <name>ChartViewWidget</name>
    <message>
        <location filename="../sysresource/chartviewwidget.cpp" line="113"/>
        <location filename="../sysresource/chartviewwidget.cpp" line="131"/>
        <source>Receive</source>
        <translation>བསྡུ་ལེན།</translation>
    </message>
    <message>
        <location filename="../sysresource/chartviewwidget.cpp" line="118"/>
        <location filename="../sysresource/chartviewwidget.cpp" line="136"/>
        <source>Send</source>
        <translation>སྐྱེལ་སྤྲོད།</translation>
    </message>
</context>
<context>
    <name>CpuDetailsModel</name>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="40"/>
        <source>Utilization</source>
        <translation>སྤྱིའི་སྤྱོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="42"/>
        <source>Current frequency</source>
        <translation>མིག་སྔའི་ཟློས་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="47"/>
        <source>Frequency</source>
        <translation>ཟློས་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="49"/>
        <source>Up time</source>
        <translation>འཁོར་སྐྱོད་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="53"/>
        <source>Sockets</source>
        <translation>བར་ཁྱམས།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="55"/>
        <source>Logical processors</source>
        <translation>གཏན་ཚིགས་སྒྲིག་གཅོད་ཆས།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="59"/>
        <source>Virtualization</source>
        <translation>རྟོག་བཟོ།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="61"/>
        <source>L1i cache</source>
        <translation>L1ལྷོད་གསོག（བཀའ་བརྡ）</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="65"/>
        <source>L1d cache</source>
        <translation>L1ལྷོད་གསོག（གཞི་གྲངས།）</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="67"/>
        <source>L2 cache</source>
        <translation>L2གསོག་འཇོག</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="71"/>
        <source>L3 cache</source>
        <translation>L3གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="73"/>
        <source>Load average</source>
        <translation>ཐེག་ཚད་དོ་མཉམ།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="77"/>
        <source>File descriptors</source>
        <translation>ཡིག་ཆའི་ཞིབ་འབྲིའི་རྟགས།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="79"/>
        <source>Processes</source>
        <translation type="unfinished">བཟོ་རྩལ།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="83"/>
        <source>Threads</source>
        <translation>ཐིག་ཚད།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="85"/>
        <source>Host name</source>
        <translation>རྩིས་འཁོར་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="89"/>
        <source>OS type</source>
        <translation>རིགས་དབྱེ།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailsmodel.h" line="91"/>
        <source>Version</source>
        <translation>པར་གཞིའི་མིང་།</translation>
    </message>
</context>
<context>
    <name>CpuDetailsWidget</name>
    <message>
        <location filename="../sysresource/cpudetailswidget.cpp" line="17"/>
        <source>Processor</source>
        <translation>སྒྲིག་གཅོད་ཆས།</translation>
    </message>
    <message>
        <location filename="../sysresource/cpudetailswidget.cpp" line="21"/>
        <source>CPU</source>
        <translation type="unfinished">སྒྲིག་གཅོད་ཆས་</translation>
    </message>
</context>
<context>
    <name>CpuHistoryWidget</name>
    <message>
        <location filename="../sysresource/cpuhistorywidget.cpp" line="64"/>
        <source>CPU</source>
        <translation>སྒྲིག་གཅོད་ཆས་</translation>
    </message>
</context>
<context>
    <name>CustomServiceNameDlg</name>
    <message>
        <location filename="../dialog/customservicenamedlg.cpp" line="18"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../dialog/customservicenamedlg.cpp" line="19"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>FileSystemInfoItem</name>
    <message>
        <location filename="../filesystem/filesysteminfoitem.cpp" line="96"/>
        <source>%1 Used</source>
        <translation>%1བཀོལ་སྤྱོད་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../filesystem/filesysteminfoitem.cpp" line="98"/>
        <source>(%1 Available/%2 Free)  Total %3  %4  Directory:%5</source>
        <translation>(%1 སྤྱོད་རུང་/%2 ཁོམ་ལོང་)  ཁྱོན་བསྡོམས་ %3 %4 དཀར་ཆག:%5</translation>
    </message>
</context>
<context>
    <name>FileSystemWidget</name>
    <message>
        <location filename="../filesystem/filesystemwidget.cpp" line="47"/>
        <source>No search result</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../filesystem/filesystemwidget.cpp" line="71"/>
        <source>Refresh</source>
        <translation>དྭངས་ཤིང་གཙང་བ།</translation>
    </message>
</context>
<context>
    <name>KChartView</name>
    <message>
        <location filename="../controls/kchartview.cpp" line="139"/>
        <source>60 seconds</source>
        <translation>སྐར་ཆ་དྲུག་ཅུ།</translation>
    </message>
</context>
<context>
    <name>KLeftWidget</name>
    <message>
        <location filename="../kleftwidget.cpp" line="190"/>
        <source>Kylin System Monitor</source>
        <translation>ཅིན་ལིན་མ་ལག་ལྟ་ཞིབ་ཚད་ལེན</translation>
    </message>
    <message>
        <location filename="../kleftwidget.cpp" line="224"/>
        <source>Processes</source>
        <translation>བཟོ་རྩལ།</translation>
    </message>
    <message>
        <location filename="../kleftwidget.cpp" line="225"/>
        <source>Services</source>
        <translation>ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../kleftwidget.cpp" line="226"/>
        <source>Disks</source>
        <translation>ཁབ་ལེན་གྱི་ཁབ་ལེན་འཁོར་ལོ།</translation>
    </message>
</context>
<context>
    <name>KRightWidget</name>
    <message>
        <source>menu</source>
        <translation type="vanished">ཟས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="76"/>
        <source>option</source>
        <translation>འདེམས་ཚན་</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="82"/>
        <source>minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>maximize/restore</source>
        <translation type="vanished">ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས་སླར་གསོ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="94"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="102"/>
        <location filename="../krightwidget.cpp" line="118"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="103"/>
        <location filename="../krightwidget.cpp" line="110"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="104"/>
        <location filename="../krightwidget.cpp" line="121"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="113"/>
        <location filename="../krightwidget.cpp" line="114"/>
        <source>Kylin System Monitor</source>
        <translation>ཅིན་ལིན་མ་ལག་ལྟ་ཞིབ་ཚད་ལེན</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="133"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="270"/>
        <location filename="../krightwidget.cpp" line="282"/>
        <source>restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="88"/>
        <location filename="../krightwidget.cpp" line="273"/>
        <location filename="../krightwidget.cpp" line="285"/>
        <source>maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="22"/>
        <source>Kylin System Monitor</source>
        <translation>ཅིན་ལིན་མ་ལག་ལྟ་ཞིབ་ཚད་ལེན</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="192"/>
        <source>Processes</source>
        <translation>བཟོ་རྩལ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="197"/>
        <source>Services</source>
        <translation>ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="201"/>
        <source>Disks</source>
        <translation>ཁབ་ལེན་གྱི་ཁབ་ལེན་འཁོར་ལོ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="204"/>
        <source>Cpu Details</source>
        <translation>Cpuཞིབ་ཕྲ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="207"/>
        <source>Mem Details</source>
        <translation>ནང་གསོག་གི་གནས་ཚུལ་ཞིབ་ཕྲ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="210"/>
        <source>Net Details</source>
        <translation>དྲ་རྒྱའི་གནས་ཚུལ་ཞིབ་ཕྲ།</translation>
    </message>
</context>
<context>
    <name>MemDetailsModel</name>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="44"/>
        <source>Used</source>
        <translation>བཀོལ་སྤྱོད་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="46"/>
        <source>Available</source>
        <translation>སྤྱད་ཆོག</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="50"/>
        <source>Shared</source>
        <translation>ནང་གསོག་མཉམ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="52"/>
        <source>Cached</source>
        <translation>ལྷོད་གསོག་ཤོད།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="56"/>
        <source>Buffers</source>
        <translation>གཞི་གྲངས་གསོག་འཇོག</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="58"/>
        <source>Cached swap</source>
        <translation>བརྗེ་རེས་གསོག་ཁུལ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="62"/>
        <source>Active</source>
        <translation type="unfinished">འཁྲུག་ཆ་དོད་པོ་</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="64"/>
        <source>Inactive</source>
        <translation>འཁྲུག་ཆ་མེད་པའི་ལྷོད་གཏོང་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="68"/>
        <source>Dirty</source>
        <translation>ཤོག་གྲངས།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="70"/>
        <source>Mapped</source>
        <translation>འཕྲོ་འོད་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="74"/>
        <source>Total swap</source>
        <translation>བརྗེ་རེས་བར་སྟོང་གི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="76"/>
        <source>Free swap</source>
        <translation>བརྗེ་ཆོག་པའི་བར་སྟོང་།</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailsmodel.h" line="80"/>
        <source>Slab</source>
        <translation>ནང་ཉིང་གཞི་གྲངས་སྒྲིག་གཞིའི་གསོག་འཇོག</translation>
    </message>
</context>
<context>
    <name>MemDetailsWidget</name>
    <message>
        <location filename="../sysresource/memdetailswidget.cpp" line="15"/>
        <location filename="../sysresource/memdetailswidget.cpp" line="20"/>
        <source>Memory</source>
        <translation type="unfinished">དྲན་ཤེས་</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailswidget.cpp" line="16"/>
        <source>Capacity size:(%1)</source>
        <translation>ཤོང་ཚད་ཆེ་ཆུང་།(བརྒྱ་ཆ1)</translation>
    </message>
    <message>
        <location filename="../sysresource/memdetailswidget.cpp" line="33"/>
        <source>Swap</source>
        <translation>བརྗེ་བའི་བར་སྟོང་།</translation>
    </message>
</context>
<context>
    <name>MemHistoryWidget</name>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="123"/>
        <source>memory</source>
        <translation>དྲན་ཤེས་</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="133"/>
        <source>swap</source>
        <translation>བརྗེ་རེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="138"/>
        <location filename="../sysresource/memhistorywidget.cpp" line="293"/>
        <source>Not enabled</source>
        <translation>ནུས་པ་ཐོན་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>NetDetailsModel</name>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="138"/>
        <location filename="../sysresource/netdetailsmodel.cpp" line="140"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="151"/>
        <location filename="../sysresource/netdetailsmodel.cpp" line="153"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="161"/>
        <source>Connection type</source>
        <translation>འབྲེལ་མཐུད་རིགས།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="168"/>
        <source>ESSID</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="173"/>
        <source>Link quality</source>
        <translation>བརྡ་རྟགས་སྤུས་ཚད།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="178"/>
        <source>Signal strength</source>
        <translation>བརྡ་རྟགས་ཡང་བ།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="183"/>
        <source>Noise level</source>
        <translation>སྒྲ།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="189"/>
        <source>MAC</source>
        <translation>MAC</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="194"/>
        <source>Bandwidth</source>
        <translation>འགྲོས་ཚད།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="199"/>
        <source>RX packets</source>
        <translation>བསྡུ་ལེན་ཁུག་མའི་གྲངས་འབོར།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="204"/>
        <source>RX bytes</source>
        <translation>བསྡོམས་རྩིས།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="209"/>
        <source>RX errors</source>
        <translation>བསྡུ་ལེན།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="214"/>
        <source>RX dropped</source>
        <translation>བསྡུ་ལེན།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="219"/>
        <source>RX overruns</source>
        <translation>FIFOདང་ལེན་བྱས།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="224"/>
        <source>RX frame</source>
        <translation>ཚོ་ཆུང་ལྡེབ་ནོར་བ།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="229"/>
        <source>TX packets</source>
        <translation>སྐུར་དངོས་གྲངས་འབོར།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="234"/>
        <source>TX bytes</source>
        <translation>བསྡོམས་པས་བསྐུར་ཟིན།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="239"/>
        <source>TX errors</source>
        <translation>སྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="244"/>
        <source>TX dropped</source>
        <translation>སྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="249"/>
        <source>TX overruns</source>
        <translation>FIFOབསྐུར་རོགས།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="254"/>
        <source>TX carrier</source>
        <translation>རླབས་ཐེག་ཟད་གྲོན།</translation>
    </message>
</context>
<context>
    <name>NetDetailsWidget</name>
    <message>
        <location filename="../sysresource/netdetailswidget.cpp" line="17"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
</context>
<context>
    <name>NetHistoryWidget</name>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="135"/>
        <source>Network History</source>
        <translation>དྲ་རྒྱའི་ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="144"/>
        <source>send</source>
        <translation>སྐུར་སྐྱེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="157"/>
        <source>receive</source>
        <translation>བསྡུ་ལེན་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>NetInfoDetailItemDelegate</name>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="43"/>
        <location filename="../sysresource/netdetailsmodel.cpp" line="45"/>
        <source>IP address:</source>
        <translation>IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="43"/>
        <source>Netmask:</source>
        <translation>དྲ་རྒྱའི་ཨང་གྲངས་བཀབ་ནས།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="43"/>
        <source>Broadcast:</source>
        <translation>ཀུན་ཁྱབ་རླུང་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="45"/>
        <source>Prefixlen:</source>
        <translation>ཨང་གྲངས་འགེབས་པའི་རིང་ཚད།</translation>
    </message>
    <message>
        <location filename="../sysresource/netdetailsmodel.cpp" line="45"/>
        <source>Scope:</source>
        <translation>ཁོངས།</translation>
    </message>
</context>
<context>
    <name>ProcPropertiesDlg</name>
    <message>
        <source>close</source>
        <translation type="vanished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="91"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>ProcessTableModel</name>
    <message>
        <location filename="../process/processtablemodel.cpp" line="214"/>
        <source>Process Name</source>
        <translation>བཟོ་རྩལ་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="218"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="222"/>
        <source>Disk</source>
        <translation>ཁབ་ལེན་གྱི་ཁབ་ལེན་</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="225"/>
        <source>CPU</source>
        <translation>སྒྲིག་གཅོད་ཆས་</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="228"/>
        <source>ID</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="231"/>
        <source>Flownet Persec</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="234"/>
        <source>Memory</source>
        <translation>དྲན་ཤེས་</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="237"/>
        <source>Priority</source>
        <translation>སྔོན་ཐོབ་ཀྱི་དོན་དག</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="271"/>
        <source>Suspend</source>
        <translation>གནས་སྐབས་མཚམས་འཇོག་</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="274"/>
        <source>No response</source>
        <translation>ལན་མི་འདེབས།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="277"/>
        <source>Uninterruptible</source>
        <translation>བར་ཆད་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>ProcessTableView</name>
    <message>
        <location filename="../process/processtableview.cpp" line="186"/>
        <source>No search result</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="273"/>
        <location filename="../process/processtableview.cpp" line="600"/>
        <location filename="../process/processtableview.cpp" line="603"/>
        <location filename="../process/processtableview.cpp" line="695"/>
        <source>End process</source>
        <translation>མཇུག་སྒྲིལ་བའི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="275"/>
        <location filename="../process/processtableview.cpp" line="640"/>
        <location filename="../process/processtableview.cpp" line="643"/>
        <location filename="../process/processtableview.cpp" line="725"/>
        <source>Kill process</source>
        <translation>གསོད་པའི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="289"/>
        <source>Very High</source>
        <translation>ཧ་ཅང་མཐོ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="290"/>
        <source>High</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="291"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན་གྱི་གནས་</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="292"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="293"/>
        <source>Very Low</source>
        <translation>ཧ་ཅང་དམའ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="294"/>
        <source>Custom</source>
        <translation>གོམས་སྲོལ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="299"/>
        <source>Change Priority</source>
        <translation>སྔོན་ལ་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="301"/>
        <source>Properties</source>
        <translation>ངོ་བོ་དང་གཟུགས་</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="319"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="325"/>
        <source>Disk</source>
        <translation>ཁབ་ལེན་གྱི་ཁབ་ལེན་</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="331"/>
        <source>CPU</source>
        <translation>སྒྲིག་གཅོད་ཆས་</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="337"/>
        <source>ID</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="343"/>
        <source>Flownet Persec</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="349"/>
        <source>Memory</source>
        <translation>དྲན་ཤེས་</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="355"/>
        <source>Priority</source>
        <translation>སྔོན་ཐོབ་ཀྱི་དོན་དག</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="546"/>
        <location filename="../process/processtableview.cpp" line="550"/>
        <source>Stop process</source>
        <translation>བཀག་འགོག་བྱེད་པའི་གོ་རིམ</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="691"/>
        <source>End the selected process %1 (PID %2) ?</source>
        <translation>%1 (PID %2) ཡིས་བདམས་པའི་གོ་རིམ་མཇུག་བསྒྲིལ་དགོས་སམ། ?</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="721"/>
        <source>Kill the selected process %1 (PID %2) ?</source>
        <translation>བདམས་ཟིན་པའི་གོ་རིམ་ %1 (PID %2) བསད་པ་ཡིན་ནམ། ?</translation>
    </message>
    <message>
        <source>End the selected process &quot;%1&quot;(PID:%2)?</source>
        <translation type="vanished">&quot;%1&quot;(PID:%2)ཡིས་བདམས་པའི་གོ་རིམ་མཇུག་བསྒྲིལ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="692"/>
        <source>Ending a process may destroy data, break the session or introduce a security risk. Only unresponsive processes should be ended.
Are you sure to continue?</source>
        <translation>བརྒྱུད་རིམ་ཞིག་མཇུག་བསྒྲིལ་ན་གཞི་གྲངས་ལ་གཏོར་བརླག་དང་། ཚོགས་འདུ་གཏོར་བརླག་གཏོང་བའམ་ཡང་ན་བདེ་འཇགས་ཀྱི་ཉེན་ཁ་ འགན་མི་འཁུར་བའི་གོ་རིམ་མ་གཏོགས་མཇུག་བསྒྲིལ་མི་རུང་།
ཁྱོད་ཀྱིས་ད་དུང་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="696"/>
        <location filename="../process/processtableview.cpp" line="726"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Kill the selected process &quot;%1&quot;(PID:%2)?</source>
        <translation type="vanished">བདམས་ཟིན་པའི་གོ་རིམ་&quot;%1&quot;(PID:%2)བསད་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="722"/>
        <source>Killing a process may destroy data, break the session or introduce a security risk. Only unresponsive processes should be killed.
Are you sure to continue?</source>
        <translation>བརྒྱུད་རིམ་ཞིག་བསད་ན་གཞི་གྲངས་མེད་པར་བཟོ་བ་དང་། ཚོགས་འདུ་གཏོར་བརླག་གཏོང་བའམ་ཡང་ན་བདེ་འཇགས་ཀྱི་ཉེན་ཁ་ འགན་མི་འཁུར་བའི་གོ་རིམ་ཁོ་ན་གསོད་དགོས།
ཁྱོད་ཀྱིས་ད་དུང་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="738"/>
        <source>Not allowed operation!</source>
        <translation>བཀོལ་སྤྱོད་བྱས་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../process/processtableview.cpp" line="739"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>ProcessWidget</name>
    <message>
        <source>Active Processes</source>
        <translation type="vanished">འཁྲུག་ཆ་དོད་པའི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processwidget.cpp" line="78"/>
        <source>Applications</source>
        <translation>རེ་འདུན་ཞུ་ཡིག</translation>
    </message>
    <message>
        <location filename="../process/processwidget.cpp" line="79"/>
        <source>My Processes</source>
        <translation>ངའི་བརྒྱུད་རིམ།</translation>
    </message>
    <message>
        <location filename="../process/processwidget.cpp" line="80"/>
        <source>All Process</source>
        <translation>བརྒྱུད་རིམ་ཡོད་ཚད།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="101"/>
        <source>User name:</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="101"/>
        <source>Process name:</source>
        <translation>བཟོ་རྩལ་གྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="101"/>
        <source>Command line:</source>
        <translation>བཀོད་འདོམས་ལམ་ཐིག་ནི།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="101"/>
        <source>Started Time:</source>
        <translation>འགོ་ཚུགས་པའི་དུས་ཚོད་ནི།</translation>
    </message>
    <message>
        <location filename="../dialog/procpropertiesdlg.cpp" line="101"/>
        <source>CPU Time:</source>
        <translation>སྒྲིག་གཅོད་ཆས་ དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="121"/>
        <source>ukui-system-monitor is already running!</source>
        <translation>རྒྱུད་ཁོངས་སོ་ལྟ་ཆས།འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="57"/>
        <location filename="../sysresource/memhistorywidget.cpp" line="39"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="43"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="270"/>
        <location filename="../process/processtablemodel.cpp" line="358"/>
        <location filename="../util.cpp" line="397"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="273"/>
        <location filename="../process/processtablemodel.cpp" line="361"/>
        <location filename="../util.cpp" line="401"/>
        <source>Zombie</source>
        <translation>རོ་མ་པི།</translation>
    </message>
    <message>
        <location filename="../process/processtablemodel.cpp" line="276"/>
        <location filename="../process/processtablemodel.cpp" line="364"/>
        <location filename="../util.cpp" line="405"/>
        <source>Uninterruptible</source>
        <translation>བར་ཆད་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="35"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="36"/>
        <source>KiB</source>
        <translation>ཁི་པི་ཡ།</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="43"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="50"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location filename="../sysresource/memhistorywidget.cpp" line="47"/>
        <location filename="../sysresource/nethistorywidget.cpp" line="57"/>
        <source>TiB</source>
        <translation>TiB</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="38"/>
        <source>KiB/s</source>
        <translation>KiB/s</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="45"/>
        <source>MiB/s</source>
        <translation>MiB/s</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="52"/>
        <source>GiB/s</source>
        <translation>GiB/s</translation>
    </message>
    <message>
        <location filename="../sysresource/nethistorywidget.cpp" line="59"/>
        <source>TiB/s</source>
        <translation>TiB/s</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="393"/>
        <source>Running</source>
        <translation>འཁོར་སྐྱོད་བྱེད་བཞིན་</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="409"/>
        <source>Sleeping</source>
        <translation>གཉིད་དུ་ཡུར་བ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="419"/>
        <source>Very High</source>
        <translation>ཧ་ཅང་མཐོ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="421"/>
        <source>High</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="423"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན་གྱི་གནས་</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="425"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="427"/>
        <source>Very Low</source>
        <translation>ཧ་ཅང་དམའ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="433"/>
        <source>Very High Priority</source>
        <translation>ཧ་ཅང་དམིགས་སུ་བཀར་ནས་སྔོན་ལ</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="435"/>
        <source>High Priority</source>
        <translation>ཚད་མཐོའི་མཐོང་ཆེན་</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="437"/>
        <source>Normal Priority</source>
        <translation>རྒྱུན་ལྡན་གྱི་སྔོན་ཐོབ་དབང་ཆ།</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="439"/>
        <source>Low Priority</source>
        <translation>དམིགས་སུ་བཀར་ནས་སྔོན་ལ་</translation>
    </message>
    <message>
        <location filename="../util.cpp" line="441"/>
        <source>Very Low Priority</source>
        <translation>ཧ་ཅང་དམའ་བའི་སྔོན་ཐོབ་ཀྱི་དོན</translation>
    </message>
</context>
<context>
    <name>ReniceDialog</name>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="68"/>
        <location filename="../dialog/renicedialog.cpp" line="302"/>
        <source>Change Priority of Process %1 (PID: %2)</source>
        <translation>བཟོ་རྩལ་གྱི་སྔོན་ཐོབ་དབང་ཆ་བསྒྱུར་བཅོས་བྱ་དགོས། %1 (PID: %2)</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="86"/>
        <location filename="../dialog/renicedialog.cpp" line="332"/>
        <source>Nice value:</source>
        <translation>རིན་ཐང་ཧ་ཅང་ཡག་པོ་</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="105"/>
        <source>Note:</source>
        <translation>མཆན་འགྲེལ།</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="109"/>
        <source>The priority of a process is given by its nice value. A lower nice value corresponds to a higher priority.</source>
        <translation>བརྒྱུད་རིམ་ཞིག་གི་སྔོན་ཐོབ་དབང་ཆ་ནི་དེའི་རིན་ཐང་བཟང་པོ་ཡིན། ཅུང་དམའ་བའི་རིན་ཐང་དེ་སྔར་ལས་མཐོ་བའི་སྔོན་ཐོབ་ཀྱི་དོན་དག་དང་མཐུན་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="121"/>
        <location filename="../dialog/renicedialog.cpp" line="312"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../dialog/renicedialog.cpp" line="126"/>
        <location filename="../dialog/renicedialog.cpp" line="322"/>
        <source>Change Priority</source>
        <translation>སྔོན་ལ་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ServiceManager</name>
    <message>
        <location filename="../service/servicemanager.cpp" line="331"/>
        <source>Failed to set service startup type</source>
        <translation>ཞབས་ཞུའི་ལས་འགོ་འཛུགས་པའི་རིགས་དབྱིབས་གཏན་འཁེལ་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicemanager.cpp" line="373"/>
        <source>Error: Failed to set service startup type due to the crashed sub process.</source>
        <translation>ནོར་འཁྲུལ། ཐོར་ཞིག་ཏུ་སོང་བའི་ཡན་ལག་བརྒྱུད་རིམ་གྱི་རྐྱེན་གྱིས་ཞབས་ཞུའི་ལས་རིགས་ཀྱི་གསར་གཏོད་རིགས་གཏན་འཁེལ་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>ServiceTableModel</name>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>stub</source>
        <translation>སི་ཐུའུ་ལུའུ་པུའུ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>loaded</source>
        <translation>དངོས་ཟོག་བླུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>not-found</source>
        <translation>མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>bad-setting</source>
        <translation>བཀོད་སྒྲིག་ཡག་པོ་མ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>error</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <source>merged</source>
        <translation>ཟླ་སྒྲིལ་བཏང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="40"/>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>masked</source>
        <translation>ངོ་འབག་གྱོན་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>active</source>
        <translation>འཁྲུག་ཆ་དོད་པོ་</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>reloading</source>
        <translation>ཡང་བསྐྱར་དངོས་ཟོག་བླུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>inactive</source>
        <translation>འགུལ་སྐྱོད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>failed</source>
        <translation>ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>activating</source>
        <translation>སྐུལ་སློང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="41"/>
        <source>deactivating</source>
        <translation>འགུལ་སྐྱོད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>dead</source>
        <translation>ཤི་སོང་།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>start-pre</source>
        <translation>འགོ་རྩོམ་པའི་སྔོན་འགྲོའི་ཆ་རྐྱེན</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>start</source>
        <translation>འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>start-post</source>
        <translation>འགོ་ཚུགས་པའི་ལས་གནས།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>running</source>
        <translation>འཁོར་སྐྱོད་བྱེད་བཞིན་</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>exited</source>
        <translation>ཕྱིར་འཐེན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>reload</source>
        <translation>བསྐྱར་དུ་ཕབ་ལེན་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>stop</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <source>stop-watchdog</source>
        <translation>ལྟ་ཞིབ་བྱེད་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="42"/>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>stop-sigterm</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>stop-sigkill</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>stop-post</source>
        <translation>ལས་གནས་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>final-sigterm</source>
        <translation>མཐའ་མའི་འགྲན་བསྡུར་གྱི་འགྲན་བསྡུར།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>final-sigkill</source>
        <translation>མཐའ་མའི་འགྲན་བསྡུར་གྱི་རྒྱལ་ཕམ་ཐག་གཅོད་ཀྱི་འགྲན་</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="43"/>
        <source>auto-restart</source>
        <translation>རང་འགུལ་གྱིས་བསྐྱར་དུ་འགོ་ཚུགས་</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>enabled</source>
        <translation>ནུས་པ་ཐོན་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>disabled</source>
        <translation>དབང་པོ་སྐྱོན་ཅན།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>static</source>
        <translation>ལྷིང་འཇགས་སུ་གནས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>transient</source>
        <translation>གནས་སྐབས་རིང་།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>indirect</source>
        <translation>གཞན་བརྒྱུད་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>enabled-runtime</source>
        <translation>འཁོར་སྐྱོད་བྱེད་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="44"/>
        <source>generated</source>
        <translation>ཐོན་སྐྱེད་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="45"/>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="45"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="45"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="152"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="154"/>
        <source>Load</source>
        <translation>ཐེག་ཚད།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="156"/>
        <source>Active</source>
        <translation>འཁྲུག་ཆ་དོད་པོ་</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="158"/>
        <source>Sub</source>
        <translation>ཡན་ལག་རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="160"/>
        <source>State</source>
        <translation>རྒྱལ་ཁབ།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="162"/>
        <source>Description</source>
        <translation>གསལ་བཤད།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="164"/>
        <source>PID</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../service/servicetablemodel.cpp" line="166"/>
        <source>StartupMode</source>
        <translation>གསར་གཏོད་མ་ལག</translation>
    </message>
</context>
<context>
    <name>ServiceTableView</name>
    <message>
        <location filename="../service/servicetableview.cpp" line="103"/>
        <source>No search result</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="128"/>
        <source>Start</source>
        <translation>འགོ་རྩོམ་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="129"/>
        <source>Stop</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="130"/>
        <source>Restart</source>
        <translation>ཡང་བསྐྱར་འགོ་འཛུགས་</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="131"/>
        <source>Startup type</source>
        <translation>འགོ་ཚུགས་པའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="132"/>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="133"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="134"/>
        <source>Refresh</source>
        <translation>དྭངས་ཤིང་གཙང་བ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="139"/>
        <source>Load</source>
        <translation>ཐེག་ཚད།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="141"/>
        <source>Active</source>
        <translation>འཁྲུག་ཆ་དོད་པོ་</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="143"/>
        <source>Sub</source>
        <translation>ཡན་ལག་རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="145"/>
        <source>State</source>
        <translation>རྒྱལ་ཁབ།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="147"/>
        <source>Startup Type</source>
        <translation>འགོ་ཚུགས་པའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="149"/>
        <source>Description</source>
        <translation>གསལ་བཤད།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="151"/>
        <source>PID</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="308"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../service/servicetableview.cpp" line="413"/>
        <location filename="../service/servicetableview.cpp" line="470"/>
        <location filename="../service/servicetableview.cpp" line="525"/>
        <source>Service instance name</source>
        <translation>ཞབས་ཞུའི་དངོས་པོའི་མིང་།</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../trayicon.cpp" line="38"/>
        <source>Exit</source>
        <translation>ཕྱིར་འབུད་</translation>
    </message>
    <message>
        <location filename="../trayicon.cpp" line="40"/>
        <source>Open monitor</source>
        <translation>རྒྱུད་ཁོངས་ལྟ་ཞིབ་ཆས་ཁ་འབྱེད་</translation>
    </message>
    <message>
        <location filename="../trayicon.cpp" line="93"/>
        <source>NET: </source>
        <translation>དྲ་རྒྱ།：</translation>
    </message>
    <message>
        <location filename="../trayicon.cpp" line="94"/>
        <source>CPU: </source>
        <translation>CPU：</translation>
    </message>
    <message>
        <location filename="../trayicon.cpp" line="95"/>
        <source>RAM: </source>
        <translation>ནང་གསོག：</translation>
    </message>
    <message>
        <location filename="../trayicon.cpp" line="111"/>
        <source>Disk: </source>
        <translation>སྡུད་སྡེར་：</translation>
    </message>
</context>
<context>
    <name>USMAboutDialog</name>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="127"/>
        <location filename="../dialog/usmaboutdialog.cpp" line="153"/>
        <source>Kylin System Monitor</source>
        <translation>ཅིན་ལིན་མ་ལག་ལྟ་ཞིབ་ཚད་ལེན</translation>
    </message>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="158"/>
        <source>version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="163"/>
        <source>System monitor is a desktop application that face desktop users of Kylin operating system,It meets the needs of users to monitor the system process, system resources and file system</source>
        <translation>མ་ལག་ལྟ་ཞིབ་ཚད་ལེན་ནི་ཅིན་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་གི་ཅོག་ཙེའི་སྟེང་གི་སྤྱོད་མཁན་ལ་ཁ་གཏད་པའི་མདུན་ངོས་ཀྱི་ཉེར་སྤྱོད་གོ་རིམ་ཞིག་ཡིན་པས། དེས་སྤྱོད་མཁན་གྱིས་མ་ལག་གི་གོ་རིམ་དང་། མ་ལག་གི་ཐོན་ཁུངས། ཡིག་ཚགས་</translation>
    </message>
    <message>
        <location filename="../dialog/usmaboutdialog.cpp" line="175"/>
        <source>Service and support team:</source>
        <translation>ཞབས་ཞུ་དང་རོགས་སྐྱོར་ཚོགས་པ་གཤམ་གསལ།</translation>
    </message>
</context>
</TS>
