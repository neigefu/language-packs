<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>web-accesslog messages grouped.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="13"/>
        <source>web-accesslog messages grouped.</source>
        <translation>Weeb-acessslogཡི་གནས་ཚུལ་ཚོགས་ཆུང་བགོས།</translation>
    </message>
</context>
<context>
    <name>Ignored URLs (simple queries).</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="44"/>
        <source>Ignored URLs (simple queries).</source>
        <translation>སྣང་མེད་དུ་བཞག་པའི་URL(སྟབས་བདེའི་འདྲི་རྩད)།</translation>
    </message>
</context>
<context>
    <name>SQL injection attempt.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="53"/>
        <source>SQL injection attempt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Multiple SQL injection attempts from same source ip.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="60"/>
        <source>Multiple SQL injection attempts from same source ip.</source>
        <translation>འབྱུང་ཁུངས་གཅིག་འདྲ་ནས་IPཡིས་SQLལ་ཚོད་ལྟ་ཐེངས་མང་བྱས།</translation>
    </message>
</context>
<context>
    <name>SQL injection attempt.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="69"/>
        <source>SQL injection attempt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SQL injection attempt.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="76"/>
        <source>SQL injection attempt.</source>
        <translation>SQLལ་ཚོད་ལྟ་བྱས་ནས་ཚོད་ལྟ་བྱས།</translation>
    </message>
</context>
<context>
    <name>Grouping of the pam_unix rules.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="13"/>
        <source>Grouping of the pam_unix rules.</source>
        <translation>pam_unix སྒྲིག་སྲོལ་གྱི་ཚོ་ཆུང་བགོས་པ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>PAM: Login session opened.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="19"/>
        <source>PAM: Login session opened.</source>
        <translation>PAM:ཚོགས་འདུའི་ཐོག་བཀོད་པའི་སྐད་ཆའི་ཁ་ཕྱེ་ཟིན།</translation>
    </message>
</context>
<context>
    <name>PAM: Login session closed.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="26"/>
        <source>PAM: Login session closed.</source>
        <translation>PAM:ཚོགས་འདུའི་ཐོག་སྐད་ཆ་ཐོ་འགོད་བྱས་ཟིན།</translation>
    </message>
</context>
<context>
    <name>PAM: User login failed.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="35"/>
        <source>PAM: User login failed.</source>
        <translation>PAM:བཀོལ་སྤྱོད་བྱེད་མཁན་གྱི་ཐོ་འགོད་ཕམ་ཁ་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>PAM: Attempt to login with an invalid user.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="41"/>
        <source>PAM: Attempt to login with an invalid user.</source>
        <translation>PAMཡིས་གོ་མི་ཆོད་པའི་སྤྱོད་མཁན་ཐོ་འགོད་བྱེད་པར་ཚོད་ལྟ་བྱས།</translation>
    </message>
</context>
<context>
    <name>PAM: Ignoring Annoying Ubuntu/debian cron login events.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="49"/>
        <source>PAM: Ignoring Annoying Ubuntu/debian cron login events.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PAM: Ignoring Annoying Ubuntu/debian cron login events.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="56"/>
        <source>PAM: Ignoring Annoying Ubuntu/debian cron login events.</source>
        <translation>PAMཡིས་མི་ལ་སུན་སྣང་སྐྱེ་བའི་དོན་རྐྱེན་སྣང་མེད་དུ་བཞག་པ།</translation>
    </message>
</context>
<context>
    <name>PAM: Ignoring events with a user or a password.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="62"/>
        <source>PAM: Ignoring events with a user or a password.</source>
        <translation>PAMཡིས་སྤྱོད་མཁན་ནམ་གསང་གྲངས་ཡོད་པའི་དོན་རྐྱེན་སྣང་མེད་དུ་བཞག་པ།</translation>
    </message>
</context>
<context>
    <name>PAM: Multiple failed logins in a small period of time.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="68"/>
        <source>PAM: Multiple failed logins in a small period of time.</source>
        <translation>PAMཡིས་དུས་ཚོད་ཐུང་ངུའི་ནང་ཐེངས་མང་ཐོ་འགོད་བྱས་ནས་ཕམ་ཁ་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>PAM and gdm are not playing nicely.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="76"/>
        <source>PAM and gdm are not playing nicely.</source>
        <translation>PAMདང་gdmགྱི་འཁོར་སྐྱོད་རྣམ་པ་མི་བཟང་།</translation>
    </message>
</context>
<context>
    <name>PAM misconfiguration.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="82"/>
        <source>PAM misconfiguration.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PAM misconfiguration.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="88"/>
        <source>PAM misconfiguration.</source>
        <translation>PAMལ་ནོར་འཁྲུལ་བཀོད་སྒྲིག་བྱས་</translation>
    </message>
</context>
<context>
    <name>PAM: User changed password.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="93"/>
        <source>PAM: User changed password.</source>
        <translation>PAM:བེད་སྤྱོད་བྱེད་མཁན་གྱིས་གསང་གྲངས་བསྒྱུར།</translation>
    </message>
</context>
<context>
    <name>unix_chkpwd grouping.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="100"/>
        <source>unix_chkpwd grouping.</source>
        <translation>unix_chkpwd་ཚོ་ཆུང་བགོས་ཆོག</translation>
    </message>
</context>
<context>
    <name>unix_chkpwd: Password check failed.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="106"/>
        <source>unix_chkpwd: Password check failed.</source>
        <translation>unix_chkpwd།གསང་བའི་ཞིབ་བཤེར་ཕམ་ཁ་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>Possible exploit attempt.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="7"/>
        <source>Possible exploit attempt.</source>
        <translation>ཕལ་ཆེར་སྐྱོན་ཆ་བེད་སྤྱོད་བྱེད་པར་ཚོད་ལྟ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>Multiple authentication failures in very short time!</name>
    <message>
        <location filename="../po/9999_custom.xml" line="18"/>
        <source>Multiple authentication failures in very short time!</source>
        <translation>དུས་ཚོད་ཧ་ཅང་ཐུང་ངུ་ཞིག་གི་ནང་དུ་ཐོབ་ཐང་ཚོད་ལྟསར་སྤྲོད་ཐེངས་མང་བྱས་</translation>
    </message>
</context>
<context>
    <name>pam: locked account.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="31"/>
        <source>pam: locked account.</source>
        <translation>PAMཡིས་རྩིས་ཐོ་གཏན་ཁེལ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>Root session opened.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="41"/>
        <source>Root session opened.</source>
        <translation>rootཡི་སྐད་ཆ་ཁ་ཕྱེ་ཟིན།</translation>
    </message>
</context>
<context>
    <name>NetworkManager starting.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="51"/>
        <source>NetworkManager starting.</source>
        <translation>Neetomangrereerreareerearreaerrear</translation>
    </message>
</context>
<context>
    <name>High-risk root session opened.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="58"/>
        <source>High-risk root session opened.</source>
        <translation>ཉེན་ཁ་ཆེ་བའི་སྐད་ཆ་དེ་ཁ་ཕྱེས་ཟིན།</translation>
    </message>
</context>
<context>
    <name>Dbus privilege escalation.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="69"/>
        <source>Dbus privilege escalation.</source>
        <translation>Dbusཁྲིམས་ཚད་མཐོ་རུ་གཏོང་དགོས།</translation>
    </message>
</context>
<context>
    <name>Catchall rule for unknown types.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="5"/>
        <source>Catchall rule for unknown types.</source>
        <translation>ཤེས་མེད་པའི་རིགས་དབྱིབས་ཀྱི་Catcallཁྲིམས་སྲོལ།</translation>
    </message>
</context>
<context>
    <name>Catchall rule for control messages.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="10"/>
        <source>Catchall rule for control messages.</source>
        <translation>གནས་ཚུལ་ཚོད་འཛིན་བྱེད་པའི་Catalལམ་སྲོལ།</translation>
    </message>
</context>
<context>
    <name>Catchall rule for non control messages.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="15"/>
        <source>Catchall rule for non control messages.</source>
        <translation>ཚོད་འཛིན་མ་བྱས་པའི་གནས་ཚུལ་གྱི་Catalལམ་སྲོལ།</translation>
    </message>
</context>
<context>
    <name>Catchall rule for possible erroneous logs.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="20"/>
        <source>Catchall rule for possible erroneous logs.</source>
        <translation>འབྱུང་སྲིད་པའི་ནོར་འཁྲུལ་གྱི་ཉིན་ཐོ་ལ་དམིགས་ནས།Catcallལ་དམིགས་ནས།</translation>
    </message>
</context>
<context>
    <name>Catchall rule for possible attack/intrusion logs.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="25"/>
        <source>Catchall rule for possible attack/intrusion logs.</source>
        <translation>བཙན་འཛུལ་བྱེད་སྲིད་པའི་ཉིན་ཐོ་ལ་ཚུར་རྒོལ་བྱེད་སྲིད་པའི་Catcaallཁྲིམས་སྲོལ་ལ་དམིགས་ནས།</translation>
    </message>
</context>
<context>
    <name>Audit: messages grouped.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="14"/>
        <source>Audit: messages grouped.</source>
        <translation>གནས་ཚུལ་ཁག་བགོས་ནས་ཞིབ་བཤེར་གཏན་འབེབས་བྱ་</translation>
    </message>
</context>
<context>
    <name>Auditd: Start / Resume</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="26"/>
        <source>Auditd: Start / Resume</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱེད་འགོ་ཚུགས་པ།སླར་གསོ་བྱེད་འགོ</translation>
    </message>
</context>
<context>
    <name>Auditd: Start / Resume FAILED</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="34"/>
        <source>Auditd: Start / Resume FAILED</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱེད་འགོ་ཚུགས་ནས་ཕམ་ཁ་སླར་གསོ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Auditd: End</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="46"/>
        <source>Auditd: End</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་པའི་ཚེས་གྲངས་ནི་མཇུག་བསྒྲ</translation>
    </message>
</context>
<context>
    <name>Auditd: Abort</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="56"/>
        <source>Auditd: Abort</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱེད་མཚམས་འཇོག་དགོས།</translation>
    </message>
</context>
<context>
    <name>Auditd: Configuration changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="67"/>
        <source>Auditd: Configuration changed</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་ བཀོད་སྒྲིག་བྱས་ཟིན་པ་</translation>
    </message>
</context>
<context>
    <name>Auditd: device enables promiscuous mode</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="82"/>
        <source>Auditd: device enables promiscuous mode</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱེད་པར་སྒྲིག་ཆས་ཀྱིས་འདྲེས་མའི་དཔེ་དབྱིབས་སྤྱོད་དགོས།</translation>
    </message>
</context>
<context>
    <name>Auditd: process ended abnormally</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="92"/>
        <source>Auditd: process ended abnormally</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱེད་པའི་བརྒྱུད་རིམ་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
</context>
<context>
    <name>Auditd: execution of a file ended abnormally</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="102"/>
        <source>Auditd: execution of a file ended abnormally</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པའི་ཡིག་ཆ་ལག་བསྟར་བྱ་རྒྱུ་རྒྱུན་ལྡན་མིན་</translation>
    </message>
</context>
<context>
    <name>Auditd: file is made executable</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="112"/>
        <source>Auditd: file is made executable</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་དོན། ཡིག་ཆ་ལག་བསྟར་བྱས་ཆོག</translation>
    </message>
</context>
<context>
    <name>Auditd: file or a directory access ended abnormally</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="122"/>
        <source>Auditd: file or a directory access ended abnormally</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་པའི་ཡིག་ཆ་འམ་དཀར་ཆག་ལ་བཅར་འདྲི་བྱེད་པ་རྒྱུན་ལྡན་མིན་པའི་མཇུག</translation>
    </message>
</context>
<context>
    <name>Auditd: failure of the Abstract Machine Test Utility (AMTU) detected</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="132"/>
        <source>Auditd: failure of the Abstract Machine Test Utility (AMTU) detected</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ། སྤྱི་མཚན་དུ་གྱུར་པའི་འཕྲུལ་འཁོར་ཚད་ལེན་ཚོད་ལྟ་དངོས་སྤྱོད་གོ་རིམ་(AMTUUU)ཕམ་ཁ་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>Auditd: maximum amount of Discretionary Access Control (DAC) or Mandatory Access Control (MAC) failures reached</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="143"/>
        <source>Auditd: maximum amount of Discretionary Access Control (DAC) or Mandatory Access Control (MAC) failures reached</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ། གང་འདོད་ལྟར་འཚམས་འདྲི་ཚོད་འཛིན་(DAC)དང་ཡང་ན་བཙན་ཤེད་ཀྱིས་འཚམས་འདྲི་བྱེད་པར་ཚོད་འཛིན་བྱེད་པའི་(MAC)</translation>
    </message>
</context>
<context>
    <name>Auditd: Role-Based Access Control (RBAC) failure detected.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="154"/>
        <source>Auditd: Role-Based Access Control (RBAC) failure detected.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། མི་སྣའི་འཚམས་འདྲིའི་ཚོད་འཛིན་(RBAC)ཕམ་ཁ་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>Auditd: user-space account addition ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="164"/>
        <source>Auditd: user-space account addition ended abnormally.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། སྤྱོད་མཁན་གྱི་བར་སྟོང་གི་རྩིས་ཐོའི་ནང་རྒྱུན་ལྡན་མིན་པའི་མཇུག་འགྲིལ</translation>
    </message>
</context>
<context>
    <name>Auditd: user-space account deletion ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="174"/>
        <source>Auditd: user-space account deletion ended abnormally.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། སྤྱོད་མཁན་གྱི་བར་སྟོང་ཐོ་ཁོངས་སུབ་པ་རྒྱུན་ལྡན་མིན་པའི་མཇུག་འགྲིལ</translation>
    </message>
</context>
<context>
    <name>Auditd: user-space account modification ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="184"/>
        <source>Auditd: user-space account modification ended abnormally.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། སྤྱོད་མཁན་གྱི་བར་སྟོང་རྩིས་ཐོའི་ཨང་གྲངས་བཟོ་བཅོས་བརྒྱབ་ནས་མཇུག་འག</translation>
    </message>
</context>
<context>
    <name>Auditd: user becomes root</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="194"/>
        <source>Auditd: user becomes root</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་སྟེ། སྤྱོད་མཁན་སྤྱོད་མཁན་དེ་སྤྱོད་མཁན་གྱིས་སྤྱོད་མཁན་དུ་འགྱུར་བར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Auditd: account login attempt ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="204"/>
        <source>Auditd: account login attempt ended abnormally.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། རྩིས་ཐོའི་ཐོ་འགོད་ཚོད་ལྟ་རྒྱུན་ལྡན་མིན་པའི་མཇུག་འགྲིལ</translation>
    </message>
</context>
<context>
    <name>Auditd: limit of failed login attempts reached.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="214"/>
        <source>Auditd: limit of failed login attempts reached.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་</translation>
    </message>
</context>
<context>
    <name>Auditd: login attempt from a forbidden location.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="224"/>
        <source>Auditd: login attempt from a forbidden location.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། རྒྱུན་སྡོད་ཀྱི་སྡོད་གནས་ནས་ཚོད་ལྟ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Auditd: login attempt reached the maximum amount of concurrent sessions.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="234"/>
        <source>Auditd: login attempt reached the maximum amount of concurrent sessions.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་</translation>
    </message>
</context>
<context>
    <name>Auditd: login attempt is made at a time when it is prevented by.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="244"/>
        <source>Auditd: login attempt is made at a time when it is prevented by.</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། ཐོ་འགོད་ཚོད་ལྟ་ནི་བཀག་འགོག་བྱེད་པའི་སྐབས་སུ་སྤེལ་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>Auditd: SELinux permission check</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="256"/>
        <source>Auditd: SELinux permission check</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས། SELinnxཡི་དབང་ཚད་ལ་ཞིབ་བཤེར་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>Auditd: SELinux mode (enforcing, permissive, off) is changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="266"/>
        <source>Auditd: SELinux mode (enforcing, permissive, off) is changed</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ། SLinnxཡི་མ་དཔེ་(1awnngsesessee)ལ་བཟོ་བཅོས་བརྒྱབ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>Auditd: SELinux error</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="277"/>
        <source>Auditd: SELinux error</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས། SELinnxཡི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>Auditd: replay attack detected</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="289"/>
        <source>Auditd: replay attack detected</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་ནས་བསྐྱར་དུ་ཚུར་རྒོལ་བྱེད་པར་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་ཡོད།</translation>
    </message>
</context>
<context>
    <name>Auditd: group ID changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="299"/>
        <source>Auditd: group ID changed</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ། IDལ་བཟོ་བཅོས་བརྒྱབ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>Auditd: user ID changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="309"/>
        <source>Auditd: user ID changed</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས།སྤྱོད་མཁན་གྱི་IDལ་བཟོ་བཅོས་བརྒྱབ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Write access</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="319"/>
        <source>Audit: Watch - Write access</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་དང་། ལྟ་ཞིབ་དང་འཚམས་འདྲིའི་དབང་ཚད་འབྲི་དགོས།</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Write access: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="326"/>
        <source>Audit: Watch - Write access: $(audit.file.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་སྟེ། ལྟ་ཞིབ་བྱ་རྒྱུའི་དབང་མཚམས་ནི་$(audit.file.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Write access: $(audit.directory.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="333"/>
        <source>Audit: Watch - Write access: $(audit.directory.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་སྟེ། ལྟ་ཞིབ་བྱེད་སྐབས་དབང་མཚམས་སུ་འགོད་དགོས་པ་སྟེ། $(audit.directory.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Read access</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="340"/>
        <source>Audit: Watch - Read access</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Read access: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="347"/>
        <source>Audit: Watch - Read access: $(audit.file.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་སྟེ། --བཅར་འདྲིའི་དབང་ཚད་ལ་བལྟས་ན། $(audit.file.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Read access: $(audit.directory.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="354"/>
        <source>Audit: Watch - Read access: $(audit.directory.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས audit.directory.name $་</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Change attribute</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="361"/>
        <source>Audit: Watch - Change attribute</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་དང་། ལྟ་ཞིབ་བྱས་ནས་ངོ་བོ་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Change attribute: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="368"/>
        <source>Audit: Watch - Change attribute: $(audit.file.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་དང་། ལྟ་ཞིབ་བྱས་ནས་ངོ་བོ་བསྒྱུར་བ་སྟེ། $(audit.file.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Change attribute: $(audit.directory.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="375"/>
        <source>Audit: Watch - Change attribute: $(audit.directory.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་དང་། ལྟ་ཞིབ་བྱས་ནས་ངོ་བོ་བསྒྱུར་བཅོས་བྱེད་$།(audit.directory.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Execute access: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="382"/>
        <source>Audit: Watch - Execute access: $(audit.file.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུ་སྟེ། ལྟ་ཞིབ་བྱེད་པ་དང་། འཚམས་འདྲིའི་དབང་མཚམས་ལག་བསྟར་བྱ་རྒྱུ་སྟེ། $(audit.file.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Created: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="389"/>
        <source>Audit: Created: $(audit.file.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ།གསར་འཛུགས་བྱས་ཟིན་པ$(audit.file.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Deleted: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="396"/>
        <source>Audit: Deleted: $(audit.file.name)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་ཟིན་པ་སྟེ། $(audit.file.name)</translation>
    </message>
</context>
<context>
    <name>Audit: Command: $(audit.exe)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="407"/>
        <source>Audit: Command: $(audit.exe)</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱ་རྒྱུའི་བཀའ་རྒྱ། $(audit.exe)</translation>
    </message>
</context>
<context>
    <name>Audit: $(audit.type).</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="414"/>
        <source>Audit: $(audit.type).</source>
        <translation>ཞིབ་བཤེར་གཏན་འབེབས་བྱས་པ$(audite)།</translation>
    </message>
</context>
<context>
    <name>System user successfully logged to the system.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="19"/>
        <source>System user successfully logged to the system.</source>
        <translation>མ་ལག་སྤྱོད་མཁན་གྱིས་མ་ལག་ལེགས་འགྲུབ་ངང་ཐོ་འགོད་བྱས་</translation>
    </message>
</context>
<context>
    <name>Buffer overflow attack on rpc.statd</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="29"/>
        <source>Buffer overflow attack on rpc.statd</source>
        <translation>rpc.sttdཡི་དལ་རྒྱུག་ཁུལ་ལ་ཚུར་རྒོལ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>Buffer overflow on WU-FTPD versions prior to 2.6</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="38"/>
        <source>Buffer overflow on WU-FTPD versions prior to 2.6</source>
        <translation>2.6གི་སྔོན་གྱི་WU-FTPDཡི་པར་གཞིའི་སྟེང་གི་དལ་རྒྱུག་ཁུལ་ནས་ཕྱིར་ཐོན་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>Possible buffer overflow attempt.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="46"/>
        <source>Possible buffer overflow attempt.</source>
        <translation>དལ་བའི་ཁུལ་དུ་ཚོད་ལྟ་བྱེད་སྲིད།</translation>
    </message>
</context>
<context>
    <name>&quot;Null&quot; user changed some information.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="54"/>
        <source>&quot;Null&quot; user changed some information.</source>
        <translation>&quot;Null&quot;སྤྱོད་མཁན་གྱིས་ཆ་འཕྲིན་ཁ་ཤས་ལ་བཟོ་བཅོས་བརྒྱབ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>Buffer overflow attempt (probably on yppasswd).</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="62"/>
        <source>Buffer overflow attempt (probably on yppasswd).</source>
        <translation>དལ་རྒྱུག་ཁུལ་དུ་ཚོད་ལྟ་བྱེད་པ་(dppssasd)སྟེང་དུ་ཡོད་ཤས་ཆེ།</translation>
    </message>
</context>
<context>
    <name>Heap overflow in the Solaris cachefsd service.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="70"/>
        <source>Heap overflow in the Solaris cachefsd service.</source>
        <translation>Soarsarsdཞབས་ཞུའི་ཁྲོད་ནས་སྤུངས་པ།</translation>
    </message>
</context>
<context>
    <name>Stack overflow attempt or program exiting with SEGV (Solaris).</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="78"/>
        <source>Stack overflow attempt or program exiting with SEGV (Solaris).</source>
        <translation>སྤུངས་栈་ཚོད་ལྟ་དང་གོ་རིམ་ལས་ཕྱིར་SEGV(Soorriss)ལས་ཕྱིར་འབུད་བྱས།</translation>
    </message>
</context>
<context>
    <name>Multiple authentication failures.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="90"/>
        <source>Multiple authentication failures.</source>
        <translation>ཐོབ་ཐང་ཚོད་ལྟསར་སྤྲོད་ཐེངས་མང་བྱས་པ་ཕམ་སོང་།</translation>
    </message>
</context>
<context>
    <name>Multiple authentication failures followed by a success.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="101"/>
        <source>Multiple authentication failures followed by a success.</source>
        <translation>ཐོབ་ཐང་ཚོད་ལྟསར་སྤྲོད་ཐེངས་མང་བྱས་ནས་ཕམ་ཉེས་བྱུང་རྗེས་ལེགས་འགྲུབ་བྱུང་བ</translation>
    </message>
</context>
<context>
    <name>Multiple viruses detected - Possible outbreak.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="110"/>
        <source>Multiple viruses detected - Possible outbreak.</source>
        <translation>ནད་དུག་སྣ་ཚོགས་ལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་ནས་འབྱུང་སྲིད་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>Attacks followed by the addition of an user.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="122"/>
        <source>Attacks followed by the addition of an user.</source>
        <translation>སྤྱོད་མཁན་གཞན་དག་གི་ཚུར་རྒོལ་ལས་བྱུང་བ་རེད།</translation>
    </message>
</context>
<context>
    <name>Network scan from same source ip.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="132"/>
        <source>Network scan from same source ip.</source>
        <translation>འབྱུང་ཁུངས་གཅིག་གི་IPནས་དྲ་རྒྱའི་བཤེར་བཤེར་བྱེད་པ།</translation>
    </message>
</context>
</TS>
