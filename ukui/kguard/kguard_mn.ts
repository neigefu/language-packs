<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>web-accesslog messages grouped.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="13"/>
        <source>web-accesslog messages grouped.</source>
        <translation>ᠸᠧᠪ acceslog ᠮᠡᠳᠡᠭᠡᠨ ᠦ ᠳᠤᠭᠤᠶᠢᠯᠠᠩ ᠬᠤᠪᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Ignored URLs (simple queries).</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="44"/>
        <source>Ignored URLs (simple queries).</source>
        <translation>ᠤᠮᠳᠤᠭᠠᠶᠢᠯᠠᠭᠰᠠᠨ URL ( ᠳᠥᠭᠥᠮ ᠢᠶᠡᠷ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ ᠯᠠᠪᠯᠠᠬᠤ ) ᠃</translation>
    </message>
</context>
<context>
    <name>SQL injection attempt.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="53"/>
        <source>SQL injection attempt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Multiple SQL injection attempts from same source ip.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="60"/>
        <source>Multiple SQL injection attempts from same source ip.</source>
        <translation>ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡ ᠢᠷᠡᠯᠲᠡ ᠶᠢᠨ IP ᠡᠴᠡ ᠣᠯᠠᠨ ᠤᠳᠠᠭ᠎ᠠ SQL ᠲᠤᠷᠰᠢᠯᠲᠠ ᠳᠤ ᠣᠷᠣᠭᠤᠯᠵᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>SQL injection attempt.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="69"/>
        <source>SQL injection attempt.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SQL injection attempt.</name>
    <message>
        <location filename="../po/0245-web_rules-part.xml" line="76"/>
        <source>SQL injection attempt.</source>
        <translation>SQL ᠲᠤᠷᠰᠢᠯᠲᠠ ᠳᠤ ᠣᠷᠣᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Grouping of the pam_unix rules.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="13"/>
        <source>Grouping of the pam_unix rules.</source>
        <translation>pam_unix ᠳᠦᠷᠢᠮ ᠦᠨ ᠳᠤᠭᠤᠶᠢᠯᠠᠩ ᠬᠤᠪᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: Login session opened.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="19"/>
        <source>PAM: Login session opened.</source>
        <translation>PAM ᠄ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠯ ᠦᠨ ᠬᠤᠷᠠᠯ ᠳᠤ ᠭᠠᠷᠬᠤ ᠦᠭᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠨᠡᠭᠡᠭᠡᠭ᠍ᠳᠡᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: Login session closed.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="26"/>
        <source>PAM: Login session closed.</source>
        <translation>PAM ᠄ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠯ ᠳᠦ ᠭᠠᠷᠬᠤ ᠬᠤᠷᠠᠯ ᠳᠤ ᠭᠠᠷᠬᠤ ᠦᠭᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠠᠭᠠᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: User login failed.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="35"/>
        <source>PAM: User login failed.</source>
        <translation>PAM ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: Attempt to login with an invalid user.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="41"/>
        <source>PAM: Attempt to login with an invalid user.</source>
        <translation>PAM ᠄ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ ᠲᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: Ignoring Annoying Ubuntu/debian cron login events.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="49"/>
        <source>PAM: Ignoring Annoying Ubuntu/debian cron login events.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PAM: Ignoring Annoying Ubuntu/debian cron login events.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="56"/>
        <source>PAM: Ignoring Annoying Ubuntu/debian cron login events.</source>
        <translation>PAM ᠄ ᠡᠭᠳᠡᠭᠦᠴᠡᠯ ᠢ ᠤᠮᠳᠤᠭᠠᠢ᠌ᠯᠠᠭᠰᠠᠨ Ubuntu/debian ᠳᠤ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠬᠡᠷᠡᠭ ᠢ ᠤᠮᠳᠤᠭᠠᠶ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: Ignoring events with a user or a password.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="62"/>
        <source>PAM: Ignoring events with a user or a password.</source>
        <translation>PAM ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠲᠡᠶ ᠬᠡᠷᠡᠭ ᠢ ᠤᠮᠳᠤᠭᠠᠶᠢᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: Multiple failed logins in a small period of time.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="68"/>
        <source>PAM: Multiple failed logins in a small period of time.</source>
        <translation>PAM ᠄ ᠣᠬᠣᠷᠬᠠᠨ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠣᠯᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠢᠯᠠᠭᠳᠠᠯ ᠳᠤ ᠭᠠᠷᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM and gdm are not playing nicely.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="76"/>
        <source>PAM and gdm are not playing nicely.</source>
        <translation>PAM ᠪᠣᠯᠣᠨ gdm ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠤ ᠪᠠᠶᠢᠳᠠᠯ ᠮᠠᠭᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM misconfiguration.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="82"/>
        <source>PAM misconfiguration.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PAM misconfiguration.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="88"/>
        <source>PAM misconfiguration.</source>
        <translation>PAM ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠨᠢ ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>PAM: User changed password.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="93"/>
        <source>PAM: User changed password.</source>
        <translation>ᠫᠫᠮ  ᠬᠡᠷᠡᠬᠯᠡᠭᠴᠢ ᠨᠢᠭᠤᠴᠠ ᠨᠤᠮᠸᠷ ᠶᠢᠡᠨ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>unix_chkpwd grouping.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="100"/>
        <source>unix_chkpwd grouping.</source>
        <translation>unix_chkpwd ᠳᠤᠭᠤᠶᠢᠯᠠᠩ ᠬᠤᠪᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>unix_chkpwd: Password check failed.</name>
    <message>
        <location filename="../po/0085-pam_rules.xml" line="106"/>
        <source>unix_chkpwd: Password check failed.</source>
        <translation>unix_chkpwd  ᠨᠢᠭᠤᠴᠠ ᠨᠤᠮᠸᠷ ᠤᠨ ᠪᠠᠶᠢᠴᠠᠭᠠᠯᠲᠠ ᠢᠯᠠᠭᠲᠠᠭᠰᠠᠨ ᠃</translation>
    </message>
</context>
<context>
    <name>Possible exploit attempt.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="7"/>
        <source>Possible exploit attempt.</source>
        <translation>ᠭᠡᠮ ᠰᠤᠭᠤᠭ ᠢ ᠠᠰᠢᠭᠯᠠᠨ ᠲᠤᠷᠰᠢᠬᠤ ᠪᠣᠯᠣᠯᠴᠠᠭ᠎ᠠ ᠲᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Multiple authentication failures in very short time!</name>
    <message>
        <location filename="../po/9999_custom.xml" line="18"/>
        <source>Multiple authentication failures in very short time!</source>
        <translation>ᠮᠠᠰᠢ ᠣᠬᠣᠷ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠣᠯᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠬᠢᠷᠢ ᠭᠡᠮ ᠢ ᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ !</translation>
    </message>
</context>
<context>
    <name>pam: locked account.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="31"/>
        <source>pam: locked account.</source>
        <translation>PAM ᠄ ᠳᠠᠩᠰᠠᠨ ᠡᠷᠦᠬᠡ ᠶᠢ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Root session opened.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="41"/>
        <source>Root session opened.</source>
        <translation>root ᠦᠭᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠨᠡᠭᠡᠭᠡᠭ᠍ᠳᠡᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>NetworkManager starting.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="51"/>
        <source>NetworkManager starting.</source>
        <translation>Networkaanager ᠡᠬᠢᠯᠡᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>High-risk root session opened.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="58"/>
        <source>High-risk root session opened.</source>
        <translation>ᠠᠶᠤᠯ ᠶᠡᠬᠡᠲᠡᠢ ᠷᠦᠲ᠋ ᠤᠨ ᠦᠭᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠨᠡᠭᠡᠭᠡᠭ᠍ᠳᠡᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Dbus privilege escalation.</name>
    <message>
        <location filename="../po/9999_custom.xml" line="69"/>
        <source>Dbus privilege escalation.</source>
        <translation>Dbus ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠳᠡᠭᠡᠭᠰᠢᠯᠡᠪᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>Catchall rule for unknown types.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="5"/>
        <source>Catchall rule for unknown types.</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ Catchalal ᠳᠦᠷᠢᠮ ᠃</translation>
    </message>
</context>
<context>
    <name>Catchall rule for control messages.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="10"/>
        <source>Catchall rule for control messages.</source>
        <translation>ᠴᠢᠮᠡᠭᠡ ᠶᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ Catchall ᠳᠦᠷᠢᠮ ᠃</translation>
    </message>
</context>
<context>
    <name>Catchall rule for non control messages.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="15"/>
        <source>Catchall rule for non control messages.</source>
        <translation>ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠪᠤᠰᠤ ᠮᠡᠳᠡᠭᠡᠨ ᠦ Catchal ᠳᠦᠷᠢᠮ ᠃</translation>
    </message>
</context>
<context>
    <name>Catchall rule for possible erroneous logs.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="20"/>
        <source>Catchall rule for possible erroneous logs.</source>
        <translation>ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠦᠨ ᠺᠠᠲ᠋ᠠᠯ ᠤᠨ ᠳᠦᠷᠢᠮ ᠳᠦ ᠴᠢᠭᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>Catchall rule for possible attack/intrusion logs.</name>
    <message>
        <location filename="../po/9999_generic.xml" line="25"/>
        <source>Catchall rule for possible attack/intrusion logs.</source>
        <translation>ᠪᠣᠯᠣᠯᠴᠠᠭ᠎ᠠ ᠲᠠᠢ ᠳᠠᠭᠠᠷᠢᠯᠲᠠ / ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭ ᠲᠦ ᠬᠠᠯᠳᠠᠭᠰᠠᠨ Catchal ᠳᠦᠷᠢᠮ ᠳᠦ ᠴᠢᠭᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>Audit: messages grouped.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="14"/>
        <source>Audit: messages grouped.</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠮᠡᠳᠡᠭᠡ ᠶᠢ ᠳᠤᠭᠤᠶᠢᠯᠠᠩ ᠬᠤᠪᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: Start / Resume</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="26"/>
        <source>Auditd: Start / Resume</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠡᠬᠢᠯᠡᠭᠡᠳ ᠰᠡᠷᠭᠦᠭᠡᠬᠦ / ᠰᠡᠷᠭᠦᠭᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: Start / Resume FAILED</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="34"/>
        <source>Auditd: Start / Resume FAILED</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠡᠬᠢᠯᠡᠭᠦᠯᠵᠦ ᠢᠯᠠᠭᠳᠠᠬᠤ ᠶᠢ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: End</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="46"/>
        <source>Auditd: End</source>
        <translation>ᠬᠢᠨᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠄ ᠳᠠᠭᠤᠰᠬᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: Abort</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="56"/>
        <source>Auditd: Abort</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠵᠣᠭᠰᠣᠭᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: Configuration changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="67"/>
        <source>Auditd: Configuration changed</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: device enables promiscuous mode</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="82"/>
        <source>Auditd: device enables promiscuous mode</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠬᠣᠯᠢᠯᠳᠤᠭᠤᠯᠬᠤ ᠵᠠᠭᠪᠤᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: process ended abnormally</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="92"/>
        <source>Auditd: process ended abnormally</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠴᠤᠪᠤᠷᠠᠯ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: execution of a file ended abnormally</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="102"/>
        <source>Auditd: execution of a file ended abnormally</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠬᠦ ᠳᠦ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠳᠠᠭᠤᠰᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: file is made executable</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="112"/>
        <source>Auditd: file is made executable</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠵᠦ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: file or a directory access ended abnormally</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="122"/>
        <source>Auditd: file or a directory access ended abnormally</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠪᠤᠶᠤ ᠭᠠᠷᠴᠠᠭ ᠤᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠯ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠳᠠᠭᠤᠰᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: failure of the Abstract Machine Test Utility (AMTU) detected</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="132"/>
        <source>Auditd: failure of the Abstract Machine Test Utility (AMTU) detected</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠬᠡᠶᠢᠰᠬᠡᠭᠦᠷ ᠦᠨ ᠬᠡᠮᠵᠢᠭᠦᠷ ᠦᠨ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ( AMTU ) ᠢᠯᠠᠭᠳᠠᠪᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: maximum amount of Discretionary Access Control (DAC) or Mandatory Access Control (MAC) failures reached</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="143"/>
        <source>Auditd: maximum amount of Discretionary Access Control (DAC) or Mandatory Access Control (MAC) failures reached</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠳᠤᠷ᠎ᠠ ᠪᠠᠷ ᠢᠶᠠᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠵᠡᠮᠳᠡᠯ ( DAC ) ᠪᠤᠶᠤ ᠠᠯᠪᠠᠳᠠᠯᠭ᠎ᠠ ᠪᠠᠷ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠨ ᠡᠵᠡᠮᠳᠡᠬᠦ ( MAC ) ᠶᠢᠨ ᠬᠠᠮᠤᠭ ᠤᠨ ᠶᠡᠬᠡ ᠲᠣᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠳᠦ ᠬᠦᠷᠦᠭᠰᠡᠨ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: Role-Based Access Control (RBAC) failure detected.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="154"/>
        <source>Auditd: Role-Based Access Control (RBAC) failure detected.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠲᠣᠭᠯᠠᠭᠴᠢ ᠳᠤ ᠰᠠᠭᠤᠷᠢᠯᠠᠭᠰᠠᠨ ᠠᠶᠢᠯᠴᠢᠯᠠᠯᠲᠠ ᠶᠢᠨ ᠡᠵᠡᠮᠳᠡᠯ ( RBAC ) ᠢᠯᠠᠭᠳᠠᠪᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: user-space account addition ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="164"/>
        <source>Auditd: user-space account addition ended abnormally.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠣᠷᠣᠨ ᠵᠠᠶ ᠶᠢᠨ ᠳᠠᠩᠰᠠᠨ ᠡᠷᠦᠬᠡ ᠳᠦ ᠨᠡᠮᠡᠵᠦ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠳᠠᠭᠤᠰᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: user-space account deletion ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="174"/>
        <source>Auditd: user-space account deletion ended abnormally.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠣᠷᠣᠨ ᠵᠠᠶ ᠶᠢᠨ ᠳᠠᠩᠰᠠᠨ ᠡᠷᠦᠬᠡ ᠶᠢ ᠬᠠᠰᠤᠬᠤ ᠳᠤ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠳᠠᠭᠤᠰᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: user-space account modification ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="184"/>
        <source>Auditd: user-space account modification ended abnormally.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠣᠷᠣᠨ ᠵᠠᠶ ᠶᠢᠨ ᠳᠠᠩᠰᠠᠨ ᠤ ᠨᠣᠮᠧᠷ ᠢ ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠳᠠᠭᠤᠰᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: user becomes root</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="194"/>
        <source>Auditd: user becomes root</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ root ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠪᠣᠯᠭᠠᠨ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: account login attempt ended abnormally.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="204"/>
        <source>Auditd: account login attempt ended abnormally.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠳᠠᠩᠰᠠᠨ ᠡᠷᠦᠬᠡ ᠳᠦ ᠲᠡᠮᠳᠡᠭᠯᠡᠨ ᠲᠤᠷᠰᠢᠬᠤ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠳᠠᠭᠤᠰᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: limit of failed login attempts reached.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="214"/>
        <source>Auditd: limit of failed login attempts reached.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ ᠨᠢᠭᠡᠨᠲᠡ ᠲᠡᠮᠳᠡᠭᠯᠡᠭᠰᠡᠨ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠳᠤ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠦᠷᠦᠭᠰᠡᠨ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: login attempt from a forbidden location.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="224"/>
        <source>Auditd: login attempt from a forbidden location.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠬᠤ ᠄ ᠪᠠᠶᠢᠩᠭᠤ ᠰᠠᠭᠤᠭ᠎ᠠ ᠪᠠᠶᠢᠷᠢ ᠠᠴᠠ ᠲᠡᠮᠳᠡᠭᠯᠡᠨ ᠲᠤᠷᠰᠢᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: login attempt reached the maximum amount of concurrent sessions.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="234"/>
        <source>Auditd: login attempt reached the maximum amount of concurrent sessions.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠄ ᠲᠡᠮᠳᠡᠭᠯᠡᠬᠦ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢᠨ ᠤᠳᠠᠭ᠎ᠠ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠦᠷᠦᠭᠰᠡᠨ ᠪᠥᠭᠡᠳ ᠦᠭᠡ ᠬᠡᠯᠡᠬᠦ ᠲᠣᠭ᠎ᠠ ᠶᠢᠨ ᠬᠠᠮᠤᠭ ᠤᠨ ᠶᠡᠬᠡ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠳᠦ ᠬᠦᠷᠴᠡᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: login attempt is made at a time when it is prevented by.</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="244"/>
        <source>Auditd: login attempt is made at a time when it is prevented by.</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠄ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠬᠦ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢ ᠬᠣᠷᠢᠭᠯᠠᠭᠳᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳᠦ ᠶᠠᠪᠤᠭᠳᠠᠭᠰᠠᠨ ᠶᠤᠮ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: SELinux permission check</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="256"/>
        <source>Auditd: SELinux permission check</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ SELinuux ᠶᠢᠨ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠦᠨ ᠪᠠᠶᠢᠴᠠᠭᠠᠯᠲᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: SELinux mode (enforcing, permissive, off) is changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="266"/>
        <source>Auditd: SELinux mode (enforcing, permissive, off) is changed</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠨᠢ ᠄ SELinuf ᠵᠠᠭᠪᠤᠷ ( lawing ᠂ perisssese ᠂ off ) ᠨᠢᠭᠡᠨᠲᠡ ᠬᠤᠪᠢᠷᠠᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: SELinux error</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="277"/>
        <source>Auditd: SELinux error</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ SELinuux ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: replay attack detected</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="289"/>
        <source>Auditd: replay attack detected</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠄ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠳᠦ ᠬᠦᠨᠳᠦ ᠳᠠᠭᠠᠷᠢᠯᠲᠠ ᠳᠤ ᠬᠦᠷᠴᠡᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: group ID changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="299"/>
        <source>Auditd: group ID changed</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠢᠨᠠᠭᠰᠠᠨ ᠄ ᠳᠤᠭᠤᠶᠢᠯᠠᠩ ᠤᠨ ID ᠨᠢᠭᠡᠨᠲᠡ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Auditd: user ID changed</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="309"/>
        <source>Auditd: user ID changed</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ID ᠨᠢᠭᠡᠨᠲᠡ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Write access</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="319"/>
        <source>Audit: Watch - Write access</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠳᠦ ᠪᠢᠴᠢᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Write access: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="326"/>
        <source>Audit: Watch - Write access: $(audit.file.name)</source>
        <translation>ᠬᠢᠨᠠᠨ ᠬᠠᠷᠭᠤᠭᠤᠯᠤᠯᠲᠠ ᠄ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠪᠢᠴᠢᠵᠦ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠄ = ( audit.file.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Write access: $(audit.directory.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="333"/>
        <source>Audit: Watch - Write access: $(audit.directory.name)</source>
        <translation>ᠬᠢᠨᠠᠨ ᠬᠠᠷᠭᠤᠭᠤᠯᠤᠯᠲᠠ ᠄ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠪᠢᠴᠢᠵᠦ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠄ ? ( audit.directory.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Read access</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="340"/>
        <source>Audit: Watch - Read access</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠢ ᠤᠩᠰᠢᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Read access: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="347"/>
        <source>Audit: Watch - Read access: $(audit.file.name)</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠦᠵᠡᠬᠦ — ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠭ᠎ᠠ ᠶᠢᠨ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠢ ᠤᠩᠰᠢᠬᠤ ᠄ = ( audit.file.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Read access: $(audit.directory.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="354"/>
        <source>Audit: Watch - Read access: $(audit.directory.name)</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠢ ᠤᠩᠰᠢᠬᠤ ᠄ ? ( audit.directory.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Change attribute</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="361"/>
        <source>Audit: Watch - Change attribute</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Change attribute: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="368"/>
        <source>Audit: Watch - Change attribute: $(audit.file.name)</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠨ᠎ᠡ ᠄ ? ( audit.file.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Change attribute: $(audit.directory.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="375"/>
        <source>Audit: Watch - Change attribute: $(audit.directory.name)</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠄ ᣛ ( audit.directory.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Watch - Execute access: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="382"/>
        <source>Audit: Watch - Execute access: $(audit.file.name)</source>
        <translation>ᠬᠢᠨᠠᠨ ᠬᠠᠷᠭᠤᠭᠤᠯᠬᠤ ᠄ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ — ᠠᠶᠢᠯᠴᠢᠯᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠄ ? ( audit.file.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Created: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="389"/>
        <source>Audit: Created: $(audit.file.name)</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠨᠢᠭᠡᠨᠲᠡ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢ ᠄ ɸ ( audit.file.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Deleted: $(audit.file.name)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="396"/>
        <source>Audit: Deleted: $(audit.file.name)</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ɸ ( audit.file.name )</translation>
    </message>
</context>
<context>
    <name>Audit: Command: $(audit.exe)</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="407"/>
        <source>Audit: Command: $(audit.exe)</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ ᠵᠠᠷᠯᠢᠭ ᠄ ᣛ ( audit.exe )</translation>
    </message>
</context>
<context>
    <name>Audit: $(audit.type).</name>
    <message>
        <location filename="../po/0365-auditd_rules.xml" line="414"/>
        <source>Audit: $(audit.type).</source>
        <translation>ᠬᠢᠨᠠᠬᠤ ᠄ = audit.type ) ᠃</translation>
    </message>
</context>
<context>
    <name>System user successfully logged to the system.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="19"/>
        <source>System user successfully logged to the system.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮᠲᠦ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ ᠠᠮᠵᠢᠯᠲᠠ ᠲᠠᠶ ᠪᠠᠷ ᠭᠠᠷᠳᠠᠪᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Buffer overflow attack on rpc.statd</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="29"/>
        <source>Buffer overflow attack on rpc.statd</source>
        <translation>rpc.statatd ᠳᠡᠭᠡᠷᠡᠬᠢ ᠠᠭᠠᠵᠢᠮ ᠣᠷᠣᠨ ᠳᠠᠭᠠᠷᠢᠯᠲᠠ ᠭᠠᠷᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Buffer overflow on WU-FTPD versions prior to 2.6</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="38"/>
        <source>Buffer overflow on WU-FTPD versions prior to 2.6</source>
        <translation>イ᠃カ ᠡᠴᠠ ᠡᠮᠣᠨ᠎ᠠ WU-FTPD ᠬᠡᠪᠯᠠᠯ ᠲᠠᠭᠡᠷ᠎ᠠ ᠠᠯᠭᠤᠷ ᠵᠠᠪᠰᠠᠷᠲᠤ ᠣᠷᠣᠠ ᠭᠠᠷᠴᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Possible buffer overflow attempt.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="46"/>
        <source>Possible buffer overflow attempt.</source>
        <translation>ᠠᠭᠠᠵᠢᠮ ᠣᠷᠣᠨ ᠤ ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠯ ᠦᠨ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢ ᠪᠢᠯᠬᠠᠵᠤ ᠦᠵᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>&quot;Null&quot; user changed some information.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="54"/>
        <source>&quot;Null&quot; user changed some information.</source>
        <translation>《 Nul 》 ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠡᠳᠦᠢ ᠲᠡᠳᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠶᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Buffer overflow attempt (probably on yppasswd).</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="62"/>
        <source>Buffer overflow attempt (probably on yppasswd).</source>
        <translation>ᠵᠠᠪᠰᠠᠷᠲᠤ ᠣᠷᠣᠨ ᠤ ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠯ ᠦᠨ ᠲᠤᠷᠰᠢᠯᠲᠠ ( yppassswd ᠳᠡᠭᠡᠷ᠎ᠡ ) ᠃</translation>
    </message>
</context>
<context>
    <name>Heap overflow in the Solaris cachefsd service.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="70"/>
        <source>Heap overflow in the Solaris cachefsd service.</source>
        <translation>Solaris cachefsdd ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠳᠡᠬᠢ ᠣᠪᠣᠭᠠᠯᠠᠨ ᠭᠠᠷᠴᠠᠶ ᠃</translation>
    </message>
</context>
<context>
    <name>Stack overflow attempt or program exiting with SEGV (Solaris).</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="78"/>
        <source>Stack overflow attempt or program exiting with SEGV (Solaris).</source>
        <translation>ᠣᠪᠣᠭᠠᠯᠠᠬᠤ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠪᠤᠶᠤ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠨᠢ SEGV ( Solaris ) ᠡᠴᠡ ᠤᠬᠤᠷᠢᠨ ᠭᠠᠷᠴᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Multiple authentication failures.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="90"/>
        <source>Multiple authentication failures.</source>
        <translation>ᠣᠯᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠭᠠᠷᠤᠯ ᠢ ᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Multiple authentication failures followed by a success.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="101"/>
        <source>Multiple authentication failures followed by a success.</source>
        <translation>ᠣᠯᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠭᠠᠷᠤᠯ ᠢ ᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠪᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Multiple viruses detected - Possible outbreak.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="110"/>
        <source>Multiple viruses detected - Possible outbreak.</source>
        <translation>ᠣᠯᠠᠨ ᠵᠦᠢᠯ ᠦᠨ ᠸᠢᠷᠦ᠋ᠰ ᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠶᠢᠴᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ — ᠳᠡᠯᠪᠡᠷᠡᠬᠦ ᠮᠠᠭᠠᠳ ᠃</translation>
    </message>
</context>
<context>
    <name>Attacks followed by the addition of an user.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="122"/>
        <source>Attacks followed by the addition of an user.</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠳᠠᠭᠠᠷᠢᠯᠲᠠ ᠠᠴᠠ ᠢᠷᠡᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Network scan from same source ip.</name>
    <message>
        <location filename="../po/0280-attack_rules.xml" line="132"/>
        <source>Network scan from same source ip.</source>
        <translation>ᠠᠳᠠᠯᠢ ᠢᠷᠡᠯᠲᠡ ᠶᠢᠨ IP ᠠᠴᠠ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠰᠢᠷᠦᠭᠦᠨ ᠰᠢᠷᠦᠭᠦᠰᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
</context>
</TS>
