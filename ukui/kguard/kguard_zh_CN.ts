<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
    <context>
        <name>web-accesslog messages grouped.</name>
        <message>
            <location filename="../po/0245-web_rules-part.xml" line="13"/>
            <source>web-accesslog messages grouped.</source>
            <translation>Web-accesslog 消息分组。</translation>
        </message>
    </context>
    <context>
        <name>Ignored URLs (simple queries).</name>
        <message>
            <location filename="../po/0245-web_rules-part.xml" line="44"/>
            <source>Ignored URLs (simple queries).</source>
            <translation>忽略的 URL（简单查询）。</translation>
        </message>
    </context>
    <context>
        <name>SQL injection attempt.</name>
        <message>
            <location filename="../po/0245-web_rules-part.xml" line="53"/>
            <source>SQL injection attempt.</source>
            <translation>SQL 注入尝试。</translation>
        </message>
    </context>
    <context>
        <name>Multiple SQL injection attempts from same source ip.</name>
        <message>
            <location filename="../po/0245-web_rules-part.xml" line="60"/>
            <source>Multiple SQL injection attempts from same source ip.</source>
            <translation>从同一源 IP 进行多次 SQL 注入尝试。</translation>
        </message>
    </context>
    <context>
        <name>SQL injection attempt.</name>
        <message>
            <location filename="../po/0245-web_rules-part.xml" line="69"/>
            <source>SQL injection attempt.</source>
            <translation>SQL 注入尝试。</translation>
        </message>
    </context>
    <context>
        <name>SQL injection attempt.</name>
        <message>
            <location filename="../po/0245-web_rules-part.xml" line="76"/>
            <source>SQL injection attempt.</source>
            <translation>SQL 注入尝试。</translation>
        </message>
    </context>
    <context>
        <name>Grouping of the pam_unix rules.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="13"/>
            <source>Grouping of the pam_unix rules.</source>
            <translation>pam_unix规则的分组。</translation>
        </message>
    </context>
    <context>
        <name>PAM: Login session opened.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="19"/>
            <source>PAM: Login session opened.</source>
            <translation>PAM：登录会话已打开。</translation>
        </message>
    </context>
    <context>
        <name>PAM: Login session closed.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="26"/>
            <source>PAM: Login session closed.</source>
            <translation>PAM：登录会话已关闭。</translation>
        </message>
    </context>
    <context>
        <name>PAM: User login failed.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="35"/>
            <source>PAM: User login failed.</source>
            <translation>PAM：用户登录失败。</translation>
        </message>
    </context>
    <context>
        <name>PAM: Attempt to login with an invalid user.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="41"/>
            <source>PAM: Attempt to login with an invalid user.</source>
            <translation>PAM：尝试使用无效用户登录。</translation>
        </message>
    </context>
    <context>
        <name>PAM: Ignoring Annoying Ubuntu/debian cron login events.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="49"/>
            <source>PAM: Ignoring Annoying Ubuntu/debian cron login events.</source>
            <translation>PAM：忽略烦人的 Ubuntu/debian cron 登录事件。</translation>
        </message>
    </context>
    <context>
        <name>PAM: Ignoring Annoying Ubuntu/debian cron login events.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="56"/>
            <source>PAM: Ignoring Annoying Ubuntu/debian cron login events.</source>
            <translation>PAM：忽略烦人的 Ubuntu/debian cron 登录事件。</translation>
        </message>
    </context>
    <context>
        <name>PAM: Ignoring events with a user or a password.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="62"/>
            <source>PAM: Ignoring events with a user or a password.</source>
            <translation>PAM：忽略具有用户或密码的事件。</translation>
        </message>
    </context>
    <context>
        <name>PAM: Multiple failed logins in a small period of time.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="68"/>
            <source>PAM: Multiple failed logins in a small period of time.</source>
            <translation>PAM：在短时间内多次登录失败。</translation>
        </message>
    </context>
    <context>
        <name>PAM and gdm are not playing nicely.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="76"/>
            <source>PAM and gdm are not playing nicely.</source>
            <translation>PAM 和 gdm 运行状态不好。</translation>
        </message>
    </context>
    <context>
        <name>PAM misconfiguration.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="82"/>
            <source>PAM misconfiguration.</source>
            <translation>PAM 配置错误。</translation>
        </message>
    </context>
    <context>
        <name>PAM misconfiguration.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="88"/>
            <source>PAM misconfiguration.</source>
            <translation>PAM 配置错误。</translation>
        </message>
    </context>
    <context>
        <name>PAM: User changed password.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="93"/>
            <source>PAM: User changed password.</source>
            <translation>PAM：用户更改了密码。</translation>
        </message>
    </context>
    <context>
        <name>unix_chkpwd grouping.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="100"/>
            <source>unix_chkpwd grouping.</source>
            <translation>unix_chkpwd分组。</translation>
        </message>
    </context>
    <context>
        <name>unix_chkpwd: Password check failed.</name>
        <message>
            <location filename="../po/0085-pam_rules.xml" line="106"/>
            <source>unix_chkpwd: Password check failed.</source>
            <translation>unix_chkpwd：密码检查失败。</translation>
        </message>
    </context>
    <context>
        <name>Possible exploit attempt.</name>
        <message>
            <location filename="../po/9999_custom.xml" line="7"/>
            <source>Possible exploit attempt.</source>
            <translation>可能的漏洞利用尝试。</translation>
        </message>
    </context>
    <context>
        <name>Multiple authentication failures in very short time!</name>
        <message>
            <location filename="../po/9999_custom.xml" line="18"/>
            <source>Multiple authentication failures in very short time!</source>
            <translation>在很短的时间内多次身份验证失败！</translation>
        </message>
    </context>
    <context>
        <name>pam: locked account.</name>
        <message>
            <location filename="../po/9999_custom.xml" line="31"/>
            <source>pam: locked account.</source>
            <translation>PAM：锁定帐户。</translation>
        </message>
    </context>
    <context>
        <name>Root session opened.</name>
        <message>
            <location filename="../po/9999_custom.xml" line="41"/>
            <source>Root session opened.</source>
            <translation>root会话已打开。</translation>
        </message>
    </context>
    <context>
        <name>NetworkManager starting.</name>
        <message>
            <location filename="../po/9999_custom.xml" line="51"/>
            <source>NetworkManager starting.</source>
            <translation>NetworkManager 启动。</translation>
        </message>
    </context>
    <context>
        <name>High-risk root session opened.</name>
        <message>
            <location filename="../po/9999_custom.xml" line="58"/>
            <source>High-risk root session opened.</source>
            <translation>高风险root会话已打开。</translation>
        </message>
    </context>
    <context>
        <name>Dbus privilege escalation.</name>
        <message>
            <location filename="../po/9999_custom.xml" line="69"/>
            <source>Dbus privilege escalation.</source>
            <translation>Dbus 权限提升。</translation>
        </message>
    </context>
    <context>
        <name>Catchall rule for unknown types.</name>
        <message>
            <location filename="../po/9999_generic.xml" line="5"/>
            <source>Catchall rule for unknown types.</source>
            <translation>未知类型的 Catchall 规则。</translation>
        </message>
    </context>
    <context>
        <name>Catchall rule for control messages.</name>
        <message>
            <location filename="../po/9999_generic.xml" line="10"/>
            <source>Catchall rule for control messages.</source>
            <translation>控制消息的 Catchall 规则。</translation>
        </message>
    </context>
    <context>
        <name>Catchall rule for non control messages.</name>
        <message>
            <location filename="../po/9999_generic.xml" line="15"/>
            <source>Catchall rule for non control messages.</source>
            <translation>非控制消息的 Catchall 规则。</translation>
        </message>
    </context>
    <context>
        <name>Catchall rule for possible erroneous logs.</name>
        <message>
            <location filename="../po/9999_generic.xml" line="20"/>
            <source>Catchall rule for possible erroneous logs.</source>
            <translation>针对可能的错误日志的 Catchall 规则。</translation>
        </message>
    </context>
    <context>
        <name>Catchall rule for possible attack/intrusion logs.</name>
        <message>
            <location filename="../po/9999_generic.xml" line="25"/>
            <source>Catchall rule for possible attack/intrusion logs.</source>
            <translation>针对可能的攻击/入侵日志的 Catchall 规则。</translation>
        </message>
    </context>
    <context>
        <name>Audit: messages grouped.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="14"/>
            <source>Audit: messages grouped.</source>
            <translation>审核：消息分组。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: Start / Resume</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="26"/>
            <source>Auditd: Start / Resume</source>
            <translation>审核：开始/恢复</translation>
        </message>
    </context>
    <context>
        <name>Auditd: Start / Resume FAILED</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="34"/>
            <source>Auditd: Start / Resume FAILED</source>
            <translation>审核：启动/恢复失败</translation>
        </message>
    </context>
    <context>
        <name>Auditd: End</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="46"/>
            <source>Auditd: End</source>
            <translation>审核日期：结束</translation>
        </message>
    </context>
    <context>
        <name>Auditd: Abort</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="56"/>
            <source>Auditd: Abort</source>
            <translation>审核：中止</translation>
        </message>
    </context>
    <context>
        <name>Auditd: Configuration changed</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="67"/>
            <source>Auditd: Configuration changed</source>
            <translation>审核：配置已更改</translation>
        </message>
    </context>
    <context>
        <name>Auditd: device enables promiscuous mode</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="82"/>
            <source>Auditd: device enables promiscuous mode</source>
            <translation>审核：设备启用混杂模式</translation>
        </message>
    </context>
    <context>
        <name>Auditd: process ended abnormally</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="92"/>
            <source>Auditd: process ended abnormally</source>
            <translation>审核：流程异常</translation>
        </message>
    </context>
    <context>
        <name>Auditd: execution of a file ended abnormally</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="102"/>
            <source>Auditd: execution of a file ended abnormally</source>
            <translation>已审核：文件执行异常结束</translation>
        </message>
    </context>
    <context>
        <name>Auditd: file is made executable</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="112"/>
            <source>Auditd: file is made executable</source>
            <translation>审核：文件可执行</translation>
        </message>
    </context>
    <context>
        <name>Auditd: file or a directory access ended abnormally</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="122"/>
            <source>Auditd: file or a directory access ended abnormally</source>
            <translation>已审核：文件或目录访问异常结束</translation>
        </message>
    </context>
    <context>
        <name>Auditd: failure of the Abstract Machine Test Utility (AMTU) detected</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="132"/>
            <source>Auditd: failure of the Abstract Machine Test Utility (AMTU) detected</source>
            <translation>已审核：检测到抽象机器测试实用程序 （AMTU） 失败</translation>
        </message>
    </context>
    <context>
        <name>Auditd: maximum amount of Discretionary Access Control (DAC) or Mandatory Access Control (MAC) failures reached</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="143"/>
            <source>Auditd: maximum amount of Discretionary Access Control (DAC) or Mandatory Access Control (MAC) failures reached</source>
            <translation>已审核：已达到任意访问控制 （DAC） 或强制访问控制 （MAC） 的最大数量</translation>
        </message>
    </context>
    <context>
        <name>Auditd: Role-Based Access Control (RBAC) failure detected.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="154"/>
            <source>Auditd: Role-Based Access Control (RBAC) failure detected.</source>
            <translation>已审核：基于角色的访问控制 （RBAC） 失败。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: user-space account addition ended abnormally.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="164"/>
            <source>Auditd: user-space account addition ended abnormally.</source>
            <translation>已审核：用户空间帐户添加异常结束。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: user-space account deletion ended abnormally.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="174"/>
            <source>Auditd: user-space account deletion ended abnormally.</source>
            <translation>已审核：用户空间帐户删除异常结束。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: user-space account modification ended abnormally.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="184"/>
            <source>Auditd: user-space account modification ended abnormally.</source>
            <translation>已审核：用户空间账号修改异常结束。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: user becomes root</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="194"/>
            <source>Auditd: user becomes root</source>
            <translation>审核：用户变为 root 用户</translation>
        </message>
    </context>
    <context>
        <name>Auditd: account login attempt ended abnormally.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="204"/>
            <source>Auditd: account login attempt ended abnormally.</source>
            <translation>已审核：帐户登录尝试异常结束。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: limit of failed login attempts reached.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="214"/>
            <source>Auditd: limit of failed login attempts reached.</source>
            <translation>已审核：已达到登录失败的限制。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: login attempt from a forbidden location.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="224"/>
            <source>Auditd: login attempt from a forbidden location.</source>
            <translation>已审核：从常住位置登录尝试。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: login attempt reached the maximum amount of concurrent sessions.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="234"/>
            <source>Auditd: login attempt reached the maximum amount of concurrent sessions.</source>
            <translation>已审核：登录尝试次数已达到并发会话数的最大值。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: login attempt is made at a time when it is prevented by.</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="244"/>
            <source>Auditd: login attempt is made at a time when it is prevented by.</source>
            <translation>已审核：登录尝试是在被阻止时进行的。</translation>
        </message>
    </context>
    <context>
        <name>Auditd: SELinux permission check</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="256"/>
            <source>Auditd: SELinux permission check</source>
            <translation>审核：SELinux 权限检查</translation>
        </message>
    </context>
    <context>
        <name>Auditd: SELinux mode (enforcing, permissive, off) is changed</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="266"/>
            <source>Auditd: SELinux mode (enforcing, permissive, off) is changed</source>
            <translation>已审核：SELinux 模式（lawing、permissive、off）已更改</translation>
        </message>
    </context>
    <context>
        <name>Auditd: SELinux error</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="277"/>
            <source>Auditd: SELinux error</source>
            <translation>审核：SELinux 错误</translation>
        </message>
    </context>
    <context>
        <name>Auditd: replay attack detected</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="289"/>
            <source>Auditd: replay attack detected</source>
            <translation>已审核：检测到重放攻击</translation>
        </message>
    </context>
    <context>
        <name>Auditd: group ID changed</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="299"/>
            <source>Auditd: group ID changed</source>
            <translation>已审核：组 ID 已更改</translation>
        </message>
    </context>
    <context>
        <name>Auditd: user ID changed</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="309"/>
            <source>Auditd: user ID changed</source>
            <translation>审核：用户 ID 已更改</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Write access</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="319"/>
            <source>Audit: Watch - Write access</source>
            <translation>审核：监视 - 写入访问权限</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Write access: $(audit.file.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="326"/>
            <source>Audit: Watch - Write access: $(audit.file.name)</source>
            <translation>审核：监视 - 写入权限：$（audit.file.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Write access: $(audit.directory.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="333"/>
            <source>Audit: Watch - Write access: $(audit.directory.name)</source>
            <translation>审核：监视 - 写入权限：$（audit.directory.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Read access</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="340"/>
            <source>Audit: Watch - Read access</source>
            <translation>审核：监视 - 读取访问权限</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Read access: $(audit.file.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="347"/>
            <source>Audit: Watch - Read access: $(audit.file.name)</source>
            <translation>审核：观看 - 读取访问权限：$（audit.file.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Read access: $(audit.directory.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="354"/>
            <source>Audit: Watch - Read access: $(audit.directory.name)</source>
            <translation>审核：监视 - 读取访问权限：$（audit.directory.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Change attribute</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="361"/>
            <source>Audit: Watch - Change attribute</source>
            <translation>审核：监视 - 更改属性</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Change attribute: $(audit.file.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="368"/>
            <source>Audit: Watch - Change attribute: $(audit.file.name)</source>
            <translation>审核：监视 - 更改属性：$（audit.file.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Change attribute: $(audit.directory.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="375"/>
            <source>Audit: Watch - Change attribute: $(audit.directory.name)</source>
            <translation>审核：监视 - 更改属性：$（audit.directory.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Watch - Execute access: $(audit.file.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="382"/>
            <source>Audit: Watch - Execute access: $(audit.file.name)</source>
            <translation>审核：监视 - 执行访问权限：$（audit.file.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Created: $(audit.file.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="389"/>
            <source>Audit: Created: $(audit.file.name)</source>
            <translation>审核：已创建：$（audit.file.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Deleted: $(audit.file.name)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="396"/>
            <source>Audit: Deleted: $(audit.file.name)</source>
            <translation>审核：已删除：$（audit.file.name）</translation>
        </message>
    </context>
    <context>
        <name>Audit: Command: $(audit.exe)</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="407"/>
            <source>Audit: Command: $(audit.exe)</source>
            <translation>审核：命令：$（audit.exe）</translation>
        </message>
    </context>
    <context>
        <name>Audit: $(audit.type).</name>
        <message>
            <location filename="../po/0365-auditd_rules.xml" line="414"/>
            <source>Audit: $(audit.type).</source>
            <translation>审核：$（audit.type）。</translation>
        </message>
    </context>
    <context>
        <name>System user successfully logged to the system.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="19"/>
            <source>System user successfully logged to the system.</source>
            <translation>系统用户成功登录系统。</translation>
        </message>
    </context>
    <context>
        <name>Buffer overflow attack on rpc.statd</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="29"/>
            <source>Buffer overflow attack on rpc.statd</source>
            <translation>rpc.statd 上的缓冲区溢出攻击</translation>
        </message>
    </context>
    <context>
        <name>Buffer overflow on WU-FTPD versions prior to 2.6</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="38"/>
            <source>Buffer overflow on WU-FTPD versions prior to 2.6</source>
            <translation>2.6 之前的 WU-FTPD 版本上的缓冲区溢出</translation>
        </message>
    </context>
    <context>
        <name>Possible buffer overflow attempt.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="46"/>
            <source>Possible buffer overflow attempt.</source>
            <translation>可能的缓冲区溢出尝试。</translation>
        </message>
    </context>
    <context>
        <name>"Null" user changed some information.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="54"/>
            <source>"Null" user changed some information.</source>
            <translation>“Null”用户更改了一些信息。</translation>
        </message>
    </context>
    <context>
        <name>Buffer overflow attempt (probably on yppasswd).</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="62"/>
            <source>Buffer overflow attempt (probably on yppasswd).</source>
            <translation>缓冲区溢出尝试（可能在 yppasswd 上）。</translation>
        </message>
    </context>
    <context>
        <name>Heap overflow in the Solaris cachefsd service.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="70"/>
            <source>Heap overflow in the Solaris cachefsd service.</source>
            <translation>Solaris cachefsd 服务中的堆溢出。</translation>
        </message>
    </context>
    <context>
        <name>Stack overflow attempt or program exiting with SEGV (Solaris).</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="78"/>
            <source>Stack overflow attempt or program exiting with SEGV (Solaris).</source>
            <translation>堆栈溢出尝试或程序退出 SEGV （Solaris）。</translation>
        </message>
    </context>
    <context>
        <name>Multiple authentication failures.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="90"/>
            <source>Multiple authentication failures.</source>
            <translation>多次身份验证失败。</translation>
        </message>
    </context>
    <context>
        <name>Multiple authentication failures followed by a success.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="101"/>
            <source>Multiple authentication failures followed by a success.</source>
            <translation>多次身份验证失败，然后成功。</translation>
        </message>
    </context>
    <context>
        <name>Multiple viruses detected - Possible outbreak.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="110"/>
            <source>Multiple viruses detected - Possible outbreak.</source>
            <translation>检测到多种病毒 - 可能爆发。</translation>
        </message>
    </context>
    <context>
        <name>Attacks followed by the addition of an user.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="122"/>
            <source>Attacks followed by the addition of an user.</source>
            <translation>来自其他用户的攻击。</translation>
        </message>
    </context>
    <context>
        <name>Network scan from same source ip.</name>
        <message>
            <location filename="../po/0280-attack_rules.xml" line="132"/>
            <source>Network scan from same source ip.</source>
            <translation>从同一源 IP 进行网络扫描。</translation>
        </message>
    </context>
</TS>
