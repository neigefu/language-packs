<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="557"/>
        <source>Filesafe</source>
        <translation>ཡིག་ཚགས་ཀྱི་བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="750"/>
        <location filename="filesafe-vfs-file.cpp" line="927"/>
        <location filename="filesafe-vfs-file.cpp" line="1139"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཆའི་དཀར་ཆག་གིས་འགུལ་སྐྱོད་དང་འདྲ་བཟོའི་བྱ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="957"/>
        <source>Virtual file directories do not support make symbolic link operations</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཆའི་དཀར་ཆག་ལ་རྒྱབ་སྐྱོར་མི་བྱེད་པར་མཚོན་རྟགས་རང་བཞིན་གྱི་སྦྲེལ་མཐུད་</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="1190"/>
        <source>The virtual file system cannot be opened</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཚགས་མ་ལག་གི་ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-plugin.h" line="41"/>
        <source>File-safe vfs of peony</source>
        <translation>ཡིག་ཆ་བདེ་འཇགས་ཡིན་པའི་མེ་ཏོག་གི་མེ་ཏོག</translation>
    </message>
</context>
</TS>
