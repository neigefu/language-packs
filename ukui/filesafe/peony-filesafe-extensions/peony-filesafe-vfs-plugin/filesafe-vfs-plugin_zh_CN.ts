<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Peony::FileSafeVfsRegister</name>
    <message>
        <source>filesafe</source>
        <translation type="vanished">文件保护箱</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>filesafe</source>
        <translation type="vanished">保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="557"/>
        <source>Filesafe</source>
        <translation>保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="957"/>
        <source>Virtual file directories do not support make symbolic link operations</source>
        <translation>虚拟文件目录不支持生成符号链接操作</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="750"/>
        <location filename="filesafe-vfs-file.cpp" line="927"/>
        <location filename="filesafe-vfs-file.cpp" line="1139"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>虚拟文件目录不支持移动和复制操作</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="1190"/>
        <source>The virtual file system cannot be opened</source>
        <translation>无法打开虚拟文件系统</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-plugin.h" line="41"/>
        <source>File-safe vfs of peony</source>
        <translation>文件保护箱虚拟文件系统插件</translation>
    </message>
</context>
</TS>
