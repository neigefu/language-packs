<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>QObject</name>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="557"/>
        <source>Filesafe</source>
        <translation>保護箱</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="750"/>
        <location filename="filesafe-vfs-file.cpp" line="927"/>
        <location filename="filesafe-vfs-file.cpp" line="1139"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>虛擬檔案目錄不支持移動和複製操作</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="957"/>
        <source>Virtual file directories do not support make symbolic link operations</source>
        <translation>虛擬檔案目錄不支持生成符號連結操作</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="1190"/>
        <source>The virtual file system cannot be opened</source>
        <translation>無法打開虛擬檔案系統</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-plugin.h" line="41"/>
        <source>File-safe vfs of peony</source>
        <translation>檔案保護箱虛擬檔案系統挿件</translation>
    </message>
</context>
</TS>
