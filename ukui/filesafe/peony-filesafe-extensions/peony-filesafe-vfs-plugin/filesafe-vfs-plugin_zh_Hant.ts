<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Peony::FileSafeVfsRegister</name>
    <message>
        <source>filesafe</source>
        <translation type="vanished">文件保护箱</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>filesafe</source>
        <translation type="vanished">保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="548"/>
        <source>Filesafe</source>
        <translation>檔安全</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="980"/>
        <source>Virtual file directories do not support make symbolic link operations</source>
        <translation>虛擬檔案目錄不支援進行符號連結操作</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="747"/>
        <location filename="filesafe-vfs-file.cpp" line="947"/>
        <location filename="filesafe-vfs-file.cpp" line="1182"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>虛擬檔案目錄不支援移動和複製操作</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="1244"/>
        <source>The virtual file system cannot be opened</source>
        <translation>無法打開虛擬檔案系統</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-plugin.h" line="41"/>
        <source>File-safe vfs of peony</source>
        <translation>牡丹的檔安全vfs</translation>
    </message>
</context>
</TS>
