<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>QObject</name>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="557"/>
        <source>Filesafe</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠶᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="750"/>
        <location filename="filesafe-vfs-file.cpp" line="927"/>
        <location filename="filesafe-vfs-file.cpp" line="1139"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>ᠬᠡᠶᠢᠰᠪᠦᠷᠢ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ&#x202f;ᠤᠨ ᠭᠠᠷᠴᠠᠭ ᠰᠢᠯᠵᠢᠭᠦᠯᠬᠦ ᠪᠠ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ&#x202f;ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="957"/>
        <source>Virtual file directories do not support make symbolic link operations</source>
        <translation>ᠬᠡᠶᠢᠰᠪᠦᠷᠢ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ&#x202f;ᠤᠨ ᠭᠠᠷᠴᠠᠭ ᠦᠯᠦ ᠳᠡᠮᠵᠢᠬᠦ ᠡᠭᠦᠰᠦᠮᠡᠯ ᠳᠡᠮᠳᠡᠭ&#x202f;ᠦᠨ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="1190"/>
        <source>The virtual file system cannot be opened</source>
        <translation>ᠬᠡᠶᠢᠰᠪᠦᠷᠢ ᠹᠠᠢᠢᠯ&#x202f;ᠤᠨ ᠰᠢᠰᠲ᠋ᠧᠮ&#x202f;ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ&#x202f;ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-plugin.h" line="41"/>
        <source>File-safe vfs of peony</source>
        <translation>ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠶᠢᠷᠴᠠᠭ&#x202f;ᠤᠨ ᠬᠡᠶᠢᠰᠪᠦᠷᠢ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ&#x202f;ᠤᠨ ᠰᠢᠰᠲ᠋ᠧᠮ&#x202f;ᠦᠨ ᠤᠭᠯᠠᠭᠤᠷᠭ᠎ᠠ ᠳᠤᠨᠤᠭ</translation>
    </message>
</context>
</TS>
