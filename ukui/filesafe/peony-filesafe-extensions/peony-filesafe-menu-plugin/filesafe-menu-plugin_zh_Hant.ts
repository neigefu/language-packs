<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Peony::FilesafeMenuPlugin</name>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="75"/>
        <source>Create New FileSafe</source>
        <translation>創建新的檔保險箱</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="107"/>
        <source>Password Setting</source>
        <translation>密碼設置</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="130"/>
        <source>FileSafe Lock</source>
        <translation>檔安全鎖</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="156"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="182"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="21"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>牡丹-Qt檔安全功能表擴展</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="22"/>
        <source>filesafe Menu Extension.</source>
        <translation>檔安全功能表擴展。</translation>
    </message>
</context>
</TS>
