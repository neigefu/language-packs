<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>Peony::FilesafeMenuPlugin</name>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="77"/>
        <source>Create New FileSafe</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠪᠠᠶᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="100"/>
        <source>导入保护箱</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠲᠤ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="102"/>
        <source>Import New Filesafe</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠢ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="133"/>
        <source>设置密码</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="135"/>
        <source>Create Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="162"/>
        <source>Password Setting</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠵᠠᠰᠠᠬᠤ
</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="185"/>
        <source>FileSafe Lock</source>
        <translation>ᠣᠨᠢᠰᠤᠯᠠᠬᠤ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠠᠶᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="210"/>
        <location filename="filesafe-menu-plugin.cpp" line="239"/>
        <source>导出保护箱</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠠᠢ᠌ᠷᠴᠠᠭ ᠢ ᠬᠥᠲᠥᠯᠥᠨ ᠭᠠᠷᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="212"/>
        <location filename="filesafe-menu-plugin.cpp" line="241"/>
        <source>Export Box</source>
        <translation>ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠬᠥᠲᠥᠯᠥᠨ ᠭᠠᠷᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="267"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="293"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="21"/>
        <source>Peony-Qt Filesafe Menu Extension</source>
        <oldsource>Peony-Qt filesafe menu Extension</oldsource>
        <translation>ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠤᠨ ᠥᠷᠭᠡᠳᠬᠡᠯ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="22"/>
        <source>Filesafe Menu Extension</source>
        <oldsource>filesafe Menu Extension</oldsource>
        <translation>ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠤᠨ ᠲᠣᠪᠶᠣᠭ ᠤᠨ ᠤᠭᠯᠠᠭᠤᠷᠭ᠎ᠠ ᠳᠤᠨᠤᠭ</translation>
    </message>
</context>
</TS>
