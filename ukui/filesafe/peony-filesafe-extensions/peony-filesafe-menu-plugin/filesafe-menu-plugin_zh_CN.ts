<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Peony::FilesafeMenuPlugin</name>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="77"/>
        <source>Create New FileSafe</source>
        <translation>创建保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="100"/>
        <source>导入保护箱</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="102"/>
        <source>Import New Filesafe</source>
        <translation type="unfinished">导入保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="133"/>
        <source>设置密码</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="135"/>
        <source>Create Password</source>
        <translation type="unfinished">设置密码</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="162"/>
        <source>Password Setting</source>
        <translation>修改保护箱密码</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="185"/>
        <source>FileSafe Lock</source>
        <translation>锁定保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="210"/>
        <location filename="filesafe-menu-plugin.cpp" line="239"/>
        <source>导出保护箱</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="212"/>
        <location filename="filesafe-menu-plugin.cpp" line="241"/>
        <source>Export Box</source>
        <translation type="unfinished">导出保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="267"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="293"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="21"/>
        <source>Peony-Qt Filesafe Menu Extension</source>
        <oldsource>Peony-Qt filesafe menu Extension</oldsource>
        <translation type="unfinished">文件保护箱扩展</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="22"/>
        <source>Filesafe Menu Extension</source>
        <oldsource>filesafe Menu Extension</oldsource>
        <translation type="unfinished">文件保护箱菜单插件</translation>
    </message>
</context>
</TS>
