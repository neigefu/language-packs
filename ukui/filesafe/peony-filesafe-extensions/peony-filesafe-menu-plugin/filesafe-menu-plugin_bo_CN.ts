<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::FilesafeMenuPlugin</name>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="77"/>
        <source>Create New FileSafe</source>
        <translation>ཡིག་ཆ་གསར་པ་གསར་སྐྲུན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="100"/>
        <source>导入保护箱</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་ནང་དུ་འདྲེན་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="102"/>
        <source>Import New Filesafe</source>
        <translation>ཡིག་ཆ་གསར་པ་བདེ་འཇགས་ཡོང་བར་ཁྲིད་སྟོན་</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="133"/>
        <source>设置密码</source>
        <translation>གསང་བ་འགོད་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="135"/>
        <source>Create Password</source>
        <translation>གསང་བའི་ཨང་གྲངས་གསར་འཛུགས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="162"/>
        <source>Password Setting</source>
        <translation>གསང་གྲངས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="185"/>
        <source>FileSafe Lock</source>
        <translation>ཡིག་ཆ་བདེ་འཇགས་ཀྱི་ཟྭ་</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="210"/>
        <location filename="filesafe-menu-plugin.cpp" line="239"/>
        <source>导出保护箱</source>
        <translation>སྲུང་སྒམ་ཁྲིད་སྟོན་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="212"/>
        <location filename="filesafe-menu-plugin.cpp" line="241"/>
        <source>Export Box</source>
        <translation>འཆིང་རྒྱ་འདོན་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="267"/>
        <source>Rename</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="293"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="21"/>
        <source>Peony-Qt Filesafe Menu Extension</source>
        <oldsource>Peony-Qt filesafe menu Extension</oldsource>
        <translation>ཡིག་ཆ་སྲུང་སྐྱོབ་སྒམ་རྒྱ་བསྐྱེད་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="22"/>
        <source>Filesafe Menu Extension</source>
        <oldsource>filesafe Menu Extension</oldsource>
        <translation>ཡིག་ཆ་སྲུང་སྐྱོབ་སྒམ་གྱི་ཟས་ཐོའི་སྟེང་དུ་ལྷུ་ལག་བས</translation>
    </message>
</context>
</TS>
