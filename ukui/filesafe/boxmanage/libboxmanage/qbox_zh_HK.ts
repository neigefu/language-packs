<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>BioProxy</name>
    <message>
        <location filename="../BioProxy.cpp" line="224"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="226"/>
        <source>FingerVein</source>
        <translation>手指靜脈</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="228"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="230"/>
        <source>Face</source>
        <translation>臉</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="232"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="234"/>
        <source>QRCode</source>
        <translation>二維碼</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="245"/>
        <source>Unplugging of %1 device detected</source>
        <translation>檢測到 %1 設備的拔出</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="251"/>
        <source>%1 device insertion detected</source>
        <translation>檢測到 %1 設備插入</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="261"/>
        <source>ukui-biometric-manager</source>
        <translation>ukui-生物識別管理器</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="264"/>
        <source>biometric</source>
        <translation>生物</translation>
    </message>
</context>
<context>
    <name>BioWidget</name>
    <message>
        <location filename="../BioWidget.cpp" line="57"/>
        <source>The login options</source>
        <translation>登錄選項</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="288"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="291"/>
        <source>FingerVein</source>
        <translation>手指靜脈</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="294"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="297"/>
        <source>Face</source>
        <translation>臉</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="300"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="303"/>
        <source>QRCode</source>
        <translation>二維碼</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="332"/>
        <location filename="../BioWidget.cpp" line="337"/>
        <location filename="../BioWidget.cpp" line="342"/>
        <location filename="../BioWidget.cpp" line="348"/>
        <location filename="../BioWidget.cpp" line="354"/>
        <source>Verify %1</source>
        <translation>驗證 %1</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="359"/>
        <source>WeChat scanning code</source>
        <translation>微信掃碼</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="369"/>
        <source> or enter password to unlock</source>
        <translation> 或輸入密碼解鎖</translation>
    </message>
</context>
<context>
    <name>BoxCreateDialog</name>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="37"/>
        <source>Create</source>
        <translation>創造</translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="vanished">新建保护箱</translation>
    </message>
    <message>
        <source>Encrypt       </source>
        <oldsource>Encrypt     </oldsource>
        <translation type="obsolete">加密       </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="115"/>
        <source>Name          </source>
        <translation>名字          </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="116"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="147"/>
        <source>At least %1 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="166"/>
        <location filename="../BoxCreateDialog.cpp" line="168"/>
        <location filename="../BoxCreateDialog.cpp" line="171"/>
        <location filename="../BoxCreateDialog.cpp" line="525"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <source>New password length cannot less than 8</source>
        <translation type="obsolete">新密碼長度不能小於8</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="599"/>
        <source>Failed to get pwquality conf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancle</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="117"/>
        <source>Confirm </source>
        <translation>確認 </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="167"/>
        <location filename="../BoxCreateDialog.cpp" line="169"/>
        <location filename="../BoxCreateDialog.cpp" line="172"/>
        <location filename="../BoxCreateDialog.cpp" line="526"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Passwd level</source>
        <translation type="vanished">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="366"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation>框名稱不能為空</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="381"/>
        <location filename="../BoxCreateDialog.cpp" line="572"/>
        <source>Box name has been exist</source>
        <oldsource>Box name has been exist!</oldsource>
        <translation>框名稱已退出</translation>
    </message>
    <message>
        <source>Create box is failed</source>
        <oldsource>Create box is failed!</oldsource>
        <translation type="obsolete">保护箱创建失败</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="374"/>
        <source>Password cannot be empty</source>
        <oldsource>Password cannot be empty!</oldsource>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="vanished">至少8位數位，包括大寫和小寫字母、數位和特殊符號 </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="142"/>
        <source>Unlock password</source>
        <translation>解鎖密碼</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="219"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation>將金鑰同步到 Kylin Cloud 帳戶</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="318"/>
        <location filename="../BoxCreateDialog.cpp" line="319"/>
        <location filename="../BoxCreateDialog.cpp" line="332"/>
        <location filename="../BoxCreateDialog.cpp" line="333"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <source>Password length can not be less than 6</source>
        <oldsource>Passwd length can not be less than 6</oldsource>
        <translation type="obsolete">密码长度不可低于6位</translation>
    </message>
    <message>
        <source>Password length can not be less than 8</source>
        <translation type="vanished">密碼長度不能小於8</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="393"/>
        <location filename="../BoxCreateDialog.cpp" line="589"/>
        <source>Password can not contain box name</source>
        <translation>密碼不能包含框名稱</translation>
    </message>
    <message>
        <source>Password  can not contain user name</source>
        <translation type="vanished">密碼不能包含使用者名</translation>
    </message>
    <message>
        <source>Password characters must contain at least two types</source>
        <translation type="vanished">密碼字元必須至少包含兩種類型</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="400"/>
        <location filename="../BoxCreateDialog.cpp" line="703"/>
        <source>Confirm password cannot be empty</source>
        <translation>確認密碼不能為空</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="407"/>
        <location filename="../BoxCreateDialog.cpp" line="710"/>
        <source>Password is not same as verify password</source>
        <oldsource>Password is not same as verify password!</oldsource>
        <translation>密碼與驗證密碼不同</translation>
    </message>
    <message>
        <source>Create globalKey failed</source>
        <oldsource>Create globalKey failed!</oldsource>
        <translation type="vanished">創建 globalKey 失敗</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="501"/>
        <location filename="../BoxCreateDialog.cpp" line="544"/>
        <location filename="../BoxCreateDialog.cpp" line="551"/>
        <location filename="../BoxCreateDialog.cpp" line="558"/>
        <location filename="../BoxCreateDialog.cpp" line="565"/>
        <source>Invaild name</source>
        <translation>Invaild 名稱</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="obsolete">密码长度不符合要求</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="obsolete">密码包含字符种类不符合要求</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="664"/>
        <location filename="../BoxCreateDialog.cpp" line="665"/>
        <source>Invaild password</source>
        <translation>Invaild 密碼</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
    <message>
        <source>Passwd level low</source>
        <translation type="vanished">密码强度低</translation>
    </message>
    <message>
        <source>Passwd level mid</source>
        <translation type="vanished">密码强度中</translation>
    </message>
    <message>
        <source>Passwd level high</source>
        <translation type="vanished">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxItemDelegate</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
</context>
<context>
    <name>BoxKeyExportDialog</name>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="112"/>
        <source>Create Key</source>
        <translation>創建金鑰</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="118"/>
        <source>Select key save path</source>
        <translation>選擇金鑰保存路徑</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="125"/>
        <source>If forget password for the box, you can use the key to reset the password</source>
        <translation>如果忘記了包裝盒的密碼，您可以使用密鑰重置密碼</translation>
    </message>
    <message>
        <source>Save Mode</source>
        <translation type="obsolete">保存方式</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="130"/>
        <source>Save Path</source>
        <translation>保存路徑</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">默认存储路径</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation type="obsolete">自定义存储路径</translation>
    </message>
    <message>
        <source>Please select key save path</source>
        <translation type="obsolete">选择密钥保存路径</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="137"/>
        <location filename="../BoxKeyExportDialog.cpp" line="159"/>
        <location filename="../BoxKeyExportDialog.cpp" line="323"/>
        <source>Browser</source>
        <translation>瀏覽器</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="148"/>
        <location filename="../BoxKeyExportDialog.cpp" line="157"/>
        <location filename="../BoxKeyExportDialog.cpp" line="321"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="153"/>
        <location filename="../BoxKeyExportDialog.cpp" line="158"/>
        <location filename="../BoxKeyExportDialog.cpp" line="228"/>
        <location filename="../BoxKeyExportDialog.cpp" line="322"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Save path is empty</source>
        <translation type="obsolete">存储路径为空</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="195"/>
        <location filename="../BoxKeyExportDialog.cpp" line="204"/>
        <source>Key file cannot write</source>
        <translation>金鑰檔案無法寫入</translation>
    </message>
    <message>
        <source>Create globalKey failed</source>
        <translation type="obsolete">创建全局密钥失败</translation>
    </message>
    <message>
        <source>Open key file failed</source>
        <translation type="obsolete">存储密钥文件失败</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="82"/>
        <location filename="../BoxKeyExportDialog.cpp" line="84"/>
        <source>Transparent Box</source>
        <translation>透明盒</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="82"/>
        <location filename="../BoxKeyExportDialog.cpp" line="84"/>
        <source>Encrypt Box</source>
        <translation>加密盒</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="93"/>
        <source>File Safe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="134"/>
        <source>Use default key path</source>
        <translation>使用預設金鑰路徑</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="218"/>
        <source>save key file</source>
        <translation>儲存金鑰檔案</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="225"/>
        <source>FileName(N):</source>
        <translation>檔案名（N）：</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="226"/>
        <source>FileType:</source>
        <translation>檔案類型：</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="227"/>
        <source>Save(S)</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>對話</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="82"/>
        <source>选择密钥保存路径</source>
        <translation>選擇金鑰保存路徑</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="107"/>
        <source>如果忘记保护箱密码，可以使用密钥重置保护箱密码</source>
        <translation>如果忘記保護箱密碼，可以使用密鑰重置保護箱密碼</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="143"/>
        <source>保存路径</source>
        <translation type="unfinished">保存路徑</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="169"/>
        <source>无效密钥路径</source>
        <translation>無效金鑰路徑</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="218"/>
        <source>浏览</source>
        <translation>流覽</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="281"/>
        <source>取消</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="300"/>
        <source>确认</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>BoxLoadingMessageBox</name>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="32"/>
        <source>File Safe</source>
        <translation type="unfinished">文件保護箱</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="80"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="81"/>
        <source>View</source>
        <translation>視圖</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="93"/>
        <source>Export succeed!</source>
        <translation>出口成功！</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="99"/>
        <source>Import succeed!</source>
        <translation>匯入成功！</translation>
    </message>
    <message>
        <source>Export failed! The path you have selected does not have permission</source>
        <translation type="vanished">匯出失敗！您選擇的路徑沒有許可權</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="134"/>
        <source>Import failed! The selected file is not file safe box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="117"/>
        <source>Export failed!</source>
        <translation>导出失败</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="122"/>
        <source>Import failed!</source>
        <translation>匯入失敗！</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="75"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="76"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>BoxMessageDialog</name>
    <message>
        <source>Password setting is successful!</source>
        <translation type="obsolete">密码修改成功！</translation>
    </message>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="25"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="29"/>
        <source>File Safe</source>
        <translation>檔安全</translation>
    </message>
</context>
<context>
    <name>BoxOccupiedTipDialog</name>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="20"/>
        <source>Lock</source>
        <translation type="unfinished">鎖</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="34"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="141"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="142"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation type="unfinished">檔保險箱中有檔被佔用，需要解鎖才能鎖定</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="42"/>
        <source>Rename</source>
        <translation type="unfinished">重新命名</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="160"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="306"/>
        <source>Files being occupied (%1)</source>
        <translation type="unfinished">佔用的檔案（%1）</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="35"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="166"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="167"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="170"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="173"/>
        <source>Mandatory lock</source>
        <translation type="unfinished">強制鎖定</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="36"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="86"/>
        <source>Lock box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="40"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="41"/>
        <source>Mandatory rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="45"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="46"/>
        <source>Mandatory delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="47"/>
        <source>Delete Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="168"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="171"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="174"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="296"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="169"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="172"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="175"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="276"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="277"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="278"/>
        <source>Hide</source>
        <translation type="unfinished">隱藏</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="269"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="270"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="271"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="316"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxPasswdSetting</name>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="46"/>
        <location filename="../BoxPasswdSetting.cpp" line="116"/>
        <source>Password setting</source>
        <translation>密碼設置</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="205"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="206"/>
        <location filename="../BoxPasswdSetting.cpp" line="449"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="207"/>
        <source>New Password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="BoxPasswdSetting.cpp" line="178"/>
        <source></source>
        <oldsource>Confirm password</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="244"/>
        <location filename="../BoxPasswdSetting.cpp" line="246"/>
        <location filename="../BoxPasswdSetting.cpp" line="248"/>
        <location filename="../BoxPasswdSetting.cpp" line="1050"/>
        <location filename="../BoxPasswdSetting.cpp" line="1362"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Please update the psw</source>
        <translation type="vanished">请导入密钥文件</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">上传</translation>
    </message>
    <message>
        <source>Psw</source>
        <translation type="vanished">密钥</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="869"/>
        <location filename="../BoxPasswdSetting.cpp" line="870"/>
        <location filename="../BoxPasswdSetting.cpp" line="1156"/>
        <location filename="../BoxPasswdSetting.cpp" line="1158"/>
        <source>Box umount failed</source>
        <translation>Box umount 失敗</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="208"/>
        <source>Confirm     </source>
        <translation>確認     </translation>
    </message>
    <message>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="vanished">至少8位數位，包括大寫和小寫字母、數位和特殊符號 </translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">路径类型</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">默认存储路径</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation type="obsolete">自定义存储路径</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="243"/>
        <location filename="../BoxPasswdSetting.cpp" line="245"/>
        <location filename="../BoxPasswdSetting.cpp" line="247"/>
        <location filename="../BoxPasswdSetting.cpp" line="1361"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="267"/>
        <location filename="../BoxPasswdSetting.cpp" line="268"/>
        <location filename="../BoxPasswdSetting.cpp" line="456"/>
        <location filename="../BoxPasswdSetting.cpp" line="457"/>
        <location filename="../BoxPasswdSetting.cpp" line="483"/>
        <location filename="../BoxPasswdSetting.cpp" line="484"/>
        <source>Use default key path</source>
        <translation>使用預設金鑰路徑</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="648"/>
        <location filename="../BoxPasswdSetting.cpp" line="649"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <source>Key file permission denied</source>
        <translation type="vanished">金鑰檔案權限被拒絕</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1338"/>
        <source>File Safe</source>
        <translation>檔安全</translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="obsolete">请导入密钥文件</translation>
    </message>
    <message>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation type="vanished">%1验证不通过，您还有%2次尝试机会</translation>
    </message>
    <message>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation type="vanished">无法验证%1，请输入密码解锁</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="854"/>
        <location filename="../BoxPasswdSetting.cpp" line="855"/>
        <source>Password can not be empty</source>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <source>Password authentication failed</source>
        <translation type="vanished">密码认证失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="278"/>
        <source>Import</source>
        <translation>進口</translation>
    </message>
    <message>
        <source>Password level</source>
        <oldsource>Passwd level</oldsource>
        <translation type="obsolete">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="478"/>
        <source>Secret key</source>
        <translation>金鑰</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="893"/>
        <location filename="../BoxPasswdSetting.cpp" line="904"/>
        <location filename="../BoxPasswdSetting.cpp" line="905"/>
        <source>Password is error</source>
        <oldsource>Passwd is error</oldsource>
        <translation>密碼錯誤</translation>
    </message>
    <message>
        <source>New password can not be empty</source>
        <oldsource>New passwd can not be empty</oldsource>
        <translation type="obsolete">新密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="915"/>
        <location filename="../BoxPasswdSetting.cpp" line="916"/>
        <location filename="../BoxPasswdSetting.cpp" line="1265"/>
        <location filename="../BoxPasswdSetting.cpp" line="1266"/>
        <source>New password cannot be same as old password</source>
        <oldsource>New passwd cannot be same as old passwd</oldsource>
        <translation>新密碼不能與舊密碼相同</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1238"/>
        <location filename="../BoxPasswdSetting.cpp" line="1240"/>
        <source>New password cannot be empty</source>
        <oldsource>New passwd cannot be empty</oldsource>
        <translation>新密碼不能為空</translation>
    </message>
    <message>
        <source>New password length cannot less than 8</source>
        <oldsource>New passwd length cannot less than 8</oldsource>
        <translation type="vanished">新密碼長度不能小於8</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="925"/>
        <location filename="../BoxPasswdSetting.cpp" line="926"/>
        <location filename="../BoxPasswdSetting.cpp" line="1276"/>
        <location filename="../BoxPasswdSetting.cpp" line="1278"/>
        <source>Verify password length cannot be empty</source>
        <oldsource>Verify passwd length cannot be empty</oldsource>
        <translation>驗證密碼長度不能為空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="935"/>
        <location filename="../BoxPasswdSetting.cpp" line="936"/>
        <location filename="../BoxPasswdSetting.cpp" line="1287"/>
        <location filename="../BoxPasswdSetting.cpp" line="1289"/>
        <source>Verify password is not same as new password</source>
        <oldsource>Verify passwd is not same as new passwd</oldsource>
        <translation>驗證密碼是否與新密碼相同</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="657"/>
        <source>Password can not contain box name</source>
        <translation>密碼不能包含框名稱</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="131"/>
        <source>At least %1 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="270"/>
        <location filename="../BoxPasswdSetting.cpp" line="271"/>
        <location filename="../BoxPasswdSetting.cpp" line="459"/>
        <location filename="../BoxPasswdSetting.cpp" line="460"/>
        <location filename="../BoxPasswdSetting.cpp" line="486"/>
        <location filename="../BoxPasswdSetting.cpp" line="487"/>
        <source>Please select the key path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="632"/>
        <location filename="../BoxPasswdSetting.cpp" line="633"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password  can not contain user name</source>
        <translation type="vanished">密碼不能包含使用者名</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="681"/>
        <source>Failed to get pwquality conf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="691"/>
        <location filename="../BoxPasswdSetting.cpp" line="1249"/>
        <location filename="../BoxPasswdSetting.cpp" line="1251"/>
        <source>New password length cannot less than %1</source>
        <translation type="unfinished">新密碼長度至少%1位</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="952"/>
        <location filename="../BoxPasswdSetting.cpp" line="954"/>
        <source>Password setting is failed</source>
        <translation>密碼設置失敗</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="970"/>
        <location filename="../BoxPasswdSetting.cpp" line="972"/>
        <location filename="../BoxPasswdSetting.cpp" line="996"/>
        <location filename="../BoxPasswdSetting.cpp" line="998"/>
        <source>Mount is failed</source>
        <translation>掛載失敗</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1038"/>
        <source>text file (*.txt)</source>
        <translation>文字檔 （*.txt）</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1039"/>
        <source>all files (*)</source>
        <translation>所有檔案 （*）</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1047"/>
        <source>FileName(N):</source>
        <translation>檔案名（N）：</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1048"/>
        <source>FileType:</source>
        <translation>檔案類型：</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1049"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1051"/>
        <source>Look in:</source>
        <translation>䀷：</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1176"/>
        <location filename="../BoxPasswdSetting.cpp" line="1177"/>
        <source>The secret key file cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1361"/>
        <source> (O)</source>
        <translation> （O）</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1362"/>
        <source> (C)</source>
        <translation> （三）</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="vanished">密碼長度不符合要求</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="vanished">密碼包含不符合要求的字元</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Password setting is failed!</source>
        <oldsource>Passwd setting is failed!</oldsource>
        <translation type="obsolete">密码设置失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1035"/>
        <source>chose your file </source>
        <translation>選擇您的檔案 </translation>
    </message>
    <message>
        <source>text file (*.txt);; all files (*);; </source>
        <oldsource>text file (*.xls *.txt);; all files (*.*);; </oldsource>
        <translation type="obsolete">文本文件(*.txt);; 所有文件 (*);;</translation>
    </message>
    <message>
        <source>The secret key file path can not be empty</source>
        <translation type="obsolete">密钥路径不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1188"/>
        <location filename="../BoxPasswdSetting.cpp" line="1189"/>
        <source>The secret key file is not exit</source>
        <translation>金鑰檔未退出</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1199"/>
        <location filename="../BoxPasswdSetting.cpp" line="1200"/>
        <source>The secret key file is unreadable</source>
        <translation>金鑰檔案不可讀</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1228"/>
        <location filename="../BoxPasswdSetting.cpp" line="1229"/>
        <source>The secret key file is wrong</source>
        <translation>金鑰檔案錯誤</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1305"/>
        <location filename="../BoxPasswdSetting.cpp" line="1307"/>
        <source>Set password by secret key file failed</source>
        <translation>通過金鑰檔設定密碼失敗</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1334"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1337"/>
        <source>Password setting is successful!</source>
        <translation>密碼設置成功！</translation>
    </message>
    <message>
        <source>Invaild name</source>
        <translation type="vanished">不允许的特殊字符</translation>
    </message>
    <message>
        <source>Invaild password</source>
        <translation type="vanished">Invaild 密碼</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxRenameDialog</name>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="40"/>
        <location filename="../BoxRenameDialog.cpp" line="57"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="121"/>
        <location filename="../BoxRenameDialog.cpp" line="123"/>
        <location filename="../BoxRenameDialog.cpp" line="126"/>
        <location filename="../BoxRenameDialog.cpp" line="508"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="122"/>
        <location filename="../BoxRenameDialog.cpp" line="124"/>
        <location filename="../BoxRenameDialog.cpp" line="127"/>
        <location filename="../BoxRenameDialog.cpp" line="509"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="146"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="147"/>
        <source>New Name</source>
        <translation>新名稱</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="148"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="230"/>
        <location filename="../BoxRenameDialog.cpp" line="231"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="283"/>
        <location filename="../BoxRenameDialog.cpp" line="284"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation>框名稱不能為空</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="329"/>
        <source>Password is error</source>
        <translation type="unfinished">密码错误</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="352"/>
        <location filename="../BoxRenameDialog.cpp" line="353"/>
        <source>File safe rename failed</source>
        <translation>檔安全重命名失敗</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="369"/>
        <location filename="../BoxRenameDialog.cpp" line="370"/>
        <source>Mount is failed</source>
        <translation>掛載失敗</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="390"/>
        <location filename="../BoxRenameDialog.cpp" line="391"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwd cannot be empty!</source>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="429"/>
        <location filename="../BoxRenameDialog.cpp" line="430"/>
        <source>The new name cannot be the same as the original name</source>
        <translation>新名稱不能與原始名稱相同</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="440"/>
        <location filename="../BoxRenameDialog.cpp" line="441"/>
        <source>File Safe already exists</source>
        <translation>檔安全已存在</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="307"/>
        <location filename="../BoxRenameDialog.cpp" line="308"/>
        <source>Box umount failed</source>
        <translation>Box umount 失敗</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="294"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <source>File safe rename failed!</source>
        <translation type="obsolete">重命名保护箱失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="413"/>
        <location filename="../BoxRenameDialog.cpp" line="414"/>
        <location filename="../BoxRenameDialog.cpp" line="421"/>
        <location filename="../BoxRenameDialog.cpp" line="422"/>
        <source>Invaild name</source>
        <translation>Invaild 名稱</translation>
    </message>
</context>
<context>
    <name>BoxTableModel</name>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Create time</source>
        <translation type="vanished">创建时间</translation>
    </message>
    <message>
        <source>Encrypt Box</source>
        <translation type="obsolete">密钥加密保护箱</translation>
    </message>
    <message>
        <source>Transparent Box</source>
        <translation type="obsolete">透明加密保护箱</translation>
    </message>
</context>
<context>
    <name>BoxTaskProcessDialog</name>
    <message>
        <location filename="../BoxTaskProcessDialog.cpp" line="31"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTaskProcessDialog.cpp" line="81"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxTypeSelectDialog</name>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="29"/>
        <source>Create</source>
        <translation>創造</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="44"/>
        <source>Select box type:</source>
        <translation>選擇包裝盒類型：</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="62"/>
        <source>Encrypt Box</source>
        <translation>加密盒</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="89"/>
        <source>Transparent Box</source>
        <translation>透明盒</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="67"/>
        <source>Use user set password protection, the protection box can only be used after unlocking with the user set password</source>
        <translation>使用使用者設置的密碼保護，保護盒只有在解鎖后才能使用使用者設置的密碼</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="94"/>
        <source>Using system security password protection, automatically unlocking when users log in to the system</source>
        <translation>採用系統安全密碼保護，使用者登錄系統時自動解鎖</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="120"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="112"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation type="vanished">對話</translation>
    </message>
    <message>
        <source>请选择保护箱类型: </source>
        <translation type="vanished">請選擇保護箱類型： </translation>
    </message>
    <message>
        <source>加密保护箱</source>
        <translation type="vanished">加密保護箱</translation>
    </message>
    <message>
        <source>该操作将会导致计算机间断性宕机，宕机时间</source>
        <translation type="vanished">該操作將會導致計算機間斷性宕機，宕機時間</translation>
    </message>
    <message>
        <source>透明加密保护箱</source>
        <translation type="vanished">透明加密保護箱</translation>
    </message>
    <message>
        <source>取消</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>确认</source>
        <translation type="vanished">確認</translation>
    </message>
</context>
<context>
    <name>BuiltinBoxPasswdSetting</name>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="39"/>
        <source>Create Password</source>
        <translation>創建密碼</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="69"/>
        <source>Create</source>
        <translation>創造</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="134"/>
        <source>Name          </source>
        <translation>名字          </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="135"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="136"/>
        <source>Confirm </source>
        <translation>確認 </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="164"/>
        <source>Unlock password</source>
        <translation>解鎖密碼</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="166"/>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation>至少8位數位，包括大寫和小寫字母、數位和特殊符號 </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="180"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="182"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="185"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="575"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="181"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="183"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="186"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="576"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="230"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation>將金鑰同步到 Kylin Cloud 帳戶</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="300"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="301"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="314"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="315"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="344"/>
        <source>Password cannot be empty</source>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="352"/>
        <source>Password length can not be less than 8</source>
        <translation>密碼長度不能小於8</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="360"/>
        <source>Password can not contain box name</source>
        <translation>密碼不能包含框名稱</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="367"/>
        <source>Password  can not contain user name</source>
        <translation>密碼不能包含使用者名</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="379"/>
        <source>Password characters must contain at least two types</source>
        <translation>密碼字元必須至少包含兩種類型</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="386"/>
        <source>Confirm password cannot be empty</source>
        <translation>確認密碼不能為空</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="393"/>
        <source>Password is not same as verify password</source>
        <translation>密碼與驗證密碼不同</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="409"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="411"/>
        <source>Password setting is failed</source>
        <translation>密碼設置失敗</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="456"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="459"/>
        <source>Password setting is successful!</source>
        <translation>密碼設置成功！</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="460"/>
        <source>File Safe</source>
        <translation>檔安全</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="485"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="486"/>
        <source>Invaild password</source>
        <translation>Invaild 密碼</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="obsolete">密碼長度不符合要求</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="obsolete">密碼包含字符種類不符合要求</translation>
    </message>
</context>
<context>
    <name>CBoxCompatibilityUpgradeOperation</name>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Upgrade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Box upgrading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Box compatibility upgrading, no closing!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="36"/>
        <source>Box compatibility upgrade failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CBoxUnlockAuthDialog</name>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="67"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="69"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="588"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重新命名</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">默认存储路径</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation type="obsolete">自定义存储路径</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="143"/>
        <source>Import</source>
        <translation>進口</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="166"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="168"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="542"/>
        <source>Key Unlock</source>
        <translation>鑰匙解鎖</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="189"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="539"/>
        <source>Use password unlock &quot;%1&quot;</source>
        <translation>使用密碼解鎖“%1”</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="190"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="543"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">路径类型</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="135"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="136"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="529"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="530"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="546"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="547"/>
        <source>Use default key path</source>
        <translation>使用預設金鑰路徑</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="138"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="139"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="532"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="533"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="549"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="550"/>
        <source>Please select the key path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="215"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="217"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="219"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="512"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="216"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="218"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="220"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="513"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="589"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="309"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="366"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="367"/>
        <source>umount is error</source>
        <translation>umount 是錯誤的</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="380"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="381"/>
        <source>Password can not be empty</source>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="395"/>
        <source>Password is error</source>
        <translation>密碼錯誤</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="414"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="415"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="420"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="421"/>
        <source>Reset key can not be empty</source>
        <translation>複位鍵不能為空</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="430"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="431"/>
        <source>Invalid key file</source>
        <translation>金鑰檔案無效</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="447"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="448"/>
        <source>The secret key file is wrong</source>
        <translation>金鑰檔案錯誤</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="522"/>
        <source>Use key unlock &quot;%1&quot;</source>
        <translation>使用金鑰解鎖“%1”</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="525"/>
        <source>Password Unlock</source>
        <translation>密碼解鎖</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="526"/>
        <source>Key</source>
        <translation>鑰匙</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="574"/>
        <source>chose your file </source>
        <translation>選擇您的檔案 </translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="577"/>
        <source>text file (*.txt)</source>
        <translation>文字檔 （*.txt）</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="578"/>
        <source>all files (*)</source>
        <translation>所有檔案 （*）</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="586"/>
        <source>FileName(N):</source>
        <translation>檔案名（N）：</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="587"/>
        <source>FileType:</source>
        <translation>檔案類型：</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="590"/>
        <source>Look in:</source>
        <translation>䀷：</translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="obsolete">请导入密钥文件</translation>
    </message>
</context>
<context>
    <name>CCreateKeyOperInPeony</name>
    <message>
        <location filename="../CreateKeyOperInPeony.cpp" line="52"/>
        <source>Create global key failed</source>
        <translation type="unfinished">創建全域鍵失敗</translation>
    </message>
</context>
<context>
    <name>CDeleteBoxOprInPeony</name>
    <message>
        <location filename="../DeleteBoxOprInPeony.cpp" line="129"/>
        <source>Delete box failed</source>
        <translation type="unfinished">刪除框失敗</translation>
    </message>
</context>
<context>
    <name>CExportAuthCred</name>
    <message>
        <source>critical</source>
        <translation type="obsolete">危急</translation>
    </message>
    <message>
        <source>Export failed</source>
        <translation type="obsolete">匯出失敗</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="obsolete">資訊</translation>
    </message>
    <message>
        <source>Export succeed</source>
        <translation type="obsolete">匯出成功</translation>
    </message>
</context>
<context>
    <name>CImportBoxOprInPeony</name>
    <message>
        <source>critical</source>
        <translation type="obsolete">危急</translation>
    </message>
    <message>
        <source>Import failed</source>
        <translation type="obsolete">匯入失敗</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="obsolete">資訊</translation>
    </message>
    <message>
        <source>Import succeed</source>
        <translation type="obsolete">導入成功</translation>
    </message>
</context>
<context>
    <name>CTitleBar</name>
    <message>
        <location filename="../TitleBar.cpp" line="128"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="133"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="138"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="143"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>ExportBoxGetPwdDialog</name>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="52"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="89"/>
        <source>Export filesafe box</source>
        <translation>匯出檔安全框</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="103"/>
        <source>set %1 export passwd</source>
        <translation>設置 %1 匯出通道</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="107"/>
        <source>This password is used to encrypt exported files</source>
        <translation>此密碼用於加密導出的檔</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="158"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="160"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="163"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="345"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="376"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="380"/>
        <source>Export</source>
        <translation>出口</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="159"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="161"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="164"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="346"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="381"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="371"/>
        <source>save kybox file</source>
        <translation>保存 Kybox 檔</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="378"/>
        <source>FileName(N):</source>
        <translation>檔案名（N）：</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="379"/>
        <source>FileType:</source>
        <translation>檔案類型：</translation>
    </message>
    <message>
        <source>critical</source>
        <translation type="vanished">危急</translation>
    </message>
    <message>
        <source>Export failed</source>
        <translation type="vanished">匯出失敗</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="vanished">資訊</translation>
    </message>
    <message>
        <source>Export succeed</source>
        <translation type="vanished">匯出成功</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名称</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="185"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="256"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="257"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="315"/>
        <source> Export passwd cannot be empty</source>
        <translation> 匯出 passwd 不能為空</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="317"/>
        <source>Export passwd cannot be empty</source>
        <translation>匯出 passwd 不能為空</translation>
    </message>
</context>
<context>
    <name>ExportBoxLoadingDialog</name>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="41"/>
        <source>File Safe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="64"/>
        <source>Exporting, please wait...</source>
        <translation>匯出中，請稍候...</translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="116"/>
        <location filename="../ExportBoxLoadingDialog.cpp" line="117"/>
        <source>Exporting %1 to %2 </source>
        <translation>將 %1 匯出到 %2 </translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="126"/>
        <location filename="../ExportBoxLoadingDialog.cpp" line="127"/>
        <source>Importing %1 ...</source>
        <translation>匯入 %1 ...</translation>
    </message>
</context>
<context>
    <name>ExportPamAuthenticDialog</name>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="232"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="236"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="533"/>
        <source>User authentication is required to perform this operation</source>
        <translation>執行此操作需要使用者身份驗證</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="243"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="244"/>
        <source>Enter the user password to allow this operation</source>
        <translation>輸入用戶密碼以允許此操作</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="267"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="268"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="270"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="506"/>
        <source>Authenticate</source>
        <translation>證實</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="273"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="274"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="277"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="278"/>
        <source>Biometric authentication</source>
        <translation>生物認證</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="339"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="398"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation>%1 驗證失敗，您還有 %2 次嘗試</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="403"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation>無法驗證%1，請輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="484"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="485"/>
        <source>Password can not be empty</source>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="548"/>
        <source>Password authentication failed</source>
        <translation>密碼身份驗證失敗</translation>
    </message>
</context>
<context>
    <name>FirstCreatBoxMessageBox</name>
    <message>
        <source>File Safe</source>
        <translation type="vanished">文件保护箱</translation>
    </message>
    <message>
        <source>This is your first creat the protective box,please save your file!</source>
        <translation type="vanished">请妥善保存密钥文件，如果忘记密码，可以使用该密钥文件进行密码找回。</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="38"/>
        <source>Please keep the key file properly. If you forget the password, you can use the key file to retrieve the password</source>
        <translation>請妥善保管金鑰檔。如果您忘記了密碼，可以使用密鑰檔來檢索密碼</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="39"/>
        <source>Save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="53"/>
        <source>save key file</source>
        <translation>儲存金鑰檔案</translation>
    </message>
    <message>
        <source>key file(*.txt)</source>
        <translation type="vanished">密钥文件（*.txt）</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="60"/>
        <source>FileName(N):</source>
        <translation>檔案名（N）：</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="61"/>
        <source>FileType:</source>
        <translation>檔案類型：</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="62"/>
        <source>Save(S)</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="63"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="146"/>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="158"/>
        <source>critical</source>
        <translation>危急</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="146"/>
        <source>save path failed</source>
        <translation>保存路徑失敗</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="158"/>
        <source>Disallowed special characters</source>
        <translation>不允許的特殊字元</translation>
    </message>
    <message>
        <source>Save Path</source>
        <translation type="vanished">保存文件</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>ImportBoxDialog</name>
    <message>
        <source>Import filesafe box</source>
        <translation type="vanished">匯入檔案安全框</translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="vanished">進口</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>New Name</source>
        <translation type="vanished">新名稱</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密碼</translation>
    </message>
    <message>
        <source>Password length can not be higer than 32</source>
        <translation type="vanished">密碼長度不能超過 32</translation>
    </message>
    <message>
        <source>Box name cannot be empty</source>
        <translation type="vanished">框名稱不能為空</translation>
    </message>
    <message>
        <source> Import passwd cannot be empty</source>
        <translation type="vanished"> import passwd 不能為空</translation>
    </message>
    <message>
        <source>Import passwd cannot be empty</source>
        <translation type="vanished">import passwd 不能為空</translation>
    </message>
    <message>
        <source>Unzip password error</source>
        <translation type="vanished">解壓縮密碼錯誤</translation>
    </message>
    <message>
        <source>Box name has been exist</source>
        <translation type="obsolete">框名稱已退出</translation>
    </message>
    <message>
        <source>critical</source>
        <translation type="vanished">危急</translation>
    </message>
    <message>
        <source>Import failed</source>
        <translation type="vanished">匯入失敗</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="vanished">資訊</translation>
    </message>
    <message>
        <source>Import succeed</source>
        <translation type="vanished">導入成功</translation>
    </message>
    <message>
        <source>chose kybox file</source>
        <translation type="vanished">選擇 Kybox 檔</translation>
    </message>
    <message>
        <source>kybox file (*.kybox)</source>
        <translation type="vanished">kybox 檔 （*.kybox）</translation>
    </message>
    <message>
        <source>FileName(N):</source>
        <translation type="vanished">檔案名（N）：</translation>
    </message>
    <message>
        <source>FileType:</source>
        <translation type="vanished">檔案類型：</translation>
    </message>
    <message>
        <source>Look in:</source>
        <translation type="vanished">䀷：</translation>
    </message>
    <message>
        <source>Please select a box for import operation</source>
        <translation type="vanished">請選擇一個框進行導入操作</translation>
    </message>
    <message>
        <source>Box name %1 has been exist, please modify the name of the box and import it</source>
        <translation type="vanished">框名稱%1已存在，請修改框名併導入</translation>
    </message>
    <message>
        <source>set %1 Import unzip passwd</source>
        <translation type="vanished">set %1 匯入解壓縮密碼</translation>
    </message>
</context>
<context>
    <name>LibBox::ExImportSettingDialog</name>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="30"/>
        <source>Export box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="31"/>
        <source>Import box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="42"/>
        <source>File storage location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="43"/>
        <source>Select the file you want to import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="58"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="75"/>
        <source>The password for the box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="95"/>
        <source>Set the password for unpacking the box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="96"/>
        <source>Input the password for unpacking the box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="123"/>
        <source>Please enter a new box name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="133"/>
        <source>Invalid name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="147"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="152"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibBox::ExportDialog</name>
    <message>
        <location filename="../ex-import/exportdialog.cpp" line="39"/>
        <source>File Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibBox::ImportDialog</name>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="76"/>
        <source>The unpack password was entered incorrectly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="95"/>
        <location filename="../ex-import/importdialog.cpp" line="156"/>
        <source>Box name %1 is existed, please modify the name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="109"/>
        <source>File Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="92"/>
        <location filename="../ex-import/importdialog.cpp" line="154"/>
        <source>Box name %1 is invalid, please modify the name.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModuleSwitchButton</name>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="51"/>
        <location filename="../ModuleSwitchButton.cpp" line="237"/>
        <location filename="../ModuleSwitchButton.cpp" line="249"/>
        <source>Set by password</source>
        <translation>按密碼設置</translation>
    </message>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="52"/>
        <source>Set by secret key</source>
        <translation>通過金鑰設置</translation>
    </message>
</context>
<context>
    <name>PamAuthenticDialog</name>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="231"/>
        <location filename="../PamAuthenticDialog.cpp" line="235"/>
        <location filename="../PamAuthenticDialog.cpp" line="543"/>
        <source>User authentication is required to perform this operation</source>
        <translation>執行此操作需要使用者身份驗證</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="242"/>
        <location filename="../PamAuthenticDialog.cpp" line="243"/>
        <source>Enter the user password to allow this operation</source>
        <translation>輸入用戶密碼以允許此操作</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="266"/>
        <location filename="../PamAuthenticDialog.cpp" line="267"/>
        <location filename="../PamAuthenticDialog.cpp" line="269"/>
        <location filename="../PamAuthenticDialog.cpp" line="516"/>
        <source>Authenticate</source>
        <oldsource>Authorization</oldsource>
        <translation>證實</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="337"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <source>You have %1 more tries!</source>
        <translation type="obsolete">你还有%1次尝试的机会</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="494"/>
        <location filename="../PamAuthenticDialog.cpp" line="495"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="576"/>
        <source>Password authentication failed</source>
        <translation>密碼身份驗證失敗</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation type="obsolete">密码认证失败</translation>
    </message>
    <message>
        <source>authorization</source>
        <translation type="obsolete">授权</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="272"/>
        <location filename="../PamAuthenticDialog.cpp" line="273"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="275"/>
        <location filename="../PamAuthenticDialog.cpp" line="276"/>
        <source>Biometric authentication</source>
        <oldsource>Biometric</oldsource>
        <translation>生物認證</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="408"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation>%1 驗證失敗，您還有 %2 次嘗試</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="413"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation>無法驗證%1，請輸入密碼解鎖</translation>
    </message>
</context>
<context>
    <name>PasswdAuthDialog</name>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="63"/>
        <location filename="../PasswdAuthDialog.cpp" line="65"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="69"/>
        <location filename="../PasswdAuthDialog.cpp" line="71"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="132"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="133"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="162"/>
        <location filename="../PasswdAuthDialog.cpp" line="164"/>
        <location filename="../PasswdAuthDialog.cpp" line="166"/>
        <location filename="../PasswdAuthDialog.cpp" line="355"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="163"/>
        <location filename="../PasswdAuthDialog.cpp" line="165"/>
        <location filename="../PasswdAuthDialog.cpp" line="167"/>
        <location filename="../PasswdAuthDialog.cpp" line="356"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="223"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="278"/>
        <location filename="../PasswdAuthDialog.cpp" line="279"/>
        <source>umount is error</source>
        <translation>umount 是錯誤的</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="292"/>
        <location filename="../PasswdAuthDialog.cpp" line="293"/>
        <source>Password can not be empty</source>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="307"/>
        <source>Password is error</source>
        <translation type="unfinished">密码错误</translation>
    </message>
    <message>
        <source>umount is error!</source>
        <translation type="vanished">锁定失败</translation>
    </message>
    <message>
        <source>Password can not be empty!</source>
        <oldsource>Passwd can not be empty!</oldsource>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <translation type="vanished">密码错误</translation>
    </message>
</context>
<context>
    <name>PasswdAuthMessagebox</name>
    <message>
        <source>Do you Confirm to delete the box</source>
        <translation type="vanished">你确认删除保护箱吗？</translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="obsolete">新建保护箱</translation>
    </message>
    <message>
        <source>Delete the file safe %1 permanently?</source>
        <oldsource>Are you sure you want to delete the file s type=&quot;unfinished&quot;afe %1 permanently?</oldsource>
        <translation type="vanished">永久刪除檔案安全 %1？</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="161"/>
        <source>Delete the file safe &quot;%1&quot; permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="193"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="196"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="198"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="407"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="194"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="197"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="199"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="408"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="195"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="283"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="284"/>
        <source>Password length can not be higer than 32</source>
        <translation>密碼長度不能超過 32</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="346"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="347"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="367"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="368"/>
        <source>umount is error</source>
        <oldsource>umount is error!</oldsource>
        <translation>umount 是錯誤的</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="388"/>
        <source>Password is error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="355"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="356"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="31"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <oldsource>Password is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Create global key failed</source>
        <translation type="vanished">創建全域鍵失敗</translation>
    </message>
    <message>
        <location filename="../CreateBoxOprInPeony.cpp" line="83"/>
        <source>Create box failed</source>
        <translation>創建框失敗</translation>
    </message>
    <message>
        <source>Delete box failed</source>
        <translation type="vanished">刪除框失敗</translation>
    </message>
    <message>
        <location filename="../CRenameBoxOprInManager.cpp" line="87"/>
        <location filename="../DeleteBoxOprInPeony.cpp" line="91"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings::BoxSettingTranslate</name>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="75"/>
        <source>password strength strategy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="78"/>
        <source>defender force prevention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="81"/>
        <source>lock automatic screen locker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="84"/>
        <source>lock timing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings::BoxSettingWidget</name>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="49"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="75"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="81"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings::RightUIFactory</name>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="271"/>
        <source> (current value)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="328"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="356"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="373"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="417"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="431"/>
        <source>open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="328"/>
        <source>Improve password strength and reduce the risk of user privacy data leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="329"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="357"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="374"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="418"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="433"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="329"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="374"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="418"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="433"/>
        <source>After the shutdown, there is a risk of leakage of user data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="330"/>
        <source>password strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="347"/>
        <source> kinds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="349"/>
        <source>The type of password character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="352"/>
        <source>Minimum password length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="352"/>
        <source>8 characters (default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="358"/>
        <source>The user name cannot be included</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="373"/>
        <source>Effectively prevent a large number of password combinations from attempting to invade operations, and maintain data security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="375"/>
        <source>defender force prevention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="391"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="401"/>
        <source> times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="393"/>
        <source>Number of initial lock errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="396"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="450"/>
        <source> minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="398"/>
        <source>First lock time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="403"/>
        <source>Maximum number of errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="417"/>
        <source>Lock the screen to wake up the desktop, you need to re-enter the password of the protection box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="419"/>
        <source>The lock screen automatically locks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="432"/>
        <source>If the protection box is not used for a period of time, the protection box will be automatically locked, and the password needs to be re-entered when you enter it again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="434"/>
        <source>Timing lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="452"/>
        <source>Timeout for the first lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UmountBoxDialog</name>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="49"/>
        <source>After the file safe is locked, the content of the file in use may be lost. Please save it first!</source>
        <translation>檔保險箱鎖定后，正在使用的文件的內容可能會丟失。請先保存！</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="58"/>
        <location filename="../UmountBoxDialog.cpp" line="90"/>
        <source>Lock</source>
        <translation>鎖</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="147"/>
        <location filename="../UmountBoxDialog.cpp" line="148"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation>檔保險箱中有檔被佔用，需要解鎖才能鎖定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="174"/>
        <location filename="../UmountBoxDialog.cpp" line="177"/>
        <location filename="../UmountBoxDialog.cpp" line="180"/>
        <location filename="../UmountBoxDialog.cpp" line="281"/>
        <location filename="../UmountBoxDialog.cpp" line="282"/>
        <location filename="../UmountBoxDialog.cpp" line="283"/>
        <source>Hide</source>
        <translation>隱藏</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="274"/>
        <location filename="../UmountBoxDialog.cpp" line="275"/>
        <location filename="../UmountBoxDialog.cpp" line="276"/>
        <source>Display</source>
        <translation>顯示</translation>
    </message>
    <message>
        <source>*Forced locking will cause file loss. Please save the file first!</source>
        <translation type="obsolete">*强制锁定会造成文件的丢失，请先保存文件！</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="50"/>
        <source>Enforce</source>
        <translation>施行</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="172"/>
        <location filename="../UmountBoxDialog.cpp" line="175"/>
        <location filename="../UmountBoxDialog.cpp" line="178"/>
        <location filename="../UmountBoxDialog.cpp" line="301"/>
        <source>Mandatory lock</source>
        <translation>強制鎖定</translation>
    </message>
    <message>
        <source>Please release these files which are openning!</source>
        <translation type="vanished">当前保护箱中有文件正在被占用,需要解除占用才能锁定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="166"/>
        <location filename="../UmountBoxDialog.cpp" line="312"/>
        <source>Files being occupied (%1)</source>
        <oldsource>files which are openning (%1)</oldsource>
        <translation>佔用的檔案（%1）</translation>
    </message>
    <message>
        <source>files which are openning!</source>
        <translation type="vanished">正在被打开的文件</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="322"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="51"/>
        <location filename="../UmountBoxDialog.cpp" line="173"/>
        <location filename="../UmountBoxDialog.cpp" line="176"/>
        <location filename="../UmountBoxDialog.cpp" line="179"/>
        <location filename="../UmountBoxDialog.cpp" line="302"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>box_task_process_dialog</name>
    <message>
        <location filename="../BoxTaskProcessDialog.ui" line="32"/>
        <source>Dialog</source>
        <translation type="unfinished">對話</translation>
    </message>
</context>
</TS>
