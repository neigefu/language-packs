<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant_HK">
<context>
    <name>BioProxy</name>
    <message>
        <location filename="../BioProxy.cpp" line="224"/>
        <source>FingerPrint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="226"/>
        <source>FingerVein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="228"/>
        <source>Iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="230"/>
        <source>Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="232"/>
        <source>VoicePrint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="234"/>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="245"/>
        <source>Unplugging of %1 device detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="251"/>
        <source>%1 device insertion detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="261"/>
        <source>ukui-biometric-manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="264"/>
        <source>biometric</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BioWidget</name>
    <message>
        <location filename="../BioWidget.cpp" line="47"/>
        <source>The login options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="233"/>
        <source>FingerPrint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="236"/>
        <source>FingerVein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="239"/>
        <source>Iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="242"/>
        <source>Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="245"/>
        <source>VoicePrint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="248"/>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="277"/>
        <location filename="../BioWidget.cpp" line="282"/>
        <location filename="../BioWidget.cpp" line="287"/>
        <location filename="../BioWidget.cpp" line="292"/>
        <location filename="../BioWidget.cpp" line="300"/>
        <source>Verify %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="305"/>
        <source>WeChat scanning code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="316"/>
        <source> or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxCreateDialog</name>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="39"/>
        <location filename="../BoxCreateDialog.cpp" line="63"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="vanished">新建保护箱</translation>
    </message>
    <message>
        <source>Encrypt       </source>
        <oldsource>Encrypt     </oldsource>
        <translation type="obsolete">加密       </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="115"/>
        <source>Name          </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="116"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="167"/>
        <location filename="../BoxCreateDialog.cpp" line="169"/>
        <location filename="../BoxCreateDialog.cpp" line="172"/>
        <location filename="../BoxCreateDialog.cpp" line="857"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancle</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="117"/>
        <source>Confirm </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="168"/>
        <location filename="../BoxCreateDialog.cpp" line="170"/>
        <location filename="../BoxCreateDialog.cpp" line="173"/>
        <location filename="../BoxCreateDialog.cpp" line="858"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwd level</source>
        <translation type="vanished">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="386"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="404"/>
        <source>Box name has been exit</source>
        <oldsource>Box name has been exit!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create box is failed</source>
        <oldsource>Create box is failed!</oldsource>
        <translation type="obsolete">保护箱创建失败</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="396"/>
        <source>Password cannot be empty</source>
        <oldsource>Password cannot be empty!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="145"/>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="143"/>
        <source>Unlock password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="239"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="336"/>
        <location filename="../BoxCreateDialog.cpp" line="337"/>
        <location filename="../BoxCreateDialog.cpp" line="350"/>
        <location filename="../BoxCreateDialog.cpp" line="351"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password length can not be less than 6</source>
        <oldsource>Passwd length can not be less than 6</oldsource>
        <translation type="obsolete">密码长度不可低于6位</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="436"/>
        <source>Password length can not be less than 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="483"/>
        <location filename="../BoxCreateDialog.cpp" line="707"/>
        <source>Password  can not contain box name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="493"/>
        <location filename="../BoxCreateDialog.cpp" line="717"/>
        <source>Password  can not contain user name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="508"/>
        <source>Password characters must contain at least two types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="515"/>
        <source>Confirm password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="526"/>
        <source>Password is not same as verify password</source>
        <oldsource>Password is not same as verify password!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="544"/>
        <source>Create globalKey failed</source>
        <oldsource>Create globalKey failed!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="676"/>
        <location filename="../BoxCreateDialog.cpp" line="677"/>
        <location filename="../BoxCreateDialog.cpp" line="836"/>
        <source>Invaild name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="obsolete">密码长度不符合要求</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="obsolete">密码包含字符种类不符合要求</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="694"/>
        <location filename="../BoxCreateDialog.cpp" line="695"/>
        <source>Invaild password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
    <message>
        <source>Passwd level low</source>
        <translation type="vanished">密码强度低</translation>
    </message>
    <message>
        <source>Passwd level mid</source>
        <translation type="vanished">密码强度中</translation>
    </message>
    <message>
        <source>Passwd level high</source>
        <translation type="vanished">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxItemDelegate</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
</context>
<context>
    <name>BoxKeyExportDialog</name>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="100"/>
        <source>Create Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="106"/>
        <source>Select key save path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="112"/>
        <source>If forget password for the box, you can use the key to reset the password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Mode</source>
        <translation type="obsolete">保存方式</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="116"/>
        <source>Save Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">默认存储路径</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation type="obsolete">自定义存储路径</translation>
    </message>
    <message>
        <source>Please select key save path</source>
        <translation type="obsolete">选择密钥保存路径</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="121"/>
        <location filename="../BoxKeyExportDialog.cpp" line="141"/>
        <source>Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="130"/>
        <location filename="../BoxKeyExportDialog.cpp" line="139"/>
        <location filename="../BoxKeyExportDialog.cpp" line="295"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="135"/>
        <location filename="../BoxKeyExportDialog.cpp" line="140"/>
        <location filename="../BoxKeyExportDialog.cpp" line="202"/>
        <location filename="../BoxKeyExportDialog.cpp" line="296"/>
        <location filename="../BoxKeyExportDialog.cpp" line="297"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save path is empty</source>
        <translation type="obsolete">存储路径为空</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="179"/>
        <source>Key file cannot write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create globalKey failed</source>
        <translation type="obsolete">创建全局密钥失败</translation>
    </message>
    <message>
        <source>Open key file failed</source>
        <translation type="obsolete">存储密钥文件失败</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="71"/>
        <location filename="../BoxKeyExportDialog.cpp" line="73"/>
        <source>Transparent Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="71"/>
        <location filename="../BoxKeyExportDialog.cpp" line="73"/>
        <source>Encrypt Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="119"/>
        <source>Use default key path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="192"/>
        <source>save key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="199"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="200"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="201"/>
        <source>Save(S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="82"/>
        <source>选择密钥保存路径</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="107"/>
        <source>如果忘记保护箱密码，可以使用密钥重置保护箱密码</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="143"/>
        <source>保存路径</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="169"/>
        <source>无效密钥路径</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="218"/>
        <source>浏览</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="281"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="300"/>
        <source>确认</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxLoadingMessageBox</name>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="79"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="80"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="91"/>
        <source>Export succeed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="97"/>
        <source>Import succeed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="112"/>
        <source>Export failed! The path you have selected does not have permission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export failed!</source>
        <translation type="vanished">导出失败</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="117"/>
        <source>Import failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="74"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="75"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxMessageDialog</name>
    <message>
        <source>Password setting is successful!</source>
        <translation type="obsolete">密码修改成功！</translation>
    </message>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="23"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="27"/>
        <source>File Safe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxPasswdSetting</name>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="46"/>
        <location filename="../BoxPasswdSetting.cpp" line="105"/>
        <source>Password setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="174"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="175"/>
        <location filename="../BoxPasswdSetting.cpp" line="442"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="176"/>
        <source>New Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="BoxPasswdSetting.cpp" line="178"/>
        <source></source>
        <oldsource>Confirm password</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="220"/>
        <location filename="../BoxPasswdSetting.cpp" line="222"/>
        <location filename="../BoxPasswdSetting.cpp" line="224"/>
        <location filename="../BoxPasswdSetting.cpp" line="1024"/>
        <location filename="../BoxPasswdSetting.cpp" line="1340"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please update the psw</source>
        <translation type="vanished">请导入密钥文件</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">上传</translation>
    </message>
    <message>
        <source>Psw</source>
        <translation type="vanished">密钥</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="778"/>
        <location filename="../BoxPasswdSetting.cpp" line="779"/>
        <location filename="../BoxPasswdSetting.cpp" line="1130"/>
        <location filename="../BoxPasswdSetting.cpp" line="1132"/>
        <source>Box umount failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="177"/>
        <source>Confirm     </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="114"/>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">路径类型</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">默认存储路径</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation type="obsolete">自定义存储路径</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="219"/>
        <location filename="../BoxPasswdSetting.cpp" line="221"/>
        <location filename="../BoxPasswdSetting.cpp" line="223"/>
        <location filename="../BoxPasswdSetting.cpp" line="1339"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="239"/>
        <location filename="../BoxPasswdSetting.cpp" line="240"/>
        <location filename="../BoxPasswdSetting.cpp" line="447"/>
        <location filename="../BoxPasswdSetting.cpp" line="448"/>
        <location filename="../BoxPasswdSetting.cpp" line="469"/>
        <location filename="../BoxPasswdSetting.cpp" line="470"/>
        <source>Use default key path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="407"/>
        <location filename="../BoxPasswdSetting.cpp" line="408"/>
        <location filename="../BoxPasswdSetting.cpp" line="415"/>
        <location filename="../BoxPasswdSetting.cpp" line="416"/>
        <location filename="../BoxPasswdSetting.cpp" line="423"/>
        <location filename="../BoxPasswdSetting.cpp" line="424"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="566"/>
        <source>Key file permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1316"/>
        <source>File Safe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="obsolete">请导入密钥文件</translation>
    </message>
    <message>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation type="vanished">%1验证不通过，您还有%2次尝试机会</translation>
    </message>
    <message>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation type="vanished">无法验证%1，请输入密码解锁</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="761"/>
        <location filename="../BoxPasswdSetting.cpp" line="762"/>
        <source>Password can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password authentication failed</source>
        <translation type="vanished">密码认证失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="246"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password level</source>
        <oldsource>Passwd level</oldsource>
        <translation type="obsolete">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="465"/>
        <source>Secret key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="819"/>
        <location filename="../BoxPasswdSetting.cpp" line="820"/>
        <source>Password is error</source>
        <oldsource>Passwd is error</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password can not be empty</source>
        <oldsource>New passwd can not be empty</oldsource>
        <translation type="obsolete">新密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="830"/>
        <location filename="../BoxPasswdSetting.cpp" line="831"/>
        <location filename="../BoxPasswdSetting.cpp" line="1243"/>
        <location filename="../BoxPasswdSetting.cpp" line="1244"/>
        <source>New password cannot be same as old password</source>
        <oldsource>New passwd cannot be same as old passwd</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1216"/>
        <location filename="../BoxPasswdSetting.cpp" line="1218"/>
        <source>New password cannot be empty</source>
        <oldsource>New passwd cannot be empty</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="850"/>
        <location filename="../BoxPasswdSetting.cpp" line="851"/>
        <location filename="../BoxPasswdSetting.cpp" line="1227"/>
        <location filename="../BoxPasswdSetting.cpp" line="1229"/>
        <source>New password length cannot less than 8</source>
        <oldsource>New passwd length cannot less than 8</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="860"/>
        <location filename="../BoxPasswdSetting.cpp" line="861"/>
        <location filename="../BoxPasswdSetting.cpp" line="1254"/>
        <location filename="../BoxPasswdSetting.cpp" line="1256"/>
        <source>Verify password length cannot be empty</source>
        <oldsource>Verify passwd length cannot be empty</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="870"/>
        <location filename="../BoxPasswdSetting.cpp" line="871"/>
        <location filename="../BoxPasswdSetting.cpp" line="1265"/>
        <location filename="../BoxPasswdSetting.cpp" line="1267"/>
        <source>Verify password is not same as new password</source>
        <oldsource>Verify passwd is not same as new passwd</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="879"/>
        <source>Password  can not contain box name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="889"/>
        <source>Password  can not contain user name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="904"/>
        <location filename="../BoxPasswdSetting.cpp" line="906"/>
        <source>Password setting is failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="930"/>
        <location filename="../BoxPasswdSetting.cpp" line="932"/>
        <location filename="../BoxPasswdSetting.cpp" line="963"/>
        <location filename="../BoxPasswdSetting.cpp" line="965"/>
        <source>Mount is failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1012"/>
        <source>text file (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1013"/>
        <source>all files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1021"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1022"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1023"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1025"/>
        <source>Look in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1339"/>
        <source> (O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1340"/>
        <source> (C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="607"/>
        <source>The password length does not meet the requirements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="619"/>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Password setting is failed!</source>
        <oldsource>Passwd setting is failed!</oldsource>
        <translation type="obsolete">密码设置失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1009"/>
        <source>chose your file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>text file (*.txt);; all files (*);; </source>
        <oldsource>text file (*.xls *.txt);; all files (*.*);; </oldsource>
        <translation type="obsolete">文本文件(*.txt);; 所有文件 (*);;</translation>
    </message>
    <message>
        <source>The secret key file path can not be empty</source>
        <translation type="obsolete">密钥路径不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1167"/>
        <location filename="../BoxPasswdSetting.cpp" line="1168"/>
        <source>The secret key file is not exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1177"/>
        <location filename="../BoxPasswdSetting.cpp" line="1178"/>
        <source>The secret key file is unreadable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1206"/>
        <location filename="../BoxPasswdSetting.cpp" line="1207"/>
        <source>The secret key file is wrong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1283"/>
        <location filename="../BoxPasswdSetting.cpp" line="1285"/>
        <source>Set password by secret key file failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1312"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1315"/>
        <source>Password setting is successful!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invaild name</source>
        <translation type="vanished">不允许的特殊字符</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="641"/>
        <location filename="../BoxPasswdSetting.cpp" line="1407"/>
        <source>Invaild password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxRenameDialog</name>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="39"/>
        <location filename="../BoxRenameDialog.cpp" line="76"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="136"/>
        <location filename="../BoxRenameDialog.cpp" line="138"/>
        <location filename="../BoxRenameDialog.cpp" line="141"/>
        <location filename="../BoxRenameDialog.cpp" line="463"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="137"/>
        <location filename="../BoxRenameDialog.cpp" line="139"/>
        <location filename="../BoxRenameDialog.cpp" line="142"/>
        <location filename="../BoxRenameDialog.cpp" line="464"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="161"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="162"/>
        <source>New Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="163"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="239"/>
        <location filename="../BoxRenameDialog.cpp" line="240"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="296"/>
        <location filename="../BoxRenameDialog.cpp" line="298"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password is error</source>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="389"/>
        <location filename="../BoxRenameDialog.cpp" line="390"/>
        <source>File safe rename failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="408"/>
        <location filename="../BoxRenameDialog.cpp" line="410"/>
        <source>Mount is failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwd cannot be empty!</source>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="308"/>
        <location filename="../BoxRenameDialog.cpp" line="309"/>
        <source>The new name cannot be the same as the original name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="321"/>
        <location filename="../BoxRenameDialog.cpp" line="322"/>
        <source>File Safe already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="346"/>
        <location filename="../BoxRenameDialog.cpp" line="347"/>
        <source>Box umount failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="332"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <source>File safe rename failed!</source>
        <translation type="obsolete">重命名保护箱失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="445"/>
        <location filename="../BoxRenameDialog.cpp" line="446"/>
        <source>Invaild name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxTableModel</name>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Create time</source>
        <translation type="vanished">创建时间</translation>
    </message>
    <message>
        <source>Encrypt Box</source>
        <translation type="obsolete">密钥加密保护箱</translation>
    </message>
    <message>
        <source>Transparent Box</source>
        <translation type="obsolete">透明加密保护箱</translation>
    </message>
</context>
<context>
    <name>BoxTypeSelectDialog</name>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="12"/>
        <location filename="../BoxTypeSelectDialog.cpp" line="48"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="54"/>
        <source>Select box type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="59"/>
        <source>Encrypt Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="60"/>
        <source>Transparent Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="68"/>
        <source>Use user set password protection, the protection box can only be used after unlocking with the user set password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="74"/>
        <source>Using system security password protection, automatically unlocking when users log in to the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="81"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="86"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.ui" line="81"/>
        <source>请选择保护箱类型: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.ui" line="145"/>
        <source>加密保护箱</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.ui" line="178"/>
        <location filename="../BoxTypeSelectDialog.ui" line="265"/>
        <source>该操作将会导致计算机间断性宕机，宕机时间</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.ui" line="232"/>
        <source>透明加密保护箱</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.ui" line="294"/>
        <source>取消</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.ui" line="301"/>
        <source>确认</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BuiltinBoxPasswdSetting</name>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="38"/>
        <source>Create Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="67"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="119"/>
        <source>Name          </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="120"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="121"/>
        <source>Confirm </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="149"/>
        <source>Unlock password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="151"/>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="165"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="167"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="170"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="575"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="166"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="168"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="171"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="576"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="236"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="306"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="307"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="320"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="321"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="350"/>
        <source>Password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="358"/>
        <source>Password length can not be less than 8</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="366"/>
        <source>Password  can not contain box name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="373"/>
        <source>Password  can not contain user name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="385"/>
        <source>Password characters must contain at least two types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="392"/>
        <source>Confirm password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="399"/>
        <source>Password is not same as verify password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="415"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="417"/>
        <source>Password setting is failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="462"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="465"/>
        <source>Password setting is successful!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="466"/>
        <source>File Safe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="491"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="492"/>
        <source>Invaild password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="505"/>
        <source>The password length does not meet the requirements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="514"/>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CBoxUnlockAuthDialog</name>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="70"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="72"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="541"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="76"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="78"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="obsolete">默认存储路径</translation>
    </message>
    <message>
        <source>Customize</source>
        <translation type="obsolete">自定义存储路径</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="137"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="160"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="162"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="499"/>
        <source>Key Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="183"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="496"/>
        <source>Use password unlock &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="184"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="500"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="obsolete">路径类型</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="133"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="134"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="489"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="490"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="501"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="502"/>
        <source>Use default key path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="209"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="211"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="213"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="474"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="210"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="212"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="214"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="475"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="542"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="280"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="337"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="338"/>
        <source>umount is error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="350"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="351"/>
        <source>Password can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="371"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="372"/>
        <source>Password is error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="386"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="387"/>
        <source>Reset key can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="396"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="397"/>
        <source>Invalid key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="413"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="414"/>
        <source>The secret key file is wrong</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="484"/>
        <source>Use key unlock &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="487"/>
        <source>Password Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="488"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="527"/>
        <source>chose your file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="530"/>
        <source>text file (*.txt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="531"/>
        <source>all files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="539"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="540"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="543"/>
        <source>Look in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="obsolete">请导入密钥文件</translation>
    </message>
</context>
<context>
    <name>CTitleBar</name>
    <message>
        <location filename="../TitleBar.cpp" line="128"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="133"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="138"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="143"/>
        <source>Return</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportBoxGetPwdDialog</name>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="54"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="91"/>
        <source>Export filesafe box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="102"/>
        <source>set %1 export passwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="106"/>
        <source>This password is used to encrypt exported files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="153"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="155"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="158"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="347"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="378"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="382"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="154"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="156"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="159"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="348"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="383"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="373"/>
        <source>save kybox file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="380"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="381"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="514"/>
        <source>critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="514"/>
        <source>Export failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="516"/>
        <source>information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="516"/>
        <source>Export succeed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名称</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="180"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="260"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="261"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="320"/>
        <source> Export passwd cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="322"/>
        <source>Export passwd cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportBoxLoadingDialog</name>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="58"/>
        <source>Exporting, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="145"/>
        <location filename="../ExportBoxLoadingDialog.cpp" line="146"/>
        <source>Exporting %1 to %2 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="186"/>
        <source>Importing %1 ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExportPamAuthenticDialog</name>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="219"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="223"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="509"/>
        <source>User authentication is required to perform this operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="230"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="231"/>
        <source>Enter the user password to allow this operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="254"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="255"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="257"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="482"/>
        <source>Authenticate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="260"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="261"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="263"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="264"/>
        <source>Biometric authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="324"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="378"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="383"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="464"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="465"/>
        <source>Password can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="546"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="547"/>
        <source>Password authentication failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirstCreatBoxMessageBox</name>
    <message>
        <source>File Safe</source>
        <translation type="vanished">文件保护箱</translation>
    </message>
    <message>
        <source>This is your first creat the protective box,please save your file!</source>
        <translation type="vanished">请妥善保存密钥文件，如果忘记密码，可以使用该密钥文件进行密码找回。</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="37"/>
        <source>Please keep the key file properly. If you forget the password, you can use the key file to retrieve the password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="38"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="52"/>
        <source>save key file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>key file(*.txt)</source>
        <translation type="vanished">密钥文件（*.txt）</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="59"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="60"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="61"/>
        <source>Save(S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="62"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="145"/>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="157"/>
        <source>critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="145"/>
        <source>save path failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="157"/>
        <source>Disallowed special characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Path</source>
        <translation type="vanished">保存文件</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>ImportBoxDialog</name>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="57"/>
        <location filename="../ImportBoxDialog.cpp" line="94"/>
        <source>Import filesafe box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="159"/>
        <location filename="../ImportBoxDialog.cpp" line="161"/>
        <location filename="../ImportBoxDialog.cpp" line="164"/>
        <location filename="../ImportBoxDialog.cpp" line="504"/>
        <location filename="../ImportBoxDialog.cpp" line="538"/>
        <location filename="../ImportBoxDialog.cpp" line="542"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="160"/>
        <location filename="../ImportBoxDialog.cpp" line="162"/>
        <location filename="../ImportBoxDialog.cpp" line="165"/>
        <location filename="../ImportBoxDialog.cpp" line="505"/>
        <location filename="../ImportBoxDialog.cpp" line="543"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="185"/>
        <source>New Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="187"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="278"/>
        <location filename="../ImportBoxDialog.cpp" line="279"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="349"/>
        <location filename="../ImportBoxDialog.cpp" line="350"/>
        <source>Box name cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="359"/>
        <source> Import passwd cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="361"/>
        <source>Import passwd cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="374"/>
        <location filename="../ImportBoxDialog.cpp" line="375"/>
        <source>Unzip password error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="461"/>
        <location filename="../ImportBoxDialog.cpp" line="608"/>
        <source>critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="461"/>
        <source>Import failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="464"/>
        <source>information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="464"/>
        <source>Import succeed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="527"/>
        <source>chose kybox file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="530"/>
        <source>kybox file (*.kybox)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="540"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="541"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="544"/>
        <source>Look in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="608"/>
        <source>Please select a box for import operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="644"/>
        <location filename="../ImportBoxDialog.cpp" line="709"/>
        <source>Box name %1 has been exist, please modify the name of the box and import it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="660"/>
        <location filename="../ImportBoxDialog.cpp" line="661"/>
        <source>set %1 Import unzip passwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ImportBoxDialog.cpp" line="725"/>
        <source>set %1 Import passwd</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModuleSwitchButton</name>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="51"/>
        <location filename="../ModuleSwitchButton.cpp" line="234"/>
        <location filename="../ModuleSwitchButton.cpp" line="246"/>
        <source>Set by password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="52"/>
        <source>Set by secret key</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PamAuthenticDialog</name>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="217"/>
        <location filename="../PamAuthenticDialog.cpp" line="221"/>
        <location filename="../PamAuthenticDialog.cpp" line="513"/>
        <source>User authentication is required to perform this operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="228"/>
        <location filename="../PamAuthenticDialog.cpp" line="229"/>
        <source>Enter the user password to allow this operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="252"/>
        <location filename="../PamAuthenticDialog.cpp" line="253"/>
        <location filename="../PamAuthenticDialog.cpp" line="255"/>
        <location filename="../PamAuthenticDialog.cpp" line="486"/>
        <source>Authenticate</source>
        <oldsource>Authorization</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="323"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have %1 more tries!</source>
        <translation type="obsolete">你还有%1次尝试的机会</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="469"/>
        <location filename="../PamAuthenticDialog.cpp" line="470"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="547"/>
        <location filename="../PamAuthenticDialog.cpp" line="548"/>
        <source>Password authentication failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation type="obsolete">密码认证失败</translation>
    </message>
    <message>
        <source>authorization</source>
        <translation type="obsolete">授权</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="258"/>
        <location filename="../PamAuthenticDialog.cpp" line="259"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="261"/>
        <location filename="../PamAuthenticDialog.cpp" line="262"/>
        <source>Biometric authentication</source>
        <oldsource>Biometric</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="383"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="388"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswdAuthDialog</name>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="62"/>
        <location filename="../PasswdAuthDialog.cpp" line="64"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="68"/>
        <location filename="../PasswdAuthDialog.cpp" line="70"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="124"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="125"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="154"/>
        <location filename="../PasswdAuthDialog.cpp" line="156"/>
        <location filename="../PasswdAuthDialog.cpp" line="158"/>
        <location filename="../PasswdAuthDialog.cpp" line="348"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="155"/>
        <location filename="../PasswdAuthDialog.cpp" line="157"/>
        <location filename="../PasswdAuthDialog.cpp" line="159"/>
        <location filename="../PasswdAuthDialog.cpp" line="349"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="222"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="277"/>
        <location filename="../PasswdAuthDialog.cpp" line="278"/>
        <source>umount is error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="289"/>
        <location filename="../PasswdAuthDialog.cpp" line="290"/>
        <source>Password can not be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password is error</source>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <source>umount is error!</source>
        <translation type="vanished">锁定失败</translation>
    </message>
    <message>
        <source>Password can not be empty!</source>
        <oldsource>Passwd can not be empty!</oldsource>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <translation type="vanished">密码错误</translation>
    </message>
</context>
<context>
    <name>PasswdAuthMessagebox</name>
    <message>
        <source>Do you confirm to delete the box</source>
        <translation type="vanished">你确认删除保护箱吗？</translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="obsolete">新建保护箱</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="165"/>
        <source>Delete the file safe %1 permanently?</source>
        <oldsource>Are you sure you want to delete the file s type=&quot;unfinished&quot;afe %1 permanently?</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="205"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="208"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="210"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="414"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="206"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="209"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="211"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="415"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="207"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="295"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="296"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="378"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="379"/>
        <source>umount is error</source>
        <oldsource>umount is error!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="366"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="367"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="30"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong password</source>
        <oldsource>Password is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../CreateKeyOperInPeony.cpp" line="52"/>
        <source>Create global key failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../CreateBoxOprInPeony.cpp" line="82"/>
        <source>Create box failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DeleteBoxOprInPeony.cpp" line="86"/>
        <source>Delete box failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UmountBoxDialog</name>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="48"/>
        <source>After the file safe is locked, the content of the file in use may be lost. Please save it first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="57"/>
        <location filename="../UmountBoxDialog.cpp" line="89"/>
        <source>Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="140"/>
        <location filename="../UmountBoxDialog.cpp" line="141"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="167"/>
        <location filename="../UmountBoxDialog.cpp" line="170"/>
        <location filename="../UmountBoxDialog.cpp" line="173"/>
        <location filename="../UmountBoxDialog.cpp" line="274"/>
        <location filename="../UmountBoxDialog.cpp" line="275"/>
        <location filename="../UmountBoxDialog.cpp" line="276"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="267"/>
        <location filename="../UmountBoxDialog.cpp" line="268"/>
        <location filename="../UmountBoxDialog.cpp" line="269"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>*Forced locking will cause file loss. Please save the file first!</source>
        <translation type="obsolete">*强制锁定会造成文件的丢失，请先保存文件！</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="49"/>
        <source>Enforce</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="165"/>
        <location filename="../UmountBoxDialog.cpp" line="168"/>
        <location filename="../UmountBoxDialog.cpp" line="171"/>
        <location filename="../UmountBoxDialog.cpp" line="294"/>
        <source>Mandatory lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please release these files which are openning!</source>
        <translation type="vanished">当前保护箱中有文件正在被占用,需要解除占用才能锁定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="159"/>
        <location filename="../UmountBoxDialog.cpp" line="305"/>
        <source>Files being occupied (%1)</source>
        <oldsource>files which are openning (%1)</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>files which are openning!</source>
        <translation type="vanished">正在被打开的文件</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="315"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="50"/>
        <location filename="../UmountBoxDialog.cpp" line="166"/>
        <location filename="../UmountBoxDialog.cpp" line="169"/>
        <location filename="../UmountBoxDialog.cpp" line="172"/>
        <location filename="../UmountBoxDialog.cpp" line="295"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
