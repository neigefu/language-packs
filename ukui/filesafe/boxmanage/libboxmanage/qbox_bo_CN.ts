<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BioProxy</name>
    <message>
        <location filename="../BioProxy.cpp" line="224"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་མོའི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="226"/>
        <source>FingerVein</source>
        <translation>མཛུབ་མོ་ཝེ་ལིན།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="228"/>
        <source>Iris</source>
        <translation>དབྱི་ལི་སི།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="230"/>
        <source>Face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="232"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="234"/>
        <source>QRCode</source>
        <translation>QRCode</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="245"/>
        <source>Unplugging of %1 device detected</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་%1སྒྲིག་ཆས་ཀྱི་ཁ་པར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="251"/>
        <source>%1 device insertion detected</source>
        <translation>%1སྒྲིག་ཆས་ནང་དུ་བཅུག་ནས་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="261"/>
        <source>ukui-biometric-manager</source>
        <translation>ukui-biometric-manager</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="264"/>
        <source>biometric</source>
        <translation>སྐྱེ་དངོས་རིག་པ།</translation>
    </message>
</context>
<context>
    <name>BioWidget</name>
    <message>
        <location filename="../BioWidget.cpp" line="57"/>
        <source>The login options</source>
        <translation>ཐོ་འགོད་ཀྱི་བསལ་འདེམས་ཀྱི་ཇུས་གཞི།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="288"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="291"/>
        <source>FingerVein</source>
        <translation>ཞེས་པ་སྡོད་རྩ།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="294"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="297"/>
        <source>Face</source>
        <translation>མིའི་གདོང་།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="300"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་རི་མོ།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="303"/>
        <source>QRCode</source>
        <translation>དོན་ཚན་གཉིས་པ།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="332"/>
        <location filename="../BioWidget.cpp" line="337"/>
        <location filename="../BioWidget.cpp" line="342"/>
        <location filename="../BioWidget.cpp" line="348"/>
        <location filename="../BioWidget.cpp" line="354"/>
        <source>Verify %1</source>
        <translation>ཚོད་ལྟས་ར་སྤྲོད་ཀྱི་བརྒྱ་ཆ་1ཡོད།</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="359"/>
        <source>WeChat scanning code</source>
        <translation>འཕྲིན་ཕྲན་ཐོག་ནས་ཨང་གྲངས་ཕྱགས་པ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="369"/>
        <source> or enter password to unlock</source>
        <translation> ཡང་ན་གསང་གྲངས་ཀྱི་སྒོ་ལྕགས་བརྒྱབ་པ།</translation>
    </message>
</context>
<context>
    <name>BoxCreateDialog</name>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="37"/>
        <source>Create</source>
        <translation>གསར་སྐྲུན་བྱས་པ།</translation>
    </message>
    <message>
        <source>Encrypt       </source>
        <translation type="vanished">གསང་གྲངས་       </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="115"/>
        <source>Name          </source>
        <translation>མིང་།          </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="116"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="117"/>
        <source>Confirm </source>
        <translation>གཏན་འཁེལ་བྱ་དགོས། </translation>
    </message>
    <message>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="vanished">ཉུང་མཐར་ཡང་གྲངས་ཀ་བརྒྱད་ཡོད་པ་དེའི་ནང་དུ་ཡི་གེ་ཆེ་འབྲི་དང་གྲངས་ཀ་དང་དམིགས་བསལ་གྱི་མཚོན་རྟགས་བཅས་ཚུད་ཡོད། </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="166"/>
        <location filename="../BoxCreateDialog.cpp" line="168"/>
        <location filename="../BoxCreateDialog.cpp" line="171"/>
        <location filename="../BoxCreateDialog.cpp" line="525"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="167"/>
        <location filename="../BoxCreateDialog.cpp" line="169"/>
        <location filename="../BoxCreateDialog.cpp" line="172"/>
        <location filename="../BoxCreateDialog.cpp" line="526"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <source>Passwd level</source>
        <translation type="vanished">འགག་སྒོ་ལས་བརྒལ་བའི་ཆུ་ཚད།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="318"/>
        <location filename="../BoxCreateDialog.cpp" line="319"/>
        <location filename="../BoxCreateDialog.cpp" line="332"/>
        <location filename="../BoxCreateDialog.cpp" line="333"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="366"/>
        <source>Box name cannot be empty</source>
        <translation>སྒམ་གྱི་མིང་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="374"/>
        <source>Password cannot be empty</source>
        <translation>གསང་གྲངས་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="381"/>
        <location filename="../BoxCreateDialog.cpp" line="572"/>
        <source>Box name has been exist</source>
        <translation>སྒམ་གྱི་མིང་ཕྱིར་འཐེན་བྱས་ཟིན།</translation>
    </message>
    <message>
        <source>Create box is failed</source>
        <translation type="vanished">གསར་སྐྲུན་སྒམ་ལ་སྐྱོན་ཤོར་བ།</translation>
    </message>
    <message>
        <source>Password length can not be less than 6</source>
        <translation type="vanished">གསང་བའི་རིང་ཚད་ནི་6ལས་ཉུང་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="400"/>
        <location filename="../BoxCreateDialog.cpp" line="703"/>
        <source>Confirm password cannot be empty</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་གསང་གྲངས་ནི་སྟོང་བ་</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="407"/>
        <location filename="../BoxCreateDialog.cpp" line="710"/>
        <source>Password is not same as verify password</source>
        <translation>གསང་གྲངས་དང་ཞིབ་བཤེར་བྱས་པའི་གསང་གྲངས་མི་འདྲ།</translation>
    </message>
    <message>
        <source>Create globalKey failed</source>
        <translation type="vanished">སའི་གོ་ལ་ཧྲིལ་པོའི་བོང་བུ་གསར་སྐྲུན་བྱེད་པར</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="501"/>
        <location filename="../BoxCreateDialog.cpp" line="544"/>
        <location filename="../BoxCreateDialog.cpp" line="551"/>
        <location filename="../BoxCreateDialog.cpp" line="558"/>
        <location filename="../BoxCreateDialog.cpp" line="565"/>
        <source>Invaild name</source>
        <translation>གོ་མི་ཐུབ་པའི་མིང་།</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="obsolete">གསང་བའི་རིང་ཚད་ཀྱི་བླང་བྱ་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="obsolete">ཨང་ཡིག་དུ་ཡིག་རྟགས་རིགས་རྒྱུད་བླང་བྱ་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="664"/>
        <location filename="../BoxCreateDialog.cpp" line="665"/>
        <source>Invaild password</source>
        <translation>གོ་མི་ལེན་པའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="vanished">འབྲིང་རིམ་གྱི་གསང་གྲངས་སྟོབས་ཤུགས།</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="142"/>
        <source>Unlock password</source>
        <translation>གསང་བའི་གསང་བ་ཕྱིར་འཐེན་པ།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="147"/>
        <source>At least %1 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished">མ་མཐར་ཡང་བརྒྱ་ཆ%1དང་།དེའི་ནང་ཆེ་འབྲི་གསལ་བྱེད་ཆུང་བ་དང་།གྲངས་ཀ་དང་དམིགས་བསལ་ཡིག་རྟགས།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="219"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation>གསང་བའི་ལྡེ་མིག་དེ་མཉམ་དུ་Koinnlodཡི་རྩིས་ཐོའི་ནང་དུ་སླེབས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="393"/>
        <location filename="../BoxCreateDialog.cpp" line="589"/>
        <source>Password can not contain box name</source>
        <translation>གསང་བའི་ནང་དུ་སྒྲོམ་གཞིའི་མིང་ཚུད་མི་རུང་།</translation>
    </message>
    <message>
        <source>Password  can not contain user name</source>
        <translation type="vanished">གསང་བའི་ནང་དུ་སྤྱོད་མཁན་གྱི་མིང་ཚུད་མི་ཆོག</translation>
    </message>
    <message>
        <source>New password length cannot less than 8</source>
        <translation type="obsolete">གསང་གྲངས་གསར་བའི་རིང་ཚད་ནི་8ལས་ཆུང་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="599"/>
        <source>Failed to get pwquality conf</source>
        <translation type="unfinished">གསང་གྲངས་ཐོབ་ཚད་ཀྱི་ཡིག་ཆར་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <source>Password characters must contain at least two types</source>
        <translation type="vanished">གསང་བའི་ཡི་གེའི་རྟགས་ལ་ངེས་པར་དུ་མ་མཐར་ཡང་རིགས་དབྱིབས་གཉིས་འདུ་དགོས།</translation>
    </message>
    <message>
        <source>Password length can not be less than 8</source>
        <translation type="vanished">གསང་བའི་རིང་ཚད་ནི་8ལས་ཆུང་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>BoxKeyExportDialog</name>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="148"/>
        <location filename="../BoxKeyExportDialog.cpp" line="157"/>
        <location filename="../BoxKeyExportDialog.cpp" line="321"/>
        <source>Confirm</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="153"/>
        <location filename="../BoxKeyExportDialog.cpp" line="158"/>
        <location filename="../BoxKeyExportDialog.cpp" line="228"/>
        <location filename="../BoxKeyExportDialog.cpp" line="322"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>Create globalKey failed</source>
        <translation type="obsolete">སའི་གོ་ལ་ཧྲིལ་པོའི་བོང་བུ་གསར་སྐྲུན་བྱེད་པར</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="218"/>
        <source>save key file</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ཉར་ཚགས་</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="226"/>
        <source>FileType:</source>
        <translation>ཡིག་ཆའི་རིགས་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="227"/>
        <source>Save(S)</source>
        <translation>ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>ཁ་བརྡ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="82"/>
        <source>选择密钥保存路径</source>
        <translation>གསང་བའི་ལྡེ་མིག་བདམས་ནས་ཉར་ཚགས་བྱེད་པའི་ཐབས་ལམ</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="107"/>
        <source>如果忘记保护箱密码，可以使用密钥重置保护箱密码</source>
        <translation>གལ་ཏེ་སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་བ་བརྗེད་ན། གསང་བའི་ལྡེ་མིག་གིས་སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་བའི་ཨང་གྲངས་བེད་སྤྱོད་བྱས་ཆོག།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="143"/>
        <source>保存路径</source>
        <translation>ཉར་ཚགས་བྱེད་པའི་ཐབས་ལམ་</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="169"/>
        <source>无效密钥路径</source>
        <translation>གསང་ལྡེའི་ཐབས་ལམ་གོ་མི་ཆོད་པ།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="218"/>
        <source>浏览</source>
        <translation>རགས་ལྟ།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="281"/>
        <source>取消</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="300"/>
        <source>确认</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="93"/>
        <source>File Safe</source>
        <translation type="unfinished">ཡིག་ཆ་ཉེན་མེད་ཡོང་བ</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="112"/>
        <source>Create Key</source>
        <translation>གསང་ལྡེ་གསར་འཛུགས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="118"/>
        <source>Select key save path</source>
        <translation>གསང་བའི་ལྡེ་མིག་བདམས་ནས་ཉར་ཚགས་བྱེད་པའི་ཐབས་ལམ</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="125"/>
        <source>If forget password for the box, you can use the key to reset the password</source>
        <translation>གལ་ཏེ་ཐུམ་སྒྲིལ་གྱི་གསང་གྲངས་བརྗེད་སོང་ན། ཁྱེད་ཀྱིས་གསང་བའི་ཨང་གྲངས་བསྐྱར་དུ་བཀོད་ཆོག།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="130"/>
        <source>Save Path</source>
        <translation>ཉར་ཚགས་བྱེད་པའི་ཐབས་ལམ་</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="137"/>
        <location filename="../BoxKeyExportDialog.cpp" line="159"/>
        <location filename="../BoxKeyExportDialog.cpp" line="323"/>
        <source>Browser</source>
        <translation>བཤར་ཆས།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="195"/>
        <location filename="../BoxKeyExportDialog.cpp" line="204"/>
        <source>Key file cannot write</source>
        <translation>གསང་བའི་ཡིག་ཆ་ནང་དུ་འབྲི་ཐབས་མེད།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="225"/>
        <source>FileName(N):</source>
        <translation>ཡིག་ཆའི་མིང་(N)ནི།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="134"/>
        <source>Use default key path</source>
        <translation>གསང་བའི་ལྡེ་མིག་སྤྱོད་པའི་ཐབས་ལམ་སྤྱོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="82"/>
        <location filename="../BoxKeyExportDialog.cpp" line="84"/>
        <source>Transparent Box</source>
        <translation>དྭངས་གསལ་སྒམ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="82"/>
        <location filename="../BoxKeyExportDialog.cpp" line="84"/>
        <source>Encrypt Box</source>
        <translation>ཚགས་དམ་པའི་སྒམ་</translation>
    </message>
</context>
<context>
    <name>BoxLoadingMessageBox</name>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="93"/>
        <source>Export succeed!</source>
        <translation>ལེགས་འགྲུབ་ཡོང་བའི་ཁྲིད་སྟོན་བྱ་</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="99"/>
        <source>Import succeed!</source>
        <translation>གྲུབ་འབྲས་ཐོབ་པར་བྱ་དགོས།!</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="117"/>
        <source>Export failed!</source>
        <translation>ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="122"/>
        <source>Import failed!</source>
        <translation>ཕམ་ཉེས་བྱུང་བ་རེད།!</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="75"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="76"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="32"/>
        <source>File Safe</source>
        <translation type="unfinished">ཡིག་ཆ་ཉེན་མེད་ཡོང་བ</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="80"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="81"/>
        <source>View</source>
        <translation>དཔེ་རིས་ལ་བལྟ་བ།</translation>
    </message>
    <message>
        <source>Export failed! The path you have selected does not have permission</source>
        <translation type="vanished">ཕམ་ཉེས་བྱུང་བ་རེད། ཁྱེད་རང་གིས་བདམས་པའི་ཐབས་ལམ་ལ་དབང་ཆ་མེད།</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="134"/>
        <source>Import failed! The selected file is not file safe box</source>
        <translation type="unfinished">ནང་དུ་འདྲེན་པ་ཕམ་སོང་།འདེམ་པའི་ཡིག་ཆ་ནི་ཡིག་ཆའི་སྲུང་སྐྱོབ་སྒམ་མ་རེད།</translation>
    </message>
</context>
<context>
    <name>BoxMessageDialog</name>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="25"/>
        <source>Ok</source>
        <translation>ད་དུང་འགྲིག་གི་རེད།</translation>
    </message>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="29"/>
        <source>File Safe</source>
        <translation>ཡིག་ཆ་ཉེན་མེད་ཡོང་བ</translation>
    </message>
</context>
<context>
    <name>BoxOccupiedTipDialog</name>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="20"/>
        <source>Lock</source>
        <translation type="unfinished">ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="34"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="141"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="142"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation type="unfinished">ཡིག་ཚགས་ཀྱི་ཉེན་འགོག་སྒམ་ནང་དུ་ཡིག་ཆ་བཟུང་ནས་ཟྭ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="42"/>
        <source>Rename</source>
        <translation type="unfinished">མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="160"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="306"/>
        <source>Files being occupied (%1)</source>
        <translation type="unfinished">བཟུང་སྤྱོད་བྱེད་བཞིན་པའི་ཡིག་ཆ། (%1)</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="35"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="166"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="167"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="170"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="173"/>
        <source>Mandatory lock</source>
        <translation type="unfinished">བཙན་ཤེད་ཀྱིས་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="36"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="86"/>
        <source>Lock box</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="40"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to rename</source>
        <translation type="unfinished">མིག་སྔར་སྲུང་སྐྱོང་སྒམ་ནང་ཡིག་ཆ་བཟུང་སྤྱོད་བྱེད་བཞིན་ཡོད་པས་བཟུང་སྤྱོད་མེད་པ་བཟོས་ཚེ་གཞི་ནས་མིང་བཏགས་ཆོག</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="41"/>
        <source>Mandatory rename</source>
        <translation type="unfinished">བཙན་ཤེད་ཀྱིས་མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="45"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to delete</source>
        <translation type="unfinished">མིག་སྔར་སྲུང་སྐྱོང་སྒམ་ནང་ཡིག་ཆ་བཟུང་སྤྱོད་བྱེད་བཞིན་ཡོད་པས་བཟུང་སྤྱོད་མེད་པ་བཟོས་ན་མ་གཏོགས་མེད་པ་བཟོ་གི་མེད།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="46"/>
        <source>Mandatory delete</source>
        <translation type="unfinished">བཙན་ཤེད་ཀྱིས་སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="47"/>
        <source>Delete Box</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ་བསུབ་རོགས།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="168"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="171"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="174"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="296"/>
        <source>Cancel</source>
        <translation type="unfinished">མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="169"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="172"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="175"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="276"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="277"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="278"/>
        <source>Hide</source>
        <translation type="unfinished">སྦས་སྐུང་བྱེད་</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="269"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="270"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="271"/>
        <source>Display</source>
        <translation type="unfinished">འགྲེམས་སྟོན།</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="316"/>
        <source>Confirm</source>
        <translation type="unfinished">ངོས་འཛིན།</translation>
    </message>
</context>
<context>
    <name>BoxPasswdSetting</name>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="46"/>
        <location filename="../BoxPasswdSetting.cpp" line="116"/>
        <source>Password setting</source>
        <translation>གསང་གྲངས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="131"/>
        <source>At least %1 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished">མ་མཐར་ཡང་བརྒྱ་ཆ%1དང་།དེའི་ནང་ཆེ་འབྲི་གསལ་བྱེད་ཆུང་བ་དང་།གྲངས་ཀ་དང་དམིགས་བསལ་ཡིག་རྟགས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="205"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="206"/>
        <location filename="../BoxPasswdSetting.cpp" line="449"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="207"/>
        <source>New Password</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="208"/>
        <source>Confirm     </source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།     </translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="243"/>
        <location filename="../BoxPasswdSetting.cpp" line="245"/>
        <location filename="../BoxPasswdSetting.cpp" line="247"/>
        <location filename="../BoxPasswdSetting.cpp" line="1361"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="270"/>
        <location filename="../BoxPasswdSetting.cpp" line="271"/>
        <location filename="../BoxPasswdSetting.cpp" line="459"/>
        <location filename="../BoxPasswdSetting.cpp" line="460"/>
        <location filename="../BoxPasswdSetting.cpp" line="486"/>
        <location filename="../BoxPasswdSetting.cpp" line="487"/>
        <source>Please select the key path</source>
        <translation type="unfinished">གསང་རྟགས་ཡིག་ཆ་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="632"/>
        <location filename="../BoxPasswdSetting.cpp" line="633"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1176"/>
        <location filename="../BoxPasswdSetting.cpp" line="1177"/>
        <source>The secret key file cannot be empty</source>
        <translation type="unfinished">གསང་རྟགས་ཡིག་ཆ་སྟོང་བར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1361"/>
        <source> (O)</source>
        <translation> (O)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="244"/>
        <location filename="../BoxPasswdSetting.cpp" line="246"/>
        <location filename="../BoxPasswdSetting.cpp" line="248"/>
        <location filename="../BoxPasswdSetting.cpp" line="1050"/>
        <location filename="../BoxPasswdSetting.cpp" line="1362"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1362"/>
        <source> (C)</source>
        <translation> (C)</translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="vanished">གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ནང་འདྲེན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="278"/>
        <source>Import</source>
        <translation>ནང་འདྲེན་</translation>
    </message>
    <message>
        <source>Password level</source>
        <translation type="vanished">གསང་བའི་རིམ་པ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="478"/>
        <source>Secret key</source>
        <translation>གསང་བའི་ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="854"/>
        <location filename="../BoxPasswdSetting.cpp" line="855"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="869"/>
        <location filename="../BoxPasswdSetting.cpp" line="870"/>
        <location filename="../BoxPasswdSetting.cpp" line="1156"/>
        <location filename="../BoxPasswdSetting.cpp" line="1158"/>
        <source>Box umount failed</source>
        <translation>སྒམ་ཆུང་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="893"/>
        <location filename="../BoxPasswdSetting.cpp" line="904"/>
        <location filename="../BoxPasswdSetting.cpp" line="905"/>
        <source>Password is error</source>
        <translation>གསང་གྲངས་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <source>New password can not be empty</source>
        <translation type="vanished">གསང་གྲངས་གསར་པ་སྟོང་པར་འགྱུར་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="915"/>
        <location filename="../BoxPasswdSetting.cpp" line="916"/>
        <location filename="../BoxPasswdSetting.cpp" line="1265"/>
        <location filename="../BoxPasswdSetting.cpp" line="1266"/>
        <source>New password cannot be same as old password</source>
        <translation>གསང་གྲངས་གསར་པ་དང་གསང་གྲངས་རྙིང་པ་གཅིག་མཚུངས་ཡོང་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1238"/>
        <location filename="../BoxPasswdSetting.cpp" line="1240"/>
        <source>New password cannot be empty</source>
        <translation>གསང་གྲངས་གསར་པ་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>New password length cannot less than 6</source>
        <translation type="vanished">གསང་གྲངས་གསར་པའི་རིང་ཚད་6ལས་ཉུང་མི་རུང་།</translation>
    </message>
    <message>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="vanished">ཉུང་མཐར་ཡང་གྲངས་ཀ་བརྒྱད་ཡོད་པ་དེའི་ནང་དུ་ཡི་གེ་ཆེ་འབྲི་དང་གྲངས་ཀ་དང་དམིགས་བསལ་གྱི་མཚོན་རྟགས་བཅས་ཚུད་ཡོད། </translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="648"/>
        <location filename="../BoxPasswdSetting.cpp" line="649"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="681"/>
        <source>Failed to get pwquality conf</source>
        <translation type="unfinished">གསང་གྲངས་ཐོབ་པར་བྱེད་པའི་ཚད་ལ་ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="691"/>
        <location filename="../BoxPasswdSetting.cpp" line="1249"/>
        <location filename="../BoxPasswdSetting.cpp" line="1251"/>
        <source>New password length cannot less than %1</source>
        <translation type="unfinished">གསང་གྲངས་གསར་བའི་རིང་ཚད་མ་མཐའ་ཡང་། %1</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="925"/>
        <location filename="../BoxPasswdSetting.cpp" line="926"/>
        <location filename="../BoxPasswdSetting.cpp" line="1276"/>
        <location filename="../BoxPasswdSetting.cpp" line="1278"/>
        <source>Verify password length cannot be empty</source>
        <translation>གསང་གྲངས་ཀྱི་རིང་ཚད་ལ་ཞིབ་བཤེར་བྱས་ན་སྟོང་</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="935"/>
        <location filename="../BoxPasswdSetting.cpp" line="936"/>
        <location filename="../BoxPasswdSetting.cpp" line="1287"/>
        <location filename="../BoxPasswdSetting.cpp" line="1289"/>
        <source>Verify password is not same as new password</source>
        <translation>གསང་གྲངས་ཞིབ་བཤེར་བྱ་རྒྱུ་དེ་གསང་གྲངས་གསར་པ་དང་མི་འདྲ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="952"/>
        <location filename="../BoxPasswdSetting.cpp" line="954"/>
        <source>Password setting is failed</source>
        <translation>གསང་གྲངས་སྒྲིག་ཆས་ལ་སྐྱོན་ཤོར་བ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="970"/>
        <location filename="../BoxPasswdSetting.cpp" line="972"/>
        <location filename="../BoxPasswdSetting.cpp" line="996"/>
        <location filename="../BoxPasswdSetting.cpp" line="998"/>
        <source>Mount is failed</source>
        <translation>རི་བོ་ཆེན་པོ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1035"/>
        <source>chose your file </source>
        <translation>ཁྱོད་ཀྱི་ཡིག་ཆ་བདམས་པ་ </translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1038"/>
        <source>text file (*.txt)</source>
        <translation>ཡི་གེའི་ཡིག་ཚགས་(*.txt)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1039"/>
        <source>all files (*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད་(*)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1047"/>
        <source>FileName(N):</source>
        <translation>FileName(N):</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1048"/>
        <source>FileType:</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1049"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1051"/>
        <source>Look in:</source>
        <translation>ནང་དུ་ལྟོས་དང་།</translation>
    </message>
    <message>
        <source>The secret key file path can not be empty</source>
        <translation type="vanished">གསང་བའི་འགག་རྩའི་ཡིག་ཚགས་ཀྱི་ལམ་བུ་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1188"/>
        <location filename="../BoxPasswdSetting.cpp" line="1189"/>
        <source>The secret key file is not exit</source>
        <translation>གསང་བའི་འགག་རྩའི་ཡིག་ཆ་ཕྱིར་མི་འབུད་པ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1199"/>
        <location filename="../BoxPasswdSetting.cpp" line="1200"/>
        <source>The secret key file is unreadable</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ནི་ལྟ་མི་ཐུབ་པ་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1228"/>
        <location filename="../BoxPasswdSetting.cpp" line="1229"/>
        <source>The secret key file is wrong</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1305"/>
        <location filename="../BoxPasswdSetting.cpp" line="1307"/>
        <source>Set password by secret key file failed</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཚགས་ལ་བརྟེན་ནས་གསང་གྲངས་གཏན་འཁེལ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1334"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1337"/>
        <source>Password setting is successful!</source>
        <translation>གསང་གྲངས་བཀོད་སྒྲིག་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1338"/>
        <source>File Safe</source>
        <translation type="unfinished">ཡིག་ཆ་ཉེན་མེད་ཡོང་བ</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="vanished">གསང་བའི་རིང་ཚད་བླང་བྱ་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="vanished">གསང་བའི་ནང་དུ་བླང་བྱ་དང་མི་མཐུན་པའི་ཡི་གེ་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>Invaild password</source>
        <translation type="vanished">གོ་མི་ལེན་པའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="vanished">འབྲིང་རིམ་གྱི་གསང་གྲངས་སྟོབས་ཤུགས།</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆེ་བ།</translation>
    </message>
    <message>
        <source>New password length cannot less than 8</source>
        <translation type="vanished">གསང་གྲངས་གསར་བའི་རིང་ཚད་ནི་8ལས་ཆུང་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="267"/>
        <location filename="../BoxPasswdSetting.cpp" line="268"/>
        <location filename="../BoxPasswdSetting.cpp" line="456"/>
        <location filename="../BoxPasswdSetting.cpp" line="457"/>
        <location filename="../BoxPasswdSetting.cpp" line="483"/>
        <location filename="../BoxPasswdSetting.cpp" line="484"/>
        <source>Use default key path</source>
        <translation>གསང་བའི་ལྡེ་མིག་སྤྱོད་པའི་ཐབས་ལམ་སྤྱོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="657"/>
        <source>Password can not contain box name</source>
        <translation>གསང་བའི་ནང་དུ་སྒྲོམ་གཞིའི་མིང་ཚུད་མི་རུང་།</translation>
    </message>
    <message>
        <source>Password  can not contain user name</source>
        <translation type="vanished">གསང་བའི་ནང་དུ་སྤྱོད་མཁན་གྱི་མིང་ཚུད་མི་ཆོག</translation>
    </message>
    <message>
        <source>Key file permission denied</source>
        <translation type="vanished">གསང་བའི་ཡིག་ཆའི་དབང་ཚད་དང་ལེན་མ་བྱས།</translation>
    </message>
</context>
<context>
    <name>BoxRenameDialog</name>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="40"/>
        <location filename="../BoxRenameDialog.cpp" line="57"/>
        <source>Rename</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="121"/>
        <location filename="../BoxRenameDialog.cpp" line="123"/>
        <location filename="../BoxRenameDialog.cpp" line="126"/>
        <location filename="../BoxRenameDialog.cpp" line="508"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="122"/>
        <location filename="../BoxRenameDialog.cpp" line="124"/>
        <location filename="../BoxRenameDialog.cpp" line="127"/>
        <location filename="../BoxRenameDialog.cpp" line="509"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="146"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="147"/>
        <source>New Name</source>
        <translation>མིང་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="148"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="230"/>
        <location filename="../BoxRenameDialog.cpp" line="231"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="283"/>
        <location filename="../BoxRenameDialog.cpp" line="284"/>
        <source>Box name cannot be empty</source>
        <translation>སྒམ་གྱི་མིང་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="390"/>
        <location filename="../BoxRenameDialog.cpp" line="391"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="429"/>
        <location filename="../BoxRenameDialog.cpp" line="430"/>
        <source>The new name cannot be the same as the original name</source>
        <translation>མིང་གསར་པ་དང་མ་ཡིག་གི་མིང་གཅིག་མཚུངས་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="440"/>
        <location filename="../BoxRenameDialog.cpp" line="441"/>
        <source>File Safe already exists</source>
        <translation>ཡིག་ཆའི་བདེ་འཇགས་གནས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="294"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="307"/>
        <location filename="../BoxRenameDialog.cpp" line="308"/>
        <source>Box umount failed</source>
        <translation>སྒམ་ཆུང་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="329"/>
        <source>Password is error</source>
        <translation>གསང་གྲངས་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="352"/>
        <location filename="../BoxRenameDialog.cpp" line="353"/>
        <source>File safe rename failed</source>
        <translation>ཡིག་ཚགས་ཀྱི་བདེ་འཇགས་མིང་ལ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="369"/>
        <location filename="../BoxRenameDialog.cpp" line="370"/>
        <source>Mount is failed</source>
        <translation>རི་བོ་ཆེན་པོ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="413"/>
        <location filename="../BoxRenameDialog.cpp" line="414"/>
        <location filename="../BoxRenameDialog.cpp" line="421"/>
        <location filename="../BoxRenameDialog.cpp" line="422"/>
        <source>Invaild name</source>
        <translation>གོ་མི་ཐུབ་པའི་མིང་།</translation>
    </message>
</context>
<context>
    <name>BoxTaskProcessDialog</name>
    <message>
        <location filename="../BoxTaskProcessDialog.cpp" line="31"/>
        <source>Prompt information</source>
        <translation type="unfinished">སྣེ་སྟོན་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../BoxTaskProcessDialog.cpp" line="81"/>
        <source>Info</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ།</translation>
    </message>
</context>
<context>
    <name>BoxTypeSelectDialog</name>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="29"/>
        <source>Create</source>
        <translation>གསར་གཏོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="120"/>
        <source>Confirm</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="112"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ཁ་བརྡ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>请选择保护箱类型: </source>
        <translation type="vanished">སྲུང་སྐྱོབ་སྒམ་གྱི་རིགས་བསལ་འདེམས་གནང་རོགས། </translation>
    </message>
    <message>
        <source>加密保护箱</source>
        <translation type="vanished">གསང་བའི་སྲུང་སྒམ།</translation>
    </message>
    <message>
        <source>该操作将会导致计算机间断性宕机，宕机时间</source>
        <translation type="vanished">བཀོལ་སྤྱོད་འདིས་རྩིས་འཁོར་བར་ཆད་རང་བཞིན་གྱི་宕་འཁོར་ལ་བརྟེན་ནས་འཕྲུལ་འཁོར་གྱི་དུས་ཚོད་宕་པ་རེད།</translation>
    </message>
    <message>
        <source>透明加密保护箱</source>
        <translation type="vanished">གསང་བའི་སྲུང་སྐྱོབ་སྒམ་ཕྱི་གསལ་ནང་གསལ་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>取消</source>
        <translation type="vanished">མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>确认</source>
        <translation type="vanished">དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="44"/>
        <source>Select box type:</source>
        <translation>ཐུམ་སྒྲིལ་སྒམ་ཆུང་གི་རིགས་བདམས་པ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="62"/>
        <source>Encrypt Box</source>
        <translation>ཚགས་དམ་པའི་སྒམ་</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="89"/>
        <source>Transparent Box</source>
        <translation>དྭངས་གསལ་སྒམ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="67"/>
        <source>Use user set password protection, the protection box can only be used after unlocking with the user set password</source>
        <translation>བེད་སྤྱོད་བྱེད་མཁན་གྱིས་བཙུགས་པའི་གསང་བའི་སྲུང་སྐྱོབ་སྒམ་དེ་ཟྭ་བརྒྱབ་རྗེས་ད་གཟོད་སྤྱོད་མཁན་གྱིས་བཟོས་པའི་གསང་བ་སྲུང་སྐྱོབ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="94"/>
        <source>Using system security password protection, automatically unlocking when users log in to the system</source>
        <translation>མ་ལག་བདེ་འཇགས་ཀྱི་གསང་བ་སྲུང་སྐྱོང་བྱེད་པ་དང་། སྤྱོད་མཁན་ཐོ་འགོད་མ་ལག་སྤྱོད་སྐབས་རང་འགུལ་གྱིས་ཟྭ་རྒྱག་དགོས།</translation>
    </message>
</context>
<context>
    <name>BuiltinBoxPasswdSetting</name>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="39"/>
        <source>Create Password</source>
        <translation>གསང་བའི་ཨང་གྲངས་གསར་འཛུགས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="69"/>
        <source>Create</source>
        <translation>གསར་གཏོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="134"/>
        <source>Name          </source>
        <translation>མིང་།          </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="135"/>
        <source>Password</source>
        <translation>གསང་བའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="136"/>
        <source>Confirm </source>
        <translation>དངོས་སུ་ཁས་ལེན་པ། </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="164"/>
        <source>Unlock password</source>
        <translation>གསང་བའི་གསང་བ་ཕྱིར་འཐེན་པ།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="166"/>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation>ཉུང་མཐར་ཡང་གྲངས་ཀ་བརྒྱད་ཡོད་པ་དེའི་ནང་དུ་ཡི་གེ་ཆེ་འབྲི་དང་གྲངས་ཀ་དང་དམིགས་བསལ་གྱི་མཚོན་རྟགས་བཅས་ཚུད་ཡོད། </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="180"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="182"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="185"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="575"/>
        <source>Confirm</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="181"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="183"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="186"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="576"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="230"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation>གསང་བའི་ལྡེ་མིག་དེ་མཉམ་དུ་Koinnlodཡི་རྩིས་ཐོའི་ནང་དུ་སླེབས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="300"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="301"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="314"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="315"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="344"/>
        <source>Password cannot be empty</source>
        <translation>གསང་བ་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="352"/>
        <source>Password length can not be less than 8</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་8ལས་ཆུང་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="360"/>
        <source>Password can not contain box name</source>
        <translation>གསང་བའི་ནང་དུ་སྒྲོམ་གཞིའི་མིང་ཚུད་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="367"/>
        <source>Password  can not contain user name</source>
        <translation>གསང་བའི་ནང་དུ་སྤྱོད་མཁན་གྱི་མིང་ཚུད་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="379"/>
        <source>Password characters must contain at least two types</source>
        <translation>གསང་བའི་ཡི་གེའི་རྟགས་ལ་ངེས་པར་དུ་མ་མཐར་ཡང་རིགས་དབྱིབས་གཉིས་འདུ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="386"/>
        <source>Confirm password cannot be empty</source>
        <translation>གསང་གྲངས་སྟོང་པར་གཏན་ཁེལ་བྱ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="393"/>
        <source>Password is not same as verify password</source>
        <translation>གསང་བ་དང་ཚོད་ལྟས་ར་སྤྲོད་ཀྱི་གསང་བ་མི་འདྲ།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="409"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="411"/>
        <source>Password setting is failed</source>
        <translation>གསང་བའི་ཨང་གྲངས་བཀོད་སྒྲིག་ལེགས་འགྲུབ་མ་བྱུང་</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="456"/>
        <source>Ok</source>
        <translation>ད་དུང་འགྲིག་གི་རེད།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="459"/>
        <source>Password setting is successful!</source>
        <translation>གསང་བའི་སྒྲིག་བཀོད་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="460"/>
        <source>File Safe</source>
        <translation>ཡིག་ཆ་ཉེན་མེད་ཡོང་བ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="485"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="486"/>
        <source>Invaild password</source>
        <translation>Invaldཡི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="obsolete">གསང་བའི་རིང་ཚད་ཀྱི་བླང་བྱ་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="obsolete">ཨང་ཡིག་དུ་ཡིག་རྟགས་རིགས་རྒྱུད་བླང་བྱ་དང་མི་མཐུན་པ།</translation>
    </message>
</context>
<context>
    <name>CBoxCompatibilityUpgradeOperation</name>
    <message>
        <source>Upgade</source>
        <translation type="obsolete">གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Box upgrading...</source>
        <translation type="unfinished">སྲུང་སྐྱོང་སྒམ།...</translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Box compatibility upgrading, no closing!</source>
        <translation type="unfinished">ཡིག་ཆའི་སྲུང་སྐྱོང་སྒམ་རིམ་སྤོར་བྱེད་སྐབས་སྒོ་རྒྱག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Upgrade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="36"/>
        <source>Box compatibility upgrade failed</source>
        <translation type="unfinished">ཡིག་ཆའི་སྲུང་སྐྱོབ་སྒམ་གྱི་རིམ་པ་འཕར་བ་ཕམ་སོང་།</translation>
    </message>
</context>
<context>
    <name>CBoxUnlockAuthDialog</name>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="67"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="69"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="588"/>
        <source>Open</source>
        <translation>ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">མིང་ཆེན་པོ་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="138"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="139"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="532"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="533"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="549"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="550"/>
        <source>Please select the key path</source>
        <translation type="unfinished">གསང་རྟགས་ཡིག་ཆ་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="143"/>
        <source>Import</source>
        <translation>ནང་འདྲེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="190"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="543"/>
        <source>Password</source>
        <translation>གསང་བའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="215"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="217"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="219"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="512"/>
        <source>Confirm</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="216"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="218"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="220"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="513"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="589"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="309"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="366"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="367"/>
        <source>umount is error</source>
        <translation>umounttཡིན་པ་དེ་ནི་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="380"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="381"/>
        <source>Password can not be empty</source>
        <translation>གསང་བ་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="395"/>
        <source>Password is error</source>
        <translation>གསང་བའི་ཨང་གྲངས་ནོར་བ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="447"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="448"/>
        <source>The secret key file is wrong</source>
        <translation>གསང་བའི་ཡིག་ཆ་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="574"/>
        <source>chose your file </source>
        <translation>ཁྱེད་ཀྱི་ཡིག་ཆ་འདེམས་པ། </translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="577"/>
        <source>text file (*.txt)</source>
        <translation>ཡིག་ཆ་འདིའི་ནང་གི་ཡིག་ཆ། (.txt)</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="578"/>
        <source>all files (*)</source>
        <translation>ཡིག་ཆ་ཚང་མ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="587"/>
        <source>FileType:</source>
        <translation>ཡིག་ཆའི་རིགས་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="590"/>
        <source>Look in:</source>
        <translation>䀷།གཤམ་གསལ་ལྟར་ཡིན།</translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="obsolete">གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ནང་འདྲེན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="166"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="168"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="542"/>
        <source>Key Unlock</source>
        <translation>ལྡེ་མིག་གིས་ཟྭ་གྲོལ་བ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="189"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="539"/>
        <source>Use password unlock &quot;%1&quot;</source>
        <translation>གསང་བའི་སྒོ་ནས་&quot;%1&quot;སྤྱོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="414"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="415"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="420"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="421"/>
        <source>Reset key can not be empty</source>
        <translation>སླར་ཡང་མཐེབ་བཀྱག་དེ་སྟོང་པར་འགྱུར་མི</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="430"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="431"/>
        <source>Invalid key file</source>
        <translation>གསང་ལྡེའི་ཡིག་ཆ་གོ་མི་ཆོད་པ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="522"/>
        <source>Use key unlock &quot;%1&quot;</source>
        <translation>གསང་ལྡེའི་སྒོ་ལྕགས་བརྒྱབ་ནས་&quot;%1&quot;སྤྱོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="525"/>
        <source>Password Unlock</source>
        <translation>གསང་བའི་སྒོ་ལྕགས་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="526"/>
        <source>Key</source>
        <translation>ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="586"/>
        <source>FileName(N):</source>
        <translation>ཡིག་ཆའི་མིང་(N)ནི།</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="135"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="136"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="529"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="530"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="546"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="547"/>
        <source>Use default key path</source>
        <translation>གསང་བའི་ལྡེ་མིག་སྤྱོད་པའི་ཐབས་ལམ་སྤྱོད་དགོས།</translation>
    </message>
</context>
<context>
    <name>CCreateKeyOperInPeony</name>
    <message>
        <location filename="../CreateKeyOperInPeony.cpp" line="52"/>
        <source>Create global key failed</source>
        <translation type="unfinished">ཁྱོན་ཡོངས་ཀྱི་མཐེབ་བཀྱག་གསར་འཛུགས་བྱས་ནས</translation>
    </message>
</context>
<context>
    <name>CDeleteBoxOprInPeony</name>
    <message>
        <location filename="../DeleteBoxOprInPeony.cpp" line="129"/>
        <source>Delete box failed</source>
        <translation type="unfinished">འཆིང་རྒྱ་མེད་པར་བཟོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>CExportAuthCred</name>
    <message>
        <source>critical</source>
        <translation type="obsolete">སྐྱོན་བརྗོད་</translation>
    </message>
    <message>
        <source>Export failed</source>
        <translation type="obsolete">ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="obsolete">ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Export succeed</source>
        <translation type="obsolete">ལེགས་འགྲུབ་ཡོང་བའི་ཁྲིད་སྟོན་བྱ་</translation>
    </message>
</context>
<context>
    <name>CImportBoxOprInPeony</name>
    <message>
        <source>critical</source>
        <translation type="obsolete">སྐྱོན་བརྗོད་</translation>
    </message>
    <message>
        <source>Import failed</source>
        <translation type="obsolete">ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="obsolete">ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Import succeed</source>
        <translation type="obsolete">གྲུབ་འབྲས་ཐོབ་པར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>CTitleBar</name>
    <message>
        <location filename="../TitleBar.cpp" line="128"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ་</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="133"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="138"/>
        <source>Options</source>
        <translation>འདེམས་ཚན་</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="143"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
</context>
<context>
    <name>ExportBoxGetPwdDialog</name>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="52"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="89"/>
        <source>Export filesafe box</source>
        <translation>ཡིག་ཆ་སྲུང་སྐྱོང་སྒམ་དུ་ཁྲིད་པ།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="103"/>
        <source>set %1 export passwd</source>
        <translation>བརྒྱ་ཆ་1ཟིན་པའི་འདྲེན་བྱེད་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="107"/>
        <source>This password is used to encrypt exported files</source>
        <translation>གསང་གྲངས་དེ་ཁྲིད་སྟོན་བྱས་པའི་ཡིག་ཆའི་གསང་གྲངས་ལ་སྤྱོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="158"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="160"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="163"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="345"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="376"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="380"/>
        <source>Export</source>
        <translation>ཁྲིད་སྟོན་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="159"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="161"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="164"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="346"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="381"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="371"/>
        <source>save kybox file</source>
        <translation>ཡིག་ཆ་སྲུང་སྐྱོང་སྒམ་དུ་ཁྲིད་པ།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="378"/>
        <source>FileName(N):</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="379"/>
        <source>FileType:</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <source>critical</source>
        <translation type="vanished">སྐྱོན་བརྗོད་</translation>
    </message>
    <message>
        <source>Export failed</source>
        <translation type="vanished">ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="vanished">ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Export succeed</source>
        <translation type="vanished">ལེགས་འགྲུབ་ཡོང་བའི་ཁྲིད་སྟོན་བྱ་</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">མིང་།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="185"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="256"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="257"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="315"/>
        <source> Export passwd cannot be empty</source>
        <translation>གསང་བའི་ཨང་གྲངས་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="317"/>
        <source>Export passwd cannot be empty</source>
        <translation>གསང་བའི་ཨང་གྲངས་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>ExportBoxLoadingDialog</name>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="41"/>
        <source>File Safe</source>
        <translation type="unfinished">ཡིག་ཆ་ཉེན་མེད་ཡོང་བ</translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="64"/>
        <source>Exporting, please wait...</source>
        <translation>ཁྲིད་སྟོན་བྱེད་རིང་།</translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="126"/>
        <location filename="../ExportBoxLoadingDialog.cpp" line="127"/>
        <source>Importing %1 ...</source>
        <translation>ནང་འདྲེན་ %1</translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="116"/>
        <location filename="../ExportBoxLoadingDialog.cpp" line="117"/>
        <source>Exporting %1 to %2 </source>
        <translation>བརྒྱ་ཆ་1ནས་2%བར་ཁྲིད་རྒྱུ་རེད། </translation>
    </message>
</context>
<context>
    <name>ExportPamAuthenticDialog</name>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="232"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="236"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="533"/>
        <source>User authentication is required to perform this operation</source>
        <translation>ཐེངས་འདིའི་བྱ་སྤྱོད་ལ་སྤྱོད་མཁན་གྱི་བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="243"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="244"/>
        <source>Enter the user password to allow this operation</source>
        <translation>སྤྱོད་མཁན་གྱི་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་བཀོལ་སྤྱོད་འདི་བྱེད་དུ་</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="267"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="268"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="270"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="506"/>
        <source>Authenticate</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="273"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="274"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="277"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="278"/>
        <source>Biometric authentication</source>
        <translation>སྐྱེ་དངོས་དབྱེ་འབྱེད་ཀྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="339"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="398"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation>%1ཚོད་ལྟ་ས་ར་སྤྲོད་མི་བརྒྱུད་ཁྱེད་ཀྱིས་ད་དུང་%2ཐེངས་ཚོད་ལྟ་བྱེད་པའི་གོ་སྐབས། </translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="403"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation>%1ར་སྤྲོད་བྱེད་མི་ཐུབ།གསང་གྲངས་མནན་ནས་ཟྭ་འབྱེད་རོགས། </translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="484"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="485"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="548"/>
        <source>Password authentication failed</source>
        <translation>གསང་གྲངས་བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>FirstCreatBoxMessageBox</name>
    <message>
        <source>question</source>
        <translation type="vanished">གནད་དོན།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="38"/>
        <source>Please keep the key file properly. If you forget the password, you can use the key file to retrieve the password</source>
        <translation>ཁྱོད་ཀྱིས་ལྡེ་མིག་གི་ཡིག་ཆ་ཉར་ཚགས་ཡག་པོ་བྱེད་རོགས། གལ་ཏེ་ཁྱོད་ཀྱིས་གསང་གྲངས་བརྗེད་སོང་ན། ཁྱོད་ཀྱིས་ལྡེ་མིག་གི་ཡིག་ཆ་བཀོལ་ནས་གསང་གྲངས་ཕྱིར་ལེན་ཐུབ།</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="39"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="53"/>
        <source>save key file</source>
        <translation>ལྡེ་མིག་གི་ཡིག་ཆ་ཉར་ཚགས</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="60"/>
        <source>FileName(N):</source>
        <translation>FileName(N):</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="61"/>
        <source>FileType:</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="62"/>
        <source>Save(S)</source>
        <translation>གྲོན་ཆུང་བྱ་དགོས། (S)</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="63"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="146"/>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="158"/>
        <source>critical</source>
        <translation>སྐྱོན་བརྗོད་</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="146"/>
        <source>save path failed</source>
        <translation>ལམ་ཕྲན་ཉར་ཚགས་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="158"/>
        <source>Disallowed special characters</source>
        <translation>དམིགས་བསལ་གྱི་མི་སྣ་མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>ImportBoxDialog</name>
    <message>
        <source>Import filesafe box</source>
        <translation type="vanished">ཡིག་ཆ་སྲུང་སྐྱོང་སྒམ་ནང་འཇུག་དགོས།</translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="vanished">ནང་འདྲེན་</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>New Name</source>
        <translation type="vanished">མིང་གསར་པ།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Password length can not be higer than 32</source>
        <translation type="vanished">གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <source> Import passwd cannot be empty</source>
        <translation type="vanished">གསང་བའི་ཨང་གྲངས་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <source>Import passwd cannot be empty</source>
        <translation type="vanished">གསང་བའི་ཨང་གྲངས་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <source>New box name can not be contained by box password</source>
        <translation type="obsolete">གསང་གྲངས་སུ་སྲུང་སྐྱོབ་སྒམ་གྱི་མིང་གསར་བ་འདུས་མི་རུང་།</translation>
    </message>
    <message>
        <source>critical</source>
        <translation type="vanished">སྐྱོན་བརྗོད་</translation>
    </message>
    <message>
        <source>Import failed</source>
        <translation type="vanished">ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="vanished">ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Import succeed</source>
        <translation type="vanished">གྲུབ་འབྲས་ཐོབ་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>chose kybox file</source>
        <translation type="vanished">སྲུང་སྐྱོབ་སྒམ་གྱི་ཡིག་ཆ་འདེམས་དགོས།</translation>
    </message>
    <message>
        <source>FileName(N):</source>
        <translation type="vanished">ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>FileType:</source>
        <translation type="vanished">ཡིག་ཆའི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <source>Look in:</source>
        <translation type="vanished">ནང་དུ་ལྟོས་དང་།</translation>
    </message>
    <message>
        <source>Please select a box for import operation</source>
        <translation type="vanished">སྲུང་སྐྱོབ་སྒམ་ཞིག་བདམས་ནས་ནང་དུ་འདྲེན་རོགས།</translation>
    </message>
    <message>
        <source>Box name %1 has been exist, please modify the name of the box and import it</source>
        <translation type="vanished">སྲུང་སྐྱོབ་སྒམ་གྱི་མིང་བརྒྱ་ཆ་1ཡོད་པས་མིང་ལ་གཙིགས་ཆེན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>set %1 Import passwd</source>
        <translation type="vanished">%1ནང་འཇུག་བྱས་པའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>kybox file (*.kybox)</source>
        <translation type="vanished">ཡིག་ཆ་kooཡིག་ཆ། (kobox)།</translation>
    </message>
    <message>
        <source>Unzip password error</source>
        <translation type="vanished">གསང་གྲངས་ཉུང་འཕྲིའི་ནོར་འཁྲུལ་སེལ་དགོས།</translation>
    </message>
    <message>
        <source>Box name cannot be empty</source>
        <translation type="vanished">སྒྲོམ་གཞིའི་མིང་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <source>set %1 Import unzip passwd</source>
        <translation type="obsolete">%1 བཀོད་སྒྲིག་བྱས་པའི་ནང་འདྲེན་གསང་གྲངས་ཉུང་འཕྲི་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Box Password</source>
        <translation type="obsolete">སྒམ་གྱི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Box name has been exist</source>
        <translation type="obsolete">སྒམ་གྱི་མིང་ཕྱིར་འཐེན་བྱས་ཟིན།</translation>
    </message>
    <message>
        <source>imput %1 box passwd</source>
        <translation type="obsolete">%1ཡི་སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>input %1 box passwd</source>
        <translation type="obsolete">%1ཡི་སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་གྲངས།</translation>
    </message>
</context>
<context>
    <name>LibBox::ExImportSettingDialog</name>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="30"/>
        <source>Export box</source>
        <translation type="unfinished">སྲུང་སྐྱོང་སྒམ།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="31"/>
        <source>Import box</source>
        <translation type="unfinished">སྲུང་སྐྱོང་སྒམ།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="42"/>
        <source>File storage location</source>
        <translation type="unfinished">ཡིག་ཆ་གསོག་འཇོག་གནས་ས།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="43"/>
        <source>Select the file you want to import</source>
        <translation type="unfinished">ཁྱོད་ཀྱིས་ནང་འཇུག་བྱེད་དགོས་པའི་ཡིག་ཆ་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="58"/>
        <source>Select</source>
        <translation type="unfinished">གདམ་ག</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="75"/>
        <source>The password for the box</source>
        <translation type="unfinished">སྒམ་གྱི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="95"/>
        <source>Set the password for unpacking the box</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="96"/>
        <source>Input the password for unpacking the box</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="123"/>
        <source>Please enter a new box name</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ་གསར་བའི་མིང་འབྲི་རོགས།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="133"/>
        <source>Invalid name</source>
        <translation type="unfinished">བཀོལ་མི་རུང་བའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="147"/>
        <source>Cancel</source>
        <translation type="unfinished">མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="152"/>
        <source>Confirm</source>
        <translation type="unfinished">ངོས་འཛིན།</translation>
    </message>
</context>
<context>
    <name>LibBox::ExportDialog</name>
    <message>
        <location filename="../ex-import/exportdialog.cpp" line="39"/>
        <source>File Select</source>
        <translation type="unfinished">ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>LibBox::ImportDialog</name>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="76"/>
        <source>The unpack password was entered incorrectly</source>
        <translation type="unfinished">གསང་གྲངས་མནན་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="95"/>
        <location filename="../ex-import/importdialog.cpp" line="156"/>
        <source>Box name %1 is existed, please modify the name.</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ་གྱི་མིང་ཡོད་པ་རེད།ཁྱོད་ཀྱིས་མིང་བཏགས་རོགས།</translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="109"/>
        <source>File Select</source>
        <translation type="unfinished">ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="92"/>
        <location filename="../ex-import/importdialog.cpp" line="154"/>
        <source>Box name %1 is invalid, please modify the name.</source>
        <translation type="unfinished">སྲུང་སྐྱོབ་སྒམ་གྱི་མིང་ཡོད་པ་རེད།ཁྱོད་ཀྱིས་མིང་བཏགས་རོགས།</translation>
    </message>
</context>
<context>
    <name>ModuleSwitchButton</name>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="51"/>
        <location filename="../ModuleSwitchButton.cpp" line="237"/>
        <location filename="../ModuleSwitchButton.cpp" line="249"/>
        <source>Set by password</source>
        <translation>གསང་གྲངས་ལ་བརྟེན་ནས་གཏན་འཁེལ</translation>
    </message>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="52"/>
        <source>Set by secret key</source>
        <translation>གསང་བའི་ལྡེ་མིག་གིས་གཏན་འཁེལ་</translation>
    </message>
</context>
<context>
    <name>PamAuthenticDialog</name>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="231"/>
        <location filename="../PamAuthenticDialog.cpp" line="235"/>
        <location filename="../PamAuthenticDialog.cpp" line="543"/>
        <source>User authentication is required to perform this operation</source>
        <translation>ཐེངས་འདིའི་བྱ་སྤྱོད་ལ་སྤྱོད་མཁན་གྱི་བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="242"/>
        <location filename="../PamAuthenticDialog.cpp" line="243"/>
        <source>Enter the user password to allow this operation</source>
        <translation>སྤྱོད་མཁན་གྱི་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་བཀོལ་སྤྱོད་འདི་བྱེད་དུ་</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="266"/>
        <location filename="../PamAuthenticDialog.cpp" line="267"/>
        <location filename="../PamAuthenticDialog.cpp" line="269"/>
        <location filename="../PamAuthenticDialog.cpp" line="516"/>
        <source>Authenticate</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="272"/>
        <location filename="../PamAuthenticDialog.cpp" line="273"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="275"/>
        <location filename="../PamAuthenticDialog.cpp" line="276"/>
        <source>Biometric authentication</source>
        <translation>སྐྱེ་དངོས་དབྱེ་འབྱེད་ཀྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="337"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="494"/>
        <location filename="../PamAuthenticDialog.cpp" line="495"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>You have %1 more tries!</source>
        <translation type="vanished">ཁྱེད་ཚོར་ད་དུང་ཚོད་ལྟ་ཐེངས་1བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="576"/>
        <source>Password authentication failed</source>
        <translation>གསང་གྲངས་བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="408"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation>བརྒྱ་ཆ་1ལ་ཚོད་ལྟས་ར་སྤྲོད་བྱས་པ་ཕམ་སོང་། ཁྱེད་རང་ལ་ད་དུང་ཚོད་ལྟ་ཐེངས་2བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="413"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation>ཚོད་ལྟས་ར་སྤྲོད་བྱེད་ཐབས་བྲལ་བས་གསང་བའི་སྒོ་ལྕགས་རྒྱག་རོགས།</translation>
    </message>
</context>
<context>
    <name>PasswdAuthDialog</name>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="63"/>
        <location filename="../PasswdAuthDialog.cpp" line="65"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="69"/>
        <location filename="../PasswdAuthDialog.cpp" line="71"/>
        <source>Rename</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="132"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="133"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="162"/>
        <location filename="../PasswdAuthDialog.cpp" line="164"/>
        <location filename="../PasswdAuthDialog.cpp" line="166"/>
        <location filename="../PasswdAuthDialog.cpp" line="355"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="223"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="163"/>
        <location filename="../PasswdAuthDialog.cpp" line="165"/>
        <location filename="../PasswdAuthDialog.cpp" line="167"/>
        <location filename="../PasswdAuthDialog.cpp" line="356"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="278"/>
        <location filename="../PasswdAuthDialog.cpp" line="279"/>
        <source>umount is error</source>
        <translation>ཝུའུ་ཁི་ལན་ནི་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="292"/>
        <location filename="../PasswdAuthDialog.cpp" line="293"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="307"/>
        <source>Password is error</source>
        <translation>གསང་གྲངས་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>PasswdAuthMessagebox</name>
    <message>
        <source>Delete the file safe %1 permanently?</source>
        <translation type="vanished">དུས་གཏན་དུ་ཡིག་ཆའི་བདེ་འཇགས་ཀྱི་ཨང་གྲངས་1མ བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="193"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="196"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="198"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="407"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="194"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="197"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="199"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="408"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="31"/>
        <source>Delete</source>
        <translation>སུབ་དགོས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="161"/>
        <source>Delete the file safe &quot;%1&quot; permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="195"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="283"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="284"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="346"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="347"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="355"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="356"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="367"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="368"/>
        <source>umount is error</source>
        <translation>ཝུའུ་ཁི་ལན་ནི་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="388"/>
        <source>Password is error</source>
        <translation type="unfinished">གསང་གྲངས་ནོར་འདུག</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation type="vanished">ནོར་འཁྲུལ་གྱི་གསང་གྲངས</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../CreateBoxOprInPeony.cpp" line="83"/>
        <source>Create box failed</source>
        <translation>འཆིང་རྒྱ་གསར་འཛུགས་བྱས་ནས་ཕམ་ཁ</translation>
    </message>
    <message>
        <source>Create global key failed</source>
        <translation type="vanished">ཁྱོན་ཡོངས་ཀྱི་མཐེབ་བཀྱག་གསར་འཛུགས་བྱས་ནས</translation>
    </message>
    <message>
        <source>Delete box failed</source>
        <translation type="vanished">འཆིང་རྒྱ་མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../CRenameBoxOprInManager.cpp" line="87"/>
        <location filename="../DeleteBoxOprInPeony.cpp" line="91"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings::BoxSettingTranslate</name>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="75"/>
        <source>password strength strategy</source>
        <translation type="unfinished">གསང་གྲངས་དྲག་ཚད་ཐབས་ཇུས།</translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="78"/>
        <source>defender force prevention</source>
        <translation type="unfinished">དྲག་ཤུགས་ལ་བརྟེན་ནས་སེལ་ཐབས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="81"/>
        <source>lock automatic screen locker</source>
        <translation type="unfinished">ཟྭ་རང་འགུལ་གྱིས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="84"/>
        <source>lock timing</source>
        <translation type="unfinished">དུས་ལྟར་ཟྭ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>Settings::BoxSettingWidget</name>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="49"/>
        <source>Settings</source>
        <translation type="unfinished">སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="75"/>
        <source>Cancel</source>
        <translation type="unfinished">མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="81"/>
        <source>Confirm</source>
        <translation type="unfinished">གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>Settings::RightUIFactory</name>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="271"/>
        <source> (current value)</source>
        <translation type="unfinished">（མིག་སྔའི་རིན་ཐང་།）</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="328"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="356"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="373"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="417"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="431"/>
        <source>open</source>
        <translation type="unfinished">འགོ་བརྩམས་པ།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="328"/>
        <source>Improve password strength and reduce the risk of user privacy data leakage</source>
        <translation type="unfinished">གསང་གྲངས་ཇེ་མཐོར་བཏང་ནས་སྤྱོད་མཁན་གྱི་གསང་བའི་གཞི་གྲངས་ཕྱིར་བསྒྲགས་པའི་ཉེན་ཁ་ཇེ་ཆུང་དུ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="329"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="357"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="374"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="418"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="433"/>
        <source>close</source>
        <translation type="unfinished">སྒོ་གཏན་རོགས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="329"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="374"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="418"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="433"/>
        <source>After the shutdown, there is a risk of leakage of user data</source>
        <translation type="unfinished">སྒོ་རྒྱབ་རྗེས།སྤྱོད་མཁན་གྱི་གཞི་གྲངས་ལ་ཕྱིར་བསྒྲགས་པའི་ཉེན་ཁ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="330"/>
        <source>password strength</source>
        <translation type="unfinished">གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="347"/>
        <source> kinds</source>
        <translation type="unfinished">ས་བོན།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="349"/>
        <source>The type of password character</source>
        <translation type="unfinished">གསང་ཨང་ཡིག་རྟགས་ཀྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="352"/>
        <source>Minimum password length</source>
        <translation type="unfinished">གསང་གྲངས་ཀྱི་ཆེས་ཐུང་བའི་རིང་ཚད།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="352"/>
        <source>8 characters (default)</source>
        <translation type="unfinished">ཡིག་རྟགས8（ཁས་ལེན）</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="358"/>
        <source>The user name cannot be included</source>
        <translation type="unfinished">སྤྱོད་མཁན་གྱི་མིང་འདུས་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="373"/>
        <source>Effectively prevent a large number of password combinations from attempting to invade operations, and maintain data security</source>
        <translation type="unfinished">གསང་གྲངས་སྡེབ་སྒྲིག་འབོར་ཆེན་གྱིས་བཙན་འཛུལ་བཀོལ་སྤྱོད་བྱེད་པར་བཀག་འགོག་ནུས་ལྡན་དང་།གཞི་གྲངས་ཉེན་མེད་ཡོང་བར་སྲུང་སྐྱོང་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="375"/>
        <source>defender force prevention</source>
        <translation type="unfinished">དྲག་ཤུགས་ལ་བརྟེན་ནས་སེལ་ཐབས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="391"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="401"/>
        <source> times</source>
        <translation type="unfinished">ཚེ་རབ།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="393"/>
        <source>Number of initial lock errors</source>
        <translation type="unfinished">ནོར་འཁྲུལ་གྱི་ཐེངས་གྲངས་ཐོག་མར་གཏན་འཁེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="396"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="450"/>
        <source> minutes</source>
        <translation type="unfinished">སྐར་མ།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="398"/>
        <source>First lock time</source>
        <translation type="unfinished">ཐོག་དང་པོར་དུས་ཚོད་གཏན་འཁེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="403"/>
        <source>Maximum number of errors</source>
        <translation type="unfinished">ནོར་འཁྲུལ་བྱུང་ཚད་ཆེ་ཤོས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="417"/>
        <source>Lock the screen to wake up the desktop, you need to re-enter the password of the protection box</source>
        <translation type="unfinished">འཆར་ངོས་ཟྭ་ཡིས་སྒྲོག་ངོས་སད་དུ་འཇུག་སྐབས།ཡང་བསྐྱར་སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་གྲངས་ནང་འཇུག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="419"/>
        <source>The lock screen automatically locks</source>
        <translation type="unfinished">ཟྭ་རང་འགུལ་གྱིས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="432"/>
        <source>If the protection box is not used for a period of time, the protection box will be automatically locked, and the password needs to be re-entered when you enter it again</source>
        <translation type="unfinished">དུས་ཚོད་ཅིག་གི་ནང་དུ་མ་བཀོལ་ན་སྲུང་སྐྱོབ་སྒམ་རང་འགུལ་གྱིས་གཏན་ཁེལ་བྱས་ཏེ་ཡང་བསྐྱར་གསང་གྲངས་ཡང་བསྐྱར་ནང་འཇུག་བྱེད་དགོས་</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="434"/>
        <source>Timing lock</source>
        <translation type="unfinished">དུས་ལྟར་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="452"/>
        <source>Timeout for the first lock</source>
        <translation type="unfinished">ཐོག་དང་པོར་དུས་བརྒལ་དུས་ཚོད་གཏན་འཁེལ་བྱས།</translation>
    </message>
</context>
<context>
    <name>UmountBoxDialog</name>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="49"/>
        <source>After the file safe is locked, the content of the file in use may be lost. Please save it first!</source>
        <translation>ཡིག་ཆའི་ཉེན་འགོག་སྒམ་ལ་ཟྭ་བརྒྱབ་རྗེས་བེད་སྤྱོད་བྱེད་བཞིན་པའི་ཡིག་ཆའི་ནང་དོན་བོར་བརླག་ཏུ་འགྲོ་སྲིད། སྔོན་ལ་ཉར་ཚགས་ཡག་པོ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="50"/>
        <source>Enforce</source>
        <translation>བཙན་ཤེད་ཀྱིས་ལག་</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="51"/>
        <location filename="../UmountBoxDialog.cpp" line="173"/>
        <location filename="../UmountBoxDialog.cpp" line="176"/>
        <location filename="../UmountBoxDialog.cpp" line="179"/>
        <location filename="../UmountBoxDialog.cpp" line="302"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="58"/>
        <location filename="../UmountBoxDialog.cpp" line="90"/>
        <source>Lock</source>
        <translation>ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="147"/>
        <location filename="../UmountBoxDialog.cpp" line="148"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation>ཡིག་ཚགས་ཀྱི་ཉེན་འགོག་སྒམ་ནང་དུ་ཡིག་ཆ་བཟུང་ནས་ཟྭ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <source>*Forced locking will cause file loss. Please save the file first!</source>
        <translation type="vanished">*བཙན་ཤེད་ཀྱིས་ཟྭ་བརྒྱབ་ན་ཡིག་ཚགས་ལ་གྱོང་གུན་བཟོ་སྲིད། སྔོན་ལ་ཡིག་ཆ་དེ་ཉར་ཚགས་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="166"/>
        <location filename="../UmountBoxDialog.cpp" line="312"/>
        <source>Files being occupied (%1)</source>
        <translation>བཟུང་སྤྱོད་བྱེད་བཞིན་པའི་ཡིག་ཆ། (%1)</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="172"/>
        <location filename="../UmountBoxDialog.cpp" line="175"/>
        <location filename="../UmountBoxDialog.cpp" line="178"/>
        <location filename="../UmountBoxDialog.cpp" line="301"/>
        <source>Mandatory lock</source>
        <translation>བཙན་ཤེད་ཀྱིས་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="174"/>
        <location filename="../UmountBoxDialog.cpp" line="177"/>
        <location filename="../UmountBoxDialog.cpp" line="180"/>
        <location filename="../UmountBoxDialog.cpp" line="281"/>
        <location filename="../UmountBoxDialog.cpp" line="282"/>
        <location filename="../UmountBoxDialog.cpp" line="283"/>
        <source>Hide</source>
        <translation>སྦས་སྐུང་བྱེད་</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="274"/>
        <location filename="../UmountBoxDialog.cpp" line="275"/>
        <location filename="../UmountBoxDialog.cpp" line="276"/>
        <source>Display</source>
        <translation>འགྲེམས་སྟོན།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="322"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>box_task_process_dialog</name>
    <message>
        <location filename="../BoxTaskProcessDialog.ui" line="32"/>
        <source>Dialog</source>
        <translation type="unfinished">ཁ་བརྡ་བྱེད་པ།</translation>
    </message>
</context>
</TS>
