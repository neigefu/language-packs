<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BioProxy</name>
    <message>
        <location filename="../BioProxy.cpp" line="224"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="226"/>
        <source>FingerVein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠬᠡ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="228"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠩᠭᠠᠨ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="230"/>
        <source>Face</source>
        <translation>ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="232"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="234"/>
        <source>QRCode</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="245"/>
        <source>Unplugging of %1 device detected</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠰᠤᠭᠤᠯᠤᠭᠳᠠᠭᠰᠠᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="251"/>
        <source>%1 device insertion detected</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="261"/>
        <source>ukui-biometric-manager</source>
        <translation>UKUI ᠪᠢᠤᠯᠤᠬᠢ ᠵᠢᠨ ᠤᠨᠴᠠᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="264"/>
        <source>biometric</source>
        <translation>ᠪᠢᠤᠯᠤᠬᠢ ᠵᠢᠨ ᠤᠨᠴᠠᠯᠢᠭ</translation>
    </message>
</context>
<context>
    <name>BioWidget</name>
    <message>
        <location filename="../BioWidget.cpp" line="57"/>
        <source>The login options</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="288"/>
        <source>FingerPrint</source>
        <translation type="unfinished">ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="291"/>
        <source>FingerVein</source>
        <translation type="unfinished">ᠬᠤᠷᠤᠭᠤᠨ ᠤ᠋ ᠬᠡ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="294"/>
        <source>Iris</source>
        <translation type="unfinished">ᠰᠤᠯᠤᠩᠭᠠᠨ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="297"/>
        <source>Face</source>
        <translation type="unfinished">ᠨᠢᠭᠤᠷ ᠴᠢᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="300"/>
        <source>VoicePrint</source>
        <translation type="unfinished">ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="303"/>
        <source>QRCode</source>
        <translation type="unfinished">ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="332"/>
        <location filename="../BioWidget.cpp" line="337"/>
        <location filename="../BioWidget.cpp" line="342"/>
        <location filename="../BioWidget.cpp" line="348"/>
        <location filename="../BioWidget.cpp" line="354"/>
        <source>Verify %1</source>
        <translation type="unfinished">ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ%1</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="359"/>
        <source>WeChat scanning code</source>
        <translation type="unfinished">ᠤᠨ ᠺᠣᠳ᠋ ᠰᠢᠷᠪᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BioWidget.cpp" line="369"/>
        <source> or enter password to unlock</source>
        <translation type="unfinished"> ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BoxCreateDialog</name>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="37"/>
        <source>Create</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="vanished">新建保护箱</translation>
    </message>
    <message>
        <source>Encrypt       </source>
        <oldsource>Encrypt     </oldsource>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ       </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="115"/>
        <source>Name          </source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ          </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="116"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="147"/>
        <source>At least %1 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="166"/>
        <location filename="../BoxCreateDialog.cpp" line="168"/>
        <location filename="../BoxCreateDialog.cpp" line="171"/>
        <location filename="../BoxCreateDialog.cpp" line="525"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="381"/>
        <location filename="../BoxCreateDialog.cpp" line="572"/>
        <source>Box name has been exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password length cannot less than 8</source>
        <translation type="obsolete">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 8 ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="599"/>
        <source>Failed to get pwquality conf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancle</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="117"/>
        <source>Confirm </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="167"/>
        <location filename="../BoxCreateDialog.cpp" line="169"/>
        <location filename="../BoxCreateDialog.cpp" line="172"/>
        <location filename="../BoxCreateDialog.cpp" line="526"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Passwd level</source>
        <translation type="vanished">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="366"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation>ᠬᠦᠷᠢᠶ᠎ᠡ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠬᠤᠭᠤᠰᠤᠨ ᠡᠰᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Box name has been exit</source>
        <oldsource>Box name has been exit!</oldsource>
        <translation type="vanished">ᠬᠦᠷᠢᠶ᠎ᠡ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Create box is failed</source>
        <oldsource>Create box is failed!</oldsource>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="374"/>
        <source>Password cannot be empty</source>
        <oldsource>Password cannot be empty!</oldsource>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="vanished">ᠠᠳᠠᠭ᠎ᠲᠠᠭᠠᠨ 8 ᠣᠷᠣᠨᠲᠤ ᠲᠣᠮᠣ ᠪᠢᠴᠢᠯᠭᠡ ᠂ ᠵᠢᠵᠢᠭ ᠪᠢᠴᠢᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠦᠰᠦᠭ ᠂ ᠲᠣᠭ᠎ᠠ ᠂ ᠣᠨᠴᠠᠭᠠᠢ ᠲᠡᠮᠳᠡᠭ </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="142"/>
        <source>Unlock password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠵᠠᠳᠠᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="219"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation>ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠯ ᠢ ᠺᠣᠯᠧᠨ ᠺᠯᠦᠢᠨ ᠳᠠᠩᠰᠠᠨ ᠤ ᠡᠷᠦᠬᠡ ᠳᠦ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠬᠦᠷᠲᠡᠯ᠎ᠡ ᠬᠠᠮᠲᠤ ᠳᠤᠨᠢ ᠬᠦᠷᠦᠭᠦᠯᠵᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="318"/>
        <location filename="../BoxCreateDialog.cpp" line="319"/>
        <location filename="../BoxCreateDialog.cpp" line="332"/>
        <location filename="../BoxCreateDialog.cpp" line="333"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Password length can not be less than 6</source>
        <oldsource>Passwd length can not be less than 6</oldsource>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 6 ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Password length can not be less than 8</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 8 ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="393"/>
        <location filename="../BoxCreateDialog.cpp" line="589"/>
        <source>Password can not contain box name</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠢ ᠪᠠᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Password  can not contain user name</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠪᠠᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Password characters must contain at least two types</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠳᠠᠭ ᠲᠠᠭᠠᠨ ᠬᠣᠶᠠᠷ ᠵᠦᠢᠯ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="400"/>
        <location filename="../BoxCreateDialog.cpp" line="703"/>
        <source>Confirm password cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠢᠰᠢ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="407"/>
        <location filename="../BoxCreateDialog.cpp" line="710"/>
        <source>Password is not same as verify password</source>
        <oldsource>Password is not same as verify password!</oldsource>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠯᠤᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢᠵᠢᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <source>Create globalKey failed</source>
        <oldsource>Create globalKey failed!</oldsource>
        <translation type="vanished">ᠪᠦᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="501"/>
        <location filename="../BoxCreateDialog.cpp" line="544"/>
        <location filename="../BoxCreateDialog.cpp" line="551"/>
        <location filename="../BoxCreateDialog.cpp" line="558"/>
        <location filename="../BoxCreateDialog.cpp" line="565"/>
        <source>Invaild name</source>
        <translation>ᠡᠰᠡ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠤᠨᠴᠤᠭᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="obsolete">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠤᠷᠲᠤ ᠨᠢ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="obsolete">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠨᠢ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠲᠥᠷᠥᠯ᠎ᠢ᠋ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="664"/>
        <location filename="../BoxCreateDialog.cpp" line="665"/>
        <source>Invaild password</source>
        <translation>ᠡᠰᠡ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠤᠨᠴᠤᠭᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
    <message>
        <source>Passwd level low</source>
        <translation type="vanished">密码强度低</translation>
    </message>
    <message>
        <source>Passwd level mid</source>
        <translation type="vanished">密码强度中</translation>
    </message>
    <message>
        <source>Passwd level high</source>
        <translation type="vanished">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxItemDelegate</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
</context>
<context>
    <name>BoxKeyExportDialog</name>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="82"/>
        <location filename="../BoxKeyExportDialog.cpp" line="84"/>
        <source>Transparent Box</source>
        <translation>ᠲᠤᠩᠭᠠᠯᠠᠭ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="82"/>
        <location filename="../BoxKeyExportDialog.cpp" line="84"/>
        <source>Encrypt Box</source>
        <translation>ᠬᠠᠢ᠌ᠷᠴᠠᠭ ᠢ ᠨᠢᠭᠲᠠᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="93"/>
        <source>File Safe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="112"/>
        <source>Create Key</source>
        <translation>ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠭᠦᠷ ᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="118"/>
        <source>Select key save path</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠪᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="125"/>
        <source>If forget password for the box, you can use the key to reset the password</source>
        <translation>ᠬᠡᠷᠪᠡ ᠪᠣᠭᠣᠳᠠᠯ ᠤᠨ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠮᠠᠷᠲᠠᠪᠠᠯ ᠂ ᠲᠠ ᠨᠢᠭᠤᠴᠠ ᠨᠤᠮᠸᠷ ᠢ ᠳᠠᠬᠢᠨ ᠲᠠᠯᠪᠢᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="130"/>
        <source>Save Path</source>
        <translation>ᠵᠠᠮ ᠮᠥᠷ ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="137"/>
        <location filename="../BoxKeyExportDialog.cpp" line="159"/>
        <location filename="../BoxKeyExportDialog.cpp" line="323"/>
        <source>Browser</source>
        <translation>ᠦᠵᠡᠭᠦᠯᠦᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="148"/>
        <location filename="../BoxKeyExportDialog.cpp" line="157"/>
        <location filename="../BoxKeyExportDialog.cpp" line="321"/>
        <source>Confirm</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="153"/>
        <location filename="../BoxKeyExportDialog.cpp" line="158"/>
        <location filename="../BoxKeyExportDialog.cpp" line="228"/>
        <location filename="../BoxKeyExportDialog.cpp" line="322"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="195"/>
        <location filename="../BoxKeyExportDialog.cpp" line="204"/>
        <source>Key file cannot write</source>
        <translation>ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠭᠰᠡᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠪᠢᠴᠢᠵᠦ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Create globalKey failed</source>
        <translation type="obsolete">ᠪᠦᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="134"/>
        <source>Use default key path</source>
        <translation>ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠶᠢ ᠲᠠᠨᠢᠬᠤ ᠵᠠᠮ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="218"/>
        <source>save key file</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="225"/>
        <source>FileName(N):</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ (N)</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="226"/>
        <source>FileType:</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.cpp" line="227"/>
        <source>Save(S)</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="82"/>
        <source>选择密钥保存路径</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠪᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="107"/>
        <source>如果忘记保护箱密码，可以使用密钥重置保护箱密码</source>
        <translation>ᠬᠡᠷᠪᠡ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠪᠠᠨ ᠮᠠᠷᠲᠠᠪᠠᠯ ᠂ ᠨᠢᠭᠤᠴᠠ ᠨᠤᠮᠸᠷ ᠢ ᠳᠠᠬᠢᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="143"/>
        <source>保存路径</source>
        <translation>ᠵᠠᠮ ᠮᠥᠷ ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="169"/>
        <source>无效密钥路径</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠭᠰᠡᠨ ᠵᠠᠮ ᠮᠥᠷ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="218"/>
        <source>浏览</source>
        <translation>ᠦᠵᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="281"/>
        <source>取消</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../BoxKeyExportDialog.ui" line="300"/>
        <source>确认</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>BoxLoadingMessageBox</name>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="32"/>
        <source>File Safe</source>
        <translation type="unfinished">ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="80"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="81"/>
        <source>View</source>
        <translation>ᠵᠢᠷᠤᠭ ᠢ ᠬᠠᠷᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="93"/>
        <source>Export succeed!</source>
        <translation>ᠠᠮᠵᠢᠯᠲᠠ᠎ᠲᠠᠢ ᠵᠠᠯᠠᠵᠤ ᠭᠠᠷᠭᠠᠪᠠ！</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="99"/>
        <source>Import succeed!</source>
        <translation>ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠪᠠ！</translation>
    </message>
    <message>
        <source>Export failed! The path you have selected does not have permission</source>
        <translation type="vanished">ᠢᠯᠠᠭᠳᠠᠯ ᠢ ᠤᠳᠤᠷᠢᠳᠤᠨ᠎ᠠ ! ᠲᠠᠨ ᠤ ᠰᠣᠩᠭᠣᠬᠤ ᠵᠠᠮ ᠳᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="134"/>
        <source>Import failed! The selected file is not file safe box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="117"/>
        <source>Export failed!</source>
        <translation>ᠡᠭᠦᠰᠦᠮᠡᠯ ᠢᠯᠠᠭᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="122"/>
        <source>Import failed!</source>
        <translation>ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../BoxLoadingMessageBox.cpp" line="75"/>
        <location filename="../BoxLoadingMessageBox.cpp" line="76"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BoxMessageDialog</name>
    <message>
        <source>Password setting is successful!</source>
        <translation type="obsolete">密码修改成功！</translation>
    </message>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="25"/>
        <source>Ok</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxMessageDialog.cpp" line="29"/>
        <source>File Safe</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠃</translation>
    </message>
</context>
<context>
    <name>BoxOccupiedTipDialog</name>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="20"/>
        <source>Lock</source>
        <translation type="unfinished">ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="34"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="141"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="142"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation type="unfinished">ᠤᠳᠤᠬᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠲᠡᠬᠢ ᠹᠠᠢᠯ ᠠᠰᠢᠭ᠋ᠯᠠᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠠᠰᠢᠭᠯᠠᠯᠳᠠ ᠡᠴᠡ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠤᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="42"/>
        <source>Rename</source>
        <translation type="unfinished">ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="160"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="306"/>
        <source>Files being occupied (%1)</source>
        <translation type="unfinished">ᠶᠠᠭ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ (%1)</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="35"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="166"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="167"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="170"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="173"/>
        <source>Mandatory lock</source>
        <translation type="unfinished">ᠠᠯᠪᠠ ᠪᠡᠷ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="36"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="86"/>
        <source>Lock box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="40"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="41"/>
        <source>Mandatory rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="45"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="46"/>
        <source>Mandatory delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="47"/>
        <source>Delete Box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="168"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="171"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="174"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="296"/>
        <source>Cancel</source>
        <translation type="unfinished">ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="169"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="172"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="175"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="276"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="277"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="278"/>
        <source>Hide</source>
        <translation type="unfinished">ᠨᠢᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="269"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="270"/>
        <location filename="../BoxOccupiedTipDialog.cpp" line="271"/>
        <source>Display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxOccupiedTipDialog.cpp" line="316"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxPasswdSetting</name>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="46"/>
        <location filename="../BoxPasswdSetting.cpp" line="116"/>
        <source>Password setting</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="205"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="206"/>
        <location filename="../BoxPasswdSetting.cpp" line="449"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="207"/>
        <source>New Password</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="BoxPasswdSetting.cpp" line="178"/>
        <source></source>
        <oldsource>Confirm password</oldsource>
        <translation></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="244"/>
        <location filename="../BoxPasswdSetting.cpp" line="246"/>
        <location filename="../BoxPasswdSetting.cpp" line="248"/>
        <location filename="../BoxPasswdSetting.cpp" line="1050"/>
        <location filename="../BoxPasswdSetting.cpp" line="1362"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Please update the psw</source>
        <translation type="vanished">请导入密钥文件</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">上传</translation>
    </message>
    <message>
        <source>Psw</source>
        <translation type="vanished">密钥</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="869"/>
        <location filename="../BoxPasswdSetting.cpp" line="870"/>
        <location filename="../BoxPasswdSetting.cpp" line="1156"/>
        <location filename="../BoxPasswdSetting.cpp" line="1158"/>
        <source>Box umount failed</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠤᠨᠢᠰᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="208"/>
        <source>Confirm     </source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ     </translation>
    </message>
    <message>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="vanished">ᠠᠳᠠᠭ᠎ᠲᠠᠭᠠᠨ 8 ᠣᠷᠣᠨᠲᠤ ᠲᠣᠮᠣ ᠪᠢᠴᠢᠯᠭᠡ ᠂ ᠵᠢᠵᠢᠭ ᠪᠢᠴᠢᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠦᠰᠦᠭ ᠂ ᠲᠣᠭ᠎ᠠ ᠂ ᠣᠨᠴᠠᠭᠠᠢ ᠲᠡᠮᠳᠡᠭ </translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="243"/>
        <location filename="../BoxPasswdSetting.cpp" line="245"/>
        <location filename="../BoxPasswdSetting.cpp" line="247"/>
        <location filename="../BoxPasswdSetting.cpp" line="1361"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="267"/>
        <location filename="../BoxPasswdSetting.cpp" line="268"/>
        <location filename="../BoxPasswdSetting.cpp" line="456"/>
        <location filename="../BoxPasswdSetting.cpp" line="457"/>
        <location filename="../BoxPasswdSetting.cpp" line="483"/>
        <location filename="../BoxPasswdSetting.cpp" line="484"/>
        <source>Use default key path</source>
        <translation>ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠶᠢ ᠲᠠᠨᠢᠬᠤ ᠵᠠᠮ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="648"/>
        <location filename="../BoxPasswdSetting.cpp" line="649"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1176"/>
        <location filename="../BoxPasswdSetting.cpp" line="1177"/>
        <source>The secret key file cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1338"/>
        <source>File Safe</source>
        <translation type="unfinished">文件保护箱</translation>
    </message>
    <message>
        <source>Enter the user password to allow this operation</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Authenticate</source>
        <translation type="vanished">ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Biometric authentication</source>
        <translation type="vanished">ᠪᠢᠤᠯᠤᠬᠢ ᠵᠢ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation type="vanished">% 1 ᠰᠢᠯᠭᠠᠨ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠨᠢ ᠪᠠᠲᠤᠯᠠᠭᠳᠠᠬᠤ ᠦᠭᠡᠢ ᠂ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠪᠠᠰᠠ % 2 ᠤᠳᠠᠭᠠᠨ᠎ᠤ᠋ ᠲᠤᠷᠰᠢᠬᠤ ᠵᠠᠪᠰᠢᠶᠠᠨ ᠪᠠᠢᠨ᠎ᠠ n</translation>
    </message>
    <message>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation type="vanished">% 1᠎ᠶ᠋ᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠣᠨᠢᠰᠣ᠎ᠶ᠋ᠢ ᠲᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="854"/>
        <location filename="../BoxPasswdSetting.cpp" line="855"/>
        <source>Password can not be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Password authentication failed</source>
        <translation type="obsolete">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1361"/>
        <source> (O)</source>
        <translation> (O)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1362"/>
        <source> (C)</source>
        <translation> (C)</translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="278"/>
        <source>Import</source>
        <translation>ᠵᠠᠯᠠᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Password level</source>
        <oldsource>Passwd level</oldsource>
        <translation type="obsolete">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="478"/>
        <source>Secret key</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <source>Key file permission denied</source>
        <translation type="vanished">ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠭᠰᠡᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠨᠢ ᠲᠡᠪᠴᠢᠭ᠍ᠳᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="893"/>
        <location filename="../BoxPasswdSetting.cpp" line="904"/>
        <location filename="../BoxPasswdSetting.cpp" line="905"/>
        <source>Password is error</source>
        <oldsource>Passwd is error</oldsource>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>New password can not be empty</source>
        <oldsource>New passwd can not be empty</oldsource>
        <translation type="obsolete">新密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="915"/>
        <location filename="../BoxPasswdSetting.cpp" line="916"/>
        <location filename="../BoxPasswdSetting.cpp" line="1265"/>
        <location filename="../BoxPasswdSetting.cpp" line="1266"/>
        <source>New password cannot be same as old password</source>
        <oldsource>New passwd cannot be same as old passwd</oldsource>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠯᠤᠨ ᠬᠠᠭᠤᠴᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢᠵᠢᠯ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>New password length cannot less than 8</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 8 ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1238"/>
        <location filename="../BoxPasswdSetting.cpp" line="1240"/>
        <source>New password cannot be empty</source>
        <oldsource>New passwd cannot be empty</oldsource>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>New password length cannot less than 6</source>
        <oldsource>New passwd length cannot less than 6</oldsource>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 6 ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="925"/>
        <location filename="../BoxPasswdSetting.cpp" line="926"/>
        <location filename="../BoxPasswdSetting.cpp" line="1276"/>
        <location filename="../BoxPasswdSetting.cpp" line="1278"/>
        <source>Verify password length cannot be empty</source>
        <oldsource>Verify passwd length cannot be empty</oldsource>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠢᠰᠢ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="935"/>
        <location filename="../BoxPasswdSetting.cpp" line="936"/>
        <location filename="../BoxPasswdSetting.cpp" line="1287"/>
        <location filename="../BoxPasswdSetting.cpp" line="1289"/>
        <source>Verify password is not same as new password</source>
        <oldsource>Verify passwd is not same as new passwd</oldsource>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠯᠤᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢᠵᠢᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="657"/>
        <source>Password can not contain box name</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠢ ᠪᠠᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="131"/>
        <source>At least %1 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="270"/>
        <location filename="../BoxPasswdSetting.cpp" line="271"/>
        <location filename="../BoxPasswdSetting.cpp" line="459"/>
        <location filename="../BoxPasswdSetting.cpp" line="460"/>
        <location filename="../BoxPasswdSetting.cpp" line="486"/>
        <location filename="../BoxPasswdSetting.cpp" line="487"/>
        <source>Please select the key path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="632"/>
        <location filename="../BoxPasswdSetting.cpp" line="633"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password  can not contain user name</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠪᠠᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="681"/>
        <source>Failed to get pwquality conf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="691"/>
        <location filename="../BoxPasswdSetting.cpp" line="1249"/>
        <location filename="../BoxPasswdSetting.cpp" line="1251"/>
        <source>New password length cannot less than %1</source>
        <translation type="unfinished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠤᠷᠲᠤ ᠨᠢ ᠠᠳᠠᠭ ᠪᠠᠭ᠎ᠠ᠎ᠳ᠋ᠠᠭᠠᠨ %1 ᠣᠷᠣᠨ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="952"/>
        <location filename="../BoxPasswdSetting.cpp" line="954"/>
        <source>Password setting is failed</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="970"/>
        <location filename="../BoxPasswdSetting.cpp" line="972"/>
        <location filename="../BoxPasswdSetting.cpp" line="996"/>
        <location filename="../BoxPasswdSetting.cpp" line="998"/>
        <source>Mount is failed</source>
        <translation>ᠤᠨᠢᠰᠤ ᠵᠢ ᠳᠠᠢᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1038"/>
        <source>text file (*.txt)</source>
        <translation>ᠲᠸᠺᠰᠲ ᠹᠠᠢᠯ(*.txt)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1039"/>
        <source>all files (*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ (*)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1047"/>
        <source>FileName(N):</source>
        <translation>fileName(N):</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1048"/>
        <source>FileType:</source>
        <translation>fileType:</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1049"/>
        <source>Open</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1051"/>
        <source>Look in:</source>
        <translation>look in:</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠤᠷᠲᠤ ᠨᠢ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠨᠢ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠲᠥᠷᠥᠯ᠎ᠢ᠋ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Password setting is failed!</source>
        <oldsource>Passwd setting is failed!</oldsource>
        <translation type="obsolete">密码设置失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1035"/>
        <source>chose your file </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ </translation>
    </message>
    <message>
        <source>text file (*.txt);; all files (*);; </source>
        <oldsource>text file (*.xls *.txt);; all files (*.*);; </oldsource>
        <translation type="obsolete">文本文件(*.txt);; 所有文件 (*);;</translation>
    </message>
    <message>
        <source>The secret key file path can not be empty</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1188"/>
        <location filename="../BoxPasswdSetting.cpp" line="1189"/>
        <source>The secret key file is not exit</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1199"/>
        <location filename="../BoxPasswdSetting.cpp" line="1200"/>
        <source>The secret key file is unreadable</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠤᠩᠰᠢᠬᠤ ᠡᠷᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1228"/>
        <location filename="../BoxPasswdSetting.cpp" line="1229"/>
        <source>The secret key file is wrong</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1305"/>
        <location filename="../BoxPasswdSetting.cpp" line="1307"/>
        <source>Set password by secret key file failed</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1334"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1337"/>
        <source>Password setting is successful!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠵᠤ ᠴᠢᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>Invaild name</source>
        <translation type="vanished">不允许的特殊字符</translation>
    </message>
    <message>
        <source>Invaild password</source>
        <translation type="vanished">ᠡᠰᠡ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠤᠨᠴᠤᠭᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxRenameDialog</name>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="40"/>
        <location filename="../BoxRenameDialog.cpp" line="57"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="121"/>
        <location filename="../BoxRenameDialog.cpp" line="123"/>
        <location filename="../BoxRenameDialog.cpp" line="126"/>
        <location filename="../BoxRenameDialog.cpp" line="508"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="122"/>
        <location filename="../BoxRenameDialog.cpp" line="124"/>
        <location filename="../BoxRenameDialog.cpp" line="127"/>
        <location filename="../BoxRenameDialog.cpp" line="509"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="146"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="147"/>
        <source>New Name</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="148"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="230"/>
        <location filename="../BoxRenameDialog.cpp" line="231"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="283"/>
        <location filename="../BoxRenameDialog.cpp" line="284"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="329"/>
        <source>Password is error</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="352"/>
        <location filename="../BoxRenameDialog.cpp" line="353"/>
        <source>File safe rename failed</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="369"/>
        <location filename="../BoxRenameDialog.cpp" line="370"/>
        <source>Mount is failed</source>
        <translation>ᠤᠨᠢᠰᠤ ᠵᠢ ᠳᠠᠢᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="390"/>
        <location filename="../BoxRenameDialog.cpp" line="391"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Passwd cannot be empty!</source>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="429"/>
        <location filename="../BoxRenameDialog.cpp" line="430"/>
        <source>The new name cannot be the same as the original name</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠤᠤᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠲᠠᠢ ᠢᠵᠢᠯ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="440"/>
        <location filename="../BoxRenameDialog.cpp" line="441"/>
        <source>File Safe already exists</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="307"/>
        <location filename="../BoxRenameDialog.cpp" line="308"/>
        <source>Box umount failed</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠤᠨᠢᠰᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="294"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <source>File safe rename failed!</source>
        <translation type="obsolete">重命名保护箱失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="413"/>
        <location filename="../BoxRenameDialog.cpp" line="414"/>
        <location filename="../BoxRenameDialog.cpp" line="421"/>
        <location filename="../BoxRenameDialog.cpp" line="422"/>
        <source>Invaild name</source>
        <translation>ᠡᠰᠡ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠤᠨᠴᠤᠭᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
</context>
<context>
    <name>BoxTableModel</name>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
</context>
<context>
    <name>BoxTaskProcessDialog</name>
    <message>
        <location filename="../BoxTaskProcessDialog.cpp" line="31"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxTaskProcessDialog.cpp" line="81"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoxTypeSelectDialog</name>
    <message>
        <source>Close</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">ᠲᠣᠪᠶᠣᠭ</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="29"/>
        <source>Create</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="44"/>
        <source>Select box type:</source>
        <translation>ᠪᠣᠭᠣᠳᠠᠯ ᠤᠨ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠤᠨ ᠲᠥᠷᠥᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠰᠣᠩᠭᠣᠬᠤ ᠨᠢ ᠄</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="62"/>
        <source>Encrypt Box</source>
        <translation>ᠬᠠᠢ᠌ᠷᠴᠠᠭ ᠢ ᠨᠢᠭᠲᠠᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="89"/>
        <source>Transparent Box</source>
        <translation>ᠲᠤᠩᠭᠠᠯᠠᠭ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="67"/>
        <source>Use user set password protection, the protection box can only be used after unlocking with the user set password</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠳᠤ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠶᠢᠭ ᠢ ᠵᠥᠪᠬᠡᠨ ᠣᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠴᠢᠳᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="94"/>
        <source>Using system security password protection, automatically unlocking when users log in to the system</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠠᠶᠤᠯ ᠦᠭᠡᠶ ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠦ ᠭᠠᠷᠬᠤ ᠳᠠᠭᠠᠨ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠠᠷ ᠣᠨᠣᠰᠢᠯᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="120"/>
        <source>Confirm</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxTypeSelectDialog.cpp" line="112"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>请选择保护箱类型: </source>
        <translation type="vanished">ᠬᠠᠢ᠌ᠷᠴᠠᠭ ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ ᠃ </translation>
    </message>
    <message>
        <source>加密保护箱</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠢ ᠨᠢᠭᠲᠠᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>该操作将会导致计算机间断性宕机，宕机时间</source>
        <translation type="vanished">ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠨᠢ ᠺᠣᠮᠫᠢᠦᠢᠲ᠋ᠧᠷ ᠦᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣᠬᠢ ᠲᠠᠰᠤᠷᠠᠬᠤ ᠴᠢᠨᠠᠷ ᠤᠨ ᠨᠢᠰᠬᠡᠯ ᠂ ᠨᠢᠰᠬᠡᠯ ᠦᠨ ᠴᠠᠭ ᠢ ᠡᠭᠦᠰᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>透明加密保护箱</source>
        <translation type="vanished">ᠲᠤᠩᠭᠠᠯᠠᠭ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠴᠢ ᠶᠢ ᠢᠯᠡ ᠲᠣᠳᠣᠷᠬᠠᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠶᠢᠷᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <source>取消</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>确认</source>
        <translation type="vanished">ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>BuiltinBoxPasswdSetting</name>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="39"/>
        <source>Create Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="69"/>
        <source>Create</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="134"/>
        <source>Name          </source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ          </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="135"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="136"/>
        <source>Confirm </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="164"/>
        <source>Unlock password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠵᠠᠳᠠᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="166"/>
        <source>At least 8 digits, including uppercase and lowercase letters, numbers, and special symbols </source>
        <translation>ᠠᠳᠠᠭ᠎ᠲᠠᠭᠠᠨ 8 ᠣᠷᠣᠨᠲᠤ ᠲᠣᠮᠣ ᠪᠢᠴᠢᠯᠭᠡ ᠂ ᠵᠢᠵᠢᠭ ᠪᠢᠴᠢᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠦᠰᠦᠭ ᠂ ᠲᠣᠭ᠎ᠠ ᠂ ᠣᠨᠴᠠᠭᠠᠢ ᠲᠡᠮᠳᠡᠭ </translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="180"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="182"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="185"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="575"/>
        <source>Confirm</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="181"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="183"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="186"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="576"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="230"/>
        <source>Synchronize the key to the kylin Cloud account</source>
        <translation>ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠯ ᠢ ᠺᠣᠯᠧᠨ ᠺᠯᠦᠢᠨ ᠳᠠᠩᠰᠠᠨ ᠤ ᠡᠷᠦᠬᠡ ᠳᠦ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠬᠦᠷᠲᠡᠯ᠎ᠡ ᠬᠠᠮᠲᠤ ᠳᠤᠨᠢ ᠬᠦᠷᠦᠭᠦᠯᠵᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="300"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="301"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="314"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="315"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="344"/>
        <source>Password cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="352"/>
        <source>Password length can not be less than 8</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 8 ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="360"/>
        <source>Password can not contain box name</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠢ ᠪᠠᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="367"/>
        <source>Password  can not contain user name</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠪᠠᠭᠲᠠᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="379"/>
        <source>Password characters must contain at least two types</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠳᠠᠭ ᠲᠠᠭᠠᠨ ᠬᠣᠶᠠᠷ ᠵᠦᠢᠯ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="386"/>
        <source>Confirm password cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠢᠰᠢ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="393"/>
        <source>Password is not same as verify password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠯᠤᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢᠵᠢᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="409"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="411"/>
        <source>Password setting is failed</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="456"/>
        <source>Ok</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="459"/>
        <source>Password setting is successful!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠵᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="460"/>
        <source>File Safe</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="485"/>
        <location filename="../BuiltinBoxPasswdSetting.cpp" line="486"/>
        <source>Invaild password</source>
        <translation>ᠡᠰᠡ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠤᠨᠴᠤᠭᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <source>The password length does not meet the requirements</source>
        <translation type="obsolete">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠤᠷᠲᠤ ᠨᠢ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>The password contains characters that do not meet the requirements</source>
        <translation type="obsolete">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠨᠢ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠲᠥᠷᠥᠯ᠎ᠢ᠋ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>CBoxCompatibilityUpgradeOperation</name>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Upgrade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Box upgrading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="28"/>
        <source>Box compatibility upgrading, no closing!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxCompatibilityUpgradeOperation.cpp" line="36"/>
        <source>Box compatibility upgrade failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CBoxUnlockAuthDialog</name>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="67"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="69"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="588"/>
        <source>Open</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="143"/>
        <source>Import</source>
        <translation>ᠵᠠᠯᠠᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="166"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="168"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="542"/>
        <source>Key Unlock</source>
        <translation>ᠲᠦᠯᠭᠢᠭᠦᠷ ᠤᠨ ᠣᠨᠢᠰᠤ ᠶᠢ ᠲᠠᠶᠢᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="189"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="539"/>
        <source>Use password unlock &quot;%1&quot;</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢᠶᠡᠷ 《 1 》 ᠶᠢ ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="190"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="543"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="135"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="136"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="529"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="530"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="546"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="547"/>
        <source>Use default key path</source>
        <translation>ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠶᠢ ᠲᠠᠨᠢᠬᠤ ᠵᠠᠮ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="138"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="139"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="532"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="533"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="549"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="550"/>
        <source>Please select the key path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="215"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="217"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="219"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="512"/>
        <source>Confirm</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="216"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="218"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="220"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="513"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="589"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="309"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="366"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="367"/>
        <source>umount is error</source>
        <translation>umount ᠪᠣᠯ ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="380"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="381"/>
        <source>Password can not be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="395"/>
        <source>Password is error</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="414"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="415"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="420"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="421"/>
        <source>Reset key can not be empty</source>
        <translation>ᠣᠷᠣᠨ ᠪᠠᠶᠢᠷᠢ ᠪᠠᠨ ᠳᠠᠬᠢᠨ ᠳᠠᠪᠲᠠᠨ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠲᠠᠷᠤᠭᠤᠯ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠶᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="430"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="431"/>
        <source>Invalid key file</source>
        <translation>ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠭᠰᠡᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="447"/>
        <location filename="../BoxUnlockAuthDialog.cpp" line="448"/>
        <source>The secret key file is wrong</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="522"/>
        <source>Use key unlock &quot;%1&quot;</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠪᠠᠷ 《 ア 》 ᠶᠢ ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="525"/>
        <source>Password Unlock</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢᠶᠡᠷ ᠣᠨᠢᠰᠤᠯᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="526"/>
        <source>Key</source>
        <translation>ᠲᠦᠯᠬᠢᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="574"/>
        <source>chose your file </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ </translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="577"/>
        <source>text file (*.txt)</source>
        <translation>ᠲᠸᠺᠰᠲ ᠹᠠᠢᠯ(*.txt)</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="578"/>
        <source>all files (*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ (*)</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="586"/>
        <source>FileName(N):</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ (N)</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="587"/>
        <source>FileType:</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../BoxUnlockAuthDialog.cpp" line="590"/>
        <source>Look in:</source>
        <translation>ᠭᠦᠢ </translation>
    </message>
    <message>
        <source>Please import the secret key file</source>
        <translation type="obsolete">ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>CCreateKeyOperInPeony</name>
    <message>
        <location filename="../CreateKeyOperInPeony.cpp" line="52"/>
        <source>Create global key failed</source>
        <translation type="unfinished">ᠪᠦᠬᠦ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ ᠢᠯᠠᠭᠳᠠᠯ ᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>CDeleteBoxOprInPeony</name>
    <message>
        <location filename="../DeleteBoxOprInPeony.cpp" line="129"/>
        <source>Delete box failed</source>
        <translation type="unfinished">ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠬᠠᠰᠤᠵᠤ ᠢᠯᠠᠭᠳᠠᠯ ᠢ ᠬᠠᠰᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>CExportAuthCred</name>
    <message>
        <source>critical</source>
        <translation type="obsolete">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <source>Export failed</source>
        <translation type="obsolete">ᠡᠭᠦᠰᠦᠮᠡᠯ ᠢᠯᠠᠭᠳᠠᠯ</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="obsolete">ᠰᠤᠷᠠᠭ</translation>
    </message>
    <message>
        <source>Export succeed</source>
        <translation type="obsolete">ᠠᠮᠵᠢᠯᠲᠠ᠎ᠲᠠᠢ ᠵᠠᠯᠠᠵᠤ ᠭᠠᠷᠭᠠᠪᠠ！</translation>
    </message>
</context>
<context>
    <name>CImportBoxOprInPeony</name>
    <message>
        <source>critical</source>
        <translation type="obsolete">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <source>Import failed</source>
        <translation type="obsolete">ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="obsolete">ᠰᠤᠷᠠᠭ</translation>
    </message>
    <message>
        <source>Import succeed</source>
        <translation type="obsolete">ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠪᠠ</translation>
    </message>
</context>
<context>
    <name>CTitleBar</name>
    <message>
        <location filename="../TitleBar.cpp" line="128"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="133"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="138"/>
        <source>Options</source>
        <translation>ᠰᠣᠩᠭᠣᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">ᠲᠣᠪᠶᠣᠭ</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="143"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ExportBoxGetPwdDialog</name>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="52"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="89"/>
        <source>Export filesafe box</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="103"/>
        <source>set %1 export passwd</source>
        <translation>% 1᠎ᠦ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="107"/>
        <source>This password is used to encrypt exported files</source>
        <translation>ᠲᠤᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠨᠢ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ᠎ᠢ᠋ ᠨᠢᠭᠤᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠠ᠋</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="158"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="160"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="163"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="345"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="376"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="380"/>
        <source>Export</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="159"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="161"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="164"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="346"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="381"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="371"/>
        <source>save kybox file</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="378"/>
        <source>FileName(N):</source>
        <translation>ᠹᠠᠢᠯ&#x202f;ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ; ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ&#x202f;ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="379"/>
        <source>FileType:</source>
        <translation>ᠹᠠᠢᠯ&#x202f;ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <source>critical</source>
        <translation type="vanished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <source>Export failed</source>
        <translation type="vanished">ᠡᠭᠦᠰᠦᠮᠡᠯ ᠢᠯᠠᠭᠳᠠᠯ</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="vanished">ᠰᠤᠷᠠᠭ</translation>
    </message>
    <message>
        <source>Export succeed</source>
        <translation type="vanished">ᠠᠮᠵᠢᠯᠲᠠ᠎ᠲᠠᠢ ᠵᠠᠯᠠᠵᠤ ᠭᠠᠷᠭᠠᠪᠠ！</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="185"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="256"/>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="257"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="315"/>
        <source> Export passwd cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠡᠭᠦᠰᠬᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ExportBoxGetPwdDialog.cpp" line="317"/>
        <source>Export passwd cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠡᠭᠦᠰᠬᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>ExportBoxLoadingDialog</name>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="41"/>
        <source>File Safe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="64"/>
        <source>Exporting, please wait...</source>
        <translation>ᠵᠠᠯᠠᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠳᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="116"/>
        <location filename="../ExportBoxLoadingDialog.cpp" line="117"/>
        <source>Exporting %1 to %2 </source>
        <translation>% 1᠎ᠡᠴᠡ 2 % ᠬᠦᠷᠲᠡᠯ᠎ᠡ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ExportBoxLoadingDialog.cpp" line="126"/>
        <location filename="../ExportBoxLoadingDialog.cpp" line="127"/>
        <source>Importing %1 ...</source>
        <translation>% 1᠎ᠶ᠋ᠢ ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ExportPamAuthenticDialog</name>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="232"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="236"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="533"/>
        <source>User authentication is required to perform this operation</source>
        <translation>ᠲᠤᠰ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠰᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="243"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="244"/>
        <source>Enter the user password to allow this operation</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="267"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="268"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="270"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="506"/>
        <source>Authenticate</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="273"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="274"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="277"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="278"/>
        <source>Biometric authentication</source>
        <translation>ᠪᠢᠤᠯᠤᠬᠢ ᠵᠢ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="339"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="398"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation>% 1 ᠰᠢᠯᠭᠠᠨ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠨᠢ ᠪᠠᠲᠤᠯᠠᠭᠳᠠᠬᠤ ᠦᠭᠡᠢ ᠂ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠪᠠᠰᠠ % 2 ᠤᠳᠠᠭᠠᠨ᠎ᠤ᠋ ᠲᠤᠷᠰᠢᠬᠤ ᠵᠠᠪᠰᠢᠶᠠᠨ ᠪᠠᠢᠨ᠎ᠠ n</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="403"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation>% 1᠎ᠶ᠋ᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠣᠨᠢᠰᠣ᠎ᠶ᠋ᠢ ᠲᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="484"/>
        <location filename="../ExportPamAuthenticDialog.cpp" line="485"/>
        <source>Password can not be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ExportPamAuthenticDialog.cpp" line="548"/>
        <source>Password authentication failed</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>FirstCreatBoxMessageBox</name>
    <message>
        <source>File Safe</source>
        <translation type="vanished">文件保护箱</translation>
    </message>
    <message>
        <source>This is your first creat the protective box,please save your file!</source>
        <translation type="vanished">请妥善保存密钥文件，如果忘记密码，可以使用该密钥文件进行密码找回。</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="38"/>
        <source>Please keep the key file properly. If you forget the password, you can use the key file to retrieve the password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠠᠢᠳᠤᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠠᠷᠠᠢ᠂ ᠬᠡᠷᠪᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠮᠠᠷᠳᠠᠪᠠᠯ᠂ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠹᠠᠢᠯ ᠵᠢᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠡᠷᠢᠵᠤ ᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="39"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="53"/>
        <source>save key file</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>key file(*.txt)</source>
        <translation type="vanished">密钥文件（*.txt）</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="60"/>
        <source>FileName(N):</source>
        <translation>fileName(N):</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="61"/>
        <source>FileType:</source>
        <translation>fileType:</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="62"/>
        <source>Save(S)</source>
        <translation>save(S)</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="63"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="146"/>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="158"/>
        <source>critical</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="146"/>
        <source>save path failed</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="158"/>
        <source>Disallowed special characters</source>
        <translation>ᠡᠰᠡ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠤᠨᠴᠤᠭᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <source>Save Path</source>
        <translation type="vanished">保存文件</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>ImportBoxDialog</name>
    <message>
        <source>Import filesafe box</source>
        <translation type="vanished">ᠣᠷᠣᠭᠤᠯᠬᠤ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="vanished">ᠵᠠᠯᠠᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>New Name</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>Password length can not be higer than 32</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Box name cannot be empty</source>
        <translation type="vanished">ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠶᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source> Import passwd cannot be empty</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤᠷᠤᠭᠤᠯᠳᠠ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Import passwd cannot be empty</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤᠷᠤᠭᠤᠯᠳᠠ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Unzip password error</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠠᠷᠢᠯᠭᠠᠬᠤ ᠪᠤᠷᠤᠭᠤ ᠶᠢ ᠵᠠᠳᠠᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Box name has been exit</source>
        <translation type="obsolete">ᠬᠦᠷᠢᠶ᠎ᠡ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>critical</source>
        <translation type="vanished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <source>Import failed</source>
        <translation type="vanished">ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>information</source>
        <translation type="vanished">ᠰᠤᠷᠠᠭ</translation>
    </message>
    <message>
        <source>Import succeed</source>
        <translation type="vanished">ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <source>chose kybox file</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠤᠷ ᠬᠠᠢᠷᠴᠠᠭ᠎ᠤ᠋ᠨ ᠹᠠᠢᠯ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>kybox file (*.kybox)</source>
        <translation type="vanished">kybox ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ( *.kybx )</translation>
    </message>
    <message>
        <source>FileName(N):</source>
        <translation type="vanished">ᠹᠠᠢᠯ&#x202f;ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>FileType:</source>
        <translation type="vanished">ᠹᠠᠢᠯ&#x202f;ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <source>Look in:</source>
        <translation type="vanished">ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Please select a box for import operation</source>
        <translation type="vanished">ᠨᠢᠭᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠤᠷ ᠬᠠᠢᠷᠴᠠᠭ ᠰᠣᠩᠭᠣᠵᠤ ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯ</translation>
    </message>
    <message>
        <source>Box name %1 has been exist, please modify the name of the box and import it</source>
        <translation type="vanished">ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ᠎ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ % 1 ᠨᠢᠭᠡᠨᠲᠡ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠣᠢ ᠂ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ</translation>
    </message>
    <message>
        <source>set %1 Import unzip passwd</source>
        <translation type="obsolete">%1 ᠎ᠦ᠌ ᠵᠠᠯᠠᠵᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠰᠤᠯᠠᠯᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>set %1 Import passwd</source>
        <translation type="vanished">% 1᠎ᠦ᠌ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>LibBox::ExImportSettingDialog</name>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="30"/>
        <source>Export box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="31"/>
        <source>Import box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="42"/>
        <source>File storage location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="43"/>
        <source>Select the file you want to import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="58"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="75"/>
        <source>The password for the box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="95"/>
        <source>Set the password for unpacking the box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="96"/>
        <source>Input the password for unpacking the box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="123"/>
        <source>Please enter a new box name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="133"/>
        <source>Invalid name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="147"/>
        <source>Cancel</source>
        <translation type="unfinished">ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ex-import/eximportsettingdialog.cpp" line="152"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibBox::ExportDialog</name>
    <message>
        <location filename="../ex-import/exportdialog.cpp" line="39"/>
        <source>File Select</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibBox::ImportDialog</name>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="76"/>
        <source>The unpack password was entered incorrectly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="95"/>
        <location filename="../ex-import/importdialog.cpp" line="156"/>
        <source>Box name %1 is existed, please modify the name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="109"/>
        <source>File Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ex-import/importdialog.cpp" line="92"/>
        <location filename="../ex-import/importdialog.cpp" line="154"/>
        <source>Box name %1 is invalid, please modify the name.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ModuleSwitchButton</name>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="51"/>
        <location filename="../ModuleSwitchButton.cpp" line="237"/>
        <location filename="../ModuleSwitchButton.cpp" line="249"/>
        <source>Set by password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠩᠬᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="52"/>
        <source>Set by secret key</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠩᠭᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>PamAuthenticDialog</name>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="231"/>
        <location filename="../PamAuthenticDialog.cpp" line="235"/>
        <location filename="../PamAuthenticDialog.cpp" line="543"/>
        <source>User authentication is required to perform this operation</source>
        <translation>ᠲᠤᠰ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠰᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="242"/>
        <location filename="../PamAuthenticDialog.cpp" line="243"/>
        <source>Enter the user password to allow this operation</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="266"/>
        <location filename="../PamAuthenticDialog.cpp" line="267"/>
        <location filename="../PamAuthenticDialog.cpp" line="269"/>
        <location filename="../PamAuthenticDialog.cpp" line="516"/>
        <source>Authenticate</source>
        <oldsource>Authorization</oldsource>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="337"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="408"/>
        <source>%1 verification failed, You have %2 more tries</source>
        <translation type="unfinished">% 1 ᠰᠢᠯᠭᠠᠨ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠨᠢ ᠪᠠᠲᠤᠯᠠᠭᠳᠠᠬᠤ ᠦᠭᠡᠢ ᠂ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠪᠠᠰᠠ % 2 ᠤᠳᠠᠭᠠᠨ᠎ᠤ᠋ ᠲᠤᠷᠰᠢᠬᠤ ᠵᠠᠪᠰᠢᠶᠠᠨ ᠪᠠᠢᠨ᠎ᠠ n</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="413"/>
        <source>Unable to validate %1,Please enter the password to unlock</source>
        <translation>% 1᠎ᠶ᠋ᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠣᠨᠢᠰᠣ᠎ᠶ᠋ᠢ ᠲᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>You have %1 more tries!</source>
        <translation type="vanished">ᠲᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠠᠰᠠ %1 ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠳᠤᠷᠰᠢᠬᠤ ᠵᠠᠪᠰᠢᠢᠶᠠᠨ ᠲᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="494"/>
        <location filename="../PamAuthenticDialog.cpp" line="495"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Unable to login using a biometric device!</source>
        <translation type="vanished">ᠪᠢᠤᠯᠤᠬᠢ ᠵᠢᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠰᠢᠭᠯᠠᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="576"/>
        <source>Password authentication failed</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation type="obsolete">密码认证失败</translation>
    </message>
    <message>
        <source>authorization</source>
        <translation type="obsolete">授权</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="272"/>
        <location filename="../PamAuthenticDialog.cpp" line="273"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="275"/>
        <location filename="../PamAuthenticDialog.cpp" line="276"/>
        <source>Biometric authentication</source>
        <oldsource>Biometric</oldsource>
        <translation>ᠪᠢᠤᠯᠤᠬᠢ ᠵᠢ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>PasswdAuthDialog</name>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="63"/>
        <location filename="../PasswdAuthDialog.cpp" line="65"/>
        <source>Open</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="69"/>
        <location filename="../PasswdAuthDialog.cpp" line="71"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="132"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="133"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="162"/>
        <location filename="../PasswdAuthDialog.cpp" line="164"/>
        <location filename="../PasswdAuthDialog.cpp" line="166"/>
        <location filename="../PasswdAuthDialog.cpp" line="355"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="163"/>
        <location filename="../PasswdAuthDialog.cpp" line="165"/>
        <location filename="../PasswdAuthDialog.cpp" line="167"/>
        <location filename="../PasswdAuthDialog.cpp" line="356"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="223"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="278"/>
        <location filename="../PasswdAuthDialog.cpp" line="279"/>
        <source>umount is error</source>
        <translation>ᠠᠴᠢᠬᠤ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="292"/>
        <location filename="../PasswdAuthDialog.cpp" line="293"/>
        <source>Password can not be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="307"/>
        <source>Password is error</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>umount is error!</source>
        <translation type="vanished">锁定失败</translation>
    </message>
    <message>
        <source>Password can not be empty!</source>
        <oldsource>Passwd can not be empty!</oldsource>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <translation type="vanished">密码错误</translation>
    </message>
</context>
<context>
    <name>PasswdAuthMessagebox</name>
    <message>
        <source>Do you Confirm to delete the box</source>
        <translation type="vanished">你确认删除保护箱吗？</translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="obsolete">新建保护箱</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="31"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Delete the file safe %1 permanently?</source>
        <oldsource>Are you sure you want to delete the file s type=&quot;unfinished&quot;afe %1 permanently?</oldsource>
        <translation type="vanished">ᠲᠠ %1 ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠷᠡᠨ ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="161"/>
        <source>Delete the file safe &quot;%1&quot; permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="193"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="196"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="198"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="407"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="194"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="197"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="199"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="408"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="195"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="283"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="284"/>
        <source>Password length can not be higer than 32</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 32 ᠤᠷᠤᠨ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="346"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="347"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="367"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="368"/>
        <source>umount is error</source>
        <oldsource>umount is error!</oldsource>
        <translation>ᠤᠨᠢᠰᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="388"/>
        <source>Password is error</source>
        <translation type="unfinished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="355"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="356"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <oldsource>Password is error!</oldsource>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../CreateBoxOprInPeony.cpp" line="83"/>
        <source>Create box failed</source>
        <translation>ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <source>Create global key failed</source>
        <translation type="vanished">ᠪᠦᠬᠦ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ ᠢᠯᠠᠭᠳᠠᠯ ᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Delete box failed</source>
        <translation type="vanished">ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠬᠠᠰᠤᠵᠤ ᠢᠯᠠᠭᠳᠠᠯ ᠢ ᠬᠠᠰᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../CRenameBoxOprInManager.cpp" line="87"/>
        <location filename="../DeleteBoxOprInPeony.cpp" line="91"/>
        <source>Box has been changed, please retry</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings::BoxSettingTranslate</name>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="75"/>
        <source>password strength strategy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="78"/>
        <source>defender force prevention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="81"/>
        <source>lock automatic screen locker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingconfig.h" line="84"/>
        <source>lock timing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings::BoxSettingWidget</name>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="49"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="75"/>
        <source>Cancel</source>
        <translation type="unfinished">ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../setting-window/boxsettingwidget.cpp" line="81"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings::RightUIFactory</name>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="271"/>
        <source> (current value)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="328"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="356"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="373"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="417"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="431"/>
        <source>open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="328"/>
        <source>Improve password strength and reduce the risk of user privacy data leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="329"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="357"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="374"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="418"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="433"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="329"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="374"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="418"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="433"/>
        <source>After the shutdown, there is a risk of leakage of user data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="330"/>
        <source>password strength</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="347"/>
        <source> kinds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="349"/>
        <source>The type of password character</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="352"/>
        <source>Minimum password length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="352"/>
        <source>8 characters (default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="358"/>
        <source>The user name cannot be included</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="373"/>
        <source>Effectively prevent a large number of password combinations from attempting to invade operations, and maintain data security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="375"/>
        <source>defender force prevention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="391"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="401"/>
        <source> times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="393"/>
        <source>Number of initial lock errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="396"/>
        <location filename="../setting-window/right/rightuifactory.cpp" line="450"/>
        <source> minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="398"/>
        <source>First lock time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="403"/>
        <source>Maximum number of errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="417"/>
        <source>Lock the screen to wake up the desktop, you need to re-enter the password of the protection box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="419"/>
        <source>The lock screen automatically locks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="432"/>
        <source>If the protection box is not used for a period of time, the protection box will be automatically locked, and the password needs to be re-entered when you enter it again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="434"/>
        <source>Timing lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setting-window/right/rightuifactory.cpp" line="452"/>
        <source>Timeout for the first lock</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UmountBoxDialog</name>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="49"/>
        <source>After the file safe is locked, the content of the file in use may be lost. Please save it first!</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠤᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠤᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠬᠡᠬᠡᠭᠳᠡᠵᠤ ᠮᠠᠭᠠᠳ᠂ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="58"/>
        <location filename="../UmountBoxDialog.cpp" line="90"/>
        <source>Lock</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="147"/>
        <location filename="../UmountBoxDialog.cpp" line="148"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation>ᠤᠳᠤᠬᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠲᠡᠬᠢ ᠹᠠᠢᠯ ᠠᠰᠢᠭ᠋ᠯᠠᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠠᠰᠢᠭᠯᠠᠯᠳᠠ ᠡᠴᠡ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠤᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="174"/>
        <location filename="../UmountBoxDialog.cpp" line="177"/>
        <location filename="../UmountBoxDialog.cpp" line="180"/>
        <location filename="../UmountBoxDialog.cpp" line="281"/>
        <location filename="../UmountBoxDialog.cpp" line="282"/>
        <location filename="../UmountBoxDialog.cpp" line="283"/>
        <source>Hide</source>
        <translation>ᠨᠢᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="274"/>
        <location filename="../UmountBoxDialog.cpp" line="275"/>
        <location filename="../UmountBoxDialog.cpp" line="276"/>
        <source>Display</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>*Forced locking will cause file loss. Please save the file first!</source>
        <translation type="obsolete">*强制锁定会造成文件的丢失，请先保存文件！</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="50"/>
        <source>Enforce</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="172"/>
        <location filename="../UmountBoxDialog.cpp" line="175"/>
        <location filename="../UmountBoxDialog.cpp" line="178"/>
        <location filename="../UmountBoxDialog.cpp" line="301"/>
        <source>Mandatory lock</source>
        <translation>ᠠᠯᠪᠠ ᠪᠡᠷ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Please release these files which are openning!</source>
        <translation type="vanished">当前保护箱中有文件正在被占用,需要解除占用才能锁定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="166"/>
        <location filename="../UmountBoxDialog.cpp" line="312"/>
        <source>Files being occupied (%1)</source>
        <oldsource>files which are openning (%1)</oldsource>
        <translation>ᠶᠠᠭ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ (%1)</translation>
    </message>
    <message>
        <source>files which are openning!</source>
        <translation type="vanished">正在被打开的文件</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="322"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="51"/>
        <location filename="../UmountBoxDialog.cpp" line="173"/>
        <location filename="../UmountBoxDialog.cpp" line="176"/>
        <location filename="../UmountBoxDialog.cpp" line="179"/>
        <location filename="../UmountBoxDialog.cpp" line="302"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>box_task_process_dialog</name>
    <message>
        <location filename="../BoxTaskProcessDialog.ui" line="32"/>
        <source>Dialog</source>
        <translation type="unfinished">ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠃</translation>
    </message>
</context>
</TS>
