<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>CustomToolButton</name>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="216"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="226"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="217"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="228"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="218"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="229"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="219"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="231"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="34"/>
        <location filename="../UI/player/miniWidget.cpp" line="330"/>
        <location filename="../UI/player/miniWidget.cpp" line="674"/>
        <location filename="../UI/player/miniWidget.cpp" line="799"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="49"/>
        <location filename="../UI/player/miniWidget.cpp" line="50"/>
        <location filename="../UI/player/miniWidget.cpp" line="391"/>
        <location filename="../UI/player/miniWidget.cpp" line="782"/>
        <source>Loop</source>
        <translation>列表迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="51"/>
        <location filename="../UI/player/miniWidget.cpp" line="399"/>
        <location filename="../UI/player/miniWidget.cpp" line="787"/>
        <source>Random</source>
        <translation>隨機播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="52"/>
        <source>Sequence</source>
        <translation>順序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="331"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="53"/>
        <location filename="../UI/player/miniWidget.cpp" line="383"/>
        <location filename="../UI/player/miniWidget.cpp" line="777"/>
        <source>CurrentItemInLoop</source>
        <translation>單曲迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="509"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="500"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <source>Favourite</source>
        <translation type="vanished">喜歡</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="414"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="422"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="553"/>
        <location filename="../UI/player/miniWidget.cpp" line="568"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
</context>
<context>
    <name>MusicDataBase</name>
    <message>
        <source>Prompt information</source>
        <translation type="obsolete">提示資訊</translation>
    </message>
    <message>
        <source>Single song name is already exists</source>
        <translation type="obsolete">歌單名已存在</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="278"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="332"/>
        <source>Unknown singer</source>
        <translation>未知歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="279"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="338"/>
        <source>Unknown album</source>
        <translation>未知專輯</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Add failed, no valid music file found</source>
        <translation>添加失敗，找不到有效的音樂檔</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="204"/>
        <source>Music Information</source>
        <translation>歌曲資訊</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">確認</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="179"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="225"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="227"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="257"/>
        <source>Song Name : </source>
        <translation>歌曲名： </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="258"/>
        <source>Singer : </source>
        <translation>歌手： </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="259"/>
        <source>Album : </source>
        <translation>專輯： </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="260"/>
        <source>File Type : </source>
        <translation>檔類型： </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="261"/>
        <source>File Size : </source>
        <translation>檔大小： </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="262"/>
        <source>File Time : </source>
        <translation>歌曲時長： </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="263"/>
        <source>File Path : </source>
        <translation>檔路徑： </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Song</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Album</source>
        <translation>專輯</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="11"/>
        <source>Time</source>
        <translation>時長</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>列表迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>隨機播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>順序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>單曲迴圈</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="38"/>
        <source>Previous</source>
        <translation>上一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="43"/>
        <location filename="../UI/player/playsongarea.cpp" line="566"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="47"/>
        <source>Next</source>
        <translation>下一首</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="52"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="107"/>
        <source>Favourite</source>
        <translation>喜歡</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="131"/>
        <location filename="../UI/player/playsongarea.cpp" line="445"/>
        <source>Loop</source>
        <translation>列表迴圈</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="156"/>
        <source>Play List</source>
        <translation>播放歷史</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="172"/>
        <location filename="../UI/player/playsongarea.cpp" line="476"/>
        <location filename="../UI/player/playsongarea.cpp" line="478"/>
        <location filename="../UI/player/playsongarea.cpp" line="628"/>
        <location filename="../UI/player/playsongarea.cpp" line="998"/>
        <location filename="../UI/player/playsongarea.cpp" line="1023"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="173"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="391"/>
        <location filename="../UI/player/playsongarea.cpp" line="403"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="556"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="139"/>
        <location filename="../UI/player/playsongarea.cpp" line="450"/>
        <source>Random</source>
        <translation>隨機播放</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">順序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="123"/>
        <location filename="../UI/player/playsongarea.cpp" line="440"/>
        <source>CurrentItemInLoop</source>
        <translation>單曲迴圈</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <location filename="../UI/base/popupDialog.cpp" line="82"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>不能包含以下字元：\ / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="55"/>
        <source>Reached upper character limit</source>
        <translation>已達到字元上限</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="64"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="109"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="113"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="121"/>
        <source>Please input playlist name:</source>
        <translation>請輸入歌單名稱：</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="60"/>
        <source>Database Error</source>
        <translation>資料庫錯誤</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="111"/>
        <source>Search Result</source>
        <translation>搜索結果</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="29"/>
        <source>Music</source>
        <translation>歌曲</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="45"/>
        <source>Singer</source>
        <translation>歌手</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="61"/>
        <source>Album</source>
        <translation>專輯</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="56"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="74"/>
        <source>Library</source>
        <translation>音樂庫</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="81"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="104"/>
        <source>My PlayList</source>
        <translation>我的歌單</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="133"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="347"/>
        <source>New Playlist</source>
        <translation>新建歌單</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="137"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="244"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="426"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="426"/>
        <source>Single song name already exists!!!</source>
        <translation>歌單名已存在！</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放歷史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="49"/>
        <source>History</source>
        <translation>播放歷史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="56"/>
        <source>Empty</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="80"/>
        <source>The playlist has no songs</source>
        <translation>無音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Clear the playlist?</source>
        <translation>清空播放列表？</translation>
    </message>
    <message>
        <source>song</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <source>total</source>
        <translation type="vanished">共</translation>
    </message>
    <message>
        <source>songs</source>
        <translation type="vanished">首</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="433"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="461"/>
        <source>path does not exist</source>
        <translation>路徑不存在</translation>
    </message>
    <message>
        <source> song</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <source> songs</source>
        <translation type="vanished"> 首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="327"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="328"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="329"/>
        <source>Play the next one</source>
        <translation>下一首播放</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">專輯</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">時長</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="284"/>
        <location filename="../UI/tableview/tableone.cpp" line="1091"/>
        <location filename="../UI/tableview/tableone.cpp" line="1150"/>
        <location filename="../UI/tableview/tableone.cpp" line="1223"/>
        <source>Song List</source>
        <translation>歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="211"/>
        <source>There are no songs!</source>
        <translation>無音樂</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="154"/>
        <source>Play All</source>
        <translation>播放全部</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="157"/>
        <source>Add Music</source>
        <translation>添加音樂</translation>
    </message>
    <message>
        <source>Add local songs</source>
        <translation type="vanished">添加本地音樂</translation>
    </message>
    <message>
        <source>Add local folders</source>
        <translation type="vanished">添加本地檔夾</translation>
    </message>
    <message>
        <source>Add Local Songs</source>
        <translation type="vanished">添加本地音樂</translation>
    </message>
    <message>
        <source>Add Local Folder</source>
        <translation type="vanished">添加本地檔夾</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="163"/>
        <location filename="../UI/tableview/tableone.cpp" line="212"/>
        <source>Open File</source>
        <translation>打開檔</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="166"/>
        <location filename="../UI/tableview/tableone.cpp" line="213"/>
        <source>Open Folder</source>
        <translation>打開檔夾</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="286"/>
        <location filename="../UI/tableview/tableone.cpp" line="359"/>
        <location filename="../UI/tableview/tableone.cpp" line="642"/>
        <location filename="../UI/tableview/tableone.cpp" line="1093"/>
        <location filename="../UI/tableview/tableone.cpp" line="1152"/>
        <source>I Love</source>
        <translation>我喜歡</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="288"/>
        <location filename="../UI/tableview/tableone.cpp" line="1095"/>
        <location filename="../UI/tableview/tableone.cpp" line="1154"/>
        <location filename="../UI/tableview/tableone.cpp" line="1200"/>
        <location filename="../UI/tableview/tableone.cpp" line="1249"/>
        <location filename="../UI/tableview/tableone.cpp" line="1278"/>
        <source>Search Result</source>
        <translation>搜索結果</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="317"/>
        <source>Play</source>
        <translation>播放</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="318"/>
        <source>Delete from list</source>
        <translation>刪除選中項</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="319"/>
        <source>Remove from local</source>
        <translation>從本地刪除</translation>
    </message>
    <message>
        <source>Delete Selected Items</source>
        <translation type="vanished">刪除選中項</translation>
    </message>
    <message>
        <source>Song Information</source>
        <translation type="vanished">歌曲資訊</translation>
    </message>
    <message>
        <source>Add To Songlist</source>
        <translation type="vanished">添加歌曲到</translation>
    </message>
    <message>
        <source>Open The Folder</source>
        <translation type="vanished">打開所在檔夾</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="389"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>確認將選中的歌曲從歌單中刪除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="415"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>歌曲從本地刪除後不可恢復，是否確定刪除？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="723"/>
        <source>Audio File</source>
        <translation>音頻檔</translation>
    </message>
    <message>
        <source>Are you sure to clear the list?</source>
        <translation type="vanished">確認清空歌單列表？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="896"/>
        <source>Add failed! Format error or no access permission!</source>
        <translation>添加失敗！格式錯誤或無訪問許可權！</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="905"/>
        <location filename="../UI/tableview/tableone.cpp" line="934"/>
        <source>Success add %1 song</source>
        <translation>成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="939"/>
        <source>Repeat add</source>
        <translation>重複添加</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="978"/>
        <source>path does not exist</source>
        <translation>路徑不存在</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1020"/>
        <source> song</source>
        <translation> 首歌曲</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您確定從本地刪除嗎？</translation>
    </message>
    <message>
        <source>Success add %1 songs</source>
        <translation type="vanished">成功添加%1首歌曲</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="916"/>
        <source>Add failed</source>
        <translation>添加失敗</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">刪除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="320"/>
        <source>View song information</source>
        <translation>歌曲資訊</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="321"/>
        <source>Add to songlist</source>
        <translation>添加歌曲到</translation>
    </message>
    <message>
        <source>Clear List</source>
        <translation type="vanished">清空列表</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="389"/>
        <location filename="../UI/tableview/tableone.cpp" line="415"/>
        <location filename="../UI/tableview/tableone.cpp" line="905"/>
        <location filename="../UI/tableview/tableone.cpp" line="916"/>
        <location filename="../UI/tableview/tableone.cpp" line="934"/>
        <location filename="../UI/tableview/tableone.cpp" line="939"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失敗！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲檔失敗！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="1022"/>
        <source> songs</source>
        <translation> 首歌曲</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="57"/>
        <source>back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="68"/>
        <source>forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音樂，歌手</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="91"/>
        <source>Not logged in</source>
        <translation>未登錄</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">主菜單</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">主菜單</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="159"/>
        <source>mini model</source>
        <translation>精簡模式</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="167"/>
        <source>To minimize the</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="186"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="675"/>
        <source>Music Player</source>
        <translation>音樂</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="962"/>
        <location filename="../UI/mainwidget.cpp" line="1260"/>
        <source>reduction</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="966"/>
        <location filename="../UI/mainwidget.cpp" line="1247"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1428"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">菜單</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">選項</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="24"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="37"/>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="39"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="125"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="41"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="123"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="43"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="121"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <source>Auto</source>
        <translation>跟隨主題</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="56"/>
        <source>Light</source>
        <translation>淺色</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="59"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">音樂</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="189"/>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation>音樂播放器是用於播放各種音樂格式的多媒體播放軟體。它是涵蓋了各種音樂格式的播放工具，操作快捷簡單。</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音樂播放器是一種用於播放各種音樂檔的多媒體播放軟體。它是涵蓋了各種音樂格式的播放工具，操作快捷簡單。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音樂播放器是一種用於播放各種音樂檔的多媒體播放軟體。它是涵蓋了各種音樂格式的播放工具，操作快捷簡單</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="188"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服務與支持團隊：</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.h" line="63"/>
        <source>kylin-music</source>
        <translation>音樂</translation>
    </message>
</context>
</TS>
