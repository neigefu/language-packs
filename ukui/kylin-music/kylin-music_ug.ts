<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>CustomToolButton</name>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="125"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="138"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="126"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="142"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="127"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="146"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="128"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="150"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="156"/>
        <source>Song List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="156"/>
        <source>I Love</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="34"/>
        <location filename="../UI/player/miniWidget.cpp" line="282"/>
        <location filename="../UI/player/miniWidget.cpp" line="654"/>
        <location filename="../UI/player/miniWidget.cpp" line="760"/>
        <source>Music Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="49"/>
        <location filename="../UI/player/miniWidget.cpp" line="50"/>
        <location filename="../UI/player/miniWidget.cpp" line="350"/>
        <location filename="../UI/player/miniWidget.cpp" line="743"/>
        <source>Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="51"/>
        <location filename="../UI/player/miniWidget.cpp" line="358"/>
        <location filename="../UI/player/miniWidget.cpp" line="748"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="52"/>
        <source>Sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="283"/>
        <source>00:00/00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="53"/>
        <location filename="../UI/player/miniWidget.cpp" line="342"/>
        <location filename="../UI/player/miniWidget.cpp" line="738"/>
        <source>CurrentItemInLoop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="172"/>
        <location filename="../UI/player/miniWidget.cpp" line="214"/>
        <location filename="../UI/player/miniWidget.cpp" line="464"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="177"/>
        <location filename="../UI/player/miniWidget.cpp" line="182"/>
        <location filename="../UI/player/miniWidget.cpp" line="219"/>
        <location filename="../UI/player/miniWidget.cpp" line="224"/>
        <location filename="../UI/player/miniWidget.cpp" line="469"/>
        <location filename="../UI/player/miniWidget.cpp" line="474"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="375"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="386"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="522"/>
        <location filename="../UI/player/miniWidget.cpp" line="538"/>
        <source>I Love</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="227"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="262"/>
        <source>Unknown singer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="228"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="265"/>
        <source>Unknown album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="411"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="411"/>
        <source>Add failed, no valid music file found</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="215"/>
        <source>Music Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="192"/>
        <source>Music Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="242"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="245"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="268"/>
        <source>Song Name : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="269"/>
        <source>Singer : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="270"/>
        <source>Album : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="271"/>
        <source>File Type : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="272"/>
        <source>File Size : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="273"/>
        <source>File Time : </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="274"/>
        <source>File Path : </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="7"/>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Singer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Album</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="29"/>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="34"/>
        <location filename="../UI/player/playsongarea.cpp" line="553"/>
        <location filename="../UI/player/playsongarea.cpp" line="882"/>
        <location filename="../UI/player/playsongarea.cpp" line="923"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="39"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="44"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="100"/>
        <source>Favourite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="128"/>
        <location filename="../UI/player/playsongarea.cpp" line="143"/>
        <location filename="../UI/player/playsongarea.cpp" line="423"/>
        <location filename="../UI/player/playsongarea.cpp" line="469"/>
        <source>Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="164"/>
        <source>Play List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="184"/>
        <location filename="../UI/player/playsongarea.cpp" line="495"/>
        <location filename="../UI/player/playsongarea.cpp" line="497"/>
        <location filename="../UI/player/playsongarea.cpp" line="617"/>
        <location filename="../UI/player/playsongarea.cpp" line="832"/>
        <location filename="../UI/player/playsongarea.cpp" line="858"/>
        <source>Music Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="185"/>
        <source>00:00/00:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="393"/>
        <location filename="../UI/player/playsongarea.cpp" line="405"/>
        <source>I Love</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="558"/>
        <location filename="../UI/player/playsongarea.cpp" line="563"/>
        <location filename="../UI/player/playsongarea.cpp" line="887"/>
        <location filename="../UI/player/playsongarea.cpp" line="892"/>
        <location filename="../UI/player/playsongarea.cpp" line="928"/>
        <location filename="../UI/player/playsongarea.cpp" line="933"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="136"/>
        <location filename="../UI/player/playsongarea.cpp" line="145"/>
        <location filename="../UI/player/playsongarea.cpp" line="432"/>
        <location filename="../UI/player/playsongarea.cpp" line="474"/>
        <source>Random</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="120"/>
        <location filename="../UI/player/playsongarea.cpp" line="147"/>
        <location filename="../UI/player/playsongarea.cpp" line="449"/>
        <location filename="../UI/player/playsongarea.cpp" line="459"/>
        <source>CurrentItemInLoop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="45"/>
        <location filename="../UI/base/popupDialog.cpp" line="87"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="45"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="53"/>
        <source>Reached upper character limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="62"/>
        <source>Music Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="115"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="119"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="128"/>
        <source>Please input playlist name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="64"/>
        <source>Database Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="109"/>
        <source>Search Result</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="25"/>
        <source>Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="41"/>
        <source>Singer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="57"/>
        <source>Album</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="31"/>
        <source>Music Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="49"/>
        <source>Library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="58"/>
        <source>Song List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="71"/>
        <source>My PlayList</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="113"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="319"/>
        <source>New Playlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="117"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="227"/>
        <source>I Love</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="278"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="366"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="278"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="366"/>
        <source>Single song name already exists!!!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="35"/>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="43"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="61"/>
        <source>The playlist has no songs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="115"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="115"/>
        <source>Clear the playlist?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="245"/>
        <source> songs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="274"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="275"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="276"/>
        <source>Play the next one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="261"/>
        <location filename="../UI/tableview/tableone.cpp" line="994"/>
        <location filename="../UI/tableview/tableone.cpp" line="1036"/>
        <location filename="../UI/tableview/tableone.cpp" line="1107"/>
        <source>Song List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="193"/>
        <source>There are no songs!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="146"/>
        <source>Play All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="149"/>
        <source>Add Music</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add local songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add local folders</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <source>Add Local Songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add Local Folder</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="155"/>
        <location filename="../UI/tableview/tableone.cpp" line="195"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="158"/>
        <location filename="../UI/tableview/tableone.cpp" line="196"/>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="263"/>
        <location filename="../UI/tableview/tableone.cpp" line="346"/>
        <location filename="../UI/tableview/tableone.cpp" line="591"/>
        <location filename="../UI/tableview/tableone.cpp" line="996"/>
        <location filename="../UI/tableview/tableone.cpp" line="1038"/>
        <source>I Love</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="265"/>
        <location filename="../UI/tableview/tableone.cpp" line="998"/>
        <location filename="../UI/tableview/tableone.cpp" line="1040"/>
        <location filename="../UI/tableview/tableone.cpp" line="1084"/>
        <location filename="../UI/tableview/tableone.cpp" line="1133"/>
        <location filename="../UI/tableview/tableone.cpp" line="1162"/>
        <source>Search Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="293"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="294"/>
        <source>Delete from list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="295"/>
        <source>Remove from local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="370"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="503"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="668"/>
        <source>Audio File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="816"/>
        <source>Add failed! Format error or no access permission!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="852"/>
        <source>Repeat add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="887"/>
        <source>path does not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="928"/>
        <source> song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="825"/>
        <location filename="../UI/tableview/tableone.cpp" line="847"/>
        <source>Success add %1 songs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="836"/>
        <source>Add failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="296"/>
        <source>View song information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="297"/>
        <source>Add to songlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="370"/>
        <location filename="../UI/tableview/tableone.cpp" line="503"/>
        <location filename="../UI/tableview/tableone.cpp" line="825"/>
        <location filename="../UI/tableview/tableone.cpp" line="836"/>
        <location filename="../UI/tableview/tableone.cpp" line="847"/>
        <location filename="../UI/tableview/tableone.cpp" line="852"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="930"/>
        <source> songs</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="56"/>
        <source>back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="67"/>
        <source>forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="90"/>
        <source>Not logged in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="158"/>
        <source>mini model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="166"/>
        <source>To minimize the</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="176"/>
        <source>maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="186"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="573"/>
        <source>Music Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="831"/>
        <location filename="../UI/mainwidget.cpp" line="1050"/>
        <source>reduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="837"/>
        <location filename="../UI/mainwidget.cpp" line="1039"/>
        <source>maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1172"/>
        <source>Prompt information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="30"/>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="43"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="45"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="127"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="47"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="125"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="54"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="59"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="62"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="123"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="232"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="267"/>
        <source>Music Player</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="264"/>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="269"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="324"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="333"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
