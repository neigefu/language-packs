<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>CustomToolButton</name>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="216"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="226"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="217"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="228"/>
        <source>Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="218"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="229"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="219"/>
        <location filename="../UI/sidebar/customToolButton.cpp" line="231"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>Song List</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠦ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/customToolButton.cpp" line="236"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠲᠠᠢ</translation>
    </message>
</context>
<context>
    <name>MiniWidget</name>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="34"/>
        <location filename="../UI/player/miniWidget.cpp" line="328"/>
        <location filename="../UI/player/miniWidget.cpp" line="672"/>
        <location filename="../UI/player/miniWidget.cpp" line="797"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="49"/>
        <location filename="../UI/player/miniWidget.cpp" line="50"/>
        <location filename="../UI/player/miniWidget.cpp" line="389"/>
        <location filename="../UI/player/miniWidget.cpp" line="780"/>
        <source>Loop</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠶ᠋ᠢᠨ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="51"/>
        <location filename="../UI/player/miniWidget.cpp" line="397"/>
        <location filename="../UI/player/miniWidget.cpp" line="785"/>
        <source>Random</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="52"/>
        <source>Sequence</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ&#x202f;ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="329"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="53"/>
        <location filename="../UI/player/miniWidget.cpp" line="381"/>
        <location filename="../UI/player/miniWidget.cpp" line="775"/>
        <source>CurrentItemInLoop</source>
        <translation>ᠳᠠᠨᠭ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="507"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="498"/>
        <source>Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="412"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="420"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/miniWidget.cpp" line="551"/>
        <location filename="../UI/player/miniWidget.cpp" line="566"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠲᠠᠢ</translation>
    </message>
</context>
<context>
    <name>MusicFileInformation</name>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="278"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="332"/>
        <source>Unknown singer</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠳᠠᠭᠤᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="279"/>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="338"/>
        <source>Unknown album</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠲᠤᠰᠭᠠᠢ ᠹᠢᠯᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UIControl/base/musicfileinformation.cpp" line="546"/>
        <source>Add failed, no valid music file found</source>
        <translation>add failed, no valid music file found</translation>
    </message>
</context>
<context>
    <name>MusicInfoDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="204"/>
        <source>Music Information</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="179"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="225"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="227"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="257"/>
        <source>Song Name : </source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠨᠡᠷ᠎ᠡ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="258"/>
        <source>Singer : </source>
        <translation>ᠳᠠᠭᠤᠴᠢᠨ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="259"/>
        <source>Album : </source>
        <translation>ᠲᠤᠰᠭᠠᠢ ᠹᠢᠯᠢᠮ ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="260"/>
        <source>File Type : </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="261"/>
        <source>File Size : </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ: </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="262"/>
        <source>File Time : </source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠴᠠᠭ᠄ </translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="263"/>
        <source>File Path : </source>
        <translation>ᠹᠠᠢᠯ&#x202f;ᠤ᠋ᠨ ᠵᠢᠮ᠄ </translation>
    </message>
</context>
<context>
    <name>MusicListModel</name>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="8"/>
        <source>Song</source>
        <translation>ᠳᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="9"/>
        <source>Singer</source>
        <translation>ᠳᠠᠭᠤᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="10"/>
        <source>Album</source>
        <translation>ᠲᠤᠰᠬᠠᠢ ᠹᠢᠯᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UIControl/tableview/musiclistmodel.cpp" line="11"/>
        <source>Time</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>PlayBackModeWidget</name>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="29"/>
        <source>Loop</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠶ᠋ᠢᠨ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="44"/>
        <source>Random</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="60"/>
        <source>Sequential</source>
        <translation>ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠵᠢᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/playbackmodewidget.cpp" line="75"/>
        <source>CurrentItemInLoop</source>
        <translation>ᠳᠠᠨᠭ ᠣᠷᠴᠢᠯ</translation>
    </message>
</context>
<context>
    <name>PlaySongArea</name>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="38"/>
        <source>Previous</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠲᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="43"/>
        <location filename="../UI/player/playsongarea.cpp" line="566"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="47"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="52"/>
        <source>Volume</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="107"/>
        <source>Favourite</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="131"/>
        <location filename="../UI/player/playsongarea.cpp" line="445"/>
        <source>Loop</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠶ᠋ᠢᠨ ᠣᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="156"/>
        <source>Play List</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠦᠭᠰᠡᠨ ᠲᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="172"/>
        <location filename="../UI/player/playsongarea.cpp" line="476"/>
        <location filename="../UI/player/playsongarea.cpp" line="478"/>
        <location filename="../UI/player/playsongarea.cpp" line="626"/>
        <location filename="../UI/player/playsongarea.cpp" line="981"/>
        <location filename="../UI/player/playsongarea.cpp" line="1006"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="173"/>
        <source>00:00/00:00</source>
        <translation>00:00/00:00</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="391"/>
        <location filename="../UI/player/playsongarea.cpp" line="403"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="556"/>
        <source>Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="139"/>
        <location filename="../UI/player/playsongarea.cpp" line="450"/>
        <source>Random</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠡ</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation type="vanished">顺序播放</translation>
    </message>
    <message>
        <location filename="../UI/player/playsongarea.cpp" line="123"/>
        <location filename="../UI/player/playsongarea.cpp" line="440"/>
        <source>CurrentItemInLoop</source>
        <translation>ᠳᠠᠨᠭ ᠣᠷᠴᠢᠯ</translation>
    </message>
</context>
<context>
    <name>PopupDialog</name>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <location filename="../UI/base/popupDialog.cpp" line="82"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="47"/>
        <source>Could not contain characters:  / : * ? &quot; &amp; &lt; &gt; |</source>
        <translation>ᠲᠤᠤᠷᠠᠬᠢ ᠲᠡᠮᠳᠡᠭ ᠤ᠋ᠳ ᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠢᠬᠤ ᠦᠭᠡᠢ :  / : * ? &quot; &amp; &lt; &gt; |</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="55"/>
        <source>Reached upper character limit</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ ᠦ᠋ᠨ ᠳᠡᠭᠡᠳᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠲᠦ᠍ ᠬᠦᠷᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="64"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="109"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="113"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/base/popupDialog.cpp" line="121"/>
        <source>Please input playlist name:</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠨᠡᠷᠡᠢᠳᠤᠯ ᠢ᠋ ᠤᠷᠤᠯᠠᠭᠠᠷᠠᠢ:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../UIControl/base/musicDataBase.cpp" line="60"/>
        <source>Database Error</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠬᠦᠮᠦᠷᠬᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>SearchEdit</name>
    <message>
        <location filename="../UI/player/searchedit.cpp" line="111"/>
        <source>Search Result</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠨᠭ</translation>
    </message>
</context>
<context>
    <name>SearchResult</name>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="29"/>
        <source>Music</source>
        <translation>ᠳᠠᠭᠤᠤ ᠭᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="45"/>
        <source>Singer</source>
        <translation>ᠳᠠᠭᠤᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../UI/player/searchresult.cpp" line="61"/>
        <source>Album</source>
        <translation>ᠲᠤᠰᠬᠠᠢ ᠹᠢᠯᠢᠮ</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="56"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="74"/>
        <source>Library</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠦᠮᠦᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="81"/>
        <source>Song List</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠦ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="104"/>
        <source>My PlayList</source>
        <translation>ᠮᠢᠨᠤ ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="134"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="347"/>
        <source>New Playlist</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="138"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="246"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="424"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="301"/>
        <location filename="../UI/sidebar/sidebarwidget.cpp" line="424"/>
        <source>Single song name already exists!!!</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠷᠤᠰᠢᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>TableHistory</name>
    <message>
        <source>HistoryPlayList</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>PlayList</source>
        <translation type="vanished">播放历史</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="49"/>
        <source>History</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠦᠭᠰᠡᠨ ᠲᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="56"/>
        <source>Empty</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="80"/>
        <source>The playlist has no songs</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠳ᠋ᠦ᠍ ᠪᠠᠰᠠ ᠲᠠᠭᠤᠤ ᠦᠭᠡᠢ ᠪᠠᠢᠨ᠎ᠠ ᠰᠢᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="140"/>
        <source>Clear the playlist?</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="433"/>
        <location filename="../UI/tableview/tablehistory.cpp" line="461"/>
        <source>path does not exist</source>
        <translation>ᠵᠠᠮ ᠮᠥᠷ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <source> songs</source>
        <translation type="vanished"> Songs</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="327"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="328"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tablehistory.cpp" line="329"/>
        <source>Play the next one</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠠᠭᠤᠤ ᠵᠢ ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>The song doesn&apos;t exist</source>
        <translation type="vanished">歌曲不存在！</translation>
    </message>
</context>
<context>
    <name>TableOne</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="vanished">歌曲</translation>
    </message>
    <message>
        <source>Singer</source>
        <translation type="vanished">歌手</translation>
    </message>
    <message>
        <source>Album</source>
        <translation type="vanished">专辑</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">时长</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="283"/>
        <location filename="../UI/tableview/tableone.cpp" line="1054"/>
        <location filename="../UI/tableview/tableone.cpp" line="1113"/>
        <location filename="../UI/tableview/tableone.cpp" line="1186"/>
        <source>Song List</source>
        <translation>ᠳᠠᠭᠤᠨ ᠦ᠌ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="209"/>
        <source>There are no songs!</source>
        <translation>ᠪᠠᠰᠠ ᠳᠠᠭᠤᠤ ᠦᠭᠡᠢ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="152"/>
        <source>Play All</source>
        <translation>ᠪᠦᠬᠦᠳᠡ ᠵᠢ ᠨᠡᠪᠳᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="155"/>
        <source>Add Music</source>
        <translation>ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Add local songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add local folders</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <source>Add Local Songs</source>
        <translation type="vanished">添加本地音乐</translation>
    </message>
    <message>
        <source>Add Local Folder</source>
        <translation type="vanished">添加本地文件夹</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="161"/>
        <location filename="../UI/tableview/tableone.cpp" line="211"/>
        <source>Open File</source>
        <translation>ᠹᠠᠢᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="164"/>
        <location filename="../UI/tableview/tableone.cpp" line="212"/>
        <source>Open Folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠢᠳᠠᠰᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="285"/>
        <location filename="../UI/tableview/tableone.cpp" line="358"/>
        <location filename="../UI/tableview/tableone.cpp" line="623"/>
        <location filename="../UI/tableview/tableone.cpp" line="1056"/>
        <location filename="../UI/tableview/tableone.cpp" line="1115"/>
        <source>I Love</source>
        <translation>ᠪᠢ ᠲᠤᠷᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="287"/>
        <location filename="../UI/tableview/tableone.cpp" line="1058"/>
        <location filename="../UI/tableview/tableone.cpp" line="1117"/>
        <location filename="../UI/tableview/tableone.cpp" line="1163"/>
        <location filename="../UI/tableview/tableone.cpp" line="1212"/>
        <location filename="../UI/tableview/tableone.cpp" line="1241"/>
        <source>Search Result</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠨᠭ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="316"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="317"/>
        <source>Delete from list</source>
        <translation>delete from list</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="318"/>
        <source>Remove from local</source>
        <translation>remove from local</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="388"/>
        <source>Confirm that the selected song will be deleted from the song list?</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠳᠠᠭᠤᠤ ᠪᠠᠨ ᠳᠠᠭᠤᠨ ᠦ᠌ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="406"/>
        <source>After the song is deleted from the local, it cannot be resumed. Is it sure to delete?</source>
        <translation>ᠳᠠᠭᠤᠤ ᠵᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ᠂ ᠨᠡᠬᠡᠷᠡᠨ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="704"/>
        <source>Audio File</source>
        <translation>ᠳᠠᠭᠤᠨ&#x202f;ᠤ᠋ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="859"/>
        <source>Add failed! Format error or no access permission!</source>
        <translation>ᠲᠤᠰ ᠬᠡᠯᠡᠪᠷᠢ ᠵᠢᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="868"/>
        <location filename="../UI/tableview/tableone.cpp" line="897"/>
        <source>Success add %1 song</source>
        <translation>ᠠᠮᠵᠢᠯᠲᠠ ᠲᠠᠢ ᠪᠠᠷ 1 ᠳᠠᠭᠤᠤ ᠨᠡᠮᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="902"/>
        <source>Repeat add</source>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠠᠪᠳᠠᠨ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="941"/>
        <source>path does not exist</source>
        <translation>ᠵᠢᠮ ᠨᠢ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="983"/>
        <source> song</source>
        <translation> ᠲᠠᠷᠢᠭᠦᠨ ᠦ᠌ ᠳᠠᠭᠤᠤ</translation>
    </message>
    <message>
        <source>Are you sure you want to delete it locally?</source>
        <translation type="vanished">您确定从本地删除吗？</translation>
    </message>
    <message>
        <source>Success add %1 songs</source>
        <translation type="vanished">success add %1 songs</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="879"/>
        <source>Add failed</source>
        <translation>ᠨᠡᠮᠡᠵᠦ ᠲᠡᠢᠯᠦᠭᠰᠡᠨ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="319"/>
        <source>View song information</source>
        <translation>view song information</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="320"/>
        <source>Add to songlist</source>
        <translation>add to songlist</translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="388"/>
        <location filename="../UI/tableview/tableone.cpp" line="406"/>
        <location filename="../UI/tableview/tableone.cpp" line="868"/>
        <location filename="../UI/tableview/tableone.cpp" line="879"/>
        <location filename="../UI/tableview/tableone.cpp" line="897"/>
        <location filename="../UI/tableview/tableone.cpp" line="902"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Add failed!</source>
        <translation type="vanished">添加失败！</translation>
    </message>
    <message>
        <source>Failed to add song file!</source>
        <translation type="vanished">添加歌曲文件失败！</translation>
    </message>
    <message>
        <source>Total </source>
        <translation type="vanished">共 </translation>
    </message>
    <message>
        <location filename="../UI/tableview/tableone.cpp" line="985"/>
        <source> songs</source>
        <translation> ᠲᠠᠷᠢᠭᠦᠨ ᠦ᠌ ᠳᠠᠭᠤᠤ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="57"/>
        <source>back</source>
        <translation>ᠡᠮᠦᠨ᠎ᠡ ᠠᠯᠬᠤᠮ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="68"/>
        <source>forward</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Search for music, singers</source>
        <translation type="vanished">搜索音乐，歌手</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="91"/>
        <source>Not logged in</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <source>menu</source>
        <translation type="vanished">主菜单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="159"/>
        <source>mini model</source>
        <translation>ᠲᠦᠭᠦᠮᠴᠢᠯᠡᠭᠰᠡᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="167"/>
        <source>To minimize the</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="177"/>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/titlebar.cpp" line="187"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="680"/>
        <source>Music Player</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <source>Song List</source>
        <translation type="vanished">歌曲列表</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="965"/>
        <location filename="../UI/mainwidget.cpp" line="1263"/>
        <source>reduction</source>
        <translation>ᠤᠤᠯ ᠬᠡᠪ ᠲᠦ᠍ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="969"/>
        <location filename="../UI/mainwidget.cpp" line="1249"/>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/mainwidget.cpp" line="1430"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">menu</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="24"/>
        <source>Options</source>
        <translation>ᠰᠣᠩᠭᠣᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="37"/>
        <source>Theme</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="39"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="125"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="41"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="123"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="43"/>
        <location filename="../UI/titlebar/menumodule.cpp" line="121"/>
        <source>Exit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="51"/>
        <source>Auto</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠢ᠋ ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="56"/>
        <source>Light</source>
        <translation>ᠭᠦᠶᠦᠬᠡᠨ ᠦᠨᠭᠭᠡ</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="59"/>
        <source>Dark</source>
        <translation>ᠭᠦᠨ ᠦᠨᠭᠭᠡ</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">quit</translation>
    </message>
    <message>
        <source>Music Player</source>
        <translation type="vanished">music player</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="189"/>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,fast and simple.</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠤ᠋ᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠨᠢ ᠡᠯ᠎ᠡ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠮᠸᠳᠢᠶᠠ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠰᠤᠹᠲ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠲᠡᠷᠡ ᠨᠢ ᠡᠯ᠎ᠡ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢ ᠬᠠᠮᠤᠷᠤᠭᠰᠠᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠪᠠᠢᠵᠤ᠂ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠨᠢ ᠳᠦᠷᠬᠡᠨ ᠳᠦᠬᠦᠮ ᠪᠠᠢᠳᠠᠭ.</translation>
    </message>
    <message>
        <source>Music player is a multimedia playback software.Cover Various music formats Playback tools for,Fast and simple.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单。</translation>
    </message>
    <message>
        <source>Music Player is a kind of multimedia player software for playing various music files.It covers a variety of music formats play tool,easy to operate.</source>
        <translation type="vanished">音乐播放器是一种用于播放各种音乐文件的多媒体播放软件。它是涵盖了各种音乐格式的播放工具，操作快捷简单</translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.cpp" line="188"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠄ </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">service &amp; support: </translation>
    </message>
    <message>
        <location filename="../UI/titlebar/menumodule.h" line="63"/>
        <source>kylin-music</source>
        <translation>ᠵᠢᠢ ᠯᠢᠨ ᠳᠠᠭᠤᠤ ᠬᠥᠭ᠍ᠵᠢᠮ ᠃</translation>
    </message>
</context>
</TS>
