<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Touchpad</name>
    <message>
        <location filename="../touchpad-settings.cpp" line="37"/>
        <source>Touchpad</source>
        <translation>ལག་ཐོགས་ཁ་པར།</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="63"/>
        <source>Touchpad</source>
        <translation>རེག་ཚོད་འཛིན་པང་ལེབ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="82"/>
        <source>Disable touchpad when using the mouse</source>
        <translation>ཙིག་རྟགས་འཇུག་པའི་སྐབས་སུ་ཚོད་འཛིན་པང་ལེབ་བཀོལ་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="100"/>
        <source>Pointer Speed</source>
        <translation>ཕྱོགས་སྟོན་འཁོར་ལོ་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="102"/>
        <source>Slow</source>
        <translation>དལ་མོ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="103"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="127"/>
        <source>Disable touchpad when typing</source>
        <translation>ཡི་གེ་གཏགས་པའི་སྐབས་སུ་ཚོད་འཛིན་པང་ལེབ་བཀོལ་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="146"/>
        <source>Touch and click on the touchpad</source>
        <translation>ལག་ཐོགས་ཁ་པར་ལ་རེག་པ་དང་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="165"/>
        <source>Scroll bar slides with finger</source>
        <translation>འགྲིལ་ཐིག་དང་ལག་ཞོར་དུ་འདྲེད་འགུལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="183"/>
        <source>Scrolling area</source>
        <translation>འགྲིལ་བའི་ས་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="186"/>
        <source>Two-finger scrolling in the middle area</source>
        <translation>དཀྱིལ་ཁུལ་དུ་ལག་པ་གཉིས་ཀྱིས་ཕར་འགྲོ་ཚུར་འོང་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="187"/>
        <source>Edge scrolling</source>
        <translation>མཐའ་འཁོར་དུ་ཕར་འགྲོ་ཚུར་འོང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="188"/>
        <source>Disable scrolling</source>
        <translation>འགྲིལ་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="202"/>
        <source>gesture</source>
        <translation>ལག་བརྡ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="233"/>
        <source>three fingers click</source>
        <translation>མཛུབ་མོ་གསུམ་གྱིས་མཐེབ་གནོན་བྱས།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="234"/>
        <source>show global search</source>
        <translation>ཁྱོན་ཡོངས་ནས་འཚོལ་ཞིབ་བྱས།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="239"/>
        <source>swipe three fingers down</source>
        <translation>ཕྱོགས་གསུམ་མར་འདྲེད་འགུལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="240"/>
        <source>show desktop</source>
        <translation>ཅོག་ཙེའི་ངོས་སུ་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="245"/>
        <source>swipe three fingers up</source>
        <translation>ཁ་ཕྱོགས་གསུམ་གྱི་སྟེང་ནས་ཤུད་སོང་།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="246"/>
        <source>open the multitasking view</source>
        <translation>ལས་འགན་མང་པོའི་ལྟ་ཚུལ་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="251"/>
        <source>swipe three fingers horizontally</source>
        <translation>མཛུབ་མོ་གསུམ་གཡས་གཡོན་འདྲེད་འགུལ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="252"/>
        <source>switch windows</source>
        <translation>ཁ་ཕྱེ་བའི་སྒེའུ་ཁུང་བར་བརྗེ་སྤོར་བྱས།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="257"/>
        <source>four fingers click</source>
        <translation>མཛུབ་མོ་བཞི་ཡིས་མཐེབ་གནོན་བྱས།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="258"/>
        <source>show sidebar</source>
        <translation>འགྲེམས་སྟོན་གྱི་གཞོགས་ངོས་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="265"/>
        <source>swipe four fingers horizontally</source>
        <translation>མཛུབ་མོ་བཞི་ཡས་མས་འདྲེད་འགུལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="266"/>
        <source>switch desktop</source>
        <translation>ཅོག་ངོས་བར་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="294"/>
        <source>more gesture</source>
        <translation>ལག་བརྡ་དེ་བས་མང་བ།</translation>
    </message>
</context>
</TS>
