<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Touchpad</name>
    <message>
        <location filename="../touchpad-settings.cpp" line="37"/>
        <source>Touchpad</source>
        <translation>觸控板</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="63"/>
        <source>Touchpad</source>
        <translation>觸控板</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="82"/>
        <source>Disable touchpad when using the mouse</source>
        <translation>插入滑鼠時禁用觸控板</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="100"/>
        <source>Pointer Speed</source>
        <translation>指標速度</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="102"/>
        <source>Slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="103"/>
        <source>Fast</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="127"/>
        <source>Disable touchpad when typing</source>
        <translation>打字時禁用觸控板</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="146"/>
        <source>Touch and click on the touchpad</source>
        <translation>觸控板輕觸點擊</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="165"/>
        <source>Scroll bar slides with finger</source>
        <translation>滾動條跟隨手指滑動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="183"/>
        <source>Scrolling area</source>
        <translation>滾動區域</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="186"/>
        <source>Two-finger scrolling in the middle area</source>
        <translation>中間區域滾動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="187"/>
        <source>Edge scrolling</source>
        <translation>邊界滾動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="188"/>
        <source>Disable scrolling</source>
        <translation>禁止滾動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="202"/>
        <source>gesture</source>
        <translation>手勢</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="233"/>
        <source>three fingers click</source>
        <translation>三指輕點</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="234"/>
        <source>show global search</source>
        <translation>呼出全域搜索</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="239"/>
        <source>swipe three fingers down</source>
        <translation>三指向下滑動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="240"/>
        <source>show desktop</source>
        <translation>顯示桌面</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="245"/>
        <source>swipe three fingers up</source>
        <translation>三指向上滑動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="246"/>
        <source>open the multitasking view</source>
        <translation>打開多任務檢視</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="251"/>
        <source>swipe three fingers horizontally</source>
        <translation>三指左右滑動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="252"/>
        <source>switch windows</source>
        <translation>打開的視窗之間切換</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="257"/>
        <source>four fingers click</source>
        <translation>四指點擊</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="258"/>
        <source>show sidebar</source>
        <translation>呼出側邊欄</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="265"/>
        <source>swipe four fingers horizontally</source>
        <translation>四指左右滑動</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="266"/>
        <source>switch desktop</source>
        <translation>桌面之間切換</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="294"/>
        <source>more gesture</source>
        <translation>更多手勢</translation>
    </message>
</context>
</TS>
