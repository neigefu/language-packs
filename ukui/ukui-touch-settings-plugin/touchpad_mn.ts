<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <location filename="../touchpad-settings.cpp" line="93"/>
        <source>three fingers click</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠬᠦᠩᠭᠡᠨ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="94"/>
        <source>show global search</source>
        <translation>ᠪᠦᠷᠢᠨ ᠬᠠᠢᠯᠲᠠ᠎ᠶ᠋ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="97"/>
        <source>swipe three fingers down</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="98"/>
        <source>show desktop</source>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="101"/>
        <source>swipe three fingers up</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠡᠭᠡᠭ᠌ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="102"/>
        <source>open the multitasking view</source>
        <translation>ᠤᠯᠠᠨ ᠡᠬᠦᠷᠬᠡᠳᠦ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="105"/>
        <source>swipe three fingers horizontally</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠪᠡᠷ᠎ᠡ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="106"/>
        <source>switch windows</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠭ᠌ᠰᠡᠨ ᠴᠤᠩᠬᠤᠨ᠎ᠤ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="109"/>
        <source>four fingers click</source>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="110"/>
        <source>show sidebar</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠪᠠᠭᠠᠷ᠎ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="116"/>
        <source>swipe four fingers horizontally</source>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠤᠶᠤ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpad-settings.cpp" line="117"/>
        <source>switch desktop</source>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤᠨ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Touchpad</name>
    <message>
        <location filename="../touchpad-settings.cpp" line="40"/>
        <source>Touchpad</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ᠌ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="66"/>
        <source>Touchpad</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ᠌ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="142"/>
        <source>Disable touchpad when using the mouse</source>
        <translation>ᠮᠠᠦ᠋ᠰ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠦᠶ᠎ᠡ᠎ᠳᠦ ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌᠎ᠶ᠋ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="167"/>
        <source>Pointer Speed</source>
        <translation>ᠵᠢᠭᠠᠯᠳᠠ᠎ᠶ᠋ᠢᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="168"/>
        <source>Slow</source>
        <translation>ᠤᠳᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="169"/>
        <source>Fast</source>
        <translation>ᠬᠤᠷᠳᠤᠨ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="216"/>
        <source>Disable touchpad when typing</source>
        <translation>ᠦᠰᠦᠭ ᠱᠢᠪᠡᠬᠦ᠌ ᠦᠶ᠎ᠡ᠎ᠳᠦ ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌᠎ᠶ᠋ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="242"/>
        <source>Touch and click on the touchpad</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ᠎ᠶ᠋ᠢ ᠬᠦᠩᠬᠡᠨ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="264"/>
        <source>Scroll bar slides with finger</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠦᠯ ᠬᠤᠷᠤᠭᠤ᠎ᠶ᠋ᠢ ᠳᠠᠭᠠᠵᠤ ᠭᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="288"/>
        <source>Scrolling area</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠦᠯ᠎ᠦᠨ ᠬᠦᠷᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="291"/>
        <source>Two-finger scrolling in the middle area</source>
        <translation>ᠳᠤᠮᠳᠠᠬᠢ ᠪᠦᠰᠡ᠎ᠳᠦ ᠦᠩᠬᠦᠷᠢᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="292"/>
        <source>Edge scrolling</source>
        <translation>ᠵᠠᠭᠠᠭ᠎ᠢᠶᠠᠷ ᠦᠩᠬᠦᠷᠢᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="293"/>
        <source>Disable scrolling</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠦᠯ᠎ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="75"/>
        <source>gesture</source>
        <translation>ᠭᠠᠷ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <source>three fingers click</source>
        <translation type="vanished">ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠬᠦᠩᠭᠡᠨ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>show global search</source>
        <translation type="obsolete">ᠪᠦᠷᠢᠨ ᠬᠠᠢᠯᠲᠠ ᠎ᠶ᠋ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>swipe three fingers down</source>
        <translation type="vanished">ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>show desktop</source>
        <translation type="obsolete">ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <source>swipe three fingers up</source>
        <translation type="vanished">ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠭᠡᠭ᠌ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>open the multitasking view</source>
        <translation type="obsolete">ᠤᠯᠠᠨ ᠡᠬᠦᠷᠬᠡᠳᠦ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>swipe three fingers horizontally</source>
        <translation type="vanished">ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠪᠡᠷ᠎ᠡ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>switch windows</source>
        <translation type="obsolete">ᠨᠡᠬᠡᠬᠡᠭ᠌ᠰᠡᠨ ᠴᠤᠩᠬᠤᠨ ᠎ᠤ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>four fingers click</source>
        <translation type="vanished">ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>show sidebar</source>
        <translation type="obsolete">ᠬᠠᠵᠠᠭᠤ ᠪᠠᠭᠠᠷ ᠎ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>swipe four fingers horizontally</source>
        <translation type="vanished">ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>switch desktop</source>
        <translation type="vanished">ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠎ᠤᠨ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="438"/>
        <source>more gesture</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠭᠠᠷ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
    </message>
</context>
</TS>
