<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>GestureGuidance</name>
    <message>
        <source>Gesture Guidance</source>
        <translation>ལག་བརྡའི་མཛུབ་སྟོན།</translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <source>PushButton</source>
        <translation>མཐེབ་གནོན།</translation>
    </message>
</context>
<context>
    <name>TouchScreen</name>
    <message>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <source>Label1</source>
        <translation>ཤོག་བྱང་1</translation>
    </message>
    <message>
        <source>Label2</source>
        <translation>ཤོག་བྱང་2</translation>
    </message>
    <message>
        <source>touchscreen gesture</source>
        <translation>ལག་ཐོགས་ཁ་པར་གྱི་ལག་བརྡ།</translation>
        <extra-contents_path>/TouchScreen/touchscreen gesture</extra-contents_path>
    </message>
    <message>
        <source>more gesture</source>
        <translation>ལག་བརྡ་དེ་བས་མང་བ།</translation>
        <extra-contents_path>/TouchScreen/more gesture</extra-contents_path>
    </message>
    <message>
        <source>swipe up from the bottom edge</source>
        <translation>མཛུབ་མོ་འཆར་ངོས་ཀྱི་ཞབས་ནས་ཡར་ལ་ཤུད་སོང་།</translation>
    </message>
    <message>
        <source>open the multitasking view</source>
        <translation>ལས་འགན་མང་པོའི་ལྟ་ཚུལ་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>swipe down from the top edge</source>
        <translation>འཆར་ངོས་ཀྱི་རྩེ་མོ་ནས་མར་ལ་འདྲེད་འགུལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>show desktop</source>
        <translation>ཅོག་ཙེའི་ངོས་སུ་མངོན</translation>
    </message>
    <message>
        <source>swipe left from the right edge</source>
        <translation>གཡས་ཕྱོགས་ནས་གཡོན་ཕྱོགས་སུ་འཐེན་པ།</translation>
    </message>
    <message>
        <source>show sidebar</source>
        <translation>འགྲེམས་སྟོན་གྱི་གཞོགས་ངོས་མངོན་པ།</translation>
    </message>
    <message>
        <source>swipe four fingers down anywhere</source>
        <translation>ས་ཆ་གང་དང་གང་དུ་མཛུབ་མོ་བཞི་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <source>show global search</source>
        <translation>ཁྱོན་ཡོངས་ནས་འཚོལ་ཞིབ་བྱས།</translation>
    </message>
    <message>
        <source>swipe four fingers horizontally</source>
        <translation>མཛུབ་མོ་བཞི་ཅན་གྱི་འཆར་ངོས་གཡོན་ཕྱོགས་སམ་གཡས་ཕྱོགས་སུ་འདྲེད་འགུལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>switch windows</source>
        <translation>ཁ་ཕྱེ་བའི་སྒེའུ་ཁུང་བར་དུ་བརྗེ་སྤོར་བྱས།</translation>
    </message>
</context>
<context>
    <name>TouchscreenSettings</name>
    <message>
        <source>GestureGuidance</source>
        <translation>ལག་བརྡའི་ཕྱོགས་སྟོན།</translation>
    </message>
    <message>
        <source>TouchScreen</source>
        <translation>ཐུག་རག་བརྙན་ཡོལ།</translation>
    </message>
</context>
</TS>
