<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>GestureGuidance</name>
    <message>
        <source>Gesture</source>
        <translation type="vanished">手势</translation>
    </message>
    <message>
        <location filename="../gestureguidance.ui" line="72"/>
        <source>Gesture Guidance</source>
        <translation>ᠭᠠᠷ᠎ᠤᠨ ᠳᠤᠬᠢᠶ᠎ᠠ᠎ᠪᠠᠷ ᠬᠦᠳᠦᠯᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <location filename="../item-widget/itemwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../item-widget/itemwidget.ui" line="80"/>
        <location filename="../item-widget/itemwidget.ui" line="195"/>
        <location filename="../item-widget/itemwidget.ui" line="230"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../item-widget/itemwidget.ui" line="112"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../touchscreen-settings.cpp" line="86"/>
        <source>swipe up from the bottom edge</source>
        <translation>ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ᠎ᠢᠶᠠᠨ ᠳᠡᠯᠬᠡᠴᠡᠨ᠎ᠦ ᠳᠤᠤᠷ᠎ᠠ ᠵᠠᠬ᠎ᠠ᠎ᠠᠴᠠ ᠳᠡᠭᠡᠭ᠌ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="87"/>
        <source>open the multitasking view</source>
        <translation>ᠤᠯᠠᠨ ᠡᠬᠦᠷᠬᠡᠳᠦ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="88"/>
        <source>swipe four fingers horizontally</source>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠤᠶᠤ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="89"/>
        <source>switch windows</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠭ᠌ᠰᠡᠨ ᠴᠤᠩᠬᠤᠨ᠎ᠤ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="90"/>
        <source>swipe four fingers down anywhere</source>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠡᠯᠬᠡᠴᠡᠨ᠎ᠦ ᠶᠠᠮᠠᠷᠪᠠ ᠪᠠᠢᠷᠢ᠎ᠠᠴᠠ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="91"/>
        <source>show global search</source>
        <translation>ᠪᠦᠷᠢᠨ ᠬᠠᠢᠯᠲᠠ᠎ᠶ᠋ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="92"/>
        <source>swipe left from the right edge</source>
        <translation>ᠭᠠᠭ᠍ᠴᠠ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠪᠠᠷᠠᠭᠤᠨ ᠭᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠠᠴᠠ ᠵᠡᠬᠦᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠵᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="93"/>
        <source>show sidebar</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠪᠠᠭᠠᠷ᠎ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TouchScreen</name>
    <message>
        <location filename="../touchscreen.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="43"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="133"/>
        <source>Label1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="247"/>
        <source>Label2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="85"/>
        <source>touchscreen gesture</source>
        <translatorcomment>触摸屏手势</translatorcomment>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠳᠡᠯᠬᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠭᠠᠷ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
        <extra-contents_path>/TouchScreen/touchscreen gesture</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="87"/>
        <source>more gesture</source>
        <translatorcomment>更多手势</translatorcomment>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠭᠠᠷ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
        <extra-contents_path>/TouchScreen/more gesture</extra-contents_path>
    </message>
    <message>
        <source>swipe up from the bottom edge</source>
        <translatorcomment>手指从屏幕底部边缘向上滑动</translatorcomment>
        <translation type="vanished">ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠎ᠢᠶᠠᠨ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠎ᠦ ᠳᠤᠤᠷ᠎ᠠ ᠵᠠᠬ᠎ᠠ ᠎ᠠᠴᠠ ᠳᠡᠭᠡᠭ᠌ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>open the multitasking view</source>
        <translatorcomment>打开多任务视图</translatorcomment>
        <translation type="vanished">ᠤᠯᠠᠨ ᠡᠬᠦᠷᠬᠡᠳᠦ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>swipe down from the top edge</source>
        <translatorcomment>单指从屏幕顶端向下滑动</translatorcomment>
        <translation type="vanished">ᠭᠠᠭ᠍ᠴᠠ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠎ᠦ ᠳᠡᠭᠡᠷ᠎ᠡ ᠵᠠᠬ᠎ᠠ ᠎ᠡᠴᠡ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>show desktop</source>
        <translatorcomment>显示桌面</translatorcomment>
        <translation type="vanished">ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <source>swipe left from the right edge</source>
        <translatorcomment>单指从屏幕右侧向左滑入</translatorcomment>
        <translation type="vanished">ᠭᠠᠭ᠍ᠴᠠ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠎ᠦ ᠪᠠᠷᠠᠭᠤᠨ ᠭᠠᠷ ᠲᠠᠯ᠎ᠠ ᠎ᠠᠴᠠ ᠵᠡᠬᠦᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠵᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <source>show sidebar</source>
        <translatorcomment>呼出侧边栏</translatorcomment>
        <translation type="vanished">ᠬᠠᠵᠠᠭᠤ ᠪᠠᠭᠠᠷ ᠎ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>swipe four fingers down anywhere</source>
        <translatorcomment>四指屏幕任意位置下滑</translatorcomment>
        <translation type="vanished">ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠬᠡᠴᠡᠨ ᠎ᠦ ᠶᠠᠮᠠᠷᠪᠠ ᠪᠠᠢᠷᠢ ᠎ᠠᠴᠠ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>show global search</source>
        <translatorcomment>呼出全局搜索</translatorcomment>
        <translation type="vanished">ᠪᠦᠷᠢᠨ ᠬᠠᠢᠯᠲᠠ ᠎ᠶ᠋ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>swipe four fingers horizontally</source>
        <translatorcomment>四指屏幕向左或向右滑动</translatorcomment>
        <translation type="vanished">ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡ ᠎ᠶ᠋ᠢ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠤᠶᠤ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>switch windows</source>
        <translatorcomment>在打开的窗口之间切换</translatorcomment>
        <translation type="vanished">ᠨᠡᠬᠡᠬᠡᠭ᠌ᠰᠡᠨ ᠴᠤᠩᠬᠤᠨ ᠎ᠤ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TouchscreenSettings</name>
    <message>
        <location filename="../touchscreen-settings.cpp" line="49"/>
        <source>GestureGuidance</source>
        <translation>ᠭᠠᠷ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ ᠎ᠪᠠᠷ ᠬᠦᠳᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="51"/>
        <source>TouchScreen</source>
        <translatorcomment>触摸屏</translatorcomment>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="55"/>
        <source>MaxTabletAndTouchScreen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="56"/>
        <source>TabletAndTouchScreen</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="78"/>
        <source>swipe left from the right edge</source>
        <translation>ᠭᠠᠭ᠍ᠴᠠ ᠬᠤᠷᠤᠭᠤ᠎ᠪᠠᠷ ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠪᠠᠷᠠᠭᠤᠨ ᠭᠠᠷ ᠲᠠᠯ᠎ᠠ᠎ᠠᠴᠠ ᠵᠡᠬᠦᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠵᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="79"/>
        <source>show sidebar</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠪᠠᠭᠠᠷ᠎ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="81"/>
        <source>Max Tablet Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="82"/>
        <source>auto switch max tablet mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="96"/>
        <source>Tablet Mode</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen-settings.cpp" line="96"/>
        <source>auto switch tablet mode</source>
        <translation></translation>
    </message>
</context>
</TS>
