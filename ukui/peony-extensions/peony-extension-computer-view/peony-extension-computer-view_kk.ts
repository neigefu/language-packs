<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk" sourcelanguage="en_GB">
<context>
    <name>ComputerItemDelegate</name>
    <message>
        <location filename="computer-view/computer-item-delegate.cpp" line="155"/>
        <source>You should mount volume first</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputerNetworkItem</name>
    <message>
        <location filename="computer-view/computer-network-item.cpp" line="45"/>
        <source>Network Neighborhood</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputerRemoteVolumeItem</name>
    <message>
        <location filename="computer-view/computer-remote-volume-item.cpp" line="62"/>
        <source>Remote</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputerUserShareItem</name>
    <message>
        <source>User Share</source>
        <translation type="vanished">本机共享</translation>
    </message>
    <message>
        <location filename="computer-view/computer-user-share-item.cpp" line="20"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputerVolumeItem</name>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="54"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="63"/>
        <location filename="computer-view/computer-volume-item.cpp" line="115"/>
        <source>File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="179"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginRemoteFilesystem</name>
    <message>
        <location filename="login-remote-filesystem.ui" line="41"/>
        <source>Connect to Sever</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="89"/>
        <source>server information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="111"/>
        <source>user information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="141"/>
        <source>tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="196"/>
        <source>user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="252"/>
        <source>password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="308"/>
        <source>protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="351"/>
        <source>server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="382"/>
        <source>directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="411"/>
        <source>SAMBA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="416"/>
        <source>FTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="445"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="476"/>
        <source>port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="508"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="513"/>
        <source>21</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="518"/>
        <source>137</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="523"/>
        <source>138</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="528"/>
        <source>139</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="533"/>
        <source>445</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="592"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="623"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ComputerViewContainer</name>
    <message>
        <location filename="computer-view-container.cpp" line="132"/>
        <source>Connect a server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sftp://, etc...</source>
        <translation type="vanished">如sftp://...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="150"/>
        <location filename="computer-view-container.cpp" line="155"/>
        <source>Unmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="162"/>
        <source>Eject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="188"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="206"/>
        <location filename="computer-view-container.cpp" line="223"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="208"/>
        <source>You have to mount this volume first</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DriveRename</name>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="165"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="177"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="177"/>
        <source>Device name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="181"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="189"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="195"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="208"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="181"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="189"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="195"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="208"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="peony-computer-view-plugin.h" line="42"/>
        <source>Computer View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="peony-computer-view-plugin.h" line="43"/>
        <source>Show drives, network and personal directories.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="674"/>
        <source>One or more programs prevented the unmount operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="675"/>
        <location filename="computer-view/computer-volume-item.cpp" line="679"/>
        <location filename="computer-view/computer-volume-item.cpp" line="683"/>
        <source>Unmount failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="vanished">错误：%1
是否强制卸载？</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="371"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="646"/>
        <source>The device has been mount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="667"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="675"/>
        <location filename="computer-view/computer-volume-item.cpp" line="683"/>
        <source>Error: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="678"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="679"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="706"/>
        <location filename="computer-view/computer-volume-item.cpp" line="721"/>
        <source>Eject failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
</context>
</TS>
