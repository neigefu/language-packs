<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn" sourcelanguage="en_GB">
<context>
    <name>ComputerItemDelegate</name>
    <message>
        <location filename="computer-view/computer-item-delegate.cpp" line="142"/>
        <source>You should mount volume first</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠤᠷᠢᠳᠠᠪᠡᠷ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ</translation>
    </message>
</context>
<context>
    <name>ComputerNetworkItem</name>
    <message>
        <location filename="computer-view/computer-network-item.cpp" line="45"/>
        <source>Network Neighborhood</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠡᠷᠭᠡᠯᠡᠳᠡᠭᠡ ᠠᠢᠯ</translation>
    </message>
</context>
<context>
    <name>ComputerRemoteVolumeItem</name>
    <message>
        <location filename="computer-view/computer-remote-volume-item.cpp" line="84"/>
        <source>Remote</source>
        <translation>ᠠᠯᠤᠰ᠎ᠤ᠋ᠨ ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>ComputerUserShareItem</name>
    <message>
        <source>User Share</source>
        <translation type="vanished">本机共享</translation>
    </message>
    <message>
        <location filename="computer-view/computer-user-share-item.cpp" line="20"/>
        <source>Data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
</context>
<context>
    <name>ComputerVolumeItem</name>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="54"/>
        <source>Volume</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ᠎ᠤ᠋ᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="63"/>
        <location filename="computer-view/computer-volume-item.cpp" line="115"/>
        <source>File System</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="179"/>
        <source>Data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
</context>
<context>
    <name>LoginRemoteFilesystem</name>
    <message>
        <location filename="login-remote-filesystem.ui" line="41"/>
        <source>Connect to Sever</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ᠎ᠲᠦ᠍ ᠵᠠᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="89"/>
        <source>server information</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠮᠡᠳᠡᠭᠡᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="111"/>
        <source>user information</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="141"/>
        <source>tag</source>
        <translation>ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="196"/>
        <source>user</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="252"/>
        <source>password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="308"/>
        <source>protocol</source>
        <translation>ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="351"/>
        <source>server</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠦᠵᠦᠭᠦᠷ᠎ᠦ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="382"/>
        <source>directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="411"/>
        <source>SAMBA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="416"/>
        <source>FTP</source>
        <translation>FTP</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="445"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="476"/>
        <source>port</source>
        <translation>ᠫᠣᠷᠲ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="508"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="513"/>
        <source>21</source>
        <translation>21</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="518"/>
        <source>137</source>
        <translation>137</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="523"/>
        <source>138</source>
        <translation>138</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="528"/>
        <source>139</source>
        <translation>139</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="533"/>
        <source>445</source>
        <translation>445</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="592"/>
        <source>ok</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="623"/>
        <source>cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerViewContainer</name>
    <message>
        <location filename="computer-view-container.cpp" line="143"/>
        <source>Connect a server</source>
        <translation>ᠠᠯᠤᠰ᠎ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ᠎ᠲᠡᠢ ᠵᠠᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>sftp://, etc...</source>
        <translation type="vanished">如sftp://...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="171"/>
        <source>Unmount</source>
        <translation>ᠠᠴᠢᠶ᠎ᠠ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="185"/>
        <source>Eject</source>
        <translation>ᠤᠨᠵᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="222"/>
        <location filename="computer-view-container.cpp" line="252"/>
        <source>format</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="273"/>
        <location filename="computer-view-container.cpp" line="293"/>
        <source>Property</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠲᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="275"/>
        <source>You have to mount this volume first</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠤᠷᠢᠳᠠᠪᠡᠷ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRename</name>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="166"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="178"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="178"/>
        <source>Device name:</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠄</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="182"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="190"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="196"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="182"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ ᠦᠶᠡᠰ ᠲᠦᠷᠦᠭᠦᠦ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ ᠨᠢ ᠪᠤᠲᠠᠷᠬᠠᠢ ᠲᠣᠭᠠᠨ᠎ᠤ᠋ ᠴᠡᠭ ᠪᠣᠯᠬᠤ᠎ᠶ᠋ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ ᠂ ᠳᠠᠬᠢᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="190"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ᠎ᠠ᠋ᠴᠠ ᠬᠡᠲᠦᠷᠡᠵᠦ ᠂ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ !</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="196"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠨᠢ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠂ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠬᠦ ᠦᠦ ? ︖</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="peony-computer-view-plugin.h" line="42"/>
        <source>Computer View</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="peony-computer-view-plugin.h" line="43"/>
        <source>Show drives, network and personal directories</source>
        <oldsource>Show drives, network and personal directories.</oldsource>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠂ ᠨᠧᠲ᠎ᠦ᠋ᠨ ᠭᠠᠷᠴᠠᠭ ᠪᠠ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠭᠠᠷᠴᠠᠭ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="727"/>
        <source>One or more programs prevented the unmount operation.</source>
        <translation>ᠨᠢᠭᠡ ᠪᠤᠶᠤ ᠣᠯᠠᠨ ᠫᠷᠤᠭ᠌ᠷᠠᠮ ᠠᠴᠢᠶ᠎ᠠ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="728"/>
        <location filename="computer-view/computer-volume-item.cpp" line="732"/>
        <location filename="computer-view/computer-volume-item.cpp" line="736"/>
        <source>Unmount failed</source>
        <translation>ᠠᠴᠢᠶ᠎ᠠ ᠪᠠᠭᠤᠯᠲᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="vanished">错误：%1
是否强制卸载？</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="321"/>
        <location filename="computer-view/computer-volume-item.cpp" line="389"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠦ ᠪᠣᠯᠪᠠᠤ !︕</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="699"/>
        <source>The device has been mount successfully!</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠡᠯᠭᠦᠨ ᠠᠴᠢᠶᠠᠯᠠᠪᠠ !︕</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="720"/>
        <location filename="computer-view/computer-volume-item.cpp" line="755"/>
        <location filename="computer-view/computer-volume-item.cpp" line="796"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠪᠡ ᠂ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠮᠵᠢᠯᠲᠠ᠎ᠲᠠᠢ ᠪᠠᠭᠤᠯᠭᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="728"/>
        <location filename="computer-view/computer-volume-item.cpp" line="736"/>
        <source>Error: %1
</source>
        <translation>ᠲᠠᠰᠢᠶᠠᠷᠠᠯ ᠄ %1 n
</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="731"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ᠎ᠶ᠋ᠢ ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠲᠠ ᠪᠠᠷᠤᠭ ᠤᠷᠢᠳᠠᠪᠡᠷ ᠵᠠᠷᠢᠮ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠬᠠᠭᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠤ᠋ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠭᠤᠷ ᠵᠡᠷᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="732"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="764"/>
        <location filename="computer-view/computer-volume-item.cpp" line="790"/>
        <source>Eject device failed, the reason may be that the device has been removed, etc.</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠦᠰᠦᠷᠦᠨ ᠭᠠᠷᠴᠤ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠂ ᠪᠠᠷᠤᠭ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠭᠰᠡᠨ ᠵᠡᠷᠭᠡ ᠰᠢᠯᠲᠠᠭᠠᠨ ᠃</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="766"/>
        <location filename="computer-view/computer-volume-item.cpp" line="792"/>
        <source>Eject failed</source>
        <translation>ᠤᠨᠵᠢᠮᠠᠯ ᠢᠯᠠᠭᠳᠠᠯ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="328"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="328"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ ᠮᠠᠭᠠᠳ ᠂ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ !︕</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="73"/>
        <source>Message recipient disconnected from message bus without replying!</source>
        <oldsource>Message recipient disconnected from message bus without replying</oldsource>
        <translation>ᠮᠡᠳᠡᠭᠡ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠤᠭᠴᠢ ᠬᠠᠷᠢᠭᠤ ᠦᠭᠡᠢ ᠪᠠᠢᠳᠠᠯ ᠳᠣᠣᠷ᠎ᠠ ᠮᠡᠳᠡᠭᠡᠨ᠎ᠦ᠌ ᠶᠡᠷᠦᠩᠬᠡᠢ ᠤᠲᠠᠰᠤ᠎ᠲᠠᠢ ᠵᠠᠯᠭᠠᠯᠳᠤᠬᠤ᠎ᠪᠠᠨ ᠲᠠᠰᠤᠯᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="75"/>
        <source>log remote error</source>
        <translation>ᠡᠳᠦᠷ᠎ᠦ᠋ᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ᠎ᠦ᠋ᠨ ᠠᠯᠤᠰ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ</translation>
    </message>
</context>
</TS>
