<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant" sourcelanguage="en_GB">
<context>
    <name>ComputerItemDelegate</name>
    <message>
        <location filename="computer-view/computer-item-delegate.cpp" line="142"/>
        <source>You should mount volume first</source>
        <translation>需要首先掛載分區</translation>
    </message>
</context>
<context>
    <name>ComputerNetworkItem</name>
    <message>
        <location filename="computer-view/computer-network-item.cpp" line="45"/>
        <source>Network Neighborhood</source>
        <translation>網上鄰居</translation>
    </message>
</context>
<context>
    <name>ComputerRemoteVolumeItem</name>
    <message>
        <location filename="computer-view/computer-remote-volume-item.cpp" line="84"/>
        <source>Remote</source>
        <translation>遠端目錄</translation>
    </message>
</context>
<context>
    <name>ComputerUserShareItem</name>
    <message>
        <source>User Share</source>
        <translation type="vanished">本机共享</translation>
    </message>
    <message>
        <location filename="computer-view/computer-user-share-item.cpp" line="20"/>
        <source>Data</source>
        <translation>數據盤</translation>
    </message>
</context>
<context>
    <name>ComputerVolumeItem</name>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="54"/>
        <source>Volume</source>
        <translation>本地分區</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="63"/>
        <location filename="computer-view/computer-volume-item.cpp" line="115"/>
        <source>File System</source>
        <translation>檔案系統</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="179"/>
        <source>Data</source>
        <translation>數據盤</translation>
    </message>
</context>
<context>
    <name>LoginRemoteFilesystem</name>
    <message>
        <location filename="login-remote-filesystem.ui" line="41"/>
        <source>Connect to Sever</source>
        <translation>連接到伺服器</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="89"/>
        <source>server information</source>
        <translation>伺服器資訊</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="111"/>
        <source>user information</source>
        <translation>用戶資訊</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="141"/>
        <source>tag</source>
        <translation>標籤</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="196"/>
        <source>user</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="252"/>
        <source>password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="308"/>
        <source>protocol</source>
        <translation>協定</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="351"/>
        <source>server</source>
        <translation>服務端位址</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="382"/>
        <source>directory</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="411"/>
        <source>SAMBA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="416"/>
        <source>FTP</source>
        <translation>FTP</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="445"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="476"/>
        <source>port</source>
        <translation>埠</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="508"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="513"/>
        <source>21</source>
        <translation>21</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="518"/>
        <source>137</source>
        <translation>137</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="523"/>
        <source>138</source>
        <translation>138</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="528"/>
        <source>139</source>
        <translation>139</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="533"/>
        <source>445</source>
        <translation>445</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="592"/>
        <source>ok</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="623"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerViewContainer</name>
    <message>
        <location filename="computer-view-container.cpp" line="143"/>
        <source>Connect a server</source>
        <translation>連接遠端伺服器</translation>
    </message>
    <message>
        <source>sftp://, etc...</source>
        <translation type="vanished">如sftp://...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="171"/>
        <source>Unmount</source>
        <translation>卸載</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="185"/>
        <source>Eject</source>
        <translation>彈出</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="222"/>
        <location filename="computer-view-container.cpp" line="252"/>
        <source>format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="273"/>
        <location filename="computer-view-container.cpp" line="293"/>
        <source>Property</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="275"/>
        <source>You have to mount this volume first</source>
        <translation>需要首先掛載分區</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRename</name>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="166"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="178"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="178"/>
        <source>Device name:</source>
        <translation>裝置名稱：</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="182"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="190"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="196"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="182"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>重命名時不支援首字元為小數點，請重新輸入！</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="190"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>設備名超過字元限制，重命名失敗！</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="196"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>重命名將會卸載設備，是否繼續？</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="peony-computer-view-plugin.h" line="42"/>
        <source>Computer View</source>
        <translation>電腦檢視</translation>
    </message>
    <message>
        <location filename="peony-computer-view-plugin.h" line="43"/>
        <source>Show drives, network and personal directories</source>
        <oldsource>Show drives, network and personal directories.</oldsource>
        <translation>顯示設備分區， 網路目錄和個人目錄</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="727"/>
        <source>One or more programs prevented the unmount operation.</source>
        <translation>一個或多個程式阻止卸載操作。</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="728"/>
        <location filename="computer-view/computer-volume-item.cpp" line="732"/>
        <location filename="computer-view/computer-volume-item.cpp" line="736"/>
        <source>Unmount failed</source>
        <translation>卸載失敗</translation>
    </message>
    <message>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="vanished">错误：%1
是否强制卸载？</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="321"/>
        <location filename="computer-view/computer-volume-item.cpp" line="389"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>操作設備前需要同步數據，請稍等！</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="699"/>
        <source>The device has been mount successfully!</source>
        <translation>設備掛載成功！</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="720"/>
        <location filename="computer-view/computer-volume-item.cpp" line="755"/>
        <location filename="computer-view/computer-volume-item.cpp" line="796"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>數據同步完成，設備已經成功卸載！</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="728"/>
        <location filename="computer-view/computer-volume-item.cpp" line="736"/>
        <source>Error: %1
</source>
        <translation>錯誤： %1\n
</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="731"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>無法卸載，您可能需要先關閉一些程式，如分區編輯器等。</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="732"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="764"/>
        <location filename="computer-view/computer-volume-item.cpp" line="790"/>
        <source>Eject device failed, the reason may be that the device has been removed, etc.</source>
        <translation>設備彈出失敗，可能是設備已經移除等原因。</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="766"/>
        <location filename="computer-view/computer-volume-item.cpp" line="792"/>
        <source>Eject failed</source>
        <translation>彈出失敗</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="328"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="328"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>可能設備不支援重命名操作，重命名失敗！</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="73"/>
        <source>Message recipient disconnected from message bus without replying!</source>
        <oldsource>Message recipient disconnected from message bus without replying</oldsource>
        <translation>消息接收者在沒有回復的情況下與消息總線斷開連接！</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="75"/>
        <source>log remote error</source>
        <translation>日誌遠程錯誤</translation>
    </message>
</context>
</TS>
