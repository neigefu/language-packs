<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN" sourcelanguage="en_GB">
<context>
    <name>ComputerItemDelegate</name>
    <message>
        <location filename="computer-view/computer-item-delegate.cpp" line="142"/>
        <source>You should mount volume first</source>
        <translation>ཐོག་མར་ཐེག་ཁུལ་བགོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ComputerNetworkItem</name>
    <message>
        <location filename="computer-view/computer-network-item.cpp" line="45"/>
        <source>Network Neighborhood</source>
        <translation>དྲ་ཐོག་གི་གྲོང་པ།</translation>
    </message>
</context>
<context>
    <name>ComputerRemoteVolumeItem</name>
    <message>
        <location filename="computer-view/computer-remote-volume-item.cpp" line="84"/>
        <source>Remote</source>
        <translation>རྒྱང་ཁྲིད་དཀར་ཆག</translation>
    </message>
</context>
<context>
    <name>ComputerUserShareItem</name>
    <message>
        <source>User Share</source>
        <translation type="vanished">本机共享</translation>
    </message>
    <message>
        <location filename="computer-view/computer-user-share-item.cpp" line="20"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>ComputerVolumeItem</name>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="54"/>
        <source>Volume</source>
        <translation>ས་འདིའི་ཡན་ལག་ས་ཁུལ།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="63"/>
        <location filename="computer-view/computer-volume-item.cpp" line="115"/>
        <source>File System</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="179"/>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>LoginRemoteFilesystem</name>
    <message>
        <location filename="login-remote-filesystem.ui" line="41"/>
        <source>Connect to Sever</source>
        <translation>ཞབས་ཞུའི་འཕྲུལ་ཆས་དང་སྦྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="89"/>
        <source>server information</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="111"/>
        <source>user information</source>
        <translation>སྤྱོད་མཁན་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="141"/>
        <source>tag</source>
        <translation>ཡིག་བྱང་།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="196"/>
        <source>user</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="252"/>
        <source>password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="308"/>
        <source>protocol</source>
        <translation>གྲོས་མཐུན།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="351"/>
        <source>server</source>
        <translation>ཞབས་ཞུའི་སྣེའི་གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="382"/>
        <source>directory</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="411"/>
        <source>SAMBA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="416"/>
        <source>FTP</source>
        <translation>FTP</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="445"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="476"/>
        <source>port</source>
        <translation>མཐུད་སྣེ།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="508"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="513"/>
        <source>21</source>
        <translation>21</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="518"/>
        <source>137</source>
        <translation>137</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="523"/>
        <source>138</source>
        <translation>༡༣༨</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="528"/>
        <source>139</source>
        <translation>139.</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="533"/>
        <source>445</source>
        <translation>445</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="592"/>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="623"/>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerViewContainer</name>
    <message>
        <location filename="computer-view-container.cpp" line="143"/>
        <source>Connect a server</source>
        <translation>རྒྱང་སྦྲེལ་ཞབས་ཞུའི་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <source>sftp://, etc...</source>
        <translation type="vanished">如sftp://...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="171"/>
        <source>Unmount</source>
        <translation>བཤིག་འདོན།</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="185"/>
        <source>Eject</source>
        <translation>དཀྲོལ་བ།</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="222"/>
        <location filename="computer-view-container.cpp" line="252"/>
        <source>format</source>
        <translation>རྣམ་གཞག་ཅན།</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="273"/>
        <location filename="computer-view-container.cpp" line="293"/>
        <source>Property</source>
        <translation>གཏོགས་གཤིས།</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="275"/>
        <source>You have to mount this volume first</source>
        <translation>ཐོག་མར་ཐེག་ཁུལ་བགོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRename</name>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="166"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="178"/>
        <source>Rename</source>
        <translation>མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="178"/>
        <source>Device name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="182"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="190"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="196"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="182"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>མིང་བསྐྱར་འདོགས་བྱེད་དུས་ཡིག་རྟགས་དེ་ཚེག་ཆུང་ཡིན་པར་རྒྱབ་སྐྱོར་མི་བྱེད་པས་ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="190"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་དེ་ཡིག་རྟགས་ཀྱི་ཚད་བཀག་ལས་བརྒལ་ཏེ་བསྐྱར་དུ་མིང་བཏགས་ནས་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="196"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>བསྐྱར་མིང་བཏགས་བྱས་ན་སྒྲིག་ཆས་བཤིག་འདོན་བྱེད་སྲིད།མུ་མཐུད་དུ་བྱེད་དམ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="peony-computer-view-plugin.h" line="42"/>
        <source>Computer View</source>
        <translation>གློག་ཀླད་ཀྱི་མཐོང་རིས།</translation>
    </message>
    <message>
        <location filename="peony-computer-view-plugin.h" line="43"/>
        <source>Show drives, network and personal directories</source>
        <oldsource>Show drives, network and personal directories.</oldsource>
        <translation>མངོན་འཆར་སྒྲིག་ཆས་དབྱེ་ཁུལ།དྲ་རྒྱའི་དཀར་ཆག་དང་མི་སྒེར་གྱི་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="727"/>
        <source>One or more programs prevented the unmount operation.</source>
        <translation>བྱ་རིམ་གཅིག་གམ་མང་པོ་བསུབ་ནས་བཀོལ་སྤྱོད་བྱེད་པར་བཀག་འགོག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="728"/>
        <location filename="computer-view/computer-volume-item.cpp" line="732"/>
        <location filename="computer-view/computer-volume-item.cpp" line="736"/>
        <source>Unmount failed</source>
        <translation>བསུབ་རྒྱུ་ལེགས་འགྲུབ་མ་བྱུང་།</translation>
    </message>
    <message>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="vanished">错误：%1
是否强制卸载？</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="321"/>
        <location filename="computer-view/computer-volume-item.cpp" line="389"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>སྒྲིག་ཆས་བཀོལ་སྤྱོད་བྱེད་པའི་སྔོན་ལ་དུས་མཉམ་དུ་གཞི་གྲངས་དགོས།ཅུང་ཙམ་སྒུག་རོགས།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="699"/>
        <source>The device has been mount successfully!</source>
        <translation>སྒྲིག་ཆས་འགེལ་འཇོག་ལེགས་འགྲུབ་བྱུང་སོང་།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="720"/>
        <location filename="computer-view/computer-volume-item.cpp" line="755"/>
        <location filename="computer-view/computer-volume-item.cpp" line="796"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>གཞི་གྲངས་དུས་མཉམ་དུ་འགྲུབ་སོང་།སྒྲིག་ཆས་བདེ་ལེགས་ངང་བསུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="728"/>
        <location filename="computer-view/computer-volume-item.cpp" line="736"/>
        <source>Error: %1
</source>
        <translation>ནོར་འཁྲུལ།%，n
</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="731"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>བསུབ་ཐབས་མེད།ཁྱེད་ཀྱིས་སྔོན་ལ་གོ་རིམ་འགའ་སྒོ་རྒྱག་དགོས།དཔེར་ན་ཁུལ་བགོ་རྩོམ་སྒྲིག་ཆས་སོགས།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="732"/>
        <source>%1</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="764"/>
        <location filename="computer-view/computer-volume-item.cpp" line="790"/>
        <source>Eject device failed, the reason may be that the device has been removed, etc.</source>
        <translation>སྒྲིག་ཆས་ལ་ཕམ་ཁ་བྱུང་བ་ནི་སྒྲིག་ཆས་མེད་པར་བཟོས་པ་སོགས་ཀྱི་རྒྱུ་རྐྱེན་ཡིན་སྲིད།</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="766"/>
        <location filename="computer-view/computer-volume-item.cpp" line="792"/>
        <source>Eject failed</source>
        <translation>ཕམ་ཁ་བྱུང་།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="328"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="328"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>སྒྲིག་ཆས་ཀྱིས་བསྐྱར་དུ་མིང་བཏགས་ནས་བཀོལ་སྤྱོད་བྱེད་པར་རྒྱབ་སྐྱོར་མི་བྱེད་ཀྱང་སྲིད།བསྐྱར་དུ་མིང་བཏགས་ནས་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="73"/>
        <source>Message recipient disconnected from message bus without replying!</source>
        <oldsource>Message recipient disconnected from message bus without replying</oldsource>
        <translation>བརྡ་འཕྲིན་བསྡུ་ལེན་མཁན་གྱིས་ལན་མ་བཏབ་པའི་གནས་ཚུལ་འོག་བརྡ་འཕྲིན་སྐུད་པ་དང་འབྲེལ་མཐུད་བྱས།</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="75"/>
        <source>log remote error</source>
        <translation>ཉིན་ཐོའི་རྒྱང་རིང་ནོར་འཁྲུལ།</translation>
    </message>
</context>
</TS>
