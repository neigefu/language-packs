<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::SetWallPaperPlugin</name>
    <message>
        <location filename="../set-wallpaper-plugin.h" line="42"/>
        <source>Peony-Qt set wallpaper Extension</source>
        <translation>ᠹᠧᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ᠎ᠲᠤ᠌ ᠬᠠᠨᠠᠨ ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../set-wallpaper-plugin.h" line="43"/>
        <source>set wallpaper Extension</source>
        <translation>ᠬᠠᠨ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠴᠠᠭᠠᠰᠤᠨ᠎ᠤ᠋ ᠵᠠᠯᠭᠠᠰᠤ᠎ᠶ᠋ᠢ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>set wallpaper Extension.</source>
        <translation type="vanished">设置壁纸插件。</translation>
    </message>
    <message>
        <location filename="../set-wallpaper-plugin.cpp" line="65"/>
        <source>Set as wallpaper</source>
        <translation>ᠬᠠᠨ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠴᠠᠭᠠᠰᠤ ᠪᠣᠯᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
