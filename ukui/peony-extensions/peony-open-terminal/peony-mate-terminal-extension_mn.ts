<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::MateTerminalMenuPlugin</name>
    <message>
        <location filename="../mate-terminal-menu-plugin.cpp" line="176"/>
        <location filename="../mate-terminal-menu-plugin.cpp" line="195"/>
        <source>Open Directory in Terminal</source>
        <translation>ᠦᠵᠦᠭᠦᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.h" line="43"/>
        <source>Peony-Qt Mate Terminal Menu Extension</source>
        <translation>UKUI ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠠᠭᠤᠷ᠎ᠤ᠋ᠨ ᠦᠵᠦᠭᠦᠷ᠎ᠦ᠋ᠨ ᠥᠷᠭᠡᠳᠬᠡᠯ</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.h" line="44"/>
        <source>Open Terminal with menu</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠳᠠᠷᠤᠪᠴᠢ ᠲᠣᠪᠶᠣᠭ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠦᠵᠦᠭᠦᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠨ ᠠ</translation>
    </message>
    <message>
        <source>Open Terminal with menu.</source>
        <translation type="vanished">使用右键菜单打开终端。</translation>
    </message>
    <message>
        <source>Open Directory in T&amp;erminal</source>
        <translation type="vanished">打开终端(&amp;E)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mate-terminal-menu-plugin.cpp" line="145"/>
        <source>Open terminal fail</source>
        <translation>ᠦᠵᠦᠭᠦᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.cpp" line="146"/>
        <source>Open terminal failed, did you removed the default terminal?  If it&apos;s true please reinstall it.</source>
        <translation>ᠦᠵᠦᠭᠦᠷ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠ ᠠᠶᠠᠳᠠᠯ ᠦᠵᠦᠭᠦᠷ᠎ᠢ᠋ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠥᠩᠭᠡᠷᠡᠭᠰᠡᠨ ᠦᠦ ? ︖ ᠬᠡᠷᠪᠡ ᠮᠥᠨ ᠪᠣᠯ ᠳᠠᠬᠢᠨ ᠰᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃</translation>
    </message>
    <message>
        <source>Open terminal failed, did you removed the default terminal?  If you have reinstalled it please logout and relogin.</source>
        <translation type="vanished">打开终端失败，您是否卸载过默认终端？如果已经重新安装了，请注销并重新登录后生效。</translation>
    </message>
    <message>
        <source>Open terminal failed, did you removed the default terminal?</source>
        <translation type="vanished">打开终端失败，您是否卸载了默认的终端应用？</translation>
    </message>
</context>
</TS>
