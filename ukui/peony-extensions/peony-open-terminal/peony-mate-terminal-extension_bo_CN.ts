<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::MateTerminalMenuPlugin</name>
    <message>
        <location filename="../mate-terminal-menu-plugin.cpp" line="176"/>
        <location filename="../mate-terminal-menu-plugin.cpp" line="195"/>
        <source>Open Directory in Terminal</source>
        <translation>མཐའ་སྣེ།</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.h" line="43"/>
        <source>Peony-Qt Mate Terminal Menu Extension</source>
        <translation>UKUIཡིག་ཆ་དོ་དམ་ཆས་ཀྱི་མཐའ་སྣེ་རྒྱ་བསྐྱེད།</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.h" line="44"/>
        <source>Open Terminal with menu</source>
        <translation>གཡས་མཐེབ་ཚལ་ཐོ་བཀོལ་ནས་མཐའ་སྣེ།</translation>
    </message>
    <message>
        <source>Open Terminal with menu.</source>
        <translation type="vanished">使用右键菜单打开终端。</translation>
    </message>
    <message>
        <source>Open Directory in T&amp;erminal</source>
        <translation type="vanished">打开终端(&amp;E)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../mate-terminal-menu-plugin.cpp" line="145"/>
        <source>Open terminal fail</source>
        <translation>མཐའ་སྣེའི་སྒོ་ཕྱེ་ནས་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../mate-terminal-menu-plugin.cpp" line="146"/>
        <source>Open terminal failed, did you removed the default terminal?  If it&apos;s true please reinstall it.</source>
        <translation>མཐའ་སྣེའི་ཁ་ཕྱེ་ནས་ཕམ་སོང་།ཁྱེད་ཀྱིས་ཐོག་མའི་ཁས་ལེན་གྱི་མཐའ་སྣེ་བསུབ་མྱོང་ངམ།གལ་ཏེ་ཡིན་ན་ཡང་བསྐྱར་སྒྲིག་སྦྱོར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Open terminal failed, did you removed the default terminal?  If you have reinstalled it please logout and relogin.</source>
        <translation type="vanished">打开终端失败，您是否卸载过默认终端？如果已经重新安装了，请注销并重新登录后生效。</translation>
    </message>
    <message>
        <source>Open terminal failed, did you removed the default terminal?</source>
        <translation type="vanished">打开终端失败，您是否卸载了默认的终端应用？</translation>
    </message>
</context>
</TS>
