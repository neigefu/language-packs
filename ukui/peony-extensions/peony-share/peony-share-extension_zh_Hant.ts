<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AdvancedSharePage</name>
    <message>
        <location filename="../advanced-share-page.cpp" line="58"/>
        <source>Advanced share</source>
        <translation>高級共用</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="109"/>
        <source>Samba set user password</source>
        <translation>Samba設置用戶密碼</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="107"/>
        <source>Samba password:</source>
        <translation>Samba密碼：</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba設置密碼失敗，請重新輸入！</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>The user has not set the samba password. If you need to log in to %1, you can set it in the upper right menu of the file manager</source>
        <translation>該用戶沒有設置samba密碼。 你需要登錄%1使用者，可以在檔管理員的右上功能表中進行設置samba密碼</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>共用配置服務異常，請確認是否有正在進行的共用配置操作，或者請重新設置共用！</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>User</source>
        <translation>使用者</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Writable</source>
        <translation>可寫</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Readonly</source>
        <translation>唯讀</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Reject</source>
        <translation>拒絕</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="365"/>
        <source>Share permission settings</source>
        <translation>共用許可權設置</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="377"/>
        <source>delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="394"/>
        <source>Add</source>
        <translation>增加</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="411"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="412"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>NetUsershareHelper</name>
    <message>
        <source>Peony-Qt-Share-Extension</source>
        <translation type="vanished">共享</translation>
    </message>
</context>
<context>
    <name>Peony::SharePropertiesPagePlugin</name>
    <message>
        <location filename="../share-properties-page-plugin.h" line="44"/>
        <source>Peony Qt Share Extension</source>
        <translation>共用</translation>
    </message>
    <message>
        <location filename="../share-properties-page-plugin.h" line="45"/>
        <source>Allow user share folders</source>
        <translation>允許共享資料夾</translation>
    </message>
</context>
<context>
    <name>SharePage</name>
    <message>
        <location filename="../share-page.cpp" line="453"/>
        <source>Share folder</source>
        <translation>共用資料夾</translation>
    </message>
    <message>
        <source>Share:</source>
        <translation type="vanished">共享:</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="142"/>
        <source>The folder is currently shared in non-read-only mode, and setting up the share requires adding other people&apos;s write permissions to the current folder?</source>
        <translation>該資料夾當前共用模式為非只讀模式，是否需要自動增加當前資料夾增加其他人的寫許可權？</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <location filename="../share-page.cpp" line="143"/>
        <location filename="../share-page.cpp" line="197"/>
        <source>question</source>
        <translation>詢問</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="145"/>
        <location filename="../share-page.cpp" line="199"/>
        <source>Confirm adding permissions</source>
        <translation>確定增加許可權</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="146"/>
        <location filename="../share-page.cpp" line="200"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <location filename="../share-page.cpp" line="113"/>
        <location filename="../share-page.cpp" line="174"/>
        <location filename="../share-page.cpp" line="183"/>
        <location filename="../share-page.cpp" line="229"/>
        <location filename="../share-page.cpp" line="239"/>
        <location filename="../share-page.cpp" line="283"/>
        <location filename="../share-page.cpp" line="320"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <source>The share name must not contain %1, and cannot start with a dash (-) or whitespace, or end with whitespace.</source>
        <translation>共用名不能包含%1，並且不能以-開頭或者空格開頭，或者空格結尾</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="113"/>
        <source>The share name cannot be the same as the current user name</source>
        <translation>共用名不能和當前使用者名相同</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <source>The share name is already used by other users. You can rename the folder and then set the share.</source>
        <translation>該共用名已經被其他使用者使用，可以重命名該資料夾后再設置共用</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="196"/>
        <source>The folder is currently shared with anonymous access set, do I need to automatically increase the executable permissions of the current file and others in the ancestor directory?</source>
        <translation>該資料夾目前共用設置了匿名訪問，是否需要自動增加當前檔和祖宗目錄的其他人的可執行許可權？</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="283"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>共用配置服務異常，請確認是否有正在進行的共用配置操作，或者請重新設置共用！</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="295"/>
        <source>Samba set user password</source>
        <translation>Samba設置用戶密碼</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="293"/>
        <source>Samba password:</source>
        <translation>Samba密碼：</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="320"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba設置密碼失敗，請重新輸入！</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="421"/>
        <source>usershare</source>
        <translation>本機共用</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="471"/>
        <source>share this folder</source>
        <translation>共用此資料夾</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="478"/>
        <source>don`t share this folder</source>
        <translation>不共用此資料夾</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="504"/>
        <source>Share name:</source>
        <translation>共用名稱：</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="509"/>
        <source>Read Only</source>
        <translation>唯讀</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="512"/>
        <source>Allow Anonymous</source>
        <translation>允許遊客訪問</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="520"/>
        <source>Advanced Sharing</source>
        <translation>高級共用</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="505"/>
        <source>Comment:</source>
        <translation>註解：</translation>
    </message>
</context>
</TS>
