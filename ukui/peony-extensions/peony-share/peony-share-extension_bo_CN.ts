<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AdvancedSharePage</name>
    <message>
        <location filename="../advanced-share-page.cpp" line="58"/>
        <source>Advanced share</source>
        <translation>མཐོ་རིམ་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="109"/>
        <source>Samba set user password</source>
        <translation>Sambaཡིས་མཛད་སྤྱོད་པའི་གསང་གྲངས་བཞག་ཡོད།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="107"/>
        <source>Samba password:</source>
        <translation>Sambaགསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Sambaཡིས་གསང་གྲངས་བཞག་ནས་ཕམ་སོང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>Tips</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>The user has not set the samba password. If you need to log in to %1, you can set it in the upper right menu of the file manager</source>
        <translation>སྤྱོད་མཁན་དེས་sambaགསང་གྲངས་བཞག་མེད།ཁྱོད་ཀྱིས་བརྒྱ་ཆའི་༡ཐོ་འགོད་བྱེད་དགོས།ཡིག་ཆའི་བདག་དམ་ཆས་ཀྱི་གཡས་ཕྱོགས་ཀྱི་ཚོད་མའི་ཐོ་སྟེང་དུ་sambaགསང་གྲངས་བཞག་ཆོག</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>མཉམ་སྤྱོད་སྒྲིག་ཆས་ཀྱི་ཞབས་ཞུ་རྒྱུན་ལྡན་མིན་པས།ད་ལྟ་སྒྲུབ་བཞིན་པའི་མཉམ་སྤྱོད་སྒྲིག་ཆས་ཡོད་མེད་དང་།ཡང་ན་བསྐྱར་དུ་བཀོད་སྒྲིག་དང་མཉམ་སྤྱོད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Writable</source>
        <translation>འབྲི།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Readonly</source>
        <translation>ཀློག་པ་ཙམ།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Reject</source>
        <translation>དང་ལེན་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="365"/>
        <source>Share permission settings</source>
        <translation>མཉམ་སྤྱོད་དབང་ཚད་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="377"/>
        <source>delete</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="394"/>
        <source>Add</source>
        <translation>ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="411"/>
        <source>Save</source>
        <translation>ཉར་ཚགས།</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="412"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>NetUsershareHelper</name>
    <message>
        <source>Peony-Qt-Share-Extension</source>
        <translation type="vanished">共享</translation>
    </message>
</context>
<context>
    <name>Peony::SharePropertiesPagePlugin</name>
    <message>
        <location filename="../share-properties-page-plugin.h" line="44"/>
        <source>Peony Qt Share Extension</source>
        <translation>མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../share-properties-page-plugin.h" line="45"/>
        <source>Allow user share folders</source>
        <translation>ཡིག་ཆའི་སྒམ་མཉམ་སྤྱོད་བྱས་ཆོག</translation>
    </message>
</context>
<context>
    <name>SharePage</name>
    <message>
        <location filename="../share-page.cpp" line="453"/>
        <source>Share folder</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <source>Share:</source>
        <translation type="vanished">共享:</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="142"/>
        <source>The folder is currently shared in non-read-only mode, and setting up the share requires adding other people&apos;s write permissions to the current folder?</source>
        <translation>ཡིག་སྣོད་དེ་མིག་སྔར་མཉམ་སྤྱོད་བྱེད་སྟངས་ནི་ཀློག་སྟངས་ཁོ་ན་མ་ཡིན་པས་མིག་སྔར་ཡིག་སྣོད་གཞན་གྱི་འབྲི་དབང་རང་འགུལ་ངང་ཆེ་རུ་གཏོང་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <location filename="../share-page.cpp" line="143"/>
        <location filename="../share-page.cpp" line="197"/>
        <source>question</source>
        <translation>འདྲི་རྩད།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="145"/>
        <location filename="../share-page.cpp" line="199"/>
        <source>Confirm adding permissions</source>
        <translation>འཕར་སྣོན་དབང་ཚད།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="146"/>
        <location filename="../share-page.cpp" line="200"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <location filename="../share-page.cpp" line="113"/>
        <location filename="../share-page.cpp" line="174"/>
        <location filename="../share-page.cpp" line="183"/>
        <location filename="../share-page.cpp" line="229"/>
        <location filename="../share-page.cpp" line="239"/>
        <location filename="../share-page.cpp" line="283"/>
        <location filename="../share-page.cpp" line="320"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <source>The share name must not contain %1, and cannot start with a dash (-) or whitespace, or end with whitespace.</source>
        <translation>མཉམ་སྤྱོད་ཀྱི་མིང་ལ་བརྒྱ་ཆ1མི་འདུས་པ་མ་ཟད།མགོ་བརྩམས་པའམ་སྟོང་ཆའི་ཐོག་ནས་འགོ་བརྩམས་མི་ཆོག་པའམ་ཡང་ན་སྟོང་ཆའི་མཇུག་བསྡུས་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="113"/>
        <source>The share name cannot be the same as the current user name</source>
        <translation>མཉམ་སྤྱོད་ཀྱི་མིང་དེ་མིག་སྔའི་སྤྱོད་མཁན་གྱི་མིང་དང་གཅིག་མཚུངས་ཡིན་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <source>The share name is already used by other users. You can rename the folder and then set the share.</source>
        <translation>མཉམ་སྤྱོད་ཀྱི་མིང་དེ་སྤྱོད་མཁན་གཞན་གྱིས་བེད་སྤྱོད་བཏང་ཟིན་པས་ཡིག་ཆའི་སྒམ་དེའི་མིང་བསྐྱར་དུ་བཏགས་རྗེས་མཉམ་སྤྱོད་བྱས་ཆོག</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="196"/>
        <source>The folder is currently shared with anonymous access set, do I need to automatically increase the executable permissions of the current file and others in the ancestor directory?</source>
        <translation>ཡིག་སྣོད་དེའི་ནང་མིག་སྔར་མིང་བཀབ་ནས་འཚམས་འདྲི་བྱས་པ་མཉམ་སྤྱོད་བྱས་ཡོད་པས་མིག་སྔར་ཡིག་ཆ་དང་མེས་པོའི་དཀར་ཆག་གི་མི་གཞན་གྱི་ལག་བསྟར་ཆོག་པའི་དབང་ཚད་རང་འགུལ་ངང་མང་དུ་གཏོང་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="283"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>མཉམ་སྤྱོད་སྒྲིག་ཆས་ཀྱི་ཞབས་ཞུ་རྒྱུན་ལྡན་མིན་པས།ད་ལྟ་སྒྲུབ་བཞིན་པའི་མཉམ་སྤྱོད་སྒྲིག་ཆས་ཡོད་མེད་དང་།ཡང་ན་བསྐྱར་དུ་བཀོད་སྒྲིག་དང་མཉམ་སྤྱོད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="295"/>
        <source>Samba set user password</source>
        <translation>Sambaཡིས་མཛད་སྤྱོད་པའི་གསང་གྲངས་བཞག་ཡོད།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="293"/>
        <source>Samba password:</source>
        <translation>Sambaགསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="320"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Sambaཡིས་གསང་གྲངས་བཞག་ནས་ཕམ་སོང་།ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="421"/>
        <source>usershare</source>
        <translation>འཕྲུལ་ཆས་འདི་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="471"/>
        <source>share this folder</source>
        <translation>ཡིག་ཆ་འདི་མཉམ་སྤྱོད་བྱས།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="478"/>
        <source>don`t share this folder</source>
        <translation>ཡིག་ཆའི་སྒམ་འདི་མཉམ་སྤྱོད་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="504"/>
        <source>Share name:</source>
        <translation>མིང་མཉམ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="509"/>
        <source>Read Only</source>
        <translation>ཀློག་པ་ཙམ།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="512"/>
        <source>Allow Anonymous</source>
        <translation>ཡུལ་སྐོར་བར་བཅར་འདྲི་བྱེད་དུ་བཅུག</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="520"/>
        <source>Advanced Sharing</source>
        <translation>མཐོ་རིམ་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="505"/>
        <source>Comment:</source>
        <translation>མཆན་འགྲེལ།</translation>
    </message>
</context>
</TS>
