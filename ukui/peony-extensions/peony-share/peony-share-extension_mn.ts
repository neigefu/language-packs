<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AdvancedSharePage</name>
    <message>
        <location filename="../advanced-share-page.cpp" line="58"/>
        <source>Advanced share</source>
        <translation>ᠳᠡᠭᠡᠳᠦ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="109"/>
        <source>Samba set user password</source>
        <translation>Samba ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="107"/>
        <source>Samba password:</source>
        <translation>Sambaᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ：</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>Tips</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>The user has not set the samba password. If you need to log in to %1, you can set it in the upper right menu of the file manager</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ sambaᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠃ ᠴᠢ %1ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢ ᠳᠠᠩᠰᠠᠯᠠᠬᠤ ᠴᠢᠬᠤᠯᠠ᠎ᠲᠠᠢ ᠂ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠪᠠᠷᠠᠭᠤᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠲᠣᠪᠶᠣᠭ᠎ᠲᠤ᠌ sambaᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠨᠢ ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠪᠤᠰᠤ ᠂ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠶᠠᠪᠤᠭᠳᠠᠵᠤ ᠪᠠᠢᠬᠤ ᠡᠰᠡᠬᠦ᠎ᠶ᠋ᠢ ᠨᠤᠲᠠᠯᠠᠭᠠᠷᠠᠢ ᠂ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠳᠠᠬᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ !︕</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Writable</source>
        <translation>ᠪᠢᠴᠢᠵᠦ ᠪᠣᠯᠬᠤᠢ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Readonly</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Reject</source>
        <translation>ᠦᠯᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="365"/>
        <source>Share permission settings</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠡᠷᠬᠡ ᠮᠠᠲᠠᠯ᠎ᠤ᠋ᠨ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="377"/>
        <source>delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ᠎ᠶ᠋ᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="394"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠭᠳᠡᠭᠦᠯᠬᠦ ᠂ ᠨᠡᠮᠡᠭᠳᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="411"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="412"/>
        <source>Cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>NetUsershareHelper</name>
    <message>
        <source>Peony-Qt-Share-Extension</source>
        <translation type="vanished">共享</translation>
    </message>
</context>
<context>
    <name>Peony::SharePropertiesPagePlugin</name>
    <message>
        <location filename="../share-properties-page-plugin.h" line="44"/>
        <source>Peony Qt Share Extension</source>
        <translation>ᠬᠠᠮᠲᠤ᠎ᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../share-properties-page-plugin.h" line="45"/>
        <source>Allow user share folders</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠹᠠᠢᠯ ᠬᠠᠪᠴᠢᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>SharePage</name>
    <message>
        <location filename="../share-page.cpp" line="453"/>
        <source>Share folder</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠹᠠᠢᠯ ᠬᠠᠪᠴᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <source>Share:</source>
        <translation type="vanished">共享:</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="142"/>
        <source>The folder is currently shared in non-read-only mode, and setting up the share requires adding other people&apos;s write permissions to the current folder?</source>
        <translation>ᠲᠤᠰ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠣᠳᠣᠬᠢ ᠴᠤᠭᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠮᠣᠳᠧᠯ ᠨᠢ ᠵᠥᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ ᠪᠤᠰᠤ ᠮᠣᠳᠧᠯ ᠪᠠᠢᠵᠤ ᠂ ᠣᠳᠣᠬᠢ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠪᠤᠰᠤᠳ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠪᠢᠴᠢᠬᠦ ᠡᠷᠬᠡ᠎ᠶ᠋ᠢ ᠠᠦ᠋ᠲ᠋ᠣ ᠨᠡᠮᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ᠤᠤ ? ︖</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <location filename="../share-page.cpp" line="143"/>
        <location filename="../share-page.cpp" line="197"/>
        <source>question</source>
        <translation>ᠯᠠᠪᠯᠠᠨ ᠠᠰᠠᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="145"/>
        <location filename="../share-page.cpp" line="199"/>
        <source>Confirm adding permissions</source>
        <translation>ᠨᠡᠮᠡᠬᠦ ᠡᠷᠬᠡ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="146"/>
        <location filename="../share-page.cpp" line="200"/>
        <source>Cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <location filename="../share-page.cpp" line="113"/>
        <location filename="../share-page.cpp" line="174"/>
        <location filename="../share-page.cpp" line="183"/>
        <location filename="../share-page.cpp" line="229"/>
        <location filename="../share-page.cpp" line="239"/>
        <location filename="../share-page.cpp" line="283"/>
        <location filename="../share-page.cpp" line="320"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <source>The share name must not contain %1, and cannot start with a dash (-) or whitespace, or end with whitespace.</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠡᠷ᠎ᠡ ᠨᠢ %1᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠦᠭᠡᠢ ᠮᠥᠷᠲᠡᠭᠡᠨ —ᠡᠬᠢᠯᠡᠯᠲᠡ ᠪᠤᠶᠤ ᠬᠣᠭᠣᠰᠣᠨ ᠵᠠᠢ᠎ᠪᠠᠷ ᠡᠬᠢᠯᠡᠬᠦ ᠪᠤᠶᠤ ᠬᠣᠭᠣᠰᠣᠨ ᠵᠠᠢ ᠲᠡᠭᠦᠰᠬᠡᠯ᠎ᠢ᠋ ᠠᠭᠤᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="113"/>
        <source>The share name cannot be the same as the current user name</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠣᠳᠣᠬᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ᠎ᠲᠡᠢ ᠠᠳᠠᠯᠢ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <source>The share name is already used by other users. You can rename the folder and then set the share.</source>
        <translation>ᠲᠤᠰ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠡᠷ᠎ᠡ᠎ᠶ᠋ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠪᠠᠢᠪᠠᠯ ᠂ ᠲᠤᠰ ᠹᠠᠢᠯ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠢ᠋ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ᠎ᠦ᠌ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠨ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠪᠣᠯᠭᠠᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="196"/>
        <source>The folder is currently shared with anonymous access set, do I need to automatically increase the executable permissions of the current file and others in the ancestor directory?</source>
        <translation>ᠲᠤᠰ ᠹᠠᠢᠯ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠲᠤ᠌ ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠠᠭᠰᠠᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠨᠡᠢᠲᠡ ᠣᠳᠣ᠎ᠳ᠋ᠤ᠌ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠪᠠ ᠂ ᠣᠳᠣᠬᠢ ᠹᠠᠢᠯ ᠪᠠ ᠡᠪᠦᠭᠡ ᠳᠡᠭᠡᠳᠦᠰ᠎ᠦ᠋ᠨ ᠭᠠᠷᠴᠠᠭ᠎ᠤ᠋ᠨ ᠪᠤᠰᠤᠳ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠡᠷᠬᠡ᠎ᠶ᠋ᠢ ᠠᠦ᠋ᠲ᠋ᠣ ᠨᠡᠮᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ᠤᠤ ? ︖</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="283"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠨᠢ ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠪᠤᠰᠤ ᠂ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠶᠠᠪᠤᠭᠳᠠᠵᠤ ᠪᠠᠢᠬᠤ ᠡᠰᠡᠬᠦ᠎ᠶ᠋ᠢ ᠨᠤᠲᠠᠯᠠᠭᠠᠷᠠᠢ ᠂ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠳᠠᠬᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ !︕</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="295"/>
        <source>Samba set user password</source>
        <translation>Samba ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="293"/>
        <source>Samba password:</source>
        <translation>Sambaᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ：</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="320"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="421"/>
        <source>usershare</source>
        <translation>ᠲᠤᠰ ᠮᠠᠰᠢᠨ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="471"/>
        <source>share this folder</source>
        <translation>ᠲᠤᠰ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠢ᠋ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="478"/>
        <source>don`t share this folder</source>
        <translation>ᠲᠤᠰ ᠹᠠᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠪᠴᠢᠭᠤᠷ᠎ᠢ᠋ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="504"/>
        <source>Share name:</source>
        <translation>ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠡᠷ᠎ᠡ ᠄</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="509"/>
        <source>Read Only</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="512"/>
        <source>Allow Anonymous</source>
        <translation>ᠵᠤᠭᠠᠴᠠᠭᠰᠠᠳ᠎ᠤ᠋ᠨ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="520"/>
        <source>Advanced Sharing</source>
        <translation>ᠳᠡᠭᠡᠳᠦ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="505"/>
        <source>Comment:</source>
        <translation>ᠲᠠᠢᠯᠪᠤᠷᠢ ᠄</translation>
    </message>
</context>
</TS>
