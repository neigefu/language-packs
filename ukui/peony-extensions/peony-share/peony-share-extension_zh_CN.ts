<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AdvancedSharePage</name>
    <message>
        <location filename="../advanced-share-page.cpp" line="58"/>
        <source>Advanced share</source>
        <translation>高级共享</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="109"/>
        <source>Samba set user password</source>
        <translation>Samba设置用户密码</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="107"/>
        <source>Samba password:</source>
        <translation>Samba密码：</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="117"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba设置密码失败，请重新输入!</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="125"/>
        <source>The user has not set the samba password. If you need to log in to %1, you can set it in the upper right menu of the file manager</source>
        <translation>该用户没有设置samba密码。你需要登录%1用户，可以在文件管理器的右上菜单中进行设置samba密码</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="129"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>共享配置服务异常，请确认是否有正在进行的共享配置操作，或者请重新设置共享！</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Writable</source>
        <translation>可写</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Readonly</source>
        <translation>只读</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="270"/>
        <source>Reject</source>
        <translation>拒绝</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="365"/>
        <source>Share permission settings</source>
        <translation>共享权限设置</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="377"/>
        <source>delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="394"/>
        <source>Add</source>
        <translation>增加</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="411"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../advanced-share-page.cpp" line="412"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>NetUsershareHelper</name>
    <message>
        <source>Peony-Qt-Share-Extension</source>
        <translation type="vanished">共享</translation>
    </message>
</context>
<context>
    <name>Peony::SharePropertiesPagePlugin</name>
    <message>
        <location filename="../share-properties-page-plugin.h" line="44"/>
        <source>Peony Qt Share Extension</source>
        <translation>共享</translation>
    </message>
    <message>
        <location filename="../share-properties-page-plugin.h" line="45"/>
        <source>Allow user share folders</source>
        <translation>允许共享文件夹</translation>
    </message>
</context>
<context>
    <name>SharePage</name>
    <message>
        <location filename="../share-page.cpp" line="453"/>
        <source>Share folder</source>
        <translation>共享文件夹</translation>
    </message>
    <message>
        <source>Share:</source>
        <translation type="vanished">共享:</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="142"/>
        <source>The folder is currently shared in non-read-only mode, and setting up the share requires adding other people&apos;s write permissions to the current folder?</source>
        <translation>该文件夹当前共享模式为非只读模式，是否需要自动增加当前文件夹增加其他人的写权限？</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <location filename="../share-page.cpp" line="143"/>
        <location filename="../share-page.cpp" line="197"/>
        <source>question</source>
        <translation>询问</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="145"/>
        <location filename="../share-page.cpp" line="199"/>
        <source>Confirm adding permissions</source>
        <translation>确定增加权限</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="146"/>
        <location filename="../share-page.cpp" line="200"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <location filename="../share-page.cpp" line="113"/>
        <location filename="../share-page.cpp" line="174"/>
        <location filename="../share-page.cpp" line="183"/>
        <location filename="../share-page.cpp" line="229"/>
        <location filename="../share-page.cpp" line="239"/>
        <location filename="../share-page.cpp" line="283"/>
        <location filename="../share-page.cpp" line="320"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="108"/>
        <source>The share name must not contain %1, and cannot start with a dash (-) or whitespace, or end with whitespace.</source>
        <translation>共享名不能包含%1，并且不能以-开头或者空格开头，或者空格结尾</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="113"/>
        <source>The share name cannot be the same as the current user name</source>
        <translation>共享名不能和当前用户名相同</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="123"/>
        <source>The share name is already used by other users. You can rename the folder and then set the share.</source>
        <translation>该共享名已经被其他用户使用，可以重命名该文件夹后再设置共享</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="196"/>
        <source>The folder is currently shared with anonymous access set, do I need to automatically increase the executable permissions of the current file and others in the ancestor directory?</source>
        <translation>该文件夹当前共享设置了匿名访问，是否需要自动增加当前文件和祖宗目录的其他人的可执行权限？</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="283"/>
        <source>Shared configuration service exception, please confirm if there is an ongoing shared configuration operation, or please reset the share!</source>
        <translation>共享配置服务异常，请确认是否有正在进行的共享配置操作，或者请重新设置共享！</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="295"/>
        <source>Samba set user password</source>
        <translation>Samba设置用户密码</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="293"/>
        <source>Samba password:</source>
        <translation>Samba密码：</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="320"/>
        <source>Samba set password failed, Please re-enter!</source>
        <translation>Samba设置密码失败，请重新输入!</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="421"/>
        <source>usershare</source>
        <translation>本机共享</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="471"/>
        <source>share this folder</source>
        <translation>共享此文件夹</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="428"/>
        <location filename="../share-page.cpp" line="478"/>
        <source>don`t share this folder</source>
        <translation>不共享此文件夹</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="504"/>
        <source>Share name:</source>
        <translation>共享名：</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="509"/>
        <source>Read Only</source>
        <translation>只读</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="512"/>
        <source>Allow Anonymous</source>
        <translation>允许游客访问</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="520"/>
        <source>Advanced Sharing</source>
        <translation>高级共享</translation>
    </message>
    <message>
        <location filename="../share-page.cpp" line="505"/>
        <source>Comment:</source>
        <translation>注释:</translation>
    </message>
</context>
</TS>
