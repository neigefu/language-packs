<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::EngrampaMenuPlugin</name>
    <message>
        <source>compress...</source>
        <translation type="vanished">压缩...</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.cpp" line="66"/>
        <source>compress</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.cpp" line="82"/>
        <source>uncompress to current path</source>
        <translation>ᠡᠨᠳᠡ ᠬᠦᠷᠲᠡᠯ᠎ᠡ ᠠᠯᠭᠤᠷᠬᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.cpp" line="93"/>
        <source>uncompress to specific path...</source>
        <translation>ᠠᠭᠰᠢᠭᠠᠬᠤ ᠁</translation>
    </message>
    <message>
        <source>uncompress to specific path</source>
        <translation type="vanished">解压缩到...</translation>
    </message>
    <message>
        <source>Peony-Qt KArchive Menu Extension</source>
        <translation type="vanished">文件压缩插件</translation>
    </message>
    <message>
        <source>KArchive Menu Extension.</source>
        <translation type="vanished">压缩菜单扩展。</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.h" line="40"/>
        <source>Peony-Qt engrampa Extension</source>
        <translation>ᠬᠠᠪᠲᠠᠭᠠᠢ ᠲᠣᠨᠣᠭ ᠢ ᠪᠠᠭᠠᠰᠬᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.h" line="41"/>
        <source>engrampa Menu Extension</source>
        <translation>ᠳᠠᠩᠰᠠ ᠡᠪᠬᠡᠮᠡᠯ ᠳᠦ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠨᠣᠭᠣᠭ᠎ᠠ ᠶᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠥᠷᠭᠡᠳᠪᠡ ᠃</translation>
    </message>
    <message>
        <source>engrampa Menu Extension.</source>
        <translation type="vanished">归档菜单扩展。</translation>
    </message>
</context>
</TS>
