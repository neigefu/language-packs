<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::EngrampaMenuPlugin</name>
    <message>
        <source>compress...</source>
        <translation type="vanished">压缩...</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.cpp" line="66"/>
        <source>compress</source>
        <translation>འཕྲི་འཐེན།</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.cpp" line="82"/>
        <source>uncompress to current path</source>
        <translation>འདིར་ཇེ་ཉུང་དུ་བཏང་།</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.cpp" line="93"/>
        <source>uncompress to specific path...</source>
        <translation>ཇེ་ཉུང་དུ་བཏང་།</translation>
    </message>
    <message>
        <source>uncompress to specific path</source>
        <translation type="vanished">解压缩到...</translation>
    </message>
    <message>
        <source>Peony-Qt KArchive Menu Extension</source>
        <translation type="vanished">文件压缩插件</translation>
    </message>
    <message>
        <source>KArchive Menu Extension.</source>
        <translation type="vanished">压缩菜单扩展。</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.h" line="40"/>
        <source>Peony-Qt engrampa Extension</source>
        <translation>བསྒར་ལྷུ་ཐུང་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../engrampa-menu-plugin.h" line="41"/>
        <source>engrampa Menu Extension</source>
        <translation>ཚལ་ཐོ་རྒྱ་བསྐྱེད།</translation>
    </message>
    <message>
        <source>engrampa Menu Extension.</source>
        <translation type="vanished">归档菜单扩展。</translation>
    </message>
</context>
</TS>
