<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Peony::BluetoothPlugin</name>
    <message>
        <location filename="../bluetoothplugin.cpp" line="99"/>
        <source>Send from bluetooth to...</source>
        <translation>從藍牙傳送檔案到...</translation>
    </message>
    <message>
        <location filename="../bluetoothplugin.h" line="53"/>
        <source>Peony-Qt bluetooth Extension</source>
        <translation>藍牙外掛程式</translation>
    </message>
    <message>
        <location filename="../bluetoothplugin.h" line="54"/>
        <source>bluetooth Menu Extension</source>
        <translation>藍牙功能表擴展</translation>
    </message>
    <message>
        <source>bluetooth Menu Extension.</source>
        <translation type="vanished">蓝牙菜单扩展.</translation>
    </message>
</context>
</TS>
