<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::BluetoothPlugin</name>
    <message>
        <location filename="../bluetoothplugin.cpp" line="99"/>
        <source>Send from bluetooth to...</source>
        <translation>ལན་ཡ་ནས་ཡིག་ཆ་སྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../bluetoothplugin.h" line="53"/>
        <source>Peony-Qt bluetooth Extension</source>
        <translation>སོ་སྔོན་འཛེར་ཆས།</translation>
    </message>
    <message>
        <location filename="../bluetoothplugin.h" line="54"/>
        <source>bluetooth Menu Extension</source>
        <translation>སོ་སྔོན་ཚལ་ཐོ་རྒྱ་བསྐྱེད།</translation>
    </message>
    <message>
        <source>bluetooth Menu Extension.</source>
        <translation type="vanished">蓝牙菜单扩展.</translation>
    </message>
</context>
</TS>
