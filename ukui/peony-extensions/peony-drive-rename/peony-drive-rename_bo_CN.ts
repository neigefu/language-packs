<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::DriveRename</name>
    <message>
        <source>drive rename</source>
        <translation type="vanished">设备重命名</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="166"/>
        <location filename="../drive-rename.cpp" line="178"/>
        <source>Rename</source>
        <translation>མིང་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="178"/>
        <source>Device name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="182"/>
        <location filename="../drive-rename.cpp" line="190"/>
        <location filename="../drive-rename.cpp" line="196"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="182"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>མིང་བསྐྱར་འདོགས་བྱེད་དུས་ཡིག་རྟགས་དེ་ཚེག་ཆུང་ཡིན་པར་རྒྱབ་སྐྱོར་མི་བྱེད་པས་ཡང་བསྐྱར་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="190"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་དེ་ཡིག་རྟགས་ཀྱི་ཚད་བཀག་ལས་བརྒལ་ཏེ་བསྐྱར་དུ་མིང་བཏགས་ནས་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="196"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>བསྐྱར་མིང་བཏགས་བྱས་ན་སྒྲིག་ཆས་བཤིག་འདོན་བྱེད་སྲིད།མུ་མཐུད་དུ་བྱེད་དམ།</translation>
    </message>
    <message>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation type="vanished">可能设备不支持重命名操作，重命名失败！</translation>
    </message>
    <message>
        <source>Failed to rename!</source>
        <translation type="vanished">重命名失败！</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRenamePlugin</name>
    <message>
        <location filename="../driverenameplugin.h" line="21"/>
        <source>drive rename</source>
        <translation>སྒྲིག་ཆས་ལ་མིང་བཏགས་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::DriverAction</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
<context>
    <name>Peony::SendToPlugin</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../drive-rename.cpp" line="328"/>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="328"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>སྒྲིག་ཆས་ཀྱིས་བསྐྱར་དུ་མིང་བཏགས་ནས་བཀོལ་སྤྱོད་བྱེད་པར་རྒྱབ་སྐྱོར་མི་བྱེད་ཀྱང་སྲིད།བསྐྱར་དུ་མིང་བཏགས་ནས་ཕམ་སོང་།</translation>
    </message>
</context>
</TS>
