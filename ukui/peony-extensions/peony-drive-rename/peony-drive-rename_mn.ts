<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::DriveRename</name>
    <message>
        <source>drive rename</source>
        <translation type="vanished">设备重命名</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="166"/>
        <location filename="../drive-rename.cpp" line="178"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="178"/>
        <source>Device name:</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠄</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="182"/>
        <location filename="../drive-rename.cpp" line="190"/>
        <location filename="../drive-rename.cpp" line="196"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="182"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ ᠦᠶᠡᠰ ᠲᠦᠷᠦᠭᠦᠦ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ ᠨᠢ ᠪᠤᠲᠠᠷᠬᠠᠢ ᠲᠣᠭᠠᠨ᠎ᠤ᠋ ᠴᠡᠭ ᠪᠣᠯᠬᠤ᠎ᠶ᠋ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ ᠂ ᠳᠠᠬᠢᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="190"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ᠎ᠠ᠋ᠴᠠ ᠬᠡᠲᠦᠷᠡᠵᠦ ᠂ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ !</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="196"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠨᠢ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠂ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠬᠦ ᠦᠦ ? ︖</translation>
    </message>
    <message>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation type="vanished">可能设备不支持重命名操作，重命名失败！</translation>
    </message>
    <message>
        <source>Failed to rename!</source>
        <translation type="vanished">重命名失败！</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRenamePlugin</name>
    <message>
        <location filename="../driverenameplugin.h" line="21"/>
        <source>drive rename</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::DriverAction</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
<context>
    <name>Peony::SendToPlugin</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../drive-rename.cpp" line="328"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="328"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠯᠡᠬᠦ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ ᠮᠠᠭᠠᠳ ᠂ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ !︕</translation>
    </message>
</context>
</TS>
