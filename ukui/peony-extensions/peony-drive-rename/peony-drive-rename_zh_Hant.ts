<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Peony::DriveRename</name>
    <message>
        <source>drive rename</source>
        <translation type="vanished">设备重命名</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="166"/>
        <location filename="../drive-rename.cpp" line="178"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="178"/>
        <source>Device name:</source>
        <translation>裝置名稱：</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="182"/>
        <location filename="../drive-rename.cpp" line="190"/>
        <location filename="../drive-rename.cpp" line="196"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="182"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>重命名時不支援首字元為小數點，請重新輸入！</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="190"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>設備名超過字元限制，重命名失敗！</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="196"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>重命名將會卸載設備，是否繼續？</translation>
    </message>
    <message>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation type="vanished">可能设备不支持重命名操作，重命名失败！</translation>
    </message>
    <message>
        <source>Failed to rename!</source>
        <translation type="vanished">重命名失败！</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRenamePlugin</name>
    <message>
        <location filename="../driverenameplugin.h" line="21"/>
        <source>drive rename</source>
        <translation>設備重新命名</translation>
    </message>
</context>
<context>
    <name>Peony::DriverAction</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
<context>
    <name>Peony::SendToPlugin</name>
    <message>
        <source>Send to a removable device</source>
        <translation type="vanished">发送到移动设备</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../drive-rename.cpp" line="328"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="328"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>可能設備不支援重命名操作，重命名失敗！</translation>
    </message>
</context>
</TS>
