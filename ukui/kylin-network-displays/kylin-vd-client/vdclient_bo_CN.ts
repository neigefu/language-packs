<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../mainwindow.cpp" line="110"/>
        <location filename="../mainwindow.cpp" line="166"/>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>主机桌面</source>
        <translation>འཕྲུལ་ཆས་གཙོ་བོའི་ཅོག་ངོས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="111"/>
        <source>断开</source>
        <translation>བར་མཚམས་ཆད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="151"/>
        <source>网络显示器已连接</source>
        <translation>དྲ་རྒྱའི་བརྡ་སྟོན་ཡོ་བྱད་འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="169"/>
        <source>投屏桌面</source>
        <translation>ཅོག་ཙེའི་ངོས་སུ་འཕངས་པ།</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../controller.cpp" line="35"/>
        <source>&quot;%1&quot; 请求连接显示器</source>
        <translation>&quot;1%&quot;རེ་ཞུའི་ཐོག་ནས་བརྡ་སྟོན་ཡོ་བྱད་སྦྲེལ་རྒྱུའི་རེ་བ་བཏོན་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../controller.cpp" line="36"/>
        <source>连接请求</source>
        <translation>འབྲེལ་མཐུད་ཀྱི་རེ་འདུན་ཞུ་བ།</translation>
    </message>
</context>
<context>
    <name>Notifier</name>
    <message>
        <location filename="../notifier.cpp" line="28"/>
        <source>网络显示器</source>
        <translation>དྲ་རྒྱའི་བརྡ་སྟོན་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>不再提示</source>
        <translation>ཡང་བསྐྱར་གསལ་འདེབས་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>拒绝</source>
        <translation>དང་ལེན་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>连接</source>
        <translation>འབྲེལ་མཐུད་བཅས་བྱ་དགོས།</translation>
    </message>
</context>
</TS>
