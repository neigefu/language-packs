<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../mainwindow.cpp" line="110"/>
        <location filename="../mainwindow.cpp" line="166"/>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>主机桌面</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠤ ᠰᠢᠷᠡᠭᠡᠨ ᠦ ᠭᠠᠳᠠᠷᠭᠤ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="111"/>
        <source>断开</source>
        <translation>ᠲᠠᠰᠤᠷᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="151"/>
        <source>网络显示器已连接</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠵᠠᠢ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="169"/>
        <source>投屏桌面</source>
        <translation>ᠳᠡᠯᠪᠡᠷᠡᠬᠦ ᠰᠢᠷᠡᠭᠡᠨ ᠦ ᠭᠠᠳᠠᠷᠭᠤ ᠳᠤ ᠣᠷᠣᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../controller.cpp" line="35"/>
        <source>&quot;%1&quot; 请求连接显示器</source>
        <translation>&quot;%1&quot; ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢ ᠬᠣᠯᠪᠣᠬᠤ ᠪᠠᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ</translation>
    </message>
    <message>
        <location filename="../controller.cpp" line="36"/>
        <source>连接请求</source>
        <translation>ᠵᠠᠯᠭᠠᠯᠳᠤᠭᠤᠯᠬᠤ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ</translation>
    </message>
</context>
<context>
    <name>Notifier</name>
    <message>
        <location filename="../notifier.cpp" line="28"/>
        <source>网络显示器</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>不再提示</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠦᠭᠡᠶ</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>拒绝</source>
        <translation>ᠲᠡᠪᠴᠢᠬᠦ ᠡᠴᠡ ᠲᠠᠲᠠᠭᠠᠯᠵᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>连接</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
