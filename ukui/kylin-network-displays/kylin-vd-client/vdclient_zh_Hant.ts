<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>ControlBar</name>
    <message>
        <location filename="../mainwindow.cpp" line="110"/>
        <location filename="../mainwindow.cpp" line="166"/>
        <location filename="../mainwindow.cpp" line="191"/>
        <source>主机桌面</source>
        <translation>主機桌面</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="111"/>
        <source>断开</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="151"/>
        <source>网络显示器已连接</source>
        <translation>網路顯示器已連接</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="169"/>
        <source>投屏桌面</source>
        <translation>投屏桌面</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../controller.cpp" line="35"/>
        <source>&quot;%1&quot; 请求连接显示器</source>
        <translation>“%1” 請求連接顯示器</translation>
    </message>
    <message>
        <location filename="../controller.cpp" line="36"/>
        <source>连接请求</source>
        <translation>連接請求</translation>
    </message>
</context>
<context>
    <name>Notifier</name>
    <message>
        <location filename="../notifier.cpp" line="28"/>
        <source>网络显示器</source>
        <translation>網路顯示器</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>不再提示</source>
        <translation>不再提示</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>拒绝</source>
        <translation>拒絕</translation>
    </message>
    <message>
        <location filename="../notifier.cpp" line="37"/>
        <source>连接</source>
        <translation>連接</translation>
    </message>
</context>
</TS>
