<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Mainwindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="41"/>
        <source>网络显示器</source>
        <translation>網路顯示器</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="96"/>
        <source>当前显卡不支持网络显示器</source>
        <translation>當前顯卡不支持網路顯示器</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="106"/>
        <source>显示器设置</source>
        <translation>顯示器設置</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <source>正在查找网络显示器......</source>
        <translation>正在查找網路顯示器......</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="301"/>
        <source>取消</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="332"/>
        <source>正在连接 &quot;%1&quot;</source>
        <translation>正在連接%1</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>确定</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>&quot;%1&quot; 已被他设备连接，请断开后重试。</source>
        <translation>“%1&apos; 已被他設備連接，請斷開後重試。</translation>
    </message>
</context>
<context>
    <name>NetWorkDisplayItem</name>
    <message>
        <location filename="../networkdisplayitem.cpp" line="15"/>
        <source>已连接</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>断开</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>连接</source>
        <translation>連接</translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <location filename="../main.cpp" line="16"/>
        <source>网络显示器</source>
        <translation>網路顯示器</translation>
    </message>
</context>
</TS>
