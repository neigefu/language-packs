<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Mainwindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="41"/>
        <source>网络显示器</source>
        <translation>网络显示器</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="96"/>
        <source>当前显卡不支持网络显示器</source>
        <translation>当前显卡不支持网络显示器</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="106"/>
        <source>显示器设置</source>
        <translation>显示器设置</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <source>正在查找网络显示器......</source>
        <translation>正在查找网络显示器......</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="301"/>
        <source>取消</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="332"/>
        <source>正在连接 &quot;%1&quot;</source>
        <translation>正在连接 &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>确定</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>&quot;%1&quot; 已被他设备连接，请断开后重试。</source>
        <translation>&quot;%1&quot; 已被他设备连接，请断开后重试。</translation>
    </message>
</context>
<context>
    <name>NetWorkDisplayItem</name>
    <message>
        <location filename="../networkdisplayitem.cpp" line="15"/>
        <source>已连接</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>断开</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>连接</source>
        <translation>连接</translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <location filename="../main.cpp" line="16"/>
        <source>网络显示器</source>
        <translation>网络显示器</translation>
    </message>
</context>
</TS>
