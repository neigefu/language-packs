<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Mainwindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="41"/>
        <source>网络显示器</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="96"/>
        <source>当前显卡不支持网络显示器</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠺᠠᠷᠲ᠋ ᠨᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="106"/>
        <source>显示器设置</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <source>正在查找网络显示器......</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢ ᠡᠷᠢᠵᠦ ᠁</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="301"/>
        <source>取消</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="332"/>
        <source>正在连接 &quot;%1&quot;</source>
        <translation>ᠶᠠᠭ ᠢ ᠬᠤᠯᠪᠤᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>确定</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>&quot;%1&quot; 已被他设备连接，请断开后重试。</source>
        <translation>&quot;%1&quot; ᠲᠡᠭᠦᠨ ᠦ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠬᠣᠯᠪᠣᠭᠳᠠᠵᠠᠢ ᠂ ᠲᠠᠰᠤᠷᠠᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠨ ᠲᠤᠷᠰᠢᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>NetWorkDisplayItem</name>
    <message>
        <location filename="../networkdisplayitem.cpp" line="15"/>
        <source>已连接</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠵᠠᠯᠭᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>断开</source>
        <translation>ᠲᠠᠰᠤᠷᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>连接</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <location filename="../main.cpp" line="16"/>
        <source>网络显示器</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠃</translation>
    </message>
</context>
</TS>
