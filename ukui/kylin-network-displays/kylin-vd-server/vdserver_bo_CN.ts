<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Mainwindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="41"/>
        <source>网络显示器</source>
        <translation>དྲ་རྒྱའི་བརྡ་སྟོན་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="96"/>
        <source>当前显卡不支持网络显示器</source>
        <translation>མིག་སྔར་ཁཱ་བྱང་གིས་དྲ་རྒྱའི་བརྡ་སྟོན་ཡོ་བྱད་ལ་རྒྱབ་སྐྱོར་མི་</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="106"/>
        <source>显示器设置</source>
        <translation>བརྡ་སྟོན་ཡོ་བྱད་སྒྲིག་སྦྱོར།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="118"/>
        <source>正在查找网络显示器......</source>
        <translation>དྲ་རྒྱའི་བརྡ་སྟོན་ཡོ་བྱད་འཚོལ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="301"/>
        <source>取消</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="332"/>
        <source>正在连接 &quot;%1&quot;</source>
        <translation>&quot;1%&quot;དང་འབྲེལ་མཐུད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="360"/>
        <source>确定</source>
        <translation>གཏན་འཁེལ་བྱ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="363"/>
        <source>&quot;%1&quot; 已被他设备连接，请断开后重试。</source>
        <translation>&quot;%1&quot; ནི་ཁོང་གི་སྒྲིག་ཆས་དང་སྦྲེལ་ཡོད་པས། མཚམས་ཆད་རྗེས་བསྐྱར་དུ་ཚོད་ལྟ་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>NetWorkDisplayItem</name>
    <message>
        <location filename="../networkdisplayitem.cpp" line="15"/>
        <source>已连接</source>
        <translation>འབྲེལ་མཐུད་བྱུང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>断开</source>
        <translation>བར་མཚམས་ཆད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../networkdisplayitem.cpp" line="35"/>
        <location filename="../networkdisplayitem.cpp" line="106"/>
        <source>连接</source>
        <translation>འབྲེལ་མཐུད་བཅས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Server</name>
    <message>
        <location filename="../main.cpp" line="16"/>
        <source>网络显示器</source>
        <translation>དྲ་རྒྱའི་བརྡ་སྟོན་ཡོ་བྱད།</translation>
    </message>
</context>
</TS>
