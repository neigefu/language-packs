<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>TouchScreen</name>
    <message>
        <location filename="../touchscreen.ui" line="14"/>
        <source>TouchScreen</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="62"/>
        <location filename="../touchscreen.cpp" line="100"/>
        <source>monitor</source>
        <translation>ᠦᠵᠡᠬᠦᠷ</translation>
        <extra-contents_path>/TouchCalibrate/monitor</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="127"/>
        <location filename="../touchscreen.cpp" line="103"/>
        <source>touch id</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠵᠢᠨ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠭᠴᠢ</translation>
        <extra-contents_path>/TouchCalibrate/touch id</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="195"/>
        <location filename="../touchscreen.cpp" line="106"/>
        <source>input device</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
        <extra-contents_path>/TouchCalibrate/input device</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="202"/>
        <source>TextLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="229"/>
        <source>map</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠳᠤᠰᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="251"/>
        <source>calibration</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠤᠨᠤᠪᠴᠢᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="21"/>
        <source>TouchCalibrate</source>
        <translatorcomment>触摸校准</translatorcomment>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠤᠨᠤᠪᠴᠢᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="454"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
</TS>
