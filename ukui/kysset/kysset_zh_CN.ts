<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>TouchScreen</name>
    <message>
        <location filename="../touchscreen.ui" line="14"/>
        <source>TouchScreen</source>
        <translation>触摸屏</translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="62"/>
        <location filename="../touchscreen.cpp" line="100"/>
        <source>monitor</source>
        <translation>显示器</translation>
        <extra-contents_path>/TouchCalibrate/monitor</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="127"/>
        <location filename="../touchscreen.cpp" line="103"/>
        <source>touch id</source>
        <translation>触摸屏标识</translation>
        <extra-contents_path>/TouchCalibrate/touch id</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="195"/>
        <location filename="../touchscreen.cpp" line="106"/>
        <source>input device</source>
        <translation>触摸设备</translation>
        <extra-contents_path>/TouchCalibrate/input device</extra-contents_path>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="202"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="229"/>
        <source>map</source>
        <translation>触摸映射</translation>
    </message>
    <message>
        <location filename="../touchscreen.ui" line="251"/>
        <source>calibration</source>
        <translation>触摸校准</translation>
    </message>
    <message>
        <location filename="../touchscreen.cpp" line="21"/>
        <source>TouchCalibrate</source>
        <translatorcomment>触摸校准</translatorcomment>
        <translation>触摸校准</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="454"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
</context>
</TS>
