<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>PowerTray</name>
    <message>
        <location filename="../powertray.cpp" line="90"/>
        <source>SetPower</source>
        <translation>設置電源和睡眠</translation>
    </message>
    <message>
        <location filename="../powertray.cpp" line="121"/>
        <location filename="../powertray.cpp" line="139"/>
        <location filename="../powertray.cpp" line="149"/>
        <source>fully charged (100%)</source>
        <translation>已充滿 （100%）</translation>
    </message>
    <message>
        <location filename="../powertray.cpp" line="123"/>
        <location filename="../powertray.cpp" line="151"/>
        <location filename="../powertray.cpp" line="160"/>
        <source>%1% available (plugged in)</source>
        <translation>% 1% 可用 （已接通電源）</translation>
    </message>
    <message>
        <location filename="../powertray.cpp" line="126"/>
        <location filename="../powertray.cpp" line="141"/>
        <location filename="../powertray.cpp" line="173"/>
        <location filename="../powertray.cpp" line="181"/>
        <location filename="../powertray.cpp" line="192"/>
        <source>%1% remaining</source>
        <translation>%1% 可用</translation>
    </message>
    <message>
        <location filename="../powertray.cpp" line="156"/>
        <source>%1 min to fully charge (%2%)</source>
        <translation>剩餘 %1 分鐘充滿 （% 2%）</translation>
    </message>
    <message>
        <location filename="../powertray.cpp" line="162"/>
        <source>%1 hr %2 min to fully charge (%3%)</source>
        <translation>剩餘 %1 小時 %2 分鐘充滿 （%3%）</translation>
    </message>
    <message>
        <location filename="../powertray.cpp" line="177"/>
        <source>%1 min (%2%) remaining</source>
        <translation>剩餘 %1 分鐘 （% 2%）</translation>
    </message>
    <message>
        <location filename="../powertray.cpp" line="183"/>
        <source>%1 hr %2 min (%3%) remaining</source>
        <translation>剩餘 %1 小時 %2 分鐘 （% 3%）</translation>
    </message>
</context>
<context>
    <name>powerwindow</name>
    <message>
        <location filename="../powerwindow.cpp" line="286"/>
        <location filename="../powerwindow.cpp" line="313"/>
        <location filename="../powerwindow.cpp" line="321"/>
        <source>Charging</source>
        <translation>正在充電</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="289"/>
        <location filename="../powerwindow.cpp" line="350"/>
        <location filename="../powerwindow.cpp" line="357"/>
        <source>Discharging</source>
        <translation>正在放電</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="284"/>
        <location filename="../powerwindow.cpp" line="304"/>
        <location filename="../powerwindow.cpp" line="311"/>
        <source>fully charged</source>
        <translation>已充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="135"/>
        <source>Endurance</source>
        <translation>最佳能效</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="143"/>
        <source>Performance</source>
        <translation>最佳性能</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="148"/>
        <source>PowerSettings</source>
        <translation>電源設置</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="151"/>
        <location filename="../powerwindow.cpp" line="245"/>
        <source>PowerMode</source>
        <translation>電源模式</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="247"/>
        <source>BatteryMode</source>
        <translation>電池模式</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="319"/>
        <source>%1 minutes 
until fully charged</source>
        <translation>%1 分鐘後
電池將充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="325"/>
        <source>%1 hour %2 minute 
until fully charged</source>
        <translation>%1 小時 %2 分鐘後
電池將充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="328"/>
        <source>%1 hour 
until fully charged</source>
        <translation>%1 小時後
電池將充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="330"/>
        <source>%1 hour %2 minutes 
until fully charged</source>
        <translation>%1 小時 %2 分鐘後
電池將充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="335"/>
        <source>%1 hours %2 minute 
until fully charged</source>
        <translation>%1 小時 %2 分鐘後
電池將充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="338"/>
        <source>%1 hours 
until fully charged</source>
        <translation>%1 小時後
電池將充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="340"/>
        <source>%1 hours %2 minutes 
until fully charged</source>
        <translation>%1 小時 %2 分鐘後
電池將充滿</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="355"/>
        <source>%1 minutes 
remaining</source>
        <translation>剩餘
%1 分鐘</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="361"/>
        <source>%1 hour %2 minute 
remaining</source>
        <translation>剩餘
%1 小時 %2 分鐘</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="364"/>
        <source>%1 hour 
remaining</source>
        <translation>剩餘
%1 小時</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="367"/>
        <source>%1 hour %2 minutes 
remaining</source>
        <translation>剩餘
%1 小時 %2 分鐘</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="372"/>
        <source>%1 hours %2 minute 
remaining</source>
        <translation>剩餘
%1 小時 %2 分鐘</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="375"/>
        <source>%1 hours 
remaining</source>
        <translation>剩餘
%1 小時</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="378"/>
        <source>%1 hours %2 minutes 
remaining</source>
        <translation>剩餘
%1 小時 %2 分鐘</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="465"/>
        <location filename="../powerwindow.cpp" line="482"/>
        <location filename="../powerwindow.cpp" line="519"/>
        <source>Better endurance</source>
        <translation>更好續航</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="469"/>
        <location filename="../powerwindow.cpp" line="486"/>
        <location filename="../powerwindow.cpp" line="515"/>
        <source>Better performance</source>
        <translation>更好性能</translation>
    </message>
    <message>
        <location filename="../powerwindow.cpp" line="473"/>
        <location filename="../powerwindow.cpp" line="490"/>
        <location filename="../powerwindow.cpp" line="511"/>
        <source>Best performance</source>
        <translation>最佳性能</translation>
    </message>
</context>
</TS>
