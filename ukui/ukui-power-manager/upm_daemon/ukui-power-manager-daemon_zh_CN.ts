<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>LowPowerWatcher</name>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="138"/>
        <source>Low battery notification</source>
        <translation>低电量消息通知</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="139"/>
        <source>The system enters a low battery state</source>
        <translation>系统进入低电量状态</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="147"/>
        <source>Very low battery notification</source>
        <translation>极低电量消息通知</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="152"/>
        <source>Current power is：%1%. The system will turn off the display in one minute</source>
        <translation>当前电量为：%1%，系统将于一分钟后关闭显示器</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="156"/>
        <source>Current power is：%1%，The system will suspend in one minute</source>
        <translation>当前电量为：%1%，系统将于一分钟后睡眠</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="159"/>
        <source>Current power is：%1%，The system will shutdown in one minute</source>
        <translation>当前电量为：%1%，系统将于一分钟后关机</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="162"/>
        <source>Current power is：%1%，The system will hibernate in one minute</source>
        <translation>当前电量为：%1%，系统将于一分钟后休眠</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="209"/>
        <source>Power Manager</source>
        <translation>电源管理器</translation>
    </message>
</context>
<context>
    <name>PowerMsgNotificat</name>
    <message>
        <source>error message</source>
        <translation type="vanished">错误消息</translation>
    </message>
    <message>
        <source>charge notification</source>
        <translation type="vanished">充电通知</translation>
    </message>
    <message>
        <source>battery is charging</source>
        <translation type="vanished">电池正在充电</translation>
    </message>
    <message>
        <source>discharged notification</source>
        <translation type="vanished">放电通知</translation>
    </message>
    <message>
        <source>battery is discharging</source>
        <translation type="vanished">电池正在放电中</translation>
    </message>
    <message>
        <source>full charge notification</source>
        <translation type="vanished">充满电通知</translation>
    </message>
    <message>
        <source>battery is full</source>
        <translation type="vanished">电池现在已经充满</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="50"/>
        <source>Charge notification</source>
        <translation>充电通知</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="51"/>
        <source>Battery is charging</source>
        <translation>电池正在充电</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="55"/>
        <source>Discharged notification</source>
        <translation>放电通知</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="56"/>
        <source>Battery is discharging</source>
        <translation>电池正在放电中</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="67"/>
        <source>Full charge notification</source>
        <translation>充满电通知</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="68"/>
        <source>Battery is full</source>
        <translation>电池现在已经充满</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="103"/>
        <source>Power Manager</source>
        <translation>电源管理器</translation>
    </message>
</context>
</TS>
