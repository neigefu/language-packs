<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>LowPowerWatcher</name>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="138"/>
        <source>Low battery notification</source>
        <translation>ᠳᠣᠤᠷ᠎ᠠ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠶᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="139"/>
        <source>The system enters a low battery state</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠣᠣᠷ᠎ᠠ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠶᠢᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠳᠤ ᠣᠷᠣᠪᠠ᠃</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="147"/>
        <source>Very low battery notification</source>
        <translation>ᠲᠤᠶᠢᠯ ᠤᠨ ᠳᠣᠣᠷ᠎ᠠ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠮᠡᠳᠡᠭᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="152"/>
        <source>Current power is：%1%. The system will turn off the display in one minute</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ ᠄ 1% ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠨᠢᠭᠡ ᠮᠢᠨᠦᠢᠲ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢ ᠬᠠᠭᠠᠬᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="156"/>
        <source>Current power is：%1%，The system will suspend in one minute</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ ᠄ 1% ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠨᠢᠭᠡ ᠮᠢᠨᠦᠢᠲ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠤᠨᠲᠠᠬᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="159"/>
        <source>Current power is：%1%，The system will shutdown in one minute</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ ᠄ 1% ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠨᠢᠭᠡ ᠮᠢᠨᠦᠢᠲ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠠᠭᠠᠭᠳᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="162"/>
        <source>Current power is：%1%，The system will hibernate in one minute</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ ᠄ 1% ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠨᠢᠭᠡ ᠮᠢᠨᠦᠢᠲ᠋ ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠤᠨᠲᠠᠬᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../lowpowerwatcher/lowpowerwatcher.cpp" line="209"/>
        <source>Power Manager</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ ᠶᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ ᠶᠢᠨ ᠪᠠᠭᠠᠵᠢ᠃</translation>
    </message>
</context>
<context>
    <name>PowerMsgNotificat</name>
    <message>
        <source>error message</source>
        <translation type="vanished">错误消息</translation>
    </message>
    <message>
        <source>charge notification</source>
        <translation type="vanished">充电通知</translation>
    </message>
    <message>
        <source>battery is charging</source>
        <translation type="vanished">电池正在充电</translation>
    </message>
    <message>
        <source>discharged notification</source>
        <translation type="vanished">放电通知</translation>
    </message>
    <message>
        <source>battery is discharging</source>
        <translation type="vanished">电池正在放电中</translation>
    </message>
    <message>
        <source>full charge notification</source>
        <translation type="vanished">充满电通知</translation>
    </message>
    <message>
        <source>battery is full</source>
        <translation type="vanished">电池现在已经充满</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="50"/>
        <source>Charge notification</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠨᠡᠮᠡᠬᠦ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="51"/>
        <source>Battery is charging</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᠢ ᠣᠳᠣ ᠶᠠᠭ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠨᠡᠮᠡᠵᠦ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="55"/>
        <source>Discharged notification</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠭᠠᠷᠭᠠᠬᠤ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="56"/>
        <source>Battery is discharging</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᠢ ᠣᠳᠣ ᠶᠠᠭ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠭᠠᠷᠭᠠᠵᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="67"/>
        <source>Full charge notification</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠨᠡᠮᠡᠬᠦ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠪᠢᠯᠬᠠᠮ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="68"/>
        <source>Battery is full</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᠢ ᠣᠳᠣ ᠨᠢᠭᠡᠨᠲᠡ ᠳᠦᠭᠦᠷᠴᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../powermsgnotificat/powermsgnotificat.cpp" line="103"/>
        <source>Power Manager</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ ᠶᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ ᠶᠢᠨ ᠪᠠᠭᠠᠵᠢ᠃</translation>
    </message>
</context>
</TS>
