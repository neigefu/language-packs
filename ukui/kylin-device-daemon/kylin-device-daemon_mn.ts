<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BaseDialog</name>
    <message>
        <source>Disk test</source>
        <translation>ᠲᠦᠬᠦᠭᠡᠷᠦᠮᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DeviceOperation</name>
    <message>
        <source>unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>FDClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation>ᠪᠠᠭᠳᠠᠭᠠᠮᠵᠢ ᠨᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <source>other user device</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠴᠢᠳ ᠦ᠋ᠨ ᠲᠦᠭᠦᠭᠡᠷᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Eject</source>
        <translation>ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>FDFrame</name>
    <message>
        <source>eject</source>
        <translation>ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>FormateDialog</name>
    <message>
        <source>Formatted successfully!</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠬᠦᠯᠪᠡ!</translation>
    </message>
    <message>
        <source>Formatting failed, please unplug the U disk and try again!</source>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠭᠤᠯᠵᠤ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Rom size:</source>
        <translation>ᠪᠠᠭᠳᠠᠭᠠᠮᠵᠢ:</translation>
    </message>
    <message>
        <source>Filesystem:</source>
        <translation>ᠹᠠᠢᠯ ᠦ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ:</translation>
    </message>
    <message>
        <source>Disk name:</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ:</translation>
    </message>
    <message>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation>ᠪᠦᠷᠢᠨ ᠠᠷᠢᠯᠭᠠᠪᠠ ( ᠤᠷᠳᠤ ᠴᠠᠭ ᠪᠠᠷᠠᠪᠠ᠂ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ!)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation>ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please back up all retained data before formatting. Do you want to continue?</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠬᠦᠯᠦᠯᠳᠡ ᠨᠢ ᠡᠪᠬᠡᠮᠡᠯ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠪᠦᠬᠦ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠠᠷᠢᠯᠭᠠᠬᠤ ᠪᠤᠯᠬᠤᠷ ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠬᠦᠯᠬᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠠᠷᠠᠢ᠂ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠬᠦᠯᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk format</source>
        <translation>ᠲᠦᠭᠦᠭᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>usb management tool</source>
        <translation>U ᠲᠠᠪᠠᠭ ᠢ᠋ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <source>ukui-flash-disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kindly reminder</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <source>wrong reminder</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Please do not pull out the USB flash disk when reading or writing</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠢᠴᠢᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠪᠢᠳᠡᠬᠡᠢ ᠰᠢᠭ᠋ᠤᠳ ᠰᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Please do not pull out the CDROM when reading or writing</source>
        <translation>ᠳ᠋ᠢᠰᠺ ᠢ᠋ ᠤᠩᠰᠢᠵᠤ ᠪᠢᠴᠢᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠪᠢᠳᠡᠬᠡᠢ ᠰᠢᠭ᠋ᠤᠳ ᠰᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Please do not pull out the SD Card when reading or writing</source>
        <translation>SD ᠺᠠᠷᠲ ᠢ᠋ ᠤᠩᠰᠢᠵᠤ ᠪᠢᠴᠢᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠪᠢᠳᠡᠬᠡᠢ ᠰᠢᠭ᠋ᠤᠳ ᠰᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>There is a problem with this device</source>
        <translation type="obsolete">此设备存在问题</translation>
    </message>
    <message>
        <source>telephone device</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠦ᠌ ᠲᠦᠭᠦᠭᠡᠷᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Removable storage device removed</source>
        <translation type="vanished">移动存储设备已移除</translation>
    </message>
    <message>
        <source>Please do not pull out the storage device when reading or writing</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠤᠩᠰᠢᠵᠤ ᠪᠢᠴᠢᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠪᠢᠳᠡᠬᠡᠢ ᠰᠢᠭ᠋ᠤᠳ ᠰᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Storage device removed</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠬᠠᠰᠤᠪᠠ</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation>ᠭᠤᠤᠯ ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <source>ukui flash disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kylin device daemon</source>
        <translation>U ᠲᠠᠪᠠᠭ ᠢ᠋ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation>ᠪᠠᠭᠳᠠᠭᠠᠮᠵᠢ ᠨᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <source>other user device</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦ᠋ᠨ ᠲᠦᠭᠦᠭᠡᠷᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>弹出</source>
        <translation>ᠦᠰᠦᠷᠴᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>RepairDialogBox</name>
    <message>
        <source>Disk test</source>
        <translation>ᠲᠦᠭᠦᠭᠡᠷᠤᠮᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk and drive are properly connected, make sure the disk is not a read-only disk, and try again. For more information, search for help on read-only files and how to change read-only files.&lt;/p&gt;</source>
        <translation type="vanished">&lt;h4&gt;系统无法识别U盘内容&lt;/h4&gt;&lt;p&gt;检查磁盘和驱动器是否正确连接，确保磁盘不是只读磁盘，然后重试。有关更多信息，请搜索有关只读文件和如何更改只读文件的帮助。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation>ᠲᠦᠭᠦᠭᠡᠷᠤᠮᠵᠢ ᠵᠢ ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Repair</source>
        <translation>ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation>&lt;h4&gt; ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠠᠭᠤᠯᠭ᠎ᠠ &lt;/h4&gt;&lt;p&gt; ᠢ᠋/ ᠵᠢ ᠳᠠᠨᠢᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠳ᠋ᠢᠰᠺ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ/ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠵᠦᠪ ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠡᠰᠡᠬᠦ᠂ ᠳ᠋ᠢᠰᠺ ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ ᠳ᠋ᠢᠰᠺ ᠪᠢᠰᠢ ᠬᠡᠳᠡᠭ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ᠃ ᠨᠡᠩ ᠤᠯᠠᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠬᠤᠯᠪᠤᠭᠳᠠᠬᠤ ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ ᠹᠠᠢᠯ ᠡᠴᠡ ᠬᠠᠢᠵᠤ᠂ ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠤᠰᠠᠯᠠᠮᠵᠢ ᠵᠢ ᠬᠡᠷᠬᠢᠨ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠵᠢ ᠪᠤᠳᠤᠭᠠᠷᠠᠢ &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive &apos;%1&apos; is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation>&lt;h4&gt; ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠠᠭᠤᠯᠭ᠎ᠠ &lt;/h4&gt;&lt;p&gt; ᠢ᠋/ ᠵᠢ ᠳᠠᠨᠢᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠳ᠋ᠢᠰᠺ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ/ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ &apos;%1&apos; ᠵᠦᠪ ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠡᠰᠡᠬᠦ᠂ ᠳ᠋ᠢᠰᠺ ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ ᠳ᠋ᠢᠰᠺ ᠪᠢᠰᠢ ᠬᠡᠳᠡᠭ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ᠃ ᠨᠡᠩ ᠤᠯᠠᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠬᠤᠯᠪᠤᠭᠳᠠᠬᠤ ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ ᠹᠠᠢᠯ ᠡᠴᠡ ᠬᠠᠢᠵᠤ᠂ ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠤᠰᠠᠯᠠᠮᠵᠢ ᠵᠢ ᠬᠡᠷᠬᠢᠨ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠵᠢ ᠪᠤᠳᠤᠭᠠᠷᠠᠢ &lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>RepairProgressBar</name>
    <message>
        <source>&lt;h3&gt;%1&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;%1&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Attempting a disk repair...</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠵᠠᠰᠠᠨ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠪᠡᠷ ᠤᠷᠤᠯᠳᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ…</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Repair successfully!</source>
        <translation>ᠵᠠᠰᠠᠨ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠴᠢᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <source>The repair completed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation type="vanished">修复失败，如果设备没有成功挂载，请尝试格式化修复！</translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk repair</source>
        <translation>ᠲᠦᠭᠦᠭᠡᠷᠤᠮᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Repair failed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation>ᠵᠠᠰᠠᠨ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠬᠡᠷᠪᠡ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠠᠮᠵᠢᠯᠳᠠ ᠲᠠᠢ ᠡᠯᠬᠦᠭᠳᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠪᠤᠯ ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠬᠦᠯᠵᠤ ᠵᠠᠰᠠᠨ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠵᠢ ᠤᠷᠤᠯᠳᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
</context>
<context>
    <name>ejectInterface</name>
    <message>
        <source>usb has been unplugged safely</source>
        <translation type="vanished">U盘已安全拔出</translation>
    </message>
    <message>
        <source>cdrom has been unplugged safely</source>
        <translation type="vanished">光盘已安全拔出</translation>
    </message>
    <message>
        <source>sd has been unplugged safely</source>
        <translation type="vanished">SD卡已安全拔出</translation>
    </message>
    <message>
        <source>usb is occupying unejectable</source>
        <translation type="vanished">U盘占用无法弹出</translation>
    </message>
    <message>
        <source>Storage device can be safely unplugged</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠵᠢᠡᠷ ᠰᠤᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>gpartedInterface</name>
    <message>
        <source>ok</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>gparted has started,can not eject</source>
        <translation>gparted ᠨᠢᠭᠡᠨᠳᠡ ᠡᠬᠢᠯᠡᠪᠡ᠂ ᠦᠰᠦᠷᠴᠤ ᠭᠠᠷᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>interactiveDialog</name>
    <message>
        <source>usb is occupying,do you want to eject it</source>
        <translation>U ᠲᠠᠪᠠᠭ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠦᠰᠦᠷᠭᠡᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠤᠤ</translation>
    </message>
    <message>
        <source>cdrom is occupying,do you want to eject it</source>
        <translation>ᠳ᠋ᠢᠰᠺ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠦᠰᠦᠷᠭᠡᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠤᠤ</translation>
    </message>
    <message>
        <source>sd is occupying,do you want to eject it</source>
        <translation>SD ᠺᠠᠷᠲ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠦᠰᠦᠷᠭᠡᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠤᠤ</translation>
    </message>
    <message>
        <source>cancle</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>cdrom is occupying</source>
        <translation>ᠳ᠋ᠢᠰᠺ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>sd is occupying</source>
        <translation>SD ᠺᠠᠷᠲ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>usb is occupying</source>
        <translation>U ᠲᠠᠪᠠᠭ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
</TS>
