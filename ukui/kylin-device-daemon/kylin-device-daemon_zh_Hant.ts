<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>BaseDialog</name>
    <message>
        <source>Disk test</source>
        <translation>磁碟測試</translation>
    </message>
</context>
<context>
    <name>DeviceOperation</name>
    <message>
        <source>unknown</source>
        <translation>未知</translation>
    </message>
</context>
<context>
    <name>FDClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation>容量為空</translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation>空白光碟</translation>
    </message>
    <message>
        <source>other user device</source>
        <translation>其他用戶設備</translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Eject</source>
        <translation>彈出</translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation>卸載</translation>
    </message>
</context>
<context>
    <name>FDFrame</name>
    <message>
        <source>eject</source>
        <translation>彈出</translation>
    </message>
</context>
<context>
    <name>FormateDialog</name>
    <message>
        <source>Formatted successfully!</source>
        <translation>格式化成功！</translation>
    </message>
    <message>
        <source>Formatting failed, please unplug the U disk and try again!</source>
        <translation>格式化失敗，請拔下U盤，然後重試！</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>格式</translation>
    </message>
    <message>
        <source>Rom size:</source>
        <translation>羅姆尺寸：</translation>
    </message>
    <message>
        <source>Filesystem:</source>
        <translation>檔案系統：</translation>
    </message>
    <message>
        <source>Disk name:</source>
        <translation>磁碟名稱：</translation>
    </message>
    <message>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation>完全擦除（時間更長，請確認！</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation>格式化磁碟</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please back up all retained data before formatting. Do you want to continue?</source>
        <translation>格式化此卷將擦除其上的所有數據。請在格式化之前備份所有保留的數據。是否要繼續？</translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk format</source>
        <translation>磁碟格式</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>usb management tool</source>
        <translation>USB 管理工具</translation>
    </message>
    <message>
        <source>ukui-flash-disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kindly reminder</source>
        <translation>溫馨提示</translation>
    </message>
    <message>
        <source>wrong reminder</source>
        <translation>錯誤的提醒</translation>
    </message>
    <message>
        <source>Please do not pull out the USB flash disk when reading or writing</source>
        <translation>讀取或寫入時請不要拔出USB快閃記憶體盤</translation>
    </message>
    <message>
        <source>Please do not pull out the CDROM when reading or writing</source>
        <translation>閱讀或寫入時請不要拔出光碟</translation>
    </message>
    <message>
        <source>Please do not pull out the SD Card when reading or writing</source>
        <translation>讀取或寫入時請不要拔出SD卡</translation>
    </message>
    <message>
        <source>There is a problem with this device</source>
        <translation type="obsolete">此设备存在问题</translation>
    </message>
    <message>
        <source>telephone device</source>
        <translation>電話設備</translation>
    </message>
    <message>
        <source>Removable storage device removed</source>
        <translation type="vanished">移动存储设备已移除</translation>
    </message>
    <message>
        <source>Please do not pull out the storage device when reading or writing</source>
        <translation>讀取或寫入時請不要拔出存儲設備</translation>
    </message>
    <message>
        <source>Storage device removed</source>
        <translation>存儲設備已刪除</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation>主視窗</translation>
    </message>
    <message>
        <source>ukui flash disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kylin device daemon</source>
        <translation>麒麟設備守護進程</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>格式</translation>
    </message>
</context>
<context>
    <name>QClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation>容量為空</translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation>空白光碟</translation>
    </message>
    <message>
        <source>other user device</source>
        <translation>其他用戶設備</translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>弹出</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RepairDialogBox</name>
    <message>
        <source>Disk test</source>
        <translation>磁碟測試</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk and drive are properly connected, make sure the disk is not a read-only disk, and try again. For more information, search for help on read-only files and how to change read-only files.&lt;/p&gt;</source>
        <translation type="vanished">&lt;h4&gt;系统无法识别U盘内容&lt;/h4&gt;&lt;p&gt;检查磁盘和驱动器是否正确连接，确保磁盘不是只读磁盘，然后重试。有关更多信息，请搜索有关只读文件和如何更改只读文件的帮助。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation>格式化磁碟</translation>
    </message>
    <message>
        <source>Repair</source>
        <translation>修</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;系統無法識別磁碟內容&lt;/h4&gt;&lt;p&gt;檢查磁碟/驅動器是否正確連接，確保磁碟不是只讀磁碟，然後重試。有關詳細資訊，請搜索有關唯讀檔以及如何更改只讀檔的説明。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive &apos;%1&apos; is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation>&lt;h4&gt;系統無法識別磁碟內容&lt;/h4&gt;&lt;p&gt;檢查磁碟/驅動器“%1”是否正確連接，確保磁碟不是只讀磁碟，然後重試。有關詳細資訊，請搜索有關唯讀檔以及如何更改只讀檔的説明。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>RepairProgressBar</name>
    <message>
        <source>&lt;h3&gt;%1&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;%1&lt;/h3&gt;</translation>
    </message>
    <message>
        <source>Attempting a disk repair...</source>
        <translation>嘗試磁碟修復...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Repair successfully!</source>
        <translation>修復成功！</translation>
    </message>
    <message>
        <source>The repair completed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation type="vanished">修复失败，如果设备没有成功挂载，请尝试格式化修复！</translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk repair</source>
        <translation>磁碟修復</translation>
    </message>
    <message>
        <source>Repair failed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation>修復失敗。如果未安裝USB快閃記憶體盤，請嘗試格式化設備！</translation>
    </message>
</context>
<context>
    <name>ejectInterface</name>
    <message>
        <source>usb has been unplugged safely</source>
        <translation type="vanished">U盘已安全拔出</translation>
    </message>
    <message>
        <source>cdrom has been unplugged safely</source>
        <translation type="vanished">光盘已安全拔出</translation>
    </message>
    <message>
        <source>sd has been unplugged safely</source>
        <translation type="vanished">SD卡已安全拔出</translation>
    </message>
    <message>
        <source>usb is occupying unejectable</source>
        <translation type="vanished">U盘占用无法弹出</translation>
    </message>
    <message>
        <source>Storage device can be safely unplugged</source>
        <translation>存儲設備可以安全地拔下插頭</translation>
    </message>
</context>
<context>
    <name>gpartedInterface</name>
    <message>
        <source>ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>gparted has started,can not eject</source>
        <translation>已啟動，無法彈出</translation>
    </message>
</context>
<context>
    <name>interactiveDialog</name>
    <message>
        <source>usb is occupying,do you want to eject it</source>
        <translation>USB正在佔用，是否要彈出</translation>
    </message>
    <message>
        <source>cdrom is occupying,do you want to eject it</source>
        <translation>光碟正在佔用，是否要彈出</translation>
    </message>
    <message>
        <source>sd is occupying,do you want to eject it</source>
        <translation>SD正在佔用，是否要彈出</translation>
    </message>
    <message>
        <source>cancle</source>
        <translation>坎克爾</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <source>cdrom is occupying</source>
        <translation>CDROM正在佔領</translation>
    </message>
    <message>
        <source>sd is occupying</source>
        <translation>SD正在佔用</translation>
    </message>
    <message>
        <source>usb is occupying</source>
        <translation>USB 正在佔用</translation>
    </message>
</context>
</TS>
