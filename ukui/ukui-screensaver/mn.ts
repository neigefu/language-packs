<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AgreementWindow</name>
    <message>
        <location filename="../src/agreementwindow.cpp" line="32"/>
        <source>I know</source>
        <translation>ᠪᠢ ᠮᠡᠳᠡᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>AuthDialog</name>
    <message>
        <source>More Devices</source>
        <translation type="obsolete">选择其他设备</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="obsolete">使用生物识别认证</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">使用密码认证</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="941"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠬᠤ</translation>
    </message>
    <message>
        <source>UnLock</source>
        <translation type="obsolete">解锁</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>LoggedIn</source>
        <translation type="obsolete">已登录</translation>
    </message>
    <message>
        <source>Password: </source>
        <translation type="vanished">密码：</translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">账户锁定%1分钟由于%2次错误尝试</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="345"/>
        <location filename="../src/authdialog.cpp" line="411"/>
        <source>Please try again in %1 minutes.</source>
        <translation>%1 ᠮᠢᠨᠦ᠋ᠲ᠎ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠠᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="354"/>
        <location filename="../src/authdialog.cpp" line="419"/>
        <source>Please try again in %1 seconds.</source>
        <translation>%1 ᠮᠢᠨᠦ᠋ᠲ᠎ᠦᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠠᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="363"/>
        <location filename="../src/authdialog.cpp" line="427"/>
        <source>Account locked permanently.</source>
        <translation>ᠳᠠᠩᠰᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠦᠨᠢᠳᠡ ᠤᠨᠢᠰᠤᠯᠠᠭᠳᠠᠪᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="650"/>
        <location filename="../src/authdialog.cpp" line="693"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>ᠨᠢᠭᠤᠷ ᠱᠢᠷᠪᠢᠵᠤ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="655"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠤᠷᠤᠮ ᠳᠠᠷᠤᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="660"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>ᠳᠠᠭᠤ᠎ᠪᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="665"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="670"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>ᠰᠤᠯᠤᠩᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ᠎ᠶᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="845"/>
        <source>Input Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1257"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>%1᠎ᠤ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1261"/>
        <location filename="../src/authdialog.cpp" line="1263"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>%1᠎ᠶᠢ/᠎ᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1293"/>
        <source>Abnormal network</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1300"/>
        <source>Face recognition waiting time out, please click refresh or enter the password to unlock.</source>
        <translation>ᠬᠦᠮᠦᠨ ᠤ ᠨᠢᠭᠤᠷ ᠢ ᠢᠯᠭᠠᠬᠤ ᠦᠶᠡᠰ᠂ ᠰᠢᠨᠡᠳᠭᠡᠬᠦ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠤᠮᠸᠷ ᠤᠨ ᠣᠨᠢᠰᠤ ᠶᠢ ᠲᠠᠷᠤᠭᠠᠷᠠᠢ᠃</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="747"/>
        <source>Password cannot be empty</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1276"/>
        <location filename="../src/authdialog.cpp" line="1280"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>%1᠎ᠶᠢᠨ/᠎ᠦᠨ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠲᠠ ᠪᠠᠰᠠ%2 ᠤᠳᠠᠭᠠᠨ᠎ᠤ ᠳᠤᠷᠱᠢᠬᠤ ᠵᠠᠪᠱᠢᠶᠠᠨ ᠲᠠᠢ</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="vanished">密码错误，请重试</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">认证失败，还剩%1次尝试机会</translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="888"/>
        <source>Authentication failure, Please try again</source>
        <translation>ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="675"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋ ᠱᠢᠷᠪᠢᠬᠦ᠌ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="235"/>
        <source>Enter the ukey password</source>
        <translation>ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="252"/>
        <source>Insert the ukey into the USB port</source>
        <translation>ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠦ ᠨᠢᠭᠤᠴᠠ ᠶᠢ USB ᠦᠵᠦᠭᠦᠷ ᠲᠦ ᠬᠠᠳᠬᠤᠵᠤ ᠣᠷᠣᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="843"/>
        <source>Password </source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ </translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="924"/>
        <source>Login</source>
        <translation>ᠳᠠᠩᠰᠠᠯᠠᠨ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">指纹验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>Fingerprint authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">指纹验证失败，您还有%1次尝试机会</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <source>Please select other biometric devices</source>
        <translation type="obsolete">请选择其他生物识别设备</translation>
    </message>
    <message>
        <source>Device Type:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation type="obsolete">设备名称：</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="119"/>
        <source>Current device: </source>
        <translation>ᠤᠳᠤᠬᠠᠨ᠎ᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ </translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="185"/>
        <source>Identify failed, Please retry.</source>
        <translation>ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="48"/>
        <source>Please select the biometric device</source>
        <translation>ᠠᠮᠢᠳᠤ ᠪᠤᠳᠠᠰ᠎ᠤᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="53"/>
        <source>Device type:</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠳᠦᠷᠰᠦ ᠄</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="69"/>
        <source>Device name:</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢᠨ ᠱᠢᠨᠵᠢ ᠳᠡᠮᠳᠡᠭ ᠄</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="79"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charsmorewidget.cpp" line="183"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="114"/>
        <source>More</source>
        <translation>ᠨᠡᠩ ᠠᠷᠪᠢᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="128"/>
        <source>ABC</source>
        <translation>ABC</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="141"/>
        <source>123</source>
        <translation>123</translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="38"/>
        <source>edit network</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="116"/>
        <source>LAN name: </source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠄ </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="117"/>
        <source>Method: </source>
        <translation>ᠠᠷᠭ᠎ᠠ ᠄ </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="118"/>
        <source>Address: </source>
        <translation>ᠬᠠᠶᠢᠭ ᠄ </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="119"/>
        <source>Netmask: </source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠠᠯᠳᠠᠯᠠᠯ ᠄ </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="120"/>
        <source>Gateway: </source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠪᠤᠭᠤᠮᠳᠠ ᠄ </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="121"/>
        <source>DNS 1: </source>
        <translation>DNS᠎ᠢ ᠰᠤᠩᠭᠤᠬᠤ </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="122"/>
        <source>DNS 2: </source>
        <translation>ᠪᠡᠯᠡᠳᠭᠡᠯ ᠰᠤᠩᠭᠤᠭᠤᠯᠢDNS ᠄ </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="124"/>
        <source>Edit Conn</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="125"/>
        <location filename="../KylinNM/src/confform.cpp" line="127"/>
        <source>Auto(DHCP)</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋(DHCP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="126"/>
        <location filename="../KylinNM/src/confform.cpp" line="128"/>
        <source>Manual</source>
        <translation>ᠭᠠᠷ᠎ᠢᠶᠠᠷ ᠬᠦᠳᠡᠯᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="158"/>
        <source>Cancel</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="159"/>
        <source>Save</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="160"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="301"/>
        <source>Can not create new wired network for without wired card</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ ᠳᠤᠳᠠᠭᠤ ᠂ ᠱᠢᠨ᠎ᠡ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠠᠢᠭᠤᠯᠬᠤ᠎ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="318"/>
        <source>New network already created</source>
        <translation>ᠱᠢᠨ᠎ᠡ ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠶᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠭᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="362"/>
        <source>New network settings already finished</source>
        <translation>ᠱᠢᠨ᠎ᠡ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠢᠷᠢᠯᠠᠯ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠰᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="395"/>
        <source>Edit Network</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="431"/>
        <source>Add Wired Network</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>create wired network successfully</source>
        <translation type="obsolete">已创建新的有线网络</translation>
    </message>
    <message>
        <source>change configuration of wired network successfully</source>
        <translation type="obsolete">新的设置已经生效</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="371"/>
        <source>New settings already effective</source>
        <translation>ᠱᠢᠨ᠎ᠡ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <source>There is a same named LAN exsits.</source>
        <translation type="obsolete">已有同名连接存在</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="61"/>
        <source>FingerPrint</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠤᠷᠤᠮ</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="63"/>
        <source>FingerVein</source>
        <translation>ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="65"/>
        <source>Iris</source>
        <translation>ᠰᠤᠯᠤᠩᠭ᠎ᠠ ᠪᠦᠷᠬᠦᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="67"/>
        <source>Face</source>
        <translation>ᠴᠢᠷᠠᠢ᠎ᠪᠠᠷ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="69"/>
        <source>VoicePrint</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠢᠷᠠᠯᠵᠢ</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="71"/>
        <source>Ukey</source>
        <translation>ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠪᠠᠷ ᠬᠥᠯᠬᠢᠳᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>ukey</source>
        <translation type="vanished">ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠪᠠᠷ ᠬᠥᠯᠬᠢᠳᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="73"/>
        <source>QRCode</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
</context>
<context>
    <name>DigitalAuthDialog</name>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="57"/>
        <location filename="../src/digitalauthdialog.cpp" line="759"/>
        <source>LoginByUEdu</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠠᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>now is authing, wait a moment</source>
        <translation type="vanished">认证中，请稍后</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="obsolete">密码错误，请重试</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="61"/>
        <source>ResetPWD?</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠪᠠᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="87"/>
        <location filename="../src/digitalauthdialog.cpp" line="776"/>
        <source>SetNewUEduPWD</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ ᠱᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="599"/>
        <source>ConfirmNewUEduPWD</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ ᠱᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="611"/>
        <source>The two password entries are inconsistent, please reset</source>
        <translation>ᠬᠤᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ᠎ᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="700"/>
        <source>Password entered incorrectly, please try again</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="224"/>
        <source>clear</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="76"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="77"/>
        <source>Connection</source>
        <translation>ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="78"/>
        <source>Wi-Fi name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="79"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="81"/>
        <source>Connect</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="104"/>
        <source>None</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="105"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠬᠦᠮᠦᠨ</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA 及 WPA2 企业</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭWi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭWi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="68"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="69"/>
        <source>Connection</source>
        <translation>ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="70"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="71"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="72"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="73"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="74"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="75"/>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="77"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="95"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="96"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="97"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="98"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠤᠨ ᠦᠬᠦᠯᠡᠪᠦᠷᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="100"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯWEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="101"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="83"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="84"/>
        <source>Connection</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="85"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="86"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="87"/>
        <source>Authentication</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ᠌ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="88"/>
        <source>Anonymous identity</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="89"/>
        <source>Allow automatic PAC pro_visioning</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋PACᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="90"/>
        <source>PAC file</source>
        <translation>PAC ᠹᠠᠢᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="91"/>
        <source>Inner authentication</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ᠎ᠶᠢᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="92"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="93"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="94"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="95"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="100"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="118"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="148"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="119"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="120"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ （ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ᠎ᠪᠠᠷ ᠳᠠᠪᠱᠢᠬᠤ ᠳᠦᠷᠢᠮ ᠡᠰᠡᠬᠦᠯ᠎ᠡASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="121"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠤᠨ ᠦᠬᠦᠯᠡᠪᠦᠷᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="123"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯWEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="124"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="136"/>
        <source>Tunneled TLS</source>
        <translation>ᠨᠦᠬᠡᠨ ᠵᠠᠮTLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="137"/>
        <source>Protected EAP (PEAP)</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ᠎ᠳᠤ ᠪᠠᠭᠳᠠᠬᠤEAP (PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="143"/>
        <source>Anonymous</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="144"/>
        <source>Authenticated</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠴᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="145"/>
        <source>Both</source>
        <translation>ᠬᠤᠶᠠᠭᠤᠯᠠ᠎ᠶᠢ ᠬᠤᠤᠰᠯᠠᠭᠤᠯᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="74"/>
        <source>Connection</source>
        <translation>ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="75"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="77"/>
        <source>Authentication</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="78"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="79"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="81"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="101"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="119"/>
        <source>Tunneled TLS</source>
        <translation>ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="120"/>
        <source>Protected EAP (PEAP)</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ᠎ᠳᠤ ᠪᠠᠭᠳᠠᠬᠤEAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="91"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="92"/>
        <source>Connection</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="93"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="94"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="95"/>
        <source>Authentication</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="96"/>
        <source>Anonymous identity</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="97"/>
        <source>Domain</source>
        <translation>ᠨᠸᠲ᠎ᠦᠨ ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="98"/>
        <source>CA certificate</source>
        <translation>CAᠦᠨᠡᠮᠯᠡᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="99"/>
        <source>CA certificate password</source>
        <translation>CA ᠦᠨᠡᠮᠯᠡᠯ᠎ᠦᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="100"/>
        <source>No CA certificate is required</source>
        <translation>CA ᠦᠨᠡᠮᠯᠡᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="101"/>
        <source>PEAP version</source>
        <translation>PEAP ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="102"/>
        <source>Inner authentication</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ᠎ᠶᠢᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="103"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="104"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="105"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="106"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="126"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="149"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="127"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="128"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="129"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="131"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="132"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="144"/>
        <source>Tunneled TLS</source>
        <translation>ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="145"/>
        <source>Protected EAP (PEAP)</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="150"/>
        <source>Choose from file</source>
        <translation>ᠹᠠᠢᠯ᠎ᠠᠴᠠ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="153"/>
        <source>Automatic</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="154"/>
        <source>Version 0</source>
        <translation>ᠬᠡᠪᠯᠡᠯ0</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="155"/>
        <source>Version 1</source>
        <translation>ᠬᠡᠪᠯᠡᠯ1</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="74"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="75"/>
        <source>Connection</source>
        <translation>ᠵᠠᠯᠭᠠᠰᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="76"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="77"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="78"/>
        <source>Authentication</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="79"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="80"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="81"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="82"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="84"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="102"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="103"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="104"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="105"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="107"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="108"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="120"/>
        <source>Tunneled TLS</source>
        <translation>ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="121"/>
        <source>Protected EAP (PEAP)</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="90"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="91"/>
        <source>Connection</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="92"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="93"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="94"/>
        <source>Authentication</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="95"/>
        <source>Identity</source>
        <translation>ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="96"/>
        <source>Domain</source>
        <translation>ᠣᠷᠣᠨ ᠪᠦᠰᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="97"/>
        <source>CA certificate</source>
        <translation>CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="98"/>
        <source>CA certificate password</source>
        <translation>CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="99"/>
        <source>No CA certificate is required</source>
        <translation>CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="100"/>
        <source>User certificate</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠨᠡᠮᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="101"/>
        <source>User certificate password</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="102"/>
        <source>User private key</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠬᠤᠪᠢ᠎ᠶᠢᠨ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="103"/>
        <source>User key password</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ᠎ᠦᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="104"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="105"/>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="107"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="125"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="148"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="152"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="156"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="126"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="127"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="128"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="130"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="131"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="143"/>
        <source>Tunneled TLS</source>
        <translation>ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="144"/>
        <source>Protected EAP (PEAP)</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="149"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="153"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="157"/>
        <source>Choose from file</source>
        <translation>ᠹᠠᠢᠯ᠎ᠠᠴᠠ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="89"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="90"/>
        <source>Connection</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="91"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="92"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="93"/>
        <source>Authentication</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="94"/>
        <source>Anonymous identity</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ᠎ᠶᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="95"/>
        <source>Domain</source>
        <translation>ᠣᠷᠣᠨ ᠪᠦᠰᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="96"/>
        <source>CA certificate</source>
        <translation>CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="97"/>
        <source>CA certificate password</source>
        <translation>CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="98"/>
        <source>No CA certificate is required</source>
        <translation>CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="99"/>
        <source>Inner authentication</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ᠎ᠶᠢᠨ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="100"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="101"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="102"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="103"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="105"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="123"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="146"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="124"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="125"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="126"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="128"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="129"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="141"/>
        <source>Tunneled TLS</source>
        <translation>ᠨᠦᠬᠡᠨ ᠵᠠᠮ TLS</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="142"/>
        <source>Protected EAP (PEAP)</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ EAP (PEAP)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="147"/>
        <source>Choose from file</source>
        <translation>ᠹᠠᠢᠯ᠎ᠠᠴᠠ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="74"/>
        <source>Connection</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="75"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="77"/>
        <source>Key</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="78"/>
        <source>WEP index</source>
        <translation>WEPᠡᠷᠢᠯᠲᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="79"/>
        <source>Authentication</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="81"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="101"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 ᠨᠢᠭᠤᠴᠠ ᠭᠬᠢᠷ (ᠠᠷᠪᠠᠨ ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠦᠢᠯᠡᠳᠦᠯ ᠪᠤᠶᠤ ASCII)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷᠲᠤ ᠦᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>ᠬᠦᠳᠡᠯᠦᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ WEP (802.1X)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA ᠵᠢᠴᠢWPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="115"/>
        <source>1(default)</source>
        <translation>1( ᠠᠶᠠᠳᠠᠯ)</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="121"/>
        <source>Open System</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠯᠳᠡᠳᠡᠢ ᠱᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="122"/>
        <source>Shared Key</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ᠌ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="82"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="83"/>
        <source>Connection</source>
        <translation>ᠬᠤᠯᠪᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="84"/>
        <source>Wi-Fi name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="85"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="86"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="87"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="88"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="90"/>
        <source>C_reate…</source>
        <translation>ᠱᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="113"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="114"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA 及 WPA2 企业</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.ui" line="14"/>
        <source>Dialog</source>
        <translation>ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="46"/>
        <source>Create Hotspot</source>
        <translation>ᠬᠤᠪᠢ᠎ᠶᠢᠨ ᠬᠠᠯᠠᠮᠱᠢᠯ ᠴᠡᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="47"/>
        <source>Network name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="48"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="49"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="50"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="51"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="54"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="55"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA ᠪᠣᠯᠣᠨ WPA2 ᠬᠤᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../src/iconedit.cpp" line="115"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>InputInfos</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="338"/>
        <source>Service exception...</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="341"/>
        <source>Invaild parameters...</source>
        <translation>ᠫᠠᠷᠠᠮᠸᠲᠷ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="344"/>
        <source>Unknown fault:%1</source>
        <translation>ᠦᠯᠦ᠍ ᠮᠡᠳᠡᠬᠦ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠄%1</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="288"/>
        <source>Recapture(60s)</source>
        <translation>ᠳᠠᠬᠢᠨ ᠤᠯᠵᠠᠯᠠᠬᠤ(60s)</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="312"/>
        <source>Recapture(%1s)</source>
        <translation>ᠳᠠᠬᠢᠨ ᠤᠯᠵᠠᠯᠠᠬᠤ(%1s)</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="141"/>
        <location filename="../src/verificationwidget.cpp" line="318"/>
        <source>Get code</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠤᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <source>KeyboardWidget</source>
        <translation type="vanished">ᠳᠠᠷᠤᠭᠤᠯ᠎ᠤᠨ ᠵᠢᠵᠢᠭ ᠲᠤᠨᠤᠭᠯᠠᠯ</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">麒麟网络工具信息提示</translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <location filename="../KylinNM/src/kylinnm.ui" line="14"/>
        <source>kylin-nm</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="413"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="417"/>
        <source>LAN</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Enabel LAN List</source>
        <translation type="obsolete">其他有线网络</translation>
    </message>
    <message>
        <source>WiFi</source>
        <translation type="obsolete">无线网络</translation>
    </message>
    <message>
        <source>Enabel WiFi List</source>
        <translation type="obsolete">其他无线网络</translation>
    </message>
    <message>
        <source>New WiFi</source>
        <translation type="obsolete">加入其他网络</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="456"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="610"/>
        <source>Advanced</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Connect Hide Network</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="vanished">已开启</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">已关闭</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="438"/>
        <source>HotSpot</source>
        <translation>ᠬᠤᠪᠢ᠎ᠶᠢᠨ ᠬᠠᠯᠠᠮᠱᠢᠯ ᠴᠡᠭ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="447"/>
        <source>FlyMode</source>
        <translation>ᠨᠢᠰᠦᠯᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="vanished">显示网络连接界面</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="293"/>
        <source>Inactivated LAN</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Inactivated WLAN</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="317"/>
        <source>Other WLAN</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="423"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="427"/>
        <source>WLAN</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ᠎ᠦ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="609"/>
        <source>Show KylinNM</source>
        <translation>KylinNM᠎ᠶᠢ/᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1279"/>
        <source>No wireless card detected</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ᠎ᠢ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1316"/>
        <source>Activated LAN</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1379"/>
        <source>Activated WLAN</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1423"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1522"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1694"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2419"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2510"/>
        <source>Not connected</source>
        <translation>ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1426"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1524"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1594"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1595"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1697"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1821"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1988"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2421"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2512"/>
        <source>Disconnected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1620"/>
        <source>No Other Wired Network Scheme</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ᠎ᠳᠤ ᠪᠤᠰᠤᠳ ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">编辑</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <source>No wifi connected.</source>
        <translation type="obsolete">未连接任何网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1842"/>
        <source>No Other Wireless Network Scheme</source>
        <translation>ᠪᠤᠰᠤᠳ ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠶᠢ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2328"/>
        <source>Wired net is disconnected</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠶᠢ ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Wi-Fi is disconnected</source>
        <translation type="obsolete">断开无线网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2741"/>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation>Wi-Fi᠎ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠶᠤ ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢ ᠪᠠᠳᠤᠯᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">其他有线网络</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="obsolete">新建有线网络</translation>
    </message>
    <message>
        <source>Hide WiFi</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="359"/>
        <source>No usable network in the list</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ᠎ᠳᠤ ᠤᠳᠤᠬᠠᠨ᠎ᠳᠠᠭᠠᠨ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1575"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1791"/>
        <source>NetOn,</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ ᠂</translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">其他无线网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>keep wired network switch is on before turning on wireless switch</source>
        <translation type="vanished">打开无线网开关前保持有线网开关打开</translation>
    </message>
    <message>
        <source>please insert the wireless network adapter</source>
        <translation type="vanished">请先插入无线网卡</translation>
    </message>
    <message>
        <source>Abnormal connection exist, program will delete it</source>
        <translation type="vanished">正在断开异常连接的网络</translation>
    </message>
    <message>
        <source>update Wi-Fi list now, click again</source>
        <translation type="vanished">正在更新 Wi-Fi列表 请再次点击</translation>
    </message>
    <message>
        <source>update Wi-Fi list now</source>
        <translation type="vanished">正在更新 Wi-Fi列表</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2695"/>
        <source>Conn Ethernet Success</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠠᠮᠵᠢᠯᠳᠠᠳᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2707"/>
        <source>Conn Ethernet Fail</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2732"/>
        <source>Conn Wifi Success</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠠᠮᠵᠢᠯᠳᠠᠳᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="170"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="184"/>
        <source>123</source>
        <translation>123</translation>
    </message>
    <message>
        <source>Ctrl</source>
        <translation type="vanished">ᠳᠠᠷᠤᠶ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Alt</source>
        <translation type="vanished">ᠣᠷᠣᠯᠠᠭᠤᠯᠬᠤ ᠵᠦᠢᠯ ᠢ ᠣᠷᠣᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <location filename="../src/lockwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/lockwidget.ui" line="58"/>
        <source>Date</source>
        <translation>ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.ui" line="51"/>
        <source>Time</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">游客</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="666"/>
        <source>Power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ᠃</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="729"/>
        <source>VirtualKeyboard</source>
        <translation>ᠬᠡᠶᠢᠰᠬᠡᠷ ᠲᠠᠷᠤᠭᠤᠯ ᠤᠨ ᠫᠠᠨᠰᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="757"/>
        <source>SwitchUser</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ ᠰᠣᠯᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="973"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>ᠬᠡᠳᠦ ᠬᠡᠳᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠠᠳᠠᠯᠢ ᠴᠠᠭ᠎ᠲᠤ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠲᠠᠢ ᠂ ᠲᠠ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">ᠬᠡᠳᠦ ᠬᠡᠳᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠠᠳᠠᠯᠢ ᠴᠠᠭ᠎ᠲᠤ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠲᠠᠢ ᠂ ᠲᠠ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="1224"/>
        <source>LAN</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="1226"/>
        <source>WLAN</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ᠎ᠦ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="72"/>
        <source>Login Options</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌ ᠰᠤᠩᠭᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="288"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="314"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ ᠃</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="727"/>
        <source>Identify device removed!</source>
        <translation>ᠬᠠᠷᠭᠤᠭᠤᠯᠵᠤ ᠱᠢᠯᠭᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠱᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>MyLineEdit</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="599"/>
        <location filename="../src/verificationwidget.cpp" line="605"/>
        <location filename="../src/verificationwidget.cpp" line="622"/>
        <source>Verification code</source>
        <translation>ᠤᠬᠤᠷ ᠮᠡᠳᠡᠭᠡᠨ᠎ᠦ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋</translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="160"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="174"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠢᠷᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../KylinNM/src/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation>ᠬᠦᠰᠦᠨᠦᠭ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="158"/>
        <source>Automatically join the network</source>
        <translation>ᠲᠤᠰ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠠᠦ᠋ᠲ᠋ᠤ᠋᠎ᠪᠠᠷ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation type="vanished">输入密码</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="42"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="43"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="44"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="46"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="45"/>
        <source>Disconnect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="47"/>
        <source>Input Password...</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ...</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="419"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠢᠭ Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠳᠦ ᠵᠠᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="555"/>
        <source>Signal：</source>
        <translation>ᠲᠤᠬᠢᠶᠠᠨ᠎ᠤ ᠡᠷᠴᠢᠮ ᠄</translation>
    </message>
    <message>
        <source>Public</source>
        <translation type="vanished">开放</translation>
    </message>
    <message>
        <source>Safe</source>
        <translation type="vanished">安全</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="vanished">速率</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="552"/>
        <source>None</source>
        <translation>ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="554"/>
        <source>WiFi Security：</source>
        <translation>Wi-Fi᠎ᠤ ᠠᠶᠤᠯᠬᠦᠢ ᠱᠢᠨᠵᠢ：</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="556"/>
        <source>MAC：</source>
        <translation>ᠹᠢᠼᠢᠺ᠎ᠦᠨ ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="833"/>
        <source>Conn Wifi Failed</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../KylinNM/src/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation>ᠬᠦᠰᠦᠨᠦᠭ</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="31"/>
        <location filename="../KylinNM/src/onelancform.cpp" line="32"/>
        <source>Connect</source>
        <translation>ᠰᠢᠷᠪᠢᠬᠦᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="34"/>
        <source>Disconnect</source>
        <translation>ᠲᠠᠰᠤᠷᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="289"/>
        <location filename="../KylinNM/src/onelancform.cpp" line="293"/>
        <source>No Configuration</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="296"/>
        <source>IPv4：</source>
        <translation>IPv4 ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="297"/>
        <source>IPv6：</source>
        <translation>IPv6 ᠬᠠᠶᠢᠭ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="298"/>
        <source>BandWidth：</source>
        <translation>ᠪᠦᠰᠡ᠎ᠶᠢᠨ ᠦᠷᠭᠡᠨ ᠄</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="299"/>
        <source>MAC：</source>
        <translation>MAC ᠬᠠᠶᠢᠭ：</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">自动</translation>
    </message>
</context>
<context>
    <name>PhoneAuthWidget</name>
    <message>
        <location filename="../src/permissioncheck.cpp" line="236"/>
        <location filename="../src/verificationwidget.cpp" line="375"/>
        <source>Verification by phoneNum</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/permissioncheck.cpp" line="241"/>
        <source>「 Use bound Phone number to verification 」</source>
        <translation>「ᠤᠶᠠᠭᠰᠠᠨ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠭᠠᠷᠠᠢ」</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="381"/>
        <source>「 Use SMS to verification 」</source>
        <translation>「ᠲᠤᠰ ᠳᠠᠩᠰᠠᠨ᠎ᠳᠤ ᠤᠶᠠᠭᠰᠠᠨ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠭᠠᠷᠠᠢ」</translation>
    </message>
    <message>
        <location filename="../src/permissioncheck.cpp" line="259"/>
        <location filename="../src/verificationwidget.cpp" line="399"/>
        <source>commit</source>
        <translation>ᠳᠤᠱᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="484"/>
        <location filename="../src/verificationwidget.cpp" line="524"/>
        <source>Network not connected~</source>
        <translation>ᠱᠢᠰᠲ᠋ᠧᠮ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠬᠤᠯᠪᠤᠭᠳᠠᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ~</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="530"/>
        <source>Network unavailable~</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠢᠳᠠᠯ ᠮᠠᠭᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ~</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="487"/>
        <source>Verification Code invalid!</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="490"/>
        <source>Verification Code incorrect.Please retry!</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ! ᠵᠦᠪ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋᠎ᠢ ᠳᠠᠭᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="493"/>
        <source>Failed time over limit!Retry after 1 hour!</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠤᠳᠠᠭ᠎ᠠ10᠎ᠶᠢ ᠬᠡᠳᠦᠷᠡᠪᠡ ᠂1 ᠴᠠᠭ᠎ᠤᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠱᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="497"/>
        <source>verifaction failed!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ᠎ᠤ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <location filename="../src/powermanager.cpp" line="293"/>
        <source>lock</source>
        <translation>ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>logout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>reboot</source>
        <translation type="vanished">重启</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="310"/>
        <source>Log Out</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="328"/>
        <location filename="../src/powermanager.cpp" line="668"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="348"/>
        <source>Power Off</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="666"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ</source>
        <translation type="vanished">ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="689"/>
        <source>Close all apps, and then shut down your computer</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦᠢᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="692"/>
        <source>Shut Down</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="721"/>
        <source>Turn off your computer, but the app stays open. When the computer is turned on, it can be restored to the state you left</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠴᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠳᠠᠭ ᠃ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳᠦ ᠂ ᠴᠢᠨᠦ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="723"/>
        <source>Hibernate</source>
        <translation>ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="752"/>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠮᠠᠰᠢᠨ ᠢᠶᠠᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠳᠠᠭ ᠂ ᠭᠡᠪᠡᠴᠦ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠣᠷᠣᠭᠳᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠡᠯᠢᠶᠡᠳ ᠪᠠᠭ᠎ᠠ ᠃ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠦᠭᠰᠡᠭᠡᠷ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠲᠦᠷᠭᠡᠨ ᠰᠡᠷᠢᠭᠡᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠴᠢᠨᠦ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="755"/>
        <source>Suspend</source>
        <translation>ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="90"/>
        <source>The screensaver is active.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠠᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="92"/>
        <source>The screensaver is inactive.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠠᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <source>exit(Esc)</source>
        <translation type="vanished">退出(Esc)</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="165"/>
        <source>Picture does not exist</source>
        <translation>ᠵᠢᠷᠤᠭ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Set as desktop wallpaper</source>
        <translation type="vanished">设置为桌面壁纸</translation>
    </message>
    <message>
        <source>Automatic switching</source>
        <translation type="vanished">自动切换</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="obsolete">您有%1条未读消息</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="1478"/>
        <source>You have new notification</source>
        <translation>ᠲᠠ ᠱᠢᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠤᠯᠤᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="1309"/>
        <source>View</source>
        <translation>ᠬᠡᠪ ᠦᠵᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>SleepTime</name>
    <message>
        <source>You have rested:</source>
        <translation type="vanished">ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠠᠮᠠᠷᠠᠪᠠ ᠄</translation>
    </message>
    <message>
        <location filename="../screensaver/sleeptime.cpp" line="72"/>
        <source>You have rested</source>
        <translation>ᠲᠠ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠮᠠᠷᠠᠵᠠᠢ᠃</translation>
    </message>
</context>
<context>
    <name>SureWindow</name>
    <message>
        <location filename="../src/surewindow.ui" line="14"/>
        <source>Form</source>
        <translation>ᠬᠦᠰᠦᠨᠦᠭ</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="56"/>
        <source>TextLabel</source>
        <translation>ᠲᠸᠺᠰᠲ᠎ᠦᠨ ᠱᠤᠱᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="179"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="160"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">同时有多个用户登录系统，您确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="56"/>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠤᠨᠳᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="59"/>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="53"/>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠬᠠᠭᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="50"/>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>login by password</source>
        <translation type="vanished">密码登录</translation>
    </message>
    <message>
        <source>login by qr code</source>
        <translation type="vanished">微信登录</translation>
    </message>
</context>
<context>
    <name>SwitchButtonGroup</name>
    <message>
        <location filename="../src/switchbuttongroup.cpp" line="35"/>
        <source>uEduPWD</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/switchbuttongroup.cpp" line="36"/>
        <source>Wechat</source>
        <translation>ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>TabletLockWidget</name>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="275"/>
        <source>New password is the same as old</source>
        <translation>ᠱᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠯᠤᠨ ᠤᠤᠯ᠎ᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠠᠳᠠᠯᠢ</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="286"/>
        <source>Reset password error:%1</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋᠎ᠢ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠄%1</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="298"/>
        <source>Please scan by correct WeChat</source>
        <translation>ᠲᠠ ᠵᠦᠪ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋᠎ᠢ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="189"/>
        <location filename="../src/tabletlockwidget.cpp" line="209"/>
        <location filename="../src/tabletlockwidget.cpp" line="228"/>
        <location filename="../src/tabletlockwidget.cpp" line="243"/>
        <location filename="../src/tabletlockwidget.cpp" line="382"/>
        <location filename="../src/tabletlockwidget.cpp" line="397"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="195"/>
        <location filename="../src/tabletlockwidget.cpp" line="407"/>
        <source>Back</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="248"/>
        <source>Skip</source>
        <translation>ᠦᠰᠦᠷᠴᠤ᠌ ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../KylinNM/src/utils.cpp" line="87"/>
        <source>kylin network applet desktop message</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠪᠠᠭᠠᠵᠢ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ᠎ᠪᠠᠷ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>VerificationWidget</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="55"/>
        <source>Please scan by bound WeChat</source>
        <translation>ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋᠎ᠢ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>VerticalVerificationWidget</name>
    <message>
        <location filename="../src/verticalVerificationwidget.cpp" line="52"/>
        <source>Please scan by bound WeChat</source>
        <translation>ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠺᠤᠳ᠋᠎ᠢ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>WeChatAuthDialog</name>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="74"/>
        <location filename="../src/wechatauthdialog.cpp" line="136"/>
        <source>Login by wechat</source>
        <translation>ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="78"/>
        <location filename="../src/wechatauthdialog.cpp" line="140"/>
        <source>Verification by wechat</source>
        <translation>ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="75"/>
        <location filename="../src/wechatauthdialog.cpp" line="137"/>
        <source>「 Use registered WeChat account to login 」</source>
        <translation>「 ᠨᠢᠭᠡᠨᠳᠡ ᠪᠦᠷᠢᠳᠭᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠸᠢᠴᠠᠲ᠎ᠤᠨ ᠨᠤᠮᠸᠷ᠎ᠢᠶᠠᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ᠌」</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="79"/>
        <location filename="../src/wechatauthdialog.cpp" line="141"/>
        <source>「 Use bound WeChat account to verification 」</source>
        <translation>「 ᠲᠤᠰ ᠳᠠᠩᠰᠠᠨ᠎ᠳᠤ ᠤᠶᠠᠭᠰᠠᠨ ᠸᠢᠴᠠᠲ᠎ᠢᠶᠠᠷ ᠪᠠᠳᠤᠯᠠᠬᠤ」</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="128"/>
        <location filename="../src/wechatauthdialog.cpp" line="183"/>
        <source>Network not connected~</source>
        <translation>ᠱᠢᠰᠲ᠋ᠧᠮ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳᠦ ᠬᠤᠯᠪᠤᠭᠳᠠᠭ᠎ᠠ ᠦᠬᠡᠢ ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ~</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="227"/>
        <source>Scan code successfully</source>
        <translation>ᠺᠤᠳ᠋ ᠱᠢᠷᠪᠢᠵᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="252"/>
        <source>Timeout!Try again!</source>
        <translation>ᠴᠠᠭ ᠬᠡᠳᠦᠷᠡᠪᠡ! ᠺᠤᠳ᠋᠎ᠢ ᠳᠠᠬᠢᠵᠤ ᠱᠢᠷᠪᠢᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation type="vanished">登录失败</translation>
    </message>
</context>
<context>
    <name>delay</name>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="205"/>
        <source>how long to show lock</source>
        <translation>ᠡᠳᠦᠢ ᠤᠷᠲᠤ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ᠎ᠪᠠᠷ ᠣᠨᠢᠰᠣ᠎ᠶ᠋ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>has-lock</name>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="208"/>
        <source>if show lock</source>
        <translation>ᠣᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠪᠡᠯ</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="43"/>
        <source>Start command for the ukui ScreenSaver.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠵᠠᠷᠯᠢᠭ᠎ᠲᠤ ᠬᠦᠷᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="48"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="192"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="194"/>
        <source>lock the screen immediately</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠳᠡᠯᠬᠡᠴᠡ᠎ᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="51"/>
        <source>query the status of the screen saver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠢ ᠤᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="54"/>
        <source>unlock the screen saver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠤᠨᠢᠰᠤ᠎ᠶᠢ ᠳᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="56"/>
        <source>show the screensaver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="58"/>
        <source>show blank and delay to lock,param:idle/lid/lowpower</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠬᠤ ᠪᠥᠭᠡᠳ ᠣᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠬᠣᠢᠰᠢᠯᠠᠭᠤᠯᠬᠤ ᠫᠷᠠᠮᠧᠲ᠋ᠷ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠄ ᠰᠤᠯᠠ / ᠳᠡᠭᠡᠷ᠎ᠡ ᠬᠠᠪᠬᠠᠭᠠᠰᠤ / ᠪᠠᠭ᠎ᠠ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="186"/>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ᠎ᠦᠨ ᠶᠠᠷᠢᠯᠴᠠᠯᠭ᠎ᠠ᠎ᠶᠢᠨ ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="196"/>
        <source>activated by session idle signal</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠰᠤᠯᠠ ᠳᠣᠬᠢᠶ᠎ᠠ᠎ᠪᠠᠷ ᠢᠳᠡᠪᠬᠢᠵᠢᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="198"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="202"/>
        <source>lock the screen and show screensaver immediately</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ᠎ᠶᠢᠨ ᠵᠡᠷᠬᠡᠴᠡᠬᠡ ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ᠎ᠢ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="200"/>
        <source>show screensaver immediately</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠡᠮ᠎ᠢ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="204"/>
        <source>show blank screensaver immediately and delay time to show lock</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠦ ᠣᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠴᠠᠭ᠎ᠢ᠋ ᠬᠣᠢᠰᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="207"/>
        <source>show blank screensaver immediately and if lock</source>
        <translation>ᠳᠠᠷᠤᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠂ ᠬᠡᠷᠪᠡ ᠣᠨᠢᠰᠤᠯᠠᠪᠠᠯ</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="66"/>
        <source>Screensaver for ukui-screensaver</source>
        <translation>ukui—screensaver ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="70"/>
        <source>show on root window</source>
        <translation>ᠦᠨᠳᠦᠰᠦ ᠴᠣᠩᠬᠣᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠠᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="72"/>
        <source>show on window.</source>
        <translation>ᠴᠣᠩᠬᠣᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="73"/>
        <source>window id</source>
        <translation>ᠴᠣᠩᠬᠣᠨ᠎ᠤ᠋ id</translation>
    </message>
</context>
</TS>
