<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AgreementWindow</name>
    <message>
        <location filename="../src/agreementwindow.cpp" line="32"/>
        <source>I know</source>
        <translation>我知道</translation>
    </message>
</context>
<context>
    <name>AuthDialog</name>
    <message>
        <source>More Devices</source>
        <translation type="obsolete">选择其他设备</translation>
    </message>
    <message>
        <source>Biometric</source>
        <translation type="obsolete">使用生物识别认证</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">使用密码认证</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="922"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <source>UnLock</source>
        <translation type="obsolete">解锁</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>LoggedIn</source>
        <translation type="obsolete">已登录</translation>
    </message>
    <message>
        <source>Password: </source>
        <translation type="vanished">密码：</translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">账户锁定%1分钟由于%2次错误尝试</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="337"/>
        <location filename="../src/authdialog.cpp" line="338"/>
        <location filename="../src/authdialog.cpp" line="406"/>
        <location filename="../src/authdialog.cpp" line="407"/>
        <source>Please try again in %1 minutes.</source>
        <translation>請在 %1 分鐘後重試。</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="347"/>
        <location filename="../src/authdialog.cpp" line="348"/>
        <location filename="../src/authdialog.cpp" line="415"/>
        <location filename="../src/authdialog.cpp" line="416"/>
        <source>Please try again in %1 seconds.</source>
        <translation>請在 %1 秒後重試。</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="357"/>
        <location filename="../src/authdialog.cpp" line="358"/>
        <location filename="../src/authdialog.cpp" line="424"/>
        <location filename="../src/authdialog.cpp" line="425"/>
        <source>Account locked permanently.</source>
        <translation>帳戶永久鎖定。</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="630"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>驗證人臉識別或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="635"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>按指紋或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="640"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>驗證聲紋或輸入密碼以解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="645"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>驗證指靜脈或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="650"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>驗證虹膜或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="826"/>
        <source>Input Password</source>
        <translation>輸入密碼</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1226"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>驗證 %1 失敗，請輸入密碼進行解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1230"/>
        <location filename="../src/authdialog.cpp" line="1232"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>無法驗證 %1，請輸入密碼進行解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1262"/>
        <source>Abnormal network</source>
        <translation>網路異常</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="724"/>
        <location filename="../src/authdialog.cpp" line="725"/>
        <source>Password cannot be empty</source>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="1245"/>
        <location filename="../src/authdialog.cpp" line="1249"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>驗證 %1 失敗，您仍有 %2 次驗證機會</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="vanished">密码错误，请重试</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">认证失败，还剩%1次尝试机会</translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="869"/>
        <source>Authentication failure, Please try again</source>
        <translation>身份驗證失敗，請重試</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="655"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>使用綁定的微信掃碼或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="227"/>
        <source>Enter the ukey password</source>
        <translation>輸入ukey密碼</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="244"/>
        <source>Insert the ukey into the USB port</source>
        <translation>將 Ukey 插入 USB 埠</translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="824"/>
        <source>Password </source>
        <translation>密碼 </translation>
    </message>
    <message>
        <location filename="../src/authdialog.cpp" line="905"/>
        <source>Login</source>
        <translation>登錄</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">指纹验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>Fingerprint authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">指纹验证失败，您还有%1次尝试机会</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
</context>
<context>
    <name>BioDevicesWidget</name>
    <message>
        <source>Please select other biometric devices</source>
        <translation type="obsolete">请选择其他生物识别设备</translation>
    </message>
    <message>
        <source>Device Type:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation type="obsolete">设备名称：</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="119"/>
        <source>Current device: </source>
        <translation>目前裝置： </translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="185"/>
        <source>Identify failed, Please retry.</source>
        <translation>識別失敗，請重試。</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="48"/>
        <source>Please select the biometric device</source>
        <translation>請選擇生物識別設備</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="53"/>
        <source>Device type:</source>
        <translation>裝置類型：</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="69"/>
        <source>Device name:</source>
        <translation>裝置名稱：</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="79"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charsmorewidget.cpp" line="183"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="114"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="128"/>
        <source>ABC</source>
        <translation>美國廣播公司</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="141"/>
        <source>123</source>
        <translation>123</translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="38"/>
        <source>edit network</source>
        <translation>編輯網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="116"/>
        <source>LAN name: </source>
        <translation>區域網名稱： </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="117"/>
        <source>Method: </source>
        <translation>方法： </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="118"/>
        <source>Address: </source>
        <translation>位址： </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="119"/>
        <source>Netmask: </source>
        <translation>網路遮罩： </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="120"/>
        <source>Gateway: </source>
        <translation>閘道： </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="121"/>
        <source>DNS 1: </source>
        <translation>網域名稱系統 1： </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="122"/>
        <source>DNS 2: </source>
        <translation>網域名稱系統 2： </translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="124"/>
        <source>Edit Conn</source>
        <translation>編輯康恩</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="125"/>
        <location filename="../KylinNM/src/confform.cpp" line="127"/>
        <source>Auto(DHCP)</source>
        <translation>自動（DHCP）</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="126"/>
        <location filename="../KylinNM/src/confform.cpp" line="128"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="158"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="159"/>
        <source>Save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="160"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="301"/>
        <source>Can not create new wired network for without wired card</source>
        <translation>沒有有線卡就無法創建新的有線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="318"/>
        <source>New network already created</source>
        <translation>已創建新網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="362"/>
        <source>New network settings already finished</source>
        <translation>新的網路設置已完成</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="395"/>
        <source>Edit Network</source>
        <translation>編輯網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="431"/>
        <source>Add Wired Network</source>
        <translation>添加有線網路</translation>
    </message>
    <message>
        <source>create wired network successfully</source>
        <translation type="obsolete">已创建新的有线网络</translation>
    </message>
    <message>
        <source>change configuration of wired network successfully</source>
        <translation type="obsolete">新的设置已经生效</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/confform.cpp" line="371"/>
        <source>New settings already effective</source>
        <translation>新設置已生效</translation>
    </message>
    <message>
        <source>There is a same named LAN exsits.</source>
        <translation type="obsolete">已有同名连接存在</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="61"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="63"/>
        <source>FingerVein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="65"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="67"/>
        <source>Face</source>
        <translation>臉</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="69"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="71"/>
        <source>ukey</source>
        <translation>尤基</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="73"/>
        <source>QRCode</source>
        <translation>微信掃碼</translation>
    </message>
</context>
<context>
    <name>DigitalAuthDialog</name>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="57"/>
        <location filename="../src/digitalauthdialog.cpp" line="759"/>
        <source>LoginByUEdu</source>
        <translation>LoginByUEdu</translation>
    </message>
    <message>
        <source>now is authing, wait a moment</source>
        <translation type="vanished">认证中，请稍后</translation>
    </message>
    <message>
        <source>Password Incorrect, Please try again</source>
        <translation type="obsolete">密码错误，请重试</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="61"/>
        <source>ResetPWD?</source>
        <translation>重置PWD？</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="87"/>
        <location filename="../src/digitalauthdialog.cpp" line="776"/>
        <source>SetNewUEduPWD</source>
        <translation>設置新UEduPWD</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="599"/>
        <source>ConfirmNewUEduPWD</source>
        <translation>確認新UEduPWD</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="611"/>
        <source>The two password entries are inconsistent, please reset</source>
        <translation>兩個密碼條目不一致，請重置</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="700"/>
        <source>Password entered incorrectly, please try again</source>
        <translation>密碼輸入錯誤，請重試</translation>
    </message>
    <message>
        <location filename="../src/digitalauthdialog.cpp" line="224"/>
        <source>clear</source>
        <translation>清楚</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="76"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="77"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="78"/>
        <source>Wi-Fi name</source>
        <translation>無線網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="79"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="80"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="81"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="104"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.cpp" line="105"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA 及 WPA2 企业</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifi.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="68"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="69"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="70"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="71"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="72"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="73"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="74"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="75"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="77"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="95"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="96"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="97"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="98"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="100"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifileap.cpp" line="101"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="83"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="84"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="85"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="86"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="87"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="88"/>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="89"/>
        <source>Allow automatic PAC pro_visioning</source>
        <translation>允許自動 PAC pro_visioning</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="90"/>
        <source>PAC file</source>
        <translation>PAC 檔</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="91"/>
        <source>Inner authentication</source>
        <translation>內部身份驗證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="92"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="93"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="94"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="95"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="100"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="118"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="148"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="119"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="120"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="121"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="123"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="124"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="136"/>
        <source>Tunneled TLS</source>
        <translation>隧道式紅綠燈系統</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="137"/>
        <source>Protected EAP (PEAP)</source>
        <translation>受保護的 EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="143"/>
        <source>Anonymous</source>
        <translation>匿名</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="144"/>
        <source>Authenticated</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecfast.cpp" line="145"/>
        <source>Both</source>
        <translation>雙</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="74"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="75"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="77"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="78"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="79"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="80"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="81"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="101"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="119"/>
        <source>Tunneled TLS</source>
        <translation>隧道式紅綠燈系統</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecleap.cpp" line="120"/>
        <source>Protected EAP (PEAP)</source>
        <translation>受保護的 EAP （PEAP）</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="91"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="92"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="93"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="94"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="95"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="96"/>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="97"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="98"/>
        <source>CA certificate</source>
        <translation>CA 證書</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="99"/>
        <source>CA certificate password</source>
        <translation>CA 憑證密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="100"/>
        <source>No CA certificate is required</source>
        <translation>不需要 CA 證書</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="101"/>
        <source>PEAP version</source>
        <translation>PEAP 版本</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="102"/>
        <source>Inner authentication</source>
        <translation>內部身份驗證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="103"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="104"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="105"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="106"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="126"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="149"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="127"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="128"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="129"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="131"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="132"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="144"/>
        <source>Tunneled TLS</source>
        <translation>隧道式紅綠燈系統</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="145"/>
        <source>Protected EAP (PEAP)</source>
        <translation>受保護的 EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="150"/>
        <source>Choose from file</source>
        <translation>從檔案中選擇</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="153"/>
        <source>Automatic</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="154"/>
        <source>Version 0</source>
        <translation>版本 0</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpeap.cpp" line="155"/>
        <source>Version 1</source>
        <translation>版本 1</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="74"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="75"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="76"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="77"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="78"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="79"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="80"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="81"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="82"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="84"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="102"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="103"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="104"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="105"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="107"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="108"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="120"/>
        <source>Tunneled TLS</source>
        <translation>隧道式紅綠燈系統</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisecpwd.cpp" line="121"/>
        <source>Protected EAP (PEAP)</source>
        <translation>受保護的 EAP （PEAP）</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="90"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="91"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="92"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="93"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="94"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="95"/>
        <source>Identity</source>
        <translation>身份</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="96"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="97"/>
        <source>CA certificate</source>
        <translation>CA 證書</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="98"/>
        <source>CA certificate password</source>
        <translation>CA 憑證密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="99"/>
        <source>No CA certificate is required</source>
        <translation>不需要 CA 證書</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="100"/>
        <source>User certificate</source>
        <translation>用戶證書</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="101"/>
        <source>User certificate password</source>
        <translation>使用者證書密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="102"/>
        <source>User private key</source>
        <translation>使用者私鑰</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="103"/>
        <source>User key password</source>
        <translation>用戶金鑰密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="104"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="105"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="107"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="125"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="148"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="152"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="156"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="126"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="127"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="128"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="130"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="131"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="143"/>
        <source>Tunneled TLS</source>
        <translation>隧道式紅綠燈系統</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="144"/>
        <source>Protected EAP (PEAP)</source>
        <translation>受保護的 EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="149"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="153"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectls.cpp" line="157"/>
        <source>Choose from file</source>
        <translation>從檔案中選擇</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="89"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="90"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="91"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="92"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="93"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="94"/>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="95"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="96"/>
        <source>CA certificate</source>
        <translation>CA 證書</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="97"/>
        <source>CA certificate password</source>
        <translation>CA 憑證密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="98"/>
        <source>No CA certificate is required</source>
        <translation>不需要 CA 證書</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="99"/>
        <source>Inner authentication</source>
        <translation>內部身份驗證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="100"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="101"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="102"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="103"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="105"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="123"/>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="146"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="124"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="125"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="126"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="128"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="129"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="141"/>
        <source>Tunneled TLS</source>
        <translation>隧道式紅綠燈系統</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="142"/>
        <source>Protected EAP (PEAP)</source>
        <translation>受保護的 EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifisectunneltls.cpp" line="147"/>
        <source>Choose from file</source>
        <translation>從檔案中選擇</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="74"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="75"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="77"/>
        <source>Key</source>
        <translation>鑰匙</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="78"/>
        <source>WEP index</source>
        <translation>WEP指數</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="79"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="80"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="81"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="101"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128 位金鑰（十六進位或 ASCII）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128 位密碼短語</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態 WEP （802.1X）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="115"/>
        <source>1(default)</source>
        <translation>1（預設）</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="121"/>
        <source>Open System</source>
        <translation>開放系統</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwep.cpp" line="122"/>
        <source>Shared Key</source>
        <translation>共用金鑰</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="82"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>添加隱藏的無線網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="83"/>
        <source>Connection</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="84"/>
        <source>Wi-Fi name</source>
        <translation>無線網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="85"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="86"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="87"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="88"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="90"/>
        <source>C_reate…</source>
        <translation>C_reate...…</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="113"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/wireless-security/dlgconnhidwifiwpa.cpp" line="114"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128 位密钥(十六进制或ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128 位密码句</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">动态 WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA 及 WPA2 企业</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.ui" line="14"/>
        <source>Dialog</source>
        <translation>對話</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="46"/>
        <source>Create Hotspot</source>
        <translation>創建熱點</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="47"/>
        <source>Network name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="48"/>
        <source>Wi-Fi security</source>
        <translation>無線網路安全</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="49"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="50"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="51"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="54"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/hot-spot/dlghotspotcreate.cpp" line="55"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 和 WPA2 個人</translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../src/iconedit.cpp" line="114"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
</context>
<context>
    <name>InputInfos</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="338"/>
        <source>Service exception...</source>
        <translation>服務異常...</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="341"/>
        <source>Invaild parameters...</source>
        <translation>不合法參數...</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="344"/>
        <source>Unknown fault:%1</source>
        <translation>未知故障：%1</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="288"/>
        <source>Recapture(60s)</source>
        <translation>奪回（60年代）</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="312"/>
        <source>Recapture(%1s)</source>
        <translation>重新擷取（%1 秒）</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="141"/>
        <location filename="../src/verificationwidget.cpp" line="318"/>
        <source>Get code</source>
        <translation>獲取代碼</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">麒麟网络工具信息提示</translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <location filename="../KylinNM/src/kylinnm.ui" line="14"/>
        <source>kylin-nm</source>
        <translation>麒麟-納米</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="413"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="417"/>
        <source>LAN</source>
        <translation>局域網</translation>
    </message>
    <message>
        <source>Enabel LAN List</source>
        <translation type="obsolete">其他有线网络</translation>
    </message>
    <message>
        <source>WiFi</source>
        <translation type="obsolete">无线网络</translation>
    </message>
    <message>
        <source>Enabel WiFi List</source>
        <translation type="obsolete">其他无线网络</translation>
    </message>
    <message>
        <source>New WiFi</source>
        <translation type="obsolete">加入其他网络</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="456"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="610"/>
        <source>Advanced</source>
        <translation>高深</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Connect Hide Network</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="vanished">已开启</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">已关闭</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="438"/>
        <source>HotSpot</source>
        <translation>熱點</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="447"/>
        <source>FlyMode</source>
        <translation>飛行模式</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="vanished">显示网络连接界面</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="293"/>
        <source>Inactivated LAN</source>
        <translation>停用的局域網</translation>
    </message>
    <message>
        <source>Inactivated WLAN</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="317"/>
        <source>Other WLAN</source>
        <translation>其他無線局域網</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="423"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="427"/>
        <source>WLAN</source>
        <translation>無線局域網</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="609"/>
        <source>Show KylinNM</source>
        <translation>顯示麒麟NM</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1279"/>
        <source>No wireless card detected</source>
        <translation>未檢測到無線網卡</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1316"/>
        <source>Activated LAN</source>
        <translation>啟動的局域網</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1379"/>
        <source>Activated WLAN</source>
        <translation>啟動的無線局域網</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1423"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1522"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1694"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2419"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2510"/>
        <source>Not connected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1426"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1524"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1594"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1595"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1697"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1821"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1988"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2421"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2512"/>
        <source>Disconnected</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1620"/>
        <source>No Other Wired Network Scheme</source>
        <translation>沒有其他有線網路方案</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">编辑</translation>
    </message>
    <message>
        <source>Done</source>
        <translation type="obsolete">完成</translation>
    </message>
    <message>
        <source>No wifi connected.</source>
        <translation type="obsolete">未连接任何网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1842"/>
        <source>No Other Wireless Network Scheme</source>
        <translation>無其他無線網路方案</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2328"/>
        <source>Wired net is disconnected</source>
        <translation>有線網路已斷開連接</translation>
    </message>
    <message>
        <source>Wi-Fi is disconnected</source>
        <translation type="obsolete">断开无线网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2741"/>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation>確認您的Wi-Fi密碼或可用的無線網卡</translation>
    </message>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">其他有线网络</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="obsolete">新建有线网络</translation>
    </message>
    <message>
        <source>Hide WiFi</source>
        <translation type="vanished">加入网络</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="359"/>
        <source>No usable network in the list</source>
        <translation>清單中沒有可用的網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1575"/>
        <location filename="../KylinNM/src/kylinnm.cpp" line="1791"/>
        <source>NetOn,</source>
        <translation>內頓，</translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">其他无线网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>keep wired network switch is on before turning on wireless switch</source>
        <translation type="vanished">打开无线网开关前保持有线网开关打开</translation>
    </message>
    <message>
        <source>please insert the wireless network adapter</source>
        <translation type="vanished">请先插入无线网卡</translation>
    </message>
    <message>
        <source>Abnormal connection exist, program will delete it</source>
        <translation type="vanished">正在断开异常连接的网络</translation>
    </message>
    <message>
        <source>update Wi-Fi list now, click again</source>
        <translation type="vanished">正在更新 Wi-Fi列表 请再次点击</translation>
    </message>
    <message>
        <source>update Wi-Fi list now</source>
        <translation type="vanished">正在更新 Wi-Fi列表</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2695"/>
        <source>Conn Ethernet Success</source>
        <translation>連接乙太網成功案例</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2707"/>
        <source>Conn Ethernet Fail</source>
        <translation>連接乙太網故障</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/kylinnm.cpp" line="2732"/>
        <source>Conn Wifi Success</source>
        <translation>連接無線成功</translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="168"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="182"/>
        <source>123</source>
        <translation>123</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="196"/>
        <source>Ctrl</source>
        <translation>按</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="224"/>
        <source>Alt</source>
        <translation>替代項</translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <location filename="../src/lockwidget.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/lockwidget.ui" line="58"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.ui" line="51"/>
        <source>Time</source>
        <translation>時間</translation>
    </message>
    <message>
        <source>Guest</source>
        <translation type="vanished">游客</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation>切換使用者</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="643"/>
        <source>Power</source>
        <translation>權力</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="691"/>
        <location filename="../src/lockwidget.cpp" line="719"/>
        <source>VirtualKeyboard</source>
        <translation>虛擬鍵盤</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="918"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation>多個用戶同時登錄。是否確實要重新啟動此系統？</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="1133"/>
        <source>LAN</source>
        <translation>局域網</translation>
    </message>
    <message>
        <location filename="../src/lockwidget.cpp" line="1135"/>
        <source>WLAN</source>
        <translation>無線局域網</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="72"/>
        <source>Login Options</source>
        <translation>登錄選項</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="288"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="314"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../src/loginoptionswidget.cpp" line="727"/>
        <source>Identify device removed!</source>
        <translation>識別已刪除的設備！</translation>
    </message>
</context>
<context>
    <name>MyLineEdit</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="599"/>
        <location filename="../src/verificationwidget.cpp" line="605"/>
        <location filename="../src/verificationwidget.cpp" line="622"/>
        <source>Verification code</source>
        <translation>驗證碼</translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="160"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="174"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../KylinNM/src/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="158"/>
        <source>Automatically join the network</source>
        <translation>自動加入網路</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation type="vanished">输入密码</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="42"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="43"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="44"/>
        <location filename="../KylinNM/src/oneconnform.cpp" line="46"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="45"/>
        <source>Disconnect</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="47"/>
        <source>Input Password...</source>
        <translation>輸入密碼...</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="419"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏的Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="555"/>
        <source>Signal：</source>
        <translation>信號：</translation>
    </message>
    <message>
        <source>Public</source>
        <translation type="vanished">开放</translation>
    </message>
    <message>
        <source>Safe</source>
        <translation type="vanished">安全</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="vanished">速率</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="552"/>
        <source>None</source>
        <translation>沒有</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="554"/>
        <source>WiFi Security：</source>
        <translation>無線安全：</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="556"/>
        <source>MAC：</source>
        <translation>MAC：</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/oneconnform.cpp" line="833"/>
        <source>Conn Wifi Failed</source>
        <translation>連接無線上網失敗</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../KylinNM/src/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="31"/>
        <location filename="../KylinNM/src/onelancform.cpp" line="32"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="34"/>
        <source>Disconnect</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="289"/>
        <location filename="../KylinNM/src/onelancform.cpp" line="293"/>
        <source>No Configuration</source>
        <translation>無配置</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="296"/>
        <source>IPv4：</source>
        <translation>IPv4：</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="297"/>
        <source>IPv6：</source>
        <translation>IPv6：</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="298"/>
        <source>BandWidth：</source>
        <translation>頻寬：</translation>
    </message>
    <message>
        <location filename="../KylinNM/src/onelancform.cpp" line="299"/>
        <source>MAC：</source>
        <translation>MAC：</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">自动</translation>
    </message>
</context>
<context>
    <name>PhoneAuthWidget</name>
    <message>
        <location filename="../src/permissioncheck.cpp" line="236"/>
        <location filename="../src/verificationwidget.cpp" line="375"/>
        <source>Verification by phoneNum</source>
        <translation>通過電話驗證數位</translation>
    </message>
    <message>
        <location filename="../src/permissioncheck.cpp" line="241"/>
        <source>「 Use bound Phone number to verification 」</source>
        <translation>「使用綁定的電話號碼進行驗證」</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="381"/>
        <source>「 Use SMS to verification 」</source>
        <translation>「 使用簡訊進行驗證 」</translation>
    </message>
    <message>
        <location filename="../src/permissioncheck.cpp" line="259"/>
        <location filename="../src/verificationwidget.cpp" line="399"/>
        <source>commit</source>
        <translation>犯</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="484"/>
        <location filename="../src/verificationwidget.cpp" line="524"/>
        <source>Network not connected~</source>
        <translation>網路未連接~</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="530"/>
        <source>Network unavailable~</source>
        <translation>網路不可用~</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="487"/>
        <source>Verification Code invalid!</source>
        <translation>驗證碼無效！</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="490"/>
        <source>Verification Code incorrect.Please retry!</source>
        <translation>驗證碼不正確。請重試！</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="493"/>
        <source>Failed time over limit!Retry after 1 hour!</source>
        <translation>失敗時間超過限制！1 小時後重試！</translation>
    </message>
    <message>
        <location filename="../src/verificationwidget.cpp" line="497"/>
        <source>verifaction failed!</source>
        <translation>驗證失敗！</translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <location filename="../src/powermanager.cpp" line="292"/>
        <source>lock</source>
        <translation>鎖</translation>
    </message>
    <message>
        <source>SwitchUser</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>logout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>reboot</source>
        <translation type="vanished">重启</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="309"/>
        <source>Log Out</source>
        <translation>註銷</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="327"/>
        <location filename="../src/powermanager.cpp" line="666"/>
        <source>Restart</source>
        <translation>重新啟動</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="347"/>
        <source>Power Off</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="664"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation>關閉所有應用，關閉計算機，然後重新打開計算機</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="686"/>
        <source>Close all apps, and then shut down your computer</source>
        <translation>關閉所有應用，然後關閉電腦</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="689"/>
        <source>Shut Down</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="717"/>
        <source>Turn off your computer, but the app stays open. When the computer is turned on, it can be restored to the state you left</source>
        <translation>關閉電腦，但應用會保持打開狀態。當計算機打開時，它可以恢復到您離開時的狀態</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="719"/>
        <source>Hibernate</source>
        <translation>冬眠</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="747"/>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation>計算機保持打開狀態，但消耗的電量更少。該應用程式保持打開狀態，可以快速喚醒並恢復到上次中斷的位置</translation>
    </message>
    <message>
        <location filename="../src/powermanager.cpp" line="750"/>
        <source>Suspend</source>
        <translation>暫停</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="90"/>
        <source>The screensaver is active.</source>
        <translation>屏幕保護程式處於活動狀態。</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="92"/>
        <source>The screensaver is inactive.</source>
        <translation>屏幕保護程式處於非活動狀態。</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <source>exit(Esc)</source>
        <translation type="vanished">退出(Esc)</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="165"/>
        <source>Picture does not exist</source>
        <translation>圖片不存在</translation>
    </message>
    <message>
        <source>Set as desktop wallpaper</source>
        <translation type="vanished">设置为桌面壁纸</translation>
    </message>
    <message>
        <source>Automatic switching</source>
        <translation type="vanished">自动切换</translation>
    </message>
    <message>
        <source>You have %1 unread message</source>
        <translation type="obsolete">您有%1条未读消息</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="1469"/>
        <source>You have new notification</source>
        <translation>您有新通知</translation>
    </message>
    <message>
        <location filename="../screensaver/screensaver.cpp" line="1300"/>
        <source>View</source>
        <translation>視圖</translation>
    </message>
</context>
<context>
    <name>SleepTime</name>
    <message>
        <source>You have rested:</source>
        <translation type="vanished">您已休息:</translation>
    </message>
    <message>
        <location filename="../screensaver/sleeptime.cpp" line="72"/>
        <source>You have rested</source>
        <translation>你已經休息了</translation>
    </message>
</context>
<context>
    <name>SureWindow</name>
    <message>
        <location filename="../src/surewindow.ui" line="14"/>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="56"/>
        <source>TextLabel</source>
        <translation>文本標籤</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="157"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/surewindow.ui" line="176"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="vanished">同时有多个用户登录系统，您确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="53"/>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>以下程式正在運行以防止系統掛起！</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="56"/>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>以下程式正在運行以防止系統休眠！</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="50"/>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>以下程式正在運行以防止系統關閉！</translation>
    </message>
    <message>
        <location filename="../src/surewindow.cpp" line="47"/>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>以下程式正在運行以防止系統重新啟動！</translation>
    </message>
</context>
<context>
    <name>SwitchButton</name>
    <message>
        <source>login by password</source>
        <translation type="vanished">密码登录</translation>
    </message>
    <message>
        <source>login by qr code</source>
        <translation type="vanished">微信登录</translation>
    </message>
</context>
<context>
    <name>SwitchButtonGroup</name>
    <message>
        <location filename="../src/switchbuttongroup.cpp" line="35"/>
        <source>uEduPWD</source>
        <translation>uEduPWD</translation>
    </message>
    <message>
        <location filename="../src/switchbuttongroup.cpp" line="36"/>
        <source>Wechat</source>
        <translation>微信</translation>
    </message>
</context>
<context>
    <name>TabletLockWidget</name>
    <message>
        <source>You have %1 unread message</source>
        <translation type="vanished">您有%1条未读消息</translation>
    </message>
    <message>
        <source>Slide to unlock</source>
        <translation type="vanished">向上滑动解锁</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="275"/>
        <source>New password is the same as old</source>
        <translation>新密碼與舊密碼相同</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="286"/>
        <source>Reset password error:%1</source>
        <translation>重置密碼錯誤：%1</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="298"/>
        <source>Please scan by correct WeChat</source>
        <translation>請通過正確的微信掃描</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="189"/>
        <location filename="../src/tabletlockwidget.cpp" line="209"/>
        <location filename="../src/tabletlockwidget.cpp" line="228"/>
        <location filename="../src/tabletlockwidget.cpp" line="243"/>
        <location filename="../src/tabletlockwidget.cpp" line="382"/>
        <location filename="../src/tabletlockwidget.cpp" line="397"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="195"/>
        <location filename="../src/tabletlockwidget.cpp" line="407"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../src/tabletlockwidget.cpp" line="248"/>
        <source>Skip</source>
        <translation>跳</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../KylinNM/src/utils.cpp" line="87"/>
        <source>kylin network applet desktop message</source>
        <translation>麒麟網路小程式桌面留言</translation>
    </message>
</context>
<context>
    <name>VerificationWidget</name>
    <message>
        <location filename="../src/verificationwidget.cpp" line="55"/>
        <source>Please scan by bound WeChat</source>
        <translation>請通過綁定微信掃描</translation>
    </message>
</context>
<context>
    <name>VerticalVerificationWidget</name>
    <message>
        <location filename="../src/verticalVerificationwidget.cpp" line="52"/>
        <source>Please scan by bound WeChat</source>
        <translation>請通過綁定微信掃描</translation>
    </message>
</context>
<context>
    <name>WeChatAuthDialog</name>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="74"/>
        <location filename="../src/wechatauthdialog.cpp" line="136"/>
        <source>Login by wechat</source>
        <translation>微信登錄</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="78"/>
        <location filename="../src/wechatauthdialog.cpp" line="140"/>
        <source>Verification by wechat</source>
        <translation>微信驗證</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="75"/>
        <location filename="../src/wechatauthdialog.cpp" line="137"/>
        <source>「 Use registered WeChat account to login 」</source>
        <translation>「 使用已註冊的微信公眾號登入 」</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="79"/>
        <location filename="../src/wechatauthdialog.cpp" line="141"/>
        <source>「 Use bound WeChat account to verification 」</source>
        <translation>「 使用綁定的微信帳號進行驗證 」</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="128"/>
        <location filename="../src/wechatauthdialog.cpp" line="183"/>
        <source>Network not connected~</source>
        <translation>網路未連接~</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="227"/>
        <source>Scan code successfully</source>
        <translation>掃碼成功</translation>
    </message>
    <message>
        <location filename="../src/wechatauthdialog.cpp" line="252"/>
        <source>Timeout!Try again!</source>
        <translation>超時！再試一次！</translation>
    </message>
    <message>
        <source>Login failed</source>
        <translation type="vanished">登录失败</translation>
    </message>
</context>
<context>
    <name>delay</name>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="179"/>
        <source>how long to show lock</source>
        <translation>顯示鎖定多長時間</translation>
    </message>
</context>
<context>
    <name>has-lock</name>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="182"/>
        <source>if show lock</source>
        <translation>如果顯示鎖定</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="43"/>
        <source>Start command for the ukui ScreenSaver.</source>
        <translation>uui 螢幕保護程式的啟動命令。</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="48"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="166"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="168"/>
        <source>lock the screen immediately</source>
        <translation>立即鎖定螢幕</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="51"/>
        <source>query the status of the screen saver</source>
        <translation>查詢屏幕保護程序的狀態</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="54"/>
        <source>unlock the screen saver</source>
        <translation>解鎖螢幕保護程式</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="56"/>
        <source>show the screensaver</source>
        <translation>顯示螢幕保護程式</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-command.cpp" line="58"/>
        <source>show blank and delay to lock,param:idle/lid/lowpower</source>
        <translation>顯示空白和延遲鎖定，參數：空閒/蓋子/低功耗</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="160"/>
        <source>Dialog for the ukui ScreenSaver.</source>
        <translation>ukui 螢幕保護程序的對話框。</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="170"/>
        <source>activated by session idle signal</source>
        <translation>由會話空閒信號啟動</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="172"/>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="176"/>
        <source>lock the screen and show screensaver immediately</source>
        <translation>鎖定螢幕並立即顯示螢幕保護程式</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="174"/>
        <source>show screensaver immediately</source>
        <translation>立即顯示螢幕保護程式</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="178"/>
        <source>show blank screensaver immediately and delay time to show lock</source>
        <translation>立即顯示空白螢幕保護程式並延遲顯示鎖定的時間</translation>
    </message>
    <message>
        <location filename="../src/ukui-screensaver-dialog.cpp" line="181"/>
        <source>show blank screensaver immediately and if lock</source>
        <translation>立即顯示空白螢幕保護程式，如果鎖定</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="66"/>
        <source>Screensaver for ukui-screensaver</source>
        <translation>ukui螢幕保護程式的螢幕保護程式</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="70"/>
        <source>show on root window</source>
        <translation>在根視窗上顯示</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="72"/>
        <source>show on window.</source>
        <translation>在視窗上顯示。</translation>
    </message>
    <message>
        <location filename="../screensaver/main.cpp" line="73"/>
        <source>window id</source>
        <translation>窗口標識</translation>
    </message>
</context>
</TS>
