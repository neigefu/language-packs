<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Kylin-camera</source>
        <translation type="vanished">麒麟攝像頭</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">攝像頭</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="vanished">攝像頭</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">版本号：</translation>
    </message>
    <message>
        <source>Camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation type="vanished">攝像頭是一款很专业的攝像頭软件。有着简单易用的特点。可以实现拍照录像以及相册显示的功能</translation>
    </message>
    <message>
        <source>privacy statement</source>
        <translation type="vanished">隐私声明</translation>
    </message>
    <message>
        <source>Kylin camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation type="vanished">麒麟攝像頭是一款很专业的攝像頭软件。有着简单易用的特点。可以实现拍照录像以及相册显示的功能</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队: </translation>
    </message>
    <message>
        <source>support</source>
        <translation type="vanished">支持</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="../src/button.cpp" line="240"/>
        <source>capture mode</source>
        <translation>拍照模式</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="241"/>
        <source>video mode</source>
        <translation>視頻模式</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="242"/>
        <location filename="../src/button.cpp" line="243"/>
        <source>cheese</source>
        <translation>拍照</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="244"/>
        <source>video</source>
        <translation>視頻</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="245"/>
        <source>stop</source>
        <translation>停</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="246"/>
        <source>album</source>
        <translation>影集</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="247"/>
        <location filename="../src/button.cpp" line="248"/>
        <source>delay</source>
        <translation>延遲</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="249"/>
        <source>mirror</source>
        <translation>鏡子</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="250"/>
        <location filename="../src/button.cpp" line="251"/>
        <source>grid</source>
        <translation>網 格</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="542"/>
        <source>waring</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="542"/>
        <source>path does not exist, please set storage path</source>
        <translation>路徑不存在，請設置存儲路徑</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="554"/>
        <source>open File</source>
        <translation>打開檔</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="555"/>
        <source>open Dir</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <source>filter</source>
        <translation type="vanished">滤镜</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="252"/>
        <source>seiral shoot</source>
        <translation>塞拉爾拍攝</translation>
    </message>
</context>
<context>
    <name>CameraPage</name>
    <message>
        <source>No devices were found</source>
        <translation type="vanished">未发现设备</translation>
    </message>
    <message>
        <source>No device were found</source>
        <translation type="obsolete">无攝像頭可用</translation>
    </message>
    <message>
        <source>Please connect the camera first</source>
        <translation type="obsolete">若要使用此功能，请先连接攝像頭。</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3秒</translation>
    </message>
    <message>
        <source>6s</source>
        <translation type="vanished">6秒</translation>
    </message>
    <message>
        <source>9s</source>
        <translation type="vanished">9秒</translation>
    </message>
    <message>
        <source>waring</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <source>save path can&apos;t write</source>
        <translation type="obsolete">储存路径没有写如权限</translation>
    </message>
    <message>
        <source>File generation in progress, please try again later</source>
        <translation type="obsolete">文件正在保存，请稍后操作</translation>
    </message>
    <message>
        <source>path does not exist, save to default path</source>
        <translation type="obsolete">路径不存在，文件保存到默认路径</translation>
    </message>
</context>
<context>
    <name>CameraTopPage</name>
    <message>
        <location filename="../src/cameratoppage.cpp" line="41"/>
        <source>No device were found</source>
        <translation>未找到設備</translation>
    </message>
    <message>
        <location filename="../src/cameratoppage.cpp" line="42"/>
        <source>Please connect the camera first and app will continuously retrieve avaliable devices for you</source>
        <translation>請先連接相機，應用程式將持續為您檢索可用設備</translation>
    </message>
    <message>
        <location filename="../src/cameratoppage.cpp" line="45"/>
        <source>not support current device</source>
        <translation>不支援當前設備</translation>
    </message>
    <message>
        <location filename="../src/cameratoppage.cpp" line="46"/>
        <source>Please check the camera or switch to avaliable device</source>
        <translation>請檢查相機或切換到可用設備</translation>
    </message>
</context>
<context>
    <name>DelayWdiget</name>
    <message>
        <location filename="../src/delaywdiget.cpp" line="56"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="57"/>
        <source>3s</source>
        <translation>3秒</translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="58"/>
        <source>6s</source>
        <translation>6秒</translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="59"/>
        <source>9s</source>
        <translation>9秒</translation>
    </message>
</context>
<context>
    <name>DevicePull</name>
    <message>
        <location filename="../src/devicepull.cpp" line="57"/>
        <source>Device is pulled out</source>
        <translation>設備被拉出</translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="68"/>
        <source>Device is pulled in</source>
        <translation>設備被拉入</translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="111"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">麒麟相機</translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="114"/>
        <source>kylin camera message</source>
        <translation>麒麟相機留言</translation>
    </message>
</context>
<context>
    <name>HasDevicePage</name>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="392"/>
        <location filename="../src/hasdevicepage.cpp" line="449"/>
        <location filename="../src/hasdevicepage.cpp" line="1006"/>
        <source>camera is being used by kylin-camera</source>
        <translation>相機正在使用麒麟相機</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="398"/>
        <location filename="../src/hasdevicepage.cpp" line="432"/>
        <location filename="../src/hasdevicepage.cpp" line="473"/>
        <location filename="../src/hasdevicepage.cpp" line="581"/>
        <location filename="../src/hasdevicepage.cpp" line="742"/>
        <location filename="../src/hasdevicepage.cpp" line="753"/>
        <location filename="../src/hasdevicepage.cpp" line="839"/>
        <location filename="../src/hasdevicepage.cpp" line="999"/>
        <source>waring</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="398"/>
        <location filename="../src/hasdevicepage.cpp" line="999"/>
        <source>The camera is occupied, please check the use of the device!</source>
        <translation>相機被佔用，請檢查設備的使用方式！</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="432"/>
        <source>The camera is occupied or there is an exception in the target switching device, please check the device!</source>
        <translation>相機被佔用或目標切換設備有異常，請檢查設備！</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="473"/>
        <location filename="../src/hasdevicepage.cpp" line="742"/>
        <location filename="../src/hasdevicepage.cpp" line="839"/>
        <source>save path can&apos;t write</source>
        <translation>保存路徑無法寫入</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="581"/>
        <source>path does not exist, save to default path</source>
        <translation>路徑不存在，保存到預設路徑</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="753"/>
        <source>File generation in progress, please try again later</source>
        <translation>檔生成正在進行中，請稍後重試</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="1437"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
    <message>
        <source>3p</source>
        <translation type="vanished">3p</translation>
    </message>
    <message>
        <source>5p</source>
        <translation type="vanished">5p</translation>
    </message>
    <message>
        <source>10p</source>
        <translation type="vanished">10p</translation>
    </message>
    <message>
        <source>20p</source>
        <translation type="vanished">20p</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">麒麟相機</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="1440"/>
        <source>kylin camera message</source>
        <translation>麒麟相機留言</translation>
    </message>
    <message>
        <source>3pic</source>
        <translation type="vanished">3张</translation>
    </message>
    <message>
        <source>5pic</source>
        <translation type="vanished">5张</translation>
    </message>
    <message>
        <source>10pic</source>
        <translation type="vanished">10张</translation>
    </message>
    <message>
        <source>20pic</source>
        <translation type="vanished">20张</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">麒麟相機</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="177"/>
        <location filename="../src/mainwindow.cpp" line="412"/>
        <source>waring</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>maximum</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>normal</source>
        <translation type="vanished">還原</translation>
    </message>
    <message>
        <source>other user already open device!</source>
        <translation type="obsolete">其他用户正在使用攝像頭</translation>
    </message>
    <message>
        <source>path does not exist, save to default path</source>
        <translation type="obsolete">路径不存在，文件保存到默认路径</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="177"/>
        <source>path does not exist, please set storage path</source>
        <translation>路徑不存在，請設置存儲路徑</translation>
    </message>
    <message>
        <source>save path can&apos;t write</source>
        <translation type="vanished">储存路径没有写如权限</translation>
    </message>
    <message>
        <source>File generation in progress, please try again later</source>
        <translation type="obsolete">文件正在保存，请稍后操作</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Are you sure?!</source>
        <translation type="vanished">你确定要这样做？</translation>
    </message>
    <message>
        <source>no</source>
        <translation type="vanished">否</translation>
    </message>
    <message>
        <source>yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="412"/>
        <source>The camera is turned on and cannot be started again.</source>
        <translation>照相機已打開，無法再次啟動。</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>set</source>
        <translation type="vanished">設置</translation>
    </message>
    <message>
        <source>quit</source>
        <translation type="vanished">退出</translation>
    </message>
</context>
<context>
    <name>NPixCapWidget</name>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="62"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="63"/>
        <source>3p</source>
        <translation>3便士</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="64"/>
        <source>5p</source>
        <translation>5便士</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="65"/>
        <source>10p</source>
        <translation>302</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="66"/>
        <source>20p</source>
        <translation>002</translation>
    </message>
    <message>
        <source>3pic</source>
        <translation type="vanished">3张</translation>
    </message>
    <message>
        <source>5pic</source>
        <translation type="vanished">5张</translation>
    </message>
    <message>
        <source>10pic</source>
        <translation type="vanished">10张</translation>
    </message>
    <message>
        <source>20pic</source>
        <translation type="vanished">20张</translation>
    </message>
    <message>
        <source>3sheets</source>
        <translation type="vanished">3张</translation>
    </message>
    <message>
        <source>5sheets</source>
        <translation type="vanished">5张</translation>
    </message>
    <message>
        <source>10sheets</source>
        <translation type="vanished">10张</translation>
    </message>
    <message>
        <source>20sheets</source>
        <translation type="vanished">20张</translation>
    </message>
</context>
<context>
    <name>NoDevicePage</name>
    <message>
        <location filename="../src/nodevicepage.cpp" line="47"/>
        <source>No device were found</source>
        <translation>未找到設備</translation>
    </message>
    <message>
        <location filename="../src/nodevicepage.cpp" line="48"/>
        <source>Please connect the camera first and app will continuously retrieve avaliable devices for you</source>
        <translation>請先連接相機，應用程式將持續為您檢索可用設備</translation>
    </message>
    <message>
        <source>Please connect the camera first</source>
        <translation type="vanished">若要使用此功能，请先连接攝像頭。</translation>
    </message>
</context>
<context>
    <name>PictureViewPage</name>
    <message>
        <location filename="../src/pictureviewpage.cpp" line="17"/>
        <source>Album</source>
        <translation>影集</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="104"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
</context>
<context>
    <name>RecordModule</name>
    <message>
        <source>stop record</source>
        <translation type="vanished">结束录像</translation>
    </message>
    <message>
        <source>countinue record</source>
        <translation type="vanished">继续录像</translation>
    </message>
    <message>
        <source>pause record</source>
        <translation type="vanished">暂停录像</translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="11"/>
        <source>stop</source>
        <translation>停</translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="17"/>
        <source>countinue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="23"/>
        <source>pause</source>
        <translation>暫停</translation>
    </message>
</context>
<context>
    <name>Setting</name>
    <message>
        <source>Delayed shooting</source>
        <translation type="vanished">延時</translation>
    </message>
    <message>
        <source>Image mirroring</source>
        <translation type="vanished">鏡像</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">設置</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">主題</translation>
    </message>
    <message>
        <source>quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">説明</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="51"/>
        <source>Delayed</source>
        <translation>延遲</translation>
    </message>
    <message>
        <source>mirroring</source>
        <translation type="vanished">鏡像</translation>
    </message>
    <message>
        <source>set</source>
        <translation type="vanished">設置</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">大約</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">麒麟相機</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="142"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="143"/>
        <source>Camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation>相機是一個非常強大的相機軟體。它具有易於使用的特點。您可以實現拍照和顯示相簿的功能</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="67"/>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="35"/>
        <location filename="../src/setting.cpp" line="158"/>
        <source>Set</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="36"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="37"/>
        <location filename="../src/setting.cpp" line="136"/>
        <source>Help</source>
        <translation>幫助</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="38"/>
        <location filename="../src/setting.cpp" line="139"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="39"/>
        <location filename="../src/setting.cpp" line="180"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="71"/>
        <source>Light</source>
        <translation>淺色模式</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="75"/>
        <source>Dark</source>
        <translation>深色模式</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="140"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
    <message>
        <source>The image path</source>
        <translation type="vanished">圖像路徑</translation>
    </message>
    <message>
        <source>The image scale</source>
        <translation type="vanished">圖像尺寸</translation>
    </message>
    <message>
        <source>Camera device</source>
        <translation type="vanished">攝像頭设备</translation>
    </message>
    <message>
        <source>Sound device</source>
        <translation type="vanished">聲音设备</translation>
    </message>
    <message>
        <source>Select the directory</source>
        <translation type="vanished">选择目錄</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>have no permissions !?</source>
        <translation type="vanished">没有权限！？</translation>
    </message>
</context>
<context>
    <name>SettingPage</name>
    <message>
        <source>setting</source>
        <translation type="vanished">設置</translation>
    </message>
    <message>
        <source>Delayed shooting</source>
        <translation type="obsolete">延迟拍照</translation>
    </message>
    <message>
        <source>Image mirroring</source>
        <translation type="obsolete">圖像鏡像</translation>
    </message>
    <message>
        <source>The image path</source>
        <translation type="vanished">圖像路径</translation>
    </message>
    <message>
        <source>The image scale</source>
        <translation type="vanished">圖像分辨率</translation>
    </message>
    <message>
        <source>Camera device</source>
        <translation type="vanished">攝像頭设备</translation>
    </message>
    <message>
        <source>Sound device</source>
        <translation type="vanished">聲音设备</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="74"/>
        <source>confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="76"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>photo save format</source>
        <translation type="vanished">圖片保存格式</translation>
    </message>
    <message>
        <source>video save format</source>
        <translation type="vanished">录像保存格式</translation>
    </message>
    <message>
        <source>camera setting</source>
        <translation type="vanished">攝像頭設置</translation>
    </message>
    <message>
        <source>The image path:</source>
        <translation type="vanished">默认存储位置:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="36"/>
        <source>browse</source>
        <translation>流覽</translation>
    </message>
    <message>
        <source>Camera device:</source>
        <translation type="vanished">默认攝像頭:</translation>
    </message>
    <message>
        <source>save photo format:</source>
        <translation type="vanished">照片格式:</translation>
    </message>
    <message>
        <source>save video format:</source>
        <translation type="vanished">视频格式:</translation>
    </message>
    <message>
        <source>The image scale:</source>
        <translation type="vanished">拍摄分辨率:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="37"/>
        <source>image path:</source>
        <translation>影像路徑：</translation>
    </message>
    <message>
        <source>video path:</source>
        <translation type="vanished">视频存储路径：</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="46"/>
        <source>device:</source>
        <translation>裝置：</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="55"/>
        <source>photo format:</source>
        <translation>照片格式：</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="62"/>
        <source>video format:</source>
        <translation>視訊格式：</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="69"/>
        <source>scale:</source>
        <translation>規模：</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="263"/>
        <location filename="../src/settingpage.cpp" line="282"/>
        <source>Select the directory</source>
        <translation>選擇目錄</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="269"/>
        <location filename="../src/settingpage.cpp" line="288"/>
        <source>error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="269"/>
        <location filename="../src/settingpage.cpp" line="288"/>
        <source>The directory does not have write permissions, select the user directory to store the files .</source>
        <translation>該目錄沒有寫入許可權，請選擇用於儲存檔案的用戶目錄。</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="313"/>
        <location filename="../src/settingpage.cpp" line="315"/>
        <source>waring</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="313"/>
        <source>The settings cannot be changed during recording</source>
        <translation>錄製期間無法更改設置</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="315"/>
        <source>The settings cannot be changed,please try again later</source>
        <translation>設置無法更改，請稍後重試</translation>
    </message>
    <message>
        <source>have no permissions !?</source>
        <translation type="vanished">没有权限！？</translation>
    </message>
</context>
<context>
    <name>SettingPageTitle</name>
    <message>
        <location filename="../src/settingpagetitle.cpp" line="21"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/settingpagetitle.cpp" line="32"/>
        <source>Camera</source>
        <translation>照相機</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Set</source>
        <translation type="vanished">設置</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="70"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="77"/>
        <source>Minimise</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="83"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="89"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">麒麟相機</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="175"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="181"/>
        <source>normal</source>
        <translation>還原</translation>
    </message>
    <message>
        <source>camera</source>
        <translation type="vanished">攝像頭</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="98"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
</context>
<context>
    <name>cameraFilterWidget</name>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="23"/>
        <source>original</source>
        <translation>原圖</translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="33"/>
        <source>warm</source>
        <translation>暖色</translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="44"/>
        <source>cool</source>
        <translation>冷色</translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="53"/>
        <source>black-and-white</source>
        <translation>黑白</translation>
    </message>
</context>
</TS>
