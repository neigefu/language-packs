<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Kylin-camera</source>
        <translation type="vanished">ᠾᠧ ᠯᠢᠨ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">ᠾᠧ ᠯᠢᠨ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">ᠬᠡᠪᠯᠡᠯ ᠦᠨ ᠨᠣᠮᠧᠷ ᠄</translation>
    </message>
    <message>
        <source>Camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation type="vanished">ᠺᠠᠮᠧᠷᠠ ᠪᠣᠯ ᠨᠢᠭᠡᠨ ᠮᠠᠰᠢ ᠲᠤᠰᠬᠠᠢ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠦᠨ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃ ᠳᠥᠭᠥᠮ ᠪᠥᠭᠡᠳ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠣᠨᠴᠠᠯᠢᠭ ᠲᠠᠢ ᠃ ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ ᠲᠠᠲᠠᠬᠤ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠵᠢᠴᠢ ᠳᠡᠪᠲᠡᠷ ᠲᠦ ᠳᠡᠪᠲᠡᠷ ᠲᠦ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠶᠢ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠵᠦ ᠴᠢᠳᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>privacy statement</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠢᠯᠡᠷᠬᠡᠶᠢᠯᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <source>Kylin camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation type="vanished">ᠾᠧ ᠯᠢᠨ ᠰᠡᠭᠦᠳᠡᠷᠯᠡᠬᠦ ᠮᠠᠰᠢᠨ ᠪᠣᠯ ᠨᠢᠭᠡᠨ ᠮᠠᠰᠢ ᠲᠤᠰᠬᠠᠢ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠦᠨ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠤ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃ ᠳᠥᠭᠥᠮ ᠪᠥᠭᠡᠳ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠣᠨᠴᠠᠯᠢᠭ ᠲᠠᠢ ᠃ ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ ᠲᠠᠲᠠᠬᠤ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠵᠢᠴᠢ ᠳᠡᠪᠲᠡᠷ ᠲᠦ ᠳᠡᠪᠲᠡᠷ ᠲᠦ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠶᠢ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠵᠦ ᠴᠢᠳᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠢᠭᠡᠳ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠶᠢᠨ ᠪᠦᠯᠬᠦᠮ ᠨᠡᠶᠢᠲᠡ ᠄ </translation>
    </message>
    <message>
        <source>support</source>
        <translation type="vanished">ᠳᠡᠮᠵᠢᠨ᠎ᠡ᠃</translation>
    </message>
</context>
<context>
    <name>Button</name>
    <message>
        <location filename="../src/button.cpp" line="240"/>
        <source>capture mode</source>
        <translation>ᠰᠡᠬᠦᠳᠡᠷᠯᠡᠬᠦ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="241"/>
        <source>video mode</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠬᠦ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="242"/>
        <location filename="../src/button.cpp" line="243"/>
        <source>cheese</source>
        <translation>ᠰᠡᠬᠦᠳᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="244"/>
        <source>video</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="245"/>
        <source>stop</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠵᠤ ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="246"/>
        <source>album</source>
        <translation>ᠬᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠬᠠᠪᠢᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="247"/>
        <location filename="../src/button.cpp" line="248"/>
        <source>delay</source>
        <translation>ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="249"/>
        <source>mirror</source>
        <translation>ᠳᠤᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="250"/>
        <location filename="../src/button.cpp" line="251"/>
        <source>grid</source>
        <translation>ᠯᠠᠪᠯᠠᠬᠤ ᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="542"/>
        <source>waring</source>
        <translation>ᠸᠠᠯᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="542"/>
        <source>path does not exist, please set storage path</source>
        <translation>ᠵᠢᠮ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢᠮ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="554"/>
        <source>open File</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="555"/>
        <source>open Dir</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>filter</source>
        <translation type="vanished">ᠰᠢᠯ</translation>
    </message>
    <message>
        <location filename="../src/button.cpp" line="252"/>
        <source>seiral shoot</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠰᠡᠬᠦᠳᠡᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>CameraPage</name>
    <message>
        <source>No devices were found</source>
        <translation type="vanished">ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠣᠯᠵᠤ ᠮᠡᠳᠡᠭᠰᠡᠨ ᠦᠭᠡᠶ</translation>
    </message>
    <message>
        <source>No device were found</source>
        <translation type="obsolete">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Please connect the camera first</source>
        <translation type="obsolete">ᠬᠡᠷᠪᠡ ᠡᠨᠡ ᠴᠢᠳᠠᠪᠬᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡᠯ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠢ ᠵᠠᠯᠭᠠᠵᠤ ᠠᠪᠤᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ</translation>
    </message>
    <message>
        <source>6s</source>
        <translation type="vanished">6 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ</translation>
    </message>
    <message>
        <source>9s</source>
        <translation type="vanished">9 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ</translation>
    </message>
    <message>
        <source>waring</source>
        <translation type="obsolete">ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭ᠍ᠬᠦ</translation>
    </message>
    <message>
        <source>save path can&apos;t write</source>
        <translation type="obsolete">ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠨᠢ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠪᠢᠴᠢᠭ᠍ᠰᠡᠨ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>File generation in progress, please try again later</source>
        <translation type="obsolete">ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠶᠠᠭ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠂ ᠵᠢᠭᠠᠬᠠᠨ ᠠᠷᠤ ᠳᠤᠨᠢ ᠠᠵᠢᠯᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>path does not exist, save to default path</source>
        <translation type="obsolete">ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠶ ᠂ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠬᠤ ᠵᠠᠮ ᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>CameraTopPage</name>
    <message>
        <location filename="../src/cameratoppage.cpp" line="41"/>
        <source>No device were found</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/cameratoppage.cpp" line="42"/>
        <source>Please connect the camera first and app will continuously retrieve avaliable devices for you</source>
        <translation>ᠤᠷᠢᠳᠠᠪᠠᠷ ᠺᠡᠮᠸᠷᠠ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠡᠷᠡᠢ᠂ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠨᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠲᠠᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠬᠠᠢᠵᠤ ᠦᠭᠬᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/cameratoppage.cpp" line="45"/>
        <source>not support current device</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ</translation>
    </message>
    <message>
        <location filename="../src/cameratoppage.cpp" line="46"/>
        <source>Please check the camera or switch to avaliable device</source>
        <translation>ᠰᠡᠭᠦᠳᠡᠷᠯᠡᠬᠦ ᠮᠠᠰᠢᠨ ᠪᠤᠶᠤ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠰᠣᠯᠢᠵᠤ ᠥᠭ᠍ᠭᠥᠭᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>DelayWdiget</name>
    <message>
        <location filename="../src/delaywdiget.cpp" line="56"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="57"/>
        <source>3s</source>
        <translation>3 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="58"/>
        <source>6s</source>
        <translation>6 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../src/delaywdiget.cpp" line="59"/>
        <source>9s</source>
        <translation>9 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ</translation>
    </message>
</context>
<context>
    <name>DevicePull</name>
    <message>
        <location filename="../src/devicepull.cpp" line="57"/>
        <source>Device is pulled out</source>
        <translation>ᠺᠠᠮᠸᠷᠠ ᠲᠠᠢ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠢ᠋ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="68"/>
        <source>Device is pulled in</source>
        <translation>ᠺᠠᠮᠸᠷᠠ ᠲᠠᠢ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠢ᠋ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="111"/>
        <source>Camera</source>
        <translation>ᠺᠸᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../src/devicepull.cpp" line="114"/>
        <source>kylin camera message</source>
        <translation>ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
</context>
<context>
    <name>HasDevicePage</name>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="392"/>
        <location filename="../src/hasdevicepage.cpp" line="449"/>
        <location filename="../src/hasdevicepage.cpp" line="1006"/>
        <source>camera is being used by kylin-camera</source>
        <translation>ᠺᠡᠮᠸᠷᠠ ᠵᠢᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="398"/>
        <location filename="../src/hasdevicepage.cpp" line="432"/>
        <location filename="../src/hasdevicepage.cpp" line="473"/>
        <location filename="../src/hasdevicepage.cpp" line="581"/>
        <location filename="../src/hasdevicepage.cpp" line="742"/>
        <location filename="../src/hasdevicepage.cpp" line="753"/>
        <location filename="../src/hasdevicepage.cpp" line="839"/>
        <location filename="../src/hasdevicepage.cpp" line="999"/>
        <source>waring</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="398"/>
        <location filename="../src/hasdevicepage.cpp" line="999"/>
        <source>The camera is occupied, please check the use of the device!</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠡᠵᠡᠯᠡᠭᠳᠡᠪᠡ᠂ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="432"/>
        <source>The camera is occupied or there is an exception in the target switching device, please check the device!</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠪᠤᠶᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="473"/>
        <location filename="../src/hasdevicepage.cpp" line="742"/>
        <location filename="../src/hasdevicepage.cpp" line="839"/>
        <source>save path can&apos;t write</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢᠮ ᠢ᠋ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠡᠷᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="581"/>
        <source>path does not exist, save to default path</source>
        <translation>ᠵᠢᠮ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ᠂ ᠹᠠᠢᠯ ᠢ᠋ ᠠᠶᠠᠳᠠᠯ ᠵᠢᠮ ᠳ᠋ᠤ᠌ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="753"/>
        <source>File generation in progress, please try again later</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠠᠵᠢᠯᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="1437"/>
        <source>Camera</source>
        <translation>ᠺᠸᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <source>3p</source>
        <translation type="vanished">3P</translation>
    </message>
    <message>
        <source>5p</source>
        <translation type="vanished">5P</translation>
    </message>
    <message>
        <source>10p</source>
        <translation type="vanished">10P</translation>
    </message>
    <message>
        <source>20p</source>
        <translation type="vanished">20P</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../src/hasdevicepage.cpp" line="1440"/>
        <source>kylin camera message</source>
        <translation>ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <source>3pic</source>
        <translation type="vanished">3 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>5pic</source>
        <translation type="vanished">5 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>10pic</source>
        <translation type="vanished">10 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>20pic</source>
        <translation type="vanished">20 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="118"/>
        <source>Camera</source>
        <translation>ᠺᠸᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="177"/>
        <location filename="../src/mainwindow.cpp" line="412"/>
        <source>waring</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>maximum</source>
        <translation type="vanished">ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>normal</source>
        <translation type="vanished">ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>other user already open device!</source>
        <translation type="obsolete">ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠶᠠᠭ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠶᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>path does not exist, save to default path</source>
        <translation type="obsolete">ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠶ ᠂ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠬᠤ ᠵᠠᠮ ᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="177"/>
        <source>path does not exist, please set storage path</source>
        <translation>ᠵᠢᠮ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢᠮ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>save path can&apos;t write</source>
        <translation type="vanished">ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠨᠢ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠪᠢᠴᠢᠭ᠍ᠰᠡᠨ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>File generation in progress, please try again later</source>
        <translation type="obsolete">ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠶᠠᠭ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠂ ᠵᠢᠭᠠᠬᠠᠨ ᠠᠷᠤ ᠳᠤᠨᠢ ᠠᠵᠢᠯᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">ᠬᠠᠰᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Are you sure?!</source>
        <translation type="vanished">ᠴᠢ ᠢᠩᠭ᠍ᠢᠬᠦ ᠭᠡᠵᠦ ᠲᠣᠭᠲᠠᠭᠠᠪᠠ ᠤᠤ ?</translation>
    </message>
    <message>
        <source>no</source>
        <translation type="vanished">ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>yes</source>
        <translation type="vanished">ᠲᠡᠢ᠌ᠮᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="412"/>
        <source>The camera is turned on and cannot be started again.</source>
        <translation>ᠰᠡᠬᠦᠳᠡᠷᠯᠡᠬᠦ ᠮᠠᠰᠢᠨ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ᠂ ᠳᠠᠬᠢᠵᠤ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">ᠬᠠᠪᠰᠤᠷᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">ᠭᠣᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <source>set</source>
        <translation type="vanished">ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>quit</source>
        <translation type="vanished">ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>NPixCapWidget</name>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="62"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="63"/>
        <source>3p</source>
        <translation>3P</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="64"/>
        <source>5p</source>
        <translation>5P</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="65"/>
        <source>10p</source>
        <translation>10P</translation>
    </message>
    <message>
        <location filename="../src/npixcapwidget.cpp" line="66"/>
        <source>20p</source>
        <translation>20P</translation>
    </message>
    <message>
        <source>3pic</source>
        <translation type="vanished">3 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>5pic</source>
        <translation type="vanished">5 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>10pic</source>
        <translation type="vanished">10 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>20pic</source>
        <translation type="vanished">20 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>3sheets</source>
        <translation type="vanished">3 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>5sheets</source>
        <translation type="vanished">5 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>10sheets</source>
        <translation type="vanished">10 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>20sheets</source>
        <translation type="vanished">20 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>NoDevicePage</name>
    <message>
        <location filename="../src/nodevicepage.cpp" line="47"/>
        <source>No device were found</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠺᠡᠮᠸᠷᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/nodevicepage.cpp" line="48"/>
        <source>Please connect the camera first and app will continuously retrieve avaliable devices for you</source>
        <translation>ᠤᠷᠢᠳᠠᠪᠠᠷ ᠺᠡᠮᠸᠷᠠ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠡᠷᠡᠢ᠂ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠨᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠲᠠᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠬᠠᠢᠵᠤ ᠦᠭᠬᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Please connect the camera first</source>
        <translation type="vanished">ᠬᠡᠷᠪᠡ ᠡᠨᠡ ᠴᠢᠳᠠᠪᠬᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡᠯ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠢ ᠵᠠᠯᠭᠠᠵᠤ ᠠᠪᠤᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>PictureViewPage</name>
    <message>
        <location filename="../src/pictureviewpage.cpp" line="17"/>
        <source>Album</source>
        <translation>ᠬᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠬᠠᠪᠢᠰᠤ</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="104"/>
        <source>Camera</source>
        <translation>ᠺᠸᠮᠸᠷᠠ</translation>
    </message>
</context>
<context>
    <name>RecordModule</name>
    <message>
        <source>stop record</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠳᠠᠭᠤᠰᠴᠠᠢ</translation>
    </message>
    <message>
        <source>countinue record</source>
        <translation type="vanished">ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <source>pause record</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢ ᠲᠦᠷ ᠵᠣᠭᠰᠣᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="11"/>
        <source>stop</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠵᠤ ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="17"/>
        <source>countinue</source>
        <translation>ᠪᠦᠸᠧᠳᠢᠨ ᠧᠽᠢ</translation>
    </message>
    <message>
        <location filename="../src/recordmodule.cpp" line="23"/>
        <source>pause</source>
        <translation>ᠲᠦᠷ ᠵᠣᠭᠰᠣᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>Setting</name>
    <message>
        <source>Delayed shooting</source>
        <translation type="vanished">ᠬᠣᠢ᠌ᠰᠢᠯᠠ</translation>
    </message>
    <message>
        <source>Image mirroring</source>
        <translation type="vanished">ᠰᠢᠯ ᠦᠨ ᠬᠥᠷᠥᠭ</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <source>quit</source>
        <translation type="vanished">ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">ᠳᠤᠰᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="51"/>
        <source>Delayed</source>
        <translation>ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <source>mirroring</source>
        <translation type="vanished">ᠰᠢᠯ ᠦᠨ ᠬᠥᠷᠥᠭ</translation>
    </message>
    <message>
        <source>set</source>
        <translation type="vanished">ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">ᠴᠢ ᠯᠢᠨ ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="35"/>
        <location filename="../src/setting.cpp" line="158"/>
        <source>Set</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="36"/>
        <source>Theme</source>
        <translation>ᠭᠣᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="37"/>
        <location filename="../src/setting.cpp" line="136"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="38"/>
        <location filename="../src/setting.cpp" line="139"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="39"/>
        <location filename="../src/setting.cpp" line="180"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="67"/>
        <source>Auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="71"/>
        <source>Light</source>
        <translation>ᠬᠦᠢᠬᠡᠨ ᠦᠩᠬᠡ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="75"/>
        <source>Dark</source>
        <translation>ᠬᠦᠨ ᠦᠩᠬᠡ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="140"/>
        <source>Camera</source>
        <translation>ᠺᠸᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="142"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ: </translation>
    </message>
    <message>
        <location filename="../src/setting.cpp" line="143"/>
        <source>Camera is a very strong camera software. It has the characteristics of easy to use. You can realize the function of taking a photo recording and displaying the album</source>
        <translation>ᠰᠡᠭᠦᠳᠡᠷᠯᠡᠬᠦ ᠮᠠᠰᠢᠨ ᠪᠣᠯ ᠨᠢᠭᠡᠨ ᠲᠤᠩ ᠬᠦᠴᠦᠷᠬᠡᠭ᠍ ᠶᠡᠬᠡ ᠰᠡᠭᠦᠳᠡᠷᠯᠡᠬᠦ ᠮᠠᠰᠢᠨ ᠤ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠮᠥᠨ ᠃ ᠲᠡᠷᠡ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳᠦ ᠬᠢᠯᠪᠠᠷ ᠣᠨᠴᠠᠯᠢᠭ ᠲᠠᠢ ᠃ ᠲᠠ ᠰᠡᠭᠦᠳᠡᠷ ᠲᠠᠲᠠᠬᠤ ᠪᠠ ᠳᠡᠪᠲᠡᠷᠯᠡᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠶᠢ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>The image path</source>
        <translation type="vanished">ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠵᠠᠮ ᠮᠥᠷ</translation>
    </message>
    <message>
        <source>The image scale</source>
        <translation type="vanished">ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠲᠣᠨᠭ᠌</translation>
    </message>
    <message>
        <source>Camera device</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Sound device</source>
        <translation type="vanished">ᠳᠠᠭᠤᠨ ᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Select the directory</source>
        <translation type="vanished">ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>have no permissions !?</source>
        <translation type="vanished">ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠦᠭᠡᠢ !?</translation>
    </message>
</context>
<context>
    <name>SettingPage</name>
    <message>
        <source>setting</source>
        <translation type="vanished">ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Delayed shooting</source>
        <translation type="obsolete">ᠰᠡᠭᠦᠳᠡᠷ ᠲᠠᠲᠠᠬᠤ ᠶᠢ ᠬᠣᠢ᠌ᠰᠢᠯᠠᠭᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Image mirroring</source>
        <translation type="obsolete">ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠰᠢᠯ</translation>
    </message>
    <message>
        <source>The image path</source>
        <translation type="vanished">ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠵᠠᠮ ᠮᠥᠷ</translation>
    </message>
    <message>
        <source>The image scale</source>
        <translation type="vanished">ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢᠨ ᠢᠯᠭᠠᠴᠠ</translation>
    </message>
    <message>
        <source>Camera device</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Sound device</source>
        <translation type="vanished">ᠳᠠᠭᠤᠨ ᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="74"/>
        <source>confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="76"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>photo save format</source>
        <translation type="vanished">ᠵᠢᠷᠤᠭ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>video save format</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>camera setting</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <source>The image path:</source>
        <translation type="vanished">ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠪᠡᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠠᠶᠢᠷᠢ：</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="36"/>
        <source>browse</source>
        <translation>ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Camera device:</source>
        <translation type="vanished">ᠺᠠᠮᠧᠷᠠ ᠶᠢ ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠵᠤ：</translation>
    </message>
    <message>
        <source>save photo format:</source>
        <translation type="vanished">ᠭᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ：</translation>
    </message>
    <message>
        <source>save video format:</source>
        <translation type="vanished">ᠬᠠᠷᠠᠭᠠᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ：</translation>
    </message>
    <message>
        <source>The image scale:</source>
        <translation type="vanished">ᠰᠡᠭᠦᠳᠡᠷᠯᠡᠬᠦ ᠢᠯᠭᠠᠴᠠ：</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="37"/>
        <source>image path:</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢᠮ:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="46"/>
        <source>device:</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="55"/>
        <source>photo format:</source>
        <translation>ᠬᠡᠷᠡᠯ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="62"/>
        <source>video format:</source>
        <translation>ᠸᠢᠳᠢᠤ᠋ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="69"/>
        <source>scale:</source>
        <translation>ᠰᠡᠬᠦᠳᠡᠷᠯᠡᠬᠦ ᠢᠯᠭᠠᠮᠵᠢ:</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="263"/>
        <location filename="../src/settingpage.cpp" line="282"/>
        <source>Select the directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="269"/>
        <location filename="../src/settingpage.cpp" line="288"/>
        <source>error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="269"/>
        <location filename="../src/settingpage.cpp" line="288"/>
        <source>The directory does not have write permissions, select the user directory to store the files .</source>
        <translation>ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠡᠷᠬᠡ ᠦᠬᠡᠢ᠂ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠭᠠᠷᠴᠠᠭ ᠳᠤᠤᠷᠠᠬᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="313"/>
        <location filename="../src/settingpage.cpp" line="315"/>
        <source>waring</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="313"/>
        <source>The settings cannot be changed during recording</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠵᠠᠰᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/settingpage.cpp" line="315"/>
        <source>The settings cannot be changed,please try again later</source>
        <translation>ᠲᠦᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>have no permissions !?</source>
        <translation type="vanished">ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠦᠭᠡᠢ !?</translation>
    </message>
</context>
<context>
    <name>SettingPageTitle</name>
    <message>
        <location filename="../src/settingpagetitle.cpp" line="21"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/settingpagetitle.cpp" line="32"/>
        <source>Camera</source>
        <translation>ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Set</source>
        <translation type="vanished">ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="70"/>
        <source>Options</source>
        <translation>ᠰᠣᠩᠭᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="77"/>
        <source>Minimise</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="83"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="89"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>kylin-camera</source>
        <translation type="vanished">ᠴᠢ ᠯᠢᠨ ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="175"/>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="181"/>
        <source>normal</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>camera</source>
        <translation type="vanished">ᠳᠦᠷᠰᠦ ᠰᠢᠩᠭᠡᠭᠡᠯᠲᠡ ᠶᠢᠨ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="98"/>
        <source>Camera</source>
        <translation>ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
</context>
<context>
    <name>cameraFilterWidget</name>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="23"/>
        <source>original</source>
        <translation>ᠤᠭ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="33"/>
        <source>warm</source>
        <translation>ᠳᠤᠯᠠᠭᠠᠨ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="44"/>
        <source>cool</source>
        <translation>ᠬᠦᠢᠳᠡᠨ ᠦᠩᠬᠡ</translation>
    </message>
    <message>
        <location filename="../src/camerafilterwidget.cpp" line="53"/>
        <source>black-and-white</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠴᠠᠭᠠᠨ</translation>
    </message>
</context>
</TS>
