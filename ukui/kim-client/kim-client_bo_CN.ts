<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AuthWidget</name>
    <message>
        <source>Step 1</source>
        <translation>གོམ་རིམ།1</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>གོམ་རིམ།</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>གོམ་པ་སྔོན་མ།</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>གོམ་པ་རྗེས་མར།</translation>
    </message>
    <message>
        <source>Authention</source>
        <translation type="vanished">账号认证</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>རྨང་གཞིའི་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>རྩིས་ཨང་དཔང་དངོས་བདེན་པ་ཡིན་པའི་ར་སྤྲོད།</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <source>Step 1</source>
        <translation>གོམ་རིམ།1</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>གོམ་རིམ།</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>དུད་སྣེ།གནམ་གྲུའི་མིང་།</translation>
    </message>
    <message>
        <source>Server</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་ས་གནས།</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>གོམ་པ་སྔོན་མ།</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>གོམ་པ་རྗེས་མར།</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <source>Hostname can not be empty!</source>
        <translation>དུད་སྣེའི་གནམ་གྲུའི་མིང་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <source>Please input server address!</source>
        <translation>ཞབས་ཞུའི་སྣེ་ཡི་ས་གནས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Server not found!</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་ཆ་འཕྲིན་མི་རྙེད།</translation>
    </message>
    <message>
        <source>Please enter a valid server hostname or IP address!</source>
        <translation type="vanished">请输入有效的服务器主机名！</translation>
    </message>
    <message>
        <source>Searching for server...</source>
        <translation>ཞབས་ཞུ་ཆས་འཚོལ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>The client and server time is inconsistent, please correct the time first!</source>
        <translation>མགྲོན་སྣེ་དང་ཞབས་ཞུའི་དུད་སྣེའི་དུས་ཚོད་གཅིག་མཐུན་མིན་པས་སྔོན་ལ་དུས་བསྟུན་བྱོས།</translation>
    </message>
    <message>
        <source>Get infomation from server failed!</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་ཆ་འཕྲིན་མི་རྙེད།</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>རྨང་གཞིའི་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>རྩིས་ཨང་དཔང་དངོས་བདེན་པ་ཡིན་པའི་ར་སྤྲོད།</translation>
    </message>
    <message>
        <source>Ip address is not allowed!</source>
        <translation>IPས་གནས་ཁ་སྣོན་བྱས་མི་ཆོག</translation>
    </message>
    <message>
        <source>Server not found, please make sure your input is correct and check your dns</source>
        <translation>ཞབས་ཞུ་ཆས་རྙེད་མི་ཐུབ།ནང་འཇུག་བྱས་པའི་ཁོངས་མིང་ཡང་དག་ཡིན་པ་མ་ཟད་མཐའ་སྣེའི་བཀོད་སྒྲིག་ཡང་དག་ཡིན་པར་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Host name is the same as server!</source>
        <translation>གནམ་གྲུའི་མིང་གཙོ་བོ་ཞབས་ཞུ་ཆས་ཀྱི་ས་གནས་དང་མི་འདྲ།</translation>
    </message>
</context>
<context>
    <name>ConfirmWidget</name>
    <message>
        <source>Finished</source>
        <translation>བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Server IP</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་ས་གནས།</translation>
    </message>
    <message>
        <source>Server Name</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་གནམ་གྲུའི་མིང་།</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>དུད་སྣེ།གནམ་གྲུའི་མིང་།</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>གོམ་པ་སྔོན་མ།</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation>དོ་དམ་ཚོད་འཛིན།</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>ལས་སྣོན་བྱེད་བཞིན་པའི་སྒང་ཡིན།བཀོལ་སྤྱོད་ཀྱི་སྒོ་མ་བརྒྱག</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <source>Enroll failed:%1</source>
        <translation>བསྣན་ཁོངས་ཕམ་པ།%1</translation>
    </message>
    <message>
        <source>Enroll failed, please check host.</source>
        <translation>སྣོན་ཡུལ་ཕམ་སོང་།གཙོ་ཆས་བཀོལ་ཆོག་མིན་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Enroll failed, please check server.</source>
        <translation>བསྣན་ཁོངས་ཕམ་སོང་།ཞབས་ཞུ་རྒྱུན་ལྡན་ཡིན་མིན་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>FinishedWidget</name>
    <message>
        <source>Reboot</source>
        <translation>མྱུར་དུ་ཡང་བསྐྱར་སྒོ་ཕྱེ།</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>གནས་སྐབས་བསྐྱར་དུ་སྒོ་མི་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Enroll to domain succeed</source>
        <translation>ཁྱབ་ཁོངས་ཚོད་འཛིན་མ་ལག་ནང་བདེ་བླག་ངང་ཞུགས།</translation>
    </message>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation>ཕྱིར་འཐེན་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>བསྐྱར་དུ་སྒོ་ཕྱེ་ནས་ཕམ་སོང་།མིག་སྔར་སྤྱོད་མཁན་གཞན་གྱིས་ཐོ་འགོད་བྱས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>InstalledWidget</name>
    <message>
        <source>Server IP</source>
        <translation type="vanished">服务器地址</translation>
    </message>
    <message>
        <source>Server name</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་གནམ་གྲུའི་མིང་།</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>དུད་སྣེ།གནམ་གྲུའི་མིང་།</translation>
    </message>
    <message>
        <source>Unenroll</source>
        <translation>ཕྱིར་འཐེན་དོ་དམ་ཚོད་འཛིན།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <source>Dbus called failed:%1</source>
        <translation>Dbusམཐུད་ཁའི་འདོན་སྤྱོད་ལ་ཕམ་ཁ་བྱུང་།%</translation>
    </message>
    <message>
        <source>Unenrolled:%1</source>
        <translation>ཕྱིར་འཐེན་དོ་དམ་ཚོད་འཛིན།%</translation>
    </message>
    <message>
        <source>Unenrolled failed: %1</source>
        <translation>ཕྱིར་འཐེན་དོ་དམ་ཚོད་འཛིན་ལ་ཕམ་ཁ་བྱུང་།%</translation>
    </message>
    <message>
        <source>Are you sure to unenroll?</source>
        <translation>ཕྱིར་འཐེན་དོ་དམ་ཚོད་འཛིན་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Unenrolling, please not close this application</source>
        <translation>ཁྱབ་ཁོངས་ཀྱི་སྦུ་གུ་ལས་འབུད་བཞིན་པའི་སྒང་ཡིན།བཀོལ་སྤྱོད་ཀྱི་སྒོ་མ་བརྒྱག</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>བཀོད་སྒྲིག་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <source>loading...</source>
        <translation>ཅི་ནུས་ཀྱིས་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kim Client</source>
        <translation>ཅ་ཁོངས་མཁོ་མཁན།</translation>
    </message>
    <message>
        <source>Caution</source>
        <translation>མཉམ་འཇོག་བྱོས།</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་སྒང་ཡིན་པས་བཀོལ་སྤྱོད་བྱེད་མཚམས་མ་འཇོག</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>PwdChangeWidget</name>
    <message>
        <source>Password expired, change password</source>
        <translation>གསང་གྲངས་དུས་ལས་བརྒལ་སོང་།གསང་གྲངས་བརྗེ་སོང་།</translation>
    </message>
    <message>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>Old password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>གསང་གྲངས་གསར་བ།</translation>
    </message>
    <message>
        <source>Reenter password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="obsolete">上一步</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="obsolete">加入管控</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>བཟོ་བཅོས།</translation>
    </message>
    <message>
        <source>Password change success.</source>
        <translation>གསང་གྲངས་བཟོ་བཅོས་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Insufficient permissions</source>
        <translation>དབང་ཚད་མི་འདང་བ།</translation>
    </message>
    <message>
        <source>The current user does not have permissions, contact your administrator.</source>
        <translation>མིག་སྔར་སྤྱོད་མཁན་ལ་དབང་ཚད་མེད་པས་དོ་དམ་པར་འབྲེལ་གཏུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Server connection failed, please check the configuration!</source>
        <translation type="vanished">服务端连接失败，请检查参数配置！</translation>
    </message>
    <message>
        <source>User authentication failed, please check the account and password!</source>
        <translation>སྤྱོད་མཁན་གྱིས་ཚོད་ལྟས་ར་སྤྲོད་བྱེད་མ་ཐུབ་པས།ཐོ་ཨང་དང་གསང་གྲངས་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Query for user failed, please check the configuration!</source>
        <translation type="vanished">用户信息查询失败，请检查连接配置！</translation>
    </message>
    <message>
        <source>User password expires, please change the password and try again!</source>
        <translation>སྤྱོད་མཁན་གྱི་གསང་གྲངས་དུས་ལས་ཡོལ་བས་གསང་གྲངས་བརྗེ་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Search for configure failed, please check the server!</source>
        <translation type="vanished">系统信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>User rights are insufficient, please contact the administrator!</source>
        <translation>སྤྱོད་མཁན་གྱི་དབང་ཚད་མི་འདང་བས་དོ་དམ་པར་འབྲེལ་གཏུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Hostname[%1] has already registered, please change the name or delete the record!</source>
        <translation>གནམ་གྲུའི་མིང་གཙོ་བོ[1%]བསྣན་པ་རེད།གནམ་གྲུ་གཙོ་བོའི་མིང་བརྗེ་བའམ་ཡང་ན་ཞབས་ཞུའི་ཟིན་ཐོ་བསུབ་རོགས།</translation>
    </message>
    <message>
        <source>Query for replica server failed, please check the server configuration!</source>
        <translation type="vanished">副本服务器信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <source>Invalid format!</source>
        <translation>གཞི་གྲངས་རྒྱུན་ལྡན་མིན་པ་དང་།ནུས་མེད་ཀྱི་གཞི་གྲངས་རྣམ་གཞག་ཐོབ་པ།</translation>
    </message>
    <message>
        <source>Request error: %1</source>
        <translation>རེ་ཞུའི་ནོར་འཁྲུལ།%</translation>
    </message>
    <message>
        <source>Request error: counts error!</source>
        <translation>ནོར་འཁྲུལ་གྱི་རེ་འདུན།ཕྱིར་ལོག་པའི་གྲངས་ཀ་མི་མཐུན།</translation>
    </message>
    <message>
        <source>Please input username!</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Please input password!</source>
        <translation>གསང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Server request error:%1</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱིས་ནོར་འཁྲུལ་གྱི་རེ་བ་ཞུས།%</translation>
    </message>
    <message>
        <source>Authentication is abnormal, check the service status!</source>
        <translation>དཔང་དངོས་བདེན་པ་ཡིན་པའི་ར་སྤྲོད་རྒྱུན་ལྡན་མིན་པས་ཞབས་འདེགས་ཞུ་ཚུལ་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>authenting, please wait...</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་པའི་རེ་ཞུའི་ཁྲོད།ཅུང་ཙམ་སྒུག་རོགས།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>མུ་མཐུད་དུ།</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>ངོས་འཛིན།</translation>
    </message>
    <message>
        <source>Please input new password!</source>
        <translation>གསང་གྲངས་གསར་བ་ཕྱིར་འདོན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Confirmed password and new password do not match!</source>
        <translation>གསང་གྲངས་གསར་བ་དང་གསང་གྲངས་གསར་བ་མི་འདྲ་བ་གཏན་ཁེལ་བྱས།</translation>
    </message>
</context>
<context>
    <name>RecoveryWidget</name>
    <message>
        <source>Recover</source>
        <translation>སླར་གསོ།།</translation>
    </message>
    <message>
        <source>exit</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>The last operation did not complete successfully, please recover!</source>
        <translation>ཐེངས་སྔོན་མའི་བཀོལ་སྤྱོད་ལེགས་འགྲུབ་མ་བྱུང་བས་སླར་གསོ་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>UnenrollAuthWidget</name>
    <message>
        <source>Please input username and password which has authority to unjoin the domain, and the click the continue button.</source>
        <translation>ཁྱེད་ཀྱིས་ཕྱིར་འཐེན་ལག་བསྟར་བྱེད་བཞིན་པའི་སྒང་ཡིན།ཁྱེད་ཀྱིས་ཕྱིར་འཐེན་བྱེད་པའི་དབང་ཆ་ཡོད་པའི་ཐོ་ཨང་ནང་འཇུག་བྱེད་རོགས།དེ་ནས་མུ་མཐུད་དུ་གནོན་རོགས།</translation>
    </message>
    <message>
        <source>Input username</source>
        <translation>ཕྱིར་འཐེན་དབང་ཆ་ཡོད་པའི་ཐོ་ཨང་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation>གསང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation>ཁོངས།</translation>
    </message>
</context>
<context>
    <name>UnenrollFinishedWidget</name>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation type="obsolete">退出成功</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>གནས་སྐབས་བསྐྱར་དུ་སྒོ་མི་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation>མྱུར་དུ་ཡང་བསྐྱར་སྒོ་ཕྱེ།</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>སྣེ་སྟོན།</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>བསྐྱར་དུ་སྒོ་ཕྱེ་ནས་ཕམ་སོང་།མིག་སྔར་སྤྱོད་མཁན་གཞན་གྱིས་ཐོ་འགོད་བྱས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>You have already unrolled from %1 domain</source>
        <translation>ཁྱེད་རང་བརྒྱ་ཆ1ཁོངས་ནས་ཕྱིར་འཐེན་བྱས་ཟིན།</translation>
    </message>
</context>
<context>
    <name>UnenrollInfoWidget</name>
    <message>
        <source>Unenrolling, please do not close this application</source>
        <translation>ཁྱབ་ཁོངས་ཀྱི་སྦུ་གུ་ལས་འབུད་བཞིན་པའི་སྒང་ཡིན།བཀོལ་སྤྱོད་ཀྱི་སྒོ་མ་བརྒྱག</translation>
    </message>
    <message>
        <source>Before you exist, please make sure that the local user is enabled and you have the password of this user.</source>
        <translation>ཡུལ་ཁོངས་ནས་ཕྱིར་མ་ལོག་གོང་།ལག་རོགས་པ་ཆུང་ཆུང་གིས་ཁྱེད་ཀྱི་མཐའ་སྣེའི་སྤྱོད་མཁན་དེ་སྤྱོད་འགོ་ཚུགས་པའི་གནས་སུ་ཡོད་པར་ངོས་འཛིན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Click the ok button to continue</source>
        <translation>ཆིག་རྡེབ་བྱས་ན་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ།</translation>
    </message>
</context>
</TS>
