<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AuthWidget</name>
    <message>
        <source>Step 1</source>
        <translation>步骤1</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>步骤2</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>上一步</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <source>Authention</source>
        <translation type="vanished">账号认证</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>基础配置</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>账号认证</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <source>Step 1</source>
        <translation>步骤1</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>步骤2</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>客户端主机名</translation>
    </message>
    <message>
        <source>Server</source>
        <translation>服务器地址</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>上一步</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Hostname can not be empty!</source>
        <translation>客户端主机名不能为空！</translation>
    </message>
    <message>
        <source>Please input server address!</source>
        <translation>请输入服务端地址！</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>Server not found!</source>
        <translation>找不到服务器信息！</translation>
    </message>
    <message>
        <source>Please enter a valid server hostname or IP address!</source>
        <translation type="vanished">请输入有效的服务器主机名！</translation>
    </message>
    <message>
        <source>Searching for server...</source>
        <translation>正在查找服务器...</translation>
    </message>
    <message>
        <source>The client and server time is inconsistent, please correct the time first!</source>
        <translation>客户端和服务端时间不一致，请先对时！</translation>
    </message>
    <message>
        <source>Get infomation from server failed!</source>
        <translation>找不到服务器信息！</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>基础配置</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>账号认证</translation>
    </message>
    <message>
        <source>Ip address is not allowed!</source>
        <translation>不允许使用IP地址加域！</translation>
    </message>
    <message>
        <source>Server not found, please make sure your input is correct and check your dns</source>
        <translation>找不到服务器，请确保输入的域名正确且终端已正确的配置DNS</translation>
    </message>
    <message>
        <source>Host name is the same as server!</source>
        <translation>主机名不能和服务器地址一样！</translation>
    </message>
</context>
<context>
    <name>ConfirmWidget</name>
    <message>
        <source>Finished</source>
        <translation>完成配置</translation>
    </message>
    <message>
        <source>Server IP</source>
        <translation>服务器地址</translation>
    </message>
    <message>
        <source>Server Name</source>
        <translation>服务器主机名</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>客户端主机名</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>上一步</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation>加入管控</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>正在加域中，请不要关闭应用！</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Enroll failed:%1</source>
        <translation>加域失败：%1</translation>
    </message>
    <message>
        <source>Enroll failed, please check host.</source>
        <translation>加域失败，请检查主机是否可用。</translation>
    </message>
    <message>
        <source>Enroll failed, please check server.</source>
        <translation>加域失败，请检查服务是否正常。</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>FinishedWidget</name>
    <message>
        <source>Reboot</source>
        <translation>立即重启</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>暂不重启</translation>
    </message>
    <message>
        <source>Enroll to domain succeed</source>
        <translation>已成功加入域控系统</translation>
    </message>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation>退出成功</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>重启失败，请检查当前是否有其他用户登录。</translation>
    </message>
</context>
<context>
    <name>InstalledWidget</name>
    <message>
        <source>Server IP</source>
        <translation type="vanished">服务器地址</translation>
    </message>
    <message>
        <source>Server name</source>
        <translation>服务器主机名</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>客户端主机名</translation>
    </message>
    <message>
        <source>Unenroll</source>
        <translation>退出管控</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Dbus called failed:%1</source>
        <translation>Dbus接口调用失败：%1</translation>
    </message>
    <message>
        <source>Unenrolled:%1</source>
        <translation>退出管控：%1</translation>
    </message>
    <message>
        <source>Unenrolled failed: %1</source>
        <translation>退出管控失败：%1</translation>
    </message>
    <message>
        <source>Are you sure to unenroll?</source>
        <translation>是否退出管控？</translation>
    </message>
    <message>
        <source>Unenrolling, please not close this application</source>
        <translation>正在退出域管，请不要关闭应用</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>配置信息</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>错误</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <source>loading...</source>
        <translation>拼命加载中...</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kim Client</source>
        <translation>加域客户端</translation>
    </message>
    <message>
        <source>Caution</source>
        <translation>注意</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>正在操作中，请不要关闭应用！</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>PwdChangeWidget</name>
    <message>
        <source>Password expired, change password</source>
        <translation>密码已过期，修改密码</translation>
    </message>
    <message>
        <source>User</source>
        <translation>用户</translation>
    </message>
    <message>
        <source>Old password</source>
        <translation>原密码</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>新密码</translation>
    </message>
    <message>
        <source>Reenter password</source>
        <translation>确认密码</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="obsolete">上一步</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="obsolete">加入管控</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>修改</translation>
    </message>
    <message>
        <source>Password change success.</source>
        <translation>密码修改成功。</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Insufficient permissions</source>
        <translation>权限不足</translation>
    </message>
    <message>
        <source>The current user does not have permissions, contact your administrator.</source>
        <translation>当前用户没有权限，请联系管理员。</translation>
    </message>
    <message>
        <source>Server connection failed, please check the configuration!</source>
        <translation type="vanished">服务端连接失败，请检查参数配置！</translation>
    </message>
    <message>
        <source>User authentication failed, please check the account and password!</source>
        <translation>用户验证失败，请检查账号和密码！</translation>
    </message>
    <message>
        <source>Query for user failed, please check the configuration!</source>
        <translation type="vanished">用户信息查询失败，请检查连接配置！</translation>
    </message>
    <message>
        <source>User password expires, please change the password and try again!</source>
        <translation>用户密码过期，请修改密码后重试！</translation>
    </message>
    <message>
        <source>Search for configure failed, please check the server!</source>
        <translation type="vanished">系统信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>User rights are insufficient, please contact the administrator!</source>
        <translation>用户权限不足，请联系管理员！</translation>
    </message>
    <message>
        <source>Hostname[%1] has already registered, please change the name or delete the record!</source>
        <translation>主机名[%1]已经加域，请更换主机名或者删除服务端记录！</translation>
    </message>
    <message>
        <source>Query for replica server failed, please check the server configuration!</source>
        <translation type="vanished">副本服务器信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Invalid format!</source>
        <translation>获取数据异常，无效的数据格式！</translation>
    </message>
    <message>
        <source>Request error: %1</source>
        <translation>请求错误：%1</translation>
    </message>
    <message>
        <source>Request error: counts error!</source>
        <translation>请求错误：返回数量不匹配！</translation>
    </message>
    <message>
        <source>Please input username!</source>
        <translation>请输入用户名！</translation>
    </message>
    <message>
        <source>Please input password!</source>
        <translation>请输入密码！</translation>
    </message>
    <message>
        <source>Server request error:%1</source>
        <translation>服务器请求错误：%1</translation>
    </message>
    <message>
        <source>Authentication is abnormal, check the service status!</source>
        <translation>认证异常，请检查服务状态!</translation>
    </message>
    <message>
        <source>authenting, please wait...</source>
        <translation>请求认证中，请稍后...</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Please input new password!</source>
        <translation>请输出新密码！</translation>
    </message>
    <message>
        <source>Confirmed password and new password do not match!</source>
        <translation>确认密码和新密码不一致！</translation>
    </message>
</context>
<context>
    <name>RecoveryWidget</name>
    <message>
        <source>Recover</source>
        <translation>恢复</translation>
    </message>
    <message>
        <source>exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>The last operation did not complete successfully, please recover!</source>
        <translation>上次操作未成功完成，请恢复！</translation>
    </message>
</context>
<context>
    <name>UnenrollAuthWidget</name>
    <message>
        <source>Please input username and password which has authority to unjoin the domain, and the click the continue button.</source>
        <translation>您正在执行退域操作，请输入具备退域权限的账号，然后点击继续。</translation>
    </message>
    <message>
        <source>Input username</source>
        <translation>请输入具备退域权限的账号</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation>请输入密码</translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation>域：</translation>
    </message>
</context>
<context>
    <name>UnenrollFinishedWidget</name>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation type="obsolete">退出成功</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>暂不重启</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation>立即重启</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>重启失败，请检查当前是否有其他用户登录。</translation>
    </message>
    <message>
        <source>You have already unrolled from %1 domain</source>
        <translation>您已退出%1域</translation>
    </message>
</context>
<context>
    <name>UnenrollInfoWidget</name>
    <message>
        <source>Unenrolling, please do not close this application</source>
        <translation>正在退出域管，请不要关闭应用</translation>
    </message>
    <message>
        <source>Before you exist, please make sure that the local user is enabled and you have the password of this user.</source>
        <translation>在退域前，请从小助手确认您的终端本地用户处于启用状态，并且您需要知道本地用户的密码才能登录到此计算机。</translation>
    </message>
    <message>
        <source>Click the ok button to continue</source>
        <translation>单击&quot;确定&quot;继续</translation>
    </message>
</context>
</TS>
