<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AuthWidget</name>
    <message>
        <source>Step 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auth</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <source>Step 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hostname can not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please input server address!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server not found!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Searching for server...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The client and server time is inconsistent, please correct the time first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Get infomation from server failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auth</source>
        <translation></translation>
    </message>
    <message>
        <source>Ip address is not allowed!</source>
        <translation></translation>
    </message>
    <message>
        <source>Server not found, please make sure your input is correct and check your dns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host name is the same as server!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfirmWidget</name>
    <message>
        <source>Finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation></translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enroll failed:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enroll failed, please check host.</source>
        <translation></translation>
    </message>
    <message>
        <source>Enroll failed, please check server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FinishedWidget</name>
    <message>
        <source>Reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enroll to domain succeed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstalledWidget</name>
    <message>
        <source>Server name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unenroll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dbus called failed:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unenrolled:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unenrolled failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure to unenroll?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unenrolling, please not close this application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <source>loading...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kim Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Caution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PwdChangeWidget</name>
    <message>
        <source>Password expired, change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Old password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reenter password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password change success.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Insufficient permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The current user does not have permissions, contact your administrator.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User authentication failed, please check the account and password!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User password expires, please change the password and try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User rights are insufficient, please contact the administrator!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hostname[%1] has already registered, please change the name or delete the record!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid format!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Request error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Request error: counts error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please input username!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please input password!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server request error:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authentication is abnormal, check the service status!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>authenting, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please input new password!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Confirmed password and new password do not match!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecoveryWidget</name>
    <message>
        <source>Recover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The last operation did not complete successfully, please recover!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnenrollAuthWidget</name>
    <message>
        <source>Please input username and password which has authority to unjoin the domain, and the click the continue button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnenrollFinishedWidget</name>
    <message>
        <source>Exit</source>
        <translation></translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation></translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation></translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation></translation>
    </message>
    <message>
        <source>You have already unrolled from %1 domain</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UnenrollInfoWidget</name>
    <message>
        <source>Unenrolling, please do not close this application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Before you exist, please make sure that the local user is enabled and you have the password of this user.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click the ok button to continue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
