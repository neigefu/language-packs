<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AuthWidget</name>
    <message>
        <source>Step 1</source>
        <translation>步驟1</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>步驟2</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>上一步</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <source>Authention</source>
        <translation type="vanished">账号认证</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>基礎配置</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>賬號認證</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <source>Step 1</source>
        <translation>步驟1</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>步驟2</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>用戶端主機名</translation>
    </message>
    <message>
        <source>Server</source>
        <translation>伺服器位址</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>上一步</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Hostname can not be empty!</source>
        <translation>用戶端主機名不能為空！</translation>
    </message>
    <message>
        <source>Please input server address!</source>
        <translation>請輸入服務端位址！</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <source>Server not found!</source>
        <translation>找不到伺服器資訊！</translation>
    </message>
    <message>
        <source>Please enter a valid server hostname or IP address!</source>
        <translation type="vanished">请输入有效的服务器主机名！</translation>
    </message>
    <message>
        <source>Searching for server...</source>
        <translation>尋找伺服器...</translation>
    </message>
    <message>
        <source>The client and server time is inconsistent, please correct the time first!</source>
        <translation>用戶端和服務端時間不一致，請先對時！</translation>
    </message>
    <message>
        <source>Get infomation from server failed!</source>
        <translation>找不到伺服器資訊！</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>基礎配置</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>賬號認證</translation>
    </message>
    <message>
        <source>Ip address is not allowed!</source>
        <translation>不允許使用IP位址加域！</translation>
    </message>
    <message>
        <source>Server not found, please make sure your input is correct and check your dns</source>
        <translation>找不到伺服器，請確保輸入的功能變數名稱正確且終端已正確的配置DNS</translation>
    </message>
    <message>
        <source>Host name is the same as server!</source>
        <translation>主機名不能和伺服器位址一樣！</translation>
    </message>
</context>
<context>
    <name>ConfirmWidget</name>
    <message>
        <source>Finished</source>
        <translation>完成配置</translation>
    </message>
    <message>
        <source>Server IP</source>
        <translation>伺服器位址</translation>
    </message>
    <message>
        <source>Server Name</source>
        <translation>伺服器主機名</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>用戶端主機名</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>上一步</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation>加入管控</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>正在加域中，請不要關閉應用！</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Enroll failed:%1</source>
        <translation>加域失敗：%1</translation>
    </message>
    <message>
        <source>Enroll failed, please check host.</source>
        <translation>加域失敗，請檢查主機是否可用。</translation>
    </message>
    <message>
        <source>Enroll failed, please check server.</source>
        <translation>加域失敗，請檢查服務是否正常。</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
</context>
<context>
    <name>FinishedWidget</name>
    <message>
        <source>Reboot</source>
        <translation>立即重啟</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>暫不重啟</translation>
    </message>
    <message>
        <source>Enroll to domain succeed</source>
        <translation>已成功加入域控系統</translation>
    </message>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation>退出成功</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>重啟失敗，請檢查當前是否有其他用戶登錄。</translation>
    </message>
</context>
<context>
    <name>InstalledWidget</name>
    <message>
        <source>Server IP</source>
        <translation type="vanished">服务器地址</translation>
    </message>
    <message>
        <source>Server name</source>
        <translation>伺服器主機名</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>用戶端主機名</translation>
    </message>
    <message>
        <source>Unenroll</source>
        <translation>退出管控</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Dbus called failed:%1</source>
        <translation>Dbus介面調用失敗：%1</translation>
    </message>
    <message>
        <source>Unenrolled:%1</source>
        <translation>退出管控：%1</translation>
    </message>
    <message>
        <source>Unenrolled failed: %1</source>
        <translation>退出管控失敗：%1</translation>
    </message>
    <message>
        <source>Are you sure to unenroll?</source>
        <translation>是否退出管控？</translation>
    </message>
    <message>
        <source>Unenrolling, please not close this application</source>
        <translation>正在退出域管，請不要關閉應用</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>配置資訊</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <source>loading...</source>
        <translation>拼命載入中...</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kim Client</source>
        <translation>加域用戶端</translation>
    </message>
    <message>
        <source>Caution</source>
        <translation>注意</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>正在操作中，請不要關閉應用！</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>PwdChangeWidget</name>
    <message>
        <source>Password expired, change password</source>
        <translation>密碼已過期，修改密碼</translation>
    </message>
    <message>
        <source>User</source>
        <translation>使用者</translation>
    </message>
    <message>
        <source>Old password</source>
        <translation>原密碼</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <source>Reenter password</source>
        <translation>確認密碼</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="obsolete">上一步</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="obsolete">加入管控</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>修改</translation>
    </message>
    <message>
        <source>Password change success.</source>
        <translation>密碼修改成功。</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Insufficient permissions</source>
        <translation>許可權不足</translation>
    </message>
    <message>
        <source>The current user does not have permissions, contact your administrator.</source>
        <translation>當前用戶沒有許可權，請聯繫管理員。</translation>
    </message>
    <message>
        <source>Server connection failed, please check the configuration!</source>
        <translation type="vanished">服务端连接失败，请检查参数配置！</translation>
    </message>
    <message>
        <source>User authentication failed, please check the account and password!</source>
        <translation>使用者驗證失敗，請檢查賬號和密碼！</translation>
    </message>
    <message>
        <source>Query for user failed, please check the configuration!</source>
        <translation type="vanished">用户信息查询失败，请检查连接配置！</translation>
    </message>
    <message>
        <source>User password expires, please change the password and try again!</source>
        <translation>用戶密碼過期，請修改密碼後重試！</translation>
    </message>
    <message>
        <source>Search for configure failed, please check the server!</source>
        <translation type="vanished">系统信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>User rights are insufficient, please contact the administrator!</source>
        <translation>用戶許可權不足，請聯繫管理員！</translation>
    </message>
    <message>
        <source>Hostname[%1] has already registered, please change the name or delete the record!</source>
        <translation>主機名[%1]已經加域，請更換主機名或者刪除服務端記錄！</translation>
    </message>
    <message>
        <source>Query for replica server failed, please check the server configuration!</source>
        <translation type="vanished">副本服务器信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Invalid format!</source>
        <translation>獲取數據異常，無效的數據格式！</translation>
    </message>
    <message>
        <source>Request error: %1</source>
        <translation>請求錯誤：%1</translation>
    </message>
    <message>
        <source>Request error: counts error!</source>
        <translation>請求錯誤：返回數量不匹配！</translation>
    </message>
    <message>
        <source>Please input username!</source>
        <translation>請輸入使用者名稱！</translation>
    </message>
    <message>
        <source>Please input password!</source>
        <translation>請輸入密碼！</translation>
    </message>
    <message>
        <source>Server request error:%1</source>
        <translation>伺服器請求錯誤：%1</translation>
    </message>
    <message>
        <source>Authentication is abnormal, check the service status!</source>
        <translation>認證異常，請檢查服務狀態！</translation>
    </message>
    <message>
        <source>authenting, please wait...</source>
        <translation>要求認證中，請稍後...</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>確認</translation>
    </message>
    <message>
        <source>Please input new password!</source>
        <translation>請輸出新密碼！</translation>
    </message>
    <message>
        <source>Confirmed password and new password do not match!</source>
        <translation>確認密碼和新密碼不一致！</translation>
    </message>
</context>
<context>
    <name>RecoveryWidget</name>
    <message>
        <source>Recover</source>
        <translation>恢復</translation>
    </message>
    <message>
        <source>exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>The last operation did not complete successfully, please recover!</source>
        <translation>上次操作未成功完成，請恢復！</translation>
    </message>
</context>
<context>
    <name>UnenrollAuthWidget</name>
    <message>
        <source>Please input username and password which has authority to unjoin the domain, and the click the continue button.</source>
        <translation>您正在執行退域操作，請輸入具備退域許可權的賬號，然後點擊繼續。</translation>
    </message>
    <message>
        <source>Input username</source>
        <translation>請輸入具備退域許可權的帳號</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation>請輸入密碼</translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation>域：</translation>
    </message>
</context>
<context>
    <name>UnenrollFinishedWidget</name>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation type="obsolete">退出成功</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>暫不重啟</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation>立即重啟</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>重啟失敗，請檢查當前是否有其他用戶登錄。</translation>
    </message>
    <message>
        <source>You have already unrolled from %1 domain</source>
        <translation>您已退出%1域</translation>
    </message>
</context>
<context>
    <name>UnenrollInfoWidget</name>
    <message>
        <source>Unenrolling, please do not close this application</source>
        <translation>正在退出域管，請不要關閉應用</translation>
    </message>
    <message>
        <source>Before you exist, please make sure that the local user is enabled and you have the password of this user.</source>
        <translation>在退域前，請從小助手確認您的終端本地用戶處於啟用狀態，並且您需要知道本地用戶的密碼才能登錄到此計算機。</translation>
    </message>
    <message>
        <source>Click the ok button to continue</source>
        <translation>按兩下確定繼續</translation>
    </message>
</context>
</TS>
