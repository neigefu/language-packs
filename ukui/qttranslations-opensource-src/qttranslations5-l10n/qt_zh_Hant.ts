<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2316"/>
        <source>Services</source>
        <translation>服務</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide %1</source>
        <translation>隱藏%1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Others</source>
        <translation>隱藏其他</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show All</source>
        <translation>全部顯示</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preferences...</source>
        <translation>偏好設置...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit %1</source>
        <translation>退出 %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About %1</source>
        <translation>關於 %1</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/audiooutput.cpp" line="+375"/>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt;音訊回放設備 &lt;b&gt;%1&lt;/b&gt; 沒有工作。 &lt;br/&gt;回滾到 &lt;b&gt;%2&lt;/b&gt;。 &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation>&lt;html&gt;切換到音訊重播設備 &lt;b&gt;%1&lt;/b&gt;，&lt;br/&gt;它剛剛變為可用並且具有更高的優先順序。 &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation>恢復到設備「%1」</translation>
    </message>
</context>
<context>
    <name>CloseButton</name>
    <message>
        <location filename="../src/gui/widgets/qtabbar.cpp" line="+2251"/>
        <source>Close Tab</source>
        <translation>關閉標籤頁</translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/phononnamespace.cpp" line="+55"/>
        <source>Notifications</source>
        <translation>通知</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Music</source>
        <translation>音樂</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video</source>
        <translation>視頻</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication</source>
        <translation>通訊</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Games</source>
        <translation>遊戲</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Accessibility</source>
        <translation>無障礙環境</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/backend.cpp" line="+171"/>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation>警告：看起來，您沒有安裝 gstreamer0.10-plugins-good 包。
    一些視頻特性已經被關閉。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation>警告：看起來，您沒有安裝基礎的 GStreamer 外掛程式。
    所有的音訊和視頻支援都已經被關閉。</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/mediaobject.cpp" line="+90"/>
        <source>Cannot start playback. 

Check your Gstreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation>不能開始重播。

請檢查您的 Gstreamer 安裝並且確認您
已經安裝 libgstreamer-plugins-base。</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation>缺少一個需要的解碼器。 您需要安裝如下解碼機來播放這個內容：%0</translation>
    </message>
    <message>
        <location line="+676"/>
        <location line="+8"/>
        <location line="+15"/>
        <location line="+9"/>
        <location line="+6"/>
        <location line="+19"/>
        <location line="+335"/>
        <location line="+24"/>
        <source>Could not open media source.</source>
        <translation>不能打開媒體源。</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>Invalid source type.</source>
        <translation>無效的源類型。</translation>
    </message>
    <message>
        <location line="+377"/>
        <source>Could not locate media source.</source>
        <translation>不能定位媒體源。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not open audio device. The device is already in use.</source>
        <translation>不能打開音訊設備。 這個設備正在被使用。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Could not decode media source.</source>
        <translation>不能解碼媒體源。</translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+42"/>
        <location line="+18"/>
        <source>Volume: %1%</source>
        <translation>音量：%1%</translation>
    </message>
    <message>
        <location line="-15"/>
        <location line="+18"/>
        <location line="+54"/>
        <source>Use this slider to adjust the volume. The leftmost position is 0%, the rightmost is %1%</source>
        <translation>請使用這個滑塊調節音量。 最左為%0，最右為%1%</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="+481"/>
        <source>%1, %2 not defined</source>
        <translation>%1，%2未定義</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Ambiguous %1 not handled</source>
        <translation>不明確的%1沒有被處理</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="+285"/>
        <source>True</source>
        <translation>真</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>假</translation>
    </message>
    <message>
        <location line="+505"/>
        <source>Insert</source>
        <translation>插入</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="+864"/>
        <source>Copy or Move a File</source>
        <translation>複製或者移動一個檔</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Read: %1</source>
        <translation>讀取：%1</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+30"/>
        <source>Write: %1</source>
        <translation>寫入：%1</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+1575"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="-157"/>
        <location line="+49"/>
        <location line="+2153"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_mac.cpp" line="+110"/>
        <source>All Files (*)</source>
        <translation>所有檔案 （*）</translation>
    </message>
    <message>
        <location line="-2089"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attributes</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+2031"/>
        <source>&amp;OK</source>
        <translation>確定（&amp;O）</translation>
    </message>
    <message>
        <location line="-1991"/>
        <source>Look &amp;in:</source>
        <translation>尋找範圍（&amp;I）：</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1981"/>
        <location line="+16"/>
        <source>File &amp;name:</source>
        <translation>檔案名稱（&amp;N）：</translation>
    </message>
    <message>
        <location line="-1996"/>
        <source>File &amp;type:</source>
        <translation>檔案類型（&amp;T）：</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>向上一級</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Create New Folder</source>
        <translation>創建新資料夾</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List View</source>
        <translation>清單檢視</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Detail View</source>
        <translation>詳細檢視</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Preview File Info</source>
        <translation>預覽文件資訊</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Preview File Contents</source>
        <translation>預覽文件內容</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Read-write</source>
        <translation>讀寫</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read-only</source>
        <translation>唯讀</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write-only</source>
        <translation>只寫</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inaccessible</source>
        <translation>不可訪問的</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Symlink to File</source>
        <translation>檔的系統連結</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Directory</source>
        <translation>目錄的系統連結</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Special</source>
        <translation>特殊的系統連結</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File</source>
        <translation>檔</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dir</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Special</source>
        <translation>特殊</translation>
    </message>
    <message>
        <location line="+704"/>
        <location line="+2100"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+337"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location line="-1990"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+84"/>
        <source>Save As</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location line="+642"/>
        <location line="+5"/>
        <location line="+355"/>
        <source>&amp;Open</source>
        <translation>開啟（&amp;O）</translation>
    </message>
    <message>
        <location line="-357"/>
        <location line="+341"/>
        <source>&amp;Save</source>
        <translation>儲存（&amp;S）</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>&amp;Rename</source>
        <translation>重新命名（&amp;R）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>刪除（&amp;D）</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>R&amp;eload</source>
        <translation>重新載入（&amp;E）</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Sort by &amp;Name</source>
        <translation>依名稱排列（&amp;N）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by &amp;Size</source>
        <translation>依大小排列（&amp;S）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort by &amp;Date</source>
        <translation>依日期排列（&amp;D）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Unsorted</source>
        <translation>未排列的（&amp;U）</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Sort</source>
        <translation>排列</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show &amp;hidden files</source>
        <translation>顯示隱藏檔案（&amp;H）</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>the file</source>
        <translation>檔</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the directory</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the symlink</source>
        <translation>系統連結</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete %1</source>
        <translation>刪除%1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;你確認你想刪除%1，“%2”？ &lt;/qt&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Yes</source>
        <translation>是（&amp;Y）</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;No</source>
        <translation>否（&amp;N）</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>New Folder 1</source>
        <translation>新建資料夾1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder</source>
        <translation>新建資料夾</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder %1</source>
        <translation>新建資料夾%1</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Find Directory</source>
        <translation>查找目錄</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+108"/>
        <source>Directories</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Directory:</source>
        <translation>目錄：</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+1110"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location line="-1109"/>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>檔%1
未找到。
請檢查路徑和檔名。</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="-289"/>
        <source>All Files (*.*)</source>
        <translation>所有檔案 （*.*）</translation>
    </message>
    <message>
        <location line="+375"/>
        <source>Open </source>
        <translation>打開 </translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Select a Directory</source>
        <translation>選擇一個目錄</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="+130"/>
        <location line="+10"/>
        <source>Could not read directory
%1</source>
        <translation>不能讀取目錄
%1</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Could not create directory
%1</source>
        <translation>不能創建目錄
%1</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Could not remove file or directory
%1</source>
        <translation>不能移除檔或者目錄
%1</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Could not rename
%1
to
%2</source>
        <translation>不能把
%1
重新命名為
%2</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Could not open
%1</source>
        <translation>不能打開
%1</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Could not write
%1</source>
        <translation>不能寫入
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="+2051"/>
        <source>Line up</source>
        <translation>排列</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Customize...</source>
        <translation>自訂...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <location filename="../src/qt3support/network/q3networkprotocol.cpp" line="+854"/>
        <source>Operation stopped by the user</source>
        <translation>操作被使用者停止</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3progressdialog.cpp" line="+224"/>
        <location line="+61"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="+189"/>
        <location line="+814"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Apply</source>
        <translation>應用</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Defaults</source>
        <translation>預設</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="+5429"/>
        <source>&amp;Undo</source>
        <translation>復原（&amp;U）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Redo</source>
        <translation>恢復（&amp;R）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cu&amp;t</source>
        <translation>剪下（&amp;T）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Copy</source>
        <translation>複製（&amp;C）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Paste</source>
        <translation>貼上（&amp;P）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>Select All</source>
        <translation>選擇全部</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="+246"/>
        <source>System</source>
        <translation>系統</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore up</source>
        <translation>向上恢復</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore down</source>
        <translation>向下恢復</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Contains commands to manipulate the window</source>
        <translation>包含操作視窗的命令。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a minimized back to normal</source>
        <translation>把一個最小化窗口恢復為普通狀態</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Moves the window out of the way</source>
        <translation>把視窗移到外面</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a maximized window back to normal</source>
        <translation>把一個最大化窗口恢復為普通狀態</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Makes the window full screen</source>
        <translation>視窗全屏化</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Closes the window</source>
        <translation>關閉視窗</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>顯示視窗名稱並且包含維護它的控制件</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <location filename="../src/qt3support/widgets/q3toolbar.cpp" line="+692"/>
        <source>More...</source>
        <translation>更多...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="+386"/>
        <location line="+260"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>協定「%1」不被支援</translation>
    </message>
    <message>
        <location line="-260"/>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>協定「%1」 不支援列出目錄</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>協定%1 不支援創建新目錄</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>協定%1 不支援移除檔或目錄</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>協定%1 不支援重新命名檔或目錄</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>協定%1 不支援獲取檔</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>協定%1 不支援上傳檔</translation>
    </message>
    <message>
        <location line="+243"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>協定%1 不支援複製或者行動檔案或目錄</translation>
    </message>
    <message>
        <location line="+237"/>
        <location line="+1"/>
        <source>(unknown)</source>
        <translation>（ 未知 ）</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="+177"/>
        <source>&amp;Cancel</source>
        <translation>取消（&amp;C）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 上一步（&amp;B）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Next &gt;</source>
        <translation>下一步（&amp;N） &gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Finish</source>
        <translation>完成（&amp;F）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Help</source>
        <translation>說明（&amp;H）</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../src/network/socket/qabstractsocket.cpp" line="+868"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+615"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+657"/>
        <location line="+26"/>
        <source>Host not found</source>
        <translation>主機未找到</translation>
    </message>
    <message>
        <location line="+50"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+3"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+4"/>
        <source>Connection refused</source>
        <translation>連接被拒絕</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Connection timed out</source>
        <translation>連接超時</translation>
    </message>
    <message>
        <location line="-547"/>
        <location line="+787"/>
        <location line="+208"/>
        <source>Operation on socket is not supported</source>
        <translation>Socket操作不被支援</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Socket operation timed out</source>
        <translation>套接字操作超時</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>Socket is not connected</source>
        <translation>套接字沒有被連接</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-8"/>
        <source>Network unreachable</source>
        <translation>網路不能訪問</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="+1199"/>
        <source>&amp;Step up</source>
        <translation>增加（&amp;S）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Step &amp;down</source>
        <translation>減少（&amp;D）</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Select All</source>
        <translation>選擇所有（&amp;S）</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+376"/>
        <source>Activate</source>
        <translation>啟動</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="+352"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>執行%1 需要Qt %2，只找到了Qt %3。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incompatible Qt Library Error</source>
        <translation>不相容的Qt錯誤</translation>
    </message>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+2"/>
        <source>Activates the program&apos;s main window</source>
        <translation>啟動這個程式的主視窗</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <location filename="../src/activeqt/container/qaxselect.ui"/>
        <source>Select ActiveX Control</source>
        <translation>選擇ActiveX控制件</translation>
    </message>
    <message>
        <location/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Cancel</source>
        <translation>取消（&amp;C）</translation>
    </message>
    <message>
        <location/>
        <source>COM &amp;Object:</source>
        <translation>COM物件（&amp;O）：</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+114"/>
        <source>Uncheck</source>
        <translation>取消選取</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Check</source>
        <translation>選中</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle</source>
        <translation>切換</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="+1253"/>
        <source>Hu&amp;e:</source>
        <translation>色調（&amp;E）：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Sat:</source>
        <translation>飽和度（&amp;S）：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Val:</source>
        <translation>亮度（&amp;V）：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Red:</source>
        <translation>紅色（&amp;R）：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Green:</source>
        <translation>綠色（&amp;G）：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bl&amp;ue:</source>
        <translation>藍色（&amp;U）：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A&amp;lpha channel:</source>
        <translation>Alpha通道（&amp;A）：</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Select Color</source>
        <translation>選擇顏色</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>&amp;Basic colors</source>
        <translation>基本顏色（&amp;B）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Custom colors</source>
        <translation>自訂顏色（&amp;C）</translation>
    </message>
    <message>
        <source>Cursor at %1, %2
Press ESC to cancel</source>
        <translation>游標在 %1， %2 位置
按ESC取消</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>新增到自訂顏色（&amp;A）</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">选择颜色</translation>
    </message>
    <message>
        <source>&amp;Pick Screen Color</source>
        <translation>選擇螢幕顏色（&amp;P）</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+1771"/>
        <location line="+65"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="+544"/>
        <source>False</source>
        <translation>假</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True</source>
        <translation>真</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+0"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：权限被拒绝</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：已经存在</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：不存在</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：资源耗尽了</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：未知错误 %2</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="+119"/>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1：鍵是空的</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1：不能製造鍵</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1：ftok 失敗</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+1276"/>
        <source>Unable to connect</source>
        <translation>不能連接</translation>
    </message>
    <message>
        <location line="+303"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>不能回滾事務</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to set autocommit</source>
        <translation>不能設置自動提交</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location line="-1043"/>
        <location line="+243"/>
        <source>Unable to execute statement</source>
        <translation>不能執行語句</translation>
    </message>
    <message>
        <location line="-206"/>
        <source>Unable to prepare statement</source>
        <translation>不能準備語句</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Unable to bind variable</source>
        <translation>不能幫定變數</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Unable to fetch record %1</source>
        <translation>不能獲取記錄%1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to fetch next</source>
        <translation>不能獲取下一個</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to fetch first</source>
        <translation>不能獲取第一個</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="+2295"/>
        <source>AM</source>
        <translation>AM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>am</source>
        <translation>am</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PM</source>
        <translation>PM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>pm</source>
        <translation>pm</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+951"/>
        <source>QDial</source>
        <translation>QDial</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SpeedoMeter</source>
        <translation>SpeedoMeter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SliderHandle</source>
        <translation>SliderHandle</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="+597"/>
        <source>What&apos;s This?</source>
        <translation>這是什麼？</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="+1866"/>
        <location line="+464"/>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+561"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+3"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Save</source>
        <translation>儲存（&amp;S）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Cancel</source>
        <translation>取消（&amp;C）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Close</source>
        <translation>關閉（&amp;C）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Apply</source>
        <translation>應用</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Don&apos;t Save</source>
        <translation>不保存</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Discard</source>
        <translation>拋棄</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Yes</source>
        <translation>是（&amp;Y）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes to &amp;All</source>
        <translation>全部是（&amp;A）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;No</source>
        <translation>否（&amp;N）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>N&amp;o to All</source>
        <translation>全部否（&amp;O）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save All</source>
        <translation>保存全部</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abort</source>
        <translation>放棄</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore Defaults</source>
        <translation>恢復預設</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Close without Saving</source>
        <translation>不保存關閉</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>&amp;OK</source>
        <translation>確定（&amp;O）</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+453"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>類型</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>類型</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Date Modified</source>
        <translation>修改日期</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="+1239"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dock</source>
        <translation>錨接</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Float</source>
        <translation>浮動</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-537"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>更少</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="+192"/>
        <source>Debug Message:</source>
        <translation>除錯訊息：</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning:</source>
        <translation>警告：</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Fatal Error:</source>
        <translation>致命錯誤：</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>&amp;Show this message again</source>
        <translation>再次顯示這個訊息（&amp;S）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;OK</source>
        <translation>確定（&amp;O）</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../src/corelib/io/qfile.cpp" line="+708"/>
        <location line="+141"/>
        <source>Destination file exists</source>
        <translation>目標檔已存在</translation>
    </message>
    <message>
        <location line="-108"/>
        <source>Cannot remove source file</source>
        <translation>無法刪除源檔</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Cannot open %1 for input</source>
        <translation>無法輸入 %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot open for output</source>
        <translation>無法輸出</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failure to write block</source>
        <translation>寫塊失敗</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot create %1 for output</source>
        <translation>無法建立 %1</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+515"/>
        <location line="+444"/>
        <source>All Files (*)</source>
        <translation>所有檔案 （*）</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Directories</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+50"/>
        <location line="+1471"/>
        <location line="+75"/>
        <source>&amp;Open</source>
        <translation>開啟（&amp;O）</translation>
    </message>
    <message>
        <location line="-1596"/>
        <location line="+50"/>
        <source>&amp;Save</source>
        <translation>儲存（&amp;S）</translation>
    </message>
    <message>
        <location line="-730"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location line="+1515"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1已經存在。
你想要替換它嗎？</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>檔%1
沒有找到。
請核實已給定正確檔名。</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+402"/>
        <source>My Computer</source>
        <translation>我的電腦</translation>
    </message>
    <message>
        <source>%1 File</source>
        <extracomment>%1 is a file name suffix, for example txt</extracomment>
        <translation>%1 檔案</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-1504"/>
        <source>&amp;Rename</source>
        <translation>重新命名（&amp;R）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>刪除（&amp;D）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show &amp;hidden files</source>
        <translation>顯示隱藏檔案（&amp;H）</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Parent Directory</source>
        <translation>父目錄</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>List View</source>
        <translation>清單檢視</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Detail View</source>
        <translation>詳細檢視</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Files of type:</source>
        <translation>檔案類型：</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+6"/>
        <location line="+648"/>
        <source>Directory:</source>
        <translation>目錄：</translation>
    </message>
    <message>
        <location line="+794"/>
        <location line="+862"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>目錄%1
沒有找到。
請核實已給定正確目錄名。</translation>
    </message>
    <message>
        <location line="-218"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>“%1”是寫保護的。
你還是想刪除它嗎？</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>你確認你想刪除「%1」？</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>你確認你想刪除「%1」？</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not delete directory.</source>
        <translation>不能刪除目錄。</translation>
    </message>
    <message>
        <location line="+407"/>
        <source>Recent Places</source>
        <translation>最近的地方</translation>
    </message>
    <message>
        <location line="-2550"/>
        <source>Save As</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="+411"/>
        <source>Drive</source>
        <translation>驅動器</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>File</source>
        <translation>檔</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unknown</source>
        <translation>未知的</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-4"/>
        <source>Find Directory</source>
        <translation>查找目錄</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Show </source>
        <translation>顯示 </translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+1970"/>
        <source>New Folder</source>
        <translation>新建資料夾</translation>
    </message>
    <message>
        <location line="-1963"/>
        <source>&amp;New Folder</source>
        <translation>新增資料夾（&amp;N）</translation>
    </message>
    <message>
        <location line="+656"/>
        <location line="+38"/>
        <source>&amp;Choose</source>
        <translation>選擇（&amp;C）</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qsidebar.cpp" line="+418"/>
        <source>Remove</source>
        <translation>拿掉</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-687"/>
        <location line="+652"/>
        <source>File &amp;name:</source>
        <translation>檔案名稱（&amp;N）：</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Look in:</source>
        <translation>檢視：</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Create New Folder</source>
        <translation>創建新資料夾</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_win.cpp" line="+160"/>
        <source>All Files (*.*)</source>
        <translation>所有檔案 （*.*）</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="+744"/>
        <source>%1 TB</source>
        <translation>%1 TB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 bytes</source>
        <translation>%1位元組</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Invalid filename</source>
        <translation>無效檔名</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt;名稱「%1」 不能被使用。 &lt;/b&gt;&lt;p&gt;請使用另外一個包含更少字元或者不含有標點符號的名稱。</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>類型</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>類型</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date Modified</source>
        <translation>修改日期</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="+234"/>
        <source>My Computer</source>
        <translation>我的電腦</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Computer</source>
        <translation>計算機</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 KB</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <location filename="../src/gui/text/qfontdatabase.cpp" line="+90"/>
        <location line="+1176"/>
        <source>Normal</source>
        <translation>普通</translation>
    </message>
    <message>
        <location line="-1173"/>
        <location line="+12"/>
        <location line="+1149"/>
        <source>Bold</source>
        <translation>粗體</translation>
    </message>
    <message>
        <location line="-1158"/>
        <location line="+1160"/>
        <source>Demi Bold</source>
        <translation>半粗體</translation>
    </message>
    <message>
        <location line="-1157"/>
        <location line="+18"/>
        <location line="+1135"/>
        <source>Black</source>
        <translation>黑體</translation>
    </message>
    <message>
        <location line="-1145"/>
        <source>Demi</source>
        <translation>半體</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1145"/>
        <source>Light</source>
        <translation>輕體</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1007"/>
        <source>Italic</source>
        <translation>義大利體</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1006"/>
        <source>Oblique</source>
        <translation>斜體</translation>
    </message>
    <message>
        <location line="+705"/>
        <source>Any</source>
        <translation>任意</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Latin</source>
        <translation>拉丁文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Greek</source>
        <translation>希臘文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cyrillic</source>
        <translation>西里爾文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Armenian</source>
        <translation>亞美尼亞文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hebrew</source>
        <translation>希伯來文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Arabic</source>
        <translation>阿拉伯文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Syriac</source>
        <translation>敘利亞文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thaana</source>
        <translation>瑪律地夫文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Devanagari</source>
        <translation>梵文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bengali</source>
        <translation>孟加拉文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gurmukhi</source>
        <translation>旁遮普文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gujarati</source>
        <translation>古吉拉特文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Oriya</source>
        <translation>奧里雅文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tamil</source>
        <translation>泰米爾文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Telugu</source>
        <translation>泰盧固文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kannada</source>
        <translation>埃納德文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Malayalam</source>
        <translation>馬拉亞拉姆文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sinhala</source>
        <translation>僧伽羅文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thai</source>
        <translation>泰國文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lao</source>
        <translation>老撾文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tibetan</source>
        <translation>藏文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Myanmar</source>
        <translation>緬甸文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Georgian</source>
        <translation>喬治亞文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Khmer</source>
        <translation>谷美爾文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Simplified Chinese</source>
        <translation>簡體中文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Traditional Chinese</source>
        <translation>繁體中文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Japanese</source>
        <translation>日文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Korean</source>
        <translation>韓文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vietnamese</source>
        <translation>越南文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol</source>
        <translation>符號</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ogham</source>
        <translation>歐甘文</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Runic</source>
        <translation>古北歐文</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="+772"/>
        <source>&amp;Font</source>
        <translation>字型（&amp;F）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font st&amp;yle</source>
        <translation>字型風格（&amp;Y）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>大小（&amp;S）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Effects</source>
        <translation>效果</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stri&amp;keout</source>
        <translation>刪除線（&amp;K）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Underline</source>
        <translation>底線（&amp;U）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sample</source>
        <translation>實例</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wr&amp;iting System</source>
        <translation>書寫系統（&amp;I）</translation>
    </message>
    <message>
        <location line="-604"/>
        <location line="+247"/>
        <source>Select Font</source>
        <translation>選擇字體</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../src/network/access/qftp.cpp" line="+826"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+683"/>
        <source>Not connected</source>
        <translation>沒有連接</translation>
    </message>
    <message>
        <location line="+65"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+65"/>
        <source>Host %1 not found</source>
        <translation>主機%1沒有找到</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+4"/>
        <source>Connection refused to host %1</source>
        <translation>連接被主機 %1 拒絕</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection timed out to host %1</source>
        <translation>主機%1連接超時</translation>
    </message>
    <message>
        <location line="+104"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+102"/>
        <location line="+1451"/>
        <source>Connected to host %1</source>
        <translation>連接到主機%1了</translation>
    </message>
    <message>
        <location line="+219"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-1290"/>
        <source>Connection refused for data connection</source>
        <translation>因為數據連接而被拒絕連接</translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+29"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+195"/>
        <location line="+728"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
    <message>
        <location line="+889"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+77"/>
        <source>Connecting to host failed:
%1</source>
        <translation>連線主機失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Login failed:
%1</source>
        <translation>登入失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Listing directory failed:
%1</source>
        <translation>列出目錄失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Changing directory failed:
%1</source>
        <translation>變更目錄失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Downloading file failed:
%1</source>
        <translation>下載檔案失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Uploading file failed:
%1</source>
        <translation>上傳檔案失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing file failed:
%1</source>
        <translation>移除檔案失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Creating directory failed:
%1</source>
        <translation>建立目錄失敗：
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing directory failed:
%1</source>
        <translation>移除目錄失敗：
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+25"/>
        <location line="+250"/>
        <source>Connection closed</source>
        <translation>連接關閉了</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-11"/>
        <source>Host %1 found</source>
        <translation>主機%1找到了</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to %1 closed</source>
        <translation>到%1的連線關閉了</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host found</source>
        <translation>主機找到了</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connected to host</source>
        <translation>連接到主機了</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2248"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>LTR</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_p.h" line="+183"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_unix.cpp" line="+178"/>
        <location line="+9"/>
        <location line="+64"/>
        <location line="+31"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="+180"/>
        <location line="+9"/>
        <location line="+40"/>
        <location line="+27"/>
        <source>Host not found</source>
        <translation>主機未找到</translation>
    </message>
    <message>
        <location line="-44"/>
        <location line="+39"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-34"/>
        <location line="+29"/>
        <source>Unknown address type</source>
        <translation>未知的地址類型</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-19"/>
        <location line="+27"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="+1574"/>
        <location line="+820"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+1160"/>
        <location line="+567"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
    <message>
        <location line="-568"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="-370"/>
        <source>Request aborted</source>
        <translation>請求被放棄了</translation>
    </message>
    <message>
        <location line="+579"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+381"/>
        <source>No server set to connect to</source>
        <translation>沒有設置要連接的伺服器</translation>
    </message>
    <message>
        <location line="+164"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+56"/>
        <source>Wrong content length</source>
        <translation>錯誤的內容長度</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+4"/>
        <source>Server closed connection unexpectedly</source>
        <translation>伺服器異常地關閉了連接</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Unknown authentication method</source>
        <translation>未知的身份驗證方法</translation>
    </message>
    <message>
        <location line="+183"/>
        <source>Error writing response to device</source>
        <translation>向設備中進行寫回復時發生錯誤</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+876"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+38"/>
        <source>Connection refused</source>
        <translation>連接被拒絕</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-304"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Host %1 not found</source>
        <translation>主機%1沒有找到</translation>
    </message>
    <message>
        <location line="+20"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+10"/>
        <location line="+19"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>HTTP request failed</source>
        <translation>HTTP請求失敗</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+69"/>
        <source>Invalid HTTP response header</source>
        <translation>無效的HTTP回應頭</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+48"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+40"/>
        <location line="+47"/>
        <source>Invalid HTTP chunked body</source>
        <translation>無效的HTTP臃腫體</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="+294"/>
        <source>Host %1 found</source>
        <translation>主機%1找到了</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host %1</source>
        <translation>連接到%1主機了</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection to %1 closed</source>
        <translation>到%1的連線關閉了</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Host found</source>
        <translation>主機找到了</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host</source>
        <translation>連接到主機了</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-22"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Connection closed</source>
        <translation>連接關閉了</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-135"/>
        <source>Proxy authentication required</source>
        <translation>代理需要認證</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Authentication required</source>
        <translation>需要認證</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Connection refused (or timed out)</source>
        <translation>連線被拒絕（或超時）</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+6"/>
        <source>Proxy requires authentication</source>
        <translation>代理需要驗證</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host requires authentication</source>
        <translation>主機需要驗證</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data corrupted</source>
        <translation>數據錯誤</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown protocol specified</source>
        <translation>所指定的協定是未知的</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SSL handshake failed</source>
        <translation>SSL 握手失敗</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-2263"/>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation>HTTPS 連接需要 SSL，但它沒有被編譯進來</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="-89"/>
        <source>Did not receive HTTP response from proxy</source>
        <translation>未收到代理的HTTP回應</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error parsing authentication request from proxy</source>
        <translation>解析代理的認證請求出錯</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Authentication required</source>
        <translation>需要認證</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Proxy denied connection</source>
        <translation>代理拒絕連接</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error communicating with HTTP proxy</source>
        <translation>和HTTP代理通訊時發生錯誤</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Proxy server not found</source>
        <translation>未找到代理伺服器</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection refused</source>
        <translation>代理連接被拒絕</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy server connection timed out</source>
        <translation>代理伺服器連接超時</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection closed prematurely</source>
        <translation>代理連接過早關閉</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="+1428"/>
        <source>Error opening database</source>
        <translation>打開資料庫錯誤</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Could not start transaction</source>
        <translation>不能開始事務</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to rollback transaction</source>
        <translation>不能回滾事務</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location line="-1097"/>
        <source>Unable to create BLOB</source>
        <translation>不能創建BLOB</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to write BLOB</source>
        <translation>不能寫入BLOB</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to open BLOB</source>
        <translation>不能打開BLOB</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to read BLOB</source>
        <translation>不能讀取BLOB</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+189"/>
        <source>Could not find array</source>
        <translation>不能找到陣列</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Could not get array data</source>
        <translation>不能得到數位數據</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Could not get query info</source>
        <translation>不能得到查詢資訊</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not start transaction</source>
        <translation>不能開始事務</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Could not allocate statement</source>
        <translation>不能分配語句</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not prepare statement</source>
        <translation>不能準備語句</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+7"/>
        <source>Could not describe input statement</source>
        <translation>不能描述輸入語句</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not describe statement</source>
        <translation>不能描述語句</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unable to close statement</source>
        <translation>不能關閉語句</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unable to execute query</source>
        <translation>不能執行查詢</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Could not fetch next item</source>
        <translation>不能獲取下一項</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Could not get statement info</source>
        <translation>不能得到語句資訊</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="+1869"/>
        <source>Permission denied</source>
        <translation>許可權被拒絕</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Too many open files</source>
        <translation>太多打開的檔</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No such file or directory</source>
        <translation>沒有這個檔或者目錄</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No space left on device</source>
        <translation>設備上沒有空間了</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qiodevice.cpp" line="+1536"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="+242"/>
        <source>XIM</source>
        <translation>XIM</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>XIM input method</source>
        <translation>XIM輸入法</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Windows input method</source>
        <translation>Windows輸入法</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mac OS X input method</source>
        <translation>Mac OS X輸入法</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qinputdialog.cpp" line="+223"/>
        <source>Enter a value:</source>
        <translation>輸入一個值：</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys： 不能载入%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys：不能卸载%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: 符号“%1”在%2（%3）没有被定义</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="+378"/>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation>不能映射“%1”：%2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>“%1”中的外掛程式驗證數據不匹配</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation>不能取消映射“%1”：%2</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>外掛程式“%1”使用了不相容的Qt庫。 (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation>外掛程式“%1”使用了不相容的Qt庫。 期待的構建鍵是“%2”，得到的卻是“%3”</translation>
    </message>
    <message>
        <location line="+340"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
    <message>
        <location line="-377"/>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+280"/>
        <source>The shared library was not found.</source>
        <translation>共用庫沒有被找到。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>檔%1 不是有效的Qt外掛程式。</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>外掛程式“%1”使用了不相容的Qt庫。 （不能混合使用庫的調試版本和發佈版本。 )</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_unix.cpp" line="+209"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+99"/>
        <source>Cannot load library %1: %2</source>
        <translation>無法載入庫%1：%2</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+26"/>
        <source>Cannot unload library %1: %2</source>
        <translation>無法卸載庫%1：%2</translation>
    </message>
    <message>
        <location line="+31"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+15"/>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation>無法解析%2中的符號% 1：% 3</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="+2680"/>
        <source>&amp;Undo</source>
        <translation>復原（&amp;U）</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>恢復（&amp;R）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>剪下（&amp;T）</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Copy</source>
        <translation>複製（&amp;C）</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Paste</source>
        <translation>貼上（&amp;P）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>選擇全部</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <location filename="../src/network/socket/qlocalserver.cpp" line="+226"/>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="+231"/>
        <source>%1: Name error</source>
        <translation>%1： 名稱錯誤</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="-8"/>
        <source>%1: Permission denied</source>
        <translation>%1：許可權被拒絕</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: Address in use</source>
        <translation>%1：位址正在被使用</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../src/network/socket/qlocalserver_win.cpp" line="+158"/>
        <source>%1: Unknown error %2</source>
        <translation>%1：未知錯誤 %2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <location filename="../src/network/socket/qlocalsocket_tcp.cpp" line="+132"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+134"/>
        <source>%1: Connection refused</source>
        <translation>%1：連接被拒絕</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Remote closed</source>
        <translation>%1：遠端已關閉</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+80"/>
        <location line="+43"/>
        <source>%1: Invalid name</source>
        <translation>%1：無效名稱</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket access error</source>
        <translation>%1：套接字訪問錯誤</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket resource error</source>
        <translation>%1：套接字資源錯誤</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket operation timed out</source>
        <translation>%1：套接字操作超時</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Datagram too large</source>
        <translation>%1：資料報太大</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="-48"/>
        <source>%1: Connection error</source>
        <translation>%1：連接錯誤</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: The socket operation is not supported</source>
        <translation>%1：套接字操作不被支援</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: Unknown error</source>
        <translation>%1：未知錯誤</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+4"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+10"/>
        <source>%1: Unknown error %2</source>
        <translation>%1：未知錯誤 %2</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="+1231"/>
        <source>Unable to open database &apos;</source>
        <translation>不能打開資料庫</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to connect</source>
        <translation>不能連接</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Unable to begin transaction</source>
        <translation>不能開始事務</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>不能回滾事務</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location line="-922"/>
        <source>Unable to fetch data</source>
        <translation>不能獲取數據</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Unable to execute query</source>
        <translation>不能執行查詢</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to store result</source>
        <translation>不能存儲結果</translation>
    </message>
    <message>
        <location line="+190"/>
        <location line="+8"/>
        <source>Unable to prepare statement</source>
        <translation>不能準備語句</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unable to reset statement</source>
        <translation>不能重置語句</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unable to bind value</source>
        <translation>不能綁定值</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to execute statement</source>
        <translation>不能執行語句</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+21"/>
        <source>Unable to bind outvalues</source>
        <translation>不能綁定外值</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Unable to store statement results</source>
        <translation>不能存儲語句結果</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>Unable to execute next query</source>
        <translation>不能執行下一個查詢</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to store next result</source>
        <translation>不能存儲下一個結果</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <location filename="../src/gui/widgets/qmdiarea.cpp" line="+290"/>
        <source>(Untitled)</source>
        <translation>（ 未命名的 ）</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="+280"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Restore Down</source>
        <translation>向下恢復</translation>
    </message>
    <message>
        <location line="+707"/>
        <source>&amp;Restore</source>
        <translation>恢復（&amp;R）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Move</source>
        <translation>移動（&amp;M）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>大小（&amp;S）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mi&amp;nimize</source>
        <translation>最小化（&amp;N）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>最大化（&amp;X）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on &amp;Top</source>
        <translation>總在最前（&amp;T）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation>關閉（&amp;C）</translation>
    </message>
    <message>
        <location line="-787"/>
        <source>- [%1]</source>
        <translation>- [%1]</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unshade</source>
        <translation>取消遮蔽</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shade</source>
        <translation>遮蔽</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Restore</source>
        <translation>恢復</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Menu</source>
        <translation>功能表</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="+157"/>
        <location line="+225"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location line="-224"/>
        <location line="+225"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location line="-223"/>
        <location line="+225"/>
        <location line="+51"/>
        <source>Execute</source>
        <translation>執行</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="obsolete">配置</translation>
    </message>
    <message>
        <source>Preference</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">选项</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="obsolete">设置</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="obsolete">安装</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="obsolete">关于%1</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">关于Qt</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="obsolete">退出%1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="-1111"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location line="-853"/>
        <location line="+852"/>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="-52"/>
        <location line="+8"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location line="+509"/>
        <source>About Qt</source>
        <translation>關於Qt</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用的是Qt %1版。&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="-1605"/>
        <source>Show Details...</source>
        <translation>顯示細節......</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Details...</source>
        <translation>隱藏細節......</translation>
    </message>
    <message>
        <location line="+1570"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;關於Qt&lt;/h3&gt;&lt;p&gt;此程式使用 Qt 版本 %1。 &lt;/p&gt;&lt;p&gt;Qt是用於跨平臺應用程式開發的C++工具組。 &lt;/p&gt;&lt;p&gt;Qt提供跨MS&amp;nbsp;Windows，Mac&amp;nbsp;OS&amp;nbsp;X，Linux和所有主要商業Unix變體的單一來源可移植性。 Qt也可用於嵌入式設備，如Qt for Embedded Linux和Qt for Windows CE。 &lt;/p&gt;&lt;p&gt;Qt有三種不同的許可選項，旨在滿足我們不同使用者的需求。 &lt;/p&gt;根據我們的商業許可協定授權的Qt適用於您不想與第三方共用任何原始程式碼或無法遵守GNU LGPL版本2.1或GNU GPL 版本3.0條款的專有/商業軟體的開發。 &lt;/p&gt;根據GNU LGPL 2.1版許可的Qt&lt;p&gt;適用於Qt應用程式（專有或開源）的開發，前提是您能遵守GNU LGPL 2.1版的條款和條件。 &lt;/p&gt;&lt;p&gt;根據GNU通用公共許可證3.0版許可的Qt適用於Qt應用程式的開發，如果您希望將此類應用程式與受GNU GPL 3.0版條款約束的軟體結合使用，或者您願意遵守GNU GPL 3.0版的條款。 &lt;/p&gt;&lt;p&gt;請參閱 &lt;a href=“http://qt.nokia.com/products/licensing”&gt;qt.nokia.com/products/licensing 瞭解&lt;/a&gt; Qt 許可的概述。 &lt;/p&gt;&lt;p&gt;版權所有 （C） 2012 諾基亞公司和/或其子公司。 &lt;/p&gt;&lt;p&gt;Qt是諾基亞的產品。 有關詳細資訊，請參閱 &lt;a href=“http://qt.nokia.com/”&gt;qt.nokia.com&lt;/a&gt;。 &lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokiae.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt也有用于嵌入式Linux和Windows CE的版本。&lt;/p&gt;&lt;p&gt;Qt是Nokia的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用了Qt %1开源版本。&lt;/p&gt;&lt;p&gt;Qt开源版本只用于开源应用程序的开发。如果要开发私有（闭源）软件，你需要一个商业的Qt协议。&lt;/p&gt;&lt;p&gt;有关Qt协议的概览，请参考&lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt Embedded.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt对于嵌入式平台也是可用的，在嵌入式平台上它被称为Qt Embedded。&lt;/p&gt;&lt;p&gt;Qt是Trolltech的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontext.cpp" line="+88"/>
        <source>Select IM</source>
        <translation>選擇輸入法</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="+95"/>
        <source>Multiple input method switcher</source>
        <translation>多輸入法切換器</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation>使用文字視窗部件上下文選單的多輸入法切換器</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qnativesocketengine.cpp" line="+206"/>
        <source>The remote host closed the connection</source>
        <translation>遠端主機關閉了這個連接</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network operation timed out</source>
        <translation>網路操作超時</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Out of resources</source>
        <translation>資源耗盡了</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unsupported socket operation</source>
        <translation>不被支援的套接字操作</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Protocol type not supported</source>
        <translation>協定類型不被支援</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid socket descriptor</source>
        <translation>無效的套接字描述符</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Network unreachable</source>
        <translation>網路不能訪問</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Permission denied</source>
        <translation>許可權被拒絕</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection timed out</source>
        <translation>連接超時</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection refused</source>
        <translation>連接被拒絕</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The bound address is already in use</source>
        <translation>要啟用的位址已經被使用</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is not available</source>
        <translation>這個位址不可用</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is protected</source>
        <translation>這個地址被保護了</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to send a message</source>
        <translation>不能發送一個消息</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to receive a message</source>
        <translation>不能接收一個消息</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to write</source>
        <translation>不能寫入</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network error</source>
        <translation>網路錯誤</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Another socket is already listening on the same port</source>
        <translation>另一個套接字已經正在監聽同一埠</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation>不能初始化非阻塞套接字</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to initialize broadcast socket</source>
        <translation>不能初始化廣播套接字</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>試圖在不支援IPv6支援的平臺上使用IPv6套接字</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Host unreachable</source>
        <translation>主機不能訪問</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Datagram was too large to send</source>
        <translation>不能發送過大的數據報</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Operation on non-socket</source>
        <translation>對非套接字操作</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>The proxy type is invalid for this operation</source>
        <translation>對於這個操作代理類型是無效的。</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesscachebackend.cpp" line="+65"/>
        <source>Error opening %1</source>
        <translation>打開%1發生錯誤</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessfilebackend.cpp" line="+99"/>
        <source>Request for opening non-local file %1</source>
        <translation>正在打開非本地檔案 %1 的請求</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error opening %1: %2</source>
        <translation>開啟 %1 錯誤：%2</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Write error writing to %1: %2</source>
        <translation>寫入 %1 錯誤：%2</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot open %1: Path is a directory</source>
        <translation>無法開啟 %1：路徑是一個目錄</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Read error reading from %1: %2</source>
        <translation>讀取 %1 錯誤：%2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessftpbackend.cpp" line="+165"/>
        <source>No suitable proxy found</source>
        <translation>未找到合適的代理</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot open %1: is a directory</source>
        <translation>無法讀取 %1：是一個目錄</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Logging in to %1 failed: authentication required</source>
        <translation>登入 %1 失敗：需要驗證</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Error while downloading %1: %2</source>
        <translation>下載 %1 時錯誤：%2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error while uploading %1: %2</source>
        <translation>上載 %1 時錯誤：%2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessHttpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesshttpbackend.cpp" line="+597"/>
        <source>No suitable proxy found</source>
        <translation>未找到合適的代理</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <location line="+128"/>
        <source>Error downloading %1 - server replied: %2</source>
        <translation>下載 %1 錯誤 - 伺服器回復：%2</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkreplyimpl.cpp" line="+68"/>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation>協定%1 是未知的</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <location line="+432"/>
        <location line="+22"/>
        <source>Operation canceled</source>
        <translation>操作被取消</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="+2069"/>
        <source>Unable to logon</source>
        <translation>不能登錄</translation>
    </message>
    <message>
        <location line="-144"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>不能初始化</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>Unable to begin transaction</source>
        <translation>不能開始事務</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to rollback transaction</source>
        <translation>不能回滾事務</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location line="-963"/>
        <location line="+161"/>
        <location line="+15"/>
        <source>Unable to bind column for batch execute</source>
        <translation>不能綁定批處理執行的列</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to execute batch statement</source>
        <translation>不能執行批處理語句</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>Unable to goto next</source>
        <translation>不能進入下一個</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Unable to alloc statement</source>
        <translation>不能分配語句</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to prepare statement</source>
        <translation>不能準備語句</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unable to bind value</source>
        <translation>不能綁定值</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">不能执行选择语句</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to execute statement</source>
        <translation>不能執行語句</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="+1785"/>
        <source>Unable to connect</source>
        <translation>不能連接</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation>不能連接—驅動程式不支援所有功能</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>Unable to disable autocommit</source>
        <translation>不能禁止自動提交</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>不能回滾事務</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to enable autocommit</source>
        <translation>不能打開自動提交</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location line="-1218"/>
        <location line="+349"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult：：reset： 不能把“SQL_CURSOR_STATIC”設置為語句屬性。 請檢查你的ODBC驅動程序設置。</translation>
    </message>
    <message>
        <location line="-332"/>
        <location line="+626"/>
        <source>Unable to execute statement</source>
        <translation>不能執行語句</translation>
    </message>
    <message>
        <location line="-555"/>
        <source>Unable to fetch next</source>
        <translation>不能獲取下一個</translation>
    </message>
    <message>
        <location line="+279"/>
        <source>Unable to prepare statement</source>
        <translation>不能準備語句</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Unable to bind variable</source>
        <translation>不能幫定變數</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+194"/>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-475"/>
        <location line="+578"/>
        <source>Unable to fetch last</source>
        <translation>不能獲取最後一個</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-672"/>
        <source>Unable to fetch</source>
        <translation>不能獲取</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Unable to fetch first</source>
        <translation>不能獲取第一個</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch previous</source>
        <translation>不能獲取上一個</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/gui/util/qdesktopservices_mac.cpp" line="+165"/>
        <source>Home</source>
        <translation>家</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdatabackend.cpp" line="+74"/>
        <source>Operation not supported on %1</source>
        <translation>在 %1 上不被支援的操作</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid URI: %1</source>
        <translation>無效的 URI：%1</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdebugpipebackend.cpp" line="+175"/>
        <source>Write error writing to %1: %2</source>
        <translation>寫入 %1 錯誤：%2</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Read error reading from %1: %2</source>
        <translation>讀取 %1 錯誤：%2</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Socket error on %1: %2</source>
        <translation>%1 上的套接字錯誤：%2</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation>遠端主機過早地關閉了在 %1 上的這個連接</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Protocol error: packet of size 0 received</source>
        <translation>協定錯誤：收到了大小為 0 的包</translation>
    </message>
    <message>
        <location filename="../src/network/kernel/qhostinfo.cpp" line="+177"/>
        <location line="+57"/>
        <source>No host name given</source>
        <translation>未指定主機名</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="+1195"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="+763"/>
        <source>Unable to connect</source>
        <translation>不能連接</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Could not begin transaction</source>
        <translation>不能開始事務</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Could not commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Could not rollback transaction</source>
        <translation>不能回滾事務</translation>
    </message>
    <message>
        <location line="+358"/>
        <source>Unable to subscribe</source>
        <translation>不能訂閱</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Unable to unsubscribe</source>
        <translation>不能取消訂閱</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location line="-1058"/>
        <source>Unable to create query</source>
        <translation>不能創建查詢</translation>
    </message>
    <message>
        <location line="+374"/>
        <source>Unable to prepare statement</source>
        <translation>不能準備語句</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="+304"/>
        <source>Centimeters (cm)</source>
        <translation>釐米 （cm）</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Millimeters (mm)</source>
        <translation>毫米 （mm）</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Inches (in)</source>
        <translation>英吋 （in）</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Points (pt)</source>
        <translation>點 （pt）</translation>
    </message>
    <message>
        <source>Pica (P̸)</source>
        <translation>派卡 （P̸）</translation>
    </message>
    <message>
        <source>Didot (DD)</source>
        <translation>迪多 （DD）</translation>
    </message>
    <message>
        <source>Cicero (CC)</source>
        <translation>西塞羅 （CC）</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupwidget.ui"/>
        <source>Form</source>
        <translation>表單</translation>
    </message>
    <message>
        <location/>
        <source>Paper</source>
        <translation>紙張</translation>
    </message>
    <message>
        <location/>
        <source>Page size:</source>
        <translation>紙張大小：</translation>
    </message>
    <message>
        <location/>
        <source>Width:</source>
        <translation>寬度：</translation>
    </message>
    <message>
        <location/>
        <source>Height:</source>
        <translation>高度：</translation>
    </message>
    <message>
        <location/>
        <source>Paper source:</source>
        <translation>紙張源：</translation>
    </message>
    <message>
        <location/>
        <source>Orientation</source>
        <translation>方向</translation>
    </message>
    <message>
        <location/>
        <source>Portrait</source>
        <translation>縱向</translation>
    </message>
    <message>
        <location/>
        <source>Landscape</source>
        <translation>橫向</translation>
    </message>
    <message>
        <location/>
        <source>Reverse landscape</source>
        <translation>反向橫向</translation>
    </message>
    <message>
        <location/>
        <source>Reverse portrait</source>
        <translation>反向縱向</translation>
    </message>
    <message>
        <location/>
        <source>Margins</source>
        <translation>邊距</translation>
    </message>
    <message>
        <location/>
        <source>top margin</source>
        <translation>上邊距</translation>
    </message>
    <message>
        <location/>
        <source>left margin</source>
        <translation>左邊距</translation>
    </message>
    <message>
        <location/>
        <source>right margin</source>
        <translation>右邊距</translation>
    </message>
    <message>
        <location/>
        <source>bottom margin</source>
        <translation>下邊距</translation>
    </message>
    <message>
        <location/>
        <source>Page Layout</source>
        <translation>頁面佈局</translation>
    </message>
    <message>
        <location/>
        <source>Pages per sheet:</source>
        <translation>每張頁數：</translation>
    </message>
    <message>
        <location/>
        <source>Page order:</source>
        <translation>列印順序：</translation>
    </message>
</context>
<context>
    <name>QCupsJobWidget</name>
    <message>
        <location/>
        <source>Job Control</source>
        <translation>工作控制</translation>
    </message>
    <message>
        <location/>
        <source>Scheduled printing:</source>
        <translation>定時列印</translation>
    </message>
    <message>
        <location/>
        <source>Print Immediately</source>
        <translation>立刻列印</translation>
    </message>
    <message>
        <location/>
        <source>Hold Indefinitely</source>
        <translation>立刻停止列印</translation>
    </message>
    <message>
        <location/>
        <source>Day (06:00 to 17:59)</source>
        <translation>白天 （06：00 to 17：59）</translation>
    </message>
    <message>
        <location/>
        <source>Night (18:00 to 05:59)</source>
        <translation>晚上 （18：00 to 05：59）</translation>
    </message>
    <message>
        <location/>
        <source>Second Shift (16:00 to 23:59)</source>
        <translation>第二班 （16：00 to 23：59）</translation>
    </message>
    <message>
        <location/>
        <source>Third Shift (00:00 to 07:59)</source>
        <translation>第三班 （00：00 to 07：59）</translation>
    </message>
    <message>
        <location/>
        <source>Weekend (Saturday to Sunday)</source>
        <translation>週末 （週六至周日）</translation>
    </message>
    <message>
        <location/>
        <source>Specific Time</source>
        <translation>指定時間</translation>
    </message>
    <message>
        <location/>
        <source>Billing information:</source>
        <translation>計費資訊：</translation>
    </message>
    <message>
        <location/>
        <source>Job priority:</source>
        <translation>優先權：</translation>
    </message>
    <message>
        <location/>
        <source>Banner Pages</source>
        <translation>橫幅頁面</translation>
    </message>
    <message>
        <location/>
        <source>Start:</source>
        <translation>開始：</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location/>
        <source>Standard</source>
        <translation>標準</translation>
    </message>
    <message>
        <location/>
        <source>Unclassified</source>
        <translation>未分類</translation>
    </message>
    <message>
        <location/>
        <source>Confidential</source>
        <translation>機密</translation>
    </message>
    <message>
        <location/>
        <source>Classified</source>
        <translation>已分類</translation>
    </message>
    <message>
        <location/>
        <source>Secret</source>
        <translation>秘密</translation>
    </message>
    <message>
        <location/>
        <source>Top Secret</source>
        <translation>絕密</translation>
    </message>
    <message>
        <location/>
        <source>End:</source>
        <translation>結束：</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+24"/>
        <source>Unknown error</source>
        <translation>未知的錯誤</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>The plugin was not loaded.</source>
        <translation>外掛程式沒有被載入。</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../src/gui/painting/qprinterinfo_unix.cpp" line="+98"/>
        <source>locally connected</source>
        <translation>本地已經連接的</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+225"/>
        <source>Aliases: %1</source>
        <translation>別名：%1</translation>
    </message>
    <message>
        <location line="+223"/>
        <location line="+199"/>
        <source>unknown</source>
        <translation>未知的</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="+375"/>
        <source>Print all</source>
        <translation>列印全部</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print selection</source>
        <translation>列印選擇</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print range</source>
        <translation>列印範圍</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation>A0 （841 x 1189 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1 (594 x 841 mm)</source>
        <translation>A1 （594 x 841 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2 (420 x 594 mm)</source>
        <translation>A2 （420 x 594 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3 (297 x 420 mm)</source>
        <translation>A3 （297 x 420 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation>A4 （210 x 297 毫米，8.26 x 11.7 英寸）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 (148 x 210 mm)</source>
        <translation>A5 （148 x 210 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6 (105 x 148 mm)</source>
        <translation>A6 （105 x 148 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7 (74 x 105 mm)</source>
        <translation>A7 （74 x 105 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8 (52 x 74 mm)</source>
        <translation>A8 （52 x 74 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9 (37 x 52 mm)</source>
        <translation>A9 （37 x 52 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation>B0 （1000 x 1414 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation>B1 （707 x 1000 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2 (500 x 707 mm)</source>
        <translation>B2 （500 x 707 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3 (353 x 500 mm)</source>
        <translation>B3 （353 x 500 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4 (250 x 353 mm)</source>
        <translation>B4 （250 x 353 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation>B5 （176 x 250 毫米，6.93 x 9.84 英寸）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6 (125 x 176 mm)</source>
        <translation>B6 （125 x 176 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7 (88 x 125 mm)</source>
        <translation>B7 （88 x 125 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8 (62 x 88 mm)</source>
        <translation>B8 （62 x 88 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9 (44 x 62 mm)</source>
        <translation>B9 （44 x 62 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10 (31 x 44 mm)</source>
        <translation>B10 （31 x 44 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E (163 x 229 mm)</source>
        <translation>C5E （163 x 229 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE (110 x 220 mm)</source>
        <translation>DLE （110 x 220 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation>Executive （7.5 x 10 英寸，191 x 254 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (210 x 330 mm)</source>
        <translation>Folio （210 x 330 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation>Ledger （432 x 279 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation>Legal （8.5 x 14 英寸，216 x 356 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation>Letter （8.5 x 11 英寸，216 x 279 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation>Tabloid （279 x 432 毫米）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation>美國普通10號信封 （105 x 241 毫米）</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+268"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qabstractprintdialog.cpp" line="+110"/>
        <location line="+13"/>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="-2"/>
        <source>Print</source>
        <translation>列印</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-357"/>
        <source>Print To File ...</source>
        <translation>列印到檔案......</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>檔%1不可寫。
請選擇不同的檔案名稱。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>%1已經存在。
你想覆蓋它嗎？</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="-210"/>
        <source>File exists</source>
        <translation>檔存在</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt;你想覆蓋它嗎？ &lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-8"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>%1是目錄。
請選擇不同的檔案名稱。</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+1"/>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation>“從”的數值不能大於“到”的數值。</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="-232"/>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8</source>
        <translation>A8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E</source>
        <translation>C5E</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE</source>
        <translation>DLE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive</source>
        <translation>決策文書</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio</source>
        <translation>對開紙</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger</source>
        <translation>帳頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal</source>
        <translation>法律文書</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter</source>
        <translation>信紙</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid</source>
        <translation>小型報紙</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope</source>
        <translation>美國普通10號信封</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Folio 紙</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom</source>
        <translation>自訂</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-522"/>
        <location line="+68"/>
        <source>&amp;Options &gt;&gt;</source>
        <translation>選項（&amp;O） &gt;&gt;</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>&amp;Print</source>
        <translation>列印（&amp;P）</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>&amp;Options &lt;&lt;</source>
        <translation>選項（&amp;O） &lt;&lt;</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>Print to File (PDF)</source>
        <translation>列印到檔案（PDF）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print to File (Postscript)</source>
        <translation>列印到檔案（Postscript）</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Local file</source>
        <translation>本地檔</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write %1 file</source>
        <translation>寫入 %1 檔案</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qabstractpagesetupdialog.cpp" line="+68"/>
        <location line="+12"/>
        <source>Page Setup</source>
        <translation>頁面設置</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpreviewdialog.cpp" line="+252"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Print Preview</source>
        <translation>列印預覽</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Next page</source>
        <translation>下一頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous page</source>
        <translation>上一頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First page</source>
        <translation>第一頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last page</source>
        <translation>最後一頁</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Fit width</source>
        <translation>適應寬度</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fit page</source>
        <translation>適應頁面</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Zoom in</source>
        <translation>放大</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom out</source>
        <translation>縮小</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Portrait</source>
        <translation>縱向</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Landscape</source>
        <translation>橫向</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show single page</source>
        <translation>顯示單頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show facing pages</source>
        <translation>顯示目前頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show overview of all pages</source>
        <translation>顯示所有頁的概覽</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Print</source>
        <translation>列印</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page setup</source>
        <translation>列印設置</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Export to PDF</source>
        <translation>匯出為PDF</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export to PostScript</source>
        <translation>匯出為PostScript</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertieswidget.ui"/>
        <source>Form</source>
        <translation>表單</translation>
    </message>
    <message>
        <location/>
        <source>Page</source>
        <translation>頁</translation>
    </message>
    <message>
        <location/>
        <source>Advanced</source>
        <translation>高級</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <location filename="../src/gui/dialogs/qprintsettingsoutput.ui"/>
        <source>Form</source>
        <translation>表單</translation>
    </message>
    <message>
        <location/>
        <source>Copies</source>
        <translation>拷貝</translation>
    </message>
    <message>
        <location/>
        <source>Print range</source>
        <translation>列印範圍</translation>
    </message>
    <message>
        <location/>
        <source>Print all</source>
        <translation>列印全部</translation>
    </message>
    <message>
        <location/>
        <source>Pages from</source>
        <translation>頁數從</translation>
    </message>
    <message>
        <location/>
        <source>to</source>
        <translation>到</translation>
    </message>
    <message>
        <location/>
        <source>Pages</source>
        <translation>頁面</translation>
    </message>
    <message>
        <location/>
        <source>Page Set:</source>
        <translation>頁面設定：</translation>
    </message>
    <message>
        <location/>
        <source>Selection</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location/>
        <source>Output Settings</source>
        <translation>輸出設置</translation>
    </message>
    <message>
        <location/>
        <source>Copies:</source>
        <translation>備份：</translation>
    </message>
    <message>
        <location/>
        <source>Collate</source>
        <translation>校對</translation>
    </message>
    <message>
        <location/>
        <source>Reverse</source>
        <translation>反向</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location/>
        <source>Color Mode</source>
        <translation>彩色模式</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>彩色</translation>
    </message>
    <message>
        <location/>
        <source>Grayscale</source>
        <translation>灰度</translation>
    </message>
    <message>
        <location/>
        <source>Duplex Printing</source>
        <translation>兩部分列印</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location/>
        <source>Long side</source>
        <translation>長側</translation>
    </message>
    <message>
        <location/>
        <source>Short side</source>
        <translation>短側</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintwidget.ui"/>
        <source>Form</source>
        <translation>表單</translation>
    </message>
    <message>
        <location/>
        <source>Printer</source>
        <translation>印表機</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>名稱（&amp;N）：</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;roperties</source>
        <translation>屬性（&amp;R）</translation>
    </message>
    <message>
        <location/>
        <source>Location:</source>
        <translation>位置：</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>預覽</translation>
    </message>
    <message>
        <location/>
        <source>Type:</source>
        <translation>類型：</translation>
    </message>
    <message>
        <location/>
        <source>Output &amp;file:</source>
        <translation>輸出檔案（&amp;F）：</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <location filename="../src/corelib/io/qprocess_unix.cpp" line="+475"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+147"/>
        <source>Could not open input redirection for reading</source>
        <translation>無法打開用於讀取的輸入重定向</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+36"/>
        <source>Could not open output redirection for writing</source>
        <translation>無法打開用於寫入的輸出重定向</translation>
    </message>
    <message>
        <location line="+235"/>
        <source>Resource error (fork failure): %1</source>
        <translation>資源錯誤（fork失敗）：%1</translation>
    </message>
    <message>
        <location line="+259"/>
        <location line="+53"/>
        <location line="+74"/>
        <location line="+67"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+422"/>
        <location line="+50"/>
        <location line="+75"/>
        <location line="+42"/>
        <location line="+54"/>
        <source>Process operation timed out</source>
        <translation>進程處理超時</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess.cpp" line="+533"/>
        <location line="+52"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-211"/>
        <location line="+50"/>
        <source>Error reading from process</source>
        <translation>從進程中讀取時發生錯誤</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+779"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+140"/>
        <source>Error writing to process</source>
        <translation>向行程寫入時發生錯誤</translation>
    </message>
    <message>
        <location line="-709"/>
        <source>Process crashed</source>
        <translation>進程已崩潰</translation>
    </message>
    <message>
        <location line="+912"/>
        <source>No program defined</source>
        <translation>未定義程式</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-341"/>
        <source>Process failed to start</source>
        <translation>啟動進程失敗</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprogressdialog.cpp" line="+182"/>
        <source>Cancel</source>
        <translation>恢復</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="-8"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <location line="+12"/>
        <source>Check</source>
        <translation>選中</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="+64"/>
        <source>no error occurred</source>
        <translation>沒有錯誤發生</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disabled feature used</source>
        <translation>使用了失效的特效</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad char class syntax</source>
        <translation>錯誤的字元類語法</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad lookahead syntax</source>
        <translation>錯誤的預測語法</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad repetition syntax</source>
        <translation>錯誤的重複語法</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid octal value</source>
        <translation>無效的八進位數值</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing left delim</source>
        <translation>找不到左分隔符</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end</source>
        <translation>意外的終止</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>met internal limit</source>
        <translation>遇到內部限制</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="+396"/>
        <source>Error to open database</source>
        <translation>打開資料庫錯誤</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to begin transaction</source>
        <translation>不能開始事務</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback Transaction</source>
        <translation>不能回滾事務</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location line="-323"/>
        <source>Unable to fetch results</source>
        <translation>不能獲取結果</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Unable to execute statement</source>
        <translation>不能執行語句</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="+528"/>
        <source>Error opening database</source>
        <translation>打開資料庫錯誤</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error closing database</source>
        <translation>關閉資料庫錯誤</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to begin transaction</source>
        <translation>不能開始事務</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to commit transaction</source>
        <translation>不能提交事務</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to rollback transaction</source>
        <translation>不能回滾事務</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location line="-400"/>
        <location line="+66"/>
        <location line="+8"/>
        <source>Unable to fetch row</source>
        <translation>不能獲取行</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Unable to execute statement</source>
        <translation>不能執行語句</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to reset statement</source>
        <translation>不能重置語句</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Unable to bind parameters</source>
        <translation>不能綁定參數</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter count mismatch</source>
        <translation>參數數量不匹配</translation>
    </message>
    <message>
        <location line="-208"/>
        <source>No query</source>
        <translation>沒有查詢</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="+448"/>
        <source>Scroll here</source>
        <translation>滾動到這裡</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left edge</source>
        <translation>左邊緣</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>頂部</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>右邊緣</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>底部</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page left</source>
        <translation>左一頁</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+143"/>
        <source>Page up</source>
        <translation>上一頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>右一頁</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+4"/>
        <source>Page down</source>
        <translation>下一頁</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scroll left</source>
        <translation>向左滾動</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>向上滾動</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>向右滾動</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>向下滾動</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-6"/>
        <source>Line up</source>
        <translation>向上排列</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Line down</source>
        <translation>向下排列</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory.cpp" line="+207"/>
        <source>%1: unable to set key on lock</source>
        <translation>%1：無法設定鎖定的鍵</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>%1: create size is less then 0</source>
        <translation>%1：創建的大小小於 0</translation>
    </message>
    <message>
        <location line="+168"/>
        <location filename="../src/corelib/kernel/qsharedmemory_p.h" line="+148"/>
        <source>%1: unable to lock</source>
        <translation>%1：無法鎖定</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1: unable to unlock</source>
        <translation>%1：無法取消鎖定</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_unix.cpp" line="+78"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+87"/>
        <source>%1: permission denied</source>
        <translation>%1：許可權被拒絕</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-22"/>
        <source>%1: already exists</source>
        <translation>%1：已經存在</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: doesn&apos;t exists</source>
        <translation>%1：不存在</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: out of resources</source>
        <translation>%1：資源耗盡了</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>%1：未知錯誤 %2</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>%1: key is empty</source>
        <translation>%1：鍵是空的</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1: unix key file doesn&apos;t exists</source>
        <translation>%1：Unix 鍵檔不存在</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1: ftok failed</source>
        <translation>%1：ftok 失敗</translation>
    </message>
    <message>
        <location line="+51"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+15"/>
        <source>%1: unable to make key</source>
        <translation>%1：不能製造鍵</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1: system-imposed size restrictions</source>
        <translation>%1：系統預設大小限制</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1: not attached</source>
        <translation>%1：沒有附加</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-27"/>
        <source>%1: invalid size</source>
        <translation>%1：無效大小</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>%1: key error</source>
        <translation>%1： 鍵錯誤</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>%1: size query failed</source>
        <translation>%1：大小查詢失敗</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="+373"/>
        <source>Space</source>
        <translation>空格</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab</source>
        <translation>Tab</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backtab</source>
        <translation>Backtab</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return</source>
        <translation>Return</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter</source>
        <translation>Enter</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pause</source>
        <translation>Pause</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>Print</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>End</source>
        <translation>End</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>Left</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Up</source>
        <translation>Up</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>Right</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Down</source>
        <translation>Down</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Down</source>
        <translation>調小音量</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Mute</source>
        <translation>靜音</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Up</source>
        <translation>調大音量</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Boost</source>
        <translation>低音增強</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Up</source>
        <translation>調大低音</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Down</source>
        <translation>調小低音</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Up</source>
        <translation>調大高音</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Down</source>
        <translation>調小高音</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Play</source>
        <translation>多媒體播放</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Stop</source>
        <translation>多媒體停止</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Previous</source>
        <translation>上一個多媒體</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Next</source>
        <translation>下一個多媒體</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Record</source>
        <translation>多媒體記錄</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Favorites</source>
        <translation>最喜愛的</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Standby</source>
        <translation>等待</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>打開URL</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Mail</source>
        <translation>啟動郵件</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Media</source>
        <translation>啟動多媒體</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (0)</source>
        <translation>啟動 （0）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (1)</source>
        <translation>啟動 （1）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (2)</source>
        <translation>啟動 （2）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (3)</source>
        <translation>啟動 （3）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (4)</source>
        <translation>啟動 （4）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (5)</source>
        <translation>啟動 （5）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (6)</source>
        <translation>啟動 （6）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (7)</source>
        <translation>啟動 （7）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (8)</source>
        <translation>啟動 （8）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (9)</source>
        <translation>啟動 （9）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (A)</source>
        <translation>啟動 （A）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (B)</source>
        <translation>啟動 （B）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (C)</source>
        <translation>啟動 （C）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (D)</source>
        <translation>啟動 （D）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (E)</source>
        <translation>啟動 （E）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (F)</source>
        <translation>啟動 （F）</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print Screen</source>
        <translation>Print Screen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Up</source>
        <translation>Page Up</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Down</source>
        <translation>Page Down</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caps Lock</source>
        <translation>Caps Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Num Lock</source>
        <translation>Num Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number Lock</source>
        <translation>Number Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Lock</source>
        <translation>Scroll Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert</source>
        <translation>Insert</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Escape</source>
        <translation>Escape</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Request</source>
        <translation>System Request</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Context1</source>
        <translation>上下文1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context2</source>
        <translation>上下文2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context3</source>
        <translation>上下文3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context4</source>
        <translation>上下文4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call</source>
        <translation>呼叫</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangup</source>
        <translation>掛起</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip</source>
        <translation>翻轉</translation>
    </message>
    <message>
        <location line="+527"/>
        <location line="+122"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+125"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+122"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+117"/>
        <source>Meta</source>
        <translation>Meta</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>F%1</source>
        <translation>F%1</translation>
    </message>
    <message>
        <location line="-720"/>
        <source>Home Page</source>
        <translation>主頁</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+151"/>
        <source>Page left</source>
        <translation>左一頁</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>上一頁</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Position</source>
        <translation>位置</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page right</source>
        <translation>右一頁</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>下一頁</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-67"/>
        <source>Connection to proxy refused</source>
        <translation>代理拒絕連接</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to proxy closed prematurely</source>
        <translation>代理連接過早關閉</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Proxy host not found</source>
        <translation>代理主機未找到</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Connection to proxy timed out</source>
        <translation>代理連接超時</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy authentication failed</source>
        <translation>代理認證失敗</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Proxy authentication failed: %1</source>
        <translation>代理認證失敗： %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>SOCKS version 5 protocol error</source>
        <translation>SOCKS版本5協議錯誤</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>General SOCKSv5 server failure</source>
        <translation>常規伺服器失敗</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation>連接不被SOCKSv5伺服器允許</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>TTL expired</source>
        <translation>TTL已過期</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>SOCKSv5 command not supported</source>
        <translation>不支援的SOCKSv5命令</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address type not supported</source>
        <translation>不支援的地址類型</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation>未知SOCKSv5代理，錯誤代碼0x%1</translation>
    </message>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">连接到套接字服务器的时候，Socks5超时错误</translation>
    </message>
    <message>
        <location line="+685"/>
        <source>Network operation timed out</source>
        <translation>網路操作超時</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-574"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>更少</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="+890"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete this record?</source>
        <translation>刪除這條記錄？</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Insert</source>
        <translation>插入</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save edits?</source>
        <translation>保存編輯？</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel your edits?</source>
        <translation>取消您的編輯？</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../src/network/ssl/qsslsocket_openssl.cpp" line="+569"/>
        <source>Unable to write data: %1</source>
        <translation>不能寫入數據：%1</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Error while reading: %1</source>
        <translation>讀取時錯誤：%1</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error during SSL handshake: %1</source>
        <translation>SSL握手錯誤：%1</translation>
    </message>
    <message>
        <location line="-524"/>
        <source>Error creating SSL context (%1)</source>
        <translation>建立SSL內容文錯誤（%1）</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>不合法或空白的密碼清單（%1）</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Error creating SSL session, %1</source>
        <translation>創建SSL會話錯誤，%1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Error creating SSL session: %1</source>
        <translation>創建SSL會話錯誤：%1</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>不能提供沒有鍵的證書，%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading local certificate, %1</source>
        <translation>不能載入本地證書，%1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error loading private key, %1</source>
        <translation>不能載入私有鍵，%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Private key does not certificate public key, %1</source>
        <translation>私有鍵不能驗證公有鍵，%1</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="-41"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+66"/>
        <source>%1: out of resources</source>
        <translation>%1：資源耗盡了</translation>
    </message>
    <message>
        <location line="-13"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+4"/>
        <source>%1: permission denied</source>
        <translation>%1：許可權被拒絕</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: already exists</source>
        <translation>%1：已經存在</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: does not exist</source>
        <translation>%1：不存在</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+3"/>
        <source>%1: unknown error %2</source>
        <translation>%1：未知錯誤 %2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="+584"/>
        <source>Unable to open connection</source>
        <translation>不能打開連接</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to use database</source>
        <translation>不能使用資料庫</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="-326"/>
        <source>Scroll Left</source>
        <translation>向左滾動</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll Right</source>
        <translation>向右滾動</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../src/network/socket/qtcpserver.cpp" line="+282"/>
        <source>Operation on socket is not supported</source>
        <translation>socket操作不被支援</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+1973"/>
        <source>&amp;Undo</source>
        <translation>復原（&amp;U）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Redo</source>
        <translation>恢復（&amp;R）</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cu&amp;t</source>
        <translation>剪下（&amp;T）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>複製（&amp;C）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copy &amp;Link Location</source>
        <translation>複製連結位置（&amp;L）</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>貼上（&amp;P）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>選擇全部</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+254"/>
        <location line="+6"/>
        <source>Press</source>
        <translation>按下</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+8"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <location filename="../src/network/socket/qudpsocket.cpp" line="+169"/>
        <source>This platform does not support IPv6</source>
        <translation>這個平臺不支援IPv6</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="+386"/>
        <source>Undo</source>
        <translation>撤銷</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Redo</source>
        <translation>恢復</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../src/gui/util/qundoview.cpp" line="+101"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt;空白&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="+834"/>
        <source>Undo</source>
        <translation>撤銷</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Redo</source>
        <translation>恢復</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+884"/>
        <source>LRM Left-to-right mark</source>
        <translation>LRM 從左到右標記</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLM Right-to-left mark</source>
        <translation>RLM 從右向左標記</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJ 零寬度連接器</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJ 零寬度非連接器</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWSP Zero width space</source>
        <translation>ZWSP 零寬度空格</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation>LRE 開始從左到右嵌入</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation>RLE 開始從右向左嵌入</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRO Start of left-to-right override</source>
        <translation>LRO 開始從左向右覆蓋</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLO Start of right-to-left override</source>
        <translation>RLO 開始從右向左覆蓋</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF Pop directional formatting</source>
        <translation>PDF 彈出方向格式</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert Unicode control character</source>
        <translation>插入Unicode控制字元</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/FrameLoaderClientQt.cpp" line="+692"/>
        <source>Request cancelled</source>
        <translation>請求被取消了</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Request blocked</source>
        <translation>請求被阻塞了</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot show URL</source>
        <translation>無法顯示URL</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frame load interruped by policy change</source>
        <translation>因為策略調整打斷了楨的載入</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot show mimetype</source>
        <translation>無法顯示 MIMETYPE</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File does not exist</source>
        <translation>檔不存在</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/network/qt/QNetworkReplyHandler.cpp" line="+382"/>
        <source>Bad HTTP request</source>
        <translation>錯誤的 HTTP 請求</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/Localizations.cpp" line="+42"/>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation>提交</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation>提交</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation>重置</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation>這是一個可以搜索的索引。 請輸入要搜尋的關鍵字： </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation>選擇檔案</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation>沒有檔案被選擇</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation>在新視窗中打開</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation>儲存連結...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation>複製連結</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation>打開圖片</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation>保存圖片</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation>複製圖片</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation>打開框架</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation>複製</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation>後退</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation>前進</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation>停止</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation>重新載入</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation>剪切</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation>粘貼</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation>沒有找到猜測</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation>忽略</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation>添加到字典</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation>搜索網頁</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation>在字典中查找</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation>打開連結</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation>忽略</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation>拼寫</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>顯示拼寫和語法</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>隱藏拼寫和語法</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation>檢查拼寫</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation>在輸入時檢查拼寫</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation>檢查語法和拼寫</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation>字體</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation>粗體</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation>義大利體</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation>下劃線</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation>輪廓</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation>方向</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Text Direction</source>
        <comment>Text direction context sub-menu item</comment>
        <translation>文本方向</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation>預設</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LTR</source>
        <comment>Left to Right context menu item</comment>
        <translation>LTR</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>RTL</source>
        <comment>Right to Left context menu item</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation>檢查</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation>沒有最近的搜索</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation>最近的搜索</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation>清除最近的搜索</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation>未知的</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation>% 1 （% 2x% 3 像素）</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/InspectorClientQt.cpp" line="+185"/>
        <source>Web Inspector - %2</source>
        <translation>網頁檢查員 - %2</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/ScrollbarQt.cpp" line="+58"/>
        <source>Scroll here</source>
        <translation>滾動到這裡</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Left edge</source>
        <translation>左邊緣</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>頂部</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>右邊緣</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>底部</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page left</source>
        <translation>左一頁</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>上一頁</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>右一頁</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>下一頁</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scroll left</source>
        <translation>向左滾動</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>向上滾動</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>向右滾動</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>向下滾動</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/FileChooserQt.cpp" line="+45"/>
        <source>%n file(s)</source>
        <comment>number of chosen file</comment>
        <translation>
            <numerusform>%n 個檔</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/Api/qwebpage.cpp" line="+1322"/>
        <source>JavaScript Alert - %1</source>
        <translation>JavaScript警告 - %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>JavaScript Confirm - %1</source>
        <translation>JavaScript確認 - %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>JavaScript Prompt - %1</source>
        <translation>JavaScript提示 - %1</translation>
    </message>
    <message>
        <location line="+333"/>
        <source>Move the cursor to the next character</source>
        <translation>移動游標到下一個字元</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous character</source>
        <translation>移動游標到上一個字元</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next word</source>
        <translation>移動游標到下一個單詞</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous word</source>
        <translation>移動游標到上一個單詞</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next line</source>
        <translation>移動游標到下一行</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous line</source>
        <translation>移動游標到上一行</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the line</source>
        <translation>移動游標到行首</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the line</source>
        <translation>移動游標到行尾</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the block</source>
        <translation>移動游標到塊首</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the block</source>
        <translation>移動游標到塊尾</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the document</source>
        <translation>移動游標到檔開頭</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the document</source>
        <translation>移動游標到檔末尾</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select all</source>
        <translation>全選</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next character</source>
        <translation>選中到下一個字元</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous character</source>
        <translation>選中到上一個字元</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next word</source>
        <translation>選中到下一個單詞</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous word</source>
        <translation>選中到上一個單詞</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next line</source>
        <translation>選中到下一行</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous line</source>
        <translation>選中到上一行</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the line</source>
        <translation>選中到行首</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the line</source>
        <translation>選中到行尾</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the block</source>
        <translation>選中到塊首</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the block</source>
        <translation>選中到塊尾</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the document</source>
        <translation>選取檔案首</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the document</source>
        <translation>選取檔案尾</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the start of the word</source>
        <translation>刪除到單詞首</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the end of the word</source>
        <translation>刪除到單詞尾</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Insert a new paragraph</source>
        <translation>插入新段落</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert a new line</source>
        <translation>插入新行</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../src/gui/kernel/qwhatsthis.cpp" line="+522"/>
        <source>What&apos;s This?</source>
        <translation>這是什麼？</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/gui/kernel/qwidget.cpp" line="+5326"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="+637"/>
        <source>Go Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Commit</source>
        <translation>提交</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Done</source>
        <translation>完成</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; 上一步（&amp;B）</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Finish</source>
        <translation>完成（&amp;F）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>說明（&amp;H）</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Next</source>
        <translation>下一步（&amp;N）</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Next &gt;</source>
        <translation>下一步（&amp;N） &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="+1094"/>
        <source>&amp;Restore</source>
        <translation>恢復（&amp;R）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Move</source>
        <translation>移動（&amp;M）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>大小（&amp;S）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mi&amp;nimize</source>
        <translation>最小化（&amp;N）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>最大化（&amp;X）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Close</source>
        <translation>關閉（&amp;C）</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stay on &amp;Top</source>
        <translation>總在最前（&amp;T）</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1059"/>
        <source>Sh&amp;ade</source>
        <translation>捲起（&amp;A）</translation>
    </message>
    <message>
        <location line="-278"/>
        <location line="+60"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="-1837"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore Down</source>
        <translation>恢復</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location line="+2053"/>
        <source>&amp;Unshade</source>
        <translation>展開（&amp;U）</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../src/xml/sax/qxml.cpp" line="+58"/>
        <source>no error occurred</source>
        <translation>沒有錯誤發生</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error triggered by consumer</source>
        <translation>由消費者出發的錯誤</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end of file</source>
        <translation>意外的文件終止</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>more than one document type definition</source>
        <translation>多於一個的文件類型定義</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing element</source>
        <translation>在解析元素的時候發生錯誤</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tag mismatch</source>
        <translation>標記不匹配</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing content</source>
        <translation>在解析內容的時候發生錯誤</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected character</source>
        <translation>意外的字元</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid name for processing instruction</source>
        <translation>無效的處理指令名稱</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>version expected while reading the XML declaration</source>
        <translation>在讀取XML聲明的時候，版本被期待</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>wrong value for standalone declaration</source>
        <translation>錯誤的獨立聲明的值</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>在讀取XML聲明的時候，編碼聲明或者獨立聲明被期待</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>在讀取XML聲明的時候，獨立聲明被期待</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing document type definition</source>
        <translation>在解析文件類型定義的時候發生錯誤</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>letter is expected</source>
        <translation>字元被期待</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing comment</source>
        <translation>在解析註釋的時候發生錯誤</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing reference</source>
        <translation>在解析參考的時候發生錯誤</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>在DTD中不允許使用內部解析的通用實體參考</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>在屬性值中不允許使用外部解析的通用實體參考</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>在DTD中不允許使用外部解析的通用實體參考</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unparsed entity reference in wrong context</source>
        <translation>沒有解析的錯誤上下文中的實體參考</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive entities</source>
        <translation>嵌套實體</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error in the text declaration of an external entity</source>
        <translation>在一個外部實體的文本聲明里有錯誤</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="+592"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+1769"/>
        <source>Extra content at end of document.</source>
        <translation>文檔末尾有額外內容。</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Invalid entity value.</source>
        <translation>無效的實體值。</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Invalid XML character.</source>
        <translation>無效的XML字元。</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>內容中不允許有“]]&gt;”序列。</translation>
    </message>
    <message>
        <location line="+309"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>命名空間的「%1」前置綴沒有被聲明</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Attribute redefined.</source>
        <translation>屬性重複定義。</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>在公有標識文本中有意外的字元“%1”。</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Invalid XML version string.</source>
        <translation>無效的XML版本字串。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported XML version.</source>
        <translation>不被支援的XML版本。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>%1是無效的編碼名稱。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Encoding %1 is unsupported</source>
        <translation>編碼%1不被支援。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Standalone accepts only yes or no.</source>
        <translation>獨立運行只允許是或者否。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation>在XML聲明中無效的屬性。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Premature end of document.</source>
        <translation>文檔過早的結束。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid document.</source>
        <translation>無效的文件。</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Expected </source>
        <translation>期待的 </translation>
    </message>
    <message>
        <location line="+11"/>
        <source>, but got &apos;</source>
        <translation>，但是得到的是”</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unexpected &apos;</source>
        <translation>意外的”</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Expected character data.</source>
        <translation>期待的字元數據。</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-995"/>
        <source>Recursive entity detected.</source>
        <translation>檢測到嵌套實體。</translation>
    </message>
    <message>
        <location line="+516"/>
        <source>Start tag expected.</source>
        <translation>開始期待的標記。</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>XML declaration not at start of document.</source>
        <translation>XML聲明沒有在文檔的開始位置。</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation>在參數實體聲明中有NDATA。</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>%1 是無效的處理指令名稱。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid processing instruction name.</source>
        <translation>無效的處理指令名稱。</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-521"/>
        <location line="+12"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+164"/>
        <location line="+53"/>
        <source>Illegal namespace declaration.</source>
        <translation>非法的命名空間聲明。</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+15"/>
        <source>Invalid XML name.</source>
        <translation>無效的XML名稱。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Opening and ending tag mismatch.</source>
        <translation>開始標記和結束標記不匹配。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>未解析實體“%1”的引用。</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+61"/>
        <location line="+40"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>實體%1 沒有被聲明。</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>在屬性值中的外部實體“%1”的引用。</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid character reference.</source>
        <translation>無效的字元引用。</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-75"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-823"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation>遇到不正確的編碼內容。</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>獨立運行偽屬性必須出現在編碼之後。</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+562"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>%1是一個無效的公有（PUBLIC）標識碼。</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreebuilder.cpp" line="+205"/>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation>帶有值 %2 的 %1 屬性已經聲明過了。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation>一個 %1 屬性必須帶有一個有效的 %2 作為值，但 %3 卻不是。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qiodevicedelegate.cpp" line="+84"/>
        <source>Network timeout.</source>
        <translation>網路超時。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+320"/>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation>元素 %1 不能被串行化，因為它出現在文檔元素之外。</translation>
    </message>
    <message>
        <source>Attribute element %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="obsolete">属性元素 %1 不能被串行化，因为它出现在最顶层。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractdatetime.cpp" line="+80"/>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation>%1 年是無效的，因為應該從 %2 開始。</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation>%1 日是在 %2...%3 範圍之外的。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation>%1 月是在 %2...%3 範圍之外的。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation>溢出：無法呈現資料 %1。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Day %1 is invalid for month %2.</source>
        <translation>%1 日對於 %2 月是無效的。</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation>時間 24：%1：%2.%3 是無效的。 小時是 24，但是分鐘、秒和毫秒不全為 0; </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation>時間 %1：%2：%3.%4 是無效的。</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation>溢出：數據無法被呈現。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractduration.cpp" line="+99"/>
        <location line="+15"/>
        <source>At least one component must be present.</source>
        <translation>至少有一個元件被呈現。</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation>至少一個時間元件必須出現在這個 %1 界限之後。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractfloatmathematician.cpp" line="+64"/>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation>在整數除法中沒有操作數，%1，可以是 %2。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation>除法中的第一個操作數，%1，不能是無窮（%2）。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation>除法中的第二個操作數，%1，不能是零（%2）。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qanyuri_p.h" line="+132"/>
        <source>%1 is not a valid value of type %2.</source>
        <translation>%1 不是類型為 %2 的有效值。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomiccasters_p.h" line="+223"/>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation>當從 %2 拋出到 %1 時，源值不能是 %3。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicmathematicians.cpp" line="+65"/>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation>整數除法（%1）除零（%2）是未定義的。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation>除法（%1）除零（%2）是未定義的。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation>求模除法（%1）除零（%2）是未定義的。</translation>
    </message>
    <message>
        <location line="+122"/>
        <location line="+32"/>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation>一個類型為 %1 的值除以 %2（不是一個數值）是不允許的。</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation>一個類型為 %1 的值除以 %2 或者 %3（正負零）是不允許的。</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation>一個類型為 %1 的值乘以 %2 或者 %3（正負無窮）是不允許的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicvalue.cpp" line="+79"/>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation>一個類型為 %1 的值不能是一個有效的布爾值（Effective Boolean Value）。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qboolean.cpp" line="+78"/>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation>有效的布爾值（Effective Boolean Value）不能被用於計算一個包含兩個或者更多原子值的序列。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qderivedinteger_p.h" line="+402"/>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation>類型為 %2 的值 %1 超過了最大值（%3）。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation>類型為 %2 的值 %1 超過了最小值（%3）。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qhexbinary.cpp" line="+91"/>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation>類型為 %1 的值必須包含偶數個數位。 值 %2 不是這樣的。</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 is not valid as a value of type %2.</source>
        <translation>%1 不是類型為 %2 的有效值。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qarithmeticexpression.cpp" line="+207"/>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation>操作元 %1 不能被用於類型 %2。</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation>操作元 %1 不能被用於類型為 %2 和 %3 的原子值。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qattributenamevalidator.cpp" line="+66"/>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation>一個被計算的屬性的名稱中的命名空間 URI 不能是 %1。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation>一個被計算的屬性的名稱不能使用帶有本地名稱 %2 的命名空間 URI %1。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastas.cpp" line="+88"/>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation>拋出類型錯誤，期望的是 %1，收到的是 %2。</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation>當拋出到 %1 或者它的派生類時，源類型必須是同一類型，或者它必須是一個字串類型。 類型 %2 是不被允許的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastingplatform.cpp" line="+134"/>
        <source>No casting is possible with %1 as the target type.</source>
        <translation>無法以 %1 為目標類型進行拋出。</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation>無法從 %1 拋出到 %2。</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation>無法拋出到 %1，因為它是一個抽象類型，並且因此無法被實例化。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation>無法從類型為 %2 的值 %1 拋出到 %3</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation>從 %2 拋出到 %1 失敗：%3</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcommentconstructor.cpp" line="+67"/>
        <source>A comment cannot contain %1</source>
        <translation>註釋不能包含 %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>A comment cannot end with a %1.</source>
        <translation>註釋不能以 %1 結尾。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomparisonplatform.cpp" line="+167"/>
        <source>No comparisons can be done involving the type %1.</source>
        <translation>對於類型 %1 不能進行比較。</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation>在類型 %2 和 %3 的原子值之間，操作符 %1 是不可用的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qdocumentcontentvalidator.cpp" line="+86"/>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation>一個屬性節點不能是一個文檔節點的子節點。 因此，這個屬性 %1 所在位置是不合適的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qexpressionfactory.cpp" line="+169"/>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation>一個庫模組不能被直接評估。 它必須從一個主模組中導入。</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>No template by name %1 exists.</source>
        <translation>沒有名為 %1 的範本存在。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qgenericpredicate.cpp" line="+106"/>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation>類型為 %1 的值不能被判斷。 一個判斷必須是數值類型或者一個有效的布爾值（Effective Boolean Value）類型。</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation>一個定位判斷必須評估一個單一數值。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qncnameconstructor_p.h" line="+113"/>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, is %2 invalid.</source>
        <translation>一個處理指令中的目標名稱不能是任何大小寫混合的 %1。 因此，%2 是無效的。</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation>%1 不是處理指令的有效目標名稱。 它必須是值 %2，例如 %3。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qpath.cpp" line="+109"/>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation>一個路徑中的最後一步必須包含節點或者原子值。 它不能是兩者的一個組合。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qprocessinginstructionconstructor.cpp" line="+84"/>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation>處理指令的數據不能包含字串 %1</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor.cpp" line="+82"/>
        <source>No namespace binding exists for the prefix %1</source>
        <translation>對於前綴 %1，沒有存在綁定的命名空間。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor_p.h" line="+156"/>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation>對於 %2 中的前置前置字碼 %1，沒有存在綁定的命名空間。</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+69"/>
        <source>%1 is an invalid %2</source>
        <translation>%1 是一個無效的 %2。</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/xmlpatterns/functions/qabstractfunctionfactory.cpp" line="+77"/>
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1 最多可以有 %n 個參數。 因此 %2 是無效的。</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1 需要至少 %n 個參數。 因此 %2 是無效的。</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qaggregatefns.cpp" line="+120"/>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation>%1 的第一個參數不能是類型 %2 的。 它必須是數字類型的，xs：yearMonthDuration 或者 xs：dayTimeDuration。</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1 的第一個參數不能是類型 %2 的。 它必須是類型 %3、%4 或者 %5 的。</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1 的第二個參數不能是類型 %2 的。 它必須是類型 %3、%4 或者 %5 的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qassemblestringfns.cpp" line="+88"/>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation>%1 不是一個有效的 XML 1.0 字元。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcomparingaggregator.cpp" line="+197"/>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation>%1 的第一個參數不能是類型 %2 的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qdatetimefn.cpp" line="+86"/>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation>如果兩個值都有區偏移（zone offset），它們必須擁有相同的區偏移。 %1 和 %2 的區偏移是不同的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qerrorfn.cpp" line="+61"/>
        <source>%1 was called.</source>
        <translation>%1 被調用了。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternmatchingfns.cpp" line="+94"/>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation>%1 必須被 %2 或者 %3 跟隨，不能在取代字串的末尾。</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation>在這個替換字串中，%1 在沒有被轉義的時候必須被至少一個數字跟隨。</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation>在這個替換字串中，%1 只能被用於轉義它本身或者 %2，而不是 %3</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternplatform.cpp" line="+92"/>
        <source>%1 matches newline characters</source>
        <translation>%1 符合了換行符</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation>%1 和 %2 匹配了一行的頭和尾。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Matches are case insensitive</source>
        <translation>匹配是大小寫不敏感的</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation>空白字元被移除了，除非當它們出現在字元類中</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation>%1 是正規表示式中的一個無效模式：%2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation>%1 是正則表示式中的一個無效標記。 有效標記為：</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+17"/>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation>如果第一個參數是空序列或者零長度字串（無命名空間），那麼就不能指定前綴。 前置綴 %1 被指定了。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencefns.cpp" line="+347"/>
        <source>It will not be possible to retrieve %1.</source>
        <translation>將不能獲取 %1。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcontextnodechecker.cpp" line="+54"/>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation>函數 %1 的第二個參數的根節點必須是一個文檔節點。 %2 不是一個文件節點。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencegeneratingfns.cpp" line="+279"/>
        <source>The default collection is undefined</source>
        <translation>默認收集（collection）是未定義的</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be retrieved</source>
        <translation>無法取得 %1</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qstringvaluefns.cpp" line="+252"/>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation>不支援正規化（normalization）表單 %1。 被支援的表單是 %2、%3、%4 和 %5，以及無，例如空字串（無正規化）。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qtimezonefns.cpp" line="+87"/>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation>區偏移（zone offset）必須在 %1...%2 範圍之內。 %3 是在範圍之外的。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 is not a whole number of minutes.</source>
        <translation>%1 不是分鐘的整數。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qcardinalityverifier.cpp" line="+58"/>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation>所需要的表間關係是 %1;得到的表間關係卻是 %2。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qitemverifier.cpp" line="+67"/>
        <source>The item %1 did not match the required type %2.</source>
        <translation>項 %1 和所需的類型 %2 不匹配。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+379"/>
        <location line="+7253"/>
        <source>%1 is an unknown schema type.</source>
        <translation>%1 是一個未知的方案類型。</translation>
    </message>
    <message>
        <location line="-6971"/>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation>只有一個 %1 的聲明可以出現在查詢序言中。</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>The initialization of variable %1 depends on itself</source>
        <translation>變數 %1 的初始化依賴於它本身</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>No variable by name %1 exists</source>
        <translation>沒有名稱為 %1 的變數存在。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qparsercontext.cpp" line="+93"/>
        <source>The variable %1 is unused</source>
        <translation>變數 %1 沒有被使用</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+2841"/>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation>不支援版本 %1。 被支援的 XQuery 版本是 1.0。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation>編碼方式 %1 是無效的。 它必須只包含拉丁字元，必須不包含空白符號，並且必須和正則表達式 %2 匹配。</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>No function with signature %1 is available</source>
        <translation>沒有簽署為 %1 的可用函數。</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+10"/>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation>預設命名空間聲明必須出現在函數、變數和選項聲明之前。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation>命名空間聲明必須出現在函數、變數和選項聲明之前。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation>模組導入不能出現在函數、變數和選項聲明之前。</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation>不能重複聲明前置綴 %1。</translation>
    </message>
    <message>
        <source>Only the prefix %1 can be declared to bind the namespace %2. By default, it is already bound to the prefix %1.</source>
        <translation type="obsolete">至于前缀 %1 可以被声明为和命名空间 %2 绑定。默认情况下，它已经被绑定到前缀 %1。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation>前綴 %1 在序言中已經聲明過了。</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation>一個選項的名稱必須帶有前綴。 對於選項沒有預設命名空間。</translation>
    </message>
    <message>
        <location line="+171"/>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation>不支援方案導入（Schema Import）特性，並且因此 %1 聲明不能出現。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation>%1 的目標命名空間不能為空。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The module import feature is not supported</source>
        <translation>不支援模組導入特性</translation>
    </message>
    <message>
        <source>A variable by name %1 has already been declared in the prolog.</source>
        <translation type="obsolete">名称为 %1 的变量已经在序言中声明过了。</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>No value is available for the external variable by name %1.</source>
        <translation>名稱為 %1 的外部變數並沒有可用的值。</translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty(try the predefined prefix %1 which exists for cases like this)</source>
        <translation type="obsolete">用户定义的函数的命名空间不能为空(请试试预定义的前缀 %1，它就是用于这种情况的)。</translation>
    </message>
    <message>
        <location line="-4154"/>
        <source>A construct was encountered which only is allowed in XQuery.</source>
        <translation>遇到了一個只允許在XQuery中出現的構造。</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>A template by name %1 has already been declared.</source>
        <translation>範本%1已被聲明</translation>
    </message>
    <message>
        <location line="+3581"/>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation>任何其他模式名稱不能出現關鍵字%1。</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The value of attribute %1 must of type %2, which %3 isn&apos;t.</source>
        <translation>屬性%1的值必須是類型%2，但%3不是。</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>The prefix %1 can not be bound. By default, it is already bound to the namespace %2.</source>
        <translation>前綴%1不能被綁定。 默認的，它已被綁定到名字空間%2。</translation>
    </message>
    <message>
        <location line="+312"/>
        <source>A variable by name %1 has already been declared.</source>
        <translation>變數%1已被聲明。</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation>樣式表函數必須有一個前綴名。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation>使用者定義函數的名字空間不能為空（試用為這種情況而存在的預定義前置綴%1）</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation>命名空間 %1 是保留的; 因此使用者定義的函數不能使用它。 請試試預定義的前置前置碼 %2，它就是用於這種情況的。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation>使用者在一個庫模組中定義的函數的命名空間必須和這個模組的命名空間一致。 也就是說，它應該是 %1，而不是 %2</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>A function already exists with the signature %1.</source>
        <translation>一個帶有簽名 %1 的函數已經存在。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation>不支援外部函數。 所有支援的函數必須可以被直接使用，不能把它們聲明為外部的</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>An argument by name %1 has already been declared. Every argument name must be unique.</source>
        <translation>名稱為 %1 的參數已經被聲明了。 每個參數名稱必須唯一。</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation>當函數%1被用於樣式匹配時，參數必須是變數參考或者字串。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation>在XSL-T樣式中，函數%1的第一個參數必須是字串，以便用於匹配。</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation>在XSL-T樣式中，函數%1的第一個參數必須是文字或者變數參考，以便用於匹配。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation>在XSL-T樣式中，函數%1不能有第三個參數。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation>在XSL-T樣式中，只用函數%1和%2可以用於匹配，%3不可以。</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation>在XSL-T仰視中，不能使用%1軸，只能使用%2軸或者%3軸。</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 is an invalid template mode name.</source>
        <translation>%1不是一個合法的範本模式名稱。</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation>一個在 for 運算式中綁定的變數的名稱必須和這個定位變數不同。 因此，這兩個名稱為 %1 的變數衝突。</translation>
    </message>
    <message>
        <location line="+758"/>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation>不支援方案驗證特性（Schema Validation Feature）。 因此，也許不能使用 %1 表達式。</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation>不支援任何編譯指示表達式（pragma expression）。 因此，必須呈現一個回調表達式（fallback expression）。</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation>每一個範本參數的名稱都必須是唯一的; %2是重複的。</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation>這個 %1 軸在 XQuery 中是不被支援的。</translation>
    </message>
    <message>
        <location line="+1150"/>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation>%1不是一個處理指令的合法名稱。</translation>
    </message>
    <message>
        <location line="-7029"/>
        <source>%1 is not a valid numeric literal.</source>
        <translation>%1 不是一個有效的數位語義。</translation>
    </message>
    <message>
        <location line="+6165"/>
        <source>No function by name %1 is available.</source>
        <translation>沒有名稱為 %1 的可用函數。</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation>當這個命名空間 URI 被綁定到一個前綴 %1 時，它不能是空字串。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 is an invalid namespace URI.</source>
        <translation>%1 是一個無效的命名空間 URI。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>It is not possible to bind to the prefix %1</source>
        <translation>無法綁定到這個前綴 %1。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>命名空間 %1 只能和 %2 綁定（並且如果是這種情況，需要提前聲明）。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>前綴 %1 只能和 %2 綁定（並且如果是這種情況，需要提前聲明）。</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation>兩個命名空間聲明屬性使用了相同的名稱：%1。</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation>命名空間 URI 必須是一個常量並且不能使用封閉的表達式。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>An attribute by name %1 has already appeared on this element.</source>
        <translation>一個名稱為 %1 的屬性已經出現在這個元素中了。</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation>一個直接元素構造器沒有很好地形成。 %1 後面跟著 %2。</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>The name %1 does not refer to any schema type.</source>
        <translation>名稱 %1 沒有指向任何方案類型。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation>%1 是一個複雜類型。 無法拋出到複雜類型。 因此，拋出到例如 %2 這樣的原子類型是可以的。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation>%1 不是原子類型。 只能拋出到原子類型。</translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction. Therefore this name test will never match.</source>
        <translation type="obsolete">%1 不是处理指令的有效名称。因此这个名称测试永远不会匹配。</translation>
    </message>
    <message>
        <location line="+145"/>
        <location line="+71"/>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation>%1 不是範圍內屬性聲明。 注意方案導入特性是不被支援的。</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation>一個擴展表達式的名稱必須在一個命名空間中。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qcardinality.cpp" line="+55"/>
        <source>empty</source>
        <translation>空白</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or one</source>
        <translation>零或者一</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>exactly one</source>
        <translation>確切地一</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>one or more</source>
        <translation>一或者更多</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or more</source>
        <translation>零或者更多</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qtypechecker.cpp" line="+63"/>
        <source>Required type is %1, but %2 was found.</source>
        <translation>需要的類型是 %1，但是找到的是 %2。</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation>把 %1 升級為 %2 會導致精度的損失。</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>The focus is undefined.</source>
        <translation>焦點未定義。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qoutputvalidator.cpp" line="+86"/>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation>不能在任何其它類型節點后添加屬性。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>An attribute by name %1 has already been created.</source>
        <translation>一個名稱為 %1 的屬性已經被創建。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qxpathhelper_p.h" line="+120"/>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation>只支援 Unicode 代碼點校驗（Unicode Codepoint Collation）（%1）。 %2 是不被支援的。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+60"/>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation>屬性 %1 不能被串行化，因為它出現在最頂層。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreeresourceloader.cpp" line="+314"/>
        <source>%1 is an unsupported encoding.</source>
        <translation>%1 是不被支援的編碼。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation>%1包含了在請求編碼%2中不允許的八進位值。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation>在使用編碼%3的%2中出現的代碼點%1不是一個有效的XML字元。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qapplytemplate.cpp" line="+119"/>
        <source>Ambiguous rule match.</source>
        <translation>含糊規則匹配。</translation>
    </message>
    <message>
        <source>In a namespace constructor, the value for a namespace value cannot be an empty string.</source>
        <translation type="obsolete">在一个命名空间构造中，命名空间的值不能为空字符串。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomputednamespaceconstructor.cpp" line="+69"/>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation>在命名空間構造函數中，命名空間的值不能為空字串。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation>前綴必須是有效的%1，而%2不是。</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The prefix %1 cannot be bound.</source>
        <translation>前綴%1不能被綁定。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation>只有前綴%1可以綁定到%2，反之也一樣</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qevaluationcache.cpp" line="+117"/>
        <source>Circularity detected</source>
        <translation>檢測到環</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qtemplate.cpp" line="+145"/>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation>需要參數%1，但是沒有提供對應的%2。</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation>參數%1已傳遞，但沒有相應的%2存在。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qunparsedtextfn.cpp" line="+65"/>
        <source>The URI cannot have a fragment</source>
        <translation>URI不能有片段</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+519"/>
        <source>Element %1 is not allowed at this location.</source>
        <translation>元素%1不能在這個位置。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Text nodes are not allowed at this location.</source>
        <translation>文本節點不能在這個位置。</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Parse error: %1</source>
        <translation>解析錯誤：%1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation>XSL-T版本屬性的值必須是%1類型的值，而%2不是。</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation>在XSL-T 2.0處理器中運行一個1.0的樣式表。</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Unknown XSL-T attribute %1.</source>
        <translation>未知的XSL-T屬性%1。</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation>屬性%1和%2彼此互斥。</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation>在一個簡化樣式表模組中，屬性%1必須存在。</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation>如果元素%1沒有屬性%2，那麼它也不能有屬性%3或者%4。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation>元素%1必須至少有屬性%2或者%3其中一個。</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation>在元素%2的%1屬性中至少要指定一個模式。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qmaintainingreader.cpp" line="+183"/>
        <source>Attribute %1 cannot appear on the element %2. Only the standard attributes can appear.</source>
        <translation>屬性%1不能出現在元素%2上。 只有標準屬性可以出現。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Attribute %1 cannot appear on the element %2. Only %3 is allowed, and the standard attributes.</source>
        <translation>屬性%1不能出現在元素%2上。 只有%3和標準屬性是允許的。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, %4, and the standard attributes.</source>
        <translation>屬性%1不能出現在元素%2上。 只有%3、%4和標準屬性是允許的。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, and the standard attributes.</source>
        <translation>屬性%1不能出現在元素%2上。 只有%3和標準屬性是允許的。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>XSL-T attributes on XSL-T elements must be in the null namespace, not in the XSL-T namespace which %1 is.</source>
        <translation>XSL-T元素中的XSL-T屬性必須放在空（null）命名空間中，而不是在XSL-T命名空間中，%1卻是這個樣子。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The attribute %1 must appear on element %2.</source>
        <translation>屬性%1必須出現在元素%2中。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The element with local name %1 does not exist in XSL-T.</source>
        <translation>有本地名稱%1的元素在XSL-T中不存在。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+123"/>
        <source>Element %1 must come last.</source>
        <translation>元素%1必須最後出現。</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one %1-element must occur before %2.</source>
        <translation>至少一個元素%1要出現在%2之前。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Only one %1-element can appear.</source>
        <translation>只能出現一個元素%1。</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>At least one %1-element must occur inside %2.</source>
        <translation>至少一個元素%1要出現在%2之內。</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation>當屬性%1出現在%2中時，不能使用順序構造。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation>元素%1必須有在一個%2屬性或者順序構造。</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation>當需要參數時，不能通過屬性%1或者順序構造提供預設值。</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>Element %1 cannot have children.</source>
        <translation>元素%1不能有子元素。</translation>
    </message>
    <message>
        <location line="+434"/>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation>元素%1不能有順序構造。</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+9"/>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation>屬性%1不能出現在%2中，因為它是%3的子元素。</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation>函數內的參數不能被聲明為通道（tunnel）。</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation>這個處理器不能感知Schema，因此%1不能被使用。</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation>頂級樣式表元素必須是在非空命名空間中的，而%1不是。</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation>元素%2中屬性%1的值必須是%3或者%4，而不是%5。</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation>屬性%1的值不能是%2。</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation>屬性%1只能出現在前%2個元素中。</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation>%2必須至少又一個子元素%1。</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+67"/>
        <source>Muted</source>
        <translation>已靜音</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+15"/>
        <source>Volume: %1%</source>
        <translation>音量：%1%</translation>
    </message>
</context>
<context>
    <name>WebCore::PlatformScrollbar</name>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">滚动到这里</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">左边缘</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">顶部</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">右边缘</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">底部</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">左一页</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">上一页</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">右一页</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">下一页</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">向左滚动</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">向上滚动</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">向右滚动</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">向下滚动</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>全部保存</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>是（&amp;Y）</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>全部是（&amp;A）</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>否（&amp;N）</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>全部否（&amp;O）</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>忽略</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>放棄</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>應用</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>恢復預設</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <source>&amp;OK</source>
        <translation>確定（&amp;O）</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>儲存（&amp;S）</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>取消（&amp;C）</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>關閉（&amp;C）</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>關閉且不保存</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>復原（&amp;U）</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>重做（&amp;R）</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>剪下（&amp;T）</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>複製（&amp;C）</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>複製連結位址（&amp;L）</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>貼上（&amp;P）</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>全部選擇</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>Printer Properties</source>
        <translation>印表機屬性</translation>
    </message>
    <message>
        <source>Job Options</source>
        <translation>工作選項</translation>
    </message>
    <message>
        <source>Page Setup Conflicts</source>
        <translation>頁面設定衝突</translation>
    </message>
    <message>
        <source>There are conflicts in page setup options. Do you want to fix them?</source>
        <translation>頁面設定中發現衝突選項，是否修復？</translation>
    </message>
    <message>
        <source>Advanced Option Conflicts</source>
        <translation>進階選項衝突</translation>
    </message>
    <message>
        <source>There are conflicts in some advanced options. Do you want to fix them?</source>
        <translation>部分進階選項發生衝突，是否修復？</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>Left to Right, Top to Bottom</source>
        <translation>左至右，上至下</translation>
    </message>
    <message>
        <source>Left to Right, Bottom to Top</source>
        <translation>左至右，下至上</translation>
    </message>
    <message>
        <source>Right to Left, Bottom to Top</source>
        <translation>右至左，下至上</translation>
    </message>
    <message>
        <source>Right to Left, Top to Bottom</source>
        <translation>右至左，上至下</translation>
    </message>
    <message>
        <source>Bottom to Top, Left to Right</source>
        <translation>下至上，左至右</translation>
    </message>
    <message>
        <source>Bottom to Top, Right to Left</source>
        <translation>下至上，右至左</translation>
    </message>
    <message>
        <source>Top to Bottom, Left to Right</source>
        <translation>上至下，左至右</translation>
    </message>
    <message>
        <source>Top to Bottom, Right to Left</source>
        <translation>上至下，右至左</translation>
    </message>
    <message>
        <source>1 (1x1)</source>
        <translation>1 (1x1)</translation>
    </message>
    <message>
        <source>2 (2x1)</source>
        <translation>2 (2x1)</translation>
    </message>
    <message>
        <source>4 (2x2)</source>
        <translation>4 (2x2)</translation>
    </message>
    <message>
        <source>6 (2x3)</source>
        <translation>6 (2x3)</translation>
    </message>
    <message>
        <source>9 (3x3)</source>
        <translation>9 (3x3)</translation>
    </message>
    <message>
        <source>16 (4x4)</source>
        <translation>16 (4x4)</translation>
    </message>
    <message>
        <source>All Pages</source>
        <translation>全部頁面</translation>
    </message>
    <message>
        <source>Odd Pages</source>
        <translation>奇數頁</translation>
    </message>
    <message>
        <source>Even Pages</source>
        <translation>偶數頁</translation>
    </message>
</context>
<context>
    <name>QPageSize</name>
    <message>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation>A8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <source>A10</source>
        <translation>A10</translation>
    </message>
    <message>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 in)</source>
        <translation>Executive 紙 （7.5 x 10 in）</translation>
    </message>
    <message>
        <source>Executive (7.25 x 10.5 in)</source>
        <translation>Executive 紙 （7.25 x 10.5 in）</translation>
    </message>
    <message>
        <source>Folio (8.27 x 13 in)</source>
        <translation>folio 紙</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation>法律專用紙</translation>
    </message>
    <message>
        <source>Letter / ANSI A</source>
        <translation>信紙 / ANSI A</translation>
    </message>
    <message>
        <source>Tabloid / ANSI B</source>
        <translation>小報 / ANSI B</translation>
    </message>
    <message>
        <source>Ledger / ANSI B</source>
        <translation>分類帳簿 / ANSI B</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>自訂</translation>
    </message>
    <message>
        <source>A3 Extra</source>
        <translation>A3 加大</translation>
    </message>
    <message>
        <source>A4 Extra</source>
        <translation>A4 加大</translation>
    </message>
    <message>
        <source>A4 Plus</source>
        <translation>A4 特大</translation>
    </message>
    <message>
        <source>A4 Small</source>
        <translation>A4 小號</translation>
    </message>
    <message>
        <source>A5 Extra</source>
        <translation>A5 加大</translation>
    </message>
    <message>
        <source>B5 Extra</source>
        <translation>B5 加大</translation>
    </message>
    <message>
        <source>JIS B0</source>
        <translation>B0(JIS)</translation>
    </message>
    <message>
        <source>JIS B1</source>
        <translation>B1(JIS)</translation>
    </message>
    <message>
        <source>JIS B2</source>
        <translation>B2(JIS)</translation>
    </message>
    <message>
        <source>JIS B3</source>
        <translation>B3(JIS)</translation>
    </message>
    <message>
        <source>JIS B4</source>
        <translation>B4(JIS)</translation>
    </message>
    <message>
        <source>JIS B5</source>
        <translation>B5(JIS)</translation>
    </message>
    <message>
        <source>JIS B6</source>
        <translation>B6(JIS)</translation>
    </message>
    <message>
        <source>JIS B7</source>
        <translation>B7(JIS)</translation>
    </message>
    <message>
        <source>JIS B8</source>
        <translation>B8(JIS)</translation>
    </message>
    <message>
        <source>JIS B9</source>
        <translation>B9(JIS)</translation>
    </message>
    <message>
        <source>JIS B10</source>
        <translation>B10(JIS)</translation>
    </message>
    <message>
        <source>ANSI C</source>
        <translation>ANSI C</translation>
    </message>
    <message>
        <source>ANSI D</source>
        <translation>ANSI D</translation>
    </message>
    <message>
        <source>ANSI E</source>
        <translation>ANSI E</translation>
    </message>
    <message>
        <source>Legal Extra</source>
        <translation>法律專用紙 加大</translation>
    </message>
    <message>
        <source>Letter Extra</source>
        <translation>信紙 加大</translation>
    </message>
    <message>
        <source>Letter Plus</source>
        <translation>信紙 特大</translation>
    </message>
    <message>
        <source>Letter Small</source>
        <translation>信紙 小號</translation>
    </message>
    <message>
        <source>Tabloid Extra</source>
        <translation>小報 加大</translation>
    </message>
    <message>
        <source>Architect A</source>
        <translation>圖紙 A</translation>
    </message>
    <message>
        <source>Architect B</source>
        <translation>圖紙 B</translation>
    </message>
    <message>
        <source>Architect C</source>
        <translation>圖紙 C</translation>
    </message>
    <message>
        <source>Architect D</source>
        <translation>圖紙 D</translation>
    </message>
    <message>
        <source>Architect E</source>
        <translation>圖紙E</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>筆記</translation>
    </message>
    <message>
        <source>Quarto</source>
        <translation>quarto 紙</translation>
    </message>
    <message>
        <source>Statement</source>
        <translation>statement 紙</translation>
    </message>
    <message>
        <source>Super A</source>
        <translation>Super A</translation>
    </message>
    <message>
        <source>Super B</source>
        <translation>Super B</translation>
    </message>
    <message>
        <source>Postcard</source>
        <translation>明信片</translation>
    </message>
    <message>
        <source>Double Postcard</source>
        <translation>雙明信片</translation>
    </message>
    <message>
        <source>PRC 16K</source>
        <translation>PRC 16K</translation>
    </message>
    <message>
        <source>PRC 32K</source>
        <translation>PRC 32K</translation>
    </message>
    <message>
        <source>PRC 32K Big</source>
        <translation>PRC 32K(Big)</translation>
    </message>
    <message>
        <source>Fan-fold US (14.875 x 11 in)</source>
        <translation>美國 Fan-fold紙</translation>
    </message>
    <message>
        <source>Fan-fold German (8.5 x 12 in)</source>
        <translation>德國 Fan-fold紙</translation>
    </message>
    <message>
        <source>Fan-fold German Legal (8.5 x 13 in)</source>
        <translation>德國法律專用紙 Fan-fold</translation>
    </message>
    <message>
        <source>Envelope B4</source>
        <translation>信封 B4</translation>
    </message>
    <message>
        <source>Envelope B5</source>
        <translation>信封 B5</translation>
    </message>
    <message>
        <source>Envelope B6</source>
        <translation>信封 B6</translation>
    </message>
    <message>
        <source>Envelope C0</source>
        <translation>信封 C0</translation>
    </message>
    <message>
        <source>Envelope C1</source>
        <translation>信封 C1</translation>
    </message>
    <message>
        <source>Envelope C2</source>
        <translation>信封 C2</translation>
    </message>
    <message>
        <source>Envelope C3</source>
        <translation>信封 C3</translation>
    </message>
    <message>
        <source>Envelope C4</source>
        <translation>信封 C4</translation>
    </message>
    <message>
        <source>Envelope C5</source>
        <translation>信封 C5</translation>
    </message>
    <message>
        <source>Envelope C6</source>
        <translation>信封 C6</translation>
    </message>
    <message>
        <source>Envelope C65</source>
        <translation>信封 C65</translation>
    </message>
    <message>
        <source>Envelope C7</source>
        <translation>信封 C7</translation>
    </message>
    <message>
        <source>Envelope DL</source>
        <translation>信封 DL</translation>
    </message>
    <message>
        <source>Envelope US 9</source>
        <translation>信封US 9</translation>
    </message>
    <message>
        <source>Envelope US 10</source>
        <translation>信封US 10</translation>
    </message>
    <message>
        <source>Envelope US 11</source>
        <translation>信封US 11</translation>
    </message>
    <message>
        <source>Envelope US 12</source>
        <translation>信封US 12</translation>
    </message>
    <message>
        <source>Envelope US 14</source>
        <translation>信封US 14</translation>
    </message>
    <message>
        <source>Envelope Monarch</source>
        <translation>信封 Monarch</translation>
    </message>
    <message>
        <source>Envelope Personal</source>
        <translation>信封 Personal</translation>
    </message>
    <message>
        <source>Envelope Chou 3</source>
        <translation>信封 Chou #3</translation>
    </message>
    <message>
        <source>Envelope Chou 4</source>
        <translation>信封 Chou #4</translation>
    </message>
    <message>
        <source>Envelope Invite</source>
        <translation>信封邀請函</translation>
    </message>
    <message>
        <source>Envelope Italian</source>
        <translation>意式信封</translation>
    </message>
    <message>
        <source>Envelope Kaku 2</source>
        <translation>信封 Kaku #2</translation>
    </message>
    <message>
        <source>Envelope Kaku 3</source>
        <translation>信封 Kaku #2</translation>
    </message>
    <message>
        <source>Envelope PRC 1</source>
        <translation>PRC 信封 #1</translation>
    </message>
    <message>
        <source>Envelope PRC 2</source>
        <translation>PRC 信封 #2</translation>
    </message>
    <message>
        <source>Envelope PRC 3</source>
        <translation>PRC 信封 #3</translation>
    </message>
    <message>
        <source>Envelope PRC 4</source>
        <translation>PRC 信封 #4</translation>
    </message>
    <message>
        <source>Envelope PRC 5</source>
        <translation>PRC 信封 #5</translation>
    </message>
    <message>
        <source>Envelope PRC 6</source>
        <translation>PRC 信封 #6</translation>
    </message>
    <message>
        <source>Envelope PRC 7</source>
        <translation>PRC 信封 #7</translation>
    </message>
    <message>
        <source>Envelope PRC 8</source>
        <translation>PRC 信封 #8</translation>
    </message>
    <message>
        <source>Envelope PRC 9</source>
        <translation>PRC 信封 #9</translation>
    </message>
    <message>
        <source>Envelope PRC 10</source>
        <translation>PRC 信封 #10</translation>
    </message>
    <message>
        <source>Envelope You 4</source>
        <translation>信封You#4</translation>
    </message>
</context>
</TS>
