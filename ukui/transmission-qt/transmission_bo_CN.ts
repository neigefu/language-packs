<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../AboutDialog.ui" line="+14"/>
        <source>About Transmission</source>
        <translation>BTཕབ་ལེན་ཡོ་བྱད་དང་འབྲེལ་བ།</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cc" line="+29"/>
        <source>&lt;b style=&apos;font-size:x-large&apos;&gt;Transmission %1&lt;/b&gt;</source>
        <translation>&lt;b style=&apos;font-size:x-large&apos;&gt; ཕབ་ལེན་ཡོ་བྱད་ %1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="+26"/>
        <source>A fast and easy BitTorrent client</source>
        <translation>BitTorrentམགྱོགས་མྱུར་དང་སྟབས་བདེ་བའི་མཁོ་མཁན་ཕྱོགས་ཤིག</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copyright (c) The Transmission Project</source>
        <translation>Copyright (c) The BTཕབ་ལེན་ཡོ་བྱད Project</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cc" line="+4"/>
        <source>C&amp;redits</source>
        <translation>བཀའ་དྲིན་ཆེ།(&amp;R)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;License</source>
        <translation>ཆོག་འཐུས་གྲོས་མཐུན།(&amp;L)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Credits</source>
        <translation>ཡིད་རྟོན་དངུལ་བུན།</translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../Application.cc" line="+281"/>
        <source>&lt;b&gt;Transmission is a file sharing program.&lt;/b&gt;</source>
        <translation>&lt;b&gt; བརྒྱུད་སྐྱེལ་ནི་ཡིག་ཆ་མཉམ་སྤྱོད་ཀྱི་གོ་རིམ་ཞིག་ཡིན། &lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>When you run a torrent, its data will be made available to others by means of upload. Any content you share is your sole responsibility.</source>
        <translation>ཁྱོད་ཀྱིས་Torrentཞིག་འཁོར་སྐྱོད་བྱེད་པའི་སྐབས་སུ།དེའི་གཞི་གྲངས་བརྒྱུད་སྐུར་གྱི་རྣམ་པས་མི་གཞན་ལ་འདོན་སྤྲོད་བྱེད་སྲིད།ཁྱེད་ཀྱིས་ཁྱེད་ཀྱིས་མཉམ་སྤྱོད་བྱས་པའི་ནང་དོན་ལ་འགན་གཅིག་པུ་འཁུར་དགོས།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>I &amp;Agree</source>
        <translation>ང་འཐད་པ་ཡིན།(&amp;A)</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Torrent Completed</source>
        <translation>Torrent་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Torrent Added</source>
        <translation>Torrent ཁ་གསབ་བྱས།</translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../DetailsDialog.cc" line="+364"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mixed</source>
        <translation>མཉམ་བསྲེས་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+218"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location line="-179"/>
        <source>Finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paused</source>
        <translation>མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Active now</source>
        <translation>ད་ལྟ་བྱ་འགུལ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 ago</source>
        <translation>%1སྔོན་གྱི་གནས་ཚུལ།</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%1 (%Ln pieces @ %2)</source>
        <translation>
            <numerusform>%1 (%Ln དུམ་བུ@ %2)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%1 (%Ln pieces)</source>
        <translation>
            <numerusform>%1 (%Ln དུམ་བུ་ )</numerusform>
        </translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Private to this tracker -- DHT and PEX disabled</source>
        <translation>མིག་སྔར་སྒེར་ལ་དབང་བའི་ Tracker སྒྲིག་བཀོད -- DHT དངPEXབཀག་སྡོམ་བྱས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Public torrent</source>
        <translation>སྤྱི་པའི་Torrent</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Created by %1</source>
        <translation>%1ཡིས་གསར་སྐྲུན་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created on %1</source>
        <translation>%1ནས་གསར་སྐྲུན་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created by %1 on %2</source>
        <translation>%2་ནས་གསར་སྐྲུན་བྱས་པའི་%1</translation>
    </message>
    <message>
        <location line="+123"/>
        <location line="+23"/>
        <source>Encrypted connection</source>
        <translation>གསང་བའི་འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Optimistic unchoke</source>
        <translation>སྤྲོ་སྣང་གིས་ཟྭ་འབྱེད་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Downloading from this peer</source>
        <translation>ལས་རིགས་གཅིག་པའི་ས་ནས་ཕབ་ལེན་བྱས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We would download from this peer if they would let us</source>
        <translation>གལ་ཏེ་ཁོ་ཚོ་འཐད་པ་ཡིན་ན།ང་ཚོས་མཛད་སྤྱོད་པ་འདི་ནས་ཕབ་ལེན་བྱེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uploading to peer</source>
        <translation>སྤྱོད་མཁན་དེ་ལ་བརྒྱུད་བསྐུར་བྱེད་བཞིན་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We would upload to this peer if they asked</source>
        <translation>གལ་ཏེ་ཁོ་ཚོས་འདྲི་རྩད་བྱས་ན་ང་ཚོས་ལས་རིགས་གཅིག་པའི་མི་འདི་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer has unchoked us, but we&apos;re not interested</source>
        <translation>ཕའེ་ཕའེ་ཡིས་ང་ཚོར་ཁ་ཡ་མི་བྱེད་མོད། འོན་ཀྱང་ང་ཚོར་སྤྲོ་སྣང་མེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We unchoked this peer, but they&apos;re not interested</source>
        <translation>ང་ཚོས་ལས་རིགས་གཅིག་པའི་མི་འདི་ལ་ཁ་ཡ་མི་བྱེད་མོད། འོན་ཀྱང་ཁོ་ཚོར་སྤྲོ་སྣང་མེད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Peer was discovered through DHT</source>
        <translation>ལས་རིགས་གཅིག་པའི་མི་ནི་DHTབརྒྱུད་ནས་རྙེད་པ་ཡིན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer was discovered through Peer Exchange (PEX)</source>
        <translation>ལས་རིགས་གཅིག་པའི་བརྗེ་རེས་(PEX)བརྒྱུད་ནས་ཤེས་རྟོགས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer is an incoming connection</source>
        <translation>སྤྱོད་མཁན་ནི་འབབ་ཚིགས་སུ་ཞུགས་པའི་སྦྲེལ་མཐུད་ཅིག་ཡིན།</translation>
    </message>
    <message numerus="yes">
        <location line="+120"/>
        <source> minute(s)</source>
        <extracomment>Spin box suffix, &quot;Stop seeding if idle for: [ 5 minutes ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> སྐར་མ་གཅིག</numerusform>
        </translation>
    </message>
    <message>
        <location line="+45"/>
        <location line="+12"/>
        <location line="+34"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Tracker already exists.</source>
        <translation>Tracker་ཡོད་པ་རེད།</translation>
    </message>
    <message>
        <location line="-628"/>
        <source>%1 (100%)</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded and verified data</extracomment>
        <translation>%1 (100%)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 of %2 (%3%)</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded and verified data, %2 is overall size of torrent data, %3 is percentage (%1/%2*100)</extracomment>
        <translation>%1 %2ཡི་ %3</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 of %2 (%3%), %4 Unverified</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded data (both verified and unverified), %2 is overall size of torrent data, %3 is percentage (%1/%2*100), %4 is amount of downloaded but not yet verified data</extracomment>
        <translation>%1 %2 (%3%),%4 ཞིབ་བཤེར་མ་བྱས།</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>%1 (%2 corrupt)</source>
        <translation>%1 (%2 ཆག་སྐྱོན་བྱུང་བ།)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1 (Ratio: %2)</source>
        <translation>%1 (མཉམ་སྤྱོད་བསྡུར་ཚད: %2)</translation>
    </message>
    <message>
        <location line="+220"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Peer is connected over uTP</source>
        <translation>ལས་རིགས་གཅིག་པའི་བར་དུ་uTPལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Add URL </source>
        <translation>སྦྲེལ་མཐུད་བསྣན་པ། </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add tracker announce URL:</source>
        <translation>Tracker བརྡ་ཁྱབ་སྦྲེལ་མཐུད་ཁ་སྣོན་བྱས།</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+46"/>
        <source>Invalid URL &quot;%1&quot;</source>
        <translation>གོ་མི་ཆོད་པའི་སྦྲེལ་མཐུདURL &quot;%1&quot;</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Edit URL </source>
        <translation>རྩོམ་སྒྲིག་གི་སྦྲེལ་མཐུད། </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit tracker announce URL:</source>
        <translation>Trackerརྩོམ་སྒྲིག་བརྡ་ཁྱབ་སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>High</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+4"/>
        <source>Use Global Settings</source>
        <translation>ཧྲིལ་པོའི་སྒྲིག་བཀོད་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Seed regardless of ratio</source>
        <translation>ལོངས་སྤྱོད་བྱེད་ཚད་ག་ཚོད་ཡིན་རུང་ཚང་མ་འདེབས་འཛུགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop seeding at ratio:</source>
        <translation>བསྡུར་ཚད་ལྟར་ས་བོན་འདེབས་མཚམས་འཇོག་རྒྱུ་སྟེ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Seed regardless of activity</source>
        <translation>བྱ་འགུལ་གྱི་རྣམ་པ་ཅི་འདྲ་ཡིན་རུང་ཚང་མ་འདེབས་འཛུགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop seeding if idle for:</source>
        <translation>ས་བོན་འདེབས་མཚམས་བཞག་ནས་ཁོམ་ལོང་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Up</source>
        <translation>ཡར་བསྐུར་བ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Down</source>
        <translation>མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Status</source>
        <translation>གནས་ཚུལ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>སྡོད་གནས།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Client</source>
        <translation>མཁོ་མཁན་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../DetailsDialog.ui" line="+14"/>
        <source>Torrent Properties</source>
        <translation>Torrent ་ངོ་བོ།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Information</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Activity</source>
        <translation>བྱེད་སྒོ་སྤེལ་བ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Have:</source>
        <translation>ཡོད་པ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Availability:</source>
        <translation>སྤྱོད་གོ་ཆོད་པའི་རང་བཞིན་ནི།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Uploaded:</source>
        <translation>གོང་དུ་བཤད་པ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Downloaded:</source>
        <translation>ཕབ་ལེན་བྱས་པ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>State:</source>
        <translation>རྒྱལ་ཁབ་ཀྱིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Running time:</source>
        <translation>འཁོར་སྐྱོད་ཀྱི་དུས་ཚོད་ནི།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Remaining time:</source>
        <translation>ལྷག་པའི་དུས་ཚོད་ནི།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Last activity:</source>
        <translation>ཐེངས་སྔོན་མའི་བྱ་འགུལ་ནི།</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Error:</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བ་སྟེ།</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Size:</source>
        <translation>ཆེ་ཆུང་ནི།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Location:</source>
        <translation>གནས་ཡུལ་ནི།</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Hash:</source>
        <translation>ཧ་ཧྲི་ནི།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Privacy:</source>
        <translation>སྒེར་གྱི་གསང་དོན་ནི།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Origin:</source>
        <translation>འབྱུང་ཁུངས་ནི།</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Comment:</source>
        <translation>གསལ་བཤད།</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Peers</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Tracker</source>
        <translation>Tracker</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Add Tracker</source>
        <translation>Tracker་ཁ་སྣོན།</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Edit Tracker</source>
        <translation>Trackerརྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove Trackers</source>
        <translation>Trackerམེད་པར་བཟོ།</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Show &amp;more details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ་གསལ་བཤད་བྱ་དགོས།(&amp;M)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;backup trackers</source>
        <translation>རྗེས་གྲབས་རྗེས་འདེད་འཕྲུལ་ཆས་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Files</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Options</source>
        <translation>བསལ་འདེམས་ཀྱི་དབང་ཆ།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Honor global &amp;limits</source>
        <translation>གཟི་བརྗིད་མཚན་སྙན་དང་མཚམས་ཚད།(&amp;L)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Limit &amp;download speed:</source>
        <translation>ཚད་བཀག་དང་ཕབ་ལེན་གྱི་མྱུར་ཚད།(&amp;D)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Limit &amp;upload speed:</source>
        <translation>ཚད་བཀག ་དང་ཕབ་ལེན་གྱི་མྱུར་ཚད་</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Torrent &amp;priority:</source>
        <translation>ཆུ་རྒྱུན་དང་སྔོན་ཐོབ་ཀྱི་དོན་དག་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Seeding Limits</source>
        <translation>ས་བོན་ཚོད་འཛིན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Ratio:</source>
        <translation>མཉམ་སྤྱོད་བསྡུར་ཚད་ནི།(&amp;R)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Idle:</source>
        <translation>ཁོམ་པ།(&amp;I):</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Peer Connections</source>
        <translation>སྤྱོད་མཁན་གྱི་སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Maximum peers:</source>
        <translation>སྤྱོད་མཁན་ཆེ་ཤོས།(&amp;M):</translation>
    </message>
</context>
<context>
    <name>FileAdded</name>
    <message>
        <location filename="../Session.cc" line="+94"/>
        <source>Add Torrent</source>
        <translation>ཁ་སྣོནTorrent</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;p&gt;&lt;b&gt;Unable to add &quot;%1&quot;.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;It is a duplicate of &quot;%2&quot; which is already added.&lt;/p&gt;</source>
        <translation>&lt;p&gt; &lt;b&gt; &quot;%1&quot;ཁ་སྣོན་བྱེད་ཐབས་བྲལ་བ་རེད། &lt;/b&gt; &lt;/p&gt; &lt;p&gt; དེ་ནི་ཁ་སྣོན་བྱས་ཟིན་པའི་&quot;%2&quot;ཡི་བསྐྱར་ཟློས་ཤིག་རེད། &lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+12"/>
        <source>Error Adding Torrent</source>
        <translation>ནོར་འཁྲུལ་ཁ་སྣོན་བྱས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Invalid Or Corrupt Torrent File</source>
        <translatorcomment>无效或损坏的种子文件</translatorcomment>
        <translation>གོ་མི་ཆོད་པའམ་ཡང་ན་རུལ་སུངས་སུ་གྱུར་པའི་དྲག་པོའི་ཡིག་ཆ།</translation>
    </message>
</context>
<context>
    <name>FileTreeItem</name>
    <message>
        <location filename="../FileTreeItem.cc" line="+271"/>
        <location filename="../FileTreeView.cc" line="+105"/>
        <location line="+257"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-256"/>
        <location line="+254"/>
        <source>High</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-255"/>
        <location line="+256"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-255"/>
        <source>Mixed</source>
        <translation>མཉམ་བསྲེས་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>FileTreeModel</name>
    <message>
        <location filename="../FileTreeModel.cc" line="+196"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Progress</source>
        <translation>ཡར་ཐོན།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Download</source>
        <translation>ཕབ་ལེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Priority</source>
        <translation>སྔོན་ཐོབ་ཀྱི་དོན་དག</translation>
    </message>
</context>
<context>
    <name>FileTreeView</name>
    <message>
        <location filename="../FileTreeView.cc" line="+247"/>
        <source>Check Selected</source>
        <translation>བདམས་ཟིན་པའི་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uncheck Selected</source>
        <translation>བདམས་ཟིན་པའི་ཞིབ་བཤེར་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Only Check Selected</source>
        <translation>བདམས་ཟིན་པའི་ཞིབ་བཤེར་ཁོ་ན།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Priority</source>
        <translation>སྔོན་ཐོབ་ཀྱི་དོན་དག</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rename...</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
</context>
<context>
    <name>FilterBar</name>
    <message>
        <location filename="../FilterBar.cc" line="+61"/>
        <location line="+143"/>
        <source>All</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Active</source>
        <translation>བྱ་འགུལ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Downloading</source>
        <translation>ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Seeding</source>
        <translation>ས་བོན་འདེབས་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paused</source>
        <translation>མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Verifying</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Show:</source>
        <translation>གསལ་བཤད་བྱས་དོན།</translation>
    </message>
</context>
<context>
    <name>FilterBarLineEdit</name>
    <message>
        <location filename="../FilterBarLineEdit.cc" line="+48"/>
        <source>Search...</source>
        <translation>འཚོལ་ཞིབ་ ...</translation>
    </message>
</context>
<context>
    <name>Formatter</name>
    <message>
        <location filename="../Formatter.cc" line="+33"/>
        <source>B/s</source>
        <translation>B/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>kB/s</source>
        <translation>kB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MB/s</source>
        <translation>MB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GB/s</source>
        <translation>GB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TB/s</source>
        <translation>TB/s</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+12"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>kB</source>
        <translation>kB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TiB</source>
        <translation>TiB</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+14"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location line="-11"/>
        <location line="+14"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+8"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message numerus="yes">
        <location line="+32"/>
        <source>%Ln day(s)</source>
        <translation>
            <numerusform>%Ln ཉིན་མོ།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln hour(s)</source>
        <translation>
            <numerusform>%Ln དུས་ཚོད།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln minute(s)</source>
        <translation>
            <numerusform>%Ln སྐར་མ་གཅིག</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln second(s)</source>
        <translation>
            <numerusform>%Ln སྐར་ཆ།</numerusform>
        </translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
</context>
<context>
    <name>FreeSpaceLabel</name>
    <message>
        <location filename="../FreeSpaceLabel.cc" line="+58"/>
        <source>&lt;i&gt;Calculating Free Space...&lt;/i&gt;</source>
        <translation>&lt;i&gt; རང་དབང་གི་བར་སྟོང་རྩིས་རྒྱག་པ... &lt;/i&gt;</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>%1 free</source>
        <translation>%1རིམ་པ་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>LicenseDialog</name>
    <message>
        <location filename="../LicenseDialog.ui" line="+14"/>
        <source>License</source>
        <translation>འཁྲོལ་འཛིན།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="+14"/>
        <location filename="../MainWindow.cc" line="+645"/>
        <source>Transmission</source>
        <translation>BTཕབ་ལེན་ཡི་བྱད།</translation>
    </message>
    <message>
        <location line="+175"/>
        <source>&amp;Torrent</source>
        <translation>Torrent(&amp;T)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Edit</source>
        <translation>རྩོམ་སྒྲིག(&amp;E)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Help</source>
        <translation>རོགས་རམ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;View</source>
        <translation>&gt; ལྟ་ཚུལ</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;File</source>
        <translation>ཡིག་ཆ།(&amp;F)</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>&amp;New...</source>
        <translation>གསར་པ།(&amp;N)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Create a new torrent</source>
        <translation>Torrent་གསར་བ་ཞིག་བཟོས།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Properties</source>
        <translation>ངོ་བོ།(&amp;P)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show torrent properties</source>
        <translation>Torrent ་ངོ་བོ་མངོན་པ།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Open the torrent&apos;s folder</source>
        <translation>Torrent ཡིག་སྣོད་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="-161"/>
        <source>Queue</source>
        <translation>རེའུ་མིག</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>&amp;Open...</source>
        <translation>ཁ་ཕྱེ་བ།(&amp;O)...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open a torrent</source>
        <translation>Torrentཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Open Fold&amp;er</source>
        <translation>ཡིག་སྣོད་སྒོ་ཕྱེ་བ།(&amp;E)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Start</source>
        <translation>འགོ་རྩོམ་པ།(&amp;S)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start torrent</source>
        <translation>Torrentའགོ་བརྩམས་པ།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Ask Tracker for &amp;More Peers</source>
        <translation>Trackerལ་སྔར་ལས་མང་བའི་སྤྱོད་མཁན་(&amp;M)རེ་ཞུ་བྱས།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ask tracker for more peers</source>
        <translation>ལས་རིགས་གཅིག་པའི་མི་དེ་བས་མང་བར་རྗེས་འདེད</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Pause</source>
        <translation>&gt; མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pause torrent</source>
        <translation>མཚམས་འཇོག་པའི་དྲག་ཏུ་གྱུར་པ།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Verify Local Data</source>
        <translation>ས་གནས་ཀྱི་གཞི་གྲངས་ལ་ལྟ་ཞིབ་བྱེད་པ།(&amp;V)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify local data</source>
        <translation>ས་གནས་དེ་གའི་གཞི་གྲངས་</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Remove</source>
        <translation>&gt; མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove torrent</source>
        <translation>ཆུ་རྒྱུན་གཙང་སེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Delete Files and Remove</source>
        <translation>ཡིག་ཆ་བསུབ་པ་དང་མེད་པར་བཟོ་བ།(&amp;D)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove torrent and delete its files</source>
        <translation>Torrenཆུ་རྒྱུན་མེད་པར་བཟོས་ནས་དེའི་ཡིག་ཆ་བསུབ་པ།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Start All</source>
        <translation>ཚང་མ་མགོ་བརྩམས།(&amp;S)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Pause All</source>
        <translation>ཚང་མ་མཚམས་འཇོག་དགོས།(&amp;P)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Quit</source>
        <translation>ལས་གནས་ནས་ཕྱིར་འཐེན་བྱ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Select All</source>
        <translation>&gt; ཚང་མ་བདམས་པ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Deselect All</source>
        <translation>ཚང་མ་ཕྱིར་འབུད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Preferences</source>
        <translation>ཐོག་མར་འདེམས་པའི་ཚན་པ།(&amp;P)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Compact View</source>
        <translation>ཚགས་དམ་པའི་མཐོང་རིས།(&amp;C)</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Compact View</source>
        <translation>ཚགས་དམ་པའི་མཐོང་རིས།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Toolbar</source>
        <translation>ལག་ཆའི་སྡེ།(&amp;T)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Filterbar</source>
        <translation>འཚག་ཆས།(&amp;F)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Statusbar</source>
        <translation>རྣམ་པའི་རེའུ་མིག(&amp;S)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Activity</source>
        <translation>རིགས་འབྱེད་པ། བྱེད་སྒོ་སྤེལ་བ།(&amp;A)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by A&amp;ge</source>
        <translation>གནས་པའི་དུས་ཚོད་ལྟར་གོ་རིམ་སྒྲིག་པ།(&amp;G)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Time &amp;Left</source>
        <translation>དུས་ཚོད་ལྷག་མ་ལྟར་གོ་རིམ་སྒྲིག་པ།(&amp;L)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Name</source>
        <translation>མིང་ལྟར་གོ་རིམ་སྒྲིག་པ།(&amp;N)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Progress</source>
        <translation>མྱུར་ཚད་ལྟར་གོ་རིམ་སྒྲིག་པ།(&amp;P)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Rati&amp;o</source>
        <translation>ལོངས་སྤྱོད་ཚད་ལྟར་གོ་རིམ་སྒྲིག་པ།(&amp;O)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Si&amp;ze</source>
        <translation>ཆེ་ཆུང་ལྟར་གོ་རིམ་སྒྲིག་པ།(&amp;Z)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Stat&amp;e</source>
        <translation>གནས་ཚུལ་ལ་གཞིགས་ནས་རིགས་འབྱེད་པ།(&amp;E)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by T&amp;racker</source>
        <translation>Tracker གོ་རིམ་ལྟར་སྒྲིག་པ(&amp;R)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Message &amp;Log</source>
        <translation>ཆ་འཕྲིན་དང་ཉིན་ཐོ།(&amp;L)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Statistics</source>
        <translation>བསྡོམས་རྩིས།(&amp;S)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Contents</source>
        <translation>ནང་དོན།(&amp;C)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;About</source>
        <translation>སྐོར། (&amp;A)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Re&amp;verse Sort Order</source>
        <translation>གོ་རིམ་ལྡོག་སྒྲིག(&amp;V)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Name</source>
        <translation>མིང་།(&amp;N)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Files</source>
        <translation>ཡིག་ཆ།(&amp;F)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Tracker</source>
        <translation>Tracker(&amp;T)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Total Ratio</source>
        <translation>སྤྱིའི་བསྡུར་ཚད།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Ratio</source>
        <translation>ཚོགས་འདུའི་བསྡུར་ཚད།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Total Transfer</source>
        <translation>བཤུག་སྤྲོད་བསྡོམས་འབོར།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Transfer</source>
        <translation>ཁ་བརྡ་བརྒྱུད་སྤྲོད་ཚད།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Main Window</source>
        <translation>སྒེའུ་ཁུང་གཙོ་བོ།(&amp;M)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Tray &amp;Icon</source>
        <translation>སྡེར་མ་དང་མཚོན་རྟགས།(&amp;I)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Change Session...</source>
        <translation>&gt; སྐབས་བརྗེའི་ཚོགས་འདུ་</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose Session</source>
        <extracomment>Start a local session or connect to a running session</extracomment>
        <translation>སྐབས་འདིའི་ཚོགས་འདུ་བདམས་པ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Set &amp;Location...</source>
        <translation>སྒྲིག་བཀོད་གནས་ཁུལ།(&amp;L)...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy Magnet Link to Clipboard</source>
        <translation>སྡུད་ཤུགས་ཕྲེང་བ་བསྐྱར་དཔར་བྱས་ནས་དྲས་སྦྱར་པང་ལེབ་ལེན་པ།(&amp;C)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open &amp;URL...</source>
        <translation>སྦྲེལ་མཐུད་ཁ་ཕྱེ་བ།(&amp;U)...</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Donate</source>
        <translation>ཞལ་འདེབས་སུ་ཕུལ་བ།(&amp;D)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Start &amp;Now</source>
        <translation>ད་ལྟ་ནས་འགོ་བརྩམས་པ།(&amp;N)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bypass the queue and start now</source>
        <translation>གྲལ་བསྒྲིགས་ནས་ད་ལྟ་ནས་མགོ་བརྩམས།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Move to &amp;Top</source>
        <translation>རྩེ་ལ་སྤོ་བ།(&amp;T)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move &amp;Up</source>
        <translation>གོང་དུ་སྤོ་བ།(&amp;U)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move &amp;Down</source>
        <translation>འོག་ཏུ་གནས་སྤོ་བྱ་དགོས།(&amp;D)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move to &amp;Bottom</source>
        <translation>མཐིལ་དུ་སྤོས་པ།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Queue</source>
        <translation>གྲལ་བསྒྲིགས་ནས་རིགས་འབྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../MainWindow.cc" line="-211"/>
        <source>Limit Download Speed</source>
        <translation>ཕབ་ལེན་གྱི་མྱུར་ཚད་ཚོད་འཛིན</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>Unlimited</source>
        <translation>ཚད་མེད་པ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+641"/>
        <location line="+8"/>
        <source>Limited at %1</source>
        <translation>ཚད་བཀག་ནི་%1</translation>
    </message>
    <message>
        <location line="-599"/>
        <source>Limit Upload Speed</source>
        <translation>ཡར་བསྐུར་བའི་མྱུར་ཚད་ཚོད་འཛིན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop Seeding at Ratio</source>
        <translation>བསྡུར་ཚད་ལྟར་ས་བོན་འདེབས་མཚམས་འཇོག་དགོས།</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Seed Forever</source>
        <translation>ས་བོན་ཇི་སྲིད་བར་དུ</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+627"/>
        <source>Stop at Ratio (%1)</source>
        <translation>བསྡུར་ཚད་ལྟར་མཚམས་འཇོག་དགོས། (%1)</translation>
    </message>
    <message>
        <location line="-393"/>
        <source> - %1:%2</source>
        <extracomment>Second (optional) part of main window title &quot;Transmission - host:port&quot; (added when connected to remote session); notice that leading space (before the dash) is included here</extracomment>
        <translation> - %1:%2</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+17"/>
        <source>Ratio: %1</source>
        <translation>མཉམ་སྤྱོད་བསྡུར་ཚད། %1</translation>
    </message>
    <message>
        <location line="-12"/>
        <location line="+6"/>
        <source>Down: %1, Up: %2</source>
        <translation>ཕབ་ལེན་པ། %1, བསྐུར་བ: %2</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Torrent Files (*.torrent);;All Files (*.*)</source>
        <translation>ཐུའོ་ལུན་གྱི་ཡིག་ཚགས་(*.torrent); ཡིག་ཆ་ཡོད་ཚད་(*.* )</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;options dialog</source>
        <translation>གླེང་མོལ་</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Open Torrent</source>
        <translation>Torrentསྒོ་ཕྱེ་པ།</translation>
    </message>
    <message>
        <location line="-918"/>
        <source>Speed Limits</source>
        <translation>མྱུར་ཚད་ཀྱི་ཚད་བཀག</translation>
    </message>
    <message>
        <location line="+449"/>
        <source>Network Error</source>
        <translation>དྲ་རྒྱའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="+433"/>
        <source>Click to disable Temporary Speed Limits
 (%1 down, %2 up)</source>
        <translation>གནས་སྐབས་ཀྱི་མྱུར་ཚད་ཚོད་འཛིན་མེད་པར་བཟོ་དགོས།
 (%1 མར་ཆག་པ་དང་། %2 ཡར་འཕར་བ། )</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Click to enable Temporary Speed Limits
 (%1 down, %2 up)</source>
        <translation>གནས་སྐབས་ཀྱི་མྱུར་ཚད་ཚོད་འཛིན་བྱེད་ཐུབ་པར་བྱ་དགོས།
 (%1 མར་ཆག་པ་དང་། %2 ཡར་འཕར་བ། )</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Remove torrent?</source>
        <translation>ཆུ་རྒྱུན་མེད་པར་བཟོ་དགོས་སམ།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete this torrent&apos;s downloaded files?</source>
        <translation>ཐེངས་འདིའི་དྲག་ཏུ་ཕབ་ལེན་བྱས་པའི་ཡིག་ཆ་བསུབ་དགོས་སམ།</translation>
    </message>
    <message numerus="yes">
        <location line="-5"/>
        <source>Remove %Ln torrent(s)?</source>
        <translation>
            <numerusform>%Ln་གནས་སྤོར་བྱེད་དམ།Torrent</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="-497"/>
        <source>Showing %L1 of %Ln torrent(s)</source>
        <translation>
            <numerusform>%L1%Lnཆམ་གྱི་གློག་རྒྱུན་མངོན་པར་བྱས་ཡོད།Torrent</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+503"/>
        <source>Delete these %Ln torrent(s)&apos; downloaded files?</source>
        <translation>
            <numerusform>%Ln&apos;ཕབ་ལེན་བྱས་པའི་ཡིག་ཆ་དེ་དག་བསུབ་དགོས་སམ།Torrent</numerusform>
        </translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Once removed, continuing the transfer will require the torrent file or magnet link.</source>
        <translation>Torrentགནས་དབྱུང་བཏང་རྗེས་མུ་མཐུད་དུ་སྤོ་སྒྱུར་བྱེད་པར་དྲག་ཤུགས་ཀྱི་ཡིག་ཆའམ་ཡང་ན་ཁབ་ལེན་སྦྲེལ་མཐུད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Once removed, continuing the transfers will require the torrent files or magnet links.</source>
        <translation>ཕྱིར་འཐེན་བྱས་རྗེས་མུ་མཐུད་དུ་སྤོ་སྒྱུར་བྱེད་པར་དྲག་ཤུགས་ཀྱི་ཡིག་ཆ་དང་ཡང་ན་ཁབ་ལེན་སྦྲེལ་མཐུད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This torrent has not finished downloading.</source>
        <translation>ཐེངས་འདིའི་དྲག་ཤུགས་ཀྱིས་ཕབ་ལེན་བྱས་ཚར་མེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>These torrents have not finished downloading.</source>
        <translation>Torrentདྲག་པོ་འདི་དག་ད་དུང་ཕབ་ལེན་བྱས་ཚར་མེད།</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This torrent is connected to peers.</source>
        <translation>Torrent་འདི་དང་ལས་རིགས་གཅིག་པའི་བར་ལ་འབྲེལ་བ་ཡོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>These torrents are connected to peers.</source>
        <translation>Torrent་འདི་དག་ནི་ལས་རིགས་གཅིག་པའི་མི་དང་འབྲེལ་བ་ཡོད།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One of these torrents is connected to peers.</source>
        <translation>Torrentདེའི་ནང་གི་དྲག་པོ་ཞིག་ནི་ལས་རིགས་གཅིག་པའི་མི་དང་འབྲེལ་བ་ཡོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Some of these torrents are connected to peers.</source>
        <translation>དེའི་ནང་གི་ཁ་ཤས་ནི་ལས་རིགས་གཅིག་པའི་མི་དང་འབྲེལ་བ་ཡོད།</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>One of these torrents has not finished downloading.</source>
        <translation>དེའི་ནང་གི་དྲག་པོ་ཞིག་ད་དུང་ཕབ་ལེན་བྱས་ཚར་མེད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Some of these torrents have not finished downloading.</source>
        <translation>དེའི་ནང་གི་དྲག་ཤུགས་ཁ་ཤས་ད་དུང་ཕབ་ལེན་བྱས་ཚར་མེད།</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>%1 has not responded yet</source>
        <translation>%1ལ་ད་དུང་ལན་བཏབ་མེད།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 is responding</source>
        <translation>%1ལ་ལན་འདེབས་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 last responded %2 ago</source>
        <translation>%1ཐེངས་སྔོན་མར་ལན་བཏབ་པའི་%2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 is not responding</source>
        <translation>%1ལ་ལན་མི་འདེབས།</translation>
    </message>
</context>
<context>
    <name>MakeDialog</name>
    <message>
        <location filename="../MakeDialog.ui" line="+17"/>
        <source>New Torrent</source>
        <translation>Torrentགསར་དུ་ཐོན་པ།</translation>
    </message>
    <message>
        <location filename="../MakeDialog.cc" line="+201"/>
        <source>&lt;i&gt;No source selected&lt;i&gt;</source>
        <translation>&lt;i&gt;འབྱུང་ཁུངས་བདམས་མེད་&lt;i&gt;པ།</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%Ln File(s)</source>
        <translation>
            <numerusform>%Ln ཡིག་ཆ།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln Piece(s)</source>
        <translation>
            <numerusform>%Ln ཁུལ་དབྱེ།</numerusform>
        </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 in %2; %3 @ %4</source>
        <translation>%2ནང་གི་1% %3 @ %4</translation>
    </message>
    <message>
        <location filename="../MakeDialog.ui" line="+9"/>
        <source>Files</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Sa&amp;ve to:</source>
        <translation>ས་ཨུལ་གྱིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Source f&amp;older:</source>
        <translation>འབྱུང་ཁུངས་f&amp;older:</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source &amp;file:</source>
        <translation>ཡོང་ཁུངས་ཡིག་ཆ།(&amp;F):</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Properties</source>
        <translation>ངོ་བོ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Trackers:</source>
        <translation>Tracker(&amp;T):</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>To add a backup URL, add it on the line after the primary URL.
To add another primary URL, add it after a blank line.</source>
        <translation>གྲ་སྒྲིག་བྱས་པའི་སྦྲེལ་མཐུད་ཅིག་སྣོན་དགོས་ན་སྦྲེལ་མཐུད་གཙོ་བོའི་འོག་ཏུ་སྣོན་རོགས།
སྦྲེལ་མཐུད་གཙོ་བོ་གཞན་པ་ཞིག་སྣོན་དགོས་ན་དེ་སྟོང་བ་ཞིག་གི་འོག་ཏུ་སྣོན་རོགས།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Co&amp;mment:</source>
        <translation>གསལ་བཤད།(&amp;M):</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Private torrent</source>
        <translation>སྒེར་གྱི་Torrent(&amp;P)</translation>
    </message>
</context>
<context>
    <name>MakeProgressDialog</name>
    <message>
        <location filename="../MakeProgressDialog.ui" line="+14"/>
        <source>New Torrent</source>
        <translation>གསར་དུ་ཐོན་པTorrent</translation>
    </message>
    <message>
        <location filename="../MakeDialog.cc" line="-108"/>
        <source>Creating &quot;%1&quot;</source>
        <translation>&quot;%1&quot;གསར་སྐྲུན་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created &quot;%1&quot;!</source>
        <translation>&quot;%1&quot;གསར་སྐྲུན་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: invalid announce URL &quot;%1&quot;</source>
        <translation>ནོར་འཁྲུལ། གོ་མི་ཆོད་པའི་ཁྱབ་བསྒྲགས་བྱས་པའི་URL&quot;%1&quot;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancelled</source>
        <translation>མེད་པར་བཟོས་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading &quot;%1&quot;: %2</source>
        <translation>&quot;%1&quot;ལ་ནོར་འཁྲུལ་བྱུང་བ་སྟེ། %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error writing &quot;%1&quot;: %2</source>
        <translation>ནང་འཇུག&quot;%1&quot;སྐབས་སུ་ནོར་འཁྲུལ:%2</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../OptionsDialog.cc" line="+56"/>
        <source>Open Torrent</source>
        <translation>སྒོ་ཕྱེ་Torrent</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Open Torrent from File</source>
        <translation>ཡིག་ཆའི་ནང་ནས་ཁ་ཕྱེ་བTorrent</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open Torrent from URL or Magnet Link</source>
        <translation>Torrentསྦྲེལ་མཐུད་དམ་སྡུད་ཤུགས་སྦྲེལ་མཐུད་ནས་ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+17"/>
        <source>&amp;Source:</source>
        <translation>ཡོང་ཁུངས།(&amp;S):</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Destination folder:</source>
        <translation>དམིགས་ཡུལ་གྱི་ཡིག་སྣོད་ནི།(&amp;D):</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="+60"/>
        <source>High</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Low</source>
        <translation>དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+35"/>
        <source>&amp;Priority:</source>
        <translation>སྔོན་ཐོབ་ཀྱི་དོན་དག་ནི།(&amp;P):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>S&amp;tart when added</source>
        <translation>ཁ་སྣོན་བྱེད་སྐབས་S&amp;tart</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="+5"/>
        <source>&amp;Verify Local Data</source>
        <translation>&gt; ས་གནས་ཀྱི་གཞི་གྲངས་ལ་</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+7"/>
        <source>Mo&amp;ve .torrent file to the trash</source>
        <translation>torrent་ཡིག་ཚགས་དེ་གད་སྙིགས་བླུགས་སྣོད་དུ་.(&amp;V)</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="-55"/>
        <source>Torrent Files (*.torrent);;All Files (*.*)</source>
        <translation>(&amp;V)་ཡིག་ཚགས་(*.torrent); ཡིག་ཆ་ཡོད་ཚད་(*.* )</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Select Destination</source>
        <translation>དམིགས་ཡུལ་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>PathButton</name>
    <message>
        <location filename="../PathButton.cc" line="+31"/>
        <location line="+72"/>
        <source>(None)</source>
        <translation>(གཅིག་ཀྱང་མེད། )</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Select Folder</source>
        <translation>ཡིག་སྣོད་གདམ་གསེས་བྱེད།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Select File</source>
        <translation>ཡིག་ཆ་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>PrefsDialog</name>
    <message>
        <location filename="../PrefsDialog.ui" line="+1139"/>
        <source>Use &amp;authentication</source>
        <translation>བཀོལ་སྤྱོད་དང་བདེན་དཔང་ར་སྤྲོད་པ།(&amp;A)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Username:</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།(&amp;U):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Pass&amp;word:</source>
        <translation>གསང་ཨང་དང་ཐ་སྙད།:</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>&amp;Open web client</source>
        <translation>&amp; སྒོ་ཕྱེ་བའི་དྲ་རྒྱའི་མངགས་བཅོལ</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Addresses:</source>
        <translation>སྡོད་གནས་ནི།</translation>
    </message>
    <message>
        <location line="-1150"/>
        <source>Speed Limits</source>
        <translation>མྱུར་ཚད་ཀྱི་ཚད་བཀག</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>&lt;small&gt;Override normal speed limits manually or at scheduled times&lt;/small&gt;</source>
        <translation>&lt;small&gt; རྒྱུན་ལྡན་གྱི་མྱུར་ཚད་ཀྱི་ཚད་བཀག་དེ་ལག་ཤེས་སམ་ཡང་ན་དུས་བཀག་ལྟར་རྩིས་མེད་དུ་གཏོང་དགོས། &lt;/small&gt;</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>&amp;Scheduled times:</source>
        <translation>སྔོན་བཀོད་བྱས་པའི་དུས་ཚོད།(&amp;S):</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>&amp;On days:</source>
        <translation>ཉིན་གྲངས།(&amp;O)</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+261"/>
        <source>Every Day</source>
        <translation>ཉིན་ལྟར།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weekdays</source>
        <translation>ནམ་རྒྱུན་གྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weekends</source>
        <translation>གཟའ་མཇུག།</translation>
    </message>
    <message>
        <location line="-152"/>
        <source>Sunday</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Monday</source>
        <translation>གཟའ་ཟླ་བའི་ཉིན།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tuesday</source>
        <translation>གཟའ་མིག་དམར་ཉིན།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Wednesday</source>
        <translation>གཟའ་ལྷག་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Thursday</source>
        <translation>གཟའ་ཕུར་བུ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Friday</source>
        <translation>གཟའ་པ་སངས་ཉིན།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Saturday</source>
        <translation>གཟའ་སྤེན་པའི་ཉིན།</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Port is &lt;b&gt;open&lt;/b&gt;</source>
        <translation>མཐུད་ཁ་ནི་&lt;b&gt;སྒོ་ཕྱེ་བ་&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Port is &lt;b&gt;closed&lt;/b&gt;</source>
        <translation>མཐུད་ཁ་ནི་&lt;b&gt;སྒོ་བརྒྱབ་ནས་རེད།&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="+513"/>
        <source>Incoming Peers</source>
        <translation>འབབ་ཚིགས་སུ་ཞུགས་པའི་མཛད་སྤྱོད་པ།</translation>
    </message>
    <message>
        <location line="+32"/>
        <location filename="../PrefsDialog.cc" line="+340"/>
        <source>Status unknown</source>
        <translation>གནས་ཚུལ་མི་ཤེས་པ།</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>&amp;Port for incoming connections:</source>
        <translation>འབབ་ཚིགས་སྦྲེལ་མཐུད་ཀྱི་མཐུད་སྣེ།(&amp;P):</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Use UPnP or NAT-PMP port &amp;forwarding from my router</source>
        <translation>UPnPའམ་ཡང་ན་NAT-PMPལམ་འཚོལ་ཆས་ནས་སྐྱེལ་འདྲེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Options</source>
        <translation>བདམས་ཚན།</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable &amp;uTP for peer connections</source>
        <translation>ལས་རིགས་གཅིག་པའི་བར་གྱི་འབྲེལ་བར་བཀག་སྤྱོད་བྱེད་ཐུབ། uTP(&amp;U)</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>uTP is a tool for reducing network congestion.</source>
        <translation>uTPནི་དྲ་རྒྱའི་འཚང་ཁ་ཇེ་ཉུང་དུ་གཏོང་བའི་ལག་ཆ་ཞིག་ཡིན།</translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="-219"/>
        <source> minute(s)</source>
        <extracomment>Spin box suffix, &quot;Stop seeding if idle for: [ 5 minutes ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> སྐར་མ་གཅིག</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+22"/>
        <source> minute(s) ago</source>
        <extracomment>Spin box suffix, &quot;Download is inactive if data sharing stopped: [ 5 minutes ago ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> སྐར་མ་གཅིག་གི་སྔོན་ལ།</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-638"/>
        <source>Automatically add .torrent files &amp;from:</source>
        <translation>རང་འགུལ་གྱིསtorrent་ཡིག་ཆ་ཁ་སྣོན་བྱས་པ་གཤམ་གསལ།(&amp;F):</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Show the Torrent Options &amp;dialog</source>
        <translation>Torrent འདི་བདམ་ཚན་གླེང་མོལ་མངོན་པར་བྱས་ཡོད།(&amp;D)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Start added torrents</source>
        <translation>་ཐོག་མར་དྲག་ཏུ་བསྣན་པའི་དྲག་ཤུགས་ཀྱི་མགོ་བརྩམས།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Mo&amp;ve the .torrent file to the trash</source>
        <translation>གད་སྙིགས་བླུགས་སྣོད་ནང་གི་ཡིག་ཚགས་.torrent་པོ་དེ་གད་སྙིགས་བླུགས་སྣོད</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Download Queue</source>
        <translation>ཕབ་ལེན་རུ་ཁག</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Ma&amp;ximum active downloads:</source>
        <translation>ཆེས་ཆེ་བའི་བྱ་འགུལ་ཡིས་རང་འགུལ་གྱིས་ཕབ་ལེན་བྱས་པ་གཤམ་གསལ།(&amp;X):</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Incomplete</source>
        <translation>ལེགས་འགྲུབ་མ་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Seeding</source>
        <translation>ས་བོན་འདེབས་པ།</translation>
    </message>
    <message>
        <location line="+548"/>
        <source>Remote</source>
        <translation>རྒྱང་རིང་།</translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="+145"/>
        <source>&lt;i&gt;Blocklist contains %Ln rule(s)&lt;/i&gt;</source>
        <translation>
            <numerusform>&lt;i&gt; བཀག་སྡོམ་བྱེད་མཁན་གྱི་མིང་ཐོའི་ནང་དུ་%Lnཡི་སྒྲིག་སྲོལ་འདུས་ཡོད།&lt;/i&gt;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-287"/>
        <source>Pick a &amp;random port every time Transmission is started</source>
        <translation>BTསྐྱེལ་འདྲེན་བྱེད་འགོ་ཚུགས་ཐེངས་རེར་སྐབས་བསྟུན་གྲུ་ཁ་ཞིག་བདམས་པ་རེད།(&amp;R)</translation>
    </message>
    <message>
        <location line="-252"/>
        <source>Limits</source>
        <translation>མཚམས་ཚད།</translation>
    </message>
    <message>
        <location line="+299"/>
        <source>Maximum peers per &amp;torrent:</source>
        <translation>Torrent ཆུ་རྒྱུན་རེ་རེའི་ཆེས་མཐོ་བའི་ལས་རིགས་གཅིག་པའི་མི་གྲངས་ནི།(&amp;T)</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Maximum peers &amp;overall:</source>
        <translation>ཆེས་མཐོ་བའི་ལས་རིགས་གཅིག་པའི་མི་དང་སྤྱིའི་</translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="-229"/>
        <source>&lt;b&gt;Update succeeded!&lt;/b&gt;&lt;p&gt;Blocklist now has %Ln rule(s).</source>
        <translation>
            <numerusform>&lt;b&gt;གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བ་རེད།&lt;/b&gt; &lt;p&gt;ད་ལྟ་བཀག་སྡོམ་བྱེད་མཁན་ལ་%Lnཡི་གཏན་འབེབས་ཡོད།</numerusform>
        </translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;b&gt;Update Blocklist&lt;/b&gt;&lt;p&gt;Getting new blocklist...</source>
        <translation>&lt;b&gt;གསར་སྒྱུར་མིང་ཐོ་འགོད&lt;/b&gt;&lt;p&gt;་པར་བཀག་སྡོམ་བྱེད་པའི་མིང་ཐོ་གསར་པ་</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-193"/>
        <source>Blocklist</source>
        <translation>བཀག་སྡོམ་མིང་ཐོ།</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Enable &amp;automatic updates</source>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་ཐུབ་པ།(&amp;A)</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+19"/>
        <source>Allow encryption</source>
        <translation>གསང་གྲངས་ཆོག་མཆན་འགོད་དགོས།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prefer encryption</source>
        <translation>གསང་གྲངས་ལ་དེ་བས་ཀྱང་དགའ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Require encryption</source>
        <translation>གསང་གྲངས་དགོས་པའི་བླང་བྱ།</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-97"/>
        <source>Privacy</source>
        <translation>སྒེར་གྱི་གསང་དོན།</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>&amp;to</source>
        <translation>་</translation>
    </message>
    <message>
        <location line="+763"/>
        <location line="+9"/>
        <source>Desktop</source>
        <translation>ཅོག་ཙེའི་སྟེང་གི</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Show Transmission icon in the &amp;notification area</source>
        <translation>བརྡ་ཁྱབ་ས་ཁོངས་སུ BTཕབ་ལེན་ཡོ་བྱད་ཀྱི་རི་མོ་འཆར་འདུག(&amp;N)</translation>
    </message>
    <message>
        <location line="-213"/>
        <source>Te&amp;st Port</source>
        <translation>ཚོད་ལྟའི་མཐུད་སྣེ།(&amp;S)</translation>
    </message>
    <message>
        <location line="-101"/>
        <source>Enable &amp;blocklist:</source>
        <translation>སྤྱོད་མི་ཆོག་པའི་རེའུ་མིག(&amp;B):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Update</source>
        <translation>གསར་སྒྱུར།(&amp;U)</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>&amp;Encryption mode:</source>
        <translation>གསང་བའི་དཔེ་དབྱིབས་ནི།(&amp;E):</translation>
    </message>
    <message>
        <location line="+453"/>
        <source>Remote Control</source>
        <translation>རྒྱང་འཛིན་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Allow &amp;remote access</source>
        <translation>རྒྱང་རིང་ནས་བཅར་འདྲི་བྱེད་ཆོག(&amp;R)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>HTTP &amp;port:</source>
        <translation>HTTP མཐུད་སྣེ།(&amp;P):</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Only allow these IP a&amp;ddresses:</source>
        <translation>IP ས་གནས་འདི་དག་མ་གཏོགས་མི་ཆོག(&amp;D):</translation>
    </message>
    <message>
        <location line="-1128"/>
        <source>&amp;Upload:</source>
        <translation>ཡར་བསྐུར།(&amp;U):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Download:</source>
        <translation>ཕབ་ལེན་བྱ་རྒྱུ་སྟེ།(&amp;D):</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Alternative Speed Limits</source>
        <translation>ཚབ་བྱེད་མྱུར་ཚད་ཀྱི་ཚད་བཀག</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>U&amp;pload:</source>
        <translation>ཡར་བསྐུར།(&amp;P):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Do&amp;wnload:</source>
        <translation>ཕབ་ལེན།(&amp;W):</translation>
    </message>
    <message>
        <location line="+836"/>
        <source>Start &amp;minimized in notification area</source>
        <translation>འགུལ་སྐྱོད་བྱེད་པའི་སྐབས་སུ་ཆེས་ཆུང་བའི་བརྡ་ཁྱབ་ས་ཁོངས་སུ་འགྱུར་བ།(&amp;M)</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Notification</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Show a notification when torrents are a&amp;dded</source>
        <translation>Torrentདུས་ནམ་ཞིག་ལ་དྲག་ཏུ་གྱུར་པའི་དུས་སུ་བརྡ་ཐོ་གཏོང་དགོས།(&amp;D)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show a notification when torrents &amp;finish</source>
        <translation>Torrent དུས་ནམ་ཞིག་ལ་བརྡ་ཐོ་གཏོང་བ་དང་མཇུག་སྒྲིལ་བའི་སྐབས་སུ་བརྡ་ཐོ་གཏོང་དགོས།(&amp;F)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Play a &amp;sound when torrents finish</source>
        <translation>Torrent ལེགས་འགྲུབ་བྱུང་བའི་སྐབས་སུ་གསལ་འདེབས་སྒྲ་གཏོང་བ།(&amp;S)</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="-83"/>
        <source>Testing TCP Port...</source>
        <translation>TCPམཐུད་སྣེ་ལ་ཚོད་ལྟ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-229"/>
        <source>Peer Limits</source>
        <translation>སྤྱོད་མཁན་གྱི་ཚད་བཀག</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Use PE&amp;X to find more peers</source>
        <translation>PE&amp;Xབཀོལ་ནས་མཛད་སྤྱོད་པ་མང་པོ་ཞིག་འཚོལ་གྱིན་ཡོད།(&amp;X)</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>PEX is a tool for exchanging peer lists with the peers you&apos;re connected to.</source>
        <translation>PEXནི་ཁྱེད་ཚོ་དང་འབྲེལ་བ་ཡོད་པའི་ལས་རིགས་གཅིག་པའི་མིང་ཐོ་བརྗེ་རེས་བྱེད་པའི་ལག་ཆ་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Use &amp;DHT to find more peers</source>
        <translation>DHTབཀོལ་ནས་ལས་རིགས་གཅིག་པའི་སྤྱོད་མཁན་དེ་བས་མང་བ་འཚོལ།(&amp;D)</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>DHT is a tool for finding peers without a tracker.</source>
        <translation>DHTནི་རྗེས་འདེད་འཕྲུལ་ཆས་མེད་པའི་Tracker ལས་རིགས་གཅིག་པའི་ལག་ཆ་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Use &amp;Local Peer Discovery to find more peers</source>
        <translation>ས་གནས་དེ་གའི་སྤྱོད་མཁན་གྱིས་སྔར་ལས་མང་བའི་སྤྱོད་མཁན་འཚོལ་བཞིན་པ་ཤེས།(&amp;L)</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>LPD is a tool for finding peers on your local network.</source>
        <translation>LPDནི་ཁྱོད་ཀྱི་ས་གནས་རང་གི་དྲ་རྒྱ་སྤྱོད་མཁན་རྙེད་པའི་ལག་ཆ་ཞིག</translation>
    </message>
    <message>
        <location line="-325"/>
        <source>Encryption</source>
        <translation>གསང་གྲངས་འགོད་པ།</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+160"/>
        <source>Select &quot;Torrent Done&quot; Script</source>
        <translation>Torrentཡི་འཁྲབ་གཞུང་བདམས་པ་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Select Incomplete Directory</source>
        <translation>ཆ་མི་ཚང་བའི་དཀར་ཆག་འདེམས་སྒྲུག་བྱ་དགོས།</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Select Watch Directory</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་གྱི་དཀར་ཆག་འདེམས་</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select Destination</source>
        <translation>དམིགས་ཡུལ་བདམ་པ།</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-355"/>
        <source>Adding</source>
        <translation>ཁ་སྣོན་བྱས་པ།</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Download is i&amp;nactive if data sharing stopped:</source>
        <extracomment>Please keep this phrase as short as possible, it&apos;s curently the longest and influences dialog width</extracomment>
        <translation>ཕབ་ལེན་ནི་བྱ་འགུལ་མིན་པ་རེད།གལ་ཏེ་གཞི་གྲངས་མཉམ་སྤྱོད་བྱེད་མཚམས་བཞག་ན།(&amp;N):</translation>
    </message>
    <message>
        <location line="-146"/>
        <source>Downloading</source>
        <translation>ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+205"/>
        <source>Append &quot;.&amp;part&quot; to incomplete files&apos; names</source>
        <translation>ཡིག་ཆའི་ཟུར་སྣོན་ལེགས་འགྲུབ་མ་བྱུང་བའི་ཆེད་དུ་&quot;.part&quot;་རྒྱ་བསྐྱེད་མིང་།(&amp;P</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Keep &amp;incomplete files in:</source>
        <translation>ཆ་མི་ཚང་བའི་ཡིག་ཆ་ཉར་ཚགས་བྱ་རྒྱུ་སྟེ།(&amp;I):</translation>
    </message>
    <message>
        <location line="-148"/>
        <source>Save to &amp;Location:</source>
        <translation>ཉར་ཚགས་བྱས་ནས་གནས་ཡུལ་དུ་ཉར་ཚགས།(&amp;L):</translation>
    </message>
    <message>
        <location line="+170"/>
        <source>Call scrip&amp;t when torrent is completed:</source>
        <translation>Torrent ལེགས་འགྲུབ་བྱུང་བའི་སྐབས་སུ་འཁྲབ་གཞུང་བཀོལ་སྤྱོད་བྱེད་པ།(&amp;T):</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stop seeding at &amp;ratio:</source>
        <translation>ས་བོན་ལས་མཚམས་བཞག་ནས་ལོངས་སྤྱོད་བྱེད་པའི་ཚད་ལ་སླེབས་པ།(&amp;R):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Stop seedi&amp;ng if idle for:</source>
        <translation>ས་བོན་འདེབས་མཚམས་བཞག་ནས་ཁོམ་ལོང་བྱུང་བ།&amp;N):</translation>
    </message>
    <message>
        <location line="-557"/>
        <source>Transmission Preferences</source>
        <translation>BTཕབ་ལེན་ཡོ་བྱད་ཐོག་མར་འདེམས་པ།</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
    <message>
        <location line="+703"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+91"/>
        <source>Not supported by remote sessions</source>
        <translation>རྒྱང་རིང་ཚོགས་འདུའི་རྒྱབ་སྐྱོར་མི་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Application.cc" line="-241"/>
        <source>Invalid option</source>
        <translation>གོ་མི་ཆོད་པའི་བདམས་ཚན།</translation>
    </message>
</context>
<context>
    <name>RelocateDialog</name>
    <message>
        <location filename="../RelocateDialog.cc" line="+65"/>
        <source>Select Location</source>
        <translation>གནས་ཡུལ་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../RelocateDialog.ui" line="+14"/>
        <source>Set Torrent Location</source>
        <translation>Torrentགནས་སྒྲིག་བཀོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Set Location</source>
        <translation>སྒྲིག་བཀོད་གནས་ཡུལ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>New &amp;location:</source>
        <translation>གནས་ཡུལ་གསར་པ།(&amp;L):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Move from the current folder</source>
        <translation>ད་ལྟའི་ཡིག་སྣོད་ནང་ནས་སྤོ་སྒྱུར་བྱེད་པ།(&amp;M)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Local data is &amp;already there</source>
        <translation>ས་གནས་ཀྱི་གཞི་གྲངས་ནི་དེ་གར་ཡོད་པ་རེད།(&amp;A)</translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../Session.cc" line="+559"/>
        <source>Error Renaming Path</source>
        <translation>ནོར་འཁྲུལ་གྱི་མིང་བསྒྱུར་བའི་ལམ་བུ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;p&gt;&lt;b&gt;Unable to rename &quot;%1&quot; as &quot;%2&quot;: %3.&lt;/b&gt;&lt;/p&gt; &lt;p&gt;Please correct the errors and try again.&lt;/p&gt;</source>
        <translation>&lt;p&gt; &lt;b&gt; &quot;%1&quot;ཞེས་མིང་བསྒྱུར་མི་ཐུབ་པ་དེ་&quot;%2&quot;:%3. &lt;/b&gt; &lt;/p&gt; &lt;p&gt; ནོར་འཁྲུལ་ཡོ་བསྲང་བྱས་ནས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས། &lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Add Torrent</source>
        <translation>Torrentཁ་སྣོན་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>SessionDialog</name>
    <message>
        <location filename="../SessionDialog.ui" line="+14"/>
        <source>Change Session</source>
        <translation>ཁ་བརྡ་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Source</source>
        <translation>འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Start &amp;Local Session</source>
        <translation>ས་གནས་ཀྱི་ཚོགས་འདུ་འགོ་ཚུགས་པ།(&amp;L)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to &amp;Remote Session</source>
        <translation>སྦྲེལ་མཐུད་དང་རྒྱང་ཁྲིད་ཚོགས་འདུ།(&amp;R)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Host:</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན།&amp;H):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Port:</source>
        <translation>མཐུད་སྣེ།(&amp;P):</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Authentication required</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་པར་མཁོ་བའི་ཐོབ་ཐང་།(&amp;A)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Username:</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།(&amp;U):</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Pass&amp;word:</source>
        <translation>གསང་ཨང་།(&amp;W):</translation>
    </message>
</context>
<context>
    <name>StatsDialog</name>
    <message>
        <location filename="../StatsDialog.ui" line="+14"/>
        <source>Statistics</source>
        <translation>བསྡོམས་རྩིས་ཀྱི་གཞི་གྲངས།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Current Session</source>
        <translation>སྐབས་འདིའི་ཚོགས་འདུ།</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+96"/>
        <source>Uploaded:</source>
        <translation>ཡར་བསྐུར་ཟིན་པ།</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Downloaded:</source>
        <translation>ཕབ་ལེན་བྱས་པ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Ratio:</source>
        <translation>མཉམ་སྤྱོད་བསྡུར་ཚད་ནི།</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Duration:</source>
        <translation>དུས་ཡུན་ནི།</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Total</source>
        <translation>བསྡོམས་འབོར།</translation>
    </message>
    <message numerus="yes">
        <location filename="../StatsDialog.cc" line="+71"/>
        <source>Started %Ln time(s)</source>
        <translation>
            <numerusform>%Lnདག་གི་དུས་ཚོད་འགོ་བརྩམས།</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Torrent</name>
    <message>
        <location filename="../Torrent.cc" line="+774"/>
        <source>Verifying local data</source>
        <translation>ས་གནས་དེ་གའི་གཞི་གྲངས་ལ་ཞིབ་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Downloading</source>
        <translation>ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seeding</source>
        <translation>ས་བོན་འདེབས་པ།</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Paused</source>
        <translation>མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Queued for verification</source>
        <translation>གྲལ་བསྒྲིགས་ནས་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Queued for download</source>
        <translation>གྲལ་བསྒྲིགས་ནས་ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Queued for seeding</source>
        <translation>གྲལ་བསྒྲིགས་ནས་ས་བོན་འདེབས་པ།</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Tracker gave a warning: %1</source>
        <translation>Trackerརྗེས་འདེད་མཁན་གྱིས་ཐ་ཚིག་སྒྲོག་རྒྱུར། %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tracker gave an error: %1</source>
        <translation>Trackerཀྱིས་ནོར་འཁྲུལ་ཞིག་བྱུང་བ་སྟེ། %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: %1</source>
        <translation>ནོར་འཁྲུལ། %1</translation>
    </message>
</context>
<context>
    <name>TorrentDelegate</name>
    <message>
        <location filename="../TorrentDelegate.cc" line="+171"/>
        <source>Magnetized transfer - retrieving metadata (%1%)</source>
        <extracomment>First part of torrent progress string; %1 is the percentage of torrent metadata downloaded</extracomment>
        <translation>ཁབ་ལེན་ཅན་གྱི་སྤོ་སྒྱུར་ - ཕྱིར་ལེན་པའི་རྒྱུའི་གཞི་གྲངས་(%1%)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 of %2 (%3%)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is how much we&apos;ll have when done, %3 is a percentage of the two</extracomment>
        <translation>%1 / %2 (%3%)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 of %2 (%3%), uploaded %4 (Ratio: %5 Goal: %6)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is the torrent&apos;s total size, %3 is a percentage of the two, %4 is how much we&apos;ve uploaded, %5 is our upload-to-download ratio, %6 is the ratio we want to reach before we stop uploading</extracomment>
        <translation>%1 / %2 (%3%), ཡར་བསྐུར་ཟིན་པ། %4 མཉམ་སྤྱོད་ཕྱོད: %5 དམིགས་འབེབ: %6)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 of %2 (%3%), uploaded %4 (Ratio: %5)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is the torrent&apos;s total size, %3 is a percentage of the two, %4 is how much we&apos;ve uploaded, %5 is our upload-to-download ratio</extracomment>
        <translation>%2(%3%)ཡི་ཁྲོད་དུ་%1,%4(མཉམ་སྤྱོད་བསྡུར་ཚད:%5)</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>%1, uploaded %2 (Ratio: %3 Goal: %4)</source>
        <extracomment>First part of torrent progress string; %1 is the torrent&apos;s total size, %2 is how much we&apos;ve uploaded, %3 is our upload-to-download ratio, %4 is the ratio we want to reach before we stop uploading</extracomment>
        <translation>%1, ཡར་བསྐུར་བ། %2 (མཉམ་སྤྱོད་བསྡུར་ཚད: %3 དམིགས་འབེབ།: %4)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1, uploaded %2 (Ratio: %3)</source>
        <extracomment>First part of torrent progress string; %1 is the torrent&apos;s total size, %2 is how much we&apos;ve uploaded, %3 is our upload-to-download ratio</extracomment>
        <translation>%1, ཡར་བསྐུར་ཟིན%2 (མཉམ་སྤྱོད་བསྡུར་ཚད: %3)</translation>
    </message>
    <message>
        <location line="+14"/>
        <source> - %1 left</source>
        <extracomment>Second (optional) part of torrent progress string; %1 is duration; notice that leading space (before the dash) is included here</extracomment>
        <translation> - %1 ལྷག་མ།</translation>
    </message>
    <message>
        <location line="+4"/>
        <source> - Remaining time unknown</source>
        <extracomment>Second (optional) part of torrent progress string; notice that leading space (before the dash) is included here</extracomment>
        <translation> - དུས་ཚོད་ལྷག་མ་མི་ཤེས་པ།</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Ratio: %1</source>
        <translation>མཉམ་སྤྱོད་བསྡུར་ཚད། %1</translation>
    </message>
    <message numerus="yes">
        <location line="+41"/>
        <source>Downloading from %Ln peer(s)</source>
        <extracomment>First part of phrase &quot;Downloading from ... peer(s) and ... web seed(s)&quot;</extracomment>
        <translation>
            <numerusform>%Lnསྤྱོད་མཁན་གྱིས་ཕབ་ལེན་བྱེད་བཞིན་པའི་སྒང་རེད་།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+15"/>
        <source>Seeding to %Ln peer(s)</source>
        <translation>
            <numerusform>%Lnལས་རིགས་གཅིག་པའི་ས་བོན་བཏབ་པ།</numerusform>
        </translation>
    </message>
    <message>
        <location line="+15"/>
        <source> - </source>
        <translation> - </translation>
    </message>
    <message numerus="yes">
        <location line="-38"/>
        <source>Downloading metadata from %Ln peer(s) (%1% done)</source>
        <translation>
            <numerusform>%Ln་སྤྱོད་མཁན་གྱིས་སྒོར་མོའི་གཞི་གྲངས་ཕབ་ལེན་བྱེད་བཞིན་ཡོད།(%1%)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>Downloading from %1 of %Ln connected peer(s)</source>
        <extracomment>First part of phrase &quot;Downloading from ... of ... connected peer(s) and ... web seed(s)&quot;</extracomment>
        <translation>
            <numerusform>%1 / %Ln སྦྲེལ་མཐུད་སྤྱོད་མཁན་གྱིས་ཕབ་ལེན་བྱེད་པ།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source> and %Ln web seed(s)</source>
        <extracomment>Second (optional) part of phrase &quot;Downloading from ... of ... connected peer(s) and ... web seed(s)&quot;; notice that leading space (before &quot;and&quot;) is included here</extracomment>
        <translation>
            <numerusform> དང་%Lnདྲ་རྒྱའི་ WEB ས་བོན་ཡང་ཡོད།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+8"/>
        <source>Seeding to %1 of %Ln connected peer(s)</source>
        <translation>
            <numerusform>%1 / %Ln  འབྲེལ་མཐུད་བྱས་པའི་ལས་རིགས་གཅིག་པའི་ཁྲོད་དུ་སོན་པ།</numerusform>
        </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location line="-70"/>
        <source>Verifying local data (%1% tested)</source>
        <translation>ས་གནས་དེ་གའི་གཞི་གྲངས་ལ་ཞིབ་བཤེར་བྱ་དགོས། (%1%ལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པ)</translation>
    </message>
</context>
<context>
    <name>TrackerDelegate</name>
    <message numerus="yes">
        <location filename="../TrackerDelegate.cc" line="+212"/>
        <source>Got a list of%1 %Ln peer(s)%2 %3 ago</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation>
            <numerusform>%3ཟིན་པའི%1 %Lnལས་རིགས་གཅིག་པའི་2%ཡི་སྔོན་ལ་ཟིན་ཡོད།</numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Peer list request %1timed out%2 %3 ago; will retry</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation>3%་གྱི་སྔོན་ལ་བཀོལ་མཁན་གྱི་རེའུ་མིག%1གི་དུས་བརྒལ་%2།ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་པ།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Got an error %1&quot;%2&quot;%3 %4 ago</source>
        <extracomment>%1 and %3 are replaced with HTML markup, %2 is error message, %4 is duration</extracomment>
        <translation>ནོར་འཁྲུལ་བྱུང་བའི་རྐྱེན་གྱིས་%1&quot;%2&quot;%3%4%ཡི་སྔོན་ལ་ནོར་འཁྲུལ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No updates scheduled</source>
        <translation>གསར་བསྒྱུར་འཆར་གཞིའི་ལས་འགན་མེད།</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Asking for more peers in %1</source>
        <extracomment>%1 is duration</extracomment>
        <translation>%1་རྗེས་ནས་མཛད་སྤྱོད་པ་མང་པོ་ཞིག་ལ་རེ་བ་ཞུས།</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Queued to ask for more peers</source>
        <translation>གྲལ་བསྒྲིགས་ནས་ལས་རིགས་གཅིག་པའི་མི་དེ་བས་མང་བ།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Asking for more peers now... &lt;small&gt;%1&lt;/small&gt;</source>
        <extracomment>%1 is duration</extracomment>
        <translation>ད་ལྟ་ལས་རིགས་གཅིག་པའི་མི་དེ་བས་མང་བ་ཞིག་ &lt;small&gt;%1&lt;/small&gt;</translation>
    </message>
    <message numerus="yes">
        <location line="+17"/>
        <source>Tracker had%1 %Ln seeder(s)%2</source>
        <extracomment>First part of phrase &quot;Tracker had ... seeder(s) and ... leecher(s) ... ago&quot;; %1 and %2 are replaced with HTML markup</extracomment>
        <translation>
            <numerusform>Tracker་ལ%1%Lnས་བོན་འདེབས་མཁན་2%ཡོད།</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source> and%1 %Ln leecher(s)%2 %3 ago</source>
        <extracomment>Second part of phrase &quot;Tracker had ... seeder(s) and ... leecher(s) ... ago&quot;; %1 and %2 are replaced with HTML markup, %3 is duration; notice that leading space (before &quot;and&quot;) is included here</extracomment>
        <translation>
            <numerusform> %1 %Lnལ་ཕབ་ལེན་སྤྱོད་མཁན་%2 ཡི་%3སྔོན་ཡིན་པ།</numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Tracker had %1no information%2 on peer counts %3 ago</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation>%3་གྱི་སྔོན་གྱི་སྤྱོད་མཁན་གྱི་གྲངས་འབོར་%1་ལ་ཆ་འཕྲིན་%2མེད།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Got a scrape error %1&quot;%2&quot;%3 %4 ago</source>
        <extracomment>%1 and %3 are replaced with HTML markup, %2 is error message, %4 is duration</extracomment>
        <translation>%4་ཡི་སྔོན་ལ་རྣམ་པ་ཞིག་འཚོལ་བཤེར་བྱས་ནས་ནོར་འཁྲུལ་%1&quot;%2&quot;%3་ཐོབ།</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Asking for peer counts in %1</source>
        <extracomment>%1 is duration</extracomment>
        <translation>%1་གི་རྗེས་ནས་མཛད་སྤྱོད་པའི་གྲངས་ཀ་རེ་ཞུ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Queued to ask for peer counts</source>
        <translation>གྲལ་སྒྲིག་ནས་མཛད་སྤྱོད་པའི་གྲངས་ཀ་རེ་ཞུ་བྱས།</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Asking for peer counts now... &lt;small&gt;%1&lt;/small&gt;</source>
        <extracomment>%1 is duration</extracomment>
        <translation>སྤྱོད་མཁན་གྱི་ཁ་གྲངས་རེ་ཞུ་བྱེད་བཞིན་ཡོད། &lt;small&gt;%1&lt;/small&gt;</translation>
    </message>
</context>
</TS>
