<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../AboutDialog.ui" line="+14"/>
        <source>About Transmission</source>
        <translation>關於 BT 下載工具</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cc" line="+29"/>
        <source>&lt;b style=&apos;font-size:x-large&apos;&gt;Transmission %1&lt;/b&gt;</source>
        <translation>&lt;b style=&apos;font-size：x-large&apos;&gt;BT下載工具 %1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../AboutDialog.ui" line="+26"/>
        <source>A fast and easy BitTorrent client</source>
        <translation>一個快速和簡單的 BitTorrent 用戶端</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Copyright (c) The Transmission Project</source>
        <translation>Copyright （c） The BT 下載工具 Project</translation>
    </message>
    <message>
        <location filename="../AboutDialog.cc" line="+4"/>
        <source>C&amp;redits</source>
        <translation>感謝（&amp;R）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;License</source>
        <translation>許可協定（&amp;L）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Credits</source>
        <translation>感謝</translation>
    </message>
</context>
<context>
    <name>Application</name>
    <message>
        <location filename="../Application.cc" line="+281"/>
        <source>&lt;b&gt;Transmission is a file sharing program.&lt;/b&gt;</source>
        <translation>&lt;b&gt;BT下載工具 是一個檔案共享程式。 &lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>When you run a torrent, its data will be made available to others by means of upload. Any content you share is your sole responsibility.</source>
        <translation>當您運行一個 Torrent，其數據將以上傳的方式提供給他人。 您要對您所共享的內容負唯一的責任。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>I &amp;Agree</source>
        <translation>我同意（&amp;A）</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Torrent Completed</source>
        <translation>Torrent 已完成</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Torrent Added</source>
        <translation>Torrent 已添加</translation>
    </message>
</context>
<context>
    <name>DetailsDialog</name>
    <message>
        <location filename="../DetailsDialog.cc" line="+364"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mixed</source>
        <translation>混合</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+218"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location line="-179"/>
        <source>Finished</source>
        <translation>已完成</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Paused</source>
        <translation>已暫停</translation>
    </message>
    <message>
        <location line="+204"/>
        <source>Active now</source>
        <translation>當前活動</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 ago</source>
        <translation>%1 之前</translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>%1 (%Ln pieces @ %2)</source>
        <translation>
            <numerusform>%1 （%Ln 個區塊 @ %2）</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%1 (%Ln pieces)</source>
        <translation>
            <numerusform>%1 （%Ln 個區塊）</numerusform>
        </translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Private to this tracker -- DHT and PEX disabled</source>
        <translation>當前 Tracker 設置為私有 -- DHT 和 PEX 被禁用</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Public torrent</source>
        <translation>公開的 Torrent</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Created by %1</source>
        <translation>由 %1 製作</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created on %1</source>
        <translation>製作於 %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created by %1 on %2</source>
        <translation>由 %1 製作於 %2</translation>
    </message>
    <message>
        <location line="+123"/>
        <location line="+23"/>
        <source>Encrypted connection</source>
        <translation>加密連接</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Optimistic unchoke</source>
        <translation>樂觀解鎖</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Downloading from this peer</source>
        <translation>正在從該用戶下載</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We would download from this peer if they would let us</source>
        <translation>如果他們允許，我們將從該用戶下載</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uploading to peer</source>
        <translation>正在上傳給該使用者</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We would upload to this peer if they asked</source>
        <translation>如果他們請求，我們將上傳給該使用者</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer has unchoked us, but we&apos;re not interested</source>
        <translation>該使用者已對我們解鎖，但我們不感興趣</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>We unchoked this peer, but they&apos;re not interested</source>
        <translation>我們已對該使用者解鎖，但他們不感興趣</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Peer was discovered through DHT</source>
        <translation>通過 DHT 發現的使用者</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer was discovered through Peer Exchange (PEX)</source>
        <translation>通過使用者交換（PEX）發現的使用者</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Peer is an incoming connection</source>
        <translation>使用者是一個入站的連接</translation>
    </message>
    <message numerus="yes">
        <location line="+120"/>
        <source> minute(s)</source>
        <extracomment>Spin box suffix, &quot;Stop seeding if idle for: [ 5 minutes ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> 分鐘</numerusform>
        </translation>
    </message>
    <message>
        <location line="+45"/>
        <location line="+12"/>
        <location line="+34"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location line="-34"/>
        <source>Tracker already exists.</source>
        <translation>Tracker 已經存在。</translation>
    </message>
    <message>
        <location line="-628"/>
        <source>%1 (100%)</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded and verified data</extracomment>
        <translation>%1 (100%)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 of %2 (%3%)</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded and verified data, %2 is overall size of torrent data, %3 is percentage (%1/%2*100)</extracomment>
        <translation>%1 / %2 (%3%)</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 of %2 (%3%), %4 Unverified</source>
        <extracomment>Text following the &quot;Have:&quot; label in torrent properties dialog; %1 is amount of downloaded data (both verified and unverified), %2 is overall size of torrent data, %3 is percentage (%1/%2*100), %4 is amount of downloaded but not yet verified data</extracomment>
        <translation>%1 / %2 （% 3%）， %4 未檢查</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>%1 (%2 corrupt)</source>
        <translation>% 1 （% 2 損壞 ）</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1 (Ratio: %2)</source>
        <translation>%1 （分享率： %2）</translation>
    </message>
    <message>
        <location line="+220"/>
        <source>N/A</source>
        <translation>N/A</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>Peer is connected over uTP</source>
        <translation>通過 uTP 連接的使用者</translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Add URL </source>
        <translation>添加連結 </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add tracker announce URL:</source>
        <translation>新增 Tracker 通告連結：</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+46"/>
        <source>Invalid URL &quot;%1&quot;</source>
        <translation>無效連結 %1</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Edit URL </source>
        <translation>編輯連結 </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit tracker announce URL:</source>
        <translation>編輯 Tracker 通告連結：</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>High</source>
        <translation>高</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal</source>
        <translation>普通</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+4"/>
        <source>Use Global Settings</source>
        <translation>使用全域設置</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Seed regardless of ratio</source>
        <translation>不管分享率多少都做種</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop seeding at ratio:</source>
        <translation>停止做種當分享率達到：</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Seed regardless of activity</source>
        <translation>不管活動狀態如何都做種</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop seeding if idle for:</source>
        <translation>停止做種當空閒達到：</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Up</source>
        <translation>上傳</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Down</source>
        <translation>下載</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Status</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Client</source>
        <translation>用戶端</translation>
    </message>
    <message>
        <location filename="../DetailsDialog.ui" line="+14"/>
        <source>Torrent Properties</source>
        <translation>Torrent 屬性</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Information</source>
        <translation>資訊</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Activity</source>
        <translation>活動</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Have:</source>
        <translation>已有：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Availability:</source>
        <translation>健康度：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Uploaded:</source>
        <translation>已上傳：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Downloaded:</source>
        <translation>已下載：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>State:</source>
        <translation>狀態：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Running time:</source>
        <translation>執行時間：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Remaining time:</source>
        <translation>剩餘時間：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Last activity:</source>
        <translation>上次活動：</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Error:</source>
        <translation>錯誤：</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Details</source>
        <translation>詳細資訊</translation>
    </message>
    <message>
        <location line="+109"/>
        <source>Size:</source>
        <translation>大小：</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Location:</source>
        <translation>位置：</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Hash:</source>
        <translation>散列：</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Privacy:</source>
        <translation>隱私：</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Origin:</source>
        <translation>來源：</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Comment:</source>
        <translation>說明：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Peers</source>
        <translation>使用者</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Tracker</source>
        <translation>Tracker</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Add Tracker</source>
        <translation>添加 Tracker</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Edit Tracker</source>
        <translation>編輯 Tracker</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Remove Trackers</source>
        <translation>拿掉 Tracker</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Show &amp;more details</source>
        <translation>顯示更多詳細資訊（&amp;M）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;backup trackers</source>
        <translation>顯示備用 Tracker（&amp;B）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Files</source>
        <translation>檔</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Honor global &amp;limits</source>
        <translation>遵循全域限制（&amp;L）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Limit &amp;download speed:</source>
        <translation>限制下載速度（&amp;D）：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Limit &amp;upload speed:</source>
        <translation>限制上傳速度（&amp;U）：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Torrent &amp;priority:</source>
        <translation>Torrent 優先順序（&amp;P）：</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Seeding Limits</source>
        <translation>做種限制</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Ratio:</source>
        <translation>分享率（&amp;R）：</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Idle:</source>
        <translation>空閒（&amp;I）：</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Peer Connections</source>
        <translation>用戶連接</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Maximum peers:</source>
        <translation>最大用戶數（&amp;M）：</translation>
    </message>
</context>
<context>
    <name>FileAdded</name>
    <message>
        <location filename="../Session.cc" line="+94"/>
        <source>Add Torrent</source>
        <translation>添加 Torrent</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;p&gt;&lt;b&gt;Unable to add &quot;%1&quot;.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;It is a duplicate of &quot;%2&quot; which is already added.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;無法添加%1。 &lt;/b&gt;&lt;/p&gt;&lt;p&gt;它與已經添加的 「%2」 重複。 &lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+12"/>
        <source>Error Adding Torrent</source>
        <translation>添加 Torrent 錯誤</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>Invalid Or Corrupt Torrent File</source>
        <translatorcomment>无效或损坏的种子文件</translatorcomment>
        <translation>無效或損壞的種子檔</translation>
    </message>
</context>
<context>
    <name>FileTreeItem</name>
    <message>
        <location filename="../FileTreeItem.cc" line="+271"/>
        <location filename="../FileTreeView.cc" line="+105"/>
        <location line="+257"/>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-256"/>
        <location line="+254"/>
        <source>High</source>
        <translation>高</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-255"/>
        <location line="+256"/>
        <source>Normal</source>
        <translation>普通</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../FileTreeView.cc" line="-255"/>
        <source>Mixed</source>
        <translation>混合</translation>
    </message>
</context>
<context>
    <name>FileTreeModel</name>
    <message>
        <location filename="../FileTreeModel.cc" line="+196"/>
        <source>File</source>
        <translation>檔</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Progress</source>
        <translation>進度</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Download</source>
        <translation>下載</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Priority</source>
        <translation>優先順序</translation>
    </message>
</context>
<context>
    <name>FileTreeView</name>
    <message>
        <location filename="../FileTreeView.cc" line="+247"/>
        <source>Check Selected</source>
        <translation>勾選選擇</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Uncheck Selected</source>
        <translation>取消勾選選擇</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Only Check Selected</source>
        <translation>僅勾選選擇</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Priority</source>
        <translation>優先順序</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rename...</source>
        <translation>重新命名...</translation>
    </message>
</context>
<context>
    <name>FilterBar</name>
    <message>
        <location filename="../FilterBar.cc" line="+61"/>
        <location line="+143"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location line="-136"/>
        <source>Active</source>
        <translation>活動</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Downloading</source>
        <translation>正在下載</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Seeding</source>
        <translation>正在做種</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paused</source>
        <translation>已暫停</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finished</source>
        <translation>已完成</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Verifying</source>
        <translation>正在檢查</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Show:</source>
        <translation>顯示：</translation>
    </message>
</context>
<context>
    <name>FilterBarLineEdit</name>
    <message>
        <location filename="../FilterBarLineEdit.cc" line="+48"/>
        <source>Search...</source>
        <translation>搜尋...</translation>
    </message>
</context>
<context>
    <name>Formatter</name>
    <message>
        <location filename="../Formatter.cc" line="+33"/>
        <source>B/s</source>
        <translation>B/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>kB/s</source>
        <translation>kB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MB/s</source>
        <translation>MB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GB/s</source>
        <translation>GB/s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TB/s</source>
        <translation>TB/s</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+12"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>kB</source>
        <translation>kB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TiB</source>
        <translation>TiB</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+14"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location line="-11"/>
        <location line="+14"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+8"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message numerus="yes">
        <location line="+32"/>
        <source>%Ln day(s)</source>
        <translation>
            <numerusform>%Ln 天</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln hour(s)</source>
        <translation>
            <numerusform>%Ln 小時</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln minute(s)</source>
        <translation>
            <numerusform>%Ln 分鐘</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln second(s)</source>
        <translation>
            <numerusform>%Ln 秒</numerusform>
        </translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+7"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
</context>
<context>
    <name>FreeSpaceLabel</name>
    <message>
        <location filename="../FreeSpaceLabel.cc" line="+58"/>
        <source>&lt;i&gt;Calculating Free Space...&lt;/i&gt;</source>
        <translation>&lt;i&gt;正在計算可用空間...&lt;/i&gt;</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>%1 free</source>
        <translation>%1 可用</translation>
    </message>
</context>
<context>
    <name>LicenseDialog</name>
    <message>
        <location filename="../LicenseDialog.ui" line="+14"/>
        <source>License</source>
        <translation>許可協定</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="+14"/>
        <location filename="../MainWindow.cc" line="+645"/>
        <source>Transmission</source>
        <translation>BT下載工具</translation>
    </message>
    <message>
        <location line="+175"/>
        <source>&amp;Torrent</source>
        <translation>Torrent(&amp;T)</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>&amp;Edit</source>
        <translation>編輯（&amp;E）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Help</source>
        <translation>說明（&amp;H）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;View</source>
        <translation>檢視（&amp;V）</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;File</source>
        <translation>檔案（&amp;F）</translation>
    </message>
    <message>
        <location line="+66"/>
        <source>&amp;New...</source>
        <translation>新增（&amp;N）...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Create a new torrent</source>
        <translation>製作一個新的 Torrent</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Properties</source>
        <translation>屬性（&amp;P）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Show torrent properties</source>
        <translation>顯示 Torrent 屬性</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Open the torrent&apos;s folder</source>
        <translation>打開 Torrent 的資料夾</translation>
    </message>
    <message>
        <location line="-161"/>
        <source>Queue</source>
        <translation>佇列</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>&amp;Open...</source>
        <translation>開啟（&amp;O）...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open a torrent</source>
        <translation>打開一個 Torrent</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Open Fold&amp;er</source>
        <translation>開啟資料夾（&amp;E）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Start</source>
        <translation>開始（&amp;S）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start torrent</source>
        <translation>開始 Torrent</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Ask Tracker for &amp;More Peers</source>
        <translation>向 Tracker 請求更多使用者（&amp;M）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ask tracker for more peers</source>
        <translation>向 Tracker 請求更多使用者</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Pause</source>
        <translation>暫停（&amp;P）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pause torrent</source>
        <translation>暫停 Torrent</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Verify Local Data</source>
        <translation>檢查本地資料（&amp;V）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Verify local data</source>
        <translation>檢查本地數據</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Remove</source>
        <translation>移除（&amp;R）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove torrent</source>
        <translation>拿掉 Torrent</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Delete Files and Remove</source>
        <translation>刪除檔案並移除（&amp;D）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove torrent and delete its files</source>
        <translation>拿掉 Torrent 並刪除它的檔案</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Start All</source>
        <translation>全部開始（&amp;S）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Pause All</source>
        <translation>全部暫停（&amp;P）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Quit</source>
        <translation>離開（&amp;Q）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Select All</source>
        <translation>全選（&amp;S）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Deselect All</source>
        <translation>取消全選（&amp;D）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Preferences</source>
        <translation>喜好設定（&amp;P）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Compact View</source>
        <translation>緊湊型檢視（&amp;C）</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+3"/>
        <source>Compact View</source>
        <translation>緊湊型檢視</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Toolbar</source>
        <translation>工具列（&amp;T）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Filterbar</source>
        <translation>過濾列（&amp;F）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Statusbar</source>
        <translation>狀態列（&amp;S）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Activity</source>
        <translation>依活動排序（&amp;A）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by A&amp;ge</source>
        <translation>依存在時間排序（&amp;G）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Time &amp;Left</source>
        <translation>依剩餘時間排序（&amp;L）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Name</source>
        <translation>依名稱排序（&amp;N）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Progress</source>
        <translation>依進度排序（&amp;P）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Rati&amp;o</source>
        <translation>依分享率排序（&amp;O）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Si&amp;ze</source>
        <translation>依大小排序（&amp;Z）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by Stat&amp;e</source>
        <translation>依狀態排序（&amp;E）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by T&amp;racker</source>
        <translation>按 Tracker 排序（&amp;R）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Message &amp;Log</source>
        <translation>訊息記錄（&amp;L）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Statistics</source>
        <translation>統計（&amp;S）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Contents</source>
        <translation>內容（&amp;C）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;About</source>
        <translation>關於（&amp;A）</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Re&amp;verse Sort Order</source>
        <translation>倒序（&amp;V）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Name</source>
        <translation>名稱（&amp;N）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Files</source>
        <translation>檔案（&amp;F）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Tracker</source>
        <translation>Tracker(&amp;T)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Total Ratio</source>
        <translation>總分享率</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Ratio</source>
        <translation>會話分享率</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Total Transfer</source>
        <translation>總傳輸量</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Session Transfer</source>
        <translation>會話傳輸量</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Main Window</source>
        <translation>主視窗（&amp;M）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Tray &amp;Icon</source>
        <translation>托盤圖示（&amp;I）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Change Session...</source>
        <translation>變更工作階段（&amp;C）...</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose Session</source>
        <extracomment>Start a local session or connect to a running session</extracomment>
        <translation>選擇工作階段</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Set &amp;Location...</source>
        <translation>設定位置（&amp;L）...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy Magnet Link to Clipboard</source>
        <translation>複製磁力連結到剪貼簿（&amp;C）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open &amp;URL...</source>
        <translation>開啟連結（&amp;U）...</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Donate</source>
        <translation>捐贈（&amp;D）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Start &amp;Now</source>
        <translation>立刻開始（&amp;N）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bypass the queue and start now</source>
        <translation>繞開佇列並立刻開始</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Move to &amp;Top</source>
        <translation>移至頂端（&amp;T）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move &amp;Up</source>
        <translation>上移（&amp;U）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move &amp;Down</source>
        <translation>下移（&amp;D）</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Move to &amp;Bottom</source>
        <translation>移至底部（&amp;B）</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Sort by &amp;Queue</source>
        <translation>依佇列排序（&amp;Q）</translation>
    </message>
    <message>
        <location filename="../MainWindow.cc" line="-211"/>
        <source>Limit Download Speed</source>
        <translation>限制下載速度</translation>
    </message>
    <message>
        <location line="-54"/>
        <source>Unlimited</source>
        <translation>不限制</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+641"/>
        <location line="+8"/>
        <source>Limited at %1</source>
        <translation>限制在 %1</translation>
    </message>
    <message>
        <location line="-599"/>
        <source>Limit Upload Speed</source>
        <translation>限制上傳速度</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop Seeding at Ratio</source>
        <translation>停止做種當分享率達到</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Seed Forever</source>
        <translation>一直做種</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+627"/>
        <source>Stop at Ratio (%1)</source>
        <translation>停止當分享率達到 （% 1）</translation>
    </message>
    <message>
        <location line="-393"/>
        <source> - %1:%2</source>
        <extracomment>Second (optional) part of main window title &quot;Transmission - host:port&quot; (added when connected to remote session); notice that leading space (before the dash) is included here</extracomment>
        <translation> - %1:%2</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Idle</source>
        <translation>空閒</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+17"/>
        <source>Ratio: %1</source>
        <translation>分享率： %1</translation>
    </message>
    <message>
        <location line="-12"/>
        <location line="+6"/>
        <source>Down: %1, Up: %2</source>
        <translation>下載： %1， 上傳： %2</translation>
    </message>
    <message>
        <location line="+421"/>
        <source>Torrent Files (*.torrent);;All Files (*.*)</source>
        <translation>Torrent 檔 （*.torrent）;; 全部檔案 （*.*）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show &amp;options dialog</source>
        <translation>顯示選項對話框（&amp;O）</translation>
    </message>
    <message>
        <location line="-9"/>
        <source>Open Torrent</source>
        <translation>打開 Torrent</translation>
    </message>
    <message>
        <location line="-918"/>
        <source>Speed Limits</source>
        <translation>速度限制</translation>
    </message>
    <message>
        <location line="+449"/>
        <source>Network Error</source>
        <translation>網路錯誤</translation>
    </message>
    <message>
        <location line="+433"/>
        <source>Click to disable Temporary Speed Limits
 (%1 down, %2 up)</source>
        <translation>點擊禁用臨時速度限制
 （% 1 下載， % 2 上傳 ）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Click to enable Temporary Speed Limits
 (%1 down, %2 up)</source>
        <translation>點擊啟用臨時速度限制
 （% 1 下載， % 2 上傳 ）</translation>
    </message>
    <message>
        <location line="+133"/>
        <source>Remove torrent?</source>
        <translation>拿掉 Torrent？</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete this torrent&apos;s downloaded files?</source>
        <translation>刪除此 Torrent 已下載的檔案？</translation>
    </message>
    <message numerus="yes">
        <location line="-5"/>
        <source>Remove %Ln torrent(s)?</source>
        <translation>
            <numerusform>拿掉 %Ln 個 Torrent？</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="-497"/>
        <source>Showing %L1 of %Ln torrent(s)</source>
        <translation>
            <numerusform>顯示 %L1 / %Ln 個 Torrent</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+503"/>
        <source>Delete these %Ln torrent(s)&apos; downloaded files?</source>
        <translation>
            <numerusform>刪除這些 %Ln 個 Torrent 已下載的檔案？</numerusform>
        </translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Once removed, continuing the transfer will require the torrent file or magnet link.</source>
        <translation>一旦移除，繼續該傳輸將會要求 Torrent 檔或者磁力連結。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Once removed, continuing the transfers will require the torrent files or magnet links.</source>
        <translation>一旦移除，繼續這些傳輸將會要求 Torrent 檔或者磁力連結。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This torrent has not finished downloading.</source>
        <translation>該 Torrent 還沒有完成下載。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>These torrents have not finished downloading.</source>
        <translation>這些 Torrent 還沒有完成下載。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This torrent is connected to peers.</source>
        <translation>該 Torrent 已連接到使用者。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>These torrents are connected to peers.</source>
        <translation>這些 Torrent 已連接到使用者。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One of these torrents is connected to peers.</source>
        <translation>其中有個 Torrent 已連接到使用者。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Some of these torrents are connected to peers.</source>
        <translation>其中有些 Torrent 已連接到使用者。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>One of these torrents has not finished downloading.</source>
        <translation>其中有個 Torrent 還沒有完成下載。</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Some of these torrents have not finished downloading.</source>
        <translation>其中有些 Torrent 還沒有完成下載。</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>%1 has not responded yet</source>
        <translation>%1 還沒有回應</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 is responding</source>
        <translation>%1 正在回應</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 last responded %2 ago</source>
        <translation>%1 上次回應在 %2 之前</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 is not responding</source>
        <translation>%1 沒有回應</translation>
    </message>
</context>
<context>
    <name>MakeDialog</name>
    <message>
        <location filename="../MakeDialog.ui" line="+17"/>
        <source>New Torrent</source>
        <translation>新的 Torrent</translation>
    </message>
    <message>
        <location filename="../MakeDialog.cc" line="+201"/>
        <source>&lt;i&gt;No source selected&lt;i&gt;</source>
        <translation>&lt;i&gt;沒有選擇來源&lt;i&gt;</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>%Ln File(s)</source>
        <translation>
            <numerusform>%Ln 個檔</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>%Ln Piece(s)</source>
        <translation>
            <numerusform>%Ln 個區塊</numerusform>
        </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 in %2; %3 @ %4</source>
        <translation>%1 / %2; %3 @ %4</translation>
    </message>
    <message>
        <location filename="../MakeDialog.ui" line="+9"/>
        <source>Files</source>
        <translation>檔</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Sa&amp;ve to:</source>
        <translation>儲存到（&amp;V）：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Source f&amp;older:</source>
        <translation>來源資料夾（&amp;O）：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Source &amp;file:</source>
        <translation>來源檔（&amp;F）：</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Properties</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Trackers:</source>
        <translation>Tracker(&amp;T):</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>To add a backup URL, add it on the line after the primary URL.
To add another primary URL, add it after a blank line.</source>
        <translation>要添加一個備用連結，請將其添加到主連結的下一行。
要添加另一個主連結，請將其添加到一個空行的下一行。</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Co&amp;mment:</source>
        <translation>說明（&amp;M）：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Private torrent</source>
        <translation>私有 Torrent（&amp;P）</translation>
    </message>
</context>
<context>
    <name>MakeProgressDialog</name>
    <message>
        <location filename="../MakeProgressDialog.ui" line="+14"/>
        <source>New Torrent</source>
        <translation>新的 Torrent</translation>
    </message>
    <message>
        <location filename="../MakeDialog.cc" line="-108"/>
        <source>Creating &quot;%1&quot;</source>
        <translation>正在製作 %1”</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Created &quot;%1&quot;!</source>
        <translation>已製作 %1！</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: invalid announce URL &quot;%1&quot;</source>
        <translation>錯誤： 無效的通告連結 %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancelled</source>
        <translation>已取消</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error reading &quot;%1&quot;: %2</source>
        <translation>讀取 %1 時發生錯誤： %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Error writing &quot;%1&quot;: %2</source>
        <translation>寫入 %1 時發生錯誤： %2</translation>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../OptionsDialog.cc" line="+56"/>
        <source>Open Torrent</source>
        <translation>打開 Torrent</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Open Torrent from File</source>
        <translation>從文件打開 Torrent</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open Torrent from URL or Magnet Link</source>
        <translation>從連結或磁力鏈接打開 Torrent</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+17"/>
        <source>&amp;Source:</source>
        <translation>來源（&amp;S）：</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>&amp;Destination folder:</source>
        <translation>目標資料夾（&amp;D）：</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="+60"/>
        <source>High</source>
        <translation>高</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal</source>
        <translation>普通</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+35"/>
        <source>&amp;Priority:</source>
        <translation>優先順序（&amp;P）：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>S&amp;tart when added</source>
        <translation>新增後開始（&amp;T）</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="+5"/>
        <source>&amp;Verify Local Data</source>
        <translation>檢查本地資料（&amp;V）</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.ui" line="+7"/>
        <source>Mo&amp;ve .torrent file to the trash</source>
        <translation>將 .torrent 檔案移至資源回收桶（&amp;V）</translation>
    </message>
    <message>
        <location filename="../OptionsDialog.cc" line="-55"/>
        <source>Torrent Files (*.torrent);;All Files (*.*)</source>
        <translation>Torrent 檔 （*.torrent）;; 全部檔案 （*.*）</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Select Destination</source>
        <translation>選擇目標</translation>
    </message>
</context>
<context>
    <name>PathButton</name>
    <message>
        <location filename="../PathButton.cc" line="+31"/>
        <location line="+72"/>
        <source>(None)</source>
        <translation>（無）</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Select Folder</source>
        <translation>選擇資料夾</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Select File</source>
        <translation>選擇檔案</translation>
    </message>
</context>
<context>
    <name>PrefsDialog</name>
    <message>
        <location filename="../PrefsDialog.ui" line="+1139"/>
        <source>Use &amp;authentication</source>
        <translation>使用身份驗證（&amp;A）</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Username:</source>
        <translation>使用者名（&amp;U）：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Pass&amp;word:</source>
        <translation>密碼（&amp;W）：</translation>
    </message>
    <message>
        <location line="-53"/>
        <source>&amp;Open web client</source>
        <translation>開啟 WEB 用戶端（&amp;O）</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Addresses:</source>
        <translation>位址：</translation>
    </message>
    <message>
        <location line="-1150"/>
        <source>Speed Limits</source>
        <translation>速度限制</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>&lt;small&gt;Override normal speed limits manually or at scheduled times&lt;/small&gt;</source>
        <translation>&lt;small&gt;手動或者定時覆蓋普通速度限制&lt;/small&gt;</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>&amp;Scheduled times:</source>
        <translation>定時（&amp;S）：</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>&amp;On days:</source>
        <translation>天數（&amp;O）：</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+261"/>
        <source>Every Day</source>
        <translation>每天</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weekdays</source>
        <translation>工作日</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weekends</source>
        <translation>週末</translation>
    </message>
    <message>
        <location line="-152"/>
        <source>Sunday</source>
        <translation>星期天</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Monday</source>
        <translation>星期一</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tuesday</source>
        <translation>星期二</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Wednesday</source>
        <translation>星期三</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Thursday</source>
        <translation>星期四</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Friday</source>
        <translation>星期五</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Saturday</source>
        <translation>星期六</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Port is &lt;b&gt;open&lt;/b&gt;</source>
        <translation>埠是 &lt;b&gt;打開的&lt;/b&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Port is &lt;b&gt;closed&lt;/b&gt;</source>
        <translation>埠是 &lt;b&gt;關閉的&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="+513"/>
        <source>Incoming Peers</source>
        <translation>入站的使用者</translation>
    </message>
    <message>
        <location line="+32"/>
        <location filename="../PrefsDialog.cc" line="+340"/>
        <source>Status unknown</source>
        <translation>狀態未知</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>&amp;Port for incoming connections:</source>
        <translation>入站連接的埠（&amp;P）：</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Use UPnP or NAT-PMP port &amp;forwarding from my router</source>
        <translation>使用路由器的 UPnP 或 NAT-PMP 連接埠轉發（&amp;F）</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Enable &amp;uTP for peer connections</source>
        <translation>為使用者連線啟用 uTP（&amp;U）</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>uTP is a tool for reducing network congestion.</source>
        <translation>uTP 是一個減小網路擁擠的工具。</translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="-219"/>
        <source> minute(s)</source>
        <extracomment>Spin box suffix, &quot;Stop seeding if idle for: [ 5 minutes ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> 分鐘</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+22"/>
        <source> minute(s) ago</source>
        <extracomment>Spin box suffix, &quot;Download is inactive if data sharing stopped: [ 5 minutes ago ]&quot; (includes leading space after the number, if needed)</extracomment>
        <translation>
            <numerusform> 分鐘之前</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-638"/>
        <source>Automatically add .torrent files &amp;from:</source>
        <translation>自動新增 .torrent 檔從（&amp;F）：</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Show the Torrent Options &amp;dialog</source>
        <translation>顯示 Torrent 的選項對話框（&amp;D）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Start added torrents</source>
        <translation>新增 Torrent 後開始（&amp;S）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Mo&amp;ve the .torrent file to the trash</source>
        <translation>將 .torrent 檔案移至資源回收桶（&amp;V）</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Download Queue</source>
        <translation>下載佇列</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Ma&amp;ximum active downloads:</source>
        <translation>最大活動下載數（&amp;X）：</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Incomplete</source>
        <translation>未完成</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Seeding</source>
        <translation>做種</translation>
    </message>
    <message>
        <location line="+548"/>
        <source>Remote</source>
        <translation>遠端</translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="+145"/>
        <source>&lt;i&gt;Blocklist contains %Ln rule(s)&lt;/i&gt;</source>
        <translation>
            <numerusform>&lt;i&gt;黑名單包含 %Ln 條規則&lt;/i&gt;</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-287"/>
        <source>Pick a &amp;random port every time Transmission is started</source>
        <translation>BT 下載工具 每次啟動時隨機選擇埠（&amp;R）</translation>
    </message>
    <message>
        <location line="-252"/>
        <source>Limits</source>
        <translation>限制</translation>
    </message>
    <message>
        <location line="+299"/>
        <source>Maximum peers per &amp;torrent:</source>
        <translation>每個 Torrent 的最大用戶數（&amp;T）：</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Maximum peers &amp;overall:</source>
        <translation>全體最大用戶數（&amp;O）：</translation>
    </message>
    <message numerus="yes">
        <location filename="../PrefsDialog.cc" line="-229"/>
        <source>&lt;b&gt;Update succeeded!&lt;/b&gt;&lt;p&gt;Blocklist now has %Ln rule(s).</source>
        <translation>
            <numerusform>&lt;b&gt;更新成功！ &lt;/b&gt;&lt;p&gt;黑名單現在有 %Ln 條規則。</numerusform>
        </translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;b&gt;Update Blocklist&lt;/b&gt;&lt;p&gt;Getting new blocklist...</source>
        <translation>&lt;b&gt;更新黑名單&lt;/b&gt;&lt;p&gt;正在取得新的黑名單...</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-193"/>
        <source>Blocklist</source>
        <translation>黑名單</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Enable &amp;automatic updates</source>
        <translation>開啟自動更新（&amp;A）</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+19"/>
        <source>Allow encryption</source>
        <translation>允許加密</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Prefer encryption</source>
        <translation>優先加密</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Require encryption</source>
        <translation>要求加密</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-97"/>
        <source>Privacy</source>
        <translation>隱私</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>&amp;to</source>
        <translation>到（&amp;T）</translation>
    </message>
    <message>
        <location line="+763"/>
        <location line="+9"/>
        <source>Desktop</source>
        <translation>桌面</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Show Transmission icon in the &amp;notification area</source>
        <translation>在通知區域顯示 BT 下載工具 圖示（&amp;N）</translation>
    </message>
    <message>
        <location line="-213"/>
        <source>Te&amp;st Port</source>
        <translation>測試埠（&amp;S）</translation>
    </message>
    <message>
        <location line="-101"/>
        <source>Enable &amp;blocklist:</source>
        <translation>啟用黑名單（&amp;B）：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Update</source>
        <translation>更新（&amp;U）</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>&amp;Encryption mode:</source>
        <translation>加密模式（&amp;E）：</translation>
    </message>
    <message>
        <location line="+453"/>
        <source>Remote Control</source>
        <translation>遠端控制</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Allow &amp;remote access</source>
        <translation>允許遠端存取（&amp;R）</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>HTTP &amp;port:</source>
        <translation>HTTP 連接埠（&amp;P）：</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Only allow these IP a&amp;ddresses:</source>
        <translation>只允許這些 IP 位址（&amp;D）：</translation>
    </message>
    <message>
        <location line="-1128"/>
        <source>&amp;Upload:</source>
        <translation>上傳（&amp;U）：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Download:</source>
        <translation>下載（&amp;D）：</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Alternative Speed Limits</source>
        <translation>備用速度限制</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>U&amp;pload:</source>
        <translation>上傳（&amp;P）：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Do&amp;wnload:</source>
        <translation>下載（&amp;W）：</translation>
    </message>
    <message>
        <location line="+836"/>
        <source>Start &amp;minimized in notification area</source>
        <translation>啟動時最小化到通知區域（&amp;M）</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Notification</source>
        <translation>通知</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Show a notification when torrents are a&amp;dded</source>
        <translation>當新增 Torrent 時顯示通知（&amp;D）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Show a notification when torrents &amp;finish</source>
        <translation>當 Torrent 完成時顯示一個通知（&amp;F）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Play a &amp;sound when torrents finish</source>
        <translation>當 Torrent 完成時播放提示音（&amp;S）</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="-83"/>
        <source>Testing TCP Port...</source>
        <translation>測試 TCP 連接埠...</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-229"/>
        <source>Peer Limits</source>
        <translation>使用者限制</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>Use PE&amp;X to find more peers</source>
        <translation>使用 PEX 以尋找更多使用者（&amp;X）</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>PEX is a tool for exchanging peer lists with the peers you&apos;re connected to.</source>
        <translation>PEX 是一個用來與您所連接的使用者交換使用者清單的工具。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Use &amp;DHT to find more peers</source>
        <translation>使用 DHT 以尋找更多使用者（&amp;D）</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>DHT is a tool for finding peers without a tracker.</source>
        <translation>DHT 是一個沒有 Tracker 也能尋找使用者的工具。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Use &amp;Local Peer Discovery to find more peers</source>
        <translation>使用本地使用者發現以尋找更多使用者（&amp;L）</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>LPD is a tool for finding peers on your local network.</source>
        <translation>LPD 是一個用來發現您本地網路使用者的工具。</translation>
    </message>
    <message>
        <location line="-325"/>
        <source>Encryption</source>
        <translation>加密</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+160"/>
        <source>Select &quot;Torrent Done&quot; Script</source>
        <translation>選擇 「Torrent 完成」 腳本</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Select Incomplete Directory</source>
        <translation>選擇未完成目錄</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Select Watch Directory</source>
        <translation>選擇監視目錄</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select Destination</source>
        <translation>選擇目標</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.ui" line="-355"/>
        <source>Adding</source>
        <translation>正在添加</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Download is i&amp;nactive if data sharing stopped:</source>
        <extracomment>Please keep this phrase as short as possible, it&apos;s curently the longest and influences dialog width</extracomment>
        <translation>下載是不活動的如果停止分享數據在（&amp;N）：</translation>
    </message>
    <message>
        <location line="-146"/>
        <source>Downloading</source>
        <translation>下載</translation>
    </message>
    <message>
        <location line="+205"/>
        <source>Append &quot;.&amp;part&quot; to incomplete files&apos; names</source>
        <translation>為未完成檔案附加 「.part」 延伸名（&amp;P）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Keep &amp;incomplete files in:</source>
        <translation>儲存未完成檔案到（&amp;I）：</translation>
    </message>
    <message>
        <location line="-148"/>
        <source>Save to &amp;Location:</source>
        <translation>儲存到位置（&amp;L）：</translation>
    </message>
    <message>
        <location line="+170"/>
        <source>Call scrip&amp;t when torrent is completed:</source>
        <translation>當 Torrent 完成時調用腳本（&amp;T）：</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Stop seeding at &amp;ratio:</source>
        <translation>停止做種當分享率達到（&amp;R）：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Stop seedi&amp;ng if idle for:</source>
        <translation>停止做種當空閒達到（&amp;N）：</translation>
    </message>
    <message>
        <location line="-557"/>
        <source>Transmission Preferences</source>
        <translation>BT下載工具 首選項</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location line="+703"/>
        <source>Network</source>
        <translation>網路</translation>
    </message>
    <message>
        <location filename="../PrefsDialog.cc" line="+91"/>
        <source>Not supported by remote sessions</source>
        <translation>遠程會話不支援</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Application.cc" line="-241"/>
        <source>Invalid option</source>
        <translation>無效選項</translation>
    </message>
</context>
<context>
    <name>RelocateDialog</name>
    <message>
        <location filename="../RelocateDialog.cc" line="+65"/>
        <source>Select Location</source>
        <translation>選擇位置</translation>
    </message>
    <message>
        <location filename="../RelocateDialog.ui" line="+14"/>
        <source>Set Torrent Location</source>
        <translation>設置 Torrent 位置</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Set Location</source>
        <translation>設置位置</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>New &amp;location:</source>
        <translation>新的位置（&amp;L）：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Move from the current folder</source>
        <translation>從目前的資料夾移動（&amp;M）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Local data is &amp;already there</source>
        <translation>本地資料已經在那裡（&amp;A）</translation>
    </message>
</context>
<context>
    <name>Session</name>
    <message>
        <location filename="../Session.cc" line="+559"/>
        <source>Error Renaming Path</source>
        <translation>重新命名路徑錯誤</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;p&gt;&lt;b&gt;Unable to rename &quot;%1&quot; as &quot;%2&quot;: %3.&lt;/b&gt;&lt;/p&gt; &lt;p&gt;Please correct the errors and try again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;無法重新命名%1 為%2： %3.&lt;/b&gt;&lt;/p&gt; &lt;p&gt;請糾正錯誤後重試。 &lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Add Torrent</source>
        <translation>添加 Torrent</translation>
    </message>
</context>
<context>
    <name>SessionDialog</name>
    <message>
        <location filename="../SessionDialog.ui" line="+14"/>
        <source>Change Session</source>
        <translation>更改工作階段</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Source</source>
        <translation>來源</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Start &amp;Local Session</source>
        <translation>開始本地工作階段（&amp;L）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Connect to &amp;Remote Session</source>
        <translation>連線遠端工作階段（&amp;R）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Host:</source>
        <translation>主機（&amp;H）：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Port:</source>
        <translation>連接埠（&amp;P）：</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>&amp;Authentication required</source>
        <translation>要求身份認證（&amp;A）</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Username:</source>
        <translation>使用者名（&amp;U）：</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Pass&amp;word:</source>
        <translation>密碼（&amp;W）：</translation>
    </message>
</context>
<context>
    <name>StatsDialog</name>
    <message>
        <location filename="../StatsDialog.ui" line="+14"/>
        <source>Statistics</source>
        <translation>統計</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Current Session</source>
        <translation>當前工作階段</translation>
    </message>
    <message>
        <location line="+12"/>
        <location line="+96"/>
        <source>Uploaded:</source>
        <translation>已上傳：</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Downloaded:</source>
        <translation>已下載：</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Ratio:</source>
        <translation>分享率：</translation>
    </message>
    <message>
        <location line="-82"/>
        <location line="+96"/>
        <source>Duration:</source>
        <translation>時長：</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Total</source>
        <translation>總計</translation>
    </message>
    <message numerus="yes">
        <location filename="../StatsDialog.cc" line="+71"/>
        <source>Started %Ln time(s)</source>
        <translation>
            <numerusform>已啟動 %Ln 次</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Torrent</name>
    <message>
        <location filename="../Torrent.cc" line="+774"/>
        <source>Verifying local data</source>
        <translation>正在檢查本地數據</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Downloading</source>
        <translation>正在下載</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Seeding</source>
        <translation>正在做種</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Finished</source>
        <translation>已完成</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Paused</source>
        <translation>已暫停</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Queued for verification</source>
        <translation>排隊檢查</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Queued for download</source>
        <translation>排隊下載</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Queued for seeding</source>
        <translation>排隊做種</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Tracker gave a warning: %1</source>
        <translation>Tracker 給出一個警告： %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tracker gave an error: %1</source>
        <translation>Tracker 給出一個錯誤： %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: %1</source>
        <translation>錯誤： %1</translation>
    </message>
</context>
<context>
    <name>TorrentDelegate</name>
    <message>
        <location filename="../TorrentDelegate.cc" line="+171"/>
        <source>Magnetized transfer - retrieving metadata (%1%)</source>
        <extracomment>First part of torrent progress string; %1 is the percentage of torrent metadata downloaded</extracomment>
        <translation>磁力傳輸 - 正在檢索元資料 （%1%）</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 of %2 (%3%)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is how much we&apos;ll have when done, %3 is a percentage of the two</extracomment>
        <translation>%1 / %2 (%3%)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 of %2 (%3%), uploaded %4 (Ratio: %5 Goal: %6)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is the torrent&apos;s total size, %3 is a percentage of the two, %4 is how much we&apos;ve uploaded, %5 is our upload-to-download ratio, %6 is the ratio we want to reach before we stop uploading</extracomment>
        <translation>%1 / %2 （% 3%）， 已上傳 %4 （分享率： %5 目標： %6）</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 of %2 (%3%), uploaded %4 (Ratio: %5)</source>
        <extracomment>First part of torrent progress string; %1 is how much we&apos;ve got, %2 is the torrent&apos;s total size, %3 is a percentage of the two, %4 is how much we&apos;ve uploaded, %5 is our upload-to-download ratio</extracomment>
        <translation>%1 / %2 （% 3%）， 已上傳 %4 （分享率： %5）</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>%1, uploaded %2 (Ratio: %3 Goal: %4)</source>
        <extracomment>First part of torrent progress string; %1 is the torrent&apos;s total size, %2 is how much we&apos;ve uploaded, %3 is our upload-to-download ratio, %4 is the ratio we want to reach before we stop uploading</extracomment>
        <translation>%1， 已上傳 %2 （分享率： %3 目標： %4）</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1, uploaded %2 (Ratio: %3)</source>
        <extracomment>First part of torrent progress string; %1 is the torrent&apos;s total size, %2 is how much we&apos;ve uploaded, %3 is our upload-to-download ratio</extracomment>
        <translation>%1， 已上傳 %2 （分享率： %3）</translation>
    </message>
    <message>
        <location line="+14"/>
        <source> - %1 left</source>
        <extracomment>Second (optional) part of torrent progress string; %1 is duration; notice that leading space (before the dash) is included here</extracomment>
        <translation> - %1 剩餘</translation>
    </message>
    <message>
        <location line="+4"/>
        <source> - Remaining time unknown</source>
        <extracomment>Second (optional) part of torrent progress string; notice that leading space (before the dash) is included here</extracomment>
        <translation> - 剩餘時間未知</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Ratio: %1</source>
        <translation>分享率： %1</translation>
    </message>
    <message numerus="yes">
        <location line="+41"/>
        <source>Downloading from %Ln peer(s)</source>
        <extracomment>First part of phrase &quot;Downloading from ... peer(s) and ... web seed(s)&quot;</extracomment>
        <translation>
            <numerusform>正在下載自 %Ln 個使用者</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+15"/>
        <source>Seeding to %Ln peer(s)</source>
        <translation>
            <numerusform>正在做種給 %Ln 個使用者</numerusform>
        </translation>
    </message>
    <message>
        <location line="+15"/>
        <source> - </source>
        <translation> - </translation>
    </message>
    <message numerus="yes">
        <location line="-38"/>
        <source>Downloading metadata from %Ln peer(s) (%1% done)</source>
        <translation>
            <numerusform>從 %Ln 個使用者下載中繼資料 （% 1% 已完成）</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>Downloading from %1 of %Ln connected peer(s)</source>
        <extracomment>First part of phrase &quot;Downloading from ... of ... connected peer(s) and ... web seed(s)&quot;</extracomment>
        <translation>
            <numerusform>正在下載自 %1 / %Ln 個已連接使用者</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source> and %Ln web seed(s)</source>
        <extracomment>Second (optional) part of phrase &quot;Downloading from ... of ... connected peer(s) and ... web seed(s)&quot;; notice that leading space (before &quot;and&quot;) is included here</extracomment>
        <translation>
            <numerusform> 和 %Ln 個 WEB 種源</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+8"/>
        <source>Seeding to %1 of %Ln connected peer(s)</source>
        <translation>
            <numerusform>正在做種給 %1 / %Ln 個已連接使用者</numerusform>
        </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location line="-70"/>
        <source>Verifying local data (%1% tested)</source>
        <translation>檢查本地資料 （% 1% 已測試 ）</translation>
    </message>
</context>
<context>
    <name>TrackerDelegate</name>
    <message numerus="yes">
        <location filename="../TrackerDelegate.cc" line="+212"/>
        <source>Got a list of%1 %Ln peer(s)%2 %3 ago</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation>
            <numerusform>在 %3前得到一份%1 %Ln個使用者%2 的清單</numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Peer list request %1timed out%2 %3 ago; will retry</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation>在 %3前使用者清單請求%1超時%2;將重試</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Got an error %1&quot;%2&quot;%3 %4 ago</source>
        <extracomment>%1 and %3 are replaced with HTML markup, %2 is error message, %4 is duration</extracomment>
        <translation>在 %4前得到一個錯誤 %1”</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>No updates scheduled</source>
        <translation>無更新計劃任務</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Asking for more peers in %1</source>
        <extracomment>%1 is duration</extracomment>
        <translation>將在 %1 後請求更多使用者</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Queued to ask for more peers</source>
        <translation>排隊請求更多使用者</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Asking for more peers now... &lt;small&gt;%1&lt;/small&gt;</source>
        <extracomment>%1 is duration</extracomment>
        <translation>要求更多使用者... &lt;small&gt;%1&lt;/small&gt;</translation>
    </message>
    <message numerus="yes">
        <location line="+17"/>
        <source>Tracker had%1 %Ln seeder(s)%2</source>
        <extracomment>First part of phrase &quot;Tracker had ... seeder(s) and ... leecher(s) ... ago&quot;; %1 and %2 are replaced with HTML markup</extracomment>
        <translation>
            <numerusform>Tracker 有%1 %Ln個做種使用者%2</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source> and%1 %Ln leecher(s)%2 %3 ago</source>
        <extracomment>Second part of phrase &quot;Tracker had ... seeder(s) and ... leecher(s) ... ago&quot;; %1 and %2 are replaced with HTML markup, %3 is duration; notice that leading space (before &quot;and&quot;) is included here</extracomment>
        <translation>
            <numerusform> 和%1 %Ln個下載使用者%2 在 %3前</numerusform>
        </translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Tracker had %1no information%2 on peer counts %3 ago</source>
        <extracomment>%1 and %2 are replaced with HTML markup, %3 is duration</extracomment>
        <translation>在 %3前 Tracker 對用戶數量%1 沒有資訊%2</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Got a scrape error %1&quot;%2&quot;%3 %4 ago</source>
        <extracomment>%1 and %3 are replaced with HTML markup, %2 is error message, %4 is duration</extracomment>
        <translation>在 %4前得到一個狀態查詢錯誤 %1”</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Asking for peer counts in %1</source>
        <extracomment>%1 is duration</extracomment>
        <translation>將在 %1 後請求用戶數量</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Queued to ask for peer counts</source>
        <translation>排隊請求用戶數量</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Asking for peer counts now... &lt;small&gt;%1&lt;/small&gt;</source>
        <extracomment>%1 is duration</extracomment>
        <translation>要求使用者數量... &lt;small&gt;%1&lt;/small&gt;</translation>
    </message>
</context>
</TS>
