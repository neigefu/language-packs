<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="50"/>
        <source>Remove</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="47"/>
        <source>Blacklist</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠳᠠᠩᠰᠠ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="50"/>
        <source>drag into blacklist</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠳᠠᠩᠰᠠᠨ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="48"/>
        <source>Connect device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ</translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠡᠬᠢᠯᠡᠬᠦ</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ᠋ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="151"/>
        <source>wirless switch is close or no wireless device</source>
        <translation>ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠨᠡᠭᠡᠭᠡᠯᠭᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠠᠭᠠᠭᠰᠠᠨ ᠪᠤᠶᠤ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠤ᠋ᠨ ᠴᠢᠳᠠᠮᠵᠢ ᠲᠠᠢ ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠺᠠᠷᠲ ᠪᠠᠢᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>start to close hotspot</source>
        <translation type="vanished">ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ᠋ ᠡᠬᠢᠯᠡᠵᠤ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="171"/>
        <source>hotpots name or device is invalid</source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠤᠶᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">不能创建密码长度小于八位的热点！</translation>
    </message>
    <message>
        <source>start to open hotspot </source>
        <translation type="vanished">ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ᠋ ᠡᠬᠢᠯᠡᠵᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ </translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="540"/>
        <source>Contains at least 8 characters</source>
        <translation>ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ 8 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="225"/>
        <source>Hotspot</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="61"/>
        <source>Settings</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="64"/>
        <source>Settings desktop message</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="336"/>
        <source>use </source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="337"/>
        <source> share network, will interrupt local wireless connection</source>
        <translation> ᠬᠠᠮᠲᠤ ᠡᠳ᠋ᠯᠡᠬᠦ ᠲᠣᠣᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠂ ᠲᠤᠰ ᠭᠠᠵᠠᠷ᠎ᠤ᠋ᠨ ᠤᠲᠠᠰᠤᠭᠤᠢ ᠵᠠᠯᠭᠠᠯᠲᠠ᠎ᠶ᠋ᠢ ᠲᠠᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="353"/>
        <location filename="../mobilehotspotwidget.cpp" line="681"/>
        <source>hotspot already close</source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ᠋ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="486"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="507"/>
        <source>Wi-Fi Name</source>
        <translation>Wi-Fi ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="528"/>
        <source>Password</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="571"/>
        <source>Frequency band</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠠᠪᠳᠠᠮᠵᠢ ᠵᠢᠨ ᠪᠦᠰᠡ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="595"/>
        <source>Net card</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠧᠲ ᠤ᠋ᠨ ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="711"/>
        <location filename="../mobilehotspotwidget.cpp" line="719"/>
        <source>hotspot already open</source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠪᠡ</translation>
    </message>
</context>
</TS>
