<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="50"/>
        <source>Remove</source>
        <translation>Remove</translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="47"/>
        <source>Blacklist</source>
        <translation>Blacklist</translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="50"/>
        <source>drag into blacklist</source>
        <translation>drag into blacklist</translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="48"/>
        <source>Connect device</source>
        <translation>Connect device</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation>MobileHotspot</translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation>mobilehotspot</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation>mobilehotspot open</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ukui control center</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ukui control center desktop message</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="61"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="64"/>
        <source>Settings desktop message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="151"/>
        <source>wirless switch is close or no wireless device</source>
        <translation>wirless switch is close or no wireless device</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="171"/>
        <source>hotpots name or device is invalid</source>
        <translation>hotpots name or device is invalid</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="225"/>
        <source>Hotspot</source>
        <translation>Hotspot</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="336"/>
        <source>use </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="337"/>
        <source> share network, will interrupt local wireless connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="353"/>
        <location filename="../mobilehotspotwidget.cpp" line="681"/>
        <source>hotspot already close</source>
        <translation>hotspot already close</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="486"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="507"/>
        <source>Wi-Fi Name</source>
        <translation>Wi-Fi Name</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="528"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="540"/>
        <source>Contains at least 8 characters</source>
        <translation>Contains at least 8 characters</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="571"/>
        <source>Frequency band</source>
        <translation>Frequency band</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="595"/>
        <source>Net card</source>
        <translation>Net card</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="711"/>
        <location filename="../mobilehotspotwidget.cpp" line="719"/>
        <source>hotspot already open</source>
        <translation>hotspot already open</translation>
    </message>
</context>
</TS>
