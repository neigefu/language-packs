<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="50"/>
        <source>Remove</source>
        <translation>移出</translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="47"/>
        <source>Blacklist</source>
        <translation>设备黑名单</translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="50"/>
        <source>drag into blacklist</source>
        <translation>添加进黑名单</translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="48"/>
        <source>Connect device</source>
        <translation>连接设备</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation>移动热点</translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation>移动热点</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation>移动热点 开启</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">控制面板</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">控制面板桌面通知</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="151"/>
        <source>wirless switch is close or no wireless device</source>
        <translation>无线开关已关闭或不存在有热点功能的无线网卡</translation>
    </message>
    <message>
        <source>start to close hotspot</source>
        <translation type="vanished">开始关闭热点</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="171"/>
        <source>hotpots name or device is invalid</source>
        <translation>热点名称或设备错误</translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">不能创建密码长度小于八位的热点！</translation>
    </message>
    <message>
        <source>start to open hotspot </source>
        <translation type="vanished">开始创建热点</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="540"/>
        <source>Contains at least 8 characters</source>
        <translation>至少包含8个字符</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="225"/>
        <source>Hotspot</source>
        <translation>移动热点</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="61"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="64"/>
        <source>Settings desktop message</source>
        <translation>设置 桌面通知</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="336"/>
        <source>use </source>
        <translation>使用 </translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="337"/>
        <source> share network, will interrupt local wireless connection</source>
        <translation> 进行热点共享，会中断本机无线网络连接</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="353"/>
        <location filename="../mobilehotspotwidget.cpp" line="681"/>
        <source>hotspot already close</source>
        <translation>热点已关闭</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="486"/>
        <source>Open</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="507"/>
        <source>Wi-Fi Name</source>
        <translation>Wi-Fi 名称</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="528"/>
        <source>Password</source>
        <translation>网络密码</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="571"/>
        <source>Frequency band</source>
        <translation>网络频带</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="595"/>
        <source>Net card</source>
        <translation>共享网卡端口</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="711"/>
        <location filename="../mobilehotspotwidget.cpp" line="719"/>
        <source>hotspot already open</source>
        <translation>热点已开启</translation>
    </message>
</context>
</TS>
