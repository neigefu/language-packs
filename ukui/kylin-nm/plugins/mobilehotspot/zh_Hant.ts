<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="30"/>
        <source>Remove</source>
        <translation>移出</translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="26"/>
        <source>Blacklist</source>
        <translation>設備黑名單</translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="30"/>
        <source>drag into blacklist</source>
        <translation>添加進黑名單</translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="27"/>
        <source>Connect device</source>
        <translation>連接設備</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation>移動熱點</translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation>移動熱點</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation>移動熱點 開啟</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="38"/>
        <source>ukui control center</source>
        <translation>烏奎控制中心</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="41"/>
        <source>ukui control center desktop message</source>
        <translation>UKUI 控制中心桌面消息</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="117"/>
        <source>wirless switch is close or no wireless device</source>
        <translation>無線開關已關閉或不存在有熱點功能的無線網卡</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="121"/>
        <source>start to close hotspot</source>
        <translation>開始關閉熱點</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="130"/>
        <source>hotpots name or device is invalid</source>
        <translation>熱點名稱或設備錯誤</translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">不能创建密码长度小于八位的热点！</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="137"/>
        <source>start to open hotspot </source>
        <translation>開始打開熱點 </translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="257"/>
        <source>Contains at least 8 characters</source>
        <translation>至少包含8個字元</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="172"/>
        <source>Hotspot</source>
        <translation>移動熱點</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="268"/>
        <location filename="../mobilehotspotwidget.cpp" line="582"/>
        <source>hotspot already close</source>
        <translation>熱點已關閉</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="408"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="429"/>
        <source>Wi-Fi Name</source>
        <translation>Wi-Fi 名稱</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="452"/>
        <source>Password</source>
        <translation>網路密碼</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="490"/>
        <source>Frequency band</source>
        <translation>網路頻帶</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="516"/>
        <source>Net card</source>
        <translation>共用網卡埠</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="611"/>
        <location filename="../mobilehotspotwidget.cpp" line="619"/>
        <source>hotspot already open</source>
        <translation>熱點已開啟</translation>
    </message>
</context>
</TS>
