<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="50"/>
        <source>Remove</source>
        <translation>སྤོ་སྐྱོད་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="47"/>
        <source>Blacklist</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་ཐོ་ནག་པོ།</translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="50"/>
        <source>drag into blacklist</source>
        <translation>མིང་ཐོ་ནག་པོའི་ཁ་སྣོན་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="48"/>
        <source>Connect device</source>
        <translation>འབྲེལ་མཐུད་སྒྲིག་ཆས།</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation>སྒུལ་བདེའི་ཧའོ་ཚི་ཀུང་སི།</translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation>སྒུལ་བདེའི་འཕྲུལ་ཆས།</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation>སྒུལ་བདེའི་འཕྲུལ་ཆས་ཀྱི་སྒོ་ཕྱེ་བ།</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ཝུའུ་ཁི་ལན་གྱི་ཚོད་འཛིན་ལྟེ་གནས།</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ukui ཚོད་འཛིན་ལྟེ་གནས་ཀྱི་ཅོག་ངོས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="151"/>
        <source>wirless switch is close or no wireless device</source>
        <translation>སྐུད་མེད་གློག་སྒོ་རྒྱག་པའམ་ཡང་ན་སྐུད་མེད་སྒྲིག་ཆས་མེད་པ།</translation>
    </message>
    <message>
        <source>start to close hotspot</source>
        <translation type="vanished">སྒོ་རྒྱག་འགོ་བརྩམས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="171"/>
        <source>hotpots name or device is invalid</source>
        <translation>ཚ་བ་ཆེ་བའི་མིང་ངམ་སྒྲིག་ཆས་ལ་ནུས་པ་མེད།</translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་གླེང་མང་བའི་གནད་དོན་བརྒྱད་ལས་ཆུང་བ་བྱེད་མི་རུང་།!</translation>
    </message>
    <message>
        <source>start to open hotspot </source>
        <translation type="vanished">ཀུན་གྱིས་དོ་སྣང་བྱེད་ཡུལ་གསར་སྐྲུན་བྱེད་འགོ་ </translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="540"/>
        <source>Contains at least 8 characters</source>
        <translation>མ་མཐར་ཡང་ཡིག་རྟགས་བརྒྱད་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="225"/>
        <source>Hotspot</source>
        <translation>ཚ་བ་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="61"/>
        <source>Settings</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="64"/>
        <source>Settings desktop message</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།  ཅོག་ངོས་ལ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="336"/>
        <source>use </source>
        <translation>བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="337"/>
        <source> share network, will interrupt local wireless connection</source>
        <translation>གླེང་མང་མཉམ་སྤྱོད་བྱས་ནས་འཕྲུལ་ཆས་ཀྱི་སྐུད་མེད་དྲ་བ་སྦྲེལ་མཐུད་བྱེད་ངེས་རེད།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="353"/>
        <location filename="../mobilehotspotwidget.cpp" line="681"/>
        <source>hotspot already close</source>
        <translation>ཚ་བ་ཆེ་བའི་གནད་དོན་ཐག་ཉེ་རུ་སོང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="486"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="507"/>
        <source>Wi-Fi Name</source>
        <translation>Wi-Fiཡི་མིང་།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="528"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="571"/>
        <source>Frequency band</source>
        <translation>ཐེངས་གྲངས་ཀྱི་རོལ་ཆའི་རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="595"/>
        <source>Net card</source>
        <translation>དྲ་རྒྱའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="711"/>
        <location filename="../mobilehotspotwidget.cpp" line="719"/>
        <source>hotspot already open</source>
        <translation>ཚ་བ་ཆེ་བའི་གནད་དོན་དེ་སྒོ་ཕྱེ་ཟིན།</translation>
    </message>
</context>
</TS>
