<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant" sourcelanguage="en">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="47"/>
        <source>Add Others</source>
        <translation>添加其他</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="51"/>
        <source>Add WiredNetork</source>
        <translation>添加有線網路</translation>
    </message>
</context>
<context>
    <name>ItemFrame</name>
    <message>
        <location filename="../itemframe.cpp" line="38"/>
        <source>Add VPN</source>
        <translation>添加 VPN</translation>
    </message>
</context>
<context>
    <name>Vpn</name>
    <message>
        <location filename="../vpn.ui" line="53"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../vpn.ui" line="68"/>
        <source>import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../vpn.cpp" line="151"/>
        <source>Show on Taskbar</source>
        <translation>在任務列顯示圖示</translation>
    </message>
    <message>
        <location filename="../vpn.cpp" line="347"/>
        <location filename="../vpn.cpp" line="454"/>
        <source>connected</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../vpn.cpp" line="349"/>
        <location filename="../vpn.cpp" line="464"/>
        <source>not connected</source>
        <translation>未連接</translation>
    </message>
</context>
<context>
    <name>VpnItem</name>
    <message>
        <location filename="../vpnitem.cpp" line="58"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../vpnitem.cpp" line="111"/>
        <location filename="../vpnitem.cpp" line="124"/>
        <source>Disconnect</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../vpnitem.cpp" line="113"/>
        <location filename="../vpnitem.cpp" line="122"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
</context>
</TS>
