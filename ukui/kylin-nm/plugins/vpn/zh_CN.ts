<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation>添加其他</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation>添加有线网络</translation>
    </message>
</context>
<context>
    <name>ItemFrame</name>
    <message>
        <location filename="../itemframe.cpp" line="38"/>
        <source>Add VPN</source>
        <translation>添加 VPN</translation>
    </message>
</context>
<context>
    <name>Vpn</name>
    <message>
        <location filename="../vpn.ui" line="53"/>
        <location filename="../vpn.cpp" line="68"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../vpn.ui" line="68"/>
        <source>import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../vpn.cpp" line="157"/>
        <location filename="../vpn.cpp" line="284"/>
        <source>Show on Taskbar</source>
        <translation>在任务栏显示图标</translation>
        <extra-contents_path>/Vpn/Show on Taskbar</extra-contents_path>
    </message>
    <message>
        <location filename="../vpn.cpp" line="286"/>
        <source>Add VPN</source>
        <translation>添加 VPN 连接</translation>
        <extra-contents_path>/Vpn/Add VPN</extra-contents_path>
    </message>
    <message>
        <location filename="../vpn.cpp" line="372"/>
        <location filename="../vpn.cpp" line="479"/>
        <source>connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../vpn.cpp" line="374"/>
        <location filename="../vpn.cpp" line="489"/>
        <source>not connected</source>
        <translation>未连接</translation>
    </message>
</context>
<context>
    <name>VpnItem</name>
    <message>
        <location filename="../vpnitem.cpp" line="56"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../vpnitem.cpp" line="108"/>
        <location filename="../vpnitem.cpp" line="121"/>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../vpnitem.cpp" line="110"/>
        <location filename="../vpnitem.cpp" line="119"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
</context>
</TS>
