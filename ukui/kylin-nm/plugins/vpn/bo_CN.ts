<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ItemFrame</name>
    <message>
        <location filename="../itemframe.cpp" line="38"/>
        <source>Add VPN</source>
        <translation>ཁ་སྣོན་རྒྱག་པ།VPN</translation>
    </message>
</context>
<context>
    <name>Vpn</name>
    <message>
        <location filename="../vpn.ui" line="53"/>
        <location filename="../vpn.cpp" line="68"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../vpn.ui" line="68"/>
        <source>import</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../vpn.cpp" line="157"/>
        <location filename="../vpn.cpp" line="284"/>
        <source>Show on Taskbar</source>
        <translation>ལས་འགན་གྱི་ངོས་སུ་དཔེ་རིས་གསལ་པོར་མངོན་པ།</translation>
        <extra-contents_path>/Vpn/Show on Taskbar</extra-contents_path>
    </message>
    <message>
        <location filename="../vpn.cpp" line="286"/>
        <source>Add VPN</source>
        <translation type="unfinished">ཁ་སྣོན་རྒྱག་པ།VPN</translation>
        <extra-contents_path>/Vpn/Add VPN</extra-contents_path>
    </message>
    <message>
        <location filename="../vpn.cpp" line="372"/>
        <location filename="../vpn.cpp" line="479"/>
        <source>connected</source>
        <translation>འབྲེལ་མཐུད་བྱུང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../vpn.cpp" line="374"/>
        <location filename="../vpn.cpp" line="489"/>
        <source>not connected</source>
        <translation>འབྲེལ་མཐུད་མ་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>VpnItem</name>
    <message>
        <location filename="../vpnitem.cpp" line="56"/>
        <source>Delete</source>
        <translation>སུབ་དགོས།</translation>
    </message>
    <message>
        <location filename="../vpnitem.cpp" line="108"/>
        <location filename="../vpnitem.cpp" line="121"/>
        <source>Disconnect</source>
        <translation>བར་མཚམས་ཆད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../vpnitem.cpp" line="110"/>
        <location filename="../vpnitem.cpp" line="119"/>
        <source>Connect</source>
        <translation>འབྲེལ་མཐུད་བཅས་བྱ་དགོས།</translation>
    </message>
</context>
</TS>
