<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation>དྲ་རྒྱ་གཞན་དག་ནང་ཞུགས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.ui" line="14"/>
        <source>WlanConnect</source>
        <translation>ཝུའུ་ལན་འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="35"/>
        <location filename="../wlanconnect.cpp" line="140"/>
        <location filename="../wlanconnect.cpp" line="211"/>
        <source>WLAN</source>
        <translation>སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="94"/>
        <location filename="../wlanconnect.cpp" line="213"/>
        <source>open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
        <extra-contents_path>/wlanconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="147"/>
        <location filename="../wlanconnect.cpp" line="210"/>
        <source>Advanced settings</source>
        <translation>སྔོན་ཐོན་གྱི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ཝུའུ་ཁི་ལན་གྱི་ཚོད་འཛིན་ལྟེ་གནས།</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ངོས་ལེབ་ངོས་ཀྱི་བརྡ་ཐོ་ཚོད་འཛིན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="123"/>
        <source>Settings</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="126"/>
        <source>Settings desktop message</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།  ཅོག་ངོས་ལ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="228"/>
        <source>No wireless network card detected</source>
        <translation>སྐུད་མེད་དྲ་རྒྱའི་བྱང་བུ་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="364"/>
        <location filename="../wlanconnect.cpp" line="1033"/>
        <location filename="../wlanconnect.cpp" line="1095"/>
        <source>connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="970"/>
        <source>card</source>
        <translation>བྱང་བུ།</translation>
    </message>
</context>
</TS>
