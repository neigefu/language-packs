<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation>添加其他</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation>添加有线网络</translation>
    </message>
</context>
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.ui" line="14"/>
        <source>WlanConnect</source>
        <translation>无线局域网</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="35"/>
        <location filename="../wlanconnect.cpp" line="140"/>
        <location filename="../wlanconnect.cpp" line="211"/>
        <source>WLAN</source>
        <translation>无线局域网</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="94"/>
        <location filename="../wlanconnect.cpp" line="213"/>
        <source>open</source>
        <translation>开启</translation>
        <extra-contents_path>/wlanconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="147"/>
        <location filename="../wlanconnect.cpp" line="210"/>
        <source>Advanced settings</source>
        <translation>高级设置</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">控制面板</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">控制面板桌面通知</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="123"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="126"/>
        <source>Settings desktop message</source>
        <translation>设置 桌面通知</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="228"/>
        <source>No wireless network card detected</source>
        <translation>未检测到无线网卡</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="364"/>
        <location filename="../wlanconnect.cpp" line="1033"/>
        <location filename="../wlanconnect.cpp" line="1095"/>
        <source>connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="970"/>
        <source>card</source>
        <translation>网卡</translation>
    </message>
</context>
</TS>
