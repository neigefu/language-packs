<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.ui" line="14"/>
        <location filename="../wlanconnect.cpp" line="97"/>
        <source>WlanConnect</source>
        <translation>無線局域網</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="35"/>
        <location filename="../wlanconnect.cpp" line="168"/>
        <source>WLAN</source>
        <translation>無線局域網</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="94"/>
        <location filename="../wlanconnect.cpp" line="170"/>
        <source>open</source>
        <translation>開啟</translation>
        <extra-contents_path>/wlanconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="147"/>
        <location filename="../wlanconnect.cpp" line="167"/>
        <source>Advanced settings</source>
        <translation>高級設置</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="80"/>
        <source>ukui control center</source>
        <translation>烏奎控制中心</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="83"/>
        <source>ukui control center desktop message</source>
        <translation>UKUI 控制中心桌面消息</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="192"/>
        <source>No wireless network card detected</source>
        <translation>未檢測到無線網卡</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="330"/>
        <location filename="../wlanconnect.cpp" line="933"/>
        <location filename="../wlanconnect.cpp" line="995"/>
        <source>connected</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="879"/>
        <source>card</source>
        <translation>網卡</translation>
    </message>
</context>
</TS>
