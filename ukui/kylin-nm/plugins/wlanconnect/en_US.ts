<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation>Add Others</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation>Add WiredNetork</translation>
    </message>
</context>
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.ui" line="14"/>
        <source>WlanConnect</source>
        <translation>WlanConnect</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="35"/>
        <location filename="../wlanconnect.cpp" line="140"/>
        <location filename="../wlanconnect.cpp" line="211"/>
        <source>WLAN</source>
        <translation>WLAN</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="94"/>
        <location filename="../wlanconnect.cpp" line="213"/>
        <source>open</source>
        <translation>open</translation>
        <extra-contents_path>/wlanconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="147"/>
        <location filename="../wlanconnect.cpp" line="210"/>
        <source>Advanced settings</source>
        <translation>Advanced settings</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ukui control center</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ukui control center desktop message</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="123"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="126"/>
        <source>Settings desktop message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="228"/>
        <source>No wireless network card detected</source>
        <translation>No wireless network card detected</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="364"/>
        <location filename="../wlanconnect.cpp" line="1033"/>
        <location filename="../wlanconnect.cpp" line="1095"/>
        <source>connected</source>
        <translation>connected</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="970"/>
        <source>card</source>
        <translation>card</translation>
    </message>
</context>
</TS>
