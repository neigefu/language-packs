<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AptProxyDialog</name>
    <message>
        <location filename="../aptproxydialog.cpp" line="24"/>
        <source>Set APT Proxy</source>
        <translation>APT ངོ་ཚབ་ བཙུགས་པ།</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="41"/>
        <source>Server Address</source>
        <translation>ཞབས་ཞུའི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="59"/>
        <source>Port</source>
        <translation>གྲུ་ཁ།</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="84"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Proxy</name>
    <message>
        <location filename="../proxy.cpp" line="63"/>
        <source>Proxy</source>
        <translation>ཚབ་བྱེད་མི་སྣ།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="200"/>
        <source>Start using</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="213"/>
        <source>Proxy mode</source>
        <translation>ཚབ་བྱེད་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="217"/>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="220"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="375"/>
        <source>Application Proxy</source>
        <translation>ཉེར་སྤྱོད་ངོ་ཚབ།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="450"/>
        <source>System Proxy</source>
        <translation>མ་ལག་གི་ཚབ་བྱེད་</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="452"/>
        <source>Auto url</source>
        <translation>རླངས་འཁོར་གྱི་དྲ་ཚིགས།</translation>
        <extra-contents_path>/Proxy/Auto url</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="454"/>
        <source>Http Proxy</source>
        <translation>HTTP ཚབ་བྱེད་མི་སྣ།</translation>
        <extra-contents_path>/Proxy/Http Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="456"/>
        <source>Https Proxy</source>
        <translation>HTTPS ཚབ་བྱེད་མི་སྣ།</translation>
        <extra-contents_path>/Proxy/Https Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="458"/>
        <source>Ftp Proxy</source>
        <translation>FTP ཚབ་བྱེད་མི་སྣ།</translation>
        <extra-contents_path>/Proxy/Ftp Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="460"/>
        <source>Socks Proxy</source>
        <translation>SOCKS ཚབ་བྱེད་མི་སྣ།</translation>
        <extra-contents_path>/Proxy/Socks Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="461"/>
        <location filename="../proxy.cpp" line="462"/>
        <location filename="../proxy.cpp" line="463"/>
        <location filename="../proxy.cpp" line="464"/>
        <location filename="../proxy.cpp" line="1068"/>
        <source>Port</source>
        <translation>གྲུ་ཁ།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="465"/>
        <source>List of ignored hosts. more than one entry, please separate with english semicolon(;)</source>
        <translation>སྣང་མེད་དུ་བཞག་པའི་བདག་པོའི་མིང་ཐོ། འཇུག་སྒོ་གཅིག་ལས་བརྒལ་ན་དབྱིན་ཡིག་གི་ཕྱེད་ཀ་དང་ཁ་གྱེས་རོགས། (;)</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="468"/>
        <source>App Proxy</source>
        <translation>ཉེར་སྤྱོད་ངོ་ཚབ།</translation>
        <extra-contents_path>/Proxy/App Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="471"/>
        <source>APT Proxy</source>
        <translation>APT ཚབ་བྱེད་མི་སྣ།</translation>
        <extra-contents_path>/Proxy/APT Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="472"/>
        <location filename="../proxy.cpp" line="1005"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="473"/>
        <source>Server Address : </source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ཀྱི་གནས་ཡུལ </translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="474"/>
        <source>Port : </source>
        <translation>གྲུ་ཁ། </translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="475"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="573"/>
        <source>The APT proxy has been turned off and needs to be restarted to take effect</source>
        <translation>ངོ་ཚབ་ཀྱི་སྒོ་བརྒྱབ་ཟིན་པས་ཡང་བསྐྱར་ནུས་པ་ཐོན་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="574"/>
        <location filename="../proxy.cpp" line="814"/>
        <source>Reboot Later</source>
        <translation>རྗེས་སུ་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="575"/>
        <location filename="../proxy.cpp" line="815"/>
        <source>Reboot Now</source>
        <translation>ད་ལྟ་བསྐྱར་དུ་ལས་ཀ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="813"/>
        <source>The system needs to be restarted to set the APT proxy, whether to reboot</source>
        <translation>མ་ལག་འདི་བསྐྱར་དུ་འགོ་ཚུགས་ནས་APT ཡི་ཚབ་བྱེད་འཕྲུལ་ཆས་གཏན་འཁེལ་བྱེད་དགོས་པ་དང་། བསྐྱར་དུ་འགོ་འཛུགས་དགོས་མིན་</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1017"/>
        <source>Proxy type</source>
        <translation>ངོ་ཚབ་ཀྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <source>HTTP</source>
        <translation type="vanished">HTTP</translation>
    </message>
    <message>
        <source>socks4</source>
        <translation type="vanished">རྐང་འབོབ་4</translation>
    </message>
    <message>
        <source>socks5</source>
        <translation type="vanished">རྐང་འབོབ་5</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1034"/>
        <source>IP address</source>
        <translation>IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1040"/>
        <location filename="../proxy.cpp" line="1071"/>
        <source>Required</source>
        <translation>བླང་བྱ་བཏོན་པ།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1061"/>
        <source>Invalid IP Address</source>
        <translation>གོ་མི་ཆོད་པའི་IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1082"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1085"/>
        <location filename="../proxy.cpp" line="1099"/>
        <source>Optional</source>
        <translation>བསལ་འདེམས་ཀྱི་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1095"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1118"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1119"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1181"/>
        <source>The following applications are allowed to use this configuration:</source>
        <translation>གཤམ་གྱི་ཉེར་སྤྱོད་གོ་རིམ་ཁྲོད་དུ་བཀོད་སྒྲིག་འདི་བཀོལ་ཆོག་པ་སྟེ།</translation>
    </message>
</context>
</TS>
