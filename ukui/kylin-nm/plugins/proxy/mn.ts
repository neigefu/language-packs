<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AptProxyDialog</name>
    <message>
        <location filename="../aptproxydialog.cpp" line="24"/>
        <source>Set APT Proxy</source>
        <translation>APT ᠤᠷᠤᠯᠠᠭᠴᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="41"/>
        <source>Server Address</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="59"/>
        <source>Port</source>
        <translation>ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="84"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Proxy</name>
    <message>
        <location filename="../proxy.cpp" line="63"/>
        <source>Proxy</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="200"/>
        <source>Start using</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="213"/>
        <source>Proxy mode</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="217"/>
        <source>Auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="220"/>
        <source>Manual</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="375"/>
        <source>Application Proxy</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="450"/>
        <source>System Proxy</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="452"/>
        <source>Auto url</source>
        <translation>URL ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
        <extra-contents_path>/Proxy/Auto url</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="454"/>
        <source>Http Proxy</source>
        <translation>HTTP ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
        <extra-contents_path>/Proxy/Http Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="456"/>
        <source>Https Proxy</source>
        <translation>HTTPS ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
        <extra-contents_path>/Proxy/Https Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="458"/>
        <source>Ftp Proxy</source>
        <translation>FTP ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
        <extra-contents_path>/Proxy/Ftp Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="460"/>
        <source>Socks Proxy</source>
        <translation>SOCKS ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
        <extra-contents_path>/Proxy/Socks Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="461"/>
        <location filename="../proxy.cpp" line="462"/>
        <location filename="../proxy.cpp" line="463"/>
        <location filename="../proxy.cpp" line="464"/>
        <location filename="../proxy.cpp" line="1068"/>
        <source>Port</source>
        <translation>ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="465"/>
        <source>List of ignored hosts. more than one entry, please separate with english semicolon(;)</source>
        <translation>ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠭᠰᠠᠨ ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ᠂ ᠠᠩᠭ᠌ᠯᠢ ᠬᠡᠯᠡᠨ ᠤ᠋ ᠵᠠᠭᠠᠭᠯᠠᠬᠤ ᠳᠡᠮᠳᠡᠭ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠷᠡᠢ (;)</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="468"/>
        <source>App Proxy</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
        <extra-contents_path>/Proxy/App Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="471"/>
        <source>APT Proxy</source>
        <translation>APT ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
        <extra-contents_path>/Proxy/APT Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="472"/>
        <location filename="../proxy.cpp" line="1005"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="473"/>
        <source>Server Address : </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ: </translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="474"/>
        <source>Port : </source>
        <translation>ᠫᠤᠷᠲ : </translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="475"/>
        <source>Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="573"/>
        <source>The APT proxy has been turned off and needs to be restarted to take effect</source>
        <translation>APT ᠤᠷᠤᠯᠠᠭᠴᠢ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ᠂ ᠳᠠᠬᠢᠵᠤ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="574"/>
        <location filename="../proxy.cpp" line="814"/>
        <source>Reboot Later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="575"/>
        <location filename="../proxy.cpp" line="815"/>
        <source>Reboot Now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="813"/>
        <source>The system needs to be restarted to set the APT proxy, whether to reboot</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ APT ᠤᠷᠤᠯᠠᠭᠴᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠤᠤ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1017"/>
        <source>Proxy type</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1034"/>
        <source>IP address</source>
        <translation>IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1040"/>
        <location filename="../proxy.cpp" line="1071"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1061"/>
        <source>Invalid IP Address</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1082"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1085"/>
        <location filename="../proxy.cpp" line="1099"/>
        <source>Optional</source>
        <translation>ᠰᠤᠩᠭᠤᠵᠤ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1095"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1118"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1119"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1181"/>
        <source>The following applications are allowed to use this configuration:</source>
        <translation>ᠳᠤᠤᠷᠠᠬᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠲᠤᠰ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ:</translation>
    </message>
</context>
</TS>
