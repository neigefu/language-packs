<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AptProxyDialog</name>
    <message>
        <location filename="../aptproxydialog.cpp" line="24"/>
        <source>Set APT Proxy</source>
        <translation>設置APT代理</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="41"/>
        <source>Server Address</source>
        <translation>伺服器位址</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="59"/>
        <source>Port</source>
        <translation>埠</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="80"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../aptproxydialog.cpp" line="84"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>Proxy</name>
    <message>
        <location filename="../proxy.cpp" line="63"/>
        <source>Proxy</source>
        <translation>代理</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="200"/>
        <source>Start using</source>
        <translation>啟用</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="213"/>
        <source>Proxy mode</source>
        <translation>代理類型</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="217"/>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="220"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="375"/>
        <source>Application Proxy</source>
        <translation>應用代理</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="450"/>
        <source>System Proxy</source>
        <translation>系統代理</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="452"/>
        <source>Auto url</source>
        <translation>配置 URL</translation>
        <extra-contents_path>/Proxy/Auto url</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="454"/>
        <source>Http Proxy</source>
        <translation>HTTP 代理</translation>
        <extra-contents_path>/Proxy/Http Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="456"/>
        <source>Https Proxy</source>
        <translation>HTTPS 代理</translation>
        <extra-contents_path>/Proxy/Https Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="458"/>
        <source>Ftp Proxy</source>
        <translation>FTP 代理</translation>
        <extra-contents_path>/Proxy/Ftp Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="460"/>
        <source>Socks Proxy</source>
        <translation>SOCKS 代理</translation>
        <extra-contents_path>/Proxy/Socks Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="461"/>
        <location filename="../proxy.cpp" line="462"/>
        <location filename="../proxy.cpp" line="463"/>
        <location filename="../proxy.cpp" line="464"/>
        <location filename="../proxy.cpp" line="1068"/>
        <source>Port</source>
        <translation>埠</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="465"/>
        <source>List of ignored hosts. more than one entry, please separate with english semicolon(;)</source>
        <translation>忽略的主機清單，請使用英文分號（; ）</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="468"/>
        <source>App Proxy</source>
        <translation>應用代理</translation>
        <extra-contents_path>/Proxy/App Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="471"/>
        <source>APT Proxy</source>
        <translation>APT 代理</translation>
        <extra-contents_path>/Proxy/APT Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../proxy.cpp" line="472"/>
        <location filename="../proxy.cpp" line="1005"/>
        <source>Open</source>
        <translation>開啟</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="473"/>
        <source>Server Address : </source>
        <translation>伺服器位址： </translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="474"/>
        <source>Port : </source>
        <translation>埠： </translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="475"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="573"/>
        <source>The apt proxy  has been turned off and needs to be restarted to take effect</source>
        <translation>apt 代理已關閉，需要重新啟動才能生效</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="574"/>
        <location filename="../proxy.cpp" line="814"/>
        <source>Reboot Later</source>
        <translation>稍後重啟</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="575"/>
        <location filename="../proxy.cpp" line="815"/>
        <source>Reboot Now</source>
        <translation>立即重啟</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="813"/>
        <source>The system needs to be restarted to set the APT proxy, whether to reboot</source>
        <translation>設置APT代理需要重啟系統後生效，是否重啟系統</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1017"/>
        <source>Proxy type</source>
        <translation>代理類型</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1034"/>
        <source>IP address</source>
        <translation>IP 位址</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1040"/>
        <location filename="../proxy.cpp" line="1071"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1061"/>
        <source>Invalid IP Address</source>
        <translation>無效的IP位址</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1082"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1085"/>
        <location filename="../proxy.cpp" line="1099"/>
        <source>Optional</source>
        <translation>選填</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1095"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1118"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1119"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../proxy.cpp" line="1181"/>
        <source>The following applications are allowed to use this configuration:</source>
        <translation>允許以下應用使用該設定：</translation>
    </message>
</context>
</TS>
