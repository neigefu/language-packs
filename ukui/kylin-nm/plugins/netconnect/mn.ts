<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠵᠠᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation>ᠤᠲᠠᠰᠤᠲᠤ ᠨᠧᠲ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.ui" line="50"/>
        <source>Wired Network</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../netconnect.ui" line="112"/>
        <location filename="../netconnect.cpp" line="155"/>
        <source>open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
        <extra-contents_path>/netconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.ui" line="198"/>
        <location filename="../netconnect.cpp" line="152"/>
        <source>Advanced settings</source>
        <translation>ᠦᠨᠳᠦᠷ ᠵᠡᠷᠬᠡ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ᠋ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <source>WiredConnect</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="64"/>
        <source>Settings</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="67"/>
        <source>Settings desktop message</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="81"/>
        <location filename="../netconnect.cpp" line="153"/>
        <source>LAN</source>
        <translation>ᠤᠲᠠᠰᠤᠲᠤ ᠲᠣᠣᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="169"/>
        <source>No ethernet device avaliable</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="437"/>
        <location filename="../netconnect.cpp" line="861"/>
        <source>connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="504"/>
        <source>card</source>
        <translation>ᠨᠧᠲ ᠤ᠋ᠨ ᠺᠠᠷᠲ</translation>
    </message>
</context>
</TS>
