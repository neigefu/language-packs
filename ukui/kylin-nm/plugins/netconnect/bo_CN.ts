<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation>སྐུད་ཡོད་བརྙན་འཕྲིན་ཁ་སྣོན་བྱས་ཡོད།</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.ui" line="50"/>
        <source>Wired Network</source>
        <translation>སྐུད་ཡོད་བརྙན་འཕྲིན་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../netconnect.ui" line="112"/>
        <location filename="../netconnect.cpp" line="155"/>
        <source>open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
        <extra-contents_path>/netconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.ui" line="198"/>
        <location filename="../netconnect.cpp" line="152"/>
        <source>Advanced settings</source>
        <translation>སྔོན་ཐོན་གྱི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ཝུའུ་ཁི་ལན་གྱི་ཚོད་འཛིན་ལྟེ་གནས།</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ukui ཚོད་འཛིན་ལྟེ་གནས་ཀྱི་ཅོག་ངོས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>WiredConnect</source>
        <translation type="vanished">སྐུད་ཡོད་སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="64"/>
        <source>Settings</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="67"/>
        <source>Settings desktop message</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།  ཅོག་ངོས་ལ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="81"/>
        <location filename="../netconnect.cpp" line="153"/>
        <source>LAN</source>
        <translation>སྐུད་ཡོད་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="169"/>
        <source>No ethernet device avaliable</source>
        <translation>ཨེ་ཙི་དྲ་རྒྱའི་སྒྲིག་ཆས་ལ་བཙན་འཛུལ་བྱས་མི་ཆོག།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="437"/>
        <location filename="../netconnect.cpp" line="861"/>
        <source>connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="504"/>
        <source>card</source>
        <translation>བྱང་བུ།</translation>
    </message>
</context>
</TS>
