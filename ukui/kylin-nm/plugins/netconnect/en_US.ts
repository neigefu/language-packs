<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation>Add Others</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation>Add WiredNetork</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.ui" line="50"/>
        <source>Wired Network</source>
        <translation>Wired Network</translation>
    </message>
    <message>
        <location filename="../netconnect.ui" line="112"/>
        <location filename="../netconnect.cpp" line="155"/>
        <source>open</source>
        <translation>open</translation>
        <extra-contents_path>/netconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.ui" line="198"/>
        <location filename="../netconnect.cpp" line="152"/>
        <source>Advanced settings</source>
        <translation>Advanced settings</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">ukui control center</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">ukui control center desktop message</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="64"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="67"/>
        <source>Settings desktop message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="81"/>
        <location filename="../netconnect.cpp" line="153"/>
        <source>LAN</source>
        <translation>LAN</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="169"/>
        <source>No ethernet device avaliable</source>
        <translation>No ethernet device avaliable</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="437"/>
        <location filename="../netconnect.cpp" line="861"/>
        <source>connected</source>
        <translation>connected</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="504"/>
        <source>card</source>
        <translation>card</translation>
    </message>
</context>
</TS>
