<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="44"/>
        <source>Add Others</source>
        <translation>添加其他</translation>
    </message>
    <message>
        <location filename="../../component/AddBtn/addnetbtn.cpp" line="48"/>
        <source>Add WiredNetork</source>
        <translation>添加有线网络</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.ui" line="50"/>
        <source>Wired Network</source>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../netconnect.ui" line="112"/>
        <location filename="../netconnect.cpp" line="155"/>
        <source>open</source>
        <translation>开启</translation>
        <extra-contents_path>/netconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.ui" line="198"/>
        <location filename="../netconnect.cpp" line="152"/>
        <source>Advanced settings</source>
        <translation>高级设置</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">控制面板</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">控制面板桌面通知</translation>
    </message>
    <message>
        <source>WiredConnect</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="64"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="67"/>
        <source>Settings desktop message</source>
        <translation>设置 桌面通知</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="81"/>
        <location filename="../netconnect.cpp" line="153"/>
        <source>LAN</source>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="169"/>
        <source>No ethernet device avaliable</source>
        <translation>未检测到有线设备</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="437"/>
        <location filename="../netconnect.cpp" line="861"/>
        <source>connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="504"/>
        <source>card</source>
        <translation>网卡</translation>
    </message>
</context>
</TS>
