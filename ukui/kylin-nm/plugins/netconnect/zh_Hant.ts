<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../addnetbtn.cpp" line="22"/>
        <source>Add WiredNetork</source>
        <translation>添加有線網路</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.ui" line="50"/>
        <location filename="../netconnect.cpp" line="152"/>
        <source>Wired Network</source>
        <translation>有線網路</translation>
    </message>
    <message>
        <location filename="../netconnect.ui" line="112"/>
        <location filename="../netconnect.cpp" line="154"/>
        <source>open</source>
        <translation>開啟</translation>
        <extra-contents_path>/netconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.ui" line="198"/>
        <location filename="../netconnect.cpp" line="151"/>
        <source>Advanced settings</source>
        <translation>高級設置</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="63"/>
        <source>ukui control center</source>
        <translation>烏奎控制中心</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="66"/>
        <source>ukui control center desktop message</source>
        <translation>UKUI 控制中心桌面消息</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="80"/>
        <source>WiredConnect</source>
        <translation>連線連接</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="177"/>
        <source>No ethernet device avaliable</source>
        <translation>未檢測到有線設備</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="426"/>
        <location filename="../netconnect.cpp" line="833"/>
        <source>connected</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="490"/>
        <source>card</source>
        <translation>網卡</translation>
    </message>
</context>
</TS>
