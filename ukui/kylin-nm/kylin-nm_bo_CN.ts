<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN" sourcelanguage="en">
<context>
    <name>ConfForm</name>
    <message>
        <source>Manual</source>
        <translation type="obsolete">手动</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">网络提示消息</translation>
    </message>
    <message>
        <source>Will check the IP address conflict</source>
        <translation type="vanished">正在检测ip地址冲突</translation>
    </message>
    <message>
        <source>IPv4 address conflict, Please change IP</source>
        <translation type="vanished">ip地址冲突，请更改ip</translation>
    </message>
    <message>
        <source>IPv6 address conflict, Please change IP</source>
        <translation type="obsolete">ip地址冲突，请更改ip {6 ?}</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="62"/>
        <source>Network profile type</source>
        <translation>དྲ་རྒྱའི་བཀོད་སྒྲིག་ཡིག་ཆའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="65"/>
        <source>Public(recommended)  Devices on the network cannot discover this computer. Generally, it is suitable for networks in public places, such as airports or coffee shops, etc.</source>
        <translation>སྤྱི་སྤྱོད།（འོས་སྦྱོར་བྱས་པ།）དྲ་རྒྱའི་སྒྲིག་ཆས་ཀྱིས་གློག་ཀླད་འདི་མཐོང་མི་ཐུབ། སྤྱིར་བཏང་གི་གནས་ཚུལ་འོག་ཏུ་མི་མང་འདུ་སའི་ནང་གི་དྲ་བ་ལ་འཚམ་པ་སྟེ།དཔེར་ན་གནམ་གྲུ་ཐང་དང་འཚིག་ཇའི་ཁང་སོགས་ལྟ་བུ།.</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="69"/>
        <source>Private  Devices on the network can discover this computer. Generally applicable to a network at home or work where you know and trust the individuals and devices on the network.</source>
        <translation>ཆེད་སྤྱོད།    དྲ་རྒྱའི་སྒྲིག་ཆས་ཀྱིས་གློག་ཀླད་འདི་མཐོང་ཐུབ། སྤྱིར་བཏང་གི་གནས་ཚུལ་འོག་ཁྱིམ་ཚང་ངམ་ལས་དོན་ཚན་པའི་དྲ་བ་དང་འཚམ་པས།ཁྱེད་ཀྱིས་དྲ་ཐོག་གི་མི་སྒེར་དང་སྒྲིག་ཆས་ལ་ངོས་འཛིན་དང་ཡིད་ཆེས་བྱེད་དགོས།.</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="73"/>
        <source>Config firewall and security settings</source>
        <translation>མེ་འགོག་གྱང་རྩིག་དང་བདེ་འཇགས་བཀོད་སྒྲིག་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>CopyButton</name>
    <message>
        <source>Copied successfully</source>
        <translation type="vanished">复制成功</translation>
    </message>
    <message>
        <source>Copied successfully!</source>
        <translation type="vanished">复制成功！</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">复制全部</translation>
    </message>
</context>
<context>
    <name>CreatNetPage</name>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="95"/>
        <source>Connection Name</source>
        <translation>འབྲེལ་མཐུད་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="96"/>
        <source>IPv4Config</source>
        <translation>IPv4ཁུང་ཙི།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="97"/>
        <source>Address</source>
        <translation>སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="98"/>
        <source>Netmask</source>
        <translation>དྲ་རྒྱའི་མ་ལག</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="99"/>
        <source>Default Gateway</source>
        <translation>ཁ་ཆད་བཞག་པའི་སྒོ་ཆེན།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="330"/>
        <source>Address conflict</source>
        <translation>ཤག་གནས་གདོང་གཏུག་</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">སྔོན་གྲབས་DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">ཚབ་བྱེད་རང་བཞིན་གྱི་DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation>རང་འགུལ་(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="118"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="192"/>
        <source>Invalid address</source>
        <translation>རྩིས་འགྲོ་མེད་པའི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="210"/>
        <source>Invalid subnet mask</source>
        <translation>རྩིས་འགྲོ་མེད་པའི་དྲ་བ་འགེབས་སྲུང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="230"/>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="231"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="302"/>
        <source>Auto Connection</source>
        <translation>རང་འགུལ་གྱིས་འབྲེལ་མཐུད་བྱེད</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="256"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="355"/>
        <source>SSID:</source>
        <translation>SSID:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="150"/>
        <source>Copied successfully!</source>
        <translation>འདྲ་བཟོ་བྱས་ནས་གྲུབ་འབྲས་ཐོབ་!</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="227"/>
        <source>Copy all</source>
        <translation>ཚང་མ་འདྲ་བཤུས་བྱེད་</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="247"/>
        <source>Please input SSID:</source>
        <translation>SSID:ནང་འཇུག་གནང་རོགས།:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="260"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="356"/>
        <source>Protocol:</source>
        <translation>གྲོས་ཆོད་ནང་དུ།:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="264"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="357"/>
        <source>Security Type:</source>
        <translation>བདེ་འཇགས་ཀྱི་རིགས་དབྱིབས་ནི།:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="268"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="358"/>
        <source>Hz:</source>
        <translation>དྲ་རྒྱའི་འཕྲིན་ལམ།:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="272"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="359"/>
        <source>Chan:</source>
        <translation>དྲ་བའི་བགྲོད་ལམ།:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="276"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="360"/>
        <source>BandWidth:</source>
        <translation>ཞེང་ཆེ་བ།:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="292"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="363"/>
        <source>IPv6:</source>
        <translation>IPv6:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="280"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="361"/>
        <source>IPv4:</source>
        <translation>IPv4:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="286"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="362"/>
        <source>IPv4 DNS:</source>
        <translation>IPv4 DNS:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="296"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="364"/>
        <source>Mac:</source>
        <translation>ཨའོ་མོན་ནི།:</translation>
    </message>
</context>
<context>
    <name>DlgHideWifi</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapFast</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPeap</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPwd</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTTLS</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTls</name>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="obsolete">用户证书</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="obsolete">用户私钥</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="obsolete">用户密钥密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWep</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWpa</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DnsSettingWidget</name>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="34"/>
        <source>DNS Server Advanced Settings</source>
        <translation>DNSགྱི་ཞབས་ཞུའི་ཡོ་བྱད་མཐོ་རིམ་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="37"/>
        <source>Tactic</source>
        <translation>ཐབས་ཇུས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="40"/>
        <source>Timeout</source>
        <translation>དུས་ཚོད་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="43"/>
        <source>Retry Count</source>
        <translation>བསྐྱར་དུ་ཚོད་ལྟ་བྱེད་ཐེངས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="46"/>
        <source>order</source>
        <translation>གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="47"/>
        <source>rotate</source>
        <translation>སྐབས་བསྟུན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="48"/>
        <source>concurrency</source>
        <translation>འགྲེམས་སྤེལ་ཡང་བྱ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="53"/>
        <source> s</source>
        <translation> སྐར་ཆ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="59"/>
        <source> times</source>
        <translation> གཉིས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="70"/>
        <source>Close</source>
        <translation type="unfinished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="73"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="76"/>
        <source>Confirm</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanDialog</name>
    <message>
        <source>Connect Enterprise WLAN</source>
        <translation type="vanished">ཁེ་ལས་WLANས སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="116"/>
        <source>Wi-Fi network requires authentication</source>
        <translation>Wi-Fiཡི་དྲ་རྒྱའི་བླང་བྱར་སྤྲོད་བྱ་རྒྱུའི་བླང་བྱ་བཏོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="121"/>
        <source>Access to Wi-Fi network &quot;</source>
        <translation>Wii-Fiབར་གྱི་དྲ་རྒྱར་འཚམས་འདྲི་གནང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="123"/>
        <source>&quot; requires a password or encryption key.</source>
        <translation>གསང་གྲངས་དང་གསང་བའི་ལྡེ་མིག་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="154"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="155"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
</context>
<context>
    <name>FirewallDialog</name>
    <message>
        <source>Allow your computer to be discovered by other computers and devices on this network？</source>
        <translation type="vanished">ཁྱོད་ཀྱི་གློག་ཀླད་དེ་དྲ་རྒྱའི་སྟེང་གི་གློག་ཀླད་དང་སྒྲིག་ཆས་གཞན་དག་གིས་རྙེད་ཐུབ་བམ།?</translation>
    </message>
    <message>
        <source>It is recommended that you enable this feature on your home and work networks rather than public networks.</source>
        <translation type="vanished">ཁྱེད་ཀྱིས་ཁྱིམ་ཚང་དང་བྱ་བའི་དྲ་རྒྱའི་སྟེང་ནས་སྤྱི་སྤྱོད་མིན་པའི་དྲ་རྒྱའི་སྟེང་ནས་བྱེད་ནུས་འདི་སྤྱོད་རྒྱུའི་གྲོས་འགོ་བཏོན།.</translation>
    </message>
    <message>
        <source>Yse</source>
        <translation type="vanished">དེ་ནི་རེད།</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">དེ་ལྟར་མ་བྱས་</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="85"/>
        <source>Allow other devices on this network to discover this computer?</source>
        <translation>དྲ་རྒྱའི་སྟེང་གི་སྒྲིག་ཆས་གཞན་པས་གློག་ཀླད་འདི་རྙེད་དུ་འཇུག་གམ།?</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="87"/>
        <source>It is not recommended to enable this feature on public networks</source>
        <translation>བསམ་འཆར་མེད་།སྤྱི་པའི་དྲ་རྒྱའི་སྟེང་ནས་ནུས་པ་འདི་མགོ་བརྩམས་།</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="89"/>
        <source>Not allowed (recommended)</source>
        <translation>མི་ཆོག་པ་(འོས་སྦྱོར།)</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="90"/>
        <source>Allowed</source>
        <translation>ཆོག་པ་</translation>
    </message>
</context>
<context>
    <name>Ipv4Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="66"/>
        <source>IPv4Config</source>
        <translation>IPv4ཁུང་ཙི།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="67"/>
        <source>Address</source>
        <translation>སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="68"/>
        <source>Netmask</source>
        <translation>དྲ་རྒྱའི་མ་ལག</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="69"/>
        <source>Default Gateway</source>
        <translation>ཁ་ཆད་བཞག་པའི་སྒོ་ཆེན།</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">སྔོན་གྲབས་DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">ཚབ་བྱེད་རང་བཞིན་གྱི་DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation>རང་འགུལ་(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="118"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="274"/>
        <source>Invalid address</source>
        <translation>རྩིས་འགྲོ་མེད་པའི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="283"/>
        <source>Invalid subnet mask</source>
        <translation>རྩིས་འགྲོ་མེད་པའི་དྲ་བ་འགེབས་སྲུང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="309"/>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="310"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="380"/>
        <source>Address conflict</source>
        <translation>ཤག་གནས་གདོང་གཏུག་</translation>
    </message>
</context>
<context>
    <name>Ipv6Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="134"/>
        <source>IPv6Config</source>
        <translation>IPv6ཁུང་ཙི།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="135"/>
        <source>Address</source>
        <translation>སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="136"/>
        <source>Subnet prefix Length</source>
        <translation>ཡན་ལག་དྲ་རྒྱའི་སྔོན་སྒྲིག་གི་རིང་ཚད།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="137"/>
        <source>Default Gateway</source>
        <translation>ཁ་ཆད་བཞག་པའི་སྒོ་ཆེན།</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">སྔོན་གྲབས་DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">ཚབ་བྱེད་རང་བཞིན་གྱི་DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="180"/>
        <source>Auto(DHCP)</source>
        <translation>རང་འགུལ་(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="181"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="232"/>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="233"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="252"/>
        <source>Invalid address</source>
        <translation>རྩིས་འགྲོ་མེད་པའི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="261"/>
        <source>Invalid gateway</source>
        <translation>རྩིས་འགྲོ་མེད་པའི་དྲ་བའི་འགག་སྒོ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="305"/>
        <source>Address conflict</source>
        <translation>ཤག་གནས་གདོང་གཏུག་</translation>
    </message>
</context>
<context>
    <name>JoinHiddenWiFiPage</name>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="140"/>
        <source>Please enter the network information</source>
        <translation>ཁྱེད་རང་ཞུགས་འདོད་པའི་དྲ་རྒྱའི་ཆ་འཕྲིན་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="145"/>
        <source>Network name(SSID)</source>
        <translation>དྲ་རྒྱའི་མིང་། (SSID)</translation>
    </message>
    <message>
        <source>Remember the Network</source>
        <translation type="vanished">དྲ་རྒྱ་དེ་སེམས་ལ་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="146"/>
        <source>Show Network List</source>
        <translation>དྲ་རྒྱའི་རེའུ་མིག་གསལ་པོར་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="147"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="148"/>
        <source>Join</source>
        <translation>དེའི་ནང་དུ་ཞུགས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="153"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="155"/>
        <source>Find and Join WLAN</source>
        <translation>འཚོལ་ཞིབ་བྱས་པ་མ་ཟད་WLANལ་ཞུགས་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>LanListItem</name>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="69"/>
        <source>Not connected</source>
        <translation>འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="126"/>
        <source>Wired Device not carried</source>
        <translation>སྐུད་ཡོད་སྒྲིག་ཆས་འཁྱེར་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="146"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="163"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="261"/>
        <source>Disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="148"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="161"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="265"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="152"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="168"/>
        <source>Property</source>
        <translation>ངོ་བོ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="153"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="170"/>
        <source>Delete</source>
        <translation>དྲ་རྒྱ་དེ་བསུབ་དགོས།</translation>
    </message>
</context>
<context>
    <name>LanPage</name>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1192"/>
        <source>No ethernet device avaliable</source>
        <translation>ཨེ་ཙི་དྲ་རྒྱའི་སྒྲིག་ཆས་ལ་བཙན་འཛུལ་བྱས་མི་ཆོག།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="746"/>
        <source>LAN</source>
        <translation>སྐུད་ཡོད་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="67"/>
        <source>conflict, unable to connect to the network normally!</source>
        <translation>གདོང་གཏུག་རྒྱུན་ལྡན་ལྟར་དྲ་རྒྱ་དང་འབྲེལ་མཐུད་བྱེད་ཐབས་བྲལ་བ་རེད།！</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="748"/>
        <source>Activated LAN</source>
        <translation>ངའི་དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="758"/>
        <source>Inactivated LAN</source>
        <translation>དྲ་བ་གཞན་དག</translation>
    </message>
    <message>
        <source>LAN Disconnected Successfully</source>
        <translation type="vanished">སྐུད་ཡོད་དྲ་བ་ཆད་སོང་།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1236"/>
        <source>Wired Device not carried</source>
        <translation>སྐུད་ཡོད་སྒྲིག་ཆས་འཁྱེར་མེད་པ།</translation>
    </message>
    <message>
        <source>LAN Connected Successfully</source>
        <translation type="vanished">སྐུད་ཡོད་དྲ་བ་སྦྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1338"/>
        <source>Connected: </source>
        <translation>འབྲེལ་མཐུད་བྱུང་ཡོད།: </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1332"/>
        <source>Not Connected</source>
        <translation>འབྲེལ་མཐུད་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <source>(Limited)</source>
        <translation>(དྲ་བར་ཚོད་འཛིན་ཐེབས་པ་རེད།)</translation>
    </message>
</context>
<context>
    <name>ListItem</name>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="178"/>
        <source>Kylin NM</source>
        <translation>དྲ་རྒྱའི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="181"/>
        <source>kylin network applet desktop message</source>
        <translation>དྲ་རྒྱའི་གསལ་འདེབས་གནས་ཚུལ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="214"/>
        <source>kylin-nm</source>
        <translation>དྲ་རྒྱའི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="299"/>
        <source>LAN</source>
        <translatorcomment>有线网络</translatorcomment>
        <translation>སྐུད་ཡོད་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="301"/>
        <source>WLAN</source>
        <translatorcomment>无线局域网</translatorcomment>
        <translation>སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="330"/>
        <source>Show MainWindow</source>
        <translation>རླུང་གཙོ་བོ་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="331"/>
        <source>Settings</source>
        <translatorcomment>设置网络项</translatorcomment>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="573"/>
        <location filename="../frontend/mainwindow.cpp" line="802"/>
        <source>Network tool</source>
        <translation>དྲ་རྒྱའི་ལག་ཆ་</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="587"/>
        <source>Network Card</source>
        <translation>དྲ་བྱང་།</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="785"/>
        <source>Not connected to the network</source>
        <translation>དྲ་རྒྱ་དང་སྦྲེལ་མཐུད་མ་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>MultipleDnsWidget</name>
    <message>
        <source>DNS server:</source>
        <translation type="vanished">DNSཞབས་ཞུ་བ།:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="57"/>
        <source>DNS server(Drag to sort)</source>
        <translation>DNS ཞབས་ཞུའི་ཡོ་བྱད། (འཐེན་འགུལ་ལ་བརྟེན་ནས་རིམ་སྒྲིག་།)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="64"/>
        <source>Click &quot;+&quot; to configure DNS</source>
        <translation>&quot;+&quot;མནན་ན་ད་གཟོད་DNSལ་བཀོད་སྒྲིག་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="101"/>
        <source>Settings</source>
        <translation>མཐོ་རིམ་གྱི་བཀོད་སྒྲིག་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="78"/>
        <source>Kylin NM</source>
        <translation>ཅིན་ལིན་NM</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="81"/>
        <source>kylin network desktop message</source>
        <translation>དྲ་རྒྱའི་གསལ་འདེབས་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="377"/>
        <source>Detail</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="378"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="379"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="381"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="383"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="390"/>
        <source>Config</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="402"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="404"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="462"/>
        <source>Forget this network</source>
        <translation>དྲ་རྒྱ་འདི་བརྗེད་སོང་།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="464"/>
        <source>Delete this network</source>
        <translation>དྲ་རྒྱ་དེ་བསུབ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="443"/>
        <source>Add LAN Connect</source>
        <translation>སྐུད་ཡོད་དྲ་བ་ཁ་སྣོན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="448"/>
        <source>Connect Hidden WLAN</source>
        <translation>ཧའེ་ཏེན་ཝེ་ལན་དང་འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="629"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="641"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1184"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="753"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="754"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="755"/>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="896"/>
        <source>start check ipv4 address conflict</source>
        <translation>ipv4ས་གནས་ཀྱི་འགལ་བ་ལ་ཞིབ་བཤེར་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="913"/>
        <source>start check ipv6 address conflict</source>
        <translation>ipv6གནས་ཡུལ་དང་འགལ་བར་ཞིབ་བཤེར་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <source>ipv4 address conflict!</source>
        <translation type="vanished">ipv4ཐག་གཅོད་གདོང་གཏུག་བྱུང་བ་རེད།!</translation>
    </message>
    <message>
        <source>ipv6 address conflict!</source>
        <translation type="vanished">ipv6ཐག་གཅོད་གདོང་གཏུག་བྱུང་བ་རེད།!</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1180"/>
        <source>this wifi no support enterprise type</source>
        <translation>wifiལ་རྒྱབ་སྐྱོར་མེད་པའི་ཁེ་ལས་ཀྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1185"/>
        <source>this wifi no support None type</source>
        <translation>wifiལ་རྒྱབ་སྐྱོར་མི་བྱེད་པར་རིགས་དབྱིབས་གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1190"/>
        <source>this wifi no support WPA2 type</source>
        <translation>wifiལ་རྒྱབ་སྐྱོར་མི་བྱེད་པའི་WPA2རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1193"/>
        <source>this wifi no support WPA3 type</source>
        <translation>wifiལ་རྒྱབ་སྐྱོར་མི་བྱེད་པའི་WPA3རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <source>SSID:</source>
        <translation type="obsolete">SSID：</translation>
    </message>
    <message>
        <source>Protocol:</source>
        <translation type="obsolete">协议：</translation>
    </message>
    <message>
        <source>Hz:</source>
        <translation type="obsolete">网络频带：</translation>
    </message>
    <message>
        <source>Chan:</source>
        <translation type="obsolete">网络通道：</translation>
    </message>
    <message>
        <source>BandWidth:</source>
        <translation type="obsolete">带宽：</translation>
    </message>
    <message>
        <source>IPv4:</source>
        <translation type="obsolete">IPv4地址：</translation>
    </message>
    <message>
        <source>IPv4 DNS:</source>
        <translation type="obsolete">IPv4 DNS服务器：</translation>
    </message>
    <message>
        <source>IPv6:</source>
        <translation type="obsolete">本地链接IPv6地址：</translation>
    </message>
    <message>
        <source>Mac:</source>
        <translation type="obsolete">物理地址：</translation>
    </message>
</context>
<context>
    <name>OldMainWindow</name>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="obsolete">打开网络工具</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../frontend/list-items/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="obsolete">忘记此网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../frontend/list-items/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>SecurityPage</name>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="107"/>
        <source>Remember the Network</source>
        <translation>དྲ་རྒྱ་དེ་སེམས་ལ་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="232"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="233"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="253"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="237"/>
        <source>EAP type</source>
        <translation>EAP རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="239"/>
        <source>Identity</source>
        <translation>ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="240"/>
        <source>Domain</source>
        <translation>ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="241"/>
        <source>CA certficate</source>
        <translation>CAལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="242"/>
        <source>no need for CA certificate</source>
        <translation>CAཡི་ལག་ཁྱེར་མི་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="243"/>
        <source>User certificate</source>
        <translation>སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="244"/>
        <source>User private key</source>
        <translation>སྤྱོད་མཁན་གྱི་སྒེར་གྱི་ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="245"/>
        <source>User key password</source>
        <translation>སྤྱོད་མཁན་གྱི་ལྡེ་མིག་གི་གསང་</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="246"/>
        <source>Password options</source>
        <translation>གསང་བའི་ཐོག་ནས་རྣམ་གྲངས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="247"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="255"/>
        <location filename="../frontend/netdetails/securitypage.h" line="125"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="251"/>
        <source>Ineer authentication</source>
        <translation>དབྱིན་ཆས་ཀྱི་བདེན་དཔང་ར་སྤྲོད་</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="252"/>
        <source>Username</source>
        <translation>བཀོལ་སྤྱོད་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="254"/>
        <source>Ask pwd each query</source>
        <translation>འདྲི་རྩད་རེ་རེར་འདྲི་རྩད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="258"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="271"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="274"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="277"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="297"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="379"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="522"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1048"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1129"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1160"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1182"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1205"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1260"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="259"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation>WPA&amp;WPA2མི་སྒེར་གྱི་ངོས་ནས་བཤད་ན།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="260"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation>WPA&amp;WPA2 ཁེ་ལས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="261"/>
        <source>WPA3 Personal</source>
        <translation>WPA3མི་སྒེར་</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="272"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="275"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="278"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="298"/>
        <source>Choose from file...</source>
        <translation>ཡིག་ཆའི་ནང་ནས་གདམ་ག་རྒྱག་དགོས།...</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store passwords only for this user</source>
        <translation>སྤྱོད་མཁན་དེ་ཁོ་ནའི་ཆེད་དུ་གསང་གྲངས་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store password only for this user</source>
        <translation>སྤྱོད་མཁན་དེ་ཁོ་ནའི་ཆེད་དུ་གསང་གྲངས་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store passwords for all users</source>
        <translation>སྤྱོད་མཁན་ཚང་མའི་གསང་བ་གསོག་ཉར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store password for all users</source>
        <translation>སྤྱོད་མཁན་ཚང་མའི་གསང་བ་གསོག་ཉར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask this password every time</source>
        <translation>ཐེངས་རེར་གསང་བ་འདི་འདྲི་རྩད་བྱེད་ཐེངས་རེ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask password every time</source>
        <translation>ཐེངས་རེར་གསང་བ་འདི་འདྲི་རྩད་བྱེད་ཐེངས་རེ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1150"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1173"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1195"/>
        <source>Choose a CA certificate</source>
        <translation>CAཡི་དཔང་ཡིག་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1151"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1174"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1196"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA དཔང་ཡིག (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="290"/>
        <source>PAC provisioning</source>
        <translatorcomment>PAC 配置</translatorcomment>
        <translation>PAC གཏན་འབེབས་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="291"/>
        <source>Allow automatic PAC provisioning</source>
        <translatorcomment>允许自动PAC配置</translatorcomment>
        <translation>རང་འགུལ་གྱིས་PACམཚོ་འདོན་བྱས་ཆོག།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="292"/>
        <source>PAC file</source>
        <translatorcomment>PAC 文件</translatorcomment>
        <translation>PACཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="293"/>
        <source>Anonymous</source>
        <translatorcomment>匿名</translatorcomment>
        <translation>མིང་མ་བཀོད་པའི་</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="294"/>
        <source>Authenticated</source>
        <translatorcomment>已认证</translatorcomment>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱས།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="295"/>
        <source>Both</source>
        <translatorcomment>两者兼用</translatorcomment>
        <translation>དེ་གཉིས་ཀ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1250"/>
        <source>Choose a PAC file</source>
        <translatorcomment>选择一个PAC文件</translatorcomment>
        <translation>PACཡིག་ཆ་ཞིག་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1251"/>
        <source>PAC Files (*.pac)</source>
        <translatorcomment>PAC文件(*.pac)</translatorcomment>
        <translation>PACཡིག་ཆ།(*.pac)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.h" line="126"/>
        <source> </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="72"/>
        <source>Current Device</source>
        <translation>མིག་སྔའི་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="79"/>
        <source>Devices Closed!</source>
        <translation>སྒྲིག་ཆས་སྒོ་རྒྱག་པ།!</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="136"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="329"/>
        <source>Kylin NM</source>
        <translation>ཅིན་ལིན་NM</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="332"/>
        <source>kylin network applet desktop message</source>
        <translation>kylinདྲ་རྒྱའི་ཀུ་ཤུའི་ཅོག་ཙེའི་ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>VpnPage</name>
    <message>
        <source>Wired Device not carried</source>
        <translation type="vanished">སྐུད་ཡོད་སྒྲིག་ཆས་འཁྱེར་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../frontend/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="41"/>
        <source>WLAN Authentication</source>
        <translation>སྐུད་མེད་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="52"/>
        <source>Input WLAN Information Please</source>
        <translation>སྐུད་མེད་ཆ་འཕྲིན་ནང་འཇུག་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="53"/>
        <source>WLAN ID：</source>
        <translation>WLAN ID:</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="54"/>
        <source>WLAN Name:</source>
        <translation>སྐུད་མེད་མིང་།:</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="55"/>
        <source>Password：</source>
        <translation>གསང་གྲངས་ནི།:</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="56"/>
        <source>Cancl</source>
        <translation>ཁན་ཁེ་ལན།</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="57"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>WlanListItem</name>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="72"/>
        <source>Not connected</source>
        <translation>འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="177"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="204"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="637"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="656"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="666"/>
        <source>Disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="179"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="208"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="304"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="647"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="664"/>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="188"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="671"/>
        <source>Forget</source>
        <translation>བརྗེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="187"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="676"/>
        <source>Property</source>
        <translation>ངོ་བོ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="325"/>
        <source>Auto Connect</source>
        <translation>རང་འགུལ་གྱིས་སྦྲེལ་མཐུད་</translation>
    </message>
</context>
<context>
    <name>WlanMoreItem</name>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanmoreitem.cpp" line="28"/>
        <source>Add Others...</source>
        <translation>གཞན་པ་ཁ་སྣོན་བྱས་ནས་...</translation>
    </message>
</context>
<context>
    <name>WlanPage</name>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="130"/>
        <source>WLAN</source>
        <translation>སྐུད་མེད་ཅུས་ཁོངས་ཀྱི་དྲ་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="106"/>
        <source>No wireless network card detected</source>
        <translation>སྐུད་མེད་དྲ་རྒྱའི་བྱང་བུ་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="132"/>
        <source>Activated WLAN</source>
        <translation>ངའི་དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="143"/>
        <source>Other WLAN</source>
        <translation>དྲ་བ་གཞན་དག</translation>
    </message>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="974"/>
        <source>WLAN Connected Successfully</source>
        <translation>སྐུད་མེད་དྲ་བ་སྦྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="970"/>
        <source>WLAN Disconnected Successfully</source>
        <translation>སྐུད་མེད་དྲ་རྒྱ་ཆད་སོང་།</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1750"/>
        <source>Connected: </source>
        <translation>འབྲེལ་མཐུད་བྱུང་ཡོད།: </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1752"/>
        <source>Not Connected</source>
        <translation>འབྲེལ་མཐུད་མ་བྱས་པ།</translation>
    </message>
    <message>
        <source>(Limited)</source>
        <translation type="vanished">(དྲ་བར་ཚོད་འཛིན་ཐེབས་པ་རེད།)</translation>
    </message>
</context>
<context>
    <name>WpaWifiDialog</name>
    <message>
        <source>EAP type</source>
        <translation type="obsolete">EAP方法</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Ask pwd each query</source>
        <translation type="obsolete">每次询问密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
    <message>
        <source>Choose from file...</source>
        <translation type="obsolete">从文件选择...</translation>
    </message>
    <message>
        <source>Choose a CA certificate</source>
        <translation type="obsolete">选择一个CA证书</translation>
    </message>
    <message>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="obsolete">CA 证书 (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>no need for CA certificate</source>
        <translation type="obsolete">不需要CA证书</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="118"/>
        <source>kylinnm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="122"/>
        <source>show kylin-nm wifi page</source>
        <translation>སྐུད་མེད་དྲ་རྒྱའི་ངོས་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>show kylin-nm lan page</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱའི་ཤོག་ངོས་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
</context>
</TS>
