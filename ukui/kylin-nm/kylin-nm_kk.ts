<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk" sourcelanguage="en">
<context>
    <name>ConfForm</name>
    <message>
        <source>Manual</source>
        <translation type="obsolete">手动</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">网络提示消息</translation>
    </message>
    <message>
        <source>Will check the IP address conflict</source>
        <translation type="vanished">正在检测ip地址冲突</translation>
    </message>
    <message>
        <source>IPV4 address conflict, Please change IP</source>
        <translation type="vanished">ip地址冲突，请更改ip</translation>
    </message>
    <message>
        <source>IPV6 address conflict, Please change IP</source>
        <translation type="obsolete">ip地址冲突，请更改ip {6 ?}</translation>
    </message>
</context>
<context>
    <name>CopyButton</name>
    <message>
        <source>Copied successfully</source>
        <translation type="vanished">复制成功</translation>
    </message>
    <message>
        <source>Copied successfully!</source>
        <translation type="vanished">复制成功！</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">复制全部</translation>
    </message>
</context>
<context>
    <name>CreatNetPage</name>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="31"/>
        <source>Connection Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="32"/>
        <source>Ipv4Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="33"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="34"/>
        <source>Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="35"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="36"/>
        <source>Prefs DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="37"/>
        <source>Alternative DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="48"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/creatnetpage.cpp" line="49"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="247"/>
        <source>Auto Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="203"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="300"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="129"/>
        <source>Copied successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="177"/>
        <source>Copy all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="197"/>
        <source>Please input SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="207"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="301"/>
        <source>Protocol:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="211"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="302"/>
        <source>Security Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="215"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="303"/>
        <source>Hz:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="219"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="304"/>
        <source>Chan:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="223"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="305"/>
        <source>BandWidth:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="237"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="308"/>
        <source>IPV6:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="227"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="306"/>
        <source>IPV4:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="231"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="307"/>
        <source>IPV4 Dns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="241"/>
        <location filename="../src/frontend/netdetails/detailpage.cpp" line="309"/>
        <source>Mac:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgHideWifi</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapFast</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPeap</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPwd</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTTLS</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTls</name>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="obsolete">用户证书</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="obsolete">用户私钥</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="obsolete">用户密钥密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWep</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWpa</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanDialog</name>
    <message>
        <location filename="../src/frontend/enterprise-wlan/enterprisewlandialog.cpp" line="25"/>
        <source>Connect Enterprise WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../src/frontend/enterprise-wlan/enterprisewlandialog.cpp" line="85"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/enterprise-wlan/enterprisewlandialog.cpp" line="86"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ipv4Page</name>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="49"/>
        <source>Ipv4Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="50"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="51"/>
        <source>Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="52"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="53"/>
        <source>Prefs DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="54"/>
        <source>Alternative DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="88"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="89"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="267"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="277"/>
        <source>Invalid subnet mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="297"/>
        <location filename="../src/frontend/netdetails/ipv4page.cpp" line="298"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ipv6Page</name>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="130"/>
        <source>Ipv6Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="131"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="132"/>
        <source>Subnet prefix Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="133"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="134"/>
        <source>Prefs DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="135"/>
        <source>Alternative DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="169"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="170"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="226"/>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="227"/>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="228"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="246"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/ipv6page.cpp" line="256"/>
        <source>Invalid gateway</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LanListItem</name>
    <message>
        <location filename="../src/frontend/list-items/lanlistitem.cpp" line="44"/>
        <source>Not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/lanlistitem.cpp" line="101"/>
        <source>Wired Device not carried</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/lanlistitem.cpp" line="122"/>
        <location filename="../src/frontend/list-items/lanlistitem.cpp" line="138"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/lanlistitem.cpp" line="124"/>
        <location filename="../src/frontend/list-items/lanlistitem.cpp" line="136"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LanPage</name>
    <message>
        <location filename="../src/frontend/tab-pages/lanpage.cpp" line="1185"/>
        <source>No ethernet device avaliable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/lanpage.cpp" line="735"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/lanpage.cpp" line="737"/>
        <source>Activated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/lanpage.cpp" line="746"/>
        <source>Inactivated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/lanpage.cpp" line="849"/>
        <source>LAN Disconnected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/lanpage.cpp" line="1209"/>
        <source>Wired Device not carried</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/lanpage.cpp" line="825"/>
        <source>LAN Connected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItem</name>
    <message>
        <location filename="../src/frontend/list-items/listitem.cpp" line="69"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/listitem.cpp" line="72"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/frontend/mainwindow.cpp" line="166"/>
        <location filename="../src/frontend/mainwindow.cpp" line="287"/>
        <source>kylin-nm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/mainwindow.cpp" line="249"/>
        <source>LAN</source>
        <translatorcomment>有线网络</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/mainwindow.cpp" line="251"/>
        <source>WLAN</source>
        <translatorcomment>无线局域网</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/mainwindow.cpp" line="284"/>
        <source>Show MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/mainwindow.cpp" line="285"/>
        <source>Settings</source>
        <translatorcomment>设置网络项</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="48"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="51"/>
        <source>kylin network desktop message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="248"/>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="249"/>
        <source>Ipv4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="250"/>
        <source>Ipv6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="252"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="267"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="270"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="273"/>
        <source>Forget this network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="304"/>
        <source>Add Lan Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="309"/>
        <source>connect hiddin wlan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="455"/>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="467"/>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="934"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="566"/>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="567"/>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="568"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="684"/>
        <source>start check ipv4 address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="701"/>
        <source>start check ipv6 address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="747"/>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="798"/>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="886"/>
        <source>ipv4 address conflict!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="806"/>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="894"/>
        <source>ipv6 address conflict!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="930"/>
        <source>this wifi no support enterprise type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="935"/>
        <source>this wifi no support None type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="940"/>
        <source>this wifi no support WPA2 type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/netdetail.cpp" line="943"/>
        <source>this wifi no support WPA3 type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>SSID:</source>
        <translation type="obsolete">SSID：</translation>
    </message>
    <message>
        <source>Protocol:</source>
        <translation type="obsolete">协议：</translation>
    </message>
    <message>
        <source>Hz:</source>
        <translation type="obsolete">网络频带：</translation>
    </message>
    <message>
        <source>Chan:</source>
        <translation type="obsolete">网络通道：</translation>
    </message>
    <message>
        <source>BandWidth:</source>
        <translation type="obsolete">带宽：</translation>
    </message>
    <message>
        <source>IPV4:</source>
        <translation type="obsolete">IPV4地址：</translation>
    </message>
    <message>
        <source>IPV4 Dns:</source>
        <translation type="obsolete">IPV4 DNS服务器：</translation>
    </message>
    <message>
        <source>IPV6:</source>
        <translation type="obsolete">本地链接IPV6地址：</translation>
    </message>
    <message>
        <source>Mac:</source>
        <translation type="obsolete">物理地址：</translation>
    </message>
</context>
<context>
    <name>OldMainWindow</name>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="obsolete">打开网络工具</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../src/frontend/list-items/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="obsolete">忘记此网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../src/frontend/list-items/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>SecurityPage</name>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="72"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="73"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="88"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="75"/>
        <source>EAP type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="77"/>
        <source>Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="78"/>
        <source>Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="79"/>
        <source>CA certficate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="80"/>
        <source>no need for CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="81"/>
        <source>User certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="82"/>
        <source>User private key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="83"/>
        <source>User key password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="86"/>
        <source>Ineer authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="87"/>
        <source>Usename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username</source>
        <oldsource>Usename</oldsource>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="89"/>
        <source>Ask pwd each query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="91"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="101"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="104"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="107"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="176"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="675"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="696"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="718"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="741"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="92"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="93"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="94"/>
        <source>WPA3 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="102"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="105"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="108"/>
        <source>Choose from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="686"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="709"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="731"/>
        <source>Choose a CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="687"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="710"/>
        <location filename="../src/frontend/netdetails/securitypage.cpp" line="732"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="../src/frontend/tab-pages/tabpage.cpp" line="45"/>
        <source>Current Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/tabpage.cpp" line="50"/>
        <source>Devices Closed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/tabpage.cpp" line="99"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/tabpage.cpp" line="154"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/tabpage.cpp" line="157"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../src/frontend/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/frontend/wificonfigdialog.cpp" line="21"/>
        <source>WLAN Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/wificonfigdialog.cpp" line="32"/>
        <source>Input WLAN Information Please</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/wificonfigdialog.cpp" line="33"/>
        <source>WLAN ID：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/wificonfigdialog.cpp" line="34"/>
        <source>WLAN Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/wificonfigdialog.cpp" line="35"/>
        <source>Password：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/wificonfigdialog.cpp" line="36"/>
        <source>Cancl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/wificonfigdialog.cpp" line="37"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanListItem</name>
    <message>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="43"/>
        <source>Not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="136"/>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="565"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="138"/>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="251"/>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="563"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="145"/>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="570"/>
        <source>Forget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/wlanlistitem.cpp" line="272"/>
        <source>Auto Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanMoreItem</name>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../src/frontend/list-items/wlanmoreitem.cpp" line="8"/>
        <source>Add Others...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanPage</name>
    <message>
        <location filename="../src/frontend/tab-pages/wlanpage.cpp" line="85"/>
        <source>WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/wlanpage.cpp" line="70"/>
        <source>No wireless network card detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/wlanpage.cpp" line="87"/>
        <source>Activated WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/wlanpage.cpp" line="97"/>
        <source>Other WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/wlanpage.cpp" line="804"/>
        <source>WLAN Connected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/frontend/tab-pages/wlanpage.cpp" line="544"/>
        <location filename="../src/frontend/tab-pages/wlanpage.cpp" line="800"/>
        <source>WLAN Disconnected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WpaWifiDialog</name>
    <message>
        <source>EAP type</source>
        <translation type="obsolete">EAP方法</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Ask pwd each query</source>
        <translation type="obsolete">每次询问密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
    <message>
        <source>Choose from file...</source>
        <translation type="obsolete">从文件选择...</translation>
    </message>
    <message>
        <source>Choose a CA certificate</source>
        <translation type="obsolete">选择一个CA证书</translation>
    </message>
    <message>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="obsolete">CA 证书 (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>no need for CA certificate</source>
        <translation type="obsolete">不需要CA证书</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="96"/>
        <source>kylinnm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="100"/>
        <source>show kylin-nm wifi page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="101"/>
        <source>show kylin-nm lan page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
