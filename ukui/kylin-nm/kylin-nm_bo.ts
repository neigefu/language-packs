<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ConfigPage</name>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="62"/>
        <source>Network profile type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="65"/>
        <source>Public(recommended)  Devices on the network cannot discover this computer. Generally, it is suitable for networks in public places, such as airports or coffee shops, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="69"/>
        <source>Private  Devices on the network can discover this computer. Generally applicable to a network at home or work where you know and trust the individuals and devices on the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="73"/>
        <source>Config firewall and security settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreatNetPage</name>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="95"/>
        <source>Connection Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="96"/>
        <source>IPv4Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="97"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="98"/>
        <source>Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="99"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="192"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="210"/>
        <source>Invalid subnet mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="230"/>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="231"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="330"/>
        <source>Address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="118"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="302"/>
        <source>Auto Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="256"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="355"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="150"/>
        <source>Copied successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="227"/>
        <source>Copy all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="247"/>
        <source>Please input SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="260"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="356"/>
        <source>Protocol:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="264"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="357"/>
        <source>Security Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="268"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="358"/>
        <source>Hz:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="272"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="359"/>
        <source>Chan:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="276"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="360"/>
        <source>BandWidth:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="280"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="361"/>
        <source>IPv4:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="286"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="362"/>
        <source>IPv4 DNS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="292"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="363"/>
        <source>IPv6:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="296"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="364"/>
        <source>Mac:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DnsSettingWidget</name>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="34"/>
        <source>DNS Server Advanced Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="37"/>
        <source>Tactic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="40"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="43"/>
        <source>Retry Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="46"/>
        <source>order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="47"/>
        <source>rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="48"/>
        <source>concurrency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="53"/>
        <source> s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="59"/>
        <source> times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="70"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="73"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="76"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanDialog</name>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="116"/>
        <source>Wi-Fi network requires authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="121"/>
        <source>Access to Wi-Fi network &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="123"/>
        <source>&quot; requires a password or encryption key.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="154"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="155"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirewallDialog</name>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="85"/>
        <source>Allow other devices on this network to discover this computer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="87"/>
        <source>It is not recommended to enable this feature on public networks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="89"/>
        <source>Not allowed (recommended)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="90"/>
        <source>Allowed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ipv4Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="67"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="68"/>
        <source>Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="69"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="66"/>
        <source>IPv4Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="118"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="274"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="283"/>
        <source>Invalid subnet mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="309"/>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="310"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="380"/>
        <source>Address conflict</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ipv6Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="135"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="136"/>
        <source>Subnet prefix Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="137"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="134"/>
        <source>IPv6Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="180"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="181"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="232"/>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="233"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="252"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="261"/>
        <source>Invalid gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="305"/>
        <source>Address conflict</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JoinHiddenWiFiPage</name>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="140"/>
        <source>Please enter the network information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="145"/>
        <source>Network name(SSID)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="146"/>
        <source>Show Network List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="147"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="148"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="153"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="155"/>
        <source>Find and Join WLAN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LanListItem</name>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="69"/>
        <source>Not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="126"/>
        <source>Wired Device not carried</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="146"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="163"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="261"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="148"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="161"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="265"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="152"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="168"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="153"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="170"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LanPage</name>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1192"/>
        <source>No ethernet device avaliable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="746"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="67"/>
        <source>conflict, unable to connect to the network normally!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="748"/>
        <source>Activated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="758"/>
        <source>Inactivated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1236"/>
        <source>Wired Device not carried</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1338"/>
        <source>Connected: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <source>(Limited)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1332"/>
        <source>Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItem</name>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="178"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="181"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="214"/>
        <source>kylin-nm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="299"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="301"/>
        <source>WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="331"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="573"/>
        <location filename="../frontend/mainwindow.cpp" line="802"/>
        <source>Network tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="587"/>
        <source>Network Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="785"/>
        <source>Not connected to the network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="330"/>
        <source>Show MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MultipleDnsWidget</name>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="57"/>
        <source>DNS server(Drag to sort)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="64"/>
        <source>Click &quot;+&quot; to configure DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="101"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="78"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="81"/>
        <source>kylin network desktop message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="377"/>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="381"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="383"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="390"/>
        <source>Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="402"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="404"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="462"/>
        <source>Forget this network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="378"/>
        <source>IPv4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="379"/>
        <source>IPv6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="443"/>
        <source>Add LAN Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="448"/>
        <source>Connect Hidden WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="464"/>
        <source>Delete this network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="629"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="641"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1184"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="753"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="754"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="755"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="896"/>
        <source>start check ipv4 address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="913"/>
        <source>start check ipv6 address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1180"/>
        <source>this wifi no support enterprise type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1185"/>
        <source>this wifi no support None type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1190"/>
        <source>this wifi no support WPA2 type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1193"/>
        <source>this wifi no support WPA3 type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../frontend/list-items/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../frontend/list-items/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SecurityPage</name>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="107"/>
        <source>Remember the Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="232"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="233"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="253"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="237"/>
        <source>EAP type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="239"/>
        <source>Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="240"/>
        <source>Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="241"/>
        <source>CA certficate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="242"/>
        <source>no need for CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="243"/>
        <source>User certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="244"/>
        <source>User private key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="245"/>
        <source>User key password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="246"/>
        <source>Password options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="247"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="255"/>
        <location filename="../frontend/netdetails/securitypage.h" line="125"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="251"/>
        <source>Ineer authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="252"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="254"/>
        <source>Ask pwd each query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="258"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="271"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="274"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="277"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="297"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="379"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="522"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1048"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1129"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1160"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1182"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1205"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1260"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="259"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="260"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="261"/>
        <source>WPA3 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="272"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="275"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="278"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="298"/>
        <source>Choose from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store passwords only for this user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store password only for this user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store passwords for all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store password for all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask this password every time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask password every time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="290"/>
        <source>PAC provisioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="291"/>
        <source>Allow automatic PAC provisioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="292"/>
        <source>PAC file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="293"/>
        <source>Anonymous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="294"/>
        <source>Authenticated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="295"/>
        <source>Both</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1150"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1173"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1195"/>
        <source>Choose a CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1151"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1174"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1196"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1250"/>
        <source>Choose a PAC file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1251"/>
        <source>PAC Files (*.pac)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.h" line="126"/>
        <source> </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="72"/>
        <source>Current Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="79"/>
        <source>Devices Closed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="136"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="329"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="332"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../frontend/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="41"/>
        <source>WLAN Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="52"/>
        <source>Input WLAN Information Please</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="53"/>
        <source>WLAN ID：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="54"/>
        <source>WLAN Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="55"/>
        <source>Password：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="56"/>
        <source>Cancl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="57"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanListItem</name>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="72"/>
        <source>Not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="177"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="204"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="637"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="656"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="666"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="179"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="208"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="304"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="647"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="664"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="187"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="676"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="188"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="671"/>
        <source>Forget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="325"/>
        <source>Auto Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanMoreItem</name>
    <message>
        <location filename="../frontend/list-items/wlanmoreitem.cpp" line="28"/>
        <source>Add Others...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanPage</name>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="130"/>
        <source>WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="132"/>
        <source>Activated WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="143"/>
        <source>Other WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1750"/>
        <source>Connected: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1752"/>
        <source>Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="106"/>
        <source>No wireless network card detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="974"/>
        <source>WLAN Connected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="970"/>
        <source>WLAN Disconnected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="118"/>
        <source>kylinnm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="122"/>
        <source>show kylin-nm wifi page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>show kylin-nm lan page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
