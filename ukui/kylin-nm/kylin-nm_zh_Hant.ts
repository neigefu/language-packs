<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant" sourcelanguage="en">
<context>
    <name>ConfForm</name>
    <message>
        <source>Manual</source>
        <translation type="obsolete">手动</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">网络提示消息</translation>
    </message>
    <message>
        <source>Will check the IP address conflict</source>
        <translation type="vanished">正在检测ip地址冲突</translation>
    </message>
    <message>
        <source>IPv4 address conflict, Please change IP</source>
        <translation type="vanished">ip地址冲突，请更改ip</translation>
    </message>
    <message>
        <source>IPv6 address conflict, Please change IP</source>
        <translation type="obsolete">ip地址冲突，请更改ip {6 ?}</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="62"/>
        <source>Network profile type</source>
        <translation>網路配置檔類型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="65"/>
        <source>Public(recommended)  Devices on the network cannot discover this computer. Generally, it is suitable for networks in public places, such as airports or coffee shops, etc.</source>
        <translation>公用（推薦） 網路中的設備不可發現此電腦。 一般情況下適用於公共場所中的網路，如機場或咖啡店等等。</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="69"/>
        <source>Private  Devices on the network can discover this computer. Generally applicable to a network at home or work where you know and trust the individuals and devices on the network.</source>
        <translation>專用網路中的設備可發現此電腦。 一般情況下適用於家庭或工作單位的網路，您認識並信任網路上的個人和設備。</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="73"/>
        <source>Config firewall and security settings</source>
        <translation>配置防火牆和安全設置</translation>
    </message>
</context>
<context>
    <name>CopyButton</name>
    <message>
        <source>Copied successfully</source>
        <translation type="vanished">复制成功</translation>
    </message>
    <message>
        <source>Copied successfully!</source>
        <translation type="vanished">复制成功！</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">复制全部</translation>
    </message>
</context>
<context>
    <name>CreatNetPage</name>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="87"/>
        <source>Connection Name</source>
        <translation>網路名稱</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="88"/>
        <source>IPv4Config</source>
        <translation>IPv4 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="89"/>
        <source>Address</source>
        <translation>IPv4 位址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="90"/>
        <source>Netmask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="91"/>
        <source>Default Gateway</source>
        <translation>默認閘道</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">首选 DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">备选 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="107"/>
        <source>Auto(DHCP)</source>
        <translation>自動 （DHCP）</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="108"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="172"/>
        <source>Invalid address</source>
        <translation>無效的IP位址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="181"/>
        <source>Invalid subnet mask</source>
        <translation>無效的子網掩碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="201"/>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="202"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="269"/>
        <source>Auto Connection</source>
        <translation>自動連接</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="223"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="322"/>
        <source>SSID:</source>
        <translation>SSID：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="149"/>
        <source>Copied successfully!</source>
        <translation>複製成功！</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="197"/>
        <source>Copy all</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="217"/>
        <source>Please input SSID:</source>
        <translation>請輸入 SSID：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="227"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="323"/>
        <source>Protocol:</source>
        <translation>協定：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="231"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="324"/>
        <source>Security Type:</source>
        <translation>安全類型：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="235"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="325"/>
        <source>Hz:</source>
        <translation>網路頻帶：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="239"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="326"/>
        <source>Chan:</source>
        <translation>網路通道：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="243"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="327"/>
        <source>BandWidth:</source>
        <translation>頻寬：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="259"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="330"/>
        <source>IPv6:</source>
        <translation>本地連結 IPv6 位址：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="247"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="328"/>
        <source>IPv4:</source>
        <translation>IPv4 位址：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="253"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="329"/>
        <source>IPv4 DNS:</source>
        <translation>IPv4 DNS 伺服器：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="263"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="331"/>
        <source>Mac:</source>
        <translation>物理位址：</translation>
    </message>
</context>
<context>
    <name>DlgHideWifi</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapFast</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPeap</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPwd</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTTLS</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTls</name>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="obsolete">用户证书</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="obsolete">用户私钥</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="obsolete">用户密钥密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWep</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWpa</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DnsSettingWidget</name>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="34"/>
        <source>DNS Server Advanced Settings</source>
        <translation>DNS 伺服器高級配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="37"/>
        <source>Tactic</source>
        <translation>策略</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="40"/>
        <source>Timeout</source>
        <translation>超時時間</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="43"/>
        <source>Retry Count</source>
        <translation>重試次數</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="46"/>
        <source>order</source>
        <translation>順序</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="47"/>
        <source>rotate</source>
        <translation>隨機</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="48"/>
        <source>concurrency</source>
        <translation>併發</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="53"/>
        <source> s</source>
        <translation> 秒</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="59"/>
        <source> times</source>
        <translation> 次</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="72"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="75"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanDialog</name>
    <message>
        <source>Connect Enterprise WLAN</source>
        <translation type="vanished">连接企业网</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="106"/>
        <source>Wi-Fi network requires authentication</source>
        <translation>Wi-Fi 網路要求認證</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="111"/>
        <source>Access to Wi-Fi network &quot;</source>
        <translation>訪問Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="113"/>
        <source>&quot; requires a password or encryption key.</source>
        <translation>需要密碼或加密金鑰。</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="148"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="149"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
</context>
<context>
    <name>FirewallDialog</name>
    <message>
        <source>Allow your computer to be discovered by other computers and devices on this network？</source>
        <translation type="vanished">是否允许你的电脑被此网络上的其他电脑和设备发现？</translation>
    </message>
    <message>
        <source>It is recommended that you enable this feature on your home and work networks rather than public networks.</source>
        <translation type="vanished">建议你在家庭和工作网络上而非公共网络上启用此功能。</translation>
    </message>
    <message>
        <source>Yse</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="85"/>
        <source>Allow other devices on this network to discover this computer?</source>
        <translation>是否允許此網路上的其他設備發現這台電腦？</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="87"/>
        <source>It is not recommended to enable this feature on public networks</source>
        <translation>不建議在公共網路上開啟此功能</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="89"/>
        <source>Not allowed (recommended)</source>
        <translation>不允許（推薦）</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="90"/>
        <source>Allowed</source>
        <translation>允許</translation>
    </message>
</context>
<context>
    <name>Ipv4Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="66"/>
        <source>IPv4Config</source>
        <translation>IPv4 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="67"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="68"/>
        <source>Netmask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="69"/>
        <source>Default Gateway</source>
        <translation>默認閘道</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">首选 DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">备选 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="115"/>
        <source>Auto(DHCP)</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="116"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="272"/>
        <source>Invalid address</source>
        <translation>無效的IP位址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="281"/>
        <source>Invalid subnet mask</source>
        <translation>無效的子網掩碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="307"/>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="308"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="378"/>
        <source>Address conflict</source>
        <translation>位址衝突</translation>
    </message>
</context>
<context>
    <name>Ipv6Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="134"/>
        <source>IPv6Config</source>
        <translation>IPv6 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="135"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="136"/>
        <source>Subnet prefix Length</source>
        <translation>子網前綴長度</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="137"/>
        <source>Default Gateway</source>
        <translation>默認閘道</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">首选 DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">备选 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="178"/>
        <source>Auto(DHCP)</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="179"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="230"/>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="231"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="250"/>
        <source>Invalid address</source>
        <translation>無效的IP位址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="259"/>
        <source>Invalid gateway</source>
        <translation>無效的閘道位址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="303"/>
        <source>Address conflict</source>
        <translation>位址衝突</translation>
    </message>
</context>
<context>
    <name>JoinHiddenWiFiPage</name>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="137"/>
        <source>Please enter the network information</source>
        <translation>請輸入您想要加入的網路資訊</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="142"/>
        <source>Network name(SSID)</source>
        <translation>網路名稱 （SSID）</translation>
    </message>
    <message>
        <source>Remember the Network</source>
        <translation type="vanished">记住该网络</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="143"/>
        <source>Show Network List</source>
        <translation>顯示網路清單</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="144"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="145"/>
        <source>Join</source>
        <translation>加入</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="148"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="150"/>
        <source>Find and Join WLAN</source>
        <translation>查找並加入無線局域網路</translation>
    </message>
</context>
<context>
    <name>LanListItem</name>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="69"/>
        <source>Not connected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="126"/>
        <source>Wired Device not carried</source>
        <translation>未插入網線</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="146"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="163"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="261"/>
        <source>Disconnect</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="148"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="161"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="265"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="152"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="168"/>
        <source>Property</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="153"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="170"/>
        <source>Delete</source>
        <translation>刪除此網路</translation>
    </message>
</context>
<context>
    <name>LanPage</name>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1187"/>
        <source>No ethernet device avaliable</source>
        <translation>未檢測到有線設備</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="744"/>
        <source>LAN</source>
        <translation>有線網路</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="67"/>
        <source>conflict, unable to connect to the network normally!</source>
        <translation>衝突，無法正常連接到網路！</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="746"/>
        <source>Activated LAN</source>
        <translation>我的網路</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="756"/>
        <source>Inactivated LAN</source>
        <translation>其他網路</translation>
    </message>
    <message>
        <source>LAN Disconnected Successfully</source>
        <translation type="vanished">有线网络已断开</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1231"/>
        <source>Wired Device not carried</source>
        <translation>未插入網線</translation>
    </message>
    <message>
        <source>LAN Connected Successfully</source>
        <translation type="vanished">有线网络已连接</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1325"/>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1333"/>
        <source>Connected: </source>
        <translation>已連線： </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1327"/>
        <source>Not Connected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1325"/>
        <source>(Limited)</source>
        <translation>（網路受限）</translation>
    </message>
</context>
<context>
    <name>ListItem</name>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="168"/>
        <source>Kylin NM</source>
        <translation>網路設置工具</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="171"/>
        <source>kylin network applet desktop message</source>
        <translation>網路提示消息</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="214"/>
        <source>kylin-nm</source>
        <translation>網路工具</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="298"/>
        <source>LAN</source>
        <translatorcomment>有线网络</translatorcomment>
        <translation>有線網路</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="300"/>
        <source>WLAN</source>
        <translatorcomment>无线局域网</translatorcomment>
        <translation>無線局域網</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="329"/>
        <source>Show MainWindow</source>
        <translation>打開網路工具</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="330"/>
        <source>Settings</source>
        <translatorcomment>设置网络项</translatorcomment>
        <translation>設置網路項</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="572"/>
        <location filename="../frontend/mainwindow.cpp" line="776"/>
        <source>Network tool</source>
        <translation>網路工具</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="586"/>
        <source>Network Card</source>
        <translation>網卡</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="759"/>
        <source>Not connected to the network</source>
        <translation>未連接網路</translation>
    </message>
</context>
<context>
    <name>MultipleDnsWidget</name>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="57"/>
        <source>DNS server(Drag to sort)</source>
        <translation>DNS 伺服器高級配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="64"/>
        <source>Click &quot;+&quot; to configure DNS</source>
        <translation>點擊 「+」配置 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="101"/>
        <source>Settings</source>
        <translation>高級設置</translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="75"/>
        <source>Kylin NM</source>
        <translation>網路設置工具</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="78"/>
        <source>kylin network desktop message</source>
        <translation>網路提示消息</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="359"/>
        <source>Detail</source>
        <translation>詳情</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="360"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="361"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="363"/>
        <source>Security</source>
        <translation>安全</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="365"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="372"/>
        <source>Config</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="384"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="386"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="444"/>
        <source>Forget this network</source>
        <translation>忘記此網路</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="446"/>
        <source>Delete this network</source>
        <translation>刪除此網路</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="425"/>
        <source>Add LAN Connect</source>
        <translation>添加有線網路</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="430"/>
        <source>Connect Hidden WLAN</source>
        <translation>連接到隱藏 WLAN</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="611"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="623"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1166"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="735"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="736"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="737"/>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="878"/>
        <source>start check ipv4 address conflict</source>
        <translation>開始檢測 ipv4 位址衝突</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="895"/>
        <source>start check ipv6 address conflict</source>
        <translation>開始檢測 ipv6 位址衝突</translation>
    </message>
    <message>
        <source>ipv4 address conflict!</source>
        <translation type="vanished">ipv4地址冲突！</translation>
    </message>
    <message>
        <source>ipv6 address conflict!</source>
        <translation type="vanished">ipv6地址冲突！</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1162"/>
        <source>this wifi no support enterprise type</source>
        <translation>此 wifi 不支援企業網類型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1167"/>
        <source>this wifi no support None type</source>
        <translation>此 wifi 不支援空類型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1172"/>
        <source>this wifi no support WPA2 type</source>
        <translation>此 wifi 不支援 WPA2 類型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1175"/>
        <source>this wifi no support WPA3 type</source>
        <translation>此 wifi 不支援 WPA3 類型</translation>
    </message>
    <message>
        <source>SSID:</source>
        <translation type="obsolete">SSID：</translation>
    </message>
    <message>
        <source>Protocol:</source>
        <translation type="obsolete">协议：</translation>
    </message>
    <message>
        <source>Hz:</source>
        <translation type="obsolete">网络频带：</translation>
    </message>
    <message>
        <source>Chan:</source>
        <translation type="obsolete">网络通道：</translation>
    </message>
    <message>
        <source>BandWidth:</source>
        <translation type="obsolete">带宽：</translation>
    </message>
    <message>
        <source>IPv4:</source>
        <translation type="obsolete">IPv4地址：</translation>
    </message>
    <message>
        <source>IPv4 DNS:</source>
        <translation type="obsolete">IPv4 DNS服务器：</translation>
    </message>
    <message>
        <source>IPv6:</source>
        <translation type="obsolete">本地链接IPv6地址：</translation>
    </message>
    <message>
        <source>Mac:</source>
        <translation type="obsolete">物理地址：</translation>
    </message>
</context>
<context>
    <name>OldMainWindow</name>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="obsolete">打开网络工具</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../frontend/list-items/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="obsolete">忘记此网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../frontend/list-items/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>SecurityPage</name>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="107"/>
        <source>Remember the Network</source>
        <translation>記住該網路</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="232"/>
        <source>Security</source>
        <translation>安全性</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="233"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="253"/>
        <source>Password</source>
        <translation>金鑰</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="237"/>
        <source>EAP type</source>
        <translation>EAP 方法</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="239"/>
        <source>Identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="240"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="241"/>
        <source>CA certficate</source>
        <translation>CA 證書</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="242"/>
        <source>no need for CA certificate</source>
        <translation>不需要 CA 證書</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="243"/>
        <source>User certificate</source>
        <translation>用戶證書</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="244"/>
        <source>User private key</source>
        <translation>使用者私鑰</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="245"/>
        <source>User key password</source>
        <translation>用戶金鑰密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="246"/>
        <source>Password options</source>
        <translation>密碼選項</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="247"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="255"/>
        <location filename="../frontend/netdetails/securitypage.h" line="125"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="251"/>
        <source>Ineer authentication</source>
        <translation>內部認證</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="252"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="254"/>
        <source>Ask pwd each query</source>
        <translation>每次詢問密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="258"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="271"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="274"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="277"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="297"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="379"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="522"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1048"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1129"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1160"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1182"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1211"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1266"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="259"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation>WPA&amp;WPA2 個人</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="260"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation>WPA&amp;WPA2 企業</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="261"/>
        <source>WPA3 Personal</source>
        <translation>WPA3 個人</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="272"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="275"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="278"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="298"/>
        <source>Choose from file...</source>
        <translation>從檔案選擇...</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store passwords only for this user</source>
        <translation>僅為該使用者存儲密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store password only for this user</source>
        <translation>僅為該使用者存儲密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store passwords for all users</source>
        <translation>存儲所有用戶的密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store password for all users</source>
        <translation>存儲所有用戶的密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask this password every time</source>
        <translation>每次詢問這個密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask password every time</source>
        <translation>每次詢問這個密碼</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1150"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1173"/>
        <source>Choose a CA certificate</source>
        <translation>選擇一個 CA 證書</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1151"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1174"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA 憑證 （*.pem *.der *.p12 *.crt *.cer *.pfx）</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="290"/>
        <source>PAC provisioning</source>
        <translation>PAC 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="291"/>
        <source>Allow automatic PAC provisioning</source>
        <translation>允許自動 PAC 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="292"/>
        <source>PAC file</source>
        <translation>PAC 檔</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="293"/>
        <source>Anonymous</source>
        <translation>匿名</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="294"/>
        <source>Authenticated</source>
        <translation>已認證</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="295"/>
        <source>Both</source>
        <translation>兩者兼用</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1256"/>
        <source>Choose a PAC file</source>
        <translation>選擇一個 PAC 檔</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1257"/>
        <source>PAC Files (*.pac)</source>
        <translation>PAC 檔（*.pac）</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.h" line="126"/>
        <source> </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="72"/>
        <source>Current Device</source>
        <translation>當前網卡</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="77"/>
        <source>Devices Closed!</source>
        <translation>設備關閉！</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="134"/>
        <source>Settings</source>
        <translation>網路設置</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="325"/>
        <source>Kylin NM</source>
        <translation>網路設置工具</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="328"/>
        <source>kylin network applet desktop message</source>
        <translation>網路提示消息</translation>
    </message>
</context>
<context>
    <name>VpnPage</name>
    <message>
        <source>Wired Device not carried</source>
        <translation type="obsolete">未插入网线</translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../frontend/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="41"/>
        <source>WLAN Authentication</source>
        <translation>無線局域網認證</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="52"/>
        <source>Input WLAN Information Please</source>
        <translation>請輸入無線域網資訊</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="53"/>
        <source>WLAN ID：</source>
        <translation>無線區域網ID：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="54"/>
        <source>WLAN Name:</source>
        <translation>WLAN名稱：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="55"/>
        <source>Password：</source>
        <translation>金鑰：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="56"/>
        <source>Cancl</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="57"/>
        <source>Ok</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>WlanListItem</name>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="74"/>
        <source>Not connected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="179"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="207"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="671"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="690"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="700"/>
        <source>Disconnect</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="181"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="211"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="338"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="681"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="698"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="190"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="705"/>
        <source>Forget</source>
        <translation>忘記此網路</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="189"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="710"/>
        <source>Property</source>
        <translation>屬性</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="359"/>
        <source>Auto Connect</source>
        <translation>自動加入該網路</translation>
    </message>
</context>
<context>
    <name>WlanMoreItem</name>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanmoreitem.cpp" line="28"/>
        <source>Add Others...</source>
        <translation>加入其他網路...</translation>
    </message>
</context>
<context>
    <name>WlanPage</name>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="129"/>
        <source>WLAN</source>
        <translation>無線局域網</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="105"/>
        <source>No wireless network card detected</source>
        <translation>未檢測到無線網卡</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="131"/>
        <source>Activated WLAN</source>
        <translation>我的網路</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="142"/>
        <source>Other WLAN</source>
        <translation>其他網路</translation>
    </message>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="927"/>
        <source>WLAN Connected Successfully</source>
        <translation>無線網路已連接</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="923"/>
        <source>WLAN Disconnected Successfully</source>
        <translation>無線網路已斷開</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1680"/>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1687"/>
        <source>Connected: </source>
        <translation>已連線： </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1682"/>
        <source>Not Connected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1680"/>
        <source>(Limited)</source>
        <translation>（有限）</translation>
    </message>
</context>
<context>
    <name>WpaWifiDialog</name>
    <message>
        <source>EAP type</source>
        <translation type="obsolete">EAP方法</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Ask pwd each query</source>
        <translation type="obsolete">每次询问密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
    <message>
        <source>Choose from file...</source>
        <translation type="obsolete">从文件选择...</translation>
    </message>
    <message>
        <source>Choose a CA certificate</source>
        <translation type="obsolete">选择一个CA证书</translation>
    </message>
    <message>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="obsolete">CA 证书 (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>no need for CA certificate</source>
        <translation type="obsolete">不需要CA证书</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="96"/>
        <source>kylinnm</source>
        <translation>麒麟網路工具</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="100"/>
        <source>show kylin-nm wifi page</source>
        <translation>顯示麒麟網路wifi頁面</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="101"/>
        <source>show kylin-nm lan page</source>
        <translation>顯示麒麟網路局域網頁面</translation>
    </message>
</context>
</TS>
