<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>SinglePage</name>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="73"/>
        <source>Settings</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="121"/>
        <source>Kylin VPN</source>
        <translation>VPN ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="124"/>
        <source>kylin vpn applet desktop message</source>
        <translation>VPN ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>VpnAdvancedPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="791"/>
        <source>MPPE encryption algorithm:</source>
        <translation>MPPE ᠨᠢᠭᠤᠴᠠᠯᠠᠨ ᠪᠤᠳᠤᠬᠤ ᠠᠷᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="792"/>
        <source>Use Stateful encryption</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ ᠳᠦᠯᠦᠪ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="793"/>
        <source>Send PPP echo packets</source>
        <translation>PPP ᠪᠤᠴᠠᠭᠠᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠢᠯᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="794"/>
        <source>Authentication Mode:</source>
        <translation>ᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="795"/>
        <source>PAP authentication</source>
        <translation>PAP ᠬᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="796"/>
        <source>CHAP authentication</source>
        <translation>CHAP ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="797"/>
        <source>MSCHAP authentication</source>
        <translation>MSCHAP ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="798"/>
        <source>MSCHAP2 authentication</source>
        <translation>MSCHAP2 ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="799"/>
        <source>EAP authentication</source>
        <translation>EAP ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="800"/>
        <source>Compression Mode:</source>
        <translation>ᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="801"/>
        <source>Allow BSD data compression</source>
        <translation>BSD ᠠᠪᠴᠢᠭᠤᠯᠤᠯ ᠢ᠋ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="802"/>
        <source>Allow Default data compression</source>
        <translation>Default ᠠᠪᠴᠢᠭᠤᠯᠤᠯ ᠢ᠋ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="803"/>
        <source>Allow TCP header compression</source>
        <translation>TCP ᠲᠤᠯᠤᠭᠠᠢ ᠠᠪᠴᠢᠭᠤᠯᠤᠯ ᠢ᠋ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="804"/>
        <source>Use protocol field compression negotiation</source>
        <translation>ᠭᠡᠷ᠎ᠡ ᠵᠢᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠵᠦᠪᠰᠢᠯᠴᠡᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="805"/>
        <source>Use Address/Control compression</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠠᠶᠢᠭ / ᠠᠪᠴᠢᠭᠤᠯᠤᠯ ᠢ᠋ ᠡᠵᠡᠮᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="811"/>
        <source>All Available</source>
        <translation>ᠶᠠᠮᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="812"/>
        <source>128-bit</source>
        <translation>128- ᠤᠷᠤᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="813"/>
        <source>40-bit</source>
        <translation>40- ᠤᠷᠤᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1010"/>
        <source>Use custom gateway port</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠨᠸᠲ ᠪᠤᠭᠤᠮᠳᠠ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1011"/>
        <source>Use compression</source>
        <translation>ᠠᠪᠴᠢᠭᠤᠯᠤᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1012"/>
        <source>Use a TCP connection</source>
        <translation>TCP ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1013"/>
        <source>Set virtual device type</source>
        <translation>ᠳᠠᠭᠤᠷᠢᠶᠠᠮᠠᠯ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1014"/>
        <source>Set virtual device name</source>
        <translation>ᠳᠠᠭᠤᠷᠢᠶᠠᠮᠠᠯ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1015"/>
        <source>Limit TCP Maximum Segment Size(MSS)</source>
        <translation>TCP ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ （MSS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1016"/>
        <source>Randomize remote hosts</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮᠴᠢᠯᠠᠭᠰᠠᠨ ᠠᠯᠤᠰ ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1017"/>
        <source>IPv6 tun link</source>
        <translation>IPv6 tun ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1018"/>
        <source>Specify ping interval</source>
        <translation>ping ᠤ᠋ᠨ/ ᠵᠢᠨ ᠦᠶᠡᠯᠡᠯ ᠢ᠋ ᠳᠤᠭᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1019"/>
        <source>Specify exit or restart ping</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠪᠤᠶᠤ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠰᠡᠨ ping ᠢ᠋/ ᠵᠢ ᠳᠤᠭᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1020"/>
        <source>Specify max routes</source>
        <translation>ᠵᠠᠮᠴᠢᠯᠠᠭᠤᠷ ᠤ᠋ᠨ ᠳᠡᠭᠡᠳᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠢ᠋ ᠳᠤᠭᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1021"/>
        <source>Infinite retry on error</source>
        <translation>ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠬᠢᠵᠠᠭᠠᠷ ᠦᠬᠡᠢ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1022"/>
        <source>Use custom key size</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1023"/>
        <source>Choose</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1026"/>
        <source>Use custom renegotiation interval</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠳᠠᠬᠢᠨ ᠵᠦᠪᠰᠢᠯᠴᠡᠬᠦ ᠵᠠᠭᠠᠭ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1028"/>
        <source>Use custom tunnel Maximum Transmission Umit(MTU)</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠰᠤᠪᠠᠭ ᠤ᠋ᠨ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠨᠢᠭᠡᠴᠢ ᠵᠢᠨ ᠳᠠᠮᠵᠢᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ （MTU）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1030"/>
        <source>Use custom UDP fragment size</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ UDP ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1032"/>
        <source>Accept authenticated packets from any address (Float)</source>
        <translation>ᠶᠠᠮᠠᠷᠪᠠ ᠬᠠᠶᠢᠭ (Float) ᠵᠢᠡᠷ ᠢᠷᠡᠭᠰᠡᠨ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠡᠶ᠎ᠡ ᠵᠢᠨ ᠭᠠᠷᠤᠯ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠠᠪᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1034"/>
        <source>Subject Match</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1035"/>
        <source>Key File</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1036"/>
        <source>Key Direction</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1038"/>
        <source>Server Address</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1039"/>
        <source>Port</source>
        <translation>ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1040"/>
        <source>Proxy USername</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1041"/>
        <source>Proxy Password</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1043"/>
        <source>General</source>
        <translation>ᠳᠦᠷᠢᠮᠵᠢᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1044"/>
        <source>TLS settings</source>
        <translation>TLS ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1045"/>
        <source>Server Certificate Check</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ ᠢ᠋ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1047"/>
        <source>Use the previous authentication end (server) certificate</source>
        <translation>ᠡᠮᠦᠨᠡᠬᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ ( ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ) ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1049"/>
        <source>Verify peer (server) certificate nsCertType specification</source>
        <translation>ᠳᠡᠩᠴᠡᠬᠦᠦ ᠴᠡᠭ ( ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ) ᠤ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ nsCertType ᠢ᠋/ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠵᠤ ᠳᠤᠭᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1050"/>
        <source>Mode</source>
        <translation>ᠮᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1051"/>
        <source>Proxies</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1052"/>
        <source>Proxy Type</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1053"/>
        <source>Security</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1054"/>
        <source>HMAC Authentication</source>
        <translation>HMAC ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1064"/>
        <source>Input content</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1066"/>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ/ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1070"/>
        <source>Self-adaption</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠵᠤᠬᠢᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1071"/>
        <source>Automatic</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1076"/>
        <source>Exit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1077"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1079"/>
        <source>Don&apos;t verify certificate identification</source>
        <translation>ᠦᠨᠡᠮᠯᠡᠯ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1080"/>
        <source>Verify the entire subject exactly</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠢ᠋ ᠪᠠᠳᠤᠳᠠᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1081"/>
        <source>Verify name exactly</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠤᠨᠤᠪᠴᠢᠳᠠᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1082"/>
        <source>Verify name by prefix</source>
        <translation>ᠤᠭᠳᠤᠪᠤᠷᠢ ᠪᠡᠷ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1084"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1087"/>
        <source>Server</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1085"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1088"/>
        <source>Client</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1090"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1094"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1103"/>
        <source>None</source>
        <translation>ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1091"/>
        <source>TLS-Certification</source>
        <translation>TLS- ᠭᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1092"/>
        <source>TLS-Encryption</source>
        <translation>TLS- ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1098"/>
        <source>Not Required</source>
        <translation>ᠱᠠᠭᠠᠷᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1102"/>
        <source>Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1176"/>
        <source>Options:</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1179"/>
        <source>Request an inner IP address</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ IP ᠬᠠᠶᠢᠭ ᠢ᠋ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1180"/>
        <source>Enforce UDP encapsulation</source>
        <translation>ᠠᠯᠪᠠ ᠪᠡᠷ UDP ᠪᠢᠳᠡᠬᠦᠮᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1181"/>
        <source>Use IP compression</source>
        <translation>IP ᠠᠪᠴᠢᠭᠤᠯᠤᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1182"/>
        <source>Enable custom password suggestions</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>VpnConfigPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="312"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="313"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="314"/>
        <source>Static Key</source>
        <translation>ᠳᠠᠢᠪᠤᠩ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="315"/>
        <source>Local IP</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="316"/>
        <source>Remote IP</source>
        <translation>ᠠᠯᠤᠰ ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="317"/>
        <source>PIN Code</source>
        <translation>PIN ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="318"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="790"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="797"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="319"/>
        <source>NT Domain</source>
        <translation>NT ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="321"/>
        <source>Server Address</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="322"/>
        <source>Authentication Mode</source>
        <translation>ᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="323"/>
        <source>CA Certificate</source>
        <translation>CA ᠦᠨᠡᠮᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="324"/>
        <source>User Certificate</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠨᠡᠮᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="325"/>
        <source>Key Direction</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="326"/>
        <source>Private Key</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="327"/>
        <source>Private Key Password</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="328"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="330"/>
        <source>Password Options</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="329"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="333"/>
        <source>Notice:
If key direction is used, it must be opposite to the VPN side used. If &apos;1&apos; is used, the connection must use &apos;0&apos;. If you are not sure which value to use, please contact your system administrator.</source>
        <translation>ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ:
ᠬᠡᠷᠪᠡ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠴᠢᠭᠯᠡᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠪᠤᠯ᠂ ᠲᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ VPN ᠦᠵᠦᠬᠦᠷ ᠲᠠᠢ ᠡᠰᠡᠷᠬᠦ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ ᠬᠡᠷᠪᠡ &apos;1&apos; ᠢ᠋/ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠪᠤᠯ᠂ ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ &apos;0&apos; ᠢ᠋/ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ ᠬᠡᠷᠪᠡ ᠠᠯᠢ ᠵᠢ ᠨᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠡᠨ ᠳᠤᠭᠳᠠᠵᠤ ᠴᠢᠳᠠᠭ᠎ᠠ ᠦᠬᠡᠢ ᠪᠤᠯ᠂ ᠲᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠲᠠᠢ ᠪᠡᠨ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="340"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="341"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="342"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="343"/>
        <source>Choose</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="350"/>
        <source>None</source>
        <translation>ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="354"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="360"/>
        <source>Save password only for this user</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="355"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="361"/>
        <source>Save password for all users</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="356"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="362"/>
        <source>Ask password every time</source>
        <translation>ᠤᠳᠠᠭ᠎ᠠ ᠪᠦᠷᠢ ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="357"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="363"/>
        <source>Don&apos;t require a password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠡᠷᠡᠭ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="366"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="367"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="795"/>
        <source>Certificate(TLS)</source>
        <translation>ᠦᠨᠡᠮᠯᠡᠯ (TLS)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="796"/>
        <source>Static key</source>
        <translation>ᠳᠠᠢᠪᠤᠩ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="798"/>
        <source>Password and certificate(TLS)</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠢᠬᠡᠳ ᠦᠨᠡᠮᠯᠡᠯ （TLS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="807"/>
        <source>Certificate/Private key</source>
        <translation>ᠦᠨᠡᠮᠯᠡᠯ / ᠬᠤᠪᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="808"/>
        <source>Certificate/ssh-agent</source>
        <translation>ᠦᠨᠡᠮᠯᠡᠯ /ssh-agent</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="809"/>
        <source>Smart card</source>
        <translation>ᠤᠶᠤᠯᠢᠭ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="864"/>
        <source>Choose a private key</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="866"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="905"/>
        <source>Key Files (*.key *.pem *.der *.p12 *.pfx)</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ (*.key *.pem *.der *.p12 *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="877"/>
        <source>Choose a CA certificate</source>
        <translation>CA ᠦᠨᠡᠮᠯᠡᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="879"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="892"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA ᠹᠠᠢᠯ (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="890"/>
        <source>Choose a User certificate</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠨᠡᠮᠯᠡᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="903"/>
        <source>Choose a Static key</source>
        <translation>ᠳᠠᠢᠪᠤᠩ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>VpnIpv4Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="152"/>
        <source>IPv4 Config</source>
        <translation>IPv4 ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="153"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="154"/>
        <source>Netmask</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠠᠯᠳᠠᠯᠠᠯ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="155"/>
        <source>Default Gateway</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠸᠲ ᠪᠤᠭᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="156"/>
        <source>DNS Server</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="157"/>
        <source>Search Domain</source>
        <translation>ᠬᠠᠢᠬᠤ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="158"/>
        <source>DHCP Client ID</source>
        <translation>DHCP ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ID</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="160"/>
        <source>Auto(DHCP)</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="161"/>
        <source>Manual</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠤᠰᠤ</translation>
    </message>
</context>
<context>
    <name>VpnIpv6Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="141"/>
        <source>IPv6 Config</source>
        <translation>IPv6 ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="142"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="143"/>
        <source>Netmask</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠠᠯᠳᠠᠯᠠᠯ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="144"/>
        <source>Default Gateway</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠸᠲ ᠪᠤᠭᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="145"/>
        <source>DNS Server</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="146"/>
        <source>Search Domain</source>
        <translation>ᠬᠠᠢᠬᠤ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="148"/>
        <source>Auto(DHCP)</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="149"/>
        <source>Manual</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠤᠰᠤ</translation>
    </message>
</context>
<context>
    <name>VpnListItem</name>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="61"/>
        <source>Not connected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="160"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="176"/>
        <source>Disconnect</source>
        <translation>ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="162"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="174"/>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>VpnPage</name>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="259"/>
        <source>VPN</source>
        <translation>vpn</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="270"/>
        <source>VPN Settings</source>
        <translation>VPN ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>vpnAddPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="15"/>
        <source>create VPN</source>
        <translation>VPN ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="47"/>
        <source>VPN Type</source>
        <translation>VPN ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="75"/>
        <source>VPN Name</source>
        <translation>VPN ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="81"/>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="103"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="98"/>
        <source>VPN Server</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="12"/>
        <source>VPN</source>
        <translation>vpn</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="41"/>
        <source>Auto Connection</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="53"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="56"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>vpnConfigPage</name>
    <message>
        <source>VPN Type</source>
        <translation type="vanished">VPN类型</translation>
    </message>
</context>
<context>
    <name>VpnDetail</name>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="61"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="145"/>
        <source>VPN</source>
        <translation>vpn</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="146"/>
        <source>IPv4</source>
        <translation>ipv4</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="148"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="225"/>
        <source>IPv6</source>
        <translation>ipv6</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="153"/>
        <source>Advanced</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="158"/>
        <source>Auto Connection</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="159"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="160"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>vpnObject</name>
    <message>
        <location filename="../frontend/vpnobject.cpp" line="31"/>
        <source>vpn tool</source>
        <translation>vpn ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
</context>
</TS>
