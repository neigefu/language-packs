<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>SinglePage</name>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="73"/>
        <source>Settings</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="121"/>
        <source>Kylin VPN</source>
        <translation>VPNཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="124"/>
        <source>kylin vpn applet desktop message</source>
        <translation>vpnཅོག་ངོས་ཀྱི་གསལ་འདེབས་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>VpnAdvancedPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="791"/>
        <source>MPPE encryption algorithm:</source>
        <translation>MPPEགསང་སྣོན་རྩིས་ཐབས།:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="792"/>
        <source>Use Stateful encryption</source>
        <translation>རྣམ་པ་ལྡན་པའི་སྒོ་ནས་ཚགས་དམ་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="793"/>
        <source>Send PPP echo packets</source>
        <translation>སྐྱེལ་སྤྲོད་བྱེད་དགོས།PPPཁུག་མ་ཕྱིར་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="794"/>
        <source>Authentication Mode:</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་སྟངས།:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="795"/>
        <source>PAP authentication</source>
        <translation>PAPདཔང་དངོས་བདེན་པ་ཡིན་པའི</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="796"/>
        <source>CHAP authentication</source>
        <translation>CHAPདཔང་དངོས་བདེན་པ་ཡིན་པའི</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="797"/>
        <source>MSCHAP authentication</source>
        <translation>MSCHAPདཔང་དངོས་བདེན་པ་ཡིན་པའི</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="798"/>
        <source>MSCHAP2 authentication</source>
        <translation>MSCHAP2དཔང་དངོས་བདེན་པ་ཡིན་པའི</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="799"/>
        <source>EAP authentication</source>
        <translation>EAPདཔང་དངོས་བདེན་པ་ཡིན་པའི</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="800"/>
        <source>Compression Mode:</source>
        <translation>ཉུང་འཕྲི་བྱེད་སྟངས།:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="801"/>
        <source>Allow BSD data compression</source>
        <translation>ཆོག་པ་བྱ་དགོས།BSDཉུང་འཕྲི་བཅས་བྱ་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="802"/>
        <source>Allow Default data compression</source>
        <translation>ཆོག་པ་བྱ་དགོས།Defaultཉུང་འཕྲི་བཅས་བྱ་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="803"/>
        <source>Allow TCP header compression</source>
        <translation>ཆོག་པ་བྱ་དགོས།TCPམགོ་བོ།ཉུང་འཕྲི་བཅས་བྱ་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="804"/>
        <source>Use protocol field compression negotiation</source>
        <translation>གྲོས་མཐུན་གྱི་ཁྱབ་ཁོངས་སྤྱད་དེ་གྲོས་མོལ་ཉུང་འཕྲ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="805"/>
        <source>Use Address/Control compression</source>
        <translation>ས་གནས་སྤྱོད་པ།/ཉུང་འཕྲི་ཚོད་འཛིན་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="811"/>
        <source>All Available</source>
        <translation>རང་སྣང་གང་དྲན་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="812"/>
        <source>128-bit</source>
        <translation>128གནས་ས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="813"/>
        <source>40-bit</source>
        <translation>40གནས་ས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1010"/>
        <source>Use custom gateway port</source>
        <translation>རང་ཉིད་ཀྱི་མཚན་ཉིད་འཇོག་པའི་དྲ་བའི་འགག་སྒོ་བེད་སྤྱོད་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1011"/>
        <source>Use compression</source>
        <translation>བཀོལ་སྤྱོད་ཉུང་འཕྲི་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1012"/>
        <source>Use a TCP connection</source>
        <translation>བེད་སྤྱོད།TCPའབྲེལ་མཐུད་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1013"/>
        <source>Set virtual device type</source>
        <translation>རྟོག་བཟོའི་སྒྲིག་ཆས་ཀྱི་རིགས་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1014"/>
        <source>Set virtual device name</source>
        <translation>རྟོག་བཟོའི་སྒྲིག་ཆས་ཀྱི་མིང་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1015"/>
        <source>Limit TCP Maximum Segment Size(MSS)</source>
        <translation>ཚོད་འཛིན།TCPཆེས་ཆེ་བའི་རིང་ཐུང་།(MSS)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1016"/>
        <source>Randomize remote hosts</source>
        <translation>སྐབས་བསྟུན་ཅན་གྱི་རྒྱང་ཁྲིད་འཕྲུལ་ཆས་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1017"/>
        <source>IPv6 tun link</source>
        <translation>IPv6 tunའབྲེལ་མཐུད་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1018"/>
        <source>Specify ping interval</source>
        <translation>དམིགས་འཛུགས་བྱས་པ།Pingདུས་འཁོར།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1019"/>
        <source>Specify exit or restart ping</source>
        <translation>ཕྱིར་འཐེན་ནམ་ཡང་ན་བསྐྱར་དུ་སྒོ་འབྱེད་རྒྱུའི་དམིགས་འཛུགས་བྱས་རིགས།Ping</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1020"/>
        <source>Specify max routes</source>
        <translation>དམིགས་འཛུགས་བྱེད་པའི་ལམ་དེ་གོང་ནས་ཚད་བཀག་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1021"/>
        <source>Infinite retry on error</source>
        <translation>ནོར་འཁྲུལ་བྱུང་དུས་ཚོད་ལྟ་ཚད་མེད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1022"/>
        <source>Use custom key size</source>
        <translation>རང་གི་མཚན་ཉིད་ཀྱི་གསང་བའི་ལྡེ་མིག་ཆེ་ཆུང་སྤྱོད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1023"/>
        <source>Choose</source>
        <translation>གདམ་ག་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1026"/>
        <source>Use custom renegotiation interval</source>
        <translation>རང་ཉིད་ཀྱི་མཚན་ཉིད་བཞག་ནས་གྲོས་མོལ་བྱེད་པའི་བར་མཚམས་འཇོག་དགོས</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1028"/>
        <source>Use custom tunnel Maximum Transmission Umit(MTU)</source>
        <translation>རང་ཉིད་ཀྱི་མཚན་ཉིད་བཞག་པའི་ཕུག་ལམ་གྱི་ཆེས་ཆེ་བའི་ཚན་པ་བརྒྱུད་གཏོང་བྱེད་པ།(MTU)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1030"/>
        <source>Use custom UDP fragment size</source>
        <translation>རང་ཉིད་ཀྱི་མཚན་ཉིད་སྤྱད་པ།UDPཆ་བགོས་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1032"/>
        <source>Accept authenticated packets from any address (Float)</source>
        <translation>ས་གནས་གང་ནས་ཡོང་བ་དང་ལེན་བྱེད་དགོས།(Float)ཐོབ་ཐང་ཚོད་ལྟསར་སྤྲོད་བྱས་ཟིན་པའི་གྲངས་གཞིའི་ཁུག་མ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1034"/>
        <source>Subject Match</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ་ཆ་འགྲིག་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1035"/>
        <source>Key File</source>
        <translation>གསང་ལྡེའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1036"/>
        <source>Key Direction</source>
        <translation>གསང་ལྡེའི་ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1038"/>
        <source>Server Address</source>
        <translation>ཞབས་ཞུ་ཡོ་བྱད་ཀྱི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1039"/>
        <source>Port</source>
        <translation>ཁ་བཤད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1040"/>
        <source>Proxy USername</source>
        <translation>བེད་སྤྱོད་བྱེད་མཁན་གྱི་ཚབ་ཏུ་མིང་།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1041"/>
        <source>Proxy Password</source>
        <translation>གསང་གྲངས་ཚབ་སྒྲུབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1043"/>
        <source>General</source>
        <translation>རྒྱུན་སྲོལ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1044"/>
        <source>TLS settings</source>
        <translation>TLSབཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1045"/>
        <source>Server Certificate Check</source>
        <translation>ཞབས་ཞུ་ཡོ་བྱད་དཔང་ཡིག་ལ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1047"/>
        <source>Use the previous authentication end (server) certificate</source>
        <translation>གོང་དུ་ཚོད་ལྟས་ར་སྤྲོད་བྱ་དགོས།(ཞབས་ཞུའི་ཡོ་བྱད།)དཔང་ཡིག</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1049"/>
        <source>Verify peer (server) certificate nsCertType specification</source>
        <translation>ཚོད་ལྟས་ར་སྤྲོད་བྱེད་པ་སོགས་ཡིན།(ཞབས་ཞུའི་ཡོ་བྱད།)དཔང་ཡིགnsCertTypeདམིགས་འཛུགས་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1050"/>
        <source>Mode</source>
        <translation>མ་དཔེ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1051"/>
        <source>Proxies</source>
        <translation>ངོ་ཚབ་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1052"/>
        <source>Proxy Type</source>
        <translation>ངོ་ཚབ་ཀྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1053"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1054"/>
        <source>HMAC Authentication</source>
        <translation>HMACདཔང་དངོས་བདེན་པ་ཡིན་པའི</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1064"/>
        <source>Input content</source>
        <translation>ནང་དོན་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1066"/>
        <source>No</source>
        <translation>དེ་ལྟར་མ་བྱས་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1070"/>
        <source>Self-adaption</source>
        <translation>རང་གིས་རང་ལ་འཕྲོད་པར</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1071"/>
        <source>Automatic</source>
        <translation>རང་འགུལ་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1076"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1077"/>
        <source>Restart</source>
        <translation>བསྐྱར་དུ་སྒོ་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1079"/>
        <source>Don&apos;t verify certificate identification</source>
        <translation>དཔང་ཡིག་གི་མཚོན་རྟགས་ལ་ཚོད་ལྟས་ར་སྤྲོད་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1080"/>
        <source>Verify the entire subject exactly</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ་ཡོངས་རྫོགས་ལ་ཚོད་ལྟས་ར་སྤྲོད་གསལ་པོ་བྱས་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1081"/>
        <source>Verify name exactly</source>
        <translation>ཞིབ་ཅིང་དག་པའི་སྒོ་ནས་ར་སྤྲོད་བྱས་པའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1082"/>
        <source>Verify name by prefix</source>
        <translation>སྔོན་གྱི་缀ཚོད་ལྟས་ར་སྤྲོད་ཀྱི་མིང་ལྟར།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1084"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1087"/>
        <source>Server</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1085"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1088"/>
        <source>Client</source>
        <translation>ཚོང་འགྲུལ་པ་སྣེ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1090"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1094"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1103"/>
        <source>None</source>
        <translation>མེད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1091"/>
        <source>TLS-Certification</source>
        <translation>TLS-དཔང་དངོས་བདེན་པ་ཡིན་པའི</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1092"/>
        <source>TLS-Encryption</source>
        <translation>TLS-ཚགས་དམ་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1098"/>
        <source>Not Required</source>
        <translation>དགོས་མཁོ་མེད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1102"/>
        <source>Default</source>
        <translation>ཁ་རོག་གེར་ཁས་ལེན</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1176"/>
        <source>Options:</source>
        <translation>རྣམ་གྲངས་འདེམས་པ།:</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1179"/>
        <source>Request an inner IP address</source>
        <translation>ནང་ཁུལ་ལ་རེ་ཞུ་བྱེད་པ།IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1180"/>
        <source>Enforce UDP encapsulation</source>
        <translation>བཙན་ཤེད་ཀྱིས་བཙན་ཤེད་བྱེདUDPཐུམ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1181"/>
        <source>Use IP compression</source>
        <translation>བེད་སྤྱོད།IPཉུང་འཕྲི་བཅས་བྱ་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1182"/>
        <source>Enable custom password suggestions</source>
        <translation>རང་ཉིད་ཀྱི་མཚན་ཉིད་གསང་བའི་གྲོས་འགོ་འདོན་དགོས།</translation>
    </message>
</context>
<context>
    <name>VpnConfigPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="312"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="313"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="314"/>
        <source>Static Key</source>
        <translation>ཁ་རོག་གེར་ལྡེ་མིག་ལྟ་བ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="315"/>
        <source>Local IP</source>
        <translation>ས་གནས་དེ་ག་རེད།IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="316"/>
        <source>Remote IP</source>
        <translation>རྒྱང་རིང་།IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="317"/>
        <source>PIN Code</source>
        <translation>PINཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="318"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="790"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="797"/>
        <source>Password</source>
        <translation>གསང་བའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="319"/>
        <source>NT Domain</source>
        <translation>NTས་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="321"/>
        <source>Server Address</source>
        <translation>ཞབས་ཞུ་ཡོ་བྱད་ཀྱི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="322"/>
        <source>Authentication Mode</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="323"/>
        <source>CA Certificate</source>
        <translation>CAདཔང་ཡིག</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="324"/>
        <source>User Certificate</source>
        <translation>སྤྱོད་མཁན།དཔང་ཡིག</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="325"/>
        <source>Key Direction</source>
        <translation>གསང་ལྡེའི་ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="326"/>
        <source>Private Key</source>
        <translation>སྒེར་གྱི་ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="327"/>
        <source>Private Key Password</source>
        <translation>སྒེར་ལ་གསང་བའི་གསང་བ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="328"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="330"/>
        <source>Password Options</source>
        <translation>གསང་བའི་ཐོག་ནས་རྣམ་གྲངས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="329"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="333"/>
        <source>Notice:
If key direction is used, it must be opposite to the VPN side used. If &apos;1&apos; is used, the connection must use &apos;0&apos;. If you are not sure which value to use, please contact your system administrator.</source>
        <translation>ཡིད་འཇོག་བྱ་དགོས་
གལ་ཏེ་གསང་བའི་ཁ་ཕྱོགས་སྤྱད་ན་ངེས་པར་དུ་VPNཡི་སྣེ་དང་ལྡོག་དགོས། གལ་ཏེ་&quot;1&quot;བེད་སྤྱོད་བྱས་ན་ངེས་པར་དུ་&quot;0&quot;བེད་སྤྱོད་གཏོང་དགོས། གལ་ཏེ་རིན་ཐང་གང་ཡིན་པ་མི་ཤེས་ན། ཁྱེད་ཀྱི་མ་ལག་དོ་དམ་པ་ལ་འབྲེལ་གཏུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="340"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="341"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="342"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="343"/>
        <source>Choose</source>
        <translation>གདམ་ག་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="350"/>
        <source>None</source>
        <translation>མེད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="354"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="360"/>
        <source>Save password only for this user</source>
        <translation>མིག་སྔར་སྤྱོད་མཁན་ཁོ་ནར་གསང་གྲངས་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="355"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="361"/>
        <source>Save password for all users</source>
        <translation>སྤྱོད་མཁན་ཚང་མར་གསང་གྲངས་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="356"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="362"/>
        <source>Ask password every time</source>
        <translation>ཚང་མར་འདྲི་རྩད་བྱེད་ཐེངས་རེ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="357"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="363"/>
        <source>Don&apos;t require a password</source>
        <translation>གསང་བ་མི་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="366"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="367"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="795"/>
        <source>Certificate(TLS)</source>
        <translation>དཔང་ཡིག(TLS)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="796"/>
        <source>Static key</source>
        <translation>ཁ་རོག་གེར་ལྡེ་མིག་ལྟ་བ</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="798"/>
        <source>Password and certificate(TLS)</source>
        <translation>གསང་བ་དང་དཔང་ཡིག(TLS)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="807"/>
        <source>Certificate/Private key</source>
        <translation>དཔང་ཡིག/སྒེར་གྱི་ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="808"/>
        <source>Certificate/ssh-agent</source>
        <translation>དཔང་ཡིག/ssh-agent</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="809"/>
        <source>Smart card</source>
        <translation>རིག་ནུས་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="864"/>
        <source>Choose a private key</source>
        <translation>སྒེར་གྱི་ལྡེ་མིག་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="866"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="905"/>
        <source>Key Files (*.key *.pem *.der *.p12 *.pfx)</source>
        <translation>སྒེར་གྱི་ལྡེ་མིག་ཡིག་ཆ།(*.key *.pem *.der *.p12 *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="877"/>
        <source>Choose a CA certificate</source>
        <translation>གདམ་ག་རྒྱག་པ།CAདཔང་ཡིག</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="879"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="892"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CAཡིག་ཆ། (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="890"/>
        <source>Choose a User certificate</source>
        <translation>སྤྱོད་མཁན་གྱི་ལག་ཁྱེར་འདེམས་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="903"/>
        <source>Choose a Static key</source>
        <translation>འཇམ་ཐིང་ཐིང་གི་ལྡེ་མིག་འདེམས་</translation>
    </message>
</context>
<context>
    <name>VpnIpv4Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="152"/>
        <source>IPv4 Config</source>
        <translation>IPv4བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="153"/>
        <source>Address</source>
        <translation>ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="154"/>
        <source>Netmask</source>
        <translation>དྲ་བ་འགེབས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="155"/>
        <source>Default Gateway</source>
        <translation>དྲ་རྒྱའི་འགག་སྒོ་ཁས་ལེན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="156"/>
        <source>DNS Server</source>
        <translation>DNSཞབས་ཞུའི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="157"/>
        <source>Search Domain</source>
        <translation>ས་ཁོངས་འཚོལ་ཞིབ་བྱ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="158"/>
        <source>DHCP Client ID</source>
        <translation>DHCPཚོང་འགྲུལ་པ་སྣེ་ལེན་བྱེད་པ།ID</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="160"/>
        <source>Auto(DHCP)</source>
        <translation>རང་འགུལ་ཡིན་དགོས།(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="161"/>
        <source>Manual</source>
        <translation>ལག་པ་འགུལ་དགོས།</translation>
    </message>
</context>
<context>
    <name>VpnIpv6Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="141"/>
        <source>IPv6 Config</source>
        <translation>IPv6བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="142"/>
        <source>Address</source>
        <translation>ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="143"/>
        <source>Netmask</source>
        <translation>དྲ་བ་འགེབས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="144"/>
        <source>Default Gateway</source>
        <translation>དྲ་རྒྱའི་འགག་སྒོ་ཁས་ལེན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="145"/>
        <source>DNS Server</source>
        <translation>DNSཞབས་ཞུའི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="146"/>
        <source>Search Domain</source>
        <translation>ས་ཁོངས་འཚོལ་ཞིབ་བྱ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="148"/>
        <source>Auto(DHCP)</source>
        <translation>རང་འགུལ་ཡིན་དགོས།(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="149"/>
        <source>Manual</source>
        <translation>ལག་པ་འགུལ་དགོས།</translation>
    </message>
</context>
<context>
    <name>VpnListItem</name>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="61"/>
        <source>Not connected</source>
        <translation>འབྲེལ་མཐུད་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="160"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="176"/>
        <source>Disconnect</source>
        <translation>བར་མཚམས་ཆད་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="162"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="174"/>
        <source>Connect</source>
        <translation>འབྲེལ་མཐུད་བཅས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>VpnPage</name>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="259"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="270"/>
        <source>VPN Settings</source>
        <translation>VPNབཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>vpnAddPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="15"/>
        <source>create VPN</source>
        <translation>གསར་འཛུགས་བྱ་དགོས།VPN</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="47"/>
        <source>VPN Type</source>
        <translation>VPNརིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="75"/>
        <source>VPN Name</source>
        <translation>VPNམིང་།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="81"/>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="103"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་སྐོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="98"/>
        <source>VPN Server</source>
        <translation>ཞབས་ཞུ་ཡོ་བྱད་ཀྱི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="12"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="41"/>
        <source>Auto Connection</source>
        <translation>རང་འགུལ་གྱིས་སྦྲེལ་མཐུད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="53"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="56"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
</context>
<context>
    <name>vpnConfigPage</name>
    <message>
        <source>VPN Type</source>
        <translation type="vanished">VPN类型</translation>
    </message>
</context>
<context>
    <name>VpnDetail</name>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="61"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="145"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="146"/>
        <source>IPv4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="148"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="225"/>
        <source>IPv6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="153"/>
        <source>Advanced</source>
        <translation>མཐོ་རིམ།མཐོ་རིམ་</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="158"/>
        <source>Auto Connection</source>
        <translation>རང་འགུལ་གྱིས་སྦྲེལ་མཐུད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="159"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="160"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ།</translation>
    </message>
</context>
<context>
    <name>vpnObject</name>
    <message>
        <location filename="../frontend/vpnobject.cpp" line="31"/>
        <source>vpn tool</source>
        <translation>VPNཡོ་བྱད།</translation>
    </message>
</context>
</TS>
