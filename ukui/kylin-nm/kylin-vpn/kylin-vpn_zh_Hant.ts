<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>SinglePage</name>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="73"/>
        <source>Settings</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="121"/>
        <source>Kylin VPN</source>
        <translation>VPN 工具</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="124"/>
        <source>kylin vpn applet desktop message</source>
        <translation>VPN 設定桌面提示</translation>
    </message>
</context>
<context>
    <name>VpnAdvancedPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="791"/>
        <source>MPPE encryption algorithm:</source>
        <translation>MPPE 加密演演算法：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="792"/>
        <source>Use Stateful encryption</source>
        <translation>使用有狀態加密</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="793"/>
        <source>Send PPP echo packets</source>
        <translation>發送 PPP 回顯包</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="794"/>
        <source>Authentication Mode:</source>
        <translation>認證方式：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="795"/>
        <source>PAP authentication</source>
        <translation>PAP 認證</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="796"/>
        <source>CHAP authentication</source>
        <translation>CHAP 認證</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="797"/>
        <source>MSCHAP authentication</source>
        <translation>MSCHAP 認證</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="798"/>
        <source>MSCHAP2 authentication</source>
        <translation>MSCHAP2 認證</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="799"/>
        <source>EAP authentication</source>
        <translation>EAP 認證</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="800"/>
        <source>Compression Mode:</source>
        <translation>壓縮方式：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="801"/>
        <source>Allow BSD data compression</source>
        <translation>允許 BSD 壓縮</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="802"/>
        <source>Allow Default data compression</source>
        <translation>允許 Default 壓縮</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="803"/>
        <source>Allow TCP header compression</source>
        <translation>允許 TCP 頭壓縮</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="804"/>
        <source>Use protocol field compression negotiation</source>
        <translation>使用協定域壓縮協商</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="805"/>
        <source>Use Address/Control compression</source>
        <translation>使用位址/控制壓縮</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="811"/>
        <source>All Available</source>
        <translation>任意</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="812"/>
        <source>128-bit</source>
        <translation>128位</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="813"/>
        <source>40-bit</source>
        <translation>40位</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1010"/>
        <source>Use custom gateway port</source>
        <translation>使用自定義閘道埠</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1011"/>
        <source>Use compression</source>
        <translation>使用壓縮</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1012"/>
        <source>Use a TCP connection</source>
        <translation>使用 TCP 連接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1013"/>
        <source>Set virtual device type</source>
        <translation>設置虛擬設備類型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1014"/>
        <source>Set virtual device name</source>
        <translation>設置虛擬設備名稱</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1015"/>
        <source>Limit TCP Maximum Segment Size(MSS)</source>
        <translation>限制 TCP 最大段尺寸（MSS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1016"/>
        <source>Randomize remote hosts</source>
        <translation>隨機化遠端主機</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1017"/>
        <source>IPv6 tun link</source>
        <translation>IPv6 tun 連接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1018"/>
        <source>Specify ping interval</source>
        <translation>指定 Ping 週期</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1019"/>
        <source>Specify exit or restart ping</source>
        <translation>指定退出或重啟的 Ping</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1020"/>
        <source>Specify max routes</source>
        <translation>指定路由上限</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1021"/>
        <source>Infinite retry on error</source>
        <translation>出錯時無限重試</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1022"/>
        <source>Use custom key size</source>
        <translation>使用自訂金鑰大小</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1023"/>
        <source>Choose</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1026"/>
        <source>Use custom renegotiation interval</source>
        <translation>使用自定義重協商間隔</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1028"/>
        <source>Use custom tunnel Maximum Transmission Umit(MTU)</source>
        <translation>使用自訂隧道最大單元傳輸（MTU）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1030"/>
        <source>Use custom UDP fragment size</source>
        <translation>使用自訂 UDP 分片大小</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1032"/>
        <source>Accept authenticated packets from any address (Float)</source>
        <translation>接受來自任何位址（Float）已通過身份驗證的數據包</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1034"/>
        <source>Subject Match</source>
        <translation>主題匹配</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1035"/>
        <source>Key File</source>
        <translation>金鑰檔案</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1036"/>
        <source>Key Direction</source>
        <translation>金鑰方向</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1038"/>
        <source>Server Address</source>
        <translation>伺服器位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1039"/>
        <source>Port</source>
        <translation>埠</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1040"/>
        <source>Proxy USername</source>
        <translation>代理使用者名</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1041"/>
        <source>Proxy Password</source>
        <translation>代理密碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1043"/>
        <source>General</source>
        <translation>常規</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1044"/>
        <source>TLS settings</source>
        <translation>TLS 設置</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1045"/>
        <source>Server Certificate Check</source>
        <translation>伺服器證書檢驗</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1047"/>
        <source>Use the previous authentication end (server) certificate</source>
        <translation>使用前面驗證端（伺服器）證書</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1049"/>
        <source>Verify peer (server) certificate nsCertType specification</source>
        <translation>驗證對等點（伺服器）證書 nsCertType 指定</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1050"/>
        <source>Mode</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1051"/>
        <source>Proxies</source>
        <translation>代理</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1052"/>
        <source>Proxy Type</source>
        <translation>代理類型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1053"/>
        <source>Security</source>
        <translation>安全</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1054"/>
        <source>HMAC Authentication</source>
        <translation>HMAC 認證</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1064"/>
        <source>Input content</source>
        <translation>輸入內容</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1066"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1070"/>
        <source>Self-adaption</source>
        <translation>自適應</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1071"/>
        <source>Automatic</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1076"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1077"/>
        <source>Restart</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1079"/>
        <source>Don&apos;t verify certificate identification</source>
        <translation>不驗證證書標識</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1080"/>
        <source>Verify the entire subject exactly</source>
        <translation>確切地驗證整個主題</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1081"/>
        <source>Verify name exactly</source>
        <translation>精確驗證名稱</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1082"/>
        <source>Verify name by prefix</source>
        <translation>按前綴驗證名稱</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1084"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1087"/>
        <source>Server</source>
        <translation>伺服器</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1085"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1088"/>
        <source>Client</source>
        <translation>用戶端</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1090"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1094"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1103"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1091"/>
        <source>TLS-Certification</source>
        <translation>TLS-認證</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1092"/>
        <source>TLS-Encryption</source>
        <translation>TLS-加密</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1098"/>
        <source>Not Required</source>
        <translation>不需要</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1102"/>
        <source>Default</source>
        <translation>預設</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1176"/>
        <source>Options:</source>
        <translation>選項：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1179"/>
        <source>Request an inner IP address</source>
        <translation>請求內部IP位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1180"/>
        <source>Enforce UDP encapsulation</source>
        <translation>強制 UDP 封裝</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1181"/>
        <source>Use IP compression</source>
        <translation>使用IP壓縮</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1182"/>
        <source>Enable custom password suggestions</source>
        <translation>啟用自訂密碼建議</translation>
    </message>
</context>
<context>
    <name>VpnConfigPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="312"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="313"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="314"/>
        <source>Static Key</source>
        <translation>靜態金鑰</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="315"/>
        <source>Local IP</source>
        <translation>本地IP位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="316"/>
        <source>Remote IP</source>
        <translation>遠端IP位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="317"/>
        <source>PIN Code</source>
        <translation>PIN 碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="318"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="790"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="797"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="319"/>
        <source>NT Domain</source>
        <translation>NT域</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="321"/>
        <source>Server Address</source>
        <translation>伺服器位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="322"/>
        <source>Authentication Mode</source>
        <translation>認證方式</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="323"/>
        <source>CA Certificate</source>
        <translation>CA 證書</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="324"/>
        <source>User Certificate</source>
        <translation>用戶證書</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="325"/>
        <source>Key Direction</source>
        <translation>金鑰方向</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="326"/>
        <source>Private Key</source>
        <translation>私鑰</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="327"/>
        <source>Private Key Password</source>
        <translation>私有金鑰密碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="328"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="330"/>
        <source>Password Options</source>
        <translation>密碼選項</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="329"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="333"/>
        <source>Notice:
If key direction is used, it must be opposite to the VPN side used. If &apos;1&apos; is used, the connection must use &apos;0&apos;. If you are not sure which value to use, please contact your system administrator.</source>
        <translation>注意：
如果使用了金鑰方向，它必須和使用的VPN端相反。 如果使用了“1”，連接必須要使用“0”。 如果不確定使用哪個值，請聯繫您的系統管理員。</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="340"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="341"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="342"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="343"/>
        <source>Choose</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="350"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="354"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="360"/>
        <source>Save password only for this user</source>
        <translation>僅對當前使用者保存密碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="355"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="361"/>
        <source>Save password for all users</source>
        <translation>為所有使用者保存密碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="356"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="362"/>
        <source>Ask password every time</source>
        <translation>每次都詢問</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="357"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="363"/>
        <source>Don&apos;t require a password</source>
        <translation>不需要密碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="366"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="367"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="795"/>
        <source>Certificate(TLS)</source>
        <translation>憑證（TLS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="796"/>
        <source>Static key</source>
        <translation>靜態金鑰</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="798"/>
        <source>Password and certificate(TLS)</source>
        <translation>密碼與憑證（TLS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="807"/>
        <source>Certificate/Private key</source>
        <translation>證書/私鑰</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="808"/>
        <source>Certificate/ssh-agent</source>
        <translation>證書/ ssh-agent</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="809"/>
        <source>Smart card</source>
        <translation>智慧卡</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="864"/>
        <source>Choose a private key</source>
        <translation>選擇私鑰</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="866"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="905"/>
        <source>Key Files (*.key *.pem *.der *.p12 *.pfx)</source>
        <translation>私鑰檔 （*.key *.pem *.der *.p12.pfx）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="877"/>
        <source>Choose a CA certificate</source>
        <translation>選擇 CA 證書</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="879"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="892"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA檔 （*.pem *.der *.p12 *.crt *.cer *.pfx）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="890"/>
        <source>Choose a User certificate</source>
        <translation>選擇用戶證書</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="903"/>
        <source>Choose a Static key</source>
        <translation>選擇靜態金鑰</translation>
    </message>
</context>
<context>
    <name>VpnIpv4Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="152"/>
        <source>IPv4 Config</source>
        <translation>IPv4 配置</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="153"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="154"/>
        <source>Netmask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="155"/>
        <source>Default Gateway</source>
        <translation>默認閘道</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="156"/>
        <source>DNS Server</source>
        <translation>DNS 伺服器</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="157"/>
        <source>Search Domain</source>
        <translation>搜索域</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="158"/>
        <source>DHCP Client ID</source>
        <translation>DHCP 用戶端 ID</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="160"/>
        <source>Auto(DHCP)</source>
        <translation>自動 （DHCP）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="161"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
</context>
<context>
    <name>VpnIpv6Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="141"/>
        <source>IPv6 Config</source>
        <translation>IPv6 配置</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="142"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="143"/>
        <source>Netmask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="144"/>
        <source>Default Gateway</source>
        <translation>默認閘道</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="145"/>
        <source>DNS Server</source>
        <translation>DNS 伺服器</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="146"/>
        <source>Search Domain</source>
        <translation>搜索域</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="148"/>
        <source>Auto(DHCP)</source>
        <translation>自動 （DHCP）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="149"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
</context>
<context>
    <name>VpnListItem</name>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="61"/>
        <source>Not connected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="160"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="176"/>
        <source>Disconnect</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="162"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="174"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
</context>
<context>
    <name>VpnPage</name>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="259"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="270"/>
        <source>VPN Settings</source>
        <translation>VPN 設置</translation>
    </message>
</context>
<context>
    <name>vpnAddPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="15"/>
        <source>create VPN</source>
        <translation>創建 VPN</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="47"/>
        <source>VPN Type</source>
        <translation>VPN 類型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="75"/>
        <source>VPN Name</source>
        <translation>VPN 名稱</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="81"/>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="103"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="98"/>
        <source>VPN Server</source>
        <translation>伺服器位址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="12"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="41"/>
        <source>Auto Connection</source>
        <translation>自動連接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="53"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="56"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>vpnConfigPage</name>
    <message>
        <source>VPN Type</source>
        <translation type="vanished">VPN类型</translation>
    </message>
</context>
<context>
    <name>VpnDetail</name>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="61"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="145"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="146"/>
        <source>IPv4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="148"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="225"/>
        <source>IPv6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="153"/>
        <source>Advanced</source>
        <translation>高級</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="158"/>
        <source>Auto Connection</source>
        <translation>自動連接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="159"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="160"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>vpnObject</name>
    <message>
        <location filename="../frontend/vpnobject.cpp" line="31"/>
        <source>vpn tool</source>
        <translation>VPN 工具</translation>
    </message>
</context>
</TS>
