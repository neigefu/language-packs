<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>SinglePage</name>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="73"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="121"/>
        <source>Kylin VPN</source>
        <translation>VPN 工具</translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/singlepage.cpp" line="124"/>
        <source>kylin vpn applet desktop message</source>
        <translation>VPN 配置桌面提示</translation>
    </message>
</context>
<context>
    <name>VpnAdvancedPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="791"/>
        <source>MPPE encryption algorithm:</source>
        <translation>MPPE 加密算法：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="792"/>
        <source>Use Stateful encryption</source>
        <translation>使用有状态加密</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="793"/>
        <source>Send PPP echo packets</source>
        <translation>发送 PPP 回显包</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="794"/>
        <source>Authentication Mode:</source>
        <translation>认证方式：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="795"/>
        <source>PAP authentication</source>
        <translation>PAP 认证</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="796"/>
        <source>CHAP authentication</source>
        <translation>CHAP 认证</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="797"/>
        <source>MSCHAP authentication</source>
        <translation>MSCHAP 认证</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="798"/>
        <source>MSCHAP2 authentication</source>
        <translation>MSCHAP2 认证</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="799"/>
        <source>EAP authentication</source>
        <translation>EAP 认证</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="800"/>
        <source>Compression Mode:</source>
        <translation>压缩方式：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="801"/>
        <source>Allow BSD data compression</source>
        <translation>允许 BSD 压缩</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="802"/>
        <source>Allow Default data compression</source>
        <translation>允许 Default 压缩</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="803"/>
        <source>Allow TCP header compression</source>
        <translation>允许 TCP 头压缩</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="804"/>
        <source>Use protocol field compression negotiation</source>
        <translation>使用协议域压缩协商</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="805"/>
        <source>Use Address/Control compression</source>
        <translation>使用地址/控制压缩</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="811"/>
        <source>All Available</source>
        <translation>任意</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="812"/>
        <source>128-bit</source>
        <translation>128位</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="813"/>
        <source>40-bit</source>
        <translation>40位</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1010"/>
        <source>Use custom gateway port</source>
        <translation>使用自定义网关端口</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1011"/>
        <source>Use compression</source>
        <translation>使用压缩</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1012"/>
        <source>Use a TCP connection</source>
        <translation>使用 TCP 连接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1013"/>
        <source>Set virtual device type</source>
        <translation>设置虚拟设备类型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1014"/>
        <source>Set virtual device name</source>
        <translation>设置虚拟设备名称</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1015"/>
        <source>Limit TCP Maximum Segment Size(MSS)</source>
        <translation>限制 TCP 最大段尺寸（MSS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1016"/>
        <source>Randomize remote hosts</source>
        <translation>随机化远程主机</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1017"/>
        <source>IPv6 tun link</source>
        <translation>IPv6 tun 连接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1018"/>
        <source>Specify ping interval</source>
        <translation>指定 Ping 周期</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1019"/>
        <source>Specify exit or restart ping</source>
        <translation>指定退出或重启的 Ping</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1020"/>
        <source>Specify max routes</source>
        <translation>指定路由上限</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1021"/>
        <source>Infinite retry on error</source>
        <translation>出错时无限重试</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1022"/>
        <source>Use custom key size</source>
        <translation>使用自定义密钥大小</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1023"/>
        <source>Choose</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1026"/>
        <source>Use custom renegotiation interval</source>
        <translation>使用自定义重协商间隔</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1028"/>
        <source>Use custom tunnel Maximum Transmission Umit(MTU)</source>
        <translation>使用自定义隧道最大单元传输（MTU）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1030"/>
        <source>Use custom UDP fragment size</source>
        <translation>使用自定义 UDP 分片大小</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1032"/>
        <source>Accept authenticated packets from any address (Float)</source>
        <translation>接受来自任何地址（Float）已通过身份验证的数据包</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1034"/>
        <source>Subject Match</source>
        <translation>主题匹配</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1035"/>
        <source>Key File</source>
        <translation>密钥文件</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1036"/>
        <source>Key Direction</source>
        <translation>密钥方向</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1038"/>
        <source>Server Address</source>
        <translation>服务器地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1039"/>
        <source>Port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1040"/>
        <source>Proxy USername</source>
        <translation>代理用户名</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1041"/>
        <source>Proxy Password</source>
        <translation>代理密码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1043"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1044"/>
        <source>TLS settings</source>
        <translation>TLS 设置</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1045"/>
        <source>Server Certificate Check</source>
        <translation>服务器证书检验</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1047"/>
        <source>Use the previous authentication end (server) certificate</source>
        <translation>使用前面验证端（服务器）证书</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1049"/>
        <source>Verify peer (server) certificate nsCertType specification</source>
        <translation>验证对等点（服务器）证书 nsCertType 指定</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1050"/>
        <source>Mode</source>
        <translation>模式</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1051"/>
        <source>Proxies</source>
        <translation>代理</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1052"/>
        <source>Proxy Type</source>
        <translation>代理类型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1053"/>
        <source>Security</source>
        <translation>安全</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1054"/>
        <source>HMAC Authentication</source>
        <translation>HMAC 认证</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1064"/>
        <source>Input content</source>
        <translation>输入内容</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1066"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1070"/>
        <source>Self-adaption</source>
        <translation>自适应</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1071"/>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1076"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1077"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1079"/>
        <source>Don&apos;t verify certificate identification</source>
        <translation>不验证证书标识</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1080"/>
        <source>Verify the entire subject exactly</source>
        <translation>确切地验证整个主题</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1081"/>
        <source>Verify name exactly</source>
        <translation>精确验证名称</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1082"/>
        <source>Verify name by prefix</source>
        <translation>按前缀验证名称</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1084"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1087"/>
        <source>Server</source>
        <translation>服务器</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1085"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1088"/>
        <source>Client</source>
        <translation>客户端</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1090"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1094"/>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1103"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1091"/>
        <source>TLS-Certification</source>
        <translation>TLS-认证</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1092"/>
        <source>TLS-Encryption</source>
        <translation>TLS-加密</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1098"/>
        <source>Not Required</source>
        <translation>不需要</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1102"/>
        <source>Default</source>
        <translation>默认</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1176"/>
        <source>Options:</source>
        <translation>选项：</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1179"/>
        <source>Request an inner IP address</source>
        <translation>请求内部 IP 地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1180"/>
        <source>Enforce UDP encapsulation</source>
        <translation>强制 UDP 封装</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1181"/>
        <source>Use IP compression</source>
        <translation>使用 IP 压缩</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnadvancedpage.cpp" line="1182"/>
        <source>Enable custom password suggestions</source>
        <translation>启用自定义密码建议</translation>
    </message>
</context>
<context>
    <name>VpnConfigPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="312"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="313"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="314"/>
        <source>Static Key</source>
        <translation>静态密钥</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="315"/>
        <source>Local IP</source>
        <translation>本地 IP 地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="316"/>
        <source>Remote IP</source>
        <translation>远程 IP 地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="317"/>
        <source>PIN Code</source>
        <translation>PIN 码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="318"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="790"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="797"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="319"/>
        <source>NT Domain</source>
        <translation>NT 域</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="321"/>
        <source>Server Address</source>
        <translation>服务器地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="322"/>
        <source>Authentication Mode</source>
        <translation>认证方式</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="323"/>
        <source>CA Certificate</source>
        <translation>CA 证书</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="324"/>
        <source>User Certificate</source>
        <translation>用户证书</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="325"/>
        <source>Key Direction</source>
        <translation>密钥方向</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="326"/>
        <source>Private Key</source>
        <translation>私钥</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="327"/>
        <source>Private Key Password</source>
        <translation>私有密钥密码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="328"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="330"/>
        <source>Password Options</source>
        <translation>密码选项</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="329"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="333"/>
        <source>Notice:
If key direction is used, it must be opposite to the VPN side used. If &apos;1&apos; is used, the connection must use &apos;0&apos;. If you are not sure which value to use, please contact your system administrator.</source>
        <translation>注意：
如果使用了密钥方向，它必须和使用的VPN端相反。如果使用了“1”，连接必须要使用“0”。如果不确定使用哪个值，请联系您的系统管理员。</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="340"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="341"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="342"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="343"/>
        <source>Choose</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="350"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="354"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="360"/>
        <source>Save password only for this user</source>
        <translation>仅对当前用户保存密码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="355"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="361"/>
        <source>Save password for all users</source>
        <translation>为所有用户保存密码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="356"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="362"/>
        <source>Ask password every time</source>
        <translation>每次都询问</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="357"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="363"/>
        <source>Don&apos;t require a password</source>
        <translation>不需要密码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="366"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="367"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="795"/>
        <source>Certificate(TLS)</source>
        <translation>证书（TLS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="796"/>
        <source>Static key</source>
        <translation>静态密钥</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="798"/>
        <source>Password and certificate(TLS)</source>
        <translation>密码和证书（TLS）</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="807"/>
        <source>Certificate/Private key</source>
        <translation>证书/私钥</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="808"/>
        <source>Certificate/ssh-agent</source>
        <translation>证书/ ssh-agent</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="809"/>
        <source>Smart card</source>
        <translation>智能卡</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="864"/>
        <source>Choose a private key</source>
        <translation>选择私钥</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="866"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="905"/>
        <source>Key Files (*.key *.pem *.der *.p12 *.pfx)</source>
        <translation>私钥文件 (*.key *.pem *.der *.p12 *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="877"/>
        <source>Choose a CA certificate</source>
        <translation>选择 CA 证书</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="879"/>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="892"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA文件 (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="890"/>
        <source>Choose a User certificate</source>
        <translation>选择用户证书</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnconfigpage.cpp" line="903"/>
        <source>Choose a Static key</source>
        <translation>选择静态密钥</translation>
    </message>
</context>
<context>
    <name>VpnIpv4Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="152"/>
        <source>IPv4 Config</source>
        <translation>IPv4 配置</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="153"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="154"/>
        <source>Netmask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="155"/>
        <source>Default Gateway</source>
        <translation>默认网关</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="156"/>
        <source>DNS Server</source>
        <translation>DNS 服务器</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="157"/>
        <source>Search Domain</source>
        <translation>搜索域</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="158"/>
        <source>DHCP Client ID</source>
        <translation>DHCP 客户端 ID</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="160"/>
        <source>Auto(DHCP)</source>
        <translation>自动 (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv4page.cpp" line="161"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
</context>
<context>
    <name>VpnIpv6Page</name>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="141"/>
        <source>IPv6 Config</source>
        <translation>IPv6 配置</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="142"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="143"/>
        <source>Netmask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="144"/>
        <source>Default Gateway</source>
        <translation>默认网关</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="145"/>
        <source>DNS Server</source>
        <translation>DNS 服务器</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="146"/>
        <source>Search Domain</source>
        <translation>搜索域</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="148"/>
        <source>Auto(DHCP)</source>
        <translation>自动 (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnipv6page.cpp" line="149"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
</context>
<context>
    <name>VpnListItem</name>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="61"/>
        <source>Not connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="160"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="176"/>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="162"/>
        <location filename="../frontend/list-items/vpnlistitem.cpp" line="174"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
</context>
<context>
    <name>VpnPage</name>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="259"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/single-pages/vpnpage.cpp" line="270"/>
        <source>VPN Settings</source>
        <translation>VPN 设置</translation>
    </message>
</context>
<context>
    <name>vpnAddPage</name>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="15"/>
        <source>create VPN</source>
        <translation>创建 VPN</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="47"/>
        <source>VPN Type</source>
        <translation>VPN 类型</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="75"/>
        <source>VPN Name</source>
        <translation>VPN 名称</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="81"/>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="103"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpnaddpage.cpp" line="98"/>
        <source>VPN Server</source>
        <translation>服务器地址</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="12"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="41"/>
        <source>Auto Connection</source>
        <translation>自动连接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="53"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetailpage.cpp" line="56"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>vpnConfigPage</name>
    <message>
        <source>VPN Type</source>
        <translation type="vanished">VPN类型</translation>
    </message>
</context>
<context>
    <name>VpnDetail</name>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="61"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="145"/>
        <source>VPN</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="146"/>
        <source>IPv4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="148"/>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="225"/>
        <source>IPv6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="153"/>
        <source>Advanced</source>
        <translation>高级</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="158"/>
        <source>Auto Connection</source>
        <translation>自动连接</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="159"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/vpndetails/vpndetail.cpp" line="160"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>vpnObject</name>
    <message>
        <location filename="../frontend/vpnobject.cpp" line="31"/>
        <source>vpn tool</source>
        <translation>VPN 工具</translation>
    </message>
</context>
</TS>
