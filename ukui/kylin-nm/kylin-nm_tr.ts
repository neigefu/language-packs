<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>BackThread</name>
    <message>
        <source>Confirm your WLAN password</source>
        <translation type="vanished">WLAN parolasını doğrula</translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <source>edit network</source>
        <translation type="vanished">Ağı düzenle</translation>
    </message>
    <message>
        <source>LAN name: </source>
        <translation type="obsolete">LAN adı: </translation>
    </message>
    <message>
        <source>Method: </source>
        <translation type="vanished">Yöntem: </translation>
    </message>
    <message>
        <source>Address: </source>
        <translation type="vanished">Adres: </translation>
    </message>
    <message>
        <source>Netmask: </source>
        <translation type="vanished">Netmask: </translation>
    </message>
    <message>
        <source>DNS 1: </source>
        <translation type="vanished">DNS 1: </translation>
    </message>
    <message>
        <source>DNS 2: </source>
        <translation type="vanished">DNS 2: </translation>
    </message>
    <message>
        <source>Edit Conn</source>
        <translation type="vanished">Bağ. Düzenle</translation>
    </message>
    <message>
        <source>Auto(DHCP)</source>
        <translation type="vanished">Oto(DHCP)</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="vanished">Elle</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Kaydet</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">Tamam</translation>
    </message>
    <message>
        <source>Can not create new wired network for without wired card</source>
        <translation type="obsolete">Kablolu kart olmadan yeni kablolu ağ oluşturulamıyor</translation>
    </message>
    <message>
        <source>New network already created</source>
        <translation type="obsolete">Yeni ağ zaten oluşturuldu</translation>
    </message>
    <message>
        <source>New network settings already finished</source>
        <translation type="obsolete">Yeni ağ ayarları zaten tamamlandı</translation>
    </message>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">Kylin ağ uygulaması masaüstü mesajı</translation>
    </message>
    <message>
        <source>Edit Network</source>
        <translation type="obsolete">Ağı Düzenle</translation>
    </message>
    <message>
        <source>Add Wired Network</source>
        <translation type="obsolete">Kablolu Ağ Ekle</translation>
    </message>
    <message>
        <source>create wired network successfully</source>
        <translation type="obsolete">Başarıyla kablolu ağ oluşturuldu</translation>
    </message>
    <message>
        <source>change configuration of wired network successfully</source>
        <translation type="obsolete">Kablolu ağın yapılandırmasını başarıyla değiştirildi</translation>
    </message>
    <message>
        <source>New settings already effective</source>
        <translation type="vanished">Yeni ayarlar zaten etkili</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="62"/>
        <source>Network profile type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="65"/>
        <source>Public(recommended)  Devices on the network cannot discover this computer. Generally, it is suitable for networks in public places, such as airports or coffee shops, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="69"/>
        <source>Private  Devices on the network can discover this computer. Generally applicable to a network at home or work where you know and trust the individuals and devices on the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="73"/>
        <source>Config firewall and security settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreatNetPage</name>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="95"/>
        <source>Connection Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="96"/>
        <source>IPv4Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="97"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="98"/>
        <source>Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="99"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="192"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="210"/>
        <source>Invalid subnet mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="230"/>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="231"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="330"/>
        <source>Address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished">Oto(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="118"/>
        <source>Manual</source>
        <translation type="unfinished">Elle</translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="302"/>
        <source>Auto Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="256"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="355"/>
        <source>SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="150"/>
        <source>Copied successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="227"/>
        <source>Copy all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="247"/>
        <source>Please input SSID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="260"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="356"/>
        <source>Protocol:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="264"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="357"/>
        <source>Security Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="268"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="358"/>
        <source>Hz:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="272"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="359"/>
        <source>Chan:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="276"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="360"/>
        <source>BandWidth:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="280"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="361"/>
        <source>IPv4:</source>
        <translation type="unfinished">IPv6 adresi: {4:?}</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="286"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="362"/>
        <source>IPv4 DNS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="292"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="363"/>
        <source>IPv6:</source>
        <translation type="unfinished">IPv6 adresi: {6:?}</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="296"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="364"/>
        <source>Mac:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgHideWifi</name>
    <message>
        <source>Add Hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN Ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">Bağlantı</translation>
    </message>
    <message>
        <source>WLAN name</source>
        <translation type="obsolete">WLAN adı</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="vanished">WLAN güvenlik</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlantı</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">Yok</translation>
    </message>
    <message>
        <source>Conn WLAN Success</source>
        <translation type="obsolete">WLAN Bağlantısı Başarılı</translation>
    </message>
    <message>
        <source>Confirm your WLAN password or usable of wireless card</source>
        <translation type="obsolete">Kablosuz şifrenizi veya kablosuz kart kullanılabilirliğini onaylayın</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="vanished">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapFast</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN Ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN güvenliği</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">Kimlik Doğrulama</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">Anonim kimlik</translation>
    </message>
    <message>
        <source>Allow automatic PAC pro_visioning</source>
        <translation type="vanished">Otomatik PAC pro_visioning&apos;e izin ver</translation>
    </message>
    <message>
        <source>PAC file</source>
        <translation type="vanished">PAC dosyası</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">İç kimlik doğrulama:</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Kullanıcı adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">Tünelli TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">Korumalı EAP (PEAP)</translation>
    </message>
    <message>
        <source>Anonymous</source>
        <translation type="vanished">Anonim</translation>
    </message>
    <message>
        <source>Authenticated</source>
        <translation type="vanished">Doğrulanmış</translation>
    </message>
    <message>
        <source>Both</source>
        <translation type="vanished">Her ikisi de</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapLeap</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN ağına bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN güvenliği:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="obsolete">Kimlik Doğrulama:</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Kullanıcı adı:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="obsolete">Tünelli TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="obsolete">Korumalı EAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPeap</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN güvenliği:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="obsolete">Kimlik Doğrulama:</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="obsolete">Anonim kimlik:</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">Domain:</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA sertifikası:</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA sertifika şifresi:</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CA sertifikası gerekmez</translation>
    </message>
    <message>
        <source>PEAP version</source>
        <translation type="vanished">PEAP sürümü:</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="obsolete">İç kimlik doğrulama:</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Kullanıcı adı:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="obsolete">Tünelli TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="obsolete">Korumalı EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">Dosyadan seçin...</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="vanished">Otomatik</translation>
    </message>
    <message>
        <source>Version 0</source>
        <translation type="vanished">Sürüm 0</translation>
    </message>
    <message>
        <source>Version 1</source>
        <translation type="vanished">Sürüm 1</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPwd</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN güvenliği:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="obsolete">Kimlik Doğrulama:</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Kullanıcı adı:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="obsolete">Tünelli TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="obsolete">Korumalı EAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTTLS</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN Güvenliği:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="obsolete">Kimlik Doğrulama:</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="obsolete">Anonim kimlik:</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">Domain:</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="obsolete">CA Sertifikası:</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="obsolete">CA sertifika şifresi:</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CA sertifikası gerekmez</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="obsolete">İç kimlik doğrulama:</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">Kullanıcı adı:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="obsolete">Tünelli TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="obsolete">Korumalı EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="obsolete">Dosyadan seçiniz...</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTls</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN güvenliği:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="obsolete">Kimlik Doğrulama:</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="vanished">Kimlik:</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">Domain:</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="obsolete">CA sertifikası:</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="obsolete">CA sertifika şifresi:</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">CA sertifikası gerekmez</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="vanished">Kullanıcı sertifikası:</translation>
    </message>
    <message>
        <source>User certificate password</source>
        <translation type="vanished">Kullanıcı sertifikası şifresi:</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="vanished">Kullanıcı özel anahtarı:</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="vanished">Kullanıcı anahtarı şifresi:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="obsolete">Tünelli TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="obsolete">Korumalı EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="obsolete">Dosyadan seç...</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiLeap</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN Ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN Güvenlik</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">Kullanıcı adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Parola</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1X)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWep</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN Ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN Güvenliği:</translation>
    </message>
    <message>
        <source>Key</source>
        <translation type="vanished">Anahtar</translation>
    </message>
    <message>
        <source>WEP index</source>
        <translation type="vanished">WEP index</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="obsolete">Kimlik Doğrulama:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="obsolete">WEP 128-bit Passphrase</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>1(default)</source>
        <translation type="vanished">1(default)</translation>
    </message>
    <message>
        <source>Open System</source>
        <translation type="vanished">Sistemi aç</translation>
    </message>
    <message>
        <source>Shared Key</source>
        <translation type="vanished">Paylaşılan Anahtar</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWpa</name>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Add Hidden WLAN</source>
        <translation type="obsolete">Gizli WLAN Ekle</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="obsolete">Bağlantı:</translation>
    </message>
    <message>
        <source>WLAN name</source>
        <translation type="obsolete">WLAN adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN güvenlik:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="obsolete">Oluştur...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>Conn WLAN Success</source>
        <translation type="obsolete">WLAN Bağlantısı Başarılı</translation>
    </message>
    <message>
        <source>Confirm your WLAN password or usable of wireless card</source>
        <translation type="obsolete">Kablosuz şifrenizi veya kablosuz kart kullanılabilirliğini onaylayın</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation type="obsolete">WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="obsolete">WEP 40/128-bit Key (Hex veya ASCII)</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="obsolete">Dinamik WEP (802.1x)</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <source>Create Hotspot</source>
        <translation type="obsolete">Etkin Nokta Oluştur</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="obsolete">Ağ adı:</translation>
    </message>
    <message>
        <source>WLAN security</source>
        <translation type="obsolete">WLAN Güvenlik:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">Tamam</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="obsolete">WPA &amp; WPA2 Kişisel</translation>
    </message>
</context>
<context>
    <name>DnsSettingWidget</name>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="34"/>
        <source>DNS Server Advanced Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="37"/>
        <source>Tactic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="40"/>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="43"/>
        <source>Retry Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="46"/>
        <source>order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="47"/>
        <source>rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="48"/>
        <source>concurrency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="53"/>
        <source> s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="59"/>
        <source> times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="70"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="73"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="76"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanDialog</name>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="116"/>
        <source>Wi-Fi network requires authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="121"/>
        <source>Access to Wi-Fi network &quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="123"/>
        <source>&quot; requires a password or encryption key.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="154"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="155"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FirewallDialog</name>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="85"/>
        <source>Allow other devices on this network to discover this computer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="87"/>
        <source>It is not recommended to enable this feature on public networks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="89"/>
        <source>Not allowed (recommended)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="90"/>
        <source>Allowed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ipv4Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="67"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="68"/>
        <source>Netmask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="69"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="66"/>
        <source>IPv4Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished">Oto(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="118"/>
        <source>Manual</source>
        <translation type="unfinished">Elle</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="274"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="283"/>
        <source>Invalid subnet mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="309"/>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="310"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="380"/>
        <source>Address conflict</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Ipv6Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="135"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="136"/>
        <source>Subnet prefix Length</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="137"/>
        <source>Default Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="134"/>
        <source>IPv6Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="180"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished">Oto(DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="181"/>
        <source>Manual</source>
        <translation type="unfinished">Elle</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="232"/>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="233"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="252"/>
        <source>Invalid address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="261"/>
        <source>Invalid gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="305"/>
        <source>Address conflict</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JoinHiddenWiFiPage</name>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="140"/>
        <source>Please enter the network information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="145"/>
        <source>Network name(SSID)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="146"/>
        <source>Show Network List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="147"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="148"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="153"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="155"/>
        <source>Find and Join WLAN</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">Kylin ağ uygulaması masaüstü mesajı</translation>
    </message>
</context>
<context>
    <name>LanListItem</name>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="69"/>
        <source>Not connected</source>
        <translation type="unfinished">Bağlanamadı</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="126"/>
        <source>Wired Device not carried</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="146"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="163"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="261"/>
        <source>Disconnect</source>
        <translation type="unfinished">Bağlantıyı Kes</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="148"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="161"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="265"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="152"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="168"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="153"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="170"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LanPage</name>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1192"/>
        <source>No ethernet device avaliable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="746"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="67"/>
        <source>conflict, unable to connect to the network normally!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="748"/>
        <source>Activated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="758"/>
        <source>Inactivated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1236"/>
        <source>Wired Device not carried</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1338"/>
        <source>Connected: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <source>(Limited)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1332"/>
        <source>Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItem</name>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="178"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="181"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished">Kylin ağ uygulaması masaüstü mesajı</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="214"/>
        <source>kylin-nm</source>
        <translation></translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">Ağ</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">Gelişmiş</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">Kablolu Ağ</translation>
    </message>
    <message>
        <source>Connect Hide Network</source>
        <translation type="vanished">Gizli Ağı Bağlan</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="299"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="301"/>
        <source>WLAN</source>
        <translation>WLAN</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="331"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="573"/>
        <location filename="../frontend/mainwindow.cpp" line="802"/>
        <source>Network tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="587"/>
        <source>Network Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="785"/>
        <source>Not connected to the network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="vanished">Aktif</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="vanished">Pasif</translation>
    </message>
    <message>
        <source>HotSpot</source>
        <translation type="vanished">HotSpot</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="330"/>
        <source>Show MainWindow</source>
        <translation>Ana Pencereyi Göster</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="vanished">Bağlanamadı</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">Bağlantı Kesildi</translation>
    </message>
    <message>
        <source>No Other Wired Network Scheme</source>
        <translation type="obsolete">Başka Kablolu Ağ Düzeni Yok</translation>
    </message>
    <message>
        <source>No Other Wireless Network Scheme</source>
        <translation type="obsolete">Başka Kablosuz Ağ Düzeni Yok</translation>
    </message>
    <message>
        <source>Wired net is disconnected</source>
        <translation type="obsolete">Kablolu ağ bağlantısı kesildi</translation>
    </message>
    <message>
        <source>WLAN is disconnected</source>
        <translation type="obsolete">Kablosuz bağlantı kesildi</translation>
    </message>
    <message>
        <source>Confirm your WLAN password or usable of wireless card</source>
        <translation type="obsolete">Kablosuz şifrenizi veya kablosuz kart kullanılabilirliğini onaylayın</translation>
    </message>
    <message>
        <source>Confirm your WLAN password</source>
        <translation type="obsolete">WLAN parolasını doğrula</translation>
    </message>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">Ethernet Ağları</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="obsolete">Yeni LAN</translation>
    </message>
    <message>
        <source>Hide WLAN</source>
        <translation type="vanished">Gizli WLAN</translation>
    </message>
    <message>
        <source>No usable network in the list</source>
        <translation type="obsolete">Listede kullanılabilir ağ yok</translation>
    </message>
    <message>
        <source>WLAN Networks</source>
        <translation type="vanished">WLAN Ağları</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">Yok</translation>
    </message>
    <message>
        <source>keep wired network switch is on before turning on wireless switch</source>
        <translation type="vanished">Kablosuz anahtarı açmadan önce kablolu ağ anahtarını açık tut</translation>
    </message>
    <message>
        <source>please insert the wireless network adapter</source>
        <translation type="vanished">Lütfen kablosuz ağ adaptörünü takın</translation>
    </message>
    <message>
        <source>Abnormal connection exist, program will delete it</source>
        <translation type="vanished">Anormal bağlantı var, program onu ​​silecek</translation>
    </message>
    <message>
        <source>update WLAN list now, click again</source>
        <translation type="vanished">Kablosuz listesini şimdi güncelle, tekrar tıkla</translation>
    </message>
    <message>
        <source>update WLAN list now</source>
        <translation type="vanished">Kablosuz listesini şimdi güncelle</translation>
    </message>
    <message>
        <source>Conn Ethernet Success</source>
        <translation type="vanished">Ethernet Bağlantısı Başarılı</translation>
    </message>
    <message>
        <source>Conn Ethernet Fail</source>
        <translation type="vanished">Ethernet Bağlantısı Hatası</translation>
    </message>
    <message>
        <source>Conn WLAN Success</source>
        <translation type="vanished">WLAN Bağlantısı Başarılı</translation>
    </message>
</context>
<context>
    <name>MultipleDnsWidget</name>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="57"/>
        <source>DNS server(Drag to sort)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="64"/>
        <source>Click &quot;+&quot; to configure DNS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="101"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="78"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="81"/>
        <source>kylin network desktop message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="377"/>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="381"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="383"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="390"/>
        <source>Config</source>
        <translation type="unfinished">Ayar</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="402"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="404"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="462"/>
        <source>Forget this network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="378"/>
        <source>IPv4</source>
        <translation type="unfinished">IPv6 adresi: {4?}</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="379"/>
        <source>IPv6</source>
        <translation type="unfinished">IPv6 adresi: {6?}</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="443"/>
        <source>Add LAN Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="448"/>
        <source>Connect Hidden WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="464"/>
        <source>Delete this network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="629"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="641"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1184"/>
        <source>None</source>
        <translation type="unfinished">Yok</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="753"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="754"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="755"/>
        <source>Auto</source>
        <translation type="unfinished">Oto</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="896"/>
        <source>start check ipv4 address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="913"/>
        <source>start check ipv6 address conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1180"/>
        <source>this wifi no support enterprise type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1185"/>
        <source>this wifi no support None type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1190"/>
        <source>this wifi no support WPA2 type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1193"/>
        <source>this wifi no support WPA3 type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NotifySend</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">--</translation>
    </message>
</context>
<context>
    <name>OldMainWindow</name>
    <message>
        <source>Ethernet Networks</source>
        <translation type="obsolete">Ethernet Ağları</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="obsolete">Yeni LAN</translation>
    </message>
    <message>
        <source>WLAN Networks</source>
        <translation type="obsolete">WLAN Ağları</translation>
    </message>
    <message>
        <source>Hide WLAN</source>
        <translation type="obsolete">Gizli WLAN</translation>
    </message>
    <message>
        <source>No usable network in the list</source>
        <translation type="obsolete">Listede kullanılabilir ağ yok</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="obsolete">Kablolu Ağ</translation>
    </message>
    <message>
        <source>HotSpot</source>
        <translation type="obsolete">HotSpot</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="obsolete">Gelişmiş</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="obsolete">Ana Pencereyi Göster</translation>
    </message>
    <message>
        <source>Wired net is disconnected</source>
        <translation type="obsolete">Kablolu ağ bağlantısı kesildi</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">Bağlanamadı</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="obsolete">Bağlantı Kesildi</translation>
    </message>
    <message>
        <source>No Other Wired Network Scheme</source>
        <translation type="obsolete">Başka Kablolu Ağ Düzeni Yok</translation>
    </message>
    <message>
        <source>No Other Wireless Network Scheme</source>
        <translation type="obsolete">Başka Kablosuz Ağ Düzeni Yok</translation>
    </message>
    <message>
        <source>WLAN is disconnected</source>
        <translation type="obsolete">Kablosuz bağlantı kesildi</translation>
    </message>
    <message>
        <source>Conn Ethernet Success</source>
        <translation type="obsolete">Ethernet Bağlantısı Başarılı</translation>
    </message>
    <message>
        <source>Conn WLAN Success</source>
        <translation type="obsolete">WLAN Bağlantısı Başarılı</translation>
    </message>
    <message>
        <source>Confirm your WLAN password or usable of wireless card</source>
        <translation type="obsolete">Kablosuz şifrenizi veya kablosuz kart kullanılabilirliğini onaylayın</translation>
    </message>
    <message>
        <source>Confirm your WLAN password</source>
        <translation type="obsolete">WLAN parolasını doğrula</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../frontend/list-items/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation>--</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation type="vanished">Parola gir</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">Ayar</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Bağlan</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">Bağlantıyı Kes</translation>
    </message>
    <message>
        <source>Input Password...</source>
        <translation type="obsolete">Parola gir...</translation>
    </message>
    <message>
        <source>Connect to Hidden WLAN Network</source>
        <translation type="vanished">Gizli WLAN Ağına Bağlan</translation>
    </message>
    <message>
        <source>Public</source>
        <translation type="vanished">Halka açık</translation>
    </message>
    <message>
        <source>Safe</source>
        <translation type="vanished">Güvenli</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="vanished">Oran</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
    <message>
        <source>WLAN Security：</source>
        <translation type="obsolete">WLAN güvenliği:</translation>
    </message>
    <message>
        <source>Sifnal：</source>
        <translation type="obsolete">Sinyal gücü:</translation>
    </message>
    <message>
        <source>MAC：</source>
        <translation type="obsolete">Fiziksel adres:</translation>
    </message>
    <message>
        <source>Conn WLAN Success</source>
        <translation type="obsolete">WLAN Bağlantısı Başarılı</translation>
    </message>
    <message>
        <source>Confirm your WLAN password or usable of wireless card</source>
        <translation type="obsolete">Kablosuz şifrenizi veya kablosuz kart kullanılabilirliğini onaylayın</translation>
    </message>
    <message>
        <source>Conn WLAN Failed</source>
        <translation type="vanished">WLAN Bağlantısı Başarısız</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../frontend/list-items/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation>--</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">Ayar</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Bağlan</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">Bağlantıyı Kes</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">Bağlanamadı</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="obsolete">Bağlantı Kesildi</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="obsolete">Kablolu Ağ</translation>
    </message>
    <message>
        <source>No Configuration</source>
        <translation type="obsolete">Yapılandırma Yok</translation>
    </message>
    <message>
        <source>IPv4：</source>
        <translation type="obsolete">IPv4 adresi:</translation>
    </message>
    <message>
        <source>IPv6：</source>
        <translation type="obsolete">IPv6 adresi:</translation>
    </message>
    <message>
        <source>BandWidth：</source>
        <translation type="obsolete">Bant genişliği:</translation>
    </message>
    <message>
        <source>MAC：</source>
        <translation type="obsolete">Fiziksel adres:</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">Oto</translation>
    </message>
</context>
<context>
    <name>SecurityPage</name>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="107"/>
        <source>Remember the Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="232"/>
        <source>Security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="233"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="253"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="237"/>
        <source>EAP type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="239"/>
        <source>Identity</source>
        <translation type="unfinished">Kimlik:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="240"/>
        <source>Domain</source>
        <translation type="unfinished">Domain:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="241"/>
        <source>CA certficate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="242"/>
        <source>no need for CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="243"/>
        <source>User certificate</source>
        <translation type="unfinished">Kullanıcı sertifikası:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="244"/>
        <source>User private key</source>
        <translation type="unfinished">Kullanıcı özel anahtarı:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="245"/>
        <source>User key password</source>
        <translation type="unfinished">Kullanıcı anahtarı şifresi:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="246"/>
        <source>Password options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="247"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="255"/>
        <location filename="../frontend/netdetails/securitypage.h" line="125"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="251"/>
        <source>Ineer authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="252"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="254"/>
        <source>Ask pwd each query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="258"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="271"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="274"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="277"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="297"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="379"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="522"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1048"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1129"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1160"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1182"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1205"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1260"/>
        <source>None</source>
        <translation type="unfinished">Yok</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="259"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="260"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="261"/>
        <source>WPA3 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="272"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="275"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="278"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="298"/>
        <source>Choose from file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store passwords only for this user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store password only for this user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store passwords for all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store password for all users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask this password every time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask password every time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="290"/>
        <source>PAC provisioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="291"/>
        <source>Allow automatic PAC provisioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="292"/>
        <source>PAC file</source>
        <translation type="unfinished">PAC dosyası</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="293"/>
        <source>Anonymous</source>
        <translation type="unfinished">Anonim</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="294"/>
        <source>Authenticated</source>
        <translation type="unfinished">Doğrulanmış</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="295"/>
        <source>Both</source>
        <translation type="unfinished">Her ikisi de</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1150"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1173"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1195"/>
        <source>Choose a CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1151"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1174"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1196"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1250"/>
        <source>Choose a PAC file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1251"/>
        <source>PAC Files (*.pac)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.h" line="126"/>
        <source> </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="72"/>
        <source>Current Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="79"/>
        <source>Devices Closed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="136"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="329"/>
        <source>Kylin NM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="332"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished">Kylin ağ uygulaması masaüstü mesajı</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">Kylin ağ uygulaması masaüstü mesajı</translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../frontend/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="41"/>
        <source>WLAN Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="52"/>
        <source>Input WLAN Information Please</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="53"/>
        <source>WLAN ID：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="54"/>
        <source>WLAN Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="55"/>
        <source>Password：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="56"/>
        <source>Cancl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="57"/>
        <source>Ok</source>
        <translation type="unfinished">Tamam</translation>
    </message>
</context>
<context>
    <name>WlanListItem</name>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="72"/>
        <source>Not connected</source>
        <translation type="unfinished">Bağlanamadı</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="177"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="204"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="637"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="656"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="666"/>
        <source>Disconnect</source>
        <translation type="unfinished">Bağlantıyı Kes</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="179"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="208"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="304"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="647"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="664"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="187"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="676"/>
        <source>Property</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="188"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="671"/>
        <source>Forget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="325"/>
        <source>Auto Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanMoreItem</name>
    <message>
        <location filename="../frontend/list-items/wlanmoreitem.cpp" line="28"/>
        <source>Add Others...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WlanPage</name>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="130"/>
        <source>WLAN</source>
        <translation type="unfinished">WLAN</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="132"/>
        <source>Activated WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="143"/>
        <source>Other WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1750"/>
        <source>Connected: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1752"/>
        <source>Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="106"/>
        <source>No wireless network card detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="974"/>
        <source>WLAN Connected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="970"/>
        <source>WLAN Disconnected Successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WpaWifiDialog</name>
    <message>
        <source>Identity</source>
        <translation type="obsolete">Kimlik:</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">Domain:</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Yok</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="118"/>
        <source>kylinnm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="122"/>
        <source>show kylin-nm wifi page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>show kylin-nm lan page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
