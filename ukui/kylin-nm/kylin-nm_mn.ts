<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn" sourcelanguage="en">
<context>
    <name>ConfForm</name>
    <message>
        <source>Manual</source>
        <translation type="obsolete">手动</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">网络提示消息</translation>
    </message>
    <message>
        <source>Will check the IP address conflict</source>
        <translation type="vanished">正在检测ip地址冲突</translation>
    </message>
    <message>
        <source>IPv4 address conflict, Please change IP</source>
        <translation type="vanished">ip地址冲突，请更改ip</translation>
    </message>
    <message>
        <source>IPv6 address conflict, Please change IP</source>
        <translation type="obsolete">ip地址冲突，请更改ip {6 ?}</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="62"/>
        <source>Network profile type</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="65"/>
        <source>Public(recommended)  Devices on the network cannot discover this computer. Generally, it is suitable for networks in public places, such as airports or coffee shops, etc.</source>
        <translation>ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ( ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ) ᠳ᠋ᠤ᠌ ᠬᠢ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠲᠤᠰ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ᠋ ᠣᠯᠵᠤ ᠮᠡᠳᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ᠂ ᠶᠡᠷᠦᠳᠡ ᠵᠢᠨ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠣᠯᠠᠨ ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠳᠠᠯᠠᠪᠠᠢ ᠳ᠋ᠤ᠌ ᠬᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠂ ᠵᠢᠰᠢᠶᠡᠯᠡᠪᠡᠯ ᠨᠢᠰᠬᠡᠯ ᠤ᠋ᠨ ᠪᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠺᠤᠹᠸ ᠵᠢᠨ ᠦᠷᠦᠬᠡ ᠵᠡᠷᠭᠡ.</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="69"/>
        <source>Private  Devices on the network can discover this computer. Generally applicable to a network at home or work where you know and trust the individuals and devices on the network.</source>
        <translation>ᠳᠤᠰᠬᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠲᠤᠰ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ᠋ ᠣᠯᠵᠤ ᠮᠡᠳᠡᠪᠡ᠂ ᠶᠡᠷᠦᠳᠡ ᠵᠢᠨ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠦᠢ ᠤ᠋ᠨ ᠪᠤᠶᠤ ᠠᠯᠪᠠᠨ ᠪᠠᠢᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠂ ᠲᠠ ᠳᠠᠨᠢᠬᠤ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠨᠠᠢᠳᠠᠪᠤᠷᠢᠳᠠᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠬᠡᠷᠡᠬᠢ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ ᠪᠤᠶᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ.</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="73"/>
        <source>Config firewall and security settings</source>
        <translation>ᠭᠠᠯ ᠰᠡᠷᠬᠡᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠮ ᠪᠤᠯᠤᠨ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>CopyButton</name>
    <message>
        <source>Copied successfully</source>
        <translation type="vanished">复制成功</translation>
    </message>
    <message>
        <source>Copied successfully!</source>
        <translation type="vanished">复制成功！</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">复制全部</translation>
    </message>
</context>
<context>
    <name>CreatNetPage</name>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="95"/>
        <source>Connection Name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="96"/>
        <source>IPv4Config</source>
        <translation>IPv4 ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="97"/>
        <source>Address</source>
        <translation>IPv4 ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="98"/>
        <source>Netmask</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠯᠬᠠᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="99"/>
        <source>Default Gateway</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠧᠲ ᠪᠤᠭᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="330"/>
        <source>Address conflict</source>
        <translation>ᠬᠠᠶᠢᠭ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">ᠳᠡᠷᠢᠬᠦᠨ ᠰᠤᠩᠭᠤᠯᠳᠠ DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">ᠪᠡᠯᠡᠳᠬᠡᠯ ᠰᠤᠩᠭᠤᠯᠳᠠ DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋ (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="118"/>
        <source>Manual</source>
        <translation>ᠭᠠᠷ᠎ᠢᠶᠠᠷ ᠬᠦᠳᠡᠯᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="192"/>
        <source>Invalid address</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="210"/>
        <source>Invalid subnet mask</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠯᠬᠠᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="230"/>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="231"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="302"/>
        <source>Auto Connection</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋ ᠪᠡᠷ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="256"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="355"/>
        <source>SSID:</source>
        <translation>SSID:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="150"/>
        <source>Copied successfully!</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="227"/>
        <source>Copy all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="247"/>
        <source>Please input SSID:</source>
        <translation>SSID ᠢ᠋/ ᠵᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="260"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="356"/>
        <source>Protocol:</source>
        <translation>ᠭᠡᠷ᠎ᠡ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="264"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="357"/>
        <source>Security Type:</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="268"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="358"/>
        <source>Hz:</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠠᠪᠳᠠᠮᠵᠢ ᠵᠢᠨ ᠪᠦᠰᠡ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="272"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="359"/>
        <source>Chan:</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠠᠮ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="276"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="360"/>
        <source>BandWidth:</source>
        <translation>ᠪᠦᠰᠡ ᠵᠢᠨ ᠦᠷᠭᠡᠨ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="292"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="363"/>
        <source>IPv6:</source>
        <translation>IPv6 ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="280"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="361"/>
        <source>IPv4:</source>
        <translation>IPv4 ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="286"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="362"/>
        <source>IPv4 DNS:</source>
        <translation>IPv4 ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="296"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="364"/>
        <source>Mac:</source>
        <translation>ᠹᠢᠽᠢᠺ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ:</translation>
    </message>
</context>
<context>
    <name>DlgHideWifi</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapFast</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPeap</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPwd</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTTLS</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTls</name>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="obsolete">用户证书</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="obsolete">用户私钥</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="obsolete">用户密钥密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWep</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWpa</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DnsSettingWidget</name>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="34"/>
        <source>DNS Server Advanced Settings</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠳᠡᠭᠡᠳᠦ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="37"/>
        <source>Tactic</source>
        <translation>ᠪᠤᠳᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="40"/>
        <source>Timeout</source>
        <translation>ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="43"/>
        <source>Retry Count</source>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠤᠷᠰᠢᠭᠰᠠᠨ ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="46"/>
        <source>order</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="47"/>
        <source>rotate</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="48"/>
        <source>concurrency</source>
        <translation>ᠵᠡᠷᠭᠡᠳᠡ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="53"/>
        <source> s</source>
        <translation> ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="59"/>
        <source> times</source>
        <translation> ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="70"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="73"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="76"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanDialog</name>
    <message>
        <source>Connect Enterprise WLAN</source>
        <translation type="vanished">ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ ᠤ᠋ᠨ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="116"/>
        <source>Wi-Fi network requires authentication</source>
        <translation>Wi-Fi ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="121"/>
        <source>Access to Wi-Fi network &quot;</source>
        <translation>Wi-Fi ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="123"/>
        <source>&quot; requires a password or encryption key.</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="154"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="155"/>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
</context>
<context>
    <name>FirewallDialog</name>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="85"/>
        <source>Allow other devices on this network to discover this computer?</source>
        <translation>ᠲᠤᠰ ᠳᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠪᠤᠰᠤᠳ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠡᠨᠡ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ᠋ ᠣᠯᠵᠤ ᠮᠡᠳᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="87"/>
        <source>It is not recommended to enable this feature on public networks</source>
        <translation>ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠲᠤᠰ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠥᠬᠡᠢ ᠪᠠᠢᠬᠤ ᠵᠢ ᠰᠠᠨᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="89"/>
        <source>Not allowed (recommended)</source>
        <translation>ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ ( ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ)</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="90"/>
        <source>Allowed</source>
        <translation>ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Ipv4Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="66"/>
        <source>IPv4Config</source>
        <translation>IPv4 ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="67"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="68"/>
        <source>Netmask</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠯᠬᠠᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="69"/>
        <source>Default Gateway</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠧᠲ ᠪᠤᠭᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">ᠳᠡᠷᠢᠬᠦᠨ ᠰᠤᠩᠭᠤᠯᠳᠠ DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">ᠪᠡᠯᠡᠳᠬᠡᠯ ᠰᠤᠩᠭᠤᠯᠳᠠ DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋ (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="118"/>
        <source>Manual</source>
        <translation>ᠭᠠᠷ᠎ᠢᠶᠠᠷ ᠬᠦᠳᠡᠯᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="274"/>
        <source>Invalid address</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="283"/>
        <source>Invalid subnet mask</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠯᠬᠠᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="309"/>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="310"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="380"/>
        <source>Address conflict</source>
        <translation>ᠬᠠᠶᠢᠭ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ</translation>
    </message>
</context>
<context>
    <name>Ipv6Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="134"/>
        <source>IPv6Config</source>
        <translation>IPv6 ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="135"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="136"/>
        <source>Subnet prefix Length</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠤᠭᠳᠤᠪᠤᠷᠢ ᠵᠢᠨ ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="137"/>
        <source>Default Gateway</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠧᠲ ᠪᠤᠭᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">ᠳᠡᠷᠢᠬᠦᠨ ᠰᠤᠩᠭᠤᠯᠳᠠ DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">ᠪᠡᠯᠡᠳᠬᠡᠯ ᠰᠤᠩᠭᠤᠯᠳᠠ DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="180"/>
        <source>Auto(DHCP)</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋ (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="181"/>
        <source>Manual</source>
        <translation>ᠭᠠᠷ᠎ᠢᠶᠠᠷ ᠬᠦᠳᠡᠯᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="232"/>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="233"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="252"/>
        <source>Invalid address</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="261"/>
        <source>Invalid gateway</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠨᠧᠲ ᠪᠤᠭᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="305"/>
        <source>Address conflict</source>
        <translation>ᠬᠠᠶᠢᠭ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ</translation>
    </message>
</context>
<context>
    <name>JoinHiddenWiFiPage</name>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="140"/>
        <source>Please enter the network information</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠣᠷᠣᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="145"/>
        <source>Network name(SSID)</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ (SSID)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="146"/>
        <source>Show Network List</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="147"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="148"/>
        <source>Join</source>
        <translation>ᠣᠷᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="153"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="155"/>
        <source>Find and Join WLAN</source>
        <translation>ᠤᠳᠠᠰᠤ ᠥᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠡᠷᠢᠵᠤ ᠣᠷᠣᠬᠤ</translation>
    </message>
</context>
<context>
    <name>LanListItem</name>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="69"/>
        <source>Not connected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="126"/>
        <source>Wired Device not carried</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠤᠳᠠᠰᠤ ᠵᠠᠯᠭᠠᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="146"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="163"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="261"/>
        <source>Disconnect</source>
        <translation>ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="148"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="161"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="265"/>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="152"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="168"/>
        <source>Property</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="153"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="170"/>
        <source>Delete</source>
        <translation>ᠲᠤᠰ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>LanPage</name>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1192"/>
        <source>No ethernet device avaliable</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="746"/>
        <source>LAN</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="67"/>
        <source>conflict, unable to connect to the network normally!</source>
        <translation>ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠦ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠪ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="748"/>
        <source>Activated LAN</source>
        <translation>ᠮᠢᠨᠤ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="758"/>
        <source>Inactivated LAN</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>LAN Disconnected Successfully</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠰᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1236"/>
        <source>Wired Device not carried</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠤᠳᠠᠰᠤ ᠵᠠᠯᠭᠠᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>LAN Connected Successfully</source>
        <translation type="vanished">ᠤᠳᠠᠰᠤᠳᠤ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1338"/>
        <source>Connected: </source>
        <translation>ᠴᠥᠷᠬᠡᠯᠡᠪᠡ: </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1332"/>
        <source>Not Connected</source>
        <translation>ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <source>(Limited)</source>
        <translation>( ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠭᠳᠠᠪᠠ)</translation>
    </message>
</context>
<context>
    <name>ListItem</name>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="178"/>
        <source>Kylin NM</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="181"/>
        <source>kylin network applet desktop message</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="214"/>
        <source>kylin-nm</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="299"/>
        <source>LAN</source>
        <translatorcomment>有线网络</translatorcomment>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="301"/>
        <source>WLAN</source>
        <translatorcomment>无线局域网</translatorcomment>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="330"/>
        <source>Show MainWindow</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠭᠠᠵᠢ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="331"/>
        <source>Settings</source>
        <translatorcomment>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠦᠷᠦᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translatorcomment>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠦᠷᠦᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="573"/>
        <location filename="../frontend/mainwindow.cpp" line="802"/>
        <source>Network tool</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="587"/>
        <source>Network Card</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="785"/>
        <source>Not connected to the network</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>MultipleDnsWidget</name>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="57"/>
        <source>DNS server(Drag to sort)</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="64"/>
        <source>Click &quot;+&quot; to configure DNS</source>
        <translation>&quot;+&quot; ᠢ᠋/ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ᠂ DNS ᠢ᠋/ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="101"/>
        <source>Settings</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="78"/>
        <source>Kylin NM</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="81"/>
        <source>kylin network desktop message</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="377"/>
        <source>Detail</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠭᠦᠢ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="378"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="379"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="381"/>
        <source>Security</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="383"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="390"/>
        <source>Config</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="402"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="404"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="462"/>
        <source>Forget this network</source>
        <translation>ᠲᠤᠰ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠮᠠᠷᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="464"/>
        <source>Delete this network</source>
        <translation>ᠲᠤᠰ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="443"/>
        <source>Add LAN Connect</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="448"/>
        <source>Connect Hidden WLAN</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ WLAN ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="629"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="641"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1184"/>
        <source>None</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="753"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="754"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="755"/>
        <source>Auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="896"/>
        <source>start check ipv4 address conflict</source>
        <translation>ipv4 ᠬᠠᠶᠢᠭ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠡᠬᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="913"/>
        <source>start check ipv6 address conflict</source>
        <translation>ipv6 ᠬᠠᠶᠢᠭ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠡᠬᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>ipv4 address conflict!</source>
        <translation type="vanished">ipv4 ᠬᠠᠶᠢᠭ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠲᠠᠢ!</translation>
    </message>
    <message>
        <source>ipv6 address conflict!</source>
        <translation type="vanished">ipv6 ᠬᠠᠶᠢᠭ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠲᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1180"/>
        <source>this wifi no support enterprise type</source>
        <translation>ᠲᠤᠰ wifi ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ ᠤ᠋ᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠦᠷᠦᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1185"/>
        <source>this wifi no support None type</source>
        <translation>ᠲᠤᠰ wifi ᠬᠤᠭᠤᠰᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1190"/>
        <source>this wifi no support WPA2 type</source>
        <translation>ᠲᠤᠰ wifiWPA2 ᠳᠦᠷᠦᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1193"/>
        <source>this wifi no support WPA3 type</source>
        <translation>ᠲᠤᠰ wifiWPA3 ᠳᠦᠷᠦᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>SSID:</source>
        <translation type="obsolete">SSID：</translation>
    </message>
    <message>
        <source>Protocol:</source>
        <translation type="obsolete">协议：</translation>
    </message>
    <message>
        <source>Hz:</source>
        <translation type="obsolete">网络频带：</translation>
    </message>
    <message>
        <source>Chan:</source>
        <translation type="obsolete">网络通道：</translation>
    </message>
    <message>
        <source>BandWidth:</source>
        <translation type="obsolete">带宽：</translation>
    </message>
    <message>
        <source>IPv4:</source>
        <translation type="obsolete">IPv4地址：</translation>
    </message>
    <message>
        <source>IPv4 DNS:</source>
        <translation type="obsolete">IPv4 DNS服务器：</translation>
    </message>
    <message>
        <source>IPv6:</source>
        <translation type="obsolete">本地链接IPv6地址：</translation>
    </message>
    <message>
        <source>Mac:</source>
        <translation type="obsolete">物理地址：</translation>
    </message>
</context>
<context>
    <name>OldMainWindow</name>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="obsolete">打开网络工具</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../frontend/list-items/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="obsolete">忘记此网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../frontend/list-items/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>SecurityPage</name>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="107"/>
        <source>Remember the Network</source>
        <translation>ᠲᠤᠰ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠴᠡᠬᠡᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="232"/>
        <source>Security</source>
        <translation>ᠠᠶᠤᠯᠬᠦᠢ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="233"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="253"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠲᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="237"/>
        <source>EAP type</source>
        <translation>EAP ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="239"/>
        <source>Identity</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ ᠵᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="240"/>
        <source>Domain</source>
        <translation>ᠤᠷᠤᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="241"/>
        <source>CA certficate</source>
        <translation>CA ᠦᠨᠡᠮᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="242"/>
        <source>no need for CA certificate</source>
        <translation>CA ᠦᠨᠡᠮᠯᠡᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="243"/>
        <source>User certificate</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠨᠡᠮᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="244"/>
        <source>User private key</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="245"/>
        <source>User key password</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="246"/>
        <source>Password options</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="247"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="255"/>
        <location filename="../frontend/netdetails/securitypage.h" line="125"/>
        <source>Required</source>
        <translation>ᠵᠠᠪᠠᠯ ᠲᠠᠭᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="251"/>
        <source>Ineer authentication</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="252"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="254"/>
        <source>Ask pwd each query</source>
        <translation>ᠤᠳᠠᠭ᠎ᠠ ᠪᠦᠷᠢ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="258"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="271"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="274"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="277"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="297"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="379"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="522"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1048"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1129"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1160"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1182"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1205"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1260"/>
        <source>None</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="259"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation>WPA&amp;WPA2 ᠬᠤᠪᠢ ᠬᠦᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="260"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation>WPA&amp;WPA2 ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="261"/>
        <source>WPA3 Personal</source>
        <translation>WPA3 ᠬᠤᠪᠢ ᠬᠦᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="272"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="275"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="278"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="298"/>
        <source>Choose from file...</source>
        <translation>ᠹᠠᠢᠯ ᠡᠴᠡ ᠰᠤᠩᠭᠤᠬᠤ...</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store passwords only for this user</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store password only for this user</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store passwords for all users</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store password for all users</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask this password every time</source>
        <translation>ᠤᠳᠠᠭ᠎ᠠ ᠪᠦᠷᠢ ᠲᠤᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask password every time</source>
        <translation>ᠤᠳᠠᠭ᠎ᠠ ᠪᠦᠷᠢ ᠲᠤᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1150"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1173"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1195"/>
        <source>Choose a CA certificate</source>
        <translation>ᠨᠢᠭᠡ CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1151"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1174"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1196"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA ᠬᠡᠷᠡᠴᠢᠯᠡᠯ (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="290"/>
        <source>PAC provisioning</source>
        <translation>PAC ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="291"/>
        <source>Allow automatic PAC provisioning</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ PAC ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="292"/>
        <source>PAC file</source>
        <translation>PAC ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="293"/>
        <source>Anonymous</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠯᠠᠭᠰᠠᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="294"/>
        <source>Authenticated</source>
        <translation>ᠬᠡᠷᠡᠴᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="295"/>
        <source>Both</source>
        <translation>ᠬᠣᠶᠠᠭᠤᠯᠠ ᠵᠢ ᠠᠭᠤᠰᠤᠯᠴᠠᠭᠤᠯᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1250"/>
        <source>Choose a PAC file</source>
        <translation>ᠨᠢᠭᠡ PAC ᠹᠠᠢᠯ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1251"/>
        <source>PAC Files (*.pac)</source>
        <translation>PAC ᠹᠠᠢᠯ (*.pac)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.h" line="126"/>
        <source> </source>
        <translation> </translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="72"/>
        <source>Current Device</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠨᠧᠲ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="79"/>
        <source>Devices Closed!</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="136"/>
        <source>Settings</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="329"/>
        <source>Kylin NM</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠪᠠᠭᠠᠵᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="332"/>
        <source>kylin network applet desktop message</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../frontend/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="41"/>
        <source>WLAN Authentication</source>
        <translation>WLAN ᠬᠡᠷᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="52"/>
        <source>Input WLAN Information Please</source>
        <translation>WLAN ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="53"/>
        <source>WLAN ID：</source>
        <translation>WLAN ID：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="54"/>
        <source>WLAN Name:</source>
        <translation>WLAN ᠨᠡᠷᠡᠢᠳᠦᠯ:</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="55"/>
        <source>Password：</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="56"/>
        <source>Cancl</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="57"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>WlanListItem</name>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="72"/>
        <source>Not connected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="177"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="204"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="637"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="656"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="666"/>
        <source>Disconnect</source>
        <translation>ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="179"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="208"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="304"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="647"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="664"/>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="188"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="671"/>
        <source>Forget</source>
        <translation>ᠲᠤᠰ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠮᠠᠷᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="187"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="676"/>
        <source>Property</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="325"/>
        <source>Auto Connect</source>
        <translation>ᠲᠤᠰ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠦ᠋ᠲ᠋ᠤ᠋ ᠪᠡᠷ ᠵᠠᠯᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>WlanMoreItem</name>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanmoreitem.cpp" line="28"/>
        <source>Add Others...</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠵᠠᠯᠭᠠᠬᠤ...</translation>
    </message>
</context>
<context>
    <name>WlanPage</name>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="130"/>
        <source>WLAN</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠬᠡᠰᠡᠭ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="106"/>
        <source>No wireless network card detected</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ᠎ᠢ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="132"/>
        <source>Activated WLAN</source>
        <translation>ᠮᠢᠨᠤ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="143"/>
        <source>Other WLAN</source>
        <translation>ᠪᠤᠰᠤᠳ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="974"/>
        <source>WLAN Connected Successfully</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="970"/>
        <source>WLAN Disconnected Successfully</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠳᠠᠰᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1750"/>
        <source>Connected: </source>
        <translation>ᠴᠥᠷᠬᠡᠯᠡᠪᠡ: </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1752"/>
        <source>Not Connected</source>
        <translation>ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>(Limited)</source>
        <translation type="vanished">( ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠭᠳᠠᠪᠠ)</translation>
    </message>
</context>
<context>
    <name>WpaWifiDialog</name>
    <message>
        <source>EAP type</source>
        <translation type="obsolete">EAP方法</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Ask pwd each query</source>
        <translation type="obsolete">每次询问密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
    <message>
        <source>Choose from file...</source>
        <translation type="obsolete">从文件选择...</translation>
    </message>
    <message>
        <source>Choose a CA certificate</source>
        <translation type="obsolete">选择一个CA证书</translation>
    </message>
    <message>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="obsolete">CA 证书 (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>no need for CA certificate</source>
        <translation type="obsolete">不需要CA证书</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="118"/>
        <source>kylinnm</source>
        <translation>ᠵᠢᠢ ᠯᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ ᠪᠠᠭᠠᠵᠢ᠃</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="122"/>
        <source>show kylin-nm wifi page</source>
        <translation>ᠾᠧ ᠯᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ wifi ᠨᠢᠭᠤᠷ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>show kylin-nm lan page</source>
        <translation>ᠾᠧ ᠯᠢᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠦ ᠲᠣᠣᠷ ᠤᠨ ᠨᠢᠭᠤᠷ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠶ ᠃</translation>
    </message>
</context>
</TS>
