<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>ConfForm</name>
    <message>
        <source>Manual</source>
        <translation type="obsolete">手动</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="obsolete">网络提示消息</translation>
    </message>
    <message>
        <source>Will check the IP address conflict</source>
        <translation type="vanished">正在检测ip地址冲突</translation>
    </message>
    <message>
        <source>IPv4 address conflict, Please change IP</source>
        <translation type="vanished">ip地址冲突，请更改ip</translation>
    </message>
    <message>
        <source>IPv6 address conflict, Please change IP</source>
        <translation type="obsolete">ip地址冲突，请更改ip {6 ?}</translation>
    </message>
</context>
<context>
    <name>ConfigPage</name>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="62"/>
        <source>Network profile type</source>
        <translation>网络配置文件类型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="65"/>
        <source>Public(recommended)  Devices on the network cannot discover this computer. Generally, it is suitable for networks in public places, such as airports or coffee shops, etc.</source>
        <translation>公用（推荐） 网络中的设备不可发现此电脑。一般情况下适用于公共场所中的网络，如机场或咖啡店等等。</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="69"/>
        <source>Private  Devices on the network can discover this computer. Generally applicable to a network at home or work where you know and trust the individuals and devices on the network.</source>
        <translation>专用  网络中的设备可发现此电脑。一般情况下适用于家庭或工作单位的网络，您认识并信任网络上的个人和设备。</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/configpage.cpp" line="73"/>
        <source>Config firewall and security settings</source>
        <translation>配置防火墙和安全设置</translation>
    </message>
</context>
<context>
    <name>CopyButton</name>
    <message>
        <source>Copied successfully</source>
        <translation type="vanished">复制成功</translation>
    </message>
    <message>
        <source>Copied successfully!</source>
        <translation type="vanished">复制成功！</translation>
    </message>
    <message>
        <source>Copy all</source>
        <translation type="vanished">复制全部</translation>
    </message>
</context>
<context>
    <name>CreatNetPage</name>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="95"/>
        <source>Connection Name</source>
        <translation>网络名称</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="96"/>
        <source>IPv4Config</source>
        <translation>IPv4 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="97"/>
        <source>Address</source>
        <translation>IPv4 地址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="98"/>
        <source>Netmask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="99"/>
        <source>Default Gateway</source>
        <translation>默认网关</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="330"/>
        <source>Address conflict</source>
        <translation>地址冲突</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">首选 DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">备选 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation>自动 (DHCP)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="118"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="192"/>
        <source>Invalid address</source>
        <translation>无效的IP地址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="210"/>
        <source>Invalid subnet mask</source>
        <translation>无效的子网掩码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="230"/>
        <location filename="../frontend/netdetails/creatnetpage.cpp" line="231"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
</context>
<context>
    <name>DetailPage</name>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="302"/>
        <source>Auto Connection</source>
        <translation>自动连接</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="256"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="355"/>
        <source>SSID:</source>
        <translation>SSID：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="150"/>
        <source>Copied successfully!</source>
        <translation>复制成功！</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="227"/>
        <source>Copy all</source>
        <translation>复制全部</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="247"/>
        <source>Please input SSID:</source>
        <translation>请输入 SSID:</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="260"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="356"/>
        <source>Protocol:</source>
        <translation>协议：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="264"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="357"/>
        <source>Security Type:</source>
        <translation>安全类型：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="268"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="358"/>
        <source>Hz:</source>
        <translation>网络频带：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="272"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="359"/>
        <source>Chan:</source>
        <translation>网络通道：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="276"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="360"/>
        <source>BandWidth:</source>
        <translation>带宽：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="292"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="363"/>
        <source>IPv6:</source>
        <translation>本地链接 IPv6 地址：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="280"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="361"/>
        <source>IPv4:</source>
        <translation>IPv4 地址：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="286"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="362"/>
        <source>IPv4 DNS:</source>
        <translation>IPv4 DNS 服务器：</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/detailpage.cpp" line="296"/>
        <location filename="../frontend/netdetails/detailpage.cpp" line="364"/>
        <source>Mac:</source>
        <translation>物理地址：</translation>
    </message>
</context>
<context>
    <name>DlgHideWifi</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapFast</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPeap</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapPwd</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTTLS</name>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiEapTls</name>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="obsolete">用户证书</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="obsolete">用户私钥</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="obsolete">用户密钥密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiLeap</name>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWep</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DlgHideWifiWpa</name>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>DnsSettingWidget</name>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="34"/>
        <source>DNS Server Advanced Settings</source>
        <translation>DNS 服务器高级配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="37"/>
        <source>Tactic</source>
        <translation>策略</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="40"/>
        <source>Timeout</source>
        <translation>超时时间</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="43"/>
        <source>Retry Count</source>
        <translation>重试次数</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="46"/>
        <source>order</source>
        <translation>顺序</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="47"/>
        <source>rotate</source>
        <translation>随机</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="48"/>
        <source>concurrency</source>
        <translation>并发</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="53"/>
        <source> s</source>
        <translation> 秒</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="59"/>
        <source> times</source>
        <translation> 次</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="70"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="73"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/dnssettingwidget.cpp" line="76"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>EnterpriseWlanDialog</name>
    <message>
        <source>Connect Enterprise WLAN</source>
        <translation type="vanished">连接企业网</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="116"/>
        <source>Wi-Fi network requires authentication</source>
        <translation>Wi-Fi 网络要求认证</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="121"/>
        <source>Access to Wi-Fi network &quot;</source>
        <translation>访问 Wi-Fi 网络</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="123"/>
        <source>&quot; requires a password or encryption key.</source>
        <translation>需要密码或加密密钥。</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="154"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/enterprise-wlan/enterprisewlandialog.cpp" line="155"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
</context>
<context>
    <name>FirewallDialog</name>
    <message>
        <source>Allow your computer to be discovered by other computers and devices on this network？</source>
        <translation type="vanished">是否允许你的电脑被此网络上的其他电脑和设备发现？</translation>
    </message>
    <message>
        <source>It is recommended that you enable this feature on your home and work networks rather than public networks.</source>
        <translation type="vanished">建议你在家庭和工作网络上而非公共网络上启用此功能。</translation>
    </message>
    <message>
        <source>Yse</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="85"/>
        <source>Allow other devices on this network to discover this computer?</source>
        <translation>是否允许此网络上的其他设备发现这台电脑？</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="87"/>
        <source>It is not recommended to enable this feature on public networks</source>
        <translation>不建议在公共网络上开启此功能</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="89"/>
        <source>Not allowed (recommended)</source>
        <translation>不允许（推荐）</translation>
    </message>
    <message>
        <location filename="../frontend/networkmode/firewalldialog.cpp" line="90"/>
        <source>Allowed</source>
        <translation>允许</translation>
    </message>
</context>
<context>
    <name>Ipv4Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="66"/>
        <source>IPv4Config</source>
        <translation>IPv4 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="67"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="68"/>
        <source>Netmask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="69"/>
        <source>Default Gateway</source>
        <translation>默认网关</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">首选 DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">备选 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="117"/>
        <source>Auto(DHCP)</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="118"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="274"/>
        <source>Invalid address</source>
        <translation>无效的IP地址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="283"/>
        <source>Invalid subnet mask</source>
        <translation>无效的子网掩码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="309"/>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="310"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv4page.cpp" line="380"/>
        <source>Address conflict</source>
        <translation>地址冲突</translation>
    </message>
</context>
<context>
    <name>Ipv6Page</name>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="134"/>
        <source>IPv6Config</source>
        <translation>IPv6 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="135"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="136"/>
        <source>Subnet prefix Length</source>
        <translation>子网前缀长度</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="137"/>
        <source>Default Gateway</source>
        <translation>默认网关</translation>
    </message>
    <message>
        <source>Prefs DNS</source>
        <translation type="vanished">首选 DNS</translation>
    </message>
    <message>
        <source>Alternative DNS</source>
        <translation type="vanished">备选 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="180"/>
        <source>Auto(DHCP)</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="181"/>
        <source>Manual</source>
        <translation>手动</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="232"/>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="233"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="252"/>
        <source>Invalid address</source>
        <translation>无效的IP地址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="261"/>
        <source>Invalid gateway</source>
        <translation>无效的网关地址</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/ipv6page.cpp" line="305"/>
        <source>Address conflict</source>
        <translation>地址冲突</translation>
    </message>
</context>
<context>
    <name>JoinHiddenWiFiPage</name>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="140"/>
        <source>Please enter the network information</source>
        <translation>请输入您想要加入的网络信息</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="145"/>
        <source>Network name(SSID)</source>
        <translation>网络名 (SSID)</translation>
    </message>
    <message>
        <source>Remember the Network</source>
        <translation type="vanished">记住该网络</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="146"/>
        <source>Show Network List</source>
        <translation>显示网络列表</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="147"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="148"/>
        <source>Join</source>
        <translation>加入</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="153"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/joinhiddenwifipage.cpp" line="155"/>
        <source>Find and Join WLAN</source>
        <translation>查找并加入无线局域网络</translation>
    </message>
</context>
<context>
    <name>LanListItem</name>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="69"/>
        <source>Not connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="126"/>
        <source>Wired Device not carried</source>
        <translation>未插入网线</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="146"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="163"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="261"/>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="148"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="161"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="265"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="152"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="168"/>
        <source>Property</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="153"/>
        <location filename="../frontend/list-items/lanlistitem.cpp" line="170"/>
        <source>Delete</source>
        <translation>删除此网络</translation>
    </message>
</context>
<context>
    <name>LanPage</name>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1192"/>
        <source>No ethernet device avaliable</source>
        <translation>未检测到有线设备</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="746"/>
        <source>LAN</source>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="67"/>
        <source>conflict, unable to connect to the network normally!</source>
        <translation>冲突，无法正常连接到网络！</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="748"/>
        <source>Activated LAN</source>
        <translation>我的网络</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="758"/>
        <source>Inactivated LAN</source>
        <translation>其他网络</translation>
    </message>
    <message>
        <source>LAN Disconnected Successfully</source>
        <translation type="vanished">有线网络已断开</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1236"/>
        <source>Wired Device not carried</source>
        <translation>未插入网线</translation>
    </message>
    <message>
        <source>LAN Connected Successfully</source>
        <translation type="vanished">有线网络已连接</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1338"/>
        <source>Connected: </source>
        <translation>已连接: </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1332"/>
        <source>Not Connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/lanpage.cpp" line="1330"/>
        <source>(Limited)</source>
        <translation>(网络受限)</translation>
    </message>
</context>
<context>
    <name>ListItem</name>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="178"/>
        <source>Kylin NM</source>
        <translation>网络设置工具</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/listitem.cpp" line="181"/>
        <source>kylin network applet desktop message</source>
        <translation>网络提示消息</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="214"/>
        <source>kylin-nm</source>
        <translation>网络工具</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="299"/>
        <source>LAN</source>
        <translatorcomment>有线网络</translatorcomment>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="301"/>
        <source>WLAN</source>
        <translatorcomment>无线局域网</translatorcomment>
        <translation>无线局域网</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="330"/>
        <source>Show MainWindow</source>
        <translation>打开网络工具</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="331"/>
        <source>Settings</source>
        <translatorcomment>设置网络项</translatorcomment>
        <translation>设置网络项</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="573"/>
        <location filename="../frontend/mainwindow.cpp" line="802"/>
        <source>Network tool</source>
        <translation>网络工具</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="587"/>
        <source>Network Card</source>
        <translation>网卡</translation>
    </message>
    <message>
        <location filename="../frontend/mainwindow.cpp" line="785"/>
        <source>Not connected to the network</source>
        <translation>未连接网络</translation>
    </message>
</context>
<context>
    <name>MultipleDnsWidget</name>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="57"/>
        <source>DNS server(Drag to sort)</source>
        <translation>DNS 服务器高级配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="64"/>
        <source>Click &quot;+&quot; to configure DNS</source>
        <translation>点击 “+”配置 DNS</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/multiplednswidget.cpp" line="101"/>
        <source>Settings</source>
        <translation>高级设置</translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="78"/>
        <source>Kylin NM</source>
        <translation>网络设置工具</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="81"/>
        <source>kylin network desktop message</source>
        <translation>网络提示消息</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="377"/>
        <source>Detail</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="378"/>
        <source>IPv4</source>
        <translation>IPv4</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="379"/>
        <source>IPv6</source>
        <translation>IPv6</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="381"/>
        <source>Security</source>
        <translation>安全</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="383"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="390"/>
        <source>Config</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="402"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="404"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="462"/>
        <source>Forget this network</source>
        <translation>忘记此网络</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="464"/>
        <source>Delete this network</source>
        <translation>删除此网络</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="443"/>
        <source>Add LAN Connect</source>
        <translation>添加有线网络</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="448"/>
        <source>Connect Hidden WLAN</source>
        <translation>连接到隐藏 WLAN</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="629"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="641"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1184"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="753"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="754"/>
        <location filename="../frontend/netdetails/netdetail.cpp" line="755"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="896"/>
        <source>start check ipv4 address conflict</source>
        <translation>开始检测 ipv4 地址冲突</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="913"/>
        <source>start check ipv6 address conflict</source>
        <translation>开始检测 ipv6 地址冲突</translation>
    </message>
    <message>
        <source>ipv4 address conflict!</source>
        <translation type="vanished">ipv4地址冲突！</translation>
    </message>
    <message>
        <source>ipv6 address conflict!</source>
        <translation type="vanished">ipv6地址冲突！</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1180"/>
        <source>this wifi no support enterprise type</source>
        <translation>此 wifi 不支持企业网类型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1185"/>
        <source>this wifi no support None type</source>
        <translation>此 wifi 不支持空类型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1190"/>
        <source>this wifi no support WPA2 type</source>
        <translation>此 wifi 不支持 WPA2 类型</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/netdetail.cpp" line="1193"/>
        <source>this wifi no support WPA3 type</source>
        <translation>此 wifi 不支持 WPA3 类型</translation>
    </message>
    <message>
        <source>SSID:</source>
        <translation type="obsolete">SSID：</translation>
    </message>
    <message>
        <source>Protocol:</source>
        <translation type="obsolete">协议：</translation>
    </message>
    <message>
        <source>Hz:</source>
        <translation type="obsolete">网络频带：</translation>
    </message>
    <message>
        <source>Chan:</source>
        <translation type="obsolete">网络通道：</translation>
    </message>
    <message>
        <source>BandWidth:</source>
        <translation type="obsolete">带宽：</translation>
    </message>
    <message>
        <source>IPv4:</source>
        <translation type="obsolete">IPv4地址：</translation>
    </message>
    <message>
        <source>IPv4 DNS:</source>
        <translation type="obsolete">IPv4 DNS服务器：</translation>
    </message>
    <message>
        <source>IPv6:</source>
        <translation type="obsolete">本地链接IPv6地址：</translation>
    </message>
    <message>
        <source>Mac:</source>
        <translation type="obsolete">物理地址：</translation>
    </message>
</context>
<context>
    <name>OldMainWindow</name>
    <message>
        <source>kylin-nm</source>
        <translation type="obsolete">网络工具</translation>
    </message>
    <message>
        <source>Show MainWindow</source>
        <translation type="obsolete">打开网络工具</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../frontend/list-items/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="obsolete">忘记此网络</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../frontend/list-items/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">断开</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>SecurityPage</name>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="107"/>
        <source>Remember the Network</source>
        <translation>记住该网络</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="232"/>
        <source>Security</source>
        <translation>安全性</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="233"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="253"/>
        <source>Password</source>
        <translation>密钥</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="237"/>
        <source>EAP type</source>
        <translation>EAP 方法</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="239"/>
        <source>Identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="240"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="241"/>
        <source>CA certficate</source>
        <translation>CA 证书</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="242"/>
        <source>no need for CA certificate</source>
        <translation>不需要 CA 证书</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="243"/>
        <source>User certificate</source>
        <translation>用户证书</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="244"/>
        <source>User private key</source>
        <translation>用户私钥</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="245"/>
        <source>User key password</source>
        <translation>用户密钥密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="246"/>
        <source>Password options</source>
        <translation>密码选项</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="247"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="255"/>
        <location filename="../frontend/netdetails/securitypage.h" line="125"/>
        <source>Required</source>
        <translation>必填</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="251"/>
        <source>Ineer authentication</source>
        <translation>内部认证</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="252"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="254"/>
        <source>Ask pwd each query</source>
        <translation>每次询问密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="258"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="271"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="274"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="277"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="297"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="379"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="522"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1048"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1129"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1160"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1182"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1205"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1260"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="259"/>
        <source>WPA&amp;WPA2 Personal</source>
        <translation>WPA&amp;WPA2 个人</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="260"/>
        <source>WPA&amp;WPA2 Enterprise</source>
        <translation>WPA&amp;WPA2 企业</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="261"/>
        <source>WPA3 Personal</source>
        <translation>WPA3 个人</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="272"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="275"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="278"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="298"/>
        <source>Choose from file...</source>
        <translation>从文件选择...</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store passwords only for this user</source>
        <translation>仅为该用户存储密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="281"/>
        <source>Store password only for this user</source>
        <translation>仅为该用户存储密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store passwords for all users</source>
        <translation>存储所有用户的密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="283"/>
        <source>Store password for all users</source>
        <translation>存储所有用户的密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask this password every time</source>
        <translation>每次询问这个密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="285"/>
        <source>Ask password every time</source>
        <translation>每次询问这个密码</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1150"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1173"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1195"/>
        <source>Choose a CA certificate</source>
        <translation>选择一个 CA 证书</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1151"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1174"/>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1196"/>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation>CA 证书 (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="290"/>
        <source>PAC provisioning</source>
        <translation>PAC 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="291"/>
        <source>Allow automatic PAC provisioning</source>
        <translation>允许自动 PAC 配置</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="292"/>
        <source>PAC file</source>
        <translation>PAC 文件</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="293"/>
        <source>Anonymous</source>
        <translation>匿名</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="294"/>
        <source>Authenticated</source>
        <translation>已认证</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="295"/>
        <source>Both</source>
        <translation>两者兼用</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1250"/>
        <source>Choose a PAC file</source>
        <translation>选择一个 PAC 文件</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.cpp" line="1251"/>
        <source>PAC Files (*.pac)</source>
        <translation>PAC 文件(*.pac)</translation>
    </message>
    <message>
        <location filename="../frontend/netdetails/securitypage.h" line="126"/>
        <source> </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="72"/>
        <source>Current Device</source>
        <translation>当前网卡</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="79"/>
        <source>Devices Closed!</source>
        <translation>设备关闭！</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="136"/>
        <source>Settings</source>
        <translation>网络设置</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="329"/>
        <source>Kylin NM</source>
        <translation>网络设置工具</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/tabpage.cpp" line="332"/>
        <source>kylin network applet desktop message</source>
        <translation>网络提示消息</translation>
    </message>
</context>
<context>
    <name>VpnPage</name>
    <message>
        <source>Wired Device not carried</source>
        <translation type="obsolete">未插入网线</translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../frontend/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="41"/>
        <source>WLAN Authentication</source>
        <translation>无线局域网认证</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="52"/>
        <source>Input WLAN Information Please</source>
        <translation>请输入无线局域网信息</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="53"/>
        <source>WLAN ID：</source>
        <translation>无线局域网ID：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="54"/>
        <source>WLAN Name:</source>
        <translation>WLAN名称：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="55"/>
        <source>Password：</source>
        <translation>密钥：</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="56"/>
        <source>Cancl</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../frontend/wificonfigdialog.cpp" line="57"/>
        <source>Ok</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>WlanListItem</name>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="72"/>
        <source>Not connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="177"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="204"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="637"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="656"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="666"/>
        <source>Disconnect</source>
        <translation>断开</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="179"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="208"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="304"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="647"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="664"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="188"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="671"/>
        <source>Forget</source>
        <translation>忘记此网络</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="187"/>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="676"/>
        <source>Property</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanlistitem.cpp" line="325"/>
        <source>Auto Connect</source>
        <translation>自动加入该网络</translation>
    </message>
</context>
<context>
    <name>WlanMoreItem</name>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/list-items/wlanmoreitem.cpp" line="28"/>
        <source>Add Others...</source>
        <translation>加入其他网络...</translation>
    </message>
</context>
<context>
    <name>WlanPage</name>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="130"/>
        <source>WLAN</source>
        <translation>无线局域网</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="106"/>
        <source>No wireless network card detected</source>
        <translation>未检测到无线网卡</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="132"/>
        <source>Activated WLAN</source>
        <translation>我的网络</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="143"/>
        <source>Other WLAN</source>
        <translation>其他网络</translation>
    </message>
    <message>
        <source>More...</source>
        <translation type="vanished">更多...</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="974"/>
        <source>WLAN Connected Successfully</source>
        <translation>无线网络已连接</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="970"/>
        <source>WLAN Disconnected Successfully</source>
        <translation>无线网络已断开</translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1750"/>
        <source>Connected: </source>
        <translation>已连接: </translation>
    </message>
    <message>
        <location filename="../frontend/tab-pages/wlanpage.cpp" line="1752"/>
        <source>Not Connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <source>(Limited)</source>
        <translation type="vanished">(网络受限)</translation>
    </message>
</context>
<context>
    <name>WpaWifiDialog</name>
    <message>
        <source>EAP type</source>
        <translation type="obsolete">EAP方法</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="obsolete">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">密钥</translation>
    </message>
    <message>
        <source>Ask pwd each query</source>
        <translation type="obsolete">每次询问密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
    <message>
        <source>Choose from file...</source>
        <translation type="obsolete">从文件选择...</translation>
    </message>
    <message>
        <source>Choose a CA certificate</source>
        <translation type="obsolete">选择一个CA证书</translation>
    </message>
    <message>
        <source>CA Files (*.pem *.der *.p12 *.crt *.cer *.pfx)</source>
        <translation type="obsolete">CA 证书 (*.pem *.der *.p12 *.crt *.cer *.pfx)</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="obsolete">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="obsolete">域</translation>
    </message>
    <message>
        <source>no need for CA certificate</source>
        <translation type="obsolete">不需要CA证书</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="118"/>
        <source>kylinnm</source>
        <translation>麒麟网络工具</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="122"/>
        <source>show kylin-nm wifi page</source>
        <translation>显示麒麟网络wifi页面</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="123"/>
        <source>show kylin-nm lan page</source>
        <translation>显示麒麟网络局域网页面</translation>
    </message>
</context>
</TS>
