<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <source>Open File</source>
        <translation type="vanished">打開</translation>
    </message>
    <message>
        <source>Save File</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>All Files (*)</source>
        <translation type="vanished">所有（*）</translation>
    </message>
</context>
<context>
    <name>KyNativeFileDialog</name>
    <message>
        <source>Go Back</source>
        <translation type="vanished">後退</translation>
    </message>
    <message>
        <source>Go Forward</source>
        <translation type="vanished">前進</translation>
    </message>
    <message>
        <source>Cd Up</source>
        <translation type="vanished">向上</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>View Type</source>
        <translation type="vanished">視圖類型</translation>
    </message>
    <message>
        <source>Sort Type</source>
        <translation type="vanished">排序類型</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">關閉</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">還原</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">檔名</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">打開</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存為</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">新建資料夾</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">目錄</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>exist, are you sure replace?</source>
        <translation type="vanished">已存在，是否替換？</translation>
    </message>
    <message>
        <source>NewFolder</source>
        <translation type="vanished">新建資料夾</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation type="vanished">撤銷</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation type="vanished">重做</translation>
    </message>
    <message>
        <source>warn</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>This operation is not supported.</source>
        <translation type="vanished">不支援此操作。</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="166"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../widget/messagebox/message-box.h" line="190"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>可執行檔「%1」 需要數量%2，找到數量%3。</translation>
    </message>
    <message>
        <location filename="../widget/messagebox/message-box.h" line="192"/>
        <source>Incompatible Qt Library Error</source>
        <translation>不相容的Qt庫錯誤</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="423"/>
        <location filename="../widget/messagebox/message-box.cpp" line="1106"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="479"/>
        <location filename="../widget/messagebox/message-box.cpp" line="897"/>
        <location filename="../widget/messagebox/message-box.cpp" line="1518"/>
        <source>Show Details...</source>
        <translation>顯示細節......</translation>
    </message>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="897"/>
        <location filename="../widget/messagebox/message-box.cpp" line="1518"/>
        <source>Hide Details...</source>
        <translation>隱藏細節......</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>File Name</source>
        <translation type="vanished">檔名稱</translation>
    </message>
    <message>
        <source>Modified Date</source>
        <translation type="vanished">修改日期</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation type="vanished">檔案類型</translation>
    </message>
    <message>
        <source>File Size</source>
        <translation type="vanished">檔大小</translation>
    </message>
    <message>
        <source>Original Path</source>
        <translation type="vanished">原始路徑</translation>
    </message>
    <message>
        <source>Descending</source>
        <translation type="vanished">降序</translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation type="vanished">升序</translation>
    </message>
    <message>
        <source>Use global sorting</source>
        <translation type="vanished">使用全域排序</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">清單檢視</translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="vanished">圖示檢視</translation>
    </message>
</context>
<context>
    <name>UKUI::TabWidget::DefaultSlideAnimatorFactory</name>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="50"/>
        <source>Default Slide</source>
        <translation>默認slide</translation>
    </message>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="51"/>
        <source>Let tab widget switch with a slide animation.</source>
        <translation>讓選項卡小部件切換為幻燈片動畫。</translation>
    </message>
</context>
</TS>
