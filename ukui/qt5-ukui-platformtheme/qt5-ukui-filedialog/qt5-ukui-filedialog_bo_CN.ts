<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="3081"/>
        <source>Open File</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3082"/>
        <source>Save File</source>
        <translation>ཡིག་ཆ་ཉར་ཚགས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3095"/>
        <source>All Files (*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད་(*)</translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="46"/>
        <source>bytes</source>
        <translation>ཡིག་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="47"/>
        <source>Bytes</source>
        <translation>ཡིག་ཚིགས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="50"/>
        <source>character</source>
        <translation>ཡི་གེའི་རྟགས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="51"/>
        <source>Character</source>
        <translation>ཡི་གེའི་རྟགས།</translation>
    </message>
    <message>
        <source>Saving &quot;%1&quot;</source>
        <translation type="vanished">&quot;%1&quot;ཉར་ཚགས་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>Saving failed, the reason is: %1</source>
        <translation>ཕམ་ཉེས་བྱུང་བའི་རྒྱུ་རྐྱེན་ནི་</translation>
    </message>
    <message>
        <source>Filename too long</source>
        <translation type="vanished">ཡིག་ཆའི་མིང་རིང་དྲགས་པ།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>Description: Save long file names to &quot;</source>
        <translation>ཞིབ་བརྗོད་བྱས་དོན། ཡིག་ཆ་རིང་པོའི་མིང་ཉར་ཚགས་བྱ་རྒྱུ་&quot;ཞེས་པར་བཟོ་བཅོས་</translation>
    </message>
    <message>
        <source>Copying &quot;%1&quot;</source>
        <translation type="vanished">&quot;བརྒྱ་ཆ་1&quot;འདྲ་བཟོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>To &quot;%1&quot;</source>
        <translation type="vanished">&quot;བརྒྱ་ཆ་1&quot;ལ་སླེབས་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Copying failed, the reason is: %1</source>
        <translation type="vanished">འདྲ་བཟོ་བྱས་པར་ཕམ་ཉེས་བྱུང་བའི་རྒྱུ་མཚན་ནི། བརྒྱ་ཆ་1ཡིན།</translation>
    </message>
    <message>
        <source>Moving &quot;%1&quot;</source>
        <translation type="vanished">&quot;བརྒྱ་ཆ་1&quot;སྤོ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Moving failed, the reason is: %1</source>
        <translation type="vanished">སྤོ་འགུལ་ཕམ་ཉེས་བྱུང་བའི་རྒྱུ་མཚན་གཤམ་གསལ།</translation>
    </message>
    <message>
        <source>File operation error:</source>
        <translation type="vanished">ཡིག་ཆ་བཀོལ་སྤྱོད་ཀྱི་ནོར་འཁྲུལ་ནི།</translation>
    </message>
    <message>
        <source>The reason is: %1</source>
        <translation type="vanished">རྒྱུ་མཚན་ནི་བརྒྱ་ཆ་1ཡོད་</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="91"/>
        <source>Truncation</source>
        <translation>བར་མཚམས་གཅོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="92"/>
        <source>Save to long file name directory</source>
        <translation>ཡིག་ཆའི་མིང་ཐོ་རིང་པོ་ཉར་ཚགས་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="93"/>
        <source>Rename</source>
        <translation>མིང་ཆེན་པོ་བཏགས་པ།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="286"/>
        <source>modify the name</source>
        <translation>མིང་བཟོ་བཅོས་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="287"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="293"/>
        <source>.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="288"/>
        <source>Explanation: When renaming a file name, ensure it is within %1 %2 and </source>
        <translation>དེའི་ཐོག་ནས་སྤྱོད་མཁན་གྱིས་ཡིག་ཆའི་མིང་བསྐྱར་དུ་བཏགས་ཏེ་བརྒྱ་ཆ་12ནང་ཚུན་དུ་འགན་ལེན་བྱ་དགོས་པ་དང་། </translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="126"/>
        <source>All applications</source>
        <translation>ཚང་མ་བེད་སྤྱོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>The file name is too long. </source>
        <translation>ཡིག་ཆའི་མིང་ཧ་ཅང་རིང་པོ་ཡོད། </translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="130"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="172"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="239"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="231"/>
        <source>Front truncation</source>
        <translation>སྔ་མ་ནས་བར་མཚམས་གཅོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="232"/>
        <source>Post truncation</source>
        <translation>རྗེས་སུ་གཅོད་མཚམས་གཅོད་དགོས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="131"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="173"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="240"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="69"/>
        <source>File &quot;%1&quot;</source>
        <translation>ཡིག་ཆ་&quot;བརྒྱ་ཆ་%1&quot;།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="71"/>
        <source>Please choose the following processing method:</source>
        <translation>གཤམ་གསལ་གྱི་ཐག་གཅོད་བྱེད་ཐབས་གདམ་གསེས་གནང་</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="292"/>
        <source>truncate interval</source>
        <translation>བར་མཚམས་བཅད་པ།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="294"/>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select </source>
        <translation>དེའི་ཐོག་ནས་སྤྱོད་མཁན་གྱིས་ཡིག་ཆའི་མིང་བསྐྱར་དུ་བཏགས་ཏེ་བརྒྱ་ཆ་12ནང་ཚུན་དུ་འགན་ལེན་བྱ་དགོས་པ་དང་། </translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select</source>
        <translation type="vanished">དེའི་ཐོག་ནས་སྤྱོད་མཁན་གྱིས་ཡིག་ཆའི་མིང་བསྐྱར་དུ་བཏགས་ཏེ་བརྒྱ་ཆ་12ནང་ཚུན་དུ་འགན་ལེན་བྱ་དགོས་པ་དང་།</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>&quot;.</source>
        <translation></translation>
    </message>
    <message>
        <source>Description: Save the file to &quot;%1/扩展&quot;.</source>
        <translation type="vanished">གསལ་བཤད་བྱས་དོན། ཡིག་ཆ་དེ་&quot;བརྒྱ་ཆ་1/1བར་དུ་ཉར་ཚགས་བྱེད་དགོས་&quot;ཞེས་གསལ་བཤད་བྱས་ཡོད།</translation>
    </message>
</context>
<context>
    <name>KyNativeFileDialog</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="282"/>
        <source>Go Back</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="289"/>
        <source>Go Forward</source>
        <translation>མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="295"/>
        <source>Cd Up</source>
        <translation>གོང་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="302"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="310"/>
        <source>View Type</source>
        <translation>མཐོང་རིས་ཀྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="321"/>
        <source>Sort Type</source>
        <translation>གོ་རིམ་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="326"/>
        <location filename="../kyfiledialog.cpp" line="444"/>
        <source>Maximize</source>
        <translation>ཆེས་ཆེ་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="340"/>
        <source>Close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="441"/>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1058"/>
        <source>Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1061"/>
        <location filename="../kyfiledialog.cpp" line="1698"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1062"/>
        <location filename="../kyfiledialog.cpp" line="1071"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1066"/>
        <source>Save as</source>
        <translation>ཉར་ཚགས་གཞན།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1068"/>
        <source>New Folder</source>
        <translation>ཡིག་ཁུག་གསར་འཛུགས།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1070"/>
        <location filename="../kyfiledialog.cpp" line="1702"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1106"/>
        <location filename="../kyfiledialog.cpp" line="1107"/>
        <location filename="../kyfiledialog.cpp" line="1109"/>
        <source>Directories</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <source>exist, are you sure replace?</source>
        <translation>གནས་ཡོད་པས། ཁྱོད་ཀྱིས་དངོས་གནས་ཚབ་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2064"/>
        <source>NewFolder</source>
        <translation>དཀར་ཆག་གསར་བ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2405"/>
        <source>Undo</source>
        <translation>ཁ་ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2412"/>
        <source>Redo</source>
        <translation>ཡང་བསྐྱར་ལས།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>warn</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>This operation is not supported.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>File save failed! </source>
        <translation>ཡིག་ཆ་ཉར་ཚགས་བྱས་ནས་ཕམ་ཉེས་བྱུང </translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>Close</source>
        <translation type="vanished">ཁ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="vanished">ལག་བསྟར་བྱེད་ཆོག་པའི་&apos;%1&apos;ལ་Qt %2,Qt%3རྙེད་པ་རེད།</translation>
    </message>
    <message>
        <source>Incompatible Qt Library Error</source>
        <translation type="vanished">ཕན་ཚུན་མཐུན་ཐབས་མེད་པའི་Qt དཔེ་མཛོད་ཁང་གི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <source>OK</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <source>Show Details...</source>
        <translation type="vanished">ཞིབ་ཕྲའི་གནས་ཚུལ་གསལ་བཤད་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation type="vanished">གནས་ཚུལ་ཞིབ་ཕྲ་སྦས་སྐུང་བྱེད་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="237"/>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="241"/>
        <source>Modified Date</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="245"/>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="249"/>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="253"/>
        <source>Original Path</source>
        <translation>ཐོག་མའི་འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="262"/>
        <source>Descending</source>
        <translation>མར་འབབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="267"/>
        <source>Ascending</source>
        <translation>རིམ་པ་ཇེ་མང་དུ་འགྲོ་བཞིན།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="273"/>
        <source>Use global sorting</source>
        <translation>ཁྱོན་ཡོངས་ཀྱི་གོ་རིམ་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="293"/>
        <source>List View</source>
        <translation>མཐོང་རིས་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="294"/>
        <source>Icon View</source>
        <translation>མཚོན་རྟགས་ལྟ་ཚུལ།</translation>
    </message>
</context>
<context>
    <name>UKUI::TabWidget::DefaultSlideAnimatorFactory</name>
    <message>
        <source>Default Slide</source>
        <translation type="vanished">ཁ་ཆད་བཞག་པའི་སྒྲོན་བརྙན</translation>
    </message>
    <message>
        <source>Let tab widget switch with a slide animation.</source>
        <translation type="vanished">སྒྲོན་བརྙན་གྱི་འགུལ་རིས་ལ་བརྟེན་ནས་ཤོག་བྱང་ཆུང་ཆུང་བརྗེ་རེས་བྱེད་དུ་འཇུག་དགོས།</translation>
    </message>
</context>
</TS>
