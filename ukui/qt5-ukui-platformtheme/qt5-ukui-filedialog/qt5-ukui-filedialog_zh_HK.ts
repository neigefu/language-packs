<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh">
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="3081"/>
        <source>Open File</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3082"/>
        <source>Save File</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3095"/>
        <source>All Files (*)</source>
        <translation>所有(*)</translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="46"/>
        <source>bytes</source>
        <translation>位元組</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="47"/>
        <source>Bytes</source>
        <translation>位元組</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="50"/>
        <source>character</source>
        <translation>字元</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="51"/>
        <source>Character</source>
        <translation>字元</translation>
    </message>
    <message>
        <source>Saving &quot;%1&quot;</source>
        <translation type="vanished">正在儲存&quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>Saving failed, the reason is: %1</source>
        <translation>儲存失敗， 原因： %1</translation>
    </message>
    <message>
        <source>Filename too long</source>
        <translation type="vanished">檔名過長</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>Description: Save long file names to &quot;</source>
        <translation>说明：将长文件名文件保存至</translation>
    </message>
    <message>
        <source>Copying &quot;%1&quot;</source>
        <translation type="vanished">複製%1</translation>
    </message>
    <message>
        <source>To &quot;%1&quot;</source>
        <translation type="vanished">到%1”</translation>
    </message>
    <message>
        <source>Copying failed, the reason is: %1</source>
        <translation type="vanished">複製失敗， 原因： %1</translation>
    </message>
    <message>
        <source>Moving &quot;%1&quot;</source>
        <translation type="vanished">正在移動%1</translation>
    </message>
    <message>
        <source>Moving failed, the reason is: %1</source>
        <translation type="vanished">移動失敗， 原因： %1</translation>
    </message>
    <message>
        <source>File operation error:</source>
        <translation type="vanished">檔案操作錯誤：</translation>
    </message>
    <message>
        <source>The reason is: %1</source>
        <translation type="vanished">原因： %1</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="91"/>
        <source>Truncation</source>
        <translation>截斷</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="92"/>
        <source>Save to long file name directory</source>
        <translation>保存至長檔名目錄</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="93"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="286"/>
        <source>modify the name</source>
        <translation>修改命名</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="287"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="293"/>
        <source>.</source>
        <translation>。</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="288"/>
        <source>Explanation: When renaming a file name, ensure it is within %1 %2 and </source>
        <translation>說明：使用者重命名檔名，保證在 %1 %2 以內，去 </translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="126"/>
        <source>All applications</source>
        <translation>全部應用</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>The file name is too long. </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="130"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="172"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="239"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">應用</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="231"/>
        <source>Front truncation</source>
        <translation>前截斷</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="232"/>
        <source>Post truncation</source>
        <translation>後截斷</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="131"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="173"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="240"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="69"/>
        <source>File &quot;%1&quot;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="71"/>
        <source>Please choose the following processing method:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="292"/>
        <source>truncate interval</source>
        <translation>截斷區間</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="294"/>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select </source>
        <translation>說明：使用者重命名檔名，保證在 %1 %2 以內，去</translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select</source>
        <translation type="vanished">說明：使用者重命名檔名，保證在 %1 %2 以內，去</translation>
    </message>
    <message>
        <source>Description: Save the file to &quot;</source>
        <translation type="vanished">說明：將檔保存至“</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>&quot;.</source>
        <translation>”。</translation>
    </message>
    <message>
        <source>Description: Save the file to &quot;%1/扩展&quot;.</source>
        <translation type="vanished">說明：將檔保存至“%1/擴展”。</translation>
    </message>
    <message>
        <source>Description: By default, save to &quot;%1/扩展&quot;.</source>
        <translation type="vanished">说明：将文件保存至“%1/扩展”。</translation>
    </message>
</context>
<context>
    <name>KyNativeFileDialog</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="282"/>
        <source>Go Back</source>
        <translation>後退</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="289"/>
        <source>Go Forward</source>
        <translation>前進</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="295"/>
        <source>Cd Up</source>
        <translation>向上</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="302"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="310"/>
        <source>View Type</source>
        <translation>視圖類型</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="321"/>
        <source>Sort Type</source>
        <translation>排序類型</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="326"/>
        <location filename="../kyfiledialog.cpp" line="444"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="340"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="441"/>
        <source>Restore</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1058"/>
        <source>Name</source>
        <translation>檔名</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1061"/>
        <location filename="../kyfiledialog.cpp" line="1698"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1062"/>
        <location filename="../kyfiledialog.cpp" line="1071"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1066"/>
        <source>Save as</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1068"/>
        <source>New Folder</source>
        <translation>新建資料夾</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1070"/>
        <location filename="../kyfiledialog.cpp" line="1702"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1106"/>
        <location filename="../kyfiledialog.cpp" line="1107"/>
        <location filename="../kyfiledialog.cpp" line="1109"/>
        <source>Directories</source>
        <translation>目錄</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <source>exist, are you sure replace?</source>
        <translation>已存在，是否替換？</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2064"/>
        <source>NewFolder</source>
        <translation>新建資料夾</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2405"/>
        <source>Undo</source>
        <translation>撤銷</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2412"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>This operation is not supported.</source>
        <translation>不支援此操作。</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>File save failed! </source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <source>Show Details...</source>
        <translation type="vanished">显示细节……</translation>
    </message>
    <message>
        <source>Hide Details...</source>
        <translation type="vanished">隐藏细节……</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="237"/>
        <source>File Name</source>
        <translation>檔名稱</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="241"/>
        <source>Modified Date</source>
        <translation>修改日期</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="245"/>
        <source>File Type</source>
        <translation>檔案類型</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="249"/>
        <source>File Size</source>
        <translation>檔大小</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="253"/>
        <source>Original Path</source>
        <translation>原始路徑</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="262"/>
        <source>Descending</source>
        <translation>降序</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="267"/>
        <source>Ascending</source>
        <translation>升序</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="273"/>
        <source>Use global sorting</source>
        <translation>使用全域排序</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="293"/>
        <source>List View</source>
        <translation>清單檢視</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="294"/>
        <source>Icon View</source>
        <translation>圖示檢視</translation>
    </message>
</context>
</TS>
