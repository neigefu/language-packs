<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="3081"/>
        <source>Open File</source>
        <translation>Open File</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3082"/>
        <source>Save File</source>
        <translation>Save File</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3095"/>
        <source>All Files (*)</source>
        <translation>All Files (*)</translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="46"/>
        <source>bytes</source>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="47"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="50"/>
        <source>character</source>
        <translation>character</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="51"/>
        <source>Character</source>
        <translation>Character</translation>
    </message>
    <message>
        <source>Saving &quot;%1&quot;</source>
        <translation type="vanished">Saving &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>Saving failed, the reason is: %1</source>
        <translation>Saving failed, the reason is: %1</translation>
    </message>
    <message>
        <source>Filename too long</source>
        <translation type="vanished">Filename too long</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>Description: Save long file names to &quot;</source>
        <translation>Description: Save long file names to &quot;</translation>
    </message>
    <message>
        <source>Copying &quot;%1&quot;</source>
        <translation type="vanished">Copying &quot;%1&quot;</translation>
    </message>
    <message>
        <source>To &quot;%1&quot;</source>
        <translation type="vanished">To &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Copying failed, the reason is: %1</source>
        <translation type="vanished">Copying failed, the reason is: %1</translation>
    </message>
    <message>
        <source>Moving &quot;%1&quot;</source>
        <translation type="vanished">Moving &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Moving failed, the reason is: %1</source>
        <translation type="vanished">Moving failed, the reason is: %1</translation>
    </message>
    <message>
        <source>File operation error:</source>
        <translation type="vanished">File operation error:</translation>
    </message>
    <message>
        <source>The reason is: %1</source>
        <translation type="vanished">The reason is: %1</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="91"/>
        <source>Truncation</source>
        <translation>Truncation</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="92"/>
        <source>Save to long file name directory</source>
        <translation>Save to long file name directory</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="93"/>
        <source>Rename</source>
        <translation>Rename</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="286"/>
        <source>modify the name</source>
        <translation>modify the name</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="287"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="293"/>
        <source>.</source>
        <translation>.</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="288"/>
        <source>Explanation: When renaming a file name, ensure it is within %1 %2 and </source>
        <translation>Explanation: When renaming a file name, ensure it is within %1 %2 and</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="126"/>
        <source>All applications</source>
        <translation>All applications</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>The file name is too long. </source>
        <translation>The file name is too long. </translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="130"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="172"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="239"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">Save</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="231"/>
        <source>Front truncation</source>
        <translation>Front truncation</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="232"/>
        <source>Post truncation</source>
        <translation>Post truncation</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="131"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="173"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="240"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="69"/>
        <source>File &quot;%1&quot;</source>
        <translation>File &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Filename too long. </source>
        <translation type="obsolete">Filename too long. </translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="71"/>
        <source>Please choose the following processing method:</source>
        <translation>Please choose the following processing method:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="292"/>
        <source>truncate interval</source>
        <translation>truncate interval</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="294"/>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select </source>
        <translation>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select </translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select</source>
        <translation type="vanished">Explanation: Truncate the portion of the file name that exceeds %1 %2 and select</translation>
    </message>
    <message>
        <source>Description: Save the file to &quot;</source>
        <translation type="vanished">Description: Save the file to &quot;</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>&quot;.</source>
        <translation>&quot;.</translation>
    </message>
    <message>
        <source>Description: Save the file to &quot;%1/扩展&quot;.</source>
        <translation type="vanished">Description: Save the file to &quot;%1/扩展&quot;.</translation>
    </message>
</context>
<context>
    <name>KyNativeFileDialog</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="282"/>
        <source>Go Back</source>
        <translation>Go Back</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="289"/>
        <source>Go Forward</source>
        <translation>Go Forward</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="295"/>
        <source>Cd Up</source>
        <translation>Cd Up</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="302"/>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="310"/>
        <source>View Type</source>
        <translation>View Type</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="321"/>
        <source>Sort Type</source>
        <translation>Sort Type</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="326"/>
        <location filename="../kyfiledialog.cpp" line="444"/>
        <source>Maximize</source>
        <translation>Maximize</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="340"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="441"/>
        <source>Restore</source>
        <translation>Restore</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1058"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1061"/>
        <location filename="../kyfiledialog.cpp" line="1698"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1062"/>
        <location filename="../kyfiledialog.cpp" line="1071"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1066"/>
        <source>Save as</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1068"/>
        <source>New Folder</source>
        <translation>New Folder</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1070"/>
        <location filename="../kyfiledialog.cpp" line="1702"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1106"/>
        <location filename="../kyfiledialog.cpp" line="1107"/>
        <location filename="../kyfiledialog.cpp" line="1109"/>
        <source>Directories</source>
        <translation>Directories</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <source>exist, are you sure replace?</source>
        <translation>exist, are you sure replace?</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2064"/>
        <source>NewFolder</source>
        <translation>NewFolder</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2405"/>
        <source>Undo</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2412"/>
        <source>Redo</source>
        <translation>Redo</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>warn</source>
        <translation>warn</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>This operation is not supported.</source>
        <translation>This operation is not supported.</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>File save failed! </source>
        <translation>File save failed! </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="237"/>
        <source>File Name</source>
        <translation>File Name</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="241"/>
        <source>Modified Date</source>
        <translation>Modified Date</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="245"/>
        <source>File Type</source>
        <translation>File Type</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="249"/>
        <source>File Size</source>
        <translation>File Size</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="253"/>
        <source>Original Path</source>
        <translation>Original Path</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="262"/>
        <source>Descending</source>
        <translation>Descending</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="267"/>
        <source>Ascending</source>
        <translation>Ascending</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="273"/>
        <source>Use global sorting</source>
        <translation>Use global sorting</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="293"/>
        <source>List View</source>
        <translation>List View</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="294"/>
        <source>Icon View</source>
        <translation>Icon View</translation>
    </message>
</context>
</TS>
