<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="3081"/>
        <source>Open File</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3082"/>
        <source>Save File</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="3095"/>
        <source>All Files (*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ (*)</translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="46"/>
        <source>bytes</source>
        <translation>ᠪᠸᠲ᠋ᠠ</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="47"/>
        <source>Bytes</source>
        <translation>ᠪᠸᠲ᠋ᠠ</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="50"/>
        <source>character</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="51"/>
        <source>Character</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Saving &quot;%1&quot;</source>
        <translation type="vanished">ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>Saving failed, the reason is: %1</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠯ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠰᠢᠯᠲᠠᠭᠠᠨ᠄ 1᠃</translation>
    </message>
    <message>
        <source>Filename too long</source>
        <translation type="vanished">ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠬᠡᠲᠦᠷᠬᠡᠢ ᠤᠷ</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>Description: Save long file names to &quot;</source>
        <translation>ᠳᠦᠷᠰᠦᠯᠡᠭᠰᠡᠨ ᠨᠢ  ᠤᠷᠲᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠬᠠᠲᠠᠭᠠᠯᠠᠶ᠎ᠠ  ᠭᠡᠵᠡᠢ</translation>
    </message>
    <message>
        <source>Copying &quot;%1&quot;</source>
        <translation type="vanished">%1&quot;᠎ᠶ᠋ᠢ ᠺᠤᠫᠢᠳᠠᠵᠤ ᠪᠣᠢ</translation>
    </message>
    <message>
        <source>To &quot;%1&quot;</source>
        <translation type="vanished">&quot;%1&quot; ᠪᠣᠯᠬᠤ ᠳᠤ ᠬᠦᠷᠲᠡᠯ᠎ᠡ</translation>
    </message>
    <message>
        <source>Copying failed, the reason is: %1</source>
        <translation type="vanished">ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠯ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠰᠢᠯᠲᠠᠭᠠᠨ᠄ 1᠃</translation>
    </message>
    <message>
        <source>Moving &quot;%1&quot;</source>
        <translation type="vanished">ᠶᠢᠡᠨ %1&quot;᠎ᠶ᠋ᠢ ᠰᠢᠯᠵᠢᠭᠦᠯᠵᠦ ᠪᠣᠢ</translation>
    </message>
    <message>
        <source>Moving failed, the reason is: %1</source>
        <translation type="vanished">ᠰᠢᠯᠵᠢᠮᠡᠯ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠤ ᠰᠢᠯᠲᠠᠭᠠᠨ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <source>File operation error:</source>
        <translation type="vanished">ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>The reason is: %1</source>
        <translation type="vanished">ᠰᠢᠯᠲᠠᠭᠠᠨ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="91"/>
        <source>Truncation</source>
        <translation>ᠲᠠᠰᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="92"/>
        <source>Save to long file name directory</source>
        <translation>ᠤᠷᠲᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷᠡᠰ ᠦᠨ ᠭᠠᠷᠴᠠᠭ ᠲᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="93"/>
        <source>Rename</source>
        <translation>ᠬᠦᠨᠳᠦ ᠨᠡᠷᠡᠶᠢᠳᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="286"/>
        <source>modify the name</source>
        <translation>ᠨᠡᠷᠡᠶᠢᠳᠬᠦ ᠶᠢ ᠵᠠᠰᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="287"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="293"/>
        <source>.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="288"/>
        <source>Explanation: When renaming a file name, ensure it is within %1 %2 and </source>
        <translation>ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠯᠲᠠ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠨᠡᠷᠡᠶᠢᠳᠦᠭᠰᠡᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠬᠦᠨᠳᠦᠷᠡᠭᠦᠯᠵᠦ ᠂ 1% 2 ᠶᠢᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠂ ᠣᠴᠢᠬᠤ ᠶᠢ ᠪᠠᠲᠤᠯᠠᠨ᠎ᠠ ᠃ </translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="126"/>
        <source>All applications</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="70"/>
        <source>The file name is too long. </source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠬᠡᠲᠦᠷᠬᠡᠢ ᠤᠷ </translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="130"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="172"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="239"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="231"/>
        <source>Front truncation</source>
        <translation>ᠡᠮᠦᠨ᠎ᠡ ᠲᠠᠰᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="232"/>
        <source>Post truncation</source>
        <translation>ᠬᠣᠢ᠌ᠨ᠎ᠠ ᠠᠴᠠ ᠲᠠᠰᠤᠷᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="131"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="173"/>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="240"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="69"/>
        <source>File &quot;%1&quot;</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ %1</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="71"/>
        <source>Please choose the following processing method:</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠮᠡᠲᠦ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠶᠢ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ᠄</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="292"/>
        <source>truncate interval</source>
        <translation>ᠣᠷᠣᠨ ᠵᠠᠢ ᠶᠢ ᠲᠠᠰᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="294"/>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select </source>
        <translation>ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠯᠲᠠ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠨᠡᠷᠡᠶᠢᠳᠦᠭᠰᠡᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠬᠦᠨᠳᠦᠷᠡᠭᠦᠯᠵᠦ ᠂ 1% 2 ᠶᠢᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠂ ᠣᠴᠢᠬᠤ ᠶᠢ ᠪᠠᠲᠤᠯᠠᠨ᠎ᠠ ᠃ </translation>
    </message>
    <message>
        <source>Explanation: Truncate the portion of the file name that exceeds %1 %2 and select</source>
        <translation type="vanished">ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠯᠲᠠ ᠄ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠨᠡᠷᠡᠶᠢᠳᠦᠭᠰᠡᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠬᠦᠨᠳᠦᠷᠡᠭᠦᠯᠵᠦ ᠂ 1% 2 ᠶᠢᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠂ ᠣᠴᠢᠬᠤ ᠶᠢ ᠪᠠᠲᠤᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog/kyfiledialogrename.cpp" line="298"/>
        <source>&quot;.</source>
        <translation></translation>
    </message>
    <message>
        <source>Description: Save the file to &quot;%1/扩展&quot;.</source>
        <translation type="vanished">ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠯᠲᠠ ᠄ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ 《 1/ᠥᠷᠭᠡᠳᠬᠡᠬᠦ 》 ᠪᠣᠯᠭᠠᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KyNativeFileDialog</name>
    <message>
        <location filename="../kyfiledialog.cpp" line="282"/>
        <source>Go Back</source>
        <translation>ᠤᠬᠤᠷᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="289"/>
        <source>Go Forward</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="295"/>
        <source>Cd Up</source>
        <translation>ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="302"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="310"/>
        <source>View Type</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="321"/>
        <source>Sort Type</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="326"/>
        <location filename="../kyfiledialog.cpp" line="444"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="340"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="441"/>
        <source>Restore</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1058"/>
        <source>Name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1061"/>
        <location filename="../kyfiledialog.cpp" line="1698"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1062"/>
        <location filename="../kyfiledialog.cpp" line="1071"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1066"/>
        <source>Save as</source>
        <translation>ᠦᠭᠡᠷ᠎ᠡ ᠭᠠᠵᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1068"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1070"/>
        <location filename="../kyfiledialog.cpp" line="1702"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1106"/>
        <location filename="../kyfiledialog.cpp" line="1107"/>
        <location filename="../kyfiledialog.cpp" line="1109"/>
        <source>Directories</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="1299"/>
        <source>exist, are you sure replace?</source>
        <translation>ᠪᠠᠢᠨ᠎ᠠ᠂ ᠰᠤᠯᠢᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2064"/>
        <source>NewFolder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2405"/>
        <source>Undo</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2412"/>
        <source>Redo</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠬᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>warn</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2609"/>
        <source>This operation is not supported.</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../kyfiledialog.cpp" line="2878"/>
        <location filename="../kyfiledialog.cpp" line="2887"/>
        <location filename="../kyfiledialog.cpp" line="2907"/>
        <location filename="../kyfiledialog.cpp" line="2914"/>
        <source>File save failed! </source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ! </translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="vanished">ᠭᠦᠢᠴᠡᠳᠬᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠹᠠᠢᠯ &apos;%1&apos; Qt% 2 ᠬᠡᠷᠡᠭᠰᠡᠵᠦ ᠂ Qt% 3 ᠢ᠋/ ᠵᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ .</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="237"/>
        <source>File Name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="241"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="245"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="249"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="253"/>
        <source>Original Path</source>
        <translation>ᠤᠭ ᠤ᠋ᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="262"/>
        <source>Descending</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="267"/>
        <source>Ascending</source>
        <translation>ᠶᠡᠬᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="273"/>
        <source>Use global sorting</source>
        <translation>ᠪᠦᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="293"/>
        <source>List View</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../ui_kyfiledialog.cpp" line="294"/>
        <source>Icon View</source>
        <translation>ᠰᠢᠪᠠᠭ᠎ᠠ ᠵᠢᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
</context>
</TS>
