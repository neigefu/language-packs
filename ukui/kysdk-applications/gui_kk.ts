<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>kdk::KAboutDialog</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="122"/>
        <source>Your system does not have any email application installed or the default mail application is not set up</source>
        <translation>Жүйеңізде орнатылған электрондық пошта бағдарламалары жоқ немесе әдепкі пошта бағдарламасының жиынтығы жоқ</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="123"/>
        <source>Unable to open mail application</source>
        <translation>Пошта қолданбасы ашылмады</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="257"/>
        <location filename="../src/kaboutdialog.cpp" line="271"/>
        <source>Service &amp; Support: </source>
        <translatorcomment>服务与支持团队：</translatorcomment>
        <translation>Қызмет және қолдау: </translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="263"/>
        <location filename="../src/kaboutdialog.cpp" line="277"/>
        <source>Privacy statement</source>
        <translation>Құпиялылық туралы мәлімдеме</translation>
    </message>
</context>
<context>
    <name>kdk::KAboutDialogPrivate</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="381"/>
        <source>version :</source>
        <translation>нұсқасы:</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="386"/>
        <source>Version number not found</source>
        <translation>Нұсқа нөмірі табылмады</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidget</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="102"/>
        <source>Select or drag and drop the folder identification path</source>
        <translation>Қалтаны сәйкестендіру жолын таңдау немесе апару және тастау</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidgetPrivate</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="62"/>
        <source>Please select file</source>
        <translation>Файлды таңдауыңызды сұраймын</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialog</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="241"/>
        <source>Enter a value:</source>
        <translation>Мәнді енгізіңіз:</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialogPrivate</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="250"/>
        <source>ok</source>
        <translation>Жақсы</translation>
    </message>
    <message>
        <location filename="../src/kinputdialog.cpp" line="252"/>
        <source>cancel</source>
        <translation>Болдырмау</translation>
    </message>
</context>
<context>
    <name>kdk::KMenuButton</name>
    <message>
        <location filename="../src/kmenubutton.cpp" line="67"/>
        <source>Options</source>
        <translation>Параметрлер</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="68"/>
        <source>Setting</source>
        <translatorcomment>设置</translatorcomment>
        <translation>Баптау</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="69"/>
        <source>Theme</source>
        <translation>Тақырып</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="70"/>
        <source>Assist</source>
        <translation>Көмек көрсету</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="71"/>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="72"/>
        <source>Quit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="79"/>
        <source>Auto</source>
        <translation>Авто</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="81"/>
        <source>Light</source>
        <translation>Жарық</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="83"/>
        <source>Dark</source>
        <translation>Қараңғы</translation>
    </message>
</context>
<context>
    <name>kdk::KProgressDialog</name>
    <message>
        <location filename="../src/kprogressdialog.cpp" line="70"/>
        <source>cancel</source>
        <translation>Болдырмау</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEditPrivate</name>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="410"/>
        <location filename="../src/ksearchlineedit.cpp" line="533"/>
        <source>Search</source>
        <translation>Іздеу</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBar</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="62"/>
        <source>Low</source>
        <translation>Төмен</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="65"/>
        <source>Medium</source>
        <translation>Орташа</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="68"/>
        <source>High</source>
        <translation>Жоғары</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBarPrivate</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="179"/>
        <source>Low</source>
        <translation>Төмен</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialog</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="124"/>
        <location filename="../src/kuninstalldialog.cpp" line="216"/>
        <source>uninstall</source>
        <translatorcomment>立即卸载</translatorcomment>
        <translation>жою</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="185"/>
        <source>deb name:</source>
        <translation>деб атауы:</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="190"/>
        <source>deb version:</source>
        <translation>deb нұсқасы:</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialogPrivate</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="305"/>
        <source>deb name:</source>
        <translation>деб атауы:</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="309"/>
        <source>deb version:</source>
        <translation>deb нұсқасы:</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBar</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="156"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="178"/>
        <source>Maximize</source>
        <translation>Барынша көбейту</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="162"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="183"/>
        <source>Restore</source>
        <translation>Қалпына келтіру</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBarPrivate</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="271"/>
        <source>Minimize</source>
        <translation>Кішірейту</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="280"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="341"/>
        <source>Maximize</source>
        <translation>Барынша көбейту</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="346"/>
        <source>Restore</source>
        <translation>Қалпына келтіру</translation>
    </message>
</context>
</TS>
