<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>kdk::KAboutDialog</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="122"/>
        <source>Your system does not have any email application installed or the default mail application is not set up</source>
        <translation>您的系统未安装任何电子邮件应用或未设置默认邮件应用</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="123"/>
        <source>Unable to open mail application</source>
        <translation>无法打开邮件客户端</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="257"/>
        <location filename="../src/kaboutdialog.cpp" line="271"/>
        <source>Service &amp; Support: </source>
        <translatorcomment>服务与支持团队：</translatorcomment>
        <translation>服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="263"/>
        <location filename="../src/kaboutdialog.cpp" line="277"/>
        <source>Privacy statement</source>
        <translation>《隐私声明》</translation>
    </message>
</context>
<context>
    <name>kdk::KAboutDialogPrivate</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="381"/>
        <source>version :</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="386"/>
        <source>Version number not found</source>
        <translation>未找到对应版本号</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidget</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="102"/>
        <source>Select or drag and drop the folder identification path</source>
        <translation>选择或拖放文件夹标识路径</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidgetPrivate</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="62"/>
        <source>Please select file</source>
        <translation>请选择文件</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialog</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="241"/>
        <source>Enter a value:</source>
        <translation>请输入数值：</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialogPrivate</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="250"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/kinputdialog.cpp" line="252"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>kdk::KMenuButton</name>
    <message>
        <location filename="../src/kmenubutton.cpp" line="67"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="68"/>
        <source>Setting</source>
        <translatorcomment>设置</translatorcomment>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="69"/>
        <source>Theme</source>
        <translation>外观</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="70"/>
        <source>Assist</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="71"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="72"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="79"/>
        <source>Auto</source>
        <translation>跟随系统</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="81"/>
        <source>Light</source>
        <translation>浅色</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="83"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
</context>
<context>
    <name>kdk::KProgressDialog</name>
    <message>
        <location filename="../src/kprogressdialog.cpp" line="70"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEditPrivate</name>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="410"/>
        <location filename="../src/ksearchlineedit.cpp" line="533"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBar</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="62"/>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="65"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="68"/>
        <source>High</source>
        <translation>高</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBarPrivate</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="179"/>
        <source>Low</source>
        <translation>低</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialog</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="124"/>
        <location filename="../src/kuninstalldialog.cpp" line="216"/>
        <source>uninstall</source>
        <translatorcomment>立即卸载</translatorcomment>
        <translation>立即卸载</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="185"/>
        <source>deb name:</source>
        <translation>包名：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="190"/>
        <source>deb version:</source>
        <translation>版本：</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialogPrivate</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="305"/>
        <source>deb name:</source>
        <translation>包名：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="309"/>
        <source>deb version:</source>
        <translation>版本：</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBar</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="156"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="178"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="162"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="183"/>
        <source>Restore</source>
        <translation>还原</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBarPrivate</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="271"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="280"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="341"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="346"/>
        <source>Restore</source>
        <translation>还原</translation>
    </message>
</context>
</TS>
