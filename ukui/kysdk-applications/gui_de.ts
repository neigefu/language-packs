<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>kdk::KAboutDialog</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="122"/>
        <source>Your system does not have any email application installed or the default mail application is not set up</source>
        <translation>Sie haben keine E-Mail-Apps auf Ihrem System installiert oder keine Standard-Mail-App festgelegt</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="123"/>
        <source>Unable to open mail application</source>
        <translation>E-Mail-Anwendung kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="257"/>
        <location filename="../src/kaboutdialog.cpp" line="271"/>
        <source>Service &amp; Support: </source>
        <translatorcomment>服务与支持团队：</translatorcomment>
        <translation>Service &amp; Support: </translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="263"/>
        <location filename="../src/kaboutdialog.cpp" line="277"/>
        <source>Privacy statement</source>
        <translation>Datenschutzerklärung</translation>
    </message>
</context>
<context>
    <name>kdk::KAboutDialogPrivate</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="381"/>
        <source>version :</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="386"/>
        <source>Version number not found</source>
        <translation>Versionsnummer nicht gefunden</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidget</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="102"/>
        <source>Select or drag and drop the folder identification path</source>
        <translation>Wählen Sie den Ordneridentifikationspfad aus oder ziehen Sie ihn per Drag &amp; Drop</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidgetPrivate</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="62"/>
        <source>Please select file</source>
        <translation>Bitte wählen Sie eine Datei aus</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialog</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="241"/>
        <source>Enter a value:</source>
        <translation>Geben Sie einen Wert ein:</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialogPrivate</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="250"/>
        <source>ok</source>
        <translation>Okay</translation>
    </message>
    <message>
        <location filename="../src/kinputdialog.cpp" line="252"/>
        <source>cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>kdk::KMenuButton</name>
    <message>
        <location filename="../src/kmenubutton.cpp" line="67"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="68"/>
        <source>Setting</source>
        <translatorcomment>设置</translatorcomment>
        <translation>Einstellung</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="69"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="70"/>
        <source>Assist</source>
        <translation>Helfen</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="71"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="72"/>
        <source>Quit</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="79"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="81"/>
        <source>Light</source>
        <translation>Licht</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="83"/>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
</context>
<context>
    <name>kdk::KProgressDialog</name>
    <message>
        <location filename="../src/kprogressdialog.cpp" line="70"/>
        <source>cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEditPrivate</name>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="410"/>
        <location filename="../src/ksearchlineedit.cpp" line="533"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBar</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="62"/>
        <source>Low</source>
        <translation>Niedrig</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="65"/>
        <source>Medium</source>
        <translation>Mittel</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="68"/>
        <source>High</source>
        <translation>Hoch</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBarPrivate</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="179"/>
        <source>Low</source>
        <translation>Niedrig</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialog</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="124"/>
        <location filename="../src/kuninstalldialog.cpp" line="216"/>
        <source>uninstall</source>
        <translatorcomment>立即卸载</translatorcomment>
        <translation>Deinstallieren</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="185"/>
        <source>deb name:</source>
        <translation>Deb Name:</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="190"/>
        <source>deb version:</source>
        <translation>Deb-Version:</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialogPrivate</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="305"/>
        <source>deb name:</source>
        <translation>Deb Name:</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="309"/>
        <source>deb version:</source>
        <translation>Deb-Version:</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBar</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="156"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="178"/>
        <source>Maximize</source>
        <translation>Maximieren</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="162"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="183"/>
        <source>Restore</source>
        <translation>Wiederherstellen</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBarPrivate</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="271"/>
        <source>Minimize</source>
        <translation>Minimieren</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="280"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="341"/>
        <source>Maximize</source>
        <translation>Maximieren</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="346"/>
        <source>Restore</source>
        <translation>Wiederherstellen</translation>
    </message>
</context>
</TS>
