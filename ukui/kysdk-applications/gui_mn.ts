<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>kdk::KAboutDialog</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="122"/>
        <source>Your system does not have any email application installed or the default mail application is not set up</source>
        <translation>ᠲᠠᠨ ᠤ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠦ ᠶᠠᠮᠠᠷ ᠴᠤ ᠡᠯᠧᠺᠲ᠋ᠷᠣᠨ ᠰᠢᠤᠳᠠᠨ ᠤ ᠵᠦᠢᠯ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠤᠶᠤ ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠬᠤ ᠰᠢᠤᠳᠠᠨ ᠤ ᠵᠦᠢᠯ ᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠬᠢᠭᠰᠡᠨ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="123"/>
        <source>Unable to open mail application</source>
        <translation>ᠰᠢᠤᠳᠠᠨ ᠤ ᠵᠦᠢᠯ ᠦᠨ ᠵᠣᠴᠢᠨ ᠡᠷᠦᠬᠡ ᠶᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="257"/>
        <location filename="../src/kaboutdialog.cpp" line="271"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ：</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="263"/>
        <location filename="../src/kaboutdialog.cpp" line="277"/>
        <source>Privacy statement</source>
        <translation>《ᠨᠢᠭᠤᠴᠠ ᠶᠢᠨ ᠢᠯᠡᠷᠬᠡᠶᠢᠯᠡᠯᠲᠡ》</translation>
    </message>
</context>
<context>
    <name>kdk::KAboutDialogPrivate</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="381"/>
        <source>version :</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="386"/>
        <source>Version number not found</source>
        <translation>ᠵᠣᠬᠢᠴᠠᠩᠭᠤᠢ ᠬᠡᠪᠯᠡᠯ ᠦᠨ ᠨᠣᠮᠧᠷ ᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidget</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="102"/>
        <source>Select or drag and drop the folder identification path</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠠᠪᠤᠳᠠᠷ ᠢ ᠰᠣᠩᠭᠣᠬᠤ ᠪᠤᠶᠤ ᠲᠠᠯᠪᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
</context>
<context>
    <name>kdk::KDragWidgetPrivate</name>
    <message>
        <location filename="../src/kdragwidget.cpp" line="62"/>
        <source>Please select file</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialog</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="241"/>
        <source>Enter a value:</source>
        <translation>ᠲᠤᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ：</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialogPrivate</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="250"/>
        <source>ok</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kinputdialog.cpp" line="252"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>kdk::KMenuButton</name>
    <message>
        <location filename="../src/kmenubutton.cpp" line="67"/>
        <source>Options</source>
        <translation>ᠰᠣᠩᠭᠣᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="68"/>
        <source>Setting</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="69"/>
        <source>Theme</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="70"/>
        <source>Assist</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="71"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="72"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="79"/>
        <source>Auto</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠢ ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="81"/>
        <source>Light</source>
        <translation>ᠬᠦᠶᠦᠬᠡᠨ ᠦᠩᠭᠡ ᠲᠠᠢ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="83"/>
        <source>Dark</source>
        <translation>ᠬᠦᠨ ᠦᠩᠭᠡ ᠲᠠᠢ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
</context>
<context>
    <name>kdk::KProgressDialog</name>
    <message>
        <location filename="../src/kprogressdialog.cpp" line="70"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEditPrivate</name>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="410"/>
        <location filename="../src/ksearchlineedit.cpp" line="533"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBar</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="62"/>
        <source>Low</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="65"/>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="68"/>
        <source>High</source>
        <translation>ᠦᠨᠳᠦᠷ</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBarPrivate</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="179"/>
        <source>Low</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialog</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="124"/>
        <location filename="../src/kuninstalldialog.cpp" line="216"/>
        <source>uninstall</source>
        <translation>ᠲᠡᠷᠡ ᠳᠠᠷᠤᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="185"/>
        <source>deb name:</source>
        <translation>ᠪᠠᠭᠳᠠᠬᠤ ᠨᠡᠷ᠎ᠡ ：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="190"/>
        <source>deb version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ：</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialogPrivate</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="305"/>
        <source>deb name:</source>
        <translation>ᠪᠠᠭᠳᠠᠬᠤ ᠨᠡᠷ᠎ᠡ：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="309"/>
        <source>deb version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ：</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBar</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="156"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="178"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="162"/>
        <location filename="../src/kwindowbuttonbar.cpp" line="183"/>
        <source>Restore</source>
        <translation>ᠤᠭ ᠢᠶᠠᠨ ᠪᠤᠴᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBarPrivate</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="271"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="280"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="341"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="346"/>
        <source>Restore</source>
        <translation>ᠤᠭ ᠢᠶᠠᠨ ᠪᠤᠴᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
