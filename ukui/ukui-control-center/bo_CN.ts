<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>About</name>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="614"/>
        <source>System Summary</source>
        <translation>ལམ་ལུགས་ཕྱོགས་བསྡོམས།</translation>
        <extra-contents_path>/About/System Summary</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="674"/>
        <source>Kernel</source>
        <translation>ནང་སྙིང་།</translation>
        <extra-contents_path>/About/Kernel</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="676"/>
        <source>CPU</source>
        <translation>CPU</translation>
        <extra-contents_path>/About/CPU</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="678"/>
        <source>Memory</source>
        <translation>དྲན་ཤེས་</translation>
        <extra-contents_path>/About/Memory</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="616"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1006"/>
        <source>Disk</source>
        <translation>ཁབ་ལེན་གྱི་ཁབ་ལེན་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="66"/>
        <source>About and Support</source>
        <translation>འབྲེལ་ཡོད་དང་རྒྱབ་སྐྱོར་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="227"/>
        <source>Version Number</source>
        <translation>པར་གཞིའི་ཨང་གྲངས།</translation>
        <extra-contents_path>/About/Version Number</extra-contents_path>
    </message>
    <message>
        <source>InterVersion</source>
        <translation type="vanished">ཕན་ཚུན་བརྗེ་རེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="247"/>
        <source>Patch Version</source>
        <translation>པར་གཞི་ཆུང་ཆུང་།</translation>
        <extra-contents_path>/About/Patch Version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="266"/>
        <source>Installed Date</source>
        <translation>སྒྲིག་སྦྱོར་ཚེས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="285"/>
        <source>Upgrade Date</source>
        <translation>རྒྱུད་ཁོངས་གསར་སྒྱུར་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="308"/>
        <source>HostName</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང</translation>
        <extra-contents_path>/About/HostName</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="451"/>
        <source>Privacy and agreement</source>
        <translation>སྒེར་གྱི་གསང་དོན་དང་གྲོས་མཐུན</translation>
        <extra-contents_path>/About/Privacy and agreement</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="463"/>
        <source>Send optional diagnostic data</source>
        <translation>བསལ་འདེམས་ཀྱི་བརྟག་དཔྱད་གཞི་གྲངས་སྐུར་སྐྱེལ་བྱ་དགོས།</translation>
        <extra-contents_path>/About/Send optional diagnostic data</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="465"/>
        <source>By sending us diagnostic data, improve the system experience and solve your problems faster</source>
        <translation>ང་ཚོར་ནད་གཞི་བརྟག་དཔྱད་ཀྱི་གཞི་གྲངས་བསྐུར་ནས་མ་ལག་གི་ཉམས་མྱོང་ལེགས་བཅོས་བྱས་ཏེ་ཁྱོད་ཀྱི་གནད་དོན་ཐག་གཅོད</translation>
    </message>
    <message>
        <source>Copyright © 2009-%1 KylinSoft. All rights reserved.</source>
        <translation type="vanished">པར་དབང ©་2009-%1 བསེ་རུ་མཉེན་པོ།. སོར་ཉར་བྱས་པའི་ཁེ་དབང་ཡོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="527"/>
        <source>&lt;&lt;Protocol&gt;&gt;</source>
        <translation>《ཚོད་སྤྱོད་འགན་འཁྲི་མེད་པར་བཟོ་བའི་གྲོས་མཐུན་》</translation>
        <extra-contents_path>/About/&lt;&lt;Protocol&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="528"/>
        <source>and</source>
        <translation>དེ་བཞིན་དེ་བཞིན་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="530"/>
        <source>&lt;&lt;Privacy&gt;&gt;</source>
        <translation>《སྤྱོད་མཁན་གྱི་གསང་དོན་གྲོས་མཐུན་》</translation>
        <extra-contents_path>/About/&lt;&lt;Privacy&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="615"/>
        <source>Support</source>
        <translation>རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="618"/>
        <source>Wechat code scanning obtains HP professional technical support</source>
        <translation>འཕྲིན་ཕྲན་ཨང་གྲངས་ལ་ཞིབ་བཤེར་བྱས་ནས་HPཆམ་ལས་ལག་རྩལ་གྱི་རྒྱབ་སྐྱོར་ཐོབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="619"/>
        <source>See more about Kylin Tianqi edu platform</source>
        <translation>See more about ཅིན་ལིན་ཐེན་ཆིའི་eduསྟེ་སྟེགས་བུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="638"/>
        <source>Learn more HP user manual&gt;&gt;</source>
        <translation>སྔར་ལས་ལྷག་པའི་སྒོ་ནས་HPབེད་སྤྱོད་བྱེད་མཁན&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="646"/>
        <source>See user manual&gt;&gt;</source>
        <translation>སྤྱོད་མཁན་གྱི་ལག་དེབ་ལ་གཟིགས&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="672"/>
        <source>Version</source>
        <translation>པར་གཞི།</translation>
        <extra-contents_path>/About/version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="680"/>
        <source>Desktop</source>
        <translation>ཅོག་ཙེའི་སྟེང་གི</translation>
        <extra-contents_path>/About/Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="682"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
        <extra-contents_path>/About/User</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="684"/>
        <source>Status</source>
        <translation>གནས་ཚུལ་གྱི་གནས་</translation>
        <extra-contents_path>/About/Status</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="688"/>
        <source>Serial</source>
        <translation>གོ་རིམ་ལྟར་ན།</translation>
        <extra-contents_path>/About/Serial</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="689"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1412"/>
        <source>DateRes</source>
        <translation>དུས་ཚོད་ཀྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="770"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1156"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1414"/>
        <source>Extend</source>
        <translation>དུས་འགྱངས་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="839"/>
        <source>Kylin Linux Desktop V10 (SP1)</source>
        <translation>ཆི་ལིན་གྱི་ཅོག་ངོས་ V10(SP1)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="999"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1008"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1560"/>
        <source>avaliable</source>
        <translation>འགན་འཁྲི་འཁུར་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1154"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1431"/>
        <source>expired</source>
        <translation>དུས་བཀག་ཐིམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1385"/>
        <source>The system needs to be restarted to set the HostName, whether to reboot</source>
        <translation>མ་ལག་འདི་བསྐྱར་དུ་འགོ་ཚུགས་ནས་གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1386"/>
        <source>Reboot Now</source>
        <translation>ད་ལྟ་བསྐྱར་དུ་ལས་ཀ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1387"/>
        <source>Reboot Later</source>
        <translation>རྗེས་སུ་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="686"/>
        <location filename="../../../plugins/system/about/about.cpp" line="751"/>
        <location filename="../../../plugins/system/about/about.cpp" line="757"/>
        <source>Active</source>
        <translation>འཁྲུག་ཆ་དོད་པོ་</translation>
        <extra-contents_path>/About/Active</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="68"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="516"/>
        <source>Copyright © 2020 KylinSoft. All rights reserved.</source>
        <translation>པར་དབང ©་2020 བསེ་རུ་མཉེན་པོ།. སོར་ཉར་བྱས་པའི་ཁེ་དབང་ཡོད་ཚད།.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="747"/>
        <location filename="../../../plugins/system/about/about.cpp" line="755"/>
        <source>Inactivated</source>
        <translation>འགུལ་སྐྱོད་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="749"/>
        <source>Trial expiration time</source>
        <translation>འདྲི་གཅོད་དུས་ཚོད་ཐིམ་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="768"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1411"/>
        <source>Activated</source>
        <translation>སྐུལ་སློང་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>Accessibility</name>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="32"/>
        <source>Vision</source>
        <translation>མཐོང་ཚོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="131"/>
        <source>Enable magnifying glass: Enlarge the content of the desktop</source>
        <translation>ཆེ་ཤེལ་སྤྱད་དེ་ཅོག་ངོས་ཀྱི་ནང་དོན་ཆེ་རུ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="138"/>
        <source>Press Win + &quot;+&quot; to zoom in on the content, Win + &quot;-&quot; to zoom out on the content.</source>
        <translation>Win+ཕྱིར་གཏོང་བའི་ནང་དོན་ལྟར་</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="225"/>
        <source>Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="315"/>
        <source>Color Filter Effect</source>
        <translation>ཚོས་གཞི་འཚག་པའི་ཕན་འབྲས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="415"/>
        <source>Color Filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="506"/>
        <source>Press Win + Ctrl + C to turn on/off color effect</source>
        <translation>Winrrirrlclccཡིས་ཁ་དོག་སྒོ་རྒྱག་པའི་ཕན་འབྲས་ལ་ཟེར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="529"/>
        <source>Other Settings</source>
        <translation>བཀོད་སྒྲིག་གཞན་དག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="585"/>
        <source>Point Size</source>
        <translation>ཆེ་ཆུང་ལ་བསམ་བློ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="604"/>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="659"/>
        <source>Set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="640"/>
        <source>Vocal Tract Regulation</source>
        <translation>སྒྲ་གདངས་སྙོམས་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="10"/>
        <source>Accessibility</source>
        <translation>ཆོག་ཆོག་པའི་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="80"/>
        <source>Window Zoom</source>
        <translation>སྒེའུ་ཁུང་ཇེ་ཆུང་དུ་སོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="81"/>
        <source>Full Screen Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="83"/>
        <source>Red/Green Filter (Protanopia)</source>
        <translation>མདོག་དམར་པོ་དང་ལྗང་མདོག་གི་འོད་འཚག་གློག་བརྙན། (Prooa)།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="84"/>
        <source>Green/Red Filter (Deuteranopia)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="85"/>
        <source>Blue/Yellow Filter (Tritanopia)</source>
        <translation>ཁ་དོག་སྔོན་པོ་དང་མདོག་སེར་པོའི་འོད་འཚག་ལེབ།(Trtata)།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="86"/>
        <source>Grayscale</source>
        <translation>ཐལ་ཚད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="87"/>
        <source>Invert</source>
        <translation>འཕོ་འགྱུར་</translation>
    </message>
</context>
<context>
    <name>AddAutoBoot</name>
    <message>
        <source>Add autoboot program</source>
        <translation type="vanished">རང་འགུལ་གྱིས་འཆར་གཞི་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="202"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">ངེས་གཏན་གྱི་རང་</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="248"/>
        <source>desktop file not allowed add</source>
        <translation>ཅོག་ངོས་ཀྱི་ཡིག་ཆ་ཁ་སྣོན་བྱས་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="297"/>
        <source>desktop file not exist</source>
        <translation>ཅོག་ཙེའི་ཡིག་ཆ་མེད་པ།</translation>
    </message>
    <message>
        <source>Select Autoboot Desktop</source>
        <translation type="vanished">རང་འགུལ་གྱིས་མདུན་ངོས་སུ་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="123"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="124"/>
        <source>Exec</source>
        <translation>ཨེ་ཤེ་ཡ་དང་ཞི་བདེ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="125"/>
        <source>Comment</source>
        <translation>དཔྱད་གཏམ་བརྗོད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="193"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ཅོག་ངོས་ཡིག་ཆ་(*.desktop)</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="200"/>
        <source>Select AutoStart Desktop</source>
        <translation>རང་འགུལ་གྱིས་མདུན་ངོས་སུ་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="201"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>AddAutoStart</name>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="177"/>
        <source>Add AutoStart program</source>
        <translation>རང་འགུལ་གྱིས་འཆར་གཞི་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="178"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="179"/>
        <source>Exec</source>
        <translation>ཨེ་ཤེ་ཡ་དང་ཞི་བདེ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="180"/>
        <source>Comment</source>
        <translation>དཔྱད་གཏམ་བརྗོད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="181"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="182"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="183"/>
        <source>Certain</source>
        <translation>ངེས་གཏན་གྱི་རང་</translation>
    </message>
</context>
<context>
    <name>AddBtn</name>
    <message>
        <location filename="../../../libukcc/widgets/AddBtn/addbtn.cpp" line="23"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
</context>
<context>
    <name>AddButton</name>
    <message>
        <location filename="../../../libukcc/widgets/SettingWidget/addbutton.cpp" line="25"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
</context>
<context>
    <name>AddInputMethodDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="26"/>
        <source>Select the input method to add</source>
        <translation>ནང་འཇུག་བྱེད་ཐབས་བདམས་ནས་ཁ་སྣོན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="82"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="101"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>Tibetan</source>
        <translation>བོད་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="7"/>
        <source>With ASCII numbers</source>
        <translation>ཨེ་ཤེ་ཡ་དང་ཞི་རྒྱ་ཆེ་མོ་ཁུལ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="15"/>
        <source>Input Method</source>
        <translation>ནང་འཇུག་བྱེད་ཐབས།</translation>
    </message>
</context>
<context>
    <name>AddLanguageDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="179"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="198"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="22"/>
        <source>Add Language</source>
        <translation>ཁ་སྣོན་བྱས་པའི་སྐད་ཆ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="23"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>AptProxyDialog</name>
    <message>
        <source>Set Apt Proxy</source>
        <translation type="vanished">Apt Proxyབཙུགས་པ།</translation>
    </message>
    <message>
        <source>Server Address</source>
        <translation type="vanished">ཞབས་ཞུའི་ས་གནས།</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">གྲུ་ཁ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Area</name>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="26"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="44"/>
        <source>Area</source>
        <translation>ས་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="59"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="236"/>
        <source>Language Format</source>
        <translation>སྐད་ཆའི་རྣམ་གཞག</translation>
        <extra-contents_path>/Area/Regional Format</extra-contents_path>
    </message>
    <message>
        <source>Current Region</source>
        <translation type="vanished">མིག་སྔའི་ས་ཁོངས།</translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="156"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="615"/>
        <source>Regional Format</source>
        <translation>དུས་ཚོད།དུས་ཚོད།དངུལ་ལོར་གྱི་རྣམ་པ།</translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="258"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="617"/>
        <source>Calendar</source>
        <translation>ལོ་ཐོ།</translation>
        <extra-contents_path>/Area/Calendar</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="344"/>
        <source>First Day Of The Week</source>
        <translation>གཟའ་འཁོར་གཅིག་གི་ཉིན་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="427"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="622"/>
        <source>Short Format Date</source>
        <translation>དུས་ཚོད་ཐུང་བའི་རྣམ་གཞག</translation>
        <extra-contents_path>/Area/Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="510"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="627"/>
        <source>Long Format Date</source>
        <translation>དུས་ཚོད་རིང་བའི་རྣམ་གཞག</translation>
        <extra-contents_path>/Area/Long Format Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="624"/>
        <source>Date</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="593"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="629"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
        <extra-contents_path>/Area/Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="676"/>
        <source>Language Format Example</source>
        <translation>སྐད་ཆའི་རྣམ་གཞག་གི་དཔེ་མཚོན་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="734"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="238"/>
        <source>System Language</source>
        <translation>མ་ལག་གི་སྐད་ཆ།</translation>
        <extra-contents_path>/Area/system language</extra-contents_path>
    </message>
    <message>
        <source>lunar</source>
        <translation type="vanished">ཟླ་བའི་གོ་ལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="768"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="241"/>
        <source>Language for system windows,menus and web pages</source>
        <translation>མ་ལག་གི་སྒེའུ་ཁུང་དང་། ཟས་ཐོ། དྲ་ངོས་བཅས་ཀྱི་སྐད་ཆ།</translation>
    </message>
    <message>
        <source>US</source>
        <translation type="vanished">ཨ་རི།</translation>
    </message>
    <message>
        <source>CN</source>
        <translation type="vanished">CN</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="362"/>
        <source>English  (US)</source>
        <translation>དབྱིན་ཡིག  (ཨ་མེ་རི་ཀ།)</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="363"/>
        <source>Simplified Chinese  (CN)</source>
        <translation>བསྡུས་གཟུགས་རྒྱ་ཡིག་  (ཀྲུང་གོ།)</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="364"/>
        <source>Tibetan  (CN)</source>
        <translation>བོད་ཡིག  (ཀྲུང་གོ།)</translation>
    </message>
    <message>
        <source>monday</source>
        <translation type="vanished">གཟའ་ཟླ་བ།</translation>
    </message>
    <message>
        <source>sunday</source>
        <translation type="vanished">གཟའ་ཉི་མ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="619"/>
        <source>First Day Of Week</source>
        <translation>གཟའ་འཁོར་གཅིག་གི་ཉིན་དང་པོ།</translation>
        <extra-contents_path>/Area/First Day Of Week</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="650"/>
        <source>12 Hours</source>
        <translation>ཆུ་ཚོད་12རིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="651"/>
        <source>24 Hours</source>
        <translation>ཆུ་ཚོད་24རིང་།</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="172"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="677"/>
        <source>MMMM dd, yyyy</source>
        <translation>yyyyལོ།MMཟླ།ddཉིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="175"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="680"/>
        <source>MMMM d, yy</source>
        <translation>yyལོ།Mཟླ།dཉིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="247"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Area/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="379"/>
        <source>Monday</source>
        <translation>གཟའ་ཟླ་བ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="380"/>
        <source>Sunday</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="631"/>
        <source>Solar Calendar</source>
        <translation>སྤྱི་ལོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="634"/>
        <source>Lunar</source>
        <translation>ལུགས་རྙིང་ལོ་ཐོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="923"/>
        <source>Modify the current region need to logout to take effect, whether to logout?</source>
        <translation>ད་ལྟའི་ས་ཁོངས་ལ་བཟོ་བཅོས་བརྒྱབ་ན་ད་གཟོད་ནུས་པ་ཐོན་ཐུབ། ཐོ་འགོད་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="924"/>
        <source>Logout later</source>
        <translation>རྗེས་སུ་ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="925"/>
        <source>Logout now</source>
        <translation>ད་ལྟ་ཐོ་འགོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="927"/>
        <source>Modify the first language need to restart to take effect, whether to restart?</source>
        <translation>སྐད་རིགས་དང་པོར་བཟོ་བཅོས་བརྒྱབ་ན་ད་གཟོད་ནུས་པ་ཐོན་ཐུབ། བསྐྱར་དུ་ཐོན་ཐུབ་མིན་ལ་བཟོ་བཅོས་རྒྱག་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Modify the first language need to reboot to take effect, whether to reboot?</source>
        <translation type="vanished">སྐད་རིགས་དང་པོར་བཟོ་བཅོས་བརྒྱབ་ན་ད་གཟོད་ནུས་པ་ཐོན་ཐུབ། བསྐྱར་དུ་ཐོན་ཐུབ་མིན་ལ་བཟོ་བཅོས་རྒྱག་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="928"/>
        <source>Restart later</source>
        <translation>རྗེས་སུ་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="929"/>
        <source>Restart now</source>
        <translation>ད་ལྟ་བསྐྱར་དུ་འཁོར་བ།</translation>
    </message>
    <message>
        <source>Reboot later</source>
        <translation type="vanished">རྗེས་སུ་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་</translation>
    </message>
    <message>
        <source>Reboot now</source>
        <translation type="vanished">ད་ལྟ་བསྐྱར་དུ་འཁོར་བ།</translation>
    </message>
    <message>
        <source>solar calendar</source>
        <translation type="vanished">ཉི་མའི་ལོ་ཐོ།</translation>
    </message>
</context>
<context>
    <name>AutoBoot</name>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="466"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ཅོག་ངོས་ཡིག་ཆ་(*.desktop)</translation>
    </message>
    <message>
        <source>select autoboot desktop</source>
        <translation type="vanished">རང་འགུལ་གྱིས་མདུན་ངོས་སུ་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <source>Select Autoboot Desktop</source>
        <translation type="vanished">རང་འགུལ་གྱིས་མདུན་ངོས་སུ་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="88"/>
        <source>Auto Start</source>
        <translation>རང་འགུལ་གྱིས་ལྷམ་ཡུ་རིང</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="474"/>
        <source>Select AutoStart Desktop</source>
        <translation>རང་འགུལ་གྱིས་མདུན་ངོས་སུ་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="475"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="476"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="623"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/AutoStart/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="630"/>
        <source>AutoStart Settings</source>
        <translation>རང་འགུལ་གྱིས་སྒྲིག་བཀོད་སྒྲིག་བྱེད་པ།</translation>
        <extra-contents_path>/AutoStart/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Autoboot Settings</source>
        <translation type="vanished">རང་འགུལ་གྱིས་སྒྲིག་བཀོད་སྒྲིག་བྱེད་པ།</translation>
        <extra-contents_path>/Autoboot/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Auto Boot</source>
        <translation type="vanished">རང་འགུལ་གྱིས་ལྷམ་ཡུ་རིང</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="243"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>Backup</name>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="53"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="119"/>
        <source>Backup</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག</translation>
        <extra-contents_path>/Backup/Backup</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="69"/>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted, 
you can restore them to ensure the integrity of your system.</source>
        <translation>ཡིག་ཆ་དེ་སྒུལ་བྱེད་འཕྲུལ་འཁོར་གཞན་དག་ལ་རྒྱབ་སྐྱོར་བྱས་ནས་སྔར་གྱི་ཡིག་ཆ་བོར་བརླག་ཏུ་སོང་བ་དང་། གཏོར་བརླག་ཐེབས་པའམ་ཡང་ན་བསུབ་པའི་སྐབས་སུ། 
དེ་དག་སླར་གསོ་བྱས་ནས་ཁྱེད་ཚོའི་མ་ལག་གི་ཆ་ཚང་རང་བཞིན་ལ་ཁག་ཐེག་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="113"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="115"/>
        <source>Begin backup</source>
        <translation>རྗེས་གྲབས་ལས་དོན་སྤེལ་འགོ་ཚུགས</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="157"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="145"/>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
        <extra-contents_path>/Backup/Restore</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="173"/>
        <source>View a list of backed-upfiles to backed up files to the system</source>
        <translation>རྒྱབ་སྐྱོར་བྱས་པའི་ཡིག་ཆ་དེ་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱེད་པའི་མིང་ཐོར་ལྟ་ཞིབ་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="213"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="141"/>
        <source>Begin restore</source>
        <translation>སླར་གསོ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="45"/>
        <source>Backup Restore</source>
        <translation>གྲབས་ཉར་སོར་ལོག་</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="121"/>
        <source>Back up your files to other drives and restore them when the source files are lost, damaged, or deleted to ensure the integrity of the system.</source>
        <translation>ཡིག་ཆ་དེ་སྒུལ་ཤུགས་གཞན་དག་ལ་རྒྱབ་སྐྱོར་བྱས་ནས་འབྱུང་ཁུངས་ཀྱི་ཡིག་ཆ་བོར་བརླག་ཏུ་སོང་བ་དང་། གཏོར་བརླག་ཐེབས་པའམ་ཡང་ན་བསུབ་རྗེས་སླར་གསོ་བྱས་ཏེ་མ་ལག་གི་ཆ་ཚང་རང་བཞིན་ལ་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="147"/>
        <source>View the backup list and restore the backup file to the system</source>
        <translation>རྗེས་གྲབས་རེའུ་མིག་ལ་ལྟ་ཞིབ་བྱས་ནས་རྗེས་གྲབས་ཡིག་ཆ་མ་ལག་ནང་དུ་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="105"/>
        <source>Backup and Restore</source>
        <translation>རྗེས་གྲབས་དང་སླར་གསོ་བྱེད་དགོས།</translation>
        <extra-contents_path>/Backup/Backup and Restore</extra-contents_path>
    </message>
    <message>
        <source>All data stored on the computer will be permanently erased,and the system will revert to 
                                its original factory state when this operation is completed.</source>
        <translation type="vanished">རྩིས་འཁོར་ནང་ཉར་ཚགས་བྱས་པའི་གཞི་གྲངས་ཚང་མ་དུས་གཏན་དུ་རྩིས་མེད་དུ་གཏོང་རྒྱུ་དང་། མ་ལག་དེ་སླར་ཡང་རྩིས་མེད་དུ་གཏོང་རྒྱུ། 
                                གཉེར་སྐྱོང་འདི་ལེགས་འགྲུབ་བྱུང་བའི་སྐབས་སུ་དེའི་སྔར་གྱི་བཟོ་གྲྭའི་རྣམ་པ་ཡིན།</translation>
    </message>
    <message>
        <source>System Recovery</source>
        <translation type="vanished">མ་ལག་སླར་གསོ་</translation>
    </message>
</context>
<context>
    <name>Boot</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="10"/>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="80"/>
        <source>Boot</source>
        <translation>རྒྱུད་ཁོངས་འགོ་སློང་།</translation>
        <extra-contents_path>/Boot/Boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="92"/>
        <source>Grub verify</source>
        <translation>Grubར་སྤྲོད་</translation>
        <extra-contents_path>/Boot/Grub verify</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="95"/>
        <source>Password required for Grub editing after enabling</source>
        <translation>སྤྱོད་རྗེས་Grubརྩོམ་སྒྲིག་།གསང་ཨང་དགོས་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="97"/>
        <source>Reset password</source>
        <translation>བསྐྱར་འགོད་གསང་ཨང་</translation>
    </message>
</context>
<context>
    <name>BrightnessFrame</name>
    <message>
        <location filename="../../../plugins/system/display/brightnessFrame.cpp" line="41"/>
        <source>Failed to get the brightness information of this monitor</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་འཕྲུལ་ཆས་འདིའི་གསལ་ཚད་ཀྱི་ཆ་འཕྲིན་ཐོབ་མ་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>ChangeFaceIntelDialog</name>
    <message>
        <source>Change User Face</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་ངོ་གདོང་བསྒྱུར་བཅོས་</translation>
        <extra-contents_path>/UserinfoIntel/Change User Face</extra-contents_path>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">ལོ་རྒྱུས།</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">ལམ་ལུགས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>select custom face file</source>
        <translation type="vanished">ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་ངོ་གདོང་ཡིག་ཆ</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">གོ་གནས་ནི། </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>The avatar is larger than 2M, please choose again</source>
        <translation type="vanished">པར་དབང་དེ་2Mལས་ཆེ་བ་དང་། ཡང་བསྐྱར་གདམ་ག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>ChangeGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>User Group Settings</source>
        <translation type="vanished">用户组设置</translation>
    </message>
    <message>
        <source>User groups available in the system</source>
        <translation type="vanished">མ་ལག་ཁྲོད་དུ་སྤྱོད་ཆོག་པའི་སྤྱོད་མཁན་ཚོགས་པ།</translation>
    </message>
    <message>
        <source>Add user group</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་ཚོ་ཆུང་ཁ་སྣོན</translation>
    </message>
</context>
<context>
    <name>ChangePhoneIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>changephone</source>
        <translation type="vanished">འགྱུར་ལྡོག་ཁ་པར།</translation>
    </message>
    <message>
        <source>Please input old phone num</source>
        <translation type="vanished">ཁ་པར་རྙིང་བ་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <source>GetVerifyCode</source>
        <translation type="vanished">GetVerifyCode</translation>
    </message>
    <message>
        <source>submit</source>
        <translation type="vanished">གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <source>Change Phone</source>
        <translation type="vanished">ཁ་པར་བརྗེ་བ།</translation>
    </message>
    <message>
        <source>Phone number</source>
        <translation type="vanished">ཁ་པར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <source>SMS verification code</source>
        <translation type="vanished">SMS ཞིབ་བཤེར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <source>Please input old phone number</source>
        <translation type="vanished">ཁ་པར་ཨང་གྲངས་རྙིང་བ་ནང་འཇུག་བྱེད་</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <source>Please enter new mobile number</source>
        <translation type="vanished">སྒུལ་བདེའི་ཨང་གྲངས་གསར་པར་འཇུག་རོགས།</translation>
    </message>
    <message>
        <source>Submit</source>
        <translation type="vanished">གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <source>changed success</source>
        <translation type="vanished">འགྱུར་ལྡོག་བྱུང་བའི་ལེགས་འགྲུབ</translation>
    </message>
    <message>
        <source>You have successfully modified your phone</source>
        <translation type="vanished">ཁྱེད་ཀྱིས་ལག་ཁྱེར་ཁ་པར་ལ་བཟོ་བཅོས་ལེགས་འགྲུབ་བྱུང་ཡོད།</translation>
    </message>
    <message>
        <source>Recapture</source>
        <translation type="vanished">ཕྱིར་འཕྲོག་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Network connection failure, please check</source>
        <translation type="vanished">དྲ་སྦྲེལ་ལ་སྐྱོན་ཤོར་བས་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>GetCode</source>
        <translation type="vanished">ཨང་ཀི་ཐོབ་པ།</translation>
    </message>
    <message>
        <source>Phone is lock,try again in an hour</source>
        <translation type="vanished">ཁ་པར་ལ་ཟྭ་བརྒྱབ་ནས་དུས་ཚོད་གཅིག་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་</translation>
    </message>
    <message>
        <source>Phone code is wrong</source>
        <translation type="vanished">ཁ་པར་ཨང་གྲངས་ནོར་སོང་།</translation>
    </message>
    <message>
        <source>Current login expired,using wechat code!</source>
        <translation type="vanished">ད་ལྟའི་ཐོ་འགོད་དུས་ཚོད་ཐིམ་ནས་འཕྲིན་ཕྲན་གྱི་ཚབ་རྟགས་བཀོལ་སྤྱོད་བྱས།</translation>
    </message>
    <message>
        <source>Unknown error, please try again later</source>
        <translation type="vanished">ནོར་འཁྲུལ་མི་ཤེས་པས་ཕྱིས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་</translation>
    </message>
    <message>
        <source>Phone can not same</source>
        <translation type="vanished">ཁ་པར་གཅིག་འདྲ་ཡོང་མི་སྲིད།</translation>
    </message>
    <message>
        <source>finished</source>
        <translation type="vanished">ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">བཀོལ་སྤྱོད་བྱས་ཟིན་པའི་ཁ་པར་ཨང་གྲངས་རེད།</translation>
    </message>
</context>
<context>
    <name>ChangePinIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>Change Password</source>
        <translation type="vanished">གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
</context>
<context>
    <name>ChangePwdIntelDialog</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">ལ་འགྱུར་ལྡོག་གཏོང་དགོས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>General Pwd</source>
        <translation type="vanished">དམག་དཔོན་ཆེན་མོ་ཕུའུ་ཝེ་ཏི།</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation type="vanished">གསང་གྲངས་རྙིང་པ།</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation type="vanished">གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <source>New Password Identify</source>
        <translation type="vanished">གསང་གྲངས་གསར་པའི་དབྱེ་འབྱེད།</translation>
    </message>
    <message>
        <source>Please set different pwd!</source>
        <translation type="vanished">ཁྱེད་ཀྱིས་མི་འདྲ་བའི་pwdསྟེང་དུ་བཀོད་སྒྲིག་</translation>
    </message>
    <message>
        <source>Old pwd is wrong!</source>
        <translation type="vanished">གནའ་བོའི་pwdནི་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <source>New pwd is too similar with old pwd!</source>
        <translation type="vanished">pwdགསར་པ་དང་གནའ་བོའི་pwdགཉིས་ཧ་ཅང་འདྲ་མཚུངས་རེད།</translation>
    </message>
    <message>
        <source>Check old pwd failed because of unknown reason!</source>
        <translation type="vanished">རྒྱུ་མཚན་མི་ཤེས་པའི་རྐྱེན་གྱིས་ཞིབ་བཤེར་རྙིང་བ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1ལས་བརྒལ་དགོས།</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1མན་གྱི་ཡི་གེ་ཞིག་དགོས།</translation>
    </message>
    <message>
        <source>Password cannot be made up entirely by Numbers!</source>
        <translation type="vanished">གསང་གྲངས་ནི་ཨང་ཀིས་ཡོངས་སུ་ཁ་གསབ་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>ChangeTypeIntelDialog</name>
    <message>
        <source>Change Account Type</source>
        <translation type="vanished">རྩིས་ཐོའི་རིགས་དབྱིབས་བསྒྱུར་བཅོས་</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install software and change system settings</source>
        <translation type="vanished">ཚད་ལྡན་སྤྱོད་མཁན་གྱིས་མཉེན་ཆས་མང་ཆེ་བ་བཀོལ་སྤྱོད་བྱས་ཆོག་མོད། འོན་ཀྱང་མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་མ་ལག་གི་སྒྲིག</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">དོ་དམ་པ།</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">སྲིད་འཛིན་དོ་དམ་མི་སྣས་ཁོ་ཚོར་མཁོ་བའི་འགྱུར་ལྡོག་གང་རུང</translation>
    </message>
    <message>
        <source>Make sure that there is at least one administrator on the computer</source>
        <translation type="vanished">རྩིས་འཁོར་ནང་དུ་ཉུང་མཐར་ཡང་དོ་དམ་པ་གཅིག་ཡོད་པར་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ChangeUserLogo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="111"/>
        <source>User logo</source>
        <translation>སྤྱོད་མཁན་གྱི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="134"/>
        <source>System Logos</source>
        <translation>མ་ལག་གི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="143"/>
        <source>Select Local Logo</source>
        <translation>ས་གནས་དེ་གའི་མཚོན་རྟགས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="152"/>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="296"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="154"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="291"/>
        <source>select custom face file</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་ངོ་གདོང་ཡིག་ཆ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="292"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="293"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="294"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="295"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="311"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="312"/>
        <source>The avatar is larger than 1M, please choose again</source>
        <translation>པར་དབང་དེ་1Mལས་ཆེ་བ་དང་། ཡང་བསྐྱར་གདམ་ག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>ChangeUserNickname</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="31"/>
        <source>Set Nickname</source>
        <translation>མཚང་མིང་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="53"/>
        <source>UserName</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="70"/>
        <source>NickName</source>
        <translation>མིང་འདོགས་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="156"/>
        <source>NickName&apos;s length must between 1~%1 characters!</source>
        <translation>བཀོལ་མཁན་གྱི་གཅེས་མིང་རིང་ཚད་ངེས་པར་དུ་1ནས་%1ཡིག་རྟགས་བར་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="158"/>
        <source>nickName already in use.</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་མཚང་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="160"/>
        <source>Can&apos;t contains &apos;:&apos;.</source>
        <translation>མི་འདུ་བ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="113"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="116"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>The length must be 1~%1 characters!</source>
        <translation type="vanished">རིང་ཚད་ནི་ངེས་པར་དུ་1%1 ཡི་ཡི་གེ་ཡིན་དགོས།</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="98"/>
        <source>Change password</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="103"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="470"/>
        <source>Current Pwd</source>
        <translation>མིག་སྔའི་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="116"/>
        <source>Required</source>
        <translation>བླང་བྱ་བཏོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="136"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="471"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="479"/>
        <source>New Pwd</source>
        <translation>གསར་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="163"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="472"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="480"/>
        <source>Sure Pwd</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="218"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="303"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="370"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="279"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="543"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་གསར་པ་དང་མི་འདྲ་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="299"/>
        <source>Same with old pwd</source>
        <translation>གསང་ཨང་རྙིང་པ་དང་འདྲ་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="353"/>
        <source>Pwd Changed Succes</source>
        <translation>གསང་ཨང་བཟོ་བཅོས་གྲུབ་འབྲས་ཐོབ་པའི་ངང་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="360"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་པར་ཡང་བསྐྱར་ནང་འཇུག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="502"/>
        <source>Contains illegal characters!</source>
        <translation>དེའི་ནང་དུ་ཁྲིམས་འགལ་གྱི་མི་སྣ་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="614"/>
        <source>current pwd cannot be empty!</source>
        <translation>ད་ལྟའི་གསང་ཨང་ནི་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="619"/>
        <source>new pwd cannot be empty!</source>
        <translation>གསར་དུ་བཏོ་བ་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="624"/>
        <source>sure pwd cannot be empty!</source>
        <translation>གསང་ཨང་ངོས་འཛིན་སྟོང་པ་ཡིན་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>ChangeUserType</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="24"/>
        <source>UserType</source>
        <translation>སྤྱོད་མཁན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="61"/>
        <source>Select account type (Ensure have admin on system):</source>
        <translation>རྩིས་ཐོའི་རིགས་བདམས་ནས་(མ་ལག་ལ་སྲིད་འཛིན་དོ་དམ་ཡོད་པར་ཁག་ཐེག་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="71"/>
        <source>administrator</source>
        <translation>དོ་དམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="73"/>
        <source>standard user</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="75"/>
        <source>change system settings, install and upgrade software.</source>
        <translation>མ་ལག་གི་སྒྲིག་བཀོད་ལ་འགྱུར་ལྡོག་གཏོང་བ་དང་། མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="77"/>
        <source>use most software, cannot change system settings.</source>
        <translation>མཉེན་ཆས་མང་ཆེ་བ་བཀོལ་སྤྱོད་བྱས་ན་མ་ལག་གི་སྒྲིག་བཀོད་ལ་འགྱུར་ལྡོག་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="130"/>
        <source>Note: Effective After Logout!!!</source>
        <translation>མཆན་འགྲེལ། རྩིས་བརྒྱབ་རྗེས་ཕན་ནུས་ཐོན་པ!!!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="141"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="144"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>ChangeValidIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>Password Validity Setting</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་གོ་ཆོད་པའི་རང་བཞིན་གཏན</translation>
    </message>
    <message>
        <source>Current passwd validity:</source>
        <translation type="vanished">མིག་སྔའི་འགག་སྒོ་ལས་བརྒལ་བའི་གོ་ཆོད་པའི་རང་བཞིན་</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="vanished">ལེགས་སྒྲིག་བྱས་པའི་དུས་ཚོད་ནི།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">ངེས་གཏན་གྱི་རང་</translation>
    </message>
</context>
<context>
    <name>ChangtimeDialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="158"/>
        <source>time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="159"/>
        <source>year</source>
        <translation>ལོ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="160"/>
        <source>month</source>
        <translation>ཟླ་བ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="161"/>
        <source>day</source>
        <translation>ཉིན་མོ།</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <source>选择自定义颜色</source>
        <translation type="vanished">选择自定义颜色</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="86"/>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="46"/>
        <source>Choose a custom color</source>
        <translation>བདམས་པའི་མཚན་ཉིད་ཁ་མདོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="254"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="290"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="457"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="476"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="50"/>
        <source>Custom color</source>
        <translation>གོམས་སྲོལ་གྱི་ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="138"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>CreateGroupDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.ui" line="26"/>
        <source>Add New Group</source>
        <translation>ཚོགས་པ་གསར་པ་ཁ་སྣོན་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="51"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="75"/>
        <source>Id</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="89"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="199"/>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="213"/>
        <source>GroupName&apos;s length must be between 1 and %1 characters!</source>
        <translation>ཚོ་མིང་གི་རིང་ཚད་ངེས་པར་དུ་1ནས་%1ཡིག་རྟགས་བར་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="86"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="47"/>
        <source>Add user group</source>
        <translation>སྤྱོད་མཁན་གྱི་ཚོ་ཆུང་ཁ་སྣོན</translation>
    </message>
</context>
<context>
    <name>CreateGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>Add New Group</source>
        <translation type="vanished">ཚོགས་པ་གསར་པ་ཁ་སྣོན་</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="vanished">ཚོ་ཆུང་གི་མིང་།</translation>
    </message>
    <message>
        <source>Group Id</source>
        <translation type="vanished">ཚོགས་པའི་ཐོབ་ཐང་ལག་ཁྱེར</translation>
    </message>
    <message>
        <source>Group Members</source>
        <translation type="vanished">ཚོ་ཆུང་གི་ཁོངས་མི།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">ངེས་གཏན་གྱི་རང་</translation>
    </message>
</context>
<context>
    <name>CreateUserIntelDialog</name>
    <message>
        <source>Add New Account</source>
        <translation type="vanished">རྩིས་ཐོ་གསར་པ་ཁ་སྣོན་</translation>
    </message>
    <message>
        <source>Account Type</source>
        <translation type="vanished">རྩིས་ཐོའི་རིགས་</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install the software and 
change system settings</source>
        <translation type="vanished">ཚད་ལྡན་སྤྱོད་མཁན་གྱིས་མཉེན་ཆས་མང་ཆེ་བ་བཀོལ་སྤྱོད་བྱས་ཆོག་མོད། འོན་ཀྱང་མཉེན་ཆས་དང་སྒྲིག་ 
མ་ལག་གི་སྒྲིག་བཀོད་ལ་འགྱུར་ལྡོག</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">དོ་དམ་པ།</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">སྲིད་འཛིན་དོ་དམ་མི་སྣས་ཁོ་ཚོར་མཁོ་བའི་འགྱུར་ལྡོག་གང་རུང</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Password Identify</source>
        <translation type="vanished">གསང་གྲངས་ངོས་འཛིན་</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1ལས་བརྒལ་དགོས།</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1མན་གྱི་ཡི་གེ་ཞིག་དགོས།</translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>The first character must be lowercase letters!</source>
        <translation type="vanished">ཡི་གེ་དང་པོ་ནི་ངེས་པར་དུ་ཡི་གེ་དམའ་རུ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <source>User name can not contain capital letters!</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་ལ་ཡི་གེ་ཆེན་པོ་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <source>The user name is already in use, please use a different one.</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་བཀོལ་སྤྱོད་བྱེད་བཞིན་ཡོད་པས་གཞན་ཞིག་བཀོལ་རོགས།</translation>
    </message>
    <message>
        <source>User name length need to less than %1 letters!</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་གི་རིང་ཚད་ནི་%1ལས་ཉུང་བའི་འཕྲིན་ཡིག་ཅིག་དགོས།</translation>
    </message>
    <message>
        <source>The user name can only be composed of letters, numbers and underline!</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་ནི་འཕྲིན་ཡིག་དང་། ཨང་གྲངས། གསལ་བཤད་བཅས་ཁོ་ནར་བརྟེན་ནས་གྲུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <source>The username is configured, please change the username</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་བཀོད་སྒྲིག་བྱས་ཟིན་པས་སྤྱོད་མཁན་གྱི་མིང་བསྒྱུར་རོགས།</translation>
    </message>
</context>
<context>
    <name>CreateUserNew</name>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="49"/>
        <source>CreateUserNew</source>
        <translation>གསར་སྐྲུན་བྱས་པའི་གསར་གཏོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="55"/>
        <source>UserName</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="65"/>
        <source>NickName</source>
        <translation>མིང་འདོགས་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="71"/>
        <source>HostName</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="80"/>
        <source>Pwd</source>
        <translation>ལམ་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="88"/>
        <source>SurePwd</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་དངོས་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="96"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="99"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="102"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="105"/>
        <source>Required</source>
        <translation>བླང་བྱ་བཏོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="109"/>
        <source>verification</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="182"/>
        <source>Select Type</source>
        <translation>རིགས་དབྱིབས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="191"/>
        <source>Administrator</source>
        <translation>སྲིད་འཛིན་དོ་དམ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="194"/>
        <source>Users can make any changes they need</source>
        <translation>སྤྱོད་མཁན་གྱིས་རང་ཉིད་ལ་མཁོ་བའི་འགྱུར་ལྡོག་གང་རུང་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="196"/>
        <source>Standard User</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="199"/>
        <source>Users cannot change system settings</source>
        <translation>སྤྱོད་མཁན་གྱིས་མ་ལག་གི་སྒྲིག་བཀོད་ལ་འགྱུར་ལྡོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="276"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="279"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="355"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="581"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་གསར་པ་དང་མི་འདྲ་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="494"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="647"/>
        <source>NickName&apos;s length must be between 1 and %1 characters!</source>
        <translation>བཀོལ་མཁན་གྱི་གཅེས་མིང་རིང་ཚད་ངེས་པར་དུ་1ནས་%1ཡིག་རྟགས་བར་ཡིན་དགོས།</translation>
    </message>
    <message>
        <source>The nick name cannot be empty</source>
        <translation type="vanished">མིང་འདོགས་པའི་མིང་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="496"/>
        <source>nickName already in use.</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་མཚང་མིང་།</translation>
    </message>
    <message>
        <source>nickName length must less than %1 letters!</source>
        <translation type="vanished">མིང་འདོགས་པའི་རིང་ཚད་ངེས་པར་དུ་བརྒྱ་ཆའི་གཅིག་གི་ཡི་གེ་ལས་ཉུང་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="522"/>
        <source>Username&apos;s folder exists, change another one</source>
        <translation>སྤྱོད་མཁན་གྱི་ཡིག་སྣོད་གནས་པ་དང་། གཞན་ཞིག་ལ་འགྱུར་ལྡོག་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="526"/>
        <source>Name corresponds to group already exists.</source>
        <translation>མིང་དང་ཚོགས་པ་གཉིས་ལ་བབ་མཚུངས་ཀྱི་མིང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="551"/>
        <source>Contains illegal characters!</source>
        <translation>དེའི་ནང་དུ་ཁྲིམས་འགལ་གྱི་མི་སྣ་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="642"/>
        <source>Username&apos;s length must be between 1 and %1 characters!</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་ཡི་རིང་ཚད་ནི་ངེས་པར་དུ་1ནས་%1བར་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="652"/>
        <source>new pwd cannot be empty!</source>
        <translation>གསར་དུ་བཏོ་བ་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="657"/>
        <source>sure pwd cannot be empty!</source>
        <translation>གསང་ཨང་ངོས་འཛིན་སྟོང་པ་ཡིན་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>CustomGlobalTheme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/globaltheme/customglobaltheme.cpp" line="38"/>
        <source>custom</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས།</translation>
    </message>
</context>
<context>
    <name>CustomLineEdit</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/customlineedit.cpp" line="28"/>
        <source>New Shortcut...</source>
        <translation>མྱུར་བགྲོད་གཞུང་ལམ་གསར་པ...</translation>
    </message>
</context>
<context>
    <name>DateTime</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="26"/>
        <source>DateTime</source>
        <translation>དུས་ཚོད་ཀྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="65"/>
        <source>current date</source>
        <translation>ད་ལྟའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="321"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="275"/>
        <source>Change timezone</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
        <extra-contents_path>/Date/Change time zone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="444"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="620"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="481"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="510"/>
        <source>RadioButton</source>
        <translation>ཀུན་ཁྱབ་རླུང་འཕྲིན་ལས་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="712"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="744"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="952"/>
        <source>titleLabel</source>
        <translation>ཁ་བྱང་ལ་པེ་ཨར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="94"/>
        <source>Date</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="175"/>
        <source>Current Date</source>
        <translation>ད་ལྟའི་དུས་ཚོད།</translation>
        <extra-contents_path>/Date/Current Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="178"/>
        <source>Other Timezone</source>
        <translation>དུས་ཚོད་གཞན་དག</translation>
        <extra-contents_path>/Date/Other Timezone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="196"/>
        <source>24-hour clock</source>
        <translation>ཆུ་ཚོད་24རིང་གི་ཆུ་ཚོད་འཁོར་ལོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="198"/>
        <source>Set Time</source>
        <translation>དུས་ཚོད་གཏན་འཁེལ་བྱ་</translation>
        <extra-contents_path>/Date/Set Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="226"/>
        <source>Set Date Manually</source>
        <translation>ལག་པས་དུས་ཚོད་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="286"/>
        <source>Sync Time</source>
        <translation>དུས་ཚོད་གཅིག་མཐུན་ཡོང་བ</translation>
        <extra-contents_path>/Date/Sync Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="289"/>
        <source>Manual Time</source>
        <translation>ལག་ཤེས་དུས་ཚོད།</translation>
        <extra-contents_path>/Date/Manual Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="375"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Date/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="461"/>
        <source>Sync Server</source>
        <translation>དུས་མཉམ་ཞབས་ཞུའི་ཡོ་བྱད</translation>
        <extra-contents_path>/Date/Sync Server</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="463"/>
        <source>Default</source>
        <translation>ཁ་ཆད་དང་འགལ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="465"/>
        <source>Customize</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="475"/>
        <source>Server Address</source>
        <translation>ཞབས་ཞུའི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="480"/>
        <source>Required</source>
        <translation>བླང་བྱ་བཏོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="481"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="640"/>
        <source>change time</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="650"/>
        <source>Add Timezone</source>
        <translation>དུས་ཚོད་ཀྱི་ཁྱབ་ཁུལ་ཁ་སྣོན</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="652"/>
        <source>Change Timezone</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="772"/>
        <source>MMMM d, yy ddd</source>
        <translation>yyལོའི་ཟླ་Mཚེས་dཉིན། ddd</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="774"/>
        <source>MMMM dd, yyyy ddd</source>
        <translation>yyyyལོའི་ཟླ་MMཚེས་ddཉིན། ddd</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="932"/>
        <source>  </source>
        <translation>  </translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="933"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="942"/>
        <source>Sync Failed</source>
        <translation>དུས་མཉམ་དུ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>DefaultApp</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="43"/>
        <source>Default App</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="67"/>
        <source>No program available</source>
        <translation>ད་ཡོད་ཀྱི་གོ་རིམ་མེད་</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="68"/>
        <source>Choose default app</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་ཉེར་</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="252"/>
        <source>Reset default apps to system recommended apps</source>
        <translation>མ་ལག་གིས་འོས་སྦྱོར་བྱས་པའི་ཉེར་སྤྱོད་གོ་རིམ་ཁྲོད་དུ་ཁ་ཆད་</translation>
        <extra-contents_path>/Defaultapp/Reset default apps to system recommended apps</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="253"/>
        <source>Reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="348"/>
        <source>Browser</source>
        <translation>བཤེར་ཆས།</translation>
        <extra-contents_path>/Defaultapp/Browser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="350"/>
        <source>Mail</source>
        <translation>སྦྲག་རྫས།</translation>
        <extra-contents_path>/Defaultapp/Mail</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="352"/>
        <source>Image Viewer</source>
        <translation>པར་རིས་ལྟ་མཁན།</translation>
        <extra-contents_path>/Defaultapp/Image Viewer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="354"/>
        <source>Audio Player</source>
        <translation>སྒྲ་ཕབ་འཕྲུལ་ཆས།</translation>
        <extra-contents_path>/Defaultapp/Audio Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="356"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
        <extra-contents_path>/Defaultapp/Video Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="358"/>
        <source>Text Editor</source>
        <translation>ཡི་གེའི་རྩོམ་སྒྲིག་པ།</translation>
        <extra-contents_path>/Defaultapp/Text Editor</extra-contents_path>
    </message>
</context>
<context>
    <name>DefaultAppWindow</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="346"/>
        <source>Select Default Application</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་ཉེར་སྤྱོད</translation>
        <extra-contents_path>/Defaultapp/Select Default Application</extra-contents_path>
    </message>
</context>
<context>
    <name>DefineGroupItemIntel</name>
    <message>
        <source>Edit</source>
        <translation type="vanished">རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>DefineShortcutItem</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/defineshortcutitem.cpp" line="58"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>DelGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="vanished">བཙོག་དངོས་མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">དྲན་སྐུལ།</translation>
    </message>
</context>
<context>
    <name>DelUserIntelDialog</name>
    <message>
        <source>   Delete</source>
        <translation type="vanished">   བསུབ་པ།</translation>
    </message>
    <message>
        <source>Define</source>
        <translation type="vanished">མཚན་ཉིད་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Delete the user, belonging to the user&apos;s desktop documents, favorites, music, pictures and video folder will be deleted!</source>
        <translation type="vanished">སྤྱོད་མཁན་བསུབ་ན་སྤྱོད་མཁན་གྱི་ཅོག་ཙེའི་ཡིག་ཆ་དང་། དགའ་ཕྱོགས། རོལ་མོ། པར་རིས། བརྙན་ཕབ་ཡིག་སྣོད་སོགས་ཚང་མ་བསུབ་རྒྱུ་རེད།</translation>
    </message>
</context>
<context>
    <name>DeleteUserExists</name>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="26"/>
        <source>Delete User</source>
        <translation>བེད་སྤྱོད་བྱེད་མཁན་བསུབས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="39"/>
        <source>Delete user &apos;</source>
        <translation>བེད་སྤྱོད་བྱེད་མཁན་བསུབ་པ&apos;</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="40"/>
        <source>&apos;? And:</source>
        <translation>&apos;? ད་དུང་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="66"/>
        <source>Keep desktop, files, favorites, music of the user</source>
        <translation>སྤྱོད་མཁན་གྱི་ཅོག་ཙེ་དང་། ཡིག་ཆ། དགའ་ཕྱོགས། རོལ་དབྱངས་བཅས་ཉར་ཚགས་ཡག་པོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="67"/>
        <source>Delete whole data belong user</source>
        <translation>གཞི་གྲངས་ཧྲིལ་པོ་བེད་སྤྱོད་བྱེད་མཁན་ལ་དབང་བ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="99"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="101"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>DigitalAuthIntelDialog</name>
    <message>
        <source>Enter Old Password</source>
        <translation type="vanished">གསང་གྲངས་རྙིང་པར་འཇུག་པ།</translation>
    </message>
    <message>
        <source>Forget Password?</source>
        <translation type="vanished">གསང་གྲངས་བརྗེད་སོང་ངམ།</translation>
    </message>
    <message>
        <source>Input New Password</source>
        <translation type="vanished">གསང་གྲངས་གསར་པ་ནང་འཇུག་</translation>
    </message>
    <message>
        <source>Input Password</source>
        <translation type="vanished">ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>The password input is error</source>
        <translation type="vanished">གསང་གྲངས་ནང་འཇུག་བྱེད་པ་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <source>Confirm New Password</source>
        <translation type="vanished">གསང་གྲངས་གསར་པ་གཏན་འཁེལ་</translation>
    </message>
    <message>
        <source>The password input is inconsistent</source>
        <translation type="vanished">གསང་གྲངས་ནང་འཇུག་བྱས་པ་གཅིག་མཐུན་མིན་པ།</translation>
    </message>
    <message>
        <source>New password can not be consistent of old password</source>
        <translation type="vanished">གསང་གྲངས་གསར་པ་ནི་གསང་གྲངས་རྙིང་བ་དང་གཅིག་མཐུན་ཡོང་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Password Change Failed</source>
        <translation type="vanished">གསང་གྲངས་བསྒྱུར་བཅོས་བྱས་ནས་ཕམ་ཉེས་བྱུང</translation>
    </message>
</context>
<context>
    <name>DigitalPhoneIntelDialog</name>
    <message>
        <source>Please Enter Edu OS Password</source>
        <translation type="vanished">Edu OS Password ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>The password input is error</source>
        <translation type="vanished">གསང་གྲངས་ནང་འཇུག་བྱེད་པ་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>DisplayPerformanceDialog</name>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="214"/>
        <source>Display Advanced Settings</source>
        <translation>སྔོན་ཐོན་གྱི་སྒྲིག་བཀོད་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="297"/>
        <source>Performance</source>
        <translation>འཁྲབ་སྟོན་གྱི་ནུས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="376"/>
        <source>Applicable to machine with discrete graphics, which can accelerate the rendering of 3D graphics.</source>
        <translation>ཁ་གསལ་གྱི་རི་མོ་སྤྱད་དེ་འཕྲུལ་འཁོར་ལ་སྤྱད་འཐུས་པས་3D图形་མགྱོགས་མྱུར་ངང་ཕྱིར་མངོན་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="392"/>
        <source>(Note: not support connect graphical with xmanager on windows.)</source>
        <translation>(མཆན། རྒྱབ་སྐྱོར་མི་བྱེད་པར་སྒེའུ་ཁུང་སྟེང་གི་xmanagerདང་འབྲེལ་མཐུད་བྱེད་མི་ཐུབ། )</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="462"/>
        <source>Compatible</source>
        <translation>ཕན་ཚུན་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="538"/>
        <source>Applicable to machine with integrated graphics,  there is no 3D graphics acceleration. </source>
        <translation>ཕྱོགས་བསྡུས་རི་མོ་ཡོད་པའི་འཕྲུལ་འཁོར་ལ་སྤྱད་འཐུས་པ་ལས་3Dརི་མོའི་མྱུར་ཚད་ཇེ་མགྱོགས་སུ་གཏོང་མི་ཐུབ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="554"/>
        <source>(Note: need connect graphical with xmanager on windows, use this option.)</source>
        <translation>(མཆན། སྒེའུ་ཁུང་སྟེང་གི་xmanagerདང་འབྲེལ་མཐུད་བྱེད་དགོས་ན། བསལ་འདེམས་འདི་བེད་སྤྱོད་བྱ་དགོས། )</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="604"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="vanished">རང་འགུལ་གྱིས་རང་</translation>
    </message>
    <message>
        <source>Auto select according to environment, delay the login time (about 0.5 sec).</source>
        <translation type="vanished">ཁོར་ཡུག་ལ་གཞིགས་ནས་རང་འགུལ་གྱིས་གདམ་གསེས་བྱས་ཏེ་ཐོ་འགོད་ཀྱི་དུས་ཚོད་ཕྱིར་འགྱངས་བྱ་དགོས། (ཧ་ལམ་སྐར་མ་0.5ཙམ་འགོར་གྱི་ཡོད། )</translation>
    </message>
    <message>
        <source>Threshold:</source>
        <translation type="vanished">སྒོ་ཐེམ་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="597"/>
        <source>Apply</source>
        <translation>རེ་ཞུ་</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ</translation>
    </message>
    <message>
        <source>(Note: select this option to use 3D graphics acceleration and xmanager.)</source>
        <translation type="vanished">(མཆན། བསལ་འདེམས་འདི་བདམས་ནས་3D图形་ཀྱི་མྱུར་ཚད་ཇེ་མགྱོགས་སུ་གཏོང་བ་དང་xmanagerབཀོལ་སྤྱོད་བྱེད་དགོས། )</translation>
    </message>
</context>
<context>
    <name>DisplaySet</name>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="36"/>
        <source>Screen</source>
        <translation>བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="38"/>
        <source>Display</source>
        <translation>འགྲེམས་སྟོན།</translation>
    </message>
</context>
<context>
    <name>DisplayWindow</name>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="14"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="32"/>
        <source>Display</source>
        <translation>འགྲེམས་སྟོན།</translation>
    </message>
    <message>
        <source>monitor</source>
        <translation type="vanished">ལྟ་ཞིབ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="320"/>
        <source>open monitor</source>
        <translation>སྒོ་འབྱེད་ལྟ་ཞིབ་ཡོ་བྱད</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="374"/>
        <source>Advanced</source>
        <translation>སྔོན་ཐོན་རང་བཞིན།</translation>
    </message>
    <message>
        <source>Mirror Display</source>
        <translation type="vanished">མེ་ལོང་འགྲེམས་སྟོན།</translation>
    </message>
    <message>
        <source>as main</source>
        <translation type="vanished">གཙོ་བོར་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="250"/>
        <source>screen zoom</source>
        <translation>བརྙན་ཤེལ་ཆེ་རུ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>follow the sunrise and sunset(17:55-05:04)</source>
        <translation type="vanished">ཉི་མ་ཤར་བ་དང་ཉི་མ་ནུབ་པའི་རྗེས་སུ་འབྲངས་པ། (17:55-05:04)</translation>
    </message>
    <message>
        <source>custom time</source>
        <translation type="vanished">གོམས་སྲོལ་གྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>opening time</source>
        <translation type="vanished">སྒོ་འབྱེད་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>closing time</source>
        <translation type="vanished">སྒོ་རྒྱག་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>color temperature</source>
        <translation type="vanished">ཁ་དོག་གི་དྲོད་ཚད།</translation>
    </message>
    <message>
        <source>warm</source>
        <translation type="vanished">དྲོ་སྐྱིད་ལྡན་པ།</translation>
    </message>
    <message>
        <source>cold</source>
        <translation type="vanished">གྲང་ངར་ཆེ་བ།</translation>
    </message>
</context>
<context>
    <name>Fonts</name>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="50"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="46"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="122"/>
        <source>Fonts</source>
        <translation>ཡིག་གཟུགས།</translation>
        <extra-contents_path>/Fonts/Fonts</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="146"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="124"/>
        <source>Font size</source>
        <translation>ཡིག་གཟུགས་ཆེ་ཆུང་།</translation>
        <extra-contents_path>/Fonts/Font size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="264"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="126"/>
        <source>Fonts select</source>
        <translation>ཡིག་གཟུགས་གདམ་གསེས་བྱེད་པ།</translation>
        <extra-contents_path>/Fonts/Fonts select</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="370"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="128"/>
        <source>Mono font</source>
        <translation>མོ་ཡིག་གི་ཡིག་གཟུགས།</translation>
        <extra-contents_path>/Fonts/Mono font</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="421"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="130"/>
        <source>Reset to default</source>
        <translation>བསྐྱར་དུ་ཁ་ཆད་དང་འགལ་བའི་གནས་</translation>
        <extra-contents_path>/Fonts/Reset to default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="144"/>
        <source>Small</source>
        <translation>ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="148"/>
        <source>Large</source>
        <translation>ཆེ་བ་</translation>
    </message>
</context>
<context>
    <name>GetShortcutWorker</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="59"/>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="83"/>
        <source>Null</source>
        <translation>སྟོང། </translation>
    </message>
</context>
<context>
    <name>GrubVerify</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="24"/>
        <source>Grub verify</source>
        <translation>Grubར་སྤྲོད་</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="29"/>
        <source>User:</source>
        <translation>སྤྱོད་མཁན་:</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="42"/>
        <source>Pwd</source>
        <translation>གསང་ཨང་</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="85"/>
        <source>Sure Pwd</source>
        <translation>གསང་ཨང་ངོས་འཛིན་</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="138"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="142"/>
        <source>Confirm</source>
        <translation>གཏན་ཁེལ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="197"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="235"/>
        <source>Inconsistency with pwd</source>
        <translation>ཐེངས་གཉིས་ནང་འཇུག་མི་མཐུན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="225"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="303"/>
        <source>pwd cannot be empty!</source>
        <translation>གསང་ཨང་སྟོང་མི་རུང་།!</translation>
    </message>
</context>
<context>
    <name>HostNameDialog</name>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="17"/>
        <source>Set HostName</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང་གཏན</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="45"/>
        <source>HostName</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="91"/>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="94"/>
        <source>Must be 1-64 characters long</source>
        <translation>ངེས་པར་དུ་ཡི་གེ་1-64ཡི་རིང་ཚད་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="161"/>
        <source>Hostname must start or end with a number and a letter</source>
        <translation>འཕྲུལ་འཁོར་གྱི་མིང་ལ་ངེས་པར་དུ་ཨང་ཀི་ཆུང་ཆུང་འབྲི་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="172"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>གཙོ་ཆས་མིང་ལ་ཚུད་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="179"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>འཕྲུལ་འཁོར་གཙོ་བོའི་མིང་ཚུད་མེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="75"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="71"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>InputPwdDialog</name>
    <message>
        <source>Set</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Set Password</source>
        <translation type="vanished">གསང་གྲངས་གཏན་འཁེལ་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="28"/>
        <source>VNC password</source>
        <translation>VNC གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="45"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="60"/>
        <source>Must be 1-8 characters long</source>
        <translation>ངེས་པར་དུ་ཡི་གེ་1-8ཀྱི་རིང་ཚད་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="77"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="81"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KbdLayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="68"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="144"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="222"/>
        <source>Variant</source>
        <translation>འགྱུར་ལྡོག་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="270"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="58"/>
        <source>Add Layout</source>
        <translation>བཀོད་པ་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="233"/>
        <source>Del</source>
        <translation>ཏེ་ཨར།</translation>
    </message>
</context>
<context>
    <name>KeyValueConverter</name>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="46"/>
        <source>System</source>
        <translation>ལམ་ལུགས།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="49"/>
        <source>Devices</source>
        <translation>སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="55"/>
        <source>Personalized</source>
        <translation>རང་གཤིས་ཅན་</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="52"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="58"/>
        <source>Account</source>
        <translation>རྩིས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="61"/>
        <source>Datetime</source>
        <translation>དུས་ཚོད་ཀྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="64"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="67"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="70"/>
        <source>Application</source>
        <translation>རེ་འདུན་ཞུ་ཡིག</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="73"/>
        <source>Investigation</source>
        <translation>བརྟག་དཔྱད།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="76"/>
        <source>Commoninfo</source>
        <translation>ཀུན་སྤྱོད་</translation>
    </message>
</context>
<context>
    <name>KeyboardControl</name>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">མཐེབ་གཞོང་།</translation>
    </message>
</context>
<context>
    <name>KeyboardMain</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="25"/>
        <source>Keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="102"/>
        <source>Keyboard settings</source>
        <translation>གཙོ་གནད་པང་ལེབ་ཀྱི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/Keyboard/Keyboard settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="114"/>
        <source>Input settings</source>
        <translation>ནང་འཇུག་གི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/Keyboard/Input settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="154"/>
        <source>Key repeat</source>
        <translation>ལྡེ་མིག་བསྐྱར་ཟློས་བྱེད་པ</translation>
        <extra-contents_path>/Keyboard/Key repeat</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="177"/>
        <source>Delay</source>
        <translation>འགོར་འགྱངས་བྱས་པ</translation>
        <extra-contents_path>/Keyboard/Delay</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="180"/>
        <source>Short</source>
        <translation>མདོར་ན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="183"/>
        <source>Long</source>
        <translation>རིང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="214"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
        <extra-contents_path>/Keyboard/Speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="217"/>
        <source>Slow</source>
        <translation>དལ་མོ།དལ་མོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="220"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="251"/>
        <source>Input test</source>
        <translation>ནང་འཇུག་ཚོད་ལྟ།</translation>
        <extra-contents_path>/Keyboard/Input test</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="272"/>
        <source>Key tips</source>
        <translation>འགག་རྩའི་མན་ངག</translation>
        <extra-contents_path>/Keyboard/Key tips</extra-contents_path>
    </message>
</context>
<context>
    <name>LanguageFrame</name>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="77"/>
        <source>Input Settings</source>
        <translation>ནང་འཇུག་གི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="78"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="121"/>
        <source>Manager Keyboard Layout</source>
        <translation>སྤྱི་གཉེར་བའི་མཐེབ་གཞོང་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="234"/>
        <source>Language</source>
        <translation>སྐད་ཆ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="250"/>
        <source>Country</source>
        <translation>རྒྱལ་ཁབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="293"/>
        <source>Variant</source>
        <translation>འགྱུར་ལྡོག་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="351"/>
        <source>Layout installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་བཀོད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="399"/>
        <source>Preview</source>
        <translation>སྔོན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="431"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="450"/>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <source>Warnning</source>
        <translation>ཐ་ཚིག་</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">རྒྱུན་ལྡན་གྱི་གནས་</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="195"/>
        <location filename="../../mainwindow.cpp" line="453"/>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="410"/>
        <location filename="../../mainwindow.cpp" line="431"/>
        <location filename="../../mainwindow.cpp" line="550"/>
        <location filename="../../mainwindow.cpp" line="1049"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <source>Main menu</source>
        <translation type="vanished">ཟས་ཐོ་གཙོ་བོ།</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">ཚལ་ཐོ་</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="192"/>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="425"/>
        <source>Back home</source>
        <translation>ཕྱིར་ལོག་མགོ་ངོས།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="451"/>
        <source>Option</source>
        <translation>འདེམས་ཚན་</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="452"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="454"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="538"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="540"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="542"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="687"/>
        <source>Specified</source>
        <translation>གཏན་འབེབས་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>This function has been controlled</source>
        <translation>འགན་ནུས་འདི་ཚོད་འཛིན་བྱས་ཟིན།</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>Form</source>
        <translation type="vanished">རྣམ་པ།</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="vanished">དོ་སྣང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>It takes effect after logging off</source>
        <translation type="vanished">ཤིང་གཅོད་བྱས་རྗེས་ནུས་པ་འཐོན་རྒྱུ།</translation>
    </message>
    <message>
        <source>Logout Now</source>
        <translation type="vanished">ད་ལྟ་ཐོ་འགོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Reboot Now</source>
        <translation type="vanished">ད་ལྟ་བསྐྱར་དུ་ལས་ཀ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>This cleanup and restore need to be done after the system restarts, whether to restart and restore immediately?</source>
        <translation type="vanished">ཐེངས་འདིའི་གཙང་བཤེར་དང་སླར་གསོ་བྱེད་པར་མ་ལག་སླར་གསོ་བྱས་རྗེས་འཕྲལ་མར་སླར་གསོ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>System Backup Tips</source>
        <translation type="vanished">མ་ལག་གི་རྗེས་གྲབས་མན་ངག</translation>
    </message>
</context>
<context>
    <name>MessageBoxDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">གླེང་མོལ།</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>You do not have administrator rights!</source>
        <translation type="vanished">ཁྱེད་ཚོར་དོ་དམ་པའི་དབང་ཆ་མེད།</translation>
    </message>
    <message>
        <source> Factory Settings cannot be restored!</source>
        <translation type="vanished"> བཟོ་གྲྭའི་སྒྲིག་བཀོད་སླར་གསོ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>MessageBoxPower</name>
    <message>
        <source>System Recovery</source>
        <translation type="vanished">མ་ལག་སླར་གསོ་</translation>
    </message>
    <message>
        <source>The battery is low,please connect the power</source>
        <translation type="vanished">གློག་སྨན་དམའ་བས་གློག་ཁུངས་སྦྲེལ་མཐུད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Keep the power connection, or the power is more than 25%.</source>
        <translation type="vanished">གློག་ཤུགས་སྦྲེལ་མཐུད་རྒྱུན་འཁྱོངས་བྱེད་པའམ་ཡང་ན་གློག་ཤུགས་བརྒྱ་ཆ་25ཡན་ཟིན་པ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Remind in 30 minutes</source>
        <translation type="vanished">སྐར་མ་30ཡི་ནང་དུ་དྲན་སྐུལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Got it</source>
        <translation type="vanished">རག་སོང་།</translation>
    </message>
</context>
<context>
    <name>MessageBoxPowerIntel</name>
    <message>
        <source>Nothing has been entered, re-enter</source>
        <translation type="vanished">ནང་དུ་ཅི་ཡང་མ་འཛུལ་བར་ཡང་བསྐྱར་ནང་འདྲེན་བྱས།</translation>
    </message>
    <message>
        <source>Remind in 30 minutes</source>
        <translation type="vanished">སྐར་མ་30ཡི་ནང་དུ་དྲན་སྐུལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Got it</source>
        <translation type="vanished">རག་སོང་།</translation>
    </message>
</context>
<context>
    <name>MouseControl</name>
    <message>
        <source>Mouse</source>
        <translation type="vanished">བྱི་བ།</translation>
    </message>
</context>
<context>
    <name>MouseUI</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="25"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="194"/>
        <source>Mouse</source>
        <translation>བྱི་བ།</translation>
        <extra-contents_path>/Mouse/Mouse</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="203"/>
        <source>Pointer</source>
        <translation>ཕྱོགས་སྟོན་འཁོར་ལོ།</translation>
        <extra-contents_path>/Mouse/Pointer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="212"/>
        <source>Cursor</source>
        <translation>དམོད་ཚིག</translation>
        <extra-contents_path>/Mouse/Cursor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="275"/>
        <source>Dominant hand</source>
        <translation>དབང་སྒྱུར་གྱི་གོ་གནས་</translation>
        <extra-contents_path>/Mouse/Dominant hand</extra-contents_path>
    </message>
    <message>
        <source>Left hand</source>
        <translation type="vanished">ལག་པ་གཡོན་པ།</translation>
    </message>
    <message>
        <source>Right hand</source>
        <translation type="vanished">ལག་པ་གཡས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="278"/>
        <source>Left key</source>
        <translation>ལག་པ་གཡས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="279"/>
        <source>Right key</source>
        <translation>ལག་པ་གཡས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="307"/>
        <source>Scroll direction</source>
        <translation>ཁ་ཕྱོགས་ལ་ཁ་ཕྱོགས་པ།</translation>
        <extra-contents_path>/Mouse/Scroll direction</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="310"/>
        <source>Forward</source>
        <translation>མདུན་དུ་སྐྱོད་པ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="311"/>
        <source>Reverse</source>
        <translation>ལྡོག་ཕྱོགས་སུ་འགྱུར་</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="340"/>
        <source>Wheel speed</source>
        <translation>འཁོར་ལོའི་མྱུར་ཚད།</translation>
        <extra-contents_path>/Mouse/Wheel speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="343"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="417"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="562"/>
        <source>Slow</source>
        <translation>དལ་མོ།དལ་མོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="352"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="427"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="572"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="376"/>
        <source>Double-click interval time</source>
        <translation>ལྡབ་འགྱུར་གྱིས་བར་མཚམས་ཀྱི་དུས་ཚོད།</translation>
        <extra-contents_path>/Mouse/Double-click interval time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="379"/>
        <source>Short</source>
        <translation>མདོར་ན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="389"/>
        <source>Long</source>
        <translation>རིང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="414"/>
        <source>Pointer speed</source>
        <translation>ཕྱོགས་སྟོན་འཁོར་ལོ་མགྱོགས་ཚད།</translation>
        <extra-contents_path>/Mouse/Pointer speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="452"/>
        <source>Mouse acceleration</source>
        <translation>བྱི་བའི་མགྱོགས་ཚད།</translation>
        <extra-contents_path>/Mouse/Mouse acceleration</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="474"/>
        <source>Show pointer position when pressing ctrl</source>
        <translation>ctrl མནན་དུས་ཕྱོགས་སྟོན་གྱི་གནས་བབ་མངོན་པར་བྱེད་དགོས།</translation>
        <extra-contents_path>/Mouse/Show pointer position when pressing ctrl</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="497"/>
        <source>Pointer size</source>
        <translation>ཕྱོགས་སྟོན་འཁོར་ལོ་ཆེ་ཆུང་།</translation>
        <extra-contents_path>/Mouse/Pointer size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="500"/>
        <source>Small(recommend)</source>
        <translation>ཆུང་བ་(འོས་སྦྱོར)།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="501"/>
        <source>Medium</source>
        <translation>འབྲིང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="502"/>
        <source>Large</source>
        <translation>ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="536"/>
        <source>Blinking cursor in text area</source>
        <translation>ཡི་གེའི་ཁྱབ་ཁོངས་སུ་འོད་ཆེམ་ཆེམ་བྱེད་པ།</translation>
        <extra-contents_path>/Mouse/Blinking cursor in text area</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="559"/>
        <source>Cursor speed</source>
        <translation>ཀྱག་ཀྱོག་གི་མྱུར་ཚད།</translation>
        <extra-contents_path>/Mouse/Cursor speed</extra-contents_path>
    </message>
</context>
<context>
    <name>MyLabel</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="130"/>
        <source>double-click to test</source>
        <translation>ཚད་ལེན་ཚོད་ལྟ་ཐེངས་གཉིས་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>Notice</name>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="162"/>
        <source>NotFaze Mode</source>
        <translation>མི་དམངས་ཀྱི་དཔེ་དབྱིབས་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="164"/>
        <source>(Notification banners, prompts will be hidden, and notification sounds will be muted)</source>
        <translation>(བརྡ་ཐོ་གཏོང་བའི་འཕྲེད་འགེལ་སྦྱར་ཡིག་དང་། བརྡ་གཏོང་ཡི་གེ་སྦས་སྐུང་བྱས་ནས་བརྡ་ཁྱབ་ཀྱི་སྒྲ་གྲགས་ཡོང་། )</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="238"/>
        <source>Automatically turn on</source>
        <translation>རང་འགུལ་གྱིས་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="263"/>
        <source>to</source>
        <translation>དེ་ལྟར་བྱས་ན་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="288"/>
        <source>Automatically turn on when multiple screens are connected</source>
        <translation>བརྙན་ཤེལ་མང་པོ་སྦྲེལ་མཐུད་བྱེད་སྐབས་རང་འགུལ་གྱིས་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="293"/>
        <source>Automatically open in full screen mode</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་པོའི་རྣམ་པའི་ཐོག་ནས་རང་འགུལ་གྱིས་སྒོ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="298"/>
        <source>Allow automatic alarm reminders in Do Not Disturb mode</source>
        <translation>རང་འགུལ་གྱིས་ཉེན་བརྡ་གཏོང་བའི་དྲན་སྐུལ་བྱེད་སྟངས་ལ་སུན་པོ་བཟོ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="329"/>
        <source>Notice Settings</source>
        <translation>བརྡ་ཐོའི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/Notice/Notice Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="331"/>
        <source>Get notifications from the app</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་ཁྲོད་ནས་བརྡ་ཐོ་གཏོང་དགོས།</translation>
        <extra-contents_path>/Notice/Get notifications from the app</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="51"/>
        <source>Notice</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
</context>
<context>
    <name>NoticeMenu</name>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="41"/>
        <source>Beep sound when notified</source>
        <translation>བརྡ་ཐོ་གཏོང་སྐབས་སྐད་ཅོར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="47"/>
        <source>Show message  on screenlock</source>
        <translation>བརྙན་ཤེལ་སྟེང་ནས་ཆ་འཕྲིན་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="53"/>
        <source>Show noticfication  on screenlock</source>
        <translation>བརྙན་ཤེལ་སྟེང་ནས་དོ་སྣང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="57"/>
        <source>Notification Style</source>
        <translation>བརྡ་ཐོ་གཏོང་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="65"/>
        <source>Banner: Appears in the upper right corner of the screen, and disappears automatically</source>
        <translation>འཕྲེད་འགེལ་ཡི་གེ། བརྙན་ཤེལ་གྱི་གཡས་ཟུར་དུ་མངོན་པ་མ་ཟད། རང་འགུལ་གྱིས་མེད་པར་གྱུར་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="70"/>
        <source>Tip:It will be kept on the screen until it is closed</source>
        <translation>རྩེ་མོ། འཆར་ངོས་སུ་ཉར་ཚགས་བྱས་ནས་སྒོ་མ་བརྒྱབ་གོང་ལ་ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="75"/>
        <source>None:Notifications will not be displayed on the screen, but will go to the notification center</source>
        <translation>གཅིག་ཀྱང་མ་ལུས་པར་འཆར་ངོས་སུ་བརྡ་ཐོ་མི་མངོན་པར་བརྡ་ཁྱབ་ལྟེ་གནས་སུ་འགྲོ་རྒྱུ་རེད།</translation>
    </message>
</context>
<context>
    <name>NumbersButtonIntel</name>
    <message>
        <source>clean</source>
        <translation type="vanished">གཙང་སྦྲ་དོད་པ།</translation>
    </message>
</context>
<context>
    <name>OutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="120"/>
        <source>resolution</source>
        <translation>གྲོས་ཆོད།</translation>
        <extra-contents_path>/Display/resolution</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="156"/>
        <source>orientation</source>
        <translation>ཁ་ཕྱོགས་</translation>
        <extra-contents_path>/Display/orientation</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="174"/>
        <source>arrow-up</source>
        <translation>མདའ་འཕེན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="175"/>
        <source>90° arrow-right</source>
        <translation>90°མདའ་གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="177"/>
        <source>arrow-down</source>
        <translation>མདའ་མོ་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="193"/>
        <source>frequency</source>
        <translation>ཐེངས་གྲངས།</translation>
        <extra-contents_path>/Display/frequency</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="176"/>
        <source>90° arrow-left</source>
        <translation>90°མདའ་གཡོན་ཕྱོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="394"/>
        <source>auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="513"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
</context>
<context>
    <name>PhoneAuthIntelDialog</name>
    <message>
        <source>Wechat Auth</source>
        <translation type="vanished">ཝེ་ཆི་ཐི་ཨོ་སི་ཁྲུ་ལི་ཡ།</translation>
    </message>
    <message>
        <source>Phone Auth</source>
        <translation type="vanished">ཁ་པར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <source>Phone number</source>
        <translation type="vanished">ཁ་པར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <source>SMS verification code</source>
        <translation type="vanished">SMS ཞིབ་བཤེར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <source>GetCode</source>
        <translation type="vanished">ཨང་ཀི་ཐོབ་པ།</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">ཕྱིར་སློག་པ།</translation>
    </message>
    <message>
        <source>Commit</source>
        <translation type="vanished">བསྒྲུབ་རྒྱུ་ཁས་ལེན་</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">ངོས་འཛིན་བྱས་པ།</translation>
    </message>
    <message>
        <source>commit</source>
        <translation type="vanished">བསྒྲུབ་རྒྱུ་ཁས་ལེན་</translation>
    </message>
    <message>
        <source>Mobile number acquisition failed</source>
        <translation type="vanished">སྒུལ་བདེའི་ཨང་གྲངས་ཉོ་སྒྲུབ་བྱེད་པར་ཕམ་</translation>
    </message>
    <message>
        <source>Recapture</source>
        <translation type="vanished">ཕྱིར་འཕྲོག་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Network connection failure, please check</source>
        <translation type="vanished">དྲ་སྦྲེལ་ལ་སྐྱོན་ཤོར་བས་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Phone is lock,try again in an hour</source>
        <translation type="vanished">ཁ་པར་ལ་ཟྭ་བརྒྱབ་ནས་དུས་ཚོད་གཅིག་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་</translation>
    </message>
    <message>
        <source>Phone code is wrong</source>
        <translation type="vanished">ཁ་པར་ཨང་གྲངས་ནོར་སོང་།</translation>
    </message>
    <message>
        <source>Current login expired,using wechat code!</source>
        <translation type="vanished">ད་ལྟའི་ཐོ་འགོད་དུས་ཚོད་ཐིམ་ནས་འཕྲིན་ཕྲན་གྱི་ཚབ་རྟགས་བཀོལ་སྤྱོད་བྱས།</translation>
    </message>
    <message>
        <source>Unknown error, please try again later</source>
        <translation type="vanished">ནོར་འཁྲུལ་མི་ཤེས་པས་ཕྱིས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་</translation>
    </message>
    <message>
        <source>Please use the correct wechat scan code</source>
        <translation type="vanished">ཡང་དག་པའི་འཕྲིན་ཕྲན་ཞིབ་བཤེར་གྱི་ཚབ་རྟགས་བཀོལ་རོགས།</translation>
    </message>
</context>
<context>
    <name>Power</name>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="62"/>
        <source>Power</source>
        <translation>སྟོབས་ཤུགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="254"/>
        <source>The system will sleep before turning off the display</source>
        <translation>མ་ལག་གི་སྒོ་རྒྱག་འཆར་ཆས་གོང་ལ་གཉིད་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="326"/>
        <source>Reduce the occupation of backend running program resources and ensure smooth operation of key and focus applications.</source>
        <translation>རྒྱབ་སྟེགས་འཁོར་རྒྱུག་གོ་རིམ་གྱི་ཐོན་ཁུངས་བཟུང་སྤྱོད་ཉུང་དུ་གཏོང་བ།འགག་རྩའི་བཀོལ་སྤྱོད་དང་མདོ་གནད་བཀོལ་སྤྱོད་བདེ་བླག་ཡོང་བའི་འགན་ལེན་བཅས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="606"/>
        <source>Require password when suspend</source>
        <translation>གཉིད་ཉལ་སྐབས་གསང་གྲངས་དགོས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="605"/>
        <source>Require password when suspend/hibernate</source>
        <translation>གཉིད་དང་གཉིད།གཉིད།གཉིད་སད་པའི་རྗེས་ནས་གསང་གྲངས་</translation>
        <extra-contents_path>/Power/Require password when suspend/hibernate</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="607"/>
        <source>Require password when hibernate</source>
        <translation>གཉིད་ལས་བསད་པར་གསང་གྲངས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>never</source>
        <translation>གཏན་ནས་བྱེད་མི་</translation>
    </message>
    <message>
        <source>Require password when suspend/hibernation</source>
        <translation type="vanished">གཉིད་ཉལ་སྐབས་གསང་གྲངས་དགོས་པའི་བླང་བྱ་བཏོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="616"/>
        <location filename="../../../plugins/system/power/power.cpp" line="617"/>
        <source>Password required when waking up the screen</source>
        <translation>བརྙན་ཤེལ་གཉིད་ལས་སད་སྐབས་མཁོ་བའི་གསང་གྲངས།</translation>
        <extra-contents_path>/Power/Password required when waking up the screen</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="621"/>
        <source>Press the power button</source>
        <translation>སྒུལ་ཤུགས་ཀྱི་མཐེབ་གཅུས་</translation>
        <extra-contents_path>/Power/Press the power button</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="626"/>
        <location filename="../../../plugins/system/power/power.cpp" line="627"/>
        <source>Time to close display</source>
        <translation>འགྲེམས་སྟོན་མཇུག་སྒྲིལ་བའི་དུས་ཚོད།</translation>
        <extra-contents_path>/Power/Time to close display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="631"/>
        <location filename="../../../plugins/system/power/power.cpp" line="632"/>
        <source>Time to sleep</source>
        <translation>གཉིད་ཉལ་བའི་དུས་ཚོད།</translation>
        <extra-contents_path>/Power/Time to sleep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="636"/>
        <location filename="../../../plugins/system/power/power.cpp" line="637"/>
        <source>Notebook cover</source>
        <translation>ཟིན་བྲིས་ཀྱི་ཁེབས་རས།</translation>
        <extra-contents_path>/Power/Notebook cover</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="641"/>
        <location filename="../../../plugins/system/power/power.cpp" line="642"/>
        <source>Dynamic resource scheduling</source>
        <translation>ཐོན་ཁུངས་འགུལ་རྣམ་བཀོད་གཏོང་</translation>
        <extra-contents_path>/Power/Dynamic resource scheduling</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="646"/>
        <location filename="../../../plugins/system/power/power.cpp" line="647"/>
        <source>Using power</source>
        <translation>དབང་ཆ་བེད་སྤྱོད་བྱ་དགོས།</translation>
        <extra-contents_path>/Power/Using power</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="651"/>
        <location filename="../../../plugins/system/power/power.cpp" line="652"/>
        <source>Using battery</source>
        <translation>གློག་སྨན་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
        <extra-contents_path>/Power/Using battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="655"/>
        <location filename="../../../plugins/system/power/power.cpp" line="656"/>
        <source> Time to darken</source>
        <translation> མུན་ནག་ཏུ་འགྱུར་བའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="660"/>
        <source>Battery level is lower than</source>
        <translation>གློག་གཡིས་ཀྱི་ཆུ་ཚད་ལས་དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="663"/>
        <source>Run</source>
        <translation>འཁོར་སྐྱོད་བྱེད་བཞིན་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="666"/>
        <location filename="../../../plugins/system/power/power.cpp" line="667"/>
        <source>Low battery notification</source>
        <translation>གློག་གཡིས་ཀྱི་བརྡ་ཐོ་དམའ་བ།</translation>
        <extra-contents_path>/Power/Low battery notification</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="671"/>
        <source>Automatically run saving mode when low battery</source>
        <translation>གློག་གཡིས་དམའ་བའི་དུས་སུ་རང་འགུལ་གྱིས་གྲོན་ཆུང་བྱེད་ཐབས་</translation>
        <extra-contents_path>/Power/&quot;Automatically run saving mode when low battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="672"/>
        <source>Automatically run saving mode when the low battery</source>
        <translation>གློག་སྨན་དམའ་བའི་སྐབས་སུ་རང་འགུལ་གྱིས་གྲོན་ཆུང་བྱེད་སྟངས་འཁོར་སྐྱོད་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="675"/>
        <location filename="../../../plugins/system/power/power.cpp" line="676"/>
        <source>Automatically run saving mode when using battery</source>
        <translation>གློག་གཡིས་བཀོལ་སྤྱོད་བྱེད་སྐབས་རང་འགུལ་གྱིས་གྲོན་ཆུང་བྱེད་སྟངས་བཀོལ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="679"/>
        <location filename="../../../plugins/system/power/power.cpp" line="680"/>
        <source>Display remaining charging time and usage time</source>
        <translation>དེ་བྱིངས་ཀྱི་གློག་གསོག་དུས་ཚོད་དང་བཀོལ་སྤྱོད་ཀྱི་དུས་ཚོད་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="164"/>
        <source>General</source>
        <translation>སྤྱིར་བཏང་གི་གནས</translation>
        <extra-contents_path>/Power/General</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="349"/>
        <source>Select Powerplan</source>
        <translation>སྒུལ་ཤུགས་འཆར་གཞི་བདམས་པ།</translation>
        <extra-contents_path>/Power/Select Powerplan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="407"/>
        <source>Battery saving plan</source>
        <translation>གློག་གཡིས་གྲོན་ཆུང་གི་འཆར་</translation>
        <extra-contents_path>/Power/Battery saving plan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>nothing</source>
        <translation>ཅི་ཡང་མ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>blank</source>
        <translation>སྟོང་ཆ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>suspend</source>
        <translation>གཉིད་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>hibernate</source>
        <translation>མངལ་གནས་སུ་སྦས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <source>interactive</source>
        <translation>ཕན་ཚུན་སྐུལ་འདེད་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <source>5min</source>
        <translation>5སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>10min</source>
        <translation>10སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>15min</source>
        <translation>15སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>30min</source>
        <translation>30སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>1h</source>
        <translation>1ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>2h</source>
        <translation>2ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>3h</source>
        <translation>3ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Balance</source>
        <translation>དོ་མཉམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Energy Efficiency</source>
        <translation>ཕན་འབྲས་ལེགས་ཤོས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Performance</source>
        <translation>མཆོག་གྱུར་གཤིས་ནུས།</translation>
    </message>
    <message>
        <source>Balance Model</source>
        <translation type="vanished">དོ་མཉམ་གྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <source>Save Model</source>
        <translation type="vanished">གསོག་འཇོག་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>Performance Model</source>
        <translation type="vanished">གྲུབ་འབྲས་ཀྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">1min</translation>
    </message>
    <message>
        <source>20min</source>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>shutdown</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
</context>
<context>
    <name>Printer</name>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="43"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="133"/>
        <source>Printers</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར།</translation>
        <extra-contents_path>/Printer/Printers</extra-contents_path>
    </message>
    <message>
        <source>Printers And Scanners</source>
        <translation type="vanished">པར་འདེབས་འཕྲུལ་འཁོར་དང་བཤེར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="179"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Printer/Add</extra-contents_path>
    </message>
</context>
<context>
    <name>PrivacyDialog</name>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="11"/>
        <source>Set</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="26"/>
        <source>End User License Agreement and Privacy Policy Statement of Kylin</source>
        <translation>ཅིན་ལིན་གྱི་སྤྱོད་མཁན་གྱི་ཆོག་འཐུས་གྲོས་མཐུན་དང་གསང་བའི་སྲིད་ཇུས་ཀྱི་གསལ་བསྒྲགས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="31"/>
        <source>Dear users of Kylin operating system and relevant products, 
         This agreement describes your rights, obligations and prerequisites for your use of this product. Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”).
        “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products.

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021
        The Agreement shall include the following content:
        I.     User license 
        II.    Java technology limitations
        III.   Cookies and other technologies
        IV.    Intellectual property clause
        V.     Open source code
        VI.   The third-party software/services
        VII.  Escape clause
        VIII. Integrity and severability of the Agreement
        IX.    Applicable law and dispute settlement

        I.      User license
        According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
        1.     User license for educational institutions
        In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product.
        2.     Use of the font software
        Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

        II.    Java technology limitations
        You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

        III.   Cookies and other technologies
        In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
        If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers.
        In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

        IV.   Intellectual property clause
        1.    Trademarks and Logos
        This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
        2.    Duplication, modification and distribution
        If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
        Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws.

        V.    Open source code
        For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

        VI.  The third-party software/services
        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
        We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

        VII. Escape clause
        1.    Limited warranty
        We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
        2.   Disclaimer
        In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
        3.   Limitation of responsibility
        To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

        VIII.Integrity and severability of the Agreement
        1.    The integrity of the Agreement
        The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
        2.   Severability of the Agreement
        If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

        IX.  Applicable law and dispute settlement
        1.   Application of governing laws
        Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
        2.  Termination
        If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
        The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.
        The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

        Privacy Policy Statement of Kylin Operating System/n        Release date of the version: July 30, 2021
        Effective date of the version: July 30, 2021

        We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.

        The Statement shall include the following content:
        I.   Collection and use your personal information
        II.  How to store and protect your personal information
        III. How to manage your personal information
        IV.  Privacy of the third-party software/services
        V.   Minors’ use of the products
        VI.  How to update this Statement
        VII. How to contact us

        I.     How to collect and use your personal information
        1.    The collection of personal information
        We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
        1)   The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
        2)   Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
        3)   Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
        4)   Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
        5)   The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
        6)   This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
        7)   The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
        8)   In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.
        2.   Use of personal information
        We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
        1)   The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
        2)   We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
        3)   We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
        4)   We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
        5)   We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
        6)   We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications andother channels;
        7)   In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.
        3.   Information sharing and provision
        We shall not share or transfer your personal information to any third party, except for the following circumstances:
        1)   After obtaining your clear consent, we shall share your personal information with the third parities;
        2)   In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
        3)   We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
        4)   With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
        5)   If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.
        4.   Exceptions with authorized consent
        1)   It is directly related to national security, national defense security and other national interests; 
        2)   It is directly related to public safety, public health and public knowledge and other major public interests; 
        3)   It is directly related to crime investigation, prosecution, judgment and execution of judgment; 
        4)   It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent; 
        5)   The personal information collected is disclosed to the public by yourself; 
        6)   Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels; 
        7)   It is necessary to sign and perform of the contract according to your requirement; 
        8)   It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
        9)   It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
        10) Other circumstances specified in the laws and regulations.

        II.   How to store and protect personal information
        1.   Information storage place
        We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
        2.   Information storage duration 
        Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
        When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
        3.   How to protect the information
        We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
        We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
        We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
        4.   Emergency response plan
        In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

        III. How to manage your personal information
        If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs. 
        Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

        IV.  Privacy of the third-party software/services

        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
        When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

        V.   Minors’ use of the products
        If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

        VI.  How to update this Statement
        We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the accountcreated by you in this product; if you are a guardian, please help your minor child to close the account created by him/her in this product.

        VII. How to contact us
        If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn). 
        We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
        The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
        Last date of updating: November 1, 2021

Address:
        Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
        Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
        Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
        Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.:
        Tianjin (022) 58955650      Beijing (010) 51659955
        Changsha (0731) 88280170        Shanghai (021) 51098866
Fax:
        Tianjin (022) 58955651      Beijing (010) 62800607
        Changsha (0731) 88280166        Shanghai (021) 51062866

        Company website: www.kylinos.cn
        E-mail: support@kylinos.cn</source>
        <translation>ཅིན་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་དང་འབྲེལ་ཡོད་ཐོན་རྫས་ཀྱི་སྙིང་ཉེ་བའི་སྤྱོད་མཁན། 
         གྲོས་མཐུན་འདིས་ཁྱེད་ཚོས་ཐོན་རྫས་འདི་བཀོལ་སྤྱོད་བྱེད་པའི་ཁེ་དབང་དང་། འོས་འགན། སྔོན་འགྲོའི་ཆ་རྐྱེན་བཅས་གསལ་བཤད་བྱས་ཡོད། ཁྱེད་ཀྱིས་གྲོས་མཐུན་གྱི་དོན་ཚན་དང་ཁ་གསབ་ཆོག་འཐུས་ལག་ཁྱེར་(གཤམ་དུ་ཐུན་མོང་དུ་&quot;གྲོས་མཐུན་&quot;ཞེས་འབོད་པ་)དང་ཁེ་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་གི་གསང་བའི་སྲིད་ཇུས་གསལ་བསྒྲགས་(གཤམ་དུ་&quot;གསལ་བསྒྲགས་&quot;ཞེས་འབོད་རྒྱུ་)ཀློག་རོགས།
        《ཆོད་ཡིག་》དང་《གསལ་བསྒྲགས་》ནང་གི་&quot;ཐོན་རྫས་འདི་&quot;ཞེས་པ་ནི་ཁེ་ལིན་སའོ་ཧྥུ་མ་རྐང་ཚད་ཡོད་ཀུང་སིས་གསར་སྤེལ་དང་། ཐོན་སྐྱེད། ཁྱབ་བསྒྲགས་བཅས་བྱས་པའི་&quot;ཁེ་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་གི་མཉེན་ཆས་ཐོན་རྫས་&quot;ལ་ཟེར། &quot;ང་ཚོ་&quot;ཞེས་པ་ནི་ཁེ་ལིན་སའོ་ཧྥུ་མ་རྐང་ཚད་ཡོད་ཀུང་སིའི་&quot;ཁྱོད་&quot;ཞེས་པ་ནི་ཁེ་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་དང་འབྲེལ་ཡོད་ཐོན་རྫས་བེད་སྤྱོད་བྱེད་མཁན་ལ་ཟེར།

ཅིན་ལིན་གྱི་སྤྱོད་མཁན་གྱི་ཆོག་འཐུས་གྲོས་མཐུན་མཇུག་བསྒྲིལ 
པར་གཞི་ཁྱབ་བསྒྲགས་བྱས་པའི་ཚེས་གྲངས། 2021ལོའི་ཟླ་7ཚེས་30ཉིན།
པར་གཞི་འདི་ལག་བསྟར་བྱེད་པའི་དུས་ཚོད། 2021ལོའི་ཟླ་7ཚེས་30ཉིན།
        གྲོས་མཐུན་ནང་གཤམ་གསལ་གྱི་ནང་དོན་ཚུད་དགོས་པ་སྟེ།
        I.     སྤྱོད་མཁན་གྱི་ལག་ཁྱེར། 
        II.Javaལག་རྩལ་གྱི་ཚད་བཀག
        གསུམ། བག་ལེབ་ཀོར་མོ་སོགས་ཀྱི་ལག་རྩལ།
        IV.ཤེས་བྱའི་ཐོན་དངོས་བདག་དབང་གི་དོན་ཚན།
        V.     སྒོ་འབྱེད་འབྱུང་ཁུངས་ཀྱི་ཚབ་རྟགས
        དྲུག་པ། ཕྱོགས་གསུམ་པའི་མཉེན་ཆས་དང་ཞབས་ཞུ།
        བདུན། བྲོས་བྱོལ་དུ་སོང་བའི་དོན་ཚན།
        བརྒྱད། གྲོས་མཐུན་གྱི་ཁ་དན་ཚིག་གནས་རང་བཞིན་དང་ཚབས་ཆེའི་རང་བཞིན།
        IX.བཅའ་ཁྲིམས་སྤྱད་འཐུས་པ་དང་རྩོད་གཞི་ཐག་གཅོད་བྱ་དགོས།

I.      སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།
        ཐོན་རྫས་འདི་དང་རྩིས་འཁོར་གྱི་མཁྲེགས་ཆས་རིགས་ལ་རིན་དོད་སྤྲད་ཟིན་པའི་སྤྱོད་མཁན་གྱི་མི་གྲངས་དང་རྩིས་འཁོར་གྱི་མཁྲེགས་ཆས་རིགས་ལ་གཞིགས་ནས་ང་ཚོས་ཁྱོད་ལ་ཕྱིར་འབུད་དང་སྤོ་སྒྱུར་བྱས་མི་ཆོག་པའི་ལག་ཁྱེར་སྤྲོད་དགོས་
        1.སློབ་གསོའི་ལས་ཁུངས་ཀྱི་སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།
        གྲོས་མཐུན་གྱི་དོན་ཚན་དང་ཆ་རྐྱེན་ལ་བརྩི་སྲུང་བྱེད་སྐབས་གལ་ཏེ་ཁྱོད་ནི་སློབ་གསོའི་ལས་ཁུངས་ཡིན་ན། ཁྱོད་ཀྱི་ལས་ཁུངས་ཀྱིས་ཟུར་བཀོད་བྱས་མེད་པའི་རྒྱུ་གཉིས་རྣམ་གཞག་གི་མཉེན་ཆས་སྤྱད་དེ་ནང་ཁུལ་དུ་སྤྱོད་དུ་འཇུག་དགོས། འདིར་བཤད་པའི་&quot;ནང་ཁུལ་དུ་སྤྱོད་རྒྱུ་&quot;ཞེས་པ་ནི་ཆོག་མཆན་ཐོབ་པའི་སྡེ་ཚན་དང་སྡེ་ཚན་དེ་གའི་ངལ་རྩོལ་གན་རྒྱ་འཇོག་མཁན་ལས་བཟོ་པ་དང་དེ་བཞིན་ཁྱེད་ཚོའི་ལས་ཁུངས་ཀྱིས་བསྡུ་ལེན་བྱས་པའི་སློབ་མས་ཐོན་རྫས་འདི་བེད་སྤྱོད་བྱས་ཆོག་པར་ཟེར
        2.ཡིག་གཟུགས་མཉེན་ཆས་བཀོལ་སྤྱོད་བྱེད་པ།
        ཡིག་གཟུགས་མཉེན་ཆས་ཞེས་པ་ནི་ཐོན་རྫས་ནང་སྔོན་ཚུད་ནས་སྒྲིག་སྦྱོར་བྱས་པའི་མཉེན་ཆས་དང་ཡིག་གཟུགས་ཀྱི་རྣམ་པ་ཐོན་སྐྱེད་བྱེད་པར་ཟེར། ཁྱེད་ཚོས་ཡིག་གཟུགས་མཉེན་ཆས་དང་མཉེན་ཆས་ལོགས་སུ་འབྱེད་མི་ཐུབ་པ་དང་། ཡིག་གཟུགས་མཉེན་ཆས་བཟོ་བཅོས་རྒྱག་མི་ཐུབ་པས། ཡིག་གཟུགས་མཉེན་ཆས་དེ་རིགས་ཐོན་རྫས་འདིའི་ཆ་ཤས་ཤིག་ཡིན་པའི་ཆ་ནས་ཁྱེད་ཚོར་སྤྲད་པའི་དུས་སུ་མེད་པའམ་ཡང་ན་རིན་དོད།  ཡང་ན་ཐོན་རྫས་དེ་རིགས་སྒྲིག་སྦྱོར་བྱས་མེད་པའི་སྒྲིག་ཆས་ནང་བེད་སྤྱོད་བྱེད་མི་རུང་། གལ་ཏེ་ཡིག་གཟུགས་མཉེན་ཆས་སྤྱད་དེ་ཕྱི་ཕྱོགས་ལ་དྲིལ་བསྒྲགས་བྱེད་པ་སོགས་ཚོང་ལས་ཀྱི་ཆེད་དུ་ཡིན་ན། ཡིག་གཟུགས་པར་དབང་བཟོ་མཁན་དང་འབྲེལ་གཏུག་དང་གྲོས་མོལ་བྱས་ནས་འབྲེལ་ཡོད་བྱ་སྤྱོད་ཀྱི་ཆོག་མཆན་ཐོབ་རོགས།

II.Javaལག་རྩལ་གྱི་ཚད་བཀག
        JPIནང་དུ་ཁ་སྣོན་བྱས་པའི་འཛིན་གྲྭ་གསར་སྐྲུན་བྱེད་པའམ་ཡང་ན་བྱེད་ཐབས་གཞན་དག་སྤྱད་དེ་JPIནང་གི་འཛིན་གྲྭ་ཁ་སྣོན་དང་བསྒྱུར་བཅོས་བྱེད་པ་གང་ཡིན་རུང་&quot;Java ལས་སྟེགས་མཐུད་ཁ་&quot;(&quot;JPI&quot;ཞེས་འབོད་པ་)བསྒྱུར་མི་རུང་། གལ་ཏེ་ཁྱོད་ཀྱིས་ཟུར་སྣོན་རིགས་ཤིག་གསར་སྐྲུན་བྱས་པ་མ་ཟད། ད་དུང་འབྲེལ་ཡོད་ཀྱི་APIsགཉིས་གསར་སྐྲུན་བྱས་ན། དེ་དག་(i)Javaསྟེགས་བུའི་ནུས་པ་རྒྱ་བསྐྱེད་པ་དང་། (གཉིས། )ཕྱོགས་གསུམ་པའི་མཉེན་ཆས་གསར་སྤེལ་བྱེད་མཁན་གྱིས་གོང་གསལ་གྱི་ཟུར་སྣོན་APIsཞེས་འབོད་སྲིད་པའི་ཟུར་སྣོན་མཉེན་ཆས་གསར་སྤེལ་བྱས་ཆོག་པས། ཁྱེད་ཚོས་ངེས་པར་དུ་འཕྲལ་མར་གསར་སྤེལ་ཚོང་པ་ཚང་མས་ཡོངས་ཁྱབ་ཏུ་APIsདེ་རིགས་རྒྱ་ཁྱབ་ཏུ་ཁྱབ། ཁྱེད་ཚོས་ཆོག་འཐུས་ལག་ཁྱེར་གཞན་དག་གསར་སྐྲུན་དང་དབང་ཆ་བསྐུར་ནས་བྱེད་སྟངས་གང་རུང་གི་ཐོག་ནས་&quot;java&quot;དང་། &quot;javax&quot;། &quot;sun&quot;བཅས་སུ་རྟགས་བརྒྱབ་པའི་འཛིན་གྲྭ་དང་། འབྲེལ་མཐུད། ཡན་ལག་ཁུག་མ་བཅས་གསར་སྐྲུན་བྱེད་མི་ཐུབ་པའམ་ཡང་ན་དབང་ཆ། Javaའཁོར་སྐྱོད་སྐབས་ཀྱི་ཁོར་ཡུག་གཉིས་གོང་འགྲིལ་ལུགས་ཚབ་ཨང་ཆོག་འཐུས་ལག་ཁྱེར་(མིག་སྔར་http://jdk.java.net་གནས་ཡོད་)ཡི་འོས་འཚམ་གྱི་པར་གཞི་ལ་བལྟས་ནས་Java བྱ་རིམ་ཆུང་བ།དང་ཉེར་སྤྱོད་གོ་རིམ་དང་མཉམ་འབྲེལ་གྱིས་འགྲེམ་སྤེལ་བྱས་པའི་འཁོར་སྐྱོད་དུས་ཚོད་ཀྱི་ཚབ་རྟགས་འདོན་སྤྲོད་བྱེད་ཐུབ་མིན་ལ་རྒྱུས་ལོན་བྱེད་དགོས།

གསུམ། བག་ལེབ་ཀོར་མོ་སོགས་ཀྱི་ལག་རྩལ།
        ང་ཚོས་སྔར་ལས་ལྷག་པའི་སྒོ་ནས་སྤྱོད་མཁན་ལ་རྒྱུས་ལོན་དང་ཞབས་འདེགས་ཞུ་བར་རོགས་རམ་བྱེད་ཆེད། ང་ཚོའི་དྲ་ཚིགས་དང་། དྲ་ཐོག་ཞབས་ཞུ། ཉེར་སྤྱོད་གོ་རིམ་བཅས་ཀྱིས་&quot;ཀ་ར་གོ་རེ་&quot;ལག་རྩལ ཀ་ར་གོ་རེ་འདི་རིགས་ནི་མ་ལག་ནང་དུ་འགྲོ་འོང་བྱེད་པའི་དྲ་རྒྱའི་འགྲིམ་འགྲུལ་དང་ཞིབ་དཔྱད་ཚད་ལེན་གྱི་ནོར་འཁྲུལ་ལས་བྱུང་བའི་འགྲིམ་འགྲུལ་གསོག་ཉར་བྱེད་པར་བཀོལ་བ་ཡིན་པས། ངེས་པར་དུ་གཏན་འཁེལ་བྱ་དགོས། ང་ཚོས་ཁྱོད་ཀྱིས་ཀ་ར་གོ་རེ་འདི་རིགས་བཀོལ་ནས་ང་ཚོའི་དྲ་ཚིགས་དང་དྲ་ཐོག་ཞབས་ཞུ་དང་འབྲེལ་འདྲིས་ཇི་ལྟར་བྱ་རྒྱུར་གོ
        གལ་ཏེ་ཁྱོད་ཀྱིས་ཀ་ར་གོ་རེ་མེད་པར་བཟོས་ནས་Firefoxབཤར་ཆས།བཀོལ་སྤྱོད་བྱེད་འདོད་ན། ཁྱོད་ཀྱིས་དེ་མེ་གསོད་མེ་འགོག་གི་གསང་དོན་དང་བདེ་འཇགས་ལྟེ་གནས་སུ་བཞག་ཆོག གལ་ཏེ་ཁྱོད་ཀྱིས་བལྟ་ཆས་གཞན་པ་བཀོལ་སྤྱོད་བྱས་ན། འབྲེལ་ཡོད་མཁོ་འདོན་ཚོང་པའི་བྱེ་བྲག་གི་ཇུས་གཞིར་འདྲི་རྩད་བྱེད་རོགས།
        《ཀྲུང་ཧྭ་མི་དམངས་སྤྱི་མཐུན་རྒྱལ་ཁབ་ཀྱི་དྲ་རྒྱའི་བདེ་འཇགས་བཅའ་ཁྲིམས་》ཀྱི་དོན་ཚན་དོན་དྲུག་པའི་ནང་གསེས་དོན་ཚན་ལྔ་པའི་གཏན་འབེབས་གཞིར་བཟུང་མི་སྒེར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="298"/>
        <source>Kylinsoft Co., Ltd.</source>
        <translation>ཅིན་ལིན་སའོ་ཧྥུ་མ་རྐང་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
</context>
<context>
    <name>Proxy</name>
    <message>
        <source>Auto url</source>
        <translation type="vanished">རླངས་འཁོར་གྱི་དྲ་ཚིགས།</translation>
    </message>
    <message>
        <source>Http Proxy</source>
        <translation type="vanished">Http Proxy</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">གྲུ་ཁ།</translation>
    </message>
    <message>
        <source>Start using</source>
        <translation type="vanished">བཀོལ་སྤྱོད་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <source>Proxy mode</source>
        <translation type="vanished">ཚབ་བྱེད་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="vanished">ལག་དེབ།</translation>
    </message>
    <message>
        <source>System Proxy</source>
        <translation type="vanished">མ་ལག་གི་ཚབ་བྱེད་</translation>
    </message>
    <message>
        <source>Https Proxy</source>
        <translation type="vanished">Https Proxy</translation>
    </message>
    <message>
        <source>Ftp Proxy</source>
        <translation type="vanished">Ftp Proxy</translation>
    </message>
    <message>
        <source>Socks Proxy</source>
        <translation type="vanished">རྐང་འབོབ་ཀྱི་ཚབ་བྱེད་མི་སྣ།</translation>
    </message>
    <message>
        <source>List of ignored hosts. more than one entry, please separate with english semicolon(;)</source>
        <translation type="vanished">སྣང་མེད་དུ་བཞག་པའི་བདག་པོའི་མིང་ཐོ། འཇུག་སྒོ་གཅིག་ལས་བརྒལ་ན་དབྱིན་ཡིག་གི་ཕྱེད་ཀ་དང་ཁ་གྱེས་རོགས། (;)</translation>
    </message>
    <message>
        <source>Apt Proxy</source>
        <translation type="vanished">Apt ལས་ཚབ་</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Server Address : </source>
        <translation type="vanished">ཞབས་ཞུའི་ཡོ་བྱད་ཀྱི་གནས་ཡུལ </translation>
    </message>
    <message>
        <source>Port : </source>
        <translation type="vanished">གྲུ་ཁ། </translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <source>The apt proxy  has been turned off and needs to be restarted to take effect</source>
        <translation type="vanished">ངོ་ཚབ་ཀྱི་སྒོ་བརྒྱབ་ཟིན་པས་ཡང་བསྐྱར་ནུས་པ་ཐོན་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Reboot Later</source>
        <translation type="vanished">རྗེས་སུ་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་</translation>
    </message>
    <message>
        <source>Reboot Now</source>
        <translation type="vanished">ད་ལྟ་བསྐྱར་དུ་ལས་ཀ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>The system needs to be restarted to set the Apt proxy, whether to reboot</source>
        <translation type="vanished">མ་ལག་འདི་བསྐྱར་དུ་འགོ་ཚུགས་ནས་Aptཡི་ཚབ་བྱེད་འཕྲུལ་ཆས་གཏན་འཁེལ་བྱེད་དགོས་པ་དང་། བསྐྱར་དུ་འགོ་འཛུགས་དགོས་མིན་</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation type="vanished">ཚབ་བྱེད་མི་སྣ།</translation>
    </message>
</context>
<context>
    <name>PwdDialog</name>
    <message>
        <source>Set</source>
        <translation type="vanished">གསར་འཛུགས་</translation>
    </message>
    <message>
        <source>Set Password</source>
        <translation type="vanished">གསང་གྲངས་གཏན་འཁེལ་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="35"/>
        <source>VNC password</source>
        <translation>VNC གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="52"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="67"/>
        <source>Must be 1-8 characters long</source>
        <translation>ངེས་པར་དུ་ཡི་གེ་1-8ཀྱི་རིང་ཚད་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="84"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="88"/>
        <source>Confirm</source>
        <translation>ངོས་འཛིན་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Unknown</source>
        <translation type="vanished">ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <source>Year</source>
        <translation type="vanished">ལོ་གཅིག</translation>
    </message>
    <message>
        <source>Jan</source>
        <translation type="vanished">ཀྲིན་ནའེ་ཡིས་</translation>
    </message>
    <message>
        <source>Feb</source>
        <translation type="vanished">ཟླ་2པར།</translation>
    </message>
    <message>
        <source>Mar</source>
        <translation type="vanished">མར་ཁེ་སི།</translation>
    </message>
    <message>
        <source>Apr</source>
        <translation type="vanished">ཟླ་བ་4ཚེས་4</translation>
    </message>
    <message>
        <source>Jun</source>
        <translation type="vanished">ཅུན་ཅུན།</translation>
    </message>
    <message>
        <source>Jul</source>
        <translation type="vanished">ཀྲུའུ་ཨར།</translation>
    </message>
    <message>
        <source>Aug</source>
        <translation type="vanished">ཟླ་8ཚེས་8</translation>
    </message>
    <message>
        <source>Sep</source>
        <translation type="vanished">སི་ཕུའུ་སི།</translation>
    </message>
    <message>
        <source>Oct</source>
        <translation type="vanished">ཟླ་10ཚེས་</translation>
    </message>
    <message>
        <source>Nov</source>
        <translation type="vanished">ཟླ་བ་བཅུ་གཅིག་</translation>
    </message>
    <message>
        <source>Dec</source>
        <translation type="vanished">ཟླ་བ་བཅུ་པའི་</translation>
    </message>
    <message>
        <source>Day</source>
        <translation type="vanished">ཉིན་མོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="71"/>
        <source>User Info</source>
        <translation>སྤྱོད་མཁན་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="vanished">གཏན་ནས་བྱེད་མི་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>May</source>
        <translation>ཟླ་བ་ལྔ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>January</source>
        <translation>ཟླ་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>February</source>
        <translation>ཟླ་2པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>March</source>
        <translation>ཟླ་བ་གསུམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>April</source>
        <translation>ཟླ་4པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>June</source>
        <translation>ཟླ་6པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>July</source>
        <translation>ཟླ་7པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>August</source>
        <translation>ཟླ་བརྒྱད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>September</source>
        <translation>ཟླ་9པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>October</source>
        <translation>ཟླ་བ་བཅུ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>Novermber</source>
        <translation>ནོ་པེར་གྱིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>December</source>
        <translation>ཟླ་བ་བཅུ་གཉིས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1335"/>
        <source>min length %1
</source>
        <translation>རིང་ཐུང་གི་ཚད་ནི་1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1345"/>
        <source>min digit num %1
</source>
        <translation>གྲངས་ཀ་ཆེས་ཆུང་བའི་གྲངས་ཀ།བརྒྱ་ཆ་1ཡོད།
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1354"/>
        <source>min upper num %1
</source>
        <translation>min སྟེང་གི་num%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1363"/>
        <source>min lower num %1
</source>
        <translation>min དམའ་རིམ་num %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1372"/>
        <source>min other num %1
</source>
        <translation>གཏེར་ཁ་གཞན་དག%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1382"/>
        <source>min char class %1
</source>
        <translation>གཏེར་ཁ་སྔོག་འདོན་འཛིན་གྲྭ་%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1391"/>
        <source>max repeat %1
</source>
        <translation>ཆེས་ཆེ་བའི་བསྐྱར་ཟློས་ཀྱི་ཚད་གཞི་ནི་1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1400"/>
        <source>max class repeat %1
</source>
        <translation>ཆེས་ཆེ་བའི་འཛིན་གྲྭ་བསྐྱར་ཟློས་བྱས་ན་%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1409"/>
        <source>max sequence %1
</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་གོ་རིམ་བརྒྱ་ཆ་1
</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="85"/>
        <source>ukui-control-center is already running!</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་ཚོད་འཛིན་ལྟེ་གནས་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="96"/>
        <source>ukui-control-center is disabled！</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་ཚོད་འཛིན་ལྟེ་གནས་ནི་དབང་པོ་སྐྱོན་ཅན་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="130"/>
        <source>ukui-control-center</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་ཚོད་འཛིན་ལྟེ་གནས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="8"/>
        <source>简体中文</source>
        <translation>བསྡུས་གཟུགས་རྒྱ་ཡིག་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="9"/>
        <source>English</source>
        <translation>དབྱིན་སྐད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="10"/>
        <source>བོད་ཡིག</source>
        <translation>བོད་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="11"/>
        <source>ᠮᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ</source>
        <translation>སོག་པོ་འི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="12"/>
        <source>繁體</source>
        <translation>བརྡ་རྙིང་</translation>
    </message>
    <message>
        <source>Монгол</source>
        <translation type="vanished">སོག་པོ་འི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="187"/>
        <source>Customize Shortcut</source>
        <translation>མགྱོགས་ལམ་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="694"/>
        <source>Edit Shortcut</source>
        <translation>མགྱོགས་ལམ་རྩོམ་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="765"/>
        <source>Programs are not allowed to be added.</source>
        <translation>གོ་རིམ་ཁ་སྣོན་བྱས་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="682"/>
        <source>xxx客户端</source>
        <translation>xxx ཤེས་ལྡན་མི་རིགས།</translation>
    </message>
</context>
<context>
    <name>ResolutionSlider</name>
    <message>
        <source>No available resolutions</source>
        <translation type="vanished">ད་ཡོད་ཀྱི་གྲོས་ཆོད་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>Screenlock</name>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="26"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="80"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="51"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="136"/>
        <source>Screenlock</source>
        <translation>བརྙན་ཤེལ་གྱི་སྒོ་བརྒྱབ་པ</translation>
        <extra-contents_path>/Screenlock/Screenlock</extra-contents_path>
    </message>
    <message>
        <source>Screenlock Interface</source>
        <translation type="vanished">བརྙན་ཤེལ་གྱི་འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="205"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="256"/>
        <source>Show message on lock screen</source>
        <translation>ཟྭ་ངོས་སུ་ཆ་འཕྲིན་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="262"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="148"/>
        <source>Show picture of screenlock on screenlogin</source>
        <translation>བརྙན་ཤེལ་སྟེང་གི་བརྙན་ཤེལ་གྱི་པར་རིས་འགྲེམས་སྟོན་བྱས།</translation>
        <extra-contents_path>/Screenlock/Show picture of screenlock on screenlogin</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="401"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="150"/>
        <source>Local Pictures</source>
        <translation>ས་གནས་ཀྱི་རི་མོ།</translation>
        <extra-contents_path>/Screenlock/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="408"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="152"/>
        <source>Online Pictures</source>
        <translation>དྲ་ཐོག་པར་རིས།</translation>
        <extra-contents_path>/Screenlock/Online Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="483"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="139"/>
        <source>Related Settings</source>
        <translation>འབྲེལ་ལྡན་སྒྲིག་འགོད་</translation>
        <extra-contents_path>/Screenlock/Related Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="539"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="142"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="698"/>
        <source>Monitor Off</source>
        <translation>སྒོ་རྒྱག་འཆར་ཆས་</translation>
        <extra-contents_path>/Screenlock/Monitor Off</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="558"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="613"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="700"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="701"/>
        <source>Set</source>
        <translation>ཉི་ནུབ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="594"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="145"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="699"/>
        <source>Screensaver</source>
        <translation>བརྙན་ཡོལ་སྲུང་སྐྱོབ་</translation>
        <extra-contents_path>/Screenlock/Screensaver</extra-contents_path>
    </message>
    <message>
        <source>Lock screen when screensaver boot</source>
        <translation type="vanished">བརྙན་ཤེལ་གྱི་ལྷམ་ཡུ་རིང་གི་དུས་སུ་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག</translation>
        <extra-contents_path>/Screenlock/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="332"/>
        <source>Lock screen delay</source>
        <translation>བརྙན་ཤེལ་གྱི་དུས་ཚོད་འགོར་འགྱངས</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="440"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="162"/>
        <source>Reset To Default</source>
        <translation>བསྐྱར་དུ་ཁ་ཆད་དང་འགལ་བའི་གནས་</translation>
        <extra-contents_path>/Screenlock/Reset To Default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>1min</source>
        <translation>1སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>5min</source>
        <translation>5སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>10min</source>
        <translation>10སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>30min</source>
        <translation>30སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>45min</source>
        <translation>45སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>1hour</source>
        <translation>1ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>2hour</source>
        <translation>2ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>3hour</source>
        <translation>3ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>Never</source>
        <translation>གཏན་ནས་བྱེད་མི་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="563"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>Wallpaper files (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *jpe *.gif *.tif *.tiff *wdp)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="605"/>
        <source>select custom wallpaper file</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་གྱང་ཤོག་ཡིག་ཆ་བདམས་པ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="606"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="607"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="608"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="609"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="610"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="59"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="99"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="209"/>
        <source>Screensaver</source>
        <translation>བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་འཕྲུལ་ཆས།</translation>
        <extra-contents_path>/Screensaver/Screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="201"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="213"/>
        <source>Idle time</source>
        <translation>སྒྱིད་ལུག་གི་དུས་ཚོད།</translation>
        <extra-contents_path>/Screensaver/Idle time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="475"/>
        <source>Lock screen when activating screensaver</source>
        <translation>བརྙན་ཤེལ་ལ་སྐུལ་སློང་བྱེད་སྐབས་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="211"/>
        <source>Screensaver program</source>
        <translation>བརྙན་ཤེལ་གྱི་འཆར་གཞི།</translation>
        <extra-contents_path>/Screensaver/Screensaver program</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="197"/>
        <source>View</source>
        <translation>ལྟ་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="272"/>
        <source>UKUI</source>
        <translation>UKUI</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="273"/>
        <source>Blank_Only</source>
        <translation>Blank_Only</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="284"/>
        <source>Customize</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="815"/>
        <source>5min</source>
        <translation>5སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="816"/>
        <source>10min</source>
        <translation>10སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>15min</source>
        <translation>15སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="817"/>
        <source>30min</source>
        <translation>30སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>1hour</source>
        <translation>1ཆུ་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="298"/>
        <source>Never</source>
        <translation>གཏན་ནས་བྱེད་མི་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="716"/>
        <source>Screensaver source</source>
        <translation>བརྙན་ཤེལ་གྱི་འབྱུང་ཁུངས།</translation>
        <extra-contents_path>/Screensaver/Screensaver source</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="722"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="772"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="730"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp *.svg)</source>
        <translation>Wallpaper files (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *jpe *.gif *.tif *.tiff *.svg)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="771"/>
        <source>select custom screensaver dir</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་བརྙན་ཤེལ་གྲོན་ཆུང་བྱེད་མཁན་བདམས་པ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="773"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="774"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="775"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="776"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="810"/>
        <source>Switching time</source>
        <translation>བརྗེ་རེས་བྱེད་པའི་དུས་ཚོད།</translation>
        <extra-contents_path>/Screensaver/Switching time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="293"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="814"/>
        <source>1min</source>
        <translation>1སྐར་མ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="861"/>
        <source>Ordinal</source>
        <translation>སྲོལ་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="862"/>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="871"/>
        <source>Random switching</source>
        <translation>སྐབས་བསྟུན་གྱིས་བརྗེ་རེས་བྱེད་པ</translation>
        <extra-contents_path>/Screensaver/Random switching</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="915"/>
        <source>Text(up to 30 characters):</source>
        <translation>ཡི་གེ(ཆེས་མང་ན་ཡི་གེ་30ཡོད་པ་གཤམ་གསལ། )</translation>
        <extra-contents_path>/Screensaver/Text(up to 30 characters):</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="950"/>
        <source>Show rest time</source>
        <translation>ངལ་གསོའི་དུས་ཚོད་མངོན་པ།</translation>
        <extra-contents_path>/Screensaver/Show rest time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="960"/>
        <source>Lock screen when screensaver boot</source>
        <translation>བརྙན་ཤེལ་གྱི་ལྷམ་ཡུ་རིང་གི་དུས་སུ་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག</translation>
        <extra-contents_path>/Screensaver/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="992"/>
        <source>Text position</source>
        <translation>ཡི་གེའི་གོ་གནས།</translation>
        <extra-contents_path>/Screensaver/Text position</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1000"/>
        <source>Centered</source>
        <translation>ལྟེ་བར་འཛིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1001"/>
        <source>Randow(Bubble text)</source>
        <translation>ལན་ཏུའོ་(ལྦུ་བ་ཅན་གྱི་ཡི་གེ)</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../searchwidget.cpp" line="61"/>
        <location filename="../../searchwidget.cpp" line="62"/>
        <location filename="../../searchwidget.cpp" line="69"/>
        <location filename="../../searchwidget.cpp" line="71"/>
        <location filename="../../searchwidget.cpp" line="76"/>
        <source>No search results</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>ShareMain</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>please select an output</source>
        <translation type="vanished">ཁྱེད་ཀྱིས་ཐོན་རྫས་ཤིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <source>Input Password</source>
        <translation type="vanished">ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Password length must be less than or equal to 8</source>
        <translation type="vanished">གསང་བའི་རིང་ཚད་ངེས་པར་དུ་8ལས་ཉུང་བའམ་ཡང་ན་8དང་མཚུངས་དགོས།</translation>
    </message>
    <message>
        <source>Password can not be blank</source>
        <translation type="vanished">གསང་གྲངས་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>Share</source>
        <translation type="vanished">མ་རྐང་འཛིན་ཤོག</translation>
    </message>
    <message>
        <source>Output</source>
        <translation type="vanished">ཐོན་ཚད།</translation>
    </message>
    <message>
        <source>Input</source>
        <translation type="vanished">མ་དངུལ་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Point</source>
        <translation type="vanished">གནད་འགག</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <source>Clipboard</source>
        <translation type="vanished">འདྲུད་པང་།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>ViewOnly</source>
        <translation type="vanished">ལྟ་ཚུལ་འཛིན་སྟངས།</translation>
    </message>
    <message>
        <source>Client Setting</source>
        <translation type="vanished">ཚོང་འགྲུལ་པ་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Client Number</source>
        <translation type="vanished">ཚོང་འགྲུལ་པའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <source>Client IP：</source>
        <translation type="vanished">མངགས་བཅོལ་བྱེད་མཁན་གྱི་ཤེས་བྱ</translation>
    </message>
    <message>
        <source>退出程序</source>
        <translation type="vanished">退出程序</translation>
    </message>
    <message>
        <source>确认退出程序！</source>
        <translation type="vanished">确认退出程序！</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="68"/>
        <source>Remote Desktop</source>
        <translation>རྒྱང་རིང་གི་ཅོག་ཙེ།</translation>
        <extra-contents_path>/Vino/Remote Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="80"/>
        <source>Connect to your desktop remotely</source>
        <translation>རྒྱང་རིང་འབྲེལ་མཐུད་གློག་ཀླད་འདི་</translation>
        <extra-contents_path>/Vino/Connect to your desktop remotely</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="98"/>
        <source>Allow others to connect to your desktop remotely using RDP</source>
        <translation>སྤྱོད་མཁན་གཞན་ལ་སྤྱོད་ཆོག་གི་RDPརྒྱང་རིང་འབྲེལ་མཐུད་གློག་ཀླད་འདི་།</translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using RDP</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="112"/>
        <source>Allow others to connect to your desktop remotely using VNC</source>
        <translation>སྤྱོད་མཁན་གཞན་ལ་སྤྱོད་ཆོག་གི་VNCརྒྱང་རིང་འབྲེལ་མཐུད་གློག་ཀླད་འདི་།</translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using VNC</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="160"/>
        <source>Require user to enter this password while using VNC: </source>
        <translation>VNCརྒྱང་རིང་འབྲེལ་མཐུད་བྱེད་སྐབས་གསང་གྲངས་འཇོག་དགོས།:</translation>
        <extra-contents_path>/Vino/Require user to enter this password while using VNC:</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="164"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <source>Allow others to view your desktop</source>
        <translation type="vanished">མི་གཞན་གྱིས་ཁྱོད་ཀྱི་ཅོག་ཙེའི་སྟེང་གི་ཅོག་ཙེ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="126"/>
        <source>Allow connection to control screen</source>
        <translation>འབྲེལ་མཐུད་བྱས་ནས་བརྙན་ཤེལ་ཚོད་འཛིན་བྱེད་དུ</translation>
        <extra-contents_path>/Vino/Allow connection to control screen</extra-contents_path>
    </message>
    <message>
        <source>Security</source>
        <translation type="vanished">བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="142"/>
        <source>You must confirm every visit for this machine</source>
        <translation>ཁྱེད་ཚོས་ངེས་པར་དུ་འཕྲུལ་ཆས་འདིའི་འཚམས་འདྲི་ཚང་མ་གཏན་འཁེལ</translation>
        <extra-contents_path>/Vino/You must confirm every visit for this machine</extra-contents_path>
    </message>
    <message>
        <source>Require user to enter this password: </source>
        <translation type="vanished">སྤྱོད་མཁན་གྱིས་གསང་གྲངས་འདིའི་ནང་དུ་འཇུག་དགོས་པའི་བླང་བྱ་ </translation>
    </message>
</context>
<context>
    <name>ShareMainHw</name>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>please select an output</source>
        <translation>ཁྱེད་ཀྱིས་ཐོན་རྫས་ཤིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="331"/>
        <source>Input Password</source>
        <translation>ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="332"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="445"/>
        <source>Share</source>
        <translation>མ་རྐང་འཛིན་ཤོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="461"/>
        <source>Allow others to view your desktop</source>
        <translation>མི་གཞན་གྱིས་ཁྱོད་ཀྱི་ཅོག་ཙེའི་སྟེང་གི་ཅོག་ཙེ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="471"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="481"/>
        <source>Require user to enter this password: </source>
        <translation>སྤྱོད་མཁན་གྱིས་གསང་གྲངས་འདིའི་ནང་དུ་འཇུག་དགོས་པའི་བླང་བྱ་ </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="483"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="496"/>
        <source>Output</source>
        <translation>ཐོན་ཚད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="508"/>
        <source>Input</source>
        <translation>མ་དངུལ་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="518"/>
        <source>Point</source>
        <translation>གནད་འགག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="520"/>
        <source>Keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="522"/>
        <source>Clipboard</source>
        <translation>འདྲུད་པང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="548"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="550"/>
        <source>ViewOnly</source>
        <translation>ལྟ་ཚུལ་འཛིན་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="558"/>
        <source>Client Setting</source>
        <translation>ཚོང་འགྲུལ་པ་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="569"/>
        <source>Client Number</source>
        <translation>ཚོང་འགྲུལ་པའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="583"/>
        <source>Client IP：</source>
        <translation>མངགས་བཅོལ་བྱེད་མཁན་གྱི་ཤེས་བྱ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>退出程序</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པའི་གོ་རིམ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>确认退出程序！</source>
        <translation>ངེས་གཏན་བྱས་ནས། </translation>
    </message>
</context>
<context>
    <name>Shortcut</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="50"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="164"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="178"/>
        <source>System Shortcut</source>
        <translation>མ་ལག་གི་མྱུར་ལམ།</translation>
        <extra-contents_path>/Shortcut/System Shortcut</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="103"/>
        <source>Custom Shortcut</source>
        <translation>འགག་སྒོའི་མྱུར་ལམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="80"/>
        <source>Shortcut</source>
        <translation>མྱུར་བགྲོད་གཞུང་ལམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="161"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Shortcut/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="166"/>
        <source>Customize Shortcut</source>
        <translation>མགྱོགས་ལམ་གཏན་འཁེལ་བྱ་དགོས།</translation>
        <extra-contents_path>/Shortcut/Customize Shortcut</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="441"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="573"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="836"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="442"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="574"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="837"/>
        <source>Use</source>
        <translation>བཀོལ་སྤྱོད། </translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="443"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="575"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="838"/>
        <source>Shortcut key conflict, use it?</source>
        <translation>བཟུང་སྤྱོད་མིན་བཀོལ་སྤྱོད། </translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="444"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="576"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="839"/>
        <source>%1 occuied, using this combination will invalidate %2</source>
        <translation>%1བཟུང་སྤྱོད་འགྲོ་བ་གོ་མི་ཆོད་པ། %2</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="467"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="606"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="868"/>
        <source>Shortcut &quot;%1&quot; occuied, please change the key combination</source>
        <translation>&quot;%1&quot;བཟུང་སྤྱོད་ཞུ་བཟོ་བཅོས། </translation>
    </message>
    <message>
        <source>Empty</source>
        <translation type="vanished">སྟོང། </translation>
    </message>
    <message>
        <source>Shortcut %1 occuied, please change the key combination</source>
        <translation type="vanished">%1བཟུང་སྤྱོད་ཞུ་བཟོ་བཅོས། </translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="677"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="678"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1039"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1043"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1061"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1065"/>
        <source>Null</source>
        <translation>སྟོང། </translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1126"/>
        <source> or </source>
        <translation> ཡང་ན་དེ་ལྟར་ </translation>
    </message>
</context>
<context>
    <name>StatusDialog</name>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="10"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="59"/>
        <source>Activation Code</source>
        <translation>སྐུལ་སློང་གི་ཚབ་རྟགས།</translation>
    </message>
</context>
<context>
    <name>Theme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="212"/>
        <source>Window Theme</source>
        <translation>སྒེའུ་ཁུང་གི་བརྗོད་བྱ་གཙོ</translation>
        <extra-contents_path>/Theme/Window Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="214"/>
        <source>Icon theme</source>
        <translation>མཚོན་རྟགས་ཀྱི་བརྗོད་བྱ་གཙོ་</translation>
        <extra-contents_path>/Theme/Icon theme</extra-contents_path>
    </message>
    <message>
        <source>Control theme</source>
        <translation type="vanished">ཚོད་འཛིན་གྱི་བརྗོད་བྱ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="217"/>
        <source>Cursor theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
        <extra-contents_path>/Theme/Cursor theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="134"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="221"/>
        <source>Effect setting</source>
        <translation>ཕན་འབྲས་ཀྱི་སྒྲིག་གཞི།</translation>
        <extra-contents_path>/Theme/Effect setting</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="249"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="223"/>
        <source>Performance mode</source>
        <translation>གྲུབ་འབྲས་ཐོབ་སྟངས།</translation>
        <extra-contents_path>/Theme/Performance mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="352"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="225"/>
        <source>Transparency</source>
        <translation>ཕྱི་གསལ་ནང་གསལ།</translation>
        <extra-contents_path>/Theme/Transparency</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="473"/>
        <source>Reset to default</source>
        <translation>བསྐྱར་དུ་ཁ་ཆད་དང་འགལ་བའི་གནས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="107"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="521"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
        <extra-contents_path>/Theme/Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <source>Default</source>
        <translation>ཁ་ཆད་དང་འགལ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="183"/>
        <source>Light</source>
        <translation>འོད་སྣང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="181"/>
        <source>Dark</source>
        <translation>མུན་ནག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="185"/>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="422"/>
        <source>Corlor</source>
        <translation>ཁུ་རུའི་རིགས།</translation>
        <extra-contents_path>/Theme/Corlor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="604"/>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
        <extra-contents_path>/Theme/Other</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="610"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="624"/>
        <source>Set</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="616"/>
        <source>Wallpaper</source>
        <translation>གྱང་ཤོག</translation>
        <extra-contents_path>/Theme/Wallpaper</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="630"/>
        <source>Beep</source>
        <translation>སྦྲང་མ།</translation>
        <extra-contents_path>/Theme/Beep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="961"/>
        <source>Blue-Crystal</source>
        <translation>ཁ་དོག་སྔོན་པོའི་ཆུ་ཤེལ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="963"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <source>Light-Seeking</source>
        <translation>འོད་ཟེར་འཚོལ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="965"/>
        <source>DMZ-Black</source>
        <translation>DMZ-ནག་པོ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="967"/>
        <source>DMZ-White</source>
        <translation>DMZ-མི་དཀར</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="969"/>
        <source>Dark-Sense</source>
        <translation>མུན་ནག་གི་ཚོར་སྣང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1084"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>basic</source>
        <translation>གཞི་རྩའི་ཆ་ནས</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>Classic</source>
        <translation>གནའ་གཞུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>classic</source>
        <translation>གནའ་གཞུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1090"/>
        <source>HeYin</source>
        <translation>ཧའེ་དབྱིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1092"/>
        <source>hp</source>
        <translation>hp</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>ukui</source>
        <translation>ཝུའུ་ཁི་ལན་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1096"/>
        <source>daybreakBlue</source>
        <translation>ཉི་མ་ཤར་བའི་པུའུ་ལའེ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1098"/>
        <source>jamPurple</source>
        <translation>ཤིང་ཏོག་བསྣུར་མ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1100"/>
        <source>magenta</source>
        <translation>མེ་དམར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1102"/>
        <source>sunRed</source>
        <translation>ཉི་མ་ཤར་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1104"/>
        <source>sunsetOrange</source>
        <translation>ཉི་མ་ནུབ་པའི་ཨོ་ལན་ཀེ་ལན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1106"/>
        <source>dustGold</source>
        <translation>གད་སྙིགས་བླུགས་སྣ་ཚོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1108"/>
        <source>polarGreen</source>
        <translation>གླིང་སྣེའི་ཀེ་ལིན།</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">ཁ་ཆད་དང་འགལ་</translation>
    </message>
</context>
<context>
    <name>TimeBtn</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="80"/>
        <source>Tomorrow</source>
        <translation>སང་ཉིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="82"/>
        <source>Yesterday</source>
        <translation>ཁ་སང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="84"/>
        <source>Today</source>
        <translation>དེ་རིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="102"/>
        <source>%1 hours earlier than local</source>
        <translation>ས་གནས་དེ་གའི་ཆུ་ཚོད་1ལས་སྔ་བ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="104"/>
        <source>%1 hours later than local</source>
        <translation>ས་གནས་དེ་གའི་ཆུ་ཚོད་1ལས་ཆུ་ཚོད་1འ</translation>
    </message>
</context>
<context>
    <name>TimeZoneChooser</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="38"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="39"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="41"/>
        <source>Change Timezone</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="36"/>
        <source>Search Timezone</source>
        <translation>དུས་ཚོད་འཚོལ་ཞིབ་བྱེད་པའི་དུས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="65"/>
        <source>To select a time zone, please click where near you on the map and select a city from the nearest city</source>
        <translation>དུས་ཚོད་ཀྱི་ས་ཁོངས་འདེམས་དགོས་ན། ས་བཀྲའི་སྟེང་གི་ཉེ་འདབས་ཀྱི་ས་ཆ་གང་དུ་སོང་ནས་ཆེས་ཉེ་བའི་གྲོང་ཁྱེར་ནས་གྲོང་ཁྱེར་ཞིག་འདེམས་རོགས།</translation>
    </message>
</context>
<context>
    <name>Touchpad</name>
    <message>
        <source>Touchpad</source>
        <translation type="vanished">ལག་ཐོགས་ཁ་པར།</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <source>Touchpad Setting</source>
        <translation type="vanished">ལག་ཐོགས་ཁ་པར་གྱི་སྒྲིག་བཀོད</translation>
    </message>
    <message>
        <source>Disable touchpad when using the mouse</source>
        <translation type="vanished">བྱི་བ་བཀོལ་སྤྱོད་བྱེད་སྐབས་ལག་ཐོགས་ཁ་པར་གྱི་ཁ་པར་ལ་</translation>
        <extra-contents_path>/Touchpad/Disable touchpad when using the mouse</extra-contents_path>
    </message>
    <message>
        <source>Pointer Speed</source>
        <translation type="vanished">ཕྱོགས་སྟོན་འཁོར་ལོ་མགྱོགས་ཚད།</translation>
        <extra-contents_path>/Touchpad/Pointer Speed</extra-contents_path>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">དལ་མོ།དལ་མོ།</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <source>Disable touchpad when typing</source>
        <translation type="vanished">ཡི་གེ་ཡི་གེ་རྒྱག་སྐབས་ལག་ཐོགས་ཁ་པར་གྱི་ལག་ཐོགས་ཁ་པར</translation>
        <extra-contents_path>/Touchpad/Disable touchpad when typing</extra-contents_path>
    </message>
    <message>
        <source>Touch and click on the touchpad</source>
        <translation type="vanished">ལག་ཐོགས་ཁ་པར་ལ་རེག་པ་དང་འབྲེལ་མཐུད་བྱེད་པ།</translation>
        <extra-contents_path>/Touchpad/Touch and click on the touchpad</extra-contents_path>
    </message>
    <message>
        <source>Scroll bar slides with finger</source>
        <translation type="vanished">མཛུབ་མོས་འདྲེད་བརྡར་ཤོར་བ།</translation>
        <extra-contents_path>/Touchpad/Scroll bar slides with finger</extra-contents_path>
    </message>
    <message>
        <source>Scrolling area</source>
        <translation type="vanished">ཤོག་ལྷེ་རྒྱག་པའི་རྒྱ་ཁྱོན།</translation>
        <extra-contents_path>/Touchpad/Scrolling area</extra-contents_path>
    </message>
    <message>
        <source>Two-finger scrolling in the middle area</source>
        <translation type="vanished">དཀྱིལ་ཁུལ་དུ་ལག་པ་གཉིས་ཀྱིས་ཕར་འགྲོ་ཚུར་འོང་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Edge scrolling</source>
        <translation type="vanished">མཐའ་འཁོར་དུ་ཕར་འགྲོ་ཚུར་འོང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Disable scrolling</source>
        <translation type="vanished">དབང་པོ་སྐྱོན་ཅན་དུ་འགྱུར་བར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>TrialDialog</name>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="12"/>
        <source>Set</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="37"/>
        <source>Yinhe Kylin OS(Trail Version) Disclaimer</source>
        <translation>དབྱིན་ཧོ་ཅིན་ལིན་གྱི་བཀོལ་སྤྱོད་རྒྱུད་ཁོངས་(ཚོད་སྤྱོད་པར་གཞི་)བསྐྱར་གཅོད་ཡོང་རེ་ཞུ་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="46"/>
        <source>Dear customer:
       Thank you for trying Yinhe Kylin OS(trail version)! This version is free for users who only try out, no commercial purpose is permitted. The trail period lasts one year and it starts from the ex-warehouse time of the OS. No after-sales service is provided during the trail stage. If any security problems occurred when user put important files or do any commercial usage in system, all consequences are taken by users. Kylin software Co., Ltd. take no legal risk in trail version.
       During trail stage,if you want any technology surpport or activate the system, please buy“Yinhe Kylin Operating System”official version or authorization by contacting 400-089-1870.</source>
        <translation>མཛའ་བརྩེ་ལྡན་པའི་མཁོ་མཁན་
       ཁྱེད་ཀྱིས་དབྱིན་ཧོ་ཅིན་ལིན་གྱི་བཀོལ་སྤྱོད་རྒྱུད་ཁོངས་(ཚོད་སྤྱོད་པར་གཞི་)ཚོད་ལྟ་བྱས་པར་ཐུགས་རྗེ་ཆེ་ཞུ་རྒྱུ་ཡིན། པར་གཞི་འདི་ནི་ཚོད་ལྟ་ཁོ་ན་བྱེད་པའི་སྤྱོད་མཁན་ལ་རིན་མེད་ཡིན་པས་ཚོང་ལས་ཀྱི་དམིགས་ཡུལ་གང་ཡང་མེད། ལམ་ཕྲན་གྱི་དུས་ཡུན་ལོ་གཅིག་ཡིན་པ་དང་དེ་ནི་ཡོ་རོབ་དང་ཡོ་རོབ་ཀྱི་མཛོད་ཁང་ལས་བརྒལ་བའི་དུས་སྐབས་ནས་འགོ་བརྩམས་པ་ཡིན། རྗེས་ཤུལ་གྱི་དུས་རིམ་དུ་ཕྱིར་འཚོང་བྱས་རྗེས་ཀྱི་ཞབས་ཞུ་འདོན་སྤྲོད་བྱས་མེད། གལ་ཏེ་སྤྱོད་མཁན་གྱིས་གལ་ཆེའི་ཡིག་ཆ་འཇོག་པའམ་ཡང་ན་མ་ལག་ནང་དུ་ཚོང་ལས་ཀྱི་བཀོལ་སྤྱོད་བྱེད་སྐབས་བདེ་འཇགས་ཀྱི་གནད་དོན་བྱུང་ན། མཇུག་འབྲས་ཚང་མ་སྤྱོད་མཁན་གྱིས་ཐག་གཅོད་བྱ་དགོས། ཅིན་ལིན་མཉེན་ཆས་མ་རྐང་ཚད་ཡོད་ཀུང་སིས་རྗེས་ཤུལ་གྱི་པར་གཞིའི་ཁྲོད་དུ་བཅའ་ཁྲིམས་ཀྱི་ཉེན་ཁ་མེད།
       རྗེས་ཤུལ་གྱི་དུས་རིམ་དུ་གལ་ཏེ་ཁྱོད་ལ་ལག་རྩལ་གང་རུང་ཞིག་དགོས་པའམ་ཡང་ན་མ་ལག་འདི་སྐུལ་སློང་བྱེད་དགོས་ན། 400-089-1870ལ་འབྲེལ་གཏུག་བྱས་ནས་&quot;དགུ་ཚིགས་ཅན་གྱི་བཀོལ་སྤྱོད་རྒྱུད་ཁོངས།&quot;ཡི་གཞུང་ཕྱོགས་ཀྱི་པར་གཞིའམ་ཡང་ན་དབང་ཆ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="60"/>
        <source>Kylin software Co., Ltd.</source>
        <translation>ཅིན་ལིན་མཉེན་ཆས་མ་རྐང་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
</context>
<context>
    <name>UkccAbout</name>
    <message>
        <location filename="../../ukccabout.cpp" line="33"/>
        <location filename="../../ukccabout.cpp" line="59"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="64"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="74"/>
        <source>Service and Support:</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་བྱ་རྒྱུ་སྟེ།</translation>
    </message>
</context>
<context>
    <name>UnifiedOutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="91"/>
        <source>resolution</source>
        <translation>གྲོས་ཆོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="128"/>
        <source>orientation</source>
        <translation>ཁ་ཕྱོགས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="133"/>
        <source>arrow-up</source>
        <translation>མདའ་འཕེན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="134"/>
        <source>90° arrow-right</source>
        <translation>90°མདའ་གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="136"/>
        <source>arrow-down</source>
        <translation>མདའ་མོ་མར་ཕབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="135"/>
        <source>90° arrow-left</source>
        <translation>90°མདའ་གཡོན་ཕྱོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="168"/>
        <source>frequency</source>
        <translation>ཐེངས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="228"/>
        <source>screen zoom</source>
        <translation>བརྙན་ཤེལ་ཆེ་རུ་གཏོང་བ།</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="393"/>
        <source>auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="41"/>
        <source>Current User</source>
        <translation>མིག་སྔའི་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="309"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="182"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
        <extra-contents_path>/Userinfo/Password</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="331"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="188"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
        <extra-contents_path>/Userinfo/Type</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="353"/>
        <source>Group</source>
        <translation>ཚོ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="500"/>
        <source>Automatic login at boot</source>
        <translation>རང་འགུལ་གྱིས་ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="422"/>
        <source>Login no passwd</source>
        <translation>ཐོ་འགོད་བྱས་མེད་པའི་ལམ་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="568"/>
        <source>Other Users</source>
        <translation>སྤྱོད་མཁན་གཞན་དག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1202"/>
        <source>root</source>
        <translation>རྩ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="160"/>
        <source>CurrentUser</source>
        <translation>གློག་སྒུལ་འཕྲུལ་འཁོར།</translation>
        <extra-contents_path>/Userinfo/CurrentUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="162"/>
        <source>OthersUser</source>
        <translation>དེ་མིན་ད་དུང</translation>
        <extra-contents_path>/Userinfo/OthersUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="194"/>
        <source>Groups</source>
        <translation>ཚོ་ཆུང་།</translation>
        <extra-contents_path>/Userinfo/Groups</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="249"/>
        <source>LoginWithoutPwd</source>
        <translation>གསང་རྟགས</translation>
        <extra-contents_path>/Userinfo/LoginWithoutPwd</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="271"/>
        <source>AutoLoginOnBoot</source>
        <translation>རང་འགུལ་གྱིས་རང་འགུལ་གྱིས་རང་འགུལ་གྱིས་རང་འགུལ་གྱིས</translation>
        <extra-contents_path>/Userinfo/AutoLoginOnBoot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="306"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Userinfo/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>The user is logged in, please delete the user after logging out</source>
        <translation>སྤྱོད་མཁན་གྱིས་ཐོ་འགོད་བྱས་ཚར་རྗེས་སྤྱོད་མཁན་བསུབ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1075"/>
        <source>The account type of “%1” has been modified, will take effect after logout, whether to logout?</source>
        <translation>&quot;%1&quot;ཡི་རྩིས་ཐོའི་རིགས་ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཐོ་འགོད་བྱས་རྗེས་ནུས་པ་འཐོན་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1076"/>
        <source>logout later</source>
        <translation>རྗེས་སུ་ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1077"/>
        <source>logout now</source>
        <translation>ད་ལྟ་ཐོ་འགོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1170"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1171"/>
        <source>The system only allows one user to log in automatically.After it is turned on, the automatic login of other users will be turned off.Is it turned on?</source>
        <translation>མ་ལག་འདིས་སྤྱོད་མཁན་གཅིག་གིས་རང་འགུལ་གྱིས་ཐོ་འགོད་བྱེད་དུ་འཇུག་པ་རེད། ཁ་ཕྱེ་རྗེས་སྤྱོད་མཁན་གཞན་དག་གི་རང་འགུལ་ཐོ་འགོད་ཀྱི་སྒོ་རྒྱག་རྒྱུ་རེད། ཁ་ཕྱེ་བ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1174"/>
        <source>Trun on</source>
        <translation>ཐེ་ལུན་གྱིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1175"/>
        <source>Close on</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1198"/>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1200"/>
        <source>Admin</source>
        <translation>སྲིད་འཛིན་དོ་དམ།</translation>
    </message>
</context>
<context>
    <name>UserInfoIntel</name>
    <message>
        <source>Current User</source>
        <translation type="vanished">མིག་སྔའི་སྤྱོད་མཁན།</translation>
        <extra-contents_path>/UserinfoIntel/Current User</extra-contents_path>
    </message>
    <message>
        <source>Change phone</source>
        <translation type="vanished">ཁ་པར་བརྗེ་བ།</translation>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="vanished">བརྗེ་བ།</translation>
        <extra-contents_path>/UserinfoIntel/Change pwd</extra-contents_path>
    </message>
    <message>
        <source>User group</source>
        <translation type="vanished">སྤྱོད་མཁན་ཚོགས་པ།</translation>
    </message>
    <message>
        <source>Del user</source>
        <translation type="vanished">ཏེ་ཨར་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>system reboot</source>
        <translation type="vanished">མ་ལག་བསྐྱར་དུ་འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Unclosed apps start after a restart</source>
        <translation type="vanished">ཁྱབ་བསྒྲགས་བྱས་མེད་པའི་ཉེར་སྤྱོད་གོ་རིམ་བསྐྱར་དུ་འགོ་ཚུགས་རྗེས།</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">སྤྱོད་མཁན་གཞན་དག</translation>
        <extra-contents_path>/UserinfoIntel/Other Users</extra-contents_path>
    </message>
    <message>
        <source>User Info Intel</source>
        <translation type="vanished">User Info Intel</translation>
    </message>
    <message>
        <source>Change Tel</source>
        <translation type="vanished">གློག་འཕྲིན་བརྗེ་བ།</translation>
        <extra-contents_path>/UserinfoIntel/Change Tel</extra-contents_path>
    </message>
    <message>
        <source>Delete user</source>
        <translation type="vanished">སྤྱོད་མཁན་བསུབ་པ།</translation>
        <extra-contents_path>/UserinfoIntel/Delete user</extra-contents_path>
    </message>
    <message>
        <source>Change user name</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱི་མིང་བསྒྱུར་དགོས།</translation>
        <extra-contents_path>/UserinfoIntel/Change user name</extra-contents_path>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">དོ་དམ་པ།</translation>
    </message>
    <message>
        <source>root</source>
        <translation type="vanished">རྩ་བ།</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="vanished">སྤྱོད་མཁན་གསར་པ་ཁ་སྣོན་</translation>
    </message>
    <message>
        <source>set pwd</source>
        <translation type="vanished">གཏན་འཁེལ་བྱས་པའི</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="vanished">བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>UtilsForUserinfo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="32"/>
        <source>Password</source>
        <translation>ཕར་འགྲོ་ཚུར་འོང་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="36"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="40"/>
        <source>Delete</source>
        <translation>ཏེ་ཨར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="218"/>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="220"/>
        <source>Admin</source>
        <translation>སྲིད་འཛིན་དོ་དམ།</translation>
    </message>
</context>
<context>
    <name>Vino</name>
    <message>
        <location filename="../../../plugins/system/vino/vino.cpp" line="28"/>
        <source>Vino</source>
        <translation>ཝེ་ནོ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
</context>
<context>
    <name>VinoHw</name>
    <message>
        <source>Vino</source>
        <translation type="vanished">ཝེ་ནོ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>VinoHw</source>
        <translation type="vanished">ཝེ་ནོ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
</context>
<context>
    <name>Vpn</name>
    <message>
        <source>VPN</source>
        <translation type="vanished">VPN</translation>
    </message>
    <message>
        <source>Add a vpn connection</source>
        <translation type="vanished">vpnས སྦྲེལ་མཐུད་ཁ་སྣོན་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="103"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="151"/>
        <source>Desktop Background</source>
        <translation>ཅོག་ཙེའི་རྒྱབ་ལྗོངས།</translation>
        <extra-contents_path>/Wallpaper/Desktop Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="401"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="156"/>
        <source>Mode</source>
        <translation>དཔེ་དབྱིབས་</translation>
        <extra-contents_path>/Wallpaper/Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="532"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="158"/>
        <source>Local Pictures</source>
        <translation>ས་གནས་ཀྱི་རི་མོ།</translation>
        <extra-contents_path>/Wallpaper/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="539"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="160"/>
        <source>Online Pictures</source>
        <translation>དྲ་ཐོག་པར་རིས།</translation>
        <extra-contents_path>/Wallpaper/Online Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="571"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="170"/>
        <source>Reset To Default</source>
        <translation>བསྐྱར་དུ་ཁ་ཆད་དང་འགལ་བའི་གནས་</translation>
        <extra-contents_path>/Wallpaper/Reset To Default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="665"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="708"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="331"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="58"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="154"/>
        <source>Background</source>
        <translation>རྒྱབ་ལྗོངས།</translation>
        <extra-contents_path>/Wallpaper/Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>color</source>
        <translation>ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>wallpaper</source>
        <translation>གྱང་ཤོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>centered</source>
        <translation>ལྟེ་བར་བཟུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>scaled</source>
        <translation>གཞི་ཁྱོན་ལྡན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>stretched</source>
        <translation>བརྐྱངས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>zoom</source>
        <translation>ཆེ་རུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>spanned</source>
        <translation>ཁྱབ་ཁོངས་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Blue cyan</source>
        <translation>ཁ་དོག་སྔོན་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Pine green</source>
        <translation>གསོམ་ཤིང་ལྗང་ཁུ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Emerald green</source>
        <translation>སྔོ་ལྗང་ལྗང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Green</source>
        <translation>ལྗང་མདོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Dark cyan</source>
        <translation>ཁ་དོག་ཟབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Slate green</source>
        <translation>རྡོ་ལེབ་ལྗང་ཁུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Mineral green</source>
        <translation>གཏེར་རྫས་ལྗང་ཁུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Taupe</source>
        <translation>ཁམ་ནག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Dark brown</source>
        <translation>ཁམ་ནག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Black</source>
        <translation>ནག་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Aurantiacus</source>
        <translation>ཚ་ལུ་མའི་སེར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Red</source>
        <translation>དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Brick-red</source>
        <translation>སོ་ཕག་དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Rose red</source>
        <translation>རྒྱ་སེ་དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Purplish red</source>
        <translation>ཁ་དོག་དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Dark magenta</source>
        <translation>རྒྱ་མཚོ་ཆེན་པོའི་དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Purple</source>
        <translation>ཁ་དོག་སྨུག་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Violet</source>
        <translation>ཁ་དོག་སྔོན་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Medium purple</source>
        <translation>ཁ་དོག་སྨུག་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Grey</source>
        <translation>སྐྱ་མདོག་སྐྱ་བོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="620"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>Wallpaper files (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *jpe *.gif *.tif *.tiff *wdp)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="660"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="703"/>
        <source>select custom wallpaper file</source>
        <translation>ཡུལ་སྲོལ་གོམས་གཤིས་ཀྱི་གྱང་ཤོག་ཡིག་ཆ་བདམས་པ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="661"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="704"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="662"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="705"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="663"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="706"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="664"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="707"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>night mode</source>
        <translation type="vanished">མཚན་མོའི་རྣམ་པ།</translation>
        <extra-contents_path>/Display/night mode</extra-contents_path>
    </message>
    <message>
        <source>screen zoom</source>
        <translation type="vanished">བརྙན་ཤེལ་ཆེ་རུ་གཏོང་བ།</translation>
        <extra-contents_path>/display/screen zoom</extra-contents_path>
    </message>
    <message>
        <source>Some applications need to be logouted to take effect</source>
        <translation type="vanished">རེ་འདུན་ཞུ་ཡིག་ཁ་ཤས་ཐོ་འགོད་བྱས་ནས་ནུས་པ་ཐོན་དགོས།</translation>
    </message>
    <message>
        <source>Mirror Display</source>
        <translation type="vanished">མེ་ལོང་འགྲེམས་སྟོན།</translation>
        <extra-contents_path>/display/unify output</extra-contents_path>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 29 seconds&lt;/font&gt;</source>
        <translation type="vanished">བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;དུས་ཚོད་སྐར་ཆ་29ཡི་རྗེས་སུ་སྒྲིག་བཀོད་ཉར་ཚགས་བྱེད་རྒྱུ་རེད། &lt;/font&gt;</translation>
    </message>
    <message>
        <source>Warnning</source>
        <translation type="vanished">ཉེན་བརྡ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="397"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="469"/>
        <source>Color Temperature</source>
        <translation>མཚན་མོའི་རྣམ་པ།</translation>
        <extra-contents_path>/Display/Color Temperature</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="805"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="807"/>
        <source>Network Display</source>
        <translation>དྲ་རྒྱ་འི་བརྡ་སྟོན་ཡོ་ཆས། </translation>
        <extra-contents_path>/Display/Network Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2348"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2375"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>When turned on, it will reduce the blue light of the screen</source>
        <translation type="vanished">སྤྱོད་འགོ་ཚུགས་རྗེས་བརྙན་ཡོལ་འོད་སྔོན་དམའ་རུ་བཏང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="414"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="431"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Custom Time</source>
        <translation>གོམས་སྲོལ་གྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="442"/>
        <source>to</source>
        <translation>དེ་ལྟར་བྱས་ན་</translation>
    </message>
    <message>
        <source>Eye Mode</source>
        <translation type="vanished">མིག་སྐྱོབ་རྣམ་པ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="473"/>
        <source>Warmer</source>
        <translation>སྔར་ལས་དྲོ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="476"/>
        <source>Colder</source>
        <translation>གྲང་ངར་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="793"/>
        <source>Multi-screen</source>
        <translation>བརྙན་ཤེལ་མང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="798"/>
        <source>First Screen</source>
        <translation>བརྙན་ཤེལ་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="799"/>
        <source>Vice Screen</source>
        <translation>བརྙན་ཤེལ་གཞོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="800"/>
        <source>Extend Screen</source>
        <translation>བརྙན་ཤེལ་རིང་དུ་གཏོང་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="801"/>
        <source>Clone Screen</source>
        <translation>ཁུ་ལོན་གྱི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <source>monitor</source>
        <translation type="vanished">ལྟ་ཞིབ་ཡོ་བྱད།</translation>
        <extra-contents_path>/display/monitor</extra-contents_path>
    </message>
    <message>
        <source>Theme follow night mode</source>
        <translation type="vanished">བརྗོད་བྱ་གཙོ་བོ་ནི་མཚན་མོའི་རྣམ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1018"/>
        <source>Auto Brightness</source>
        <translation>རང་འགུལ་གྱི་འོད་ཟེར་</translation>
        <extra-contents_path>/Display/Auto Brightness</extra-contents_path>
    </message>
    <message>
        <source>Adjust screen brightness by ambient</source>
        <translation type="vanished">ཁོར་ཡུག་གིས་བརྙན་ཤེལ་གྱི་གསལ་ཚད་སྙོམས་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Dynamic light</source>
        <translation type="vanished">འགུལ་རྣམ་གྱི་འོད་སྣང་།</translation>
        <extra-contents_path>/Display/Dynamic light</extra-contents_path>
    </message>
    <message>
        <source>Optimize display content to extend battery life</source>
        <translation type="vanished">འགྲེམས་སྟོན་གྱི་ནང་དོན་ལེགས་སུ་བཏང་ནས་གློག་གཡིས་ཀྱི་ཚེ་ཚད་ཇེ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1078"/>
        <source>resolution</source>
        <translation>གྲོས་ཆོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1081"/>
        <source>orientation</source>
        <translation>ཁ་ཕྱོགས་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1084"/>
        <source>frequency</source>
        <translation>ཐེངས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1087"/>
        <source>scale</source>
        <translation>གཞི་ཁྱོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1297"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ</translation>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 14 seconds&lt;/font&gt;</source>
        <translation type="vanished">བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;དུས་ཚོད་སྐར་ཆ་14ཡི་རྗེས་སུ་སྒྲིག་བཀོད་ཉར་ཚགས་བྱེད་རྒྱུ་རེད། &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1069"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1070"/>
        <source>Not Save</source>
        <translation>ཉར་ཚགས་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after %2 seconds&lt;/font&gt;</source>
        <translation type="vanished">བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;སྒྲིག་བཀོད་དེ་%2སྐར་ཆ་&lt;/font&gt;ཡི་རྗེས་སུ་ཉར་ཚགས་བྱེད་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <source>The zoom function needs to log out to take effect</source>
        <translation type="vanished">ཆེ་རུ་གཏོང་བའི་ནུས་པ་ཐོ་འགོད་བྱས་ནས་ནུས་པ་ཐོན་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1300"/>
        <source>Log out now</source>
        <translation>ད་ལྟ་ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1301"/>
        <source>Later</source>
        <translation>རྗེས་སུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1318"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1328"/>
        <source>(Effective after logout)</source>
        <translation>ཐོ་ཁོངས་ནས་སུབ་པར་ནུས་པ་ཐོན་པ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1693"/>
        <source>Open time should be earlier than close time!</source>
        <translation>སྒོ་འབྱེད་པའི་དུས་ཚོད་ནི་ཉེ་བའི་དུས་ཚོད་ལས་སྔ་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2264"/>
        <source>are you sure to apply?
1 Select &quot;apply&quot;, manually log out late
2 Select &quot;log out to apply&quot;, log out now to apply
3 Select &quot;cancel&quot;, cancel to apply</source>
        <translation>ཁྱོད་ཀྱིས་གཏན་ཁེལ་བྱེད་པའི་རེ་ཞུ་བྱས་ཆོག་གམ། 
༡་མོས་“ཉེར་སྤྱོད་”འཕྱི་བསུབ། 
༢་མོས་“བསུབ་རེ་ཞུ་”ཏུ་བསུབ་རེ་ཞུ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>select</source>
        <translation>བསལ་འདེམས། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>apply</source>
        <translation>ཉེར་སྤྱོད། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>log out to apply</source>
        <translation>བསུབ་གོ་ཆོད་པ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2347"/>
        <source>Turning on &apos;Eye Protection Mode&apos; will turn off &apos;Color Temperature&apos;. Continue turning it on?</source>
        <translation>“མིག་སྲུང་རྣམ་པའི”སྒོ་མོ་ཕྱེ་ཚེ“ཚོས་གཞི་དྲོད་ཚད”ཀྱི་སྒོ་རྒྱག་རྒྱུ་དང་མུ་མཐུད་སྒོ་མོ་འབྱེད་རྒྱུ་ཡིན་ནམ།？</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2349"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2376"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2374"/>
        <source>Turning on &apos;Color Temperature&apos; will turn off &apos;Eye Protection Mode&apos;. Continue turning it on?</source>
        <translation>“ཁ་དོག་དྲོད་ཚད”ཀྱི་སྒོ་མོ་ཕྱེ་ཚེ“མིག་སྲུང་རྣམ་པ”མུ་མཐུད་འབྱེད་སྲིད་དམ།？</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>All Day</source>
        <translation>ཉིན་གང་བོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Follow the sunrise and sunset</source>
        <translation>ཉི་མ་ཤར་བ་དང་ཉི་མ་ནུབ་པའི་རྗེས་སུ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2713"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2727"/>
        <source>Brightness</source>
        <translation>འོད་ཆེམ་ཆེམ་དུ་འཕྲོ</translation>
        <extra-contents_path>/Display/Brightness</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>please insure at least one output!</source>
        <translation>མ་མཐར་ཡང་ཐོན་ཚད་གཅིག་ལ་ཉེན་ཁ་བཟོ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="371"/>
        <source>Eye Protection Mode</source>
        <translation>མིག་སྐྱོབ་རྣམ་པ་</translation>
        <extra-contents_path>/Display/Eye Protection Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="372"/>
        <source>When turned on, it can reduce blue light to prevent eye, the screen will turn yellow.</source>
        <translation>ཁ་ཕྱེས་རྗེས་འོད་སྔོན་རྩིབས་འཕྲོ་ཇེ་ཉུང་དུ་བཏང་ནས་མིག་ལ་ཐང་ཆད་པར་སྔོན་འགོག་བྱེད་ཐུབ།འཆར་ངོས་ཅུང་སེར་པོར་འགྱུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="340"/>
        <source>Color Temperature And Eye Care</source>
        <translation>མདོག་དྲོད་དང་མིག་སྲུང་</translation>
        <extra-contents_path>/Display/Color Temperature And Eye Care</extra-contents_path>
    </message>
    <message>
        <source>When turned on, it will reduce the blue light of the screen.</source>
        <translation type="vanished">སྤྱོད་འགོ་ཚུགས་རྗེས་བརྙན་ཡོལ་འོད་སྔོན་དམའ་རུ་བཏང་བ།.</translation>
    </message>
    <message>
        <source>Net Monitor</source>
        <translation type="vanished">དྲ་རྒྱ་འི་བརྡ་སྟོན་ཡོ་ཆས། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="840"/>
        <source>Display</source>
        <translation>བརྡ་སྟོན་ཡོ་ཆས། </translation>
        <extra-contents_path>/Display/Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="845"/>
        <source>Primary Screen</source>
        <translation>གཙོ་བརྙན་ཤེལ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1091"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after 14 seconds&lt;/font&gt;</source>
        <translation>བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;དུས་ཚོད་སྐར་ཆ་14ཡི་རྗེས་སུ་སྒྲིག་བཀོད་ཉར་ཚགས་བྱེད་རྒྱུ་རེད། &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1102"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after %2 seconds&lt;/font&gt;</source>
        <translation>བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;དུས་ཚོད་སྐར་ཆ་%2ཡི་རྗེས་སུ་སྒྲིག་བཀོད་ཉར་ཚགས་བྱེད་རྒྱུ་རེད། &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1299"/>
        <source>The zoom has been modified, it will take effect after you log off</source>
        <translation>འཆར་ངོས་ཇེ་ཆུང་དུ་བཏང་སྟེ་བཟོ་བཅོས་བྱས།ཐོ་ཁོངས་ནས་སུབ་རྗེས་ནུས་པ་ཐོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1692"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1792"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1793"/>
        <source>Sorry, your configuration could not be applied.
Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU.</source>
        <translation>དགོངས་པ་མ་ཚོམ། ཁྱེད་ཚོའི་བཀོད་སྒྲིག་བཀོལ་སྤྱོད་བྱེད་ཐབས་མེད།
ཐུན་མོང་གི་རྒྱུ་རྐྱེན་ནི་སྤྱིའི་བརྙན་ཤེལ་གྱི་གཞི་ཁྱོན་ཆེ་དྲགས་པའམ་ཡང་ན་ཁྱོད་ཀྱིས་GPUཡི་རྒྱབ་སྐྱོར་ལས་མང་བའི་མངོན་ཚུལ་མངོན་ཐུབ་པ་དེ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>addShortcutDialog</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="88"/>
        <source>Exec</source>
        <translation>ཨེ་ཤེ་ཡ་དང་ཞི་བདེ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="174"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="222"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="305"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="248"/>
        <source>Key</source>
        <translation>ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="373"/>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="126"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="354"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="275"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="86"/>
        <source>Add Shortcut</source>
        <translation>མགྱོགས་ལམ་ཁ་སྣོན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="103"/>
        <source>Please enter a shortcut</source>
        <translation>མྱུར་བགྲོད་གཞུང་ལམ་ནང་དུ་འཛུལ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="233"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ཅོག་ངོས་ཡིག་ཆ་(*.desktop)</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="274"/>
        <source>select desktop</source>
        <translation>ཅོག་ཙེའི་སྟེང་ནས་གདམ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="329"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="348"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="357"/>
        <source>Invalid application</source>
        <translation>གོ་མི་ཆོད་པའི་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="331"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="344"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="353"/>
        <source>Shortcut conflict</source>
        <translation>མྱུར་བགྲོད་གདོང་གཏུག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="333"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="346"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="355"/>
        <source>Invalid shortcut</source>
        <translation>གོ་མི་ཆོད་པའི་མྱུར་བགྲོད་གཞུང་ལམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="336"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="341"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="360"/>
        <source>Name repetition</source>
        <translation>མིང་བསྐྱར་ཟློས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="364"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="542"/>
        <source>Shortcut cannot be empty</source>
        <translation>མྱུར་ལམ་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="546"/>
        <source>Name cannot be empty</source>
        <translation>མིང་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="556"/>
        <source>Desktop prohibits adding</source>
        <translation>བཀག་འགོག</translation>
    </message>
</context>
<context>
    <name>changeUserGroup</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="31"/>
        <source>user group</source>
        <translation>སྤྱོད་མཁན་ཚོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="120"/>
        <source>Group:</source>
        <translation>ཚོ་ཆུང་ནི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="133"/>
        <source>GID:</source>
        <translation>སྤྱོད་མཁན་:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="147"/>
        <source>GNum:</source>
        <translation>ཁོངས་མི།:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="191"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="617"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="194"/>
        <source>Save</source>
        <translation>ཉར་ཚགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="618"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Tips</source>
        <translation>གསལ་འདེབས་བྱེད་ཐབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Invalid Id!</source>
        <translation>གོ་མི་ཆོད་པའི་ཐོབ་ཐང་ལག་ཁྱེར་</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="575"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="583"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="659"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <source>Invalid Group Name!</source>
        <translation>གོ་མི་ཆོད་པའི་ཚོགས་པའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="615"/>
        <source>Whether delete the group: “%1” ?</source>
        <translation>ཚོ་ཆུང་དེ་བསུབ་ཡོད་མེད་ལ་མ་བལྟོས་པར་&quot;%1&quot;ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="616"/>
        <source>which will make some file components in the file system invalid!</source>
        <translation>དེས་ཡིག་ཆའི་མ་ལག་ཁྲོད་ཀྱི་ཡིག་ཆའི་ལྷུ་ལག་ཁ་ཤས་གོ་མི་ཆོད་པར་འགྱུར་སྲིད།</translation>
    </message>
</context>
<context>
    <name>changtimedialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="32"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="115"/>
        <source>current date</source>
        <translation>ད་ལྟའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="200"/>
        <source>time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="321"/>
        <source>year</source>
        <translation>ལོ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="398"/>
        <source>month</source>
        <translation>ཟླ་བ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="472"/>
        <source>day</source>
        <translation>ཉིན་མོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="574"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="593"/>
        <source>confirm</source>
        <translation>ངོས་འཛིན་བྱས་པ།</translation>
    </message>
</context>
</TS>
