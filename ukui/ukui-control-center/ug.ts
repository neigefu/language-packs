<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>About</name>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="614"/>
        <source>System Summary</source>
        <translation>سىستېما خۇلاسە</translation>
        <extra-contents_path>/About/System Summary</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="615"/>
        <source>Support</source>
        <translation>قوللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="227"/>
        <source>Version Number</source>
        <translation>نەشر نومۇرى</translation>
        <extra-contents_path>/About/Version Number</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="684"/>
        <source>Status</source>
        <translation>ھالەت</translation>
        <extra-contents_path>/About/Status</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="689"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1412"/>
        <source>DateRes</source>
        <translation>DateRes</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="618"/>
        <source>Wechat code scanning obtains HP professional technical support</source>
        <translation>ئۈندىدار كودىنى سىكاننېرلاش HP كەسپىي تېخنىكا جەھەتتىن قوللاشقا ئېرىشتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="308"/>
        <source>HostName</source>
        <translation>HostName</translation>
        <extra-contents_path>/About/HostName</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="619"/>
        <source>See more about Kylin Tianqi edu platform</source>
        <translation>Kylin Tianqi edu سۇپىسى ھەققىدە تېخىمۇ كۆپ مەلۇمات</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="527"/>
        <source>&lt;&lt;Protocol&gt;&gt;</source>
        <translation>&lt;&lt;Protocol&gt;&gt;</translation>
        <extra-contents_path>/About/&lt;&lt;Protocol&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="66"/>
        <source>About and Support</source>
        <translation>ھەققىدە ۋە قوللاش</translation>
    </message>
    <message>
        <source>InterVersion</source>
        <translation type="vanished">InterVersion</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="247"/>
        <source>Patch Version</source>
        <translation>Patch نەشرى</translation>
        <extra-contents_path>/About/Patch Version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="266"/>
        <source>Installed Date</source>
        <translation>قاچىلانغان ۋاقتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="285"/>
        <source>Upgrade Date</source>
        <translation>يېڭىلاش ۋاقتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="451"/>
        <source>Privacy and agreement</source>
        <translation>مەخپىيەتلىك ۋە كېلىشىم</translation>
        <extra-contents_path>/About/Privacy and agreement</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="463"/>
        <source>Send optional diagnostic data</source>
        <translation>ئىختىيارىي دىياگنوز سانلىق مەلۇماتى يوللاش</translation>
        <extra-contents_path>/About/Send optional diagnostic data</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="465"/>
        <source>By sending us diagnostic data, improve the system experience and solve your problems faster</source>
        <translation>دىياگنوز سانلىق مەلۇماتىنى بىزگە يوللاش ئارقىلىق، سىستېما تەجرىبىسىنى ئۆستۈرۈپ، مەسىلىلەرنى تېخىمۇ تېز ھەل قىلىش</translation>
    </message>
    <message>
        <source>Copyright © 2009-%1 KylinSoft. All rights reserved.</source>
        <translation type="vanished">Copyright © 2009-%1 KylinSoft. بارلىق ھوقۇق ماقامى.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="528"/>
        <source>and</source>
        <translation>ۋە</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="530"/>
        <source>&lt;&lt;Privacy&gt;&gt;</source>
        <translation>&lt;&lt;Privacy&gt;&gt;</translation>
        <extra-contents_path>/About/&lt;&lt;Privacy&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="638"/>
        <source>Learn more HP user manual&gt;&gt;</source>
        <translation>HP ئىشلەتكۈچى قوللانمىسىنى تېخىمۇ كۆپ بىلىۋېلىڭ&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="646"/>
        <source>See user manual&gt;&gt;</source>
        <translation>قوللانما قوللانمىسىغا قاراڭ&gt;&gt;</translation>
    </message>
    <message>
        <source>Not activated (trial period)</source>
        <translation type="vanished">未激活(试用期)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="749"/>
        <source>Trial expiration time</source>
        <translation>سىناپ ئىشلىتىش مۇددىتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1154"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1431"/>
        <source>expired</source>
        <translation>ۋاقتى ئۆتۈپ كەتكەن</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="770"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1156"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1414"/>
        <source>Extend</source>
        <translation>ئۇزارتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="516"/>
        <source>Copyright © 2020 KylinSoft. All rights reserved.</source>
        <translation>Copyright © 2020 KylinSoft. بارلىق ھوقۇق ماقامى.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1385"/>
        <source>The system needs to be restarted to set the HostName, whether to reboot</source>
        <translation>HostName نى بەلگىلەش ئۈچۈن سىستېمىنى قايتا قوزغىتىش كېرەك، قايتا قوزغىتىش كېرەكمۇ يوق</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1386"/>
        <source>Reboot Now</source>
        <translation>ھازىر قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1387"/>
        <source>Reboot Later</source>
        <translation>كېيىن قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <source>Technical service has expired</source>
        <translation type="vanished">已过期</translation>
    </message>
    <message>
        <source>Extended</source>
        <translation type="vanished">延长服务</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="999"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1008"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1560"/>
        <source>avaliable</source>
        <translation>ئىناۋەتسىز</translation>
    </message>
    <message>
        <source>version</source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Copyright 2009-2020 @ Kylinos All rights reserved</source>
        <translation type="vanished">版权所有2009-2020@kylinos保留所有权利</translation>
    </message>
    <message>
        <source>Copyright 2009-2021 @ Kylinos All rights reserved</source>
        <translation type="vanished">版权所有2009-2021@kylinos保留所有权利</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="672"/>
        <source>Version</source>
        <translation>نەشرى</translation>
        <extra-contents_path>/About/version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="839"/>
        <source>Kylin Linux Desktop V10 (SP1)</source>
        <translation>Kylin Linux ئۈستەل يۈزى V10 (SP1)</translation>
    </message>
    <message>
        <source>Copyright @ 2009-2021 KylinSoft. All rights reserved.</source>
        <translation type="vanished">版权所有 @ 2009-2021 麒麟软件 保留所有权利。</translation>
    </message>
    <message>
        <source>Copyright © 2009-2021 KylinSoft. All rights reserved.</source>
        <translation type="vanished">版权所有 © 2009-2021 麒麟软件 保留所有权利。</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="674"/>
        <source>Kernel</source>
        <translation>Kernel</translation>
        <extra-contents_path>/About/Kernel</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="676"/>
        <source>CPU</source>
        <translation>CPU</translation>
        <extra-contents_path>/About/CPU</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="678"/>
        <source>Memory</source>
        <translation>ئىچكى ساقلىغۇچ</translation>
        <extra-contents_path>/About/Memory</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="616"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1006"/>
        <source>Disk</source>
        <translation>دىسكاز</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="680"/>
        <source>Desktop</source>
        <translation>ئۈستەلئۈستى</translation>
        <extra-contents_path>/About/Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="682"/>
        <source>User</source>
        <translation>ئىشلەتكۈچى</translation>
        <extra-contents_path>/About/User</extra-contents_path>
    </message>
    <message>
        <source>Active Status</source>
        <translation type="vanished">激活状态</translation>
    </message>
    <message>
        <source>DataRes</source>
        <translation type="vanished">有效期</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="688"/>
        <source>Serial</source>
        <translation>قاتار</translation>
        <extra-contents_path>/About/Serial</extra-contents_path>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="vanished">免责协议</translation>
    </message>
    <message>
        <source>Service serial number</source>
        <translation type="vanished">序列号</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="686"/>
        <location filename="../../../plugins/system/about/about.cpp" line="751"/>
        <location filename="../../../plugins/system/about/about.cpp" line="757"/>
        <source>Active</source>
        <translation>ئاكتىپ</translation>
        <extra-contents_path>/About/Active</extra-contents_path>
    </message>
    <message>
        <source>Trial version disclaimer</source>
        <translation type="vanished">试用版免责声明</translation>
    </message>
    <message>
        <source>Devices Summary</source>
        <translation type="vanished">设备规格</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="68"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <source>The system has expired. The expiration time is:</source>
        <translation type="vanished">您的系统已激活，技术服务已到期：</translation>
    </message>
    <message>
        <source>Disk:</source>
        <translation type="vanished">硬盘:</translation>
    </message>
    <message>
        <source> available</source>
        <translation type="vanished">可用</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="747"/>
        <location filename="../../../plugins/system/about/about.cpp" line="755"/>
        <source>Inactivated</source>
        <translation>مۇلازمەتكە ئۇچرىدى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="768"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1411"/>
        <source>Activated</source>
        <translation>قوزغىتىلدى</translation>
    </message>
    <message>
        <source>UNKNOWN</source>
        <translation type="vanished">未知的</translation>
    </message>
    <message>
        <source>Current desktop env:</source>
        <translation type="vanished">当前桌面环境：</translation>
    </message>
    <message>
        <source>OS Version:</source>
        <translation type="vanished">系统版本：</translation>
    </message>
    <message>
        <source>CPU Arch:</source>
        <translation type="vanished">CPU架构：</translation>
    </message>
    <message>
        <source>Kernel Version</source>
        <translation type="vanished">内核版本</translation>
    </message>
    <message>
        <source>Manufacturers:</source>
        <translation type="vanished">制造商：</translation>
    </message>
    <message>
        <source>Product Name:</source>
        <translation type="vanished">产品名：</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Serial Number:</source>
        <translation type="vanished">序列号：</translation>
    </message>
</context>
<context>
    <name>Accessibility</name>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="32"/>
        <source>Vision</source>
        <translation>كۆرۈش (ۋىدى)</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="131"/>
        <source>Enable magnifying glass: Enlarge the content of the desktop</source>
        <translation>چوڭايتىش ئەينىكىنى قوزغىتىش: ئۈستەل يۈزىنىڭ مەزمۇنىنى چوڭايتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="138"/>
        <source>Press Win + &quot;+&quot; to zoom in on the content, Win + &quot;-&quot; to zoom out on the content.</source>
        <translation>Win + &quot;+&quot; نى بېسىپ، مەزمۇننى چوڭايتىپ، Win + «-» نى بېسىپ، مەزمۇننى چوڭايتىڭ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="225"/>
        <source>Zoom</source>
        <translation>چوڭايت</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="315"/>
        <source>Color Filter Effect</source>
        <translation>رەڭ سۈزگۈچ ئۈنۈمى</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="415"/>
        <source>Color Filter</source>
        <translation>رەڭ سۈزگۈچ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="506"/>
        <source>Press Win + Ctrl + C to turn on/off color effect</source>
        <translation>Win + Ctrl + C نى بېسىپ رەڭ ئۈنۈمىنى ئېچىش/ ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="529"/>
        <source>Other Settings</source>
        <translation>باشقا تەڭشەكلەر</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="585"/>
        <source>Point Size</source>
        <translation>نۇقتا چوڭلۇقى</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="604"/>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="659"/>
        <source>Set</source>
        <translation>بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="640"/>
        <source>Vocal Tract Regulation</source>
        <translation>ئاۋاز يولى نىزامى</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="10"/>
        <source>Accessibility</source>
        <translation>زىيارەت قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="80"/>
        <source>Window Zoom</source>
        <translation>كۆزنەكنى چوڭايت</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="81"/>
        <source>Full Screen Zoom</source>
        <translation>پۈتۈن ئېكران چوڭايتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="83"/>
        <source>Red/Green Filter (Protanopia)</source>
        <translation>قىزىل/يېشىل سۈزگۈچ (Protanopia)</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="84"/>
        <source>Green/Red Filter (Deuteranopia)</source>
        <translation>يېشىل/قىزىل سۈزگۈچ (دېۋتېرەنopia)</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="85"/>
        <source>Blue/Yellow Filter (Tritanopia)</source>
        <translation>كۆك/سېرىق سۈزگۈچ (ترىتانپىيە)</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="86"/>
        <source>Grayscale</source>
        <translation>كۈل رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="87"/>
        <source>Invert</source>
        <translation>تەتۈر</translation>
    </message>
</context>
<context>
    <name>AddAppDialog</name>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>AddAutoBoot</name>
    <message>
        <source>Add AutoBoot</source>
        <translation type="vanished">添加自启动程序</translation>
    </message>
    <message>
        <source>Add autoboot program</source>
        <translation type="vanished">Autoboot پروگرامما قوشۇش</translation>
    </message>
    <message>
        <source>Program name</source>
        <translation type="vanished">程序名</translation>
    </message>
    <message>
        <source>Program exec</source>
        <translation type="vanished">程序路径</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">ئېچىش</translation>
    </message>
    <message>
        <source>Program comment</source>
        <translation type="vanished">程序描述</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="123"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="124"/>
        <source>Exec</source>
        <translation>Exec</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="125"/>
        <source>Comment</source>
        <translation>ئىنكاس</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="200"/>
        <source>Select AutoStart Desktop</source>
        <translation>AutoStart ئۈستەلئۈستىنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="202"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">چۇقۇم</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="193"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ئۈستەل يۈزى ھۆججەتلىرى(*.desktop)</translation>
    </message>
    <message>
        <source>Select Autoboot Desktop</source>
        <translation type="vanished">ئاپتوماتىك قوزغىتىدىغان ئۈستەل يۈزىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="201"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="248"/>
        <source>desktop file not allowed add</source>
        <translation>ئۈستەلئۈستى ھۆججىتى قوشۇشقا يول قويۇلمىدى</translation>
    </message>
    <message>
        <source>desktop file  already exist</source>
        <translation type="vanished">桌面文件已经存在</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="297"/>
        <source>desktop file not exist</source>
        <translation>ئۈستەلئۈستى ھۆججىتى مەۋجۇت ئەمەس</translation>
    </message>
</context>
<context>
    <name>AddAutoStart</name>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="177"/>
        <source>Add AutoStart program</source>
        <translation>AutoStart پروگراممىسىنى قوشۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="178"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="179"/>
        <source>Exec</source>
        <translation>Exec</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="180"/>
        <source>Comment</source>
        <translation>ئىنكاس</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="181"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="182"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="183"/>
        <source>Certain</source>
        <translation>چۇقۇم</translation>
    </message>
</context>
<context>
    <name>AddBtn</name>
    <message>
        <location filename="../../../libukcc/widgets/AddBtn/addbtn.cpp" line="23"/>
        <source>Add</source>
        <translation>قوش</translation>
    </message>
</context>
<context>
    <name>AddButton</name>
    <message>
        <location filename="../../../libukcc/widgets/SettingWidget/addbutton.cpp" line="25"/>
        <source>Add</source>
        <translation>قوش</translation>
    </message>
</context>
<context>
    <name>AddInputMethodDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="26"/>
        <source>Select the input method to add</source>
        <translation>خەت كىرگۈزگۈچنى تاللاپ قوشۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="82"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="101"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>keyboard</source>
        <translation>كۇنۇپكا تاختىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>Tibetan</source>
        <translation>تىبەت تىلى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="7"/>
        <source>With ASCII numbers</source>
        <translation>ASCII سانلىرى بىلەن</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="15"/>
        <source>Input Method</source>
        <translation>كىرگۈزگۈچ</translation>
    </message>
</context>
<context>
    <name>AddLanguageDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="179"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="198"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="22"/>
        <source>Add Language</source>
        <translation>تىل قوشۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="23"/>
        <source>Search</source>
        <translation>ئىزدە</translation>
    </message>
</context>
<context>
    <name>AddNetBtn</name>
    <message>
        <source>Add WiredNetork</source>
        <translation type="vanished">添加有线网络</translation>
    </message>
</context>
<context>
    <name>AppDetail</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">更改时间</translation>
    </message>
    <message>
        <source>Allow notification</source>
        <translation type="vanished">允许通知</translation>
    </message>
    <message>
        <source>Number of notification centers</source>
        <translation type="vanished">最大通知数量</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>AppUpdateWid</name>
    <message>
        <source>Lack of local disk space!</source>
        <translation type="vanished">磁盘空间不足！</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">更新</translation>
    </message>
    <message>
        <source>Network abnormal!</source>
        <translation type="vanished">网络异常！</translation>
    </message>
    <message>
        <source>Download failed!</source>
        <translation type="vanished">下载失败！</translation>
    </message>
    <message>
        <source>failed to get from the source!</source>
        <translation type="vanished">从源中获取失败！</translation>
    </message>
    <message>
        <source>The download cache has been removed</source>
        <translation type="vanished">下载缓存已被删除</translation>
    </message>
    <message>
        <source>Being installed</source>
        <translation type="vanished">正在安装</translation>
    </message>
    <message>
        <source>Update succeeded , please restart the system!</source>
        <translation type="vanished">更新成功，请重启系统!</translation>
    </message>
    <message>
        <source>Update succeeded , please log in to the system again!</source>
        <translation type="vanished"> 更新成功，请注销重新登录系统!</translation>
    </message>
    <message>
        <source>Update succeeded!</source>
        <translation type="vanished">更新成功！</translation>
    </message>
    <message>
        <source>Update failed!</source>
        <translation type="vanished">更新失败！</translation>
    </message>
    <message>
        <source>Failure reason:</source>
        <translation type="vanished">失败原因：</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <source>Update log</source>
        <translation type="vanished">更新日志</translation>
    </message>
    <message>
        <source>Newest:</source>
        <translation type="vanished">最新：</translation>
    </message>
    <message>
        <source>Download size:</source>
        <translation type="vanished">下载大小：</translation>
    </message>
    <message>
        <source>Current version:</source>
        <translation type="vanished">当前版本：</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>In the pause</source>
        <translation type="vanished">暂停中</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Update succeeded , It is recommended that you restart later!</source>
        <translation type="vanished">更新成功，建议您稍后重启！</translation>
    </message>
    <message>
        <source>Update succeeded , It is recommended that you log out later and log in again!</source>
        <translation type="vanished">更新成功，建议您稍后注销重新登录系统！</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>A single update will not automatically backup the system, if you want to backup, please click Update All.</source>
        <translation type="vanished">单个更新不会自动备份系统，如需备份，请点击全部更新。</translation>
    </message>
    <message>
        <source>Prompt information</source>
        <translation type="vanished">提示信息</translation>
    </message>
    <message>
        <source>Do not backup, continue to update</source>
        <translation type="vanished">不备份，继续更新</translation>
    </message>
    <message>
        <source>Cancel update</source>
        <translation type="vanished">取消更新</translation>
    </message>
    <message>
        <source>This time will no longer prompt</source>
        <translation type="vanished">本次更新不再提示</translation>
    </message>
    <message>
        <source>Calculate the download speed</source>
        <translation type="vanished">正在获取当前下载进度</translation>
    </message>
    <message>
        <source>Get depends failed!</source>
        <translation type="vanished">依赖获取异常！</translation>
    </message>
    <message>
        <source>In the update</source>
        <translation type="vanished">更新中</translation>
    </message>
    <message>
        <source>Ready to install</source>
        <translation type="vanished">准备安装</translation>
    </message>
    <message>
        <source>Calculate the download progress</source>
        <translation type="vanished">正在计算当前下载速度</translation>
    </message>
    <message>
        <source>No content.</source>
        <translation type="vanished">暂无内容.</translation>
    </message>
</context>
<context>
    <name>AptProxyDialog</name>
    <message>
        <source>Set Apt Proxy</source>
        <translation type="vanished">Apt Proxy نى بەلگىلەش</translation>
    </message>
    <message>
        <source>Server Address</source>
        <translation type="vanished">مۇلازىمىتېر ئادرېسى</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">پورت</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>Area</name>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="26"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="44"/>
        <source>Area</source>
        <translation>رايون تەۋەلىكى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="156"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="615"/>
        <source>Regional Format</source>
        <translation>رايون شەكلى</translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="427"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="622"/>
        <source>Short Format Date</source>
        <translation>قىسقا فورمات ۋاقتى</translation>
        <extra-contents_path>/Area/Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="510"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="627"/>
        <source>Long Format Date</source>
        <translation>ئۇزۇن فورمات ۋاقتى</translation>
        <extra-contents_path>/Area/Long Format Date</extra-contents_path>
    </message>
    <message>
        <source>Current Region</source>
        <translation type="vanished">ھازىرقى رايون</translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="344"/>
        <source>First Day Of The Week</source>
        <translation>ھەپتىنىڭ بىرىنچى كۈنى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="258"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="617"/>
        <source>Calendar</source>
        <translation>كالېندار</translation>
        <extra-contents_path>/Area/Calendar</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="59"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="236"/>
        <source>Language Format</source>
        <translation>تىل شەكلى</translation>
        <extra-contents_path>/Area/Regional Format</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="624"/>
        <source>Date</source>
        <translation>چېسلا</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="593"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="629"/>
        <source>Time</source>
        <translation>ۋاقتىدا</translation>
        <extra-contents_path>/Area/Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="676"/>
        <source>Language Format Example</source>
        <translation>تىل شەكلى مىسالى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="768"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <source>Area showing time currency format</source>
        <translation type="vanished">显示日期、货币、时间、货币格式的区域</translation>
    </message>
    <message>
        <source>Regional format data</source>
        <translation type="vanished">区域格式数据</translation>
    </message>
    <message>
        <source>lunar</source>
        <translation type="vanished">ئاي</translation>
    </message>
    <message>
        <source>First day of the week</source>
        <translation type="vanished">一周的第一天</translation>
    </message>
    <message>
        <source>day</source>
        <translation type="vanished">号</translation>
    </message>
    <message>
        <source>area</source>
        <translation type="vanished">区域语言</translation>
    </message>
    <message>
        <source>current area</source>
        <translation type="vanished">当前区域</translation>
    </message>
    <message>
        <source>display format area</source>
        <translation type="vanished">显示日期、时间、货币格式的区域</translation>
    </message>
    <message>
        <source>US</source>
        <translation type="vanished">ئامېرىكا</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="241"/>
        <source>Language for system windows,menus and web pages</source>
        <translation>سىستېما windows،تىزىملىك ۋە تور بەتلەر ئۈچۈن تىل</translation>
    </message>
    <message>
        <source>Add main language</source>
        <translation type="vanished">添加首语言</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="734"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="238"/>
        <source>System Language</source>
        <translation>سىستېما تىلى</translation>
        <extra-contents_path>/Area/system language</extra-contents_path>
    </message>
    <message>
        <source>Simplified Chinese</source>
        <translation type="vanished">简体中文</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="172"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="677"/>
        <source>MMMM dd, yyyy</source>
        <translation>MMMM dd, yyyy</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="175"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="680"/>
        <source>MMMM d, yy</source>
        <translation>MMMM d, yy</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="247"/>
        <source>Add</source>
        <translation>قوش</translation>
        <extra-contents_path>/Area/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="362"/>
        <source>English  (US)</source>
        <translation>ئىنگلىز تىلى (ئامېرىكا)</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="363"/>
        <source>Simplified Chinese  (CN)</source>
        <translation>ئاددىيلاشتۇرۇلغان خەنزۇچە (CN)</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="364"/>
        <source>Tibetan  (CN)</source>
        <translation>تىبەتچە (CN)</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="379"/>
        <source>Monday</source>
        <translation>دۈشەنبە</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="380"/>
        <source>Sunday</source>
        <translation>يەكشەنبە</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="619"/>
        <source>First Day Of Week</source>
        <translation>ھەپتىنىڭ بىرىنچى كۈنى</translation>
        <extra-contents_path>/Area/First Day Of Week</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="631"/>
        <source>Solar Calendar</source>
        <translation>قۇياش كالېندارى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="634"/>
        <source>Lunar</source>
        <translation>ئاي</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="650"/>
        <source>12 Hours</source>
        <translation>12 سائەت</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="651"/>
        <source>24 Hours</source>
        <translation>24 سائەت</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="923"/>
        <source>Modify the current region need to logout to take effect, whether to logout?</source>
        <translation>ھازىرقى رايونلارنى ئۆزگەرتىشكە توغرا كەلسە، ئۈنۈمىنى كۆرسىتىش كېرەكمۇ- يوق؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="924"/>
        <source>Logout later</source>
        <translation>كېيىن چېكىنىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="925"/>
        <source>Logout now</source>
        <translation>ھازىر چېكىنىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="927"/>
        <source>Modify the first language need to restart to take effect, whether to restart?</source>
        <translation>1.تىلنى ئۆزگەرتىش قايتا قوزغاتسا ئاندىن ئۈنۈمگە ئىھتىياجلىق بولىدۇ،قايتا قوزغىتىش كېرەكمۇ يوق؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="928"/>
        <source>Restart later</source>
        <translation>كېيىن قايتا قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="929"/>
        <source>Restart now</source>
        <translation>ھازىر قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <source>Modify the first language need to reboot to take effect, whether to reboot?</source>
        <translation type="vanished">1. تىلنى ئۆزگەرتىش ئارقىلىق قايتىدىن قوزغىتىلىشقا توغرا كېلىدۇ، قايتىدىن قوزغىلىش كېرەكمۇ-يوق؟</translation>
    </message>
    <message>
        <source>Reboot later</source>
        <translation type="vanished">كېيىن قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <source>Reboot now</source>
        <translation type="vanished">ھازىر قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <source>calendar</source>
        <translation type="vanished">日历</translation>
    </message>
    <message>
        <source>first day of week</source>
        <translation type="vanished">一周的第一天</translation>
    </message>
    <message>
        <source>date</source>
        <translation type="vanished">日期</translation>
    </message>
    <message>
        <source>2019/12/17</source>
        <translation type="vanished">2019/12/17</translation>
    </message>
    <message>
        <source>time</source>
        <translation type="vanished">时间</translation>
    </message>
    <message>
        <source>9:52</source>
        <translation type="vanished">9:52</translation>
    </message>
    <message>
        <source>change format of data</source>
        <translation type="vanished">更改数据格式</translation>
    </message>
    <message>
        <source>first language</source>
        <translation type="vanished">首选语言</translation>
    </message>
    <message>
        <source>system language</source>
        <translation type="vanished">显示语言</translation>
    </message>
    <message>
        <source>CN</source>
        <translation type="vanished">CN</translation>
    </message>
    <message>
        <source>Need to cancel to take effect</source>
        <translation type="vanished">需要注销生效</translation>
    </message>
    <message>
        <source>Need to log off to take effect</source>
        <translation type="vanished">需要注销生效</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <source>country</source>
        <translation type="vanished">显示日期，时间，货币格式的区域</translation>
    </message>
    <message>
        <source>regional format</source>
        <translation type="vanished">区域格式数据</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">English</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">简体中文</translation>
    </message>
    <message>
        <source>add main language</source>
        <translation type="vanished">添加首语言</translation>
    </message>
    <message>
        <source>solar calendar</source>
        <translation type="vanished">قۇياش كالېندارى</translation>
    </message>
    <message>
        <source>monday</source>
        <translation type="vanished">星期一</translation>
    </message>
    <message>
        <source>sunday</source>
        <translation type="vanished">星期日</translation>
    </message>
    <message>
        <source>change data format</source>
        <translation type="vanished">更改数据格式</translation>
    </message>
</context>
<context>
    <name>AreaCodeLineEdit</name>
    <message>
        <source>Sign up by Phone</source>
        <translation type="vanished">请输入手机号码</translation>
    </message>
</context>
<context>
    <name>Audio</name>
    <message>
        <source>Audio</source>
        <translation type="vanished">声音</translation>
    </message>
</context>
<context>
    <name>AutoBoot</name>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="466"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ئۈستەل يۈزى ھۆججەتلىرى(*.desktop)</translation>
    </message>
    <message>
        <source>select autoboot desktop</source>
        <translation type="vanished">ئاپتوماتىك قوزغىتىدىغان ئۈستەل يۈزىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="88"/>
        <source>Auto Start</source>
        <translation>ئاپتۇماتىك قوزغىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="474"/>
        <source>Select AutoStart Desktop</source>
        <translation>AutoStart ئۈستەلئۈستىنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="475"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="476"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="623"/>
        <source>Add</source>
        <translation>قوش</translation>
        <extra-contents_path>/AutoStart/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="630"/>
        <source>AutoStart Settings</source>
        <translation>AutoStart تەڭشەكلىرى</translation>
        <extra-contents_path>/AutoStart/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Autoboot Settings</source>
        <translation type="vanished">Autoboot تەڭشەكلىرى</translation>
        <extra-contents_path>/autoboot/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Add autoboot app </source>
        <translation type="vanished">添加自启动程序 </translation>
    </message>
    <message>
        <source>autoboot</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>Autoboot</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>Auto Boot</source>
        <translation type="vanished">ئاپتوماتىك ئۆتۈك</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">程序名称</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">当前状态</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="243"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
</context>
<context>
    <name>Backup</name>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="53"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="119"/>
        <source>Backup</source>
        <translation>زاپاسلاش</translation>
        <extra-contents_path>/Backup/Backup</extra-contents_path>
    </message>
    <message>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted, you can restore them to ensure
the integrity of your system.</source>
        <translation type="vanished">将您的文件备份到其他驱动器，当源文件丢失、受损或被删除时可以还原它们，保证系统的完整性。</translation>
    </message>
    <message>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted,
you can restore them to ensure the integrity of your system.</source>
        <translation type="vanished">将您的文件备份到其他驱动器，当源文件丢失、受损、删除时还原它们，保证系统的完整性。</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="69"/>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted, 
you can restore them to ensure the integrity of your system.</source>
        <translation>ھۆججەتلىرىڭىزنى باشقا قوزغاتقۇچلارغا زاپاسلىسىڭىز، ئەسلى ھۆججەتلەر يوقاپ كەتكەن، بۇزۇلغان ياكى ئۆچۈرۈلگەندە ، 
سىز ئۇلارنى ئەسلىگە كەلتۈرۈپ، سىستېمىڭىزنىڭ پۈتۈنلىكىگە كاپالەتلىك قىلالايسىز.</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="113"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="115"/>
        <source>Begin backup</source>
        <translation>زاپاسلاشنى باشلاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="157"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="145"/>
        <source>Restore</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
        <extra-contents_path>/Backup/Restore</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="173"/>
        <source>View a list of backed-upfiles to backed up files to the system</source>
        <translation>سىستېمىغا زاپاس ھۆججەتلەرنى زاپاسلاش تىزىملىكىنى كۆرۈش</translation>
    </message>
    <message>
        <source>View a list of backed-upfiles to restore backed up files to the system</source>
        <translation type="vanished">查看备份列表，将已备份文件还原至系统</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="213"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="141"/>
        <source>Begin restore</source>
        <translation>ئەسلىگە كەلتۈرۈشنى باشلاش</translation>
    </message>
    <message>
        <source>backup</source>
        <translation type="vanished">备份</translation>
    </message>
    <message>
        <source>All data stored on the computer will be permanently erased,and the system will revert to
                                its original factory state when this operation is completed.</source>
        <translation type="vanished">将永久抹掉存储在计算机的所有数据，且无法撤销此操作。抹掉完成后系统将恢复至最初始出厂状态</translation>
    </message>
    <message>
        <source>All data stored on the computer will be permanently erased,and the system will revert to 
                                its original factory state when this operation is completed.</source>
        <translation type="vanished">كومپىيۇتېردا ساقلانغان بارلىق سانلىق مەلۇماتلار مەڭگۈلۈك ئۆچۈپ، سىستېما قايتىدىن 
                                بۇ مەشغۇلات تاماملانغاندا ئۇنىڭ ئەسلىدىكى زاۋۇت ھالىتى</translation>
    </message>
    <message>
        <source>Clear and restore</source>
        <translation type="vanished">ئايدىڭلاشتۇرۇش ۋە ئەسلىگە كەلتۈرۈش</translation>
        <extra-contents_path>/Backup/Clear and restore</extra-contents_path>
    </message>
    <message>
        <source>System Recovery</source>
        <translation type="vanished">سىستېما ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="45"/>
        <source>Backup Restore</source>
        <translation>زاپاس ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="121"/>
        <source>Back up your files to other drives and restore them when the source files are lost, damaged, or deleted to ensure the integrity of the system.</source>
        <translation>ھۆججەتلىرىڭىزنى باشقا قوزغاتقۇچلارغا زاپاسلاپ، مەنبە ھۆججەتلىرى يوقاپ كەتكەندە، بۇزۇلغاندا ياكى ئۆچۈرۈلگەندە سىستېمىنىڭ پۈتۈنلۈكىگە كاپالەتلىك قىلىڭ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="147"/>
        <source>View the backup list and restore the backup file to the system</source>
        <translation>زاپاس تىزىملىكنى كۆرۈش ۋە زاپاس ھۆججەتنى سىستېمىغا ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="105"/>
        <source>Backup and Restore</source>
        <translation>زاپاسلاش ۋە ئەسلىگە كەلتۈرۈش</translation>
        <extra-contents_path>/Backup/Backup and Restore</extra-contents_path>
    </message>
</context>
<context>
    <name>BindPhoneDialog</name>
    <message>
        <source>Your account here</source>
        <translation type="obsolete">请输入用户名</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="obsolete">输入密码</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">输入验证码</translation>
    </message>
    <message>
        <source>Get</source>
        <translation type="vanished">获取验证码</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">获取绑定手机验证码</translation>
    </message>
</context>
<context>
    <name>BiometricEnrollDialog</name>
    <message>
        <source>Biometrics </source>
        <translation type="vanished">生物识别</translation>
    </message>
    <message>
        <source>Continue to enroll </source>
        <translation type="vanished">继续录入</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation type="vanished">完成</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>Fingervein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">声纹</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="vanished">录入</translation>
    </message>
    <message>
        <source>Verify</source>
        <translation type="vanished">验证</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation type="vanished">需要授权，请先进行认证已继续操作</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="vanished">录入成功</translation>
    </message>
    <message>
        <source>Verify successfully</source>
        <translation type="vanished">验证成功</translation>
    </message>
    <message>
        <source>Not Match</source>
        <translation type="vanished">不匹配</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="vanished">D-Bus获取错误</translation>
    </message>
    <message>
        <source>Device is busy</source>
        <translation type="vanished">设备忙</translation>
    </message>
    <message>
        <source>No such device</source>
        <translation type="vanished">设备不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">权限不够</translation>
    </message>
</context>
<context>
    <name>BiometricMoreInfoDialog</name>
    <message>
        <source>Biometrics </source>
        <translation type="vanished">生物识别</translation>
    </message>
    <message>
        <source>Default device </source>
        <translation type="vanished">默认设备</translation>
    </message>
    <message>
        <source>Verify Type:</source>
        <translation type="vanished">验证类型：</translation>
    </message>
    <message>
        <source>Bus Type:</source>
        <translation type="vanished">总线类型：</translation>
    </message>
    <message>
        <source>Device Status:</source>
        <translation type="vanished">设备状态：</translation>
    </message>
    <message>
        <source>Storage Type:</source>
        <translation type="vanished">存储类型：</translation>
    </message>
    <message>
        <source>Identification Type:</source>
        <translation type="vanished">验证类型：</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="vanished">已连接</translation>
    </message>
    <message>
        <source>Unconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>Fingervein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">声纹</translation>
    </message>
    <message>
        <source>Hardware Verification</source>
        <translation type="vanished">硬件验证</translation>
    </message>
    <message>
        <source>Software Verification</source>
        <translation type="vanished">软件验证</translation>
    </message>
    <message>
        <source>Mix Verification</source>
        <translation type="vanished">混合验证</translation>
    </message>
    <message>
        <source>Other Verification</source>
        <translation type="vanished">其他验证</translation>
    </message>
    <message>
        <source>Device Storage</source>
        <translation type="vanished">设备存储</translation>
    </message>
    <message>
        <source>OS Storage</source>
        <translation type="vanished">系统存储</translation>
    </message>
    <message>
        <source>Mix Storage</source>
        <translation type="vanished">混合存储</translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="vanished">串口</translation>
    </message>
    <message>
        <source>USB</source>
        <translation type="vanished">USB</translation>
    </message>
    <message>
        <source>PCIE</source>
        <translation type="vanished">PCIE</translation>
    </message>
    <message>
        <source>Any</source>
        <translation type="vanished">任意类型</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="vanished">其他</translation>
    </message>
    <message>
        <source>Hardware Identification</source>
        <translation type="vanished">硬件识别</translation>
    </message>
    <message>
        <source>Software Identification</source>
        <translation type="vanished">软件识别</translation>
    </message>
    <message>
        <source>Mix Identification</source>
        <translation type="vanished">混合识别</translation>
    </message>
    <message>
        <source>Other Identification</source>
        <translation type="vanished">其他识别</translation>
    </message>
</context>
<context>
    <name>Biometrics</name>
    <message>
        <source>Biometrics</source>
        <translation type="vanished">生物特征与密码</translation>
    </message>
</context>
<context>
    <name>BiometricsWidget</name>
    <message>
        <source>Biometric password</source>
        <translation type="vanished">生物特征</translation>
    </message>
    <message>
        <source>Account password</source>
        <translation type="vanished">帐户密码</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Enable biometrics </source>
        <translation type="vanished">生物特征</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名</translation>
    </message>
    <message>
        <source>Add biometric feature</source>
        <translation type="vanished">添加生物密码</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation type="vanished">标准用户</translation>
    </message>
    <message>
        <source>Admin</source>
        <translation type="vanished">管理员</translation>
    </message>
    <message>
        <source>root</source>
        <translation type="vanished">Root</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>BlueToothMain</name>
    <message>
        <source>Turn off Bluetooth</source>
        <translation type="vanished">关闭蓝牙</translation>
    </message>
    <message>
        <source>Turn on Bluetooth</source>
        <translation type="vanished">开启蓝牙</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <source>Turn on :</source>
        <translation type="vanished">开启：</translation>
    </message>
    <message>
        <source>Bluetooth adapter</source>
        <translation type="vanished">蓝牙适配器</translation>
    </message>
    <message>
        <source>Show icon on taskbar</source>
        <translation type="vanished">在任务栏显示蓝牙图标</translation>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation type="vanished">可被附近的蓝牙设备发现</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">声音</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="obsolete">其他</translation>
    </message>
    <message>
        <source>Bluetooth adapter is not detected!</source>
        <translation type="vanished">未检测到蓝牙适配器！</translation>
    </message>
    <message>
        <source>Bluetooth adapter is abnormal !</source>
        <translation type="vanished">蓝牙适配器异常！</translation>
    </message>
    <message>
        <source>You can refer to the rfkill command for details.</source>
        <translation type="vanished">可查阅rfkill命令了解详情</translation>
    </message>
    <message>
        <source>Allow Bluetooth devices to be discoverable</source>
        <translation type="vanished">允许蓝牙设备可以被发现</translation>
    </message>
    <message>
        <source>Discoverable</source>
        <translation type="vanished">设备可见性</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="vanished">我的设备</translation>
    </message>
    <message>
        <source>Can now be found as </source>
        <translation type="vanished">现在可被发现为 </translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">蓝牙设备</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">刷新</translation>
    </message>
</context>
<context>
    <name>BlueToothMainWindow</name>
    <message>
        <source>Bluetooth adapter is abnormal !</source>
        <translation type="obsolete">蓝牙适配器异常！</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="obsolete">蓝牙</translation>
    </message>
    <message>
        <source>Turn on :</source>
        <translation type="obsolete">开启：</translation>
    </message>
    <message>
        <source>Show icon on taskbar</source>
        <translation type="obsolete">在任务栏显示蓝牙图标</translation>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation type="obsolete">可被附近的蓝牙设备发现</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="obsolete">我的设备</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="obsolete">蓝牙设备</translation>
    </message>
</context>
<context>
    <name>Bluetooth</name>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
</context>
<context>
    <name>BluetoothNameLabel</name>
    <message>
        <source>Can now be found as </source>
        <translation type="vanished">现在可被发现为 </translation>
    </message>
    <message>
        <source>Double-click to change the device name</source>
        <translation type="vanished">双击修改设备名称</translation>
    </message>
    <message>
        <source>Can now be found as &quot;%1&quot;</source>
        <translation type="vanished">现在可被发现为&quot;%1&quot;</translation>
    </message>
    <message>
        <source>Tip</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>The length of the device name does not exceed %1 characters !</source>
        <translation type="vanished">设备名称的长度不超过 %1 个字符！</translation>
    </message>
</context>
<context>
    <name>Boot</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="10"/>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="80"/>
        <source>Boot</source>
        <translation>ئۆتۈك</translation>
        <extra-contents_path>/Boot/Boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="92"/>
        <source>Grub verify</source>
        <translation>Grub دەلىللەش</translation>
        <extra-contents_path>/Boot/Grub verify</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="95"/>
        <source>Password required for Grub editing after enabling</source>
        <translation>Grub نى ئېچىش ئۈچۈن تەلەپ قىلىنىدىغان پارول</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="97"/>
        <source>Reset password</source>
        <translation>پارولنى قايتا بېكىتىش</translation>
    </message>
</context>
<context>
    <name>BrightnessFrame</name>
    <message>
        <location filename="../../../plugins/system/display/brightnessFrame.cpp" line="41"/>
        <source>Failed to get the brightness information of this monitor</source>
        <translation>بۇ كۆزەتكۈچىنىڭ يورۇقلۇق ئۇچۇرىغا ئېرىشەلمىدى</translation>
    </message>
</context>
<context>
    <name>CertificationDialog</name>
    <message>
        <source>UserCertification</source>
        <translation type="vanished">用户认证</translation>
    </message>
    <message>
        <source>User:</source>
        <translation type="vanished">用户名：</translation>
    </message>
    <message>
        <source>Passwd:</source>
        <translation type="vanished">密码：</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Certification</source>
        <translation type="vanished">认证</translation>
    </message>
</context>
<context>
    <name>ChangeFaceDialog</name>
    <message>
        <source>select custom face file</source>
        <translation type="vanished">选择自定义头像文件</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">选择</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">位置： </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">文件名： </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">文件类型： </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>The avatar is larger than 1M, please choose again</source>
        <translation type="vanished">警告，头像大于1M请重新选择</translation>
    </message>
    <message>
        <source>The avatar is larger than 2M, please choose again</source>
        <translation type="vanished">警告，头像大于2M请重新选择</translation>
    </message>
    <message>
        <source>Change User Face</source>
        <translation type="vanished">更改用户头像</translation>
    </message>
    <message>
        <source>System Icon</source>
        <translation type="vanished">系统头像</translation>
    </message>
    <message>
        <source>Select face from local</source>
        <translation type="vanished">从本地选择用户头像</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>ChangeFaceIntelDialog</name>
    <message>
        <source>Change User Face</source>
        <translation type="vanished">ئىشلەتكۈچى چىرايىنى ئۆزگەرتىش</translation>
        <extra-contents_path>/UserinfoIntel/Change User Face</extra-contents_path>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">تارىخ - مەدەنىيەت</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">سىستېما</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>select custom face file</source>
        <translation type="vanished">خاس يۈز ھۆججىتىنى تاللاش</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">تاللاش</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">ئورنى: </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">ھۆججەت نامى: </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">FileType: </translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">دىققەت</translation>
    </message>
    <message>
        <source>The avatar is larger than 2M, please choose again</source>
        <translation type="vanished">باش سۈرەت 2M دىن چوڭ، قايتا تاللاڭ</translation>
    </message>
</context>
<context>
    <name>ChangeFeatureName</name>
    <message>
        <source>Change Username</source>
        <translation type="vanished">修改用户名</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="vanished">特征名称</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Name already in use, change another one.</source>
        <translation type="vanished">该用户名已存在，请更改。</translation>
    </message>
</context>
<context>
    <name>ChangeGroupDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>User Group Settings</source>
        <translation type="vanished">ئىشلەتكۈچى گۇرۇپپا تەڭشەكلىرى</translation>
    </message>
    <message>
        <source>User groups available in the system</source>
        <translation type="vanished">系统中可用的用户组</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="obsolete">添加新用户</translation>
    </message>
    <message>
        <source>User group</source>
        <translation type="vanished">ئابونتلار توپى</translation>
    </message>
    <message>
        <source>Add user group</source>
        <translation type="vanished">ئابونتلار گۇرۇپپىسى قوشۇش</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation type="vanished">ئەسكەرتىش</translation>
    </message>
    <message>
        <source>Invalid Id!</source>
        <translation type="vanished">ئىناۋەتسىز كىملىك!</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">ماقۇل</translation>
    </message>
    <message>
        <source>Invalid Group Name!</source>
        <translation type="vanished">ئىناۋەتسىز توپ نامى!</translation>
    </message>
</context>
<context>
    <name>ChangeGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>User Group Settings</source>
        <translation type="vanished">ئىشلەتكۈچى گۇرۇپپا تەڭشەكلىرى</translation>
    </message>
    <message>
        <source>User groups available in the system</source>
        <translation type="vanished">سېستىمىدا بار ئىشلەتكۈچى گۇرۇپپىلىرى</translation>
    </message>
    <message>
        <source>Add user group</source>
        <translation type="vanished">ئابونتلار گۇرۇپپىسى قوشۇش</translation>
    </message>
</context>
<context>
    <name>ChangePhoneIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>changephone</source>
        <translation type="vanished">ئۆزگىرىشچان يانفون</translation>
    </message>
    <message>
        <source>Please input old phone num</source>
        <translation type="vanished">كونا تېلېفون num نى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">TextLabel</translation>
    </message>
    <message>
        <source>GetVerifyCode</source>
        <translation type="vanished">GetVerifyCode</translation>
    </message>
    <message>
        <source>submit</source>
        <translation type="vanished">تاپشۇرماق</translation>
    </message>
    <message>
        <source>Change Phone</source>
        <translation type="vanished">تېلېفون ئالماشتۇرۇش</translation>
    </message>
    <message>
        <source>Phone number</source>
        <translation type="vanished">تېلېفۇن نۇمۇرى</translation>
    </message>
    <message>
        <source>SMS verification code</source>
        <translation type="vanished">SMS دەلىللەش كودى</translation>
    </message>
    <message>
        <source>Please input old phone number</source>
        <translation type="vanished">كونا تېلېفون نومۇرىنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">كېيىنكى</translation>
    </message>
    <message>
        <source>Please enter new mobile number</source>
        <translation type="vanished">يېڭى يانفون نومۇرىنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <source>Submit</source>
        <translation type="vanished">تاپشۇرماق</translation>
    </message>
    <message>
        <source>changed success</source>
        <translation type="vanished">مۇۋەپپىقىيەتنى ئۆزگەرتتى</translation>
    </message>
    <message>
        <source>You have successfully modified your phone</source>
        <translation type="vanished">يانفونىڭىزنى مۇۋەپپەقىيەتلىك ئۆزگەرتتىڭىز</translation>
    </message>
    <message>
        <source>Recapture</source>
        <translation type="vanished">قايتا تۇتۇلۇش</translation>
    </message>
    <message>
        <source>Network connection failure, please check</source>
        <translation type="vanished">تور ئۇلاشتا كاشىلا كۆرۈلسە، تەكشۈرۈپ كۆرۈڭ</translation>
    </message>
    <message>
        <source>GetCode</source>
        <translation type="vanished">GetCode</translation>
    </message>
    <message>
        <source>Phone is lock,try again in an hour</source>
        <translation type="vanished">تېلېفون قۇلۇپلۇق، بىر سائەتتىن كېيىن قايتا سىناپ بېقىڭ</translation>
    </message>
    <message>
        <source>Phone code is wrong</source>
        <translation type="vanished">تېلېفون نومۇرى خاتا</translation>
    </message>
    <message>
        <source>Current login expired,using wechat code!</source>
        <translation type="vanished">نۆۋەتتىكى كىرىش ۋاقتى ئۆتۈپ كەتتى،ئۈندىدار كودى ئارقىلىق!</translation>
    </message>
    <message>
        <source>Unknown error, please try again later</source>
        <translation type="vanished">نامەلۇم خاتالىق، سەل تۇرۇپ قايتا سىناپ بېقىڭ</translation>
    </message>
    <message>
        <source>Phone can not same</source>
        <translation type="vanished">تېلېفون بىلەن ئوخشاش بولمايدۇ</translation>
    </message>
    <message>
        <source>finished</source>
        <translation type="vanished">تۈگىدى</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">تېلېفون نومۇرى بۇرۇن ئىشلىتىلىپ بولغان!</translation>
    </message>
</context>
<context>
    <name>ChangePinIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>Change Password</source>
        <translation type="vanished">پارولنى ئۆزگەرتىش</translation>
    </message>
</context>
<context>
    <name>ChangeProjectionName</name>
    <message>
        <source>Name is too long, change another one.</source>
        <translation type="vanished">名称过长，请更改</translation>
    </message>
    <message>
        <source>Change Username</source>
        <translation type="vanished">修改用户名</translation>
    </message>
    <message>
        <source>Changename</source>
        <translation type="vanished">修改名称</translation>
    </message>
    <message>
        <source>ChangeProjectionname</source>
        <translation type="vanished">设备名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">更改密码</translation>
    </message>
    <message>
        <source>Pwd type</source>
        <translation type="vanished">密码类型</translation>
    </message>
    <message>
        <source>Cur pwd</source>
        <translation type="vanished">当前密码</translation>
    </message>
    <message>
        <source>New pwd</source>
        <translation type="vanished">新密码</translation>
    </message>
    <message>
        <source>New pwd sure</source>
        <translation type="vanished">新密码确认</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="vanished">更改密码</translation>
    </message>
    <message>
        <source>Cur pwd checking!</source>
        <translation type="vanished">当前密码检查!</translation>
    </message>
    <message>
        <source>General Pwd</source>
        <translation type="vanished">通用密码</translation>
    </message>
    <message>
        <source>Current Password</source>
        <translation type="vanished">当前密码</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation type="vanished">新密码</translation>
    </message>
    <message>
        <source>New Password Identify</source>
        <translation type="vanished">新密码确认</translation>
    </message>
    <message>
        <source>Authentication failed, input authtok again!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Pwd input error, re-enter!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Contains illegal characters!</source>
        <translation type="vanished">含有非法字符！</translation>
    </message>
    <message>
        <source>Same with old pwd</source>
        <translation type="vanished">与旧密码相同</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">密码长度至少大于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">密码长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to more than 5 character!</source>
        <translation type="vanished">密码长度需要大于5个字符！</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">与新密码不同</translation>
    </message>
</context>
<context>
    <name>ChangePwdIntelDialog</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">Pwd نى ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>General Pwd</source>
        <translation type="vanished">ارينە پۋد</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation type="vanished">كونا پارول</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation type="vanished">يېڭى پارول</translation>
    </message>
    <message>
        <source>New Password Identify</source>
        <translation type="vanished">يېڭى پارول بېكىتىش</translation>
    </message>
    <message>
        <source>Please set different pwd!</source>
        <translation type="vanished">ئوخشاش بولمىغان pwd نى تەڭشەڭ!</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">pwd بىلەن ماس كەلمەسلىك</translation>
    </message>
    <message>
        <source>Old pwd is wrong!</source>
        <translation type="vanished">كونا پ ي د خاتا!</translation>
    </message>
    <message>
        <source>New pwd is too similar with old pwd!</source>
        <translation type="vanished">يېڭى PWD نى كونا PWD بىلەن بەك ئوخشىشىپ كىتىپتۇ!</translation>
    </message>
    <message>
        <source>Check old pwd failed because of unknown reason!</source>
        <translation type="vanished">سەۋەبى نامەلۇم بولغاچقا كونا pwd نىڭ مەغلۇپ بولغانلىقىنى تەكشۈرۈڭ!</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">پارول ئۇزۇنلۇقى ٪1 تىن يۇقىرى ھەرپكە ئېھتىياجلىق!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">پارولنىڭ ئۇزۇنلۇقى ٪1 تىن تۆۋەن بولۇش كېرەك!</translation>
    </message>
    <message>
        <source>Password cannot be made up entirely by Numbers!</source>
        <translation type="vanished">پارولنى پۈتۈنلەي سانلار ئارقىلىق تولۇقلاشقا بولمايدۇ!</translation>
    </message>
</context>
<context>
    <name>ChangeTypeDialog</name>
    <message>
        <source>Make sure that there is at least one administrator on the computer</source>
        <translation type="vanished">请确保该计算机上至少有一个管理员用户</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install software and change system settings</source>
        <translation type="vanished">标准帐户可以使用大多数软件，但是不能安装软件和更改系统配置</translation>
    </message>
    <message>
        <source>Change Account Type</source>
        <translation type="vanished">更改用户类型</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">标准用户</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot change system settings</source>
        <translation type="vanished">标准帐户可以使用大多数软件，但是不能修改系统配置</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">管理员用户</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">管理员帐户可以更改任何系统配置，包括安装软件和升级软件</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Change type</source>
        <translation type="vanished">更改类型</translation>
    </message>
</context>
<context>
    <name>ChangeTypeIntelDialog</name>
    <message>
        <source>Change Account Type</source>
        <translation type="vanished">ھېسابات تۈرىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ئۆلچەملىك ئىشلەتكۈچى</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install software and change system settings</source>
        <translation type="vanished">ئۆلچەملىك ئىشلەتكۈچىلەر كۆپ قىسىم يۇمشاق دېتاللارنى ئىشلىتەلەيدۇ، ئەمما يۇمشاق دېتال قاچىلىيالمايدۇ ۋە سىستېما تەڭشەكلىرىنى ئۆزگەرتەلمەيدۇ</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">باشقۇرغۇچى</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">باشقۇرغۇچىلار ئۆزلىرى ئېھتىياجلىق بولغان ھەرقانداق ئۆزگەرتىشنى ئېلىپ بارالايدۇ</translation>
    </message>
    <message>
        <source>Make sure that there is at least one administrator on the computer</source>
        <translation type="vanished">كومپيۇتېردا ئاز دېگەندە بىر باشقۇرغۇچىنىڭ بارلىقىغا كاپالەتلىك قىلىش كېرەك</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>ChangeUserLogo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="134"/>
        <source>System Logos</source>
        <translation>سىستېما Logos</translation>
    </message>
    <message>
        <source>Local Logo</source>
        <translation type="vanished">本地头像</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="111"/>
        <source>User logo</source>
        <translation>ئىشلەتكۈچى logoى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="143"/>
        <source>Select Local Logo</source>
        <translation>يەرلىك تۇغنى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="152"/>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="296"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="154"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="291"/>
        <source>select custom face file</source>
        <translation>خاس يۈز ھۆججىتىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="292"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="293"/>
        <source>Position: </source>
        <translation>ئورنى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="294"/>
        <source>FileName: </source>
        <translation>ھۆججەت نامى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="295"/>
        <source>FileType: </source>
        <translation>FileType: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="311"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="312"/>
        <source>The avatar is larger than 1M, please choose again</source>
        <translation>باش سۈرەت 1M دىن چوڭ، قايتا تاللاڭ</translation>
    </message>
</context>
<context>
    <name>ChangeUserName</name>
    <message>
        <source>Change Username</source>
        <translation type="vanished">修改用户名</translation>
    </message>
    <message>
        <source>NickName</source>
        <translation type="vanished">用户昵称</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>ComName</source>
        <translation type="vanished">计算机名</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Name already in use, change another one.</source>
        <translation type="vanished">该用户名已存在，请更改。</translation>
    </message>
</context>
<context>
    <name>ChangeUserNickname</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="31"/>
        <source>Set Nickname</source>
        <translation>تەخەللۇس بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="53"/>
        <source>UserName</source>
        <translation>ئىشلەتكۈچى نامى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="70"/>
        <source>NickName</source>
        <translation>تەخەللۇسلار</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="156"/>
        <source>NickName&apos;s length must between 1~%1 characters!</source>
        <translation>لەقەمنىڭ ئۇزۇنلۇقى چوقۇم 1 ~٪ 1 ھەرپلەر ئارىسىدا بولۇشى كېرەك!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="158"/>
        <source>nickName already in use.</source>
        <translation>تور نامى ئىشلىتىلىپ بولغان.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="160"/>
        <source>Can&apos;t contains &apos;:&apos;.</source>
        <translation>&apos;:&apos; نى ئۆز ئىچىگە ئالالمايدۇ.</translation>
    </message>
    <message>
        <source>The length must be 1~%1 characters!</source>
        <translation type="vanished">ئۇزۇنلۇقى چوقۇم 1 ~٪ 1 ھەرپ بولۇشى كېرەك!</translation>
    </message>
    <message>
        <source>nickName length must less than %1 letters!</source>
        <translation type="vanished">用户昵称长度必须小于%1！</translation>
    </message>
    <message>
        <source>Name already in use, change another one.</source>
        <translation type="vanished">该用户名已存在，请更改。</translation>
    </message>
    <message>
        <source>ComputerName</source>
        <translation type="vanished">计算机名</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="113"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="116"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="98"/>
        <source>Change password</source>
        <translation>پارولنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="103"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="470"/>
        <source>Current Pwd</source>
        <translation>نۆۋەتتىكى Pwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="116"/>
        <source>Required</source>
        <translation>تەلەپ قىلىنىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="136"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="471"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="479"/>
        <source>New Pwd</source>
        <translation>يېڭى پ ك ك</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="163"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="472"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="480"/>
        <source>Sure Pwd</source>
        <translation>ئەلۋەتتە Pwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="218"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="303"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="370"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="279"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="543"/>
        <source>Inconsistency with pwd</source>
        <translation>pwd بىلەن ماس كەلمەسلىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="299"/>
        <source>Same with old pwd</source>
        <translation>كونا پ ي د بىلەن ئوخشاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="353"/>
        <source>Pwd Changed Succes</source>
        <translation>Pwd ئۆزگەرتىلگەن سۇكېس</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="360"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>دەلىللەش مەغلۇپ بولدى، authtok نى قايتا كىرگۈزۈڭ!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="502"/>
        <source>Contains illegal characters!</source>
        <translation>قانۇنسىز شەخسلەرنى ئۆز ئىچىگە ئالىدۇ!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="614"/>
        <source>current pwd cannot be empty!</source>
        <translation>نۆۋەتتىكى pwd نى بىكار قىلغىلى بولمايدۇ!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="619"/>
        <source>new pwd cannot be empty!</source>
        <translation>يېڭى pwd قۇرۇق بولالمايدۇ!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="624"/>
        <source>sure pwd cannot be empty!</source>
        <translation>pwd چوقۇم قۇرۇق بولمايدۇ!</translation>
    </message>
</context>
<context>
    <name>ChangeUserType</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="24"/>
        <source>UserType</source>
        <translation>UserType</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="71"/>
        <source>administrator</source>
        <translation>باشقۇرغۇچى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="73"/>
        <source>standard user</source>
        <translation>ئۆلچەملىك ئىشلەتكۈچى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="61"/>
        <source>Select account type (Ensure have admin on system):</source>
        <translation>ھېسابات تۈرىنى تاللاڭ (سىستېمىدا admin بولۇشىغا كاپالەتلىك قىلىش):</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="75"/>
        <source>change system settings, install and upgrade software.</source>
        <translation>سىستېما تەڭشەكلىرىنى ئۆزگەرتىش، قاچىلاش ۋە يېڭىلاش يۇمشاق دېتالى.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="77"/>
        <source>use most software, cannot change system settings.</source>
        <translation>كۆپ قىسىم يۇمشاق دېتاللارنى ئىشلىتىدۇ، سىستېما تەڭشەكلىرىنى ئۆزگەرتەلمەيدۇ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="130"/>
        <source>Note: Effective After Logout!!!</source>
        <translation>ئەسكەرتىش: چېكىنگەندىن كېيىن ئۈنۈملۈك!!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="141"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="144"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>ChangeVaildDialog</name>
    <message>
        <source>Password Validity Setting</source>
        <translation type="vanished">密码有效期设置</translation>
    </message>
    <message>
        <source>Current passwd validity:</source>
        <translation type="vanished">当前密码有效期至:</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="vanished">调整有效期至:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>ChangeValidDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">更改时间</translation>
    </message>
    <message>
        <source>Password Validity Setting</source>
        <translation type="vanished">密码有效期设置</translation>
    </message>
    <message>
        <source>Current passwd validity:</source>
        <translation type="vanished">当前密码有效期至:</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="vanished">调整有效期至:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Change valid</source>
        <translation type="vanished">密码时效</translation>
    </message>
</context>
<context>
    <name>ChangeValidIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>Password Validity Setting</source>
        <translation type="vanished">پارولنىڭ كۈچكە ئىگە بولۇش تەڭشىكى</translation>
    </message>
    <message>
        <source>Current passwd validity:</source>
        <translation type="vanished">نۆۋەتتىكى يوللانمىنىڭ كۈچكە ئىگە ۋاقتى:</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="vanished">چېسلانى تەڭشەپ:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">چۇقۇم</translation>
    </message>
</context>
<context>
    <name>ChangtimeDialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="161"/>
        <source>day</source>
        <translation>كۈن</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="158"/>
        <source>time</source>
        <translation>ۋاقتىدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="159"/>
        <source>year</source>
        <translation>يىل</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="160"/>
        <source>month</source>
        <translation>ئاي</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <source>选择自定义颜色</source>
        <translation type="vanished">选择自定义颜色</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="86"/>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="46"/>
        <source>Choose a custom color</source>
        <translation>خاس رەڭ تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="254"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="290"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="457"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="476"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="50"/>
        <source>Custom color</source>
        <translation>خاس رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="138"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>CreateGroupDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.ui" line="26"/>
        <source>Add New Group</source>
        <translation>يېڭى گۇرۇپپا قوشۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="51"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="75"/>
        <source>Id</source>
        <translation>كىملىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="89"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="199"/>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="213"/>
        <source>GroupName&apos;s length must be between 1 and %1 characters!</source>
        <translation>GroupName نىڭ ئۇزۇنلۇقى چوقۇم ٪1 بىلەن ٪1 ھەرپ ئارىلىقىدا بولۇشى كېرەك!</translation>
    </message>
    <message>
        <source>Members</source>
        <translation type="vanished">ئەزالار</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="vanished">组名</translation>
    </message>
    <message>
        <source>Group Id</source>
        <translation type="vanished">组ID</translation>
    </message>
    <message>
        <source>Group Members</source>
        <translation type="vanished">组成员</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="86"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">چۇقۇم</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="47"/>
        <source>Add user group</source>
        <translation>ئابونتلار توپىنى قوشۇش</translation>
    </message>
</context>
<context>
    <name>CreateGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>Add New Group</source>
        <translation type="vanished">يېڭى گۇرۇپپا قوشۇش</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="vanished">گۇرۇپپا ئىسمى</translation>
    </message>
    <message>
        <source>Group Id</source>
        <translation type="vanished">گۇرۇپپا كىملىكى</translation>
    </message>
    <message>
        <source>Group Members</source>
        <translation type="vanished">گۇرۇپپا ئەزالىرى</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">چۇقۇم</translation>
    </message>
</context>
<context>
    <name>CreateUserDialog</name>
    <message>
        <source>UserName</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>ComName</source>
        <translation type="vanished">计算机名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <source>Account Type</source>
        <translation type="vanished">用户类型</translation>
    </message>
    <message>
        <source>Add New Account</source>
        <translation type="vanished">添加新用户</translation>
    </message>
    <message>
        <source>PwdType</source>
        <translation type="vanished">密码类型</translation>
    </message>
    <message>
        <source>PasswordSure</source>
        <translation type="vanished">确认密码</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">标准用户</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot change system settings</source>
        <translation type="vanished">标准帐户可以使用大多数软件，但是不能修改系统配置</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install the software and
change system settings</source>
        <translation type="vanished">标准帐户可以使用大多数软件，但是不能安装软件和更改系统配置</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">管理员用户</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">管理员帐户可以更改任何系统配置，包括安装软件和升级软件</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Password Identify</source>
        <translation type="vanished">密码确认</translation>
    </message>
    <message>
        <source>General Password</source>
        <translation type="vanished">通用密码</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">和密码不一致</translation>
    </message>
    <message>
        <source>Must be begin with lower letters!</source>
        <translation type="vanished">用户名必须以小写字母开始！</translation>
    </message>
    <message>
        <source>Can not contain capital letters!</source>
        <translation type="vanished">用户名不能包含大写字母！</translation>
    </message>
    <message>
        <source>Name already in use, change another one.</source>
        <translation type="vanished">该用户名已存在，请更改。</translation>
    </message>
    <message>
        <source>Name corresponds to group already exists.</source>
        <translation type="vanished">用户名对应组已存在，请更改。</translation>
    </message>
    <message>
        <source>Name length must less than %1 letters!</source>
        <translation type="vanished">用户名长度必须小于%1！</translation>
    </message>
    <message>
        <source>Can only contain letters,digits,underline!</source>
        <translation type="vanished">用户名仅能包含字母，数字及下划线</translation>
    </message>
    <message>
        <source>Username&apos;s folder exists, change another one</source>
        <translation type="vanished">用户目录已存在，请更改</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">密码长度至少大于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">密码长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="vanished">添加新用户</translation>
    </message>
    <message>
        <source>Password cannot be made up entirely by Numbers!</source>
        <translation type="obsolete">密码复杂度过低！</translation>
    </message>
    <message>
        <source>Contains illegal characters!</source>
        <translation type="vanished">含有非法字符！</translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="vanished">用户名不能为空</translation>
    </message>
    <message>
        <source>The first character must be lowercase letters!</source>
        <translation type="vanished">首字符必须为小写字符！</translation>
    </message>
    <message>
        <source>User name can not contain capital letters!</source>
        <translation type="vanished">用户名不能包含大写字符！</translation>
    </message>
    <message>
        <source>The user name is already in use, please use a different one.</source>
        <translation type="vanished">用户名已存在，请换用其他用户名。</translation>
    </message>
    <message>
        <source>The name corresponds to the group already exists.</source>
        <translation type="vanished">用户名对应组已存在，请更换用户名.</translation>
    </message>
    <message>
        <source>User name length need to less than %1 letters!</source>
        <translation type="vanished">用户名长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>The user name can only be composed of letters, numbers and underline!</source>
        <translation type="vanished">用户名只能由字母、数字以及下划线组成！</translation>
    </message>
    <message>
        <source>The username is configured, please change the username</source>
        <translation type="vanished">用户配置已存在，请更换用户名</translation>
    </message>
</context>
<context>
    <name>CreateUserIntelDialog</name>
    <message>
        <source>Add New Account</source>
        <translation type="vanished">يېڭى ھېساب قوشۇش</translation>
    </message>
    <message>
        <source>Account Type</source>
        <translation type="vanished">ھېسابات تۈرى</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ئۆلچەملىك ئىشلەتكۈچى</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install the software and
change system settings</source>
        <translation type="vanished">标准账户可以使用大多数软件，但是不能安装软件和更改系统配置</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install the software and 
change system settings</source>
        <translation type="vanished">ئۆلچەملىك ئىشلەتكۈچىلەر كۆپ قىسىم يۇمشاق دېتاللارنى ئىشلىتەلەيدۇ، ئەمما دېتالنى قاچىلىيالمايدۇ ۋە 
سىستېما تەڭشەكلىرىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">باشقۇرغۇچى</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">باشقۇرغۇچىلار ئۆزلىرى ئېھتىياجلىق بولغان ھەرقانداق ئۆزگەرتىشنى ئېلىپ بارالايدۇ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="vanished">ئىشلەتكۈچى نامى</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ئىم</translation>
    </message>
    <message>
        <source>Password Identify</source>
        <translation type="vanished">مەخپىي نومۇرنى پەرقلەندۈرۈش</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">pwd بىلەن ماس كەلمەسلىك</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">پارول ئۇزۇنلۇقى ٪1 تىن يۇقىرى ھەرپكە ئېھتىياجلىق!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">پارولنىڭ ئۇزۇنلۇقى ٪1 تىن تۆۋەن بولۇش كېرەك!</translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="vanished">ئىشلەتكۈچى نامىنى بوش قويۇشقا بولمايدۇ</translation>
    </message>
    <message>
        <source>The first character must be lowercase letters!</source>
        <translation type="vanished">بىرىنچى پېرسوناژ چوقۇم كىچىك ھەرپلەردىن بولۇشى كېرەك!</translation>
    </message>
    <message>
        <source>User name can not contain capital letters!</source>
        <translation type="vanished">ئىشلەتكۈچى نامى چوڭ ھەرپلەرنى ئۆز ئىچىگە ئالالمايدۇ!</translation>
    </message>
    <message>
        <source>The user name is already in use, please use a different one.</source>
        <translation type="vanished">ئىشلەتكۈچى نامىنى ئىشلىتىشكە باشلىدى، باشقا بىرنى ئىشلىتىڭ.</translation>
    </message>
    <message>
        <source>User name length need to less than %1 letters!</source>
        <translation type="vanished">ئىشلەتكۈچى نامىنىڭ ئۇزۇنلۇقى ٪1 ھەرپتىن ئاز بولۇش كېرەك!</translation>
    </message>
    <message>
        <source>The user name can only be composed of letters, numbers and underline!</source>
        <translation type="vanished">ئىشلەتكۈچى نامىنى پەقەت ھەرپ، سان ۋە ئاستىدىن تەشكىل تاپقىلى بولىدۇ!</translation>
    </message>
    <message>
        <source>The username is configured, please change the username</source>
        <translation type="vanished">ئىشلەتكۈچى نامى تەڭشىلىپتۇ، ئىشلەتكۈچى نامىنى ئۆزگەرتىڭ</translation>
    </message>
</context>
<context>
    <name>CreateUserNew</name>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="49"/>
        <source>CreateUserNew</source>
        <translation>CreateUserNew</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="55"/>
        <source>UserName</source>
        <translation>ئىشلەتكۈچى نامى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="65"/>
        <source>NickName</source>
        <translation>تەخەللۇسلار</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="71"/>
        <source>HostName</source>
        <translation>HostName</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="80"/>
        <source>Pwd</source>
        <translation>Pwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="88"/>
        <source>SurePwd</source>
        <translation>SurePwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="96"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="99"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="102"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="105"/>
        <source>Required</source>
        <translation>تەلەپ قىلىنىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="109"/>
        <source>verification</source>
        <translation>دەلىللەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="182"/>
        <source>Select Type</source>
        <translation>تۈرىنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="191"/>
        <source>Administrator</source>
        <translation>باشقۇرغۇچى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="194"/>
        <source>Users can make any changes they need</source>
        <translation>ئابونتلار ئۆزلىرى ئېھتىياجلىق بولغان ھەرقانداق ئۆزگەرتىشنى ئېلىپ بارالايدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="196"/>
        <source>Standard User</source>
        <translation>ئۆلچەملىك ئىشلەتكۈچى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="199"/>
        <source>Users cannot change system settings</source>
        <translation>ئىشلەتكۈچىلەر سىستېما تەڭشەكلىرىنى ئۆزگەرتەلمەيدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="276"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="279"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="355"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="581"/>
        <source>Inconsistency with pwd</source>
        <translation>pwd بىلەن ماس كەلمەسلىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="494"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="647"/>
        <source>NickName&apos;s length must be between 1 and %1 characters!</source>
        <translation>تور نامىنىڭ ئۇزۇنلۇقى چوقۇم 1-٪1 ھەرپ ئارىلىقىدا بولۇشى كېرەك!</translation>
    </message>
    <message>
        <source>The nick name cannot be empty</source>
        <translation type="vanished">تەخەللۇس نامىنى بوش قويۇشقا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="496"/>
        <source>nickName already in use.</source>
        <translation>تور نامى ئىشلىتىلىپ بولغان.</translation>
    </message>
    <message>
        <source>nickName length must less than %1 letters!</source>
        <translation type="vanished">تور نامىنىڭ ئۇزۇنلۇقى چوقۇم ٪1 ھەرپتىن ئاز بولۇشى كېرەك!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="642"/>
        <source>Username&apos;s length must be between 1 and %1 characters!</source>
        <translation>ئىشلەتكۈچى نامىنىڭ ئۇزۇنلۇقى چوقۇم ٪1 بىلەن ٪1 ھەرپ ئارىلىقىدا بولۇشى كېرەك!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="652"/>
        <source>new pwd cannot be empty!</source>
        <translation>يېڭى pwd قۇرۇق بولالمايدۇ!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="657"/>
        <source>sure pwd cannot be empty!</source>
        <translation>pwd چوقۇم قۇرۇق بولمايدۇ!</translation>
    </message>
    <message>
        <source>Name already in use.</source>
        <translation type="vanished">该用户名已存在。</translation>
    </message>
    <message>
        <source>Nickname cannot same with username</source>
        <translation type="vanished">用户昵称和用户名不能相同</translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="vanished">用户名不能为空</translation>
    </message>
    <message>
        <source>Must be begin with lower letters!</source>
        <translation type="vanished">用户名必须以小写字母开始！</translation>
    </message>
    <message>
        <source>Can not contain capital letters!</source>
        <translation type="vanished">用户名不能包含大写字母！</translation>
    </message>
    <message>
        <source>Name already in use, change another one.</source>
        <translation type="vanished">该用户名已存在，请更改。</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="526"/>
        <source>Name corresponds to group already exists.</source>
        <translation>ئىسىم ئاللىقاچان مەۋجۇت بولۇپ بولغان توپقا ماس كېلىدىغان .</translation>
    </message>
    <message>
        <source>Name length must less than %1 letters!</source>
        <translation type="vanished">用户名长度必须小于%1！</translation>
    </message>
    <message>
        <source>Can only contain letters,digits,underline!</source>
        <translation type="vanished">用户名仅能包含字母，数字及下划线</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="522"/>
        <source>Username&apos;s folder exists, change another one</source>
        <translation>ئىشلەتكۈچى نامىنىڭ ھۆججەت قىسقۇچى مەۋجۇت، باشقا بىرسىنى ئالماشتۇر</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="551"/>
        <source>Contains illegal characters!</source>
        <translation>قانۇنسىز شەخسلەرنى ئۆز ئىچىگە ئالىدۇ!</translation>
    </message>
</context>
<context>
    <name>CustomGlobalTheme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/globaltheme/customglobaltheme.cpp" line="38"/>
        <source>custom</source>
        <translation>ئۆرپ-ئادەت</translation>
    </message>
</context>
<context>
    <name>CustomLineEdit</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/customlineedit.cpp" line="28"/>
        <source>New Shortcut...</source>
        <translation>يېڭى قىسقا يول...</translation>
    </message>
</context>
<context>
    <name>DataFormat</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">更改时间</translation>
    </message>
    <message>
        <source>change format of data</source>
        <translation type="vanished">更改数据格式</translation>
    </message>
    <message>
        <source>calendar</source>
        <translation type="vanished">日历</translation>
    </message>
    <message>
        <source>first day</source>
        <translation type="vanished">一周第一天</translation>
    </message>
    <message>
        <source>date</source>
        <translation type="vanished">日期</translation>
    </message>
    <message>
        <source>time</source>
        <translation type="vanished">时间</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>first day of week</source>
        <translation type="vanished">一周的第一天</translation>
    </message>
    <message>
        <source>lunar</source>
        <translation type="vanished">农历</translation>
    </message>
    <message>
        <source>solar calendar</source>
        <translation type="vanished">公历</translation>
    </message>
    <message>
        <source>monday</source>
        <translation type="vanished">星期一</translation>
    </message>
    <message>
        <source>sunday</source>
        <translation type="vanished">星期日</translation>
    </message>
</context>
<context>
    <name>DateTime</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="26"/>
        <source>DateTime</source>
        <translation>چېسلا ۋاقتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="65"/>
        <source>current date</source>
        <translation>نۆۋەتتىكى چېسلا</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="321"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="275"/>
        <source>Change timezone</source>
        <translation>ۋاقىت رايونىنى ئۆزگەرتىش</translation>
        <extra-contents_path>/Date/Change time zone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="444"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="620"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="481"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="510"/>
        <source>RadioButton</source>
        <translation>RadioButton</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="712"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="744"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="952"/>
        <source>titleLabel</source>
        <translation>titleLabel</translation>
    </message>
    <message>
        <source>timezone</source>
        <translation type="vanished">时区</translation>
    </message>
    <message>
        <source>Sync system time</source>
        <translation type="vanished">同步系统时间</translation>
    </message>
    <message>
        <source>Sync from network successful</source>
        <translation type="vanished">时间同步成功</translation>
    </message>
    <message>
        <source>Sync from network failed</source>
        <translation type="vanished">时间同步失败</translation>
    </message>
    <message>
        <source>Change time</source>
        <translation type="vanished">手动更改时间</translation>
    </message>
    <message>
        <source>Change time zone</source>
        <translation type="vanished">更改时区</translation>
    </message>
    <message>
        <source>Sync complete</source>
        <translation type="vanished">同步完成</translation>
    </message>
    <message>
        <source>datetime</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>Datetime</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>Dat</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="94"/>
        <source>Date</source>
        <translation>چېسلا</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="175"/>
        <source>Current Date</source>
        <translation>نۆۋەتتىكى ۋاقتى</translation>
        <extra-contents_path>/Date/Current Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="178"/>
        <source>Other Timezone</source>
        <translation>باشقا ۋاقىت رايونى</translation>
        <extra-contents_path>/Date/Other Timezone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="196"/>
        <source>24-hour clock</source>
        <translation>24 سائەتلىك سائەت</translation>
    </message>
    <message>
        <source>Sync from network</source>
        <translation type="obsolete">同步时间</translation>
    </message>
    <message>
        <source>Add time zones to display the time, up to 5 can be added</source>
        <translation type="vanished">添加时区以显示时间，最多可以添加5个</translation>
    </message>
    <message>
        <source>Add time zones to display the time,only 5 can be added</source>
        <translation type="vanished">添加时区，最多添加５个</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="461"/>
        <source>Sync Server</source>
        <translation>ماس قەدەمدە مۇلازىمىتېر</translation>
        <extra-contents_path>/Date/Sync Server</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="650"/>
        <source>Add Timezone</source>
        <translation>ۋاقىت رايونى قوشۇش</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="289"/>
        <source>Manual Time</source>
        <translation>قوللانما ۋاقتى</translation>
        <extra-contents_path>/Date/Manual Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="286"/>
        <source>Sync Time</source>
        <translation>ماس قەدەم ۋاقتى</translation>
        <extra-contents_path>/Date/Sync Time</extra-contents_path>
    </message>
    <message>
        <source>Auto Sync Time</source>
        <translation type="vanished">自动同步时间</translation>
        <extra-contents_path>/Date/Auto Sync Time</extra-contents_path>
    </message>
    <message>
        <source>Time Server</source>
        <translation type="vanished">服务器</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="obsolete">网络</translation>
        <extra-contents_path>/Date/Network</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="198"/>
        <source>Set Time</source>
        <translation>بەلگىلەنگەن ۋاقىت</translation>
        <extra-contents_path>/Date/Set Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="226"/>
        <source>Set Date Manually</source>
        <translation>ۋاقتىنى قولدا بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="375"/>
        <source>Add</source>
        <translation>قوش</translation>
        <extra-contents_path>/Date/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="463"/>
        <source>Default</source>
        <translation>كۆڭۈلدىكى سۆز</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="465"/>
        <source>Customize</source>
        <translation>خاسلاشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="475"/>
        <source>Server Address</source>
        <translation>مۇلازىمىتېر ئادرېسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="480"/>
        <source>Required</source>
        <translation>تەلەپ قىلىنىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="481"/>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="652"/>
        <source>Change Timezone</source>
        <translation>ۋاقىت رايونىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="772"/>
        <source>MMMM d, yy ddd</source>
        <translation>MMMM d, yy ddd</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="774"/>
        <source>MMMM dd, yyyy ddd</source>
        <translation>MMMM dd, yyyy ddd</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="932"/>
        <source>  </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="933"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="942"/>
        <source>Sync Failed</source>
        <translation>ماس قەدەمدە مەغلۇپ بولدى</translation>
    </message>
    <message>
        <source>AM </source>
        <translation type="vanished">上午</translation>
    </message>
    <message>
        <source>PM </source>
        <translation type="vanished">下午</translation>
    </message>
    <message>
        <source>Sync network time</source>
        <translation type="vanished">同步网络时间</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="640"/>
        <source>change time</source>
        <translation>ئۆزگەرتىش ۋاقتى</translation>
    </message>
</context>
<context>
    <name>DefaultApp</name>
    <message>
        <source>defaultapp</source>
        <translation type="vanished">默认应用</translation>
    </message>
    <message>
        <source>Defaultapp</source>
        <translation type="vanished">默认应用</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="43"/>
        <source>Default App</source>
        <translation>كۆڭۈلدىكى ئەپ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="67"/>
        <source>No program available</source>
        <translation>پروگرامما يوق</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="68"/>
        <source>Choose default app</source>
        <translation>كۆڭۈلدىكى ئەپنى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="252"/>
        <source>Reset default apps to system recommended apps</source>
        <translation>سىستېما تەۋسىيە قىلىنغان ئەپلەرگە كۆڭۈلدىكى ئەپلەرنى قايتا بېكىتىش</translation>
        <extra-contents_path>/Defaultapp/Reset default apps to system recommended apps</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="253"/>
        <source>Reset</source>
        <translation>قايتا تىڭشى</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="348"/>
        <source>Browser</source>
        <translation>تور كۆرگۈچ</translation>
        <extra-contents_path>/Defaultapp/Browser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="350"/>
        <source>Mail</source>
        <translation>پوچتا يوللانمىسى</translation>
        <extra-contents_path>/Defaultapp/Mail</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="352"/>
        <source>Image Viewer</source>
        <translation>رەسىم كۆرگۈ</translation>
        <extra-contents_path>/Defaultapp/Image Viewer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="354"/>
        <source>Audio Player</source>
        <translation>ئۈن قويغۇچ</translation>
        <extra-contents_path>/Defaultapp/Audio Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="356"/>
        <source>Video Player</source>
        <translation>سىن قويغۇچ</translation>
        <extra-contents_path>/Defaultapp/Video Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="358"/>
        <source>Text Editor</source>
        <translation>تېكىست تەھرىرلىگۈچ</translation>
        <extra-contents_path>/Defaultapp/Text Editor</extra-contents_path>
    </message>
</context>
<context>
    <name>DefaultAppWindow</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="346"/>
        <source>Select Default Application</source>
        <translation>كۆڭۈلدىكى ئىلتىماسنى تاللاڭ</translation>
        <extra-contents_path>/Defaultapp/Select Default Application</extra-contents_path>
    </message>
    <message>
        <source>Browser</source>
        <translation type="vanished">浏览器</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation type="vanished">电子邮件</translation>
    </message>
    <message>
        <source>Image Viewer</source>
        <translation type="vanished">图像查看器</translation>
    </message>
    <message>
        <source>Audio Player</source>
        <translation type="vanished">音频播放器</translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation type="vanished">视频播放器</translation>
    </message>
    <message>
        <source>Text Editor</source>
        <translation type="vanished">文档编辑器</translation>
    </message>
    <message>
        <source>Reset to default</source>
        <translation type="vanished">恢复默认设置</translation>
    </message>
</context>
<context>
    <name>DefineGroupItem</name>
    <message>
        <source>Edit</source>
        <translation type="vanished">تەھرىرلەش</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>DefineGroupItemIntel</name>
    <message>
        <source>Edit</source>
        <translation type="vanished">تەھرىرلەش</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">ئۆچۈر</translation>
    </message>
</context>
<context>
    <name>DefineShortcutItem</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/defineshortcutitem.cpp" line="58"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
</context>
<context>
    <name>DelGroupDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>Are you sure to delete this group,
which will make some file components
in the file system invalid!</source>
        <translation type="vanished">确定删除此用户组? 这将使得文件系统\n中的某些文件组件ID无效!</translation>
    </message>
    <message>
        <source>Are you sure to delete the group:   </source>
        <translation type="vanished">توپنى چوقۇم ئۆچۈرەمسىز:   </translation>
    </message>
    <message>
        <source>which will make some file components in the file system invalid!</source>
        <translation type="vanished">بۇ ھۆججەت سىستېمىسىدىكى بەزى ھۆججەت دېتاللىرىنى ئىناۋەتسىز قىلىدۇ!</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">ئۆچۈر</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="vanished">删除文件</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">提醒</translation>
    </message>
    <message>
        <source>Are you sure to delete &quot;%1&quot; group,
which will make some file components
in the file system invalid!</source>
        <translation type="vanished">确定删除&quot;%1&quot;组,这将使得文件系统
中的某些文件组件ID无效!</translation>
    </message>
    <message>
        <source>Delete user group</source>
        <translation type="vanished">ئىشلەتكۈچى توپىنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <source>Are you sure to delete the group, which will make some file components in the file system invalid!</source>
        <translation type="vanished">确定删除该用户组，这将使得文件系统中的某些文件组件ID无效！</translation>
    </message>
</context>
<context>
    <name>DelGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">TextLabel</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="vanished">RemoveFile</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">ئەسكەرتىپ قويۇش</translation>
    </message>
</context>
<context>
    <name>DelUserDialog</name>
    <message>
        <source>Delete the user, belonging to the user&apos;s desktop,
documents, favorites, music, pictures and video
folder will be deleted!</source>
        <translation type="vanished">删除用户，用户所属的桌面、文档、收藏夹、音乐、图片和视频文件夹中的内容将全部被删除！</translation>
    </message>
    <message>
        <source>keep the user&apos;s data, like desktop,documents, favorites, music, pictures and so on</source>
        <translation type="vanished">保留用户下所属的桌面、文件、收藏夹、音乐等文件</translation>
    </message>
    <message>
        <source>delete whole data belong user</source>
        <translation type="vanished">删除该用户所有文件</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>KeepFile</source>
        <translation type="vanished">保留文件</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="vanished">删除文件</translation>
    </message>
    <message>
        <source>Delete the user &apos;</source>
        <translation type="vanished">是否删除用户&apos;</translation>
    </message>
    <message>
        <source>&apos;and:</source>
        <translation type="vanished">&apos;同时:</translation>
    </message>
</context>
<context>
    <name>DelUserIntelDialog</name>
    <message>
        <source>   Delete</source>
        <translation type="vanished">   ئۆچۈر</translation>
    </message>
    <message>
        <source>Define</source>
        <translation type="vanished">ئېنىقلىما</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Delete the user, belonging to the user&apos;s desktop documents, favorites, music, pictures and video folder will be deleted!</source>
        <translation type="vanished">ئابونتنى ئۆچۈرۈپ تاشلاڭ، ئىشلەتكۈچىنىڭ ئۈستەلئۈستى ھۆججەتلىرىگە تەۋە، ياخشى كۆرىدىغانلار، مۇزىكا، رەسىم ۋە سىن ھۆججەت قىسقۇچى ئۆچۈرۈلىدۇ!</translation>
    </message>
</context>
<context>
    <name>DeleteUserExists</name>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="26"/>
        <source>Delete User</source>
        <translation>ئىشلەتكۈچىنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="39"/>
        <source>Delete user &apos;</source>
        <translation>ئىشلەتكۈچىنى ئۆچۈرۈش &apos;</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="40"/>
        <source>&apos;? And:</source>
        <translation>&apos;? يەنە:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="66"/>
        <source>Keep desktop, files, favorites, music of the user</source>
        <translation>ئۈستەل يۈزى، ھۆججەت، ئامراقلار، ئابونتلارنىڭ مۇزىكىسىنى ساقلاش</translation>
    </message>
    <message>
        <source>Keep user&apos;s home folder</source>
        <translation type="vanished">保留用户家目录</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="99"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="101"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="67"/>
        <source>Delete whole data belong user</source>
        <translation>پۈتۈن سانلىق مەلۇماتنى ئۆچۈرۈش ئىشلەتكۈچىگە تەۋە</translation>
    </message>
</context>
<context>
    <name>Desktop</name>
    <message>
        <source>Icon Show On Desktop</source>
        <translation type="vanished">显示在桌面的图标</translation>
    </message>
    <message>
        <source>Computerdesktop</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <source>Trashdesktop</source>
        <translation type="vanished">垃圾箱</translation>
    </message>
    <message>
        <source>Homedesktop</source>
        <translation type="vanished">家目录</translation>
    </message>
    <message>
        <source>Volumedesktop</source>
        <translation type="vanished">挂载卷</translation>
    </message>
    <message>
        <source>Networkdesktop</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>Set Start Menu</source>
        <translation type="vanished">设置开始菜单</translation>
    </message>
    <message>
        <source>Always use the start menu in full screen</source>
        <translation type="vanished">一直使用全屏&quot;开始&quot;菜单</translation>
    </message>
    <message>
        <source>Icon Lock on Menu</source>
        <translation type="vanished">锁定在开始菜单的图标</translation>
    </message>
    <message>
        <source>Computermenu</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <source>Trashmenu</source>
        <translation type="vanished">回收站</translation>
    </message>
    <message>
        <source>Filesystemmenu</source>
        <translation type="vanished">个人</translation>
    </message>
    <message>
        <source>Tray icon</source>
        <translation type="vanished">显示在托盘上的图标</translation>
    </message>
    <message>
        <source>Homemenu</source>
        <translation type="vanished">家目录</translation>
    </message>
    <message>
        <source>Settingmenu</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Networkmenu</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
</context>
<context>
    <name>DeviceInfoItem</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">断开</translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation type="vanished">正在连接</translation>
    </message>
    <message>
        <source>Disconnecting</source>
        <translation type="vanished">正在断开</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="vanished">已连接</translation>
    </message>
    <message>
        <source>Ununited</source>
        <translation type="vanished">已配对</translation>
    </message>
    <message>
        <source>Connect fail</source>
        <translation type="vanished">连接失败</translation>
    </message>
    <message>
        <source>Send files</source>
        <translation type="vanished">发送文件</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">移除</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Sure to remove,</source>
        <translation type="vanished">确定移除，</translation>
    </message>
    <message>
        <source>After removal, the next connection requires matching PIN code!</source>
        <translation type="vanished">移除后，下次连接需重新配对PIN码！</translation>
    </message>
    <message>
        <source>Device connected</source>
        <translation type="vanished">设备连接</translation>
    </message>
    <message>
        <source>Device not connected</source>
        <translation type="vanished">设备未连接</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <source>FingerPrint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="vanished">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="vanished">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="vanished">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="vanished">声纹</translation>
    </message>
</context>
<context>
    <name>Dialog_login_reg</name>
    <message>
        <source>Sign in</source>
        <translation type="vanished">登录</translation>
    </message>
    <message>
        <source>Sign up</source>
        <translation type="vanished">注册云帐户</translation>
    </message>
    <message>
        <source>Login in progress</source>
        <translation type="vanished">登录中</translation>
    </message>
    <message>
        <source>Error code:</source>
        <translation type="vanished">错误代码：</translation>
    </message>
    <message>
        <source>!</source>
        <translation type="vanished">！</translation>
    </message>
    <message>
        <source>Internal error occurring!</source>
        <translation type="vanished">服务器错误！</translation>
    </message>
    <message>
        <source>Failed to sign up!</source>
        <translation type="vanished">注册失败!</translation>
    </message>
    <message>
        <source>Failed attempt to return value!</source>
        <translation type="vanished">登录返回值异常！</translation>
    </message>
    <message>
        <source>Check your connection!</source>
        <translation type="vanished">登录失败或网络异常！</translation>
    </message>
    <message>
        <source>Failed to get by phone!</source>
        <translation type="vanished">手机获取验证码失败！</translation>
    </message>
    <message>
        <source>Failed to get by user!</source>
        <translation type="vanished">用户名获取验证码失败！</translation>
    </message>
    <message>
        <source>Failed to reset password!</source>
        <translation type="vanished">重置密码失败！</translation>
    </message>
    <message>
        <source>Phone binding falied!</source>
        <translation type="vanished">手机绑定失败！</translation>
    </message>
    <message>
        <source>Please check your information!</source>
        <translation type="vanished">缺少必要信息！</translation>
    </message>
    <message>
        <source>Please check your account!</source>
        <translation type="vanished">帐户或密码错误！</translation>
    </message>
    <message>
        <source>Failed due to server error!</source>
        <translation type="vanished">服务器错误！</translation>
    </message>
    <message>
        <source>User existing!</source>
        <translation type="vanished">用户名已存在！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已被使用！</translation>
    </message>
    <message>
        <source>Please check your format!</source>
        <translation type="vanished">手机号码格式错误！</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation type="vanished">该手机当日接收短信次数达到上限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation type="vanished">手机验证码错误！</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户名不存在！</translation>
    </message>
    <message>
        <source>User has bound the phone!</source>
        <translation type="vanished">用户已经绑定手机号！</translation>
    </message>
    <message>
        <source>Sending code error occurring!</source>
        <translation type="vanished">发送验证码异常！</translation>
    </message>
    <message>
        <source>Your code is wrong!</source>
        <translation type="vanished">验证码错误！</translation>
    </message>
    <message>
        <source>Binding Phone</source>
        <translation type="vanished">绑定手机</translation>
    </message>
    <message>
        <source>Bind now</source>
        <translation type="vanished">绑定</translation>
    </message>
    <message>
        <source>Resend ( %1 )</source>
        <translation type="vanished">重新发送（%1）</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">获取绑定手机验证码</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">发送验证码</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="obsolete">至少六位，包含大小写字母、数字</translation>
    </message>
    <message>
        <source>Please check your password!</source>
        <translation type="vanished">两次密码设置不一致！</translation>
    </message>
    <message>
        <source>Sign in Cloud</source>
        <translation type="vanished">登录云帐户</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="vanished">忘记密码</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">返回登录</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="vanished">注册云帐户</translation>
    </message>
    <message>
        <source>Sign up now</source>
        <translation type="vanished">立即注册</translation>
    </message>
</context>
<context>
    <name>DigitalAuthIntelDialog</name>
    <message>
        <source>Enter Old Password</source>
        <translation type="vanished">كونا پارولنى كىرگۈز</translation>
    </message>
    <message>
        <source>Forget Password?</source>
        <translation type="vanished">مەخپىي نومۇرنى ئۇنتۇپ قالامسىز؟</translation>
    </message>
    <message>
        <source>Input New Password</source>
        <translation type="vanished">يېڭى ئىم كىرگۈزۈش</translation>
    </message>
    <message>
        <source>Input Password</source>
        <translation type="vanished">ئىم كىرگۈزۈش</translation>
    </message>
    <message>
        <source>The password input is error</source>
        <translation type="vanished">پارول كىرگۈزۈش خاتالىق</translation>
    </message>
    <message>
        <source>Confirm New Password</source>
        <translation type="vanished">يېڭى پارولنى جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>The password input is inconsistent</source>
        <translation type="vanished">مەخپىي نومۇر كىرگۈزۈش بىردەك ئەمەس</translation>
    </message>
    <message>
        <source>New password can not be consistent of old password</source>
        <translation type="vanished">يېڭى پارول كونا پارول بىلەن بىردەك بولالمايدۇ</translation>
    </message>
    <message>
        <source>Password Change Failed</source>
        <translation type="vanished">پارول ئۆزگەرتىش مەغلۇپ بولدى</translation>
    </message>
</context>
<context>
    <name>DigitalPhoneIntelDialog</name>
    <message>
        <source>Please Enter Edu OS Password</source>
        <translation type="vanished">Edu OS مەخپىي نومۇرىنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <source>The password input is error</source>
        <translation type="vanished">پارول كىرگۈزۈش خاتالىق</translation>
    </message>
</context>
<context>
    <name>DisplayPerformanceDialog</name>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="214"/>
        <source>Display Advanced Settings</source>
        <translation>ئىلغار تەڭشەكلەرنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="297"/>
        <source>Performance</source>
        <translation>ئويۇن قويۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="376"/>
        <source>Applicable to machine with discrete graphics, which can accelerate the rendering of 3D graphics.</source>
        <translation>3D گرافىكىنىڭ شەكىللىنىشىنى تېزلەتكىلى بولىدۇ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="392"/>
        <source>(Note: not support connect graphical with xmanager on windows.)</source>
        <translation>(ئەسكەرتىش: windows دىكى xmanager بىلەن گرافىكىلىق ئۇلاشنى قوللىمايدۇ.)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="462"/>
        <source>Compatible</source>
        <translation>ماسلاشتۇرۇلغان</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="538"/>
        <source>Applicable to machine with integrated graphics,  there is no 3D graphics acceleration. </source>
        <translation>توپلاشتۇرۇلغان گرافىكىلىق ماشىنىغا قوللىنىشقا بولىدۇ، 3D گرافىكىنىڭ تېزلىشى يوق. </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="554"/>
        <source>(Note: need connect graphical with xmanager on windows, use this option.)</source>
        <translation>(ئەسكەرتىش: windows دىكى xmanager بىلەن گرافىكىلىق ئۇلىنىشقا موھتاج، بۇ تاللانمىنى ئىشلىتىڭ.)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="604"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="vanished">ئاپتۇماتىك</translation>
    </message>
    <message>
        <source>Auto select according to environment, delay the login time (about 0.5 sec).</source>
        <translation type="vanished">مۇھىتقا ئاساسەن ئاپتوماتىك تاللاش، كىرىش ۋاقتىنى كېچىكتۈرۈش (تەخمىنەن 0.5 سېكون).</translation>
    </message>
    <message>
        <source>Threshold:</source>
        <translation type="vanished">چەرچەن:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="597"/>
        <source>Apply</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">قايتا تىڭشى</translation>
    </message>
    <message>
        <source>(Note: select this option to use 3D graphics acceleration and xmanager.)</source>
        <translation type="vanished">(ئەسكەرتىش: 3D گرافىكىلارنى تېزلىتىش ۋە xmanager ئىشلىتىش ئۈچۈن بۇ تاللاشنى تاللاڭ.)</translation>
    </message>
</context>
<context>
    <name>DisplaySet</name>
    <message>
        <source>display</source>
        <translation type="vanished">显示器</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="36"/>
        <source>Screen</source>
        <translation>ئېكران</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="38"/>
        <source>Display</source>
        <translation>كۆرسىتىش</translation>
    </message>
</context>
<context>
    <name>DisplayWindow</name>
    <message>
        <source>monitor</source>
        <translation type="vanished">كۆزەتكۈچى</translation>
    </message>
    <message>
        <source>set as home screen</source>
        <translation type="vanished">设为主屏</translation>
    </message>
    <message>
        <source>close monitor</source>
        <translation type="vanished">关闭显示器</translation>
    </message>
    <message>
        <source>unify output</source>
        <translation type="vanished">统一输出</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="320"/>
        <source>open monitor</source>
        <translation>ئوچۇق كۆزەتكۈچ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="14"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="32"/>
        <source>Display</source>
        <translation>كۆرسىتىش</translation>
    </message>
    <message>
        <source>as main</source>
        <translation type="vanished">ئاساسلىقى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="250"/>
        <source>screen zoom</source>
        <translation>ئېكراننى چوڭايت</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="374"/>
        <source>Advanced</source>
        <translation>ئىلغار</translation>
    </message>
    <message>
        <source>screen brightness adjustment</source>
        <translation type="vanished">调整屏幕亮度</translation>
    </message>
    <message>
        <source>dark</source>
        <translation type="vanished">暗</translation>
    </message>
    <message>
        <source>bright</source>
        <translation type="vanished">亮</translation>
    </message>
    <message>
        <source>follow the sunrise and sunset(17:55-05:04)</source>
        <translation type="vanished">كۈن چىقىش ۋە كۈن پېتىشقا ئەگىشىپ مېڭىش (17:55-05:04)</translation>
    </message>
    <message>
        <source>custom time</source>
        <translation type="vanished">ئۆرپ- ئادەت ۋاقتى</translation>
    </message>
    <message>
        <source>opening time</source>
        <translation type="vanished">ئېچىلىش ۋاقتى</translation>
    </message>
    <message>
        <source>closing time</source>
        <translation type="vanished">يېپىلىش ۋاقتى</translation>
    </message>
    <message>
        <source>color temperature</source>
        <translation type="vanished">رەڭ تېمپېراتۇرىسى</translation>
    </message>
    <message>
        <source>warm</source>
        <translation type="vanished">ئىللىقلىق</translation>
    </message>
    <message>
        <source>cold</source>
        <translation type="vanished">سوغۇق</translation>
    </message>
    <message>
        <source>apply</source>
        <translation type="vanished">应用</translation>
    </message>
    <message>
        <source>Mirror Display</source>
        <translation type="vanished">ئەينەك كۆرسىتىش</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">چۇقۇم</translation>
    </message>
    <message>
        <source>Edit User Group</source>
        <translation type="vanished">ئىشلەتكۈچىلەر گۇرۇپپىسىنى تەھرىرلەش</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <source>Id</source>
        <translation type="vanished">كىملىك</translation>
    </message>
    <message>
        <source>Members</source>
        <translation type="vanished">ئەزالار</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation type="vanished">ئەسكەرتىش</translation>
    </message>
    <message>
        <source>Invalid Id!</source>
        <translation type="vanished">ئىناۋەتسىز كىملىك!</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">ماقۇل</translation>
    </message>
    <message>
        <source>Edit user group</source>
        <translation type="vanished">ئىشلەتكۈچى گۇرۇپپىسىنى تەھرىرلەش</translation>
    </message>
</context>
<context>
    <name>EditPassDialog</name>
    <message>
        <source>Edit Password</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">请输入用户名</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="obsolete">输入密码</translation>
    </message>
    <message>
        <source>Your new password here</source>
        <translation type="vanished">新密码</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">输入验证码</translation>
    </message>
    <message>
        <source>Your code</source>
        <translation type="vanished">请输入验证码</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">获取绑定手机验证码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Confirm your new password</source>
        <translation type="vanished">确认新密码</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">至少六位，包含大小写字母、数字</translation>
    </message>
    <message>
        <source>Your password is valid!</source>
        <translation type="vanished">您的密码是有效的！</translation>
    </message>
    <message>
        <source>Please check your password!</source>
        <translation type="vanished">两次密码设置不一致！</translation>
    </message>
    <message>
        <source>Resend(</source>
        <translation type="vanished">重新发送(</translation>
    </message>
    <message>
        <source>)</source>
        <translation type="vanished">)</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">发送验证码</translation>
    </message>
    <message>
        <source>Success！</source>
        <translation type="vanished">成功！</translation>
    </message>
    <message>
        <source>Reback sign in</source>
        <translation type="vanished">重新登录</translation>
    </message>
    <message>
        <source>Error code:</source>
        <translation type="vanished">错误代码：</translation>
    </message>
    <message>
        <source>!</source>
        <translation type="vanished">！</translation>
    </message>
    <message>
        <source>Internal error occurring!</source>
        <translation type="vanished">服务器错误！</translation>
    </message>
    <message>
        <source>Failed to sign up!</source>
        <translation type="vanished">注册失败!</translation>
    </message>
    <message>
        <source>Failed attempt to return value!</source>
        <translation type="vanished">尝试获取返回值失败！</translation>
    </message>
    <message>
        <source>Check your connection!</source>
        <translation type="vanished">登录失败或网络异常！</translation>
    </message>
    <message>
        <source>Failed to get by phone!</source>
        <translation type="vanished">手机获取验证码失败！</translation>
    </message>
    <message>
        <source>Failed to get by user!</source>
        <translation type="vanished">用户名获取验证码失败！</translation>
    </message>
    <message>
        <source>Failed to reset password!</source>
        <translation type="vanished">重置密码失败！</translation>
    </message>
    <message>
        <source>Please check your information!</source>
        <translation type="vanished">缺少必要信息！</translation>
    </message>
    <message>
        <source>Please check your account!</source>
        <translation type="vanished">帐户或密码错误！</translation>
    </message>
    <message>
        <source>Failed due to server error!</source>
        <translation type="vanished">服务器错误！</translation>
    </message>
    <message>
        <source>User existing!</source>
        <translation type="vanished">用户名已存在！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已被使用！</translation>
    </message>
    <message>
        <source>Please check your format!</source>
        <translation type="vanished">手机号码格式错误！</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation type="vanished">该手机当日接收短信次数达到上限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation type="vanished">手机验证码错误！</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户名不存在！</translation>
    </message>
    <message>
        <source>Sending code error occurring!</source>
        <translation type="vanished">发送验证码异常！</translation>
    </message>
</context>
<context>
    <name>EditPushButton</name>
    <message>
        <source>Reset</source>
        <translation type="vanished">重置密码</translation>
    </message>
</context>
<context>
    <name>ExperiencePlan</name>
    <message>
        <source>User Experience</source>
        <translation type="vanished">用户体验</translation>
    </message>
    <message>
        <source>Join in user Experience plan</source>
        <translation type="vanished">加入用户体验计划</translation>
    </message>
    <message>
        <source>User experience plan terms, see</source>
        <translation type="vanished">用户体验计划条款，参见</translation>
    </message>
    <message>
        <source>《User Experience plan》</source>
        <translation type="vanished">《用户体验计划》</translation>
    </message>
    <message>
        <source>experienceplan</source>
        <translation type="vanished">体验计划</translation>
    </message>
    <message>
        <source>Experienceplan</source>
        <translation type="vanished">体验计划</translation>
    </message>
</context>
<context>
    <name>Fonts</name>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="50"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="46"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="122"/>
        <source>Fonts</source>
        <translation>خەت نۇسخىلىرى</translation>
        <extra-contents_path>/Fonts/Fonts</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="264"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="126"/>
        <source>Fonts select</source>
        <translation>خەت نۇسخىلىرىنى تاللاش</translation>
        <extra-contents_path>/Fonts/Fonts select</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="146"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="124"/>
        <source>Font size</source>
        <translation>خەت چوڭلۇقى</translation>
        <extra-contents_path>/Fonts/Font size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="370"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="128"/>
        <source>Mono font</source>
        <translation>Mono خەت نۇسخىسى</translation>
        <extra-contents_path>/Fonts/Mono font</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="421"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="130"/>
        <source>Reset to default</source>
        <translation>كۆڭۈلدىكى ھالىتىگە قايتاي</translation>
        <extra-contents_path>/Fonts/Reset to default</extra-contents_path>
    </message>
    <message>
        <source>Gtk default font</source>
        <translation type="vanished">通用字体</translation>
    </message>
    <message>
        <source>Document font</source>
        <translation type="vanished">文档字体</translation>
    </message>
    <message>
        <source>Monospace font</source>
        <translation type="vanished">等宽字体</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation type="vanished">高级设置</translation>
    </message>
    <message>
        <source>Peony font</source>
        <translation type="vanished">桌面字体</translation>
    </message>
    <message>
        <source>titlebar font</source>
        <translation type="vanished">标题字体</translation>
    </message>
    <message>
        <source>Select text sample that looks clearest</source>
        <translation type="vanished">选择看起来清晰的字体效果</translation>
    </message>
    <message>
        <source>fonts</source>
        <translation type="vanished">字体</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="vanished">11</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="vanished">12</translation>
    </message>
    <message>
        <source>13</source>
        <translation type="vanished">13</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
    <message>
        <source>16</source>
        <translation type="vanished">16</translation>
    </message>
    <message>
        <source>Thanks For Using The ukcc</source>
        <translation type="vanished">欢迎使用设置</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="144"/>
        <source>Small</source>
        <translation>كىچىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="148"/>
        <source>Large</source>
        <translation>چوڭ</translation>
    </message>
</context>
<context>
    <name>FrameItem</name>
    <message>
        <source>Sync failed, please login out to retry!</source>
        <translation type="vanished">同步失败，请检查网络或退出云帐户重试！</translation>
    </message>
    <message>
        <source>Change configuration file failed, please login out to retry!</source>
        <translation type="vanished">配置文件更改失败，请检查网络或退出云帐户重试！</translation>
    </message>
    <message>
        <source>Configuration file not exist, please login out to retry!</source>
        <translation type="vanished">配置文件不存在，请检查网络或退出云帐户重试！</translation>
    </message>
    <message>
        <source>Cloud verifyed file download failed, please login out to retry!</source>
        <translation type="vanished">云校验失败，请检查网络或退出云帐户重试！</translation>
    </message>
    <message>
        <source>OSS access failed, please login out to retry!</source>
        <translation type="vanished">文件存储访问失败，请检查网络或退出云帐户重试！</translation>
    </message>
    <message>
        <source>Sync failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">同步失败，建议重试或者重新登录来获取最佳体验！</translation>
    </message>
    <message>
        <source>Change configuration file failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">配置文件设置失败，建议重试或者重新登录来获取最佳体验！</translation>
    </message>
    <message>
        <source>Configuration file not exist, please retry or login out to get a better experience!</source>
        <translation type="vanished">配置文件不存在，建议重试或者重新登录来获取最佳体验！</translation>
    </message>
    <message>
        <source>Cloud verifyed file download failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">云端校验失败，建议重试或者重新登录来获取最佳体验！</translation>
    </message>
    <message>
        <source>OSS access failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">文件存储访问失败，建议重试或者重新登录来获取最佳体验！</translation>
    </message>
    <message>
        <source>Sync failed,please relogin!</source>
        <translation type="vanished">同步失败，请重新登录！</translation>
    </message>
    <message>
        <source>Change configuration file failed,please relogin!</source>
        <translation type="vanished">修改配置文件失败，请重新登录！</translation>
    </message>
    <message>
        <source>Configuration file not exist,please relogin!</source>
        <translation type="vanished">配置文件不存在，请重试！</translation>
    </message>
    <message>
        <source>Cloud verifyed file download failed,please relogin!</source>
        <translation type="vanished">云文件验证失败，请重试！</translation>
    </message>
    <message>
        <source>OSS access failed,please relogin!</source>
        <translation type="vanished">连接失败，请重试或重新登录！</translation>
    </message>
</context>
<context>
    <name>Gesture</name>
    <message>
        <source>Gesture</source>
        <translatorcomment>手势</translatorcomment>
        <translation type="obsolete">手势</translation>
    </message>
</context>
<context>
    <name>GetShortcutWorker</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="59"/>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="83"/>
        <source>Null</source>
        <translation>Null</translation>
    </message>
</context>
<context>
    <name>GrubVerify</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="24"/>
        <source>Grub verify</source>
        <translation>Grub دەلىللەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="29"/>
        <source>User:</source>
        <translation>ئىشلەتكۈچى:</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="42"/>
        <source>Pwd</source>
        <translation>Pwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="85"/>
        <source>Sure Pwd</source>
        <translation>ئەلۋەتتە Pwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="138"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="142"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="197"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="235"/>
        <source>Inconsistency with pwd</source>
        <translation>pwd بىلەن ماس كەلمەسلىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="225"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="303"/>
        <source>pwd cannot be empty!</source>
        <translation>pwd قۇرۇق بولالمايدۇ!</translation>
    </message>
</context>
<context>
    <name>HistoryUpdateListWig</name>
    <message>
        <source>Success</source>
        <translation type="vanished">更新成功</translation>
    </message>
    <message>
        <source>Failed</source>
        <translation type="vanished">更新失败</translation>
    </message>
</context>
<context>
    <name>HostNameDialog</name>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="17"/>
        <source>Set HostName</source>
        <translation>HostName نى بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="45"/>
        <source>HostName</source>
        <translation>HostName</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="91"/>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="94"/>
        <source>Must be 1-64 characters long</source>
        <translation>چوقۇم ئۇزۇنلۇقى 1-64 ھەرپ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="161"/>
        <source>Hostname must start or end with a number and a letter</source>
        <translation>Hostname چوقۇم نومۇر ۋە ھەرپ بىلەن باشلىشى ياكى ئاخىرلىشىشى كېرەك</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="172"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>ساھىپخان نامىنى كەينى-كەينىدىن &apos; &gt; ۋە &gt; دەپ ئاتاش مۇمكىن ئەمەس . &apos;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="179"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>ساھىپخان نامىنى كەينى-كەينىگە قويۇشقا بولمايدۇ &apos; . &apos;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="75"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="71"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>InputPwdDialog</name>
    <message>
        <source>Set Password</source>
        <translation type="vanished">پارول بەلگىلەش</translation>
    </message>
    <message>
        <source>Password can not be blank</source>
        <translation type="vanished">密码不能为空</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="77"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="28"/>
        <source>VNC password</source>
        <translation>VNC مەخپىي نومۇرى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="45"/>
        <source>Password</source>
        <translation>ئىم</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="60"/>
        <source>Must be 1-8 characters long</source>
        <translation>چوقۇم ئۇزۇنلۇقى 1-8 ھەرپ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="81"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>less than or equal to 8</source>
        <translation type="vanished">密码长度需要小于等于８</translation>
    </message>
</context>
<context>
    <name>ItemList</name>
    <message>
        <source>Walpaper</source>
        <translation type="vanished">桌面壁纸</translation>
    </message>
    <message>
        <source>ScreenSaver</source>
        <translation type="vanished">屏保</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">开始菜单</translation>
    </message>
    <message>
        <source>Quick Start</source>
        <translation type="vanished">快速启动项</translation>
    </message>
    <message>
        <source>Avatar</source>
        <translation type="vanished">头像</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">任务栏</translation>
    </message>
    <message>
        <source>Font</source>
        <translation type="vanished">字体</translation>
    </message>
    <message>
        <source>Wallpaper</source>
        <translation type="vanished">桌面背景</translation>
    </message>
    <message>
        <source>Themes</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Area</source>
        <translation type="vanished">区域语言</translation>
    </message>
    <message>
        <source>Date/Time</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>Default Open</source>
        <translation type="vanished">默认打开方式</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">侧边栏</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Peony</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Boot</source>
        <translation type="vanished">开机启动项</translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="vanished">电源</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation type="vanished">文本编辑器</translation>
    </message>
    <message>
        <source>Terminal</source>
        <translation type="vanished">终端</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">鼠标</translation>
    </message>
    <message>
        <source>TouchPad</source>
        <translation type="vanished">触控板</translation>
    </message>
    <message>
        <source>KeyBoard</source>
        <translation type="vanished">键盘</translation>
    </message>
    <message>
        <source>ShortCut</source>
        <translation type="vanished">快捷键</translation>
    </message>
</context>
<context>
    <name>KbPreviewFrame</name>
    <message>
        <source>Keyboard Preview</source>
        <translation type="obsolete">布局预览</translation>
    </message>
    <message>
        <source>No preview found</source>
        <translation type="vanished">无预览</translation>
    </message>
    <message>
        <source>Unable to open Preview !</source>
        <translation type="vanished">无法打开预览！</translation>
    </message>
</context>
<context>
    <name>KbdLayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="68"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="144"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="222"/>
        <source>Variant</source>
        <translation>ۋاراتا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="270"/>
        <source>Add</source>
        <translation>قوش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="58"/>
        <source>Add Layout</source>
        <translation>ئورۇنلاشتۇرما قوشۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="233"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <source>Keyboard Preview</source>
        <translation type="vanished">布局预览</translation>
    </message>
</context>
<context>
    <name>KeyValueConverter</name>
    <message>
        <source>system</source>
        <translation type="vanished">系统</translation>
    </message>
    <message>
        <source>devices</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>personalized</source>
        <translation type="vanished">个性化</translation>
    </message>
    <message>
        <source>network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>account</source>
        <translation type="vanished">帐户</translation>
    </message>
    <message>
        <source>datetime</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>update</source>
        <translation type="vanished">更新和备份</translation>
    </message>
    <message>
        <source>messages</source>
        <translation type="vanished">通知关于</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="46"/>
        <source>System</source>
        <translation>سىستېما</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="49"/>
        <source>Devices</source>
        <translation>ئۈسكۈنىلەر</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="55"/>
        <source>Personalized</source>
        <translation>شەخسىيلەشتۈرۈلگەن</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="52"/>
        <source>Network</source>
        <translation>تور</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="58"/>
        <source>Account</source>
        <translation>ھېساب</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="61"/>
        <source>Datetime</source>
        <translation>چېسلا ۋاقتى</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="64"/>
        <source>Update</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="67"/>
        <source>Security</source>
        <translation>بىخەتەرلىك</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="70"/>
        <source>Application</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="73"/>
        <source>Investigation</source>
        <translation>تەكشۈرۈش</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="76"/>
        <source>Commoninfo</source>
        <translation>Commoninfo</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation type="vanished">通知关于</translation>
    </message>
</context>
<context>
    <name>KeyboardControl</name>
    <message>
        <source>Keys Settings</source>
        <translation type="vanished">通用设置</translation>
    </message>
    <message>
        <source>Enable repeat key</source>
        <translation type="vanished">启用按键重复设置</translation>
    </message>
    <message>
        <source>Delay</source>
        <translation type="vanished">延迟</translation>
    </message>
    <message>
        <source>Short</source>
        <translation type="vanished">短</translation>
    </message>
    <message>
        <source>Long</source>
        <translation type="vanished">长</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="vanished">速度</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">慢</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快</translation>
    </message>
    <message>
        <source>Input characters to test the repetition effect：</source>
        <translation type="vanished">输入字符测试重复效果：</translation>
    </message>
    <message>
        <source>Input Settings</source>
        <translation type="vanished">输入法设置</translation>
    </message>
    <message>
        <source>Input Set</source>
        <translation type="vanished">输入法设置</translation>
    </message>
    <message>
        <source>Input settings</source>
        <translation type="vanished">输入法设置</translation>
    </message>
    <message>
        <source>Input characters to test the repetition effect:</source>
        <translation type="vanished">输入字符测试重复效果：</translation>
    </message>
    <message>
        <source>Tip of keyboard</source>
        <translation type="vanished">启用按键提示</translation>
    </message>
    <message>
        <source>reset default layout</source>
        <translation type="vanished">恢复默认布局</translation>
    </message>
    <message>
        <source>Reset layout</source>
        <translation type="vanished">重置布局</translation>
    </message>
    <message>
        <source>Message of capslock</source>
        <translation type="vanished">大写锁定提示</translation>
    </message>
    <message>
        <source>Enable numlock</source>
        <translation type="vanished">小键盘开启提示</translation>
    </message>
    <message>
        <source>Keyboard Layout</source>
        <translation type="vanished">键盘布局</translation>
    </message>
    <message>
        <source>Keyboard layout</source>
        <translation type="vanished">键盘布局</translation>
    </message>
    <message>
        <source>Install layouts</source>
        <translation type="vanished">安装其他布局</translation>
    </message>
    <message>
        <source>keyboard</source>
        <translation type="vanished">键盘</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">كۇنۇپكا تاختىسى</translation>
    </message>
</context>
<context>
    <name>KeyboardMain</name>
    <message>
        <source>Key board settings</source>
        <translation type="vanished">ئاچقۇچلۇق تاختا تەڭشەكلىرى</translation>
    </message>
    <message>
        <source>Input method settings</source>
        <translation type="vanished">输入法设置</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="25"/>
        <source>Keyboard</source>
        <translation>كۇنۇپكا تاختىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="102"/>
        <source>Keyboard settings</source>
        <translation>كۇنۇپكا تاختىسى تەڭشەكلىرى</translation>
        <extra-contents_path>/Keyboard/Keyboard settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="114"/>
        <source>Input settings</source>
        <translation>كىرگۈزۈش تەڭشەكلىرى</translation>
        <extra-contents_path>/Keyboard/Input settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="154"/>
        <source>Key repeat</source>
        <translation>ئاچقۇچ تەكرارلاش</translation>
        <extra-contents_path>/Keyboard/Key repeat</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="177"/>
        <source>Delay</source>
        <translation>كېچىكتۈرۈش</translation>
        <extra-contents_path>/Keyboard/Delay</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="180"/>
        <source>Short</source>
        <translation>قىسقا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="183"/>
        <source>Long</source>
        <translation>ئۇزۇن</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="214"/>
        <source>Speed</source>
        <translation>سۈرئەت</translation>
        <extra-contents_path>/Keyboard/Speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="217"/>
        <source>Slow</source>
        <translation>ئاستا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="220"/>
        <source>Fast</source>
        <translation>روزا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="251"/>
        <source>Input test</source>
        <translation>كىرگۈزۈش سىنىقى</translation>
        <extra-contents_path>/Keyboard/Input test</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="272"/>
        <source>Key tips</source>
        <translation>ئاچقۇچلۇق ئەسكەرتىش</translation>
        <extra-contents_path>/Keyboard/Key tips</extra-contents_path>
    </message>
</context>
<context>
    <name>KeyboardPainter</name>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Keyboard layout levels</source>
        <translation type="vanished">键盘布局等级</translation>
    </message>
    <message>
        <source>Level %1, %2</source>
        <translation type="vanished">等级 %1 %2</translation>
    </message>
    <message>
        <source>Keyboard Preview</source>
        <translation type="vanished">布局预览</translation>
    </message>
</context>
<context>
    <name>LanguageFrame</name>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="77"/>
        <source>Input Settings</source>
        <translation>كىرگۈزۈش تەڭشەكلىرى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="78"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="26"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="121"/>
        <source>Manager Keyboard Layout</source>
        <translation>باشقۇرغۇ كۇنۇپكا تاختىسى ئورۇنلاشتۇرمىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="234"/>
        <source>Language</source>
        <translation>تىل</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="250"/>
        <source>Country</source>
        <translation>دۆلەت</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="293"/>
        <source>Variant</source>
        <translation>ۋاراتا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="351"/>
        <source>Layout installed</source>
        <translation>ئورۇنلاشتۇرۇلغان</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="399"/>
        <source>Preview</source>
        <translation>كۆرۈنمە يۈزى</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="431"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="450"/>
        <source>Install</source>
        <translation>قاچىلاش</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <source>Forget</source>
        <translation type="vanished">忘记密码</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">发送验证码</translation>
    </message>
    <message>
        <source>User Sign in</source>
        <translation type="vanished">帐号密码登录</translation>
    </message>
    <message>
        <source>Quick Sign in</source>
        <translation type="vanished">短信快捷登录</translation>
    </message>
    <message>
        <source>Your account/phone here</source>
        <translation type="vanished">请输入用户名/手机号码</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">请输入用户名</translation>
    </message>
    <message>
        <source>Your phone number here</source>
        <translation type="vanished">手机号码</translation>
    </message>
    <message>
        <source>Your account/phone/email here</source>
        <translation type="vanished">请输入用户名/手机号码/邮箱</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">输入密码</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">输入验证码</translation>
    </message>
</context>
<context>
    <name>MCodeWidget</name>
    <message>
        <source>SongTi</source>
        <translation type="vanished">宋体</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Sign in</source>
        <translation type="vanished">登录</translation>
    </message>
    <message>
        <source>Sign up</source>
        <translation type="vanished">注册云帐户</translation>
    </message>
    <message>
        <source>Login in progress</source>
        <translation type="vanished">登录中</translation>
    </message>
    <message>
        <source>Error code:</source>
        <translation type="vanished">错误代码：</translation>
    </message>
    <message>
        <source>!</source>
        <translation type="vanished">！</translation>
    </message>
    <message>
        <source>Internal error occurring!</source>
        <translation type="vanished">服务器错误！</translation>
    </message>
    <message>
        <source>Internal error occurred!</source>
        <translation type="vanished">服务器错误！</translation>
    </message>
    <message>
        <source>Failed to sign up!</source>
        <translation type="vanished">注册失败!</translation>
    </message>
    <message>
        <source>Failed attempt to return value!</source>
        <translation type="vanished">返回值失败!</translation>
    </message>
    <message>
        <source>Check your connection!</source>
        <translation type="vanished">登录失败或网络异常！</translation>
    </message>
    <message>
        <source>Failed to get by phone!</source>
        <translation type="vanished">手机获取验证码失败！</translation>
    </message>
    <message>
        <source>Failed to get by user!</source>
        <translation type="vanished">用户名获取验证码失败！</translation>
    </message>
    <message>
        <source>Failed to reset password!</source>
        <translation type="vanished">重置密码失败！</translation>
    </message>
    <message>
        <source>Timeout!</source>
        <translation type="vanished">登录超时，请重新输入验证码登录！</translation>
    </message>
    <message>
        <source>Phone binding falied!</source>
        <translation type="vanished">手机绑定失败！</translation>
    </message>
    <message>
        <source>Please check your information!</source>
        <translation type="vanished">缺少必要信息！</translation>
    </message>
    <message>
        <source>Please check your account!</source>
        <translation type="vanished">帐户或密码错误！</translation>
    </message>
    <message>
        <source>Failed due to server error!</source>
        <translation type="vanished">服务器错误！</translation>
    </message>
    <message>
        <source>User and passsword can&apos;t be empty!</source>
        <translation type="vanished">用户以及密码不能为空！</translation>
    </message>
    <message>
        <source>User existing!</source>
        <translation type="vanished">用户名已存在！</translation>
    </message>
    <message>
        <source>User doesn&apos;t exist!</source>
        <translation type="vanished">用户不存在！</translation>
    </message>
    <message>
        <source>Network can not reach!</source>
        <translation type="vanished">网络不可达！</translation>
    </message>
    <message>
        <source>Phone can&apos;t be empty!</source>
        <translation type="vanished">手机号不能为空！</translation>
    </message>
    <message>
        <source>Account or password error!</source>
        <translation type="vanished">帐户或密码错误！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已被使用！</translation>
    </message>
    <message>
        <source>Please check your format!</source>
        <translation type="vanished">手机号码格式错误！</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation type="vanished">该手机当日接收短信次数达到上限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation type="vanished">手机验证码错误！</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户名不存在！</translation>
    </message>
    <message>
        <source>User has bound the phone!</source>
        <translation type="vanished">用户已经绑定手机号！</translation>
    </message>
    <message>
        <source>Sending code error occurred!</source>
        <translation type="vanished">发送验证码异常！</translation>
    </message>
    <message>
        <source>Phone code is expired!</source>
        <translation type="vanished">验证码过期！</translation>
    </message>
    <message>
        <source>Phone code error!</source>
        <translation type="vanished">验证码错误！</translation>
    </message>
    <message>
        <source>Code can not be empty!</source>
        <translation type="vanished">图片验证码不能为空！</translation>
    </message>
    <message>
        <source>MCode can not be empty!</source>
        <translation type="vanished">手机验证码不能为空！</translation>
    </message>
    <message>
        <source>Sending code error occurring!</source>
        <translation type="vanished">发送验证码异常！</translation>
    </message>
    <message>
        <source>Your code is wrong!</source>
        <translation type="vanished">验证码错误！</translation>
    </message>
    <message>
        <source>Please check your phone!</source>
        <translation type="vanished">请检查您的手机号码格式！</translation>
    </message>
    <message>
        <source>Please check your password!</source>
        <translation type="vanished">两次密码设置不一致！</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">至少六位，包含大小写字母、数字</translation>
    </message>
    <message>
        <source>Sign in Cloud</source>
        <translation type="vanished">登录云帐户</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="vanished">忘记密码</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">返回登录</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="vanished">注册云帐户</translation>
    </message>
    <message>
        <source>Sign up now</source>
        <translation type="vanished">立即注册</translation>
    </message>
    <message>
        <source>Please confirm your password!</source>
        <translation type="vanished">两次密码输入不一致！</translation>
    </message>
    <message>
        <source>Resend ( %1 )</source>
        <translation type="vanished">重新发送(%1)</translation>
    </message>
    <message>
        <source>Get</source>
        <translation type="vanished">获取验证码</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">获取绑定手机验证码</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">发送验证码</translation>
    </message>
    <message>
        <source>Binding Phone</source>
        <translation type="vanished">绑定手机</translation>
    </message>
    <message>
        <source>Please make sure your password is safety!</source>
        <translation type="vanished">请确保您的密码符合要求！</translation>
    </message>
    <message>
        <source>Bind now</source>
        <translation type="vanished">绑定</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Your account：%1</source>
        <translation type="vanished">您的云帐户：%1</translation>
    </message>
    <message>
        <source>Unauthorized device or OSS falied.
Please retry for login!</source>
        <translation type="vanished">OSS访问失败，请检查您的网络后再登录！</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">退出登录</translation>
    </message>
    <message>
        <source>Sync</source>
        <translation type="vanished">同步中</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation type="vanished">登录</translation>
        <extra-contents_path>/networkaccount/Sign in</extra-contents_path>
    </message>
    <message>
        <source>Enable item sync</source>
        <translation type="vanished">开启单项同步</translation>
    </message>
    <message>
        <source>Logout failed,please check your connection</source>
        <translation type="vanished">登录失败，请检查你的网络连接</translation>
    </message>
    <message>
        <source>Stop sync</source>
        <translation type="vanished">结束同步</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的云帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Waitting for sync!</source>
        <translation type="vanished">等待同步！</translation>
    </message>
    <message>
        <source>Synchronize your personalized settings and data</source>
        <translation type="vanished">同步您帐户的数据以及个性化设置</translation>
    </message>
    <message>
        <source>This operation may cover your settings!</source>
        <translation type="vanished">该操作可能覆盖您现有的设置！</translation>
    </message>
    <message>
        <source>The latest time sync is: </source>
        <translation type="vanished">上次同步时间为: </translation>
    </message>
    <message>
        <source>Waiting for initialization...</source>
        <translation type="vanished">等待云帐户初始化...</translation>
    </message>
    <message>
        <source>Network can not reach!</source>
        <translation type="vanished">网络不可达！</translation>
    </message>
    <message>
        <source>The Cloud Account Service version is out of date!</source>
        <translation type="vanished">云帐户服务版本已经过期，请升级！</translation>
    </message>
    <message>
        <source>KylinID open error!</source>
        <translation type="vanished">麒麟ID客户端打开失败!</translation>
    </message>
    <message>
        <source>Unauthorized device or OSS falied.
Please retry or relogin!</source>
        <translation type="vanished">设备凭证已过期，请重新登录！</translation>
    </message>
    <message>
        <source>Authorization failed!</source>
        <translation type="vanished">认证失败，请重新登录！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/networkaccount/mainwidget.cpp" line="708"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">云帐户消息</translation>
    </message>
    <message>
        <source>Synchronize your computer&apos;s settings into your cloud account here.</source>
        <translation type="vanished">将本机的设置同步至云帐户，通过云帐户随时随地开启个性设置！</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Sync downloading,please wait!</source>
        <translation type="vanished">同步下载中，请稍后......！</translation>
    </message>
    <message>
        <source>Sync uploading,please wait!</source>
        <translation type="vanished">同步上传中，请稍后......！</translation>
    </message>
    <message>
        <source>Sync failed, please check your internet connection or login out to retry!</source>
        <translation type="vanished">同步失败，请检查您的网络连接或者登出再重试一次！</translation>
    </message>
    <message>
        <source>%1,</source>
        <translation type="vanished">%1,</translation>
    </message>
    <message>
        <source>Synchronized failed: %1 please retry or login out to get a better experience.</source>
        <translation type="vanished">同步失败：%1 请重试或者重新登录来获取最佳体验！</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <source>Synchronized failed: %1, please retry or login out to get a better experience.</source>
        <translation type="vanished">同步失败：%1，请重试或者重新登录来获取最佳体验！</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Search</source>
        <translation type="vanished">ئىزدە</translation>
    </message>
    <message>
        <source>UKCC</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="410"/>
        <location filename="../../mainwindow.cpp" line="431"/>
        <location filename="../../mainwindow.cpp" line="550"/>
        <location filename="../../mainwindow.cpp" line="1049"/>
        <source>Settings</source>
        <translation>تەڭشەكلەر</translation>
    </message>
    <message>
        <source>Main menu</source>
        <translation type="vanished">ئاساسلىق تىزىملىك</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="452"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <source>Maximize/Normal</source>
        <translation type="vanished">最大化/正常</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <source>Warnning</source>
        <translation>ئاگاھلاندۇرۇش</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">نورمال</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="195"/>
        <location filename="../../mainwindow.cpp" line="453"/>
        <source>Maximize</source>
        <translation>ئەڭ چوڭ چەككە</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="obsolete">开始菜单</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="192"/>
        <source>Restore</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="425"/>
        <source>Back home</source>
        <translation>يۇرتىغا قايتىش</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="451"/>
        <source>Option</source>
        <translation>تاللانما</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="454"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="538"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="540"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="542"/>
        <source>Exit</source>
        <translation>چىقىش ئېغىزى</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>نەشرى: </translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="687"/>
        <source>Specified</source>
        <translation>بەلگىلەنگەن</translation>
    </message>
    <message>
        <source>ukcc</source>
        <translation type="vanished">控制面板</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>This function has been controlled</source>
        <translation>بۇ فۇنكسىيە كونترول قىلىندى</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">首页</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>Form</source>
        <translation type="vanished">جەدۋەل</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="vanished">دىققەت قىلىش</translation>
    </message>
    <message>
        <source>It takes effect after logging off</source>
        <translation type="vanished">چۇشۇنۇپ بولغاندىن كىيىن كۈچكە ئىگە بولىدۇ</translation>
    </message>
    <message>
        <source>Logout Now</source>
        <translation type="vanished">ھازىر چېكىنىش</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Reboot Now</source>
        <translation type="vanished">ھازىر قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <source>This cleanup and restore need to be done after the system restarts, whether to restart and restore immediately?</source>
        <translation type="vanished">بۇ تازىلاش ۋە ئەسلىگە كەلتۈرۈش سىستېمىسى قايتا قوزغالغاندىن كېيىن چوقۇم قىلىش كېرەك، دەرھال قايتا قوزغىتىش ۋە ئەسلىگە كەلتۈرۈش كېرەكمۇ-يوق؟</translation>
    </message>
    <message>
        <source>System Backup Tips</source>
        <translation type="vanished">سىستېما زاپاسلاش ئەسكەرتىشلىرى</translation>
    </message>
</context>
<context>
    <name>MessageBoxDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">ئۇچۇر</translation>
    </message>
    <message>
        <source>You do not have administrator rights!</source>
        <translation type="vanished">سىزنىڭ باشقۇرغۇچى ھوقۇقىڭىز يوق!</translation>
    </message>
    <message>
        <source> Factory Settings cannot be restored!</source>
        <translation type="vanished"> زاۋۇت تەڭشەكلىرىنى ئەسلىگە كەلتۈرگىلى بولمايدۇ!</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">ماقۇل</translation>
    </message>
</context>
<context>
    <name>MessageBoxPower</name>
    <message>
        <source>System Recovery</source>
        <translation type="vanished">سىستېما ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <source>The battery is low,please connect the power</source>
        <translation type="vanished">باتارېيە تۆۋەن، توك مەنبەسىنى ئۇلاڭ</translation>
    </message>
    <message>
        <source>Keep the power connection, or the power is more than 25%.</source>
        <translation type="vanished">توك بىلەن ئۇلىنىشنى ساقلاپ قېلىش كېرەك، بولمىسا توكى ٪25 تىن يۇقىرى بولىدۇ.</translation>
    </message>
    <message>
        <source>Remind in 30 minutes</source>
        <translation type="vanished">30 مىنۇت ئىچىدە ئەسكەرتىپ قويۇش</translation>
    </message>
    <message>
        <source>Got it</source>
        <translation type="vanished">چۈشەندىم</translation>
    </message>
</context>
<context>
    <name>MessageBoxPowerIntel</name>
    <message>
        <source>Nothing has been entered, re-enter</source>
        <translation type="vanished">ھېچنېمە كىرگۈزۈلمىدى، قايتا كىرىڭلار</translation>
    </message>
    <message>
        <source>Remind in 30 minutes</source>
        <translation type="vanished">30 مىنۇت ئىچىدە ئەسكەرتىپ قويۇش</translation>
    </message>
    <message>
        <source>Got it</source>
        <translation type="vanished">چۈشەندىم</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <source>MobileHotspot</source>
        <translation type="vanished">移动热点</translation>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <source>ukui control center</source>
        <translation type="vanished">控制面板</translation>
    </message>
    <message>
        <source>ukui control center desktop message</source>
        <translation type="vanished">控制面板桌面通知</translation>
    </message>
    <message>
        <source>start to close hotspot</source>
        <translation type="vanished">开始关闭热点</translation>
    </message>
    <message>
        <source>hotpots name or device is invalid</source>
        <translation type="vanished">热点名称或设备错误</translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">不能创建密码长度小于八位的热点！</translation>
    </message>
    <message>
        <source>start to open hotspot </source>
        <translation type="vanished">开始创建热点</translation>
    </message>
    <message>
        <source>Hotspot</source>
        <translation type="vanished">移动热点</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">开启</translation>
    </message>
    <message>
        <source>Wi-Fi Name</source>
        <translation type="vanished">Wi-Fi名称</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">网络密码</translation>
    </message>
    <message>
        <source>Frequency band</source>
        <translation type="vanished">网络频带</translation>
    </message>
    <message>
        <source>Net card</source>
        <translation type="vanished">共享网卡端口</translation>
    </message>
    <message>
        <source>hotspot already close</source>
        <translation type="vanished">热点已关闭</translation>
    </message>
    <message>
        <source>hotspot already open</source>
        <translation type="vanished">热点已开启</translation>
    </message>
</context>
<context>
    <name>MouseControl</name>
    <message>
        <source>Mouse Key Settings</source>
        <translation type="vanished">鼠标键设置</translation>
    </message>
    <message>
        <source>Hand habit</source>
        <translation type="vanished">惯用手</translation>
    </message>
    <message>
        <source>Pointer Settings</source>
        <translation type="vanished">指针设置</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="vanished">速度</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">慢</translation>
    </message>
    <message>
        <source>mouse wheel speed</source>
        <translation type="vanished">鼠标滚轮速度</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">快</translation>
    </message>
    <message>
        <source>Doubleclick  delay</source>
        <translation type="vanished">鼠标双击间隔时长</translation>
    </message>
    <message>
        <source>Short</source>
        <translation type="vanished">短</translation>
    </message>
    <message>
        <source>Long</source>
        <translation type="vanished">长</translation>
    </message>
    <message>
        <source>Acceleration</source>
        <translation type="vanished">鼠标加速</translation>
    </message>
    <message>
        <source>Cursor weight</source>
        <translation type="vanished">光标粗细</translation>
    </message>
    <message>
        <source>Sensitivity</source>
        <translation type="vanished">敏感度</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="vanished">低</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="vanished">高</translation>
    </message>
    <message>
        <source>Visibility</source>
        <translation type="vanished">按Ctrl键显示指针位置</translation>
    </message>
    <message>
        <source>Pointer size</source>
        <translation type="vanished">指针大小</translation>
    </message>
    <message>
        <source>Cursor Settings</source>
        <translation type="vanished">光标设置</translation>
    </message>
    <message>
        <source> Cursor weight</source>
        <translation type="obsolete">光标粗细</translation>
    </message>
    <message>
        <source>Thin</source>
        <translation type="vanished">细</translation>
    </message>
    <message>
        <source>Coarse</source>
        <translation type="vanished">粗</translation>
    </message>
    <message>
        <source>Cursor speed</source>
        <translation type="vanished">光标速度</translation>
    </message>
    <message>
        <source>Enable flashing on text area</source>
        <translation type="vanished">启用文本区域的光标闪烁</translation>
    </message>
    <message>
        <source>mouse</source>
        <translation type="vanished">鼠标</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">مائۇس</translation>
    </message>
    <message>
        <source>Lefthand</source>
        <translation type="vanished">左手</translation>
    </message>
    <message>
        <source>Righthand</source>
        <translation type="vanished">右手</translation>
    </message>
    <message>
        <source>Default(Recommended)</source>
        <translation type="vanished">默认（推荐）</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="vanished">中等</translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="vanished">较大</translation>
    </message>
</context>
<context>
    <name>MouseUI</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="25"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="194"/>
        <source>Mouse</source>
        <translation>مائۇس</translation>
        <extra-contents_path>/Mouse/Mouse</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="203"/>
        <source>Pointer</source>
        <translation>كۆرسەتكۈچى</translation>
        <extra-contents_path>/Mouse/Pointer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="212"/>
        <source>Cursor</source>
        <translation>بۈگۈر</translation>
        <extra-contents_path>/Mouse/Cursor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="275"/>
        <source>Dominant hand</source>
        <translation>ھۆكۈمرانلىق قول</translation>
        <extra-contents_path>/Mouse/Dominant hand</extra-contents_path>
    </message>
    <message>
        <source>Left hand</source>
        <translation type="vanished">سول قول</translation>
    </message>
    <message>
        <source>Right hand</source>
        <translation type="vanished">ئوڭ قول</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="278"/>
        <source>Left key</source>
        <translation>سول كۇنۇپكا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="279"/>
        <source>Right key</source>
        <translation>ئوڭ كۇنۇپكا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="307"/>
        <source>Scroll direction</source>
        <translation>سىيرىلما يۆنىلىش</translation>
        <extra-contents_path>/Mouse/Scroll direction</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="310"/>
        <source>Forward</source>
        <translation>ئالدىغا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="311"/>
        <source>Reverse</source>
        <translation>تەتۈر</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="340"/>
        <source>Wheel speed</source>
        <translation>چاق سۈرئىتى</translation>
        <extra-contents_path>/Mouse/Wheel speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="343"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="417"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="562"/>
        <source>Slow</source>
        <translation>ئاستا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="352"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="427"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="572"/>
        <source>Fast</source>
        <translation>روزا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="376"/>
        <source>Double-click interval time</source>
        <translation>قوش چېكىلىش ئارىلىقىدىكى ۋاقىت</translation>
        <extra-contents_path>/Mouse/Double-click interval time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="379"/>
        <source>Short</source>
        <translation>قىسقا</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="389"/>
        <source>Long</source>
        <translation>ئۇزۇن</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="414"/>
        <source>Pointer speed</source>
        <translation>نۇقتا تېزلىكى</translation>
        <extra-contents_path>/Mouse/Pointer speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="452"/>
        <source>Mouse acceleration</source>
        <translation>مائۇس تىزلەتمىسى</translation>
        <extra-contents_path>/Mouse/Mouse acceleration</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="474"/>
        <source>Show pointer position when pressing ctrl</source>
        <translation>ctrl باسقاندا كۆرسەتكۈچى ئورنىنى كۆرسىتىش</translation>
        <extra-contents_path>/Mouse/Show pointer position when pressing ctrl</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="497"/>
        <source>Pointer size</source>
        <translation>نۇقتا چوڭلۇقى</translation>
        <extra-contents_path>/Mouse/Pointer size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="500"/>
        <source>Small(recommend)</source>
        <translation>كىچىك(تەۋسىيە)</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="501"/>
        <source>Medium</source>
        <translation>ئوتتۇراھال</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="502"/>
        <source>Large</source>
        <translation>چوڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="536"/>
        <source>Blinking cursor in text area</source>
        <translation>تېكىست رايونىدا كۆزنى يۇمۇپ ئاچىدىغان كەرە</translation>
        <extra-contents_path>/Mouse/Blinking cursor in text area</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="559"/>
        <source>Cursor speed</source>
        <translation>ئىملا تېزلىكى</translation>
        <extra-contents_path>/Mouse/Cursor speed</extra-contents_path>
    </message>
</context>
<context>
    <name>MyLabel</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="130"/>
        <source>double-click to test</source>
        <translation>قوش چېكىپ سىناش</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <source>Netconnect Status</source>
        <translation type="vanished">网络状态</translation>
    </message>
    <message>
        <source>Waitting...</source>
        <translation type="vanished">加载中...</translation>
    </message>
    <message>
        <source>Available Network</source>
        <translation type="vanished">可用网络</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">刷新</translation>
    </message>
    <message>
        <source>open wifi</source>
        <translation type="vanished">打开WLAN</translation>
    </message>
    <message>
        <source>Wired Network</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation type="vanished">高级设置</translation>
    </message>
    <message>
        <source>Other Networks</source>
        <translation type="vanished">其他网络</translation>
    </message>
    <message>
        <source>open</source>
        <translation type="vanished">开启</translation>
    </message>
    <message>
        <source>Network settings</source>
        <translation type="vanished">网络设置</translation>
    </message>
    <message>
        <source>Change net settings</source>
        <translation type="vanished">更改网络设置</translation>
    </message>
    <message>
        <source>netconnect</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>Netconnect</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>Link</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>WiredConnect</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>card</source>
        <translation type="vanished">网卡</translation>
    </message>
    <message>
        <source>Add Wired Network</source>
        <translation type="vanished">添加有线网络</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="vanished">已连接</translation>
    </message>
    <message>
        <source>No net</source>
        <translation type="vanished">无连接</translation>
    </message>
    <message>
        <source>Detail</source>
        <translation type="vanished">网络详情</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>Refreshing...</source>
        <translation type="vanished">刷新中...</translation>
    </message>
    <message>
        <source>connected</source>
        <translation type="vanished">已连接</translation>
    </message>
    <message>
        <source>No network</source>
        <translation type="vanished">无网络连接</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">未连接</translation>
    </message>
</context>
<context>
    <name>NetDetail</name>
    <message>
        <source>SSID:</source>
        <translation type="vanished">SSID：</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="vanished">协议：</translation>
    </message>
    <message>
        <source>Security Type:</source>
        <translation type="vanished">安全类型：</translation>
    </message>
    <message>
        <source>Hz:</source>
        <translation type="vanished">网络频带：</translation>
    </message>
    <message>
        <source>Chan:</source>
        <translation type="vanished">网络通道：</translation>
    </message>
    <message>
        <source>Link Speed(rx/tx)</source>
        <translation type="vanished">链接速度(接收/传输):</translation>
    </message>
    <message>
        <source>Link Speed(rx/tx):</source>
        <translation type="vanished">链接速度(接收/传输):</translation>
    </message>
    <message>
        <source>BandWidth:</source>
        <translation type="vanished">带宽：</translation>
    </message>
    <message>
        <source>IPV4:</source>
        <translation type="vanished">IPV4：</translation>
    </message>
    <message>
        <source>IPV4 Dns:</source>
        <translation type="vanished">IPV4 Dns:</translation>
    </message>
    <message>
        <source>IPV4 GateWay:</source>
        <translation type="vanished">网关：</translation>
    </message>
    <message>
        <source>IPV4 Prefix:</source>
        <translation type="vanished">前缀：</translation>
    </message>
    <message>
        <source>IPV6:</source>
        <translation type="vanished">IPV6:</translation>
    </message>
    <message>
        <source>IPV6 Prefix:</source>
        <translation type="vanished">前缀：</translation>
    </message>
    <message>
        <source>IPV6 GateWay:</source>
        <translation type="vanished">网关：</translation>
    </message>
    <message>
        <source>Mac:</source>
        <translation type="vanished">物理地址：</translation>
    </message>
</context>
<context>
    <name>Notice</name>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="162"/>
        <source>NotFaze Mode</source>
        <translation>NotFaze ئەندىزىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="164"/>
        <source>(Notification banners, prompts will be hidden, and notification sounds will be muted)</source>
        <translation>(ئۇقتۇرۇش بايرىقى، ئەسكەرتمە يوشۇرۇنىدۇ، ئۇقتۇرۇش ئاۋازلىرى ئاۋازسىزلىنىدۇ)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="238"/>
        <source>Automatically turn on</source>
        <translation>ئاپتۇماتىك ئېچىلىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="263"/>
        <source>to</source>
        <translation>تو</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="288"/>
        <source>Automatically turn on when multiple screens are connected</source>
        <translation>كۆپ ئېكران ئۇلانغاندا ئاپتوماتىك ئېچىلىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="293"/>
        <source>Automatically open in full screen mode</source>
        <translation>پۈتۈن ئېكران ھالىتىدە ئاپتوماتىك ئېچىلىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="298"/>
        <source>Allow automatic alarm reminders in Do Not Disturb mode</source>
        <translation>دەخلى قىلماسلىق ھالىتىدە ئاپتوماتىك سىگنال بېرىش ئەسكەرتمىسىگە يول قويۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="329"/>
        <source>Notice Settings</source>
        <translation>ئۇقتۇرۇش تەڭشەكلىرى</translation>
        <extra-contents_path>/Notice/Notice Settings</extra-contents_path>
    </message>
    <message>
        <source>Set the type of notice in the operation center</source>
        <translation type="vanished">设置在通知中心显示的通知信息</translation>
    </message>
    <message>
        <source>Set notice type of operation center</source>
        <translation type="vanished">设置在通知中心显示的通知信息</translation>
    </message>
    <message>
        <source>Show new feature ater system upgrade</source>
        <translation type="vanished">系统版本更新后显示新增内容</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="331"/>
        <source>Get notifications from the app</source>
        <translation>ئەپتىن ئۇقتۇرۇش ئېلىش</translation>
        <extra-contents_path>/Notice/Get notifications from the app</extra-contents_path>
    </message>
    <message>
        <source>Show notifications on the lock screen</source>
        <translation type="vanished">在锁屏界面上显示通知</translation>
    </message>
    <message>
        <source>Notice Origin</source>
        <translation type="vanished">设置通知来源</translation>
    </message>
    <message>
        <source>notice</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="51"/>
        <source>Notice</source>
        <translation>ئۇقتۇرۇش</translation>
    </message>
</context>
<context>
    <name>NoticeMenu</name>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="41"/>
        <source>Beep sound when notified</source>
        <translation>ئۇقتۇرۇش قىلغاندا قوڭغۇراق ئاۋازى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="47"/>
        <source>Show message  on screenlock</source>
        <translation>ئېكران قۇلۇپىدا ئۇچۇر كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="53"/>
        <source>Show noticfication  on screenlock</source>
        <translation>ئېكران قۇلۇپىدا دېققىتنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="57"/>
        <source>Notification Style</source>
        <translation>ئۇقتۇرۇش ئۇسلۇبى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="65"/>
        <source>Banner: Appears in the upper right corner of the screen, and disappears automatically</source>
        <translation>پىلاكات: ئېكراننىڭ ئوڭ ئۈستۈنكى بۇلۇڭىدا كۆرۈنۈپ، ئاپتوماتىك يوقاپ كېتىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="70"/>
        <source>Tip:It will be kept on the screen until it is closed</source>
        <translation>ئەسكەرتىش: ئېكراندا تاقالغانغا قەدەر ساقلىنىپ قالىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="75"/>
        <source>None:Notifications will not be displayed on the screen, but will go to the notification center</source>
        <translation>يوق:ئۇقتۇرۇشلار ئېكراندا كۆرۈنمەيدۇ، بەلكى ئۇقتۇرۇش مەركىزىگە بارىدۇ</translation>
    </message>
</context>
<context>
    <name>NumbersButtonIntel</name>
    <message>
        <source>clean</source>
        <translation type="vanished">پاكىزە</translation>
    </message>
</context>
<context>
    <name>OutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="120"/>
        <source>resolution</source>
        <translation>ئېنىقلىما</translation>
        <extra-contents_path>/Display/resolution</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="156"/>
        <source>orientation</source>
        <translation>يۆلىنىش</translation>
        <extra-contents_path>/Display/orientation</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="174"/>
        <source>arrow-up</source>
        <translation>ئوقيا ئېتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="175"/>
        <source>90° arrow-right</source>
        <translation>90° ئوقيا ئوڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="177"/>
        <source>arrow-down</source>
        <translation>ئوقيا ئېتىش</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <source>Some applications need to be logouted to take effect</source>
        <translation type="vanished">部分程序需要注销生效</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="176"/>
        <source>90° arrow-left</source>
        <translation>90° ئوقيا سول</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="193"/>
        <source>frequency</source>
        <translation>چاستوتىسى</translation>
        <extra-contents_path>/Display/frequency</extra-contents_path>
    </message>
    <message>
        <source>refresh rate</source>
        <translation type="vanished">刷新率</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="394"/>
        <source>auto</source>
        <translation>ئاپتو</translation>
    </message>
    <message>
        <source>screen zoom</source>
        <translation type="vanished">ئېكراننى چوڭايت</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="513"/>
        <source>%1 Hz</source>
        <translation>٪1 Hz</translation>
    </message>
</context>
<context>
    <name>PassDialog</name>
    <message>
        <source>Get the phone binding code</source>
        <translation type="vanished">获取绑定手机验证码</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">请输入用户名</translation>
    </message>
    <message>
        <source>Your new password here</source>
        <translation type="vanished">新密码</translation>
    </message>
    <message>
        <source>Confirm your new password</source>
        <translation type="vanished">确认新密码</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">输入验证码</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">至少六位，包含大小写字母、数字</translation>
    </message>
    <message>
        <source>Your password is valid!</source>
        <translation type="vanished">您的密码是有效的！</translation>
    </message>
</context>
<context>
    <name>PhoneAuthIntelDialog</name>
    <message>
        <source>Wechat Auth</source>
        <translation type="vanished">ئۈندىدار ئەپ دىتالى</translation>
    </message>
    <message>
        <source>Phone Auth</source>
        <translation type="vanished">تېلېفون Auth</translation>
    </message>
    <message>
        <source>Phone number</source>
        <translation type="vanished">تېلېفۇن نۇمۇرى</translation>
    </message>
    <message>
        <source>SMS verification code</source>
        <translation type="vanished">SMS دەلىللەش كودى</translation>
    </message>
    <message>
        <source>GetCode</source>
        <translation type="vanished">GetCode</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">قايتىش</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Commit</source>
        <translation type="vanished">ئىش قىلىش</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>commit</source>
        <translation type="vanished">ئىش قىلىش</translation>
    </message>
    <message>
        <source>Mobile number acquisition failed</source>
        <translation type="vanished">كۆچمە نومۇر ئېلىش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <source>Recapture</source>
        <translation type="vanished">قايتا تۇتۇلۇش</translation>
    </message>
    <message>
        <source>Network connection failure, please check</source>
        <translation type="vanished">تور ئۇلاشتا كاشىلا كۆرۈلسە، تەكشۈرۈپ كۆرۈڭ</translation>
    </message>
    <message>
        <source>Phone is lock,try again in an hour</source>
        <translation type="vanished">تېلېفون قۇلۇپلۇق، بىر سائەتتىن كېيىن قايتا سىناپ بېقىڭ</translation>
    </message>
    <message>
        <source>Phone code is wrong</source>
        <translation type="vanished">تېلېفون نومۇرى خاتا</translation>
    </message>
    <message>
        <source>Current login expired,using wechat code!</source>
        <translation type="vanished">نۆۋەتتىكى كىرىش ۋاقتى ئۆتۈپ كەتتى،ئۈندىدار كودى ئارقىلىق!</translation>
    </message>
    <message>
        <source>Unknown error, please try again later</source>
        <translation type="vanished">نامەلۇم خاتالىق، سەل تۇرۇپ قايتا سىناپ بېقىڭ</translation>
    </message>
    <message>
        <source>Please use the correct wechat scan code</source>
        <translation type="vanished">توغرا ئۈندىداردىكى سىكاننىرلاش كودىنى ئىشلىتىڭ</translation>
    </message>
</context>
<context>
    <name>Power</name>
    <message>
        <source>select power plan</source>
        <translation type="vanished">电源计划</translation>
    </message>
    <message>
        <source>Balance (suggest)</source>
        <translation type="vanished">تەڭپۇڭلۇق (تەۋسىيە)</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">ساقلاۋاتىدۇ</translation>
    </message>
    <message>
        <source>Minimize performance</source>
        <translation type="vanished">尽可能降低计算机能耗</translation>
    </message>
    <message>
        <source>Bala&amp;nce (suggest)</source>
        <translation type="obsolete">
</translation>
    </message>
    <message>
        <source>Autobalance energy and performance with available hardware</source>
        <translation type="vanished">ئىشلىتىشكە بولىدىغان قاتتىق دېتال ئارقىلىق ئاپتوماتىك تەڭپۇڭلۇق ئېنېرگىيەسى ۋە ئىقتىدارى</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="vanished">自定义</translation>
    </message>
    <message>
        <source>Users develop personalized power plans</source>
        <translation type="vanished">ئابونتلار شەخسىي ھوقۇق پىلانى تۈزۈپ چىقىش</translation>
    </message>
    <message>
        <source>Power supply</source>
        <translation type="vanished">电源供给</translation>
    </message>
    <message>
        <source>Battery powered</source>
        <translation type="vanished">电池供给</translation>
    </message>
    <message>
        <source>Change PC sleep time:</source>
        <translation type="vanished">系统进入空闲状态并于此时间后睡眠：</translation>
    </message>
    <message>
        <source>Change DP close time:</source>
        <translation type="vanished">系统进入空闲状态并于此时间后关闭显示器：</translation>
    </message>
    <message>
        <source>Change pc sleep time:</source>
        <translation type="vanished">系统进入空闲状态并于此时间后挂起：</translation>
    </message>
    <message>
        <source>Change dp close time:</source>
        <translation type="vanished">系统进入空闲状态并于此时间后关闭显示器：</translation>
    </message>
    <message>
        <source>When close lid:</source>
        <translation type="vanished">关闭笔记本电脑上盖时：</translation>
    </message>
    <message>
        <source>Screen darkens use battery:</source>
        <translation type="vanished">无操作状态下于此时间后减小屏幕亮度：</translation>
    </message>
    <message>
        <source>Power Other Settings</source>
        <translation type="vanished">电源图标设置</translation>
    </message>
    <message>
        <source>S3 to S4 when:</source>
        <translation type="vanished">挂起此时间后转为睡眠：</translation>
    </message>
    <message>
        <source>Power Icon Settings</source>
        <translation type="vanished">电源图标设置</translation>
    </message>
    <message>
        <source>Power icon:</source>
        <translation type="vanished">电源图标：</translation>
    </message>
    <message>
        <source>power</source>
        <translation type="vanished">电源</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="62"/>
        <source>Power</source>
        <translation>ھوقۇق</translation>
    </message>
    <message>
        <source>Change PC sleep time after %1 min:</source>
        <translation type="vanished">系统%1分钟后进入空闲状态并于此时间后挂起：</translation>
    </message>
    <message>
        <source>Change DP close time after %1 min:</source>
        <translation type="vanished">系统%1分钟后进入空闲状态并于此时间后关闭显示器：</translation>
    </message>
    <message>
        <source>Enter idle state %1 min and sleep after %2 min :</source>
        <translation type="vanished">系统%1分钟后进入空闲状态并于%2分钟后挂起：</translation>
    </message>
    <message>
        <source>Enter idle state %1 min and close after %2 min :</source>
        <translation type="vanished">系统%1分钟后进入空闲状态并于%2分钟后关闭显示器：</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>never</source>
        <translation>مەڭگۈ</translation>
    </message>
    <message>
        <source>10 min</source>
        <translation type="vanished">10分钟</translation>
    </message>
    <message>
        <source>30 min</source>
        <translation type="vanished">30分钟</translation>
    </message>
    <message>
        <source>60 min</source>
        <translation type="vanished">60分钟</translation>
    </message>
    <message>
        <source>120 min</source>
        <translation type="vanished">120分钟</translation>
    </message>
    <message>
        <source>300 min</source>
        <translation type="vanished">300分钟</translation>
    </message>
    <message>
        <source>20 min</source>
        <translation type="vanished">20分钟</translation>
    </message>
    <message>
        <source>1 min</source>
        <translation type="vanished">1分钟</translation>
    </message>
    <message>
        <source>5 min</source>
        <translation type="vanished">5分钟</translation>
    </message>
    <message>
        <source>Require password when suspend/hibernation</source>
        <translation type="vanished">ئۇيقۇ/ئۇيقۇ ھالىتىدە مەخپىي نومۇر تەلەپ قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="606"/>
        <source>Require password when suspend</source>
        <translation>توختىتىپ قويۇشتا پارول تەلەپ قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="605"/>
        <source>Require password when suspend/hibernate</source>
        <translation>ۋاقىتنى توختىتىپ قويۇش/hibernate دا پارول تەلەپ قىلىش</translation>
        <extra-contents_path>/Power/Require password when suspend/hibernate</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="254"/>
        <source>The system will sleep before turning off the display</source>
        <translation>كۆرۈنمە يۈزنى ئۆچۈرۈشتىن بۇرۇن سېستىما ئۇخلايدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="326"/>
        <source>Reduce the occupation of backend running program resources and ensure smooth operation of key and focus applications.</source>
        <translation>ئارقا قوشۇمچە ئىجرا قىلىش پىروگرامما بايلىقى كەسپىنى ئازايتىپ، نۇقتىلىق ۋە فوكۇس قوللىنىشنىڭ ئوڭۇشلۇق ئىشلىنىشىگە كاپالەتلىك قىلىش كېرەك.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="607"/>
        <source>Require password when hibernate</source>
        <translation>تىزىمغا ئالدۇرغاندا پارول تەلەپ قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="616"/>
        <location filename="../../../plugins/system/power/power.cpp" line="617"/>
        <source>Password required when waking up the screen</source>
        <translation>ئېكراننى ئويغىتىشتا زۆرۈر بولغان پارول</translation>
        <extra-contents_path>/Power/Password required when waking up the screen</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="621"/>
        <source>Press the power button</source>
        <translation>توك كۇنۇپكىسىنى بېسىش</translation>
        <extra-contents_path>/Power/Press the power button</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="626"/>
        <location filename="../../../plugins/system/power/power.cpp" line="627"/>
        <source>Time to close display</source>
        <translation>كۆرسىتىشنى تاقاش ۋاقتى</translation>
        <extra-contents_path>/Power/Time to close display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="631"/>
        <location filename="../../../plugins/system/power/power.cpp" line="632"/>
        <source>Time to sleep</source>
        <translation>ئۇخلاش ۋاقتى</translation>
        <extra-contents_path>/Power/Time to sleep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="636"/>
        <location filename="../../../plugins/system/power/power.cpp" line="637"/>
        <source>Notebook cover</source>
        <translation>خاتىرە كومپىيۇتېر مۇقاۋىسى</translation>
        <extra-contents_path>/Power/Notebook cover</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="641"/>
        <location filename="../../../plugins/system/power/power.cpp" line="642"/>
        <source>Dynamic resource scheduling</source>
        <translation>ھەرىكەتچان بايلىق سىخېمىسى</translation>
        <extra-contents_path>/Power/Dynamic resource scheduling</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="646"/>
        <location filename="../../../plugins/system/power/power.cpp" line="647"/>
        <source>Using power</source>
        <translation>توك ئىشلىتىش</translation>
        <extra-contents_path>/Power/Using power</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="651"/>
        <location filename="../../../plugins/system/power/power.cpp" line="652"/>
        <source>Using battery</source>
        <translation>باتارېيە ئىشلىتىش</translation>
        <extra-contents_path>/Power/Using battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="655"/>
        <location filename="../../../plugins/system/power/power.cpp" line="656"/>
        <source> Time to darken</source>
        <translation> قاراڭغۇلۇققا پاتىدىغان ۋاقىت</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="660"/>
        <source>Battery level is lower than</source>
        <translation>باتارېيە دەرىجىسى ئۇنىڭدىن تۆۋەن</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="663"/>
        <source>Run</source>
        <translation>يۈگۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="666"/>
        <location filename="../../../plugins/system/power/power.cpp" line="667"/>
        <source>Low battery notification</source>
        <translation>باتارېيە مىقدارى تۆۋەن بولۇش ئۇقتۇرۇشى</translation>
        <extra-contents_path>/Power/Low battery notification</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="671"/>
        <source>Automatically run saving mode when low battery</source>
        <translation>باتارىيە ئاز بولغاندا تېجەش ھالىتىنى ئاپتوماتىك ئىجرا قىلىش</translation>
        <extra-contents_path>/Power/&quot;Automatically run saving mode when low battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="672"/>
        <source>Automatically run saving mode when the low battery</source>
        <translation>باتارىيە تۆۋەن بولغاندا تىجەش ھالىتىنى ئاپتوماتىك ئىجرا قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="675"/>
        <location filename="../../../plugins/system/power/power.cpp" line="676"/>
        <source>Automatically run saving mode when using battery</source>
        <translation>باتارىيە ئىشلەتكەندە تېجەش ھالىتىنى ئاپتوماتىك ئىجرا قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="679"/>
        <location filename="../../../plugins/system/power/power.cpp" line="680"/>
        <source>Display remaining charging time and usage time</source>
        <translation>ئېشىپ قالغان توك قاچىلاش ۋاقتى ۋە ئىشلىتىش ۋاقتىنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="164"/>
        <source>General</source>
        <translation>ارينا</translation>
        <extra-contents_path>/Power/General</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="349"/>
        <source>Select Powerplan</source>
        <translation>Powerplan تاللاش</translation>
        <extra-contents_path>/Power/Select Powerplan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="407"/>
        <source>Battery saving plan</source>
        <translation>باتارېيە تېجەش پىلانى</translation>
        <extra-contents_path>/Power/Battery saving plan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>nothing</source>
        <translation>ھېچنېمە</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>blank</source>
        <translation>قۇرۇق</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>suspend</source>
        <translation>توختىتىپ قويۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>hibernate</source>
        <translation>hibernate</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <source>interactive</source>
        <translation>ئۆز ئارا تەسىرلىشىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <source>5min</source>
        <translation>5min</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Balance</source>
        <translation>تەڭپۇڭلۇق</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Energy Efficiency</source>
        <translation>ئېنېرگىيە ئۈنۈمى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Performance</source>
        <translation>ئويۇن قويۇش</translation>
    </message>
    <message>
        <source>Performance Model</source>
        <translation type="vanished">ئىقتىدار ئۈلگىسى</translation>
    </message>
    <message>
        <source>20min</source>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <source>10minn</source>
        <translation type="vanished">10分钟</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>15min</source>
        <translation>15min</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>30min</source>
        <translation>30min</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>1h</source>
        <translation>1ھ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>2h</source>
        <translation>2ھ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>10min</source>
        <translation>10min</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>3h</source>
        <translation>3ھ</translation>
    </message>
    <message>
        <source>Balance Model</source>
        <translation type="vanished">تەڭپۇڭلۇق مودېلى</translation>
    </message>
    <message>
        <source>Save Model</source>
        <translation type="vanished">مودېلنى ساقلاش</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">1min</translation>
    </message>
    <message>
        <source>3min</source>
        <translation type="vanished">3分钟</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>shutdown</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <source>always</source>
        <translation type="vanished">显示电源图标在托盘栏</translation>
    </message>
    <message>
        <source>present</source>
        <translation type="vanished">仅当存在电池时显示</translation>
    </message>
    <message>
        <source>charge</source>
        <translation type="vanished">仅当使用电池时显示</translation>
    </message>
    <message>
        <source>Perform operations when battery is low:</source>
        <translation type="vanished">低电量执行操作:</translation>
    </message>
    <message>
        <source>General Settings</source>
        <translation type="vanished">通用设置</translation>
    </message>
    <message>
        <source>When the power button is pressed:</source>
        <translation type="vanished">按电源键时执行:</translation>
    </message>
</context>
<context>
    <name>Printer</name>
    <message>
        <source>Add Printers And Scanners</source>
        <translation type="vanished">添加打印机和扫描仪</translation>
    </message>
    <message>
        <source>Add printers and scanners</source>
        <translation type="vanished">添加打印机和扫描仪</translation>
    </message>
    <message>
        <source>Attrs</source>
        <translation type="vanished">属性</translation>
    </message>
    <message>
        <source>Attributes</source>
        <translation type="vanished">属性</translation>
    </message>
    <message>
        <source>List Of Existing Printers</source>
        <translation type="vanished">可用打印机列表</translation>
    </message>
    <message>
        <source>printer</source>
        <translation type="vanished">打印机</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="43"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="133"/>
        <source>Printers</source>
        <translation>پرىنتېرلاش</translation>
        <extra-contents_path>/Printer/Printers</extra-contents_path>
    </message>
    <message>
        <source>Printers And Scanners</source>
        <translation type="vanished">پرىنتېرلاش ۋە سىكاننېرلاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="179"/>
        <source>Add</source>
        <translation>قوش</translation>
        <extra-contents_path>/Printer/Add</extra-contents_path>
    </message>
</context>
<context>
    <name>PrivacyDialog</name>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="11"/>
        <source>Set</source>
        <translation>بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="26"/>
        <source>End User License Agreement and Privacy Policy Statement of Kylin</source>
        <translation>ئاخىرقى ئىشلەتكۈچىلەر ئىجازەتنامىسى كېلىشىمى ۋە جېننىڭ مەخپىيەتلىك سىياسىتى باياناتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="31"/>
        <source>Dear users of Kylin operating system and relevant products, 
         This agreement describes your rights, obligations and prerequisites for your use of this product. Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”).
        “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products.

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021
        The Agreement shall include the following content:
        I.     User license 
        II.    Java technology limitations
        III.   Cookies and other technologies
        IV.    Intellectual property clause
        V.     Open source code
        VI.   The third-party software/services
        VII.  Escape clause
        VIII. Integrity and severability of the Agreement
        IX.    Applicable law and dispute settlement

        I.      User license
        According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
        1.     User license for educational institutions
        In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product.
        2.     Use of the font software
        Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

        II.    Java technology limitations
        You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

        III.   Cookies and other technologies
        In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
        If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers.
        In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

        IV.   Intellectual property clause
        1.    Trademarks and Logos
        This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
        2.    Duplication, modification and distribution
        If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
        Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws.

        V.    Open source code
        For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

        VI.  The third-party software/services
        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
        We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

        VII. Escape clause
        1.    Limited warranty
        We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
        2.   Disclaimer
        In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
        3.   Limitation of responsibility
        To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

        VIII.Integrity and severability of the Agreement
        1.    The integrity of the Agreement
        The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
        2.   Severability of the Agreement
        If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

        IX.  Applicable law and dispute settlement
        1.   Application of governing laws
        Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
        2.  Termination
        If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
        The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.
        The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

        Privacy Policy Statement of Kylin Operating System/n        Release date of the version: July 30, 2021
        Effective date of the version: July 30, 2021

        We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.

        The Statement shall include the following content:
        I.   Collection and use your personal information
        II.  How to store and protect your personal information
        III. How to manage your personal information
        IV.  Privacy of the third-party software/services
        V.   Minors’ use of the products
        VI.  How to update this Statement
        VII. How to contact us

        I.     How to collect and use your personal information
        1.    The collection of personal information
        We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
        1)   The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
        2)   Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
        3)   Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
        4)   Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
        5)   The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
        6)   This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
        7)   The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
        8)   In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.
        2.   Use of personal information
        We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
        1)   The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
        2)   We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
        3)   We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
        4)   We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
        5)   We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
        6)   We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications andother channels;
        7)   In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.
        3.   Information sharing and provision
        We shall not share or transfer your personal information to any third party, except for the following circumstances:
        1)   After obtaining your clear consent, we shall share your personal information with the third parities;
        2)   In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
        3)   We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
        4)   With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
        5)   If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.
        4.   Exceptions with authorized consent
        1)   It is directly related to national security, national defense security and other national interests; 
        2)   It is directly related to public safety, public health and public knowledge and other major public interests; 
        3)   It is directly related to crime investigation, prosecution, judgment and execution of judgment; 
        4)   It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent; 
        5)   The personal information collected is disclosed to the public by yourself; 
        6)   Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels; 
        7)   It is necessary to sign and perform of the contract according to your requirement; 
        8)   It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
        9)   It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
        10) Other circumstances specified in the laws and regulations.

        II.   How to store and protect personal information
        1.   Information storage place
        We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
        2.   Information storage duration 
        Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
        When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
        3.   How to protect the information
        We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
        We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
        We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
        4.   Emergency response plan
        In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

        III. How to manage your personal information
        If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs. 
        Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

        IV.  Privacy of the third-party software/services

        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
        When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

        V.   Minors’ use of the products
        If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

        VI.  How to update this Statement
        We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the accountcreated by you in this product; if you are a guardian, please help your minor child to close the account created by him/her in this product.

        VII. How to contact us
        If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn). 
        We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
        The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
        Last date of updating: November 1, 2021

Address:
        Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
        Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
        Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
        Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.:
        Tianjin (022) 58955650      Beijing (010) 51659955
        Changsha (0731) 88280170        Shanghai (021) 51098866
Fax:
        Tianjin (022) 58955651      Beijing (010) 62800607
        Changsha (0731) 88280166        Shanghai (021) 51062866

        Company website: www.kylinos.cn
        E-mail: support@kylinos.cn</source>
        <translation>جېننىڭ مەشغۇلات سىستېمىسى ۋە مۇناسىۋەتلىك مەھسۇلاتلارنىڭ قەدىرلىك ئابۇنتلىرى ، 
         بۇ كېلىشىمدە مەزكۇر مەھسۇلاتنى ئىشلىتىش ھوقۇقىڭىز، مەجبۇرىيىتىڭىز ۋە ئالدىنقى شەرتلىرىڭىز بايان قىلىنغان. كېلىشىم ماددىلىرى ۋە قوشۇمچە ئىجازەتنامىسى (تۆۋەندە كوللېكتىپ ھالدا «كېلىشىم» دېيىلىدۇ) ۋە Kylin مەشغۇلات سىستېمىسىنىڭ مەخپىيەتلىك سىياسەت باياناتى (تۆۋەندە «بايانات» دېيىلىدۇ)نى ئوقۇپ بېقىڭ.
        كېلىشىمدىكى «بۇ مەھسۇلات» ۋە «بايانات»دە، «Kylin مەشغۇلات سىستېمىسى يۇمشاق دېتال مەھسۇلاتى» نى تەتقىق قىلىپ ياساپ چىققان، ئىشلەپچىقارغان ۋە ئېلان قىلغان ھەمدە ئىشخانا خىزمىتىنى بىر تەرەپ قىلىش ياكى كارخانا ۋە ھۆكۈمەتلەرنىڭ ئۇچۇر ئۇل ئەسلىھەسىنى قۇرۇش ئۈچۈن ئىشلىتىلگەن «جىلۋىن مەشغۇلات سىستېمىسى يۇمشاق دېتال مەھسۇلاتى» نى كۆرسىتىدۇ. «بىز» بولسا Kylinsoft چەكلىك مەسئۇلىيەت شىركىتى «سىز» دېگەن مەنىنى بىلدۈرىدىغان بولۇپ، كىنىشكا ھەققىنى تۆلەيدىغان ۋە Kylin مەشغۇلات سىستېمىسى ۋە مۇناسىۋەتلىك مەھسۇلاتلارنى ئىشلەتكەن ئابونتلارنى كۆرسىتىدۇ.

Kylin نىڭ ئاخىرقى ئىشلەتكۈچى ئىجازەت كېلىشىمى 
نەشرى تارقىتىلغان ۋاقتى: 2021-يىل 30-ئىيۇل
نەشرىنىڭ ئۈنۈملۈك ۋاقتى: 2021-يىل 30-ئىيۇل
        كېلىشىمدە تۆۋەندىكى مەزمۇن بار:
        I.     ئىشلەتكۈچى ئىجازەتنامىسى 
        II. Java تېخنىكىسى چەكلىمىسى
        III. پېچىنە-پىرەنىك قاتارلىق تېخنىكىلار
        IV. بىلىم مۈلۈك ھوقۇقى ماددىسى
        V.     ئېچىش مەنبە كودى
        VI. ئۈچىنچى تەرەپ يۇمشاق دېتالى/مۇلازىمىتىر
        VII. قېچىش ماددىسى
        VIII. كېلىشىمنىڭ پۈتۈنلۈكى ۋە ئېغىرلىقى
        IX. قوللىنىشچان قانۇن ۋە ماجىرالارنى ھەل قىلىش

I.      ئىشلەتكۈچى ئىجازەتنامىسى
        بۇ مەھسۇلات ۋە كومپيۇتېر قاتتىق دېتالى تۈرىگە ھەق تۆلىگەن ئىشلەتكۈچىلەرنىڭ سانى ۋە كومپيۇتېر قاتتىق دېتاللىرىنىڭ سانىغا ئاساسەن، ئىجازەتنامە ئالغان ئورۇن ۋە ئىشچى-خىزمەتچىلەرگە قوشۇمچە يۇمشاق دېتال (تۆۋەندە دېيىلىدۇ) قوشۇمچە يۇمشاق دېتال (تۆۋەندە دېيىلىدۇ) ۋە جېننسوفت تەمىنلىگەن خاتالىقنى تۈزىتىپ ئىشلىتىشكە ئىجازەت بېرىمىز.
        1.مائارىپ ئاپپاراتلىرىنىڭ ئىشلەتكۈچى كىنىشكىسى
        كېلىشىمنامىنىڭ ماددىلىرى ۋە شەرتلىرىگە رىئايە قىلغان ئەھۋالدا، ئەگەر سىز مائارىپ ئورگىنى بولسىڭىز، ئورگان تەرەپ قوشۇلمىغان ئىككىلىك فورماتتىكى يۇمشاق دېتالنى ئىشلىتىشىگە ۋە پەقەت ئىچكى قىسىمدا ئىشلىتىشىگە رۇخسەت قىلىنىدۇ. بۇ يەردىكى «ئىچكى پايدىلىنىش ئۈچۈن» دېگىنىمىز، كىنىشكىسى بار ئورۇن ۋە ئىشچى-خىزمەتچىلەرنىڭ ئىدارى بىلەن ئەمگەك توختامى ئىمزالىشى، شۇنداقلا سىلەرنىڭ ئورگان تەرەپ تىزىملاتقان ئوقۇغۇچىلار بۇ مەھسۇلاتنى ئىشلەتسە بولىدىغانلىقىنى كۆرسىتىدۇ.
        2.خەت نۇسخىسى يۇمشاق دېتالىنى ئىشلىتىش
        خەت نۇسخىسى يۇمشاق دېتالى مەھسۇلاتقا ئالدىن قاچىلانغان ۋە خەت نۇسخىسى ئۇسلۇبى ھاسىل قىلىدىغان يۇمشاق دېتالنى كۆرسىتىدۇ. خەت نۇسخىسى يۇمشاق دېتالىنى يۇمشاق دېتالدىن ئايرىيالمايسىز ھەمدە مەزكۇر خەت نۇسخىسى يۇمشاق دېتالىنىڭ مەزكۇر مەھسۇلاتنىڭ بىر قىسمى بولۇش سۈپىتى بىلەن، سىزگە يەتكۈزۈلگەن ۋاقىتتا بولمىغان ھەرقانداق ئىقتىدارنى قوشۇشقا ئۇرۇنۇش ئارقىلىق خەت نۇسخىسى يۇمشاق دېتالىنى ئۆزگەرتەلمەيسىز، ياكى ھەرقانداق ھەق ياكى باشقا ھەقلىق سودا مەھسۇلاتى سۈپىتىدە تەمىنلەنگەن ھۆججەتلەرگە خەت نۇسخىسى يۇمشاق دېتالىنى قىستۇرالمايسىز ،  ياكى بۇ مەھسۇلات ئورنىتىلمىغان ئۈسكۈنىلەردە ئىشلىتىشكە بولمايدۇ. ئەگەر سىز خەت نۇسخىسى يۇمشاق دېتالىنى سىرتقى تەشۋىقات قاتارلىق باشقا سودا مەقسەتلىرى ئۈچۈن ئىشلەتسىڭىز، مۇناسىۋەتلىك قىلمىشلىرىڭىزنىڭ رۇخسىتىنى ئېلىش ئۈچۈن خەت نۇسخىسى نەشر ھوقۇقى ياسىمىچىلىقى بىلەن ئالاقىلىشىپ ۋە سۆھبەتلىشىپ بېقىڭ.

II. Java تېخنىكىسى چەكلىمىسى
        JPI دا قوشۇمچە دەرسلەرنى قۇرۇش ياكى باشقا ئۇسۇلدا JPI دا دەرس قوشۇش ياكى ئۆزگەرتىش ئارقىلىق بولسۇن، «Java Platform Interface» (يەنى &quot;JPI&quot; دەپ ئاتىلىدۇ) نى ئۆزگەرتەلمەيسىز. ئەگەر سىز قوشۇمچە دەرسنى ھەم بىر ياكى بىر قانچە مۇناسىۋەتلىك API نى قۇرسىڭىز، ئۇلار (i) Java سۇپىسىنىڭ فۇنكسىيەسىنى كېڭەيتسە؛ ھەمدە (ii) ئۈچىنچى تەرەپ يۇمشاق دېتال ئاچقۇچىلار تەرىپىدىن يۇقارقى قوشۇمچە ئايفونلارنى چاقىرىش مۇمكىن بولغان قوشۇمچە يۇمشاق دېتاللارنى ئېچىش ئۈچۈن ئىشلىتىلىشى مۇمكىن، سىز دەرھال بارلىق ئاچقۇچىلارنىڭ ھەقسىز ئىشلىتىشى ئۈچۈن بۇنداق ApIs نىڭ توغرا چۈشەندۈرۈشىنى دەرھال ئېلان قىلىشىڭىز كېرەك. باشقا ئىجازەتنامىلەرگە «java»، «java»، «javax» ۋە «قۇياش» دەپ بەلگە قويۇلغان قوشۇمچە تۈر، ئارايۈز ياكى تارماق بوغچىلارنى ھەرقانداق ئۇسۇلدا ياكى سۈن فامىلە كېلىشىمىدە كۆرسىتىلگەن مۇشۇنىڭغا ئوخشاش كېلىشىملەرنى قۇرۇشقا بولمايدۇ. Java Runtime Environment ئىككىلىك كود ئىجازەتنامىسى (ھازىرقى http://jdk.java.net جايلاشقان) نىڭ مۇۋاپىق نۇسخىسىنى كۆرۈپ، Java mini پروگراممىلىرى ۋە ئەپلىرى بىلەن ئورتاق تارقىتىلغان runtime كودىنىڭ بار-يوقلۇقىنى چۈشىنىپ بېقىڭ.

III. پېچىنە-پىرەنىك قاتارلىق تېخنىكىلار
        ئابونتلارنى تېخىمۇ ياخشى چۈشىنىشىمىز ۋە مۇلازىمەت قىلىشىمىزغا ياردەم بېرىش ئۈچۈن تور بېكىتىمىز، تور مۇلازىمىتىمىز ۋە قوللىنىشچان پروگراممىلىرىمىز «Cookie» تېخنىكىسىدىن پايدىلىنىشى مۇمكىن. بۇنداق Cookie لار تور ئېقىم مىقدارىنى سېستىمىغا كىرىپ چىققان ۋە تەكشۈرۈش خاتالىقى سەۋەبىدىن ھاسىل بولغان ئېقىم مىقدارىنى ساقلاشقا ئىشلىتىلىدۇ، شۇڭا چوقۇم بېكىتىش كېرەك. بىز سىزنىڭ بۇنداق Cookie لارنى ئىشلىتىش ئارقىلىق تور بېكىتىمىز ۋە تور مۇلازىمىتىمىز بىلەن قانداق ئالاقە قىلغانلىقىڭىزنى چۈشىنىمىز.
        Cookie نى ئۆچۈرۈپ Firefox تور كۆرگۈچىنى ئىشلەتمەكچى بولسىڭىز Firefox نىڭ مەخپىيەتلىك ۋە بىخەتەرلىك مەركىزىدە تەڭشەپ قويسىڭىز بولىدۇ. ئەگەر باشقا تور كۆرگۈچلەرنى ئىشلەتسىڭىز مۇناسىۋەتلىك تەمىنلىگۈچىلەردىن كونكىرت لايىھەلەردىن مەسلىھەت سوراڭ.
        ‹‹جۇڭخۇا خەلق جۇمھۇرىيىتىنىڭ تور بىخەتەرلىكى قانۇنى››نىڭ 76 – ماددىسىدىكى بەلگىلىمىگە ئاساسەن، شەخسىي ئۇچۇر</translation>
    </message>
    <message>
        <source>Dear users of Kylin operating system and relevant products,

   Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”). When you click the next step to confirm your complete understanding of the content, it shall indicate that you have accepted the clauses of the Agreement, and the Agreement shall take effect immediately and be legally binding on you and the Company.
   “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products. 

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021 

The Agreement shall include the following content:
I. 	User license 
II. 	Java technology limitations
III. 	Cookies and other technologies
IV. 	Intellectual property clause
V. 	Open source code
VI. 	The third-party software/services
VII. Escape clause
VIII. Integrity and severability of the Agreement
IX. 	Applicable law and dispute settlement

   I. 	User license

   According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
   1.	User license for educational institutions
   In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product. 
   2.	Use of the font software
   Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

   II. 	Java technology limitations 

   You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

   III. 	Cookies and other technologies

   In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
   If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers. 
   In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

   IV. 	Intellectual property clause

   1.	Trademarks and Logos
   This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
  2.	Duplication, modification and distribution
   If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
   Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws. 

   V. 	Open source code

   For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

   VI.  The third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
   We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

   VII. 	Escape clause

   1.	Limited warranty
    We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
   2.	Disclaimer
   In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
   3.	Limitation of responsibility
   To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

   VIII. 	Integrity and severability of the Agreement

   1.	The integrity of the Agreement
  The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
   2.	Severability of the Agreement
   If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

   IX. 	Applicable law and dispute settlement

   1.	Application of governing laws
   Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
   2.	Termination
   If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
   The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.

   The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

Privacy Policy Statement of Kylin Operating System
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021

We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.
   The Statement shall include the following content:
    I. 	Collection and use your personal information
   II. 	How to store and protect your personal information
   III. 	How to manage your personal information
   IV. 	Privacy of the third-party software/services
   V. 	Minors’ use of the products
   VI. 	How to update this Statement
   VII. How to contact us

    I. 	How to collect and use your personal information

   1.	The collection of personal information
   We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
   1)	The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
   2)	Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
   3)	Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
   4)	Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
   5)	The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
   6)	This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
   7)	The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
   8)	In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.

  2.	Use of personal information
   We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
   1)	The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
   2)	We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
   3)	We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
   4)	We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
   5)	We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
   6)	We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications and other channels;
   7)	In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.

   3.	Information sharing and provision
   We shall not share or transfer your personal information to any third party, except for the following circumstances:
   1)	After obtaining your clear consent, we shall share your personal information with the third parities;
   2)	In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
    3)	We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
   4)	With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
    5)	If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.

   4.	Exceptions with authorized consent
   1)	It is directly related to national security, national defense security and other national interests;
   2)	It is directly related to public safety, public health and public knowledge and other major public interests;
   3)	It is directly related to crime investigation, prosecution, judgment and execution of judgment;
   4)	It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent;
   5)	The personal information collected is disclosed to the public by yourself;
   6)	Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels;
   7)	It is necessary to sign and perform of the contract according to your requirement;
   8)	It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
   9)	It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
   10)	Other circumstances specified in the laws and regulations.

   II. 	How to store and protect personal information

   1.	Information storage place
   We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
   2.	Information storage duration
  Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
   When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
   3.	How to protect the information
   We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
   We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
   We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
   4.	Emergency response plan
    In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

   III. 	How to manage your personal information

   If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs.
   Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

   IV. 	Privacy of the third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
   When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

   V. 	Minors’ use of the products

   If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

   VI. 	How to update this Statement

   We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the account created by you in this product; if you are a  guardian, please help your minor child to close the account created by him/her in this product.

   VII. 	How to contact us

  If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn).
   We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
   The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
   Last date of updating: November 1, 2021

Address: Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
             Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
             Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
             Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.: Tianjin (022) 58955650       Beijing (010) 51659955
             Changsha (0731) 88280170    Shanghai (021) 51098866
Fax: Tianjin (022) 58955651       Beijing (010) 62800607
             Changsha (0731) 88280166    Shanghai (021) 51062866

Company website: www.kylinos.cn
E-mail: support@kylinos.cn</source>
        <translation type="vanished">尊敬的银河麒麟操作系统及相关产品用户:
   请您仔细阅读本协议条款、补充许可条款（统称“协议”）及银河麒麟操作系统隐私政策声明（以下简称“声明”）。当您确认了解并点击下一步时，即表明您已接受本协议的条款，本协议将立即生效，对您和本公司双方具有法律约束力。
   本协议及声明中的“本产品”是指由麒麟软件有限公司开发并制作发行的用于办公或构建企业及政府的信息化基础设施——“银河麒麟操作系统软件产品”。“我们”是指麒麟软件有限公司。“您”是指支付授权费用并使用银河麒麟操作系统及相关产品的用户。

银河麒麟最终用户使用许可协议
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

本协议将向您说明以下内容：
一、使用许可
二、Java技术限制
三、Cookie和其他技术
四、知识产权条款
五、开放源代码说明
六、第三方软件/服务说明
七、免责条款
八、协议完整性及可分割性说明
九、适用法律及争议解决
一、使用许可

    按照已经为本产品支付费用的用户数目及计算机硬件类型，我们向您授予非排他、不可转让的许可，仅允许被授权人单位及与其签订劳动合同的员工使用由麒麟软件提供的随附软件和文档以及任何错误纠正。
   1.教育机构使用许可
    在遵守本协议的条款和条件的情况下，如果您是教育机构，允许贵机构仅在内部使用随附的未经修改的二进制格式的软件。此处的“在内部使用”是指被授权人单位及与其签订劳动合同的员工以及在贵机构入学的学生使用本产品。 
   2.字型软件使用
   字型软件指本产品中预装的和生成字体样式的软件。您不可从软件中分离字型软件，不可改动字型软件，以新增此等字型软件被作为本产品的一部分交付予您时所不具备的任何功能，不可将字型软件嵌入作为商业产品提供以换取收费或其他报酬的文件、不可脱离安装了本产品的机器使用。如将字型软件用于对外宣传等其他商业用途时，请您与字体版权厂商联系协商以获得对您相关行为的许可。

二、Java技术限制

   您不可更改“Java平台界面”（简称“JPI”，即指明为“java”包或“java”包的任何子包中的类），无论通过在JPI中创建额外的类，还是通过其他方式导致对JPI中的类进行增添或更动，均为不可。如果您创建一个额外的类以及一个或多个相关的API，而它们（i）扩展Java平台的功能；并且（ii）可供第三方软件开发者用于开发可调用上述额外API的额外软件，则您必须迅即广泛公布对此种API的准确说明，以供所有开发者免费使用。您不可创建、或授权其他被许可人创建以任何方式标示为“java”、“javax”、“sun”的额外的类、界面、子包或Sun在任何命名约定中指明的类似约定。参见Java运行时环境二进制代码许可的适当版本（目前位于http://jdk.java.net），以了解可与Java小程序和应用程序共同分发的运行时代码的可供情况。

三、Cookie和其他技术

   为帮助我们更好地了解并服务用户，我们的网站、在线服务和应用程序可能会使用“Cookie”技术。这些Cookie用于存储进出系统的网络流量以及因检测错误而生成的流量，因此必须设置。 我们通过使用这些Cookie来了解您与我们的网站和在线服务如何进行交互。 
   如果您想禁用 Cookie 并且使用的是Firefox浏览器，可在Firefox的隐私与安全中心进行设置。如果您使用的是其他浏览器，请向相关供应商咨询具体方案。
依照《中华人民共和国网络安全法》第七十六条第五款，个人信息，是指以电子或者其他方式记录的能够单独或者与其他信息结合识别自然人个人身份的各种信息，包括但不限于自然人的姓名、出生日期、身份证件号码、个人生物识别信息、住址、电话号码等。如果Cookie中包含上述信息，或者存在通过Cookie收集的非个人信息与其他个人信息合并后的信息，出于本隐私政策的目的，我们会将合并后的信息视为个人隐私信息，将参照银河麒麟隐私政策声明，为您的个人信息提供相应的安全保护措施。

四、知识产权条款

   1.商标和标识
    本产品受到版权（著作权）法、商标法和其他法律及国际知识产权公约的保护。我们或其许可方保留对本产品的所有权及所有相关的知识产权。对于我们或其许可方的任何商标、服务标记、标识或商号的任何权利、所有权或利益，本协议均不作任何授权。您对麒麟软件标记的任何使用都应有利于麒麟软件，未经我们书面同意，不得擅自使用麒麟软件任何商标、标识。 
   2.关于复制、修改及分发
   如果在所有复制品中维持本协议书不变，您可以且必须根据《GNU GPL-GNU通用公共许可证》复制、修改及分发银河麒麟操作系统软件产品中遵守《GNU GPL-GNU通用公共许可证》协议的软件，其他不遵守《GNU GPL-GNU通用公共许可证》协议的银河麒麟操作系统软件产品必须根据相关法律、其他许可协议进行复制、修改及分发，但任何以银河麒麟操作系统软件产品为基础的衍生发行版未经我们的书面授权不能使用任何我们的商标或其他任何标志。
   特别注意：该复制、修改及分发不包括本产品中包含的任何不适用《GNU GPL-GNU通用公共许可证》的软件，如银河麒麟操作系统软件产品中包含的软件商店、输入法软件、字库软件、第三方应用软件等。除非适用法律予以禁止，否则您不得对上述软件进行复制、修改（包括反编译或反向工程）、分发。

五、开放源代码说明

   对于本产品中包含的任何开放源代码，本协议的任何条款均不得限制、约束或以其它方式影响任何适用开放源代码许可证赋予您的任何相应的权利或者义务或您应遵守的各种条件。

六、第三方软件/服务说明

   本协议所指的第三方软件/服务是指由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。本产品可能包含或捆绑有第三方软件/服务，这些第三方软件/服务附带单独的许可协议，您使用附带单独许可协议的任何第三方软件/服务需受到该单独许可协议的约束。
我们不对本产品中的第三方软件/服务拥有任何控制权，也不对其提供方或用户行为的合法性、准确性、有效性、安全性进行任何明示或默示的保证或担保。

七、免责条款

   1.有限担保
    我们向您担保，自购买或其他合法取得本产品之日起九十（90）天内（以销售合同日期为准），本产品的存储介质（如果有）在正常使用的情况下无材料和工艺方面的缺陷。在本有限担保项下，您可获得的所有补偿及我们的全部责任为由我们选择更换本产品介质或退还本产品的购买费用。
    2.免责声明
    除上述有限担保外，本软件按“原样”提供，不提供任何明示或默示的条件、陈述及担保，包括对适销性、对特定用途的适用性或非侵权性的任何默示的担保，均不予负责，但本免责声明被认定为法律上无效的情况除外。
    3.责任限制
    在法律允许范围内，无论在何种情况下，无论采用何种有关责任的理论，无论因何种方式导致，对于因使用或无法使用本软件引起的或与之相关的任何收益损失、利润或数据损失，或者对于特殊的、间接的、后果性的、偶发的或惩罚性的损害赔偿，我们或其许可方均不承担任何责任（即使我们已被告知可能出现上述损害赔偿）。根据本协议，在任何情况下，无论是在合同、侵权行为（包括过失）方面，还是在其他方面，我们对您的责任将不超过您就本软件所支付的金额。即使上述担保未能达到其基本目的，上述限制仍然适用。

八、协议完整性及可分割性说明

1.协议完整性
    本协议是我们就产品使用与您达成的完整协议。它取代此前或同期的所有和本协议不一致的口头或书面往来信息、建议、陈述和担保。在本协议期间，有关报价、订单、回执或各方之间就本协议内容进行的其他往来通信中的任何冲突条款或附加条款，均以本协议为准。对本协议的任何修改均无约束力，除非通过书面进行修改并由每一方的授权代表签字。
2.可分割性
如果本协议中有任何规定被认定为无法执行，则删除相应规定，本协议仍然有效，除非该删除会防碍各方根本目的的实现（在这种情况下，本协议将立即终止）。

九、适用法律及争议解决

1.管辖法律适用
    与本协议相关的任何争议解决（包括但不限于诉讼、仲裁等）均受适用中华人民共和国法律管辖。选择其它任何国家和地区的法律规则不予适用。
2.终止
如果本软件成为或在任一方看来可能成为任何知识产权侵权索赔之标的，则任一方可立即终止本协议。
    本协议在终止之前有效。您可以随时终止本协议，但必须同时销毁本软件的全部正本和副本。如果您未遵守本协议的任何规定，则本协议将不经我们发出通知立即终止。终止时，您必须销毁本软件的全部正本和副本，并且需承担因未遵守本协议而导致的法律责任。

本协议提供中英文两种版本，以上任何内容如有歧义，以中文版本为准。


银河麒麟操作系统隐私政策声明
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

   我们非常重视个人信息和隐私保护，为了保证合法、合理、适度的收集、存储、使用您的个人隐私信息，并在安全、可控的情况下进行传输、存储，我们制定了本声明。我们将会按照法律要求和业界成熟安全标准，为您的个人信息提供相应的安全保护措施。

本声明将向您说明以下内容：
一、关于收集和使用您的个人信息
二、如何存储和保护您的个人信息
三、如何管理您的个人信息
四、关于第三方软件/服务的隐私说明
五、关于未成年人使用产品
六、本声明如何更新
七、如何联系我们

一、如何收集和使用您的个人信息

    1.收集个人信息的情况
   我们在您使用本产品过程中收集相关的信息，主要为了向您提供更高质量、更易用的产品和更好的服务。收集的部分信息由您直接提供，其他信息则由我们通过您与产品的交互以及对产品的使用和体验收集而来。除非我们已根据适用的法律规定取得您的明示同意，我们不会主动收集并处理您的个人信息。
   1）本产品授权许可机制，会根据您所使用计算机的网卡、固件和主板等信息通过加密机制和转换方法生成申请产品正式授权许可的机器码；您将该机器码发送给麒麟软件商务人员后，可根据合同及相关协议申请正式许可。该机器码不直接包含您所使用计算机的网卡、固件和主板等设备的具体信息。
   2）本产品软件商店的服务器端，会根据您所使用计算机的CPU类型信息以及IP地址进行连接，同时我们会收集您使用本产品软件商店的相关信息，包括但不限于打开软件商店的时间、各页面之间的交互、搜索内容、下载的内容等，收集的相关信息一般记录在软件商店的服务端系统的日志中，具体存储位置可能因为不同的服务场景有所变动。
   3）本产品的升级更新，会根据您所使用计算机的IP地址进行连接，以便实现您升级更新系统；
   4）因业务往来及技术服务等向您收集电子邮箱、电话、姓名等个人信息。
   5）本产品的生物特征管理工具支持系统组件使用生物特征进行认证，包括指纹、指静脉、虹膜、声纹等。您录入的生物特征信息将储存在本地计算机，这部分信息我们仅接收验证结果，不会收集和上传。如您不需要使用生物特征进行系统认证，可以在生物特征管理工具中关闭该功能。
   6）本产品提供录音功能，您在使用本产品录音软件中，我们仅会将您使用录音时的音频内容存储在本地计算机中，不会进行收集和上传。
   7）本产品的服务与支持功能会收集由您提供给我们的日志、电子邮箱、电话、姓名等信息，便于提供技术服务，我们将妥善保管您的个人信息。
   8）本产品升级过程中，如需新增收集您的个人信息，我们将及时更新本部分内容。
   2.使用个人信息的情况
   我们严格遵守法律法规的规定及与您的约定，将收集的信息用于以下用途。若我们超出以下用途，我们将再次向您进行说明，并征得您的同意。
   1）涉及产品许可机制、软件商店使用、系统更新维护、生物识别、在线服务等需要；
   2）我们会利用相关信息协助提升产品的安全性、可靠性和可持续服务；
   3）我们会利用收集的信息（例如您提供的电子邮件地址、电话等）直接与您沟通。例如，业务联系、技术支持或服务回访；
   4）我们会利用收集的数据改进产品当前的易用性、提升产品用户体验（例如软件商店的个性化推荐）以及修复产品缺陷等；
   5）我们会将所收集到的用户行为数据用于大数据分析。例如，我们将收集到的信息用于分析形成不包含任何个人信息的城市热力图或行业洞察报告。我们可能对外公开并与我们的合作伙伴分享经统计加工后不含身份识别内容的信息，用于了解用户如何使用我们服务或让公众了解我们服务的总体使用趋势;

   6）我们可能使用您的相关信息，在相关网站、应用及其他渠道向您提供与您更加相关的广告;
   7）为了遵从相关法律法规、部门规章、政府指令的相关要求。
   3.信息的分享及对外提供
   我们不会共享或转让您的个人信息至第三方，但以下情况除外：
   1）获取您的明确同意后，我们会与第三方分享您的个人信息；
   2）为实现外部处理的目的，我们可能与关联公司或其他第三方合作伙伴（第三方服务供应商、承包商、代理、应用开发者等）分享您的个人信息。我们将采用加密、匿名化处理等手段来保障您的信息安全；
   3）我们不会对外公开披露所收集的个人信息，如必须公开披露时，我们会向您告知此次公开披露的目的、披露信息的类型及可能涉及的敏感信息，并征得您的同意；
   4）随着我们业务的持续发展，我们有可能进行合并、收购、资产转让等交易，我们将告知相关情形，按照法律法规及不低于本声明所要求的标准继续保护或要求新的控制者继续保护您的个人信息；
   5）如我们使用您的个人信息，超出了与收集时所声称的目的及具有直接或合理关联的范围，我们将在使用您的个人信息前，再次向您告知并征得您的同意。
   4.征得授权同意的例外情况
   1）与国家安全、国防安全等国家利益直接相关的；
   2）与公共安全、公共卫生、公众知情等重大公共利益直接相关的；
   3）与犯罪侦查、起诉、审判和判决执行等直接相关的；
   4）出于维护您或其他个人的生命、财产等重大合法权益但又无法得到您本人同意的；
   5）所收集的个人信息是您自行向社会公众公开的；
   6）从合法公开披露的信息中收集的个人信息，如合法的新闻报道、政府信息公开等渠道；
   7）根据您要求签订和履行合同所必需的；
   8）用于维护所提供的产品或服务的安全稳定运行所必需的。如发现、处置产品或服务的故障；
   9）出于公共利益开展统计或学术研究所必需，且其对外提供学术研究或描述的结果时，对结果中所包含的个人信息进行去标识化处理的；
   10）法律法规规定的其他情形。

二、我们如何存储和保护您的个人信息

   1.信息存储的地点
   我们会按照法律法规规定，将在中国境内收集和产生的个人信息存储于中国境内。
   2.信息存储的期限
   一般而言，我们仅为实现目的所必需的时间或法律法规规定最短期限保留您的个人信息。记录在日志中的信息会按配置在一定期限保存及自动删除。
   当我们的产品或服务发生停止运营的情形时，我们将以通知、公告等形式通知您，在合理的期限内删除您的个人信息或进行匿名化处理，并立即停止收集个人信息的活动。
   3.我们如何保护这些信息
   我们努力为用户的信息安全提供保障，以防止信息的丢失、不当使用、未经授权访问或披露。
   我们将在合理的安全水平内使用安全保护措施以保障信息的安全。例如，我们会使用加密技术等手段来保护您的系统级账户密码。
   我们建立专门的管理制度、流程和组织以保障信息的安全。例如，我们严格限制访问信息的人员范围，要求他们遵守保密义务。
   4.应急预案
   若发生个人信息泄露等安全事件，我们会依法启动应急预案，阻止安全事件扩大，并以推送通知、公告等形式告知您安全事件的情况、事件可能对您的影响以及我们将采取的补救措施。我们还将按照法律法规和监管部门要求，上报个人信息安全事件的处置情况。

三、如何管理您的个人信息

   如果担心因使用本产品导致个人信息的泄露，您可根据个人及业务需要考虑暂停或不使用涉及个人信息的相关功能，如产品正式授权许可、应用商店、系统更新升级、生物识别等。
在使用本产品之上的第三方软件/服务时，请注意个人隐私保护。

四、关于第三方软件/服务的隐私说明

   本协议所指的第三方软件/服务是由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。
您在本产品之上安装或使用第三方软件/服务时，第三方软件/服务的隐私保护和法律责任由第三方软件/服务自行负责，请您仔细阅读和审查第三方软件/服务对应的隐私声明或条款，注意个人隐私保护。

五、关于未成年人使用产品

   如果您是未成年人，则需要您的监护人同意您使用本产品并同意相关服务条款。除了提供产品所需要的信息外，我们不会刻意要求未成年人提供其他更多数据。在征得监护人同意或授权后，未成年人所创建的帐户即被视为等同于其他任何帐户。我们制定了专门的信息处理规则以保护使用本产品的未成年人的个人信息安全。监护人也应采取适当的预防措施保护未成年人，监督其对本产品的使用。

六、本声明如何更新

   我们可能会随时更新本声明，并且会在变更时通过产品安装过程或公司网站向您展示变更后的声明。在这些变更生效后使用服务或根据这些条款授予许可的任何软件，即表示您同意新的条款。如果您不同意新的条款，则必须停止使用本产品，请关闭您在本产品之上创建的帐户；如果您是监护人，请帮助您的未成年子女关闭他或她在本产品之上创建的帐户。

七、如何联系我们

   如您对本声明存在任何疑问，或任何相关的投诉、意见，可通过我们的客服热线400-089-1870、官方网站（www.kylinos.cn）或本产品中“服务与支持”应用进行咨询或反映。您也可以通过发送邮件至market@kylinos.cn与我们联系。
   我们会及时、妥善处理您的问题。一般情况下，我们将在15个工作日内给予答复。
   本声明自更新之日起生效，同时提供中英文两种版本，以上任何条款如有歧义，以中文版本为准。
   最近更新日期：2021年11月1日

地址：天津市滨海高新区塘沽海洋科技园信安创业广场3号楼（300450）
北京市海淀区北四环西路9号银谷大厦（100190）
长沙市开福区芙蓉中路1段303号富兴世界金融中心T3栋（410000）
上海市徐汇区番禺路1028号数娱大厦（200030）
电话：天津（022）58955650  北京（010）51659955  
长沙（0731）88280170 上海（021）51098866</translation>
    </message>
    <message>
        <source>Dear users of Kylin operating system and relevant products,

   Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”). When you click the next step to confirm your complete understanding of the content, it shall indicate that you have accepted the clauses of the Agreement, and the Agreement shall take effect immediately and be legally binding on you and the Company.
   “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products. 

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021 

The Agreement shall include the following content:
I. 	User license 
II. 	Java technology limitations
III. 	Cookies and other technologies
IV. 	Intellectual property clause
V. 	Open source code
VI. 	The third-party software/services
VII. Escape clause
VIII. Integrity and severability of the Agreement
IX. 	Applicable law and dispute settlement

   I. 	User license

   According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
   1.	User license for educational institutions
   In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product. 
   2.	Use of the font software
   Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

   II. 	Java technology limitations 

   You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

   III. 	Cookies and other technologies

   In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
   If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers. 
   In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

   IV. 	Intellectual property clause

   1.	Trademarks and Logos
   This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
  2.	Duplication, modification and distribution
   If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
   Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws. 

   V. 	Open source code

   For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

   VI.  The third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
   We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

   VII. 	Escape clause

   1.	Limited warranty
    We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
   2.	Disclaimer
   In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
   3.	Limitation of responsibility
   To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

   VIII. 	Integrity and severability of the Agreement

   1.	The integrity of the Agreement
  The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
   2.	Severability of the Agreement
   If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

   IX. 	Applicable law and dispute settlement

   1.	Application of governing laws
   Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
   2.	Termination
   If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
   The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.

   The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

Privacy Policy Statement of Kylin Operating System
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021

We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.
   The Statement shall include the following content:
    I. 	Collection and use your personal information
   II. 	How to store and protect your personal information
   III. 	How to manage your personal information
   IV. 	Privacy of the third-party software/services
   V. 	Minors’ use of the products
   VI. 	How to update this Statement
   VII. How to contact us

    I. 	How to collect and use your personal information

   1.	The collection of personal information
   We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
   1)	The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
   2)	Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
   3)	Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
   4)	Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
   5)	The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
   6)	This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
   7)	The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
   8)	In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.

  2.	Use of personal information
   We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
   1)	The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
   2)	We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
   3)	We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
   4)	We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
   5)	We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
   6)	We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications and other channels;
   7)	In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.

   3.	Information sharing and provision
   We shall not share or transfer your personal information to any third party, except for the following circumstances:
   1)	After obtaining your clear consent, we shall share your personal information with the third parities;
   2)	In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
    3)	We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
   4)	With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
    5)	If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.

   4.	Exceptions with authorized consent
   1)	It is directly related to national security, national defense security and other national interests;
   2)	It is directly related to public safety, public health and public knowledge and other major public interests;
   3)	It is directly related to crime investigation, prosecution, judgment and execution of judgment;
   4)	It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent;
   5)	The personal information collected is disclosed to the public by yourself;
   6)	Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels;
   7)	It is necessary to sign and perform of the contract according to your requirement;
   8)	It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
   9)	It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
   10)	Other circumstances specified in the laws and regulations.

   II. 	How to store and protect personal information

   1.	Information storage place
   We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
   2.	Information storage duration
  Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
   When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
   3.	How to protect the information
   We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
   We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
   We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
   4.	Emergency response plan
    In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

   III. 	How to manage your personal information

   If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs.
   Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

   IV. 	Privacy of the third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
   When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

   V. 	Minors’ use of the products

   If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

   VI. 	How to update this Statement

   We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the account created by you in this product; if you are a parent or guardian, please help your minor child to close the account created by him/her in this product.

   VII. 	How to contact us

  If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn).
   We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
   The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
   Last date of updating: November 1, 2021

Address: Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
             Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
             Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
             Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.: Tianjin (022) 58955650       Beijing (010) 51659955
             Changsha (0731) 88280170    Shanghai (021) 51098866
Fax: Tianjin (022) 58955651       Beijing (010) 62800607
             Changsha (0731) 88280166    Shanghai (021) 51062866

Company website: www.kylinos.cn
E-mail: support@kylinos.cn</source>
        <translation type="vanished">尊敬的银河麒麟操作系统及相关产品用户:
   请您仔细阅读本协议条款、补充许可条款（统称“协议”）及银河麒麟操作系统隐私政策声明（以下简称“声明”）。当您确认了解并点击下一步时，即表明您已接受本协议的条款，本协议将立即生效，对您和本公司双方具有法律约束力。
   本协议及声明中的“本产品”是指由麒麟软件有限公司开发并制作发行的用于办公或构建企业及政府的信息化基础设施——“银河麒麟操作系统软件产品”。“我们”是指麒麟软件有限公司。“您”是指支付授权费用并使用银河麒麟操作系统及相关产品的用户。

银河麒麟最终用户使用许可协议
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

本协议将向您说明以下内容：
一、使用许可
二、Java技术限制
三、Cookie和其他技术
四、知识产权条款
五、开放源代码说明
六、第三方软件/服务说明
七、免责条款
八、协议完整性及可分割性说明
九、适用法律及争议解决
一、使用许可

    按照已经为本产品支付费用的用户数目及计算机硬件类型，我们向您授予非排他、不可转让的许可，仅允许被授权人单位及与其签订劳动合同的员工使用由麒麟软件提供的随附软件和文档以及任何错误纠正。
   1.教育机构使用许可
    在遵守本协议的条款和条件的情况下，如果您是教育机构，允许贵机构仅在内部使用随附的未经修改的二进制格式的软件。此处的“在内部使用”是指被授权人单位及与其签订劳动合同的员工以及在贵机构入学的学生使用本产品。 
   2.字型软件使用
   字型软件指本产品中预装的和生成字体样式的软件。您不可从软件中分离字型软件，不可改动字型软件，以新增此等字型软件被作为本产品的一部分交付予您时所不具备的任何功能，不可将字型软件嵌入作为商业产品提供以换取收费或其他报酬的文件、不可脱离安装了本产品的机器使用。如将字型软件用于对外宣传等其他商业用途时，请您与字体版权厂商联系协商以获得对您相关行为的许可。

二、Java技术限制

   您不可更改“Java平台界面”（简称“JPI”，即指明为“java”包或“java”包的任何子包中的类），无论通过在JPI中创建额外的类，还是通过其他方式导致对JPI中的类进行增添或更动，均为不可。如果您创建一个额外的类以及一个或多个相关的API，而它们（i）扩展Java平台的功能；并且（ii）可供第三方软件开发者用于开发可调用上述额外API的额外软件，则您必须迅即广泛公布对此种API的准确说明，以供所有开发者免费使用。您不可创建、或授权其他被许可人创建以任何方式标示为“java”、“javax”、“sun”的额外的类、界面、子包或Sun在任何命名约定中指明的类似约定。参见Java运行时环境二进制代码许可的适当版本（目前位于http://jdk.java.net），以了解可与Java小程序和应用程序共同分发的运行时代码的可供情况。

三、Cookie和其他技术

   为帮助我们更好地了解并服务用户，我们的网站、在线服务和应用程序可能会使用“Cookie”技术。这些Cookie用于存储进出系统的网络流量以及因检测错误而生成的流量，因此必须设置。 我们通过使用这些Cookie来了解您与我们的网站和在线服务如何进行交互。 
   如果您想禁用 Cookie 并且使用的是Firefox浏览器，可在Firefox的隐私与安全中心进行设置。如果您使用的是其他浏览器，请向相关供应商咨询具体方案。
依照《中华人民共和国网络安全法》第七十六条第五款，个人信息，是指以电子或者其他方式记录的能够单独或者与其他信息结合识别自然人个人身份的各种信息，包括但不限于自然人的姓名、出生日期、身份证件号码、个人生物识别信息、住址、电话号码等。如果Cookie中包含上述信息，或者存在通过Cookie收集的非个人信息与其他个人信息合并后的信息，出于本隐私政策的目的，我们会将合并后的信息视为个人隐私信息，将参照银河麒麟隐私政策声明，为您的个人信息提供相应的安全保护措施。

四、知识产权条款

   1.商标和标识
    本产品受到版权（著作权）法、商标法和其他法律及国际知识产权公约的保护。我们或其许可方保留对本产品的所有权及所有相关的知识产权。对于我们或其许可方的任何商标、服务标记、标识或商号的任何权利、所有权或利益，本协议均不作任何授权。您对麒麟软件标记的任何使用都应有利于麒麟软件，未经我们书面同意，不得擅自使用麒麟软件任何商标、标识。 
   2.关于复制、修改及分发
   如果在所有复制品中维持本协议书不变，您可以且必须根据《GNU GPL-GNU通用公共许可证》复制、修改及分发银河麒麟操作系统软件产品中遵守《GNU GPL-GNU通用公共许可证》协议的软件，其他不遵守《GNU GPL-GNU通用公共许可证》协议的银河麒麟操作系统软件产品必须根据相关法律、其他许可协议进行复制、修改及分发，但任何以银河麒麟操作系统软件产品为基础的衍生发行版未经我们的书面授权不能使用任何我们的商标或其他任何标志。
   特别注意：该复制、修改及分发不包括本产品中包含的任何不适用《GNU GPL-GNU通用公共许可证》的软件，如银河麒麟操作系统软件产品中包含的软件商店、输入法软件、字库软件、第三方应用软件等。除非适用法律予以禁止，否则您不得对上述软件进行复制、修改（包括反编译或反向工程）、分发。

五、开放源代码说明

   对于本产品中包含的任何开放源代码，本协议的任何条款均不得限制、约束或以其它方式影响任何适用开放源代码许可证赋予您的任何相应的权利或者义务或您应遵守的各种条件。

六、第三方软件/服务说明

   本协议所指的第三方软件/服务是指由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。本产品可能包含或捆绑有第三方软件/服务，这些第三方软件/服务附带单独的许可协议，您使用附带单独许可协议的任何第三方软件/服务需受到该单独许可协议的约束。
我们不对本产品中的第三方软件/服务拥有任何控制权，也不对其提供方或用户行为的合法性、准确性、有效性、安全性进行任何明示或默示的保证或担保。

七、免责条款

   1.有限担保
    我们向您担保，自购买或其他合法取得本产品之日起九十（90）天内（以销售合同日期为准），本产品的存储介质（如果有）在正常使用的情况下无材料和工艺方面的缺陷。在本有限担保项下，您可获得的所有补偿及我们的全部责任为由我们选择更换本产品介质或退还本产品的购买费用。
    2.免责声明
    除上述有限担保外，本软件按“原样”提供，不提供任何明示或默示的条件、陈述及担保，包括对适销性、对特定用途的适用性或非侵权性的任何默示的担保，均不予负责，但本免责声明被认定为法律上无效的情况除外。
    3.责任限制
    在法律允许范围内，无论在何种情况下，无论采用何种有关责任的理论，无论因何种方式导致，对于因使用或无法使用本软件引起的或与之相关的任何收益损失、利润或数据损失，或者对于特殊的、间接的、后果性的、偶发的或惩罚性的损害赔偿，我们或其许可方均不承担任何责任（即使我们已被告知可能出现上述损害赔偿）。根据本协议，在任何情况下，无论是在合同、侵权行为（包括过失）方面，还是在其他方面，我们对您的责任将不超过您就本软件所支付的金额。即使上述担保未能达到其基本目的，上述限制仍然适用。

八、协议完整性及可分割性说明

1.协议完整性
    本协议是我们就产品使用与您达成的完整协议。它取代此前或同期的所有和本协议不一致的口头或书面往来信息、建议、陈述和担保。在本协议期间，有关报价、订单、回执或各方之间就本协议内容进行的其他往来通信中的任何冲突条款或附加条款，均以本协议为准。对本协议的任何修改均无约束力，除非通过书面进行修改并由每一方的授权代表签字。
2.可分割性
如果本协议中有任何规定被认定为无法执行，则删除相应规定，本协议仍然有效，除非该删除会防碍各方根本目的的实现（在这种情况下，本协议将立即终止）。

九、适用法律及争议解决

1.管辖法律适用
    与本协议相关的任何争议解决（包括但不限于诉讼、仲裁等）均受适用中华人民共和国法律管辖。选择其它任何国家和地区的法律规则不予适用。
2.终止
如果本软件成为或在任一方看来可能成为任何知识产权侵权索赔之标的，则任一方可立即终止本协议。
    本协议在终止之前有效。您可以随时终止本协议，但必须同时销毁本软件的全部正本和副本。如果您未遵守本协议的任何规定，则本协议将不经我们发出通知立即终止。终止时，您必须销毁本软件的全部正本和副本，并且需承担因未遵守本协议而导致的法律责任。

本协议提供中英文两种版本，以上任何内容如有歧义，以中文版本为准。


银河麒麟操作系统隐私政策声明
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

   我们非常重视个人信息和隐私保护，为了保证合法、合理、适度的收集、存储、使用您的个人隐私信息，并在安全、可控的情况下进行传输、存储，我们制定了本声明。我们将会按照法律要求和业界成熟安全标准，为您的个人信息提供相应的安全保护措施。

本声明将向您说明以下内容：
一、关于收集和使用您的个人信息
二、如何存储和保护您的个人信息
三、如何管理您的个人信息
四、关于第三方软件/服务的隐私说明
五、关于未成年人使用产品
六、本声明如何更新
七、如何联系我们

一、如何收集和使用您的个人信息

    1.收集个人信息的情况
   我们在您使用本产品过程中收集相关的信息，主要为了向您提供更高质量、更易用的产品和更好的服务。收集的部分信息由您直接提供，其他信息则由我们通过您与产品的交互以及对产品的使用和体验收集而来。除非我们已根据适用的法律规定取得您的明示同意，我们不会主动收集并处理您的个人信息。
   1）本产品授权许可机制，会根据您所使用计算机的网卡、固件和主板等信息通过加密机制和转换方法生成申请产品正式授权许可的机器码；您将该机器码发送给麒麟软件商务人员后，可根据合同及相关协议申请正式许可。该机器码不直接包含您所使用计算机的网卡、固件和主板等设备的具体信息。
   2）本产品软件商店的服务器端，会根据您所使用计算机的CPU类型信息以及IP地址进行连接，同时我们会收集您使用本产品软件商店的相关信息，包括但不限于打开软件商店的时间、各页面之间的交互、搜索内容、下载的内容等，收集的相关信息一般记录在软件商店的服务端系统的日志中，具体存储位置可能因为不同的服务场景有所变动。
   3）本产品的升级更新，会根据您所使用计算机的IP地址进行连接，以便实现您升级更新系统；
   4）因业务往来及技术服务等向您收集电子邮箱、电话、姓名等个人信息。
   5）本产品的生物特征管理工具支持系统组件使用生物特征进行认证，包括指纹、指静脉、虹膜、声纹等。您录入的生物特征信息将储存在本地计算机，这部分信息我们仅接收验证结果，不会收集和上传。如您不需要使用生物特征进行系统认证，可以在生物特征管理工具中关闭该功能。
   6）本产品提供录音功能，您在使用本产品录音软件中，我们仅会将您使用录音时的音频内容存储在本地计算机中，不会进行收集和上传。
   7）本产品的服务与支持功能会收集由您提供给我们的日志、电子邮箱、电话、姓名等信息，便于提供技术服务，我们将妥善保管您的个人信息。
   8）本产品升级过程中，如需新增收集您的个人信息，我们将及时更新本部分内容。
   2.使用个人信息的情况
   我们严格遵守法律法规的规定及与您的约定，将收集的信息用于以下用途。若我们超出以下用途，我们将再次向您进行说明，并征得您的同意。
   1）涉及产品许可机制、软件商店使用、系统更新维护、生物识别、在线服务等需要；
   2）我们会利用相关信息协助提升产品的安全性、可靠性和可持续服务；
   3）我们会利用收集的信息（例如您提供的电子邮件地址、电话等）直接与您沟通。例如，业务联系、技术支持或服务回访；
   4）我们会利用收集的数据改进产品当前的易用性、提升产品用户体验（例如软件商店的个性化推荐）以及修复产品缺陷等；
   5）我们会将所收集到的用户行为数据用于大数据分析。例如，我们将收集到的信息用于分析形成不包含任何个人信息的城市热力图或行业洞察报告。我们可能对外公开并与我们的合作伙伴分享经统计加工后不含身份识别内容的信息，用于了解用户如何使用我们服务或让公众了解我们服务的总体使用趋势;

   6）我们可能使用您的相关信息，在相关网站、应用及其他渠道向您提供与您更加相关的广告;
   7）为了遵从相关法律法规、部门规章、政府指令的相关要求。
   3.信息的分享及对外提供
   我们不会共享或转让您的个人信息至第三方，但以下情况除外：
   1）获取您的明确同意后，我们会与第三方分享您的个人信息；
   2）为实现外部处理的目的，我们可能与关联公司或其他第三方合作伙伴（第三方服务供应商、承包商、代理、应用开发者等）分享您的个人信息。我们将采用加密、匿名化处理等手段来保障您的信息安全；
   3）我们不会对外公开披露所收集的个人信息，如必须公开披露时，我们会向您告知此次公开披露的目的、披露信息的类型及可能涉及的敏感信息，并征得您的同意；
   4）随着我们业务的持续发展，我们有可能进行合并、收购、资产转让等交易，我们将告知相关情形，按照法律法规及不低于本声明所要求的标准继续保护或要求新的控制者继续保护您的个人信息；
   5）如我们使用您的个人信息，超出了与收集时所声称的目的及具有直接或合理关联的范围，我们将在使用您的个人信息前，再次向您告知并征得您的同意。
   4.征得授权同意的例外情况
   1）与国家安全、国防安全等国家利益直接相关的；
   2）与公共安全、公共卫生、公众知情等重大公共利益直接相关的；
   3）与犯罪侦查、起诉、审判和判决执行等直接相关的；
   4）出于维护您或其他个人的生命、财产等重大合法权益但又无法得到您本人同意的；
   5）所收集的个人信息是您自行向社会公众公开的；
   6）从合法公开披露的信息中收集的个人信息，如合法的新闻报道、政府信息公开等渠道；
   7）根据您要求签订和履行合同所必需的；
   8）用于维护所提供的产品或服务的安全稳定运行所必需的。如发现、处置产品或服务的故障；
   9）出于公共利益开展统计或学术研究所必需，且其对外提供学术研究或描述的结果时，对结果中所包含的个人信息进行去标识化处理的；
   10）法律法规规定的其他情形。

二、我们如何存储和保护您的个人信息

   1.信息存储的地点
   我们会按照法律法规规定，将在中国境内收集和产生的个人信息存储于中国境内。
   2.信息存储的期限
   一般而言，我们仅为实现目的所必需的时间或法律法规规定最短期限保留您的个人信息。记录在日志中的信息会按配置在一定期限保存及自动删除。
   当我们的产品或服务发生停止运营的情形时，我们将以通知、公告等形式通知您，在合理的期限内删除您的个人信息或进行匿名化处理，并立即停止收集个人信息的活动。
   3.我们如何保护这些信息
   我们努力为用户的信息安全提供保障，以防止信息的丢失、不当使用、未经授权访问或披露。
   我们将在合理的安全水平内使用安全保护措施以保障信息的安全。例如，我们会使用加密技术等手段来保护您的系统级账户密码。
   我们建立专门的管理制度、流程和组织以保障信息的安全。例如，我们严格限制访问信息的人员范围，要求他们遵守保密义务。
   4.应急预案
   若发生个人信息泄露等安全事件，我们会依法启动应急预案，阻止安全事件扩大，并以推送通知、公告等形式告知您安全事件的情况、事件可能对您的影响以及我们将采取的补救措施。我们还将按照法律法规和监管部门要求，上报个人信息安全事件的处置情况。

三、如何管理您的个人信息

   如果担心因使用本产品导致个人信息的泄露，您可根据个人及业务需要考虑暂停或不使用涉及个人信息的相关功能，如产品正式授权许可、应用商店、系统更新升级、生物识别等。
在使用本产品之上的第三方软件/服务时，请注意个人隐私保护。

四、关于第三方软件/服务的隐私说明

   本协议所指的第三方软件/服务是由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。
您在本产品之上安装或使用第三方软件/服务时，第三方软件/服务的隐私保护和法律责任由第三方软件/服务自行负责，请您仔细阅读和审查第三方软件/服务对应的隐私声明或条款，注意个人隐私保护。

五、关于未成年人使用产品

   如果您是未成年人，则需要您的监护人同意您使用本产品并同意相关服务条款。除了提供产品所需要的信息外，我们不会刻意要求未成年人提供其他更多数据。在征得监护人同意或授权后，未成年人所创建的帐户即被视为等同于其他任何帐户。我们制定了专门的信息处理规则以保护使用本产品的未成年人的个人信息安全。监护人也应采取适当的预防措施保护未成年人，监督其对本产品的使用。

六、本声明如何更新

   我们可能会随时更新本声明，并且会在变更时通过产品安装过程或公司网站向您展示变更后的声明。在这些变更生效后使用服务或根据这些条款授予许可的任何软件，即表示您同意新的条款。如果您不同意新的条款，则必须停止使用本产品，请关闭您在本产品之上创建的帐户；如果您是家长或监护人，请帮助您的未成年子女关闭他或她在本产品之上创建的帐户。

七、如何联系我们

   如您对本声明存在任何疑问，或任何相关的投诉、意见，可通过我们的客服热线400-089-1870、官方网站（www.kylinos.cn）或本产品中“服务与支持”应用进行咨询或反映。您也可以通过发送邮件至market@kylinos.cn与我们联系。
   我们会及时、妥善处理您的问题。一般情况下，我们将在15个工作日内给予答复。
   本声明自更新之日起生效，同时提供中英文两种版本，以上任何条款如有歧义，以中文版本为准。
   最近更新日期：2021年11月1日

地址：天津市滨海高新区塘沽海洋科技园信安创业广场3号楼（300450）
北京市海淀区北四环西路9号银谷大厦（100190）
长沙市开福区芙蓉中路1段303号富兴世界金融中心T3栋（410000）
上海市徐汇区番禺路1028号数娱大厦（200030）
电话：天津（022）58955650  北京（010）51659955  
长沙（0731）88280170 上海（021）51098866</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="298"/>
        <source>Kylinsoft Co., Ltd.</source>
        <translation>Kylinsoft چەكلىك شىركىتى</translation>
    </message>
</context>
<context>
    <name>Projection</name>
    <message>
        <source>Projection</source>
        <translation type="vanished">投屏</translation>
    </message>
    <message>
        <source>Open Projection</source>
        <translation type="vanished">开启投屏</translation>
    </message>
    <message>
        <source>Projection Name</source>
        <translation type="vanished">投屏端名称</translation>
    </message>
    <message>
        <source>Add Bluetooths</source>
        <translation type="vanished">添加蓝牙</translation>
    </message>
</context>
<context>
    <name>Proxy</name>
    <message>
        <source>Auto Proxy</source>
        <translation type="vanished">自动代理</translation>
        <extra-contents_path>/Proxy/Auto Proxy</extra-contents_path>
    </message>
    <message>
        <source>Auto proxy</source>
        <translation type="vanished">开启自动代理</translation>
    </message>
    <message>
        <source>Auto url</source>
        <translation type="vanished">ئاپتوماتىك تور ئادرېسى</translation>
        <extra-contents_path>/Proxy/Auto url</extra-contents_path>
    </message>
    <message>
        <source>Manual Proxy</source>
        <translation type="vanished">手动代理</translation>
        <extra-contents_path>/Proxy/Manual Proxy</extra-contents_path>
    </message>
    <message>
        <source>Manual proxy</source>
        <translation type="vanished">开启手动代理</translation>
    </message>
    <message>
        <source>Http Proxy</source>
        <translation type="vanished">Http Proxy</translation>
        <extra-contents_path>/Proxy/Http Proxy</extra-contents_path>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">پورت</translation>
    </message>
    <message>
        <source>Cetification</source>
        <translation type="vanished">认证</translation>
    </message>
    <message>
        <source>System Proxy</source>
        <translation type="vanished">سىستېما Proxy</translation>
    </message>
    <message>
        <source>Https Proxy</source>
        <translation type="vanished">Https Proxy</translation>
        <extra-contents_path>/Proxy/Https Proxy</extra-contents_path>
    </message>
    <message>
        <source>Ftp Proxy</source>
        <translation type="vanished">Ftp Proxy</translation>
        <extra-contents_path>/Proxy/Ftp Proxy</extra-contents_path>
    </message>
    <message>
        <source>Socks Proxy</source>
        <translation type="vanished">پايپاق Proxy</translation>
        <extra-contents_path>/Proxy/Socks Proxy</extra-contents_path>
    </message>
    <message>
        <source>List of ignored hosts. more than one entry, please separate with english semicolon(;)</source>
        <translation type="vanished">سەل قارالغان رىياسەتچىلەر تىزىملىكى بىردىن ئارتۇق كىرگۈزۈڭ، ئىنگىلىزچە يېرىمكول (;)</translation>
    </message>
    <message>
        <source>Enable Authentication</source>
        <translation type="vanished">启用认证</translation>
    </message>
    <message>
        <source>User Name</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <source>Apt Proxy</source>
        <translation type="vanished">Apt Proxy</translation>
        <extra-contents_path>/Proxy/Apt Proxy</extra-contents_path>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">ئېچىش</translation>
    </message>
    <message>
        <source>Server Address : </source>
        <translation type="vanished">مۇلازىمىتېر ئادرېسى : </translation>
    </message>
    <message>
        <source>Port : </source>
        <translation type="vanished">پورتى : </translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="vanished">تەھرىرلەش</translation>
    </message>
    <message>
        <source>The apt proxy  has been turned off and needs to be restarted to take effect</source>
        <translation type="vanished">apt proxy نى ئېتىۋەتتى، كۈچكە ئىگە بولۇش ئۈچۈن قايتا قوزغىتىش كېرەك</translation>
    </message>
    <message>
        <source>The system needs to be restarted to set the Apt proxy, whether to reboot</source>
        <translation type="vanished">Apt proxy نى بەلگىلەش ئۈچۈن سىستېمىنى قايتا قوزغىتىش كېرەك، قايتا قوزغىتىش كېرەكمۇ يوق</translation>
    </message>
    <message>
        <source>Reboot Now</source>
        <translation type="vanished">ھازىر قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <source>Start using</source>
        <translation type="vanished">ئىشلىتىشنى باشلاڭ</translation>
    </message>
    <message>
        <source>Proxy mode</source>
        <translation type="vanished">Proxy ھالىتى</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">ئاپتۇماتىك</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="vanished">قوللانما</translation>
    </message>
    <message>
        <source>Reboot Later</source>
        <translation type="vanished">كېيىن قايتا قوزغىتىڭ</translation>
    </message>
    <message>
        <source>proxy</source>
        <translation type="vanished">代理</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation type="vanished">Proxy</translation>
    </message>
</context>
<context>
    <name>PwdDialog</name>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="35"/>
        <source>VNC password</source>
        <translation>VNC مەخپىي نومۇرى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="52"/>
        <source>Password</source>
        <translation>ئىم</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="67"/>
        <source>Must be 1-8 characters long</source>
        <translation>چوقۇم ئۇزۇنلۇقى 1-8 ھەرپ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="84"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="88"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>display</source>
        <translation type="vanished">显示器</translation>
    </message>
    <message>
        <source>defaultapp</source>
        <translation type="vanished">默认应用</translation>
    </message>
    <message>
        <source>power</source>
        <translation type="vanished">电源</translation>
    </message>
    <message>
        <source>autoboot</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>printer</source>
        <translation type="vanished">打印机</translation>
    </message>
    <message>
        <source>mousecontrol</source>
        <translation type="vanished">鼠标</translation>
    </message>
    <message>
        <source>mouse</source>
        <translation type="vanished">鼠标</translation>
    </message>
    <message>
        <source>touchpad</source>
        <translation type="vanished">触控板</translation>
    </message>
    <message>
        <source>keyboard</source>
        <translation type="vanished">键盘</translation>
    </message>
    <message>
        <source>shortcut</source>
        <translation type="vanished">快捷键</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">声音</translation>
    </message>
    <message>
        <source>background</source>
        <translation type="vanished">背景</translation>
    </message>
    <message>
        <source>screenlock</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>fonts</source>
        <translation type="vanished">字体</translation>
    </message>
    <message>
        <source>Screensaver</source>
        <translation type="vanished">屏保</translation>
    </message>
    <message>
        <source>desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>netconnect</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>vpn</source>
        <translation type="vanished">VPN</translation>
    </message>
    <message>
        <source>proxy</source>
        <translation type="vanished">代理</translation>
    </message>
    <message>
        <source>userinfo</source>
        <translation type="vanished">帐户信息</translation>
    </message>
    <message>
        <source>datetime</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>area</source>
        <translation type="vanished">区域语言</translation>
    </message>
    <message>
        <source>update</source>
        <translation type="vanished">更新和备份</translation>
    </message>
    <message>
        <source>backup</source>
        <translation type="vanished">备份</translation>
    </message>
    <message>
        <source>notice</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>experienceplan</source>
        <translation type="vanished">体验计划</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>ukui-control-center had already running!</source>
        <translation type="vanished">控制面板已经在运行！</translation>
    </message>
    <message>
        <source>basicIcon</source>
        <translation type="vanished">基础</translation>
    </message>
    <message>
        <source>classicalIcon</source>
        <translation type="vanished">经典</translation>
    </message>
    <message>
        <source>defaultIcon</source>
        <translation type="vanished">默认</translation>
    </message>
    <message>
        <source>blue-crystal</source>
        <translation type="vanished">蓝水晶</translation>
    </message>
    <message>
        <source>dark-sense</source>
        <translation type="vanished">深色质感</translation>
    </message>
    <message>
        <source>DMZ-Black</source>
        <translation type="vanished">DMZ-黑</translation>
    </message>
    <message>
        <source>DMZ-White</source>
        <translation type="vanished">DMZ-白</translation>
    </message>
    <message>
        <source>basic</source>
        <translation type="vanished">基础</translation>
    </message>
    <message>
        <source>classical</source>
        <translation type="vanished">经典</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">默认</translation>
    </message>
    <message>
        <source>fashion</source>
        <translation type="vanished">时尚</translation>
    </message>
    <message>
        <source>hp</source>
        <translation type="vanished">惠普</translation>
    </message>
    <message>
        <source>ukui</source>
        <translation type="vanished">基础</translation>
    </message>
    <message>
        <source>lightseeking</source>
        <translation type="vanished">寻光</translation>
    </message>
    <message>
        <source>HeYin</source>
        <translation type="vanished">和印</translation>
    </message>
    <message>
        <source>found</source>
        <translation type="vanished">寻光</translation>
    </message>
    <message>
        <source>heyin</source>
        <translation type="vanished">和印</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">نامەلۇم</translation>
    </message>
    <message>
        <source>Custom Shortcut</source>
        <translation type="vanished">自定义快捷键</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="187"/>
        <source>Customize Shortcut</source>
        <translation>قىسقا يولنى تەڭشەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="694"/>
        <source>Edit Shortcut</source>
        <translation>قىسقارتىلما يولنى تەھرىرلەش</translation>
    </message>
    <message>
        <source>Update Shortcut</source>
        <translation type="vanished">更新快捷键</translation>
    </message>
    <message>
        <source>Add Shortcut</source>
        <translation type="vanished">添加快捷键</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">显示器</translation>
    </message>
    <message>
        <source>Defaultapp</source>
        <translation type="vanished">默认应用</translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="vanished">电源</translation>
    </message>
    <message>
        <source>Autoboot</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>TouchScreen</source>
        <translation type="vanished">触摸屏</translation>
    </message>
    <message>
        <source>User Info Intel</source>
        <translation type="vanished">账户信息</translation>
    </message>
    <message>
        <source>Biometrics</source>
        <translation type="vanished">生物特征与密码</translation>
    </message>
    <message>
        <source>Default App</source>
        <translation type="vanished">默认应用</translation>
    </message>
    <message>
        <source>Auto Boot</source>
        <translation type="vanished">开机启动</translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="vanished">打印机</translation>
    </message>
    <message>
        <source>Projection</source>
        <translation type="vanished">投屏</translation>
    </message>
    <message>
        <source>System Recovery</source>
        <translation type="vanished">系统还原</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">鼠标</translation>
    </message>
    <message>
        <source>Touchpad</source>
        <translation type="vanished">触控板</translation>
    </message>
    <message>
        <source>Gesture</source>
        <translatorcomment>手势</translatorcomment>
        <translation type="obsolete">手势</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">键盘</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation type="vanished">快捷键</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">声音</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <source>Background</source>
        <translation type="vanished">背景</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>WiredConnect</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>WlanConnect</source>
        <translation type="vanished">无线局域网</translation>
    </message>
    <message>
        <source>MobileHotspot</source>
        <translation type="vanished">移动热点</translation>
    </message>
    <message>
        <source>Screenlock</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation type="vanished">字体</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>Vino</source>
        <translation type="vanished">远程桌面</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="71"/>
        <source>User Info</source>
        <translation>ئىشلەتكۈچى ئۇچۇرى</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="vanished">时间和日期</translation>
    </message>
    <message>
        <source>Dat</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>Security Center</source>
        <translation type="vanished">安全中心</translation>
    </message>
    <message>
        <source>Netconnect</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>Vpn</source>
        <translation type="vanished">VPN</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation type="vanished">代理</translation>
    </message>
    <message>
        <source>Userinfo</source>
        <translation type="vanished">帐户信息</translation>
    </message>
    <message>
        <source>Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Datetime</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>Area</source>
        <translation type="vanished">区域语言</translation>
    </message>
    <message>
        <source>SecurityCenter</source>
        <translation type="vanished">安全中心</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">安全与更新</translation>
    </message>
    <message>
        <source>Backup</source>
        <translation type="vanished">备份</translation>
    </message>
    <message>
        <source>Upgrade</source>
        <translation type="vanished">更新</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Experienceplan</source>
        <translation type="vanished">体验计划</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="vanished">مەڭگۈ</translation>
    </message>
    <message>
        <source>10min</source>
        <translation type="vanished">10min</translation>
    </message>
    <message>
        <source>20min</source>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <source>40min</source>
        <translation type="vanished">40min</translation>
    </message>
    <message>
        <source>80min</source>
        <translation type="vanished">80min</translation>
    </message>
    <message>
        <source>interactive</source>
        <translation type="vanished">询问</translation>
    </message>
    <message>
        <source>suspend</source>
        <translation type="vanished">睡眠</translation>
    </message>
    <message>
        <source>hibernate</source>
        <translation type="vanished">休眠</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>nothing</source>
        <translation type="vanished">无操作</translation>
    </message>
    <message>
        <source>blank</source>
        <translation type="vanished">关闭显示器</translation>
    </message>
    <message>
        <source>Year</source>
        <translation type="vanished">يىل</translation>
    </message>
    <message>
        <source>Jan</source>
        <translation type="vanished">Jan</translation>
    </message>
    <message>
        <source>Feb</source>
        <translation type="vanished">Feb</translation>
    </message>
    <message>
        <source>Mar</source>
        <translation type="vanished">مار</translation>
    </message>
    <message>
        <source>Apr</source>
        <translation type="vanished">Apr</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>May</source>
        <translation>ماي</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>January</source>
        <translation>يانۋار</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>February</source>
        <translation>فېۋرال</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>March</source>
        <translation>مارت</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>April</source>
        <translation>ئاپرېل</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>June</source>
        <translation>6-ئاي</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>July</source>
        <translation>ئىيۇل</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>August</source>
        <translation>ئاۋغۇست</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>September</source>
        <translation>سېنتەبىر</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>October</source>
        <translation>ئۆكتەبىر</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>Novermber</source>
        <translation>نوvermber</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>December</source>
        <translation>دېكابىر</translation>
    </message>
    <message>
        <source>Jun</source>
        <translation type="vanished">جۈن</translation>
    </message>
    <message>
        <source>Jul</source>
        <translation type="vanished">جۇلا</translation>
    </message>
    <message>
        <source>Aug</source>
        <translation type="vanished">ئاۋغۇست</translation>
    </message>
    <message>
        <source>Sep</source>
        <translation type="vanished">Sep</translation>
    </message>
    <message>
        <source>Oct</source>
        <translation type="vanished">ئۆكتەبىر</translation>
    </message>
    <message>
        <source>Nov</source>
        <translation type="vanished">Nov</translation>
    </message>
    <message>
        <source>Dec</source>
        <translation type="vanished">Dec</translation>
    </message>
    <message>
        <source>Day</source>
        <translation type="vanished">كۈن</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="96"/>
        <source>ukui-control-center is disabled！</source>
        <translation>ئۇكۇي كونترول مەركىزى مېيىپ!</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="130"/>
        <source>ukui-control-center</source>
        <translation>ئۇكۇي-كونترول-مەركىزى</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="85"/>
        <source>ukui-control-center is already running!</source>
        <translation>ئۇكۇي كونتىرول قىلىش مەركىزى ئاللىقاچان يۈگرەيدۇ!</translation>
    </message>
    <message>
        <source>Pwd input error, re-enter!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Go to monitor settings page</source>
        <translation type="vanished">管理和配置显示和监视器</translation>
    </message>
    <message>
        <source>Go to defaultapp settings page</source>
        <translation type="vanished">选择默认应用</translation>
    </message>
    <message>
        <source>Go to printer settings page</source>
        <translation type="vanished">打印机管理</translation>
    </message>
    <message>
        <source>Go to projection settings page</source>
        <translation type="vanished">投屏设置</translation>
    </message>
    <message>
        <source>Go to mouse settings page</source>
        <translation type="vanished">配置鼠标选项</translation>
    </message>
    <message>
        <source>Go to touchpad settings page</source>
        <translation type="vanished">触控板管理</translation>
    </message>
    <message>
        <source>Go to keyboard settings page</source>
        <translation type="vanished">键盘设置</translation>
    </message>
    <message>
        <source>Go to shortcut settings page</source>
        <translation type="vanished">配置快捷键</translation>
    </message>
    <message>
        <source>Go to bluetooth settings page</source>
        <translation type="vanished">蓝牙设置</translation>
    </message>
    <message>
        <source>Go to background settings page</source>
        <translation type="vanished">配置桌面壁纸</translation>
    </message>
    <message>
        <source>Go to theme settings page</source>
        <translation type="vanished">配置主题</translation>
    </message>
    <message>
        <source>Go to screenlock settings page</source>
        <translation type="vanished">锁屏设置</translation>
    </message>
    <message>
        <source>Go to screensaver settings page</source>
        <translation type="vanished">屏保设置</translation>
    </message>
    <message>
        <source>Go to fonts settings page</source>
        <translation type="vanished">配置用户字体</translation>
    </message>
    <message>
        <source>Go to netconnect settings page</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>Go to proxy settings page</source>
        <translation type="vanished">代理设置</translation>
    </message>
    <message>
        <source>Go to mobilehotspot settings page</source>
        <translation type="vanished">移动热点</translation>
    </message>
    <message>
        <source>Go to userinfo settings page</source>
        <translation type="vanished">管理用户信息</translation>
    </message>
    <message>
        <source>Go to cloudaccount settings page</source>
        <translation type="vanished">配置您的网络帐户</translation>
    </message>
    <message>
        <source>Go to area settings page</source>
        <translation type="vanished">区域语言</translation>
    </message>
    <message>
        <source>Go to update settings page</source>
        <translation type="vanished">更新管理</translation>
    </message>
    <message>
        <source>Go to backup settings page</source>
        <translation type="vanished">备份管理</translation>
    </message>
    <message>
        <source>Go to upgrade settings page</source>
        <translation type="vanished">更新设置</translation>
    </message>
    <message>
        <source>Go to about settings page</source>
        <translation type="vanished">此系统的信息</translation>
    </message>
    <message>
        <source>Go to search settings page</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>Go to power settings page</source>
        <translation type="vanished">配置电源管理</translation>
    </message>
    <message>
        <source>Go to datetime settings page</source>
        <translation type="vanished">管理日期和时间</translation>
    </message>
    <message>
        <source>Go to desktop settings page</source>
        <translation type="vanished">配置托盘，开始菜单图标</translation>
    </message>
    <message>
        <source>Go to audio settings page</source>
        <translation type="vanished">音量设置</translation>
    </message>
    <message>
        <source>Go to notice settings page</source>
        <translation type="vanished">通知管理模块</translation>
    </message>
    <message>
        <source>Go to vpn settings page</source>
        <translation type="vanished">VPN模块</translation>
    </message>
    <message>
        <source>Go to autoboot settings page</source>
        <translation type="vanished">自动启动的应用程序</translation>
    </message>
    <message>
        <source>Connection failed, attempting reconnect</source>
        <translation type="vanished">连接失败，尝试重新连接</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1335"/>
        <source>min length %1
</source>
        <translation>min ئۇزۇنلۇقى ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1345"/>
        <source>min digit num %1
</source>
        <translation>مىڭ خانىلىق num ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1354"/>
        <source>min upper num %1
</source>
        <translation>min ئۈستۈنكى num ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1363"/>
        <source>min lower num %1
</source>
        <translation>min تۆۋەن num ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1372"/>
        <source>min other num %1
</source>
        <translation>min باشقا num ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1382"/>
        <source>min char class %1
</source>
        <translation>min char سىنىپى ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1391"/>
        <source>max repeat %1
</source>
        <translation>max قايتا تەكرارلاش ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1400"/>
        <source>max class repeat %1
</source>
        <translation>ئەڭ چوڭ دەرىس تەكرارلاش ٪1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1409"/>
        <source>max sequence %1
</source>
        <translation>ئەڭ چوڭ تەرتىپ ٪1
</translation>
    </message>
    <message>
        <source>system upgrade new backup</source>
        <translation type="vanished">系统升级新建备份</translation>
    </message>
    <message>
        <source>system upgrade increment backup</source>
        <translation type="vanished">系统升级增量备份</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="682"/>
        <source>xxx客户端</source>
        <translation>xxx兏兏兏端</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="765"/>
        <source>Programs are not allowed to be added.</source>
        <translation>پروگراممىلارنىڭ قوشۇلۇشىغا يول قويۇلمايدۇ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="8"/>
        <source>简体中文</source>
        <translation>简体中文</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="9"/>
        <source>English</source>
        <translation>ئىنگىلىز تىلى</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="10"/>
        <source>བོད་ཡིག</source>
        <translation>བོད་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="11"/>
        <source>ᠮᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ</source>
        <translation>ᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="12"/>
        <source>繁體</source>
        <translation>繁體</translation>
    </message>
</context>
<context>
    <name>RegDialog</name>
    <message>
        <source>Get</source>
        <translation type="vanished">获取验证码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">输入密码</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">请输入用户名</translation>
    </message>
    <message>
        <source>Confirm your password</source>
        <translation type="vanished">确认密码</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">输入验证码</translation>
    </message>
    <message>
        <source>This operation is permanent</source>
        <translation type="vanished">设置后不可更改，最高30位</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">至少六位，包含大小写字母、数字</translation>
    </message>
    <message>
        <source>Your password is valid!</source>
        <translation type="vanished">您的密码是有效的！</translation>
    </message>
</context>
<context>
    <name>ResolutionSlider</name>
    <message>
        <source>(recommend)</source>
        <translation type="vanished">(推荐)</translation>
    </message>
    <message>
        <source>No available resolutions</source>
        <translation type="vanished">ئىشلەتكىلى بولىدىغان ئېنىقلىما يوق</translation>
    </message>
</context>
<context>
    <name>Screenlock</name>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="26"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="80"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="51"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="136"/>
        <source>Screenlock</source>
        <translation>ئېكران قۇلۇپى</translation>
        <extra-contents_path>/Screenlock/Screenlock</extra-contents_path>
    </message>
    <message>
        <source>Screenlock Interface</source>
        <translation type="vanished">ئېكران قۇلۇپى ئارايۈز</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="205"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="256"/>
        <source>Show message on lock screen</source>
        <translation>قۇلۇپ ئېكرانىدا ئۇچۇر كۆرسىتىش</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="vanished">كۆرۈش</translation>
        <extra-contents_path>/Screenlock/Browse</extra-contents_path>
    </message>
    <message>
        <source>Online Picture</source>
        <translation type="vanished">توردا رەسىم</translation>
        <extra-contents_path>/Screenlock/Online Picture</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="401"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="150"/>
        <source>Local Pictures</source>
        <translation>يەرلىك رەسىملەر</translation>
        <extra-contents_path>/Screenlock/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="408"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="152"/>
        <source>Online Pictures</source>
        <translation>توردىكى رەسىملەر</translation>
        <extra-contents_path>/Screenlock/Online Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="440"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="162"/>
        <source>Reset To Default</source>
        <translation>سۈكۈتكە قايتاي</translation>
        <extra-contents_path>/Screenlock/Reset To Default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="483"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="139"/>
        <source>Related Settings</source>
        <translation>مۇناسىۋەتلىك تەڭشەكلەر</translation>
        <extra-contents_path>/Screenlock/Related Settings</extra-contents_path>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="obsolete">TextLabel</translation>
    </message>
    <message>
        <source>Screenlock Set</source>
        <translation type="vanished">锁屏设置</translation>
    </message>
    <message>
        <source>Lock screen when screensaver boot</source>
        <translation type="vanished">ئېكراننى كۆزنەك تاقاشتا ئېكراننى قۇلۇپلاش</translation>
        <extra-contents_path>/Screenlock/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="332"/>
        <source>Lock screen delay</source>
        <translation>قۇلۇپ ئېكرانى كېچىكتۈرۈش</translation>
    </message>
    <message>
        <source>Min</source>
        <translation type="vanished">分钟</translation>
    </message>
    <message>
        <source>Select screenlock background</source>
        <translation type="vanished">选择锁屏背景</translation>
    </message>
    <message>
        <source>Browser online wp</source>
        <translation type="vanished">浏览线上壁纸</translation>
    </message>
    <message>
        <source>Browser local wp</source>
        <translation type="vanished">浏览本地壁纸</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="262"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="148"/>
        <source>Show picture of screenlock on screenlogin</source>
        <translation>screenlogin دا ئېكران قۇلۇپىنىڭ سۈرىتىنى كۆرسىتىش پروگراممىسى</translation>
        <extra-contents_path>/Screenlock/Show picture of screenlock on screenlogin</extra-contents_path>
    </message>
    <message>
        <source>Enabel screenlock</source>
        <translation type="vanished">开启锁屏</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">浏览</translation>
    </message>
    <message>
        <source>screenlock</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>picture</source>
        <translation type="obsolete">图片</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>Never</source>
        <translation>مەڭگۈ</translation>
    </message>
    <message>
        <source>1m</source>
        <translation type="vanished">1م</translation>
    </message>
    <message>
        <source>5m</source>
        <translation type="vanished">5م</translation>
    </message>
    <message>
        <source>10m</source>
        <translation type="vanished">10م</translation>
    </message>
    <message>
        <source>30m</source>
        <translation type="vanished">30م</translation>
    </message>
    <message>
        <source>45m</source>
        <translation type="vanished">45م</translation>
    </message>
    <message>
        <source>1h</source>
        <translation type="vanished">1ھ</translation>
    </message>
    <message>
        <source>1.5h</source>
        <translation type="vanished">1.5h</translation>
    </message>
    <message>
        <source>3h</source>
        <translation type="vanished">3ھ</translation>
    </message>
    <message>
        <source>2h</source>
        <translation type="vanished">2ھ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="563"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>تام قەغىزى ھۆججەتلىرى(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</translation>
    </message>
    <message>
        <source>allFiles(*.*)</source>
        <translation type="vanished">所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>1min</source>
        <translation>1min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>5min</source>
        <translation>5min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>10min</source>
        <translation>10min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>30min</source>
        <translation>30min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>45min</source>
        <translation>45min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>1hour</source>
        <translation>1ھۇدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>2hour</source>
        <translation>2خۇدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>3hour</source>
        <translation>3خۇدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="605"/>
        <source>select custom wallpaper file</source>
        <translation>خاس تام قەغىزى ھۆججىتىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="606"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="607"/>
        <source>Position: </source>
        <translation>ئورنى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="608"/>
        <source>FileName: </source>
        <translation>ھۆججەت نامى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="609"/>
        <source>FileType: </source>
        <translation>FileType: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="610"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="539"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="142"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="698"/>
        <source>Monitor Off</source>
        <translation>كۆزەتكۈچنى ئۆچۈرۈش</translation>
        <extra-contents_path>/Screenlock/Monitor Off</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="594"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="145"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="699"/>
        <source>Screensaver</source>
        <translation>ئېكران كۆرگەزمىچى</translation>
        <extra-contents_path>/Screenlock/Screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="558"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="613"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="700"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="701"/>
        <source>Set</source>
        <translation>بەلگىلەش</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="59"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="99"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="209"/>
        <source>Screensaver</source>
        <translation>ئېكران كۆرگەزمىچى</translation>
        <extra-contents_path>/Screensaver/Screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="201"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="213"/>
        <source>Idle time</source>
        <translation>بىكار ۋاقتى</translation>
        <extra-contents_path>/Screensaver/Idle time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="475"/>
        <source>Lock screen when activating screensaver</source>
        <translation>ئېكران ئېكرانىنى قوزغىتىپ تۇرغاندا ئېكراننى قۇلۇپلاش</translation>
    </message>
    <message>
        <source>Enable screensaver</source>
        <translation type="vanished">开启屏保</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="211"/>
        <source>Screensaver program</source>
        <translation>ئېكران ئېكرانى پروگراممىسى</translation>
        <extra-contents_path>/Screensaver/Screensaver program</extra-contents_path>
    </message>
    <message>
        <source>idle time</source>
        <translation type="vanished">等待时间</translation>
    </message>
    <message>
        <source>Min</source>
        <translation type="vanished">分钟</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="960"/>
        <source>Lock screen when screensaver boot</source>
        <translation>ئېكراننى كۆزنەك تاقاشتا ئېكراننى قۇلۇپلاش</translation>
        <extra-contents_path>/Screensaver/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <source>screensaver</source>
        <translation type="vanished">屏保</translation>
    </message>
    <message>
        <source>Default_ukui</source>
        <translation type="vanished">默认屏保</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="197"/>
        <source>View</source>
        <translation>كۆرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="915"/>
        <source>Text(up to 30 characters):</source>
        <translation>تېكىست(ئەڭ كۆپ بولغاندا 30 ھەرپ):</translation>
        <extra-contents_path>/Screensaver/Text(up to 30 characters):</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="950"/>
        <source>Show rest time</source>
        <translation>ئارام ئېلىش ۋاقتىنى كۆرسىتىش</translation>
        <extra-contents_path>/Screensaver/Show rest time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="272"/>
        <source>UKUI</source>
        <translation>UKUI</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="273"/>
        <source>Blank_Only</source>
        <translation>Blank_Only</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="284"/>
        <source>Customize</source>
        <translation>خاسلاشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="815"/>
        <source>5min</source>
        <translation>5min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="816"/>
        <source>10min</source>
        <translation>10min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>15min</source>
        <translation>15min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="817"/>
        <source>30min</source>
        <translation>30min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>1hour</source>
        <translation>1ھۇدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="716"/>
        <source>Screensaver source</source>
        <translation>ئېكران يۈزى مەنبەسى</translation>
        <extra-contents_path>/Screensaver/Screensaver source</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="722"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="772"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="730"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp *.svg)</source>
        <translation>تام قەغىزى ھۆججەتلىرى(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp *.svg)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="771"/>
        <source>select custom screensaver dir</source>
        <translation>خاس ئېكران كۆرگۈچ dir نى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="773"/>
        <source>Position: </source>
        <translation>ئورنى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="774"/>
        <source>FileName: </source>
        <translation>ھۆججەت نامى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="775"/>
        <source>FileType: </source>
        <translation>FileType: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="776"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="810"/>
        <source>Switching time</source>
        <translation>ئالماشتۇرۇش ۋاقتى</translation>
        <extra-contents_path>/Screensaver/Switching time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="293"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="814"/>
        <source>1min</source>
        <translation>1min</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="861"/>
        <source>Ordinal</source>
        <translation>ئوردىدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="871"/>
        <source>Random switching</source>
        <translation>خالىغانچە ئالماشتۇرۇش</translation>
        <extra-contents_path>/Screensaver/Random switching</extra-contents_path>
    </message>
    <message>
        <source>Display text</source>
        <translation type="vanished">显示文本</translation>
    </message>
    <message>
        <source>Enter text, up to 30 characters</source>
        <translation type="vanished">输入文本，最多30个字符</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="992"/>
        <source>Text position</source>
        <translation>تېكىست ئورنى</translation>
        <extra-contents_path>/Screensaver/Text position</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1000"/>
        <source>Centered</source>
        <translation>مەركەز قىلىنغان</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1001"/>
        <source>Randow(Bubble text)</source>
        <translation>رانۋېر (كۆپۈكچە تېكىست)</translation>
    </message>
    <message>
        <source>1m</source>
        <translation type="vanished">1m</translation>
    </message>
    <message>
        <source>5m</source>
        <translation type="vanished">5m</translation>
    </message>
    <message>
        <source>10m</source>
        <translation type="vanished">10m</translation>
    </message>
    <message>
        <source>30m</source>
        <translation type="vanished">30m</translation>
    </message>
    <message>
        <source>45m</source>
        <translation type="vanished">45m</translation>
    </message>
    <message>
        <source>1h</source>
        <translation type="vanished">1h</translation>
    </message>
    <message>
        <source>1.5h</source>
        <translation type="vanished">1.5h</translation>
    </message>
    <message>
        <source>3h</source>
        <translation type="vanished">3h</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="862"/>
        <source>Random</source>
        <translation>ئىختىيارىي</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="298"/>
        <source>Never</source>
        <translation>مەڭگۈ</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>Create Index</source>
        <translation type="vanished">创建索引</translation>
    </message>
    <message>
        <source>Create index</source>
        <translation type="vanished">创建索引</translation>
    </message>
    <message>
        <source>Creating index can help you getting results quickly.</source>
        <translation type="vanished">创建索引可以帮助您快速获取搜索结果</translation>
    </message>
    <message>
        <source>Web Engine</source>
        <translation type="vanished">搜索引擎</translation>
    </message>
    <message>
        <source>Default web searching engine</source>
        <translation type="vanished">默认互联网搜索引擎</translation>
    </message>
    <message>
        <source>baidu</source>
        <translation type="vanished">百度</translation>
    </message>
    <message>
        <source>sougou</source>
        <translation type="vanished">搜狗</translation>
    </message>
    <message>
        <source>360</source>
        <translation type="vanished">360</translation>
    </message>
    <message>
        <source>Block Folders</source>
        <translation type="vanished">屏蔽文件夹</translation>
    </message>
    <message>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation type="vanished">搜索将不再查看以下文件夹，通过添加和删除可以设置文件索引位置。</translation>
    </message>
    <message>
        <source>Choose folder</source>
        <translation type="vanished">选择要屏蔽的文件夹</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Directories</source>
        <translation type="vanished">文件夹</translation>
    </message>
    <message>
        <source>select blocked folder</source>
        <translation type="vanished">选择要屏蔽的文件夹</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">选择</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">位置: </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">文件名: </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">文件类型: </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation type="vanished">添加文件夹失败，路径为空！</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation type="vanished">添加文件夹失败，请选择用户目录下的文件夹！</translation>
    </message>
    <message>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation type="vanished">添加文件夹失败，父文件夹已被屏蔽！</translation>
    </message>
    <message>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation type="vanished">添加文件夹失败，该文件夹已被屏蔽！</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <source>Touchpad</source>
        <translation type="obsolete">触控板</translation>
    </message>
    <message>
        <location filename="../../searchwidget.cpp" line="61"/>
        <location filename="../../searchwidget.cpp" line="62"/>
        <location filename="../../searchwidget.cpp" line="69"/>
        <location filename="../../searchwidget.cpp" line="71"/>
        <location filename="../../searchwidget.cpp" line="76"/>
        <source>No search results</source>
        <translation>ئىزدەش نەتىجىسى يوق</translation>
    </message>
</context>
<context>
    <name>SecurityCenter</name>
    <message>
        <source>SecurityCenter</source>
        <translation type="vanished">安全中心</translation>
    </message>
    <message>
        <source>Computer Security Overview</source>
        <translation type="vanished">安全功能概览</translation>
    </message>
    <message>
        <source>Understand current computer security situation and take measures</source>
        <translation type="vanished">保障系统安全性,并采取有效措施</translation>
    </message>
    <message>
        <source>Summarize</source>
        <translation type="vanished">概述</translation>
    </message>
    <message>
        <source>Recognize the current security of the system, and can take the necessary settings</source>
        <translation type="vanished">了解系统当前安全性，并可采取必要的设置操作</translation>
    </message>
    <message>
        <source>Run Security Center</source>
        <translation type="vanished">打开安全中心</translation>
    </message>
    <message>
        <source>Security Center</source>
        <translation type="vanished">安全中心</translation>
    </message>
    <message>
        <source>Virus Protection</source>
        <translation type="vanished">病毒防护</translation>
    </message>
    <message>
        <source>Protect system from threats</source>
        <translation type="vanished">实时防护，帮助系统免受威胁</translation>
    </message>
    <message>
        <source>Network Protection</source>
        <translation type="vanished">网络保护</translation>
    </message>
    <message>
        <source>Setup app that can access web</source>
        <translation type="vanished">设置可访问网络的应用</translation>
    </message>
    <message>
        <source>App Execution Control</source>
        <translation type="vanished">应用执行控制</translation>
    </message>
    <message>
        <source>App install and exe protection</source>
        <translation type="vanished">应用程序安装和执行保护</translation>
    </message>
    <message>
        <source>Account Security</source>
        <translation type="vanished">帐户安全</translation>
    </message>
    <message>
        <source>Protect account and login security</source>
        <translation type="vanished">提供帐户相关的安全保障</translation>
    </message>
    <message>
        <source>Safety check-up</source>
        <translation type="vanished">安全体检</translation>
    </message>
    <message>
        <source>Detect abnormal configuration</source>
        <translation type="vanished">检查修复系统漏洞和异常配置</translation>
    </message>
    <message>
        <source>Virus defense</source>
        <translation type="vanished">病毒防护</translation>
    </message>
    <message>
        <source>Real time protection from virus threat</source>
        <translation type="vanished">保护系统免受恶意程序攻击</translation>
    </message>
    <message>
        <source>App protection</source>
        <translation type="vanished">应用控制与保护</translation>
    </message>
    <message>
        <source>App install</source>
        <translation type="vanished">提供应用程序安装,运行防护</translation>
    </message>
    <message>
        <source>Net protection</source>
        <translation type="vanished">网络保护</translation>
    </message>
    <message>
        <source>Secure Config</source>
        <translation type="vanished">系统安全配置</translation>
    </message>
    <message>
        <source>Simple Config</source>
        <translation type="vanished">启用系统安全功能的相关配置</translation>
    </message>
    <message>
        <source>Network protection</source>
        <translation type="vanished">网络保护</translation>
    </message>
    <message>
        <source>Manage and control network</source>
        <translation type="vanished">提供应用联网行为的管控</translation>
    </message>
    <message>
        <source>Secure mode configuration</source>
        <translatorcomment>启用系统安全功能配置</translatorcomment>
        <translation type="vanished">安全模式配置</translation>
    </message>
    <message>
        <source>Simple configuraion</source>
        <translation type="vanished">启用系统安全功能简易配置</translation>
    </message>
</context>
<context>
    <name>ShareMain</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">دىققەت</translation>
    </message>
    <message>
        <source>please select an output</source>
        <translation type="vanished">چىقىرىشنى تاللاڭ</translation>
    </message>
    <message>
        <source>Input Password</source>
        <translation type="vanished">ئىم كىرگۈزۈش</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ئىم</translation>
    </message>
    <message>
        <source>Share</source>
        <translation type="vanished">تەڭ بەھرىمان بولۇش</translation>
    </message>
    <message>
        <source>Output</source>
        <translation type="vanished">چىقىرىش</translation>
    </message>
    <message>
        <source>Input</source>
        <translation type="vanished">كىرگۈزۈش</translation>
    </message>
    <message>
        <source>Point</source>
        <translation type="vanished">نۇقتا</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">كۇنۇپكا تاختىسى</translation>
    </message>
    <message>
        <source>Clipboard</source>
        <translation type="vanished">قىسقۇچ تاختىسى</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">ياپ</translation>
    </message>
    <message>
        <source>ViewOnly</source>
        <translation type="vanished">كۆرۈش ئايرىم-ئايرىم ھالدا</translation>
    </message>
    <message>
        <source>Client Setting</source>
        <translation type="vanished">خېرىدار تەڭشىكى</translation>
    </message>
    <message>
        <source>Client Number</source>
        <translation type="vanished">خېرىدار نومۇرى</translation>
    </message>
    <message>
        <source>Client IP：</source>
        <translation type="vanished">خېرىدار IP:</translation>
    </message>
    <message>
        <source>退出程序</source>
        <translation type="vanished">退出程序</translation>
    </message>
    <message>
        <source>确认退出程序！</source>
        <translation type="vanished">确认退出程序！</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="68"/>
        <source>Remote Desktop</source>
        <translation>يىراق ئۈستەلئۈستى</translation>
        <extra-contents_path>/Vino/Remote Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="80"/>
        <source>Connect to your desktop remotely</source>
        <translation>ئۈستەلئۈستىڭىزگە يىراقتىن ئۇلاش</translation>
        <extra-contents_path>/Vino/Connect to your desktop remotely</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="98"/>
        <source>Allow others to connect to your desktop remotely using RDP</source>
        <translation>RDP ئارقىلىق باشقىلارنىڭ ئۈستەلئۈستىڭىزگە يىراقتىن ئۇلىشىغا يول قويۇش</translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using RDP</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="112"/>
        <source>Allow others to connect to your desktop remotely using VNC</source>
        <translation>VNC ئارقىلىق باشقىلارنىڭ ئۈستەلئۈستىڭىزگە يىراقتىن ئۇلىشىغا يول قويۇش</translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using VNC</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="160"/>
        <source>Require user to enter this password while using VNC: </source>
        <translation>VNC ئىشلەتكەندە ئىشلەتكۈچىدىن بۇ پارولنى كىرگۈزۈشنى تەلەپ قىلىمىز: </translation>
        <extra-contents_path>/Vino/Require user to enter this password while using VNC:</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="164"/>
        <source>Edit</source>
        <translation>تەھرىرلەش</translation>
    </message>
    <message>
        <source>Allow others to view your desktop</source>
        <translation type="vanished">باشقىلارنىڭ ئۈستەلئۈستىڭىزنى كۆرۈشىگە يول قويۇش</translation>
        <extra-contents_path>/Vino/Allow others to view your desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="126"/>
        <source>Allow connection to control screen</source>
        <translation>ئۇلاش ئارقىلىق ئېكراننى كونتىرول قىلىش</translation>
        <extra-contents_path>/Vino/Allow connection to control screen</extra-contents_path>
    </message>
    <message>
        <source>Security</source>
        <translation type="vanished">بىخەتەرلىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="142"/>
        <source>You must confirm every visit for this machine</source>
        <translation>سىز چوقۇم بۇ ماشىنىنىڭ ھەر بىر زىيارەتنى جەزملەشتۈرۈشىڭىز كېرەك.</translation>
        <extra-contents_path>/Vino/You must confirm every visit for this machine</extra-contents_path>
    </message>
    <message>
        <source>Require user to enter this password: </source>
        <translation type="vanished">ئىشلەتكۈچىدىن بۇ مەخپىي شىفىرنى كىرگۈزۈشنى تەلەپ قىلىش: </translation>
        <extra-contents_path>/Vino/Require user to enter this password:</extra-contents_path>
    </message>
    <message>
        <source>Password can not be blank</source>
        <translation type="vanished">پارولنى بوش قويغىلى بولمايدۇ</translation>
    </message>
    <message>
        <source>Password length must be less than or equal to 8</source>
        <translation type="vanished">مەخپىي نومۇرنىڭ ئۇزۇنلۇقى چوقۇم 8 دىن تۆۋەن ياكى تەڭ بولۇشى كېرەك</translation>
    </message>
    <message>
        <source>Password length is greater than 8</source>
        <translation type="vanished">密码长度大于８</translation>
    </message>
</context>
<context>
    <name>ShareMainHw</name>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>please select an output</source>
        <translation>چىقىرىشنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="331"/>
        <source>Input Password</source>
        <translation>ئىم كىرگۈزۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="332"/>
        <source>Password</source>
        <translation>ئىم</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="445"/>
        <source>Share</source>
        <translation>تەڭ بەھرىمان بولۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="461"/>
        <source>Allow others to view your desktop</source>
        <translation>باشقىلارنىڭ ئۈستەلئۈستىڭىزنى كۆرۈشىگە يول قويۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="471"/>
        <source>Security</source>
        <translation>بىخەتەرلىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="481"/>
        <source>Require user to enter this password: </source>
        <translation>ئىشلەتكۈچىدىن بۇ مەخپىي شىفىرنى كىرگۈزۈشنى تەلەپ قىلىش: </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="483"/>
        <source>Edit</source>
        <translation>تەھرىرلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="496"/>
        <source>Output</source>
        <translation>چىقىرىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="508"/>
        <source>Input</source>
        <translation>كىرگۈزۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="518"/>
        <source>Point</source>
        <translation>نۇقتا</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="520"/>
        <source>Keyboard</source>
        <translation>كۇنۇپكا تاختىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="522"/>
        <source>Clipboard</source>
        <translation>قىسقۇچ تاختىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="548"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="550"/>
        <source>ViewOnly</source>
        <translation>كۆرۈش ئايرىم-ئايرىم ھالدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="558"/>
        <source>Client Setting</source>
        <translation>خېرىدار تەڭشىكى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="569"/>
        <source>Client Number</source>
        <translation>خېرىدار نومۇرى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="583"/>
        <source>Client IP：</source>
        <translation>خېرىدار IP:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>退出程序</source>
        <translation>退出程序</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>确认退出程序！</source>
        <translation>确认退出程序！</translation>
    </message>
</context>
<context>
    <name>Shortcut</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="50"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="164"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="178"/>
        <source>System Shortcut</source>
        <translation>سىستېما قىسقارتىش يولى</translation>
        <extra-contents_path>/Shortcut/System Shortcut</extra-contents_path>
    </message>
    <message>
        <source>Show all shortcut</source>
        <translation type="vanished">显示全部快捷键</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="103"/>
        <source>Custom Shortcut</source>
        <translation>ئىختىيارى قىسقا يول</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="166"/>
        <source>Customize Shortcut</source>
        <translation>قىسقا يولنى تەڭشەش</translation>
        <extra-contents_path>/Shortcut/Customize Shortcut</extra-contents_path>
    </message>
    <message>
        <source>Add custom shortcut</source>
        <translation type="vanished">添加自定义快捷键</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="161"/>
        <source>Add</source>
        <translation>قوش</translation>
        <extra-contents_path>/Shortcut/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="441"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="573"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="836"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="442"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="574"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="837"/>
        <source>Use</source>
        <translation>ئىشلىتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="443"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="575"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="838"/>
        <source>Shortcut key conflict, use it?</source>
        <translation>قىسقا يول ئاچقۇچ توقۇنۇشى، ئىشلىتەمسىز؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="444"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="576"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="839"/>
        <source>%1 occuied, using this combination will invalidate %2</source>
        <translation>٪1 قوشۇلدى، بۇ بىرىكمە ئارقىلىق ٪2 ئىناۋەتسىز بولىدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="467"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="606"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="868"/>
        <source>Shortcut &quot;%1&quot; occuied, please change the key combination</source>
        <translation>قىسقا يول «٪1» نى ئۆزگەرتىڭ، كونۇپكا بىرىكمىسىنى ئالماشتۇرۇڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="677"/>
        <source>Edit</source>
        <translation>تەھرىرلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="678"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1039"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1043"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1061"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1065"/>
        <source>Null</source>
        <translation>Null</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1126"/>
        <source> or </source>
        <translation> ياكى </translation>
    </message>
    <message>
        <source>disable</source>
        <translation type="vanished">无效</translation>
    </message>
    <message>
        <source>Reset default</source>
        <translation type="vanished">恢复默认快捷键</translation>
    </message>
    <message>
        <source>shortcut</source>
        <translation type="vanished">快捷键</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="80"/>
        <source>Shortcut</source>
        <translation>قىسقا يول</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">系统</translation>
    </message>
</context>
<context>
    <name>ShowAllShortcut</name>
    <message>
        <source>Dialog</source>
        <translation type="obsolete">更改时间</translation>
    </message>
    <message>
        <source>System Shortcuts</source>
        <translation type="vanished">快捷键</translation>
    </message>
    <message>
        <source>Show all shortcut</source>
        <translation type="vanished">显示全部快捷键</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">桌面</translation>
    </message>
</context>
<context>
    <name>StatusDialog</name>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="10"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="59"/>
        <source>Activation Code</source>
        <translation>قوزغىتىش كودى</translation>
    </message>
</context>
<context>
    <name>SuccessDiaolog</name>
    <message>
        <source>Reback sign in</source>
        <translation type="vanished">重新登录</translation>
    </message>
    <message>
        <source>Sign up success!</source>
        <translation type="vanished">注册账号成功！</translation>
    </message>
    <message>
        <source>Reset success!</source>
        <translation type="vanished">重置密码成功！</translation>
    </message>
    <message>
        <source>Sign in success!</source>
        <translation type="vanished">登录帐号成功！</translation>
    </message>
    <message>
        <source>Binding phone success!</source>
        <translation type="vanished">绑定手机！</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>SyncDialog</name>
    <message>
        <source>Sync</source>
        <translation type="vanished">同步</translation>
    </message>
    <message>
        <source>Do not</source>
        <translation type="vanished">放弃</translation>
    </message>
    <message>
        <source>Last sync at %1</source>
        <translation type="vanished">上次同步于%1</translation>
    </message>
    <message>
        <source>Sync now?</source>
        <translation type="vanished">选择以下同步覆盖，继续？</translation>
    </message>
    <message>
        <source>Wallpaper</source>
        <translation type="vanished">桌面壁纸</translation>
    </message>
    <message>
        <source>ScreenSaver</source>
        <translation type="vanished">屏保</translation>
    </message>
    <message>
        <source>Font</source>
        <translation type="vanished">字体</translation>
    </message>
    <message>
        <source>Avatar</source>
        <translation type="vanished">头像</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">开始菜单</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">任务栏</translation>
    </message>
    <message>
        <source>Quick Start</source>
        <translation type="vanished">快速启动项</translation>
    </message>
    <message>
        <source>Themes</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">鼠标</translation>
    </message>
    <message>
        <source>TouchPad</source>
        <translation type="vanished">触控板</translation>
    </message>
    <message>
        <source>KeyBoard</source>
        <translation type="vanished">键盘</translation>
    </message>
    <message>
        <source>ShortCut</source>
        <translation type="vanished">快捷键</translation>
    </message>
    <message>
        <source>Area</source>
        <translation type="vanished">区域语言</translation>
    </message>
    <message>
        <source>Date/Time</source>
        <translation type="vanished">时间日期</translation>
    </message>
    <message>
        <source>Default Open</source>
        <translation type="vanished">默认打开方式</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Peony</source>
        <translation type="vanished">文件管理器</translation>
    </message>
    <message>
        <source>Boot</source>
        <translation type="vanished">开机启动项</translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="vanished">电源</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation type="vanished">文本编辑器</translation>
    </message>
    <message>
        <source>Terminal</source>
        <translation type="vanished">终端</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">影音</translation>
    </message>
</context>
<context>
    <name>TabWid</name>
    <message>
        <source>Check Update</source>
        <translation type="vanished">检查更新</translation>
    </message>
    <message>
        <source>initializing</source>
        <translation type="vanished">初始化中</translation>
    </message>
    <message>
        <source>Service connection abnormal,please retest!</source>
        <translation type="vanished">服务连接异常，请重新检测！</translation>
    </message>
    <message>
        <source>Prompt information</source>
        <translation type="vanished">提示信息</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation type="vanished">立即更新</translation>
    </message>
    <message>
        <source>Cancel update</source>
        <translation type="vanished">取消更新</translation>
    </message>
    <message>
        <source>No,I Don&apos;t Backup</source>
        <translation type="vanished">否，我不备份</translation>
    </message>
    <message>
        <source>Being updated...</source>
        <translation type="vanished">正在更新...</translation>
    </message>
    <message>
        <source>UpdateAll</source>
        <translation type="vanished">全部更新</translation>
    </message>
    <message>
        <source>The backup restore partition could not be found. The system will not be backed up in this update!</source>
        <translation type="vanished">未能找到备份还原分区，本次更新不会备份系统！</translation>
    </message>
    <message>
        <source>Kylin backup restore tool is doing other operations, please update later.</source>
        <translation type="vanished">麒麟备份还原工具正在进行其他操作，请稍后更新</translation>
    </message>
    <message>
        <source>The source manager configuration file is abnormal, the system temporarily unable to update!</source>
        <translation type="vanished">源管理器配置文件异常，暂时无法更新！</translation>
    </message>
    <message>
        <source>Backup already, no need to backup again.</source>
        <translation type="vanished">已备份，无需再次备份</translation>
    </message>
    <message>
        <source>Kylin backup restore tool does not exist, this update will not backup the system!</source>
        <translation type="vanished">麒麟备份还原工具不存在，本次更新不会备份系统</translation>
    </message>
    <message>
        <source>Backup complete.</source>
        <translation type="vanished">备份完成</translation>
    </message>
    <message>
        <source>In backup:</source>
        <translation type="vanished">备份中：</translation>
    </message>
    <message>
        <source>Start backup,getting progress</source>
        <translation type="vanished">开始备份，正在获取进度</translation>
    </message>
    <message>
        <source>Ready to install</source>
        <translation type="vanished">准备安装</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Please back up the system before all updates to avoid unnecessary losses</source>
        <translation type="vanished">请在所有更新前备份系统，以免造成不必要的损失</translation>
    </message>
    <message>
        <source>Only Update</source>
        <translation type="vanished">仅更新</translation>
    </message>
    <message>
        <source>Back And Update</source>
        <translation type="vanished">备份并更新</translation>
    </message>
    <message>
        <source>Failed to write configuration file, this update will not back up the system!</source>
        <translation type="vanished">写入配置文件失败，本次更新不会备份系统！</translation>
    </message>
    <message>
        <source>Insufficient backup space, this update will not backup your system!</source>
        <translation type="vanished">备份空间不足，本次更新不会备份系统！</translation>
    </message>
    <message>
        <source>Kylin backup restore tool could not find the UUID, this update will not backup the system!</source>
        <translation type="vanished">麒麟备份还原工具无法找到UUID，本次更新不会备份系统</translation>
    </message>
    <message>
        <source>Backup interrupted, stop updating!</source>
        <translation type="vanished">备份过程被中断，停止更新！</translation>
    </message>
    <message>
        <source>Failed to connect to software warehouse!</source>
        <translation type="vanished">连接软件仓库失败</translation>
    </message>
    <message>
        <source>Downloading and installing updates...</source>
        <translation type="vanished">正在下载并安装更新...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>System is backing up...</source>
        <translation type="vanished">系统正在备份中...</translation>
    </message>
    <message>
        <source>Backup finished!</source>
        <translation type="vanished">备份完成！</translation>
    </message>
    <message>
        <source>Kylin backup restore tool exception:</source>
        <translation type="vanished">麒麟备份还原工具异常：</translation>
    </message>
    <message>
        <source>There will be no backup in this update!</source>
        <translation type="vanished">本次更新不会备份系统！</translation>
    </message>
    <message>
        <source>Getting update list</source>
        <translation type="vanished">正在获取更新列表</translation>
    </message>
    <message>
        <source>Software source update failed: </source>
        <translation type="vanished">软件源更新失败：</translation>
    </message>
    <message>
        <source>Update software source :</source>
        <translation type="vanished">更新软件源进度：</translation>
    </message>
    <message>
        <source>Reconnect times:</source>
        <translation type="vanished">重连次数：</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">更新</translation>
    </message>
    <message>
        <source>View history</source>
        <translation type="vanished">查看更新历史</translation>
    </message>
    <message>
        <source>Update Settings</source>
        <translation type="vanished">更新设置</translation>
    </message>
    <message>
        <source>Allowed to renewable notice</source>
        <translation type="vanished">允许通知可更新的应用</translation>
    </message>
    <message>
        <source>Backup current system before updates all</source>
        <translation type="vanished">全部更新前备份系统</translation>
    </message>
    <message>
        <source>Your system is the latest!</source>
        <translation type="vanished">您的系统已是最新！</translation>
    </message>
    <message>
        <source>Updatable app detected on your system!</source>
        <translation type="vanished">检测到你的系统有可更新的应用！</translation>
    </message>
    <message>
        <source>Automatically download and install updates</source>
        <translation type="vanished">自动下载和安装更新</translation>
    </message>
    <message>
        <source>After it is turned on, the system will automatically download and install updates when there is an available network and available backup and restore partitions.</source>
        <translation type="vanished">开启后，当有可用网络和可用备份和恢复分区时，系统会自动下载和安装更新</translation>
    </message>
    <message>
        <source>Last refresh:</source>
        <translation type="vanished">上次更新：</translation>
    </message>
    <message>
        <source>Last Checked:</source>
        <translation type="vanished">上次检测：</translation>
    </message>
    <message>
        <source>trying to reconnect </source>
        <translation type="vanished">重新尝试连接</translation>
    </message>
    <message>
        <source> times</source>
        <translation type="vanished">次数</translation>
    </message>
    <message>
        <source>Updating the software source</source>
        <translation type="vanished">正在更新软件源</translation>
    </message>
    <message>
        <source>This update will not backup the current system, do you want to continue the update?</source>
        <translation type="vanished">本次更新不会备份当前系统，是否继续更新？</translation>
    </message>
    <message>
        <source>Yes, keep updating</source>
        <translation type="vanished">是，继续更新</translation>
    </message>
    <message>
        <source>No, backup now</source>
        <translation type="vanished">否，立即备份</translation>
    </message>
    <message>
        <source>Not updated</source>
        <translation type="vanished">暂不更新</translation>
    </message>
    <message>
        <source>Part of the update failed!</source>
        <translation type="vanished">部分更新失败！</translation>
    </message>
    <message>
        <source>An important update is in progress, please wait.</source>
        <translation type="vanished">正在进行一项重要更新，请等待。</translation>
    </message>
    <message>
        <source>The backup restore partition is abnormal. You may not have a backup restore partition.For more details,see /var/log/backup.log</source>
        <translation type="vanished">备份还原分区异常，您可能没有备份还原分区。更多详细信息，可以参看/var/log/backup.log</translation>
    </message>
    <message>
        <source>Other err! please refers /var/log/backup.txt!</source>
        <translation type="vanished">其他错误!请查看/var/log/backup.txt</translation>
    </message>
    <message>
        <source>Calculating Capacity...</source>
        <translation type="vanished">计算系统空间大小</translation>
    </message>
    <message>
        <source>Update software source progress:</source>
        <translation type="vanished">更新软件源进度：</translation>
    </message>
    <message>
        <source>Reconnect times</source>
        <translation type="vanished">重连次数</translation>
    </message>
    <message>
        <source>Allows notifications to be updatable for applications.</source>
        <translation type="vanished">允许通知可更新的应用</translation>
    </message>
    <message>
        <source>Backup the current system as a rollback version before all updates</source>
        <translation type="vanished">全部更新前备份当前系统为可回退的版本</translation>
    </message>
</context>
<context>
    <name>Theme</name>
    <message>
        <source>Theme Mode</source>
        <translation type="vanished">主题模式</translation>
        <extra-contents_path>/Theme/Theme Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="107"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="521"/>
        <source>Theme</source>
        <translation>ئۇسلۇب</translation>
        <extra-contents_path>/Theme/Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <source>Default</source>
        <translation>كۆڭۈلدىكى سۆز</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="183"/>
        <source>Light</source>
        <translation>نۇر</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="181"/>
        <source>Dark</source>
        <translation>قاراڭغۇلۇق</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="185"/>
        <source>Auto</source>
        <translation>ئاپتۇماتىك</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="422"/>
        <source>Corlor</source>
        <translation>Corlor</translation>
        <extra-contents_path>/Theme/Corlor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="604"/>
        <source>Other</source>
        <translation>باشقا</translation>
        <extra-contents_path>/Theme/Other</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="610"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="624"/>
        <source>Set</source>
        <translation>بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="616"/>
        <source>Wallpaper</source>
        <translation>تام قەغىزى</translation>
        <extra-contents_path>/Theme/Wallpaper</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="630"/>
        <source>Beep</source>
        <translation>بىتچىت</translation>
        <extra-contents_path>/Theme/Beep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="961"/>
        <source>Blue-Crystal</source>
        <translation>كۆك كىرىستال</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="963"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <source>Light-Seeking</source>
        <translation>يورۇقلۇق ئىزدەيمەن</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="965"/>
        <source>DMZ-Black</source>
        <translation>DMZ-Black</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="967"/>
        <source>DMZ-White</source>
        <translation>DMZ-White</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="969"/>
        <source>Dark-Sense</source>
        <translation>زۇلمەتلىك سەزگۈ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1084"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>basic</source>
        <translation>ئاساسىي</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>Classic</source>
        <translation>نادىر ناخشا – فىلىم</translation>
    </message>
    <message>
        <source>Origins-Tracing</source>
        <translation type="vanished">溯源</translation>
    </message>
    <message>
        <source>fashion</source>
        <translation type="vanished">时尚</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1092"/>
        <source>hp</source>
        <translation>hp</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>ukui</source>
        <translation>زۇكۇي</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1090"/>
        <source>HeYin</source>
        <translation>خېيىن</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>classic</source>
        <translation>نادىر</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1096"/>
        <source>daybreakBlue</source>
        <translation>daybreakBlue</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1098"/>
        <source>jamPurple</source>
        <translation>jamPurple</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1100"/>
        <source>magenta</source>
        <translation>magenta</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1102"/>
        <source>sunRed</source>
        <translation>كۈنRed</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1104"/>
        <source>sunsetOrange</source>
        <translation>sunsetOrange</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1106"/>
        <source>dustGold</source>
        <translation>چاڭگۇلد</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1108"/>
        <source>polarGreen</source>
        <translation>polarGreen</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">كۆڭۈلدىكىدەك</translation>
    </message>
    <message>
        <source>Middle</source>
        <translation type="vanished">中</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="212"/>
        <source>Window Theme</source>
        <translation>كۆزنەك ئۇسلۇبى</translation>
        <extra-contents_path>/Theme/Window Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="214"/>
        <source>Icon theme</source>
        <translation>سىنبەلگىلىك ئۇسلۇب</translation>
        <extra-contents_path>/Theme/Icon theme</extra-contents_path>
    </message>
    <message>
        <source>Control theme</source>
        <translation type="vanished">ئۇسلۇبنى كونترول قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="217"/>
        <source>Cursor theme</source>
        <translation>Cursor ئۇسلۇبى</translation>
        <extra-contents_path>/Theme/Cursor theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="134"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="221"/>
        <source>Effect setting</source>
        <translation>ئۈنۈم تەڭشىكى</translation>
        <extra-contents_path>/Theme/Effect setting</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="352"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="225"/>
        <source>Transparency</source>
        <translation>سۈزۈكلۈك</translation>
        <extra-contents_path>/Theme/Transparency</extra-contents_path>
    </message>
    <message>
        <source>Transparent effects</source>
        <translation type="vanished">透明特效</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="249"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="223"/>
        <source>Performance mode</source>
        <translation>ئىجرا قىلىش شەكلى</translation>
        <extra-contents_path>/Theme/Performance mode</extra-contents_path>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="vanished">透明特效</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="vanished">低</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="vanished">高</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="473"/>
        <source>Reset to default</source>
        <translation>كۆڭۈلدىكى ھالىتىگە قايتاي</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">主题</translation>
    </message>
</context>
<context>
    <name>TimeBtn</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="80"/>
        <source>Tomorrow</source>
        <translation>ئەتە</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="82"/>
        <source>Yesterday</source>
        <translation>تۈنۈگۈن</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="84"/>
        <source>Today</source>
        <translation>تارىخ-بۈگۈن</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="102"/>
        <source>%1 hours earlier than local</source>
        <translation>يەرلىكتىن ٪1 سائەت بۇرۇن</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="104"/>
        <source>%1 hours later than local</source>
        <translation>يەرلىكتىن ٪1 سائەت كېيىن</translation>
    </message>
</context>
<context>
    <name>TimeZoneChooser</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="38"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="39"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <source>Change time zone</source>
        <translation type="vanished">更改时区</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="36"/>
        <source>Search Timezone</source>
        <translation>ئىزدە ۋاقىت رايونى</translation>
    </message>
    <message>
        <source>Input what you are looking for</source>
        <translation type="vanished">搜索时区</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="65"/>
        <source>To select a time zone, please click where near you on the map and select a city from the nearest city</source>
        <translation>ۋاقىت رايونىنى تاللاش ئۈچۈن خەرىتىدىكى ئۆزىڭىزنىڭ يېنىدىكى يەرنى چېكىپ، ئەڭ يېقىن شەھەردىن شەھەر تاللاڭ</translation>
    </message>
    <message>
        <source>change timezone</source>
        <translation type="vanished">修改系统时区</translation>
    </message>
    <message>
        <source>change zonne</source>
        <translation type="vanished">更改时区</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="41"/>
        <source>Change Timezone</source>
        <translation>ۋاقىت رايونىنى ئۆزگەرتىش</translation>
    </message>
</context>
<context>
    <name>TouchScreen</name>
    <message>
        <source>TouchScreen</source>
        <translation type="vanished">触摸屏</translation>
    </message>
    <message>
        <source>monitor</source>
        <translation type="vanished">显示器</translation>
    </message>
    <message>
        <source>touch id</source>
        <translation type="vanished">触摸屏标识</translation>
    </message>
    <message>
        <source>map</source>
        <translation type="vanished">触摸映射</translation>
    </message>
    <message>
        <source>calibration</source>
        <translation type="vanished">触摸校准</translation>
    </message>
    <message>
        <source>No touch screen found</source>
        <translation type="vanished">未发现触摸屏设备</translation>
    </message>
    <message>
        <source>input device</source>
        <translation type="vanished">触摸设备</translation>
    </message>
</context>
<context>
    <name>Touchpad</name>
    <message>
        <source>Touchpad Settings</source>
        <translation type="vanished">触控板设置</translation>
    </message>
    <message>
        <source>Enabled touchpad</source>
        <translation type="vanished">启用触控板</translation>
    </message>
    <message>
        <source>Disable touchpad while typing</source>
        <translation type="vanished">打字时禁用触控板</translation>
    </message>
    <message>
        <source>Enable mouse clicks with touchpad</source>
        <translation type="vanished">启用触控板的鼠标点击</translation>
    </message>
    <message>
        <source> Mouse to disable  touchpad</source>
        <translation type="vanished">连接鼠标或无线触控板时禁用内置触摸版</translation>
    </message>
    <message>
        <source>Scrolling</source>
        <translation type="vanished">滚动</translation>
    </message>
    <message>
        <source>No touchpad found</source>
        <translation type="vanished">未发现触控板设备</translation>
    </message>
    <message>
        <source>touchpad</source>
        <translation type="vanished">触控板</translation>
    </message>
    <message>
        <source>Touchpad</source>
        <translation type="vanished">Touchpad</translation>
    </message>
    <message>
        <source>Disable rolling</source>
        <translation type="vanished">禁用滚动</translation>
    </message>
    <message>
        <source>Edge scrolling</source>
        <translation type="vanished">边界滚动</translation>
    </message>
    <message>
        <source>Two-finger scrolling</source>
        <translation type="vanished">双指滚动</translation>
    </message>
    <message>
        <source>Vertical edge scrolling</source>
        <translation type="vanished">垂直边界滚动</translation>
    </message>
    <message>
        <source>Horizontal edge scrolling</source>
        <translation type="vanished">水平边界滚动</translation>
    </message>
    <message>
        <source>Vertical two-finger scrolling</source>
        <translation type="vanished">垂直双指滚动</translation>
    </message>
    <message>
        <source>Horizontal two-finger scrolling</source>
        <translation type="vanished">水平双指滚动</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <source>Touchpad Setting</source>
        <translation type="vanished">Touchpad تەڭشىكى</translation>
    </message>
    <message>
        <source>No touchpad found</source>
        <translation type="vanished">未发现触控板设备</translation>
    </message>
    <message>
        <source>Disable touchpad when using the mouse</source>
        <translation type="vanished">مائۇسنى ئىشلەتكەندە Touchpad نى ئىشلىتىشنى بىكار قىلىش</translation>
        <extra-contents_path>/Touchpad/Disable touchpad when using the mouse</extra-contents_path>
    </message>
    <message>
        <source>Cursor Speed</source>
        <translation type="vanished">光标速度</translation>
        <extra-contents_path>/Touchpad/Cursor Speed</extra-contents_path>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">ئاستا</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">روزا</translation>
    </message>
    <message>
        <source>Disable touchpad when typing</source>
        <translation type="vanished">خەت يازغاندا touchpad نى ئىچىشنى بىكار قىلىش</translation>
        <extra-contents_path>/Touchpad/Disable touchpad when typing</extra-contents_path>
    </message>
    <message>
        <source>Touch and click on the touchpad</source>
        <translation type="vanished">touchpad نى چېكىش ۋە چېكىش</translation>
        <extra-contents_path>/Touchpad/Touch and click on the touchpad</extra-contents_path>
    </message>
    <message>
        <source>Scroll bar slides with finger</source>
        <translation type="vanished">بارماق بىلەن سىيرىلما تاختا سىيرىلما</translation>
        <extra-contents_path>/Touchpad/Scroll bar slides with finger</extra-contents_path>
    </message>
    <message>
        <source>Scrolling area</source>
        <translation type="vanished">سىيرىلما رايونى</translation>
        <extra-contents_path>/Touchpad/Scrolling area</extra-contents_path>
    </message>
    <message>
        <source>Disable scrolling</source>
        <translation type="vanished">سىيرىلما دىسكىلارنى بىكار قىلىش</translation>
    </message>
    <message>
        <source>Edge scrolling</source>
        <translation type="vanished">گىرۋىكىگە سىيرىلما</translation>
    </message>
    <message>
        <source>Pointer Speed</source>
        <translation type="vanished">نۇقتا تېزلىكى</translation>
        <extra-contents_path>/Touchpad/Pointer Speed</extra-contents_path>
    </message>
    <message>
        <source>Two-finger scrolling in the middle area</source>
        <translation type="vanished">ئوتتۇرا رايوندا ئىككى بارماق بىلەن سىيرىلما</translation>
    </message>
</context>
<context>
    <name>TrialDialog</name>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="12"/>
        <source>Set</source>
        <translation>بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="37"/>
        <source>Yinhe Kylin OS(Trail Version) Disclaimer</source>
        <translation>يىنخې Kylin OS (ئىز نۇسخىسى) نى تارقاتقۇچى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="46"/>
        <source>Dear customer:
       Thank you for trying Yinhe Kylin OS(trail version)! This version is free for users who only try out, no commercial purpose is permitted. The trail period lasts one year and it starts from the ex-warehouse time of the OS. No after-sales service is provided during the trail stage. If any security problems occurred when user put important files or do any commercial usage in system, all consequences are taken by users. Kylin software Co., Ltd. take no legal risk in trail version.
       During trail stage,if you want any technology surpport or activate the system, please buy“Yinhe Kylin Operating System”official version or authorization by contacting 400-089-1870.</source>
        <translation>ھۆرمەتلىك خېرىدار:
       يىنخې Kylin OS(ئىز نۇسخىسى)نى سىناپ باققىنىڭىزغا رەھمەت! بۇ نەشرى پەقەت سىناپ باققان ئابونتلارغا ھەقسىز، سودا مەقسىتىگە يول قويۇلمايدۇ. ئىز قوغلاش ۋاقتى بىر يىل داۋاملىشىدۇ، ئۇ OS نىڭ ئالدىنقى ئىسكىلات ۋاقتىدىن باشلىنىدۇ.ئىز قوغلاپ سېتىش باسقۇچىدا سېتىشتىن كېيىنكى مۇلازىمەت بىلەن تەمىنلىمەيدۇ. ئەگەر ئىشلەتكۈچى مۇھىم ھۆججەتلەرنى قويغاندا ياكى سىستېما ئىچىدىكى ھەرقانداق سودا ئىشلىتىشنى قىلغاندا بىخەتەرلىك مەسىلىسى كۆرۈلسە، بارلىق ئاقىۋەتلەرنى ئىشلەتكۈچىلەر ئۈستىگە ئالىدۇ. Kylin يۇمشاق دېتال چەكلىك شىركىتى ئىز نۇسخىسىدا ھېچقانداق قانۇنىي خەتەرگە تەۋەككۈل قىلالمايدۇ.
       ئىز-دېرەكسىز باسقۇچتا، ئەگەر سىز ھەرقانداق تېخنىكىنى قوللانماقچى ياكى سىستېمىنى ئاكتىپلاشتۇرماقچى بولسىڭىز، 400-089-1870 بىلەن ئالاقىلاشقاندا «يىنخې جىلۋىن مەشغۇلات سىستېمىسى»نىڭ رەسمىي نۇسخىسى ياكى ھوقۇقلۇق نۇسخىسىنى سېتىۋالسىڭىز بولىدۇ.</translation>
    </message>
    <message>
        <source>Dear customer:
    Thank you for trying Yinhe Kylin OS(trail version)! This version is free for users who only try out, no commercial purpose is permitted. The trail period lasts one year and it starts from the ex-warehouse time of the OS. No after-sales service is provided during the trail stage. If any security problems occurred when user put important files or do any commercial usage in system, all consequences are taken by users. Kylin software Co., Ltd. take no legal risk in trail version.
    During trail stage,if you want any technology surpport or activate the system, please buy“Yinhe Kylin Operating System”official version or authorization by contacting 400-089-1870.</source>
        <translation type="vanished">尊敬的客户：
    您好！随机安装的“银河麒麟操作系统（试用版）”是针对该版本对应的行业客户的免费试用版本，用于整机的试用、测试和评估，不能用于其他任何商业用途。此试用版本以软件出库时间计时，试用时间为一年。试用期间不提供相关正版软件的售后服务，如果客户在试用版本上自行存放重要文件及私自进行商业用途，由此产生的任何安全问题及结果一概由用户自己承担，麒麟软件有限公司不承担任何法律风险。
    在试用过程中，如希望激活或者得到专业的技术服务支持，请您购买“银河麒麟操作系统”正式版本或授权，联系方式如下：400-089-1870。</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="60"/>
        <source>Kylin software Co., Ltd.</source>
        <translation>Kylin يۇمشاق دېتال چەكلىك شىركىتى</translation>
    </message>
    <message>
        <source>www.Kylinos.cn</source>
        <translation type="vanished">www.kylinos.cn</translation>
    </message>
</context>
<context>
    <name>UkccAbout</name>
    <message>
        <location filename="../../ukccabout.cpp" line="33"/>
        <location filename="../../ukccabout.cpp" line="59"/>
        <source>Settings</source>
        <translation>تەڭشەكلەر</translation>
    </message>
    <message>
        <source>UKCC</source>
        <translation type="vanished">控制面板</translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="64"/>
        <source>Version: </source>
        <translation>نەشرى: </translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="74"/>
        <source>Service and Support:</source>
        <translation>مۇلازىمەت ۋە قوللاش:</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="vanished">开发者:</translation>
    </message>
    <message>
        <source>Vesion</source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>The control panel provides a friendly graphical user interface to manage common configuration items of the operating system. System configuration provides system, equipment, personalization, network, account, time and date, account, time and date, update, notification and operation module operations. </source>
        <translation type="vanished">“设置”提供了一个友好的用户图形界面，以及易于操作的功能模块划分，比如：系统、设备、主题、时间日期、语言、更新和安全等模块。您可以更改系统设置来自定系统。</translation>
    </message>
</context>
<context>
    <name>UkmediaApplicationWidget</name>
    <message>
        <source>Application Volume</source>
        <translation type="obsolete">应用音量</translation>
    </message>
    <message>
        <source>No application is currently playing or recording audio</source>
        <translation type="obsolete">当前没有应用程序正在播放或录制音频</translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <source>Input</source>
        <translation type="vanished">输入</translation>
    </message>
    <message>
        <source>Input Device</source>
        <translation type="vanished">选择输入设备</translation>
    </message>
    <message>
        <source>Input Device:</source>
        <translation type="vanished">选择输入设备</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">音量</translation>
    </message>
    <message>
        <source>Input Level</source>
        <translation type="vanished">输入反馈</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="vanished">低</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="vanished">高</translation>
    </message>
    <message>
        <source>Select input device</source>
        <translation type="vanished">选择输入设备</translation>
    </message>
    <message>
        <source>Input device</source>
        <translation type="vanished">选择输入设备</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">音量大小</translation>
    </message>
    <message>
        <source>Input level</source>
        <translation type="vanished">输入等级</translation>
    </message>
    <message>
        <source>low</source>
        <translation type="vanished">低</translation>
    </message>
    <message>
        <source>high</source>
        <translation type="vanished">高</translation>
    </message>
    <message>
        <source>Connector</source>
        <translation type="vanished">连接器</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <source>sound error</source>
        <translation type="vanished">声音错误</translation>
    </message>
    <message>
        <source>load sound failed</source>
        <translation type="vanished">加载声音失败</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">无</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <source>Output</source>
        <translation type="vanished">输出</translation>
    </message>
    <message>
        <source>Output Device</source>
        <translation type="vanished">选择输出设备</translation>
    </message>
    <message>
        <source>Output Device:</source>
        <translation type="vanished">选择输出设备</translation>
    </message>
    <message>
        <source>Master Volume</source>
        <translation type="vanished">音量</translation>
    </message>
    <message>
        <source>Balance</source>
        <translation type="vanished">声道平衡</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="vanished">右</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation type="vanished">配置</translation>
    </message>
    <message>
        <source>Card</source>
        <translation type="vanished">声卡</translation>
    </message>
    <message>
        <source>Select output device</source>
        <translation type="vanished">选择输出设备</translation>
    </message>
    <message>
        <source>Output device</source>
        <translation type="vanished">选择输出设备</translation>
    </message>
    <message>
        <source>Master volume</source>
        <translation type="vanished">主音量大小</translation>
    </message>
    <message>
        <source>Channel balance</source>
        <translation type="vanished">声道平衡</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="vanished">左</translation>
    </message>
    <message>
        <source>right</source>
        <translation type="vanished">右</translation>
    </message>
    <message>
        <source>Connector</source>
        <translation type="vanished">连接器</translation>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <source>System sound</source>
        <translation type="vanished">系统音效</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation type="vanished">音效主题</translation>
    </message>
    <message>
        <source>Prompt voice</source>
        <translation type="vanished">通知音</translation>
    </message>
    <message>
        <source>Boot music</source>
        <translation type="vanished">开关机音乐</translation>
    </message>
    <message>
        <source>System sound theme</source>
        <translation type="vanished">系统音效</translation>
    </message>
    <message>
        <source>prompt voice</source>
        <translation type="vanished">通知音</translation>
    </message>
    <message>
        <source>Shutdown</source>
        <translation type="obsolete">关机</translation>
    </message>
    <message>
        <source>Lagout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>System Sound</source>
        <translation type="vanished">系统音效</translation>
    </message>
    <message>
        <source>Sound Theme</source>
        <translation type="vanished">音效主题</translation>
    </message>
    <message>
        <source>Alert Sound</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Alert Volume</source>
        <translation type="vanished">提示音大小</translation>
    </message>
    <message>
        <source>Logout Music</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>Boot Music</source>
        <translation type="vanished">开关机音乐</translation>
    </message>
    <message>
        <source>Beep Switch</source>
        <translation type="vanished">提示音</translation>
    </message>
    <message>
        <source>Poweroff Music</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>Startup Music</source>
        <translation type="vanished">开机</translation>
    </message>
    <message>
        <source>Wakeup Music</source>
        <translation type="vanished">唤醒</translation>
    </message>
    <message>
        <source>Sleep Music</source>
        <translation type="vanished">睡眠</translation>
    </message>
    <message>
        <source>Window Closed</source>
        <translation type="vanished">窗口关闭</translation>
    </message>
    <message>
        <source>Volume Change</source>
        <translation type="vanished">音量调节</translation>
    </message>
    <message>
        <source>Setting Menu</source>
        <translation type="vanished">设置菜单</translation>
    </message>
</context>
<context>
    <name>UnifiedOutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="91"/>
        <source>resolution</source>
        <translation>ئېنىقلىما</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="128"/>
        <source>orientation</source>
        <translation>يۆلىنىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="133"/>
        <source>arrow-up</source>
        <translation>ئوقيا ئېتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="134"/>
        <source>90° arrow-right</source>
        <translation>90° ئوقيا ئوڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="136"/>
        <source>arrow-down</source>
        <translation>ئوقيا ئېتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="135"/>
        <source>90° arrow-left</source>
        <translation>90° ئوقيا سول</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="168"/>
        <source>frequency</source>
        <translation>چاستوتىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="228"/>
        <source>screen zoom</source>
        <translation>ئېكراننى چوڭايت</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <source>refresh rate</source>
        <translation type="vanished">刷新率</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="393"/>
        <source>auto</source>
        <translation>ئاپتو</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <source>Update</source>
        <translation type="vanished">更新</translation>
    </message>
    <message>
        <source>System Update</source>
        <translation type="vanished">系统更新</translation>
        <extra-contents_path>/Update/System Update</extra-contents_path>
    </message>
    <message>
        <source>Last check time:</source>
        <translation type="vanished">上次检查时间：</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="vanished">检查更新</translation>
    </message>
</context>
<context>
    <name>UpdateDbus</name>
    <message>
        <source>ukui-control-center</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>System-Upgrade</source>
        <translation type="vanished">系统更新</translation>
    </message>
    <message>
        <source>ukui-control-center-update</source>
        <translation type="vanished">控制面板-更新提示</translation>
    </message>
</context>
<context>
    <name>UpdateLog</name>
    <message>
        <source>Update log</source>
        <translation type="vanished">更新日志</translation>
    </message>
</context>
<context>
    <name>UpdateSource</name>
    <message>
        <source>Connection failed, please reconnect!</source>
        <translation type="vanished">连接失败，请重新连接！</translation>
    </message>
</context>
<context>
    <name>Upgrade</name>
    <message>
        <source>Upgrade</source>
        <translation type="vanished">更新</translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <source>userinfo</source>
        <translation type="vanished">帐户信息</translation>
    </message>
    <message>
        <source>Userinfo</source>
        <translation type="vanished">帐户信息</translation>
    </message>
    <message>
        <source>User Info</source>
        <translation type="vanished">帐户信息</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">标准用户</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">管理员用户</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1202"/>
        <source>root</source>
        <translation>يىلتىز</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1170"/>
        <source>Hint</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <source>Modify the account type need to logout to take effect, whether to logout?</source>
        <translation type="vanished">更改账户类型需要注销后生效，是否注销？</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1075"/>
        <source>The account type of “%1” has been modified, will take effect after logout, whether to logout?</source>
        <translation>«٪1» نىڭ ھېسابات تىپى ئۆزگەرتىلدى، چېكىنگەندىن كېيىن كۈچكە ئىگە بولامدۇ- يوق؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1076"/>
        <source>logout later</source>
        <translation>كېيىن چېكىنىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1077"/>
        <source>logout now</source>
        <translation>ھازىر چېكىنىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1171"/>
        <source>The system only allows one user to log in automatically.After it is turned on, the automatic login of other users will be turned off.Is it turned on?</source>
        <translation>سىستېما پەقەت بىرلا ئابونتنىڭ ئاپتوماتىك كىرەلىشىگە يول قويىدۇ. ئېچىۋېتىلگەندىن كېيىن باشقا ئابونتلارنىڭ ئاپتوماتىك كىرىشى ئۆچۈرۈلىدۇ. ئېچىلىپ كەتتىمۇ؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1174"/>
        <source>Trun on</source>
        <translation>Trun on</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1175"/>
        <source>Close on</source>
        <translation>يېپىش</translation>
    </message>
    <message>
        <source>Add biometric feature</source>
        <translation type="vanished">添加生物密码</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <source>Verify</source>
        <translation type="vanished">验证</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1198"/>
        <source>Standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1200"/>
        <source>Admin</source>
        <translation>admin</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="160"/>
        <source>CurrentUser</source>
        <translation>CurrentUser</translation>
        <extra-contents_path>/Userinfo/CurrentUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="162"/>
        <source>OthersUser</source>
        <translation>OthersUser</translation>
        <extra-contents_path>/Userinfo/OthersUser</extra-contents_path>
    </message>
    <message>
        <source>Passwd</source>
        <translation type="vanished">Passwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="194"/>
        <source>Groups</source>
        <translation>گۇرۇپپىلار</translation>
        <extra-contents_path>/Userinfo/Groups</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="271"/>
        <source>AutoLoginOnBoot</source>
        <translation>AutoLoginOnBoot</translation>
        <extra-contents_path>/Userinfo/AutoLoginOnBoot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="249"/>
        <source>LoginWithoutPwd</source>
        <translation>كىرىشWithoutPwd</translation>
        <extra-contents_path>/Userinfo/LoginWithoutPwd</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="306"/>
        <source>Add</source>
        <translation>قوش</translation>
        <extra-contents_path>/Userinfo/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>The user is logged in, please delete the user after logging out</source>
        <translation>ئابۇنت تىزىملىتىپ كىردى، تىزىملىتىپ كىرگەندىن كېيىن ئابونتنى ئۆچۈرۈڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="41"/>
        <source>Current User</source>
        <translation>نۆۋەتتىكى ئىشلەتكۈچى</translation>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="vanished">更改密码</translation>
    </message>
    <message>
        <source>Change type</source>
        <translation type="vanished">更改类型</translation>
    </message>
    <message>
        <source>Change valid</source>
        <translation type="vanished">密码时效</translation>
        <extra-contents_path>/userinfo/Change valid</extra-contents_path>
    </message>
    <message>
        <source>User group</source>
        <translation type="vanished">用户组</translation>
    </message>
    <message>
        <source>Change vaild</source>
        <translation type="vanished">密码时效</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="309"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="182"/>
        <source>Password</source>
        <translation>ئىم</translation>
        <extra-contents_path>/Userinfo/Password</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="331"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="188"/>
        <source>Type</source>
        <translation>تۈرى</translation>
        <extra-contents_path>/Userinfo/Type</extra-contents_path>
    </message>
    <message>
        <source>Valid</source>
        <translation type="vanished">密码时效</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="353"/>
        <source>Group</source>
        <translation>گۇرۇپپا</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="422"/>
        <source>Login no passwd</source>
        <translation>كىرىش يوق passwd</translation>
    </message>
    <message>
        <source>enable autoLogin</source>
        <translation type="vanished">autoLogin نى قوزغىتىش</translation>
        <extra-contents_path>/Userinfo/enable autoLogin</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="500"/>
        <source>Automatic login at boot</source>
        <translation>Boot دا ئاپتوماتىك كىرىش</translation>
    </message>
    <message>
        <source>Currently in Live mode, please create a new user and log out</source>
        <translation type="vanished">当前处于试用模式，请创建一个新用户并注销生效</translation>
    </message>
    <message>
        <source>Biometric Password</source>
        <translation type="vanished">生物密码</translation>
    </message>
    <message>
        <source>advanced settings </source>
        <translation type="vanished">高级设置</translation>
    </message>
    <message>
        <source>enable biometrics </source>
        <translation type="vanished">打开生物特征</translation>
    </message>
    <message>
        <source>types of biometric password </source>
        <translation type="vanished">生物密码类型</translation>
    </message>
    <message>
        <source>biometric device </source>
        <translation type="vanished">生物设备</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="568"/>
        <source>Other Users</source>
        <translation>باشقا ئىشلەتكۈچىلەر</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="vanished">添加新用户</translation>
    </message>
</context>
<context>
    <name>UserInfoIntel</name>
    <message>
        <source>Current User</source>
        <translation type="vanished">نۆۋەتتىكى ئىشلەتكۈچى</translation>
        <extra-contents_path>/UserinfoIntel/Current User</extra-contents_path>
    </message>
    <message>
        <source>Change phone</source>
        <translation type="vanished">تېلېفون ئالماشتۇرۇش</translation>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="vanished">pwd نى ئۆزگەرتىش</translation>
        <extra-contents_path>/UserinfoIntel/Change pwd</extra-contents_path>
    </message>
    <message>
        <source>User group</source>
        <translation type="vanished">ئابونتلار توپى</translation>
    </message>
    <message>
        <source>Del user</source>
        <translation type="vanished">Del ئىشلەتكۈچى</translation>
    </message>
    <message>
        <source>system reboot</source>
        <translation type="vanished">سىستېما قايتا قوزغىتى</translation>
    </message>
    <message>
        <source>Unclosed apps start after a restart</source>
        <translation type="vanished">قىستۇرمىسىز ئەپ قايتا قوزغاتقاندىن كېيىن باشلىنىدۇ</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">باشقا ئىشلەتكۈچىلەر</translation>
        <extra-contents_path>/UserinfoIntel/Other Users</extra-contents_path>
    </message>
    <message>
        <source>User Info Intel</source>
        <translation type="vanished">User Info Intel</translation>
    </message>
    <message>
        <source>Change Tel</source>
        <translation type="vanished">Tel نى ئۆزگەرتىش</translation>
        <extra-contents_path>/UserinfoIntel/Change Tel</extra-contents_path>
    </message>
    <message>
        <source>Delete user</source>
        <translation type="vanished">ئىشلەتكۈچىنى ئۆچۈرۈش</translation>
        <extra-contents_path>/UserinfoIntel/Delete user</extra-contents_path>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ئۆلچەملىك ئىشلەتكۈچى</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">باشقۇرغۇچى</translation>
    </message>
    <message>
        <source>root</source>
        <translation type="vanished">يىلتىز</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="vanished">يېڭى ئىشلەتكۈچى قوشۇش</translation>
    </message>
    <message>
        <source>set pwd</source>
        <translation type="vanished">pwd نى بەلگىلەش</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="vanished">ئۆزگەرتىش</translation>
    </message>
</context>
<context>
    <name>UtilsForUserinfo</name>
    <message>
        <source>Passwd</source>
        <translation type="vanished">Passwd</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="36"/>
        <source>Type</source>
        <translation>تۈرى</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="32"/>
        <source>Password</source>
        <translation>ئىم</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="40"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="218"/>
        <source>Standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="220"/>
        <source>Admin</source>
        <translation>admin</translation>
    </message>
</context>
<context>
    <name>Vino</name>
    <message>
        <location filename="../../../plugins/system/vino/vino.cpp" line="28"/>
        <source>Vino</source>
        <translation>Vino</translation>
    </message>
</context>
<context>
    <name>Vpn</name>
    <message>
        <source>Add Vpn Connect</source>
        <translation type="vanished">添加VPN连接</translation>
    </message>
    <message>
        <source>Add vpn connect</source>
        <translation type="vanished">添加VPN连接</translation>
        <extra-contents_path>/Vpn/Add vpn connect</extra-contents_path>
    </message>
    <message>
        <source>vpn</source>
        <translation type="vanished">VPN</translation>
    </message>
    <message>
        <source>Vpn</source>
        <translation type="vanished">VPN</translation>
        <extra-contents_path>/Vpn/Vpn</extra-contents_path>
    </message>
    <message>
        <source>VPN</source>
        <translation type="vanished">VPN</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="103"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="151"/>
        <source>Desktop Background</source>
        <translation>ئۈستەلئۈستى ئارقا كۆرۈنۈشى</translation>
        <extra-contents_path>/Wallpaper/Desktop Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="401"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="156"/>
        <source>Mode</source>
        <translation>مودى</translation>
        <extra-contents_path>/Wallpaper/Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="532"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="158"/>
        <source>Local Pictures</source>
        <translation>يەرلىك رەسىملەر</translation>
        <extra-contents_path>/Wallpaper/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="539"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="160"/>
        <source>Online Pictures</source>
        <translation>توردىكى رەسىملەر</translation>
        <extra-contents_path>/Wallpaper/Online Pictures</extra-contents_path>
    </message>
    <message>
        <source>Online Picture</source>
        <translation type="vanished">توردا رەسىم</translation>
        <extra-contents_path>/Wallpaper/Online Picture</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="571"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="170"/>
        <source>Reset To Default</source>
        <translation>سۈكۈتكە قايتاي</translation>
        <extra-contents_path>/Wallpaper/Reset To Default</extra-contents_path>
    </message>
    <message>
        <source>Select from</source>
        <translation type="vanished">选择背景形式</translation>
    </message>
    <message>
        <source>Picture options</source>
        <translation type="vanished">图片放置方式</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="vanished">كۆرۈش</translation>
        <extra-contents_path>/Wallpaper/Browse</extra-contents_path>
    </message>
    <message>
        <source>Reset to default</source>
        <translation type="vanished">恢复默认设置</translation>
    </message>
    <message>
        <source>Browser online wp</source>
        <translation type="vanished">浏览线上壁纸</translation>
    </message>
    <message>
        <source>Restore default wp</source>
        <translation type="vanished">恢复默认壁纸</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="obsolete">添加</translation>
    </message>
    <message>
        <source>background</source>
        <translation type="vanished">背景</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="331"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="58"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="154"/>
        <source>Background</source>
        <translation>تەگلىك</translation>
        <extra-contents_path>/Wallpaper/Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>picture</source>
        <translation>رەسىم</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>color</source>
        <translation>رەڭ</translation>
    </message>
    <message>
        <source>Add custom shortcut</source>
        <translation type="obsolete">添加自定义快捷键</translation>
    </message>
    <message>
        <source>Custom color</source>
        <translation type="vanished">自定义颜色</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>wallpaper</source>
        <translation>تام قەغىزى</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>centered</source>
        <translation>مەركەز قىلىنغان</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>scaled</source>
        <translation>كۆلەملەشكەن</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>stretched</source>
        <translation>سوزۇلغان</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>zoom</source>
        <translation>چوڭايت</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>spanned</source>
        <translation>ئۇپرىغان</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Blue cyan</source>
        <translation>كۆك سىيان</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Pine green</source>
        <translation>قارىغاي ياپيېشىل</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Emerald green</source>
        <translation>يېشىل زۇمرەت</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Green</source>
        <translation>يېشىل رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Dark cyan</source>
        <translation>قېنىق سىيان</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Slate green</source>
        <translation>يېشىل تاختا</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Mineral green</source>
        <translation>مېنىرال يېشىل رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Taupe</source>
        <translation>Taupe</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Dark brown</source>
        <translation>قېنىق قوڭۇر رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Black</source>
        <translation>قارا</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Aurantiacus</source>
        <translation>Aurantiacus</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Red</source>
        <translation>قىزىل رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Brick-red</source>
        <translation>خىش-قىزىل رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Rose red</source>
        <translation>قىزىلگۈل</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Purplish red</source>
        <translation>سۆسۈن قىزىل رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Dark magenta</source>
        <translation>قېنىق ماگىنىت</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Purple</source>
        <translation>سۆسۈن رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Violet</source>
        <translation>Violet</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Medium purple</source>
        <translation>ئوتتۇرا دەرىجىلىك بىنەپشە رەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Grey</source>
        <translation>كۈلرەڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="620"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>تام قەغىزى ھۆججەتلىرى(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</translation>
    </message>
    <message>
        <source>allFiles(*.*)</source>
        <translation type="vanished">所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="660"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="703"/>
        <source>select custom wallpaper file</source>
        <translation>خاس تام قەغىزى ھۆججىتىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="661"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="704"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="662"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="705"/>
        <source>Position: </source>
        <translation>ئورنى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="663"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="706"/>
        <source>FileName: </source>
        <translation>ھۆججەت نامى: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="664"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="707"/>
        <source>FileType: </source>
        <translation>FileType: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="665"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="708"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>screen zoom </source>
        <translation type="vanished">屏幕缩放 </translation>
    </message>
    <message>
        <source>unify output</source>
        <translation type="vanished">统一输出</translation>
    </message>
    <message>
        <source>night mode</source>
        <translation type="vanished">تۈن ھالىتى</translation>
        <extra-contents_path>/Display/night mode</extra-contents_path>
    </message>
    <message>
        <source>Some applications need to be logouted to take effect</source>
        <translation type="vanished">بىر قىسىم قوللىنىشچان پروگراممىلارنىڭ ئۈنۈمگە ئىھتىياجى بار</translation>
    </message>
    <message>
        <source>Night Mode</source>
        <translation type="vanished">تۈن ھالىتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2348"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2375"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="414"/>
        <source>Time</source>
        <translation>ۋاقتىدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="431"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Custom Time</source>
        <translation>تاموژنا ۋاقتى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="442"/>
        <source>to</source>
        <translation>تو</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="397"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="469"/>
        <source>Color Temperature</source>
        <translation>رەڭ تېمپېراتۇرىسى</translation>
        <extra-contents_path>/Display/Color Temperature</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="340"/>
        <source>Color Temperature And Eye Care</source>
        <translation>رەڭ تېمپېراتۇرىسى ۋە كۆز ئاسراش</translation>
        <extra-contents_path>/Display/Color Temperature And Eye Care</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="371"/>
        <source>Eye Protection Mode</source>
        <translation>كۆز ئاسراش ئەندىزىسى</translation>
        <extra-contents_path>/Display/Eye Protection Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="372"/>
        <source>When turned on, it can reduce blue light to prevent eye, the screen will turn yellow.</source>
        <translation>يانغاندا كۆك نۇرنى ئازايتىپ كۆزنىڭ ئالدىنى ئالغىلى بولىدۇ، ئېكران سارغىيىپ كېتىدۇ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="473"/>
        <source>Warmer</source>
        <translation>تېخىمۇ ئىللىق</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="476"/>
        <source>Colder</source>
        <translation>سوغوق</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="793"/>
        <source>Multi-screen</source>
        <translation>كۆپ ئېكرانلىق</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="798"/>
        <source>First Screen</source>
        <translation>بىرىنچى ئېكران</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="801"/>
        <source>Clone Screen</source>
        <translation>كىلون ئېكرانى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="845"/>
        <source>Primary Screen</source>
        <translation>باشلانغۇچ ئېكران</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1018"/>
        <source>Auto Brightness</source>
        <translation>ئاپتۇماتىك يورۇقلۇق</translation>
        <extra-contents_path>/Display/Auto Brightness</extra-contents_path>
    </message>
    <message>
        <source>Adjust screen brightness by ambient</source>
        <translation type="vanished">Ambient ئارقىلىق ئېكراننىڭ يورۇقلۇقىنى تەڭشەش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1087"/>
        <source>scale</source>
        <translation>كۆلەم</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1091"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after 14 seconds&lt;/font&gt;</source>
        <translation>٪1 نىڭ ئېكرانى ئۆزگەرتىلدى، ساقلاش كېرەكمۇ يوق ؟ &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;تەڭشەكلەر 14 سېكۇنتتىن كېيىن ئەسلىگە كېلىدۇ&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1102"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after %2 seconds&lt;/font&gt;</source>
        <translation>٪1 نىڭ ئېكرانى ئۆزگەرتىلدى، ساقلاش كېرەكمۇ يوق ؟ &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;تەڭشەكلەر ٪2 سېكۇنتتىن كېيىن ئەسلىگە كېلىدۇ&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1299"/>
        <source>The zoom has been modified, it will take effect after you log off</source>
        <translation>چوڭايتىپ ئۆزگەرتىش كىرگۈزۈلدى، سىز چېكىنگەندىن كېيىن كۈچكە ئىگە بولىدۇ</translation>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 14 seconds&lt;/font&gt;</source>
        <translation type="vanished">٪1 نىڭ ئېكرانى ئۆزگەرتىلدى، ساقلاش كېرەكمۇ يوق ؟ &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;تەڭشەكلەر 14 سېكۇنتتىن كېيىن ساقلىنىدۇ&lt;/font&gt;</translation>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after %2 seconds&lt;/font&gt;</source>
        <translation type="vanished">٪1 نىڭ ئېكرانى ئۆزگەرتىلدى، ساقلاش كېرەكمۇ يوق ؟ &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;تەڭشەكلەر ٪2 سېكۇنتتىن كېيىن ساقلىنىدۇ&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Follow the sunrise and sunset</source>
        <translation>كۈن چىقىش ۋە كۈن پېتىشقا ئەگىشىپ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="800"/>
        <source>Extend Screen</source>
        <translation>ئېكراننى ئۇزارتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="799"/>
        <source>Vice Screen</source>
        <translation>مۇئاۋىن ئېكران</translation>
    </message>
    <message>
        <source>monitor</source>
        <translation type="vanished">كۆزەتكۈچى</translation>
        <extra-contents_path>/display/monitor</extra-contents_path>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">ئۇچۇر-خەۋەر</translation>
    </message>
    <message>
        <source>Theme follow night mode</source>
        <translation type="vanished">ئۇسلۇب كېچىلىك ھالىتىگە ئەگىشىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1078"/>
        <source>resolution</source>
        <translation>ئېنىقلىما</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1081"/>
        <source>orientation</source>
        <translation>يۆلىنىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1084"/>
        <source>frequency</source>
        <translation>چاستوتىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1297"/>
        <source>Hint</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
the settings will be saved after 14 seconds</source>
        <translation type="vanished">是否保留当前修改的配置？将在14秒后自动保存配置</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1069"/>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1070"/>
        <source>Not Save</source>
        <translation>ساقلاش ئەمەس</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
the settings will be saved after %1 seconds</source>
        <translation type="vanished">是否保留当前修改的配置？将在%1秒后自动保存配置</translation>
    </message>
    <message>
        <source>The zoom function needs to log out to take effect</source>
        <translation type="vanished">چوڭايتما ئىقتىدارى چېكىنىپ ئۈنۈمگە ئىھتىياجلىق</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="805"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="807"/>
        <source>Network Display</source>
        <translation>تور كۆرگەزمىسى</translation>
        <extra-contents_path>/Display/Network Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="840"/>
        <source>Display</source>
        <translation>كۆرسىتىش</translation>
        <extra-contents_path>/Display/Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1300"/>
        <source>Log out now</source>
        <translation>ھازىر چېكىن</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1301"/>
        <source>Later</source>
        <translation>كېيىنچە</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1318"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1328"/>
        <source>(Effective after logout)</source>
        <translation>(چېكىنگەندىن كېيىن ئۈنۈملۈك)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2264"/>
        <source>are you sure to apply?
1 Select &quot;apply&quot;, manually log out late
2 Select &quot;log out to apply&quot;, log out now to apply
3 Select &quot;cancel&quot;, cancel to apply</source>
        <translation>سىز چوقۇم ئىلتىماس قىلامسىز؟
1 «ئىلتىماس قىلىش»نى تاللاڭ، قولدا تىزىملىتىپ كەچ كىرىڭ
2«تىزىملىتىپ ئىلتىماس قىلىش» نى تاللاڭ، ھازىر تىزىملىتىپ ئىلتىماس قىلىڭ
3«ئەمەلدىن قالدۇرۇش»نى تاللاڭ، ئەمەلدىن قالدۇرۇپ ئىلتىماس قىلىڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>apply</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>log out to apply</source>
        <translation>تىزىملىتىپ ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2347"/>
        <source>Turning on &apos;Eye Protection Mode&apos; will turn off &apos;Color Temperature&apos;. Continue turning it on?</source>
        <translation>&apos;كۆز ئاسراش شەكلى&apos;نى ئېچىش «رەڭلىك تېمپېراتۇرا» نى ئۆچۈرۈۋېتىدۇ. داۋاملىق ئېچىپ قويامسىز؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2349"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2376"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2374"/>
        <source>Turning on &apos;Color Temperature&apos; will turn off &apos;Eye Protection Mode&apos;. Continue turning it on?</source>
        <translation>&apos;رەڭلىك تېمپېراتۇرا&apos; نى ئېچىش «كۆز ئاسراش شەكلى»نى ئۆچۈرۈۋېتىدۇ. داۋاملىق ئېچىپ قويامسىز؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>All Day</source>
        <translation>كۈن بويى</translation>
    </message>
    <message>
        <source>Follow the sunrise and sunset(17:55-06:23)</source>
        <translation type="vanished">跟随日出日落(17:55-06:23)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2713"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2727"/>
        <source>Brightness</source>
        <translation>يورۇقلۇق</translation>
        <extra-contents_path>/Display/Brightness</extra-contents_path>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
the settings will be saved after 9 seconds</source>
        <translation type="vanished">修改分辨率或刷新率后，由于显示设备与 显卡兼容性问题，有可能显示不正常。系统将在9秒后保存配置</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
The settings will be saved after 9 seconds</source>
        <translation type="vanished">修改分辨率或刷新率后，由于显示设备与 显卡兼容性问题，有可能显示不正常。系统将在9秒后保存配置</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
If something goes wrong, the settings will be restored after 10 seconds</source>
        <translation type="vanished">修改分辨率或刷新率后，由于显示设备与显卡存在兼容性问题，有可能显示不正常或者无法显示．如果出现异常，系统将在10秒后还原设置</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
If something goes wrong, the settings will be restored after 9 seconds</source>
        <translation type="vanished">修改分辨率或刷新率后，由于显示设备与显卡存在兼容性问题，有可能显示不正常或者无法显示．如果出现异常，系统将在9秒后还原设置</translation>
    </message>
    <message>
        <source>screen zoom</source>
        <translation type="vanished">ئېكراننى چوڭايت</translation>
        <extra-contents_path>/display/screen zoom</extra-contents_path>
    </message>
    <message>
        <source>Mirror Display</source>
        <translation type="vanished">ئەينەك كۆرسىتىش</translation>
        <extra-contents_path>/display/unify output</extra-contents_path>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 29 seconds&lt;/font&gt;</source>
        <translation type="vanished">٪1 نىڭ ئېكرانى ئۆزگەرتىلدى، ساقلاش كېرەكمۇ يوق ؟ &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;تەڭشەكلەر 29 سېكۇنتتىن كېيىن ساقلىنىدۇ&lt;/font&gt;</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
the settings will be saved after 29 seconds</source>
        <translation type="vanished">修改分辨率或刷新率后，由于显示设备与 显卡兼容性问题，有可能显示不正常。系统将在29秒后保存配置</translation>
    </message>
    <message>
        <source>Save Config</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Restore Config</source>
        <translation type="vanished">恢复</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display 
the settings will be saved after %1 seconds</source>
        <translation type="vanished">修改分辨率或刷新率后，由于显示设备与 显卡兼容性问题，有可能显示不正常。系统将在%1秒后保存配置 </translation>
    </message>
    <message>
        <source>Warnning</source>
        <translation type="vanished">ئاگاھلاندۇرۇش</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
If something goes wrong, the settings will be restored after %1 seconds</source>
        <translation type="vanished">修改分辨率或刷新率后，由于显示设备与显卡存在兼容性问题，有可能显示不正常或者无法显示．如果出现异常，系统将在%1秒后还原设置</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>please insure at least one output!</source>
        <translation>ئەڭ ئاز بولغاندىمۇ بىر مالنى سۇغۇرتىغا ئالدۇرۇڭ!</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1692"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1792"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <source>The screen resolution has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 14 seconds&lt;/font&gt;</source>
        <translation type="vanished">屏幕分辨率已修改，是否保存？&lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;系统将在14秒后自动保存。&lt;/font&gt;</translation>
    </message>
    <message>
        <source>The screen resolution has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after %1 seconds&lt;/font&gt;</source>
        <translation type="vanished">屏幕分辨率已修改，是否保存？&lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;系统将在%1秒后自动保存。&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1693"/>
        <source>Open time should be earlier than close time!</source>
        <translation>ئېچىۋېتىش ۋاقتى يېقىن ۋاقىتتىن بالدۇر بولۇشى كېرەك!</translation>
    </message>
    <message>
        <source>Morning time should be earlier than evening time!</source>
        <translation type="vanished">早晨时刻应早于晚上的时刻!</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1793"/>
        <source>Sorry, your configuration could not be applied.
Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU.</source>
        <translation>كەچۈرۈڭ، سەپلىمىسىڭىز قوللىنىلمىدى.
دائىم كۆرۈلىدىغان سەۋەبلەر: ئومۇمىي ئېكران چوڭلۇقى بەك چوڭ، ياكى GPU قوللىغاندىنمۇ كۆپ كۆرسىتىش ئىقتىدارى قوشۇلغان.</translation>
    </message>
    <message>
        <source>@title:window</source>
        <comment>Unsupported Configuration</comment>
        <translation type="vanished">窗口</translation>
    </message>
    <message>
        <source>Some applications need to be restarted to take effect</source>
        <translation type="vanished">缩放配置需要注销后生效</translation>
    </message>
    <message>
        <source>touch id</source>
        <translation type="obsolete">触摸屏标识</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
</context>
<context>
    <name>WlanConnect</name>
    <message>
        <source>WlanConnect</source>
        <translation type="vanished">无线局域网</translation>
    </message>
    <message>
        <source>WLAN</source>
        <translation type="vanished">无线局域网</translation>
    </message>
    <message>
        <source>open</source>
        <translation type="vanished">开启</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation type="vanished">高级设置</translation>
    </message>
    <message>
        <source>Other Networks</source>
        <translation type="vanished">其他网络</translation>
    </message>
    <message>
        <source>Network settings</source>
        <translation type="vanished">网络设置</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="vanished">已连接</translation>
    </message>
    <message>
        <source>Detail</source>
        <translation type="vanished">网络详情</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">网络连接</translation>
    </message>
    <message>
        <source>card</source>
        <translation type="vanished">网卡</translation>
    </message>
    <message>
        <source>connected</source>
        <translation type="vanished">已连接</translation>
    </message>
</context>
<context>
    <name>addShortcutDialog</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <source>Shortcut name</source>
        <translation type="vanished">快捷键名称</translation>
    </message>
    <message>
        <source>Shortcut exec</source>
        <translation type="vanished">快捷键程序</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="88"/>
        <source>Exec</source>
        <translation>Exec</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="126"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="174"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="248"/>
        <source>Key</source>
        <translation>ئاچقۇچ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="222"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="305"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <source>Invalid executable, please re-enter</source>
        <translation type="vanished">无效的可执行程序，请重新选择</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="354"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="275"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="373"/>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Add custom shortcut</source>
        <translation type="vanished">添加自定义快捷键</translation>
    </message>
    <message>
        <source>shortcut conflict</source>
        <translation type="vanished">快捷键冲突</translation>
    </message>
    <message>
        <source>invaild shortcut</source>
        <translation type="vanished">无效快捷键</translation>
    </message>
    <message>
        <source>repeated naming</source>
        <translation type="vanished">快捷键名称重复</translation>
    </message>
    <message>
        <source>Add shortcut</source>
        <translation type="vanished">添加快捷键</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="86"/>
        <source>Add Shortcut</source>
        <translation>قىسقارتىش قوشۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="103"/>
        <source>Please enter a shortcut</source>
        <translation>بىر قىسقا يولنى كىرگۈزۈڭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="233"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ئۈستەل يۈزى ھۆججەتلىرى(*.desktop)</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="274"/>
        <source>select desktop</source>
        <translation>ئۈستەلئۈستى تاللاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="329"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="348"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="357"/>
        <source>Invalid application</source>
        <translation>ئىناۋەتسىز ئىلتىماس</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="331"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="344"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="353"/>
        <source>Shortcut conflict</source>
        <translation>قىسقا يول توقۇنۇشى</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="333"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="346"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="355"/>
        <source>Invalid shortcut</source>
        <translation>ئىناۋەتسىز قىسقا يول</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="336"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="341"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="360"/>
        <source>Name repetition</source>
        <translation>ئىسىم قايتا تەكرارلاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="364"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="542"/>
        <source>Shortcut cannot be empty</source>
        <translation>قىسقا يولنى بوش قويۇشقا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="546"/>
        <source>Name cannot be empty</source>
        <translation>ئىسىمنى بوش قويۇشقا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="556"/>
        <source>Desktop prohibits adding</source>
        <translation>ئۈستەلئۈستى قوشۇشنى چەكلىدى</translation>
    </message>
</context>
<context>
    <name>area_code_lineedit</name>
    <message>
        <source>Sign up by Phone</source>
        <translation type="vanished">请输入手机号码</translation>
    </message>
</context>
<context>
    <name>changeUserGroup</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="31"/>
        <source>user group</source>
        <translation>ئابونتلار گۇرۇپپىسى</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="120"/>
        <source>Group:</source>
        <translation>توپ:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="133"/>
        <source>GID:</source>
        <translation>GID:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="147"/>
        <source>GNum:</source>
        <translation>GNum:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="191"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="617"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="194"/>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="618"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Tips</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Invalid Id!</source>
        <translation>ئىناۋەتسىز كىملىك!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="575"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="583"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="659"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <source>Invalid Group Name!</source>
        <translation>ئىناۋەتسىز توپ نامى!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="615"/>
        <source>Whether delete the group: “%1” ?</source>
        <translation>توپنى ئۆچۈرەمدۇ يوق: &quot;٪1&quot; ؟</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="616"/>
        <source>which will make some file components in the file system invalid!</source>
        <translation>بۇ ھۆججەت سىستېمىسىدىكى بەزى ھۆججەت دېتاللىرىنى ئىناۋەتسىز قىلىدۇ!</translation>
    </message>
</context>
<context>
    <name>changtimedialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="32"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="115"/>
        <source>current date</source>
        <translation>نۆۋەتتىكى چېسلا</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="200"/>
        <source>time</source>
        <translation>ۋاقتىدا</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="321"/>
        <source>year</source>
        <translation>يىل</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="398"/>
        <source>month</source>
        <translation>ئاي</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="472"/>
        <source>day</source>
        <translation>كۈن</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="574"/>
        <source>cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="593"/>
        <source>confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>config_list_widget</name>
    <message>
        <source>wallpaper</source>
        <translation type="vanished">平铺</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的云帐户：%1</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">退出登录</translation>
    </message>
    <message>
        <source>Sync</source>
        <translation type="vanished">同步中</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation type="vanished">登录</translation>
    </message>
    <message>
        <source>Stop sync</source>
        <translation type="vanished">结束同步</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Synchronize your personalized settings and data</source>
        <translation type="vanished">同步您帐户的数据以及个性化设置</translation>
    </message>
    <message>
        <source>Login Cloud to get a better experience</source>
        <translation type="vanished">同步您帐户的数据以及个性化设置</translation>
    </message>
    <message>
        <source>Sign in/Sign up</source>
        <translation type="vanished">登录/注册</translation>
    </message>
    <message>
        <source>You must sign in when you attempt to sync your settings.</source>
        <translation type="vanished">如需同步设置，请先登录您的云帐户。</translation>
    </message>
    <message>
        <source>Your account：%1</source>
        <translation type="vanished">您的云帐户：%1</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
</context>
<context>
    <name>item_list</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">开始菜单</translation>
    </message>
    <message>
        <source>Quick Start</source>
        <translation type="vanished">快速启动项</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">任务栏</translation>
    </message>
    <message>
        <source>ScreenSaver</source>
        <translation type="vanished">屏保</translation>
    </message>
    <message>
        <source>User Profile</source>
        <translation type="vanished">用户头像</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">天气</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Walpaper</source>
        <translation type="vanished">桌面壁纸</translation>
    </message>
</context>
<context>
    <name>ksc_main_page_widget</name>
    <message>
        <source>Run Security Center</source>
        <translation type="obsolete">打开安全中心</translation>
    </message>
</context>
<context>
    <name>ksc_module_func_widget</name>
    <message>
        <source>Network Protection</source>
        <translation type="obsolete">网络保护</translation>
    </message>
</context>
<context>
    <name>m_updatelog</name>
    <message>
        <source>No content.</source>
        <translation type="vanished">暂无内容.</translation>
    </message>
    <message>
        <source>Search content</source>
        <translation type="vanished">搜索内容</translation>
    </message>
    <message>
        <source>History Log</source>
        <translation type="vanished">历史更新</translation>
    </message>
    <message>
        <source>Update Details</source>
        <translation type="vanished">更新详情</translation>
    </message>
</context>
<context>
    <name>mcode_widget</name>
    <message>
        <source>SongTi</source>
        <translation type="vanished">宋体</translation>
    </message>
</context>
<context>
    <name>networkaccount</name>
    <message>
        <source>Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
</context>
<context>
    <name>ql_pushbutton_edit</name>
    <message>
        <source>Reset</source>
        <translation type="vanished">重置密码</translation>
    </message>
</context>
</TS>
