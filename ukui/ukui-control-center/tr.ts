<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>About</name>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="614"/>
        <source>System Summary</source>
        <translation>Sistem Özeti</translation>
        <extra-contents_path>/About/System Summary</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="615"/>
        <source>Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="227"/>
        <source>Version Number</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/Version Number</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="684"/>
        <source>Status</source>
        <translation type="unfinished">Durum</translation>
        <extra-contents_path>/About/Status</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="689"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1412"/>
        <source>DateRes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="618"/>
        <source>Wechat code scanning obtains HP professional technical support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="619"/>
        <source>See more about Kylin Tianqi edu platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="527"/>
        <source>&lt;&lt;Protocol&gt;&gt;</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/&lt;&lt;Protocol&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="66"/>
        <source>About and Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="247"/>
        <source>Patch Version</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/Patch Version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="266"/>
        <source>Installed Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="285"/>
        <source>Upgrade Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="308"/>
        <source>HostName</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/HostName</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="451"/>
        <source>Privacy and agreement</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/Privacy and agreement</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="463"/>
        <source>Send optional diagnostic data</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/Send optional diagnostic data</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="465"/>
        <source>By sending us diagnostic data, improve the system experience and solve your problems faster</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="528"/>
        <source>and</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="530"/>
        <source>&lt;&lt;Privacy&gt;&gt;</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/&lt;&lt;Privacy&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="638"/>
        <source>Learn more HP user manual&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="646"/>
        <source>See user manual&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="749"/>
        <source>Trial expiration time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="839"/>
        <source>Kylin Linux Desktop V10 (SP1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1154"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1431"/>
        <source>expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="770"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1156"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1414"/>
        <source>Extend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="516"/>
        <source>Copyright © 2020 KylinSoft. All rights reserved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1385"/>
        <source>The system needs to be restarted to set the HostName, whether to reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1386"/>
        <source>Reboot Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1387"/>
        <source>Reboot Later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="999"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1008"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1560"/>
        <source>avaliable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>version</source>
        <translation type="vanished">Sürüm:</translation>
        <extra-contents_path>/about/version</extra-contents_path>
    </message>
    <message>
        <source>Copyright 2009-2020 @ Kylinos All rights reserved</source>
        <translation type="vanished">Telif Hakkı 2009-2020 @ Kylinos Tüm hakları saklıdır</translation>
    </message>
    <message>
        <source>Copyright 2009-2021 @ Kylinos All rights reserved</source>
        <translation type="obsolete">Telif Hakkı 2009-2020 @ Kylinos Tüm hakları saklıdır {2009-2021 ?}</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="672"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="674"/>
        <source>Kernel</source>
        <translation>Çekirdek:</translation>
        <extra-contents_path>/About/Kernel</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="676"/>
        <source>CPU</source>
        <translation>CPU:</translation>
        <extra-contents_path>/About/CPU</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="678"/>
        <source>Memory</source>
        <translation>Hafıza:</translation>
        <extra-contents_path>/About/Memory</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="616"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1006"/>
        <source>Disk</source>
        <translation>Disk:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="680"/>
        <source>Desktop</source>
        <translation>Masaüstü:</translation>
        <extra-contents_path>/About/Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="682"/>
        <source>User</source>
        <translation>Kullanıcı: </translation>
        <extra-contents_path>/About/User</extra-contents_path>
    </message>
    <message>
        <source>Active Status</source>
        <translation type="vanished">Aktif Durum</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="688"/>
        <source>Serial</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/About/Serial</extra-contents_path>
    </message>
    <message>
        <source>Service serial number</source>
        <translation type="vanished">Servis Seri No</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="686"/>
        <location filename="../../../plugins/system/about/about.cpp" line="751"/>
        <location filename="../../../plugins/system/about/about.cpp" line="757"/>
        <source>Active</source>
        <translation>Aktif</translation>
        <extra-contents_path>/About/Active</extra-contents_path>
    </message>
    <message>
        <source>Trial version disclaimer</source>
        <translation type="vanished">Deneme sürümü sorumluluk reddi</translation>
    </message>
    <message>
        <source>Devices Summary</source>
        <translation type="vanished">Cihaz Özeti</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">Hakkında</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="68"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
    <message>
        <source>Disk:</source>
        <translation type="vanished">Disk:</translation>
    </message>
    <message>
        <source> available</source>
        <translation type="vanished">Uygun</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="747"/>
        <location filename="../../../plugins/system/about/about.cpp" line="755"/>
        <source>Inactivated</source>
        <translation>Aktif değil</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="768"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1411"/>
        <source>Activated</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <source>UNKNOWN</source>
        <translation type="vanished">BİLİNMEYEN</translation>
    </message>
    <message>
        <source>Current desktop env:</source>
        <translation type="vanished">Aktif Masaüstü:</translation>
    </message>
    <message>
        <source>OS Version:</source>
        <translation type="vanished">OS Sürümü:</translation>
    </message>
    <message>
        <source>CPU Arch:</source>
        <translation type="vanished">CPU Mimarisi:</translation>
    </message>
    <message>
        <source>Kernel Version</source>
        <translation type="vanished">Kernel Sürümü:</translation>
    </message>
    <message>
        <source>Manufacturers:</source>
        <translation type="vanished">Üreticiler:</translation>
    </message>
    <message>
        <source>Product Name:</source>
        <translation type="vanished">Ürün Adı:</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation type="vanished">Sürüm:</translation>
    </message>
    <message>
        <source>Serial Number:</source>
        <translation type="vanished">Seri Numarası:</translation>
    </message>
</context>
<context>
    <name>Accessibility</name>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="32"/>
        <source>Vision</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="131"/>
        <source>Enable magnifying glass: Enlarge the content of the desktop</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="138"/>
        <source>Press Win + &quot;+&quot; to zoom in on the content, Win + &quot;-&quot; to zoom out on the content.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="225"/>
        <source>Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="315"/>
        <source>Color Filter Effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="415"/>
        <source>Color Filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="506"/>
        <source>Press Win + Ctrl + C to turn on/off color effect</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="529"/>
        <source>Other Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="585"/>
        <source>Point Size</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="604"/>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="659"/>
        <source>Set</source>
        <translation>Ayarla</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="640"/>
        <source>Vocal Tract Regulation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="10"/>
        <source>Accessibility</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="80"/>
        <source>Window Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="81"/>
        <source>Full Screen Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="83"/>
        <source>Red/Green Filter (Protanopia)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="84"/>
        <source>Green/Red Filter (Deuteranopia)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="85"/>
        <source>Blue/Yellow Filter (Tritanopia)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="86"/>
        <source>Grayscale</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="87"/>
        <source>Invert</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AddAppDialog</name>
    <message>
        <source>OK</source>
        <translation type="vanished">Tamam</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
</context>
<context>
    <name>AddAutoBoot</name>
    <message>
        <source>Add AutoBoot</source>
        <translation type="vanished">Otomatik Başlatma Ekle</translation>
    </message>
    <message>
        <source>Add autoboot program</source>
        <translation type="vanished">Oto başlat programı ekle</translation>
    </message>
    <message>
        <source>Program name</source>
        <translation type="vanished">Program adı</translation>
    </message>
    <message>
        <source>Program exec</source>
        <translation type="vanished">Program komutu</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">Aç</translation>
    </message>
    <message>
        <source>Program comment</source>
        <translation type="vanished">Program Açıklaması</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="202"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">Belirli</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="297"/>
        <source>desktop file not exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select Autoboot Desktop</source>
        <translation type="vanished">otomatik önyükleme masaüstü seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="123"/>
        <source>Name</source>
        <translation type="unfinished">İsim</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="124"/>
        <source>Exec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="125"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="193"/>
        <source>Desktop files(*.desktop)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="200"/>
        <source>Select AutoStart Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="201"/>
        <source>Select</source>
        <translation type="unfinished">Seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="248"/>
        <source>desktop file not allowed add</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddAutoStart</name>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="177"/>
        <source>Add AutoStart program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="178"/>
        <source>Name</source>
        <translation type="unfinished">İsim</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="179"/>
        <source>Exec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="180"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="181"/>
        <source>Open</source>
        <translation type="unfinished">Aç</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="182"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="183"/>
        <source>Certain</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddBtn</name>
    <message>
        <location filename="../../../libukcc/widgets/AddBtn/addbtn.cpp" line="23"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
    </message>
</context>
<context>
    <name>AddButton</name>
    <message>
        <location filename="../../../libukcc/widgets/SettingWidget/addbutton.cpp" line="25"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
    </message>
</context>
<context>
    <name>AddInputMethodDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="26"/>
        <source>Select the input method to add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="82"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="101"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>keyboard</source>
        <translation type="unfinished">Klavye</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>Tibetan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="7"/>
        <source>With ASCII numbers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="15"/>
        <source>Input Method</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddLanguageDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="179"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="198"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="22"/>
        <source>Add Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="23"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
</context>
<context>
    <name>AppDetail</name>
    <message>
        <source>Allow notification</source>
        <translation type="vanished">Bildirime İzin Ver</translation>
    </message>
    <message>
        <source>Number of notification centers</source>
        <translation type="vanished">Bildirim merkezi numarası</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">Onayla</translation>
    </message>
</context>
<context>
    <name>AppUpdateWid</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>AptProxyDialog</name>
    <message>
        <source>Port</source>
        <translation type="obsolete">Port</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">Onayla</translation>
    </message>
</context>
<context>
    <name>Area</name>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="26"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="44"/>
        <source>Area</source>
        <translation>Alan</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="344"/>
        <source>First Day Of The Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="258"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="617"/>
        <source>Calendar</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/Calendar</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="59"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="236"/>
        <source>Language Format</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/Regional Format</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="156"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="615"/>
        <source>Regional Format</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="427"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="622"/>
        <source>Short Format Date</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="510"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="627"/>
        <source>Long Format Date</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/Long Format Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="624"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="593"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="629"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="676"/>
        <source>Language Format Example</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="734"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="238"/>
        <source>System Language</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/system language</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="768"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Area showing time currency format</source>
        <translation type="vanished">Zaman para birimi biçimini gösteren alan</translation>
    </message>
    <message>
        <source>Regional format data</source>
        <translation type="vanished">Bölgesel format verileri</translation>
    </message>
    <message>
        <source>lunar</source>
        <translation type="vanished">Ay takvimi</translation>
    </message>
    <message>
        <source>First day of the week</source>
        <translation type="vanished">Haftanın ilk günü</translation>
    </message>
    <message>
        <source>day</source>
        <translation type="vanished">Gün</translation>
    </message>
    <message>
        <source>area</source>
        <translation type="vanished">Alan</translation>
    </message>
    <message>
        <source>current area</source>
        <translation type="vanished">Güncel Alan</translation>
        <extra-contents_path>/area/current area</extra-contents_path>
    </message>
    <message>
        <source>display format area</source>
        <translation type="vanished">Zaman Biçimi Düzeni</translation>
    </message>
    <message>
        <source>US</source>
        <translation type="vanished">ABD</translation>
    </message>
    <message>
        <source>format of area</source>
        <translation type="vanished">Zaman Biçimi</translation>
        <extra-contents_path>/area/format of area</extra-contents_path>
    </message>
    <message>
        <source>addwgt</source>
        <translation type="vanished">Widget Ekle</translation>
    </message>
    <message>
        <source>Add main language</source>
        <translation type="vanished">Ana Dil Ekle</translation>
    </message>
    <message>
        <source>calendar</source>
        <translation type="vanished">Takvim</translation>
    </message>
    <message>
        <source>first day of week</source>
        <translation type="vanished">Haftanın ilk günü</translation>
    </message>
    <message>
        <source>date</source>
        <translation type="vanished">Tarih</translation>
    </message>
    <message>
        <source>time</source>
        <translation type="vanished">Zaman</translation>
    </message>
    <message>
        <source>change format of data</source>
        <translation type="vanished">Zaman Biçimini Düzenle</translation>
    </message>
    <message>
        <source>first language</source>
        <translation type="vanished">Sistem Dili</translation>
        <extra-contents_path>/area/first language</extra-contents_path>
    </message>
    <message>
        <source>system language</source>
        <translation type="vanished">Sistem Dili</translation>
    </message>
    <message>
        <source>CN</source>
        <translation type="vanished">CN</translation>
    </message>
    <message>
        <source>Need to cancel to take effect</source>
        <translation type="vanished">Etkili olması için iptal etmeniz gerekiyor</translation>
    </message>
    <message>
        <source>Need to log off to take effect</source>
        <translation type="vanished">Etkili olması için oturumu kapatmanız gerekiyor</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">İngilizce</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">Çince</translation>
    </message>
    <message>
        <source>add main language</source>
        <translation type="vanished">Ana dil ekle</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="172"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="677"/>
        <source>MMMM dd, yyyy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="175"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="680"/>
        <source>MMMM d, yy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="241"/>
        <source>Language for system windows,menus and web pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="247"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
        <extra-contents_path>/Area/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="362"/>
        <source>English  (US)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="363"/>
        <source>Simplified Chinese  (CN)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="364"/>
        <source>Tibetan  (CN)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="379"/>
        <source>Monday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="380"/>
        <source>Sunday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="619"/>
        <source>First Day Of Week</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Area/First Day Of Week</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="927"/>
        <source>Modify the first language need to restart to take effect, whether to restart?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="928"/>
        <source>Restart later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="929"/>
        <source>Restart now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>solar calendar</source>
        <translation type="vanished">Güneş Takvimi</translation>
    </message>
    <message>
        <source>monday</source>
        <translation type="vanished">Pazartesi</translation>
    </message>
    <message>
        <source>sunday</source>
        <translation type="vanished">Pazar</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="631"/>
        <source>Solar Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="634"/>
        <source>Lunar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="650"/>
        <source>12 Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="651"/>
        <source>24 Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="923"/>
        <source>Modify the current region need to logout to take effect, whether to logout?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="924"/>
        <source>Logout later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="925"/>
        <source>Logout now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>change data format</source>
        <translation type="vanished">Zaman Biçimini Düzenle</translation>
    </message>
</context>
<context>
    <name>AreaCodeLineEdit</name>
    <message>
        <source>Sign up by Phone</source>
        <translation type="vanished">Telefonla kaydolun</translation>
    </message>
</context>
<context>
    <name>Audio</name>
    <message>
        <source>Audio</source>
        <translation type="vanished">Ses</translation>
    </message>
</context>
<context>
    <name>AutoBoot</name>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="466"/>
        <source>Desktop files(*.desktop)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>select autoboot desktop</source>
        <translation type="obsolete">otomatik önyükleme masaüstü seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="88"/>
        <source>Auto Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="474"/>
        <source>Select AutoStart Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="475"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="476"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="623"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
        <extra-contents_path>/AutoStart/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="630"/>
        <source>AutoStart Settings</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/AutoStart/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Autoboot Settings</source>
        <translation type="vanished">Otomatik Açılış Ayarları</translation>
        <extra-contents_path>/autoboot/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Add autoboot app </source>
        <translation type="vanished">Otomatik Yükleme Uygulaması Ekle </translation>
    </message>
    <message>
        <source>autoboot</source>
        <translation type="vanished">Otomatik Başlat</translation>
    </message>
    <message>
        <source>Autoboot</source>
        <translation type="vanished">Oto. başlat</translation>
    </message>
    <message>
        <source>Auto Boot</source>
        <translation type="obsolete">Oto. Başlat</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">İsim</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">Durum</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="243"/>
        <source>Delete</source>
        <translation type="unfinished">Sil</translation>
    </message>
</context>
<context>
    <name>Backup</name>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="53"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="119"/>
        <source>Backup</source>
        <translation>Yedekle</translation>
        <extra-contents_path>/Backup/Backup</extra-contents_path>
    </message>
    <message>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted, you can restore them to ensure 
the integrity of your system.</source>
        <translation type="vanished">Dosyalarınızı diğer sürücülere yedekleyin ve orijinal dosyalar kaybolduğunda, hasar gördüğünde veya silindiğinde, bunları sağlamak için geri yükleyebilirsiniz.
sisteminizin bütünlüğü.</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="69"/>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted, 
you can restore them to ensure the integrity of your system.</source>
        <translation>Dosyalarınızı diğer sürücülere yedekleyin ve orijinal dosyalar kaybolduğunda, hasar gördüğünde veya silindiğinde, 
sisteminizin bütünlüğünü sağlamak için bunları geri yükleyebilirsiniz.</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="113"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="115"/>
        <source>Begin backup</source>
        <translation>Yedekleme Başlat</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="157"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="145"/>
        <source>Restore</source>
        <translation>Geri Yükle</translation>
        <extra-contents_path>/Backup/Restore</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="173"/>
        <source>View a list of backed-upfiles to backed up files to the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View a list of backed-upfiles to restore backed up files to the system</source>
        <translation type="vanished">Yedeklenen dosyaları sisteme geri yüklemek için yedeklenmiş dosyaların listesini görüntüle</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="213"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="141"/>
        <source>Begin restore</source>
        <translation>Yükleme Başlat</translation>
    </message>
    <message>
        <source>backup</source>
        <translation type="vanished">Yedekleme</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="45"/>
        <source>Backup Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="121"/>
        <source>Back up your files to other drives and restore them when the source files are lost, damaged, or deleted to ensure the integrity of the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="147"/>
        <source>View the backup list and restore the backup file to the system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="105"/>
        <source>Backup and Restore</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Backup/Backup and Restore</extra-contents_path>
    </message>
</context>
<context>
    <name>BindPhoneDialog</name>
    <message>
        <source>Your account here</source>
        <translation type="obsolete">Hesabınız burada</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="obsolete">Parolanız burada</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">Kodunuz Burada</translation>
    </message>
    <message>
        <source>Get</source>
        <translation type="obsolete">Al</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">Telefon kodu al</translation>
    </message>
</context>
<context>
    <name>BiometricEnrollDialog</name>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
</context>
<context>
    <name>BiometricsWidget</name>
    <message>
        <source>root</source>
        <translation type="obsolete">Root</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Sil</translation>
    </message>
</context>
<context>
    <name>BlueToothMain</name>
    <message>
        <source>Audio</source>
        <translation type="obsolete">Ses</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="obsolete">Yenile</translation>
    </message>
</context>
<context>
    <name>Boot</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="10"/>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="80"/>
        <source>Boot</source>
        <translation type="unfinished">Boot</translation>
        <extra-contents_path>/Boot/Boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="92"/>
        <source>Grub verify</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Boot/Grub verify</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="95"/>
        <source>Password required for Grub editing after enabling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="97"/>
        <source>Reset password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrightnessFrame</name>
    <message>
        <location filename="../../../plugins/system/display/brightnessFrame.cpp" line="41"/>
        <source>Failed to get the brightness information of this monitor</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CertificationDialog</name>
    <message>
        <source>UserCertification</source>
        <translation type="vanished">Kullanıcı Sertifikası</translation>
    </message>
    <message>
        <source>User:</source>
        <translation type="vanished">Kullanıcı:</translation>
    </message>
    <message>
        <source>Passwd:</source>
        <translation type="vanished">Parola:</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Kapat</translation>
    </message>
    <message>
        <source>Certification</source>
        <translation type="vanished">Sertifika</translation>
    </message>
</context>
<context>
    <name>ChangeFaceDialog</name>
    <message>
        <source>select custom face file</source>
        <translation type="vanished">Özel yüz dosyası seç</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">Seç:</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">Konum: </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">Dosya Adı: </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">Dosya türü: </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">Uyarı</translation>
    </message>
    <message>
        <source>The avatar is larger than 1M, please choose again</source>
        <translation type="obsolete">Avatar 2M&apos;den büyük, lütfen başka seçin {1M?}</translation>
    </message>
    <message>
        <source>The avatar is larger than 2M, please choose again</source>
        <translation type="vanished">Avatar 2M&apos;den büyük, lütfen başka seçin</translation>
    </message>
    <message>
        <source>Change User Face</source>
        <translation type="vanished">Kullanıcı Logosunu Değiştir</translation>
    </message>
    <message>
        <source>Select face from local</source>
        <translation type="vanished">Bilgisayardan Logo Seçin</translation>
    </message>
</context>
<context>
    <name>ChangeFaceIntelDialog</name>
    <message>
        <source>Change User Face</source>
        <translation type="obsolete">Kullanıcı Logosunu Değiştir</translation>
        <extra-contents_path>/UserinfoIntel/Change User Face</extra-contents_path>
    </message>
    <message>
        <source>System</source>
        <translation type="obsolete">Sistem</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">Onayla</translation>
    </message>
    <message>
        <source>select custom face file</source>
        <translation type="obsolete">Özel yüz dosyası seç</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="obsolete">Konum: </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="obsolete">Dosya Adı: </translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Uyarı</translation>
    </message>
    <message>
        <source>The avatar is larger than 2M, please choose again</source>
        <translation type="obsolete">Avatar 2M&apos;den büyük, lütfen başka seçin</translation>
    </message>
</context>
<context>
    <name>ChangeFeatureName</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>ChangeGroupDialog</name>
    <message>
        <source>User Group Settings</source>
        <translation type="obsolete">Kullanıcı Grubu Ayarları</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>User groups available in the system</source>
        <translation type="obsolete">Sistemde bulunan kullanıcı grupları</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="obsolete">Yeni Kullanıcı Ekle</translation>
    </message>
    <message>
        <source>User group</source>
        <translation type="obsolete">Kullanıcı Grubu</translation>
    </message>
    <message>
        <source>Add user group</source>
        <translation type="obsolete">Kullanıcı grubu ekle</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
</context>
<context>
    <name>ChangeGroupIntelDialog</name>
    <message>
        <source>User Group Settings</source>
        <translation type="obsolete">Kullanıcı Grubu Ayarları</translation>
    </message>
    <message>
        <source>User groups available in the system</source>
        <translation type="obsolete">Sistemde bulunan kullanıcı grupları</translation>
    </message>
    <message>
        <source>Add user group</source>
        <translation type="obsolete">Kullanıcı grubu ekle</translation>
    </message>
</context>
<context>
    <name>ChangePhoneIntelDialog</name>
    <message>
        <source>Phone number already in used!</source>
        <translation type="obsolete">Telefon numarası zaten kullanılıyor!</translation>
    </message>
</context>
<context>
    <name>ChangeProjectionName</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">Parola Değiştir</translation>
    </message>
    <message>
        <source>Pwd type</source>
        <translation type="vanished">Parola türü</translation>
    </message>
    <message>
        <source>New pwd</source>
        <translation type="vanished">Yeni parola</translation>
    </message>
    <message>
        <source>New pwd sure</source>
        <translation type="vanished">Yeniden Parola</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Onayla</translation>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="vanished">Parola Değiştir</translation>
    </message>
    <message>
        <source>General Pwd</source>
        <translation type="vanished">Genel Parola</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation type="vanished">Yeni Parola</translation>
    </message>
    <message>
        <source>New Password Identify</source>
        <translation type="vanished">Yeni Parola Tanımlama</translation>
    </message>
    <message>
        <source>Contains illegal characters!</source>
        <translation type="obsolete">Uygun olmayan karakterler içeriyor!</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">Şifre uzunluğu %1 karakterden fazla olmalı!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">Şifre uzunluğu %1 karakterden az olmalı!</translation>
    </message>
    <message>
        <source>Password length needs to more than 5 character!</source>
        <translation type="vanished">Şifre uzunluğu 5 karakterden fazla olmalıdır!</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">Parola ile tutarsızlık</translation>
    </message>
</context>
<context>
    <name>ChangePwdIntelDialog</name>
    <message>
        <source>Change Pwd</source>
        <translation type="obsolete">Parola Değiştir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">Onayla</translation>
    </message>
    <message>
        <source>General Pwd</source>
        <translation type="obsolete">Genel Parola</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation type="obsolete">Yeni Parola</translation>
    </message>
    <message>
        <source>New Password Identify</source>
        <translation type="obsolete">Yeni Parola Tanımlama</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="obsolete">Şifre uzunluğu %1 karakterden fazla olmalı!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="obsolete">Şifre uzunluğu %1 karakterden az olmalı!</translation>
    </message>
    <message>
        <source>Password cannot be made up entirely by Numbers!</source>
        <translation type="obsolete">Parola tamamen Sayılarla oluşturulamaz!</translation>
    </message>
</context>
<context>
    <name>ChangeTypeDialog</name>
    <message>
        <source>Make sure that there is at least one administrator on the computer</source>
        <translation type="vanished">Bilgisayarda en az bir yönetici olduğundan emin olun</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install software and change system settings</source>
        <translation type="vanished">Standart kullanıcılar çoğu yazılımı kullanabilir, ancak yazılım yükleyemez ve sistem ayarlarını değiştiremez</translation>
    </message>
    <message>
        <source>Change Account Type</source>
        <translation type="vanished">Hesap Türünü Değiştir</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">Standart kullanıcı</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">Yönetici</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">Yöneticiler istedikleri değişiklikleri yapabilir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Onayla</translation>
    </message>
    <message>
        <source>Change type</source>
        <translation type="vanished">Türü Değiştir</translation>
    </message>
</context>
<context>
    <name>ChangeTypeIntelDialog</name>
    <message>
        <source>Change Account Type</source>
        <translation type="obsolete">Hesap Türünü Değiştir</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install software and change system settings</source>
        <translation type="obsolete">Standart kullanıcılar çoğu yazılımı kullanabilir, ancak yazılım yükleyemez ve sistem ayarlarını değiştiremez</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="obsolete">Yönetici</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="obsolete">Yöneticiler istedikleri değişiklikleri yapabilir</translation>
    </message>
    <message>
        <source>Make sure that there is at least one administrator on the computer</source>
        <translation type="obsolete">Bilgisayarda en az bir yönetici olduğundan emin olun</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">Onayla</translation>
    </message>
</context>
<context>
    <name>ChangeUserLogo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="134"/>
        <source>System Logos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="111"/>
        <source>User logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="143"/>
        <source>Select Local Logo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="152"/>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="296"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="154"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="291"/>
        <source>select custom face file</source>
        <translation type="unfinished">Özel yüz dosyası seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="292"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="293"/>
        <source>Position: </source>
        <translation type="unfinished">Konum: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="294"/>
        <source>FileName: </source>
        <translation type="unfinished">Dosya Adı: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="295"/>
        <source>FileType: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="311"/>
        <source>Warning</source>
        <translation type="unfinished">Uyarı</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="312"/>
        <source>The avatar is larger than 1M, please choose again</source>
        <translation type="unfinished">Avatar 2M&apos;den büyük, lütfen başka seçin {1M?}</translation>
    </message>
</context>
<context>
    <name>ChangeUserName</name>
    <message>
        <source>UserName</source>
        <translation type="obsolete">Kullanıcı Adı</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>ChangeUserNickname</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="31"/>
        <source>Set Nickname</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="53"/>
        <source>UserName</source>
        <translation type="unfinished">Kullanıcı Adı</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="70"/>
        <source>NickName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="156"/>
        <source>NickName&apos;s length must between 1~%1 characters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="158"/>
        <source>nickName already in use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="160"/>
        <source>Can&apos;t contains &apos;:&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="113"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="116"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="98"/>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="103"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="470"/>
        <source>Current Pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="116"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="136"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="471"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="479"/>
        <source>New Pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="163"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="472"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="480"/>
        <source>Sure Pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="214"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="218"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="303"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="370"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="279"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="543"/>
        <source>Inconsistency with pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="299"/>
        <source>Same with old pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="353"/>
        <source>Pwd Changed Succes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="360"/>
        <source>Authentication failed, input authtok again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="502"/>
        <source>Contains illegal characters!</source>
        <translation type="unfinished">Uygun olmayan karakterler içeriyor!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="614"/>
        <source>current pwd cannot be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="619"/>
        <source>new pwd cannot be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="624"/>
        <source>sure pwd cannot be empty!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangeUserType</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="24"/>
        <source>UserType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="71"/>
        <source>administrator</source>
        <translation type="unfinished">Yönetici</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="73"/>
        <source>standard user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="61"/>
        <source>Select account type (Ensure have admin on system):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="75"/>
        <source>change system settings, install and upgrade software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="77"/>
        <source>use most software, cannot change system settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="130"/>
        <source>Note: Effective After Logout!!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="141"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="144"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
</context>
<context>
    <name>ChangeVaildDialog</name>
    <message>
        <source>Password Validity Setting</source>
        <translation type="vanished">Parola Geçerlilik Ayarı</translation>
    </message>
    <message>
        <source>Current passwd validity:</source>
        <translation type="vanished">Mevcut şifre geçerliliği:</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="vanished">Tarihi şu şekilde ayarlayın:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">Belirli</translation>
    </message>
</context>
<context>
    <name>ChangeValidDialog</name>
    <message>
        <source>Password Validity Setting</source>
        <translation type="vanished">Şifre Geçerlilik Ayarı</translation>
    </message>
    <message>
        <source>Current passwd validity:</source>
        <translation type="vanished">Mevcut şifre geçerliliği:</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="vanished">Tarihi şu şekilde ayarlayın:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">Belirli</translation>
    </message>
    <message>
        <source>Change valid</source>
        <translation type="vanished">Geçerliliğini Değiştir</translation>
    </message>
</context>
<context>
    <name>ChangeValidIntelDialog</name>
    <message>
        <source>Current passwd validity:</source>
        <translation type="obsolete">Mevcut şifre geçerliliği:</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="obsolete">Tarihi şu şekilde ayarlayın:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>ChangtimeDialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="161"/>
        <source>day</source>
        <translation>Gün</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="158"/>
        <source>time</source>
        <translation>Saat</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="159"/>
        <source>year</source>
        <translation>Yıl</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="160"/>
        <source>month</source>
        <translation>Ay</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="32"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>     B</source>
        <translation type="vanished">     M</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="86"/>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="46"/>
        <source>Choose a custom color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="254"/>
        <source>HEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="290"/>
        <source>RGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="457"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="476"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <source>     R</source>
        <translation type="vanished">   K</translation>
    </message>
    <message>
        <source>     G</source>
        <translation type="vanished">     Y</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="50"/>
        <source>Custom color</source>
        <translation>Özel renk</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="138"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
</context>
<context>
    <name>CreateGroupDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.ui" line="26"/>
        <source>Add New Group</source>
        <translation type="unfinished">Yeni Grup Ekle</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="51"/>
        <source>Name</source>
        <translation type="unfinished">İsim</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="75"/>
        <source>Id</source>
        <translation type="unfinished">Id</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="89"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="199"/>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="213"/>
        <source>GroupName&apos;s length must be between 1 and %1 characters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Members</source>
        <translation type="obsolete">Üyeler</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="vanished">Grup Adı</translation>
    </message>
    <message>
        <source>Group Id</source>
        <translation type="vanished">Grup ID</translation>
    </message>
    <message>
        <source>Group Members</source>
        <translation type="vanished">Grup Üyeleri</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="86"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="obsolete">Belirli</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="47"/>
        <source>Add user group</source>
        <translation type="unfinished">Kullanıcı grubu ekle</translation>
    </message>
</context>
<context>
    <name>CreateGroupIntelDialog</name>
    <message>
        <source>Add New Group</source>
        <translation type="obsolete">Yeni Grup Ekle</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="obsolete">Grup Adı</translation>
    </message>
    <message>
        <source>Group Id</source>
        <translation type="obsolete">Grup ID</translation>
    </message>
    <message>
        <source>Group Members</source>
        <translation type="obsolete">Grup Üyeleri</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>CreateUserDialog</name>
    <message>
        <source>UserName</source>
        <translation type="vanished">Kullanıcı Adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">Parola</translation>
    </message>
    <message>
        <source>Account Type</source>
        <translation type="vanished">Hesap Türü</translation>
    </message>
    <message>
        <source>Add New Account</source>
        <translation type="vanished">Yeni Hesap Ekle</translation>
    </message>
    <message>
        <source>PwdType</source>
        <translation type="vanished">Parola Türü</translation>
    </message>
    <message>
        <source>PasswordSure</source>
        <translation type="vanished">Parola Güvenliği</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">Standart kullanıcı</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install the software and 
change system settings</source>
        <translation type="vanished">Standart kullanıcılar çoğu yazılımı kullanabilir, ancak yazılımı ve
sistem ayarlarını değiştir</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">Yönetici</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">Yöneticiler istedikleri değişiklikleri yapabilir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Onayla</translation>
    </message>
    <message>
        <source>Password Identify</source>
        <translation type="vanished">Parola Tanımlama</translation>
    </message>
    <message>
        <source>General Password</source>
        <translation type="vanished">Genel Parola</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">Parola ile tutarsız</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">Şifre uzunluğu %1 karakterden fazla olmalı!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">Şifre uzunluğu %1 karakterden az olmalı!</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="vanished">Yeni Kullanıcı Ekle</translation>
    </message>
    <message>
        <source>Password cannot be made up entirely by Numbers!</source>
        <translation type="obsolete">Parola tamamen Sayılarla oluşturulamaz!</translation>
    </message>
    <message>
        <source>Contains illegal characters!</source>
        <translation type="obsolete">Uygun olmayan karakterler içeriyor!</translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="vanished">Kullanıcı adı boş olamaz</translation>
    </message>
    <message>
        <source>The first character must be lowercase letters!</source>
        <translation type="vanished">İlk karakter küçük harf olmalıdır!</translation>
    </message>
    <message>
        <source>User name can not contain capital letters!</source>
        <translation type="vanished">Kullanıcı adı büyük harf içeremez!</translation>
    </message>
    <message>
        <source>The user name is already in use, please use a different one.</source>
        <translation type="vanished">Kullanıcı adı zaten kullanılıyor, lütfen farklı bir ad kullanın.</translation>
    </message>
    <message>
        <source>User name length need to less than %1 letters!</source>
        <translation type="vanished">Kullanıcı adı uzunluğu %1 harften az olmalı!</translation>
    </message>
    <message>
        <source>The user name can only be composed of letters, numbers and underline!</source>
        <translation type="vanished">Kullanıcı adı sadece harf, rakam ve altı çizili olabilir!</translation>
    </message>
    <message>
        <source>The username is configured, please change the username</source>
        <translation type="vanished">Kullanıcı adı yapılandırıldı, lütfen kullanıcı adını değiştirin</translation>
    </message>
</context>
<context>
    <name>CreateUserIntelDialog</name>
    <message>
        <source>Add New Account</source>
        <translation type="obsolete">Yeni Hesap Ekle</translation>
    </message>
    <message>
        <source>Account Type</source>
        <translation type="obsolete">Hesap Türü</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install the software and 
change system settings</source>
        <translation type="obsolete">Standart kullanıcılar çoğu yazılımı kullanabilir, ancak yazılımı ve
sistem ayarlarını değiştir</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="obsolete">Yönetici</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="obsolete">Yöneticiler istedikleri değişiklikleri yapabilir</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">Onayla</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="obsolete">Kullanıcı Adı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
    <message>
        <source>Password Identify</source>
        <translation type="obsolete">Parola Tanımlama</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="obsolete">Şifre uzunluğu %1 karakterden fazla olmalı!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="obsolete">Şifre uzunluğu %1 karakterden az olmalı!</translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="obsolete">Kullanıcı adı boş olamaz</translation>
    </message>
    <message>
        <source>The first character must be lowercase letters!</source>
        <translation type="obsolete">İlk karakter küçük harf olmalıdır!</translation>
    </message>
    <message>
        <source>User name can not contain capital letters!</source>
        <translation type="obsolete">Kullanıcı adı büyük harf içeremez!</translation>
    </message>
    <message>
        <source>The user name is already in use, please use a different one.</source>
        <translation type="obsolete">Kullanıcı adı zaten kullanılıyor, lütfen farklı bir ad kullanın.</translation>
    </message>
    <message>
        <source>User name length need to less than %1 letters!</source>
        <translation type="obsolete">Kullanıcı adı uzunluğu %1 harften az olmalı!</translation>
    </message>
    <message>
        <source>The user name can only be composed of letters, numbers and underline!</source>
        <translation type="obsolete">Kullanıcı adı sadece harf, rakam ve altı çizili olabilir!</translation>
    </message>
    <message>
        <source>The username is configured, please change the username</source>
        <translation type="obsolete">Kullanıcı adı yapılandırıldı, lütfen kullanıcı adını değiştirin</translation>
    </message>
</context>
<context>
    <name>CreateUserNew</name>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="49"/>
        <source>CreateUserNew</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="55"/>
        <source>UserName</source>
        <translation type="unfinished">Kullanıcı Adı</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="65"/>
        <source>NickName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="71"/>
        <source>HostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="80"/>
        <source>Pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="88"/>
        <source>SurePwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="96"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="99"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="102"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="105"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="109"/>
        <source>verification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="182"/>
        <source>Select Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="191"/>
        <source>Administrator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="194"/>
        <source>Users can make any changes they need</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="196"/>
        <source>Standard User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="199"/>
        <source>Users cannot change system settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="276"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="279"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="355"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="581"/>
        <source>Inconsistency with pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="494"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="647"/>
        <source>NickName&apos;s length must be between 1 and %1 characters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="496"/>
        <source>nickName already in use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="642"/>
        <source>Username&apos;s length must be between 1 and %1 characters!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="652"/>
        <source>new pwd cannot be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="657"/>
        <source>sure pwd cannot be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="obsolete">Kullanıcı adı boş olamaz</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="526"/>
        <source>Name corresponds to group already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="522"/>
        <source>Username&apos;s folder exists, change another one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="551"/>
        <source>Contains illegal characters!</source>
        <translation type="unfinished">Uygun olmayan karakterler içeriyor!</translation>
    </message>
</context>
<context>
    <name>CustomGlobalTheme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/globaltheme/customglobaltheme.cpp" line="38"/>
        <source>custom</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomLineEdit</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/customlineedit.cpp" line="28"/>
        <source>New Shortcut...</source>
        <translation type="unfinished">Yeni Kısayol</translation>
    </message>
</context>
<context>
    <name>DataFormat</name>
    <message>
        <source>change format of data</source>
        <translation type="vanished">Veri biçimini değiştir</translation>
    </message>
    <message>
        <source>calendar</source>
        <translation type="vanished">Takvim</translation>
    </message>
    <message>
        <source>first day</source>
        <translation type="vanished">Haftanın İlk Günü</translation>
    </message>
    <message>
        <source>date</source>
        <translation type="vanished">Tarih</translation>
    </message>
    <message>
        <source>time</source>
        <translation type="vanished">Saat</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">Doğrula</translation>
    </message>
    <message>
        <source>first day of week</source>
        <translation type="vanished">Haftanın ilk günü</translation>
    </message>
    <message>
        <source>lunar</source>
        <translation type="vanished">Ay Takvimi</translation>
    </message>
    <message>
        <source>solar calendar</source>
        <translation type="vanished">Güneş Takvimi</translation>
    </message>
    <message>
        <source>monday</source>
        <translation type="vanished">Pazartesi</translation>
    </message>
    <message>
        <source>sunday</source>
        <translation type="vanished">Pazar</translation>
    </message>
</context>
<context>
    <name>DateTime</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="26"/>
        <source>DateTime</source>
        <translation>Tarih Zaman</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="65"/>
        <source>current date</source>
        <translation>Şimdiki tarih</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="321"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="275"/>
        <source>Change timezone</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Date/Change time zone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="444"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="620"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="481"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="510"/>
        <source>RadioButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="712"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="744"/>
        <source>:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="952"/>
        <source>titleLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>timezone</source>
        <translation type="vanished">Zaman dilimi</translation>
    </message>
    <message>
        <source>Sync system time</source>
        <translation type="vanished">Sistem Saatini Senkronize Et</translation>
        <extra-contents_path>/datetime/Sync system time</extra-contents_path>
    </message>
    <message>
        <source>Change time</source>
        <translation type="vanished">Saati değiştir</translation>
        <extra-contents_path>/date/Change time</extra-contents_path>
    </message>
    <message>
        <source>Change time zone</source>
        <translation type="vanished">Zaman dilimini değiştir</translation>
        <extra-contents_path>/date/Change time zone</extra-contents_path>
    </message>
    <message>
        <source>Sync complete</source>
        <translation type="vanished">Senkronizasyon Tamamlandı</translation>
    </message>
    <message>
        <source>datetime</source>
        <translation type="vanished">Tarih Saat</translation>
    </message>
    <message>
        <source>Datetime</source>
        <translation type="vanished">Tarih Saat</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="94"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="175"/>
        <source>Current Date</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Date/Current Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="178"/>
        <source>Other Timezone</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Date/Other Timezone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="196"/>
        <source>24-hour clock</source>
        <translation>24 saat biçimi</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="198"/>
        <source>Set Time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Date/Set Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="226"/>
        <source>Set Date Manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="286"/>
        <source>Sync Time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Date/Sync Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="289"/>
        <source>Manual Time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Date/Manual Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="375"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
        <extra-contents_path>/Date/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="650"/>
        <source>Add Timezone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Sil</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="obsolete">Ağ</translation>
        <extra-contents_path>/Date/Network</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="461"/>
        <source>Sync Server</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Date/Sync Server</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="463"/>
        <source>Default</source>
        <translation type="unfinished">Varsayılan</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="465"/>
        <source>Customize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="475"/>
        <source>Server Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="480"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="481"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="640"/>
        <source>change time</source>
        <translation>Saati değiştir</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="652"/>
        <source>Change Timezone</source>
        <translation type="unfinished">Zaman Dilimini Değiştir</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="772"/>
        <source>MMMM d, yy ddd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="774"/>
        <source>MMMM dd, yyyy ddd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="932"/>
        <source>  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="933"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="942"/>
        <source>Sync Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DefaultApp</name>
    <message>
        <source>defaultapp</source>
        <translation type="vanished">Varsayılan Uygulama</translation>
    </message>
    <message>
        <source>Defaultapp</source>
        <translation type="vanished">Varsayılan Uygulama</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="43"/>
        <source>Default App</source>
        <translation type="unfinished">Varsayılan Uygulama</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="67"/>
        <source>No program available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="68"/>
        <source>Choose default app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="252"/>
        <source>Reset default apps to system recommended apps</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Defaultapp/Reset default apps to system recommended apps</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="253"/>
        <source>Reset</source>
        <translation type="unfinished">Sıfırla</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="348"/>
        <source>Browser</source>
        <translation type="unfinished">Web Tarayıcı:</translation>
        <extra-contents_path>/Defaultapp/Browser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="350"/>
        <source>Mail</source>
        <translation type="unfinished">E-Posta:</translation>
        <extra-contents_path>/Defaultapp/Mail</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="352"/>
        <source>Image Viewer</source>
        <translation type="unfinished">Resim Görüntüleyici:</translation>
        <extra-contents_path>/Defaultapp/Image Viewer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="354"/>
        <source>Audio Player</source>
        <translation type="unfinished">Ses Oynatıcı:</translation>
        <extra-contents_path>/Defaultapp/Audio Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="356"/>
        <source>Video Player</source>
        <translation type="unfinished">Video Oynatıcı:</translation>
        <extra-contents_path>/Defaultapp/Video Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="358"/>
        <source>Text Editor</source>
        <translation type="unfinished">Metin Düzenleyici:</translation>
        <extra-contents_path>/Defaultapp/Text Editor</extra-contents_path>
    </message>
</context>
<context>
    <name>DefaultAppWindow</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="346"/>
        <source>Select Default Application</source>
        <translation>Varsayılan Uygulama Seç</translation>
        <extra-contents_path>/Defaultapp/Select Default Application</extra-contents_path>
    </message>
    <message>
        <source>Browser</source>
        <translation type="vanished">Web Tarayıcı:</translation>
    </message>
    <message>
        <source>Mail</source>
        <translation type="vanished">E-Posta:</translation>
    </message>
    <message>
        <source>Image Viewer</source>
        <translation type="vanished">Resim Görüntüleyici:</translation>
    </message>
    <message>
        <source>Audio Player</source>
        <translation type="vanished">Ses Oynatıcı:</translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation type="vanished">Video Oynatıcı:</translation>
    </message>
    <message>
        <source>Text Editor</source>
        <translation type="vanished">Metin Düzenleyici:</translation>
    </message>
    <message>
        <source>Reset to default</source>
        <translation type="vanished">Varsayılana dön</translation>
    </message>
</context>
<context>
    <name>DefineGroupItem</name>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Düzenle</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Sil</translation>
    </message>
</context>
<context>
    <name>DefineGroupItemIntel</name>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Düzenle</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Sil</translation>
    </message>
</context>
<context>
    <name>DefineShortcutItem</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/defineshortcutitem.cpp" line="58"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
</context>
<context>
    <name>DelGroupDialog</name>
    <message>
        <source>Are you sure to delete this group, 
which will make some file components 
in the file system invalid!</source>
        <translation type="vanished">Bu grubu silmek istediğinizden emin misiniz,
bazı dosya bileşenleri oluşturacak
dosya sisteminde geçersiz!</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Sil</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="obsolete">Dosya Sil</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="obsolete">Hatırlat</translation>
    </message>
    <message>
        <source>Are you sure to delete &quot;%1&quot; group, 
which will make some file components 
in the file system invalid!</source>
        <translation type="obsolete">%1 grubu silmek istediğinizden emin misiniz,
bazı dosya bileşenleri oluşturacak
dosya sisteminde geçersiz!</translation>
    </message>
</context>
<context>
    <name>DelGroupIntelDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="obsolete">Dosya Sil</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="obsolete">Hatırlat</translation>
    </message>
</context>
<context>
    <name>DelUserDialog</name>
    <message>
        <source>Delete the user, belonging to the user&apos;s desktop,
documents, favorites, music, pictures and video 
folder will be deleted!</source>
        <translation type="vanished">Kullanıcıyı sil, kullanıcının masaüstü, belgeler, sık kullanılanlar, müzik, resim ve video klasörü silinecektir!</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Sil</translation>
    </message>
    <message>
        <source>KeepFile</source>
        <translation type="vanished">Dosyayı Tut</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="vanished">Dosya Sil</translation>
    </message>
</context>
<context>
    <name>DelUserIntelDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>DeleteUserExists</name>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="26"/>
        <source>Delete User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="39"/>
        <source>Delete user &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="40"/>
        <source>&apos;? And:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="66"/>
        <source>Keep desktop, files, favorites, music of the user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="67"/>
        <source>Delete whole data belong user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="99"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="101"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
</context>
<context>
    <name>Desktop</name>
    <message>
        <source>Icon Show On Desktop</source>
        <translation type="vanished">Masaüstünde Simgeleri Göster</translation>
        <extra-contents_path>/desktop/Icon Show On Desktop</extra-contents_path>
    </message>
    <message>
        <source>Computerdesktop</source>
        <translation type="vanished">Bilgisayar</translation>
    </message>
    <message>
        <source>Trashdesktop</source>
        <translation type="vanished">Çöp</translation>
    </message>
    <message>
        <source>Homedesktop</source>
        <translation type="vanished">Giriş</translation>
    </message>
    <message>
        <source>Volumedesktop</source>
        <translation type="vanished">Birimler</translation>
    </message>
    <message>
        <source>Networkdesktop</source>
        <translation type="vanished">Ağ</translation>
    </message>
    <message>
        <source>Set Start Menu</source>
        <translation type="vanished">Başlat Menüsü Ayarla</translation>
    </message>
    <message>
        <source>Always use the start menu in full screen</source>
        <translation type="vanished">Her zaman tam ekranda başlat menüsünü kullanın</translation>
    </message>
    <message>
        <source>Icon Lock on Menu</source>
        <translation type="vanished">Menüde Kilit Simgesi</translation>
    </message>
    <message>
        <source>Computermenu</source>
        <translation type="vanished">Bilgisayar</translation>
    </message>
    <message>
        <source>Trashmenu</source>
        <translation type="vanished">Çöp Kutusu</translation>
    </message>
    <message>
        <source>Filesystemmenu</source>
        <translation type="vanished">Dosya Sistemi</translation>
    </message>
    <message>
        <source>Tray icon</source>
        <translation type="vanished">Tepsi Simgesi</translation>
        <extra-contents_path>/desktop/Tray icon</extra-contents_path>
    </message>
    <message>
        <source>Homemenu</source>
        <translation type="vanished">Giriş</translation>
    </message>
    <message>
        <source>Settingmenu</source>
        <translation type="vanished">Ayarlar</translation>
    </message>
    <message>
        <source>Networkmenu</source>
        <translation type="vanished">Ağ</translation>
    </message>
    <message>
        <source>desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="obsolete">Sol</translation>
    </message>
    <message>
        <source>Right</source>
        <translation type="obsolete">Sağ</translation>
    </message>
</context>
<context>
    <name>DeviceInfoItem</name>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">Bağlantıyı kes</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>Dialog_login_reg</name>
    <message>
        <source>Sign in</source>
        <translation type="vanished">Oturum aç</translation>
    </message>
    <message>
        <source>Sign up</source>
        <translation type="vanished">Kayıt ol</translation>
    </message>
    <message>
        <source>Login in progress</source>
        <translation type="vanished">Giriş devam ediyor</translation>
    </message>
    <message>
        <source>Error code:</source>
        <translation type="vanished">Hata kodu:</translation>
    </message>
    <message>
        <source>!</source>
        <translation type="vanished">!</translation>
    </message>
    <message>
        <source>Internal error occurring!</source>
        <translation type="vanished">Dahili hata meydana geliyor!</translation>
    </message>
    <message>
        <source>Failed to sign up!</source>
        <translation type="vanished">Kayıt başarısız oldu!</translation>
    </message>
    <message>
        <source>Failed attempt to return value!</source>
        <translation type="vanished">Değer döndürme denemesi başarısız oldu!</translation>
    </message>
    <message>
        <source>Check your connection!</source>
        <translation type="vanished">Bağlantınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Failed to get by phone!</source>
        <translation type="vanished">Telefonla ulaşılamadı!</translation>
    </message>
    <message>
        <source>Failed to get by user!</source>
        <translation type="vanished">Kullanıcı tarafından alınamadı!</translation>
    </message>
    <message>
        <source>Failed to reset password!</source>
        <translation type="vanished">Şifre sıfırlanamadı!</translation>
    </message>
    <message>
        <source>Phone binding falied!</source>
        <translation type="vanished">Telefon bağlantısı hatalı!</translation>
    </message>
    <message>
        <source>Please check your information!</source>
        <translation type="vanished">Lütfen bilgilerinizi kontrol edin!</translation>
    </message>
    <message>
        <source>Please check your account!</source>
        <translation type="vanished">Lütfen hesabınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Failed due to server error!</source>
        <translation type="vanished">Sunucu hatası nedeniyle başarısız oldu!</translation>
    </message>
    <message>
        <source>User existing!</source>
        <translation type="vanished">Kullanıcı var!</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">Telefon numarası zaten kullanılıyor!</translation>
    </message>
    <message>
        <source>Please check your format!</source>
        <translation type="vanished">Lütfen formatınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation type="vanished">Sınıra ulaştın!</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">Lütfen telefon numaranızı kontrol edin!</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation type="vanished">Lütfen kodunuzu kontrol edin!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">Hesap mevcut değil!</translation>
    </message>
    <message>
        <source>User has bound the phone!</source>
        <translation type="vanished">Kullanıcı telefonu bağladı!</translation>
    </message>
    <message>
        <source>Sending code error occurring!</source>
        <translation type="vanished">Kod gönderme hatası oluşuyor!</translation>
    </message>
    <message>
        <source>Your code is wrong!</source>
        <translation type="vanished">Kodunuz yanlış!</translation>
    </message>
    <message>
        <source>Binding Phone</source>
        <translation type="vanished">Telefon Bağlanıyor</translation>
    </message>
    <message>
        <source>Bind now</source>
        <translation type="vanished">Şimdi bağla</translation>
    </message>
    <message>
        <source>Resend ( %1 )</source>
        <translation type="vanished">Yeniden gönder (%1)</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">Telefon kodunu alın</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Gönder</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="obsolete">En az 6 bit, harf ve rakam içermelidir</translation>
    </message>
    <message>
        <source>Please check your password!</source>
        <translation type="vanished">Lütfen şifrenizi kontrol edin!</translation>
    </message>
    <message>
        <source>Sign in Cloud</source>
        <translation type="vanished">Bulutta oturum açın</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="vanished">Unut</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">Ayarla</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Geri</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="vanished">Hesap Oluştur</translation>
    </message>
    <message>
        <source>Sign up now</source>
        <translation type="vanished">Şimdi kayıt ol</translation>
    </message>
</context>
<context>
    <name>DisplayPerformanceDialog</name>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="214"/>
        <source>Display Advanced Settings</source>
        <translation>Gelişmiş Ayarları Göster</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="297"/>
        <source>Performance</source>
        <translation>Performans</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="376"/>
        <source>Applicable to machine with discrete graphics, which can accelerate the rendering of 3D graphics.</source>
        <translation>3D grafiklerin oluşturulmasını hızlandırabilen ayrı grafiklere sahip makine için geçerlidir.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="392"/>
        <source>(Note: not support connect graphical with xmanager on windows.)</source>
        <translation>(Not: Pencerede xmanager ile grafiksel bağlantıyı desteklemez.)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="462"/>
        <source>Compatible</source>
        <translation>Uyumlu</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="538"/>
        <source>Applicable to machine with integrated graphics,  there is no 3D graphics acceleration. </source>
        <translation>Dahili ekran kartı olan bilgisayara uygulanabilir, 3D grafik hızlandırma yoktur. </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="554"/>
        <source>(Note: need connect graphical with xmanager on windows, use this option.)</source>
        <translation>(Not: Pencerede xmanager ile grafiksel olarak bağlanmanız gerekir, bu seçeneği kullanın.)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="604"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="vanished">Otomatik</translation>
    </message>
    <message>
        <source>Auto select according to environment, delay the login time (about 0.5 sec).</source>
        <translation type="vanished">Ortama göre otomatik seçim, oturum açma süresini geciktirir (yaklaşık 0,5 sn.).</translation>
    </message>
    <message>
        <source>Threshold:</source>
        <translation type="vanished">Eşik:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="597"/>
        <source>Apply</source>
        <translation>Uygula</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">Sıfırla</translation>
    </message>
    <message>
        <source>(Note: select this option to use 3D graphics acceleration and xmanager.)</source>
        <translation type="vanished">(Not: 3D grafik hızlandırma ve xmanager kullanmak için bu seçeneği seçin.)</translation>
    </message>
</context>
<context>
    <name>DisplaySet</name>
    <message>
        <source>display</source>
        <translation type="vanished">Ekran</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="36"/>
        <source>Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="38"/>
        <source>Display</source>
        <translation>Ekran</translation>
    </message>
</context>
<context>
    <name>DisplayWindow</name>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>monitor</source>
        <translation type="vanished">Ekran:</translation>
    </message>
    <message>
        <source>set as home screen</source>
        <translation type="vanished">Ana Ekran Yap</translation>
    </message>
    <message>
        <source>close monitor</source>
        <translation type="vanished">Ekranı Kapat</translation>
    </message>
    <message>
        <source>unify output</source>
        <translation type="vanished">Çıktıyı Birleştir</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="320"/>
        <source>open monitor</source>
        <translation>Monitörü Aç</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="32"/>
        <source>Display</source>
        <translation>Ekran</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="250"/>
        <source>screen zoom</source>
        <translation type="unfinished">Ekran Yakınlaştırma:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="374"/>
        <source>Advanced</source>
        <translation>Gelişmiş</translation>
    </message>
    <message>
        <source>screen brightness adjustment</source>
        <translation type="vanished">Ekran Parlaklığı</translation>
    </message>
    <message>
        <source>dark</source>
        <translation type="vanished">Koyu</translation>
    </message>
    <message>
        <source>bright</source>
        <translation type="vanished">Açık</translation>
    </message>
    <message>
        <source>follow the sunrise and sunset(17:55-05:04)</source>
        <translation type="vanished">Gün doğumunu ve gün batımını takip et(17:55-05:04)</translation>
    </message>
    <message>
        <source>custom time</source>
        <translation type="vanished">Özel zaman</translation>
    </message>
    <message>
        <source>opening time</source>
        <translation type="vanished">Açılış zamanı</translation>
    </message>
    <message>
        <source>closing time</source>
        <translation type="vanished">Kapanış zamanı</translation>
    </message>
    <message>
        <source>color temperature</source>
        <translation type="vanished">Renk Sıcaklığı</translation>
    </message>
    <message>
        <source>warm</source>
        <translation type="vanished">Ilık</translation>
    </message>
    <message>
        <source>cold</source>
        <translation type="vanished">Soğuk</translation>
    </message>
    <message>
        <source>apply</source>
        <translation type="vanished">Uygula</translation>
    </message>
</context>
<context>
    <name>EditGroupDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="obsolete">Belirli</translation>
    </message>
    <message>
        <source>Edit User Group</source>
        <translation type="obsolete">Kullanıcı Grubunu Düzenle</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">İsim</translation>
    </message>
    <message>
        <source>Id</source>
        <translation type="obsolete">Id</translation>
    </message>
    <message>
        <source>Members</source>
        <translation type="obsolete">Üyeler</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
</context>
<context>
    <name>EditPassDialog</name>
    <message>
        <source>Edit Password</source>
        <translation type="vanished">Parolayı Düzenle</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">Hesabınız burada</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="obsolete">Parolanız burada</translation>
    </message>
    <message>
        <source>Your new password here</source>
        <translation type="vanished">Yeni parolanız burada</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">Kodunuz burada</translation>
    </message>
    <message>
        <source>Your code</source>
        <translation type="vanished">Kodunuz</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">Telefon kodunu al</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Onayla</translation>
    </message>
    <message>
        <source>Confirm your new password</source>
        <translation type="vanished">Yeni parolanızı onaylayın</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">En az 6 karakter, harf ve rakam ekleyin</translation>
    </message>
    <message>
        <source>Your password is valid!</source>
        <translation type="vanished">Parolanız geçerlidir!</translation>
    </message>
    <message>
        <source>Please check your password!</source>
        <translation type="vanished">Lütfen şifrenizi kontrol edin!</translation>
    </message>
    <message>
        <source>Resend(</source>
        <translation type="vanished">Yeniden gönder(</translation>
    </message>
    <message>
        <source>)</source>
        <translation type="vanished">)</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Gönder</translation>
    </message>
    <message>
        <source>Success！</source>
        <translation type="vanished">Başarılı!</translation>
    </message>
    <message>
        <source>Reback sign in</source>
        <translation type="vanished">Yeniden oturum aç</translation>
    </message>
    <message>
        <source>Error code:</source>
        <translation type="vanished">Hata kodu:</translation>
    </message>
    <message>
        <source>!</source>
        <translation type="vanished">!</translation>
    </message>
    <message>
        <source>Internal error occurring!</source>
        <translation type="vanished">Dahili hata oluştu!</translation>
    </message>
    <message>
        <source>Failed to sign up!</source>
        <translation type="vanished">Kaydolamadı!</translation>
    </message>
    <message>
        <source>Failed attempt to return value!</source>
        <translation type="vanished">Değer döndürülemedi!</translation>
    </message>
    <message>
        <source>Check your connection!</source>
        <translation type="vanished">Bağlantınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Failed to get by phone!</source>
        <translation type="vanished">Telefonla alınamadı!</translation>
    </message>
    <message>
        <source>Failed to get by user!</source>
        <translation type="vanished">Kullanıcı tarafından alınamadı!</translation>
    </message>
    <message>
        <source>Failed to reset password!</source>
        <translation type="vanished">Şifre sıfırlanamadı!</translation>
    </message>
    <message>
        <source>Please check your information!</source>
        <translation type="vanished">Lütfen bilgilerinizi kontrol edin!</translation>
    </message>
    <message>
        <source>Please check your account!</source>
        <translation type="vanished">Lütfen hesabınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Failed due to server error!</source>
        <translation type="vanished">Sunucu hatası nedeniyle başarısız oldu!</translation>
    </message>
    <message>
        <source>User existing!</source>
        <translation type="vanished">Kullanıcı mevcut!</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">Telefon numarası zaten kullanılıyor!</translation>
    </message>
    <message>
        <source>Please check your format!</source>
        <translation type="vanished">Lütfen biçiminizi kontrol edin!</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation type="vanished">Sınıra ulaşıyorsunuz!</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">Lütfen telefon numaranızı kontrol edin!</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation type="vanished">Lütfen kodunuzu kontrol edin!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">Hesap mevcut değil!</translation>
    </message>
    <message>
        <source>Sending code error occurring!</source>
        <translation type="vanished">Gönderme kodu hatası oluştu!</translation>
    </message>
</context>
<context>
    <name>EditPushButton</name>
    <message>
        <source>Reset</source>
        <translation type="vanished">Sıfırla</translation>
    </message>
</context>
<context>
    <name>ExperiencePlan</name>
    <message>
        <source>User Experience</source>
        <translation type="vanished">Kullanıcı Deneyimi</translation>
    </message>
    <message>
        <source>Join in user Experience plan</source>
        <translation type="vanished">Kullanıcı Deneyimi planına katılın</translation>
    </message>
    <message>
        <source>User experience plan terms, see</source>
        <translation type="vanished">Kullanıcı deneyimi planı şartları, bkz</translation>
    </message>
    <message>
        <source>《User Experience plan》</source>
        <translation type="vanished">(Kullanıcı Deneyimi Planı)</translation>
    </message>
    <message>
        <source>experienceplan</source>
        <translation type="vanished">Deneyim Planı</translation>
    </message>
    <message>
        <source>Experienceplan</source>
        <translation type="vanished">Deneyim Planı</translation>
    </message>
</context>
<context>
    <name>Fonts</name>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="50"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="46"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="122"/>
        <source>Fonts</source>
        <translation>Yazı Tipi</translation>
        <extra-contents_path>/Fonts/Fonts</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="264"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="126"/>
        <source>Fonts select</source>
        <translation>Font Seç</translation>
        <extra-contents_path>/Fonts/Fonts select</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="146"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="124"/>
        <source>Font size</source>
        <translation>Font Boyutu</translation>
        <extra-contents_path>/Fonts/Font size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="370"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="128"/>
        <source>Mono font</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Fonts/Mono font</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="421"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="130"/>
        <source>Reset to default</source>
        <translation>Varsayılana Sıfırla</translation>
        <extra-contents_path>/Fonts/Reset to default</extra-contents_path>
    </message>
    <message>
        <source>Gtk default font</source>
        <translation type="vanished">Gtk Fontu</translation>
    </message>
    <message>
        <source>Document font</source>
        <translation type="vanished">Belge Fontu:</translation>
    </message>
    <message>
        <source>Monospace font</source>
        <translation type="vanished">Monospace Fontu:</translation>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation type="vanished">Gelişmiş Ayarlar</translation>
    </message>
    <message>
        <source>Peony font</source>
        <translation type="vanished">Peony Fontu:</translation>
    </message>
    <message>
        <source>titlebar font</source>
        <translation type="vanished">Başlık Fontu:</translation>
    </message>
    <message>
        <source>Select text sample that looks clearest</source>
        <translation type="vanished">En net görünen metin örneğini seç</translation>
    </message>
    <message>
        <source>fonts</source>
        <translation type="vanished">Fontlar</translation>
    </message>
    <message>
        <source>11</source>
        <translation type="vanished">11</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="vanished">12</translation>
    </message>
    <message>
        <source>13</source>
        <translation type="vanished">13</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
    <message>
        <source>15</source>
        <translation type="vanished">15</translation>
    </message>
    <message>
        <source>16</source>
        <translation type="vanished">16</translation>
    </message>
    <message>
        <source>Thanks For Using The ukcc</source>
        <translation type="vanished">Denetim Merkezini kullandığınız için teşekkürler</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="144"/>
        <source>Small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="148"/>
        <source>Large</source>
        <translation type="unfinished">Büyük</translation>
    </message>
</context>
<context>
    <name>FrameItem</name>
    <message>
        <source>Sync failed, please login out to retry!</source>
        <translation type="vanished">Senkronizasyon başarısız, tekrar denemek için lütfen giriş yapın!</translation>
    </message>
    <message>
        <source>Change configuration file failed, please login out to retry!</source>
        <translation type="vanished">Yapılandırma dosyasını değiştiremedi, tekrar denemek için lütfen oturumu kapatın!</translation>
    </message>
    <message>
        <source>Configuration file not exist, please login out to retry!</source>
        <translation type="vanished">Yapılandırma dosyası mevcut değil, lütfen yeniden denemek için giriş yapın!</translation>
    </message>
    <message>
        <source>Cloud verifyed file download failed, please login out to retry!</source>
        <translation type="vanished">Bulutla doğrulanmış dosya indirme işlemi başarısız oldu, yeniden denemek için lütfen oturumu kapatın!</translation>
    </message>
    <message>
        <source>OSS access failed, please login out to retry!</source>
        <translation type="vanished">OSS erişimi başarısız, yeniden denemek için lütfen oturumu kapatın!</translation>
    </message>
    <message>
        <source>Sync failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">Senkronizasyon başarısız oldu, daha iyi bir deneyim için lütfen tekrar deneyin veya oturum açın!</translation>
    </message>
    <message>
        <source>Change configuration file failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">Yapılandırma dosyasını değiştirme başarısız oldu, daha iyi bir deneyim için lütfen tekrar deneyin veya oturumu kapatın!</translation>
    </message>
    <message>
        <source>Configuration file not exist, please retry or login out to get a better experience!</source>
        <translation type="vanished">Yapılandırma dosyası mevcut değil, daha iyi bir deneyim için lütfen yeniden deneyin veya oturum açın!</translation>
    </message>
    <message>
        <source>Cloud verifyed file download failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">Bulutla doğrulanmış dosya indirme işlemi başarısız oldu, daha iyi bir deneyim için lütfen tekrar deneyin veya oturumu kapatın!</translation>
    </message>
    <message>
        <source>OSS access failed, please retry or login out to get a better experience!</source>
        <translation type="vanished">OSS erişimi başarısız oldu, daha iyi bir deneyim için lütfen tekrar deneyin veya oturumu kapatın!</translation>
    </message>
</context>
<context>
    <name>GetShortcutWorker</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="59"/>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="83"/>
        <source>Null</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GrubVerify</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="24"/>
        <source>Grub verify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="29"/>
        <source>User:</source>
        <translation type="unfinished">Kullanıcı:</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="42"/>
        <source>Pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="85"/>
        <source>Sure Pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="138"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="142"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="197"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="235"/>
        <source>Inconsistency with pwd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="225"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="303"/>
        <source>pwd cannot be empty!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HostNameDialog</name>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="17"/>
        <source>Set HostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="45"/>
        <source>HostName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="91"/>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="94"/>
        <source>Must be 1-64 characters long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="161"/>
        <source>Hostname must start or end with a number and a letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="172"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="179"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="75"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="71"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
</context>
<context>
    <name>InputPwdDialog</name>
    <message>
        <source>Set</source>
        <translation type="obsolete">Ayarla</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="28"/>
        <source>VNC password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="45"/>
        <source>Password</source>
        <translation type="unfinished">Parola</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="60"/>
        <source>Must be 1-8 characters long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="77"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="81"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
</context>
<context>
    <name>ItemList</name>
    <message>
        <source>Walpaper</source>
        <translation type="vanished">Duvar Kağıdı</translation>
    </message>
    <message>
        <source>ScreenSaver</source>
        <translation type="vanished">Ekran Koruyucu</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Menü</translation>
    </message>
    <message>
        <source>Quick Start</source>
        <translation type="vanished">Hızlı Başlat</translation>
    </message>
    <message>
        <source>Avatar</source>
        <translation type="obsolete">Avatar</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Sekme</translation>
    </message>
    <message>
        <source>Themes</source>
        <translation type="obsolete">Temalar</translation>
    </message>
    <message>
        <source>Area</source>
        <translation type="obsolete">Alan</translation>
    </message>
    <message>
        <source>Date/Time</source>
        <translation type="obsolete">Tarih/Zaman</translation>
    </message>
    <message>
        <source>Default Open</source>
        <translation type="obsolete">Varsayılan Açık</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Bildirim</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="obsolete">Seçenek</translation>
    </message>
    <message>
        <source>Peony</source>
        <translation type="obsolete">Peony</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">Hava Durumu</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">Medya</translation>
    </message>
    <message>
        <source>Boot</source>
        <translation type="obsolete">Boot</translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="obsolete">Güç</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation type="obsolete">Düzenleyici</translation>
    </message>
    <message>
        <source>Terminal</source>
        <translation type="obsolete">Uçbirim</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">Fare</translation>
    </message>
    <message>
        <source>TouchPad</source>
        <translation type="vanished">Dokunmatik Yüzey</translation>
    </message>
    <message>
        <source>KeyBoard</source>
        <translation type="vanished">Klavye</translation>
    </message>
    <message>
        <source>ShortCut</source>
        <translation type="vanished">Kısayol</translation>
    </message>
</context>
<context>
    <name>KbPreviewFrame</name>
    <message>
        <source>No preview found</source>
        <translation type="obsolete">Önizleme bulunamadı</translation>
    </message>
    <message>
        <source>Unable to open Preview !</source>
        <translation type="obsolete">Önizleme açılamıyor!</translation>
    </message>
</context>
<context>
    <name>KbdLayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="68"/>
        <source>C</source>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="144"/>
        <source>L</source>
        <translation>L</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="222"/>
        <source>Variant</source>
        <translation>Varyant</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="270"/>
        <source>Add</source>
        <translation>Ekle</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="58"/>
        <source>Add Layout</source>
        <translation>Düzen Ekle</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="233"/>
        <source>Del</source>
        <translation>Sil</translation>
    </message>
    <message>
        <source>Keyboard Preview</source>
        <translation type="obsolete">Klavye Önizleme</translation>
    </message>
</context>
<context>
    <name>KeyValueConverter</name>
    <message>
        <source>system</source>
        <translation type="vanished">Sistem</translation>
    </message>
    <message>
        <source>devices</source>
        <translation type="vanished">Aygıtlar</translation>
    </message>
    <message>
        <source>personalized</source>
        <translation type="vanished">Özelleştirme</translation>
    </message>
    <message>
        <source>network</source>
        <translation type="vanished">Ağ</translation>
    </message>
    <message>
        <source>account</source>
        <translation type="vanished">Hesap</translation>
    </message>
    <message>
        <source>datetime</source>
        <translation type="vanished">Tarih Saat</translation>
    </message>
    <message>
        <source>update</source>
        <translation type="vanished">Güncelleme</translation>
    </message>
    <message>
        <source>messages</source>
        <translation type="vanished">Mesajlar</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="46"/>
        <source>System</source>
        <translation>Sistem</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="49"/>
        <source>Devices</source>
        <translation>Aygıtlar</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="55"/>
        <source>Personalized</source>
        <translation>Özelleştirme</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="52"/>
        <source>Network</source>
        <translation>Ağ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="58"/>
        <source>Account</source>
        <translation>Hesap</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="61"/>
        <source>Datetime</source>
        <translation>Tarih-Zaman</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="64"/>
        <source>Update</source>
        <translation>Güncelleme</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="67"/>
        <source>Security</source>
        <translation type="unfinished">Güvenlik</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="70"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="73"/>
        <source>Investigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="76"/>
        <source>Commoninfo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation type="vanished">Mesajlar</translation>
    </message>
</context>
<context>
    <name>KeyboardControl</name>
    <message>
        <source>Keys Settings</source>
        <translation type="vanished">Anahtar Ayarları</translation>
    </message>
    <message>
        <source>Enable repeat key</source>
        <translation type="vanished">Yenileme tuşunu etkinleştir</translation>
        <extra-contents_path>/keyboard/Enable repeat key</extra-contents_path>
    </message>
    <message>
        <source>Delay</source>
        <translation type="vanished">Gecikme</translation>
        <extra-contents_path>/keyboard/Delay</extra-contents_path>
    </message>
    <message>
        <source>Short</source>
        <translation type="vanished">Kısa</translation>
    </message>
    <message>
        <source>Long</source>
        <translation type="vanished">Uzun</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="vanished">Hız</translation>
        <extra-contents_path>/keyboard/Speed</extra-contents_path>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Yavaş</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Hızlı</translation>
    </message>
    <message>
        <source>Input characters to test the repetition effect：</source>
        <translation type="obsolete">Tekrarlama etkisini test etmek için karakterleri girin: </translation>
    </message>
    <message>
        <source>Input characters to test the repetition effect:</source>
        <translation type="vanished">Tekrarlama etkisini test etmek için karakterleri girin:</translation>
        <extra-contents_path>/keyboard/Input characters to test the repetition effect:</extra-contents_path>
    </message>
    <message>
        <source>Tip of keyboard</source>
        <translation type="vanished">Klavye İpucu</translation>
        <extra-contents_path>/keyboard/Tip of keyboard</extra-contents_path>
    </message>
    <message>
        <source>reset default layout</source>
        <translation type="vanished">Varsayılan Düzene Dön</translation>
        <extra-contents_path>/keyboard/reset default layout</extra-contents_path>
    </message>
    <message>
        <source>Reset layout</source>
        <translation type="vanished">Düzeni Sıfırla</translation>
    </message>
    <message>
        <source>Message of capslock</source>
        <translation type="vanished">Capslock mesajı</translation>
    </message>
    <message>
        <source>Enable numlock</source>
        <translation type="vanished">Numlock Etkinleştir</translation>
    </message>
    <message>
        <source>Keyboard Layout</source>
        <translation type="vanished">Klavye Düzeni</translation>
    </message>
    <message>
        <source>Keyboard layout</source>
        <translation type="vanished">Klavye Düzeni</translation>
        <extra-contents_path>/keyboard/Keyboard layout</extra-contents_path>
    </message>
    <message>
        <source>Install layouts</source>
        <translation type="vanished">Klavye kur</translation>
    </message>
    <message>
        <source>keyboard</source>
        <translation type="vanished">Klavye</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">Klavye</translation>
    </message>
</context>
<context>
    <name>KeyboardMain</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="25"/>
        <source>Keyboard</source>
        <translation type="unfinished">Klavye</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="102"/>
        <source>Keyboard settings</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Keyboard/Keyboard settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="114"/>
        <source>Input settings</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Keyboard/Input settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="154"/>
        <source>Key repeat</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Keyboard/Key repeat</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="177"/>
        <source>Delay</source>
        <translation type="unfinished">Gecikme</translation>
        <extra-contents_path>/Keyboard/Delay</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="180"/>
        <source>Short</source>
        <translation type="unfinished">Kısa</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="183"/>
        <source>Long</source>
        <translation type="unfinished">Uzun</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="214"/>
        <source>Speed</source>
        <translation type="unfinished">Hız</translation>
        <extra-contents_path>/Keyboard/Speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="217"/>
        <source>Slow</source>
        <translation type="unfinished">Yavaş</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="220"/>
        <source>Fast</source>
        <translation type="unfinished">Hızlı</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="251"/>
        <source>Input test</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Keyboard/Input test</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="272"/>
        <source>Key tips</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Keyboard/Key tips</extra-contents_path>
    </message>
</context>
<context>
    <name>KeyboardPainter</name>
    <message>
        <source>Close</source>
        <translation type="obsolete">Kapat</translation>
    </message>
    <message>
        <source>Keyboard layout levels</source>
        <translation type="obsolete">Klavye düzeni seviyeleri</translation>
    </message>
    <message>
        <source>Level %1, %2</source>
        <translation type="obsolete">Seviye %1, %2</translation>
    </message>
</context>
<context>
    <name>LanguageFrame</name>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="77"/>
        <source>Input Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="78"/>
        <source>Delete</source>
        <translation type="unfinished">Sil</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="121"/>
        <source>Manager Keyboard Layout</source>
        <translation>Yönetici Klavye Düzeni</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="234"/>
        <source>Language</source>
        <translation>Dil</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="250"/>
        <source>Country</source>
        <translation>Ülke</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="293"/>
        <source>Variant</source>
        <translation>Düzen</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="351"/>
        <source>Layout installed</source>
        <translation>Düzen yüklendi</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="399"/>
        <source>Preview</source>
        <translation>Önizleme</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="431"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="450"/>
        <source>Install</source>
        <translation>Kur</translation>
    </message>
</context>
<context>
    <name>LoginDialog</name>
    <message>
        <source>Forget</source>
        <translation type="vanished">Unut</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Gönder</translation>
    </message>
    <message>
        <source>User Sign in</source>
        <translation type="vanished">Kullanıcı Girişi</translation>
    </message>
    <message>
        <source>Quick Sign in</source>
        <translation type="vanished">Hızlı Giriş</translation>
    </message>
    <message>
        <source>Your account/phone here</source>
        <translation type="obsolete">Hesabınız/telefonunuz burada</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">Kodunuz Burada</translation>
    </message>
    <message>
        <source>Your phone number here</source>
        <translation type="vanished">Telefon Numaranız Burada</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">Parolanız Burada</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">Kodunuz Burada</translation>
    </message>
</context>
<context>
    <name>MCodeWidget</name>
    <message>
        <source>SongTi</source>
        <translation type="vanished">SongTi</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Sign in</source>
        <translation type="vanished">Oturum Aç</translation>
    </message>
    <message>
        <source>Sign up</source>
        <translation type="vanished">Kayıt Ol</translation>
    </message>
    <message>
        <source>Login in progress</source>
        <translation type="vanished">Giriş devam ediyor</translation>
    </message>
    <message>
        <source>Error code:</source>
        <translation type="vanished">Hata kodu:</translation>
    </message>
    <message>
        <source>!</source>
        <translation type="vanished">!</translation>
    </message>
    <message>
        <source>Internal error occurring!</source>
        <translation type="vanished">Dahili hata oluştu!</translation>
    </message>
    <message>
        <source>Internal error occurred!</source>
        <translation type="vanished">Dahili hata oluştu!</translation>
    </message>
    <message>
        <source>Failed to sign up!</source>
        <translation type="vanished">Oturum açma hatalı!</translation>
    </message>
    <message>
        <source>Failed attempt to return value!</source>
        <translation type="vanished">Değer döndürme denemesi başarısız oldu!</translation>
    </message>
    <message>
        <source>Check your connection!</source>
        <translation type="vanished">Bağlantınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Failed to get by phone!</source>
        <translation type="vanished">Telefonla ulaşılamadı!</translation>
    </message>
    <message>
        <source>Failed to get by user!</source>
        <translation type="vanished">Kullanıcı tarafından alınamadı!</translation>
    </message>
    <message>
        <source>Failed to reset password!</source>
        <translation type="vanished">Şifre sıfırlanamadı!</translation>
    </message>
    <message>
        <source>Timeout!</source>
        <translation type="vanished">Zaman aşımı!</translation>
    </message>
    <message>
        <source>Phone binding falied!</source>
        <translation type="vanished">Telefon bağlantısı hatalı!</translation>
    </message>
    <message>
        <source>Please check your information!</source>
        <translation type="vanished">Lütfen bilginizi kontrol edin!</translation>
    </message>
    <message>
        <source>Please check your account!</source>
        <translation type="vanished">Lütfen hesabınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Failed due to server error!</source>
        <translation type="vanished">Sunucu hatası nedeniyle başarısız oldu!</translation>
    </message>
    <message>
        <source>User existing!</source>
        <translation type="vanished">Kullanıcı var!</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">Telefon numarası zaten kullanılıyor!</translation>
    </message>
    <message>
        <source>Please check your format!</source>
        <translation type="vanished">Lütfen formatınızı kontrol edin!</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation type="vanished">Sınıra ulaştınız!</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">Lütfen telefon numaranızı kontrol edin!</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation type="vanished">Lütfen kodunuzu kontrol edin!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">Hesap mevcut değil!</translation>
    </message>
    <message>
        <source>User has bound the phone!</source>
        <translation type="vanished">Kullanıcı telefonu bağladı!</translation>
    </message>
    <message>
        <source>Sending code error occurred!</source>
        <translation type="vanished">Kod gönderme hatası oluştu!</translation>
    </message>
    <message>
        <source>Sending code error occurring!</source>
        <translation type="vanished">Gönderme kodu hatası oluştu!</translation>
    </message>
    <message>
        <source>Your code is wrong!</source>
        <translation type="vanished">Kodunuz yanlış!</translation>
    </message>
    <message>
        <source>Please check your phone!</source>
        <translation type="obsolete">Lütfen telefonunuzu kontrol edin!</translation>
    </message>
    <message>
        <source>Please check your password!</source>
        <translation type="vanished">Lütfen şifrenizi kontrol edin!</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">En az 6 karakter, harf ve rakam ekleyin</translation>
    </message>
    <message>
        <source>Sign in Cloud</source>
        <translation type="vanished">Giriş yap Bulut</translation>
    </message>
    <message>
        <source>Forget</source>
        <translation type="vanished">Unut</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">Ayarla</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="vanished">Geri</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation type="vanished">Hesap Oluştur</translation>
    </message>
    <message>
        <source>Sign up now</source>
        <translation type="vanished">Şimdi kayıt ol</translation>
    </message>
    <message>
        <source>Please confirm your password!</source>
        <translation type="vanished">Lütfen şifrenizi doğrulayınız!</translation>
    </message>
    <message>
        <source>Resend ( %1 )</source>
        <translation type="vanished">Yeniden gönder (%1)</translation>
    </message>
    <message>
        <source>Get</source>
        <translation type="obsolete">Al</translation>
    </message>
    <message>
        <source>Get phone code</source>
        <translation type="vanished">Telefon kodunu alın</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="vanished">Gönder</translation>
    </message>
    <message>
        <source>Binding Phone</source>
        <translation type="vanished">Telefon Bağlanıyor</translation>
    </message>
    <message>
        <source>Please make sure your password is safety!</source>
        <translation type="obsolete">Lütfen şifrenizin güvenli olduğundan emin olun!</translation>
    </message>
    <message>
        <source>Bind now</source>
        <translation type="vanished">Şimdi bağla</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">Bağlantı kesildi</translation>
    </message>
    <message>
        <source>Your account：%1</source>
        <translation type="vanished">Hesabınız: % 1</translation>
    </message>
    <message>
        <source>Unauthorized device or OSS falied.
Please retry for login!</source>
        <translation type="obsolete">Yetkisiz cihaz veya işletim sistemi başarısız oldu.
Lütfen giriş için tekrar deneyin!</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Çıkış</translation>
    </message>
    <message>
        <source>Sync</source>
        <translation type="vanished">Senk.</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation type="vanished">Giriş Yap</translation>
        <extra-contents_path>/networkaccount/Sign in</extra-contents_path>
    </message>
    <message>
        <source>Stop sync</source>
        <translation type="vanished">Senk. durdur</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">Ayarlarınızı senkronize edin</translation>
    </message>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">Hesabınız: %1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">Otomatik Senk.</translation>
    </message>
    <message>
        <source>Synchronize your personalized settings and data</source>
        <translation type="vanished">Kişiselleştirilmiş ayarlarınızı ve verilerinizi senkronize edin</translation>
    </message>
    <message>
        <source>The latest time sync is: </source>
        <translation type="obsolete">En son zaman senkronizasyonu: </translation>
    </message>
    <message>
        <source>This operation may cover your settings!</source>
        <translation type="vanished">Bu işlem ayarlarınızı kapsayabilir!</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">Cloud ID masaüstü mesajı</translation>
    </message>
    <message>
        <source>Synchronize your computer&apos;s settings into your cloud account here.</source>
        <translation type="vanished">Bilgisayarınızın ayarlarını burada bulut hesabınızla senkronize edin.</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">Medya</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">Hava Durumu</translation>
    </message>
    <message>
        <source>Sync downloading,please wait!</source>
        <translation type="vanished">Senkronizasyon indiriliyor, lütfen bekleyin!</translation>
    </message>
    <message>
        <source>Sync uploading,please wait!</source>
        <translation type="vanished">Yüklemeyi senkronize edin, lütfen bekleyin!</translation>
    </message>
    <message>
        <source>Sync failed, please check your internet connection or login out to retry!</source>
        <translation type="vanished">Senkronizasyon başarısız oldu, lütfen internet bağlantınızı kontrol edin veya yeniden denemek için oturumu kapatın!</translation>
    </message>
    <message>
        <source>%1,</source>
        <translation type="vanished">%1,</translation>
    </message>
    <message>
        <source>Synchronized failed: %1 please retry or login out to get a better experience.</source>
        <translation type="vanished">Senkronize edilemedi:%1 daha iyi bir deneyim için lütfen tekrar deneyin veya oturumu kapatın.</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
    <message>
        <source>Synchronized failed: %1, please retry or login out to get a better experience.</source>
        <translation type="vanished">Senkronize edilemedi:%1, daha iyi bir deneyim için lütfen tekrar deneyin veya oturumu kapatın.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
    <message>
        <source>UKCC</source>
        <translation type="vanished">Denetim Merkezi</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="410"/>
        <location filename="../../mainwindow.cpp" line="431"/>
        <location filename="../../mainwindow.cpp" line="550"/>
        <location filename="../../mainwindow.cpp" line="1049"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="425"/>
        <source>Back home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="451"/>
        <source>Option</source>
        <translation type="unfinished">Seçenek</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="452"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <source>Warnning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="192"/>
        <source>Restore</source>
        <translation type="unfinished">Geri Yükle</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="195"/>
        <location filename="../../mainwindow.cpp" line="453"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="obsolete">Menü</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="454"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="538"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="540"/>
        <source>About</source>
        <translation type="unfinished">Hakkında</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="542"/>
        <source>Exit</source>
        <translation type="unfinished">Çıkış</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="687"/>
        <source>Specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ukcc</source>
        <translation type="vanished">Denetim Merkezi</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>Warning</source>
        <translation type="unfinished">Uyarı</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>This function has been controlled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HOME</source>
        <translation type="vanished">GİRİŞ</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
</context>
<context>
    <name>MessageBoxDialog</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <source>Open</source>
        <translation type="obsolete">Aç</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
</context>
<context>
    <name>MouseControl</name>
    <message>
        <source>Mouse Key Settings</source>
        <translation type="vanished">Fare Ayarları</translation>
    </message>
    <message>
        <source>Hand habit</source>
        <translation type="vanished">Kullanılan El</translation>
        <extra-contents_path>/mouse/Hand habit</extra-contents_path>
    </message>
    <message>
        <source>Pointer Settings</source>
        <translation type="vanished">İşaretçi Ayarları</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation type="vanished">Hız</translation>
        <extra-contents_path>/mouse/Speed</extra-contents_path>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">Yavaş</translation>
    </message>
    <message>
        <source>mouse wheel speed</source>
        <translation type="vanished">Fare Hızı</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">Hızlı</translation>
    </message>
    <message>
        <source>Doubleclick  delay</source>
        <translation type="vanished">Çift Tıklama Gecikmesi</translation>
        <extra-contents_path>/mouse/Doubleclick  delay</extra-contents_path>
    </message>
    <message>
        <source>Short</source>
        <translation type="vanished">Kısa</translation>
    </message>
    <message>
        <source>Long</source>
        <translation type="vanished">Uzun</translation>
    </message>
    <message>
        <source>Sensitivity</source>
        <translation type="vanished">Hassaslık</translation>
        <extra-contents_path>/mouse/Sensitivity</extra-contents_path>
    </message>
    <message>
        <source>Low</source>
        <translation type="vanished">Düşük</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="vanished">Yüksek</translation>
    </message>
    <message>
        <source>Visibility</source>
        <translation type="vanished">Görünürlük</translation>
        <extra-contents_path>/mouse/Visibility</extra-contents_path>
    </message>
    <message>
        <source>Pointer size</source>
        <translation type="vanished">İşaretçi Boyutu</translation>
        <extra-contents_path>/mouse/Pointer size</extra-contents_path>
    </message>
    <message>
        <source>Cursor Settings</source>
        <translation type="vanished">İmleç Ayarları</translation>
    </message>
    <message>
        <source>Cursor weight</source>
        <translation type="vanished">İmleç Genişliği</translation>
    </message>
    <message>
        <source> Cursor weight</source>
        <translation type="obsolete">İmleç ağırlığı</translation>
    </message>
    <message>
        <source>Thin</source>
        <translation type="vanished">İnce</translation>
    </message>
    <message>
        <source>Coarse</source>
        <translation type="vanished">Kalın</translation>
    </message>
    <message>
        <source>Cursor speed</source>
        <translation type="vanished">İmleç Hızı</translation>
        <extra-contents_path>/mouse/Cursor speed</extra-contents_path>
    </message>
    <message>
        <source>Enable flashing on text area</source>
        <translation type="vanished">Metin alanında yanıp sönmeyi etkinleştir</translation>
        <extra-contents_path>/mouse/Enable flashing on text area</extra-contents_path>
    </message>
    <message>
        <source>mouse</source>
        <translation type="vanished">Fare</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">Fare</translation>
    </message>
    <message>
        <source>Lefthand</source>
        <translation type="vanished">Sol el</translation>
    </message>
    <message>
        <source>Righthand</source>
        <translation type="vanished">Sağ el</translation>
    </message>
    <message>
        <source>Default(Recommended)</source>
        <translation type="vanished">Standart (Önerilen)</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="vanished">Orta</translation>
    </message>
    <message>
        <source>Large</source>
        <translation type="vanished">Büyük</translation>
    </message>
</context>
<context>
    <name>MouseUI</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="25"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="194"/>
        <source>Mouse</source>
        <translation type="unfinished">Fare</translation>
        <extra-contents_path>/Mouse/Mouse</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="203"/>
        <source>Pointer</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Pointer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="212"/>
        <source>Cursor</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Cursor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="275"/>
        <source>Dominant hand</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Dominant hand</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="278"/>
        <source>Left key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="279"/>
        <source>Right key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="307"/>
        <source>Scroll direction</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Scroll direction</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="310"/>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="311"/>
        <source>Reverse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="340"/>
        <source>Wheel speed</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Wheel speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="343"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="417"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="562"/>
        <source>Slow</source>
        <translation type="unfinished">Yavaş</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="352"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="427"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="572"/>
        <source>Fast</source>
        <translation type="unfinished">Hızlı</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="376"/>
        <source>Double-click interval time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Double-click interval time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="379"/>
        <source>Short</source>
        <translation type="unfinished">Kısa</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="389"/>
        <source>Long</source>
        <translation type="unfinished">Uzun</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="414"/>
        <source>Pointer speed</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Pointer speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="452"/>
        <source>Mouse acceleration</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Mouse acceleration</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="474"/>
        <source>Show pointer position when pressing ctrl</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Show pointer position when pressing ctrl</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="497"/>
        <source>Pointer size</source>
        <translation type="unfinished">İşaretçi Boyutu</translation>
        <extra-contents_path>/Mouse/Pointer size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="500"/>
        <source>Small(recommend)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="501"/>
        <source>Medium</source>
        <translation type="unfinished">Orta</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="502"/>
        <source>Large</source>
        <translation type="unfinished">Büyük</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="536"/>
        <source>Blinking cursor in text area</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Mouse/Blinking cursor in text area</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="559"/>
        <source>Cursor speed</source>
        <translation type="unfinished">İmleç Hızı</translation>
        <extra-contents_path>/Mouse/Cursor speed</extra-contents_path>
    </message>
</context>
<context>
    <name>MyLabel</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="130"/>
        <source>double-click to test</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <source>Netconnect Status</source>
        <translation type="vanished">Bağlantı Durumu</translation>
        <extra-contents_path>/netconnect/Netconnect Status</extra-contents_path>
    </message>
    <message>
        <source>Waitting...</source>
        <translation type="vanished">Bekleniyor...</translation>
    </message>
    <message>
        <source>Available Network</source>
        <translation type="vanished">Uygun Ağ</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Yenile</translation>
    </message>
    <message>
        <source>open wifi</source>
        <translation type="vanished">Wifi aç</translation>
        <extra-contents_path>/netconnect/open wifi</extra-contents_path>
    </message>
    <message>
        <source>Advanced settings</source>
        <translation type="obsolete">Gelişmiş Ayarlar</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>Network settings</source>
        <translation type="vanished">Ağ Ayarları</translation>
        <extra-contents_path>/netconnect/Network settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>Change net settings</source>
        <translation type="vanished">Bağlantı ayarlarını değiştir</translation>
    </message>
    <message>
        <source>netconnect</source>
        <translation type="vanished">Ağ bağlantısı</translation>
    </message>
    <message>
        <source>Netconnect</source>
        <translation type="vanished">Bağlantı</translation>
    </message>
    <message>
        <source>Refreshing...</source>
        <translation type="obsolete">Yenileniyor...</translation>
    </message>
    <message>
        <source>connected</source>
        <translation type="vanished">Bağlandı</translation>
    </message>
    <message>
        <source>No network</source>
        <translation type="vanished">Bağlantı yok</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Bağlan</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">Bağlantıyı kes</translation>
    </message>
</context>
<context>
    <name>Notice</name>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="162"/>
        <source>NotFaze Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="164"/>
        <source>(Notification banners, prompts will be hidden, and notification sounds will be muted)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="238"/>
        <source>Automatically turn on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="263"/>
        <source>to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="288"/>
        <source>Automatically turn on when multiple screens are connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="293"/>
        <source>Automatically open in full screen mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="298"/>
        <source>Allow automatic alarm reminders in Do Not Disturb mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="329"/>
        <source>Notice Settings</source>
        <translation>Bildirim Ayarları</translation>
        <extra-contents_path>/Notice/Notice Settings</extra-contents_path>
    </message>
    <message>
        <source>Set the type of notice in the operation center</source>
        <translation type="vanished">İşlem Merkezinde Bildirim Türünü Ayarla</translation>
        <extra-contents_path>/notice/Set the type of notice in the operation center</extra-contents_path>
    </message>
    <message>
        <source>Show new feature ater system upgrade</source>
        <translation type="vanished">Sistem yükseltmesinden sonra yeni özelliği göster</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="331"/>
        <source>Get notifications from the app</source>
        <translation>Uygulamalardan Bildirim Al</translation>
        <extra-contents_path>/Notice/Get notifications from the app</extra-contents_path>
    </message>
    <message>
        <source>Show notifications on the lock screen</source>
        <translation type="vanished">Kilit ekranında bildirimleri göster</translation>
    </message>
    <message>
        <source>Notice Origin</source>
        <translation type="vanished">Bildirim Kaynağı</translation>
    </message>
    <message>
        <source>notice</source>
        <translation type="vanished">Bildirim</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="51"/>
        <source>Notice</source>
        <translation>Bildirim</translation>
    </message>
</context>
<context>
    <name>NoticeMenu</name>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="41"/>
        <source>Beep sound when notified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="47"/>
        <source>Show message  on screenlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="53"/>
        <source>Show noticfication  on screenlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="57"/>
        <source>Notification Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="65"/>
        <source>Banner: Appears in the upper right corner of the screen, and disappears automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="70"/>
        <source>Tip:It will be kept on the screen until it is closed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="75"/>
        <source>None:Notifications will not be displayed on the screen, but will go to the notification center</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="120"/>
        <source>resolution</source>
        <translation>Çözünürlük:</translation>
        <extra-contents_path>/Display/resolution</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="156"/>
        <source>orientation</source>
        <translation>Yönlendirme:</translation>
        <extra-contents_path>/Display/orientation</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="174"/>
        <source>arrow-up</source>
        <translation>Yukarı</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="175"/>
        <source>90° arrow-right</source>
        <translation>90° Sağa</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="177"/>
        <source>arrow-down</source>
        <translation>Aşağı</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="193"/>
        <source>frequency</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Display/frequency</extra-contents_path>
    </message>
    <message>
        <source>Some applications need to be logouted to take effect</source>
        <translation type="obsolete">Bazı uygulamaların aktif olması için oturum açılması gerekir</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="176"/>
        <source>90° arrow-left</source>
        <translation>90° sola</translation>
    </message>
    <message>
        <source>refresh rate</source>
        <translation type="vanished">Yenileme Hızı:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="394"/>
        <source>auto</source>
        <translation>Otomatik</translation>
    </message>
    <message>
        <source>100%</source>
        <translation type="vanished">%100</translation>
    </message>
    <message>
        <source>200%</source>
        <translation type="vanished">%200</translation>
    </message>
    <message>
        <source>300%</source>
        <translation type="vanished">%300</translation>
    </message>
    <message>
        <source>screen zoom</source>
        <translation type="vanished">Ekran Yakınlaştırma:</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="513"/>
        <source>%1 Hz</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PassDialog</name>
    <message>
        <source>Get the phone binding code</source>
        <translation type="vanished">Telefon bağlama kodunu alın</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">Hesabınız burada</translation>
    </message>
    <message>
        <source>Your new password here</source>
        <translation type="vanished">Parolanız burada</translation>
    </message>
    <message>
        <source>Confirm your new password</source>
        <translation type="vanished">Yeni şifrenizi onaylayın</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">Kodunuz burada</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">En az 6 karakter, harf ve rakam ekleyin</translation>
    </message>
    <message>
        <source>Your password is valid!</source>
        <translation type="vanished">Parolanız geçerlidir!</translation>
    </message>
</context>
<context>
    <name>PhoneAuthIntelDialog</name>
    <message>
        <source>Confirm</source>
        <translation type="obsolete">Onayla</translation>
    </message>
</context>
<context>
    <name>Power</name>
    <message>
        <source>select power plan</source>
        <translation type="vanished">Güç Yönetimini Ayarla</translation>
    </message>
    <message>
        <source>Balance (suggest)</source>
        <translation type="vanished">Dengeli (Önerilen)</translation>
    </message>
    <message>
        <source>Saving</source>
        <translation type="vanished">Kaydediliyor</translation>
    </message>
    <message>
        <source>Minimize performance</source>
        <translation type="vanished">Performansı en aza indir</translation>
    </message>
    <message>
        <source>Bala&amp;nce (suggest)</source>
        <translation type="obsolete">Denge (önerilen)
</translation>
    </message>
    <message>
        <source>Autobalance energy and performance with available hardware</source>
        <translation type="vanished">Mevcut donanım ile enerji ve performansı otomatik olarak dengeleme</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="vanished">Özel</translation>
        <extra-contents_path>/power/Custom</extra-contents_path>
    </message>
    <message>
        <source>Users develop personalized power plans</source>
        <translation type="vanished">Kullanıcılar kişiselleştirilmiş güç planları geliştirir</translation>
    </message>
    <message>
        <source>Power supply</source>
        <translation type="vanished">A/C Gücünde</translation>
    </message>
    <message>
        <source>Battery powered</source>
        <translation type="vanished">Pil Gücünde</translation>
    </message>
    <message>
        <source>Change PC sleep time:</source>
        <translation type="obsolete">PC uyku süresini değiştirin:</translation>
    </message>
    <message>
        <source>Change DP close time:</source>
        <translation type="obsolete">DP kapanış zamanını değiştirin:</translation>
    </message>
    <message>
        <source>Change pc sleep time:</source>
        <translation type="vanished">Uyku Moduna Al:</translation>
    </message>
    <message>
        <source>Change dp close time:</source>
        <translation type="vanished">Bilgisayarı Kapat:</translation>
    </message>
    <message>
        <source>When close lid:</source>
        <translation type="vanished">Kapak Kapandığında:</translation>
    </message>
    <message>
        <source>Screen darkens use battery:</source>
        <translation type="vanished">Pilde Ekranı Kapatma Süresi:</translation>
    </message>
    <message>
        <source>Power Other Settings</source>
        <translation type="vanished">Diğer Güç Ayarları</translation>
    </message>
    <message>
        <source>S3 to S4 when:</source>
        <translation type="vanished">S3 ile S4 durumlarında:</translation>
    </message>
    <message>
        <source>Power Icon Settings</source>
        <translation type="vanished">Güç Simgesi Ayarları</translation>
    </message>
    <message>
        <source>Power icon:</source>
        <translation type="vanished">Güç Simgesi:</translation>
    </message>
    <message>
        <source>power</source>
        <translation type="vanished">Güç</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="62"/>
        <source>Power</source>
        <translation>Güç</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="254"/>
        <source>The system will sleep before turning off the display</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="326"/>
        <source>Reduce the occupation of backend running program resources and ensure smooth operation of key and focus applications.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="606"/>
        <source>Require password when suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="605"/>
        <source>Require password when suspend/hibernate</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Require password when suspend/hibernate</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="607"/>
        <source>Require password when hibernate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>never</source>
        <translation>Asla</translation>
    </message>
    <message>
        <source>10 min</source>
        <translation type="vanished">10 dk</translation>
    </message>
    <message>
        <source>30 min</source>
        <translation type="vanished">30 dk</translation>
    </message>
    <message>
        <source>60 min</source>
        <translation type="vanished">60 dk</translation>
    </message>
    <message>
        <source>120 min</source>
        <translation type="vanished">120 dk</translation>
    </message>
    <message>
        <source>300 min</source>
        <translation type="vanished">300 dk</translation>
    </message>
    <message>
        <source>20 min</source>
        <translation type="vanished">20 dk</translation>
    </message>
    <message>
        <source>1 min</source>
        <translation type="vanished">1 dk</translation>
    </message>
    <message>
        <source>5 min</source>
        <translation type="vanished">5 dk</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="616"/>
        <location filename="../../../plugins/system/power/power.cpp" line="617"/>
        <source>Password required when waking up the screen</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Password required when waking up the screen</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="621"/>
        <source>Press the power button</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Press the power button</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="626"/>
        <location filename="../../../plugins/system/power/power.cpp" line="627"/>
        <source>Time to close display</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Time to close display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="631"/>
        <location filename="../../../plugins/system/power/power.cpp" line="632"/>
        <source>Time to sleep</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Time to sleep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="636"/>
        <location filename="../../../plugins/system/power/power.cpp" line="637"/>
        <source>Notebook cover</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Notebook cover</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="641"/>
        <location filename="../../../plugins/system/power/power.cpp" line="642"/>
        <source>Dynamic resource scheduling</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Dynamic resource scheduling</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="646"/>
        <location filename="../../../plugins/system/power/power.cpp" line="647"/>
        <source>Using power</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Using power</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="651"/>
        <location filename="../../../plugins/system/power/power.cpp" line="652"/>
        <source>Using battery</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Using battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="655"/>
        <location filename="../../../plugins/system/power/power.cpp" line="656"/>
        <source> Time to darken</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="660"/>
        <source>Battery level is lower than</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="663"/>
        <source>Run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="666"/>
        <location filename="../../../plugins/system/power/power.cpp" line="667"/>
        <source>Low battery notification</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Low battery notification</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="671"/>
        <source>Automatically run saving mode when low battery</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/&quot;Automatically run saving mode when low battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="672"/>
        <source>Automatically run saving mode when the low battery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="675"/>
        <location filename="../../../plugins/system/power/power.cpp" line="676"/>
        <source>Automatically run saving mode when using battery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="679"/>
        <location filename="../../../plugins/system/power/power.cpp" line="680"/>
        <source>Display remaining charging time and usage time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="164"/>
        <source>General</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/General</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="349"/>
        <source>Select Powerplan</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Select Powerplan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="407"/>
        <source>Battery saving plan</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Power/Battery saving plan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>nothing</source>
        <translation>Hiçbiri</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>blank</source>
        <translation>Boş</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>suspend</source>
        <translation>Askıya Al</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>hibernate</source>
        <translation>Beklemeye Al</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <source>interactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <source>5min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>10min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>15min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>30min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>1h</source>
        <translation type="unfinished">1 sa</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>2h</source>
        <translation type="unfinished">3 sa {2h?}</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>3h</source>
        <translation type="unfinished">3 sa</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Balance</source>
        <translation type="unfinished">Denge</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Energy Efficiency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Performance</source>
        <translation type="unfinished">Performans</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>shutdown</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <source>always</source>
        <translation type="vanished">Her Zaman</translation>
    </message>
    <message>
        <source>present</source>
        <translation type="vanished">Mevcut</translation>
    </message>
    <message>
        <source>charge</source>
        <translation type="vanished">Şarj</translation>
    </message>
</context>
<context>
    <name>Printer</name>
    <message>
        <source>Add Printers And Scanners</source>
        <translation type="vanished">Yazıcı ve Tarayıcı Ekle</translation>
        <extra-contents_path>/printer/Add Printers And Scanners</extra-contents_path>
    </message>
    <message>
        <source>Add printers and scanners</source>
        <translation type="vanished">Yazıcı Ve Tarayıcı Ekle</translation>
    </message>
    <message>
        <source>List Of Existing Printers</source>
        <translation type="vanished">Mevcut Yazıcıların Listesi</translation>
    </message>
    <message>
        <source>printer</source>
        <translation type="vanished">Yazıcı</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="43"/>
        <source>Printer</source>
        <translation>Yazıcı</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="133"/>
        <source>Printers</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Printer/Printers</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="179"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
        <extra-contents_path>/Printer/Add</extra-contents_path>
    </message>
</context>
<context>
    <name>PrivacyDialog</name>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="11"/>
        <source>Set</source>
        <translation type="unfinished">Ayarla</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="26"/>
        <source>End User License Agreement and Privacy Policy Statement of Kylin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="31"/>
        <source>Dear users of Kylin operating system and relevant products, 
         This agreement describes your rights, obligations and prerequisites for your use of this product. Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”).
        “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products.

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021
        The Agreement shall include the following content:
        I.     User license 
        II.    Java technology limitations
        III.   Cookies and other technologies
        IV.    Intellectual property clause
        V.     Open source code
        VI.   The third-party software/services
        VII.  Escape clause
        VIII. Integrity and severability of the Agreement
        IX.    Applicable law and dispute settlement

        I.      User license
        According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
        1.     User license for educational institutions
        In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product.
        2.     Use of the font software
        Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

        II.    Java technology limitations
        You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

        III.   Cookies and other technologies
        In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
        If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers.
        In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

        IV.   Intellectual property clause
        1.    Trademarks and Logos
        This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
        2.    Duplication, modification and distribution
        If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
        Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws.

        V.    Open source code
        For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

        VI.  The third-party software/services
        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
        We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

        VII. Escape clause
        1.    Limited warranty
        We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
        2.   Disclaimer
        In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
        3.   Limitation of responsibility
        To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

        VIII.Integrity and severability of the Agreement
        1.    The integrity of the Agreement
        The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
        2.   Severability of the Agreement
        If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

        IX.  Applicable law and dispute settlement
        1.   Application of governing laws
        Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
        2.  Termination
        If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
        The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.
        The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

        Privacy Policy Statement of Kylin Operating System/n        Release date of the version: July 30, 2021
        Effective date of the version: July 30, 2021

        We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.

        The Statement shall include the following content:
        I.   Collection and use your personal information
        II.  How to store and protect your personal information
        III. How to manage your personal information
        IV.  Privacy of the third-party software/services
        V.   Minors’ use of the products
        VI.  How to update this Statement
        VII. How to contact us

        I.     How to collect and use your personal information
        1.    The collection of personal information
        We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
        1)   The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
        2)   Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
        3)   Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
        4)   Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
        5)   The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
        6)   This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
        7)   The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
        8)   In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.
        2.   Use of personal information
        We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
        1)   The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
        2)   We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
        3)   We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
        4)   We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
        5)   We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
        6)   We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications andother channels;
        7)   In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.
        3.   Information sharing and provision
        We shall not share or transfer your personal information to any third party, except for the following circumstances:
        1)   After obtaining your clear consent, we shall share your personal information with the third parities;
        2)   In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
        3)   We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
        4)   With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
        5)   If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.
        4.   Exceptions with authorized consent
        1)   It is directly related to national security, national defense security and other national interests; 
        2)   It is directly related to public safety, public health and public knowledge and other major public interests; 
        3)   It is directly related to crime investigation, prosecution, judgment and execution of judgment; 
        4)   It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent; 
        5)   The personal information collected is disclosed to the public by yourself; 
        6)   Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels; 
        7)   It is necessary to sign and perform of the contract according to your requirement; 
        8)   It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
        9)   It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
        10) Other circumstances specified in the laws and regulations.

        II.   How to store and protect personal information
        1.   Information storage place
        We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
        2.   Information storage duration 
        Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
        When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
        3.   How to protect the information
        We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
        We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
        We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
        4.   Emergency response plan
        In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

        III. How to manage your personal information
        If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs. 
        Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

        IV.  Privacy of the third-party software/services

        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
        When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

        V.   Minors’ use of the products
        If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

        VI.  How to update this Statement
        We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the accountcreated by you in this product; if you are a guardian, please help your minor child to close the account created by him/her in this product.

        VII. How to contact us
        If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn). 
        We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
        The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
        Last date of updating: November 1, 2021

Address:
        Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
        Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
        Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
        Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.:
        Tianjin (022) 58955650      Beijing (010) 51659955
        Changsha (0731) 88280170        Shanghai (021) 51098866
Fax:
        Tianjin (022) 58955651      Beijing (010) 62800607
        Changsha (0731) 88280166        Shanghai (021) 51062866

        Company website: www.kylinos.cn
        E-mail: support@kylinos.cn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="298"/>
        <source>Kylinsoft Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Proxy</name>
    <message>
        <source>Auto Proxy</source>
        <translation type="vanished">Otomatik</translation>
        <extra-contents_path>/Proxy/Auto Proxy</extra-contents_path>
    </message>
    <message>
        <source>Auto proxy</source>
        <translation type="vanished">Otomatik</translation>
        <extra-contents_path>/proxy/Auto proxy</extra-contents_path>
    </message>
    <message>
        <source>Auto url</source>
        <translation type="vanished">Oto URL</translation>
        <extra-contents_path>/Proxy/Auto url</extra-contents_path>
    </message>
    <message>
        <source>Manual Proxy</source>
        <translation type="vanished">Elle</translation>
        <extra-contents_path>/Proxy/Manual Proxy</extra-contents_path>
    </message>
    <message>
        <source>Manual proxy</source>
        <translation type="vanished">Elle</translation>
        <extra-contents_path>/proxy/Manual proxy</extra-contents_path>
    </message>
    <message>
        <source>Http Proxy</source>
        <translation type="vanished">HTTP Proxy</translation>
        <extra-contents_path>/Proxy/Http Proxy</extra-contents_path>
    </message>
    <message>
        <source>Port</source>
        <translation type="vanished">Port</translation>
    </message>
    <message>
        <source>Cetification</source>
        <translation type="vanished">Sertifika</translation>
    </message>
    <message>
        <source>Https Proxy</source>
        <translation type="vanished">HTTPS Proxy</translation>
        <extra-contents_path>/Proxy/Https Proxy</extra-contents_path>
    </message>
    <message>
        <source>Ftp Proxy</source>
        <translation type="vanished">Ftp Proxy</translation>
        <extra-contents_path>/Proxy/Ftp Proxy</extra-contents_path>
    </message>
    <message>
        <source>Socks Proxy</source>
        <translation type="vanished">Socks Proxy</translation>
        <extra-contents_path>/Proxy/Socks Proxy</extra-contents_path>
    </message>
    <message>
        <source>List of ignored hosts. more than one entry, please separate with english semicolon(;)</source>
        <translation type="vanished">Yok sayılan ana bilgisayarların listesi. birden fazla giriş için (;) ile ayırın</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">Aç</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Düzenle</translation>
    </message>
    <message>
        <source>proxy</source>
        <translation type="vanished">Ağ Vekili</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation type="vanished">Ağ Vekili</translation>
    </message>
</context>
<context>
    <name>PwdDialog</name>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="35"/>
        <source>VNC password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="52"/>
        <source>Password</source>
        <translation type="unfinished">Parola</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="67"/>
        <source>Must be 1-8 characters long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="84"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="88"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>display</source>
        <translation type="vanished">Ekran</translation>
    </message>
    <message>
        <source>defaultapp</source>
        <translation type="vanished">Varsayılan Uyg.</translation>
    </message>
    <message>
        <source>power</source>
        <translation type="vanished">Güç</translation>
    </message>
    <message>
        <source>autoboot</source>
        <translation type="vanished">Otomatik Başlat</translation>
    </message>
    <message>
        <source>printer</source>
        <translation type="vanished">Yazıcı</translation>
    </message>
    <message>
        <source>mousecontrol</source>
        <translation type="vanished">Fare Kontrolü</translation>
    </message>
    <message>
        <source>mouse</source>
        <translation type="vanished">Fare</translation>
    </message>
    <message>
        <source>touchpad</source>
        <translation type="vanished">Dokunmatik Yüzey</translation>
    </message>
    <message>
        <source>keyboard</source>
        <translation type="vanished">Klavye</translation>
    </message>
    <message>
        <source>shortcut</source>
        <translation type="vanished">Kısayol</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">Ses</translation>
    </message>
    <message>
        <source>background</source>
        <translation type="vanished">Arkaplan</translation>
    </message>
    <message>
        <source>screenlock</source>
        <translation type="vanished">Kilit Ekranı</translation>
    </message>
    <message>
        <source>fonts</source>
        <translation type="vanished">Fontlar</translation>
    </message>
    <message>
        <source>Screensaver</source>
        <translation type="vanished">Ekran koruyucu</translation>
    </message>
    <message>
        <source>desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <source>netconnect</source>
        <translation type="vanished">Ağ Bağlantısı</translation>
    </message>
    <message>
        <source>vpn</source>
        <translation type="vanished">VPN</translation>
    </message>
    <message>
        <source>proxy</source>
        <translation type="vanished">Ağ Vekili</translation>
    </message>
    <message>
        <source>userinfo</source>
        <translation type="vanished">Kullanıcı Bilgisi</translation>
    </message>
    <message>
        <source>datetime</source>
        <translation type="vanished">Tarih Saat</translation>
    </message>
    <message>
        <source>area</source>
        <translation type="vanished">Alan</translation>
    </message>
    <message>
        <source>update</source>
        <translation type="vanished">Güncelleme</translation>
    </message>
    <message>
        <source>backup</source>
        <translation type="vanished">Yedekleme</translation>
    </message>
    <message>
        <source>notice</source>
        <translation type="vanished">Bildirim</translation>
    </message>
    <message>
        <source>about</source>
        <translation type="vanished">Hakkında</translation>
    </message>
    <message>
        <source>experienceplan</source>
        <translation type="vanished">Deneyim Planı</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">Tema</translation>
    </message>
    <message>
        <source>ukui-control-center had already running!</source>
        <translation type="vanished">ukui-kontrol-merkezi zaten çalışıyor!</translation>
    </message>
    <message>
        <source>basicIcon</source>
        <translation type="vanished">Temel Simge</translation>
    </message>
    <message>
        <source>classicalIcon</source>
        <translation type="vanished">Klasik Tema</translation>
    </message>
    <message>
        <source>defaultIcon</source>
        <translation type="vanished">Varsayılan Simge</translation>
    </message>
    <message>
        <source>basic</source>
        <translation type="vanished">Temel</translation>
    </message>
    <message>
        <source>classical</source>
        <translation type="vanished">Klasik</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">Varsayılan</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Bilinmeyen</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="187"/>
        <source>Customize Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="694"/>
        <source>Edit Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update Shortcut</source>
        <translation type="vanished">Kısayoları Güncelle</translation>
    </message>
    <message>
        <source>Add Shortcut</source>
        <translation type="vanished">Kısayol Ekle</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">Ekran</translation>
    </message>
    <message>
        <source>Defaultapp</source>
        <translation type="vanished">Uygulama</translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="vanished">Güç</translation>
    </message>
    <message>
        <source>Autoboot</source>
        <translation type="vanished">Otomatik Başlat</translation>
    </message>
    <message>
        <source>Default App</source>
        <translation type="obsolete">Varsayılan Uyg.</translation>
    </message>
    <message>
        <source>Auto Boot</source>
        <translation type="obsolete">Otomatik Başlat</translation>
    </message>
    <message>
        <source>Printer</source>
        <translation type="vanished">Yazıcı</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="vanished">Fare</translation>
    </message>
    <message>
        <source>Touchpad</source>
        <translation type="vanished">Touchpad</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">Klavye</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation type="vanished">Kısayol</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">Ses</translation>
    </message>
    <message>
        <source>Background</source>
        <translation type="vanished">Arkaplan</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">Tema</translation>
    </message>
    <message>
        <source>Screenlock</source>
        <translation type="vanished">Ekran kilidi</translation>
    </message>
    <message>
        <source>Fonts</source>
        <translation type="vanished">Yazı Tipi</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="71"/>
        <source>User Info</source>
        <translation type="unfinished">Kullanıcı Bilgisi</translation>
    </message>
    <message>
        <source>Security Center</source>
        <translation type="obsolete">Güvenlik Merkezi</translation>
    </message>
    <message>
        <source>Netconnect</source>
        <translation type="vanished">Ağ</translation>
    </message>
    <message>
        <source>Vpn</source>
        <translation type="vanished">VPN</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation type="vanished">Ağ Vekili</translation>
    </message>
    <message>
        <source>Userinfo</source>
        <translation type="vanished">Kullanıcı</translation>
    </message>
    <message>
        <source>Cloud Account</source>
        <translation type="obsolete">Bulut Hesabı</translation>
    </message>
    <message>
        <source>Datetime</source>
        <translation type="vanished">Tarih saat</translation>
    </message>
    <message>
        <source>Area</source>
        <translation type="vanished">Alan</translation>
    </message>
    <message>
        <source>SecurityCenter</source>
        <translation type="vanished">Güvenlik Merkezi</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Güncelleme</translation>
    </message>
    <message>
        <source>Backup</source>
        <translation type="vanished">Yedekleme</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="vanished">Bildirim</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Hakkında</translation>
    </message>
    <message>
        <source>Experienceplan</source>
        <translation type="vanished">Deneyim planı</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1335"/>
        <source>min length %1
</source>
        <translation>En az uzunluk %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1345"/>
        <source>min digit num %1
</source>
        <translation>En az basamak sayısı %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1354"/>
        <source>min upper num %1
</source>
        <translation>En az üst sayı %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1363"/>
        <source>min lower num %1
</source>
        <translation>En az alt sayı %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1372"/>
        <source>min other num %1
</source>
        <translation>En az diğer sayı %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1382"/>
        <source>min char class %1
</source>
        <translation>En az karakter sınıfı %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1391"/>
        <source>max repeat %1
</source>
        <translation>En fazla tekrar %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1400"/>
        <source>max class repeat %1
</source>
        <translation>En fazla sınıf tekrarı %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1409"/>
        <source>max sequence %1
</source>
        <translation>En fazla dizi %1</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="vanished">Asla</translation>
    </message>
    <message>
        <source>suspend</source>
        <translation type="obsolete">Askıya Al</translation>
    </message>
    <message>
        <source>hibernate</source>
        <translation type="obsolete">Beklemeye Al</translation>
    </message>
    <message>
        <source>shutdown</source>
        <translation type="obsolete">Kapat</translation>
    </message>
    <message>
        <source>nothing</source>
        <translation type="obsolete">Hiçbiri</translation>
    </message>
    <message>
        <source>blank</source>
        <translation type="obsolete">Boş</translation>
    </message>
    <message>
        <source>Year</source>
        <translation type="vanished">Yıl</translation>
    </message>
    <message>
        <source>Jan</source>
        <translation type="vanished">Oca</translation>
    </message>
    <message>
        <source>Feb</source>
        <translation type="vanished">Şub</translation>
    </message>
    <message>
        <source>Mar</source>
        <translation type="vanished">Mar</translation>
    </message>
    <message>
        <source>Apr</source>
        <translation type="vanished">Nis</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>May</source>
        <translation>May</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>January</source>
        <translation>Ocak</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>February</source>
        <translation>Şubat</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>March</source>
        <translation>Mart</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>April</source>
        <translation>Nisan</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>June</source>
        <translation>Haziran</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>July</source>
        <translation>Temmuz</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>August</source>
        <translation>Ağustos</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>September</source>
        <translation>Eylül</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>October</source>
        <translation>Ekim</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>Novermber</source>
        <translation>Kasım</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>December</source>
        <translation>Aralık</translation>
    </message>
    <message>
        <source>Jun</source>
        <translation type="vanished">Haz</translation>
    </message>
    <message>
        <source>Jul</source>
        <translation type="vanished">Tem</translation>
    </message>
    <message>
        <source>Aug</source>
        <translation type="vanished">Ağu</translation>
    </message>
    <message>
        <source>Sep</source>
        <translation type="vanished">Eyl</translation>
    </message>
    <message>
        <source>Oct</source>
        <translation type="vanished">Eki</translation>
    </message>
    <message>
        <source>Nov</source>
        <translation type="vanished">Kas</translation>
    </message>
    <message>
        <source>Dec</source>
        <translation type="vanished">Ara</translation>
    </message>
    <message>
        <source>Day</source>
        <translation type="vanished">Gün</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="85"/>
        <source>ukui-control-center is already running!</source>
        <translation>Ukui Denetim Merkezi zaten çalışıyor!</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="96"/>
        <source>ukui-control-center is disabled！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="130"/>
        <source>ukui-control-center</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure to delete &quot;%1&quot; group, 
which will make some file components 
in the file system invalid!</source>
        <translation type="obsolete">%1 grubu silmek istediğinizden emin misiniz,
bazı dosya bileşenleri oluşturacak
dosya sisteminde geçersiz!</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="682"/>
        <source>xxx客户端</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="8"/>
        <source>简体中文</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="9"/>
        <source>English</source>
        <translation type="unfinished">İngilizce</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="10"/>
        <source>བོད་ཡིག</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="11"/>
        <source>ᠮᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="12"/>
        <source>繁體</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="765"/>
        <source>Programs are not allowed to be added.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegDialog</name>
    <message>
        <source>Get</source>
        <translation type="vanished">Al</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">Parolanız burada</translation>
    </message>
    <message>
        <source>Your account here</source>
        <translation type="vanished">Hesabınız burada</translation>
    </message>
    <message>
        <source>Confirm your password</source>
        <translation type="vanished">Parolanızı doğrulayın</translation>
    </message>
    <message>
        <source>Your code here</source>
        <translation type="vanished">Kodunuz burada</translation>
    </message>
    <message>
        <source>This operation is permanent</source>
        <translation type="vanished">Bu işlem kalıcıdır</translation>
    </message>
    <message>
        <source>At least 6 bit, include letters and digt</source>
        <translation type="vanished">En az 6 karakter, harf ve rakam ekleyin</translation>
    </message>
    <message>
        <source>Your password is valid!</source>
        <translation type="vanished">Parolanız geçerlidir!</translation>
    </message>
</context>
<context>
    <name>ResolutionSlider</name>
    <message>
        <source>(recommend)</source>
        <translation type="vanished">(Önerilen)</translation>
    </message>
    <message>
        <source>No available resolutions</source>
        <translation type="vanished">Kullanılabilir çözünürlük yok</translation>
    </message>
</context>
<context>
    <name>Screenlock</name>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="26"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="80"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="51"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="136"/>
        <source>Screenlock</source>
        <translation>Ekran kilidi</translation>
        <extra-contents_path>/Screenlock/Screenlock</extra-contents_path>
    </message>
    <message>
        <source>Screenlock Interface</source>
        <translation type="vanished">Ekran Kilidi Arayüzü</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="205"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="256"/>
        <source>Show message on lock screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="401"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="150"/>
        <source>Local Pictures</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screenlock/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="408"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="152"/>
        <source>Online Pictures</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screenlock/Online Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="440"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="162"/>
        <source>Reset To Default</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screenlock/Reset To Default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="483"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="139"/>
        <source>Related Settings</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screenlock/Related Settings</extra-contents_path>
    </message>
    <message>
        <source>Screenlock Set</source>
        <translation type="vanished">Ekran Kilidi Ayarı</translation>
    </message>
    <message>
        <source>Lock screen when screensaver boot</source>
        <translation type="vanished">Ekran koruyucu açıldığında ekranı kilitle</translation>
        <extra-contents_path>/Screenlock/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="332"/>
        <source>Lock screen delay</source>
        <translation>Ekran kilidi gecikmesi</translation>
    </message>
    <message>
        <source>Min</source>
        <translation type="vanished">Dk</translation>
    </message>
    <message>
        <source>Select screenlock background</source>
        <translation type="vanished">Kilit Ekranı Arkaplanını Seç</translation>
    </message>
    <message>
        <source>Browser online wp</source>
        <translation type="vanished">İnternetten Al</translation>
    </message>
    <message>
        <source>Browser local wp</source>
        <translation type="vanished">Bilgisayardan Ekle</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="262"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="148"/>
        <source>Show picture of screenlock on screenlogin</source>
        <translation>Ekran girişinde ekran kilidinin resmini göster</translation>
        <extra-contents_path>/Screenlock/Show picture of screenlock on screenlogin</extra-contents_path>
    </message>
    <message>
        <source>Enabel screenlock</source>
        <translation type="vanished">Ekran kilidi aktif</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">Aç</translation>
    </message>
    <message>
        <source>screenlock</source>
        <translation type="vanished">Ekran kilidi</translation>
    </message>
    <message>
        <source>picture</source>
        <translation type="obsolete">Resim</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>Never</source>
        <translation>Asla</translation>
    </message>
    <message>
        <source>5m</source>
        <translation type="vanished">5 dk</translation>
    </message>
    <message>
        <source>10m</source>
        <translation type="vanished">10 dk</translation>
    </message>
    <message>
        <source>30m</source>
        <translation type="vanished">30 dk</translation>
    </message>
    <message>
        <source>45m</source>
        <translation type="vanished">45 dk</translation>
    </message>
    <message>
        <source>1m</source>
        <translation type="vanished">1 dk</translation>
    </message>
    <message>
        <source>1h</source>
        <translation type="vanished">1 sa</translation>
    </message>
    <message>
        <source>1.5h</source>
        <translation type="vanished">1.5 sa</translation>
    </message>
    <message>
        <source>3h</source>
        <translation type="vanished">3 sa</translation>
    </message>
    <message>
        <source>2h</source>
        <translation type="obsolete">3 sa {2h?}</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="563"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation type="unfinished">Duvarkağıdı Dosyaları(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</translation>
    </message>
    <message>
        <source>allFiles(*.*)</source>
        <translation type="obsolete">Tüm Dosyalar(*.*)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>1min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>5min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>10min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>30min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>45min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>1hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>2hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>3hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="605"/>
        <source>select custom wallpaper file</source>
        <translation type="unfinished">Özel duvar kağıdı dosyasını seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="606"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="607"/>
        <source>Position: </source>
        <translation type="unfinished">Konum: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="608"/>
        <source>FileName: </source>
        <translation type="unfinished">Dosya Adı: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="609"/>
        <source>FileType: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="610"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="539"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="142"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="698"/>
        <source>Monitor Off</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screenlock/Monitor Off</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="594"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="145"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="699"/>
        <source>Screensaver</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screenlock/Screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="558"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="613"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="700"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="701"/>
        <source>Set</source>
        <translation type="unfinished">Ayarla</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="59"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="99"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="209"/>
        <source>Screensaver</source>
        <translation>Ekran Koruyucu</translation>
        <extra-contents_path>/Screensaver/Screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="201"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="213"/>
        <source>Idle time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screensaver/Idle time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="475"/>
        <source>Lock screen when activating screensaver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enable screensaver</source>
        <translation type="vanished">Ekran Koruyucu Aktif</translation>
        <extra-contents_path>/screensaver/Enable screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="211"/>
        <source>Screensaver program</source>
        <translation>Ekran Koruyucu Programı</translation>
        <extra-contents_path>/Screensaver/Screensaver program</extra-contents_path>
    </message>
    <message>
        <source>idle time</source>
        <translation type="vanished">Bekleme Süresi</translation>
        <extra-contents_path>/screensaver/idle time</extra-contents_path>
    </message>
    <message>
        <source>Min</source>
        <translation type="vanished">Dk</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="960"/>
        <source>Lock screen when screensaver boot</source>
        <translation>Ekran koruyucu açıldığında ekranı kilitle</translation>
        <extra-contents_path>/Screensaver/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <source>screensaver</source>
        <translation type="vanished">Ekran Koruyucu</translation>
    </message>
    <message>
        <source>Default_ukui</source>
        <translation type="vanished">Ukui Varsayılanı</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="197"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="915"/>
        <source>Text(up to 30 characters):</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screensaver/Text(up to 30 characters):</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="950"/>
        <source>Show rest time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screensaver/Show rest time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="272"/>
        <source>UKUI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="273"/>
        <source>Blank_Only</source>
        <translation>Sadece Kalın</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="284"/>
        <source>Customize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="815"/>
        <source>5min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="816"/>
        <source>10min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="817"/>
        <source>30min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>15min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>1hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="716"/>
        <source>Screensaver source</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screensaver/Screensaver source</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="722"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="772"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="730"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp *.svg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="771"/>
        <source>select custom screensaver dir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="773"/>
        <source>Position: </source>
        <translation type="unfinished">Konum: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="774"/>
        <source>FileName: </source>
        <translation type="unfinished">Dosya Adı: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="775"/>
        <source>FileType: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="776"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="810"/>
        <source>Switching time</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screensaver/Switching time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="293"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="814"/>
        <source>1min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="861"/>
        <source>Ordinal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="871"/>
        <source>Random switching</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screensaver/Random switching</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="992"/>
        <source>Text position</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Screensaver/Text position</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1000"/>
        <source>Centered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1001"/>
        <source>Randow(Bubble text)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="862"/>
        <source>Random</source>
        <translation>Rasgele</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="298"/>
        <source>Never</source>
        <translation>Asla</translation>
    </message>
    <message>
        <source>5m</source>
        <translation type="vanished">5 dk</translation>
    </message>
    <message>
        <source>10m</source>
        <translation type="vanished">10 dk</translation>
    </message>
    <message>
        <source>30m</source>
        <translation type="vanished">30 dk</translation>
    </message>
    <message>
        <source>45m</source>
        <translation type="vanished">45 dk</translation>
    </message>
    <message>
        <source>1m</source>
        <translation type="vanished">1 dk</translation>
    </message>
    <message>
        <source>1h</source>
        <translation type="vanished">1 sa</translation>
    </message>
    <message>
        <source>1.5h</source>
        <translation type="vanished">1.5 sa</translation>
    </message>
    <message>
        <source>3h</source>
        <translation type="vanished">3 sa</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <source>Search</source>
        <translation type="obsolete">Ara</translation>
    </message>
    <message>
        <source>360</source>
        <translation type="obsolete">3 sa {360?}</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="obsolete">Konum: </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="obsolete">Dosya Adı: </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Uyarı</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <source>Touchpad</source>
        <translation type="obsolete">Dokunmatik Yüzey</translation>
    </message>
    <message>
        <location filename="../../searchwidget.cpp" line="61"/>
        <location filename="../../searchwidget.cpp" line="62"/>
        <location filename="../../searchwidget.cpp" line="69"/>
        <location filename="../../searchwidget.cpp" line="71"/>
        <location filename="../../searchwidget.cpp" line="76"/>
        <source>No search results</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SecurityCenter</name>
    <message>
        <source>SecurityCenter</source>
        <translation type="vanished">Güvenlik Merkezi</translation>
    </message>
    <message>
        <source>Summarize</source>
        <translation type="vanished">Özetler</translation>
    </message>
    <message>
        <source>Recognize the current security of the system, and can take the necessary settings</source>
        <translation type="vanished">Sistemin mevcut güvenliğini tanır ve gerekli ayarları yapabilir</translation>
    </message>
    <message>
        <source>Run Security Center</source>
        <translation type="vanished">Güvenlik Merkezi&apos;ni çalıştırın</translation>
    </message>
    <message>
        <source>Security Center</source>
        <translation type="obsolete">Güvenlik Merkezi</translation>
    </message>
    <message>
        <source>Virus Protection</source>
        <translation type="vanished">Virüs Koruması</translation>
    </message>
    <message>
        <source>Protect system from threats</source>
        <translation type="vanished">Sistemi tehditlerden koruyun</translation>
    </message>
    <message>
        <source>Network Protection</source>
        <translation type="vanished">Ağ Koruması</translation>
    </message>
    <message>
        <source>Setup app that can access web</source>
        <translation type="vanished">Web&apos;e erişebilen kurulum uygulaması</translation>
    </message>
    <message>
        <source>App Execution Control</source>
        <translation type="vanished">Uygulama Yürütme Denetimi</translation>
    </message>
    <message>
        <source>App install and exe protection</source>
        <translation type="vanished">Uygulama yükleme ve exe koruması</translation>
    </message>
    <message>
        <source>Account Security</source>
        <translation type="vanished">Hesap güvenliği</translation>
        <extra-contents_path>/securitycenter/Account Security</extra-contents_path>
    </message>
    <message>
        <source>Protect account and login security</source>
        <translation type="vanished">Hesabı ve giriş güvenliğini koruyun</translation>
    </message>
</context>
<context>
    <name>ShareMain</name>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Uyarı</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Parola</translation>
    </message>
    <message>
        <source>Share</source>
        <translation type="obsolete">Paylaş</translation>
    </message>
    <message>
        <source>Output</source>
        <translation type="obsolete">Çıkış</translation>
    </message>
    <message>
        <source>Input</source>
        <translation type="obsolete">Giriş</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="obsolete">Klavye</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Kapat</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="68"/>
        <source>Remote Desktop</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Vino/Remote Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="80"/>
        <source>Connect to your desktop remotely</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Vino/Connect to your desktop remotely</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="98"/>
        <source>Allow others to connect to your desktop remotely using RDP</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using RDP</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="112"/>
        <source>Allow others to connect to your desktop remotely using VNC</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using VNC</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="160"/>
        <source>Require user to enter this password while using VNC: </source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Vino/Require user to enter this password while using VNC:</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="164"/>
        <source>Edit</source>
        <translation type="unfinished">Düzenle</translation>
    </message>
    <message>
        <source>Allow others to view your desktop</source>
        <translation type="obsolete">Başkalarının masaüstünüzü görüntülemesine izin verin</translation>
        <extra-contents_path>/Vino/Allow others to view your desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="126"/>
        <source>Allow connection to control screen</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Vino/Allow connection to control screen</extra-contents_path>
    </message>
    <message>
        <source>Security</source>
        <translation type="obsolete">Güvenlik</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="142"/>
        <source>You must confirm every visit for this machine</source>
        <translation type="unfinished">Bu makine için her ziyareti onaylamalısınız</translation>
        <extra-contents_path>/Vino/You must confirm every visit for this machine</extra-contents_path>
    </message>
    <message>
        <source>Require user to enter this password: </source>
        <translation type="obsolete">Kullanıcının bu şifreyi girmesini zorunlu kılın: </translation>
        <extra-contents_path>/Vino/Require user to enter this password:</extra-contents_path>
    </message>
</context>
<context>
    <name>ShareMainHw</name>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>Warning</source>
        <translation type="unfinished">Uyarı</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>please select an output</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="331"/>
        <source>Input Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="332"/>
        <source>Password</source>
        <translation type="unfinished">Parola</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="445"/>
        <source>Share</source>
        <translation type="unfinished">Paylaş</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="461"/>
        <source>Allow others to view your desktop</source>
        <translation type="unfinished">Başkalarının masaüstünüzü görüntülemesine izin verin</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="471"/>
        <source>Security</source>
        <translation type="unfinished">Güvenlik</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="481"/>
        <source>Require user to enter this password: </source>
        <translation type="unfinished">Kullanıcının bu şifreyi girmesini zorunlu kılın: </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="483"/>
        <source>Edit</source>
        <translation type="unfinished">Düzenle</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="496"/>
        <source>Output</source>
        <translation type="unfinished">Çıkış</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="508"/>
        <source>Input</source>
        <translation type="unfinished">Giriş</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="518"/>
        <source>Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="520"/>
        <source>Keyboard</source>
        <translation type="unfinished">Klavye</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="522"/>
        <source>Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="548"/>
        <source>Close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="550"/>
        <source>ViewOnly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="558"/>
        <source>Client Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="569"/>
        <source>Client Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="583"/>
        <source>Client IP：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>退出程序</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>确认退出程序！</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Shortcut</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="50"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="164"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="178"/>
        <source>System Shortcut</source>
        <translation>Sistem Kısayolları</translation>
        <extra-contents_path>/Shortcut/System Shortcut</extra-contents_path>
    </message>
    <message>
        <source>Show all shortcut</source>
        <translation type="vanished">Tüm Kısayolları Göster</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="103"/>
        <source>Custom Shortcut</source>
        <translation>Özel Kısayollar</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="166"/>
        <source>Customize Shortcut</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Shortcut/Customize Shortcut</extra-contents_path>
    </message>
    <message>
        <source>Add custom shortcut</source>
        <translation type="vanished">Özel Kısayol Ekle</translation>
        <extra-contents_path>/shortcut/Add custom shortcut</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="161"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
        <extra-contents_path>/Shortcut/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="441"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="573"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="836"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="442"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="574"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="837"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="443"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="575"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="838"/>
        <source>Shortcut key conflict, use it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="444"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="576"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="839"/>
        <source>%1 occuied, using this combination will invalidate %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="467"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="606"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="868"/>
        <source>Shortcut &quot;%1&quot; occuied, please change the key combination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="677"/>
        <source>Edit</source>
        <translation type="unfinished">Düzenle</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="678"/>
        <source>Delete</source>
        <translation type="unfinished">Sil</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1039"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1043"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1061"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1065"/>
        <source>Null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1126"/>
        <source> or </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>disable</source>
        <translation type="obsolete">Kapalı</translation>
    </message>
    <message>
        <source>Reset default</source>
        <translation type="vanished">Varsayılana Sıfırla</translation>
    </message>
    <message>
        <source>shortcut</source>
        <translation type="vanished">Kısayol</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="80"/>
        <source>Shortcut</source>
        <translation>Kısayol</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">Sistem</translation>
    </message>
</context>
<context>
    <name>ShowAllShortcut</name>
    <message>
        <source>System Shortcuts</source>
        <translation type="vanished">Sistem Kısayolları</translation>
    </message>
    <message>
        <source>Show all shortcut</source>
        <translation type="vanished">Tüm kısayolları göster</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
</context>
<context>
    <name>StatusDialog</name>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="10"/>
        <source>About</source>
        <translation type="unfinished">Hakkında</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="59"/>
        <source>Activation Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SuccessDiaolog</name>
    <message>
        <source>Reback sign in</source>
        <translation type="vanished">Yeniden oturum açma</translation>
    </message>
    <message>
        <source>Sign up success!</source>
        <translation type="vanished">Kayıt başarılı!</translation>
    </message>
    <message>
        <source>Reset success!</source>
        <translation type="vanished">Sıfırlama başarılı!</translation>
    </message>
    <message>
        <source>Sign in success!</source>
        <translation type="vanished">Giriş başarılı!</translation>
    </message>
    <message>
        <source>Binding phone success!</source>
        <translation type="vanished">Telefon bağlama başarılı!</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">Onayla</translation>
    </message>
</context>
<context>
    <name>SyncDialog</name>
    <message>
        <source>ScreenSaver</source>
        <translation type="obsolete">Ekran Koruyucu</translation>
    </message>
    <message>
        <source>Avatar</source>
        <translation type="obsolete">Avatar</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="obsolete">Menü</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="obsolete">Sekme</translation>
    </message>
    <message>
        <source>Quick Start</source>
        <translation type="obsolete">Hızlı Başlat</translation>
    </message>
    <message>
        <source>Themes</source>
        <translation type="obsolete">Temalar</translation>
    </message>
    <message>
        <source>Mouse</source>
        <translation type="obsolete">Fare</translation>
    </message>
    <message>
        <source>TouchPad</source>
        <translation type="obsolete">Dokunmatik Yüzey</translation>
    </message>
    <message>
        <source>KeyBoard</source>
        <translation type="obsolete">Klavye</translation>
    </message>
    <message>
        <source>ShortCut</source>
        <translation type="obsolete">Kısayol</translation>
    </message>
    <message>
        <source>Area</source>
        <translation type="obsolete">Alan</translation>
    </message>
    <message>
        <source>Date/Time</source>
        <translation type="obsolete">Tarih/Zaman</translation>
    </message>
    <message>
        <source>Default Open</source>
        <translation type="obsolete">Varsayılan Açık</translation>
    </message>
    <message>
        <source>Notice</source>
        <translation type="obsolete">Bildirim</translation>
    </message>
    <message>
        <source>Option</source>
        <translation type="obsolete">Seçenek</translation>
    </message>
    <message>
        <source>Peony</source>
        <translation type="obsolete">Peony</translation>
    </message>
    <message>
        <source>Boot</source>
        <translation type="obsolete">Boot</translation>
    </message>
    <message>
        <source>Power</source>
        <translation type="obsolete">Güç</translation>
    </message>
    <message>
        <source>Editor</source>
        <translation type="obsolete">Düzenleyici</translation>
    </message>
    <message>
        <source>Terminal</source>
        <translation type="obsolete">Uçbirim</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="obsolete">Hava Durumu</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">Medya</translation>
    </message>
</context>
<context>
    <name>TabWid</name>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
</context>
<context>
    <name>Theme</name>
    <message>
        <source>Theme Mode</source>
        <translation type="vanished">Tema Modu</translation>
        <extra-contents_path>/Theme/Theme Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="107"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="521"/>
        <source>Theme</source>
        <translation>Tema</translation>
        <extra-contents_path>/Theme/Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <source>Default</source>
        <translation>Varsayılan</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="185"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="422"/>
        <source>Corlor</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Theme/Corlor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="604"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Theme/Other</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="610"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="624"/>
        <source>Set</source>
        <translation type="unfinished">Ayarla</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="616"/>
        <source>Wallpaper</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Theme/Wallpaper</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="630"/>
        <source>Beep</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Theme/Beep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="961"/>
        <source>Blue-Crystal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="963"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <source>Light-Seeking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="965"/>
        <source>DMZ-Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="967"/>
        <source>DMZ-White</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="969"/>
        <source>Dark-Sense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1084"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>basic</source>
        <translation type="unfinished">Temel</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>Classic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1090"/>
        <source>HeYin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1092"/>
        <source>hp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>ukui</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1096"/>
        <source>daybreakBlue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1098"/>
        <source>jamPurple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1100"/>
        <source>magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1102"/>
        <source>sunRed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1104"/>
        <source>sunsetOrange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1106"/>
        <source>dustGold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1108"/>
        <source>polarGreen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>default</source>
        <translation type="obsolete">Varsayılan</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="183"/>
        <source>Light</source>
        <translation>Açık</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="181"/>
        <source>Dark</source>
        <translation>Koyu</translation>
    </message>
    <message>
        <source>Middle</source>
        <translation type="vanished">Orta</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="212"/>
        <source>Window Theme</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Theme/Window Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="214"/>
        <source>Icon theme</source>
        <translation>Simge Teması</translation>
        <extra-contents_path>/Theme/Icon theme</extra-contents_path>
    </message>
    <message>
        <source>Control theme</source>
        <translation type="vanished">Kontrol Teması</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="217"/>
        <source>Cursor theme</source>
        <translation>İmleç Teması</translation>
        <extra-contents_path>/Theme/Cursor theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="134"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="221"/>
        <source>Effect setting</source>
        <translation>Efekt Ayarları</translation>
        <extra-contents_path>/Theme/Effect setting</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="352"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="225"/>
        <source>Transparency</source>
        <translation>Şeffaflık</translation>
        <extra-contents_path>/Theme/Transparency</extra-contents_path>
    </message>
    <message>
        <source>Transparent effects</source>
        <translation type="vanished">Şeffaflık Etkileri</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="249"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="223"/>
        <source>Performance mode</source>
        <translation>Performans Modu</translation>
        <extra-contents_path>/Theme/Performance mode</extra-contents_path>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="vanished">Şeffaflık</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="vanished">Düşük</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="vanished">Yüksek</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="473"/>
        <source>Reset to default</source>
        <translation>Varsayılana Dön</translation>
    </message>
    <message>
        <source>theme</source>
        <translation type="vanished">Tema</translation>
    </message>
</context>
<context>
    <name>TimeBtn</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="80"/>
        <source>Tomorrow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="82"/>
        <source>Yesterday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="84"/>
        <source>Today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="102"/>
        <source>%1 hours earlier than local</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="104"/>
        <source>%1 hours later than local</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimeZoneChooser</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="38"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="39"/>
        <source>Confirm</source>
        <translation>Onayla</translation>
    </message>
    <message>
        <source>Change time zone</source>
        <translation type="vanished">Zaman dilimini değiştir</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="36"/>
        <source>Search Timezone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="65"/>
        <source>To select a time zone, please click where near you on the map and select a city from the nearest city</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>change timezone</source>
        <translation type="vanished">zaman dilimini değiştir</translation>
    </message>
    <message>
        <source>change zonne</source>
        <translation type="vanished">Dilimi Değiştir</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="41"/>
        <source>Change Timezone</source>
        <translation>Zaman Dilimini Değiştir</translation>
    </message>
</context>
<context>
    <name>TouchScreen</name>
    <message>
        <source>monitor</source>
        <translation type="obsolete">Ekran:</translation>
    </message>
</context>
<context>
    <name>Touchpad</name>
    <message>
        <source>Touchpad Settings</source>
        <translation type="vanished">Dokunmatik Yüzey Ayarları</translation>
        <extra-contents_path>/touchpad/Touchpad Settings</extra-contents_path>
    </message>
    <message>
        <source>Enabled touchpad</source>
        <translation type="vanished">Dokunmatik yüzey aktif</translation>
    </message>
    <message>
        <source>Disable touchpad while typing</source>
        <translation type="vanished">Yazarken dokunmatik yüzeyi devre dışı bırak</translation>
    </message>
    <message>
        <source>Enable mouse clicks with touchpad</source>
        <translation type="vanished">Dokunmatik yüzeyde fare tıklamalarını etkinleştir</translation>
    </message>
    <message>
        <source>Scrolling</source>
        <translation type="vanished">Kaydırma</translation>
    </message>
    <message>
        <source>No touchpad found</source>
        <translation type="vanished">Dokunmatik yüzey bulunamadı</translation>
    </message>
    <message>
        <source>touchpad</source>
        <translation type="vanished">Dokunmatik Yüzey</translation>
    </message>
    <message>
        <source>Touchpad</source>
        <translation type="vanished">Dokunmatik Yüzey</translation>
    </message>
    <message>
        <source>Disable rolling</source>
        <translation type="vanished">Kaydırma kapalı</translation>
    </message>
    <message>
        <source>Vertical edge scrolling</source>
        <translation type="vanished">Dikey kenar kaydırma</translation>
    </message>
    <message>
        <source>Horizontal edge scrolling</source>
        <translation type="vanished">Yatay kenar kaydırma</translation>
    </message>
    <message>
        <source>Vertical two-finger scrolling</source>
        <translation type="vanished">Dikey iki parmakla kaydırma</translation>
    </message>
    <message>
        <source>Horizontal two-finger scrolling</source>
        <translation type="vanished">Yatay iki parmakla kaydırma</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <source>No touchpad found</source>
        <translation type="obsolete">Dokunmatik yüzey bulunamadı</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="obsolete">Yavaş</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="obsolete">Hızlı</translation>
    </message>
</context>
<context>
    <name>TrialDialog</name>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="12"/>
        <source>Set</source>
        <translation type="unfinished">Ayarla</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="37"/>
        <source>Yinhe Kylin OS(Trail Version) Disclaimer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="46"/>
        <source>Dear customer:
       Thank you for trying Yinhe Kylin OS(trail version)! This version is free for users who only try out, no commercial purpose is permitted. The trail period lasts one year and it starts from the ex-warehouse time of the OS. No after-sales service is provided during the trail stage. If any security problems occurred when user put important files or do any commercial usage in system, all consequences are taken by users. Kylin software Co., Ltd. take no legal risk in trail version.
       During trail stage,if you want any technology surpport or activate the system, please buy“Yinhe Kylin Operating System”official version or authorization by contacting 400-089-1870.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="60"/>
        <source>Kylin software Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkccAbout</name>
    <message>
        <source>UKCC</source>
        <translation type="obsolete">Denetim Merkezi</translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="33"/>
        <location filename="../../ukccabout.cpp" line="59"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="64"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="74"/>
        <source>Service and Support:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaApplicationWidget</name>
    <message>
        <source>Application Volume</source>
        <translation type="obsolete">Uygulama Sesi</translation>
    </message>
    <message>
        <source>No application is currently playing or recording audio</source>
        <translation type="obsolete">Şu anda hiçbir uygulama ses çalmıyor veya ses kaydetmiyor</translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <source>Input</source>
        <translation type="vanished">Giriş</translation>
    </message>
    <message>
        <source>Input Device</source>
        <translation type="vanished">Giriş Aygıtı</translation>
        <extra-contents_path>/audio/Input Device</extra-contents_path>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">Ses</translation>
    </message>
    <message>
        <source>Input Level</source>
        <translation type="vanished">Giriş Seviyesi</translation>
        <extra-contents_path>/audio/Input Level</extra-contents_path>
    </message>
    <message>
        <source>Low</source>
        <translation type="vanished">Az</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="vanished">Yüksek</translation>
    </message>
    <message>
        <source>Select input device</source>
        <translation type="vanished">Giriş cihazını seçin</translation>
    </message>
    <message>
        <source>Input device</source>
        <translation type="vanished">Giriş aygıtı</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">Ses</translation>
    </message>
    <message>
        <source>Input level</source>
        <translation type="vanished">Giriş Seviyesi</translation>
    </message>
    <message>
        <source>low</source>
        <translation type="vanished">Düşük</translation>
    </message>
    <message>
        <source>high</source>
        <translation type="vanished">Yüksek</translation>
    </message>
    <message>
        <source>Connector</source>
        <translation type="vanished">Bağlayıcı</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <source>sound error</source>
        <translation type="vanished">Ses hatası</translation>
    </message>
    <message>
        <source>load sound failed</source>
        <translation type="vanished">Yükleme sesi başarısız</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <source>Output</source>
        <translation type="vanished">Çıkış</translation>
    </message>
    <message>
        <source>Output Device</source>
        <translation type="vanished">Çıkış Aygıtı</translation>
        <extra-contents_path>/audio/Output Device</extra-contents_path>
    </message>
    <message>
        <source>Master Volume</source>
        <translation type="vanished">Ana Ses</translation>
        <extra-contents_path>/audio/Master Volume</extra-contents_path>
    </message>
    <message>
        <source>Balance</source>
        <translation type="vanished">Denge</translation>
        <extra-contents_path>/audio/Balance</extra-contents_path>
    </message>
    <message>
        <source>Right</source>
        <translation type="vanished">Sağ</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation type="vanished">Profil</translation>
        <extra-contents_path>/audio/Profile</extra-contents_path>
    </message>
    <message>
        <source>Card</source>
        <translation type="vanished">Kart</translation>
        <extra-contents_path>/audio/Card</extra-contents_path>
    </message>
    <message>
        <source>Select output device</source>
        <translation type="vanished">Çıkış cihazını seçin</translation>
    </message>
    <message>
        <source>Output device</source>
        <translation type="vanished">Çıkış aygıtı</translation>
    </message>
    <message>
        <source>Master volume</source>
        <translation type="vanished">Ana ses</translation>
    </message>
    <message>
        <source>Channel balance</source>
        <translation type="vanished">Kanal Dengesi</translation>
    </message>
    <message>
        <source>Left</source>
        <translation type="vanished">Sol</translation>
    </message>
    <message>
        <source>right</source>
        <translation type="vanished">Sağ</translation>
    </message>
    <message>
        <source>Connector</source>
        <translation type="vanished">Ses Çıkışı</translation>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <source>System sound</source>
        <translation type="vanished">Sistem Sesi</translation>
    </message>
    <message>
        <source>Sound theme</source>
        <translation type="vanished">Ses Teması</translation>
    </message>
    <message>
        <source>Prompt voice</source>
        <translation type="vanished">Ses İstemi</translation>
    </message>
    <message>
        <source>Boot music</source>
        <translation type="vanished">Açılış Sesi</translation>
    </message>
    <message>
        <source>System sound theme</source>
        <translation type="vanished">Sistem Sesi Teması</translation>
    </message>
    <message>
        <source>prompt voice</source>
        <translation type="vanished">Ses İstemi</translation>
    </message>
    <message>
        <source>Shutdown</source>
        <translation type="obsolete">Kapat</translation>
    </message>
    <message>
        <source>Lagout</source>
        <translation type="vanished">Çıkış</translation>
    </message>
    <message>
        <source>System Sound</source>
        <translation type="vanished">Sistem Sesi</translation>
    </message>
    <message>
        <source>Sound Theme</source>
        <translation type="vanished">Ses Teması</translation>
        <extra-contents_path>/audio/Sound Theme</extra-contents_path>
    </message>
    <message>
        <source>Alert Sound</source>
        <translation type="vanished">Uyarı Sesi</translation>
        <extra-contents_path>/audio/Alert Sound</extra-contents_path>
    </message>
    <message>
        <source>Boot Music</source>
        <translation type="vanished">Açılış Müziği</translation>
        <extra-contents_path>/audio/Boot Music</extra-contents_path>
    </message>
    <message>
        <source>Beep Switch</source>
        <translation type="vanished">Sesli Uyarı Anahtarı</translation>
        <extra-contents_path>/audio/Beep Switch</extra-contents_path>
    </message>
    <message>
        <source>Window Closed</source>
        <translation type="vanished">Pencere Kapalı</translation>
    </message>
    <message>
        <source>Volume Change</source>
        <translation type="vanished">Ses Değişimi</translation>
        <extra-contents_path>/audio/Volume Change</extra-contents_path>
    </message>
    <message>
        <source>Setting Menu</source>
        <translation type="vanished">Ayar Menüsü</translation>
    </message>
</context>
<context>
    <name>UnifiedOutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="91"/>
        <source>resolution</source>
        <translation>Çözünürlük:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="128"/>
        <source>orientation</source>
        <translation>Uyumluluk</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="133"/>
        <source>arrow-up</source>
        <translation>Yukarı</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="134"/>
        <source>90° arrow-right</source>
        <translation>90° sağa</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="136"/>
        <source>arrow-down</source>
        <translation>Aşağı</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="135"/>
        <source>90° arrow-left</source>
        <translation>90° sola</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="168"/>
        <source>frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="228"/>
        <source>screen zoom</source>
        <translation type="unfinished">Ekran Yakınlaştırma:</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <source>refresh rate</source>
        <translation type="vanished">Yenileme Oranı</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="393"/>
        <source>auto</source>
        <translation>Oto</translation>
    </message>
    <message>
        <source>aa</source>
        <translation type="vanished">aa</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <source>Update</source>
        <translation type="vanished">Güncelle</translation>
    </message>
    <message>
        <source>System Update</source>
        <translation type="vanished">Sistem Güncelle</translation>
        <extra-contents_path>/update/System Update</extra-contents_path>
    </message>
    <message>
        <source>Last check time:</source>
        <translation type="vanished">Son kontrol zamanı:</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation type="obsolete">Güncellemeleri kontrol et</translation>
    </message>
    <message>
        <source>CheckUpdate</source>
        <translation type="vanished">Güncelleme Kontrolü</translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <source>userinfo</source>
        <translation type="vanished">Kullanıcı</translation>
    </message>
    <message>
        <source>Userinfo</source>
        <translation type="vanished">Kullanıcı</translation>
    </message>
    <message>
        <source>User Info</source>
        <translation type="obsolete">Kullanıcı Bilgisi</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">Standart Kullanıcı</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">Yönetici</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1202"/>
        <source>root</source>
        <translation>Root</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Sil</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1198"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1200"/>
        <source>Admin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="obsolete">Sil</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="160"/>
        <source>CurrentUser</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Userinfo/CurrentUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="162"/>
        <source>OthersUser</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Userinfo/OthersUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="194"/>
        <source>Groups</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Userinfo/Groups</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="249"/>
        <source>LoginWithoutPwd</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Userinfo/LoginWithoutPwd</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="271"/>
        <source>AutoLoginOnBoot</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Userinfo/AutoLoginOnBoot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="306"/>
        <source>Add</source>
        <translation type="unfinished">Ekle</translation>
        <extra-contents_path>/Userinfo/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>Warning</source>
        <translation type="unfinished">Uyarı</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>The user is logged in, please delete the user after logging out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1075"/>
        <source>The account type of “%1” has been modified, will take effect after logout, whether to logout?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1076"/>
        <source>logout later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1077"/>
        <source>logout now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1170"/>
        <source>Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1171"/>
        <source>The system only allows one user to log in automatically.After it is turned on, the automatic login of other users will be turned off.Is it turned on?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1174"/>
        <source>Trun on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1175"/>
        <source>Close on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="41"/>
        <source>Current User</source>
        <translation>Mevcut Kullanıcı</translation>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="vanished">Parola Değiştir</translation>
    </message>
    <message>
        <source>Change type</source>
        <translation type="vanished">Tür Değiştir</translation>
    </message>
    <message>
        <source>Change valid</source>
        <translation type="vanished">Geçerli olanı değiştir</translation>
        <extra-contents_path>/userinfo/Change valid</extra-contents_path>
    </message>
    <message>
        <source>User group</source>
        <translation type="obsolete">Kullanıcı Grubu</translation>
    </message>
    <message>
        <source>Change vaild</source>
        <translation type="vanished">Geçerli Değişiklik</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="309"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="182"/>
        <source>Password</source>
        <translation type="unfinished">Parola</translation>
        <extra-contents_path>/Userinfo/Password</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="331"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="188"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Userinfo/Type</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="353"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="422"/>
        <source>Login no passwd</source>
        <translation>Şifresiz giriş</translation>
    </message>
    <message>
        <source>enable autoLogin</source>
        <translation type="vanished">Otomatik giriş</translation>
        <extra-contents_path>/Userinfo/enable autoLogin</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="500"/>
        <source>Automatic login at boot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Currently in Live mode, please create a new user and log out</source>
        <translation type="obsolete">Şu anda Canlı modda, lütfen yeni bir kullanıcı oluşturun ve oturumu kapatın</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="568"/>
        <source>Other Users</source>
        <translation>Diğer Kullanıcılar</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="vanished">Yeni Kullanıcı Ekle</translation>
    </message>
</context>
<context>
    <name>UserInfoIntel</name>
    <message>
        <source>Current User</source>
        <translation type="obsolete">Mevcut Kullanıcı</translation>
        <extra-contents_path>/UserinfoIntel/Current User</extra-contents_path>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="obsolete">Parola Değiştir</translation>
        <extra-contents_path>/UserinfoIntel/Change pwd</extra-contents_path>
    </message>
    <message>
        <source>User group</source>
        <translation type="obsolete">Kullanıcı Grubu</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="obsolete">Diğer Kullanıcılar</translation>
        <extra-contents_path>/UserinfoIntel/Other Users</extra-contents_path>
    </message>
    <message>
        <source>administrator</source>
        <translation type="obsolete">Yönetici</translation>
    </message>
    <message>
        <source>root</source>
        <translation type="obsolete">Root</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="obsolete">Yeni Kullanıcı Ekle</translation>
    </message>
</context>
<context>
    <name>UtilsForUserinfo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="36"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="obsolete">Sil</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="32"/>
        <source>Password</source>
        <translation type="unfinished">Parola</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="40"/>
        <source>Delete</source>
        <translation type="unfinished">Sil</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="218"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="220"/>
        <source>Admin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vino</name>
    <message>
        <location filename="../../../plugins/system/vino/vino.cpp" line="28"/>
        <source>Vino</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Vpn</name>
    <message>
        <source>Add Vpn Connect</source>
        <translation type="vanished">Vpn Bağlantısı Ekle</translation>
    </message>
    <message>
        <source>Add vpn connect</source>
        <translation type="vanished">Vpn Bağlantısı Ekle</translation>
        <extra-contents_path>/Vpn/Add vpn connect</extra-contents_path>
    </message>
    <message>
        <source>vpn</source>
        <translation type="vanished">VPN</translation>
    </message>
    <message>
        <source>Vpn</source>
        <translation type="vanished">VPN</translation>
        <extra-contents_path>/Vpn/Vpn</extra-contents_path>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="103"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="151"/>
        <source>Desktop Background</source>
        <translation>Masaüstü Arkaplanı</translation>
        <extra-contents_path>/Wallpaper/Desktop Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="401"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="156"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Wallpaper/Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="532"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="158"/>
        <source>Local Pictures</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Wallpaper/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="539"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="160"/>
        <source>Online Pictures</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Wallpaper/Online Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="571"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="170"/>
        <source>Reset To Default</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Wallpaper/Reset To Default</extra-contents_path>
    </message>
    <message>
        <source>Select from</source>
        <translation type="vanished">Seç</translation>
        <extra-contents_path>/wallpaper/Select from</extra-contents_path>
    </message>
    <message>
        <source>Picture options</source>
        <translation type="vanished">Resim Ayarları</translation>
    </message>
    <message>
        <source>Browser local wp</source>
        <translation type="vanished">Bilgisayardan Seç</translation>
        <extra-contents_path>/wallpaper/Browser local wp</extra-contents_path>
    </message>
    <message>
        <source>Reset to default</source>
        <translation type="vanished">Varsayılana Dön</translation>
        <extra-contents_path>/wallpaper/Reset to default</extra-contents_path>
    </message>
    <message>
        <source>Browser online wp</source>
        <translation type="vanished">İnternetten Al</translation>
        <extra-contents_path>/wallpaper/Browser online wp</extra-contents_path>
    </message>
    <message>
        <source>Restore default wp</source>
        <translation type="vanished">Varsayılan wp&apos;yi geri yükle</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">Tamam</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="obsolete">Ekle</translation>
    </message>
    <message>
        <source>background</source>
        <translation type="vanished">Arkaplan</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="331"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="58"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="154"/>
        <source>Background</source>
        <translation>Arkaplan</translation>
        <extra-contents_path>/Wallpaper/Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>picture</source>
        <translation>Resim</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>color</source>
        <translation>Renk</translation>
    </message>
    <message>
        <source>Add custom shortcut</source>
        <translation type="vanished">Özel Kısayol Ekle</translation>
    </message>
    <message>
        <source>Custom color</source>
        <translation type="vanished">Özel Renk</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>wallpaper</source>
        <translation>Duvar Kağıdı</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>centered</source>
        <translation>Ortalanmış</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>scaled</source>
        <translation>Döşe</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>stretched</source>
        <translation>Uzatılmış</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>zoom</source>
        <translation>Yakınlaştır</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>spanned</source>
        <translation>Yayılmış</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Blue cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Pine green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Emerald green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Dark cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Slate green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Mineral green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Taupe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Dark brown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Aurantiacus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Brick-red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Rose red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Purplish red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Dark magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Purple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Violet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Medium purple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Grey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="620"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation type="unfinished">Duvarkağıdı Dosyaları(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</translation>
    </message>
    <message>
        <source>allFiles(*.*)</source>
        <translation type="obsolete">Tüm Dosyalar(*.*)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="660"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="703"/>
        <source>select custom wallpaper file</source>
        <translation>Özel duvar kağıdı dosyasını seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="661"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="704"/>
        <source>Select</source>
        <translation>Seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="662"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="705"/>
        <source>Position: </source>
        <translation>Konum: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="663"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="706"/>
        <source>FileName: </source>
        <translation>Dosya Adı: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="664"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="707"/>
        <source>FileType: </source>
        <translation>Dosya Türü: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="665"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="708"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>screen zoom </source>
        <translation type="vanished">Ekranı Yaklaştır </translation>
    </message>
    <message>
        <source>unify output</source>
        <translation type="obsolete">Çıktıyı Birleştir</translation>
    </message>
    <message>
        <source>night mode</source>
        <translation type="vanished">Gece Modu</translation>
        <extra-contents_path>/Display/night mode</extra-contents_path>
    </message>
    <message>
        <source>screen zoom</source>
        <translation type="obsolete">Ekran Yakınlaştırma:</translation>
        <extra-contents_path>/display/screen zoom</extra-contents_path>
    </message>
    <message>
        <source>Some applications need to be logouted to take effect</source>
        <translation type="vanished">Bazı uygulamaların aktif olması için oturum açılması gerekir</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2348"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2375"/>
        <source>Open</source>
        <translation type="unfinished">Aç</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="414"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="431"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Custom Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="442"/>
        <source>to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="397"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="469"/>
        <source>Color Temperature</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Display/Color Temperature</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="473"/>
        <source>Warmer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="476"/>
        <source>Colder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="793"/>
        <source>Multi-screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="798"/>
        <source>First Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="801"/>
        <source>Clone Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="800"/>
        <source>Extend Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="799"/>
        <source>Vice Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>monitor</source>
        <translation type="obsolete">Ekran:</translation>
        <extra-contents_path>/display/monitor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1297"/>
        <source>Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1091"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after 14 seconds&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="340"/>
        <source>Color Temperature And Eye Care</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Display/Color Temperature And Eye Care</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="371"/>
        <source>Eye Protection Mode</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Display/Eye Protection Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="372"/>
        <source>When turned on, it can reduce blue light to prevent eye, the screen will turn yellow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="845"/>
        <source>Primary Screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1069"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1070"/>
        <source>Not Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1102"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after %2 seconds&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1299"/>
        <source>The zoom has been modified, it will take effect after you log off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1318"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1328"/>
        <source>(Effective after logout)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2264"/>
        <source>are you sure to apply?
1 Select &quot;apply&quot;, manually log out late
2 Select &quot;log out to apply&quot;, log out now to apply
3 Select &quot;cancel&quot;, cancel to apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>apply</source>
        <translation type="unfinished">Uygula</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>log out to apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2347"/>
        <source>Turning on &apos;Eye Protection Mode&apos; will turn off &apos;Color Temperature&apos;. Continue turning it on?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2349"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2376"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2374"/>
        <source>Turning on &apos;Color Temperature&apos; will turn off &apos;Eye Protection Mode&apos;. Continue turning it on?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>All Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2713"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2727"/>
        <source>Brightness</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Display/Brightness</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>please insure at least one output!</source>
        <translation>Lütfen en az bir çıktı alın!</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="805"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="807"/>
        <source>Network Display</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Display/Network Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="840"/>
        <source>Display</source>
        <translation type="unfinished">Ekran</translation>
        <extra-contents_path>/Display/Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1018"/>
        <source>Auto Brightness</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Display/Auto Brightness</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1078"/>
        <source>resolution</source>
        <translation type="unfinished">Çözünürlük:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1081"/>
        <source>orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1084"/>
        <source>frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1087"/>
        <source>scale</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1300"/>
        <source>Log out now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1301"/>
        <source>Later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1692"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1792"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>Warning</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1693"/>
        <source>Open time should be earlier than close time!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Follow the sunrise and sunset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Morning time should be earlier than evening time!</source>
        <translation type="vanished">Sabah vakti akşam saatinden daha erken olmalı!</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1793"/>
        <source>Sorry, your configuration could not be applied.
Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU.</source>
        <translation>Maalesef, yapılandırmanız uygulanamadı. 
Genel nedenler, toplam ekran boyutunun çok büyük olması veya GPU&apos;nuz tarafından desteklenenden daha fazla ekran etkinleştirmenizdir.</translation>
    </message>
    <message>
        <source>@title:window</source>
        <comment>Unsupported Configuration</comment>
        <translation type="vanished">Desteklenmeyen Yapılandırma</translation>
    </message>
    <message>
        <source>Some applications need to be restarted to take effect</source>
        <translation type="vanished">Bazı uygulamaların etkili olması için yeniden başlatılması gerekiyor</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="obsolete">%1</translation>
    </message>
</context>
<context>
    <name>WlanConnect</name>
    <message>
        <source>Advanced settings</source>
        <translation type="obsolete">Gelişmiş Ayarlar</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>Network settings</source>
        <translation type="obsolete">Ağ Ayarları</translation>
        <extra-contents_path>/wlanconnect/Network settings&quot;</extra-contents_path>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Bağlan</translation>
    </message>
    <message>
        <source>connected</source>
        <translation type="obsolete">Bağlandı</translation>
    </message>
</context>
<context>
    <name>addShortcutDialog</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Shortcut name</source>
        <translation type="vanished">Kısayol Adı</translation>
    </message>
    <message>
        <source>Shortcut exec</source>
        <translation type="vanished">Kısayol komutu</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="88"/>
        <source>Exec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="126"/>
        <source>Open</source>
        <translation>Aç</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="174"/>
        <source>Name</source>
        <translation type="unfinished">İsim</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="248"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="222"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="305"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid executable, please re-enter</source>
        <translation type="vanished">Geçersiz yürütülebilir dosya, lütfen tekrar girin</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="354"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="275"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="373"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">Belli</translation>
    </message>
    <message>
        <source>Add custom shortcut</source>
        <translation type="vanished">Özel kısayol ekle</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="86"/>
        <source>Add Shortcut</source>
        <translation type="unfinished">Kısayol Ekle</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="103"/>
        <source>Please enter a shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="233"/>
        <source>Desktop files(*.desktop)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="274"/>
        <source>select desktop</source>
        <translation>Masaüstü seç</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="329"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="348"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="357"/>
        <source>Invalid application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="331"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="344"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="353"/>
        <source>Shortcut conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="333"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="346"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="355"/>
        <source>Invalid shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="336"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="341"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="360"/>
        <source>Name repetition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="364"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="542"/>
        <source>Shortcut cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="546"/>
        <source>Name cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="556"/>
        <source>Desktop prohibits adding</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>area_code_lineedit</name>
    <message>
        <source>Sign up by Phone</source>
        <translation type="vanished">Telefonla Kaydolun</translation>
    </message>
</context>
<context>
    <name>changeUserGroup</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="31"/>
        <source>user group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="120"/>
        <source>Group:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="133"/>
        <source>GID:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="147"/>
        <source>GNum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="191"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="617"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="194"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="618"/>
        <source>Confirm</source>
        <translation type="unfinished">Onayla</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Invalid Id!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="575"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="583"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="659"/>
        <source>OK</source>
        <translation type="unfinished">Tamam</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <source>Invalid Group Name!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="615"/>
        <source>Whether delete the group: “%1” ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="616"/>
        <source>which will make some file components in the file system invalid!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>changtimedialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="32"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="115"/>
        <source>current date</source>
        <translation>Güncel tarih</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="200"/>
        <source>time</source>
        <translation>Saat</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="321"/>
        <source>year</source>
        <translation>Yıl</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="398"/>
        <source>month</source>
        <translation>Ay</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="472"/>
        <source>day</source>
        <translation>Gün</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="574"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="593"/>
        <source>confirm</source>
        <translation>Doğrula</translation>
    </message>
</context>
<context>
    <name>config_list_widget</name>
    <message>
        <source>wallpaper</source>
        <translation type="vanished">Duvarkağıdı</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">Ayarlarınızı senkronize edin</translation>
    </message>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">Hesabınız:%1</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Çıkış</translation>
    </message>
    <message>
        <source>Sync</source>
        <translation type="vanished">Senkronize</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation type="vanished">Oturum aç</translation>
    </message>
    <message>
        <source>Stop sync</source>
        <translation type="vanished">Senkronizasyonu durdur</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">Otomatik senkronizasyon</translation>
    </message>
    <message>
        <source>Synchronize your personalized settings and data</source>
        <translation type="vanished">Kişiselleştirilmiş ayarlarınızı ve verilerinizi senkronize edin</translation>
    </message>
    <message>
        <source>Login Cloud to get a better experience</source>
        <translation type="vanished">Daha iyi bir deneyim için Cloud&apos;a giriş yapın</translation>
    </message>
    <message>
        <source>Sign in/Sign up</source>
        <translation type="vanished">Giriş yap/Kayıt Ol</translation>
    </message>
    <message>
        <source>You must sign in when you attempt to sync your settings.</source>
        <translation type="vanished">Ayarlarınızı eşitlemeye çalıştığınızda oturum açmalısınız.。</translation>
    </message>
    <message>
        <source>Your account：%1</source>
        <translation type="vanished">Hesabınız:%1</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">Bağlantı Kesildi</translation>
    </message>
</context>
<context>
    <name>item_list</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">Menü</translation>
    </message>
    <message>
        <source>Quick Start</source>
        <translation type="vanished">Hızlı Başlat</translation>
    </message>
    <message>
        <source>Tab</source>
        <translation type="vanished">Sekme</translation>
    </message>
    <message>
        <source>ScreenSaver</source>
        <translation type="vanished">Ekran Koruyucu</translation>
    </message>
    <message>
        <source>User Profile</source>
        <translation type="vanished">Kullanıcı Profili</translation>
    </message>
    <message>
        <source>Weather</source>
        <translation type="vanished">Hava Durumu</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="vanished">Medya</translation>
    </message>
    <message>
        <source>Walpaper</source>
        <translation type="vanished">Duvar Kağıdı</translation>
    </message>
</context>
<context>
    <name>ksc_main_page_widget</name>
    <message>
        <source>Run Security Center</source>
        <translation type="obsolete">Güvenlik Merkezi&apos;ni çalıştırın</translation>
    </message>
</context>
<context>
    <name>ksc_module_func_widget</name>
    <message>
        <source>Network Protection</source>
        <translation type="obsolete">Ağ Koruması</translation>
        <extra-contents_path>/securitycenter/Network Protection</extra-contents_path>
    </message>
</context>
<context>
    <name>networkaccount</name>
    <message>
        <source>Cloud Account</source>
        <translation type="obsolete">Bulut Hesabı</translation>
    </message>
</context>
<context>
    <name>ql_pushbutton_edit</name>
    <message>
        <source>Reset</source>
        <translation type="vanished">Sıfırla</translation>
    </message>
</context>
</TS>
