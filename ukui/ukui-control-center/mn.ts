<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>About</name>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="614"/>
        <source>System Summary</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠢᠮᠤ</translation>
        <extra-contents_path>/About/System Summary</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="615"/>
        <source>Support</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ / ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="227"/>
        <source>Version Number</source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
        <extra-contents_path>/About/Version Number</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="684"/>
        <source>Status</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠪᠠᠢᠳᠠᠯ</translation>
        <extra-contents_path>/About/Status</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="689"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1412"/>
        <source>DateRes</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠳᠠᠭᠤᠰᠬᠤ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="618"/>
        <source>Wechat code scanning obtains HP professional technical support</source>
        <translation>ᠸᠢᠴᠠᠲ ᠵᠢᠡᠷ ᠰᠢᠷᠪᠢᠵᠤ HP ᠳᠤᠰᠬᠠᠢ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠬᠦᠷᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="308"/>
        <source>HostName</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
        <extra-contents_path>/About/HostName</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="619"/>
        <source>See more about Kylin Tianqi edu platform</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠲᠢᠶᠠᠨ ᠴᠢ ᠰᠤᠷᠭᠠᠨ ᠬᠥᠮᠦᠵᠢᠯ ᠤ᠋ᠨ ᠳᠠᠪᠴᠠᠩ ᠡᠴᠡ ᠨᠡᠩ ᠣᠯᠠᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠣᠯᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="527"/>
        <source>&lt;&lt;Protocol&gt;&gt;</source>
        <translation>&lt;&lt; ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠡᠴᠡ ᠬᠡᠯᠳᠦᠷᠢᠬᠦᠯᠬᠦ ᠭᠡᠷ᠎ᠡ &gt;&gt;</translation>
        <extra-contents_path>/About/&lt;&lt;Protocol&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="66"/>
        <source>About and Support</source>
        <translation>ᠲᠤᠬᠠᠢ ᠬᠢᠬᠡᠳ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <source>InterVersion</source>
        <translation type="vanished">内部版本</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="247"/>
        <source>Patch Version</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠨᠦᠬᠦᠬᠡᠰᠦ ᠵᠢᠨ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ</translation>
        <extra-contents_path>/About/Patch Version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="266"/>
        <source>Installed Date</source>
        <translation>ᠰᠠᠭᠤᠯᠭᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="285"/>
        <source>Upgrade Date</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="451"/>
        <source>Privacy and agreement</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠬᠢᠬᠡᠳ ᠭᠡᠷ᠎ᠡ</translation>
        <extra-contents_path>/About/Privacy and agreement</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="463"/>
        <source>Send optional diagnostic data</source>
        <translation>ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠬᠤ ᠤᠨᠤᠰᠢᠯᠠᠭᠠᠨ ᠤ᠋ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠢᠯᠡᠬᠡᠬᠦ</translation>
        <extra-contents_path>/About/Send optional diagnostic data</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="465"/>
        <source>By sending us diagnostic data, improve the system experience and solve your problems faster</source>
        <translation>ᠪᠢᠳᠡᠨ ᠳ᠋ᠤ᠌ ᠣᠨᠣᠰᠢᠯᠠᠬᠤ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠢᠯᠡᠬᠡᠬᠦ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠮᠡᠳᠡᠷᠡᠮᠵᠢ ᠵᠢ ᠳᠡᠭᠡᠭᠰᠢᠯᠡᠬᠦᠯᠵᠤ᠂ ᠲᠠᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠨᠡᠩ ᠳᠦᠷᠭᠡᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="528"/>
        <source>and</source>
        <translation>ᠪᠤᠯᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="530"/>
        <source>&lt;&lt;Privacy&gt;&gt;</source>
        <translation>&lt;&lt; ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠵᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠭᠡᠷ᠎ᠡ&gt;&gt;</translation>
        <extra-contents_path>/About/&lt;&lt;Privacy&gt;&gt;</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="638"/>
        <source>Learn more HP user manual&gt;&gt;</source>
        <translation>ᠨᠡᠩ ᠣᠯᠠᠨ HP ᠢ᠋/ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="646"/>
        <source>See user manual&gt;&gt;</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Not activated (trial period)</source>
        <translation type="vanished">未激活(试用期)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="749"/>
        <source>Trial expiration time</source>
        <translation>ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠳᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1154"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1431"/>
        <source>expired</source>
        <translation>ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠦᠩᠬᠡᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="770"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1156"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1414"/>
        <source>Extend</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠵᠢ ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="516"/>
        <source>Copyright © 2020 KylinSoft. All rights reserved.</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠡᠷᠬᠡ © 2020 ᠴᠢ ᠯᠢᠨ ᠰᠤᠹᠲ ᠲᠤ᠌ ᠬᠠᠷᠢᠶᠠᠯᠠᠭᠳᠠᠨ᠎ᠠ᠂ ᠪᠦᠬᠦ ᠡᠷᠬᠡᠵᠢ ᠦᠯᠡᠳᠡᠬᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1385"/>
        <source>The system needs to be restarted to set the HostName, whether to reboot</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢᠭᠡᠨᠳᠡ ᠵᠠᠰᠠᠭᠳᠠᠪᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠃ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠵᠢ ᠵᠦᠪᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1386"/>
        <source>Reboot Now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1387"/>
        <source>Reboot Later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="999"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1008"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1560"/>
        <source>avaliable</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="672"/>
        <source>Version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
        <extra-contents_path>/About/version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="839"/>
        <source>Kylin Linux Desktop V10 (SP1)</source>
        <translation>ᠶᠢᠨ ᠾᠧ ᠴᠢ ᠯᠢᠨ ᠵᠢᠯᠤᠭᠤᠳᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ V10（SP1）</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="674"/>
        <source>Kernel</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠴᠦᠮ᠎ᠡ</translation>
        <extra-contents_path>/About/Kernel</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="676"/>
        <source>CPU</source>
        <translation>CPU</translation>
        <extra-contents_path>/About/CPU</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="678"/>
        <source>Memory</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠤᠷ</translation>
        <extra-contents_path>/About/Memory</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="616"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1006"/>
        <source>Disk</source>
        <translation>ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="680"/>
        <source>Desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
        <extra-contents_path>/About/Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="682"/>
        <source>User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
        <extra-contents_path>/About/User</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="688"/>
        <source>Serial</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ</translation>
        <extra-contents_path>/About/Serial</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="686"/>
        <location filename="../../../plugins/system/about/about.cpp" line="751"/>
        <location filename="../../../plugins/system/about/about.cpp" line="757"/>
        <source>Active</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠡᠬᠢᠯᠡᠬᠦᠯᠪᠡ</translation>
        <extra-contents_path>/About/Active</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="68"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="747"/>
        <location filename="../../../plugins/system/about/about.cpp" line="755"/>
        <source>Inactivated</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="768"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1411"/>
        <source>Activated</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
</context>
<context>
    <name>Accessibility</name>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="32"/>
        <source>Vision</source>
        <translation>ᠬᠠᠷᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="131"/>
        <source>Enable magnifying glass: Enlarge the content of the desktop</source>
        <translation>ᠶᠡᠬᠡᠰᠬᠡᠬᠦ ᠰᠢᠯ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠄ ᠰᠢᠷᠡᠭᠡᠨ ᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠶᠢ ᠶᠡᠬᠡᠰᠬᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="138"/>
        <source>Press Win + &quot;+&quot; to zoom in on the content, Win + &quot;-&quot; to zoom out on the content.</source>
        <translation>ᠸᠢᠨ + &quot; + &quot; ᠪᠠᠷ ᠶᠡᠬᠡᠰᠬᠡᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠷ ᠸᠢᠳᠢ + &quot; —&quot; ᠪᠠᠷ ᠪᠠᠭᠠᠰᠬᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="225"/>
        <source>Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="315"/>
        <source>Color Filter Effect</source>
        <translation>ᠥᠩᠭᠡ ᠶᠢᠨ ᠱᠦᠭᠦᠬᠦ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="415"/>
        <source>Color Filter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="506"/>
        <source>Press Win + Ctrl + C to turn on/off color effect</source>
        <translation>Winn+Ctrl+ C ᠶᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠬᠠᠭᠠᠬᠤ ᠥᠩᠭᠡ ᠶᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="529"/>
        <source>Other Settings</source>
        <translation>ᠪᠤᠰᠤᠳ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="585"/>
        <source>Point Size</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="604"/>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="659"/>
        <source>Set</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.ui" line="640"/>
        <source>Vocal Tract Regulation</source>
        <translation>ᠳᠠᠭᠤᠨ ᠢᠶᠠᠷ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="10"/>
        <source>Accessibility</source>
        <translation>ᠰᠠᠭᠠᠳ ᠥᠬᠡᠢ ᠣᠷᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="80"/>
        <source>Window Zoom</source>
        <translation>ᠴᠣᠩᠬᠣ ᠪᠠᠷ ᠠᠲᠤᠷᠢᠭᠤᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="81"/>
        <source>Full Screen Zoom</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="83"/>
        <source>Red/Green Filter (Protanopia)</source>
        <translation>ᠤᠯᠠᠭᠠᠨ ᠥᠩᠭᠡ / ᠨᠣᠭᠣᠭᠠᠨ ᠱᠦᠭᠦᠬᠦ ᠭᠡᠷᠡᠯ ᠦᠨ ᠹᠢᠯᠢᠮ ( ᠫᠷᠤᠲ᠋ᠠᠲ᠋ᠧᠷ ᠫᠣᠫᠠᠰ )</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="84"/>
        <source>Green/Red Filter (Deuteranopia)</source>
        <translation>ᠨᠣᠭᠣᠭᠠᠨ ᠥᠩᠭᠡ / ᠤᠯᠠᠭᠠᠨ ᠥᠩᠭᠡ ᠶᠢᠨ ᠱᠦᠭᠦᠬᠦ ᠭᠡᠷᠡᠯ ᠦᠨ ᠹᠢᠯᠢᠮ ( Deuteranoia )</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="85"/>
        <source>Blue/Yellow Filter (Tritanopia)</source>
        <translation>ᠬᠦᠬᠡ ᠥᠩᠭᠡ / ᠰᠢᠷ᠎ᠠ ᠥᠩᠭᠡ ᠶᠢᠨ ᠱᠦᠭᠦᠬᠦ ᠹᠢᠯᠢᠮ (Tritanoopia)</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="86"/>
        <source>Grayscale</source>
        <translation>ᠴᠠᠢᠷᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/accessibility/accessibility.cpp" line="87"/>
        <source>Invert</source>
        <translation>ᠤᠷᠪᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>AddAutoBoot</name>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="123"/>
        <source>Name</source>
        <translation>ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="124"/>
        <source>Exec</source>
        <translation>ᠫᠷᠣᠭ᠌ᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="125"/>
        <source>Comment</source>
        <translation>ᠲᠠᠢᠯᠪᠤᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="200"/>
        <source>Select AutoStart Desktop</source>
        <translation>ᠥᠪᠡᠷ᠎ᠢ᠋ᠶ᠋ᠡᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="202"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="193"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ (*.desktop)</translation>
    </message>
    <message>
        <source>Select Autoboot Desktop</source>
        <translation type="vanished">ᠥᠪᠡᠷ᠎ᠢ᠋ᠶ᠋ᠡᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>select autoboot desktop</source>
        <translation type="vanished">ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠫᠷᠣᠭ᠌ᠷᠠᠮ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="201"/>
        <source>Select</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="248"/>
        <source>desktop file not allowed add</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠨᠡᠮᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="297"/>
        <source>desktop file not exist</source>
        <translation>desktop ᠹᠠᠢᠯ ᠣᠷᠣᠰᠢᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>AddAutoStart</name>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="177"/>
        <source>Add AutoStart program</source>
        <translation>ᠥᠪᠡᠷ ᠢ ᠪᠡᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ ᠨᠡᠮᠡᠵᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="178"/>
        <source>Name</source>
        <translation>ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="179"/>
        <source>Exec</source>
        <translation>ᠫᠷᠣᠭ᠌ᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="180"/>
        <source>Comment</source>
        <translation>ᠲᠠᠢᠯᠪᠤᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="181"/>
        <source>Open</source>
        <translation>Уламжлалт хятад</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="182"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="183"/>
        <source>Certain</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>AddBtn</name>
    <message>
        <location filename="../../../libukcc/widgets/AddBtn/addbtn.cpp" line="23"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>AddButton</name>
    <message>
        <location filename="../../../libukcc/widgets/SettingWidget/addbutton.cpp" line="25"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>AddInputMethodDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="26"/>
        <source>Select the input method to add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ ᠪᠡᠷ ᠪᠠᠢᠭ᠎ᠠ ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="82"/>
        <source>No</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="101"/>
        <source>Yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠳᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>Tibetan</source>
        <translation>ᠲᠦᠪᠡᠳ ᠬᠡᠯᠡ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="7"/>
        <source>With ASCII numbers</source>
        <translation>ASCII ᠠᠭᠤᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠲᠣᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="15"/>
        <source>Input Method</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>AddLanguageDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="198"/>
        <source>Yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="179"/>
        <source>No</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="22"/>
        <source>Add Language</source>
        <translation>ᠦᠭᠡ ᠬᠡᠯᠡ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="23"/>
        <source>Search</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠡᠷᠢᠬᠦ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠡᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>Area</name>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="26"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="44"/>
        <source>Area</source>
        <translation>ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠦᠭᠡ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="156"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="615"/>
        <source>Regional Format</source>
        <translation>ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="427"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="622"/>
        <source>Short Format Date</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠤ᠋ ᠣᠬᠣᠷ ᠹᠤᠷᠮᠠᠲ</translation>
        <extra-contents_path>/Area/Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="510"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="627"/>
        <source>Long Format Date</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠤ᠋ ᠤᠷᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
        <extra-contents_path>/Area/Long Format Date</extra-contents_path>
    </message>
    <message>
        <source>Current Region</source>
        <translation type="vanished">ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ᠂ ᠴᠠᠭ ᠪᠤᠯᠤᠨ ᠵᠣᠭᠣᠰ ᠮᠦᠩᠬᠦᠨ ᠤ᠋ ᠵᠠᠭᠪᠤᠷ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠪᠦᠰᠡ</translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="344"/>
        <source>First Day Of The Week</source>
        <translation>ᠨᠢᠭᠡ ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="258"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="617"/>
        <source>Calendar</source>
        <translation>ᠴᠠᠭ ᠤᠯᠠᠷᠢᠯ ᠤ᠋ᠨ ᠪᠢᠴᠢᠭ</translation>
        <extra-contents_path>/Area/Calendar</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="59"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="236"/>
        <source>Language Format</source>
        <translation>ᠦᠭᠡ ᠬᠡᠯᠡᠨ ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
        <extra-contents_path>/Area/Regional Format</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="624"/>
        <source>Date</source>
        <translation>ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠠ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="593"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="629"/>
        <source>Time</source>
        <translation>ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
        <extra-contents_path>/Area/Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="676"/>
        <source>Language Format Example</source>
        <translation>ᠬᠡᠯᠡᠨ ᠦ ᠵᠠᠭᠪᠤᠷ ᠤᠨ ᠵᠢᠱᠢᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="768"/>
        <source>TextLabel</source>
        <translation>ᠲᠧᠺᠰᠲ ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>lunar</source>
        <translation type="vanished">ᠪᠢᠯᠢᠭ ᠤ᠋ᠨ ᠲᠣᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>US</source>
        <translation type="vanished">ᠠᠮᠸᠷᠢᠺᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="241"/>
        <source>Language for system windows,menus and web pages</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠴᠣᠩᠬᠣ᠂ ᠲᠣᠪᠶᠣᠭ ᠪᠠ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ᠌ ᠨᠢᠭᠤᠷ᠎ᠲᠤ᠌ ᠢᠯᠡᠷᠡᠭᠰᠡᠨ ᠦᠭᠡ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="734"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="238"/>
        <source>System Language</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠦᠭᠡ ᠬᠡᠯᠡ</translation>
        <extra-contents_path>/Area/system language</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="172"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="677"/>
        <source>MMMM dd, yyyy</source>
        <translation>yyyy ᠣᠨ᠎ᠤ᠋ MM ᠰᠠᠷ᠎ᠠ ᠵᠢᠨ dd ᠤ᠋ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="175"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="680"/>
        <source>MMMM d, yy</source>
        <translation>yy ᠤᠨ ᠤ᠋ M ᠰᠠᠷ᠎ᠠ ᠵᠢᠨ d ᠤ᠋ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="247"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
        <extra-contents_path>/Area/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="362"/>
        <source>English  (US)</source>
        <translation>ᠠᠩᠭ᠌ᠯᠢ ᠬᠡᠯᠡ (᠎ᠠᠮᠧᠷᠢᠺᠠ )</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="363"/>
        <source>Simplified Chinese  (CN)</source>
        <translation>ᠤᠯᠠᠮᠵᠢᠯᠠᠯᠳᠤ ᠬᠢᠲᠠᠳ ᠬᠡᠯᠡ (᠎ᠳᠤᠮᠳᠠᠳᠤ ᠤᠯᠤᠰ )</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="364"/>
        <source>Tibetan  (CN)</source>
        <translation>ᠲᠥᠪᠡᠳ ᠬᠡᠯᠡ (᠎ᠳᠤᠮᠳᠠᠳᠤ ᠤᠯᠤᠰ )</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="379"/>
        <source>Monday</source>
        <translation>ᠭᠠᠷᠠᠭ᠎ᠤ᠋ᠨ ᠨᠢᠭᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="380"/>
        <source>Sunday</source>
        <translation>ᠭᠠᠷᠠᠭ᠎ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="619"/>
        <source>First Day Of Week</source>
        <translation>ᠨᠢᠭᠡ ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠡᠳᠦᠷ</translation>
        <extra-contents_path>/Area/First Day Of Week</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="631"/>
        <source>Solar Calendar</source>
        <translation>ᠠᠷᠭ᠎ᠠ ᠵᠢᠨ ᠤᠯᠠᠷᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="634"/>
        <source>Lunar</source>
        <translation>ᠪᠢᠯᠢᠭ ᠤ᠋ᠨ ᠤᠯᠠᠷᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="650"/>
        <source>12 Hours</source>
        <translation>12 ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="651"/>
        <source>24 Hours</source>
        <translation>24 ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="927"/>
        <source>Modify the first language need to restart to take effect, whether to restart?</source>
        <translation>ᠲᠦᠷᠦᠭᠦᠦ ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠬᠡᠯᠡ᠎ᠶ᠋ᠢ ᠵᠠᠰᠠᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠪᠡᠯ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠡᠰᠡᠬᠦ ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="928"/>
        <source>Restart later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="929"/>
        <source>Restart now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="923"/>
        <source>Modify the current region need to logout to take effect, whether to logout?</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠵᠠᠰᠠᠪᠠᠯ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="924"/>
        <source>Logout later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="925"/>
        <source>Logout now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Modify the first language need to reboot to take effect, whether to reboot?</source>
        <translation type="vanished">ᠡᠬᠢᠨ ᠤ᠋ ᠦᠭᠡ ᠬᠡᠯᠡ ᠵᠢ ᠵᠠᠰᠠᠪᠠᠯ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠤᠤ ?</translation>
    </message>
    <message>
        <source>Reboot later</source>
        <translation type="vanished">ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Reboot now</source>
        <translation type="vanished">ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>2019/12/17</source>
        <translation type="vanished">2019/12/17</translation>
    </message>
    <message>
        <source>9:52</source>
        <translation type="vanished">9:52</translation>
    </message>
    <message>
        <source>English</source>
        <translation type="vanished">English</translation>
    </message>
    <message>
        <source>solar calendar</source>
        <translation type="vanished">ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠤᠨ ᠳᠤᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>monday</source>
        <translation type="vanished">ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠨᠢᠭᠡᠨ</translation>
    </message>
    <message>
        <source>sunday</source>
        <translation type="vanished">ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠡᠳᠦᠷ</translation>
    </message>
</context>
<context>
    <name>AutoBoot</name>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="466"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ (*.desktop)</translation>
    </message>
    <message>
        <source>select autoboot desktop</source>
        <translation type="vanished">ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠫᠷᠣᠭ᠌ᠷᠠᠮ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>Select Autoboot Desktop</source>
        <translation type="vanished">ᠥᠪᠡᠷ᠎ᠢ᠋ᠶ᠋ᠡᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="88"/>
        <source>Auto Start</source>
        <translation>ᠮᠠᠰᠢᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ </translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="474"/>
        <source>Select AutoStart Desktop</source>
        <translation>ᠥᠪᠡᠷ᠎ᠢ᠋ᠶ᠋ᠡᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="475"/>
        <source>Select</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="476"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="623"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
        <extra-contents_path>/AutoStart/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="630"/>
        <source>AutoStart Settings</source>
        <translation>ᠮᠠᠰᠢᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
        <extra-contents_path>/AutoStart/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Autoboot Settings</source>
        <translation type="vanished">ᠮᠠᠰᠢᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
        <extra-contents_path>/Autoboot/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <source>Auto Boot</source>
        <translation type="vanished">ᠮᠠᠰᠢᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="243"/>
        <source>Delete</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠵᠤ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Backup</name>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="53"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="119"/>
        <source>Backup</source>
        <translation>ᠨᠥᠭᠡᠴᠡ</translation>
        <extra-contents_path>/Backup/Backup</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="69"/>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted, 
you can restore them to ensure the integrity of your system.</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠤᠰᠤᠳ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠲᠤ᠌ ᠨᠦᠭᠡᠴᠡᠯᠡᠵᠤ᠂ ᠡᠬᠢ ᠹᠠᠢᠯ ᠠᠯᠳᠠᠭᠳᠠᠬᠤ᠂ ᠬᠣᠬᠢᠷᠠᠬᠤ᠂ ᠬᠠᠰᠤᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠡᠬᠡᠬᠦᠯᠵᠤ᠂
ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠪᠦᠷᠢᠨ ᠪᠦᠳᠦᠨ ᠴᠢᠨᠠᠷ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="113"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="115"/>
        <source>Begin backup</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="157"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="145"/>
        <source>Restore</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Backup/Restore</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="173"/>
        <source>View a list of backed-upfiles to backed up files to the system</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ᠂ ᠨᠦᠬᠡᠴᠡᠯᠡᠭᠰᠡᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="213"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="141"/>
        <source>Begin restore</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>All data stored on the computer will be permanently erased,and the system will revert to 
                                its original factory state when this operation is completed.</source>
        <translation type="vanished">ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠲᠤ᠌ ᠬᠢ ᠪᠦᠬᠦᠢᠯᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ ᠵᠢ ᠥᠨᠢᠳᠡ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠵᠣᠭᠰᠣᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ
ᠴᠡᠪᠡᠷᠯᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠡᠷᠬᠦᠬᠡᠳ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠠᠩᠬᠠᠨ ᠤ᠋ ᠦᠢᠯᠡᠳᠪᠦᠷᠢ ᠡᠴᠡ ᠭᠠᠷᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠪᠤᠴᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>System Recovery</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="45"/>
        <source>Backup Restore</source>
        <translation>ᠪᠡᠯᠡᠳᠬᠡᠯ ᠡᠭᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="121"/>
        <source>Back up your files to other drives and restore them when the source files are lost, damaged, or deleted to ensure the integrity of the system.</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠤᠰᠤᠳ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠲᠤ᠌ ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ᠂ ᠡᠬᠢ ᠹᠠᠢᠯ ᠠᠯᠳᠠᠭᠳᠠᠬᠤ᠂ ᠬᠣᠬᠢᠷᠠᠬᠤ᠂ ᠬᠠᠰᠤᠭᠳᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠳᠡᠳᠡᠬᠡᠷ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠪᠦᠷᠢᠨ ᠪᠦᠳᠦᠨ ᠴᠢᠨᠠᠷ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="147"/>
        <source>View the backup list and restore the backup file to the system</source>
        <translation>ᠨᠥᠭᠡᠴᠡ ᠵᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ᠂ ᠨᠥᠭᠡᠴᠡ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="105"/>
        <source>Backup and Restore</source>
        <translation>ᠨᠥᠭᠡᠴᠡ ᠵᠢ ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/Backup/Backup and Restore</extra-contents_path>
    </message>
</context>
<context>
    <name>Boot</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="10"/>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="80"/>
        <source>Boot</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠢ᠋ ᠡᠬᠢᠯᠡᠭᠦᠯᠬᠦ</translation>
        <extra-contents_path>/Boot/Boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="92"/>
        <source>Grub verify</source>
        <translation>Grub ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Boot/Grub verify</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="95"/>
        <source>Password required for Grub editing after enabling</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠦ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ᠎ᠦ᠌ ᠳᠠᠷᠠᠭ᠎ᠠ Grub ᠢ᠋/ ᠵᠢ ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠡᠷᠡᠭᠰᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/boot.cpp" line="97"/>
        <source>Reset password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠳᠠᠬᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BrightnessFrame</name>
    <message>
        <location filename="../../../plugins/system/display/brightnessFrame.cpp" line="41"/>
        <source>Failed to get the brightness information of this monitor</source>
        <translation>ᠲᠤᠰ ᠦᠵᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠡᠷᠡᠯᠳᠦᠴᠡ ᠵᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠣᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>ChangeFaceIntelDialog</name>
    <message>
        <source>Change User Face</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
        <extra-contents_path>/UserinfoIntel/Change User Face</extra-contents_path>
    </message>
    <message>
        <source>History</source>
        <translation type="vanished">ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠪᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>select custom face file</source>
        <translation type="vanished">ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠰᠣᠩᠭᠣᠬᠤ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="vanished">ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>Position: </source>
        <translation type="vanished">ᠪᠠᠢᠷᠢ: </translation>
    </message>
    <message>
        <source>FileName: </source>
        <translation type="vanished">ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <source>FileType: </source>
        <translation type="vanished">ᠹᠠᠢᠯ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ: </translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>The avatar is larger than 2M, please choose again</source>
        <translation type="vanished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ ᠨᠢ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ 2M ᠡᠴᠡ ᠶᠡᠬᠡ ᠂ ᠳᠠᠬᠢᠵᠤ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>ChangeGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>User Group Settings</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>User groups available in the system</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠬᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ</translation>
    </message>
    <message>
        <source>Add user group</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ChangePhoneIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>changephone</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Please input old phone num</source>
        <translation type="vanished">ᠬᠠᠭᠤᠴᠢᠨ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">ᠲᠸᠺᠰᠲ ᠱᠤᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>GetVerifyCode</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠺᠣᠳ᠋ ᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <source>submit</source>
        <translation type="vanished">ᠲᠤᠰᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Change Phone</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Phone number</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <source>SMS verification code</source>
        <translation type="vanished">ᠣᠬᠣᠷ ᠮᠡᠳᠡᠭᠡᠨ ᠤ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <source>Please input old phone number</source>
        <translation type="vanished">ᠬᠠᠭᠤᠴᠢᠨ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Next</source>
        <translation type="vanished">ᠳᠠᠷᠠᠭ᠎ᠠ ᠵᠢᠨ ᠠᠯᠬᠤᠮ</translation>
    </message>
    <message>
        <source>Please enter new mobile number</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Submit</source>
        <translation type="vanished">ᠲᠤᠰᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>changed success</source>
        <translation type="vanished">ᠵᠠᠰᠠᠪᠠ</translation>
    </message>
    <message>
        <source>You have successfully modified your phone</source>
        <translation type="vanished">ᠲᠠ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨ ᠰᠣᠯᠢᠪᠠ</translation>
    </message>
    <message>
        <source>Recapture</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Network connection failure, please check</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠰᠠᠭᠠᠳᠤᠯ ᠭᠠᠷᠪᠠ᠂ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>GetCode</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠺᠣᠳ᠋ ᠢ᠋ ᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Phone is lock,try again in an hour</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠨᠢᠰᠤᠯᠠᠭᠳᠠᠪᠠ᠂1 ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Phone code is wrong</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠺᠣᠳ᠋ ᠪᠤᠷᠤᠭᠤ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Current login expired,using wechat code!</source>
        <translation type="vanished">ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠥᠩᠭᠡᠷᠡᠪᠡ᠂ ᠸᠢᠴᠠᠲ ᠵᠢᠡᠷ ᠳᠠᠬᠢᠵᠤ ᠰᠢᠷᠪᠢᠭᠡᠳ ᠨᠡᠪᠳᠡᠷᠡᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <source>Unknown error, please try again later</source>
        <translation type="vanished">ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Phone can not same</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠬᠠᠭᠤᠴᠢᠨ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠠᠳᠠᠯᠢ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>finished</source>
        <translation type="vanished">ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">ᠲᠤᠰ ᠨᠤᠮᠸᠷ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠩᠰᠠᠯᠠᠭᠤᠯᠪᠠ᠂ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠰᠣᠯᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
</context>
<context>
    <name>ChangePinIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Change Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ChangePwdIntelDialog</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>General Pwd</source>
        <translation type="vanished">ᠨᠡᠢᠳᠡᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <source>Old Password</source>
        <translation type="vanished">ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <source>New Password</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <source>New Password Identify</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Please set different pwd!</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠲᠠᠢ ᠠᠳᠠᠯᠢ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ !</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">ᠬᠣᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <source>Old pwd is wrong!</source>
        <translation type="vanished">ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠪᠤᠷᠤᠭᠤ !</translation>
    </message>
    <message>
        <source>New pwd is too similar with old pwd!</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠪᠤᠯᠤᠨ ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠠᠳᠠᠯᠢᠪᠳᠤᠷ ᠴᠢᠨᠠᠷ ᠬᠡᠳᠦ ᠶᠡᠬᠡ !</translation>
    </message>
    <message>
        <source>Check old pwd failed because of unknown reason!</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ ( ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠰᠢᠯᠳᠠᠭᠠᠨ) !</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ !</translation>
    </message>
    <message>
        <source>Password cannot be made up entirely by Numbers!</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠪᠤᠳᠤᠯᠢᠶᠠᠨ ᠲᠠᠢ ᠰᠢᠨᠵᠢ ᠬᠡᠳᠦ ᠳᠤᠤᠷ᠎ᠠ !</translation>
    </message>
</context>
<context>
    <name>ChangeTypeIntelDialog</name>
    <message>
        <source>Change Account Type</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install software and change system settings</source>
        <translation type="vanished">ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠳᠠᠩᠰᠠ ᠶᠡᠬᠡᠩᠬᠢ ᠰᠣᠹᠲ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠬᠡᠪᠡᠴᠤ ᠰᠣᠹᠲ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠪᠤᠶᠤ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠩᠰᠠ ᠰᠣᠹᠲ ᠤᠭᠰᠠᠷᠠᠬᠤ᠂ ᠰᠣᠹᠲ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠵᠡᠷᠭᠡ ᠶᠠᠮᠠᠷᠪᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Make sure that there is at least one administrator on the computer</source>
        <translation type="vanished">ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠬᠤ᠄ ᠲᠤᠰ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠳᠡᠭᠡᠷ᠎ᠡ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠵᠢᠨ ᠡᠷᠬᠡ ᠲᠠᠢ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ChangeUserLogo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="134"/>
        <source>System Logos</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="111"/>
        <source>User logo</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="143"/>
        <source>Select Local Logo</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="152"/>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="296"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="154"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="291"/>
        <source>select custom face file</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="292"/>
        <source>Select</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="293"/>
        <source>Position: </source>
        <translation>ᠪᠠᠢᠷᠢ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="294"/>
        <source>FileName: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="295"/>
        <source>FileType: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="311"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="312"/>
        <source>The avatar is larger than 1M, please choose again</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ ᠨᠢ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ 1M ᠡᠴᠡ ᠶᠡᠬᠡ ᠂ ᠳᠠᠬᠢᠵᠤ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>ChangeUserNickname</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="31"/>
        <source>Set Nickname</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ ᠵᠢ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="53"/>
        <source>UserName</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="70"/>
        <source>NickName</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="156"/>
        <source>NickName&apos;s length must between 1~%1 characters!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠤᠷᠲᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1᠎ᠡᠴᠡ %1 ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="158"/>
        <source>nickName already in use.</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="160"/>
        <source>Can&apos;t contains &apos;:&apos;.</source>
        <translation>&apos;:&apos; ᠢ᠋/ ᠶ᠋ᠢ ᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="113"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="116"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="98"/>
        <source>Change password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="103"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="470"/>
        <source>Current Pwd</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="116"/>
        <source>Required</source>
        <translation>ᠵᠠᠪᠠᠯ ᠲᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="136"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="471"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="479"/>
        <source>New Pwd</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="163"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="472"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="480"/>
        <source>Sure Pwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="218"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="303"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="370"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="279"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="543"/>
        <source>Inconsistency with pwd</source>
        <translation>ᠬᠣᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="299"/>
        <source>Same with old pwd</source>
        <translation>ᠬᠠᠭᠤᠴᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠲᠠᠢ ᠠᠳᠠᠯᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="353"/>
        <source>Pwd Changed Succes</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="360"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ᠂ ᠳᠠᠬᠢᠵᠤ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="502"/>
        <source>Contains illegal characters!</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="614"/>
        <source>current pwd cannot be empty!</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="619"/>
        <source>new pwd cannot be empty!</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="624"/>
        <source>sure pwd cannot be empty!</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ !</translation>
    </message>
</context>
<context>
    <name>ChangeUserType</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="24"/>
        <source>UserType</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="71"/>
        <source>administrator</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="73"/>
        <source>standard user</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="61"/>
        <source>Select account type (Ensure have admin on system):</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ ( ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠳᠠᠩᠰᠠ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ):</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="75"/>
        <source>change system settings, install and upgrade software.</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠩᠰᠠ ᠰᠣᠹᠲ ᠢ᠋ ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦᠯᠬᠦ ᠵᠡᠷᠭᠡ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="77"/>
        <source>use most software, cannot change system settings.</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠡᠬᠡᠩᠬᠢ ᠰᠣᠹᠲ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠬᠡᠪᠡᠴᠤ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠵᠠᠰᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="130"/>
        <source>Note: Effective After Logout!!!</source>
        <translation>ᠠᠩᠬᠠᠷ: ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠨᠢ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠦᠴᠦᠨ ᠲᠠᠢ !!!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="141"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="144"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>ChangeValidIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Password Validity Setting</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Current passwd validity:</source>
        <translation type="vanished">ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ :</translation>
    </message>
    <message>
        <source>Adjust date to:</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠳ:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ChangtimeDialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="161"/>
        <source>day</source>
        <translation>ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="158"/>
        <source>time</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="159"/>
        <source>year</source>
        <translation>ᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="160"/>
        <source>month</source>
        <translation>ᠰᠠᠷ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="86"/>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="46"/>
        <source>Choose a custom color</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠥᠩᠭᠡ᠎ᠶ᠋ᠢ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="254"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="290"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="457"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="476"/>
        <source>OK</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="50"/>
        <source>Custom color</source>
        <translation>ᠦᠩᠭᠡ ᠵᠢ ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="138"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>CreateGroupDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.ui" line="26"/>
        <source>Add New Group</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="51"/>
        <source>Name</source>
        <translation>ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="75"/>
        <source>Id</source>
        <translation>ᠪᠦᠯᠦᠭ ID</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="89"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="199"/>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="213"/>
        <source>GroupName&apos;s length must be between 1 and %1 characters!</source>
        <translation>ᠪᠦᠯᠦᠭ᠎ᠦ᠋ᠨ ᠨᠡᠷ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠤᠷᠲᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1᠎ᠡᠴᠡ %1 ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="86"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="47"/>
        <source>Add user group</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>CreateGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Add New Group</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Group Name</source>
        <translation type="vanished">ᠳᠤᠭᠤᠶᠢᠯᠠᠩ ᠤᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠃</translation>
    </message>
    <message>
        <source>Group Id</source>
        <translation type="vanished">ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ID</translation>
    </message>
    <message>
        <source>Group Members</source>
        <translation type="vanished">ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠬᠡᠰᠢᠬᠦᠨ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Certain</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>CreateUserIntelDialog</name>
    <message>
        <source>Add New Account</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Account Type</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Standard users can use most software, but cannot install the software and 
change system settings</source>
        <translation type="vanished">ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠳᠠᠩᠰᠠ ᠶᠡᠬᠡᠩᠬᠢ ᠰᠣᠹᠲ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠬᠡᠪᠡᠴᠤ ᠰᠣᠹᠲ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠪᠤᠶᠤ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ
ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>Administrators can make any changes they need</source>
        <translation type="vanished">ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠩᠰᠠ ᠰᠣᠹᠲ ᠤᠭᠰᠠᠷᠠᠬᠤ᠂ ᠰᠣᠹᠲ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠵᠡᠷᠭᠡ ᠶᠠᠮᠠᠷᠪᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="vanished">ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <source>Password Identify</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Inconsistency with pwd</source>
        <translation type="vanished">ᠬᠣᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ!</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ !</translation>
    </message>
    <message>
        <source>The user name cannot be empty</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>The first character must be lowercase letters!</source>
        <translation type="vanished">ᠡᠬᠢᠨ ᠤ᠋ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠷᠬᠡᠪᠰᠢ ᠵᠢᠵᠢᠭ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ !</translation>
    </message>
    <message>
        <source>User name can not contain capital letters!</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠳᠤᠮᠤ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ !</translation>
    </message>
    <message>
        <source>The user name is already in use, please use a different one.</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠰᠣᠯᠢᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <source>User name length need to less than %1 letters!</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠤᠷᠳᠤ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ !</translation>
    </message>
    <message>
        <source>The user name can only be composed of letters, numbers and underline!</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠥᠪᠬᠡᠨ ᠠᠪᠢᠶ᠎ᠠ᠂ ᠲᠣᠭ᠎ᠠ ᠵᠢᠴᠢ ᠳᠤᠤᠷ᠎ᠠ ᠵᠢᠷᠤᠭᠠᠰᠤ ᠪᠡᠷ ᠪᠦᠷᠢᠯᠳᠦᠨ᠎ᠡ !</translation>
    </message>
    <message>
        <source>The username is configured, please change the username</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠰᠣᠯᠢᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>CreateUserNew</name>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="49"/>
        <source>CreateUserNew</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="55"/>
        <source>UserName</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="65"/>
        <source>NickName</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="71"/>
        <source>HostName</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="80"/>
        <source>Pwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="88"/>
        <source>SurePwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="96"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="99"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="102"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="105"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="109"/>
        <source>verification</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="182"/>
        <source>Select Type</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="191"/>
        <source>Administrator</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="194"/>
        <source>Users can make any changes they need</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠠᠮᠠᠷᠪᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="196"/>
        <source>Standard User</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="199"/>
        <source>Users cannot change system settings</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠵᠠᠰᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="276"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="279"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="355"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="581"/>
        <source>Inconsistency with pwd</source>
        <translation>ᠬᠣᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="494"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="647"/>
        <source>NickName&apos;s length must be between 1 and %1 characters!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠤᠷᠲᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1᠎ᠡᠴᠡ %1 ᠦᠰᠦᠭ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="496"/>
        <source>nickName already in use.</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="642"/>
        <source>Username&apos;s length must be between 1 and %1 characters!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠤᠷᠳᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1 ᠡᠴᠡ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="652"/>
        <source>new pwd cannot be empty!</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="657"/>
        <source>sure pwd cannot be empty!</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="526"/>
        <source>Name corresponds to group already exists.</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠬᠠᠷᠠᠭᠠᠯᠵᠠᠭᠰᠠᠨ ᠪᠦᠯᠦᠭ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="522"/>
        <source>Username&apos;s folder exists, change another one</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠭᠠᠷᠴᠠᠭ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="551"/>
        <source>Contains illegal characters!</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ !</translation>
    </message>
</context>
<context>
    <name>CustomGlobalTheme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/globaltheme/customglobaltheme.cpp" line="38"/>
        <source>custom</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>CustomLineEdit</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/customlineedit.cpp" line="28"/>
        <source>New Shortcut...</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠲᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ...</translation>
    </message>
</context>
<context>
    <name>DateTime</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="26"/>
        <source>DateTime</source>
        <translation>ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ ᠤ᠋ ᠦᠭᠡ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="65"/>
        <source>current date</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="321"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="275"/>
        <source>Change timezone</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠢ᠋ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
        <extra-contents_path>/Date/Change time zone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="444"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="620"/>
        <source>TextLabel</source>
        <translation>ᠲᠧᠺᠰᠲ ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="481"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="510"/>
        <source>RadioButton</source>
        <translation>RadioButton</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="712"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="744"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="952"/>
        <source>titleLabel</source>
        <translation>titleLabel</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="94"/>
        <source>Date</source>
        <translation>ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠠ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="175"/>
        <source>Current Date</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
        <extra-contents_path>/Date/Current Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="178"/>
        <source>Other Timezone</source>
        <translation>ᠪᠤᠰᠤᠳ ᠴᠠᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠤ᠋ ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
        <extra-contents_path>/Date/Other Timezone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="196"/>
        <source>24-hour clock</source>
        <translation>24 ᠴᠠᠭ᠎ᠤ᠋ᠨ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="461"/>
        <source>Sync Server</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠭᠰᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ</translation>
        <extra-contents_path>/Date/Sync Server</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="650"/>
        <source>Add Timezone</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠢ᠋ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="289"/>
        <source>Manual Time</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠤᠰᠤ ᠪᠡᠷ ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Date/Manual Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="286"/>
        <source>Sync Time</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠭᠰᠠᠨ ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
        <extra-contents_path>/Date/Sync Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="198"/>
        <source>Set Time</source>
        <translation>ᠴᠠᠭ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Date/Set Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="226"/>
        <source>Set Date Manually</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠤᠰᠤ ᠪᠡᠷ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="375"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
        <extra-contents_path>/Date/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="463"/>
        <source>Default</source>
        <translation>ᠭᠦᠨ ᠬᠦᠶᠦᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="465"/>
        <source>Customize</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="475"/>
        <source>Server Address</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="480"/>
        <source>Required</source>
        <translation>ᠡᠷᠬᠡᠪᠰᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="481"/>
        <source>Save</source>
        <translation>ᠰᠢᠷᠪᠢᠭᠡᠳ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="652"/>
        <source>Change Timezone</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠢ᠋ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="772"/>
        <source>MMMM d, yy ddd</source>
        <translation>yy ᠤᠨ M ᠰᠠᠷ᠎ᠠ d ᠡᠳᠦᠷ ddd</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="774"/>
        <source>MMMM dd, yyyy ddd</source>
        <translation>yyyy ᠤᠨ MM ᠰᠠᠷ᠎ᠠ dd ᠡᠳᠦᠷ ddd</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="932"/>
        <source>  </source>
        <translation>  </translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="933"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="942"/>
        <source>Sync Failed</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Sync failed</source>
        <translation type="vanished">ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="640"/>
        <source>change time</source>
        <translation>ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ᠄</translation>
    </message>
</context>
<context>
    <name>DefaultApp</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="43"/>
        <source>Default App</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="67"/>
        <source>No program available</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠫᠷᠣᠭ᠌ᠷᠠᠮ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="68"/>
        <source>Choose default app</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠵᠢ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="252"/>
        <source>Reset default apps to system recommended apps</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠵᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠪᠣᠯᠭᠠᠵᠤ ᠳᠠᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Defaultapp/Reset default apps to system recommended apps</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="253"/>
        <source>Reset</source>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="348"/>
        <source>Browser</source>
        <translation>ᠦᠵᠡᠬᠦᠷ</translation>
        <extra-contents_path>/Defaultapp/Browser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="350"/>
        <source>Mail</source>
        <translation>ᠢᠮᠸᠯ</translation>
        <extra-contents_path>/Defaultapp/Mail</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="352"/>
        <source>Image Viewer</source>
        <translation>ᠢᠮᠡᠭᠸ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠤᠷ</translation>
        <extra-contents_path>/Defaultapp/Image Viewer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="354"/>
        <source>Audio Player</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠦᠬᠦᠷ</translation>
        <extra-contents_path>/Defaultapp/Audio Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="356"/>
        <source>Video Player</source>
        <translation>ᠸᠢᠳᠢᠤ᠋ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠦᠬᠦᠷ</translation>
        <extra-contents_path>/Defaultapp/Video Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="358"/>
        <source>Text Editor</source>
        <translation>ᠲᠸᠺᠰᠲ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠭᠤᠷ</translation>
        <extra-contents_path>/Defaultapp/Text Editor</extra-contents_path>
    </message>
</context>
<context>
    <name>DefaultAppWindow</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="346"/>
        <source>Select Default Application</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ</translation>
        <extra-contents_path>/Defaultapp/Select Default Application</extra-contents_path>
    </message>
</context>
<context>
    <name>DefineGroupItemIntel</name>
    <message>
        <source>Edit</source>
        <translation type="vanished">ᠨᠠᠢ᠌ᠷᠠᠭᠤᠯᠤᠭᠴᠢ ᠃</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">ᠰᠢᠯᠵᠢᠬᠦᠯᠵᠤ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DefineShortcutItem</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/defineshortcutitem.cpp" line="58"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DelGroupIntelDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">textLabel</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>RemoveFile</source>
        <translation type="vanished">ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DelUserIntelDialog</name>
    <message>
        <source>   Delete</source>
        <translation type="vanished">   ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Define</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Delete the user, belonging to the user&apos;s desktop documents, favorites, music, pictures and video folder will be deleted!</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠬᠠᠰᠤᠪᠠᠯ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠬᠠᠷᠢᠶᠠᠯᠠᠭᠳᠠᠬᠤ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠂ ᠳᠤᠺᠦᠢᠮᠸᠨ᠋ᠲ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠤᠷ᠂ ᠳᠠᠭᠤᠤ ᠬᠥᠭᠵᠢᠮ᠂ ᠵᠢᠷᠤᠭ ᠪᠣᠯᠤᠨ᠎ᠠ ᠸᠢᠳᠢᠤ᠋ ᠵᠢᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠬᠢ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠦᠬᠦᠨ ᠵᠢᠡᠷ ᠵᠢᠨᠨ ᠬᠠᠰᠤᠭᠳᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ !</translation>
    </message>
</context>
<context>
    <name>DeleteUserExists</name>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="26"/>
        <source>Delete User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ ᠬᠠᠰᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="39"/>
        <source>Delete user &apos;</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠡᠰᠡᠬᠦ &apos;</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="40"/>
        <source>&apos;? And:</source>
        <translation>&apos;? ᠮᠦᠷᠳᠡᠭᠡᠨ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="66"/>
        <source>Keep desktop, files, favorites, music of the user</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠬᠠᠷᠢᠶᠠᠯᠠᠭᠳᠠᠬᠤ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠂ ᠹᠠᠢᠯ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠤᠷ᠂ ᠳᠠᠭᠤᠤ ᠬᠥᠭᠵᠢᠮ ᠵᠡᠷᠭᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠦᠯᠡᠳᠡᠬᠡᠵᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="99"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="101"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="67"/>
        <source>Delete whole data belong user</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠬᠦᠢᠯᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DigitalAuthIntelDialog</name>
    <message>
        <source>Enter Old Password</source>
        <translation type="vanished">ᠬᠠᠭᠤᠴᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Forget Password?</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠵᠢᠨᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠤᠤ ?</translation>
    </message>
    <message>
        <source>Input New Password</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Input Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>The password input is error</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠪᠤᠷᠤᠭᠤ ᠣᠷᠣᠭᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <source>Confirm New Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>The password input is inconsistent</source>
        <translation type="vanished">ᠬᠣᠶᠠᠷ ᠤᠳᠠᠭ᠎ᠠ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>New password can not be consistent of old password</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠢᠭᠡᠳ ᠬᠠᠭᠤᠴᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢᠵᠢᠯ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Password Change Failed</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>DigitalPhoneIntelDialog</name>
    <message>
        <source>Please Enter Edu OS Password</source>
        <translation type="vanished">Edu OS ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>The password input is error</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠪᠤᠷᠤᠭᠤ ᠣᠷᠣᠭᠤᠯᠪᠠ</translation>
    </message>
</context>
<context>
    <name>DisplayPerformanceDialog</name>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="214"/>
        <source>Display Advanced Settings</source>
        <translation>ᠥᠨᠳᠥᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="297"/>
        <source>Performance</source>
        <translation>ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="376"/>
        <source>Applicable to machine with discrete graphics, which can accelerate the rendering of 3D graphics.</source>
        <translation>ᠳᠤᠰᠠᠭᠠᠷ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠺᠠᠷᠲ ᠲᠠᠢ ᠮᠠᠰᠢᠨ ᠳ᠋ᠤ᠌ ᠳᠤᠬᠢᠷᠠᠵᠤ᠂ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢ ᠪᠦᠷᠢᠨ ᠪᠠᠳᠠᠷᠠᠭᠤᠯᠵᠤ᠂ 3D ᠳᠦᠰᠦ ᠵᠢᠷᠤᠵᠤ ᠦᠢᠯᠡᠳᠬᠦ ᠵᠢ ᠬᠤᠷᠳᠤᠳᠬᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="392"/>
        <source>(Note: not support connect graphical with xmanager on windows.)</source>
        <translation>( ᠠᠩᠬᠠᠷ᠄ ᠲᠤᠰ ᠵᠠᠭᠪᠤᠷ ᠨᠢ windows ᠳᠡᠭᠡᠷ᠎ᠡ xmanager ᠵᠡᠷᠭᠡ ᠪᠠᠭᠠᠵᠢ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠳᠦᠷᠰᠦ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠥᠬᠡᠢ.)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="462"/>
        <source>Compatible</source>
        <translation>ᠠᠭᠤᠰᠤᠯᠴᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="538"/>
        <source>Applicable to machine with integrated graphics,  there is no 3D graphics acceleration. </source>
        <translation>ᠳᠦᠪᠯᠡᠷᠡᠭᠰᠡᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠺᠠᠷᠲ ᠲᠤ᠌ ᠳᠤᠬᠢᠷᠠᠵᠤ᠂ BMC ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠺᠠᠷᠲ ᠵᠡᠷᠭᠡ ᠵᠥᠪᠬᠡᠨ ᠰᠠᠭᠤᠷᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢ ᠪᠤᠢ ᠪᠣᠯᠭᠠᠵᠤ᠂ 3D ᠳᠦᠷᠰᠦ ᠵᠢᠷᠤᠵᠤ ᠦᠢᠯᠡᠳᠬᠦ ᠵᠢ ᠬᠤᠷᠳᠤᠳᠬᠠᠬᠤ ᠥᠬᠡᠢ. </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="554"/>
        <source>(Note: need connect graphical with xmanager on windows, use this option.)</source>
        <translation>( ᠠᠩᠬᠠᠷ᠄windows ᠳᠡᠭᠡᠷᠡᠬᠢ xmanager ᠵᠡᠷᠭᠡ ᠪᠠᠭᠠᠵᠢ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠳᠦᠷᠰᠦ ᠵᠢ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠮᠠᠰᠢᠨ ᠵᠢᠡᠷ ᠲᠤᠰ ᠳᠦᠷᠦᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ.)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="604"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="vanished">ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Auto select according to environment, delay the login time (about 0.5 sec).</source>
        <translation type="vanished">ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠭᠠᠯᠳᠠ ᠣᠷᠴᠢᠨ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠵᠤ᠂ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠴᠠᠭ ᠢ᠋ ᠬᠤᠢᠰᠢᠯᠠᠭᠤᠯᠬᠤ (0.5 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ ᠭᠠᠷᠤᠢ).</translation>
    </message>
    <message>
        <source>Threshold:</source>
        <translation type="vanished">ᠪᠣᠱᠣᠭᠠᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="597"/>
        <source>Apply</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>(Note: select this option to use 3D graphics acceleration and xmanager.)</source>
        <translation type="vanished">( ᠠᠩᠬᠠᠷ᠄ 3D ᠳᠦᠷᠰᠦ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠬᠤᠷᠳᠤᠳᠬᠠᠬᠤ᠂ ᠮᠥᠨ xmanager ᠢ᠋/ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠳᠦᠷᠰᠦ ᠵᠢ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠲᠤᠰ ᠵᠦᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ.)</translation>
    </message>
</context>
<context>
    <name>DisplaySet</name>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="36"/>
        <source>Screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="38"/>
        <source>Display</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>DisplayWindow</name>
    <message>
        <source>monitor</source>
        <translation type="vanished">ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="320"/>
        <source>open monitor</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠴᠢ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="32"/>
        <source>Display</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠴᠢ</translation>
    </message>
    <message>
        <source>as main</source>
        <translation type="vanished">ᠭᠤᠤᠯ ᠳᠡᠯᠭᠡᠴᠡ ᠪᠡᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="250"/>
        <source>screen zoom</source>
        <translation>ᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="374"/>
        <source>Advanced</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>follow the sunrise and sunset(17:55-05:04)</source>
        <translation type="vanished">ᠨᠠᠷᠠ ᠤᠨᠠᠵᠤ ᠨᠠᠷᠠ ᠭᠠᠷᠬᠤ ᠵᠢ ᠳᠠᠭᠠᠬᠤ (17:55-05:04)</translation>
    </message>
    <message>
        <source>custom time</source>
        <translation type="vanished">ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>opening time</source>
        <translation type="vanished">ᠡᠬᠢᠯᠡᠬᠦ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>closing time</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>color temperature</source>
        <translation type="vanished">ᠦᠩᠭᠡ ᠵᠢᠨ ᠳᠤᠯᠠᠭᠠᠴᠠ</translation>
    </message>
    <message>
        <source>warm</source>
        <translation type="vanished">ᠳᠤᠯᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <source>cold</source>
        <translation type="vanished">ᠬᠦᠢᠳᠡᠨ</translation>
    </message>
    <message>
        <source>Mirror Display</source>
        <translation type="vanished">ᠳᠤᠯᠢᠳᠠᠰᠤ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
</context>
<context>
    <name>EditPushButton</name>
    <message>
        <source>Reset</source>
        <translation type="vanished">重置密码</translation>
    </message>
</context>
<context>
    <name>Fonts</name>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="50"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="46"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="122"/>
        <source>Fonts</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌</translation>
        <extra-contents_path>/Fonts/Fonts</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="264"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="126"/>
        <source>Fonts select</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠰᠣᠩᠭᠣᠬᠤ</translation>
        <extra-contents_path>/Fonts/Fonts select</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="146"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="124"/>
        <source>Font size</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
        <extra-contents_path>/Fonts/Font size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="370"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="128"/>
        <source>Mono font</source>
        <translation>ᠠᠳᠠᠯᠢ ᠥᠷᠭᠡᠴᠡ ᠲᠠᠢ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌</translation>
        <extra-contents_path>/Fonts/Mono font</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="421"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="130"/>
        <source>Reset to default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Fonts/Reset to default</extra-contents_path>
    </message>
    <message>
        <source>11</source>
        <translation type="vanished">11</translation>
    </message>
    <message>
        <source>12</source>
        <translation type="vanished">12</translation>
    </message>
    <message>
        <source>13</source>
        <translation type="vanished">13</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
    <message>
        <source>16</source>
        <translation type="vanished">16</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="144"/>
        <source>Small</source>
        <translation>ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="148"/>
        <source>Large</source>
        <translation>ᠶᠡᠬᠡ</translation>
    </message>
</context>
<context>
    <name>GetShortcutWorker</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="59"/>
        <location filename="../../../plugins/devices/shortcut/getshortcutworker.cpp" line="83"/>
        <source>Null</source>
        <translation>ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>GrubVerify</name>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="24"/>
        <source>Grub verify</source>
        <translation>Grub ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="29"/>
        <source>User:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ：</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="42"/>
        <source>Pwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="85"/>
        <source>Sure Pwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="138"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="142"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="197"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="235"/>
        <source>Inconsistency with pwd</source>
        <translation>ᠬᠣᠶᠠᠷ ᠤᠳᠠᠭ᠎ᠠ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="225"/>
        <location filename="../../../plugins/commoninfo/boot/grubverifydialog.cpp" line="303"/>
        <source>pwd cannot be empty!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
</context>
<context>
    <name>HostNameDialog</name>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="17"/>
        <source>Set HostName</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="45"/>
        <source>HostName</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="91"/>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="94"/>
        <source>Must be 1-64 characters long</source>
        <translation>ᠤᠷᠳᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1-64 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="161"/>
        <source>Hostname must start or end with a number and a letter</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ᠎ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠣᠭ᠎ᠠ ᠂ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ᠎ᠪᠠᠷ ᠡᠬᠢᠯᠡᠯᠲᠡ ᠪᠤᠶᠤ ᠲᠡᠭᠦᠰᠬᠡᠯ ᠪᠣᠯᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="172"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ᠎ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠬᠦ ‘ — ᠪᠣᠯᠤᠨ‘ . ’ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="179"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ᠎ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠰᠡᠨ “ . ” ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="75"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="71"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>InputPwdDialog</name>
    <message>
        <source>Set Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="77"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Set</source>
        <translation type="vanished">ᠣᠴᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="28"/>
        <source>VNC password</source>
        <translation>VNC ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="45"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="60"/>
        <source>Must be 1-8 characters long</source>
        <translation>ᠤᠷᠳᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1-8 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="81"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KbdLayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="68"/>
        <source>C</source>
        <translation>ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠶᠣᠰᠣᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="144"/>
        <source>L</source>
        <translation>ᠦᠭᠡ᠎ᠪᠡᠷ ᠨᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="222"/>
        <source>Variant</source>
        <translation>ᠬᠤᠪᠢᠰᠤᠯ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="270"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="58"/>
        <source>Add Layout</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="233"/>
        <source>Del</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KeyValueConverter</name>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="46"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="49"/>
        <source>Devices</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="55"/>
        <source>Personalized</source>
        <translation>ᠦᠪᠡᠷᠮᠢᠴᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="52"/>
        <source>Network</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="58"/>
        <source>Account</source>
        <translation>ᠳᠠᠩᠰᠠ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="61"/>
        <source>Datetime</source>
        <translation>ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ ᠤ᠋ ᠦᠭᠡ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="64"/>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="67"/>
        <source>Security</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠰᠢᠨᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="70"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠰᠣᠹᠲ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="73"/>
        <source>Investigation</source>
        <translation>ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="76"/>
        <source>Commoninfo</source>
        <translation>ᠨᠡᠢᠳᠡᠮ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>KeyboardMain</name>
    <message>
        <source>Key board settings</source>
        <translation type="vanished">ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠳᠠᠪᠠᠭ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="25"/>
        <source>Keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠳᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="102"/>
        <source>Keyboard settings</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠲᠠᠪᠠᠭ᠎ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
        <extra-contents_path>/Keyboard/Keyboard settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="114"/>
        <source>Input settings</source>
        <translation>ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
        <extra-contents_path>/Keyboard/Input settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="154"/>
        <source>Key repeat</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ ᠳᠠᠪᠬᠤᠷᠳᠠᠬᠤ</translation>
        <extra-contents_path>/Keyboard/Key repeat</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="177"/>
        <source>Delay</source>
        <translation>ᠬᠤᠢᠰᠢᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Keyboard/Delay</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="180"/>
        <source>Short</source>
        <translation>ᠪᠣᠭᠣᠨᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="183"/>
        <source>Long</source>
        <translation>ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="214"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
        <extra-contents_path>/Keyboard/Speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="217"/>
        <source>Slow</source>
        <translation>ᠤᠳᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="220"/>
        <source>Fast</source>
        <translation>ᠬᠤᠷᠳᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="251"/>
        <source>Input test</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠳᠤᠷᠰᠢᠯᠳᠠ</translation>
        <extra-contents_path>/Keyboard/Input test</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="272"/>
        <source>Key tips</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢᠨ ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠤᠯ</translation>
        <extra-contents_path>/Keyboard/Key tips</extra-contents_path>
    </message>
</context>
<context>
    <name>LanguageFrame</name>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="78"/>
        <source>Delete</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠵᠤ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="77"/>
        <source>Input Settings</source>
        <translation>ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="26"/>
        <source>Dialog</source>
        <translation>ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="121"/>
        <source>Manager Keyboard Layout</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ᠎ᠶ᠋ᠢᠨ ᠲᠠᠪᠠᠭ᠎ᠤ᠋ᠨ ᠪᠠᠢᠷᠢᠰᠢᠯ᠎ᠢ᠋ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="234"/>
        <source>Language</source>
        <translation>ᠦᠭᠡ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="250"/>
        <source>Country</source>
        <translation>ᠤᠯᠤᠰ ᠤᠷᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="293"/>
        <source>Variant</source>
        <translation>ᠬᠤᠪᠢᠰᠤᠯ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="351"/>
        <source>Layout installed</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="399"/>
        <source>Preview</source>
        <translation>ᠤᠷᠢᠳᠴᠢᠯᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="431"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="450"/>
        <source>Install</source>
        <translation>ᠣᠴᠢᠵᠤ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Search</source>
        <translation type="vanished">ᠲᠠᠨ ᠤ᠋ ᠡᠷᠢᠬᠦ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠡᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="410"/>
        <location filename="../../mainwindow.cpp" line="431"/>
        <location filename="../../mainwindow.cpp" line="550"/>
        <location filename="../../mainwindow.cpp" line="1049"/>
        <source>Settings</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Main menu</source>
        <translation type="vanished">ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="452"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="vanished">ᠡᠩ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <source>Warnning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="192"/>
        <source>Restore</source>
        <translation>ᠠᠩᠭᠢᠵᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="195"/>
        <location filename="../../mainwindow.cpp" line="453"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠶᠡᠭᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="425"/>
        <source>Back home</source>
        <translation>ᠲᠦᠷᠦᠭᠦᠦ ᠨᠢᠭᠤᠷ᠎ᠲᠤ᠌ ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="451"/>
        <source>Option</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="454"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="538"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="540"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="542"/>
        <source>Exit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="551"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠄ </translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="687"/>
        <source>Specified</source>
        <translation>ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠣᠨᠣᠭ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="174"/>
        <location filename="../../mainwindow.cpp" line="1186"/>
        <source>This function has been controlled</source>
        <translation>ᠲᠤᠰ ᠴᠢᠳᠠᠪᠬᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>Form</source>
        <translation type="vanished">ᠹᠤᠤᠮ</translation>
    </message>
    <message>
        <source>Attention</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>It takes effect after logging off</source>
        <translation type="vanished">ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠷᠤᠢ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Logout Now</source>
        <translation type="vanished">ᠳᠠᠷᠤᠢ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Reboot Now</source>
        <translation type="vanished">ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>This cleanup and restore need to be done after the system restarts, whether to restart and restore immediately?</source>
        <translation type="vanished">ᠲᠤᠰ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠬᠢᠬᠡᠳ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠨᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠶᠠᠪᠤᠭᠳᠠᠨ᠎ᠠ᠂ ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠤᠤ ?</translation>
    </message>
    <message>
        <source>System Backup Tips</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
</context>
<context>
    <name>MessageBoxDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>You do not have administrator rights!</source>
        <translation type="vanished">ᠲᠠ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠵᠢᠨ ᠡᠷᠬᠡ ᠥᠬᠡᠢ !</translation>
    </message>
    <message>
        <source> Factory Settings cannot be restored!</source>
        <translation type="vanished"> ᠦᠢᠯᠡᠳᠪᠦᠷᠢ ᠡᠴᠡ ᠭᠠᠷᠬᠤ ᠦᠶ᠎ᠡ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ !</translation>
    </message>
</context>
<context>
    <name>MessageBoxPower</name>
    <message>
        <source>System Recovery</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>The battery is low,please connect the power</source>
        <translation type="vanished">ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ ᠵᠢᠨ ᠴᠡᠨᠡᠭ ᠨᠡᠯᠢᠶᠡᠳ ᠳᠤᠤᠷ᠎ᠠ᠂ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠬᠦᠰᠬᠡᠬᠦᠷ ᠢ᠋ ᠵᠠᠯᠭᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Keep the power connection, or the power is more than 25%.</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠳ᠋ᠤ᠌ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠬᠦᠰᠬᠡᠬᠦᠷ ᠢ᠋ ᠵᠠᠯᠭᠠᠭᠰᠠᠨ ᠪᠠᠢᠬᠤ ᠡᠬᠷᠡᠭᠳᠡᠢ᠂ ᠡᠰᠡᠪᠡᠯ ᠴᠡᠨᠡᠭ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ 25% ᠡᠴᠡ ᠳᠤᠤᠷ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>Remind in 30 minutes</source>
        <translation type="vanished">30 ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠠᠳᠠᠳᠤ ᠰᠠᠨᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Got it</source>
        <translation type="vanished">ᠮᠡᠳᠡᠯ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>MessageBoxPowerIntel</name>
    <message>
        <source>Nothing has been entered, re-enter</source>
        <translation type="vanished">ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠠᠭᠤᠯᠭ᠎ᠠ ᠣᠷᠣᠭᠤᠯᠤᠭ᠎ᠠ ᠥᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Remind in 30 minutes</source>
        <translation type="vanished">30 ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠠᠳᠠᠳᠤ ᠰᠠᠨᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Got it</source>
        <translation type="vanished">ᠮᠡᠳᠡᠯ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>MouseUI</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="25"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="194"/>
        <source>Mouse</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ</translation>
        <extra-contents_path>/Mouse/Mouse</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="203"/>
        <source>Pointer</source>
        <translation>ᠵᠢᠭᠠᠯᠳᠠ</translation>
        <extra-contents_path>/Mouse/Pointer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="212"/>
        <source>Cursor</source>
        <translation>ᠺᠸᠰᠸ</translation>
        <extra-contents_path>/Mouse/Cursor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="275"/>
        <source>Dominant hand</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ ᠤ᠋ᠨ ᠭᠤᠤᠯ ᠳᠠᠷᠤᠪᠴᠢ</translation>
        <extra-contents_path>/Mouse/Dominant hand</extra-contents_path>
    </message>
    <message>
        <source>Left hand</source>
        <translation type="vanished">ᠵᠡᠬᠦᠨ ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
    <message>
        <source>Right hand</source>
        <translation type="vanished">ᠪᠠᠷᠠᠭᠤᠨ ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="278"/>
        <source>Left key</source>
        <translation>ᠵᠡᠭᠦᠨ ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="279"/>
        <source>Right key</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="307"/>
        <source>Scroll direction</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠦᠯᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
        <extra-contents_path>/Mouse/Scroll direction</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="310"/>
        <source>Forward</source>
        <translation>ᠵᠥᠪ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="311"/>
        <source>Reverse</source>
        <translation>ᠡᠰᠡᠷᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="340"/>
        <source>Wheel speed</source>
        <translation>ᠥᠩᠬᠥᠷᠢᠬᠦ ᠬᠤᠷᠳᠤᠴᠠ</translation>
        <extra-contents_path>/Mouse/Wheel speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="343"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="417"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="562"/>
        <source>Slow</source>
        <translation>ᠤᠳᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="352"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="427"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="572"/>
        <source>Fast</source>
        <translation>ᠬᠤᠷᠳᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="376"/>
        <source>Double-click interval time</source>
        <translation>ᠬᠤᠤᠰ ᠵᠢᠡᠷ ᠳᠤᠪᠰᠢᠬᠤ ᠵᠠᠢ ᠵᠢᠨ ᠤᠷᠳᠤ</translation>
        <extra-contents_path>/Mouse/Double-click interval time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="379"/>
        <source>Short</source>
        <translation>ᠪᠣᠭᠣᠨᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="389"/>
        <source>Long</source>
        <translation>ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="414"/>
        <source>Pointer speed</source>
        <translation>ᠵᠢᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
        <extra-contents_path>/Mouse/Pointer speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="452"/>
        <source>Mouse acceleration</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ ᠬᠤᠷᠳᠤᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Mouse/Mouse acceleration</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="474"/>
        <source>Show pointer position when pressing ctrl</source>
        <translation>ctrl ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠠᠷᠤᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠵᠢᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠪᠠᠢᠷᠢ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/Mouse/Show pointer position when pressing ctrl</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="497"/>
        <source>Pointer size</source>
        <translation>ᠵᠢᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
        <extra-contents_path>/Mouse/Pointer size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="500"/>
        <source>Small(recommend)</source>
        <translation>ᠪᠠᠭ᠎ᠠ ( ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ)</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="501"/>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="502"/>
        <source>Large</source>
        <translation>ᠶᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="536"/>
        <source>Blinking cursor in text area</source>
        <translation>ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠺᠸᠰᠸ ᠴᠠᠬᠢᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Mouse/Blinking cursor in text area</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="559"/>
        <source>Cursor speed</source>
        <translation>ᠺᠸᠰᠸ ᠵᠢᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
        <extra-contents_path>/Mouse/Cursor speed</extra-contents_path>
    </message>
</context>
<context>
    <name>MyLabel</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="130"/>
        <source>double-click to test</source>
        <translation>ᠬᠤᠤᠰ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ ᠬᠡᠮᠵᠢᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>Notice</name>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="162"/>
        <source>NotFaze Mode</source>
        <translation>ᠦᠢᠮᠡᠬᠦᠯᠬᠦ ᠥᠬᠡᠢ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="164"/>
        <source>(Notification banners, prompts will be hidden, and notification sounds will be muted)</source>
        <translation>( ᠮᠡᠳᠡᠭᠳᠡᠯ ᠤ᠋ᠨ ᠬᠥᠨᠳᠡᠯᠡᠨ ᠪᠢᠴᠢᠬᠡᠰᠦ᠂ ᠰᠠᠨᠠᠭᠠᠯᠤᠮᠵᠢ ᠨᠢ ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠳᠠᠵᠤ᠂ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠤ᠋ᠨ ᠳᠠᠭᠤ ᠵᠢ ᠴᠢᠮᠡᠭᠡ ᠥᠬᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="238"/>
        <source>Automatically turn on</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="263"/>
        <source>to</source>
        <translation>ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="288"/>
        <source>Automatically turn on when multiple screens are connected</source>
        <translation>ᠣᠯᠠᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠪᠡᠷ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="293"/>
        <source>Automatically open in full screen mode</source>
        <translation>ᠪᠦᠬᠦᠢ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠵᠠᠭᠪᠤᠷ ᠳᠤᠤᠷ᠎ᠠ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="298"/>
        <source>Allow automatic alarm reminders in Do Not Disturb mode</source>
        <translation>ᠦᠢᠮᠡᠬᠦᠯᠬᠦ ᠥᠬᠡᠢ ᠵᠠᠭᠪᠤᠷ ᠳᠤᠤᠷ᠎ᠠ ᠰᠡᠷᠢᠬᠦᠯᠬᠡᠳᠦ ᠴᠠᠭ ᠤ᠋ᠨ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="329"/>
        <source>Notice Settings</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
        <extra-contents_path>/Notice/Notice Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="331"/>
        <source>Get notifications from the app</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠡᠴᠡ ᠢᠷᠡᠭᠰᠡᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠢ᠋ ᠤᠯᠪᠠ</translation>
        <extra-contents_path>/Notice/Get notifications from the app</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="51"/>
        <source>Notice</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
</context>
<context>
    <name>NoticeMenu</name>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="41"/>
        <source>Beep sound when notified</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠬᠦ ᠦᠶ᠎ᠡ ᠵᠢᠨ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠳᠠᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="47"/>
        <source>Show message  on screenlock</source>
        <translation>ᠣᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠵᠢᠨ ᠵᠠᠭᠠᠭ ᠭᠠᠳᠠᠷᠭᠤ ᠳᠡᠭᠡᠷ᠎ᠡ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="53"/>
        <source>Show noticfication  on screenlock</source>
        <translation>ᠣᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠵᠠᠭᠠᠭ ᠭᠠᠳᠠᠷᠭᠤ ᠳᠡᠭᠡᠷ᠎ᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="57"/>
        <source>Notification Style</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ ᠤ᠋ᠨ ᠶᠠᠩᠵᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="65"/>
        <source>Banner: Appears in the upper right corner of the screen, and disappears automatically</source>
        <translation>ᠬᠥᠨᠳᠡᠯᠡᠨ ᠪᠢᠴᠢᠬᠡᠰᠦ᠄ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠪᠠᠷᠠᠭᠤᠨ ᠳᠡᠬᠡᠳᠦ ᠥᠨᠴᠥᠭ ᠲᠤ᠌ ᠢᠯᠡᠷᠡᠵᠤ᠂ ᠥᠪᠡᠷ ᠵᠢᠨᠨ ᠠᠷᠢᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="70"/>
        <source>Tip:It will be kept on the screen until it is closed</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ᠄ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠦᠯᠡᠳᠡᠭᠡᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ᠂ ᠬᠠᠭᠠᠬᠤ ᠬᠦᠷᠳᠡᠯ᠎ᠡ ᠪᠠᠢᠯᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="75"/>
        <source>None:Notifications will not be displayed on the screen, but will go to the notification center</source>
        <translation>ᠥᠬᠡᠢ᠄ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳᠡᠬᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ᠂ ᠬᠡᠪᠡᠴᠤ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠤ᠋ᠨ ᠲᠥᠪ ᠲᠤ᠌ ᠣᠷᠣᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>NumbersButtonIntel</name>
    <message>
        <source>clean</source>
        <translation type="vanished">ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>OutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="120"/>
        <source>resolution</source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ</translation>
        <extra-contents_path>/Display/resolution</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="156"/>
        <source>orientation</source>
        <translation>ᠴᠢᠭᠯᠡᠯ</translation>
        <extra-contents_path>/Display/orientation</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="174"/>
        <source>arrow-up</source>
        <translation>ᠡᠷᠬᠢᠯᠳᠦᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="175"/>
        <source>90° arrow-right</source>
        <translation>90°ᠴᠠᠭ ᠤ᠋ᠨ ᠵᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="177"/>
        <source>arrow-down</source>
        <translation>ᠳᠡᠬᠡᠭᠰᠢ ᠳᠣᠷᠣᠭᠰᠢ ᠳᠤᠩᠭᠤᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="176"/>
        <source>90° arrow-left</source>
        <translation>90° ᠴᠠᠭ ᠤ᠋ᠨ ᠵᠡᠬᠦᠦ ᠵᠢᠨ ᠡᠰᠡᠷᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="193"/>
        <source>frequency</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠨᠣᠷᠮ᠎ᠠ</translation>
        <extra-contents_path>/Display/frequency</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="394"/>
        <source>auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="513"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
</context>
<context>
    <name>PhoneAuthIntelDialog</name>
    <message>
        <source>Wechat Auth</source>
        <translation type="vanished">ᠸᠢᠴᠠᠲ ᠵᠢᠡᠷ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Phone Auth</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠡᠷ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Phone number</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <source>SMS verification code</source>
        <translation type="vanished">ᠣᠬᠣᠷ ᠮᠡᠳᠡᠭᠡᠨ ᠤ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <source>GetCode</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠺᠣᠳ᠋ ᠢ᠋ ᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Return</source>
        <translation type="vanished">return</translation>
    </message>
    <message>
        <source>Commit</source>
        <translation type="vanished">ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>commit</source>
        <translation type="vanished">ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Mobile number acquisition failed</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠣᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ᠂ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠤᠰᠤ ᠪᠡᠷ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Recapture</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠣᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Network connection failure, please check</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠰᠠᠭᠠᠳᠤᠯ ᠭᠠᠷᠪᠠ᠂ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Phone is lock,try again in an hour</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠨᠢᠭᠡᠨᠳᠡ ᠤᠨᠢᠰᠤᠯᠠᠭᠳᠠᠪᠠ᠂1 ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Phone code is wrong</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠺᠣᠳ᠋ ᠪᠤᠷᠤᠭᠤ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Current login expired,using wechat code!</source>
        <translation type="vanished">ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠥᠩᠭᠡᠷᠡᠪᠡ᠂ ᠸᠢᠴᠠᠲ ᠵᠢᠡᠷ ᠳᠠᠬᠢᠵᠤ ᠰᠢᠷᠪᠢᠭᠡᠳ ᠨᠡᠪᠳᠡᠷᠡᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <source>Unknown error, please try again later</source>
        <translation type="vanished">ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Please use the correct wechat scan code</source>
        <translation type="vanished">ᠵᠥᠪ ᠸᠢᠴᠠᠲ ᠪᠡᠷ ᠰᠢᠷᠪᠢᠭᠰᠡᠨ ᠺᠣᠳ᠋ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>Power</name>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="62"/>
        <source>Power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="254"/>
        <source>The system will sleep before turning off the display</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠦᠵᠡᠭᠦᠷ᠎ᠢ᠋ ᠬᠠᠭᠠᠬᠤ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠤᠨᠲᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="326"/>
        <source>Reduce the occupation of backend running program resources and ensure smooth operation of key and focus applications.</source>
        <translation>ᠠᠷᠤ ᠲᠠᠪᠴᠠᠩ᠎ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ᠎ᠤ᠋ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ᠎ᠤ᠋ᠨ ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ᠎ᠢ᠋ ᠡᠵᠡᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠪᠠᠭᠠᠰᠬᠠᠵᠤ ᠂ ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠬᠢᠭᠡᠳ ᠲᠥᠪᠯᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠴᠢᠭ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠤᠷᠤᠰᠬᠠᠯ᠎ᠲᠠᠢ ᠤᠷᠤᠭᠰᠢᠲᠠᠢ ᠶᠠᠪᠤᠭᠳᠠᠬᠤ᠎ᠶ᠋ᠢ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="605"/>
        <source>Require password when suspend/hibernate</source>
        <translation>ᠤᠨᠲᠠᠬᠤ / ᠢᠴᠡᠭᠡᠯᠡᠵᠦ ᠰᠡᠷᠡᠭᠡᠪᠡᠯ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠡᠷᠡᠭᠲᠡᠢ</translation>
        <extra-contents_path>/Power/Require password when suspend/hibernate</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="606"/>
        <source>Require password when suspend</source>
        <translation>ᠤᠨᠲᠠᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠡᠷᠢᠭᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠡᠷᠡᠭᠲᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="607"/>
        <source>Require password when hibernate</source>
        <translation>ᠢᠴᠡᠭᠡᠯᠡᠭᠰᠡᠨ᠎ᠦ᠌ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠭᠤᠳᠠᠨ ᠰᠡᠷᠡᠭᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠡᠷᠡᠭᠲᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="616"/>
        <location filename="../../../plugins/system/power/power.cpp" line="617"/>
        <source>Password required when waking up the screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢ ᠳᠠᠭᠤᠳᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠬᠦ ᠦᠶᠡᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠡᠷᠡᠭᠰᠡᠨ ᠠ</translation>
        <extra-contents_path>/Power/Password required when waking up the screen</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="621"/>
        <source>Press the power button</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠬᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠳᠠᠷᠤᠪᠴᠢ᠎ᠶ᠋ᠢ ᠳᠠᠷᠤᠬᠤ ᠦᠶᠡᠰ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Power/Press the power button</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="626"/>
        <location filename="../../../plugins/system/power/power.cpp" line="627"/>
        <source>Time to close display</source>
        <translation>ᠡᠨᠡ ᠴᠠᠭ᠎ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ᠎ᠦ᠋ᠨ ᠬᠣᠢᠨ᠎ᠠ ᠦᠵᠡᠭᠦᠷ᠎ᠢ᠋ ᠬᠠᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Power/Time to close display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="631"/>
        <location filename="../../../plugins/system/power/power.cpp" line="632"/>
        <source>Time to sleep</source>
        <translation>ᠲᠤᠰ ᠴᠠᠭ᠎ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠤᠨᠳᠠᠬᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
        <extra-contents_path>/Power/Time to sleep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="636"/>
        <location filename="../../../plugins/system/power/power.cpp" line="637"/>
        <source>Notebook cover</source>
        <translation>ᠭᠠᠷ ᠺᠤᠮᠫᠢᠦ᠋ᠲᠸᠷ ᠬᠠᠭᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Power/Notebook cover</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="641"/>
        <location filename="../../../plugins/system/power/power.cpp" line="642"/>
        <source>Dynamic resource scheduling</source>
        <translation>ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ᠎ᠤ᠋ᠨ ᠬᠥᠳᠡᠯᠦᠩᠭᠦᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ</translation>
        <extra-contents_path>/Power/Dynamic resource scheduling</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="646"/>
        <location filename="../../../plugins/system/power/power.cpp" line="647"/>
        <source>Using power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠬᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠴᠠᠭ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
        <extra-contents_path>/Power/Using power</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="651"/>
        <location filename="../../../plugins/system/power/power.cpp" line="652"/>
        <source>Using battery</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠶᠡᠰ</translation>
        <extra-contents_path>/Power/Using battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="655"/>
        <location filename="../../../plugins/system/power/power.cpp" line="656"/>
        <source> Time to darken</source>
        <translation> ᠲᠤᠰ ᠴᠠᠭ᠎ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠭᠡᠷᠡᠯᠲᠦᠴᠡ᠎ᠶ᠋ᠢ ᠪᠠᠭᠤᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="660"/>
        <source>Battery level is lower than</source>
        <translation>ᠴᠡᠨᠡᠭ ᠳᠣᠣᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="663"/>
        <source>Run</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠦᠶᠡᠰ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="666"/>
        <location filename="../../../plugins/system/power/power.cpp" line="667"/>
        <source>Low battery notification</source>
        <translation>ᠴᠡᠨᠡᠭ ᠳᠤᠤᠷ᠎ᠠ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠮᠡᠳᠡᠭᠳᠡᠬᠦ</translation>
        <extra-contents_path>/Power/Low battery notification</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="671"/>
        <source>Automatically run saving mode when low battery</source>
        <translation>ᠴᠡᠨᠡᠭ ᠳᠤᠤᠷ᠎ᠠ ᠦᠶᠡᠰ ᠡᠨᠧᠷᠭᠢ ᠠᠷᠪᠢᠯᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ᠎ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
        <extra-contents_path>/Power/&quot;Automatically run saving mode when low battery</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="672"/>
        <source>Automatically run saving mode when the low battery</source>
        <translation>ᠴᠡᠨᠡᠭ ᠳᠣᠣᠷ᠎ᠠ ᠦᠶᠡᠰ ᠡᠨᠧᠷᠭᠢ ᠠᠷᠪᠢᠯᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ᠎ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="675"/>
        <location filename="../../../plugins/system/power/power.cpp" line="676"/>
        <source>Automatically run saving mode when using battery</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠶᠡᠰ ᠡᠨᠧᠷᠭᠢ ᠬᠡᠮᠨᠡᠬᠦ ᠵᠠᠭᠪᠤᠷ᠎ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="679"/>
        <location filename="../../../plugins/system/power/power.cpp" line="680"/>
        <source>Display remaining charging time and usage time</source>
        <translation>ᠦᠯᠡᠳᠡᠪᠦᠷᠢ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠴᠡᠨᠡᠭᠯᠡᠬᠦ ᠴᠠᠭ ᠪᠠ ᠦᠯᠡᠳᠡᠪᠦᠷᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠴᠠᠭ᠎ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="164"/>
        <source>General</source>
        <translation>ᠳᠦᠷᠢᠮᠵᠢᠯ</translation>
        <extra-contents_path>/Power/General</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="349"/>
        <source>Select Powerplan</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠬᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠲᠥᠯᠥᠪᠯᠡᠭᠡ</translation>
        <extra-contents_path>/Power/Select Powerplan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="407"/>
        <source>Battery saving plan</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ᠎ᠶ᠋ᠢᠨ ᠡᠨᠧᠷᠭᠢ ᠬᠡᠮᠨᠡᠬᠦ ᠲᠥᠯᠥᠪᠯᠡᠭᠡ</translation>
        <extra-contents_path>/Power/Battery saving plan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>nothing</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>blank</source>
        <translation>ᠦᠵᠡᠬᠦᠷ ᠢ᠋ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>suspend</source>
        <translation>ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>hibernate</source>
        <translation>ᠢᠴᠡᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <source>interactive</source>
        <translation>ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <source>5min</source>
        <translation>5 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>never</source>
        <translation>ᠶᠡᠷᠦ ᠡᠴᠡ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>3h</source>
        <translation>3 ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Balance</source>
        <translation>ᠲᠡᠩᠴᠡᠭᠦᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Energy Efficiency</source>
        <translation>ᠬᠠᠮᠤᠭ ᠰᠠᠢᠨ ᠡᠨᠧᠷᠭᠢ᠎ᠶ᠋ᠢᠨ ᠠᠰᠢᠭᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="770"/>
        <location filename="../../../plugins/system/power/power.cpp" line="775"/>
        <source>Performance</source>
        <translation>ᠬᠠᠮᠤᠭ ᠰᠠᠢᠨ ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Performance Model</source>
        <translation type="vanished">ᠣᠨᠴᠠᠭᠠᠢ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <source>20min</source>
        <translation type="vanished">20分钟</translation>
    </message>
    <message>
        <source>10minn</source>
        <translation type="vanished">10分钟</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>15min</source>
        <translation>15 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>30min</source>
        <translation>30 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>1h</source>
        <translation>1 ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>2h</source>
        <translation>2 ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="750"/>
        <location filename="../../../plugins/system/power/power.cpp" line="760"/>
        <source>10min</source>
        <translation>10 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="733"/>
        <location filename="../../../plugins/system/power/power.cpp" line="743"/>
        <location filename="../../../plugins/system/power/power.cpp" line="781"/>
        <source>shutdown</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Printer</name>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="43"/>
        <source>Printer</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="133"/>
        <source>Printers</source>
        <translatorcomment>打印机</translatorcomment>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ</translation>
        <extra-contents_path>/Printer/Printers</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="179"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
        <extra-contents_path>/Printer/Add</extra-contents_path>
    </message>
</context>
<context>
    <name>PrivacyDialog</name>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="11"/>
        <source>Set</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="26"/>
        <source>End User License Agreement and Privacy Policy Statement of Kylin</source>
        <translation>ᠳᠡᠭᠷᠢ ᠵᠢᠨ ᠣᠶᠣᠳᠠᠯ ᠴᠢ ᠯᠢᠨ ᠤ᠋ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯᠴᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠤ᠋ᠨ ᠭᠡᠷ᠎ᠡ ᠪᠤᠯᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ ᠵᠢᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="31"/>
        <source>Dear users of Kylin operating system and relevant products, 
         This agreement describes your rights, obligations and prerequisites for your use of this product. Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”).
        “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products.

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021
        The Agreement shall include the following content:
        I.     User license 
        II.    Java technology limitations
        III.   Cookies and other technologies
        IV.    Intellectual property clause
        V.     Open source code
        VI.   The third-party software/services
        VII.  Escape clause
        VIII. Integrity and severability of the Agreement
        IX.    Applicable law and dispute settlement

        I.      User license
        According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
        1.     User license for educational institutions
        In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product.
        2.     Use of the font software
        Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

        II.    Java technology limitations
        You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

        III.   Cookies and other technologies
        In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
        If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers.
        In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

        IV.   Intellectual property clause
        1.    Trademarks and Logos
        This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
        2.    Duplication, modification and distribution
        If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
        Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws.

        V.    Open source code
        For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

        VI.  The third-party software/services
        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
        We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

        VII. Escape clause
        1.    Limited warranty
        We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
        2.   Disclaimer
        In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
        3.   Limitation of responsibility
        To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

        VIII.Integrity and severability of the Agreement
        1.    The integrity of the Agreement
        The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
        2.   Severability of the Agreement
        If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

        IX.  Applicable law and dispute settlement
        1.   Application of governing laws
        Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
        2.  Termination
        If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
        The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.
        The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

        Privacy Policy Statement of Kylin Operating System/n        Release date of the version: July 30, 2021
        Effective date of the version: July 30, 2021

        We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.

        The Statement shall include the following content:
        I.   Collection and use your personal information
        II.  How to store and protect your personal information
        III. How to manage your personal information
        IV.  Privacy of the third-party software/services
        V.   Minors’ use of the products
        VI.  How to update this Statement
        VII. How to contact us

        I.     How to collect and use your personal information
        1.    The collection of personal information
        We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
        1)   The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
        2)   Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
        3)   Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
        4)   Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
        5)   The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
        6)   This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
        7)   The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
        8)   In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.
        2.   Use of personal information
        We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
        1)   The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
        2)   We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
        3)   We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
        4)   We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
        5)   We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
        6)   We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications andother channels;
        7)   In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.
        3.   Information sharing and provision
        We shall not share or transfer your personal information to any third party, except for the following circumstances:
        1)   After obtaining your clear consent, we shall share your personal information with the third parities;
        2)   In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
        3)   We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
        4)   With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
        5)   If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.
        4.   Exceptions with authorized consent
        1)   It is directly related to national security, national defense security and other national interests; 
        2)   It is directly related to public safety, public health and public knowledge and other major public interests; 
        3)   It is directly related to crime investigation, prosecution, judgment and execution of judgment; 
        4)   It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent; 
        5)   The personal information collected is disclosed to the public by yourself; 
        6)   Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels; 
        7)   It is necessary to sign and perform of the contract according to your requirement; 
        8)   It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
        9)   It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
        10) Other circumstances specified in the laws and regulations.

        II.   How to store and protect personal information
        1.   Information storage place
        We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
        2.   Information storage duration 
        Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
        When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
        3.   How to protect the information
        We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
        We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
        We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
        4.   Emergency response plan
        In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

        III. How to manage your personal information
        If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs. 
        Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

        IV.  Privacy of the third-party software/services

        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
        When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

        V.   Minors’ use of the products
        If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

        VI.  How to update this Statement
        We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the accountcreated by you in this product; if you are a guardian, please help your minor child to close the account created by him/her in this product.

        VII. How to contact us
        If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn). 
        We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
        The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
        Last date of updating: November 1, 2021

Address:
        Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
        Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
        Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
        Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.:
        Tianjin (022) 58955650      Beijing (010) 51659955
        Changsha (0731) 88280170        Shanghai (021) 51098866
Fax:
        Tianjin (022) 58955651      Beijing (010) 62800607
        Changsha (0731) 88280166        Shanghai (021) 51062866

        Company website: www.kylinos.cn
        E-mail: support@kylinos.cn</source>
        <translation>ᠬᠦᠨᠳᠦᠲᠦ ᠲᠩᠷᠢ᠎ᠶ᠋ᠢᠨ ᠣᠶᠣᠳᠠᠯ᠎ᠤ᠋ᠨ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠵᠢᠴᠢ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠄
 ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠨᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠡᠷᠬᠡ ᠂ ᠡᠭᠦᠷᠭᠡ ᠵᠢᠴᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠶ᠋ᠢᠨ ᠤᠷᠢᠳᠠᠯ ᠨᠥᠬᠥᠴᠡᠯ᠎ᠢ᠋ ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ ᠂ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠬᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ ︵ ᠨᠢᠭᠡᠳᠦᠯᠲᠡᠢ᠎ᠪᠡᠷ ︽ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ︾ ᠭᠡᠳᠡᠭ ︶ ᠵᠢᠴᠢ ᠲᠡᠭᠷᠢ᠎ᠶ᠋ᠢᠨ ᠣᠶᠣᠳᠠᠯ᠎ᠤ᠋ᠨ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠬᠤᠪᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢᠨ ᠲᠥᠷᠥ᠎ᠶ᠋ᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠨᠠᠷᠢᠨ ᠤᠩᠰᠢᠵᠤ ᠥᠭᠭᠦᠭᠡᠷᠡᠢ ︵ ᠳᠣᠷᠣᠭᠰᠢ ︽ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ ︾ ᠭᠡᠵᠦ ᠲᠣᠪᠴᠢᠯᠠᠨ᠎ᠠ ︶ ᠃
 ᠲᠤᠰ ᠵᠥᠪᠯᠡᠯᠴᠡᠬᠡᠷ ᠵᠢᠴᠢ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠳ᠋ᠡᠬᠢ ︽ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ ︾ ᠭᠡᠳᠡᠭ ᠨᠢ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ ᠰᠣᠹᠲ᠎ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠲᠤ ᠺᠣᠮᠫᠠᠨᠢ ᠡᠴᠡ ᠨᠡᠭᠡᠭᠡᠨ ᠬᠥᠭᠵᠢᠭᠦᠯᠦᠭᠰᠡᠨ ᠪᠥᠭᠡᠳ ᠦᠢᠯᠡᠳᠴᠦ ᠲᠠᠷᠬᠠᠭᠠᠭᠰᠠᠨ ᠠᠯᠪᠠᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ ᠵᠢᠴᠢ ᠵᠠᠰᠠᠭ᠎ᠤ᠋ᠨ ᠣᠷᠳᠣᠨ᠎ᠤ᠋ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢᠴᠢᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠰᠠᠭᠤᠷᠢ ᠪᠡᠯᠡᠳᠬᠡᠮᠵᠢ ︱ ︽ ᠮᠥᠩᠭᠥᠨ ᠭᠣᠣᠯ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠬᠡᠷᠡᠭᠰᠡᠯ᠎ᠦ᠋ᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ ︾ ᠢ᠋ ᠵᠢᠭᠠᠵᠤ ᠪᠣᠢ ᠃ 《 ᠪᠢᠳᠡ 》 ᠭᠡᠵᠦ ᠴᠢ ᠯᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠲᠤ ᠺᠣᠮᠫᠠᠨᠢ᠎ᠶ᠋ᠢ ᠵᠢᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠃ 《 ᠲᠠ 》 ᠭᠡᠳᠡᠭ ᠨᠢ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠭᠰᠠᠨ ᠵᠠᠷᠤᠳᠠᠯ᠎ᠢ᠋ ᠥᠭᠬᠦ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠲᠩᠷᠢ᠎ᠶ᠋ᠢᠨ ᠣᠶᠣᠳᠠᠯ᠎ᠤ᠋ᠨ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ᠎ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠵᠢᠴᠢ ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ᠎ᠢ᠋ ᠵᠢᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠃
 ᠶᠢᠨ ᠾᠧ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢ ᠡᠴᠦᠰ᠎ᠲᠡᠭᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ
 ᠬᠡᠪᠯᠡᠯ ᠨᠡᠶᠢᠲᠡᠯᠡᠬᠦ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ：2021 - 7 - 30
 ᠬᠡᠪᠯᠡᠯ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠪᠣᠯᠬᠤ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠄ 〔 2011 〕 ᠣᠨ 〔 7 〕 ᠰᠠᠷ᠎ᠠ 〔 30 〕 ᠤ᠋ ᠡᠳᠦᠷ
 ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠨᠢ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠳᠣᠣᠷᠠᠬᠢ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠨ᠎ᠠ᠄
 ᠨᠢᠭᠡ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ
 ᠬᠣᠶᠠᠷ ᠂ Java ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ
 ᠭᠤᠷᠪᠠ ᠂ Cookie ᠪᠠ ᠪᠤᠰᠤᠳ ᠮᠡᠷᠭᠡᠵᠢᠯ
 ᠳᠥᠷᠪᠡ ᠂ ᠮᠡᠳᠡᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠦ᠌ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ᠎ᠶ᠋ᠢᠨ ᠲᠤᠬᠠᠢ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ
 ᠲᠠᠪᠤ ᠂ ᠨᠡᠭᠡᠭᠡᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠡᠬᠢ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯ
 ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠂ ᠭᠤᠷᠪᠠᠳᠠᠬᠢ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ
 ᠳᠣᠯᠣᠭ᠎ᠠ ᠂ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠠ᠋ᠴᠠ ᠬᠡᠯᠲᠦᠷᠢᠭᠦᠯᠬᠦ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ
 ᠨᠠᠢ᠍ᠮᠠ ᠂ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠪᠦᠷᠢᠨ ᠪᠦᠲᠦᠨ ᠴᠢᠨᠠᠷ ᠵᠢᠴᠢ ᠰᠠᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠴᠢᠨᠠᠷ᠎ᠤ᠋ᠨ ᠲᠤᠬᠠᠢ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ
 ᠶᠢᠰᠦ ᠂ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢᠴᠢ ᠮᠠᠷᠭᠤᠯᠳᠤᠭᠠᠨ᠎ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ
 ᠨᠢᠭᠡ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ
 ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠨᠢᠭᠡᠨᠲᠡ ᠵᠠᠷᠤᠳᠠᠯ ᠦᠭᠭᠦᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠲᠣᠭ᠎ᠠ ᠵᠢᠴᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠬᠠᠲᠠᠭᠤ ᠬᠡᠷᠡᠭᠰᠡᠯ᠎ᠦ᠋ᠨ ᠲᠥᠷᠥᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠣᠰᠣᠭᠠᠷ ᠂ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠡᠭᠦᠨ᠎ᠢ᠋ ᠦᠯᠦ ᠭᠠᠳᠠᠭᠤᠷᠴᠢᠯᠠᠬᠤ ᠂ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ ᠥᠭᠴᠦ ᠦᠯᠦ ᠪᠣᠯᠬᠤ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠣᠯᠭᠣᠵᠤ ᠂ ᠵᠥᠪᠬᠡᠨ ᠡᠷᠬᠡ ᠣᠯᠤᠭᠰᠠᠨ ᠬᠥᠮᠦᠨ ᠨᠢᠭᠡᠴᠡ ᠵᠢᠴᠢ ᠲᠡᠭᠦᠨ᠎ᠲᠡᠢ ᠬᠥᠳᠡᠯᠮᠦᠷᠢ᠎ᠶ᠋ᠢᠨ ᠭᠡᠷ᠎ᠡ ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠠᠵᠢᠯᠲᠠᠨ ᠠᠵᠢᠯᠴᠢᠨ᠎ᠤ᠋ ᠴᠢᠯ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠬᠠᠩᠭᠠᠭᠰᠠᠨ ᠳᠠᠭᠠᠯᠲᠠ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠪᠠ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠵᠢᠴᠢ ᠶᠠᠮᠠᠷᠪᠠ ᠪᠤᠷᠤᠭᠤ᠎ᠶ᠋ᠢ ᠵᠠᠯᠠᠷᠠᠭᠤᠯᠬᠤ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ ᠃
 1. ᠰᠤᠷᠭᠠᠨ ᠬᠥᠮᠦᠵᠢᠯ᠎ᠦ᠋ᠨ ᠪᠠᠢᠭᠤᠯᠤᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ
 ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠵᠦᠢᠯ ᠪᠠᠳᠠᠭ ᠪᠠ ᠨᠥᠬᠥᠴᠡᠯ᠎ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠳᠣᠣᠷ᠎ᠠ ᠂ ᠬᠡᠷᠪᠡ ᠲᠠ ᠰᠤᠷᠭᠠᠨ ᠬᠥᠮᠦᠵᠢᠯ᠎ᠦ᠋ᠨ ᠪᠠᠢᠭᠤᠯᠤᠮᠵᠢ ᠪᠣᠯᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠬᠦᠷᠪᠡᠯ ᠂ ᠡᠷᠬᠢᠮ ᠪᠠᠢᠭᠤᠯᠤᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠬᠡᠨ ᠳᠣᠲᠣᠭᠠᠳᠤ᠎ᠳ᠋ᠤ᠌ ᠳᠠᠭᠠᠯᠳᠤᠭᠤᠯᠤᠨ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠬᠣᠶᠠᠷᠲᠤ᠎ᠶ᠋ᠢᠨ ᠳᠦᠷᠢᠮᠲᠦ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ ᠃ ᠡᠨᠳᠡ ᠬᠡᠯᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ 《 ᠳᠣᠲᠣᠭᠠᠳᠤ᠎ᠳ᠋ᠠᠭᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ 》 ᠭᠡᠳᠡᠭ ᠨᠢ ᠡᠷᠬᠡ ᠣᠯᠤᠭᠰᠠᠨ ᠬᠥᠮᠦᠨ ᠨᠢᠭᠡᠴᠡ ᠵᠢᠴᠢ ᠲᠡᠭᠦᠨ᠎ᠯᠦ᠍ᠭᠡ ᠬᠥᠳᠡᠯᠮᠦᠷᠢ᠎ᠶ᠋ᠢᠨ ᠭᠡᠷ᠎ᠡ ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠠᠵᠢᠯᠲᠠᠨ ᠵᠢᠴᠢ ᠡᠷᠬᠢᠮ ᠪᠠᠢᠭᠤᠯᠤᠮᠵᠢ᠎ᠳ᠋ᠤ᠌ ᠰᠤᠷᠭᠠᠭᠤᠯᠢ᠎ᠳ᠋ᠤ᠌ ᠣᠷᠣᠭᠰᠠᠨ ᠰᠤᠷᠤᠭᠴᠢ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠵᠢᠭᠠᠵᠤ ᠪᠣᠢ ᠃
 2. ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ
 ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠭᠡᠳᠡᠭ ᠨᠢ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠪᠠ ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠲᠢᠭ᠎ᠦ᠋ᠨ ᠶᠠᠩᠵᠤ ᠡᠭᠦᠰᠬᠡᠬᠦ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠵᠢᠭᠠᠵᠤ ᠪᠣᠢ ᠃ ᠲᠠ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠠ᠋ᠴᠠ ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠰᠠᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠂ ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠂ ᠰᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠨᠡᠮᠡᠭᠳᠡᠭᠰᠡᠨ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠨᠢᠭᠡ ᠬᠡᠰᠡᠭ ᠨᠢ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠤᠰᠢᠶᠠᠬᠤ ᠦᠶ᠎ᠡ᠎ᠳ᠋ᠦ᠍ ᠪᠦᠷᠢᠯᠳᠦᠭᠦᠯᠦᠭᠰᠡᠨ ᠥᠬᠡᠢ ᠠᠯᠢᠪᠠ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠶ᠋ᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠂ ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ᠎ᠤ᠋ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠪᠣᠯᠭᠠᠨ ᠰᠣᠯᠢᠵᠤ ᠰᠦᠢᠳᠬᠡᠯ ᠬᠤᠷᠢᠶᠠᠬᠤ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠬᠥᠯᠥᠰᠥ ᠣᠯᠭᠣᠮᠵᠢ᠎ᠶ᠋ᠢ ᠣᠯᠭᠣᠬᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ᠎ᠢ᠋ ᠰᠢᠭᠢᠳᠬᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠂ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠮᠠᠰᠢᠨ᠎ᠢ᠋ ᠤᠭᠰᠠᠷᠠᠬᠤ᠎ᠠ᠋ᠴᠠ ᠰᠠᠯᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠃ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠂ ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠭᠠᠳᠠᠭᠠᠳᠤ ᠤᠬᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠡᠷᠭᠡ ᠪᠤᠰᠤᠳ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠴᠡᠭᠡ᠎ᠳ᠋ᠦ᠍ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠶᠡᠰ ᠂ ᠲᠠ ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠲᠢᠭ᠎ᠦ᠋ᠨ ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠡᠷᠬᠡ᠎ᠲᠡᠢ ᠦᠢᠯᠡᠳᠪᠦᠷᠢ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠴᠢᠨ᠎ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠵᠤ ᠵᠥᠪᠳᠡᠯᠴᠡᠨ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠢ᠋ ᠣᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃
 ᠬᠣᠶᠠᠷ ᠂ Java ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ
 ᠲᠠ 《 Java ᠲᠠᠪᠴᠠᠩ᠎ᠤ᠋ᠨ ᠵᠤᠯᠭᠠᠭᠤᠷᠢ 》 (᠎ᠳᠥᠭᠥᠮ᠎ᠢ᠋ᠶ᠋ᠡᠷ 《 JPI 》 ᠭᠡᠳᠡᠭ ᠂ ᠳᠠᠷᠤᠢ 《 java 》 ᠪᠣᠭᠴᠣ ᠪᠤᠶᠤ 《 java 》 ᠪᠣᠭᠴᠣ᠎ᠶ᠋ᠢᠨ ᠠᠯᠢ ᠴᠤ᠌ ᠲᠥᠷᠥᠯ）᠎ᠢ᠋ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠂ JPIᠳᠤ ᠢᠯᠡᠭᠦᠴᠡ ᠬᠡᠯᠪᠡᠷᠢ ᠭᠡᠵᠦ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠴᠤ᠌ ᠲᠡᠷᠡ ᠂ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠪᠤᠰᠤᠳ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ JPI᠎ᠲᠠᠬᠢ ᠲᠥᠷᠥᠯ᠎ᠦ᠋ᠨ ᠨᠡᠮᠡᠯᠲᠡ ᠪᠤᠶᠤ ᠨᠡᠩ ᠬᠥᠳᠡᠯᠦᠯ᠎ᠲᠡᠢ ᠪᠣᠯᠭᠠᠭᠰᠠᠨ ᠴᠤ᠌ ᠲᠡᠷᠡ ᠂ ᠴᠥᠮ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠃ ᠬᠡᠷᠪᠡ ᠲᠠ ᠨᠢᠭᠡᠨ ᠢᠯᠡᠭᠦᠴᠡ ᠲᠥᠷᠥᠯ ᠵᠢᠴᠢ ᠨᠢᠭᠡ ᠪᠤᠶᠤ ᠣᠯᠠᠨ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ API ᠢ ᠪᠠᠢᠭᠤᠯᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠬᠦᠷᠪᠡᠯ ᠂ ᠲᠡᠳᠡᠭᠡᠷ ᠨᠢ Java ᠲᠠᠪᠴᠠᠩ᠎ᠤ᠋ᠨ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠶ᠋ᠢ ᠥᠷᠭᠡᠳᠬᠡᠳᠡᠭ ︔ ᠲᠡᠷᠡᠴᠢᠯᠡᠨ (᠎ii )᠎ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠨ ᠠᠰᠢᠭᠯᠠᠭᠴᠢᠳ᠎ᠤ᠋ᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠷᠠᠳᠤᠭᠰᠠᠨ ᠢᠯᠡᠭᠦᠴᠡ API ᠪᠤᠰᠤ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠨ ᠬᠥᠭᠵᠢᠭᠦᠯᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠶᠠᠪᠤᠳᠠᠯ᠎ᠢ᠋ ᠬᠠᠩᠭᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠂ ᠲᠠ ᠡᠷᠬᠡᠪᠰᠢ ᠡᠢᠮᠦᠷᠬᠦ API ᠵᠢᠨ ᠲᠤᠬᠠᠢ ᠣᠨᠣᠪᠴᠢᠲᠠᠢ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢ ᠳᠠᠷᠤᠢ ᠲᠦᠷᠭᠡᠨ ᠨᠡᠢᠲᠡᠯᠡᠵᠦ ᠂ ᠪᠣᠢ ᠪᠦᠬᠦᠢ ᠨᠡᠭᠡᠭᠡᠨ ᠠᠰᠢᠭᠯᠠᠭᠴᠢ᠎ᠳ᠋ᠤ᠌ ᠦᠨ᠎ᠡ ᠲᠥᠯᠥᠪᠦᠷᠢ ᠦᠭᠡᠢ᠎ᠪᠡᠷ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠬᠠᠩᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃ ᠲᠠ ᠪᠤᠰᠤᠳ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠭᠳᠡᠭᠴᠢ᠎ᠳ᠋ᠦ᠍ ᠶᠠᠮᠠᠷᠪᠠ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠪᠡᠷ 《 java 》 ᠂ 《 javax 》 ᠂ 《 sun》 ᠭᠡᠵᠦ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠭᠰᠡᠨ ᠢᠯᠡᠭᠦᠴᠡ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠂ ᠵᠠᠭᠠᠭ ᠭᠠᠳᠠᠷᠭᠤ ᠂ ᠪᠣᠭᠴᠣ ᠪᠤᠶᠤ Sun᠎ᠦ᠌ ᠶᠠᠮᠠᠷᠪᠠ ᠨᠡᠷᠡᠢᠳᠦᠯ᠎ᠦ᠋ᠨ ᠭᠡᠷ᠎ᠡ᠎ᠳ᠋ᠦ᠍ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠠᠳᠠᠯᠢᠪᠲᠤᠷ ᠪᠣᠯᠵᠣᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠡᠭᠦᠳᠬᠦ ᠦᠭᠡᠢ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃ Javaᠤᠨ ᠶᠠᠪᠤᠭᠳᠠᠬᠤ ᠣᠷᠴᠢᠨ᠎ᠤ᠋ ᠬᠣᠶᠠᠷᠲᠤ᠎ᠶ᠋ᠢᠨ ᠳᠦᠷᠢᠮᠲᠦ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠵᠣᠬᠢᠮᠵᠢᠲᠠᠢ ᠬᠡᠪᠯᠡᠯ (᠎ᠣᠳᠣ http : / / jdk . java . net )᠎ᠢ ᠯᠠᠪᠯᠠᠵᠤ ᠂ Java ᠵᠢᠵᠢᠭ ᠫᠷᠤᠭ᠌ᠷᠠᠮ ᠪᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠲᠠᠢ ᠬᠠᠮᠲᠤ᠎ᠪᠠᠷ ᠲᠠᠷᠬᠠᠭᠠᠭᠰᠠᠨ ᠶᠠᠪᠤᠭᠳᠠᠬᠤ ᠦᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠬᠠᠩᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠪᠠᠢᠳᠠᠯ᠎ᠢ᠋ ᠣᠢᠯᠠᠭᠠᠨ᠎ᠠ ᠃
 ᠭᠤᠷᠪᠠ ᠂ Cookie ᠪᠠ ᠪᠤᠰᠤᠳ ᠮᠡᠷᠭᠡᠵᠢᠯ
 ᠪᠢᠳᠡᠨ᠎ᠢ᠋ ᠬᠠᠪᠰᠤᠷᠴᠤ ᠪᠠᠢᠭᠠᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ᠎ᠢ᠋ ᠨᠡᠩ ᠰᠠᠢᠨ ᠣᠢᠯᠠᠭᠠᠬᠤ ᠪᠠ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠯᠬᠦ᠎ᠶ᠋ᠢᠨ ᠲᠥᠯᠥᠭᠡ ᠂ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠨᠧᠲ ᠥᠷᠲᠡᠭᠡ ᠂ ᠨᠧᠲ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠨᠢ ︽ Cookie ︾ ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠮᠡᠳᠡᠨ᠎ᠡ ᠃ ᠡᠳᠡᠭᠡᠷ Cookieᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠨᠧᠲ ᠤᠷᠤᠰᠬᠠᠯ᠎ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠎ᠶ᠋ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢᠴᠢ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠰᠠᠨ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ᠎ᠠ᠋ᠴᠠ ᠪᠣᠯᠤᠭᠰᠠᠨ ᠤᠷᠤᠰᠬᠠᠭᠳᠠᠯ᠎ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠳᠡᠭ ᠂ ᠡᠢᠮᠦ᠎ᠡᠴᠡ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃ ᠪᠢᠳᠡ ᠡᠳᠡᠭᠡᠷ Cookieᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠲᠠᠨ᠎ᠤ᠋ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ᠌ ᠥᠷᠲᠡᠭᠡ ᠪᠣᠯᠤᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠳ᠋ᠦ᠍ ᠭᠠᠷᠴᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ ᠰᠦᠯᠵᠢᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ᠎ᠪᠠᠨ ᠬᠡᠷᠬᠢᠨ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ᠎ᠶ᠋ᠢ ᠣᠢᠯᠠᠭᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 ᠬᠡᠷᠪᠡ ᠲᠠ Cookieᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ Firefox ᠬᠠᠢᠭᠤᠷ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠭᠡᠪᠡᠯ Firefox ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠪᠤᠶᠤ ᠠᠶᠤᠯ ᠦᠭᠡᠢ ᠲᠥᠪ᠎ᠲᠦ᠍ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃ ᠬᠡᠷᠪᠡ ᠲᠠ ᠪᠤᠰᠤᠳ ᠬᠠᠢᠭᠤᠷ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠪᠣᠯ ᠂ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠬᠠᠩᠭᠠᠭᠴᠢ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠴᠢᠨ᠎ᠠ᠋ᠴᠠ ᠪᠣᠳᠠᠲᠠᠢ ᠲᠥᠰᠦᠯ ᠯᠠᠪᠯᠠᠭᠠᠷᠠᠢ ᠃
 《 ᠪᠦᠭᠦᠳᠡ ᠨᠠᠢᠷᠠᠮᠳᠠᠬᠤ ᠳᠤᠮᠳᠠᠳᠤ ᠠᠷᠠᠳ ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ᠌ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠠᠤᠯᠢ 》 ᠶ᠋ᠢᠨ ᠳᠠᠯᠠᠨ ᠵᠢᠷᠭᠤᠳᠤᠭᠠᠷ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠲᠠᠪᠤᠳᠤᠭᠠᠷ ᠵᠤᠷᠪᠤᠰ᠎ᠤ᠋ᠨ ᠶᠣᠰᠣᠭᠠᠷ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠭᠡᠳᠡᠭ ᠨᠢ ᠡᠯᠧᠺᠲ᠋ᠷᠣᠨ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠡᠮᠳᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠲᠤᠰᠠᠭᠠᠷ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠯᠤᠭ᠎ᠠ ᠤᠶᠠᠯᠳᠤᠨ ᠪᠠᠢᠭᠠᠯᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠪᠡᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠬᠢᠷᠢ ᠭᠡᠮ᠎ᠢ᠋ ᠢᠯᠭᠠᠨ ᠲᠠᠨᠢᠬᠤ ᠡᠯ᠎ᠡ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠵᠢᠭᠠᠵᠤ ᠪᠣᠢ ᠂ ᠡᠭᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠪᠠᠢᠭᠠᠯᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠣᠪᠣᠭ ᠨᠡᠷ᠎ᠡ ᠂ ᠲᠥᠷᠥᠭᠰᠡᠨ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠂ ᠪᠡᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠦᠨᠡᠮᠯᠡᠯ᠎ᠦ᠋ᠨ ᠨᠣᠮᠧᠷ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ᠎ᠢ᠋ ᠢᠯᠭᠠᠨ ᠲᠠᠨᠢᠬᠤ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠂ ᠰᠠᠭᠤᠭ᠎ᠠ ᠭᠠᠵᠠᠷ ᠂ ᠤᠲᠠᠰᠤᠨ᠎ᠤ᠋ ᠨᠣᠮᠧᠷ ᠵᠡᠷᠭᠡ ᠪᠠᠭᠲᠠᠵᠤ ᠪᠣᠢ ᠃ ᠬᠡᠷᠪᠡ Cookie ᠳᠣᠲᠣᠷ᠎ᠠ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠷᠠᠳᠤᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠪᠤᠶᠤ Cookie᠎ᠢ ᠳᠠᠮᠵᠢᠨ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠪᠤᠰᠤ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠨᠢ ᠪᠤᠰᠤᠳ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠲᠠᠢ ᠨᠡᠢᠯᠡᠭᠦᠯᠦᠭᠰᠡᠨ᠎ᠦ᠌ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠪᠠᠯ ᠂ ᠲᠤᠰ ᠨᠢᠭᠤᠴᠠ ᠡᠵᠡᠮᠰᠢᠬᠦ ᠲᠥᠷᠥ᠎ᠶ᠋ᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠵᠣᠷᠢᠯᠭ᠎ᠠ᠎ᠠ᠋ᠴᠠ ᠪᠣᠯᠵᠤ ᠂ ᠪᠢᠳᠡ ᠨᠡᠢᠯᠡᠭᠦᠯᠦᠭᠰᠡᠨ᠎ᠦ᠌ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠨᠢᠭᠤᠴᠠ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠷ ᠦᠵᠡᠵᠦ ᠂ ᠲᠩᠷᠢ᠎ᠶ᠋ᠢᠨ ᠣᠶᠣᠳᠠᠯ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ ᠬᠤᠪᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢᠨ ᠲᠥᠷᠥ᠎ᠶ᠋ᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠢᠯᠡᠷᠬᠡᠢ᠎ᠪᠡᠷ ᠯᠠᠪᠯᠠᠯᠲᠠ ᠪᠣᠯᠭᠠᠵᠤ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠳ᠋ᠤ᠌ ᠵᠣᠬᠢᠴᠠᠩᠭᠤᠢ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠠᠩᠭᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 ᠳᠥᠷᠪᠡ ᠂ ᠮᠡᠳᠡᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠦ᠌ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ᠎ᠶ᠋ᠢᠨ ᠲᠤᠬᠠᠢ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ
 1 ᠲᠠᠸᠠᠷ᠎ᠤ᠋ᠨ ᠲᠡᠮᠳᠡᠭ ᠪᠠ ᠢᠯᠭᠠᠬᠤ ᠲᠡᠮᠳᠡᠭ
 ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠨᠢ ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠡᠷᠬᠡ (᠎ᠵᠣᠬᠢᠶᠠᠯ ᠪᠦᠲᠦᠭᠡᠯ᠎ᠦ᠋ᠨ ᠡᠷᠬᠡ )᠎ᠶ᠋ᠢᠨ ᠬᠠᠤᠯᠢ ᠂ ᠲᠠᠸᠠᠷ᠎ᠤ᠋ᠨ ᠲᠡᠮᠳᠡᠭ᠎ᠦ᠋ᠨ ᠬᠠᠤᠯᠢ ᠪᠠ ᠪᠤᠰᠤᠳ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠵᠢᠴᠢ ᠣᠯᠠᠨ ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠮᠡᠳᠡᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠦ᠌ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ᠎ᠶ᠋ᠢᠨ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠭᠡᠷ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢ ᠬᠦᠷᠲᠡᠨ᠎ᠡ ᠃ ᠪᠢᠳᠡ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠲᠡᠭᠦᠨ᠎ᠦ᠌ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠠᠯ᠎ᠠ ᠨᠢ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ ᠵᠢᠴᠢ ᠠᠯᠢᠪᠠ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠮᠡᠳᠡᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠦ᠌ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ᠎ᠪᠡᠨ ᠦᠯᠡᠳᠡᠭᠡᠨ᠎ᠡ ᠃ ᠪᠢᠳᠡ ᠪᠤᠶᠤ ᠲᠡᠭᠦᠨ᠎ᠦ᠌ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠭᠰᠡᠨ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠲᠠᠸᠠᠷ᠎ᠤ᠋ᠨ ᠲᠡᠮᠳᠡᠭᠲᠦ ᠂ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠲᠡᠮᠳᠡᠭ ᠂ ᠲᠡᠮᠳᠡᠭ ᠪᠤᠶᠤ ᠲᠠᠸᠠᠷ᠎ᠤ᠋ᠨ ᠲᠡᠮᠳᠡᠭᠲᠦ᠎ᠶ᠋ᠢᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠡᠷᠬᠡ ᠂ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ ᠪᠤᠶᠤ ᠠᠰᠢᠭ ᠲᠤᠰᠠ᠎ᠶ᠋ᠢᠨ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠲᠦ᠍ ᠴᠥᠮ ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠬᠤ ᠦᠭᠡᠢ ᠃ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠲᠡᠮᠳᠡᠭ᠎ᠢ᠋ ᠲᠠ ᠶᠠᠮᠠᠷᠪᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠴᠦ᠍ ᠪᠦᠷ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠡᠴᠡ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠲᠤ᠌ ᠠᠰᠢᠭᠲᠠᠢ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠂ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠪᠢᠴᠢᠭ᠎ᠢ᠋ᠶ᠋ᠡᠷ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠭᠰᠡᠨ ᠦᠭᠡᠢ ᠪᠣᠯ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠢᠨ᠎ᠦ᠌ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠲᠠᠸᠠᠷ᠎ᠤ᠋ᠨ ᠲᠡᠮᠳᠡᠭ ᠪᠠ ᠲᠡᠮᠳᠡᠭ᠎ᠢ᠋ ᠳᠤᠷ᠎ᠠ᠎ᠪᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃
 2. ᠺᠣᠫᠢᠳᠠᠬᠤ ᠂ ᠵᠠᠰᠠᠬᠤ ᠵᠢᠴᠢ ᠬᠤᠪᠢᠶᠠᠨ ᠲᠠᠷᠬᠠᠭᠠᠬᠤ ᠲᠤᠬᠠᠢ
 ᠬᠡᠷᠪᠡ ᠠᠯᠢᠪᠠ ᠬᠠᠭᠤᠯᠪᠤᠷᠢ᠎ᠶ᠋ᠢ ᠠᠪᠴᠤ ᠂ ᠠᠯᠢᠪᠠ ᠡᠲᠡᠭᠡᠳ ᠳᠣᠲᠣᠷ᠎ᠠ ᠲᠤᠰ ᠵᠥᠪᠯᠡᠯᠴᠡᠬᠡᠷ᠎ᠦ᠋ᠨ ᠪᠢᠴᠢᠭ᠎ᠢ᠋ ᠬᠤᠪᠢᠷᠠᠭᠠᠬᠤ ᠦᠭᠡᠢ ᠪᠠᠢᠪᠠᠯ ᠂ ᠲᠠ ᠡᠷᠬᠡᠪᠰᠢ 《 GNU GPL—GNU ᠨᠡᠢᠳᠡᠮ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ 》 ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ 》 ᠢ᠋ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠂ ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠵᠢᠴᠢ ᠮᠥᠩᠭᠥᠨ ᠭᠣᠣᠯ᠎ᠤ᠋ᠨ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ 《 GNU - GNU ᠨᠡᠢᠳᠡᠮ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ 》 ᠦ᠋ᠨ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠨ ᠰᠠᠬᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠡᠷᠬᠡᠪᠰᠢ 《 GNU ᠨᠡᠢᠳᠡᠮ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ 》 ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠰᠤᠳ ᠲᠡᠭᠷᠢ᠎ᠶ᠋ᠢᠨ ᠣᠶᠣᠳᠠᠯ᠎ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠂ ᠪᠤᠰᠤᠳ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠲᠣᠭᠲᠠᠯ᠎ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠵᠦ ᠳᠠᠬᠢᠨ ᠦᠢᠯᠡᠳᠬᠦ ᠂ ᠵᠠᠰᠠᠬᠤ ᠵᠢᠴᠢ ᠬᠤᠪᠢᠶᠠᠨ ᠲᠠᠷᠬᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠂ ᠭᠡᠪᠡᠴᠦ ᠠᠯᠢᠪᠠ ᠮᠥᠩᠭᠥᠨ ᠭᠣᠣᠯ᠎ᠤ᠋ᠨ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ᠶ᠋ᠡᠷ ᠰᠠᠭᠤᠷᠢ ᠪᠣᠯᠭᠠᠭᠰᠠᠨ ᠠᠯᠢᠪᠠ ᠬᠡᠪᠯᠡᠯ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠎ᠦ᠋ᠨ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠶᠠᠮᠠᠷᠪᠠ ᠪᠤᠰᠤᠳ ᠶᠠᠮᠠᠷᠪᠠ ᠪᠤᠰᠤᠳ ᠲᠡᠮᠳᠡᠭ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃
 ᠣᠨᠴᠠᠭᠠᠢᠯᠠᠨ ᠠᠩᠬᠠᠷᠬᠤ ᠨᠢ ᠄ ᠲᠤᠰ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠂ ᠵᠠᠰᠠᠬᠤ ᠵᠢᠴᠢ ᠬᠤᠪᠢᠶᠠᠨ ᠲᠠᠷᠬᠠᠭᠠᠬᠤ ᠨᠢ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠪᠠᠭᠲᠠᠬᠤ ᠦᠭᠡᠢ ︽ GNU GPL－GNU ᠨᠡᠢᠳᠡᠮ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ ︾ ᠠᠯᠢᠪᠠ ᠰᠣᠹᠲ ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠲᠩᠷᠢ᠎ᠶ᠋ᠢᠨ ᠬ᠍᠋ᠠᠶᠢ᠍᠋ᠠᠶᠢ᠍ᠰ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠰᠣᠹᠲ᠎ᠤ᠋ᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠠᠭᠤᠯᠬᠤ ᠰᠣᠹᠲ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭ᠎ᠠ ᠂ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠰᠣᠹᠲ ᠂ ᠦᠰᠦᠭ᠎ᠦ᠋ᠨ ᠬᠥᠮᠥᠷᠭᠡ᠎ᠶ᠋ᠢᠨ ᠰᠣᠹᠲ ᠂ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠰᠣᠹᠲ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃ ᠬᠠᠤᠯᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ ᠦᠭᠡᠢ ᠪᠣᠯ ᠲᠠ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠷᠠᠳᠤᠭᠰᠠᠨ ᠰᠣᠹᠲ᠎ᠢ᠋ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠂ ᠲᠡᠭᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ (᠎ᠭᠡᠳᠡᠷᠭᠦ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠨ ᠣᠷᠴᠢᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠡᠰᠡᠷᠭᠦ ᠴᠢᠭᠯᠡᠯ᠎ᠦ᠋ᠨ ᠢᠨᠵᠧᠨᠧᠷᠢᠩ᠎ᠢ᠋ ᠪᠠᠭᠲᠠᠭᠠᠨ᠎ᠠ )᠎ᠬᠤᠪᠢ ᠲᠠᠷᠬᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃
 ᠲᠠᠪᠤ ᠂ ᠨᠡᠭᠡᠭᠡᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠡᠬᠢ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯ
 ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠠᠭᠤᠯᠤᠭᠳᠠᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠶᠠᠮᠠᠷᠪᠠ ᠨᠡᠭᠡᠭᠡᠯᠲᠡᠲᠦ ᠡᠬᠢ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠲᠤᠬᠠᠢ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ ᠨᠢ ᠴᠥᠮ ᠨᠡᠭᠡᠭᠡᠯᠲᠡᠲᠦ ᠡᠬᠢ ᠺᠣᠳ᠋᠎ᠤ᠋ᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ᠎ᠳ᠋ᠦ᠍ ᠣᠯᠭᠣᠭᠰᠠᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠵᠣᠬᠢᠴᠠᠩᠭᠤᠢ ᠡᠷᠬᠡ ᠠᠰᠢᠭ ᠪᠤᠶᠤ ᠡᠭᠦᠷᠭᠡ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠵᠢᠷᠤᠮᠯᠠᠬᠤ ᠶᠣᠰᠣᠲᠠᠢ ᠡᠯ᠎ᠡ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠨᠥᠬᠥᠴᠡᠯ᠎ᠢ᠋ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ ᠂ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠨᠥᠯᠥᠭᠡᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠃
 ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠂ ᠭᠤᠷᠪᠠᠳᠠᠬᠢ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ
 ᠲᠤᠰ ᠵᠥᠪᠯᠡᠯᠴᠡᠬᠡᠷ᠎ᠲᠦ᠍ ᠵᠢᠭᠠᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠭᠡᠳᠡᠭ ᠨᠢ ᠲᠩᠷᠢ᠎ᠶ᠋ᠢᠨ ᠣᠶᠣᠳᠠᠯ ᠪᠤᠰᠤ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠴᠢᠨ᠎ᠤ᠋ ᠪᠤᠰᠤᠳ ᠵᠣᠬᠢᠶᠠᠨ ᠪᠠᠢᠭᠤᠯᠤᠯᠲᠠ ᠪᠤᠶᠤ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠨᠡᠭᠡᠭᠡᠭᠰᠡᠨ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠵᠢᠭᠠᠵᠤ ᠪᠣᠢ ᠃ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠨᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠪᠤᠶᠤ ᠬᠦᠯᠢᠬᠦ ᠮᠠᠭᠠᠳ ᠂ ᠡᠳᠡᠭᠡᠷ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠳᠠᠭᠠᠯᠲᠠ ᠬᠡᠰᠡᠭ᠎ᠦ᠋ᠨ ᠲᠤᠰᠠᠭᠠᠷ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠲᠣᠭᠲᠠᠯ ᠂ ᠳᠠ ᠳᠠᠭᠠᠯᠳᠤᠭᠤᠯᠤᠯ ᠲᠤᠰᠠᠭᠠᠷ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠲᠤᠰ ᠲᠤᠰᠠᠭᠠᠷ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ᠎ᠢ᠋ ᠬᠦᠯᠢᠶᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ᠃
 ᠪᠢᠳᠡ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠳ᠋ᠦ᠍ ᠶᠠᠮᠠᠷᠪᠠ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠡᠷᠬᠡ᠎ᠲᠡᠢ ᠪᠠᠢᠬᠤ ᠦᠭᠡᠢ ᠂ ᠪᠠᠰᠠ ᠲᠡᠭᠦᠨ᠎ᠦ᠌ ᠬᠠᠩᠭᠠᠭᠰᠠᠨ ᠲᠠᠯ᠎ᠠ ᠪᠤᠶᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠦᠢᠯᠡ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠴᠢ ᠴᠢᠨᠠᠷ ᠂ ᠣᠨᠣᠪᠴᠢᠲᠤ ᠴᠢᠨᠠᠷ ᠂ ᠪᠦᠲᠦᠮᠵᠢᠲᠦ ᠴᠢᠨᠠᠷ ᠂ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠴᠢᠨᠠᠷ᠎ᠲᠤ᠌ ᠶᠠᠮᠠᠷᠪᠠ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ ᠪᠤᠶᠤ ᠳᠤᠭᠤᠢ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ ᠬᠢᠬᠦ ᠦᠭᠡᠢ ᠃
 ᠳᠣᠯᠣᠭ᠎ᠠ ᠂ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠠ᠋ᠴᠠ ᠬᠡᠯᠲᠦᠷᠢᠭᠦᠯᠬᠦ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ
 1. ᠬᠢᠵᠠᠭᠠᠷᠲᠠᠢ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ
 ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠤᠳᠠᠯᠳᠤᠨ ᠠᠪᠤᠭᠰᠠᠨ ᠪᠤᠶᠤ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠭᠠᠷ ᠣᠯᠤᠭᠰᠠᠨ ᠡᠳᠦᠷ᠎ᠡᠴᠡ ᠡᠬᠢᠯᠡᠨ 90 (᠎90 )᠎ᠡᠳᠦᠷ᠎ᠦ᠋ᠨ ᠳᠣᠲᠣᠷ᠎ᠠ (᠎ᠪᠣᠷᠣᠯᠠᠭᠤᠯᠬᠤ ᠭᠡᠷ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ᠎ᠪᠠᠷ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠪᠣᠯᠭᠠᠨ᠎ᠠ )᠎ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠠᠭᠤᠴᠢ (᠎ᠬᠡᠷᠪᠡ ᠪᠠᠢᠪᠠᠯ )᠎ᠨᠢ ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠪᠠᠢᠳᠠᠯ ᠳᠣᠣᠷ᠎ᠠ ᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ ᠪᠠ ᠤᠷᠠᠯᠠᠯ᠎ᠤ᠋ᠨ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠳᠤᠲᠠᠭᠳᠠᠯ ᠥᠬᠡᠢ ᠭᠡᠵᠦ ᠪᠠᠲᠤᠯᠠᠨ ᠬᠡᠯᠡᠶ᠎ᠡ ᠃ ᠲᠤᠰ ᠬᠢᠵᠠᠭᠠᠷᠲᠤ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠵᠦᠢᠯ ᠳᠡᠭᠡᠷ᠎ᠡ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠣᠯᠵᠤ ᠪᠣᠯᠬᠤ ᠪᠣᠢ ᠪᠦᠬᠦᠢ ᠨᠥᠬᠥᠪᠥᠷᠢ ᠮᠥᠩᠭᠥ ᠵᠢᠴᠢ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠪᠦᠬᠦ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠪᠣᠯ ᠪᠢᠳᠡ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠵᠠᠭᠤᠴᠢ᠎ᠶ᠋ᠢ ᠰᠣᠩᠭᠣᠬᠤ ᠪᠤᠶᠤ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠬᠤᠳᠠᠯᠳᠤᠨ ᠠᠪᠬᠤ ᠵᠠᠷᠤᠳᠠᠯ᠎ᠢ᠋ ᠪᠤᠴᠠᠭᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠲᠥᠯᠥᠭᠡ ᠶᠦ᠍ᠮ ᠃
 2. ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠠ᠋ᠴᠠ ᠬᠡᠯᠲᠦᠷᠢᠭᠦᠯᠬᠦ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ
 ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠷᠠᠳᠤᠭᠰᠠᠨ ᠪᠦᠷᠢᠨ ᠪᠤᠰᠤ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠵᠦᠢᠯ ᠂ ᠤᠭ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠲᠤ᠌ 《 ᠤᠤᠯ ᠶᠠᠩᠵᠤ 》 ᠶᠣᠰᠣᠭᠠᠷ ᠬᠠᠩᠭᠠᠬᠤ ᠂ ᠶᠠᠮᠠᠷᠴᠤ ᠲᠣᠳᠣᠷᠬᠠᠢ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠪᠢᠴᠢᠬᠦ ᠪᠤᠶᠤ ᠳᠤᠭᠤᠢ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠨᠥᠬᠥᠴᠡᠯ ᠂ ᠲᠣᠭᠠᠴᠢᠯᠲᠠ ᠬᠢᠭᠡᠳ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ ᠬᠠᠩᠭᠠᠬᠤ ᠦᠭᠡᠢ ᠂ ᠡᠭᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠪᠣᠷᠣᠯᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠳᠣᠭᠢᠷᠠᠬᠤ ᠴᠢᠨᠠᠷᠲᠠᠢ ᠂ ᠣᠨᠴᠠ ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠴᠡᠭᠡᠨ᠎ᠳ᠋ᠦ᠍ ᠲᠣᠬᠢᠷᠠᠬᠤ ᠪᠤᠶᠤ ᠡᠷᠬᠡ᠎ᠳ᠋ᠦ᠍ ᠬᠠᠯᠳᠠᠬᠤ ᠦᠭᠡᠢ ᠴᠢᠨᠠᠷ᠎ᠤ᠋ᠨ ᠲᠤᠬᠠᠢ ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠳᠤᠭᠤᠢ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ ᠬᠢᠬᠦ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠦᠭᠡᠢ ᠂ ᠭᠡᠪᠡᠴᠦ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠠ᠋ᠴᠠ ᠬᠡᠯᠲᠦᠷᠢᠭᠦᠯᠬᠦ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ ᠨᠢ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢᠨ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠭᠡᠵᠦ ᠨᠤᠲᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ ᠡᠭᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠪᠠᠭᠲᠠᠬᠤ ᠦᠭᠡᠢ ᠃
 3. ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ
 ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠂ ᠶᠠᠮᠠᠷ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠤ᠋ᠨ ᠳᠣᠣᠷ᠎ᠠ ᠴᠤ᠌ ᠭᠡᠰᠡᠨ ᠂ ᠶᠠᠮᠠᠷ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠣᠨᠣᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠴᠦ᠍ ᠲᠡᠷᠡ ᠂ ᠶᠠᠮᠠᠷ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠪᠣᠯᠤᠭᠰᠠᠨ ᠴᠤ᠌ ᠲᠡᠷᠡ ᠂ ᠲᠤᠰ ᠵᠥᠭᠡᠯᠡᠨ ᠬᠡᠷᠡᠭᠰᠡᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠪᠤᠶᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ᠎ᠡᠴᠡ ᠪᠣᠯᠤᠭᠰᠠᠨ ᠪᠤᠶᠤ ᠲᠡᠭᠦᠨ᠎ᠲᠡᠢ ᠬᠣᠯᠪᠣᠭᠳᠠᠬᠤ ᠶᠠᠮᠠᠷᠪᠠ ᠠᠰᠢᠭ ᠣᠷᠣᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠬᠣᠬᠢᠷᠠᠯ ᠂ ᠠᠰᠢᠭ ᠬᠣᠨᠵᠢᠪᠤᠷᠢ ᠪᠤᠶᠤ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ᠎ᠶ᠋ᠢᠨ ᠬᠣᠬᠢᠷᠠᠯ ᠂ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠣᠨᠴᠠᠭᠠᠢ ᠂ ᠳᠠᠮ ᠂ ᠦᠷ᠎ᠡ ᠤᠷᠰᠢᠭᠲᠤ ᠴᠢᠨᠠᠷᠲᠠᠢ ᠂ ᠭᠡᠨᠡᠳᠲᠡ᠎ᠶ᠋ᠢᠨ ᠪᠤᠶᠤ ᠶᠠᠯᠠᠯᠠᠨ ᠰᠢᠳᠬᠡᠬᠦ ᠴᠢᠨᠠᠷᠲᠠᠢ ᠬᠣᠬᠢᠷᠠᠯ᠎ᠤ᠋ᠨ ᠲᠥᠯᠥᠪᠦᠷᠢ ᠂ ᠪᠢᠳᠡ ᠪᠤᠶᠤ ᠲᠡᠭᠦᠨ᠎ᠦ᠌ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠠᠯ᠎ᠠ ᠴᠥᠮ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠷᠠᠳᠤᠭᠰᠠᠨ ᠬᠣᠬᠢᠷᠠᠯ᠎ᠤ᠋ᠨ ᠲᠥᠯᠥᠪᠦᠷᠢ ᠭᠠᠷᠴᠤ ᠮᠡᠳᠡᠨ᠎ᠡ ᠭᠡᠵᠦ ᠶᠠᠮᠠᠷᠪᠠ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠡᠭᠦᠷᠭᠡᠯᠡᠬᠦ ᠦᠭᠡᠢ ᠃ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠪᠡᠯ ᠂ ᠶᠠᠮᠠᠷᠪᠠ ᠪᠠᠢᠳᠠᠯ ᠳᠣᠣᠷ᠎ᠠ ᠂ ᠭᠡᠷ᠎ᠡ ᠂ ᠡᠷᠬᠡ᠎ᠳ᠋ᠦ᠍ ᠬᠠᠯᠳᠠᠭᠰᠠᠨ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ︵ ᠣᠰᠣᠯ ᠪᠠᠭᠲᠠᠨ᠎ᠠ ︶ ᠶ᠋ᠢᠨ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠂ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠪᠤᠰᠤᠳ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠂ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠠᠷᠢᠭᠤᠴᠠᠬᠤ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠨᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠲᠤᠰ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠲᠤ᠌ ᠵᠠᠷᠤᠴᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠵᠣᠭᠣᠰ᠎ᠤ᠋ᠨ ᠲᠣᠭ᠎ᠠ᠎ᠠ᠋ᠴᠠ ᠬᠡᠲᠦᠷᠡᠬᠦ ᠦᠭᠡᠢ ᠃ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠷᠠᠳᠤᠭᠰᠠᠨ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ ᠨᠢ ᠲᠡᠭᠦᠨ᠎ᠦ᠌ ᠦᠨᠳᠦᠰᠦᠨ ᠵᠣᠷᠢᠯᠭ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠬᠦᠷᠴᠦ ᠳᠡᠢᠯᠦᠭᠰᠡᠨ ᠥᠬᠡᠢ ᠴᠦ᠍ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠷᠠᠳᠤᠭᠰᠠᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠮᠥᠨ ᠬᠦ᠌ ᠲᠣᠬᠢᠷᠠᠨ᠎ᠠ ᠃
 ᠨᠠᠢ᠍ᠮᠠ ᠂ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠪᠦᠷᠢᠨ ᠪᠦᠲᠦᠨ ᠴᠢᠨᠠᠷ ᠵᠢᠴᠢ ᠰᠠᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠴᠢᠨᠠᠷ᠎ᠤ᠋ᠨ ᠲᠤᠬᠠᠢ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ
 ᠭᠡᠷ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠷᠢᠨ ᠪᠦᠲᠦᠨ ᠴᠢᠨᠠᠷ
 ᠤᠭ ᠭᠡᠷ᠎ᠡ ᠪᠣᠯ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠲᠠᠯ᠎ᠠ᠎ᠪᠠᠷ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠦᠷᠢᠨ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠃ ᠲᠡᠷᠡ ᠨᠢ ᠡᠭᠦᠨ᠎ᠡᠴᠡ ᠡᠮᠦᠨᠡᠬᠢ ᠪᠤᠶᠤ ᠠᠳᠠᠯᠢ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠤ᠋ ᠪᠣᠢ ᠪᠦᠬᠦᠢ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠲᠡᠢ ᠨᠢᠭᠡᠳᠦᠯ ᠦᠭᠡᠢ ᠠᠮᠠᠨ ᠶᠠᠷᠢᠶ᠎ᠠ ᠪᠤᠶᠤ ᠪᠢᠴᠢᠭ᠎ᠦ᠋ᠨ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ᠋ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠂ ᠰᠠᠨᠠᠭᠤᠯᠭ᠎ᠠ ᠂ ᠲᠣᠭᠠᠴᠢᠯᠲᠠ ᠪᠠ ᠳᠠᠭᠠᠭᠠᠯᠲᠠ᠎ᠶ᠋ᠢ ᠣᠷᠣᠯᠠᠨ᠎ᠠ ᠃ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠦᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠦᠯᠬᠦ ᠂ ᠵᠠᠬᠢᠶᠠᠯᠠᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠂ ᠬᠠᠷᠢᠭᠤ ᠪᠠᠷᠢᠮᠲᠠ ᠪᠤᠶᠤ ᠡᠯ᠎ᠡ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ᠎ᠪᠠᠨ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠲᠤᠬᠠᠢ ᠬᠢᠭᠰᠡᠨ ᠪᠤᠰᠤᠳ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠣᠯᠪᠣᠭᠠᠨ᠎ᠤ᠋ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠮᠥᠷᠭᠥᠯᠳᠦᠭᠡᠨ᠎ᠦ᠌ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ ᠪᠤᠶᠤ ᠳᠠᠭᠠᠯᠳᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ ᠨᠢ ᠴᠥᠮ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠲᠦ᠍ ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠂ ᠪᠢᠴᠢᠭ᠎ᠢ᠋ᠶ᠋ᠡᠷ ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ᠎ᠪᠠᠷ ᠳᠠᠮᠵᠢᠬᠤ ᠪᠢᠰᠢ ᠪᠥᠭᠡᠳ ᠲᠠᠯ᠎ᠠ ᠨᠢᠭᠡᠪᠦᠷᠢ᠎ᠶ᠋ᠢᠨ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠭᠰᠠᠨ ᠲᠥᠯᠥᠭᠡᠯᠡᠭᠴᠢ ᠭᠠᠷ᠎ᠤ᠋ᠨ ᠦᠰᠦᠭ ᠵᠢᠷᠤᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃
 2. ᠰᠠᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠴᠢᠨᠠᠷ
 ᠬᠡᠷᠪᠡ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠲᠣᠭᠲᠠᠭᠠᠯ᠎ᠢ᠋ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠭᠡᠵᠦ ᠦᠵᠡᠪᠡᠯ ᠂ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠬᠠᠰᠤᠬᠤ ᠨᠢ ᠮᠥᠨ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠂ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠨᠢ ᠡᠯ᠎ᠡ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠦᠨᠳᠦᠰᠦᠨ ᠵᠣᠷᠢᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠬᠠᠷᠰᠢᠯᠠᠬᠤ ᠦᠭᠡᠢ ᠪᠠᠢᠬᠤ᠎ᠶ᠋ᠢ ᠭᠠᠷᠭᠠᠪᠠᠯ (᠎ᠡᠢᠮᠦ ᠪᠠᠢᠳᠠᠯ ᠳᠣᠣᠷ᠎ᠠ ᠂ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠲᠡᠷᠡ ᠳᠠᠷᠤᠢ᠎ᠳ᠋ᠤᠨᠢ ᠵᠣᠭᠰᠣᠭᠠᠨ᠎ᠠ )᠎᠃
 ᠶᠢᠰᠦ ᠂ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢᠴᠢ ᠮᠠᠷᠭᠤᠯᠳᠤᠭᠠᠨ᠎ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ
 1. ᠬᠠᠷᠢᠶᠠᠯᠠᠯ᠎ᠤ᠋ᠨ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ
 ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠲᠡᠢ ᠬᠣᠯᠪᠣᠭᠳᠠᠬᠤ ᠶᠠᠮᠠᠷᠪᠠ ᠮᠠᠷᠭᠤᠯᠳᠤᠭᠠᠨ᠎ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ (᠎ᠡᠭᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠵᠠᠷᠭᠤ ᠬᠢᠬᠦ ᠂ ᠬᠥᠨᠳᠡᠯᠡᠨ᠎ᠡᠴᠡ ᠲᠠᠰᠤᠯᠬᠤ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠴᠤ ᠡᠭᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠭᠳᠠᠬᠤ ᠥᠬᠡᠢ )᠎ᠳ᠋ᠦ᠍ ᠴᠥᠮ ᠪᠥᠬᠥᠳᠡ ᠨᠠᠢᠷᠠᠮᠳᠠᠬᠤ ᠳᠤᠮᠳᠠᠳᠤ ᠠᠷᠠᠳ ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃ ᠪᠤᠰᠤᠳ ᠤᠯᠤᠰ ᠪᠠ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠬᠠᠤᠯᠢ ᠳᠦᠷᠢᠮ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠭᠡᠢ ᠃
 2. ᠡᠴᠦᠰᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ
 ᠬᠡᠷᠪᠡ ᠲᠤᠰ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠨᠢ ᠨᠢᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠨᠢ ᠪᠣᠯᠬᠤ ᠪᠤᠶᠤ ᠠᠯᠢ ᠨᠢᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠨᠢ ᠦᠵᠡᠭᠰᠡᠨ ᠴᠦ᠍ ᠮᠡᠳᠡᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠦ᠌ ᠥᠮᠴᠢᠯᠡᠬᠦ ᠡᠷᠬᠡ᠎ᠶ᠋ᠢᠨ ᠡᠷᠬᠡ᠎ᠳ᠋ᠦ᠍ ᠬᠠᠯᠳᠠᠭᠰᠠᠨ ᠡᠴᠡ ᠲᠥᠯᠥᠪᠦᠷᠢ ᠠᠪᠬᠤ ᠬᠠᠷᠠᠯᠲᠠ ᠪᠣᠯᠵᠤ ᠮᠡᠳᠡᠪᠡᠯ ᠂ ᠠᠯᠢ ᠨᠢᠭᠡ ᠡᠲᠡᠭᠡᠳ ᠨᠢ ᠴᠦ᠍ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠵᠣᠭᠰᠣᠭᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠳᠠᠭᠤᠰᠬᠤ᠎ᠠ᠋ᠴᠠ ᠡᠮᠦᠨ᠎ᠡ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠃ ᠲᠠ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠬᠡᠵᠢᠶ᠎ᠡ ᠴᠦ᠍ ᠳᠠᠭᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠭᠡᠪᠡᠴᠦ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠰ ᠰᠣᠹᠲ᠋ᠸᠠᠢᠷ᠎ᠤ᠋ᠨ ᠪᠦᠬᠦ ᠵᠢᠩᠬᠢᠨᠢ ᠳᠡᠪᠲᠡᠷ ᠪᠠ ᠬᠠᠭᠤᠯᠪᠤᠷᠢ᠎ᠶ᠋ᠢ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃ ᠬᠡᠷᠪᠡ ᠲᠠ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠦ᠋ᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠲᠣᠭᠲᠠᠭᠠᠯ᠎ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ ᠪᠣᠯ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠨᠢ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠳᠡᠯ᠎ᠢ᠋ ᠳᠠᠮᠵᠢᠬᠤ ᠦᠭᠡᠢ᠎ᠪᠡᠷ ᠲᠡᠷᠡ ᠳᠠᠷᠤᠢ ᠳᠠᠭᠤᠰᠤᠨ᠎ᠠ ᠃ ᠲᠡᠭᠦᠰᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠲᠠ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠰ ᠰᠤᠹᠲ᠋ᠧᠠᠢᠷ᠎ᠤ᠋ᠨ ᠪᠦᠬᠦ ᠵᠢᠩᠬᠢᠨᠢ ᠳᠡᠪᠲᠡᠷ ᠪᠠ ᠬᠠᠭᠤᠯᠪᠤᠷᠢ ᠳᠡᠪᠲᠡᠷ᠎ᠢ᠋ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠪᠥᠭᠡᠳ ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ᠎ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ᠎ᠡᠴᠡ ᠪᠣᠯᠤᠭᠰᠠᠨ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠡᠭᠦᠷᠭᠡᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃
 ᠲᠤᠰ ᠬᠡᠯᠡᠯᠴᠡᠭᠡᠷ ᠨᠢ ᠬᠢᠲᠠᠳ ᠠᠩᠭ᠌ᠯᠢ ᠬᠣᠶᠠᠷ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠬᠡᠪᠯᠡᠯ᠎ᠢ᠋ ᠬᠠᠩᠭᠠᠳᠠᠭ ᠂ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠠᠯᠢ ᠨᠢᠭᠡ ᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠢ ᠴᠦ᠍ ᠰᠠᠯᠠᠭ᠎ᠠ ᠤᠳᠬ᠎ᠠ᠎ᠲᠠᠢ ᠪᠣᠯ ᠬᠢᠲᠠᠳ ᠬᠡᠯᠡᠨ᠎ᠦ᠌ ᠬᠡᠪᠯᠡᠯ᠎ᠢ᠋ᠶ᠋ᠡᠷ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃
 ᠶᠢᠨ ᠾᠧ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠬᠤᠪᠢ᠎ᠶ᠋ᠢᠨ ᠲᠥᠷᠥ᠎ᠶ᠋ᠢᠨ ᠪᠣᠳᠣᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ
 ᠬᠡᠪᠯᠡᠯ ᠨᠡᠶᠢᠲᠡᠯᠡᠬᠦ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ：2021 - 7 - 30
 ᠬᠡᠪᠯᠡᠯ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠪᠣᠯᠬᠤ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠄ 〔 2011 〕 ᠣᠨ 〔 7 〕 ᠰᠠᠷ᠎ᠠ 〔 30 〕 ᠤ᠋ ᠡᠳᠦᠷ
 ᠪᠢᠳᠡ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠪᠣᠯᠤᠨ ᠬᠤᠪᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢ ᠲᠤᠢᠯ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠴᠢᠬᠤᠯᠠᠴᠢᠯᠠᠵᠤ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠨ᠎ᠤ᠋ ᠂ ᠵᠦᠢ ᠵᠣᠬᠢᠰᠲᠠᠢ ᠂ ᠵᠣᠬᠢᠰᠲᠠᠢ ᠂ ᠵᠣᠬᠢᠰᠲᠠᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠨ ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠂ ᠡᠵᠡᠮᠳᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠳᠣᠣᠷ᠎ᠠ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠪᠠ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠲᠤᠰᠠᠳᠠ ᠂ ᠪᠢᠳᠡ ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠲᠣᠭᠲᠠᠭᠠᠪᠠ ᠃ ᠪᠢᠳᠡ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢᠨ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ ᠪᠠ ᠠᠵᠢᠯ ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠬᠢᠨ᠎ᠦ᠌ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠦᠷᠦᠭᠰᠡᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠶᠣᠰᠣᠭᠠᠷ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠳ᠋ᠤ᠌ ᠵᠣᠬᠢᠴᠠᠩᠭᠤᠢ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠠᠩᠭᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ ᠨᠢ ᠳᠣᠣᠷᠠᠬᠢ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠨ᠎ᠠ᠄
 ᠨᠢᠭᠡ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠪᠠ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠲᠤᠬᠠᠢ
 ᠬᠣᠶᠠᠷ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠬᠡᠷᠬᠢᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠠ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠪᠣᠢ
 ᠭᠤᠷᠪᠠ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠬᠡᠷᠬᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠪᠣᠢ
 ᠳᠥᠷᠪᠡ ᠂ ᠭᠤᠷᠪᠠᠳᠠᠬᠢ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢᠨ ᠲᠤᠬᠠᠢ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ
 ᠲᠠᠪᠤ ᠂ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠦᠯᠦ ᠬᠦᠷᠦᠭᠰᠡᠳ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠲᠤᠬᠠᠢ
 ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠂ ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ ᠨᠢ ᠬᠡᠷᠬᠢᠨ ᠰᠢᠨᠡᠳᠬᠡᠬᠦ
 ᠳᠣᠯᠣᠭ᠎ᠠ ᠂ ᠬᠡᠷᠬᠢᠨ ᠪᠢᠳᠡᠨ᠎ᠲᠡᠢ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠪᠣᠢ
 ᠨᠢᠭᠡ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠬᠢᠨ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠪᠠ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠣᠢ
 1. ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ
 ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠶᠠᠪᠤᠴᠠ᠎ᠳ᠋ᠤ᠌ ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠵᠤ ᠂ ᠭᠣᠣᠯᠳᠠᠭᠤ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠩ ᠴᠢᠨᠠᠷ ᠰᠠᠢᠲᠠᠢ ᠂ ᠨᠡᠩ ᠠᠮᠠᠷᠬᠠᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠪᠠ ᠨᠡᠩ ᠰᠠᠢᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠠᠩᠭᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠲᠥᠯᠥᠭᠡ ᠶᠦ᠍ᠮ ᠃ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠡᠰᠡᠭ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠲᠠ ᠰᠢᠭᠤᠳ ᠬᠠᠩᠭᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠪᠤᠰᠤᠳ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠪᠣᠯ ᠪᠢᠳᠡ ᠲᠠ ᠪᠣᠯᠤᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠰᠣᠯᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢᠴᠢ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠠ ᠪᠡᠶᠡᠴᠢᠯᠡᠨ ᠣᠢᠯᠠᠭᠠᠬᠤ ᠠᠷᠭ᠎ᠠ᠎ᠪᠠᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠬᠤᠷᠠᠮᠳᠤᠭᠤᠯᠳᠠᠭ ᠃ ᠪᠢᠳᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠲᠣᠬᠢᠷᠠᠬᠤ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢᠨ ᠲᠣᠭᠲᠠᠭᠠᠯ᠎ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠲᠠᠨ᠎ᠤ᠋ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯ᠎ᠢ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ ᠣᠯᠤᠭᠰᠠᠨ ᠪᠣᠯ ᠂ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠰᠠᠨᠠᠭᠠᠴᠢᠯᠠᠭ᠎ᠠ᠎ᠲᠠᠢ᠎ᠪᠠᠷ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠦᠭᠡᠢ ᠶᠦ᠍ᠮ ᠃
 ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠬᠤ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠲᠣᠭᠲᠠᠴᠠ ᠨᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠨᠧᠲ ᠺᠠᠷᠲ ᠂ ᠬᠠᠲᠠᠭᠤ ᠲᠣᠨᠣᠭ ᠪᠠ ᠭᠣᠣᠯ ᠬᠠᠪᠲᠠᠰᠤ ᠵᠡᠷᠭᠡ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ ᠲᠣᠭᠲᠠᠴᠠ ᠪᠠ ᠰᠣᠯᠢᠬᠤ ᠠᠷᠭ᠎ᠠ᠎ᠪᠠᠷ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠬᠤ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠮᠠᠰᠢᠨ᠎ᠤ᠋ ᠨᠣᠮᠧᠷ᠎ᠢ᠋ ᠪᠦᠷᠢᠯᠳᠦᠭᠦᠯᠦᠨ᠎ᠡ ; ᠲᠠ ᠲᠤᠰ ᠮᠠᠰᠢᠨ᠎ᠤ᠋ ᠨᠣᠮᠧᠷ᠎ᠢ᠋ᠶ᠋ᠡᠨ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ᠎ᠤ᠋ ᠶᠠᠪᠤᠳᠠᠯ᠎ᠤ᠋ᠨ ᠠᠵᠢᠯᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠶᠠᠪᠤᠭᠤᠯᠤᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠭᠡᠷ᠎ᠡ ᠵᠢᠴᠢ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠵᠥᠪᠯᠡᠯᠴᠡᠬᠡᠷ᠎ᠦ᠋ᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠵᠦ ᠵᠢᠩᠬᠢᠨᠢ᠎ᠪᠡᠷ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ ᠃ ᠲᠤᠰ ᠮᠠᠰᠢᠨ᠎ᠤ᠋ ᠺᠣᠳ᠋ ᠨᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠨᠧᠲ ᠺᠠᠷᠲ ᠂ ᠬᠠᠲᠠᠭᠤ ᠲᠣᠨᠣᠭ ᠪᠠ ᠭᠣᠣᠯ ᠬᠠᠪᠲᠠᠰᠤ ᠵᠡᠷᠭᠡ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠪᠣᠳᠠᠲᠠᠢ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠰᠢᠭᠤᠳ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠥᠬᠡᠢ ᠃
 ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠰᠣᠹᠲ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠦᠵᠦᠭᠦᠷ ᠨᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ CPU ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ᠎ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠵᠢᠴᠢ IP ᠬᠠᠶᠢᠭ᠎ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠵᠦ ᠵᠠᠯᠭᠠᠨ᠎ᠠ ᠂ ᠡᠭᠦᠨ᠎ᠦ᠌ ᠬᠠᠮᠲᠤ ᠪᠢᠳᠡ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠰᠣᠹᠲ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠵᠤ ᠂ ᠭᠡᠪᠡᠴᠦ ᠰᠣᠹᠲ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ᠎ᠤ᠋ ᠴᠠᠭ᠎ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠂ ᠡᠯ᠎ᠡ ᠨᠢᠭᠤᠷ ᠬᠣᠭᠣᠷᠣᠨᠳᠣᠬᠢ ᠬᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠂ ᠡᠷᠢᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠂ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠪᠣᠯᠪᠠᠴᠤ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠶᠡᠷᠦ᠎ᠶ᠋ᠢᠨ ᠪᠠᠢᠳᠠᠯ ᠳᠣᠣᠷ᠎ᠠ ᠰᠣᠹᠲ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠦᠵᠦᠭᠦᠷ᠎ᠦ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠡᠳᠦᠷ᠎ᠦ᠋ᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ᠎ᠳ᠋ᠡᠭᠡᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠵᠦ ᠂ ᠪᠣᠳᠠᠲᠠᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠠᠢᠷᠢ ᠨᠢ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠲᠠᠯᠠᠪᠤᠷ᠎ᠤ᠋ᠨ ᠦᠵᠡᠭᠳᠡᠯ ᠡᠴᠡ ᠪᠣᠯᠵᠤ ᠬᠤᠪᠢᠷᠠᠯᠲᠠ᠎ᠲᠠᠢ ᠪᠠᠢᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ ᠃
 (᠎3 )᠎ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠡᠰ ᠲᠠᠪᠰᠢᠵᠤ ᠰᠢᠨᠡᠳᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ IPᠬᠠᠶᠢᠭ᠎ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠵᠦ ᠵᠠᠯᠭᠠᠨ᠎ᠠ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠳᠡᠰ ᠲᠠᠪᠰᠢᠵᠤ ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠢ᠋ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠳᠥᠭᠥᠮ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ︔
 4. ᠲᠤᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠠᠵᠢᠯ᠎ᠤ᠋ᠨ ᠶᠠᠪᠤᠯᠴᠠᠭ᠎ᠠ ᠵᠢᠴᠢ ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠡᠷᠭᠡ᠎ᠡᠴᠡ ᠪᠣᠯᠵᠣ ᠂ ᠢᠮᠧᠯ᠎ᠦ᠋ᠨ ᠬᠠᠢᠷᠴᠠᠭ ᠂ ᠤᠲᠠᠰᠤ ᠂ ᠣᠪᠣᠭ ᠨᠡᠷ᠎ᠡ ᠵᠡᠷᠭᠡ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 5. ᠲᠤᠰ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠦ᠌ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ᠎ᠤ᠋ᠨ ᠣᠨᠴᠠᠯᠢᠭ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠪᠠᠭᠠᠵᠢ ᠨᠢ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠲᠣᠨᠣᠭᠯᠠᠭᠠᠰᠤ᠎ᠶ᠋ᠢ ᠳᠠᠮᠵᠢᠨ ᠂ ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ᠋ ᠬᠡ ᠂ ᠬᠤᠷᠤᠭᠤᠨ᠎ᠤ᠋ ᠨᠠᠮᠵᠢᠭᠤᠨ ᠰᠤᠳᠠᠯ ᠂ ᠰᠣᠯᠣᠩᠭᠠᠨ ᠬᠠᠯᠢᠰᠤ ᠂ ᠳᠠᠭᠤᠨ ᠰᠤᠳᠠᠰᠤ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃ ᠲᠠᠨ᠎ᠤ᠋ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ᠎ᠤ᠋ᠨ ᠥᠪᠡᠷᠮᠢᠴᠡ ᠣᠨᠴᠠᠯᠢᠭᠲᠤ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ᠎ᠤ᠋ᠨ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠲᠦ᠍ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠂ ᠡᠨᠡ ᠬᠡᠰᠡᠭ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠪᠢᠳᠡ ᠵᠥᠪᠬᠡᠨ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠲᠤᠯᠠᠭᠰᠠᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ᠎ᠢ᠋ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠤᠨ᠎ᠠ ᠂ ᠴᠤᠭᠯᠠᠭᠤᠯᠵᠤ ᠳᠡᠭᠡᠭᠰᠢ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠦᠭᠡᠢ ᠃ ᠲᠠ ᠬᠡᠷᠪᠡ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ᠎ᠤ᠋ᠨ ᠣᠨᠴᠠᠯᠢᠭ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠰᠢᠰᠲ᠋ᠧᠮᠲᠦ ᠭᠡᠷᠡᠴᠢᠯᠡᠯ ᠬᠢᠬᠦ ᠴᠢᠬᠤᠯᠠ ᠦᠭᠡᠢ ᠪᠣᠯ ᠂ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ᠎ᠤ᠋ᠨ ᠣᠨᠴᠠᠯᠢᠭ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠪᠠᠭᠠᠵᠢ᠎ᠳ᠋ᠤ᠌ ᠲᠤᠰ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠶ᠋ᠢ ᠬᠠᠭᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 6. ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠳᠠᠭᠤ ᠰᠢᠩᠭᠡᠭᠡᠬᠦ ᠴᠢᠳᠠᠮᠵᠢ ᠬᠠᠩᠭᠠᠳᠠᠭ ᠂ ᠲᠠ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠠᠭᠤ ᠰᠢᠩᠭᠡᠭᠡᠬᠦ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠲᠤ᠌ ᠪᠢᠳᠡ ᠵᠥᠪᠬᠡᠨ ᠲᠠᠨ᠎ᠤ᠋ ᠳᠠᠭᠤ ᠰᠢᠩᠭᠡᠭᠡᠬᠦ ᠦᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠳᠠᠭᠤᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ᠎ᠤ᠋ᠨ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠳᠣᠲᠣᠷ᠎ᠠ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠂ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠪᠠ ᠳᠡᠭᠡᠭᠰᠢ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠦᠭᠡᠢ ᠃
 7 )᠎ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠠ ᠳᠡᠮᠵᠢᠯᠭᠡ᠎ᠶ᠋ᠢᠨ ᠴᠢᠳᠠᠮᠵᠢ ᠨᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠪᠢᠳᠡᠨ᠎ᠳ᠋ᠦ᠍ ᠬᠠᠩᠭᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ᠎ᠦ᠋ᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠂ ᠡᠯᠧᠺᠲ᠋ᠷᠣᠨ ᠰᠢᠤᠳᠠᠨ ᠬᠠᠢᠷᠴᠠᠭ ᠂ ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ ᠂ ᠣᠪᠣᠭ ᠨᠡᠷ᠎ᠡ ᠵᠡᠷᠭᠡ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠵᠤ ᠂ ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠠᠩᠭᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠳᠥᠭᠥᠮ ᠂ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠲᠣᠬᠢᠷᠠᠮᠵᠢᠲᠠᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃
 8 )᠎ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ ᠶᠠᠪᠤᠴᠠ᠎ᠳ᠋ᠤ᠌ ᠂ ᠬᠡᠷᠪᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠰᠢᠨ᠎ᠡ᠎ᠪᠡᠷ ᠨᠡᠮᠡᠵᠦ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠴᠢᠬᠤᠯᠠ᠎ᠲᠠᠢ ᠪᠣᠯ ᠂ ᠪᠢᠳᠡ ᠲᠤᠰ ᠬᠡᠰᠡᠭ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠴᠠᠭ ᠲᠤᠬᠠᠢ᠎ᠳ᠋ᠤᠨᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠨ᠎ᠡ ᠃
 2. ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠪᠠᠢᠳᠠᠯ
 ᠪᠢᠳᠡ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠬᠠᠤᠯᠢ ᠳᠦᠷᠢᠮ᠎ᠦ᠋ᠨ ᠲᠣᠭᠲᠠᠭᠠᠯ ᠵᠢᠴᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠪᠣᠯᠵᠣᠨ ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠶᠣᠰᠣᠭᠠᠷ ᠂ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠳᠣᠣᠷᠠᠬᠢ ᠵᠦᠢᠯ᠎ᠳ᠋ᠦ᠍ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃ ᠬᠡᠷᠪᠡ ᠪᠢᠳᠡ ᠳᠣᠣᠷᠠᠬᠢ ᠮᠡᠲᠦ ᠬᠡᠷᠡᠭᠴᠡᠭᠡ᠎ᠡᠴᠡ ᠬᠡᠲᠦᠷᠡᠭᠰᠡᠨ ᠪᠠᠢᠪᠠᠯ ᠂ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠳᠠᠬᠢᠨᠲᠠ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯ ᠬᠢᠬᠦ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠲᠠᠨ᠎ᠤ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠢ᠋ ᠣᠯᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 (᠎1 )᠎ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠦ᠋ᠨ ᠲᠣᠭᠲᠠᠴᠠ ᠂ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠰᠢᠨᠡᠳᠬᠡᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ ᠂ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ᠎ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠲᠠᠨᠢᠯᠲᠠ ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠡᠷᠭᠡ ᠬᠡᠷᠡᠭᠴᠡᠭᠡ᠎ᠳ᠋ᠦ᠍ ᠬᠣᠯᠪᠣᠭᠳᠠᠨ᠎ᠠ ︔
 2. ᠪᠢᠳᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠠᠰᠢᠭᠯᠠᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠠᠶᠤᠯᠭᠦᠢ ᠴᠢᠨᠠᠷ ᠂ ᠨᠠᠢᠳᠠᠪᠤᠷᠢᠲᠤ ᠴᠢᠨᠠᠷ ᠪᠠ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠬᠦ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠬᠠᠮᠵᠢᠨ ᠳᠡᠭᠡᠭᠰᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ;
 3 ᠂ ᠪᠢᠳᠡ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ (᠎ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠠᠩᠭᠠᠭᠰᠠᠨ ᠡᠯᠧᠺᠲ᠋ᠷᠣᠨ ᠰᠢᠤᠳᠠᠨ ᠵᠦᠢᠯ ᠂ ᠤᠲᠠᠰᠤ ᠵᠡᠷᠭᠡ )᠎ᠶ᠋ᠢ ᠠᠰᠢᠭᠯᠠᠨ ᠰᠢᠭᠤᠳ ᠲᠠᠨ᠎ᠲᠠᠢ ᠨᠡᠪᠲᠡᠷᠡᠯᠴᠡᠵᠦ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠂ ᠲᠤᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠠᠵᠢᠯ᠎ᠤ᠋ᠨ ᠬᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠂ ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠪᠤᠶᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠬᠠᠷᠢᠭᠤ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠭ᠎ᠠ ;
 4. ᠪᠢᠳᠡ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ᠎ᠶ᠋ᠢ ᠠᠰᠢᠭᠯᠠᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠣᠳᠣᠬᠠᠨ᠎ᠤ᠋ ᠳᠥᠭᠥᠮ ᠬᠡᠷᠡᠭᠴᠡᠭᠡᠲᠦ ᠴᠢᠨᠠᠷ᠎ᠢ᠋ ᠰᠠᠢᠵᠢᠷᠠᠭᠤᠯᠬᠤ ᠂ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠥᠩᠭᠡᠷᠡᠭᠡᠯᠲᠡ (᠎ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠭᠦᠷ᠎ᠦ᠋ᠨ ᠥᠪᠡᠷᠮᠢᠴᠡ ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ )᠎ᠵᠢᠴᠢ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠤᠲᠠᠭᠳᠠᠯ᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢ ᠳᠡᠭᠡᠭᠰᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ︔
 5 ᠪᠢᠳᠡ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ᠎ᠶ᠋ᠢ ᠶᠡᠬᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠵᠠᠳᠠᠯᠤᠯᠲᠠ᠎ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠂ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠶᠠᠮᠠᠷᠪᠠ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠦᠭᠡᠢ ᠬᠣᠲᠠ᠎ᠶ᠋ᠢᠨ ᠢᠯᠴᠢ᠎ᠶ᠋ᠢᠨ ᠬᠦᠴᠦᠨ᠎ᠦ᠌ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠠᠵᠢᠯ ᠲᠥᠷᠥᠯ᠎ᠦ᠋ᠨ ᠵᠠᠳᠠᠯᠤᠯ᠎ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠦᠯᠦᠯᠲᠡ᠎ᠶ᠋ᠢ ᠪᠦᠷᠢᠯᠳᠦᠭᠦᠯᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃ ᠪᠢᠳᠡ ᠭᠠᠳᠠᠭᠰᠢ ᠢᠯᠡ ᠪᠠᠢᠯᠭᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠮᠠᠨ᠎ᠤ᠋ ᠬᠠᠮᠲᠤᠷᠠᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠬᠠᠨᠢᠳᠠᠨ᠎ᠲᠠᠢ᠎ᠪᠠᠨ ᠲᠣᠭ᠎ᠠ ᠪᠦᠷᠢᠳᠬᠡᠯ᠎ᠦ᠋ᠨ ᠬᠠᠢᠯᠲᠠ᠎ᠶ᠋ᠢ ᠥᠩᠭᠡᠷᠡᠭᠦᠯᠦᠭᠰᠡᠨ᠎ᠦ᠌ ᠳᠠᠷᠠᠭ᠎ᠠ ᠪᠡᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠭᠠᠷᠤᠯ᠎ᠢ᠋ ᠢᠯᠭᠠᠨ ᠲᠣᠳᠣᠯᠠᠬᠤ ᠠᠭᠤᠯᠭ᠎ᠠ ᠦᠭᠡᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠤᠪᠢᠶᠠᠯᠴᠠᠨ ᠡᠳ᠋ᠯᠡᠵᠦ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ᠎ᠦ᠋ᠨ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠬᠡᠷᠬᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠤᠶᠤ ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ᠎ᠳ᠋ᠦ᠍ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠶᠡᠷᠦᠩᠬᠡᠢ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠨᠳᠤᠰᠢ᠎ᠶ᠋ᠢ ᠣᠢᠯᠠᠭᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ︔
 6. ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠂ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ ᠵᠢᠴᠢ ᠪᠤᠰᠤᠳ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠨᠡᠩ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠵᠠᠷ ᠮᠡᠳᠡᠭᠡ᠎ᠶ᠋ᠢ ᠬᠠᠩᠭᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ︔
 (᠎7 )᠎ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠂ ᠬᠠᠤᠯᠢ ᠲᠣᠭᠲᠠᠭᠠᠯ ᠂ ᠰᠠᠯᠠᠭ᠎ᠠ ᠮᠥᠴᠢᠷ᠎ᠦ᠋ᠨ ᠳᠦᠷᠢᠮ ᠲᠣᠭᠲᠠᠭᠠᠯ ᠂ ᠵᠠᠰᠠᠭ᠎ᠤ᠋ᠨ ᠣᠷᠳᠣᠨ᠎ᠤ᠋ ᠵᠢᠭᠠᠪᠤᠷᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠳᠠᠭᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠲᠥᠯᠥᠭᠡ ᠃
 3. ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠤᠪᠢᠶᠠᠯᠴᠠᠬᠤ ᠪᠠ ᠭᠠᠳᠠᠭᠰᠢ ᠬᠠᠩᠭᠠᠬᠤ
 ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠲᠦ᠍ ᠬᠠᠮᠲᠤ᠎ᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠪᠤᠶᠤ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ ᠥᠭᠬᠦ ᠦᠭᠡᠢ ᠂ ᠭᠡᠪᠡᠴᠦ ᠳᠣᠣᠷᠠᠬᠢ ᠪᠠᠢᠳᠠᠯ ᠡᠭᠦᠨ᠎ᠳ᠋ᠦ᠍ ᠪᠠᠭᠲᠠᠬᠤ ᠦᠭᠡᠢ ᠄
 1. ᠲᠠᠨ᠎ᠤ᠋ ᠲᠣᠳᠣᠷᠬᠠᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠢ᠋ ᠣᠯᠤᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠪᠢᠳᠡ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠲᠡᠢ᠎ᠪᠡᠨ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠤᠪᠢᠶᠠᠯᠴᠠᠨ᠎ᠠ ︔
 2. ᠭᠠᠳᠠᠨᠠᠬᠢ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠬᠠᠷᠠᠯᠲᠠ᠎ᠶ᠋ᠢ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠬᠦ᠎ᠶ᠋ᠢᠨ ᠲᠥᠯᠥᠭᠡ ᠂ ᠪᠢᠳᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠺᠣᠮᠫᠠᠨᠢ ᠪᠤᠶᠤ ᠪᠤᠰᠤᠳ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠬᠠᠮᠲᠤᠷᠠᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠬᠠᠨᠢ (᠎ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠠᠩᠭᠠᠬᠤ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠴᠢᠨ ᠂ ᠳᠠᠭᠠᠭᠠᠴᠢᠯᠠᠭᠴᠢ ᠂ ᠣᠷᠣᠯᠠᠭᠴᠢ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠡᠷᠭᠡ )᠎ᠲᠡᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠤᠪᠢᠶᠠᠯᠴᠠᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ ᠃ ᠪᠢᠳᠡ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ ᠂ ᠨᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ᠎ᠪᠠᠷ ᠲᠠᠨ᠎ᠤ᠋ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠢ᠋ ᠪᠠᠲᠤᠯᠠᠨ᠎ᠠ ︔
 ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠭᠠᠳᠠᠭᠰᠢ ᠢᠯᠡ ᠨᠡᠢᠲᠡᠯᠡᠬᠦ ᠦᠭᠡᠢ ᠂ ᠬᠡᠷᠪᠡ ᠡᠷᠬᠡᠪᠰᠢ ᠢᠯᠡ᠎ᠪᠡᠷ ᠨᠡᠢᠲᠡᠯᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ᠦᠶ᠎ᠡ᠎ᠳ᠋ᠦ᠍ ᠂ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠡᠨᠡ ᠤᠳᠠᠭᠠᠨ᠎ᠤ᠋ ᠢᠯᠡ᠎ᠪᠡᠷ ᠨᠡᠢᠲᠡᠯᠡᠭᠰᠡᠨ ᠵᠣᠷᠢᠯᠭ᠎ᠠ ᠂ ᠨᠡᠢᠲᠡᠯᠡᠭᠰᠡᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢᠨ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠵᠢᠴᠢ ᠬᠣᠯᠪᠣᠭᠳᠠᠬᠤ ᠰᠡᠷᠭᠡᠭ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠮᠡᠳᠡᠭᠳᠡᠵᠦ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠢ᠋ ᠣᠯᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ︔
 4 ᠮᠠᠨ᠎ᠤ᠋ ᠲᠤᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠠᠵᠢᠯ᠎ᠤ᠋ᠨ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠬᠥᠭᠵᠢᠬᠦ᠎ᠶ᠋ᠢ ᠳᠠᠭᠠᠯᠳᠤᠨ ᠪᠢᠳᠡ ᠨᠡᠢᠯᠡᠭᠦᠯᠬᠦ ᠂ ᠬᠤᠳᠠᠯᠳᠤᠨ ᠠᠪᠬᠤ ᠂ ᠡᠳ᠋ ᠬᠥᠷᠥᠩᠭᠡ᠎ᠪᠡᠨ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ ᠥᠭᠬᠦ ᠵᠡᠷᠭᠡ ᠠᠷᠠᠯᠵᠢᠶ᠎ᠠ ᠬᠢᠬᠦ ᠪᠣᠯᠤᠯᠴᠠᠭ᠎ᠠ᠎ᠲᠠᠢ ᠲᠤᠯᠠ ᠂ ᠪᠢᠳᠡ ᠬᠣᠯᠪᠣᠭᠳᠠᠯ ᠪᠦᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ᠎ᠢ᠋ ᠮᠡᠳᠡᠭᠳᠡᠵᠦ ᠂ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠬᠠᠤᠯᠢ ᠲᠣᠭᠲᠠᠭᠠᠯ ᠵᠢᠴᠢ ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠳ᠋ᠦ᠍ ᠱᠠᠭᠠᠷᠳᠠᠭᠰᠠᠨ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠶᠣᠰᠣᠭᠠᠷ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠰᠢᠨ᠎ᠡ ᠡᠵᠡᠮᠳᠡᠭᠴᠢ᠎ᠳ᠋ᠦ᠍ ᠱᠠᠭᠠᠷᠳᠠᠵᠤ ᠪᠠᠢᠭᠠᠳ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠨ᠎ᠠ ;
 ᠬᠡᠷᠪᠡ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡᠯᠡᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠲᠡᠢ᠎ᠪᠡᠨ ᠠᠳᠠᠯᠢ᠎ᠪᠠᠷ ᠂ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ᠎ᠳ᠋ᠤ᠌᠎ᠪᠠᠨ ᠬᠡᠯᠡᠭᠰᠡᠨ ᠵᠣᠷᠢᠯᠭ᠎ᠠ ᠪᠣᠯᠤᠨ ᠰᠢᠭᠤᠳ ᠪᠤᠶᠤ ᠵᠦᠢ ᠵᠣᠬᠢᠰᠲᠠᠢ ᠬᠣᠯᠪᠣᠭ᠎ᠠ᠎ᠲᠠᠢ ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠡᠪᠴᠢᠶ᠎ᠡ᠎ᠡᠴᠡ ᠬᠡᠲᠦᠷᠡᠭᠰᠡᠨ ᠪᠣᠯ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡᠯᠡᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠡᠴᠡ᠎ᠪᠡᠨ ᠡᠮᠦᠨ᠎ᠡ ᠳᠠᠬᠢᠨ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠡᠯᠡᠵᠦ ᠥᠭᠬᠦ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠲᠠᠨ᠎ᠤ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠢ᠋ ᠣᠯᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 4. ᠡᠷᠬᠡ ᠣᠯᠭᠣᠬᠤ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠢ᠋ ᠣᠯᠤᠭᠰᠠᠨ ᠣᠨᠴᠠᠭᠠᠢ ᠪᠠᠢᠳᠠᠯ
 (᠎1 )᠎ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠂ ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠪᠠᠲᠤᠯᠠᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠵᠡᠷᠭᠡ ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠠᠰᠢᠭ ᠲᠤᠰᠠ᠎ᠲᠠᠢ ᠰᠢᠭᠤᠳ ᠬᠣᠯᠪᠣᠭ᠎ᠠ᠎ᠲᠠᠢ ︔
 2. ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠂ ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠡᠷᠡᠭᠦᠯ ᠬᠠᠮᠠᠭᠠᠯᠠᠯ ᠂ ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠢ᠋ ᠮᠡᠳᠡᠬᠦ ᠵᠡᠷᠭᠡ ᠴᠢᠬᠤᠯᠠ ᠶᠡᠬᠡ ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠠᠰᠢᠭ ᠲᠤᠰᠠ᠎ᠲᠠᠢ ᠰᠢᠭᠤᠳ ᠬᠣᠯᠪᠣᠭ᠎ᠠ᠎ᠲᠠᠢ ᠪᠠᠢᠬᠤ ︔
 3 )᠎ᠶᠠᠯᠠᠲᠤ ᠬᠡᠷᠡᠭ᠎ᠢ᠋ ᠲᠤᠷᠰᠢᠨ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠂ ᠵᠠᠭᠠᠯᠳᠤᠬᠤ ᠂ ᠰᠢᠭᠦᠨ ᠲᠠᠰᠤᠯᠬᠤ ᠪᠠ ᠰᠢᠭᠦᠨ ᠲᠠᠰᠤᠯᠤᠯᠲᠠ᠎ᠶ᠋ᠢ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠵᠡᠷᠭᠡ᠎ᠲᠡᠢ ᠰᠢᠭᠤᠳ ᠬᠣᠯᠪᠣᠭ᠎ᠠ᠎ᠲᠠᠢ ᠪᠠᠢᠬᠤ ︔
 4. ᠲᠠᠨ᠎ᠤ᠋ ᠠᠮᠢ ᠨᠠᠰᠤ ᠂ ᠡᠳ᠋ ᠬᠥᠷᠥᠩᠭᠡ ᠵᠡᠷᠭᠡ ᠴᠢᠬᠤᠯᠠ ᠶᠡᠬᠡ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠨ᠎ᠤ᠋ ᠡᠷᠬᠡ ᠠᠰᠢᠭ᠎ᠢ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠰᠠᠨ ᠪᠣᠯᠪᠠᠴᠤ ᠲᠠᠨ᠎ᠤ᠋ ᠲᠤᠰ ᠪᠡᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ᠎ᠢ᠋ ᠣᠯᠬᠤ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ︔
 5） ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠲᠠ ᠨᠡᠢᠭᠡᠮ ᠨᠡᠢᠲᠡ᠎ᠳ᠋ᠦ᠍ ᠥᠪᠡᠰᠦᠪᠡᠨ ᠢᠯᠡ᠎ᠪᠡᠷ ᠨᠡᠢᠲᠡᠯᠡᠵᠦ ᠪᠠᠢᠬᠤ ;
 6. ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠭᠠᠷ ᠢᠯᠡ ᠨᠡᠢᠲᠡᠯᠡᠭᠰᠡᠨ ᠰᠤᠷᠠᠭ ᠴᠢᠮᠡᠭᠡᠨ᠎ᠡᠴᠡ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠨ᠎ᠤ᠋ ᠰᠣᠨᠢᠨ ᠮᠡᠳᠡᠭᠡᠯᠡᠯ ᠂ ᠵᠠᠰᠠᠭ᠎ᠤ᠋ᠨ ᠣᠷᠳᠣᠨ᠎ᠤ᠋ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠢᠯᠡ ᠪᠣᠯᠭᠠᠬᠤ ᠵᠡᠷᠭᠡ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ︔
 7 )᠎ᠲᠠᠨ᠎ᠤ᠋ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠭᠡᠷ᠎ᠡ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠪᠠ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠵᠠᠪᠠᠯ ᠱᠠᠭᠠᠷᠳᠠᠭᠳᠠᠬᠤ ;
 8. ᠬᠠᠩᠭᠠᠭᠰᠠᠨ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ ᠪᠤᠶᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠲᠣᠭᠲᠠᠭᠤᠨ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ᠎ᠢ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠵᠠᠪᠠᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ ᠃ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠪᠤᠶᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢᠨ ᠭᠡᠮ ᠰᠣᠭᠣᠭ᠎ᠢ᠋ ᠮᠡᠳᠡᠵᠦ ᠂ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ;
 9. ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ᠎ᠶ᠋ᠢᠨ ᠠᠰᠢᠭ ᠲᠤᠰᠠ᠎ᠶ᠋ᠢᠨ ᠡᠭᠦᠳᠡᠨ᠎ᠡᠴᠡ ᠪᠦᠷᠢᠳᠬᠡᠯ ᠪᠤᠶᠤ ᠡᠷᠳᠡᠮ ᠰᠢᠨᠵᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠰᠤᠳᠤᠯᠤᠯ᠎ᠤ᠋ᠨ ᠴᠢᠬᠤᠯᠠ ᠬᠡᠷᠡᠭᠴᠡᠭᠡ᠎ᠲᠡᠢ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠡᠷᠳᠡᠮ ᠰᠢᠨᠵᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠰᠤᠳᠤᠯᠭᠠᠨ ᠪᠤᠶᠤ ᠳᠦᠷᠰᠦᠯᠡᠨ ᠲᠣᠭᠠᠴᠢᠭᠰᠠᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠬᠠᠩᠭᠠᠬᠤ᠎ᠳ᠋ᠠᠭᠠᠨ ᠂ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ᠎ᠳ᠋ᠦ᠍ ᠠᠭᠤᠯᠤᠭᠳᠠᠬᠤ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠳ᠋ᠤ᠌ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ ᠬᠢᠵᠦ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ︔
 10 ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠬᠠᠤᠯᠢ ᠲᠣᠭᠲᠠᠭᠠᠯ᠎ᠳ᠋ᠤ᠌ ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠪᠤᠰᠤᠳ ᠪᠠᠢᠳᠠᠯ ᠃
 ᠬᠣᠶᠠᠷ ᠂ ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠬᠢᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠠ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠪᠣᠢ
 1. ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠭᠠᠵᠠᠷ
 ᠪᠢᠳᠡ ᠬᠠᠤᠯᠢ ᠲᠣᠭᠲᠠᠭᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠢᠭᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠳᠠᠭᠠᠤ ᠂ ᠬᠢᠲᠠᠳ᠎ᠤ᠋ᠨ ᠨᠤᠲᠤᠭ ᠳᠡᠪᠢᠰᠬᠡᠷ᠎ᠲᠦ᠍ ᠴᠤᠭᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠣᠯᠤᠨ ᠪᠣᠢ ᠪᠣᠯᠤᠭᠰᠠᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡᠯᠡᠯ᠎ᠢ᠋ ᠬᠢᠲᠠᠳ᠎ᠤ᠋ᠨ ᠨᠤᠲᠤᠭ ᠳᠡᠪᠢᠰᠬᠡᠷ᠎ᠲᠦ᠍ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 2. ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ
 ᠡᠩ᠎ᠦ᠋ᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠳ᠋ᠤ᠌ ᠪᠢᠳᠡ ᠵᠥᠪᠬᠡᠨ ᠬᠠᠷᠠᠯᠲᠠ᠎ᠪᠠᠨ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠵᠠᠪᠠᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ ᠴᠠᠭ ᠪᠤᠶᠤ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠬᠠᠤᠯᠢ ᠳᠦᠷᠢᠮ᠎ᠳ᠋ᠦ᠍ ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠣᠬᠣᠷ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠦᠯᠡᠳᠡᠭᠡᠨ᠎ᠡ ᠃ ᠡᠳᠦᠷ᠎ᠦ᠋ᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ᠎ᠳ᠋ᠡᠬᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠠ ᠠᠦ᠋ᠲ᠋ᠣᠮᠠᠲ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠪᠠᠯᠠᠯᠵᠤ ᠬᠠᠶᠠᠳᠠᠭ ᠪᠠᠢᠨ᠎ᠠ ᠃
 ᠮᠠᠨ᠎ᠤ᠋ ᠦᠢᠯᠡᠳᠬᠦᠨ ᠪᠤᠶᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠳ᠋ᠦ᠍ ᠡᠵᠡᠩᠨᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠨᠢ ᠵᠣᠭᠰᠣᠭᠠᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠭᠠᠷᠬᠤ ᠦᠶ᠎ᠡ᠎ᠳ᠋ᠦ᠍ ᠂ ᠪᠢᠳᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠂ ᠠᠯᠪᠠᠨ ᠵᠠᠷ ᠵᠡᠷᠭᠡ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠪᠡᠷ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠮᠡᠳᠡᠭᠳᠡᠵᠦ ᠂ ᠵᠦᠢ ᠵᠣᠬᠢᠰᠲᠠᠢ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠠᠰᠤᠬᠤ ᠪᠤᠶᠤ ᠨᠡᠷ᠎ᠡ᠎ᠪᠡᠨ ᠨᠢᠭᠤᠵᠤ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠨ᠎ᠡ ᠂ ᠡᠭᠦᠨ᠎ᠦ᠌ ᠬᠠᠮᠲᠤ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠳᠠᠷᠤᠢ᠎ᠳ᠋ᠤᠨᠢ ᠵᠣᠭᠰᠣᠭᠠᠨ᠎ᠠ ᠃
 3. ᠡᠳᠡᠭᠡᠷ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢ ᠬᠡᠷᠬᠢᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠪᠣᠢ
 ᠪᠢᠳᠡ ᠴᠢᠷᠮᠠᠢᠵᠤ ᠪᠠᠢᠭᠠᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ᠎ᠦ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠳ᠋ᠦ᠍ ᠪᠠᠲᠤᠯᠠᠭ᠎ᠠ ᠬᠠᠩᠭᠠᠵᠤ ᠂ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠭᠡᠭᠡᠭᠳᠡᠬᠦ ᠂ ᠵᠣᠬᠢᠰ ᠥᠬᠡᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠂ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠭᠰᠠᠨ ᠥᠬᠡᠢ᠎ᠪᠡᠷ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠨᠡᠢᠲᠡᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠰᠡᠷᠭᠡᠢᠯᠡᠪᠡ ᠃
 ᠪᠢᠳᠡ ᠵᠣᠬᠢᠰᠲᠠᠢ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠢᠷᠢ ᠬᠡᠮᠵᠢᠶᠡᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠠᠪᠴᠤ ᠪᠠᠢᠭᠠᠳ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠢ᠋ ᠪᠠᠲᠤᠯᠠᠨ᠎ᠠ ᠃ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠂ ᠪᠢᠳᠡ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠵᠡᠷᠭᠡ ᠠᠷᠭ᠎ᠠ ᠪᠠᠷᠢᠯ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠨ᠎ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠳᠡᠰ᠎ᠦ᠋ᠨ ᠳᠠᠩᠰᠠ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ᠎ᠢ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃
 ᠪᠢᠳᠡ ᠲᠤᠰᠬᠠᠢ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠳᠦᠷᠢᠮ ᠂ ᠶᠠᠪᠤᠴᠠ ᠪᠣᠯᠤᠨ ᠵᠣᠬᠢᠶᠠᠨ ᠪᠠᠢᠭᠤᠯᠤᠯᠲᠠ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠂ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠬᠢᠶᠠᠨ᠎ᠤ᠋ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠢ᠋ ᠪᠠᠲᠤᠯᠠᠵᠤ ᠃ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠪᠢᠳᠡ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠲᠠᠨ᠎ᠤ᠋ ᠬᠡᠪᠴᠢᠶ᠎ᠡ᠎ᠶ᠋ᠢ ᠴᠢᠩᠭ᠎ᠠ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠵᠤ ᠂ ᠲᠡᠳᠡᠨ᠎ᠦ᠌ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠡᠭᠦᠷᠭᠡ᠎ᠶ᠋ᠢ ᠵᠢᠷᠤᠮᠯᠠᠨ ᠰᠠᠬᠢᠬᠤ᠎ᠶ᠋ᠢ ᠱᠠᠭᠠᠷᠳᠠᠨ᠎ᠠ ᠃
 4. ᠪᠠᠴᠢᠮ ᠪᠠᠢᠳᠠᠯ᠎ᠢ᠋ ᠠᠷᠭᠠᠴᠠᠬᠤ ᠤᠷᠢᠳᠴᠢᠯᠠᠭᠰᠠᠨ ᠲᠥᠰᠦᠯ
 ᠬᠡᠷᠪᠡ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠯᠡᠪᠡᠷᠡᠭᠰᠡᠨ ᠵᠡᠷᠭᠡ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠡᠷᠡᠭ ᠭᠠᠷᠪᠠᠯ ᠪᠢᠳᠡ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠭᠠᠷ ᠪᠠᠴᠢᠮ ᠪᠠᠢᠳᠠᠯ᠎ᠢ᠋ ᠠᠷᠭᠠᠴᠠᠬᠤ ᠲᠥᠰᠦᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠵᠦ ᠂ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠡᠷᠡᠭ᠎ᠦ᠋ᠨ ᠶᠡᠬᠡᠳᠬᠦ᠎ᠶ᠋ᠢ ᠬᠣᠷᠢᠭᠯᠠᠬᠤ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠲᠤ ᠂ ᠬᠦᠷᠭᠡᠬᠦ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠂ ᠠᠯᠪᠠᠨ ᠵᠠᠷ ᠵᠡᠷᠭᠡ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠪᠡᠷ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠡᠷᠡᠭ᠎ᠦ᠋ᠨ ᠪᠠᠢᠳᠠᠯ ᠂ ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ᠎ᠤ᠋ᠨ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠨᠥᠯᠥᠭᠡ ᠪᠣᠯᠬᠤ ᠵᠢᠴᠢ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠠᠪᠬᠤ ᠭᠡᠵᠦ ᠪᠠᠢᠭ᠎ᠠ ᠠᠪᠤᠷᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠎ᠶ᠋ᠢ ᠮᠡᠳᠡᠭᠳᠡᠨ᠎ᠡ ᠃ ᠪᠢᠳᠡ ᠪᠠᠰᠠ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠬᠠᠤᠯᠢ ᠳᠦᠷᠢᠮ ᠪᠠ ᠬᠢᠨᠠᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ᠎ᠶ᠋ᠢᠨ ᠰᠠᠯᠠᠭ᠎ᠠ ᠮᠥᠴᠢᠷ᠎ᠦ᠋ᠨ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ ᠶᠣᠰᠣᠭᠠᠷ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠦ᠌ ᠬᠡᠷᠡᠭ᠎ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠢ᠋ᠶ᠋ᠠᠨ ᠮᠡᠳᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃
 ᠭᠤᠷᠪᠠ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠪᠠᠨ ᠬᠡᠷᠬᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠪᠣᠢ
 ᠬᠡᠷᠪᠡ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ᠎ᠡᠴᠡ ᠪᠣᠯᠵᠣ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠵᠠᠳᠠᠷᠠᠬᠤ᠎ᠠ᠋ᠴᠠ ᠠᠶᠤᠵᠤ ᠪᠠᠢᠪᠠᠯ ᠂ ᠲᠠ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ ᠵᠢᠴᠢ ᠲᠤᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠠᠵᠢᠯ᠎ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠴᠡᠭᠡ᠎ᠪᠡᠨ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠨ᠍ᠭ᠍ᠭᠢ᠎ᠳ᠋ᠤ᠌ ᠬᠣᠯᠪᠣᠭᠳᠠᠬᠤ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠶ᠋ᠢ ᠲᠦᠷ ᠵᠣᠭᠰᠣᠭᠠᠬᠤ ᠪᠤᠶᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠭᠡᠢ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠵᠢᠩᠬᠢᠨᠢ᠎ᠪᠡᠷ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠬᠤ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠳᠡᠯᠭᠡᠭᠦᠷ ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠢ᠋ ᠰᠢᠨᠡᠳᠬᠡᠵᠦ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠦᠯᠬᠦ ᠂ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ᠎ᠤ᠋ᠨ ᠢᠯᠭᠠᠨ ᠲᠠᠨᠢᠬᠤ ᠵᠡᠷᠭᠡ ᠃
 ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠡᠭᠡᠷ᠎ᠡ ᠪᠠᠢᠭ᠎ᠠ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠶᠡᠰ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠠᠩᠬᠠᠷᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃
 ᠳᠥᠷᠪᠡ ᠂ ᠭᠤᠷᠪᠠᠳᠠᠬᠢ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢᠨ ᠲᠤᠬᠠᠢ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠯᠲᠠ
 ᠲᠤᠰ ᠵᠥᠪᠯᠡᠯᠴᠡᠬᠡᠷ᠎ᠲᠦ᠍ ᠵᠢᠭᠠᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠣᠯ ᠲᠩᠷᠢ᠎ᠶ᠋ᠢᠨ ᠣᠶᠣᠳᠠᠯ᠎ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ ᠪᠤᠶᠤ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠨᠡᠭᠡᠭᠡᠭᠰᠡᠨ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 ᠲᠠ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠡᠭᠡᠷ᠎ᠡ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠪᠤᠶᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠂ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢᠨ ᠬᠤᠪᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠪᠠ ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠥᠪᠡᠰᠦᠪᠡᠨ ᠬᠠᠷᠢᠭᠤᠴᠠᠨ᠎ᠠ ᠂ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠡᠲᠡᠭᠡᠳ᠎ᠦ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ / ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ᠎ᠶ᠋ᠢᠨ ᠬᠠᠷᠠᠭᠠᠯᠵᠠᠭᠰᠠᠨ ᠬᠤᠪᠢ᠎ᠶ᠋ᠢᠨ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢ ᠨᠠᠷᠢᠨ ᠤᠩᠰᠢᠵᠤ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠂ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠨᠢᠭᠤᠴᠠ᠎ᠶ᠋ᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ᠎ᠶ᠋ᠢ ᠠᠩᠬᠠᠷᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃
 ᠲᠠᠪᠤ ᠂ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠦᠯᠦ ᠬᠦᠷᠦᠭᠰᠡᠳ᠎ᠦ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠲᠤᠬᠠᠢ
 ᠬᠡᠷᠪᠡ ᠲᠠ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠦᠯᠦ ᠬᠦᠷᠦᠭᠰᠡᠳ ᠪᠣᠯ ᠂ ᠲᠠᠨ᠎ᠤ᠋ ᠬᠠᠷᠠᠭᠠᠯᠵᠠᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠴᠢ ᠴᠢᠨᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠲᠤᠰ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ᠎ᠢ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃ ᠪᠦᠲᠦᠭᠡᠭᠳᠡᠬᠦᠨ ᠬᠠᠩᠭᠠᠬᠤ᠎ᠳ᠋ᠤ᠌ ᠱᠠᠭᠠᠷᠳᠠᠭᠳᠠᠬᠤ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠠ᠋ᠴᠠ ᠭᠠᠳᠠᠨ᠎ᠠ ᠂ ᠪᠢᠳᠡ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠦᠯᠦ ᠬᠦᠷᠦᠭᠰᠡᠳ᠎ᠲᠦ᠍ ᠪᠤᠰᠤᠳ ᠨᠡᠩ ᠣᠯᠠᠨ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠬᠠᠩᠭᠠᠬᠤ᠎ᠶ᠋ᠢ ᠵᠣᠷᠢᠭᠤᠳᠠ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ ᠦᠭᠡᠢ ᠃ ᠬᠢᠨᠠᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠪᠤᠶᠤ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠦᠯᠦ ᠬᠦᠷᠦᠭᠰᠡᠳ᠎ᠦ᠋ᠨ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠠᠩᠰᠠ ᠨᠢ ᠳᠠᠷᠤᠢ ᠪᠤᠰᠤᠳ ᠶᠠᠮᠠᠷᠪᠠ ᠳᠠᠩᠰᠠᠨ ᠡᠷᠦᠬᠡ᠎ᠪᠡᠷ ᠲᠣᠭᠠᠴᠠᠭᠳᠠᠨ᠎ᠠ ᠃ ᠪᠢᠳᠡ ᠲᠤᠰᠬᠠᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳᠦᠷᠢᠮ ᠲᠣᠭᠲᠠᠭᠠᠵᠤ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠦᠯᠦ ᠬᠦᠷᠦᠭᠰᠡᠳ᠎ᠦ᠋ᠨ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ᠎ᠦ᠌ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠎ᠶ᠋ᠢᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ᠎ᠢ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠪᠠ ᠃ ᠬᠢᠨᠠᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠴᠢ ᠨᠢ ᠪᠠᠰᠠ ᠵᠣᠬᠢᠬᠤ ᠰᠡᠷᠭᠡᠢᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠠᠪᠴᠤ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠦᠯᠦ ᠬᠦᠷᠦᠭᠰᠡᠳ᠎ᠢ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠵᠤ ᠂ ᠲᠡᠳᠡᠨ᠎ᠦ᠌ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠬᠢᠨᠠᠯᠲᠠ ᠲᠠᠯᠪᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃
 ᠵᠢᠷᠭᠤᠭ᠎ᠠ ᠂ ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ ᠨᠢ ᠬᠡᠷᠬᠢᠨ ᠰᠢᠨᠡᠳᠬᠡᠬᠦ
 ᠪᠢᠳᠡ ᠪᠠᠷᠤᠭ ᠴᠠᠭ ᠢᠮᠠᠭᠲᠠ ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠪᠥᠭᠡᠳ ᠬᠤᠪᠢᠷᠠᠬᠤ ᠦᠶ᠎ᠡ᠎ᠳ᠋ᠦ᠍ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠶᠠᠪᠤᠴᠠ ᠪᠤᠶᠤ ᠺᠣᠮᠫᠠᠨᠢ᠎ᠶ᠋ᠢᠨ ᠨᠧᠲ ᠥᠷᠲᠡᠭᠡ᠎ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠲᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠦᠵᠡᠭᠦᠯᠬᠦ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃ ᠡᠳᠡᠭᠡᠷ ᠬᠤᠪᠢᠷᠠᠯᠲᠠ ᠨᠢ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠪᠣᠯᠤᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠤᠶᠤ ᠡᠳᠡᠭᠡᠷ ᠵᠦᠢᠯ ᠪᠠᠳᠠᠭ᠎ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠵᠦ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠣᠯᠭᠣᠭᠰᠠᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠂ ᠳᠠᠷᠤᠢ ᠲᠠᠨ᠎ᠤ᠋ ᠰᠢᠨ᠎ᠡ ᠵᠦᠢᠯ ᠪᠠᠳᠠᠭ᠎ᠢ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠭᠰᠡᠨ᠎ᠢ᠋ ᠢᠯᠡᠳᠬᠡᠨ᠎ᠡ ᠃ ᠬᠡᠷᠪᠡ ᠲᠠ ᠰᠢᠨ᠎ᠡ ᠵᠦᠢᠯ ᠪᠠᠳᠠᠭ᠎ᠢ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠦᠭᠡᠢ ᠪᠣᠯ ᠂ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠎ᠪᠡᠨ ᠵᠣᠭᠰᠣᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠂ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌᠎ᠪᠡᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠠᠩᠰᠠ᠎ᠪᠠᠨ ᠬᠠᠭᠠᠭᠠᠷᠠᠢ ; ᠬᠡᠷᠪᠡ ᠲᠠ ᠬᠢᠨᠠᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠴᠢ ᠪᠣᠯ ᠨᠠᠰᠤᠨ᠎ᠳ᠋ᠤ᠌ ᠬᠦᠷᠦᠭᠡᠳᠦᠢ ᠦᠷ᠎ᠡ ᠬᠡᠦᠬᠡᠳ᠎ᠢ᠋ᠶ᠋ᠡᠨ ᠬᠠᠪᠰᠤᠷᠴᠤ ᠪᠠᠢᠭᠠᠳ ᠲᠡᠭᠦᠨ᠎ᠦ᠌ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠡᠭᠡᠷ᠎ᠡ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠠᠩᠰᠠ᠎ᠶ᠋ᠢ ᠬᠠᠭᠠᠯᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃
 ᠳᠣᠯᠣᠭ᠎ᠠ ᠂ ᠬᠡᠷᠬᠢᠨ ᠪᠢᠳᠡᠨ᠎ᠲᠡᠢ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠪᠣᠢ
 ᠬᠡᠷᠪᠡ ᠲᠠ ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ᠎ᠳ᠋ᠦ᠍ ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠠᠰᠠᠭᠤᠯᠲᠠ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠬᠤ ᠂ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠶᠠᠮᠠᠷᠪᠠ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠵᠠᠭᠠᠯᠳᠤᠯᠭ᠎ᠠ ᠂ ᠰᠠᠨᠠᠯ᠎ᠢ᠋ᠶ᠋ᠠᠨ ᠪᠢᠳᠡᠨ᠎ᠦ᠌ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ᠎ᠦ᠌ ᠤᠲᠠᠰᠤ 400—089—1870 ᠂ ᠠᠯᠪᠠᠨ ᠲᠠᠯ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠰᠦᠯᠵᠢᠶ᠎ᠡ (᠎www . kylinos . cn )᠎ᠪᠤᠶᠤ ᠲᠤᠰ ᠦᠢᠯᠡᠳᠬᠦᠨ᠎ᠦ᠌ ᠳᠣᠲᠣᠷᠠᠬᠢ 《 ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠠ ᠳᠡᠮᠵᠢᠯᠭᠡ 》 ᠶ᠋ᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ᠎ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠲᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃ ᠲᠠ ᠪᠠᠰᠠ ᠢᠮᠧᠯ᠎ᠢ᠋ᠶ᠋ᠡᠷ mar ket@kylinos . cn ‍ᠢ ᠮᠠᠨ᠎ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃
 ᠪᠢᠳᠡ ᠲᠠᠨ᠎ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ᠎ᠢ᠋ ᠴᠠᠭ ᠲᠤᠬᠠᠢ᠎ᠳ᠋ᠤᠨᠢ ᠂ ᠲᠣᠬᠢᠲᠠᠢ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠨ᠎ᠡ ᠃ ᠡᠩ᠎ᠦ᠋ᠨ ᠪᠠᠢᠳᠠᠯ ᠳᠣᠣᠷ᠎ᠠ ᠂ ᠪᠢᠳᠡ 15 ᠠᠵᠢᠯᠯᠠᠬᠤ ᠡᠳᠦᠷ᠎ᠦ᠋ᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠬᠠᠷᠢᠭᠤᠯᠲᠠ ᠥᠭᠭᠦᠨ᠎ᠡ ᠃
 ᠲᠤᠰ ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯᠲᠡ ᠨᠢ ᠰᠢᠨᠡᠳᠬᠡᠭᠳᠡᠭᠰᠡᠨ ᠡᠳᠦᠷ᠎ᠡᠴᠡ ᠡᠬᠢᠯᠡᠨ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠡᠭᠦᠨ᠎ᠦ᠌ ᠬᠠᠮᠲᠤ ᠬᠢᠲᠠᠳ ᠠᠩᠭ᠌ᠯᠢ ᠬᠣᠶᠠᠷ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠬᠡᠪᠯᠡᠯ᠎ᠢ᠋ ᠬᠠᠩᠭᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠠᠯᠢ ᠨᠢᠭᠡ ᠵᠦᠢᠯ ᠨᠢ ᠬᠡᠷᠪᠡ ᠰᠠᠯᠪᠤᠷᠢ ᠤᠳᠬ᠎ᠠ᠎ᠲᠠᠢ ᠪᠣᠯ ᠬᠢᠲᠠᠳ ᠬᠡᠯᠡᠨ᠎ᠦ᠌ ᠬᠡᠪᠯᠡᠯ᠎ᠢ᠋ᠶ᠋ᠡᠷ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃
 ᠣᠷᠴᠢᠮ᠎ᠤ᠋ᠨ ᠰᠢᠨᠡᠳᠬᠡᠭᠰᠡᠨ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ : 2021 ᠣᠨ᠎ᠤ᠋ 11 ᠰᠠᠷ᠎ᠠ᠎ᠶ᠋ᠢᠨ 1᠎ᠦ᠌ ᠡᠳᠦᠷ
 ᠬᠠᠶᠢᠭ ᠄
 ᠲᠢᠶᠠᠨᠵᠢᠨ ᠬᠣᠲᠠ᠎ᠶ᠋ᠢᠨ ᠪᠢᠨ ᠬᠠᠢ ᠳᠡᠭᠡᠳᠦ ᠰᠢᠨ᠎ᠡ ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠣᠷᠣᠨ᠎ᠤ᠋ ᠳᠠᠩ ᠭᠦ ᠳᠠᠯᠠᠢ ᠲᠠᠩᠭᠢᠰ᠎ᠤ᠋ᠨ ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ ᠮᠡᠷᠭᠡᠵᠢᠯ᠎ᠦ᠋ᠨ ᠬᠦᠷᠢᠶᠡᠯᠡᠩ᠎ᠦ᠋ᠨ ᠰᠢᠨ ᠠᠨ ᠠᠳᠠᠯ ᠦᠢᠯᠡᠰ ᠪᠣᠰᠬᠠᠬᠤ ᠲᠠᠯᠠᠪᠠᠢ᠎ᠶ᠋ᠢᠨ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠠᠰᠠᠷ (᠎300450 )
 ᠪᠡᠭᠡᠵᠢᠩ ᠬᠣᠲᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢ ᠳ᠋ᠢᠶᠠᠨ ᠲᠣᠭᠣᠷᠢᠭ᠎ᠤ᠋ᠨ ᠬᠣᠢᠲᠤ ᠳᠥᠷᠪᠡᠳᠦᠭᠡᠷ ᠲᠣᠭᠣᠷᠢᠯ ᠵᠠᠮ᠎ᠤ᠋ᠨ ᠪᠠᠷᠠᠭᠤᠨ ᠵᠠᠮ᠎ᠤ᠋ᠨ 9 ᠨᠣᠮᠧᠷᠲᠦ ᠶᠢᠨ ᠭᠦ ᠲᠣᠮᠣ ᠠᠰᠠᠷ
 ᠴᠠᠩᠱᠠ ᠬᠣᠲᠠ᠎ᠶ᠋ᠢᠨ ᠺᠠᠢ ᠹᠦ ᠲᠣᠭᠣᠷᠢᠭ᠎ᠤ᠋ᠨ ᠹᠦ ᠷᠦᠩ ᠳᠤᠮᠳᠠ ᠵᠠᠮ᠎ᠤ᠋ᠨ 1 ᠬᠡᠰᠡᠭ 303 ᠳ᠋ᠤᠭᠠᠷ ᠨᠣᠮᠧᠷᠲᠦ ᠹᠦ ᠰᠢᠨ ᠳᠡᠯᠡᠬᠡᠢ᠎ᠶ᠋ᠢᠨ ᠮᠥᠩᠭᠥᠨ ᠭᠦᠢᠯᠭᠡᠭᠡᠨ᠎ᠦ᠌ ᠲᠥᠪ T3 ᠰᠠᠭᠤᠷᠢ (᠎41000000 )
 ᠱᠠᠩᠬᠠᠢ ᠬᠣᠲᠠ᠎ᠶ᠋ᠢᠨ ᠰᠢᠦᠢ ᠾᠦᠢ ᠲᠣᠭᠣᠷᠢᠭ᠎ᠤ᠋ᠨ ᠹᠠᠨ ᠢᠦᠢ ᠵᠠᠮ᠎ᠤ᠋ᠨ 1028 ᠳ᠋ᠤᠭᠠᠷ ᠲᠣᠭᠠᠲᠤ ᠰᠣᠶᠣᠯ ᠴᠡᠩᠭᠡᠯ᠎ᠦ᠋ᠨ ᠲᠣᠮᠣ ᠠᠰᠠᠷ (᠎20030 )
 ᠤᠲᠠᠰᠤ ᠄
 ᠲᠢᠶᠠᠨᠵᠢᠨ ︵ 022 ︶ 589565650 ᠪᠡᠭᠡᠵᠢᠩ ︵ 2010 ︶ 51659955
 ᠴᠠᠩᠱᠠ (᠎0731 )᠎88280170 ᠱᠠᠩᠬᠠᠢ (᠎021 )᠎5109866
 ᠳᠦᠷᠰᠦᠲᠦ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠄
 ᠲᠢᠶᠠᠨᠵᠢᠨ ︵ 022 ︶ 5895651 ᠪᠡᠭᠡᠵᠢᠩ ︵ 2010 ︶ 62800607
 ᠴᠠᠩᠱᠠ ︵ 0731 ︶ 88280166 ᠱᠠᠩᠬᠠᠢ ︵ 021 ︶ 51062866
 ᠺᠣᠮᠫᠠᠨᠢ᠎ᠶ᠋ᠢᠨ ᠨᠧᠲ ᠥᠷᠲᠡᠭᠡ ᠄ www . kylinos . cn
 ᠢᠮᠧᠯ ᠄ support@kylinos . cn</translation>
    </message>
    <message>
        <source>Dear users of Kylin operating system and relevant products,

   Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”). When you click the next step to confirm your complete understanding of the content, it shall indicate that you have accepted the clauses of the Agreement, and the Agreement shall take effect immediately and be legally binding on you and the Company.
   “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products. 

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021 

The Agreement shall include the following content:
I. 	User license 
II. 	Java technology limitations
III. 	Cookies and other technologies
IV. 	Intellectual property clause
V. 	Open source code
VI. 	The third-party software/services
VII. Escape clause
VIII. Integrity and severability of the Agreement
IX. 	Applicable law and dispute settlement

   I. 	User license

   According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
   1.	User license for educational institutions
   In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product. 
   2.	Use of the font software
   Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

   II. 	Java technology limitations 

   You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

   III. 	Cookies and other technologies

   In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
   If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers. 
   In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

   IV. 	Intellectual property clause

   1.	Trademarks and Logos
   This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
  2.	Duplication, modification and distribution
   If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
   Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws. 

   V. 	Open source code

   For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

   VI.  The third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
   We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

   VII. 	Escape clause

   1.	Limited warranty
    We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
   2.	Disclaimer
   In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
   3.	Limitation of responsibility
   To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

   VIII. 	Integrity and severability of the Agreement

   1.	The integrity of the Agreement
  The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
   2.	Severability of the Agreement
   If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

   IX. 	Applicable law and dispute settlement

   1.	Application of governing laws
   Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
   2.	Termination
   If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
   The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.

   The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

Privacy Policy Statement of Kylin Operating System
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021

We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.
   The Statement shall include the following content:
    I. 	Collection and use your personal information
   II. 	How to store and protect your personal information
   III. 	How to manage your personal information
   IV. 	Privacy of the third-party software/services
   V. 	Minors’ use of the products
   VI. 	How to update this Statement
   VII. How to contact us

    I. 	How to collect and use your personal information

   1.	The collection of personal information
   We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
   1)	The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
   2)	Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
   3)	Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
   4)	Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
   5)	The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
   6)	This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
   7)	The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
   8)	In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.

  2.	Use of personal information
   We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
   1)	The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
   2)	We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
   3)	We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
   4)	We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
   5)	We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
   6)	We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications and other channels;
   7)	In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.

   3.	Information sharing and provision
   We shall not share or transfer your personal information to any third party, except for the following circumstances:
   1)	After obtaining your clear consent, we shall share your personal information with the third parities;
   2)	In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
    3)	We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
   4)	With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
    5)	If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.

   4.	Exceptions with authorized consent
   1)	It is directly related to national security, national defense security and other national interests;
   2)	It is directly related to public safety, public health and public knowledge and other major public interests;
   3)	It is directly related to crime investigation, prosecution, judgment and execution of judgment;
   4)	It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent;
   5)	The personal information collected is disclosed to the public by yourself;
   6)	Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels;
   7)	It is necessary to sign and perform of the contract according to your requirement;
   8)	It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
   9)	It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
   10)	Other circumstances specified in the laws and regulations.

   II. 	How to store and protect personal information

   1.	Information storage place
   We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
   2.	Information storage duration
  Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
   When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
   3.	How to protect the information
   We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
   We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
   We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
   4.	Emergency response plan
    In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

   III. 	How to manage your personal information

   If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs.
   Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

   IV. 	Privacy of the third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
   When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

   V. 	Minors’ use of the products

   If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

   VI. 	How to update this Statement

   We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the account created by you in this product; if you are a  guardian, please help your minor child to close the account created by him/her in this product.

   VII. 	How to contact us

  If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn).
   We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
   The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
   Last date of updating: November 1, 2021

Address: Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
             Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
             Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
             Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.: Tianjin (022) 58955650       Beijing (010) 51659955
             Changsha (0731) 88280170    Shanghai (021) 51098866
Fax: Tianjin (022) 58955651       Beijing (010) 62800607
             Changsha (0731) 88280166    Shanghai (021) 51062866

Company website: www.kylinos.cn
E-mail: support@kylinos.cn</source>
        <translation type="vanished">尊敬的银河麒麟操作系统及相关产品用户:
   请您仔细阅读本协议条款、补充许可条款（统称“协议”）及银河麒麟操作系统隐私政策声明（以下简称“声明”）。当您确认了解并点击下一步时，即表明您已接受本协议的条款，本协议将立即生效，对您和本公司双方具有法律约束力。
   本协议及声明中的“本产品”是指由麒麟软件有限公司开发并制作发行的用于办公或构建企业及政府的信息化基础设施——“银河麒麟操作系统软件产品”。“我们”是指麒麟软件有限公司。“您”是指支付授权费用并使用银河麒麟操作系统及相关产品的用户。

银河麒麟最终用户使用许可协议
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

本协议将向您说明以下内容：
一、使用许可
二、Java技术限制
三、Cookie和其他技术
四、知识产权条款
五、开放源代码说明
六、第三方软件/服务说明
七、免责条款
八、协议完整性及可分割性说明
九、适用法律及争议解决
一、使用许可

    按照已经为本产品支付费用的用户数目及计算机硬件类型，我们向您授予非排他、不可转让的许可，仅允许被授权人单位及与其签订劳动合同的员工使用由麒麟软件提供的随附软件和文档以及任何错误纠正。
   1.教育机构使用许可
    在遵守本协议的条款和条件的情况下，如果您是教育机构，允许贵机构仅在内部使用随附的未经修改的二进制格式的软件。此处的“在内部使用”是指被授权人单位及与其签订劳动合同的员工以及在贵机构入学的学生使用本产品。 
   2.字型软件使用
   字型软件指本产品中预装的和生成字体样式的软件。您不可从软件中分离字型软件，不可改动字型软件，以新增此等字型软件被作为本产品的一部分交付予您时所不具备的任何功能，不可将字型软件嵌入作为商业产品提供以换取收费或其他报酬的文件、不可脱离安装了本产品的机器使用。如将字型软件用于对外宣传等其他商业用途时，请您与字体版权厂商联系协商以获得对您相关行为的许可。

二、Java技术限制

   您不可更改“Java平台界面”（简称“JPI”，即指明为“java”包或“java”包的任何子包中的类），无论通过在JPI中创建额外的类，还是通过其他方式导致对JPI中的类进行增添或更动，均为不可。如果您创建一个额外的类以及一个或多个相关的API，而它们（i）扩展Java平台的功能；并且（ii）可供第三方软件开发者用于开发可调用上述额外API的额外软件，则您必须迅即广泛公布对此种API的准确说明，以供所有开发者免费使用。您不可创建、或授权其他被许可人创建以任何方式标示为“java”、“javax”、“sun”的额外的类、界面、子包或Sun在任何命名约定中指明的类似约定。参见Java运行时环境二进制代码许可的适当版本（目前位于http://jdk.java.net），以了解可与Java小程序和应用程序共同分发的运行时代码的可供情况。

三、Cookie和其他技术

   为帮助我们更好地了解并服务用户，我们的网站、在线服务和应用程序可能会使用“Cookie”技术。这些Cookie用于存储进出系统的网络流量以及因检测错误而生成的流量，因此必须设置。 我们通过使用这些Cookie来了解您与我们的网站和在线服务如何进行交互。 
   如果您想禁用 Cookie 并且使用的是Firefox浏览器，可在Firefox的隐私与安全中心进行设置。如果您使用的是其他浏览器，请向相关供应商咨询具体方案。
依照《中华人民共和国网络安全法》第七十六条第五款，个人信息，是指以电子或者其他方式记录的能够单独或者与其他信息结合识别自然人个人身份的各种信息，包括但不限于自然人的姓名、出生日期、身份证件号码、个人生物识别信息、住址、电话号码等。如果Cookie中包含上述信息，或者存在通过Cookie收集的非个人信息与其他个人信息合并后的信息，出于本隐私政策的目的，我们会将合并后的信息视为个人隐私信息，将参照银河麒麟隐私政策声明，为您的个人信息提供相应的安全保护措施。

四、知识产权条款

   1.商标和标识
    本产品受到版权（著作权）法、商标法和其他法律及国际知识产权公约的保护。我们或其许可方保留对本产品的所有权及所有相关的知识产权。对于我们或其许可方的任何商标、服务标记、标识或商号的任何权利、所有权或利益，本协议均不作任何授权。您对麒麟软件标记的任何使用都应有利于麒麟软件，未经我们书面同意，不得擅自使用麒麟软件任何商标、标识。 
   2.关于复制、修改及分发
   如果在所有复制品中维持本协议书不变，您可以且必须根据《GNU GPL-GNU通用公共许可证》复制、修改及分发银河麒麟操作系统软件产品中遵守《GNU GPL-GNU通用公共许可证》协议的软件，其他不遵守《GNU GPL-GNU通用公共许可证》协议的银河麒麟操作系统软件产品必须根据相关法律、其他许可协议进行复制、修改及分发，但任何以银河麒麟操作系统软件产品为基础的衍生发行版未经我们的书面授权不能使用任何我们的商标或其他任何标志。
   特别注意：该复制、修改及分发不包括本产品中包含的任何不适用《GNU GPL-GNU通用公共许可证》的软件，如银河麒麟操作系统软件产品中包含的软件商店、输入法软件、字库软件、第三方应用软件等。除非适用法律予以禁止，否则您不得对上述软件进行复制、修改（包括反编译或反向工程）、分发。

五、开放源代码说明

   对于本产品中包含的任何开放源代码，本协议的任何条款均不得限制、约束或以其它方式影响任何适用开放源代码许可证赋予您的任何相应的权利或者义务或您应遵守的各种条件。

六、第三方软件/服务说明

   本协议所指的第三方软件/服务是指由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。本产品可能包含或捆绑有第三方软件/服务，这些第三方软件/服务附带单独的许可协议，您使用附带单独许可协议的任何第三方软件/服务需受到该单独许可协议的约束。
我们不对本产品中的第三方软件/服务拥有任何控制权，也不对其提供方或用户行为的合法性、准确性、有效性、安全性进行任何明示或默示的保证或担保。

七、免责条款

   1.有限担保
    我们向您担保，自购买或其他合法取得本产品之日起九十（90）天内（以销售合同日期为准），本产品的存储介质（如果有）在正常使用的情况下无材料和工艺方面的缺陷。在本有限担保项下，您可获得的所有补偿及我们的全部责任为由我们选择更换本产品介质或退还本产品的购买费用。
    2.免责声明
    除上述有限担保外，本软件按“原样”提供，不提供任何明示或默示的条件、陈述及担保，包括对适销性、对特定用途的适用性或非侵权性的任何默示的担保，均不予负责，但本免责声明被认定为法律上无效的情况除外。
    3.责任限制
    在法律允许范围内，无论在何种情况下，无论采用何种有关责任的理论，无论因何种方式导致，对于因使用或无法使用本软件引起的或与之相关的任何收益损失、利润或数据损失，或者对于特殊的、间接的、后果性的、偶发的或惩罚性的损害赔偿，我们或其许可方均不承担任何责任（即使我们已被告知可能出现上述损害赔偿）。根据本协议，在任何情况下，无论是在合同、侵权行为（包括过失）方面，还是在其他方面，我们对您的责任将不超过您就本软件所支付的金额。即使上述担保未能达到其基本目的，上述限制仍然适用。

八、协议完整性及可分割性说明

1.协议完整性
    本协议是我们就产品使用与您达成的完整协议。它取代此前或同期的所有和本协议不一致的口头或书面往来信息、建议、陈述和担保。在本协议期间，有关报价、订单、回执或各方之间就本协议内容进行的其他往来通信中的任何冲突条款或附加条款，均以本协议为准。对本协议的任何修改均无约束力，除非通过书面进行修改并由每一方的授权代表签字。
2.可分割性
如果本协议中有任何规定被认定为无法执行，则删除相应规定，本协议仍然有效，除非该删除会防碍各方根本目的的实现（在这种情况下，本协议将立即终止）。

九、适用法律及争议解决

1.管辖法律适用
    与本协议相关的任何争议解决（包括但不限于诉讼、仲裁等）均受适用中华人民共和国法律管辖。选择其它任何国家和地区的法律规则不予适用。
2.终止
如果本软件成为或在任一方看来可能成为任何知识产权侵权索赔之标的，则任一方可立即终止本协议。
    本协议在终止之前有效。您可以随时终止本协议，但必须同时销毁本软件的全部正本和副本。如果您未遵守本协议的任何规定，则本协议将不经我们发出通知立即终止。终止时，您必须销毁本软件的全部正本和副本，并且需承担因未遵守本协议而导致的法律责任。

本协议提供中英文两种版本，以上任何内容如有歧义，以中文版本为准。


银河麒麟操作系统隐私政策声明
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

   我们非常重视个人信息和隐私保护，为了保证合法、合理、适度的收集、存储、使用您的个人隐私信息，并在安全、可控的情况下进行传输、存储，我们制定了本声明。我们将会按照法律要求和业界成熟安全标准，为您的个人信息提供相应的安全保护措施。

本声明将向您说明以下内容：
一、关于收集和使用您的个人信息
二、如何存储和保护您的个人信息
三、如何管理您的个人信息
四、关于第三方软件/服务的隐私说明
五、关于未成年人使用产品
六、本声明如何更新
七、如何联系我们

一、如何收集和使用您的个人信息

    1.收集个人信息的情况
   我们在您使用本产品过程中收集相关的信息，主要为了向您提供更高质量、更易用的产品和更好的服务。收集的部分信息由您直接提供，其他信息则由我们通过您与产品的交互以及对产品的使用和体验收集而来。除非我们已根据适用的法律规定取得您的明示同意，我们不会主动收集并处理您的个人信息。
   1）本产品授权许可机制，会根据您所使用计算机的网卡、固件和主板等信息通过加密机制和转换方法生成申请产品正式授权许可的机器码；您将该机器码发送给麒麟软件商务人员后，可根据合同及相关协议申请正式许可。该机器码不直接包含您所使用计算机的网卡、固件和主板等设备的具体信息。
   2）本产品软件商店的服务器端，会根据您所使用计算机的CPU类型信息以及IP地址进行连接，同时我们会收集您使用本产品软件商店的相关信息，包括但不限于打开软件商店的时间、各页面之间的交互、搜索内容、下载的内容等，收集的相关信息一般记录在软件商店的服务端系统的日志中，具体存储位置可能因为不同的服务场景有所变动。
   3）本产品的升级更新，会根据您所使用计算机的IP地址进行连接，以便实现您升级更新系统；
   4）因业务往来及技术服务等向您收集电子邮箱、电话、姓名等个人信息。
   5）本产品的生物特征管理工具支持系统组件使用生物特征进行认证，包括指纹、指静脉、虹膜、声纹等。您录入的生物特征信息将储存在本地计算机，这部分信息我们仅接收验证结果，不会收集和上传。如您不需要使用生物特征进行系统认证，可以在生物特征管理工具中关闭该功能。
   6）本产品提供录音功能，您在使用本产品录音软件中，我们仅会将您使用录音时的音频内容存储在本地计算机中，不会进行收集和上传。
   7）本产品的服务与支持功能会收集由您提供给我们的日志、电子邮箱、电话、姓名等信息，便于提供技术服务，我们将妥善保管您的个人信息。
   8）本产品升级过程中，如需新增收集您的个人信息，我们将及时更新本部分内容。
   2.使用个人信息的情况
   我们严格遵守法律法规的规定及与您的约定，将收集的信息用于以下用途。若我们超出以下用途，我们将再次向您进行说明，并征得您的同意。
   1）涉及产品许可机制、软件商店使用、系统更新维护、生物识别、在线服务等需要；
   2）我们会利用相关信息协助提升产品的安全性、可靠性和可持续服务；
   3）我们会利用收集的信息（例如您提供的电子邮件地址、电话等）直接与您沟通。例如，业务联系、技术支持或服务回访；
   4）我们会利用收集的数据改进产品当前的易用性、提升产品用户体验（例如软件商店的个性化推荐）以及修复产品缺陷等；
   5）我们会将所收集到的用户行为数据用于大数据分析。例如，我们将收集到的信息用于分析形成不包含任何个人信息的城市热力图或行业洞察报告。我们可能对外公开并与我们的合作伙伴分享经统计加工后不含身份识别内容的信息，用于了解用户如何使用我们服务或让公众了解我们服务的总体使用趋势;

   6）我们可能使用您的相关信息，在相关网站、应用及其他渠道向您提供与您更加相关的广告;
   7）为了遵从相关法律法规、部门规章、政府指令的相关要求。
   3.信息的分享及对外提供
   我们不会共享或转让您的个人信息至第三方，但以下情况除外：
   1）获取您的明确同意后，我们会与第三方分享您的个人信息；
   2）为实现外部处理的目的，我们可能与关联公司或其他第三方合作伙伴（第三方服务供应商、承包商、代理、应用开发者等）分享您的个人信息。我们将采用加密、匿名化处理等手段来保障您的信息安全；
   3）我们不会对外公开披露所收集的个人信息，如必须公开披露时，我们会向您告知此次公开披露的目的、披露信息的类型及可能涉及的敏感信息，并征得您的同意；
   4）随着我们业务的持续发展，我们有可能进行合并、收购、资产转让等交易，我们将告知相关情形，按照法律法规及不低于本声明所要求的标准继续保护或要求新的控制者继续保护您的个人信息；
   5）如我们使用您的个人信息，超出了与收集时所声称的目的及具有直接或合理关联的范围，我们将在使用您的个人信息前，再次向您告知并征得您的同意。
   4.征得授权同意的例外情况
   1）与国家安全、国防安全等国家利益直接相关的；
   2）与公共安全、公共卫生、公众知情等重大公共利益直接相关的；
   3）与犯罪侦查、起诉、审判和判决执行等直接相关的；
   4）出于维护您或其他个人的生命、财产等重大合法权益但又无法得到您本人同意的；
   5）所收集的个人信息是您自行向社会公众公开的；
   6）从合法公开披露的信息中收集的个人信息，如合法的新闻报道、政府信息公开等渠道；
   7）根据您要求签订和履行合同所必需的；
   8）用于维护所提供的产品或服务的安全稳定运行所必需的。如发现、处置产品或服务的故障；
   9）出于公共利益开展统计或学术研究所必需，且其对外提供学术研究或描述的结果时，对结果中所包含的个人信息进行去标识化处理的；
   10）法律法规规定的其他情形。

二、我们如何存储和保护您的个人信息

   1.信息存储的地点
   我们会按照法律法规规定，将在中国境内收集和产生的个人信息存储于中国境内。
   2.信息存储的期限
   一般而言，我们仅为实现目的所必需的时间或法律法规规定最短期限保留您的个人信息。记录在日志中的信息会按配置在一定期限保存及自动删除。
   当我们的产品或服务发生停止运营的情形时，我们将以通知、公告等形式通知您，在合理的期限内删除您的个人信息或进行匿名化处理，并立即停止收集个人信息的活动。
   3.我们如何保护这些信息
   我们努力为用户的信息安全提供保障，以防止信息的丢失、不当使用、未经授权访问或披露。
   我们将在合理的安全水平内使用安全保护措施以保障信息的安全。例如，我们会使用加密技术等手段来保护您的系统级账户密码。
   我们建立专门的管理制度、流程和组织以保障信息的安全。例如，我们严格限制访问信息的人员范围，要求他们遵守保密义务。
   4.应急预案
   若发生个人信息泄露等安全事件，我们会依法启动应急预案，阻止安全事件扩大，并以推送通知、公告等形式告知您安全事件的情况、事件可能对您的影响以及我们将采取的补救措施。我们还将按照法律法规和监管部门要求，上报个人信息安全事件的处置情况。

三、如何管理您的个人信息

   如果担心因使用本产品导致个人信息的泄露，您可根据个人及业务需要考虑暂停或不使用涉及个人信息的相关功能，如产品正式授权许可、应用商店、系统更新升级、生物识别等。
在使用本产品之上的第三方软件/服务时，请注意个人隐私保护。

四、关于第三方软件/服务的隐私说明

   本协议所指的第三方软件/服务是由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。
您在本产品之上安装或使用第三方软件/服务时，第三方软件/服务的隐私保护和法律责任由第三方软件/服务自行负责，请您仔细阅读和审查第三方软件/服务对应的隐私声明或条款，注意个人隐私保护。

五、关于未成年人使用产品

   如果您是未成年人，则需要您的监护人同意您使用本产品并同意相关服务条款。除了提供产品所需要的信息外，我们不会刻意要求未成年人提供其他更多数据。在征得监护人同意或授权后，未成年人所创建的帐户即被视为等同于其他任何帐户。我们制定了专门的信息处理规则以保护使用本产品的未成年人的个人信息安全。监护人也应采取适当的预防措施保护未成年人，监督其对本产品的使用。

六、本声明如何更新

   我们可能会随时更新本声明，并且会在变更时通过产品安装过程或公司网站向您展示变更后的声明。在这些变更生效后使用服务或根据这些条款授予许可的任何软件，即表示您同意新的条款。如果您不同意新的条款，则必须停止使用本产品，请关闭您在本产品之上创建的帐户；如果您是监护人，请帮助您的未成年子女关闭他或她在本产品之上创建的帐户。

七、如何联系我们

   如您对本声明存在任何疑问，或任何相关的投诉、意见，可通过我们的客服热线400-089-1870、官方网站（www.kylinos.cn）或本产品中“服务与支持”应用进行咨询或反映。您也可以通过发送邮件至market@kylinos.cn与我们联系。
   我们会及时、妥善处理您的问题。一般情况下，我们将在15个工作日内给予答复。
   本声明自更新之日起生效，同时提供中英文两种版本，以上任何条款如有歧义，以中文版本为准。
   最近更新日期：2021年11月1日

地址：天津市滨海高新区塘沽海洋科技园信安创业广场3号楼（300450）
北京市海淀区北四环西路9号银谷大厦（100190）
长沙市开福区芙蓉中路1段303号富兴世界金融中心T3栋（410000）
上海市徐汇区番禺路1028号数娱大厦（200030）
电话：天津（022）58955650  北京（010）51659955  
长沙（0731）88280170 上海（021）51098866</translation>
    </message>
    <message>
        <source>Dear users of Kylin operating system and relevant products,

   Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”). When you click the next step to confirm your complete understanding of the content, it shall indicate that you have accepted the clauses of the Agreement, and the Agreement shall take effect immediately and be legally binding on you and the Company.
   “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products. 

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021 

The Agreement shall include the following content:
I. 	User license 
II. 	Java technology limitations
III. 	Cookies and other technologies
IV. 	Intellectual property clause
V. 	Open source code
VI. 	The third-party software/services
VII. Escape clause
VIII. Integrity and severability of the Agreement
IX. 	Applicable law and dispute settlement

   I. 	User license

   According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
   1.	User license for educational institutions
   In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product. 
   2.	Use of the font software
   Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

   II. 	Java technology limitations 

   You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

   III. 	Cookies and other technologies

   In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
   If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers. 
   In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

   IV. 	Intellectual property clause

   1.	Trademarks and Logos
   This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
  2.	Duplication, modification and distribution
   If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
   Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws. 

   V. 	Open source code

   For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

   VI.  The third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
   We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

   VII. 	Escape clause

   1.	Limited warranty
    We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
   2.	Disclaimer
   In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
   3.	Limitation of responsibility
   To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

   VIII. 	Integrity and severability of the Agreement

   1.	The integrity of the Agreement
  The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
   2.	Severability of the Agreement
   If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

   IX. 	Applicable law and dispute settlement

   1.	Application of governing laws
   Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
   2.	Termination
   If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
   The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.

   The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

Privacy Policy Statement of Kylin Operating System
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021

We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.
   The Statement shall include the following content:
    I. 	Collection and use your personal information
   II. 	How to store and protect your personal information
   III. 	How to manage your personal information
   IV. 	Privacy of the third-party software/services
   V. 	Minors’ use of the products
   VI. 	How to update this Statement
   VII. How to contact us

    I. 	How to collect and use your personal information

   1.	The collection of personal information
   We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
   1)	The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
   2)	Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
   3)	Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
   4)	Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
   5)	The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
   6)	This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
   7)	The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
   8)	In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.

  2.	Use of personal information
   We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
   1)	The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
   2)	We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
   3)	We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
   4)	We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
   5)	We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
   6)	We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications and other channels;
   7)	In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.

   3.	Information sharing and provision
   We shall not share or transfer your personal information to any third party, except for the following circumstances:
   1)	After obtaining your clear consent, we shall share your personal information with the third parities;
   2)	In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
    3)	We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
   4)	With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
    5)	If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.

   4.	Exceptions with authorized consent
   1)	It is directly related to national security, national defense security and other national interests;
   2)	It is directly related to public safety, public health and public knowledge and other major public interests;
   3)	It is directly related to crime investigation, prosecution, judgment and execution of judgment;
   4)	It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent;
   5)	The personal information collected is disclosed to the public by yourself;
   6)	Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels;
   7)	It is necessary to sign and perform of the contract according to your requirement;
   8)	It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
   9)	It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
   10)	Other circumstances specified in the laws and regulations.

   II. 	How to store and protect personal information

   1.	Information storage place
   We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
   2.	Information storage duration
  Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
   When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
   3.	How to protect the information
   We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
   We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
   We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
   4.	Emergency response plan
    In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

   III. 	How to manage your personal information

   If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs.
   Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

   IV. 	Privacy of the third-party software/services

   The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
   When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

   V. 	Minors’ use of the products

   If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

   VI. 	How to update this Statement

   We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the account created by you in this product; if you are a parent or guardian, please help your minor child to close the account created by him/her in this product.

   VII. 	How to contact us

  If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn).
   We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
   The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
   Last date of updating: November 1, 2021

Address: Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
             Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
             Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
             Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.: Tianjin (022) 58955650       Beijing (010) 51659955
             Changsha (0731) 88280170    Shanghai (021) 51098866
Fax: Tianjin (022) 58955651       Beijing (010) 62800607
             Changsha (0731) 88280166    Shanghai (021) 51062866

Company website: www.kylinos.cn
E-mail: support@kylinos.cn</source>
        <translation type="vanished">尊敬的银河麒麟操作系统及相关产品用户:
   请您仔细阅读本协议条款、补充许可条款（统称“协议”）及银河麒麟操作系统隐私政策声明（以下简称“声明”）。当您确认了解并点击下一步时，即表明您已接受本协议的条款，本协议将立即生效，对您和本公司双方具有法律约束力。
   本协议及声明中的“本产品”是指由麒麟软件有限公司开发并制作发行的用于办公或构建企业及政府的信息化基础设施——“银河麒麟操作系统软件产品”。“我们”是指麒麟软件有限公司。“您”是指支付授权费用并使用银河麒麟操作系统及相关产品的用户。

银河麒麟最终用户使用许可协议
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

本协议将向您说明以下内容：
一、使用许可
二、Java技术限制
三、Cookie和其他技术
四、知识产权条款
五、开放源代码说明
六、第三方软件/服务说明
七、免责条款
八、协议完整性及可分割性说明
九、适用法律及争议解决
一、使用许可

    按照已经为本产品支付费用的用户数目及计算机硬件类型，我们向您授予非排他、不可转让的许可，仅允许被授权人单位及与其签订劳动合同的员工使用由麒麟软件提供的随附软件和文档以及任何错误纠正。
   1.教育机构使用许可
    在遵守本协议的条款和条件的情况下，如果您是教育机构，允许贵机构仅在内部使用随附的未经修改的二进制格式的软件。此处的“在内部使用”是指被授权人单位及与其签订劳动合同的员工以及在贵机构入学的学生使用本产品。 
   2.字型软件使用
   字型软件指本产品中预装的和生成字体样式的软件。您不可从软件中分离字型软件，不可改动字型软件，以新增此等字型软件被作为本产品的一部分交付予您时所不具备的任何功能，不可将字型软件嵌入作为商业产品提供以换取收费或其他报酬的文件、不可脱离安装了本产品的机器使用。如将字型软件用于对外宣传等其他商业用途时，请您与字体版权厂商联系协商以获得对您相关行为的许可。

二、Java技术限制

   您不可更改“Java平台界面”（简称“JPI”，即指明为“java”包或“java”包的任何子包中的类），无论通过在JPI中创建额外的类，还是通过其他方式导致对JPI中的类进行增添或更动，均为不可。如果您创建一个额外的类以及一个或多个相关的API，而它们（i）扩展Java平台的功能；并且（ii）可供第三方软件开发者用于开发可调用上述额外API的额外软件，则您必须迅即广泛公布对此种API的准确说明，以供所有开发者免费使用。您不可创建、或授权其他被许可人创建以任何方式标示为“java”、“javax”、“sun”的额外的类、界面、子包或Sun在任何命名约定中指明的类似约定。参见Java运行时环境二进制代码许可的适当版本（目前位于http://jdk.java.net），以了解可与Java小程序和应用程序共同分发的运行时代码的可供情况。

三、Cookie和其他技术

   为帮助我们更好地了解并服务用户，我们的网站、在线服务和应用程序可能会使用“Cookie”技术。这些Cookie用于存储进出系统的网络流量以及因检测错误而生成的流量，因此必须设置。 我们通过使用这些Cookie来了解您与我们的网站和在线服务如何进行交互。 
   如果您想禁用 Cookie 并且使用的是Firefox浏览器，可在Firefox的隐私与安全中心进行设置。如果您使用的是其他浏览器，请向相关供应商咨询具体方案。
依照《中华人民共和国网络安全法》第七十六条第五款，个人信息，是指以电子或者其他方式记录的能够单独或者与其他信息结合识别自然人个人身份的各种信息，包括但不限于自然人的姓名、出生日期、身份证件号码、个人生物识别信息、住址、电话号码等。如果Cookie中包含上述信息，或者存在通过Cookie收集的非个人信息与其他个人信息合并后的信息，出于本隐私政策的目的，我们会将合并后的信息视为个人隐私信息，将参照银河麒麟隐私政策声明，为您的个人信息提供相应的安全保护措施。

四、知识产权条款

   1.商标和标识
    本产品受到版权（著作权）法、商标法和其他法律及国际知识产权公约的保护。我们或其许可方保留对本产品的所有权及所有相关的知识产权。对于我们或其许可方的任何商标、服务标记、标识或商号的任何权利、所有权或利益，本协议均不作任何授权。您对麒麟软件标记的任何使用都应有利于麒麟软件，未经我们书面同意，不得擅自使用麒麟软件任何商标、标识。 
   2.关于复制、修改及分发
   如果在所有复制品中维持本协议书不变，您可以且必须根据《GNU GPL-GNU通用公共许可证》复制、修改及分发银河麒麟操作系统软件产品中遵守《GNU GPL-GNU通用公共许可证》协议的软件，其他不遵守《GNU GPL-GNU通用公共许可证》协议的银河麒麟操作系统软件产品必须根据相关法律、其他许可协议进行复制、修改及分发，但任何以银河麒麟操作系统软件产品为基础的衍生发行版未经我们的书面授权不能使用任何我们的商标或其他任何标志。
   特别注意：该复制、修改及分发不包括本产品中包含的任何不适用《GNU GPL-GNU通用公共许可证》的软件，如银河麒麟操作系统软件产品中包含的软件商店、输入法软件、字库软件、第三方应用软件等。除非适用法律予以禁止，否则您不得对上述软件进行复制、修改（包括反编译或反向工程）、分发。

五、开放源代码说明

   对于本产品中包含的任何开放源代码，本协议的任何条款均不得限制、约束或以其它方式影响任何适用开放源代码许可证赋予您的任何相应的权利或者义务或您应遵守的各种条件。

六、第三方软件/服务说明

   本协议所指的第三方软件/服务是指由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。本产品可能包含或捆绑有第三方软件/服务，这些第三方软件/服务附带单独的许可协议，您使用附带单独许可协议的任何第三方软件/服务需受到该单独许可协议的约束。
我们不对本产品中的第三方软件/服务拥有任何控制权，也不对其提供方或用户行为的合法性、准确性、有效性、安全性进行任何明示或默示的保证或担保。

七、免责条款

   1.有限担保
    我们向您担保，自购买或其他合法取得本产品之日起九十（90）天内（以销售合同日期为准），本产品的存储介质（如果有）在正常使用的情况下无材料和工艺方面的缺陷。在本有限担保项下，您可获得的所有补偿及我们的全部责任为由我们选择更换本产品介质或退还本产品的购买费用。
    2.免责声明
    除上述有限担保外，本软件按“原样”提供，不提供任何明示或默示的条件、陈述及担保，包括对适销性、对特定用途的适用性或非侵权性的任何默示的担保，均不予负责，但本免责声明被认定为法律上无效的情况除外。
    3.责任限制
    在法律允许范围内，无论在何种情况下，无论采用何种有关责任的理论，无论因何种方式导致，对于因使用或无法使用本软件引起的或与之相关的任何收益损失、利润或数据损失，或者对于特殊的、间接的、后果性的、偶发的或惩罚性的损害赔偿，我们或其许可方均不承担任何责任（即使我们已被告知可能出现上述损害赔偿）。根据本协议，在任何情况下，无论是在合同、侵权行为（包括过失）方面，还是在其他方面，我们对您的责任将不超过您就本软件所支付的金额。即使上述担保未能达到其基本目的，上述限制仍然适用。

八、协议完整性及可分割性说明

1.协议完整性
    本协议是我们就产品使用与您达成的完整协议。它取代此前或同期的所有和本协议不一致的口头或书面往来信息、建议、陈述和担保。在本协议期间，有关报价、订单、回执或各方之间就本协议内容进行的其他往来通信中的任何冲突条款或附加条款，均以本协议为准。对本协议的任何修改均无约束力，除非通过书面进行修改并由每一方的授权代表签字。
2.可分割性
如果本协议中有任何规定被认定为无法执行，则删除相应规定，本协议仍然有效，除非该删除会防碍各方根本目的的实现（在这种情况下，本协议将立即终止）。

九、适用法律及争议解决

1.管辖法律适用
    与本协议相关的任何争议解决（包括但不限于诉讼、仲裁等）均受适用中华人民共和国法律管辖。选择其它任何国家和地区的法律规则不予适用。
2.终止
如果本软件成为或在任一方看来可能成为任何知识产权侵权索赔之标的，则任一方可立即终止本协议。
    本协议在终止之前有效。您可以随时终止本协议，但必须同时销毁本软件的全部正本和副本。如果您未遵守本协议的任何规定，则本协议将不经我们发出通知立即终止。终止时，您必须销毁本软件的全部正本和副本，并且需承担因未遵守本协议而导致的法律责任。

本协议提供中英文两种版本，以上任何内容如有歧义，以中文版本为准。


银河麒麟操作系统隐私政策声明
版本发布日期：【2021】年【7】月【30】日
版本生效日期：【2021】年【7】月【30】日

   我们非常重视个人信息和隐私保护，为了保证合法、合理、适度的收集、存储、使用您的个人隐私信息，并在安全、可控的情况下进行传输、存储，我们制定了本声明。我们将会按照法律要求和业界成熟安全标准，为您的个人信息提供相应的安全保护措施。

本声明将向您说明以下内容：
一、关于收集和使用您的个人信息
二、如何存储和保护您的个人信息
三、如何管理您的个人信息
四、关于第三方软件/服务的隐私说明
五、关于未成年人使用产品
六、本声明如何更新
七、如何联系我们

一、如何收集和使用您的个人信息

    1.收集个人信息的情况
   我们在您使用本产品过程中收集相关的信息，主要为了向您提供更高质量、更易用的产品和更好的服务。收集的部分信息由您直接提供，其他信息则由我们通过您与产品的交互以及对产品的使用和体验收集而来。除非我们已根据适用的法律规定取得您的明示同意，我们不会主动收集并处理您的个人信息。
   1）本产品授权许可机制，会根据您所使用计算机的网卡、固件和主板等信息通过加密机制和转换方法生成申请产品正式授权许可的机器码；您将该机器码发送给麒麟软件商务人员后，可根据合同及相关协议申请正式许可。该机器码不直接包含您所使用计算机的网卡、固件和主板等设备的具体信息。
   2）本产品软件商店的服务器端，会根据您所使用计算机的CPU类型信息以及IP地址进行连接，同时我们会收集您使用本产品软件商店的相关信息，包括但不限于打开软件商店的时间、各页面之间的交互、搜索内容、下载的内容等，收集的相关信息一般记录在软件商店的服务端系统的日志中，具体存储位置可能因为不同的服务场景有所变动。
   3）本产品的升级更新，会根据您所使用计算机的IP地址进行连接，以便实现您升级更新系统；
   4）因业务往来及技术服务等向您收集电子邮箱、电话、姓名等个人信息。
   5）本产品的生物特征管理工具支持系统组件使用生物特征进行认证，包括指纹、指静脉、虹膜、声纹等。您录入的生物特征信息将储存在本地计算机，这部分信息我们仅接收验证结果，不会收集和上传。如您不需要使用生物特征进行系统认证，可以在生物特征管理工具中关闭该功能。
   6）本产品提供录音功能，您在使用本产品录音软件中，我们仅会将您使用录音时的音频内容存储在本地计算机中，不会进行收集和上传。
   7）本产品的服务与支持功能会收集由您提供给我们的日志、电子邮箱、电话、姓名等信息，便于提供技术服务，我们将妥善保管您的个人信息。
   8）本产品升级过程中，如需新增收集您的个人信息，我们将及时更新本部分内容。
   2.使用个人信息的情况
   我们严格遵守法律法规的规定及与您的约定，将收集的信息用于以下用途。若我们超出以下用途，我们将再次向您进行说明，并征得您的同意。
   1）涉及产品许可机制、软件商店使用、系统更新维护、生物识别、在线服务等需要；
   2）我们会利用相关信息协助提升产品的安全性、可靠性和可持续服务；
   3）我们会利用收集的信息（例如您提供的电子邮件地址、电话等）直接与您沟通。例如，业务联系、技术支持或服务回访；
   4）我们会利用收集的数据改进产品当前的易用性、提升产品用户体验（例如软件商店的个性化推荐）以及修复产品缺陷等；
   5）我们会将所收集到的用户行为数据用于大数据分析。例如，我们将收集到的信息用于分析形成不包含任何个人信息的城市热力图或行业洞察报告。我们可能对外公开并与我们的合作伙伴分享经统计加工后不含身份识别内容的信息，用于了解用户如何使用我们服务或让公众了解我们服务的总体使用趋势;

   6）我们可能使用您的相关信息，在相关网站、应用及其他渠道向您提供与您更加相关的广告;
   7）为了遵从相关法律法规、部门规章、政府指令的相关要求。
   3.信息的分享及对外提供
   我们不会共享或转让您的个人信息至第三方，但以下情况除外：
   1）获取您的明确同意后，我们会与第三方分享您的个人信息；
   2）为实现外部处理的目的，我们可能与关联公司或其他第三方合作伙伴（第三方服务供应商、承包商、代理、应用开发者等）分享您的个人信息。我们将采用加密、匿名化处理等手段来保障您的信息安全；
   3）我们不会对外公开披露所收集的个人信息，如必须公开披露时，我们会向您告知此次公开披露的目的、披露信息的类型及可能涉及的敏感信息，并征得您的同意；
   4）随着我们业务的持续发展，我们有可能进行合并、收购、资产转让等交易，我们将告知相关情形，按照法律法规及不低于本声明所要求的标准继续保护或要求新的控制者继续保护您的个人信息；
   5）如我们使用您的个人信息，超出了与收集时所声称的目的及具有直接或合理关联的范围，我们将在使用您的个人信息前，再次向您告知并征得您的同意。
   4.征得授权同意的例外情况
   1）与国家安全、国防安全等国家利益直接相关的；
   2）与公共安全、公共卫生、公众知情等重大公共利益直接相关的；
   3）与犯罪侦查、起诉、审判和判决执行等直接相关的；
   4）出于维护您或其他个人的生命、财产等重大合法权益但又无法得到您本人同意的；
   5）所收集的个人信息是您自行向社会公众公开的；
   6）从合法公开披露的信息中收集的个人信息，如合法的新闻报道、政府信息公开等渠道；
   7）根据您要求签订和履行合同所必需的；
   8）用于维护所提供的产品或服务的安全稳定运行所必需的。如发现、处置产品或服务的故障；
   9）出于公共利益开展统计或学术研究所必需，且其对外提供学术研究或描述的结果时，对结果中所包含的个人信息进行去标识化处理的；
   10）法律法规规定的其他情形。

二、我们如何存储和保护您的个人信息

   1.信息存储的地点
   我们会按照法律法规规定，将在中国境内收集和产生的个人信息存储于中国境内。
   2.信息存储的期限
   一般而言，我们仅为实现目的所必需的时间或法律法规规定最短期限保留您的个人信息。记录在日志中的信息会按配置在一定期限保存及自动删除。
   当我们的产品或服务发生停止运营的情形时，我们将以通知、公告等形式通知您，在合理的期限内删除您的个人信息或进行匿名化处理，并立即停止收集个人信息的活动。
   3.我们如何保护这些信息
   我们努力为用户的信息安全提供保障，以防止信息的丢失、不当使用、未经授权访问或披露。
   我们将在合理的安全水平内使用安全保护措施以保障信息的安全。例如，我们会使用加密技术等手段来保护您的系统级账户密码。
   我们建立专门的管理制度、流程和组织以保障信息的安全。例如，我们严格限制访问信息的人员范围，要求他们遵守保密义务。
   4.应急预案
   若发生个人信息泄露等安全事件，我们会依法启动应急预案，阻止安全事件扩大，并以推送通知、公告等形式告知您安全事件的情况、事件可能对您的影响以及我们将采取的补救措施。我们还将按照法律法规和监管部门要求，上报个人信息安全事件的处置情况。

三、如何管理您的个人信息

   如果担心因使用本产品导致个人信息的泄露，您可根据个人及业务需要考虑暂停或不使用涉及个人信息的相关功能，如产品正式授权许可、应用商店、系统更新升级、生物识别等。
在使用本产品之上的第三方软件/服务时，请注意个人隐私保护。

四、关于第三方软件/服务的隐私说明

   本协议所指的第三方软件/服务是由非银河麒麟操作系统生产商的其他组织或个人开发的相关软件/服务。
您在本产品之上安装或使用第三方软件/服务时，第三方软件/服务的隐私保护和法律责任由第三方软件/服务自行负责，请您仔细阅读和审查第三方软件/服务对应的隐私声明或条款，注意个人隐私保护。

五、关于未成年人使用产品

   如果您是未成年人，则需要您的监护人同意您使用本产品并同意相关服务条款。除了提供产品所需要的信息外，我们不会刻意要求未成年人提供其他更多数据。在征得监护人同意或授权后，未成年人所创建的帐户即被视为等同于其他任何帐户。我们制定了专门的信息处理规则以保护使用本产品的未成年人的个人信息安全。监护人也应采取适当的预防措施保护未成年人，监督其对本产品的使用。

六、本声明如何更新

   我们可能会随时更新本声明，并且会在变更时通过产品安装过程或公司网站向您展示变更后的声明。在这些变更生效后使用服务或根据这些条款授予许可的任何软件，即表示您同意新的条款。如果您不同意新的条款，则必须停止使用本产品，请关闭您在本产品之上创建的帐户；如果您是家长或监护人，请帮助您的未成年子女关闭他或她在本产品之上创建的帐户。

七、如何联系我们

   如您对本声明存在任何疑问，或任何相关的投诉、意见，可通过我们的客服热线400-089-1870、官方网站（www.kylinos.cn）或本产品中“服务与支持”应用进行咨询或反映。您也可以通过发送邮件至market@kylinos.cn与我们联系。
   我们会及时、妥善处理您的问题。一般情况下，我们将在15个工作日内给予答复。
   本声明自更新之日起生效，同时提供中英文两种版本，以上任何条款如有歧义，以中文版本为准。
   最近更新日期：2021年11月1日

地址：天津市滨海高新区塘沽海洋科技园信安创业广场3号楼（300450）
北京市海淀区北四环西路9号银谷大厦（100190）
长沙市开福区芙蓉中路1段303号富兴世界金融中心T3栋（410000）
上海市徐汇区番禺路1028号数娱大厦（200030）
电话：天津（022）58955650  北京（010）51659955  
长沙（0731）88280170 上海（021）51098866</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="298"/>
        <source>Kylinsoft Co., Ltd.</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠳᠤ ᠺᠣᠮᠫᠠᠨᠢ.</translation>
    </message>
</context>
<context>
    <name>PwdDialog</name>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="35"/>
        <source>VNC password</source>
        <translation>VNC ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="52"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="67"/>
        <source>Must be 1-8 characters long</source>
        <translation>ᠤᠷᠳᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1-8 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="84"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/pwddialog.cpp" line="88"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Unknown</source>
        <translation type="vanished">ᠮᠡᠳᠡᠬᠦ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="187"/>
        <source>Customize Shortcut</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="694"/>
        <source>Edit Shortcut</source>
        <translation>ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="71"/>
        <source>User Info</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="vanished">ᠶᠡᠷᠦ ᠡᠴᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>10min</source>
        <translation type="vanished">10min</translation>
    </message>
    <message>
        <source>20min</source>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <source>40min</source>
        <translation type="vanished">40min</translation>
    </message>
    <message>
        <source>80min</source>
        <translation type="vanished">80min</translation>
    </message>
    <message>
        <source>Year</source>
        <translation type="vanished">ᠵᠢᠯ ᠃</translation>
    </message>
    <message>
        <source>Jan</source>
        <translation type="vanished">ᠨᠢᠭᠡ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Feb</source>
        <translation type="vanished">ᠬᠣᠶᠠᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Mar</source>
        <translation type="vanished">ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Apr</source>
        <translation type="vanished">ᠳᠥᠷᠪᠡᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>May</source>
        <translation>ᠳᠠᠪᠤᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>January</source>
        <translation>ᠨᠢᠭᠡ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>February</source>
        <translation>ᠬᠣᠶᠠᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>March</source>
        <translation>ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>April</source>
        <translation>ᠳᠥᠷᠪᠡᠳᠦᠭᠡᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="31"/>
        <source>June</source>
        <translation>ᠵᠢᠷᠭᠤᠭᠠᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>July</source>
        <translation>ᠳᠣᠯᠣᠭᠠᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>August</source>
        <translation>ᠨᠠᠢ᠍ᠮᠠᠳᠤᠭᠠᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>September</source>
        <translation>ᠶᠢᠰᠦᠳᠦᠭᠡᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>October</source>
        <translation>ᠠᠷᠪᠠᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>Novermber</source>
        <translation>ᠠᠷᠪᠠᠨ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="32"/>
        <source>December</source>
        <translation>ᠠᠷᠪᠠᠨ ᠬᠣᠶᠠᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Jun</source>
        <translation type="vanished">ᠵᠢᠷᠭᠤᠭᠠᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Jul</source>
        <translation type="vanished">ᠳᠣᠯᠣᠭᠠᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Aug</source>
        <translation type="vanished">ᠨᠠᠢᠮᠠᠳᠤᠭᠠᠷ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Sep</source>
        <translation type="vanished">ᠶᠢᠰᠦᠳᠦᠭᠡᠷ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Oct</source>
        <translation type="vanished">ᠠᠷᠪᠠᠨ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Nov</source>
        <translation type="vanished">ᠠᠷᠪᠠᠨ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Dec</source>
        <translation type="vanished">ᠠᠷᠪᠠᠨ ᠬᠣᠶᠠᠳᠤᠭᠠᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <source>Day</source>
        <translation type="vanished">ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="96"/>
        <source>ukui-control-center is disabled！</source>
        <translation>ukui ᠡᠵᠡᠮᠳᠡᠬᠦ ᠳᠦᠪ ᠨᠢᠭᠡᠨᠳᠡ ᠴᠠᠭᠠᠵᠠᠯᠠᠭᠳᠠᠪᠠ！</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="130"/>
        <source>ukui-control-center</source>
        <translation>ukui ᠡᠵᠡᠮᠳᠡᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="85"/>
        <source>ukui-control-center is already running!</source>
        <translation>ukui ᠡᠵᠡᠮᠳᠡᠯ ᠬᠠᠪᠳᠠᠰᠤ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1335"/>
        <source>min length %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠤᠷᠳᠤ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1345"/>
        <source>min digit num %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠲᠣᠭ᠎ᠠ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1354"/>
        <source>min upper num %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠳᠡᠭᠡᠳᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠤ᠋ᠨ ᠲᠣᠭᠠᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1363"/>
        <source>min lower num %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠳᠤᠤᠷᠠᠳᠤ ᠬᠢᠵᠠᠭᠠᠷ ᠤ᠋ᠨ ᠲᠣᠭᠠᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1372"/>
        <source>min other num %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠪᠤᠰᠤᠳ ᠲᠣᠭᠠᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1382"/>
        <source>min char class %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1391"/>
        <source>max repeat %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠳᠠᠪᠬᠤᠷᠳᠠᠬᠤ ᠲᠣᠭ᠎ᠠ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1400"/>
        <source>max class repeat %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠳᠠᠪᠬᠤᠴᠠᠬᠤ ᠲᠣᠭ᠎ᠠ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1409"/>
        <source>max sequence %1
</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="682"/>
        <source>xxx客户端</source>
        <translation>xxx ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="765"/>
        <source>Programs are not allowed to be added.</source>
        <translation>ᠲᠤᠰ ᠫᠷᠣᠭ᠌ᠷᠠᠮ ᠨᠡᠮᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="8"/>
        <source>简体中文</source>
        <translation>ᠤᠯᠠᠮᠵᠢᠯᠠᠯᠳᠤ ᠬᠢᠲᠠᠳ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="9"/>
        <source>English</source>
        <translation>ᠠᠩᠭ᠌ᠯᠢ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="10"/>
        <source>བོད་ཡིག</source>
        <translation>ᠲᠥᠪᠡᠳ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="11"/>
        <source>ᠮᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ</source>
        <translation>ᠮᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="12"/>
        <source>繁體</source>
        <translation>ᠦᠷᠡᠵᠢᠯᠲᠦ ᠪᠡᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Монгол</source>
        <translation type="vanished">ᠮᠣᠩᠭᠣᠯ ᠪᠢᠴᠢᠭ</translation>
    </message>
</context>
<context>
    <name>ResolutionSlider</name>
    <message>
        <source>No available resolutions</source>
        <translation type="vanished">ᠵᠤᠬᠢᠰᠳᠠᠢ ᠢᠯᠭᠠᠮᠵᠢ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>Screenlock</name>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="26"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="80"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="51"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="136"/>
        <source>Screenlock</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠣᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Screenlock/Screenlock</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="205"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="256"/>
        <source>Show message on lock screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠴᠣᠣᠵᠢᠯᠠᠬᠤ ᠦᠶᠡᠰ ᠵᠠᠩᠭᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Browse</source>
        <translation type="vanished">ᠦᠵᠡ ᠃</translation>
        <extra-contents_path>/Screenlock/Browse</extra-contents_path>
    </message>
    <message>
        <source>Online Picture</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Screenlock/Online Picture</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="401"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="150"/>
        <source>Local Pictures</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ᠎ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Screenlock/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="408"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="152"/>
        <source>Online Pictures</source>
        <translation>ᠱᠤᠭᠤᠮ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Screenlock/Online Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="440"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="162"/>
        <source>Reset To Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Screenlock/Reset To Default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="483"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="139"/>
        <source>Related Settings</source>
        <translation>ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
        <extra-contents_path>/Screenlock/Related Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="332"/>
        <source>Lock screen delay</source>
        <translation>ᠲᠤᠰ ᠴᠠᠭ᠎ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ᠎ᠦ᠋ᠨ ᠠᠷᠤ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="262"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="148"/>
        <source>Show picture of screenlock on screenlogin</source>
        <translation>ᠣᠨᠢᠰᠤᠯᠠᠭᠰᠠᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠬᠠᠨᠠᠨ ᠵᠢᠷᠤᠭ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠠᠭᠠᠭ ᠭᠠᠳᠠᠷᠭᠤ ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠬᠦ</translation>
        <extra-contents_path>/Screenlock/Show picture of screenlock on screenlogin</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>Never</source>
        <translation>ᠶᠡᠷᠦ ᠡᠴᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <source>1m</source>
        <translation type="vanished">1m</translation>
    </message>
    <message>
        <source>5m</source>
        <translation type="vanished">5m</translation>
    </message>
    <message>
        <source>10m</source>
        <translation type="vanished">10m</translation>
    </message>
    <message>
        <source>30m</source>
        <translation type="vanished">30m</translation>
    </message>
    <message>
        <source>45m</source>
        <translation type="vanished">45m</translation>
    </message>
    <message>
        <source>1h</source>
        <translation type="vanished">1h</translation>
    </message>
    <message>
        <source>1.5h</source>
        <translation type="vanished">1.5h</translation>
    </message>
    <message>
        <source>3h</source>
        <translation type="vanished">3h</translation>
    </message>
    <message>
        <source>2h</source>
        <translation type="vanished">2h</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="563"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠹᠠᠢᠯ (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>1min</source>
        <translation>1 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>5min</source>
        <translation>5 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>10min</source>
        <translation>10 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>30min</source>
        <translation>30 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="240"/>
        <source>45min</source>
        <translation>45 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>1hour</source>
        <translation>1 ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>2hour</source>
        <translation>2 ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="241"/>
        <source>3hour</source>
        <translation>3 ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="605"/>
        <source>select custom wallpaper file</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠬᠠᠨᠠᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="606"/>
        <source>Select</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="607"/>
        <source>Position: </source>
        <translation>ᠪᠠᠢᠷᠢ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="608"/>
        <source>FileName: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="609"/>
        <source>FileType: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="610"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="539"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="142"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="698"/>
        <source>Monitor Off</source>
        <translation>ᠦᠵᠡᠬᠦᠷ ᠢ᠋ ᠬᠠᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Screenlock/Monitor Off</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="594"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="145"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="699"/>
        <source>Screensaver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Screenlock/Screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="558"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="613"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="700"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="701"/>
        <source>Set</source>
        <translation>ᠣᠴᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="59"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="99"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="209"/>
        <source>Screensaver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Screensaver/Screensaver</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="201"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="213"/>
        <source>Idle time</source>
        <translation>ᠲᠤᠰ ᠴᠠᠭ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/Screensaver/Idle time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="475"/>
        <source>Lock screen when activating screensaver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠭᠦᠯᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢ ᠣᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="211"/>
        <source>Screensaver program</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠫᠷᠣᠭ᠌ᠷᠠᠮ</translation>
        <extra-contents_path>/Screensaver/Screensaver program</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="960"/>
        <source>Lock screen when screensaver boot</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠳᠡᠯᠭᠡᠴᠡ ᠵᠢ ᠣᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Screensaver/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="197"/>
        <source>View</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="915"/>
        <source>Text(up to 30 characters):</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠲᠸᠺᠰᠲ ( ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 30 ᠦᠰᠦᠭ):</translation>
        <extra-contents_path>/Screensaver/Text(up to 30 characters):</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="950"/>
        <source>Show rest time</source>
        <translation>ᠠᠮᠠᠷᠠᠬᠤ ᠴᠠᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/Screensaver/Show rest time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="272"/>
        <source>UKUI</source>
        <translation>UKUI</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="273"/>
        <source>Blank_Only</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="284"/>
        <source>Customize</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="815"/>
        <source>5min</source>
        <translation>5 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="816"/>
        <source>10min</source>
        <translation>10 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>15min</source>
        <translation>15 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="817"/>
        <source>30min</source>
        <translation>30 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="297"/>
        <source>1hour</source>
        <translation>1 ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="716"/>
        <source>Screensaver source</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠢᠷᠡᠯᠳᠡ</translation>
        <extra-contents_path>/Screensaver/Screensaver source</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="722"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="772"/>
        <source>Select</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="730"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp *.svg)</source>
        <translation>ᠬᠠᠨᠠᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠹᠠᠢᠯ (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp *.svg)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="771"/>
        <source>select custom screensaver dir</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠵᠢᠮ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="773"/>
        <source>Position: </source>
        <translation>ᠪᠠᠢᠷᠢ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="774"/>
        <source>FileName: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="775"/>
        <source>FileType: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="776"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="810"/>
        <source>Switching time</source>
        <translation>ᠳᠠᠪᠳᠠᠮᠵᠢ ᠵᠢ ᠰᠣᠯᠢᠬᠤ</translation>
        <extra-contents_path>/Screensaver/Switching time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="293"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="814"/>
        <source>1min</source>
        <translation>1 ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="861"/>
        <source>Ordinal</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠵᠢᠡᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="871"/>
        <source>Random switching</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ ᠵᠢᠡᠷ ᠰᠣᠯᠢᠬᠤ</translation>
        <extra-contents_path>/Screensaver/Random switching</extra-contents_path>
    </message>
    <message>
        <source>Display text</source>
        <translation type="vanished">显示文本</translation>
    </message>
    <message>
        <source>Enter text, up to 30 characters</source>
        <translation type="vanished">输入文本，最多30个字符</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="992"/>
        <source>Text position</source>
        <translation>ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠪᠠᠢᠷᠢ</translation>
        <extra-contents_path>/Screensaver/Text position</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1000"/>
        <source>Centered</source>
        <translation>ᠳᠦᠪᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="1001"/>
        <source>Randow(Bubble text)</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ ( ᠬᠦᠬᠡᠰᠦ ᠲᠸᠺᠰᠲ)</translation>
    </message>
    <message>
        <source>1m</source>
        <translation type="vanished">1m</translation>
    </message>
    <message>
        <source>5m</source>
        <translation type="vanished">5m</translation>
    </message>
    <message>
        <source>10m</source>
        <translation type="vanished">10m</translation>
    </message>
    <message>
        <source>30m</source>
        <translation type="vanished">30m</translation>
    </message>
    <message>
        <source>45m</source>
        <translation type="vanished">45m</translation>
    </message>
    <message>
        <source>1h</source>
        <translation type="vanished">1h</translation>
    </message>
    <message>
        <source>1.5h</source>
        <translation type="vanished">1.5h</translation>
    </message>
    <message>
        <source>3h</source>
        <translation type="vanished">3h</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="862"/>
        <source>Random</source>
        <translation>ᠳᠠᠰᠢᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="298"/>
        <source>Never</source>
        <translation>ᠶᠡᠷᠦ ᠡᠴᠡ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../searchwidget.cpp" line="61"/>
        <location filename="../../searchwidget.cpp" line="62"/>
        <location filename="../../searchwidget.cpp" line="69"/>
        <location filename="../../searchwidget.cpp" line="71"/>
        <location filename="../../searchwidget.cpp" line="76"/>
        <source>No search results</source>
        <translation>ᠡᠷᠢᠯᠲᠡ ᠶᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>ShareMain</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>please select an output</source>
        <translation type="vanished">ᠭᠠᠷᠭᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>Input Password</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Share</source>
        <translation type="vanished">ᠬᠤᠪᠢᠶᠠᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Output</source>
        <translation type="vanished">ᠭᠠᠷᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>Input</source>
        <translation type="vanished">ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Point</source>
        <translation type="vanished">ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ</translation>
    </message>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠳᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <source>Clipboard</source>
        <translation type="vanished">ᠬᠠᠢᠴᠢᠯᠠᠬᠤ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>ViewOnly</source>
        <translation type="vanished">ᠵᠥᠪᠬᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Client Setting</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Client Number</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠲᠣᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <source>Client IP：</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ IP ᠬᠠᠶᠢᠭ：</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="68"/>
        <source>Remote Desktop</source>
        <translation>ᠠᠯᠤᠰ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
        <extra-contents_path>/Vino/Remote Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="80"/>
        <source>Connect to your desktop remotely</source>
        <translation>ᠲᠤᠰ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠲᠡᠢ ᠠᠯᠤᠰ᠎ᠠ᠋ᠴᠠ ᠬᠣᠯᠪᠣᠬᠤ</translation>
        <extra-contents_path>/Vino/Connect to your desktop remotely</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="98"/>
        <source>Allow others to connect to your desktop remotely using RDP</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ RDP ᠠᠯᠤᠰ ᠡᠴᠡ ᠲᠤᠰ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠲᠡᠢ ᠬᠣᠯᠪᠣᠬᠤ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using RDP</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="112"/>
        <source>Allow others to connect to your desktop remotely using VNC</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ VNC ᠠᠯᠤᠰ ᠡᠴᠡ ᠲᠤᠰ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠲᠡᠢ ᠬᠣᠯᠪᠣᠬᠤ᠎ᠶ᠋ᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ</translation>
        <extra-contents_path>/Vino/Allow others to connect to your desktop remotely using VNC</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="160"/>
        <source>Require user to enter this password while using VNC: </source>
        <translation>VNC ᠠᠯᠤᠰ ᠴᠦᠷᠬᠡᠯᠡᠭᠡ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠶᠡᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ：</translation>
        <extra-contents_path>/Vino/Require user to enter this password while using VNC:</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="164"/>
        <source>Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Allow others to view your desktop</source>
        <translation type="vanished">ᠪᠤᠰᠤᠳ ᠬᠥᠮᠦᠨ ᠠᠯᠤᠰ ᠡᠴᠡ ᠲᠠᠨ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠲᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
        <extra-contents_path>/Vino/Allow others to view your desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="126"/>
        <source>Allow connection to control screen</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠥᠮᠦᠨ ᠠᠯᠤᠰ ᠡᠴᠡ ᠲᠠᠨ ᠤ᠋ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ᠂ ᠲᠠᠨ ᠤ᠋ ᠳᠡᠯᠭᠡᠴᠡ ᠵᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
        <extra-contents_path>/Vino/Allow connection to control screen</extra-contents_path>
    </message>
    <message>
        <source>Security</source>
        <translation type="vanished">ᠠᠶᠤᠯᠬᠦᠢ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="142"/>
        <source>You must confirm every visit for this machine</source>
        <translation>ᠲᠠ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠤᠳᠠᠭ᠎ᠠ ᠪᠦᠷᠢ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
        <extra-contents_path>/Vino/You must confirm every visit for this machine</extra-contents_path>
    </message>
    <message>
        <source>Require user to enter this password: </source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠲᠤᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ: </translation>
        <extra-contents_path>/Vino/Require user to enter this password:</extra-contents_path>
    </message>
    <message>
        <source>Password can not be blank</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Password length must be less than or equal to 8</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠤᠷᠳᠤ 8 ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
</context>
<context>
    <name>ShareMainHw</name>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="201"/>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="271"/>
        <source>please select an output</source>
        <translation>ᠨᠢᠭᠡᠨ ᠭᠠᠷᠭᠠᠬᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="331"/>
        <source>Input Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="332"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="445"/>
        <source>Share</source>
        <translation>ᠬᠠᠮᠲᠤ᠎ᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="461"/>
        <source>Allow others to view your desktop</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠥᠮᠦᠨ ᠲᠠᠨ᠎ᠤ᠋ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠲᠤ᠌ ᠠᠯᠤᠰ᠎ᠠ᠋ᠴᠠ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ᠎ᠶ᠋ᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="471"/>
        <source>Security</source>
        <translation>ᠠᠶᠤᠯᠭᠦᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="481"/>
        <source>Require user to enter this password: </source>
        <translation>ᠲᠤᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠪᠡᠷ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ ：</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="483"/>
        <source>Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="496"/>
        <source>Output</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="508"/>
        <source>Input</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="518"/>
        <source>Point</source>
        <translation>ᠮᠠᠦ᠋ᠰ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="520"/>
        <source>Keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="522"/>
        <source>Clipboard</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠮᠠᠯ ᠬᠠᠪᠲᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="548"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="550"/>
        <source>ViewOnly</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="558"/>
        <source>Client Setting</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠦᠵᠦᠭᠦᠷ᠎ᠦ᠋ᠨ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="569"/>
        <source>Client Number</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠦᠵᠦᠭᠦᠷ᠎ᠦ᠋ᠨ ᠲᠣᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="583"/>
        <source>Client IP：</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠦᠵᠦᠭᠦᠷ᠎ᠦ᠋ᠨ IP ᠬᠠᠶᠢᠭ ：</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>退出程序</source>
        <translation>ᠪᠤᠴᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemainhw.cpp" line="714"/>
        <source>确认退出程序！</source>
        <translation>ᠪᠤᠴᠠᠬᠤ ᠫᠷᠤᠭ᠌ᠷᠠᠮ᠎ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ !</translation>
    </message>
</context>
<context>
    <name>Shortcut</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="50"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="164"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="178"/>
        <source>System Shortcut</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ</translation>
        <extra-contents_path>/Shortcut/System Shortcut</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="103"/>
        <source>Custom Shortcut</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="166"/>
        <source>Customize Shortcut</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ</translation>
        <extra-contents_path>/Shortcut/Customize Shortcut</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="161"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ᠌</translation>
        <extra-contents_path>/Shortcut/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="441"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="573"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="836"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="442"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="574"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="837"/>
        <source>Use</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="443"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="575"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="838"/>
        <source>Shortcut key conflict, use it?</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠢᠯᠡᠮᠡᠯ᠎ᠦ᠋ᠨ ᠮᠥᠷᠭᠦᠯᠳᠦᠯ ᠂ ᠲᠤᠰ ᠨᠡᠢᠯᠡᠮᠡᠯ᠎ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="444"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="576"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="839"/>
        <source>%1 occuied, using this combination will invalidate %2</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠡᠵᠡᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠂ ᠲᠤᠰ ᠨᠡᠢᠯᠡᠮᠡᠯ ᠨᠡᠢᠭᠡᠮᠯᠢᠭ ᠨᠢ “ %2 ” ᠢ᠋/ ᠵᠢ ᠳᠠᠷᠤᠪᠴᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠮᠰᠠᠯ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="467"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="606"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="868"/>
        <source>Shortcut &quot;%1&quot; occuied, please change the key combination</source>
        <translation>ᠲᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ “ %1 ” ᠨᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠳ᠋ᠦ᠍ ᠡᠵᠡᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠂ ᠳᠠᠷᠤᠪᠴᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠢᠯᠡᠮᠡᠯ᠎ᠢ᠋ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠪᠣᠯᠪᠠᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="677"/>
        <source>Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="678"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1039"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1043"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1061"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1065"/>
        <source>Null</source>
        <translation>ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="1126"/>
        <source> or </source>
        <translation> ᠡᠰᠡᠬᠦᠯ᠎ᠡ </translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="80"/>
        <source>Shortcut</source>
        <translation>ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
</context>
<context>
    <name>StatusDialog</name>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="10"/>
        <source>About</source>
        <translation>ᠲᠤᠰ ᠮᠠᠰᠢᠨ᠎ᠤ᠋ ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="59"/>
        <source>Activation Code</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠣᠳ᠋</translation>
    </message>
</context>
<context>
    <name>Theme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="107"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="521"/>
        <source>Theme</source>
        <translation>ᠭᠣᠤᠯ ᠰᠡᠳᠦᠪ</translation>
        <extra-contents_path>/Theme/Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <source>Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="183"/>
        <source>Light</source>
        <translation>ᠬᠦᠶᠦᠭᠡᠨ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="181"/>
        <source>Dark</source>
        <translation>ᠭᠦᠨ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="179"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="185"/>
        <source>Auto</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠢ᠋ ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="422"/>
        <source>Corlor</source>
        <translation>ᠳᠤᠳᠤᠳᠬᠠᠯ ᠦᠩᠭᠡ</translation>
        <extra-contents_path>/Theme/Corlor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="604"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
        <extra-contents_path>/Theme/Other</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="610"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="624"/>
        <source>Set</source>
        <translation>ᠣᠴᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="616"/>
        <source>Wallpaper</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠬᠠᠨᠠᠨ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Theme/Wallpaper</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="630"/>
        <source>Beep</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠳᠠᠭᠤ</translation>
        <extra-contents_path>/Theme/Beep</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="961"/>
        <source>Blue-Crystal</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᠯᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="963"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <source>Light-Seeking</source>
        <translation>ᠭᠡᠷᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="965"/>
        <source>DMZ-Black</source>
        <translation>DMZ- ᠬᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="967"/>
        <source>DMZ-White</source>
        <translation>ᠴᠠᠭᠠᠨ ᠳᠠᠷᠤᠮᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="969"/>
        <source>Dark-Sense</source>
        <translation>ᠬᠢᠯᠤᠬᠡᠷ ᠬᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1084"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1088"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>basic</source>
        <translation>ᠰᠠᠭᠤᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>Classic</source>
        <translation>ᠰᠤᠩᠭᠤᠳᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1092"/>
        <source>hp</source>
        <translation>ᠬᠤᠢ ᠫᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1094"/>
        <source>ukui</source>
        <translation>ukui</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1090"/>
        <source>HeYin</source>
        <translation>ᠶᠢᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1086"/>
        <source>classic</source>
        <translation>ᠰᠤᠩᠭᠤᠳᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1096"/>
        <source>daybreakBlue</source>
        <translation>ᠬᠥᠬᠡ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1098"/>
        <source>jamPurple</source>
        <translation>ᠬᠦᠷᠡᠩ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1100"/>
        <source>magenta</source>
        <translation>ᠮᠠᠨᠳᠠᠷᠸ᠎ᠠ ᠤᠯᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1102"/>
        <source>sunRed</source>
        <translation>ᠤᠯᠠᠭᠠᠨ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1104"/>
        <source>sunsetOrange</source>
        <translation>ᠰᠢᠷ᠎ᠠ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1106"/>
        <source>dustGold</source>
        <translation>ᠰᠢᠷ᠎ᠠ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1108"/>
        <source>polarGreen</source>
        <translation>ᠨᠣᠭᠣᠭᠠᠨ ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">默认</translation>
    </message>
    <message>
        <source>Middle</source>
        <translation type="vanished">中</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="212"/>
        <source>Window Theme</source>
        <translation>ᠴᠣᠩᠬᠣᠨ ᠤ᠋ ᠭᠠᠳᠠᠷ ᠦᠵᠡᠮᠵᠢ</translation>
        <extra-contents_path>/Theme/Window Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="214"/>
        <source>Icon theme</source>
        <translation>ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Theme/Icon theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="217"/>
        <source>Cursor theme</source>
        <translation>ᠺᠸᠰᠸ</translation>
        <extra-contents_path>/Theme/Cursor theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="134"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="221"/>
        <source>Effect setting</source>
        <translation>ᠴᠣᠩᠬᠣᠨ ᠤ᠋ ᠣᠨᠴᠠᠭᠠᠢ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ</translation>
        <extra-contents_path>/Theme/Effect setting</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="352"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="225"/>
        <source>Transparency</source>
        <translation>ᠳᠤᠩᠭᠠᠯᠠᠭ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
        <extra-contents_path>/Theme/Transparency</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="249"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="223"/>
        <source>Performance mode</source>
        <translation>ᠣᠨᠴᠠᠭᠠᠢ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ</translation>
        <extra-contents_path>/Theme/Performance mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="473"/>
        <source>Reset to default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>TimeBtn</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="80"/>
        <source>Tomorrow</source>
        <translation>ᠮᠠᠷᠭᠠᠰᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="82"/>
        <source>Yesterday</source>
        <translation>ᠥᠴᠥᠭᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="84"/>
        <source>Today</source>
        <translation>ᠥᠨᠥᠳᠥᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="102"/>
        <source>%1 hours earlier than local</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠡᠴᠡ %1 ᠴᠠᠭ ᠡᠷᠲᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="104"/>
        <source>%1 hours later than local</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠡᠴᠡ %1 ᠴᠠᠭ ᠤᠷᠤᠢ</translation>
    </message>
</context>
<context>
    <name>TimeZoneChooser</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="38"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="39"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="36"/>
        <source>Search Timezone</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠢ᠋ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="65"/>
        <source>To select a time zone, please click where near you on the map and select a city from the nearest city</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠪᠡᠯ᠂ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ ᠳᠡᠭᠡᠷ᠎ᠡ ᠥᠪᠡᠷ ᠤ᠋ᠨ ᠪᠠᠢᠭ᠎ᠠ ᠪᠠᠢᠷᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ᠂ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠢᠷᠠᠯᠴᠠᠭ᠎ᠠ ᠬᠤᠳᠠ ᠡᠴᠡ ᠨᠢᠭᠡ ᠬᠤᠳᠠ ᠵᠢ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="41"/>
        <source>Change Timezone</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠢ᠋ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>TrialDialog</name>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="12"/>
        <source>Set</source>
        <translation>ᠣᠴᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="37"/>
        <source>Yinhe Kylin OS(Trail Version) Disclaimer</source>
        <translation>ᠳᠡᠭᠷᠢ ᠵᠢᠨ ᠣᠶᠣᠳᠠᠯ ᠴᠢ ᠯᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ( ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ) ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠡᠴᠡ ᠬᠡᠯᠳᠦᠷᠢᠬᠦᠯᠬᠦ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="46"/>
        <source>Dear customer:
       Thank you for trying Yinhe Kylin OS(trail version)! This version is free for users who only try out, no commercial purpose is permitted. The trail period lasts one year and it starts from the ex-warehouse time of the OS. No after-sales service is provided during the trail stage. If any security problems occurred when user put important files or do any commercial usage in system, all consequences are taken by users. Kylin software Co., Ltd. take no legal risk in trail version.
       During trail stage,if you want any technology surpport or activate the system, please buy“Yinhe Kylin Operating System”official version or authorization by contacting 400-089-1870.</source>
        <translation>ᠬᠦᠨᠳᠦᠳᠦ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌:
ᠰᠠᠢᠨ ᠤᠤ! ᠳᠠᠰᠢᠷᠠᠮ ᠵᠢᠡᠷ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠳᠡᠭᠷᠢ ᠵᠢᠨ ᠣᠶᠣᠳᠠᠯ ᠴᠢ ᠯᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ( ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠡᠪᠯᠡᠯ) ᠨᠢ ᠲᠤᠰ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠬᠠᠷᠠᠭᠠᠯᠵᠠᠭᠰᠠᠨ ᠰᠠᠯᠪᠤᠷᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠳᠦᠯᠦᠪᠦᠷᠢ ᠥᠬᠡᠢ ᠪᠡᠷ ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦᠯᠬᠦ ᠬᠡᠪᠯᠡᠯ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠪᠦᠳᠦᠨ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠂ ᠬᠢᠨᠠᠨ ᠳᠤᠷᠰᠢᠬᠤ ᠵᠢᠴᠢ ᠦᠨᠡᠯᠡᠭᠡ ᠬᠢᠬᠦ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ᠂ ᠪᠤᠰᠤᠳ ᠶᠠᠮᠠᠷᠪᠠ ᠠᠷᠠᠯᠵᠢᠶᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠳ᠋ᠤ᠌ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠥᠬᠡᠢ᠃ ᠲᠤᠰ ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠡᠪᠯᠡᠯ ᠨᠢ ᠰᠣᠹᠲ ᠦᠢᠯᠡᠳᠪᠦᠷᠢ ᠡᠴᠡ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠡᠴᠡ ᠡᠬᠢᠯᠡᠨ ᠴᠠᠭ ᠪᠤᠳᠤᠵᠤ᠂ ᠨᠢᠭᠡ ᠵᠢᠯ ᠤ᠋ᠨ ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠲᠠᠢ᠃ ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ ᠳ᠋ᠤ᠌ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠵᠢᠩᠬᠢᠨᠢ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠬᠠᠩᠭᠠᠬᠤ ᠥᠬᠡᠢ᠂ ᠬᠡᠷᠪᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠡᠪᠯᠡᠯ ᠳᠡᠭᠡᠷ᠎ᠡ ᠥᠪᠡᠷ ᠵᠢᠨᠨ ᠴᠢᠬᠤᠯᠠ ᠹᠠᠢᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠰᠠᠨ ᠪᠤᠶᠤ ᠬᠤᠪᠢ ᠪᠡᠷ ᠵᠢᠨᠨ ᠠᠷᠠᠯᠵᠢᠶᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠳ᠋ᠤ᠌ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠣᠯ᠂ ᠳᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠣᠯᠤᠭᠰᠠᠨ ᠶᠠᠮᠠᠷᠪᠠ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠴᠢ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠪᠦᠬᠦᠨ ᠢ᠋ ᠡᠬᠦᠷᠬᠡᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ ᠴᠢ ᠯᠢᠨ ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠳᠤ ᠺᠣᠮᠫᠠᠨᠢ ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠬᠠᠤᠯᠢ ᠵᠢᠨ ᠬᠠᠷᠢᠭᠤᠴᠠᠯᠭ᠎ᠠ ᠡᠬᠦᠷᠬᠡᠯᠡᠬᠦ ᠥᠬᠡᠢ᠃
ᠳᠤᠷᠰᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠶᠠᠪᠤᠴᠠ ᠳ᠋ᠤ᠌ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠪᠤᠶᠤ ᠲᠤᠰᠬᠠᠢ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠦᠷᠳᠡᠬᠦ ᠵᠢ ᠬᠦᠰᠡᠪᠡᠯ᠂ ᠲᠠ ᠳᠡᠭᠷᠢ ᠵᠢᠨ ᠣᠶᠣᠳᠠᠯ ᠴᠢ ᠯᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠵᠢᠩᠬᠢᠨᠢ ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠬᠤᠳᠠᠯᠳᠤᠨ ᠠᠪᠬᠤ ᠪᠤᠶᠤ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠤᠳᠠᠰᠤ᠄ 400-089-1870.</translation>
    </message>
    <message>
        <source>Dear customer:
    Thank you for trying Yinhe Kylin OS(trail version)! This version is free for users who only try out, no commercial purpose is permitted. The trail period lasts one year and it starts from the ex-warehouse time of the OS. No after-sales service is provided during the trail stage. If any security problems occurred when user put important files or do any commercial usage in system, all consequences are taken by users. Kylin software Co., Ltd. take no legal risk in trail version.
    During trail stage,if you want any technology surpport or activate the system, please buy“Yinhe Kylin Operating System”official version or authorization by contacting 400-089-1870.</source>
        <translation type="vanished">尊敬的客户：
    您好！随机安装的“银河麒麟操作系统（试用版）”是针对该版本对应的行业客户的免费试用版本，用于整机的试用、测试和评估，不能用于其他任何商业用途。此试用版本以软件出库时间计时，试用时间为一年。试用期间不提供相关正版软件的售后服务，如果客户在试用版本上自行存放重要文件及私自进行商业用途，由此产生的任何安全问题及结果一概由用户自己承担，麒麟软件有限公司不承担任何法律风险。
    在试用过程中，如希望激活或者得到专业的技术服务支持，请您购买“银河麒麟操作系统”正式版本或授权，联系方式如下：400-089-1870。</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="60"/>
        <source>Kylin software Co., Ltd.</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠳᠤ ᠺᠣᠮᠫᠠᠨᠢ</translation>
    </message>
    <message>
        <source>www.Kylinos.cn</source>
        <translation type="vanished">www.kylinos.cn</translation>
    </message>
</context>
<context>
    <name>UkccAbout</name>
    <message>
        <location filename="../../ukccabout.cpp" line="33"/>
        <location filename="../../ukccabout.cpp" line="59"/>
        <source>Settings</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="64"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ ：</translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="74"/>
        <source>Service and Support:</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ:</translation>
    </message>
</context>
<context>
    <name>UnifiedOutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="91"/>
        <source>resolution</source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="128"/>
        <source>orientation</source>
        <translation>ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="133"/>
        <source>arrow-up</source>
        <translation>ᠡᠷᠬᠢᠯᠳᠦᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="134"/>
        <source>90° arrow-right</source>
        <translation>90°ᠴᠠᠭ ᠤ᠋ᠨ ᠵᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="136"/>
        <source>arrow-down</source>
        <translation>ᠳᠡᠬᠡᠭᠰᠢ ᠳᠣᠷᠣᠭᠰᠢ ᠳᠤᠩᠭᠤᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="135"/>
        <source>90° arrow-left</source>
        <translation>90° ᠴᠠᠭ ᠤ᠋ᠨ ᠵᠡᠬᠦᠦ ᠵᠢᠨ ᠡᠰᠡᠷᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="168"/>
        <source>frequency</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="228"/>
        <source>screen zoom</source>
        <translation>ᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠡᠯᠭᠡᠴᠡ</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="393"/>
        <source>auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋</translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1202"/>
        <source>root</source>
        <translation>root</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1170"/>
        <source>Hint</source>
        <translation>ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1075"/>
        <source>The account type of “%1” has been modified, will take effect after logout, whether to logout?</source>
        <translation>“%1” ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢᠭᠡᠨᠳᠡ ᠵᠠᠰᠠᠭᠳᠠᠪᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1076"/>
        <source>logout later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1077"/>
        <source>logout now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1171"/>
        <source>The system only allows one user to log in automatically.After it is turned on, the automatic login of other users will be turned off.Is it turned on?</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠵᠥᠪᠬᠡᠨ ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ᠂ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠢ ᠬᠠᠭᠠᠨ᠎ᠠ᠂ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1174"/>
        <source>Trun on</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1175"/>
        <source>Close on</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1198"/>
        <source>Standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1200"/>
        <source>Admin</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="160"/>
        <source>CurrentUser</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
        <extra-contents_path>/Userinfo/CurrentUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="162"/>
        <source>OthersUser</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
        <extra-contents_path>/Userinfo/OthersUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="306"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
        <extra-contents_path>/Userinfo/Add</extra-contents_path>
    </message>
    <message>
        <source>Passwd</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="194"/>
        <source>Groups</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ</translation>
        <extra-contents_path>/Userinfo/Groups</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="271"/>
        <source>AutoLoginOnBoot</source>
        <translation>ᠮᠠᠰᠢᠨ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
        <extra-contents_path>/Userinfo/AutoLoginOnBoot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="249"/>
        <source>LoginWithoutPwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠥᠬᠡᠢ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
        <extra-contents_path>/Userinfo/LoginWithoutPwd</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="683"/>
        <source>The user is logged in, please delete the user after logging out</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠪᠳᠡᠷᠡᠪᠡ᠂ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠬᠠᠰᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="41"/>
        <source>Current User</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="309"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="182"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
        <extra-contents_path>/Userinfo/Password</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="331"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="188"/>
        <source>Type</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
        <extra-contents_path>/Userinfo/Type</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="353"/>
        <source>Group</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="422"/>
        <source>Login no passwd</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠥᠬᠡᠢ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="500"/>
        <source>Automatic login at boot</source>
        <translation>ᠮᠠᠰᠢᠨ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="568"/>
        <source>Other Users</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>UserInfoIntel</name>
    <message>
        <source>Current User</source>
        <translation type="vanished">ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
        <extra-contents_path>/UserinfoIntel/Current User</extra-contents_path>
    </message>
    <message>
        <source>Change phone</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Change pwd</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
        <extra-contents_path>/UserinfoIntel/Change pwd</extra-contents_path>
    </message>
    <message>
        <source>User group</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ</translation>
    </message>
    <message>
        <source>Del user</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>system reboot</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Unclosed apps start after a restart</source>
        <translation type="vanished">ᠬᠠᠭᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠫᠷᠣᠭ᠌ᠷᠠᠮ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠮᠥᠨ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
        <extra-contents_path>/UserinfoIntel/Other Users</extra-contents_path>
    </message>
    <message>
        <source>User Info Intel</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ Intel</translation>
    </message>
    <message>
        <source>Change Tel</source>
        <translation type="vanished">ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
        <extra-contents_path>/UserinfoIntel/Change Tel</extra-contents_path>
    </message>
    <message>
        <source>Delete user</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ ᠬᠠᠰᠤᠨ᠎ᠠ᠃</translation>
        <extra-contents_path>/UserinfoIntel/Delete user</extra-contents_path>
    </message>
    <message>
        <source>Change user name</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠰᠣᠯᠢᠬᠤ</translation>
        <extra-contents_path>/UserinfoIntel/Change user name</extra-contents_path>
    </message>
    <message>
        <source>standard user</source>
        <translation type="vanished">ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>administrator</source>
        <translation type="vanished">ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <source>root</source>
        <translation type="vanished">ᠦᠨᠳᠦᠰᠦ</translation>
    </message>
    <message>
        <source>Add new user</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>set pwd</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="vanished">ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UtilsForUserinfo</name>
    <message>
        <source>Passwd</source>
        <translation type="vanished">ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="36"/>
        <source>Type</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">del</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="32"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋᠎ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="40"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="218"/>
        <source>Standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="220"/>
        <source>Admin</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>Vino</name>
    <message>
        <location filename="../../../plugins/system/vino/vino.cpp" line="28"/>
        <source>Vino</source>
        <translation>ᠠᠯᠤᠰ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>VinoHw</name>
    <message>
        <source>Vino</source>
        <translation type="vanished">ᠠᠯᠤᠰ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="103"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="151"/>
        <source>Desktop Background</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠠᠷᠤ ᠦᠵᠡᠭᠳᠡᠯ</translation>
        <extra-contents_path>/Wallpaper/Desktop Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="401"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="156"/>
        <source>Mode</source>
        <translation>ᠮᠤᠳ᠋</translation>
        <extra-contents_path>/Wallpaper/Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="532"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="158"/>
        <source>Local Pictures</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ᠎ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Wallpaper/Local Pictures</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="539"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="160"/>
        <source>Online Pictures</source>
        <translation>ᠱᠤᠭᠤᠮ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Wallpaper/Online Pictures</extra-contents_path>
    </message>
    <message>
        <source>Online Picture</source>
        <translation type="vanished">ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠤᠭ</translation>
        <extra-contents_path>/Wallpaper/Online Picture</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="571"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="170"/>
        <source>Reset To Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Wallpaper/Reset To Default</extra-contents_path>
    </message>
    <message>
        <source>Browse</source>
        <translation type="vanished">ᠦᠵᠡ ᠃</translation>
        <extra-contents_path>/Wallpaper/Browse</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="331"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="58"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="154"/>
        <source>Background</source>
        <translation>ᠠᠷᠤ ᠦᠵᠡᠭᠳᠡᠯ</translation>
        <extra-contents_path>/Wallpaper/Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>picture</source>
        <translation>ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="185"/>
        <source>color</source>
        <translation>ᠦᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>wallpaper</source>
        <translation>ᠬᠡᠪᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>centered</source>
        <translation>ᠳᠦᠪᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>scaled</source>
        <translation>ᠳᠠᠭᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>stretched</source>
        <translation>ᠰᠤᠩᠭᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>zoom</source>
        <translation>ᠵᠣᠬᠢᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="201"/>
        <source>spanned</source>
        <translation>ᠠᠯᠤᠰᠯᠠᠭᠰᠠᠨ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Blue cyan</source>
        <translation>ᠬᠥᠬᠡ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Pine green</source>
        <translation>ᠰᠤᠯᠠ ᠾᠦᠢᠢᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Emerald green</source>
        <translation>ᠡᠷᠭᠢ ᠶᠢᠨ ᠨᠣᠭᠣᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Green</source>
        <translation>ᠨᠣᠭᠣᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Dark cyan</source>
        <translation>ᠭᠦᠨ ᠬᠥᠬᠡ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Slate green</source>
        <translation>ᠴᠢᠯᠠᠭᠤᠨ ᠬᠠᠪᠲᠠᠰᠤ ᠨᠣᠭᠣᠭᠠᠨ ᠥᠩᠭᠡ ᠲᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="349"/>
        <source>Mineral green</source>
        <translation>ᠡᠷᠦᠳᠡᠰᠦ ᠨᠣᠭᠣᠭᠠᠨ ᠥᠩᠭᠡ ᠲᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Taupe</source>
        <translation>ᠦᠨᠡᠰᠦᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Dark brown</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠬᠦᠷᠡᠩ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Black</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Aurantiacus</source>
        <translation>ᠵᠦᠷᠵᠢ ᠶᠢᠨ ᠰᠢᠷ᠎ᠠ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Red</source>
        <translation>ᠤᠯᠠᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Brick-red</source>
        <translation>ᠲᠣᠭᠣᠰᠬ᠎ᠠ ᠤᠯᠠᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Rose red</source>
        <translation>ᠴᠠᠷᠴᠠᠬᠠᠶ ᠤᠯᠠᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="350"/>
        <source>Purplish red</source>
        <translation>ᠪᠣᠷᠣ ᠤᠯᠠᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Dark magenta</source>
        <translation>ᠭᠦᠨ ᠳᠠᠯᠠᠢ ᠶᠢᠨ ᠤᠯᠠᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Purple</source>
        <translation>ᠬᠦᠷᠡᠩ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Violet</source>
        <translation>ᠬᠦᠬᠡ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Medium purple</source>
        <translation>ᠳᠤᠮᠳᠠ ᠬᠦᠷᠡᠩ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="351"/>
        <source>Grey</source>
        <translation>ᠦᠨᠡᠰᠦᠨ ᠴᠠᠭᠠᠨ ᠥᠩᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="620"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠹᠠᠢᠯ (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</translation>
    </message>
    <message>
        <source>allFiles(*.*)</source>
        <translation type="vanished">所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="660"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="703"/>
        <source>select custom wallpaper file</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠬᠠᠨᠠᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="661"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="704"/>
        <source>Select</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="662"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="705"/>
        <source>Position: </source>
        <translation>ᠪᠠᠢᠷᠢ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="663"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="706"/>
        <source>FileName: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="664"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="707"/>
        <source>FileType: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ: </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="665"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="708"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>night mode</source>
        <translation type="vanished">ᠦᠩᠭᠡ ᠵᠢᠨ ᠳᠤᠯᠠᠭᠠᠴᠠ</translation>
        <extra-contents_path>/Display/night mode</extra-contents_path>
    </message>
    <message>
        <source>Night Mode</source>
        <translation type="vanished">ᠦᠩᠭᠡ ᠵᠢᠨ ᠳᠤᠯᠠᠭᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2348"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2375"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="414"/>
        <source>Time</source>
        <translation>ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="431"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Custom Time</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="442"/>
        <source>to</source>
        <translation>ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="397"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="469"/>
        <source>Color Temperature</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠦᠩᠭᠡ ᠵᠢᠨ ᠳᠤᠯᠠᠭᠠᠴᠠ</translation>
        <extra-contents_path>/Display/Color Temperature</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="371"/>
        <source>Eye Protection Mode</source>
        <translation>ᠨᠢᠳᠦ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
        <extra-contents_path>/Display/Eye Protection Mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="372"/>
        <source>When turned on, it can reduce blue light to prevent eye, the screen will turn yellow.</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠴᠡᠩᠬᠡᠷ ᠭᠡᠷᠡᠯ ᠤ᠋ᠨ ᠰᠠᠴᠤᠷᠠᠭ᠎ᠢ᠋ ᠪᠠᠭᠠᠰᠬᠠᠵᠤ᠂ ᠨᠢᠳᠦᠨ᠎ᠦ᠌ ᠶᠠᠳᠠᠷᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠳᠠᠢᠯᠵᠤ᠂ ᠳᠡᠯᠭᠡᠴᠡ ᠲᠣᠳᠣᠷᠬᠠᠢ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠪᠡᠷ ᠰᠢᠷ᠎ᠠ ᠪᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>When turned on, it will reduce the blue light of the screen.</source>
        <translation type="vanished">ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ᠌ ᠥᠩᠭᠡ᠎ᠶ᠋ᠢᠨ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠢ᠋ ᠲᠤᠶᠠᠭᠠᠨ᠎ᠳ᠋ᠤ᠌ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠤᠭᠠᠳ ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠬᠥᠬᠡ ᠭᠡᠷᠡᠯ᠎ᠢ᠋ ᠪᠠᠭᠤᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="340"/>
        <source>Color Temperature And Eye Care</source>
        <translation>ᠦᠩᠭᠡ ᠵᠢᠨ ᠳᠤᠯᠠᠭᠠᠴᠠ ᠬᠢᠬᠡᠳ ᠨᠢᠳᠦ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ</translation>
        <extra-contents_path>/Display/Color Temperature And Eye Care</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="473"/>
        <source>Warmer</source>
        <translation>ᠨᠡᠯᠢᠶᠡᠳ ᠳᠤᠯᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="476"/>
        <source>Colder</source>
        <translation>ᠨᠡᠯᠢᠶᠡᠳ ᠬᠦᠢᠳᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="793"/>
        <source>Multi-screen</source>
        <translation>ᠣᠯᠠᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠪᠡᠷ ᠢᠯᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="798"/>
        <source>First Screen</source>
        <translation>ᠨᠢᠭᠡᠳᠦᠬᠡᠷ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="801"/>
        <source>Clone Screen</source>
        <translation>ᠳᠤᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Net Monitor</source>
        <translation type="vanished">ᠲᠣᠣᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦᠵᠡᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="840"/>
        <source>Display</source>
        <translation>ᠦᠵᠡᠬᠦᠷ</translation>
        <extra-contents_path>/Display/Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="845"/>
        <source>Primary Screen</source>
        <translation>ᠭᠣᠣᠯ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1018"/>
        <source>Auto Brightness</source>
        <translation>ᠬᠡᠷᠡᠯᠳᠦᠴᠡ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Display/Auto Brightness</extra-contents_path>
    </message>
    <message>
        <source>Adjust screen brightness by ambient</source>
        <translation type="vanished">ᠣᠷᠴᠢᠨ ᠲᠣᠭᠣᠷᠢᠨ ᠤ᠋ ᠬᠡᠷᠡᠯᠳᠦᠴᠡ ᠵᠢ ᠮᠡᠳᠡᠷᠡᠬᠦ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠬᠡᠷᠡᠯᠳᠦᠴᠡ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Dynamic light</source>
        <translation type="vanished">ᠬᠥᠳᠡᠯᠭᠡᠭᠡᠨ ᠤ᠋ ᠠᠷᠤ ᠭᠡᠷᠡᠯ</translation>
        <extra-contents_path>/Display/Dynamic light</extra-contents_path>
    </message>
    <message>
        <source>Optimize display content to extend battery life</source>
        <translation type="vanished">ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠰᠢᠯᠢᠳᠡᠭᠵᠢᠬᠦᠯᠬᠦ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ ᠵᠢᠨ ᠠᠮᠢ ᠨᠠᠰᠤ ᠵᠢ ᠤᠷᠳᠤᠳᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1087"/>
        <source>scale</source>
        <translation>ᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1091"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after 14 seconds&lt;/font&gt;</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ %1 ᠢ᠋/ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠵᠠᠰᠠᠪᠠ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠤ ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt; ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ 14 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠨ᠎ᠡ᠃&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1102"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after %2 seconds&lt;/font&gt;</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ %1 ᠢ᠋/ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠵᠠᠰᠠᠪᠠ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠤ ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt; ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ %2 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠨ᠎ᠡ᠃&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1299"/>
        <source>The zoom has been modified, it will take effect after you log off</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠶᠡᠬᠡᠰᠬᠡᠯ ᠨᠢᠭᠡᠨᠲᠡ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠭᠳᠡᠪᠡ ᠂ ᠳᠠᠩᠰᠠᠨ᠎ᠠ᠋ᠴᠠ ᠬᠠᠰᠤᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ ᠪᠣᠯᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 14 seconds&lt;/font&gt;</source>
        <translation type="vanished">屏幕%1已修改，是否保存？&lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;系统将在14秒后自动保存。&lt;/font&gt;</translation>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after %2 seconds&lt;/font&gt;</source>
        <translation type="vanished">ᠳᠡᠯᠭᠡᠴᠡ %1 ᠢ᠋/ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠵᠠᠰᠠᠪᠠ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠤ ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt; ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ %2 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ᠃&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>Follow the sunrise and sunset</source>
        <translation>ᠨᠠᠷᠠ ᠭᠠᠷᠬᠤ ᠬᠢᠭᠡᠳ ᠨᠠᠷᠠ ᠤᠨᠠᠬᠤ᠎ᠶᠢ ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="800"/>
        <source>Extend Screen</source>
        <translation>ᠦᠷᠬᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="799"/>
        <source>Vice Screen</source>
        <translation>ᠪᠤᠰᠤᠳ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <source>monitor</source>
        <translation type="vanished">ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠴᠢ</translation>
        <extra-contents_path>/display/monitor</extra-contents_path>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Theme follow night mode</source>
        <translation type="vanished">ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠰᠥᠨᠢ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠢ᠋ ᠳᠠᠭᠠᠵᠤ ᠬᠤᠪᠢᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1078"/>
        <source>resolution</source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1081"/>
        <source>orientation</source>
        <translation>ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1084"/>
        <source>frequency</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1297"/>
        <source>Hint</source>
        <translation>ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>After modifying the resolution or refresh rate, due to compatibility issues between the display device and the graphics card, the display may be abnormal or unable to display
the settings will be saved after 14 seconds</source>
        <translation type="vanished">是否保留当前修改的配置？将在14秒后自动保存配置</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1069"/>
        <source>Save</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1070"/>
        <source>Not Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="805"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="807"/>
        <source>Network Display</source>
        <translation>ᠲᠣᠣᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦᠵᠡᠭᠦᠷ</translation>
        <extra-contents_path>/Display/Network Display</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1300"/>
        <source>Log out now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1301"/>
        <source>Later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1318"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1328"/>
        <source>(Effective after logout)</source>
        <translation>(᠎ᠳᠠᠩᠰᠠᠨ᠎ᠠ᠋ᠴᠠ ᠬᠠᠰᠤᠭᠰᠠᠨ᠎ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠦᠴᠦᠨ᠎ᠲᠡᠢ )</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2264"/>
        <source>are you sure to apply?
1 Select &quot;apply&quot;, manually log out late
2 Select &quot;log out to apply&quot;, log out now to apply
3 Select &quot;cancel&quot;, cancel to apply</source>
        <translation>ᠰᠠᠢᠬᠠᠨ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠡᠭᠡᠳᠦ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢᠨ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠯᠲᠡ᠎ᠶ᠋ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠡᠰᠡᠬᠦ᠎ᠶ᠋ᠢ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠤᠤ ?
1. 《 ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ 》 ᠶ᠋ᠢ ᠰᠣᠩᠭᠣᠬᠤ ᠂ ᠲᠦᠷ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠤᠰᠤ ᠪᠡᠷ ᠳᠠᠩᠰᠠᠨ᠎ᠠ᠋ᠴᠠ ᠬᠠᠰᠤᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ
2.《 ᠳᠠᠩᠰᠠᠨ᠎ᠠ᠋ᠴᠠ ᠬᠠᠰᠤᠬᠤ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ 》 ᠶ᠋ᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠳᠠᠷᠤᠢᠬᠠᠨ ᠳᠠᠩᠰᠠᠨ᠎ᠠ᠋ᠴᠠ ᠬᠠᠰᠤᠬᠤ
3. ᠪᠣᠯᠢᠬᠤ ᠶ᠋ᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>select</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>apply</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>log out to apply</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2268"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2347"/>
        <source>Turning on &apos;Eye Protection Mode&apos; will turn off &apos;Color Temperature&apos;. Continue turning it on?</source>
        <translation>&apos;ᠨᠢᠳᠦ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ&apos; ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠪᠡᠯ &apos;ᠥᠩᠭᠡᠨ ᠳᠤᠯᠠᠭᠠᠨ&apos; ᠢ᠋ ᠬᠠᠭᠠᠵᠤ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2349"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2376"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2374"/>
        <source>Turning on &apos;Color Temperature&apos; will turn off &apos;Eye Protection Mode&apos;. Continue turning it on?</source>
        <translation>&apos;ᠥᠩᠭᠡᠨ ᠳᠤᠯᠠᠭᠠᠨ&apos; ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠪᠡᠯ &apos; ᠨᠢᠳᠦ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ&apos; ᠢ᠋ ᠬᠠᠭᠠᠨ᠎ᠠ᠂ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2426"/>
        <source>All Day</source>
        <translation>ᠪᠦᠳᠦᠨ ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2713"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2727"/>
        <source>Brightness</source>
        <translation>ᠬᠡᠷᠡᠯᠳᠦᠴᠡ</translation>
        <extra-contents_path>/Display/Brightness</extra-contents_path>
    </message>
    <message>
        <source>screen zoom</source>
        <translation type="vanished">ᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠡᠯᠭᠡᠴᠡ</translation>
        <extra-contents_path>/display/screen zoom</extra-contents_path>
    </message>
    <message>
        <source>Mirror Display</source>
        <translation type="vanished">ᠳᠤᠯᠢᠳᠠᠰᠤ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
        <extra-contents_path>/display/unify output</extra-contents_path>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 29 seconds&lt;/font&gt;</source>
        <translation type="vanished">ᠳᠡᠯᠭᠡᠴᠡ %1 ᠢ᠋/ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠵᠠᠰᠠᠪᠠ᠂ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠤ ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt; ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ 29 ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ᠃&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Warnning</source>
        <translation type="vanished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>please insure at least one output!</source>
        <translation>ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠳᠡᠯᠭᠡᠴᠡ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1692"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1785"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1792"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2099"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1693"/>
        <source>Open time should be earlier than close time!</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ ᠴᠠᠭ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠠᠭᠠᠬᠤ ᠴᠠᠭ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1793"/>
        <source>Sorry, your configuration could not be applied.
Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU.</source>
        <translation>ᠠᠭᠤᠴᠢᠯᠠᠭᠠᠷᠠᠢ᠂ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ᠃</translation>
    </message>
    <message>
        <source>%1</source>
        <translation type="vanished">%1</translation>
    </message>
</context>
<context>
    <name>addShortcutDialog</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="88"/>
        <source>Exec</source>
        <translation>ᠫᠷᠣᠭ᠌ᠷᠠᠮ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="126"/>
        <source>Open</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="174"/>
        <source>Name</source>
        <translation>ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="248"/>
        <source>Key</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="222"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="305"/>
        <source>TextLabel</source>
        <translation>ᠲᠧᠺᠰᠲ ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="354"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="275"/>
        <source>Cancel</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="373"/>
        <source>Save</source>
        <translation>ᠰᠢᠷᠪᠢᠭᠡᠳ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="86"/>
        <source>Add Shortcut</source>
        <translation>ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="103"/>
        <source>Please enter a shortcut</source>
        <translation>ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="233"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ (*.desktop)</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="274"/>
        <source>select desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="329"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="348"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="357"/>
        <source>Invalid application</source>
        <translation>ᠲᠤᠰ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="331"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="344"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="353"/>
        <source>Shortcut conflict</source>
        <translation>ᠲᠤᠰ ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢᠨ ᠬᠠᠮᠰᠠᠯ ᠨᠢᠭᠡᠨᠳᠡ ᠡᠵᠡᠯᠡᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="333"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="346"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="355"/>
        <source>Invalid shortcut</source>
        <translation>ᠲᠤᠰ ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢᠨ ᠬᠠᠮᠰᠠᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="336"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="341"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="360"/>
        <source>Name repetition</source>
        <translation>ᠲᠤᠰ ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢᠨ ᠨᠠᠷᠡᠢᠳᠦᠯ ᠳᠠᠪᠬᠤᠴᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="364"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="542"/>
        <source>Shortcut cannot be empty</source>
        <translation>ᠳᠦᠳᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="546"/>
        <source>Name cannot be empty</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="556"/>
        <source>Desktop prohibits adding</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠨᠡᠮᠡᠬᠦ᠎ᠶ᠋ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>changeUserGroup</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="31"/>
        <source>user group</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠪᠦᠯᠦᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="120"/>
        <source>Group:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠪᠦᠯᠦᠭ ：</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="133"/>
        <source>GID:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ID：</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="147"/>
        <source>GNum:</source>
        <translation>ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠬᠡᠰᠢᠬᠦᠨ:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="191"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="617"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="194"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="618"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Tips</source>
        <translation>ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="572"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="656"/>
        <source>Invalid Id!</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ID!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="575"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="583"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="659"/>
        <source>OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="580"/>
        <source>Invalid Group Name!</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ !</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="615"/>
        <source>Whether delete the group: “%1” ?</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠦᠯᠦᠭ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ: “%1” ?</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="616"/>
        <source>which will make some file components in the file system invalid!</source>
        <translation>ᠡᠨᠡ ᠨᠢ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠡᠬᠢ ᠵᠠᠷᠢᠮ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>changtimedialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="32"/>
        <source>Dialog</source>
        <translation>ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="115"/>
        <source>current date</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ᠋ ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="200"/>
        <source>time</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="321"/>
        <source>year</source>
        <translation>ᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="398"/>
        <source>month</source>
        <translation>ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="472"/>
        <source>day</source>
        <translation>ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="574"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="593"/>
        <source>confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
</TS>
