<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>VERSION</source>
        <translation type="vanished">版本号</translation>
    </message>
</context>
<context>
    <name>BatteryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="257"/>
        <source>Model</source>
        <translation>電池型號</translation>
    </message>
    <message>
        <source>soc</source>
        <translation type="vanished">剩余电量</translation>
    </message>
    <message>
        <source>Estimated Service Time</source>
        <translation type="vanished">预计使用时间</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="160"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="196"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="245"/>
        <source>Battery</source>
        <translation>電池</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="249"/>
        <source>Serail Number</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="245"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="253"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="261"/>
        <source>State</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="265"/>
        <source>Percentage</source>
        <translation>電量</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="269"/>
        <source>Energy</source>
        <translation>容量</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="273"/>
        <source>Energy Full</source>
        <translation>完全充滿容量</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="277"/>
        <source>Time To Empty</source>
        <translation>預計使用時間</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="281"/>
        <source>Used Times</source>
        <translation>使用次數</translation>
    </message>
</context>
<context>
    <name>BluetoothInfo</name>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="63"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="159"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="195"/>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="224"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="229"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="248"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="399"/>
        <source>Enable </source>
        <translation>啟用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="232"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="240"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="245"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="402"/>
        <source>Disable </source>
        <translation>禁用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="282"/>
        <source>Bus Address</source>
        <translation>總線位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="286"/>
        <source>Function</source>
        <translation>功能</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="290"/>
        <source>Frequency</source>
        <translation>頻率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="294"/>
        <source>Configuration</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="298"/>
        <source>Type</source>
        <translation>設備類型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="302"/>
        <source>ID</source>
        <translation>ID號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="306"/>
        <source>Model</source>
        <translation>設備型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="310"/>
        <source>Resource</source>
        <translation>資源</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="314"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="318"/>
        <source>Version</source>
        <translation>設備版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="322"/>
        <source>Data Width</source>
        <translation>數據寬度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="326"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="330"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="334"/>
        <source>Speed</source>
        <translation>速率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="338"/>
        <source>Serial Number</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="342"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="346"/>
        <source>Link Mode</source>
        <translation>連接模式</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="350"/>
        <source>Link Policy</source>
        <translation>連接策略</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="354"/>
        <source>Capabilities</source>
        <translation>功能</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="358"/>
        <source>Bus</source>
        <translation>總線</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="362"/>
        <source>SCO MTU</source>
        <translation>SCO 最大傳輸單元</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="366"/>
        <source>ACL MTU</source>
        <translation>ACL 最大傳輸單元</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="370"/>
        <source>Packet Type</source>
        <translation>數據包類型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="374"/>
        <source>Features</source>
        <translation>特徵</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="412"/>
        <source>Bluetooth Disable</source>
        <translation>藍牙已禁用</translation>
    </message>
    <message>
        <source>Bluetooth Disable or  Device not exitst</source>
        <translation type="obsolete">蓝牙驱动已禁用或者缺失蓝牙设备</translation>
    </message>
    <message>
        <source>Bluetooth Disable </source>
        <translation type="obsolete">蓝牙已禁用</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="410"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="415"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>CDRomInfo</name>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="161"/>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="197"/>
        <source>CD-ROM</source>
        <translation>光驅</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="246"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="250"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="254"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="258"/>
        <source>Model</source>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="262"/>
        <source>Serail Number</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="266"/>
        <source>Bus Info</source>
        <translation>總線資訊</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="270"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="274"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="278"/>
        <source>Device Number</source>
        <translation>設備編號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="299"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>CameraInfo</name>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="157"/>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="193"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="242"/>
        <source>Name</source>
        <translation>設備名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="246"/>
        <source>Resolution</source>
        <translation>解析度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="250"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="254"/>
        <source>Model</source>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="258"/>
        <source>Interface</source>
        <translation>介面</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="262"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="266"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="270"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="274"/>
        <source>Bus Info</source>
        <translation>總線資訊</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="278"/>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
</context>
<context>
    <name>CpuFMPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="79"/>
        <source>Current CPU frequency</source>
        <translation>CPU當前頻率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="101"/>
        <source>Current average CPU core frequency</source>
        <translation>當前CPU核心平均主頻</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="111"/>
        <source>CPU FM Note: The CPU FM function has some risks, please use it carefully! After FM is completed, restarting will restore the default configuration!</source>
        <translation>CPU調頻說明：CPU調頻功能存在一定的風險，請謹慎操作！ 調頻完成重啟計算機將還原預設配置！</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="151"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>CpuFMSetWidget</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="87"/>
        <source>CPU Management Strategy</source>
        <translation>CPU管理策略</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="95"/>
        <source>performance</source>
        <translation>性能模式</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="102"/>
        <source>powersave</source>
        <translation>省電模式</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="109"/>
        <source>userspace</source>
        <translation>自訂</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="116"/>
        <source>schedutil</source>
        <translation>均衡模式</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="123"/>
        <source>ondemand</source>
        <translation>隨需應變模式</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="130"/>
        <source>conservative</source>
        <translation>保守模式</translation>
    </message>
</context>
<context>
    <name>DeviceMonitorPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="49"/>
        <source>The equipment is normal and the heat dissipation is good</source>
        <translation>設備正常，散熱情況良好</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="50"/>
        <source>Equipment temperature is high, please pay attention to heat dissipation</source>
        <translation>設備溫度較高，請注意散熱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="51"/>
        <source>Equipment temperature is abnormal, please pay attention to heat dissipation</source>
        <translation>設備溫度異常，請注意散熱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="176"/>
        <source>CPU Temp</source>
        <translation>CPU溫度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="182"/>
        <source>HARDDISK Temp</source>
        <translation>硬碟 溫度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="188"/>
        <source>GPU Temp</source>
        <translation>GPU溫度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="194"/>
        <source>DEV Temp</source>
        <translation>設備溫度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="223"/>
        <source>DEV Usage</source>
        <translation>設備使用率</translation>
    </message>
    <message>
        <source>BaseBoard Temp</source>
        <translation type="vanished">主板温度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="211"/>
        <source>CPU Usage</source>
        <translation>CPU使用率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="217"/>
        <source>Memory Usage</source>
        <translation>記憶體使用率</translation>
    </message>
</context>
<context>
    <name>DriveInfoPage</name>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="69"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="133"/>
        <source>Motherboard</source>
        <translation>主機板</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="137"/>
        <source>Graphics Card</source>
        <translation>顯卡</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="141"/>
        <source>Wired Network Card</source>
        <translation>有線網卡</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="145"/>
        <source>Sound Card</source>
        <translation>聲卡</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="149"/>
        <source>Wireless Network Card</source>
        <translation>無線網卡</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="153"/>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="159"/>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="163"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
</context>
<context>
    <name>DriveManage</name>
    <message>
        <location filename="../../plugins/drivemanage/drivemanage.cpp" line="43"/>
        <source>DriveManager</source>
        <translation>驅動管理</translation>
    </message>
</context>
<context>
    <name>FanInfo</name>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="161"/>
        <source>Fan</source>
        <translation>風扇</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="197"/>
        <source>Network Card</source>
        <translation>網卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="246"/>
        <source>Speed</source>
        <translation>轉速</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="267"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>GraphicsCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="73"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="171"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="207"/>
        <source>Graphics Card</source>
        <translation>顯卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="256"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="260"/>
        <source>SubSystem</source>
        <translation>子製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="264"/>
        <source>Name</source>
        <translation>設備名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="268"/>
        <source>IsDiscrete</source>
        <translation>獨立顯卡（是/否）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="272"/>
        <source>Memory</source>
        <translation>顯存</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="285"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="288"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="298"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="300"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="311"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="313"/>
        <source>Video Memory</source>
        <translation>顯存</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="319"/>
        <source>Memory Type</source>
        <translation>顯存類型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="323"/>
        <source>Model</source>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="327"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="331"/>
        <source>Bit Width</source>
        <translation>數據位寬</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="335"/>
        <source>Funtion</source>
        <translation>功能</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="339"/>
        <source>Clock</source>
        <translation>時鐘頻率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="343"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="347"/>
        <source>Dbus Info</source>
        <translation>總線資訊</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="351"/>
        <source>ID</source>
        <translation>ID號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="355"/>
        <source>Device</source>
        <translation>設備</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="359"/>
        <source>GDDR Capacity</source>
        <translation>GDDR容量</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="363"/>
        <source>EGL Version</source>
        <translation>EGL版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="367"/>
        <source>EGL Client APIs</source>
        <translation>EGL介面</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="371"/>
        <source>GL Version</source>
        <translation>GL版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="375"/>
        <source>GLSL Version</source>
        <translation>GLSL版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="396"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>HWMonitorInfo</name>
    <message>
        <location filename="../../plugins/hwmonitor/hwmonitor.cpp" line="32"/>
        <source>HardwareMonitor</source>
        <translation>硬體監測</translation>
    </message>
</context>
<context>
    <name>HWMonitorWidget</name>
    <message>
        <source>Device Monitor</source>
        <translation type="vanished">设备监测</translation>
    </message>
    <message>
        <source>CPU FM</source>
        <translation type="vanished">CPU调频</translation>
    </message>
</context>
<context>
    <name>HWParam</name>
    <message>
        <location filename="../../plugins/hwparam/hwparam.cpp" line="34"/>
        <source>HardwareParam</source>
        <translation>硬體參數</translation>
    </message>
</context>
<context>
    <name>HardDiskInfo</name>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="64"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="162"/>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="198"/>
        <source>Hard Disk Info</source>
        <translation>硬碟資訊</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="247"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="251"/>
        <source>Name</source>
        <translation>硬碟名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="261"/>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="263"/>
        <source>Capacity</source>
        <translation>容量</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="268"/>
        <source>Used Times</source>
        <translation>使用次數</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="272"/>
        <source>Interface</source>
        <translation>介面</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="278"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="280"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="282"/>
        <source>Main Disk</source>
        <translation>主硬碟（是/否）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="288"/>
        <source>SSD</source>
        <translation>固態</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="290"/>
        <source>HDD</source>
        <translation>機械</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="292"/>
        <source>Type</source>
        <translation>類型（固態/機械）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="296"/>
        <source>Serial Num</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="300"/>
        <source>Model</source>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="304"/>
        <source>Transfer Rate</source>
        <translation>數據傳輸率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="308"/>
        <source>Read Speed</source>
        <translation>磁碟讀取速度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="312"/>
        <source>Write Speed</source>
        <translation>磁碟寫入速度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="316"/>
        <source>Firmware Version</source>
        <translation>固件版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="320"/>
        <source>UFS Version</source>
        <translation>UFS 版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="341"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="194"/>
        <source>Manufacturer</source>
        <translation>整機製造商</translation>
    </message>
    <message>
        <source>MachineModel</source>
        <translation type="obsolete">整机型号</translation>
    </message>
    <message>
        <source>SerialNum</source>
        <translation type="obsolete">序列号</translation>
    </message>
    <message>
        <source>SystemBits</source>
        <translation type="obsolete">系统位数</translation>
    </message>
    <message>
        <source>KernelArch</source>
        <translation type="obsolete">内核架构</translation>
    </message>
    <message>
        <source>HostName</source>
        <translation type="obsolete">主机名</translation>
    </message>
    <message>
        <source>OSVersion</source>
        <translation type="obsolete">操作系统版本</translation>
    </message>
    <message>
        <source>KernelVersion</source>
        <translation type="obsolete">内核版本</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="71"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="200"/>
        <source>Machine Model</source>
        <translation>整機型號</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="206"/>
        <source>Serial Number</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="212"/>
        <source>System Bits</source>
        <translation>系統位數</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="218"/>
        <source>Kernel Arch</source>
        <translation>內核架構</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="224"/>
        <source>Host Name</source>
        <translation>主機名</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="252"/>
        <source>Kylin Linux Desktop V10 (SP1)</source>
        <translation>麒麟Linux桌面V10 （SP1）</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="257"/>
        <source>Kylin Linux Desktop (Touch Screen) V10 (SP1)</source>
        <translation>銀河麒麟桌面操作系統（大屏版）V10 （SP1）</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="259"/>
        <source>Kylin Linux Desktop (Tablet) V10 (SP1)</source>
        <translation>銀河麒麟桌面作業系統（平板版）V10 （SP1）</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="263"/>
        <source>OS Version</source>
        <translation>操作系統版本</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="270"/>
        <source>Kernel Version</source>
        <translation>內核版本</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="276"/>
        <source>Processor</source>
        <translation>處理器</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="309"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="313"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="342"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="346"/>
        <source>Memory</source>
        <translation>記憶體</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="354"/>
        <source>Mother Board</source>
        <translation>主機板</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="367"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="372"/>
        <source>Hard Disk</source>
        <translation>硬碟</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="379"/>
        <source>Graphics Card</source>
        <translation>顯卡</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="391"/>
        <source>Network Card</source>
        <translation>網卡</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="397"/>
        <source>Sound Card</source>
        <translation>聲卡</translation>
    </message>
    <message>
        <source>MainBoard</source>
        <translation type="obsolete">主板</translation>
    </message>
    <message>
        <source>HardDisk</source>
        <translation type="obsolete">硬盘</translation>
    </message>
    <message>
        <source>GraphicsCard</source>
        <translation type="obsolete">显卡</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="385"/>
        <source>Monitor</source>
        <translation>顯示器</translation>
    </message>
    <message>
        <source>NetworkCard</source>
        <translation type="obsolete">网卡</translation>
    </message>
    <message>
        <source>SoundCard</source>
        <translation type="obsolete">声卡</translation>
    </message>
</context>
<context>
    <name>KAAboutDialog</name>
    <message>
        <location filename="../kaaboutdialog.cpp" line="117"/>
        <location filename="../kaaboutdialog.cpp" line="143"/>
        <source>ToolKit</source>
        <translation>工具箱</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="148"/>
        <source>VERSION:  </source>
        <translation>版本號：  </translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="153"/>
        <source>ToolKit provides some extended functions and users can query the hardware details of the current computer.</source>
        <translation>工具箱是由麒麟團隊開發的一款系統輔助軟體。 主要幫助使用者查看當前計算機的硬體詳細資訊、設備狀態等。 目前已具備了整機資訊，硬體參數，硬體監測等功能。</translation>
    </message>
    <message>
        <source>ToolKit is a powerful system supporting software which is developed by Kylin team. Mainly for the naive user, it can help users manage the system. At present, It provides system junk scanning and cleaning, viewing the system hardware and software information, task manager, and some other functions.</source>
        <translation type="obsolete">工具箱是由麒麟团队开发的一款功能强大的系统辅助软件。主要面向初级用户，能够帮助用户对系统进行管理。目前已具备了系统垃圾扫描与清理、系统软硬件信息查看、系统全方位定制、任务管理器等功能。</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="177"/>
        <source>Service &amp; Support :</source>
        <translation>服務和支援：</translation>
    </message>
</context>
<context>
    <name>KABaseInfoPage</name>
    <message>
        <location filename="../../plugins/hwparam/kabaseinfopage.cpp" line="94"/>
        <source>kylin-assistant</source>
        <translation>工具箱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/kabaseinfopage.cpp" line="97"/>
        <source>kylin-assistant info</source>
        <translation>工具箱資訊</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/kabaseinfopage.cpp" line="98"/>
        <source>Disable failed</source>
        <translation>禁用失敗</translation>
    </message>
</context>
<context>
    <name>KALabel</name>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
</context>
<context>
    <name>KAUsageItem</name>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="64"/>
        <source>Used</source>
        <translation>已用</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="66"/>
        <source>Left</source>
        <translation>可用</translation>
    </message>
</context>
<context>
    <name>KDriveInfoItem</name>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
</context>
<context>
    <name>KInfoListItem</name>
    <message>
        <source>Copy</source>
        <translation type="obsolete">复制</translation>
    </message>
</context>
<context>
    <name>KLeftWidget</name>
    <message>
        <location filename="../kleftwidget.cpp" line="64"/>
        <source>ToolKit</source>
        <translation>工具箱</translation>
    </message>
</context>
<context>
    <name>KRightWidget</name>
    <message>
        <source>menu</source>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="59"/>
        <source>options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="65"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="71"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="82"/>
        <location filename="../krightwidget.cpp" line="96"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="83"/>
        <location filename="../krightwidget.cpp" line="90"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="84"/>
        <location filename="../krightwidget.cpp" line="107"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="155"/>
        <location filename="../krightwidget.cpp" line="159"/>
        <source>ToolKit</source>
        <translation>工具箱</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="161"/>
        <source>Version：</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="162"/>
        <source>Service &amp; Support: &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>&lt;p&gt;服務與支援： &lt;a style=&apos;color： #464646;&apos; href=&apos;mailto：//support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;ToolKit is a powerful system supporting software which is developed by Kylin team. Mainly for the naive user, it can help users manage the system. At present, It provides system junk scanning and cleaning, viewing the system hardware and software information, task manager, and some other functions.&lt;/p&gt;</source>
        <translation type="vanished"> &lt;p&gt;工具箱是由麒麟团队开发的一款功能强大的系统辅助软件。主要面向初级用户，能够帮助用户对系统进行管理。目前已具备了系统垃圾扫描与清理、系统软硬件信息查看、系统全方位定制、任务管理器等功能。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Service &amp; Support : &lt;a style=&apos;color: black;&apos; href=&apos;mailto:support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;服务与支持: &lt;a style=&apos;color: black;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>KeyboardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="161"/>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="197"/>
        <source>Keyboard</source>
        <translation>鍵盤</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="246"/>
        <source>Name</source>
        <translation>設備名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="254"/>
        <source>Model</source>
        <translation>設備型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="258"/>
        <source>Manufacurer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="262"/>
        <source>Interface</source>
        <translation>介面</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="266"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="270"/>
        <source>Device Address</source>
        <translation>設備位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="291"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>LoadWidget</name>
    <message>
        <location filename="../loadwidget.cpp" line="81"/>
        <source>Enabling, please wait</source>
        <translation>正在啟用，請稍等</translation>
    </message>
    <message>
        <location filename="../loadwidget.cpp" line="85"/>
        <source>Disabling, please wait</source>
        <translation>正在禁用，請稍等</translation>
    </message>
    <message>
        <location filename="../loadwidget.cpp" line="125"/>
        <source>VERSION</source>
        <translation>版本號</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="../../CommonControl/loadingwidget.cpp" line="58"/>
        <source>Scanning, please wait</source>
        <translation>正在掃描，請稍等</translation>
    </message>
</context>
<context>
    <name>MachineInfo</name>
    <message>
        <location filename="../../plugins/machineinfo/machineinfo.cpp" line="36"/>
        <source>MachineInfo</source>
        <translation>整機資訊</translation>
    </message>
</context>
<context>
    <name>MainInfoPage</name>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="205"/>
        <source>Processor</source>
        <translation>處理器</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="225"/>
        <source>Memory</source>
        <translation>記憶體</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="245"/>
        <source>Graphics Card</source>
        <translation>顯卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="265"/>
        <source>Motherboard</source>
        <translation>主機板</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="285"/>
        <source>Network Card</source>
        <translation>網卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="305"/>
        <source>Hard Disk</source>
        <translation>硬碟</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="326"/>
        <source>Monitor</source>
        <translation>顯示器</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="346"/>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="363"/>
        <source>Sound Card</source>
        <translation>聲卡</translation>
    </message>
    <message>
        <source>Voice Card</source>
        <translation type="obsolete">声卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="384"/>
        <source>Keyboard</source>
        <translation>鍵盤</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="404"/>
        <source>Mouse</source>
        <translation>滑鼠</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="424"/>
        <source>Battery</source>
        <translation>電池</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="444"/>
        <source>CD-ROM</source>
        <translation>光驅</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="464"/>
        <source>Camera</source>
        <translation>攝像頭</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="484"/>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="504"/>
        <source>Fan</source>
        <translation>風扇</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="46"/>
        <source>ToolKit</source>
        <translation>工具箱</translation>
    </message>
    <message>
        <source>MachineInfo</source>
        <translation type="obsolete">整机信息</translation>
    </message>
    <message>
        <source>HardwareMonitor</source>
        <translation type="obsolete">硬件监测</translation>
    </message>
</context>
<context>
    <name>MemoryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="64"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="160"/>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="196"/>
        <source>Memory Info</source>
        <translation>記憶體資訊</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="245"/>
        <source>Slot</source>
        <translation>插槽</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="249"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="253"/>
        <source>Freq</source>
        <translation>頻率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="257"/>
        <source>Bus Width</source>
        <translation>總位寬</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="267"/>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="269"/>
        <source>Total Capacity</source>
        <translation>總容量</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="274"/>
        <source>Used Capacity</source>
        <translation>已用容量</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="278"/>
        <source>Serail Num</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="282"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="286"/>
        <source>Data Width</source>
        <translation>數據位寬</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="290"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="294"/>
        <source>Speed</source>
        <translation>速率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="298"/>
        <source>Config Speed</source>
        <translation>配置速率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="302"/>
        <source>Channel</source>
        <translation>插槽號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="306"/>
        <source>Array Handle</source>
        <translation>數位程式</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="310"/>
        <source>Part Number</source>
        <translation>產品型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="314"/>
        <source>Physical ID</source>
        <translation>物理ID</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="318"/>
        <source>Model</source>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="339"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>MonitorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="67"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="224"/>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="260"/>
        <source>Monitor</source>
        <translation>顯示器</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="309"/>
        <source>Manufacturer</source>
        <translation>顯示器廠商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="313"/>
        <source>Name</source>
        <translation>顯示器型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="317"/>
        <source>Size</source>
        <translation>螢幕尺寸/英寸</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="321"/>
        <source>Ratio</source>
        <translation>圖像高寬比</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="325"/>
        <source>Resolution</source>
        <translation>當前解析度</translation>
    </message>
    <message>
        <source>MAX Resolution</source>
        <translation type="vanished">最大可用分辨率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="329"/>
        <source>Optimal Resolution</source>
        <translation>最優可用解析度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="335"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="337"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="339"/>
        <source>Main Screen</source>
        <translation>主顯示器（是/否）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="343"/>
        <source>Interface</source>
        <translation>介面</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="347"/>
        <source>Visible Area</source>
        <translation>可視面積</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="351"/>
        <source>Product Week</source>
        <translation>生產日期/周</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="355"/>
        <source>Product Year</source>
        <translation>生產日期/年</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="359"/>
        <source>Gamma</source>
        <translation>伽馬值</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="380"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>MotherBoardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="167"/>
        <source>Name</source>
        <translation>主機板名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="171"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="179"/>
        <source>Chipset</source>
        <translation>晶片組</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="183"/>
        <source>Serial Num</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="226"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="175"/>
        <source>Version</source>
        <translation>主機板版本</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="66"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="202"/>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="204"/>
        <source>Publish Date</source>
        <translation>發佈日期</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="209"/>
        <source>BIOS Manufacturer</source>
        <translation>BIOS製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="213"/>
        <source>BIOS Version</source>
        <translation>BIOS版本</translation>
    </message>
</context>
<context>
    <name>MouseInfo</name>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="162"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="198"/>
        <source>Mouse</source>
        <translation>滑鼠</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="63"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="221"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="251"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="253"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="286"/>
        <source>Name</source>
        <translation>設備名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="256"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="261"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="339"/>
        <source>Enable </source>
        <translation>啟用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="271"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="276"/>
        <source>Disabled </source>
        <translation>禁用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="294"/>
        <source>Model</source>
        <translation>設備型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="299"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="301"/>
        <source>Manufacurer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="306"/>
        <source>Interface</source>
        <translation>介面</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="310"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="314"/>
        <source>Device Address</source>
        <translation>設備位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="345"/>
        <source>Mouse device disabled</source>
        <translation>滑鼠已禁用</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="347"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>NetCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="65"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="162"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="198"/>
        <source>Network Card</source>
        <translation>網卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="230"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="236"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="289"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="407"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="411"/>
        <source>Enable </source>
        <translation>啟用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="277"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="279"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="297"/>
        <source>Name</source>
        <translation>網卡名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="239"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="281"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="286"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="414"/>
        <source>Disable </source>
        <translation>禁用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="303"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="305"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="307"/>
        <source>Type</source>
        <translation>網卡類型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="303"/>
        <source>Wired</source>
        <translation>有線</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="305"/>
        <source>Wireless</source>
        <translation>無線</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="312"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="316"/>
        <source>Bus Address</source>
        <translation>總線位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="320"/>
        <source>MAC</source>
        <translation>MAC位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="324"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="328"/>
        <source>Link Speed</source>
        <translation>連接速度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="332"/>
        <source>MTU</source>
        <translation>MTU</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="336"/>
        <source>IP</source>
        <translation>IP位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="340"/>
        <source>Model</source>
        <translation>型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="344"/>
        <source>Subnet Mask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="348"/>
        <source>Gateway</source>
        <translation>閘道位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="352"/>
        <source>DNS Server</source>
        <translation>DNS伺服器</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="362"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="364"/>
        <source>Bytes Received</source>
        <translation>已接收位元組</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="375"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="377"/>
        <source>Bytes Sent</source>
        <translation>已發送位元組</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="426"/>
        <source>Network card device is disabled</source>
        <translation>網卡設備已禁用</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="428"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>ProcessorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="156"/>
        <source>Processor</source>
        <translation>處理器</translation>
    </message>
    <message>
        <source>Number Of Cores</source>
        <translation type="vanished">核心数目</translation>
    </message>
    <message>
        <source>Number Of Threads</source>
        <translation type="vanished">线程数</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="180"/>
        <source>Slot</source>
        <translation>插槽</translation>
    </message>
    <message>
        <source>Fref</source>
        <translation type="vanished">基准频率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="63"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="160"/>
        <source>Manufacturer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="164"/>
        <source>Architecture</source>
        <translation>架構</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="168"/>
        <source>Core Num</source>
        <translation>核心數</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="172"/>
        <source>Core Online Num</source>
        <translation>在線核心數</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="176"/>
        <source>Thread</source>
        <translation>每核線程數</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="184"/>
        <source>Max Frequency</source>
        <translation>最大主頻</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="188"/>
        <source>Frequency</source>
        <translation>頻率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="192"/>
        <source>L1 Cache</source>
        <translation>一級緩存</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="202"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="204"/>
        <source>L1d Cache</source>
        <translation>一級快取（資料）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="215"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="217"/>
        <source>L1i Cache</source>
        <translation>一級快取（指令）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="228"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="230"/>
        <source>L2 Cache</source>
        <translation>二級緩存</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="241"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="243"/>
        <source>L3 Cache</source>
        <translation>三級緩存</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="248"/>
        <source>Instruction Set</source>
        <translation>指令集</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="252"/>
        <source>EXT Instruction Set</source>
        <translation>擴展指令集</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="256"/>
        <source>Used</source>
        <translation>使用率</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>app is already running!</source>
        <translation>程式已經在運行中！</translation>
    </message>
</context>
<context>
    <name>VoiceCardInfo</name>
    <message>
        <source>Voice Card</source>
        <translation type="obsolete">声卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="66"/>
        <source>CopyAll</source>
        <translation>複製全部</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="179"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="215"/>
        <source>Sound Card</source>
        <translation>聲卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="329"/>
        <source>Bus Address</source>
        <translation>總線位址</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="337"/>
        <source>Name</source>
        <translation>聲卡名稱</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="333"/>
        <source>Drive</source>
        <translation>聲卡驅動</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="293"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="308"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="325"/>
        <source>Model</source>
        <translation>聲卡型號</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="247"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="251"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="317"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="381"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="385"/>
        <source>Enable </source>
        <translation>啟用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="254"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="309"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="314"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="388"/>
        <source>Disabled </source>
        <translation>禁用 </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="341"/>
        <source>Manufacurer</source>
        <translation>製造商</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="345"/>
        <source>Clock</source>
        <translation>時鐘頻率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="349"/>
        <source>Bit Width</source>
        <translation>數據位寬</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="400"/>
        <source>Sound card device is disabled</source>
        <translation>聲卡設備已禁用</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="402"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>設備不存在或者獲取到的設備資訊為空</translation>
    </message>
</context>
<context>
    <name>processorinfo</name>
    <message>
        <source>Processor</source>
        <translation type="obsolete">处理器</translation>
    </message>
    <message>
        <source>Number Of Cores</source>
        <translation type="obsolete">核心数目</translation>
    </message>
    <message>
        <source>Number Of Threads</source>
        <translation type="obsolete">线程数</translation>
    </message>
    <message>
        <source>Slot</source>
        <translation type="obsolete">插槽</translation>
    </message>
    <message>
        <source>Fref</source>
        <translation type="obsolete">基准频率</translation>
    </message>
    <message>
        <source>L1 Cache</source>
        <translation type="obsolete">一级缓存</translation>
    </message>
    <message>
        <source>L2 Cache</source>
        <translation type="obsolete">二级缓存</translation>
    </message>
    <message>
        <source>L3 Cache</source>
        <translation type="obsolete">三级缓存</translation>
    </message>
    <message>
        <source>Instruction Set</source>
        <translation type="obsolete">指令集</translation>
    </message>
    <message>
        <source>Used</source>
        <translation type="obsolete">使用率</translation>
    </message>
</context>
</TS>
