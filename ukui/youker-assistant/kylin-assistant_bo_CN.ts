<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BatteryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="147"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="173"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="222"/>
        <source>Battery</source>
        <translation>གློག་སྨན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="226"/>
        <source>Serail Number</source>
        <translation>གོ་རིམ་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="222"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="230"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="234"/>
        <source>Model</source>
        <translation>གློག་སྨན་གྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="238"/>
        <source>State</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="242"/>
        <source>Percentage</source>
        <translation>གློག་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="246"/>
        <source>Energy</source>
        <translation>ནུས་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="250"/>
        <source>Energy Full</source>
        <translation>ནུས་ཚད་ཀྱིས་ཁེངས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="254"/>
        <source>Time To Empty</source>
        <translation>སྟོང་པར་ལུས་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="258"/>
        <source>Used Times</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་པའི་ཐེངས་གྲངས།</translation>
    </message>
</context>
<context>
    <name>BluetoothInfo</name>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="145"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="171"/>
        <source>Bluetooth</source>
        <translation>སོ་སྔོན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="220"/>
        <source>Bus Address</source>
        <translation>སྤྱིའི་གློག་སྐུད་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="224"/>
        <source>Function</source>
        <translation>བྱེད་ནུས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="228"/>
        <source>Frequency</source>
        <translation>ཐེངས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="232"/>
        <source>Configuration</source>
        <translation>བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="236"/>
        <source>Type</source>
        <translation>སྒྲིག་ཆས་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="240"/>
        <source>ID</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="244"/>
        <source>Model</source>
        <translation>སྒྲིག་ཆས་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="248"/>
        <source>Resource</source>
        <translation>ཐོན་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="252"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="256"/>
        <source>Version</source>
        <translation>སྒྲིག་ཆས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="260"/>
        <source>Data Width</source>
        <translation>གཞི་གྲངས་ཀྱི་ཞེང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="264"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="268"/>
        <source>Driver</source>
        <translation>སྐུལ་འདེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="272"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="276"/>
        <source>Serial Number</source>
        <translation>གོ་རིམ་གྱི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="280"/>
        <source>Address</source>
        <translation>སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="284"/>
        <source>Link Mode</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="288"/>
        <source>Link Policy</source>
        <translation>སྦྲེལ་མཐུད་སྲིད་ཇུས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="292"/>
        <source>Capabilities</source>
        <translation>ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="296"/>
        <source>Bus</source>
        <translation>སྤྱིའི་གློག་སྐུད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="300"/>
        <source>SCO MTU</source>
        <translation>SCO ཆེས་ཆེ་བའི་བརྒྱུད་གཏོང་སྡེ་ཚན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="304"/>
        <source>ACL MTU</source>
        <translation>ACL ཆེས་ཆེ་བའི་བརྒྱུད་གཏོང་སྡེ་ཚན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="308"/>
        <source>Packet Type</source>
        <translation>ཁུག་མའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="312"/>
        <source>Features</source>
        <translation>ཁྱད་ཆོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="333"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>CDRomInfo</name>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="174"/>
        <source>CD-ROM</source>
        <translation>འོད་ཁུལ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="223"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="227"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="231"/>
        <source>Version</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="235"/>
        <source>Model</source>
        <translation>དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="239"/>
        <source>Serail Number</source>
        <translation>གོ་རིམ་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="243"/>
        <source>Bus Info</source>
        <translation>སྤྱིའི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="247"/>
        <source>Driver</source>
        <translation>སྐུལ་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="251"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="255"/>
        <source>Device Number</source>
        <translation>སྒྲིག་ཆས་ཀྱི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="276"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>CameraInfo</name>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="144"/>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="170"/>
        <source>Camera</source>
        <translation>པར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="219"/>
        <source>Name</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="223"/>
        <source>Resolution</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="227"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="231"/>
        <source>Model</source>
        <translation>དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="235"/>
        <source>Interface</source>
        <translation>འབྲེལ་མཐུད་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="239"/>
        <source>Driver</source>
        <translation>སྐུལ་འདེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="243"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="247"/>
        <source>Version</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="251"/>
        <source>Bus Info</source>
        <translation>སྤྱིའི་གློག་སྐུད་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="255"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
</context>
<context>
    <name>CpuFMPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="79"/>
        <source>Current CPU frequency</source>
        <translation>CPUམིག་སྔའི་འགུལ་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="101"/>
        <source>Current average CPU core frequency</source>
        <translation>མིག་སྔར་ཆ་སྙོམས་ཀྱི་CPUལ་ལྟེ་བའི་ཟློས་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="111"/>
        <source>CPU FM Note: The CPU FM function has some risks, please use it carefully! After FM is completed, restarting will restore the default configuration!</source>
        <translation>CPU FM Note: CPU FMནི་ཉེན་ཁ་ངེས་ཅན་ཞིག་ཡོད་པས་ཁྱེད་ཀྱིས་ནན་ཏན་གྱིས་བཀོལ་སྤྱོད་བྱེད་རོགས། FMལེགས་འགྲུབ་བྱུང་རྗེས་ཡང་བསྐྱར་བསྐྱར་དུ་བཀོད་སྒྲིག་བྱས་ན་སྔོན་ཆད་ཀྱི་བཀོད་སྒྲིག་སླར་གསོ་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="151"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>CpuFMSetWidget</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="87"/>
        <source>CPU Management Strategy</source>
        <translation>CPU དོ་དམ་འཐབ་ཇུས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="95"/>
        <source>performance</source>
        <translation>འཁྲབ་སྟོན་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="102"/>
        <source>powersave</source>
        <translation>གློག་གྲོན་ཆུང་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="109"/>
        <source>userspace</source>
        <translation>མཚན་ཉིད་རང་འཇོག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="116"/>
        <source>schedutil</source>
        <translation>དོ་མཉམ་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="123"/>
        <source>ondemand</source>
        <translation>དགོས་མཁོ་དང་བསྟུན་ནས་འགྱུར་བའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="130"/>
        <source>conservative</source>
        <translation>བག་འཁུམ་གྱི་རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>DeviceMonitorPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="49"/>
        <source>The equipment is normal and the heat dissipation is good</source>
        <translation>སྒྲིག་ཆས་རྒྱུན་ལྡན་ཡིན་པ་དང་། ཚ་ནུས་མེད་པར་གྱུར་པ་ཧ་ཅང་བཟང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="50"/>
        <source>Equipment temperature is high, please pay attention to heat dissipation</source>
        <translation>སྒྲིག་ཆས་ཀྱི་དྲོད་ཚད་མཐོ་བས་ཚ་བ་སེལ་རྒྱུར་མཉམ་འཇོག་བྱོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="51"/>
        <source>Equipment temperature is abnormal, please pay attention to heat dissipation</source>
        <translation>སྒྲིག་ཆས་ཀྱི་དྲོད་ཚད་རྒྱུན་ལྡན་མིན་པས་ཚ་བ་སེལ་རྒྱུར་མཉམ་འཇོག་བྱོས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="176"/>
        <source>CPU Temp</source>
        <translation>CPU དྲོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="182"/>
        <source>HARDDISK Temp</source>
        <translation>སྲ་ཞིང་མཁྲེགས་པའི་དྲོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="188"/>
        <source>GPU Temp</source>
        <translation>GPU དྲོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="194"/>
        <source>DEV Temp</source>
        <translation>སྒྲིག་ཆས་ཀྱི་དྲོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="223"/>
        <source>DEV Usage</source>
        <translation>སྒྲིག་ཆས་ཀྱི་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="211"/>
        <source>CPU Usage</source>
        <translation>CPUབཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="217"/>
        <source>Memory Usage</source>
        <translation>ནང་ཉིང་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>DriveInfoPage</name>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="69"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="124"/>
        <source>Motherboard</source>
        <translation>པང་ལེབ།</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="128"/>
        <source>Graphics Card</source>
        <translation>བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="132"/>
        <source>Wired Network Card</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="136"/>
        <source>Sound Card</source>
        <translation>སྒྲའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="140"/>
        <source>Wireless Network Card</source>
        <translation>སྐུད་མེད་དྲ་བའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="146"/>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="150"/>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
    </message>
</context>
<context>
    <name>DriveManage</name>
    <message>
        <location filename="../../plugins/drivemanage/drivemanage.cpp" line="43"/>
        <source>DriveManager</source>
        <translation>སྐུལ་འདེད་དོ་དམ་ཆས།</translation>
    </message>
</context>
<context>
    <name>FanInfo</name>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="174"/>
        <source>Fan</source>
        <translation>རླུང་གཡབ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="223"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="244"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>GraphicsCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="71"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="156"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="182"/>
        <source>Graphics Card</source>
        <translation>བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="231"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="235"/>
        <source>SubSystem</source>
        <translation>ཡན་ལག་མ་ལག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="239"/>
        <source>Name</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="243"/>
        <source>IsDiscrete</source>
        <translation>ཁེར་རྐྱང་གི་བྱང་བུ།(ཡིན་ཡང་ན་མིན།)</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="247"/>
        <source>Memory</source>
        <translation>འཆར་གསོག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="260"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="263"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="267"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="271"/>
        <source>Video Memory</source>
        <translation>འཆར་གསོག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="276"/>
        <source>Memory Type</source>
        <translation>འཆར་གསོག་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="280"/>
        <source>Model</source>
        <translation>དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="284"/>
        <source>Version</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="288"/>
        <source>Bit Width</source>
        <translation>གཞི་གྲངས་ཀྱི་ཞེང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="292"/>
        <source>Funtion</source>
        <translation>བྱེད་ནུས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="296"/>
        <source>Clock</source>
        <translation>ཆུ་ཚོད་འཁོར་ལོ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="300"/>
        <source>Driver</source>
        <translation>སྐུལ་འདེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="304"/>
        <source>Dbus Info</source>
        <translation>སྤྱིའི་གློག་སྐུད་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="308"/>
        <source>ID</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར་གྱི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="312"/>
        <source>Device</source>
        <translation>སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="316"/>
        <source>GDDR Capacity</source>
        <translation>འཆར་གསོག་ཤོང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="320"/>
        <source>EGL Version</source>
        <translation>EGLཔར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="324"/>
        <source>EGL Client APIs</source>
        <translation>EGL སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="328"/>
        <source>GL Version</source>
        <translation>GLཔར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="332"/>
        <source>GLSL Version</source>
        <translation>GLSLཔར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="353"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>HWMonitorInfo</name>
    <message>
        <location filename="../../plugins/hwmonitor/hwmonitor.cpp" line="32"/>
        <source>HardwareMonitor</source>
        <translation>མཁྲེགས་ཆས་ཚོད་འཛིན་བྱེད་མཁན།</translation>
    </message>
</context>
<context>
    <name>HWParam</name>
    <message>
        <location filename="../../plugins/hwparam/hwparam.cpp" line="34"/>
        <source>HardwareParam</source>
        <translation>མཁྲེགས་ཆས་ཀྱི་གཞི་གྲངས།</translation>
    </message>
</context>
<context>
    <name>HardDiskInfo</name>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="147"/>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="173"/>
        <source>Hard Disk Info</source>
        <translation>སྲ་ཞིང་མཁྲེགས་པའི་ཁབ་ལེན་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="222"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="226"/>
        <source>Name</source>
        <translation>མཁྲེགས་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="230"/>
        <source>Capacity</source>
        <translation>ཤོང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="234"/>
        <source>Used Times</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་པའི་ཐེངས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="238"/>
        <source>Interface</source>
        <translation>འབྲེལ་མཐུད་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="244"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="246"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="248"/>
        <source>Main Disk</source>
        <translation>གཙོ་བོ་ཁབ་ལེན་འཁོར་ལོ།(ཡིན་ཡང་ན་མིན)</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="254"/>
        <source>SSD</source>
        <translation>སྲ་རྣམ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="256"/>
        <source>HDD</source>
        <translation>འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="258"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས།(སྲ་རྣམ་ཡང་ན་འཕྲུལ་ཆས)</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="262"/>
        <source>Serial Num</source>
        <translation>གོ་རིམ་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="266"/>
        <source>Model</source>
        <translation>དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="270"/>
        <source>Transfer Rate</source>
        <translation>གཞི་གྲངས་བརྒྱུད་སྤྲོད་བྱེད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="274"/>
        <source>Read Speed</source>
        <translation>སྡུད་སྡེར་ཀློག་ལེན་གྱི་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="278"/>
        <source>Write Speed</source>
        <translation>སྡུད་སྡེར་འབྲི་བའི་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="282"/>
        <source>Firmware Version</source>
        <translation>སྲ་མཁྲེགས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="303"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="69"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="183"/>
        <source>Manufacturer</source>
        <translation>འཕྲུལ་ཆས་བཟོ་སྐྲུན་ཚོང་ཁང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="189"/>
        <source>Machine Model</source>
        <translation>འཕྲུལ་འཁོར་གྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="195"/>
        <source>Serial Number</source>
        <translation>གོ་རིམ་གྱི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="201"/>
        <source>System Bits</source>
        <translation>མ་ལག་གི་གནས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="207"/>
        <source>Kernel Arch</source>
        <translation>ནང་ཉིང་སྒྲོམ་གཞིའི་སྒྲིག་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="213"/>
        <source>Host Name</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="219"/>
        <source>OS Version</source>
        <translation>བཀོད་སྤྱོད་མ་ལག་གི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="225"/>
        <source>Kernel Version</source>
        <translation>ནང་སྙིང་གི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="231"/>
        <source>Processor</source>
        <translation>ལས་སྣོན་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="237"/>
        <source>Memory</source>
        <translation>ནང་ཉིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="243"/>
        <source>Mother Board</source>
        <translation>གཙོ་པང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="249"/>
        <source>Hard Disk</source>
        <translation>སྲ་ཞིང་མཁྲེགས་པའི་སྡེར་མ།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="255"/>
        <source>Graphics Card</source>
        <translation>འཆར་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="267"/>
        <source>Network Card</source>
        <translation>དྲ་རྒྱའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="273"/>
        <source>Sound Card</source>
        <translation>སྒྲའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="261"/>
        <source>Monitor</source>
        <translation>འཆར་ངོས་ཆས།</translation>
    </message>
</context>
<context>
    <name>KAAboutDialog</name>
    <message>
        <location filename="../kaaboutdialog.cpp" line="117"/>
        <location filename="../kaaboutdialog.cpp" line="143"/>
        <source>ToolKit</source>
        <translation>ཡོ་བྱད་ཀྱི་སྒམ།</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="148"/>
        <source>VERSION:  </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།  </translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="153"/>
        <source>ToolKit provides some extended functions and users can query the hardware details of the current computer.</source>
        <translation>ལག་ཆའི་སྒམ་ནི་ཆི་ལིན་ཚོགས་པས་གསར་སྤེལ་བྱས་པའི་མ་ལག་གི་རམ་འདེགས་མཉེན་ཆས་ཤིག་ཡིན།གཙོ་བོ་སྤྱོད་མཁན་ལ་མིག་སྔའི་རྩིས་འཁོར་གྱི་མཁྲེགས་ཆས་ཀྱི་ཞིབ་ཕྲའི་ཆ་འཕྲིན་དང་སྒྲིག་ཆས་ཀྱི་རྣམ་པ་སོགས་ལ་ལྟ་ཞིབ་བྱེད་རོགས་བྱས་པ་རེད།མིག་སྔར་འཕྲུལ་ཆས་ཧྲིལ་པོའི་ཆ་འཕྲིན་དང་།མཁྲེགས་ཆས་ཞུགས་གྲངས།་མཁྲེགས་ཆས་ལྟ་ཞིབ་ཚད་ལེན་སོགས་ཀྱི་ནུས་པ་འཛོམས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="177"/>
        <source>Service &amp; Support :</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར།</translation>
    </message>
</context>
<context>
    <name>KAUsageItem</name>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="64"/>
        <source>Used</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="66"/>
        <source>Left</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་ཆོག་པ།</translation>
    </message>
</context>
<context>
    <name>KLeftWidget</name>
    <message>
        <location filename="../kleftwidget.cpp" line="64"/>
        <source>ToolKit</source>
        <translation>ཡོ་བྱད་ཀྱི་སྒམ།</translation>
    </message>
</context>
<context>
    <name>KRightWidget</name>
    <message>
        <location filename="../krightwidget.cpp" line="52"/>
        <source>menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="58"/>
        <source>minimize</source>
        <translation>ཆེས་ཆུང་དུ་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="64"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="75"/>
        <location filename="../krightwidget.cpp" line="89"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="76"/>
        <location filename="../krightwidget.cpp" line="83"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="77"/>
        <location filename="../krightwidget.cpp" line="96"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>KeyboardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="174"/>
        <source>Keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="223"/>
        <source>Name</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="231"/>
        <source>Model</source>
        <translation>སྒྲིག་ཆས་ཀྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="235"/>
        <source>Manufacurer</source>
        <translation>བཟོ་སྐྲུན་ཚོང་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="239"/>
        <source>Interface</source>
        <translation>འབྲེལ་མཐུད་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="243"/>
        <source>Driver</source>
        <translation>སྐུལ་འདེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="247"/>
        <source>Device Address</source>
        <translation>སྒྲིག་ཆས་ཀྱི་སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="268"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="../../CommonControl/loadingwidget.cpp" line="51"/>
        <source>Scanning, please wait</source>
        <translation>ཞིབ་བཤེར་བྱེད་བཞིན་པའི་སྒང་ཡིན།ཅུང་ཙམ་སྒུག་རོགས།</translation>
    </message>
</context>
<context>
    <name>MachineInfo</name>
    <message>
        <location filename="../../plugins/machineinfo/machineinfo.cpp" line="36"/>
        <source>MachineInfo</source>
        <translation>འཕྲུལ་ཆས་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>MainInfoPage</name>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="205"/>
        <source>Processor</source>
        <translation>ལས་སྣོན་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="225"/>
        <source>Memory</source>
        <translation>ནང་ཉིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="245"/>
        <source>Graphics Card</source>
        <translation>འཆར་བྱང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="265"/>
        <source>Motherboard</source>
        <translation>པང་ལེབ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="285"/>
        <source>Network Card</source>
        <translation>དྲ་རྒྱའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="305"/>
        <source>Hard Disk</source>
        <translation>སྲ་ཞིང་མཁྲེགས་པའི་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="326"/>
        <source>Monitor</source>
        <translation>འཆར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="346"/>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="363"/>
        <source>Sound Card</source>
        <translation>སྒྲའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="384"/>
        <source>Keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="404"/>
        <source>Mouse</source>
        <translation>ཙིག་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="424"/>
        <source>Battery</source>
        <translation>གློག་སྨན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="444"/>
        <source>CD-ROM</source>
        <translation>འོད་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="464"/>
        <source>Camera</source>
        <translation>པར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="484"/>
        <source>Bluetooth</source>
        <translation>སོ་སྔོན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="504"/>
        <source>Fan</source>
        <translation>རླུང་གཡབ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="45"/>
        <source>ToolKit</source>
        <translation>ཡོ་བྱད་ཀྱི་སྒམ།</translation>
    </message>
</context>
<context>
    <name>MemoryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="145"/>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="171"/>
        <source>Memory Info</source>
        <translation>ནང་ཉིང་ཀྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="220"/>
        <source>Slot</source>
        <translation>འཇུག་ཤུར།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="224"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="228"/>
        <source>Freq</source>
        <translation>ཟློས་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="232"/>
        <source>Bus Width</source>
        <translation>སྤྱིའི་གནས་ཞེང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="236"/>
        <source>Total Capacity</source>
        <translation>སྤྱིའི་ཤོང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="240"/>
        <source>Used Capacity</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་ནུས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="244"/>
        <source>Serail Num</source>
        <translation>གོ་རིམ་གྱི་ཨང་གི</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="248"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="252"/>
        <source>Data Width</source>
        <translation>གཞི་གྲངས་ཀྱི་ཞེང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="256"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="260"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="264"/>
        <source>Config Speed</source>
        <translation>སྒྲིག་བཀོད་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="268"/>
        <source>Channel</source>
        <translation>འཇུག་ཤུར་ཨང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="272"/>
        <source>Array Handle</source>
        <translation>སྒྲིག་ཆས་ཀྱི་ཡུ་བ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="276"/>
        <source>Part Number</source>
        <translation>ཆ་ཤས་ཀྱི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="280"/>
        <source>Physical ID</source>
        <translation>དངོས་ལུགསID</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="284"/>
        <source>Model</source>
        <translation>དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="305"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>MonitorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="65"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="197"/>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="223"/>
        <source>Monitor</source>
        <translation>འཆར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="272"/>
        <source>Manufacturer</source>
        <translation>འཆར་ཆས་ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="276"/>
        <source>Name</source>
        <translation>འཆར་ཆས་ཀྱི་ཁྱད་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="280"/>
        <source>Size</source>
        <translation>འཆར་ངོས་ཀྱི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="284"/>
        <source>Ratio</source>
        <translation>རི་མོའི་བསྡུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="288"/>
        <source>Resolution</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="292"/>
        <source>MAX Resolution</source>
        <translation>ཆེས་ཆེ་བའི་དབྱེ་འབྱེད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="298"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="300"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="302"/>
        <source>Main Screen</source>
        <translation>བརྙན་ཤེལ་གཙོ་བོ།(ཡིན་ནམ་མིན།)</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="306"/>
        <source>Interface</source>
        <translation>འབྲེལ་མཐུད་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="310"/>
        <source>Visible Area</source>
        <translation>མཐོང་ཐུབ་པའི་རྒྱ་ཁྱོན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="314"/>
        <source>Product Week</source>
        <translation>ཐོན་རྫས་ཐོན་སྐྱེད་པའི་ཟླ་ཚེས་དང་གཟའ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="318"/>
        <source>Product Year</source>
        <translation>ཐོན་རྫས་ཐོན་སྐྱེད་བྱེད་པའི་ཟླ་ཚེས་དང་ལོ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="322"/>
        <source>Gamma</source>
        <translation>སྐྱ་མ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="343"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>MotherBoardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="155"/>
        <source>Name</source>
        <translation>གཙོ་ངོས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="159"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="167"/>
        <source>Chipset</source>
        <translation>ཉིང་ལྷེབ་ཚོ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="171"/>
        <source>Serial Num</source>
        <translation>གོ་རིམ་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="196"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="163"/>
        <source>Version</source>
        <translation>མ་པང་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="61"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="175"/>
        <source>Publish Date</source>
        <translation>ཁྱབ་བསྒྲགས་བྱས་པའི་ཚེས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="179"/>
        <source>BIOS Manufacturer</source>
        <translation>BIOSཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="183"/>
        <source>BIOS Version</source>
        <translation>BIOSཔར་གཞི།</translation>
    </message>
</context>
<context>
    <name>MouseInfo</name>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="148"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="174"/>
        <source>Mouse</source>
        <translation>ཙིག་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="223"/>
        <source>Name</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="231"/>
        <source>Model</source>
        <translation>སྒྲིག་ཆས་ཀྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="235"/>
        <source>Manufacurer</source>
        <translation>ཐན་སྐྱེད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="239"/>
        <source>Interface</source>
        <translation>འབྲེལ་མཐུད་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="243"/>
        <source>Driver</source>
        <translation>སྐུལ་འདེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="247"/>
        <source>Device Address</source>
        <translation>སྒྲིག་ཆས་ཀྱི་སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="268"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>NetCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="145"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="171"/>
        <source>Network Card</source>
        <translation>དྲ་རྒྱའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="220"/>
        <source>Name</source>
        <translation>དྲ་རྒྱའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="226"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="228"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="230"/>
        <source>Type</source>
        <translation>དྲ་རྒྱའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="226"/>
        <source>Wired</source>
        <translation>སྐུད་པ་འཐེན་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="228"/>
        <source>Wireless</source>
        <translation>སྐུད་མེད་གློག་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="235"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="239"/>
        <source>Bus Address</source>
        <translation>སྤྱིའི་གློག་སྐུད་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="243"/>
        <source>MAC</source>
        <translation>MACས་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="247"/>
        <source>Driver</source>
        <translation>སྐུལ་འདེད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="251"/>
        <source>Link Speed</source>
        <translation>སྦྲེལ་མཐུད་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="255"/>
        <source>MTU</source>
        <translation>MTU</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="259"/>
        <source>IP</source>
        <translation>IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="263"/>
        <source>Model</source>
        <translation>དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="267"/>
        <source>Subnet Mask</source>
        <translation>དྲ་རྒྱའི་གསང་ཨང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="271"/>
        <source>Gateway</source>
        <translation>དྲ་རྒྱའི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="275"/>
        <source>DNS Server</source>
        <translation>DNS ཞབས་ཞུ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="279"/>
        <source>Bytes Received</source>
        <translation>ཡིག་ཚིགས་བསྡུ་ལེན་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="283"/>
        <source>Bytes Sent</source>
        <translation>ཡིག་ཚིགས་བསྐུར་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="304"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>ProcessorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="154"/>
        <source>Processor</source>
        <translation>ལས་སྣོན་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="178"/>
        <source>Slot</source>
        <translation>འཇུག་ཤུར།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="158"/>
        <source>Manufacturer</source>
        <translation>ཐོན་སྐྱེད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="61"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="162"/>
        <source>Architecture</source>
        <translation>བཟོ་སྐྲུན་རིག་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="166"/>
        <source>Core Num</source>
        <translation>ལྟེ་བའི་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="170"/>
        <source>Core Online Num</source>
        <translation>དྲ་ཐོག་ལྟེ་བའི་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="174"/>
        <source>Thread</source>
        <translation>ཉིང་ཐིག་རེ་རེའི་གྲངས་ཀ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="182"/>
        <source>Max Frequency</source>
        <translation>ཆེས་ཆེ་བའི་ཟློས་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="186"/>
        <source>Frequency</source>
        <translation>ཐེངས་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="190"/>
        <source>L1 Cache</source>
        <translation>རིམ་པ་དང་པོའི་གསོག་འཇོག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="194"/>
        <source>L1d Cache</source>
        <translation>རིམ་པ་དང་པོའི་གསོག་འཇོག(གཞི་གྲངས།)</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="198"/>
        <source>L1i Cache</source>
        <translation>རིམ་པ་དང་པོའི་གསོག་འཇོག(བཀའ་བརྡ།)</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="202"/>
        <source>L2 Cache</source>
        <translation>རིམ་པ་གཉིས་པའི་གསོག་འཇོག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="206"/>
        <source>L3 Cache</source>
        <translation>རིམ་པ་གསུམ་པའི་གསོག་འཇོག</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="210"/>
        <source>Instruction Set</source>
        <translation>བཀའ་བཀོད་ཕྱོགས་བསྒྲིགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="214"/>
        <source>EXT Instruction Set</source>
        <translation>བཀའ་བརྡའི་ཚོགས་པ་རྒྱ་བསྐྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="218"/>
        <source>Used</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>app is already running!</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>VoiceCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ཚང་མ་འདྲ་བཟོ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="161"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="187"/>
        <source>Sound Card</source>
        <translation>སྒྲའི་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="236"/>
        <source>Bus Address</source>
        <translation>སྤྱིའི་གློག་སྐུད་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="240"/>
        <source>Name</source>
        <translation>སྒྲའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="244"/>
        <source>Drive</source>
        <translation>སྒྲ་བྱང་གི་སྒུལ་ཤུགས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="248"/>
        <source>Model</source>
        <translation>སྒྲ་བྱང་བཟོ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="252"/>
        <source>Manufacurer</source>
        <translation>བཟོ་སྐྲུན་ཚོང་པ།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="256"/>
        <source>Clock</source>
        <translation>དུས་ཚོད་ཀྱི་འགུལ་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="260"/>
        <source>Bit Width</source>
        <translation>གཞི་གྲངས་ཀྱི་ཞེང་ཚད།</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="281"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>སྒྲིག་ཆས་མེད་པའམ་ཡང་ན་ཐོབ་པའི་སྒྲིག་ཆས་ཀྱི་ཆ་འཕྲིན་སྟོང་བ་ཡིན།</translation>
    </message>
</context>
</TS>
