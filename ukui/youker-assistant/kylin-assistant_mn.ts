<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>VERSION</source>
        <translation type="vanished">版本号</translation>
    </message>
</context>
<context>
    <name>BatteryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="257"/>
        <source>Model</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>soc</source>
        <translation type="vanished">剩余电量</translation>
    </message>
    <message>
        <source>Estimated Service Time</source>
        <translation type="vanished">预计使用时间</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="160"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="196"/>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="245"/>
        <source>Battery</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="249"/>
        <source>Serail Number</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="245"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="253"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="261"/>
        <source>State</source>
        <translation>ᠳᠦᠯᠦᠪ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="265"/>
        <source>Percentage</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="269"/>
        <source>Energy</source>
        <translation>ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="273"/>
        <source>Energy Full</source>
        <translation>ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ᠎ᠶ᠋ᠢ ᠪᠦᠷᠢᠨ ᠳᠦᠬᠦᠷᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="277"/>
        <source>Time To Empty</source>
        <translation>ᠤᠷᠢᠳᠴᠢᠯᠠᠭ᠍ᠰᠠᠨ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/batteryinfo.cpp" line="281"/>
        <source>Used Times</source>
        <translation>ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌ ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>BluetoothInfo</name>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="63"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="159"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="195"/>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="224"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="229"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="248"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="399"/>
        <source>Enable </source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="232"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="240"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="245"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="402"/>
        <source>Disable </source>
        <translation>ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="282"/>
        <source>Bus Address</source>
        <translation>ᠪᠠᠰ᠎ᠤᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="286"/>
        <source>Function</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="290"/>
        <source>Frequency</source>
        <translation>ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="294"/>
        <source>Configuration</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="298"/>
        <source>Type</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="302"/>
        <source>ID</source>
        <translation>ID ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="306"/>
        <source>Model</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="310"/>
        <source>Resource</source>
        <translation>ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="314"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="318"/>
        <source>Version</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="322"/>
        <source>Data Width</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠦᠷᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="326"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="330"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="334"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="338"/>
        <source>Serial Number</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ ᠤ᠋ᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="342"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="346"/>
        <source>Link Mode</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="350"/>
        <source>Link Policy</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠪᠤᠳᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="354"/>
        <source>Capabilities</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="358"/>
        <source>Bus</source>
        <translation>ᠪᠠᠰ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="362"/>
        <source>SCO MTU</source>
        <translation>SCO ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠨᠢᠭᠡᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="366"/>
        <source>ACL MTU</source>
        <translation>ACL ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠨᠢᠭᠡᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="370"/>
        <source>Packet Type</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠪᠠᠭ᠍ᠯᠠᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="374"/>
        <source>Features</source>
        <translation>ᠤᠨᠴᠠᠯᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="410"/>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="415"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/bluetoothinfo.cpp" line="412"/>
        <source>Bluetooth Disable</source>
        <translation>ᠯᠠᠨᠶᠠ ᠨᠢᠭᠡᠨᠲᠡ ᠴᠠᠭᠠᠵᠠᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>CDRomInfo</name>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ ᠺᠤᠪᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="161"/>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="197"/>
        <source>CD-ROM</source>
        <translation>ᠭᠡᠷᠡᠯᠢᠭ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="246"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="250"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="254"/>
        <source>Version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="258"/>
        <source>Model</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="262"/>
        <source>Serail Number</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ ᠤ᠋ᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="266"/>
        <source>Bus Info</source>
        <translation>ᠪᠠᠰ᠎ᠤᠨ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="270"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="274"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="278"/>
        <source>Device Number</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/cdrominfo.cpp" line="299"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠱᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠤᠯᠵᠤ ᠠᠪᠤᠭ᠍ᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠵᠠᠩᠬᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>CameraInfo</name>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="157"/>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="193"/>
        <source>Camera</source>
        <translation>ᠺᠡᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="242"/>
        <source>Name</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="246"/>
        <source>Resolution</source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="250"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠬᠦ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="254"/>
        <source>Model</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="258"/>
        <source>Interface</source>
        <translation>ᠵᠠᠯᠭᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="262"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="266"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="270"/>
        <source>Version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="274"/>
        <source>Bus Info</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠬᠡᠯᠬᠢᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/camerainfo.cpp" line="278"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
</context>
<context>
    <name>CpuFMPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="79"/>
        <source>Current CPU frequency</source>
        <translation>CPU ᠤᠳᠤᠬᠠᠨ᠎ᠤ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="101"/>
        <source>Current average CPU core frequency</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ CPU ᠤ᠋ᠨ/ ᠵᠢᠨ ᠭᠣᠣᠯ ᠳᠠᠪᠲᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="111"/>
        <source>CPU FM Note: The CPU FM function has some risks, please use it carefully! After FM is completed, restarting will restore the default configuration!</source>
        <translation>CPU ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠰᠡᠯᠭᠦᠬᠦ ᠨᠢ CPU ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠳ᠋ᠤ᠌ ᠵᠣᠬᠢᠬᠤ ᠠᠶᠤᠯ ᠲᠦᠭᠰᠢᠭᠦᠷᠢ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢᠬᠤ᠎ᠶ᠋ᠢ ᠬᠠᠷᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠬᠡᠷᠰᠡᠦᠯᠡᠵᠦ ᠠᠵᠢᠯᠯᠠᠭᠠᠷᠠᠢ ! ᠳᠠᠪᠲᠠᠮᠵᠢ ᠰᠡᠯᠭᠦᠬᠦ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠢ᠋ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠵᠦ ᠠᠶᠠᠳᠠᠯ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ᠎ᠢ᠋ ᠰᠡᠷᠭᠦᠭᠡᠨ᠎ᠡ !</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmpage.cpp" line="151"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>CpuFMSetWidget</name>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="87"/>
        <source>CPU Management Strategy</source>
        <translation>CPU ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠪᠤᠳᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="95"/>
        <source>performance</source>
        <translation>ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠪ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="102"/>
        <source>powersave</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠡᠮᠨᠡᠬᠦ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="109"/>
        <source>userspace</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="116"/>
        <source>schedutil</source>
        <translation>ᠵᠢᠭ᠌ᠳᠡ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="123"/>
        <source>ondemand</source>
        <translation>ᠲᠠᠰᠢᠷᠠᠮ᠎ᠤ᠋ᠨ ᠬᠤᠪᠢᠷᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/cpufmsetwidget.cpp" line="130"/>
        <source>conservative</source>
        <translation>ᠬᠡᠪᠱᠢᠮᠡᠯ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
</context>
<context>
    <name>DeviceMonitorPage</name>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="49"/>
        <source>The equipment is normal and the heat dissipation is good</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠬᠡᠪ᠎ᠦᠨ᠂ ᠢᠯᠴᠢ ᠳᠠᠷᠬᠠᠭᠠᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠰᠠᠢᠨ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="50"/>
        <source>Equipment temperature is high, please pay attention to heat dissipation</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠡᠯᠢᠶᠡᠳ ᠦᠨᠳᠦᠷ᠂ ᠢᠯᠴᠢ ᠳᠠᠷᠬᠠᠭᠠᠬᠤ᠎ᠶ᠋ᠢ ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="51"/>
        <source>Equipment temperature is abnormal, please pay attention to heat dissipation</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠡᠪ᠎ᠦᠨ ᠪᠤᠰᠤ᠂ ᠢᠯᠴᠢ ᠳᠠᠷᠬᠠᠭᠠᠬᠤ᠎ᠶ᠋ᠢ ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="176"/>
        <source>CPU Temp</source>
        <translation>CPU ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="182"/>
        <source>HARDDISK Temp</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="188"/>
        <source>GPU Temp</source>
        <translation>GPU ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠤᠯᠠᠭᠠᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="194"/>
        <source>DEV Temp</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠳᠤᠯᠠᠭᠠᠨ᠎ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="223"/>
        <source>DEV Usage</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <source>BaseBoard Temp</source>
        <translation type="vanished">主板温度</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="211"/>
        <source>CPU Usage</source>
        <translation>CPU ᠬᠡᠷᠡᠭ᠌ᠯᠡᠭᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/devicemonitorpage.cpp" line="217"/>
        <source>Memory Usage</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
</context>
<context>
    <name>DriveInfoPage</name>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="69"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="133"/>
        <source>Motherboard</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="137"/>
        <source>Graphics Card</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ᠎ᠶ᠋ᠢᠨ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="141"/>
        <source>Wired Network Card</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="145"/>
        <source>Sound Card</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="149"/>
        <source>Wireless Network Card</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="153"/>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="159"/>
        <location filename="../../plugins/drivemanage/driveinfopage.cpp" line="163"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
</context>
<context>
    <name>DriveManage</name>
    <message>
        <location filename="../../plugins/drivemanage/drivemanage.cpp" line="43"/>
        <source>DriveManager</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ᠎ᠦᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>FanInfo</name>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="161"/>
        <source>Fan</source>
        <translation>ᠰᠡᠩᠰᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="197"/>
        <source>Network Card</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ᠌ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="246"/>
        <source>Speed</source>
        <translation>ᠡᠷᠭᠢᠯᠳᠡ᠎ᠶ᠋ᠢᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/faninfo.cpp" line="267"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>GraphicsCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="73"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="171"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="207"/>
        <source>Graphics Card</source>
        <translation>ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="256"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="260"/>
        <source>SubSystem</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="264"/>
        <source>Name</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="268"/>
        <source>IsDiscrete</source>
        <translation>ᠳᠤᠰᠠᠭᠠᠷ ᠳᠡᠯᠭᠡᠴᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="272"/>
        <source>Memory</source>
        <translation>ᠢᠯᠡ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="285"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="288"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="298"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="300"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="311"/>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="313"/>
        <source>Video Memory</source>
        <translation>ᠢᠯᠡ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="319"/>
        <source>Memory Type</source>
        <translation>ᠢᠯᠡ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="323"/>
        <source>Model</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="327"/>
        <source>Version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="331"/>
        <source>Bit Width</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠣᠷᠣᠨ᠎ᠤ᠋ ᠥᠷᠭᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="335"/>
        <source>Funtion</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="339"/>
        <source>Clock</source>
        <translation>ᠴᠠᠭ᠎ᠤᠨ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="343"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="347"/>
        <source>Dbus Info</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠬᠡᠯᠬᠢᠶ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="351"/>
        <source>ID</source>
        <translation>ID ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="355"/>
        <source>Device</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="359"/>
        <source>GDDR Capacity</source>
        <translation>GDDR ᠪᠠᠭᠲᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="363"/>
        <source>EGL Version</source>
        <translation>EGL ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="367"/>
        <source>EGL Client APIs</source>
        <translation>EGL ᠵᠠᠯᠭᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="371"/>
        <source>GL Version</source>
        <translation>GL ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="375"/>
        <source>GLSL Version</source>
        <translation>GLSL ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/graphicscardinfo.cpp" line="396"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>HWMonitorInfo</name>
    <message>
        <location filename="../../plugins/hwmonitor/hwmonitor.cpp" line="32"/>
        <source>HardwareMonitor</source>
        <translation>ᠬᠠᠷᠳ᠋ᠸᠡᠢᠷ᠎ᠢ ᠬᠢᠨᠠᠨ ᠱᠢᠯᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>HWMonitorWidget</name>
    <message>
        <source>Device Monitor</source>
        <translation type="vanished">设备监测</translation>
    </message>
    <message>
        <source>CPU FM</source>
        <translation type="vanished">CPU调频</translation>
    </message>
</context>
<context>
    <name>HWParam</name>
    <message>
        <location filename="../../plugins/hwparam/hwparam.cpp" line="34"/>
        <source>HardwareParam</source>
        <translation>ᠬᠠᠷᠳ᠋ᠸᠡᠢᠷ᠎ᠤᠨ ᠫᠠᠷᠠᠮᠸᠲᠷ</translation>
    </message>
</context>
<context>
    <name>HardDiskInfo</name>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="64"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="162"/>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="198"/>
        <source>Hard Disk Info</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ᠎ᠦᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="247"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="251"/>
        <source>Name</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ᠎ᠦᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="261"/>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="263"/>
        <source>Capacity</source>
        <translation>ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="268"/>
        <source>Used Times</source>
        <translation>ᠬᠡᠷᠡᠭ᠌ᠯᠡᠭ᠌ᠰᠡᠨ ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="272"/>
        <source>Interface</source>
        <translation>ᠠᠭᠤᠯᠵᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="278"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ /ᠳᠡᠢᠮᠤ/</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="280"/>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ /ᠦᠬᠡᠢ/</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="282"/>
        <source>Main Disk</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ (ᠮᠦᠨ/ ᠪᠢᠰᠢ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="288"/>
        <source>SSD</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳᠦᠯᠦᠪ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="290"/>
        <source>HDD</source>
        <translation>ᠮᠸᠬᠠᠨᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="292"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ （ᠬᠠᠳᠠᠭᠤ ᠳᠦᠯᠦᠪ/ ᠮᠸᠬᠠᠨᠢᠭ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="296"/>
        <source>Serial Num</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ᠎ᠤᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="300"/>
        <source>Model</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="304"/>
        <source>Transfer Rate</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="308"/>
        <source>Read Speed</source>
        <translation>ᠳ᠋ᠢᠰᠺ ᠤᠩᠱᠢᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="312"/>
        <source>Write Speed</source>
        <translation>ᠳ᠋ᠢᠰᠺ᠎ᠲᠦ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="316"/>
        <source>Firmware Version</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳᠤᠨᠤᠭ᠍ᠯᠠᠯ᠎ᠤᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="320"/>
        <source>UFS Version</source>
        <translation>UFS ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/harddiskinfo.cpp" line="341"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="194"/>
        <source>Manufacturer</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠮᠠᠱᠢᠨ᠎ᠤ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <source>MachineModel</source>
        <translation type="obsolete">整机型号</translation>
    </message>
    <message>
        <source>SerialNum</source>
        <translation type="obsolete">序列号</translation>
    </message>
    <message>
        <source>SystemBits</source>
        <translation type="obsolete">系统位数</translation>
    </message>
    <message>
        <source>KernelArch</source>
        <translation type="obsolete">内核架构</translation>
    </message>
    <message>
        <source>HostName</source>
        <translation type="obsolete">主机名</translation>
    </message>
    <message>
        <source>OSVersion</source>
        <translation type="obsolete">操作系统版本</translation>
    </message>
    <message>
        <source>KernelVersion</source>
        <translation type="obsolete">内核版本</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="71"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="200"/>
        <source>Machine Model</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠮᠠᠱᠢᠨ᠎ᠤ ᠬᠡᠯᠪᠡᠷᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="206"/>
        <source>Serial Number</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ ᠤ᠋ᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="212"/>
        <source>System Bits</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠣᠷᠣᠨ ᠲᠣᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="218"/>
        <source>Kernel Arch</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠴᠦᠮ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠷᠢᠯᠳᠦᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="224"/>
        <source>Host Name</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠱᠢᠨ᠎ᠤ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="252"/>
        <source>Kylin Linux Desktop V10 (SP1)</source>
        <translation>ᠴᠢ ᠯᠢᠨ Linux ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ V10 (SP1 )</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="257"/>
        <source>Kylin Linux Desktop (Touch Screen) V10 (SP1)</source>
        <translation>ᠶᠢᠨ ᠾᠧ ᠴᠢ ᠯᠢᠨ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠵᠢᠯᠤᠭᠤᠳᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ （ᠲᠣᠮᠣ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠬᠡᠪᠯᠡᠯ） V10 (SP1 )</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="259"/>
        <source>Kylin Linux Desktop (Tablet) V10 (SP1)</source>
        <translation>ᠶᠢᠨ ᠾᠧ ᠴᠢ ᠯᠢᠨ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠤ᠋ᠨ ᠵᠢᠯᠤᠭᠤᠳᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ (ᠬᠠᠪᠲᠠᠭᠠᠢ ᠬᠡᠪᠯᠡᠯ） V10 (SP1 )</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="263"/>
        <source>OS Version</source>
        <translation>ᠵᠢᠯᠤᠭᠤᠳᠬᠤ ᠱᠢᠰᠲᠸᠮ᠎ᠦᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="270"/>
        <source>Kernel Version</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠴᠦᠮ᠎ᠡ᠎ᠶ᠋ᠢᠨ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="276"/>
        <source>Processor</source>
        <translation>ᠱᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="309"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="313"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="342"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="346"/>
        <source>Memory</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="354"/>
        <source>Mother Board</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="367"/>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="372"/>
        <source>Hard Disk</source>
        <translation>ᠬᠠᠳᠠᠭᠤ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="379"/>
        <source>Graphics Card</source>
        <translation>ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="391"/>
        <source>Network Card</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠳᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="397"/>
        <source>Sound Card</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ᠋ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <source>MainBoard</source>
        <translation type="obsolete">主板</translation>
    </message>
    <message>
        <source>HardDisk</source>
        <translation type="obsolete">硬盘</translation>
    </message>
    <message>
        <source>GraphicsCard</source>
        <translation type="obsolete">显卡</translation>
    </message>
    <message>
        <location filename="../../plugins/machineinfo/infopage.cpp" line="385"/>
        <source>Monitor</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ</translation>
    </message>
    <message>
        <source>NetworkCard</source>
        <translation type="obsolete">网卡</translation>
    </message>
    <message>
        <source>SoundCard</source>
        <translation type="obsolete">声卡</translation>
    </message>
</context>
<context>
    <name>KAAboutDialog</name>
    <message>
        <location filename="../kaaboutdialog.cpp" line="117"/>
        <location filename="../kaaboutdialog.cpp" line="143"/>
        <source>ToolKit</source>
        <translation>ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="148"/>
        <source>VERSION:  </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠎ᠦᠨ ᠳ᠋ᠤᠭᠠᠷ:  </translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="153"/>
        <source>ToolKit provides some extended functions and users can query the hardware details of the current computer.</source>
        <translation>ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ ᠪᠣᠯ ᠴᠢ ᠯᠢᠨ ᠪᠦᠯᠬᠦᠮ᠎ᠡᠴᠡ ᠰᠤᠳᠤᠯᠤᠨ ᠨᠡᠭᠡᠭᠡᠭᠰᠡᠨ ᠨᠢᠭᠡ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠲᠤᠰᠠᠯᠠᠬᠤ ᠰᠤᠹᠲ ᠪᠣᠯᠤᠨ᠎ᠠ ᠃ ᠭᠣᠣᠯᠳᠠᠭᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢ ᠬᠠᠪᠰᠤᠷᠤᠨ ᠣᠳᠣᠬᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠬᠠᠷᠳᠧᠡᠢᠷ᠎ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ᠂ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠪᠠᠢᠳᠠᠯ ᠵᠡᠷᠭᠡ᠎ᠶ᠋ᠢ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠳᠡᠭ᠃ ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌ ᠤᠭ ᠠᠫᠫᠠᠷᠠᠲ ᠪᠣᠯᠤᠨ ᠬᠠᠷᠳᠧᠡᠢᠷ᠎ᠦ᠋ᠨ ᠫᠠᠷᠠᠮᠸᠲᠷ᠂ ᠬᠠᠷᠳᠧᠡᠢᠷ᠎ᠢ᠋ ᠬᠢᠨᠠᠨ ᠰᠢᠯᠭᠠᠬᠤ ᠵᠡᠷᠭᠡ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠲᠠᠢ ᠪᠣᠯᠤᠭᠰᠠᠨ ᠪᠠᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>ToolKit is a powerful system supporting software which is developed by Kylin team. Mainly for the naive user, it can help users manage the system. At present, It provides system junk scanning and cleaning, viewing the system hardware and software information, task manager, and some other functions.</source>
        <translation type="obsolete">工具箱是由麒麟团队开发的一款功能强大的系统辅助软件。主要面向初级用户，能够帮助用户对系统进行管理。目前已具备了系统垃圾扫描与清理、系统软硬件信息查看、系统全方位定制、任务管理器等功能。</translation>
    </message>
    <message>
        <location filename="../kaaboutdialog.cpp" line="177"/>
        <source>Service &amp; Support :</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ ᠳᠡᠮᠵᠢᠯᠬᠡ:</translation>
    </message>
</context>
<context>
    <name>KABaseInfoPage</name>
    <message>
        <location filename="../../plugins/hwparam/kabaseinfopage.cpp" line="94"/>
        <source>kylin-assistant</source>
        <translation>ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/kabaseinfopage.cpp" line="97"/>
        <source>kylin-assistant info</source>
        <translation>ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ᠎ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/kabaseinfopage.cpp" line="98"/>
        <source>Disable failed</source>
        <translation>ᠴᠠᠭᠠᠵᠠᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>KALabel</name>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
</context>
<context>
    <name>KAUsageItem</name>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="64"/>
        <source>Used</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwmonitor/kausageitem.cpp" line="66"/>
        <source>Left</source>
        <translation>ᠬᠡᠷᠡᠭ᠌ᠯᠡᠵᠦ᠍ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>KDriveInfoItem</name>
    <message>
        <source>Copy</source>
        <translation type="vanished">复制</translation>
    </message>
</context>
<context>
    <name>KInfoListItem</name>
    <message>
        <source>Copy</source>
        <translation type="obsolete">复制</translation>
    </message>
</context>
<context>
    <name>KLeftWidget</name>
    <message>
        <location filename="../kleftwidget.cpp" line="64"/>
        <source>ToolKit</source>
        <translation>ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
</context>
<context>
    <name>KRightWidget</name>
    <message>
        <source>menu</source>
        <translation type="obsolete">ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="59"/>
        <source>options</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="65"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="71"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="82"/>
        <location filename="../krightwidget.cpp" line="96"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="83"/>
        <location filename="../krightwidget.cpp" line="90"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="84"/>
        <location filename="../krightwidget.cpp" line="107"/>
        <source>Exit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="155"/>
        <location filename="../krightwidget.cpp" line="159"/>
        <source>ToolKit</source>
        <translation>ᠪᠠᠭᠠᠵᠢ ᠵᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="161"/>
        <source>Version：</source>
        <translation>ᠬᠡᠪᠯᠡᠯ：</translation>
    </message>
    <message>
        <location filename="../krightwidget.cpp" line="162"/>
        <source>Service &amp; Support: &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>&lt;p&gt; ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠠ ᠳᠡᠮᠵᠢᠯᠭᠡ : &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;ToolKit is a powerful system supporting software which is developed by Kylin team. Mainly for the naive user, it can help users manage the system. At present, It provides system junk scanning and cleaning, viewing the system hardware and software information, task manager, and some other functions.&lt;/p&gt;</source>
        <translation type="vanished"> &lt;p&gt;工具箱是由麒麟团队开发的一款功能强大的系统辅助软件。主要面向初级用户，能够帮助用户对系统进行管理。目前已具备了系统垃圾扫描与清理、系统软硬件信息查看、系统全方位定制、任务管理器等功能。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Service &amp; Support : &lt;a style=&apos;color: black;&apos; href=&apos;mailto:support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</source>
        <translation type="vanished">&lt;p&gt;服务和支持: &lt;a style=&apos;color: black;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>KeyboardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="161"/>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="197"/>
        <source>Keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠳᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="62"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="246"/>
        <source>Name</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="254"/>
        <source>Model</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="258"/>
        <source>Manufacurer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="262"/>
        <source>Interface</source>
        <translation>ᠠᠭᠤᠯᠵᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="266"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="270"/>
        <source>Device Address</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/keyboardinfo.cpp" line="291"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>LoadWidget</name>
    <message>
        <location filename="../loadwidget.cpp" line="81"/>
        <source>Enabling, please wait</source>
        <translation>ᠶᠠᠭ ᠡᠬᠢᠯᠡᠵᠦ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠭᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../loadwidget.cpp" line="85"/>
        <source>Disabling, please wait</source>
        <translation>ᠶᠠᠭ ᠴᠠᠭᠠᠵᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠭᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../loadwidget.cpp" line="125"/>
        <source>VERSION</source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <location filename="../../CommonControl/loadingwidget.cpp" line="58"/>
        <source>Scanning, please wait</source>
        <translation>ᠱᠢᠷᠪᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>MachineInfo</name>
    <message>
        <location filename="../../plugins/machineinfo/machineinfo.cpp" line="36"/>
        <source>MachineInfo</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠮᠠᠱᠢᠨ᠎ᠤ ᠵᠠᠩᠬᠢ</translation>
    </message>
</context>
<context>
    <name>MainInfoPage</name>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="205"/>
        <source>Processor</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="225"/>
        <source>Memory</source>
        <translation>ᠳᠣᠲᠣᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="245"/>
        <source>Graphics Card</source>
        <translation>ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="265"/>
        <source>Motherboard</source>
        <translation>ᠭᠣᠣᠯ ᠬᠠᠪᠲᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="285"/>
        <source>Network Card</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ᠌ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="305"/>
        <source>Hard Disk</source>
        <translation>ᠬᠠᠲᠠᠭᠤ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="326"/>
        <source>Monitor</source>
        <translation>ᠦᠵᠡᠭᠦᠯᠦᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="346"/>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="363"/>
        <source>Sound Card</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ᠋ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <source>Voice Card</source>
        <translation type="obsolete">声卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="384"/>
        <source>Keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="404"/>
        <source>Mouse</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="424"/>
        <source>Battery</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="444"/>
        <source>CD-ROM</source>
        <translation>ᠭᠡᠷᠡᠯᠢᠭ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="464"/>
        <source>Camera</source>
        <translation>ᠺᠸᠮᠸᠷᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="484"/>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/maininfopage.cpp" line="504"/>
        <source>Fan</source>
        <translation>ᠰᠡᠩᠰᠡ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="46"/>
        <source>ToolKit</source>
        <translation>ᠪᠠᠭᠠᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <source>MachineInfo</source>
        <translation type="obsolete">整机信息</translation>
    </message>
    <message>
        <source>HardwareMonitor</source>
        <translation type="obsolete">硬件监测</translation>
    </message>
</context>
<context>
    <name>MemoryInfo</name>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="64"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="160"/>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="196"/>
        <source>Memory Info</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="245"/>
        <source>Slot</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠬᠤᠪᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="249"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="253"/>
        <source>Freq</source>
        <translation>ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="257"/>
        <source>Bus Width</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠤᠷᠤᠨ᠎ᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="267"/>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="269"/>
        <source>Total Capacity</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="274"/>
        <source>Used Capacity</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠭ᠌ᠰᠡᠨ ᠪᠠᠭ᠍ᠳᠠᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="278"/>
        <source>Serail Num</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ ᠤ᠋ᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="282"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="286"/>
        <source>Data Width</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠤᠷᠤᠨ᠎ᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="290"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="294"/>
        <source>Speed</source>
        <translation>ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="298"/>
        <source>Config Speed</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="302"/>
        <source>Channel</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠬᠤᠪᠢᠯ᠎ᠤᠨ ᠳ᠋ᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="306"/>
        <source>Array Handle</source>
        <translation>ᠲᠤᠭᠠᠨ ᠪᠦᠯᠦᠭ᠎ᠦᠨ ᠫᠷᠤᠭ᠌ᠷᠡᠮ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="310"/>
        <source>Part Number</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭ᠌ᠳᠡᠬᠦᠨ᠎ᠦ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="314"/>
        <source>Physical ID</source>
        <translation>ᠹᠢᠽᠢᠺ᠎ᠦᠨ ID</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="318"/>
        <source>Model</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/memoryinfo.cpp" line="339"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>MonitorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="67"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="224"/>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="260"/>
        <source>Monitor</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="309"/>
        <source>Manufacturer</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠷ ᠤ᠋ᠨ ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="313"/>
        <source>Name</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠷ᠎ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="317"/>
        <source>Size</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡᠨ᠎ᠦ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ / ᠢᠨᠴ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="321"/>
        <source>Ratio</source>
        <translation>ᠢᠮᠡᠭᠧ ᠵᠢᠷᠤᠭ᠎ᠤᠨ ᠦᠨᠳᠦᠷ ᠪᠤᠯᠤᠨ ᠦᠷᠬᠡᠨ᠎ᠦ ᠬᠠᠷᠢᠴᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="325"/>
        <source>Resolution</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠢᠯᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <source>MAX Resolution</source>
        <translation type="vanished">ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤᠢᠴᠠ ᠢᠯᠭᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="329"/>
        <source>Optimal Resolution</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠢᠯᠢᠳᠡᠭ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠢᠯᠭᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="335"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ /ᠳᠡᠢᠮᠤ/</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="337"/>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ /ᠡᠰᠡᠬᠦ᠌᠂ ᠦᠬᠡᠢ/</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="339"/>
        <source>Main Screen</source>
        <translation>ᠭᠤᠤᠯ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠷ （ᠮᠦᠨ/ ᠪᠢᠰᠢ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="343"/>
        <source>Interface</source>
        <translation>ᠠᠭᠤᠯᠵᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="347"/>
        <source>Visible Area</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ᠎ᠳᠤ ᠬᠦᠷᠳᠡᠬᠦ᠌ ᠳᠠᠯᠠᠪᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="351"/>
        <source>Product Week</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠰᠡᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ/ ᠭᠠᠷᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="355"/>
        <source>Product Year</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠰᠡᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ/ ᠤᠨ/</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="359"/>
        <source>Gamma</source>
        <translation>ᠭᠠᠮᠮᠠ ᠬᠡᠮᠵᠢᠭ᠌ᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/monitorinfo.cpp" line="380"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>MotherBoardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="167"/>
        <source>Name</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤᠨ᠎ᠤ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="171"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="179"/>
        <source>Chipset</source>
        <translation>ᠴᠢᠫ᠎ᠦᠨ ᠪᠦᠯᠦᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="183"/>
        <source>Serial Num</source>
        <translation>ᠴᠤᠪᠤᠷᠠᠯ ᠤ᠋ᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="226"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="175"/>
        <source>Version</source>
        <translation>ᠭᠤᠤᠯ ᠬᠠᠪᠳᠠᠰᠤᠨ᠎ᠤ ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="66"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="202"/>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="204"/>
        <source>Publish Date</source>
        <translation>ᠨᠡᠢᠳᠡᠯᠡᠭ᠌ᠰᠡᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="209"/>
        <source>BIOS Manufacturer</source>
        <translation>BIOS ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭ᠌ᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/motherboardinfo.cpp" line="213"/>
        <source>BIOS Version</source>
        <translation>BIOS ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
</context>
<context>
    <name>MouseInfo</name>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="162"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="198"/>
        <source>Mouse</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="63"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="221"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="251"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="253"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="286"/>
        <source>Name</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="256"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="261"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="339"/>
        <source>Enable </source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="271"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="276"/>
        <source>Disabled </source>
        <translation>ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="294"/>
        <source>Model</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="299"/>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="301"/>
        <source>Manufacurer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="306"/>
        <source>Interface</source>
        <translation>ᠠᠭᠤᠯᠵᠠᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="310"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="314"/>
        <source>Device Address</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="345"/>
        <source>Mouse device disabled</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ᠎ᠢ᠋ ᠴᠠᠭᠠᠵᠠᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/mouseinfo.cpp" line="347"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>NetCardInfo</name>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="65"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="162"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="198"/>
        <source>Network Card</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ᠌ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="230"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="236"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="289"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="407"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="411"/>
        <source>Enable </source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="239"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="281"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="286"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="414"/>
        <source>Disable </source>
        <translation>ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="277"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="279"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="297"/>
        <source>Name</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠺᠠᠷᠲ᠎ᠤᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="303"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="305"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="307"/>
        <source>Type</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ᠎ᠦ ᠺᠠᠷᠲ᠎ᠦᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="303"/>
        <source>Wired</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="305"/>
        <source>Wireless</source>
        <translation>ᠤᠳᠠᠰᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="312"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="316"/>
        <source>Bus Address</source>
        <translation>ᠪᠠᠰ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="320"/>
        <source>MAC</source>
        <translation>MAC ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="324"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="328"/>
        <source>Link Speed</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ᠌ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="332"/>
        <source>MTU</source>
        <translation>MTU</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="336"/>
        <source>IP</source>
        <translation>IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="340"/>
        <source>Model</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="344"/>
        <source>Subnet Mask</source>
        <translation>ᠰᠠᠯᠠᠭ᠎ᠠ ᠨᠧᠲ᠎ᠦ᠋ᠨ ᠳᠠᠯᠳᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="348"/>
        <source>Gateway</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠪᠤᠭᠤᠮᠳᠠ᠎ᠶ᠋ᠢᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="352"/>
        <source>DNS Server</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="362"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="364"/>
        <source>Bytes Received</source>
        <translation>ᠪᠠᠢᠲ ᠢ᠋ ᠨᠢᠬᠡᠨᠳᠡ ᠳᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="375"/>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="377"/>
        <source>Bytes Sent</source>
        <translation>ᠪᠠᠢᠲ ᠢ᠋ ᠨᠢᠬᠡᠨᠳᠡ ᠢᠯᠡᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="426"/>
        <source>Network card device is disabled</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠴᠠᠭᠠᠵᠢᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/netcardinfo.cpp" line="428"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>ProcessorInfo</name>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="156"/>
        <source>Processor</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠦᠷ</translation>
    </message>
    <message>
        <source>Number Of Cores</source>
        <translation type="vanished">核心数目</translation>
    </message>
    <message>
        <source>Number Of Threads</source>
        <translation type="vanished">线程数</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="180"/>
        <source>Slot</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠬᠣᠪᠢᠯ</translation>
    </message>
    <message>
        <source>Fref</source>
        <translation type="vanished">基准频率</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="63"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="160"/>
        <source>Manufacturer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="164"/>
        <source>Architecture</source>
        <translation>ᠪᠦᠷᠢᠯᠳᠦᠴᠡ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="168"/>
        <source>Core Num</source>
        <translation>ᠴᠦᠮ᠎ᠡ ᠲᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="172"/>
        <source>Core Online Num</source>
        <translation>ᠶᠠᠪᠤᠭ᠍ᠳᠠᠵᠤ ᠪᠤᠢ ᠴᠦᠮ᠎ᠡ ᠲᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="176"/>
        <source>Thread</source>
        <translation>ᠴᠥᠮ᠎ᠡ ᠵᠢᠨ ᠲᠷᠢᠳ᠋ ᠪᠦᠷᠢ᠎ᠶ᠋ᠢᠨ ᠲᠣᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="184"/>
        <source>Max Frequency</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡ ᠭᠤᠤᠯ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="188"/>
        <source>Frequency</source>
        <translation>ᠳᠠᠪᠲᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="192"/>
        <source>L1 Cache</source>
        <translation>ᠨᠢᠬᠡᠳᠦᠬᠡᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="202"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="204"/>
        <source>L1d Cache</source>
        <translation>ᠨᠢᠬᠡᠳᠦᠬᠡᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ （ᠳ᠋ᠠᠢᠲ᠋ᠠ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="215"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="217"/>
        <source>L1i Cache</source>
        <translation>ᠨᠢᠬᠡᠳᠦᠬᠡᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ （ᠵᠠᠷᠯᠢᠭ）</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="228"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="230"/>
        <source>L2 Cache</source>
        <translation>ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="241"/>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="243"/>
        <source>L3 Cache</source>
        <translation>ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠳᠡᠰ᠎ᠦᠨ ᠨᠠᠮᠬᠠᠳᠭᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="248"/>
        <source>Instruction Set</source>
        <translation>ᠵᠠᠷᠯᠢᠭ᠎ᠤᠨ ᠴᠤᠭ᠍ᠯᠠᠷᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="252"/>
        <source>EXT Instruction Set</source>
        <translation>ᠵᠠᠷᠯᠢᠭ᠎ᠤᠨ ᠴᠤᠭ᠍ᠯᠠᠷᠠᠯ᠎ᠢ ᠦᠷᠬᠡᠳᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/processorinfo.cpp" line="256"/>
        <source>Used</source>
        <translation>ᠭᠡᠷᠡᠭ᠌ᠯᠡᠬᠡᠨ᠎ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>app is already running!</source>
        <translation>ᠫᠷᠤᠭ᠌ᠷᠡᠮ ᠨᠢᠬᠡᠨᠳᠡ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
<context>
    <name>VoiceCardInfo</name>
    <message>
        <source>Voice Card</source>
        <translation type="obsolete">声卡</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="66"/>
        <source>CopyAll</source>
        <translation>ᠪᠦᠬᠦᠨ᠎ᠢ᠋ ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="179"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="215"/>
        <source>Sound Card</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ᠋ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="329"/>
        <source>Bus Address</source>
        <translation>ᠪᠠᠰ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="337"/>
        <source>Name</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠺᠠᠷᠲ᠎ᠦᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="333"/>
        <source>Drive</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠺᠠᠷᠲ᠎ᠦᠨ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="247"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="251"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="317"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="381"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="385"/>
        <source>Enable </source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="254"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="309"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="314"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="388"/>
        <source>Disabled </source>
        <translation>ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="293"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="308"/>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="325"/>
        <source>Model</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ ᠺᠠᠷᠲ᠎ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="341"/>
        <source>Manufacurer</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="345"/>
        <source>Clock</source>
        <translation>ᠴᠠᠭ᠎ᠤ᠋ᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="349"/>
        <source>Bit Width</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠤᠷᠤᠨ᠎ᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="400"/>
        <source>Sound card device is disabled</source>
        <translation>ᠳᠠᠭᠤᠨ᠎ᠤ᠋ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠴᠠᠭᠠᠵᠠᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../plugins/hwparam/voicecardinfo.cpp" line="402"/>
        <source>Device not exitst or Get Device is Empty</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ ᠪᠤᠶᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
</context>
<context>
    <name>processorinfo</name>
    <message>
        <source>Processor</source>
        <translation type="obsolete">处理器</translation>
    </message>
    <message>
        <source>Number Of Cores</source>
        <translation type="obsolete">核心数目</translation>
    </message>
    <message>
        <source>Number Of Threads</source>
        <translation type="obsolete">线程数</translation>
    </message>
    <message>
        <source>Slot</source>
        <translation type="obsolete">插槽</translation>
    </message>
    <message>
        <source>Fref</source>
        <translation type="obsolete">基准频率</translation>
    </message>
    <message>
        <source>L1 Cache</source>
        <translation type="obsolete">一级缓存</translation>
    </message>
    <message>
        <source>L2 Cache</source>
        <translation type="obsolete">二级缓存</translation>
    </message>
    <message>
        <source>L3 Cache</source>
        <translation type="obsolete">三级缓存</translation>
    </message>
    <message>
        <source>Instruction Set</source>
        <translation type="obsolete">指令集</translation>
    </message>
    <message>
        <source>Used</source>
        <translation type="obsolete">使用率</translation>
    </message>
</context>
</TS>
