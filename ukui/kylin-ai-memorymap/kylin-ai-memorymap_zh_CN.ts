<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutPage</name>
    <message>
        <source>MemoryMap</source>
        <translation>记忆地图</translation>
    </message>
    <message>
        <source>Version:</source>
        <translation>版本:</translation>
    </message>
    <message>
        <source>Service &amp; Support</source>
        <translation>服务与支持</translation>
    </message>
    <message>
        <source>version:</source>
        <translation type="vanished">版本:</translation>
    </message>
    <message>
        <source> Service and Support Team:</source>
        <translation type="vanished">服务与支持团队:</translation>
    </message>
</context>
<context>
    <name>AllTextBox</name>
    <message>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <source>SelectAll</source>
        <translation>全选</translation>
    </message>
</context>
<context>
    <name>AppProgramIcon</name>
    <message>
        <source>MemoryMap</source>
        <translation>记忆地图</translation>
    </message>
</context>
<context>
    <name>CloseBtnPopUp</name>
    <message>
        <source>Please select the state after closing</source>
        <translation type="vanished">请选择关闭后状态:</translation>
    </message>
    <message>
        <source>Please select the state after closing:</source>
        <translation>请选择关闭后状态：</translation>
    </message>
    <message>
        <source>Minimize to tray</source>
        <translation>最小化到系统托盘</translation>
    </message>
    <message>
        <source>Exit program</source>
        <translation>直接退出</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>MemoryMapBackend</name>
    <message>
        <source>MemoryMap</source>
        <translation>记忆地图</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <source>Enter your memory fragments...</source>
        <translation>输入你的记忆碎片...</translation>
    </message>
</context>
<context>
    <name>SearchResultPage</name>
    <message>
        <source>Text Matching</source>
        <translation>文本匹配</translation>
    </message>
    <message>
        <source>Image Matching</source>
        <translation>图像匹配</translation>
    </message>
    <message>
        <source>We couldn&apos;t find the results you were looking for</source>
        <translation>未搜索到您要的结果</translation>
    </message>
</context>
<context>
    <name>ShowImageInfoPage</name>
    <message>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Regional identification</source>
        <translation>区域识别</translation>
    </message>
</context>
<context>
    <name>StartMemoryBtn</name>
    <message>
        <source>MemoryMap</source>
        <translation>记忆地图</translation>
    </message>
    <message>
        <source>Memory map, allowing you to bid farewell to the pain of searching and experience memories like photos</source>
        <translation>记忆地图，让你告别寻找之苦，以照片记忆般的体验</translation>
    </message>
    <message>
        <source> Quickly retrieve everything from your computer!</source>
        <translation>快速找回电脑中的一切！</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>开始记忆</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>TitleBarMenu</name>
    <message>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>Open Memory Map</source>
        <translation>打开记忆地图</translation>
    </message>
    <message>
        <source>Start Memorizing</source>
        <translation>开始记忆</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Pause Memory</source>
        <translation>暂停记忆</translation>
    </message>
    <message>
        <source>StartMemorizing</source>
        <translation type="vanished">开始记忆</translation>
    </message>
    <message>
        <source>MemoryMap</source>
        <translation>记忆地图</translation>
    </message>
    <message>
        <source>MemoryMap-Remembering...</source>
        <translation>记忆地图-记忆中...</translation>
    </message>
</context>
<context>
    <name>VideoPreviewPage</name>
    <message>
        <source>Previous Page</source>
        <translation>上一页</translation>
    </message>
    <message>
        <source>Next Page</source>
        <translation>下一页</translation>
    </message>
</context>
<context>
    <name>VideoReader</name>
    <message>
        <source>today</source>
        <translation>今天</translation>
    </message>
    <message>
        <source>yesterday</source>
        <translation type="vanished">昨天</translation>
    </message>
    <message>
        <source>The day before yesterday</source>
        <translation type="vanished">前天</translation>
    </message>
    <message>
        <source>2 days ago</source>
        <translation>前天</translation>
    </message>
    <message>
        <source>1 day ago</source>
        <translation>昨天</translation>
    </message>
</context>
<context>
    <name>WindowSettings</name>
    <message>
        <source>MemoryMap</source>
        <translation>记忆地图</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>MemoryMap Window</source>
        <translation>记忆地图</translation>
    </message>
</context>
</TS>
