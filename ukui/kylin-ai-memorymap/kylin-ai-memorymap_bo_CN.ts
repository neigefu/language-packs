<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AboutPage.qml" line="46"/>
        <source>MemoryMap</source>
        <translatorcomment>记忆地图</translatorcomment>
        <translation>དྲན་རྟགས་ཀྱི་ས་ཁྲ།</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AboutPage.qml" line="51"/>
        <source>Version:</source>
        <translatorcomment>版本</translatorcomment>
        <translation>ཐོན་རྩལ།：</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AboutPage.qml" line="57"/>
        <source>Service &amp; Support</source>
        <translatorcomment>服务和支持</translatorcomment>
        <translation>ཞབས་ཏོག་དང་རྒྱབ་སྐྱོར།</translation>
    </message>
</context>
<context>
    <name>AllTextBox</name>
    <message>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="282"/>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="315"/>
        <source>Copy</source>
        <translatorcomment>拷贝</translatorcomment>
        <translation>འདྲ་བཤད།</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="283"/>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="322"/>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="327"/>
        <source>SelectAll</source>
        <translatorcomment>全选</translatorcomment>
        <translation>ཆ་ཚང་འདེམས་གཏོང།</translation>
    </message>
</context>
<context>
    <name>AppProgramIcon</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AppProgramIcon.qml" line="19"/>
        <source>MemoryMap</source>
        <translatorcomment>记忆地图</translatorcomment>
        <translation>དྲན་རྟགས་ཀྱི་ས་ཁྲ།</translation>
    </message>
</context>
<context>
    <name>CloseBtnPopUp</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="58"/>
        <source>Please select the state after closing:</source>
        <translatorcomment>请选择关闭后状态</translatorcomment>
        <translation>སྒོ་སྒྲིག་རྗེས་གནས་སྟངས་འདེམས་གནང་།：</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="71"/>
        <source>Minimize to tray</source>
        <translatorcomment>最小化到系统托盘</translatorcomment>
        <translation>ལྕགས་ཁྲ་ནང་ཆུང་དུ་བཞག་</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="82"/>
        <source>Exit program</source>
        <translatorcomment>直接退出</translatorcomment>
        <translation>ཐད་ཀར་ཕྱིར་ཐོན་</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="104"/>
        <source>cancel</source>
        <translatorcomment>取消</translatorcomment>
        <translation>ཕྱིར་ལྷག</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="114"/>
        <source>sure</source>
        <translatorcomment>确定</translatorcomment>
        <translation>ཡིད་ཆེས་བརྟན།</translation>
    </message>
</context>
<context>
    <name>MemoryMapBackend</name>
    <message>
        <location filename="../memorymapbackend.cpp" line="68"/>
        <source>MemoryMap</source>
        <translatorcomment>记忆地图</translatorcomment>
        <translation>དྲན་རྟགས་ཀྱི་ས་ཁྲ།</translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../memorymapui/ui/qml/SearchBox.qml" line="23"/>
        <source>Enter your memory fragments...</source>
        <translatorcomment>输入你的记忆碎片</translatorcomment>
        <translation>ཁྱོད་ཀྱི་དྲན་རྟགས་ཀྱི་ཆ་ཤས་བཙུགས་གནང་།...</translation>
    </message>
</context>
<context>
    <name>SearchResultPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/SearchResultPage.qml" line="23"/>
        <source>Text Matching</source>
        <translatorcomment>文本匹配</translatorcomment>
        <translation>ཡིག་ཆ་མཐུན་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/SearchResultPage.qml" line="25"/>
        <source>Image Matching</source>
        <translatorcomment>图像匹配</translatorcomment>
        <translation>པར་རིས་མཐུན་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/SearchResultPage.qml" line="34"/>
        <source>We couldn&apos;t find the results you were looking for</source>
        <translatorcomment>未搜索到您要的结果</translatorcomment>
        <translation>ཁྱེད་ཀྱིས་འཚོལ་བའི་འབྲས་བུ་རྙེད་མ་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>ShowImageInfoPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/ShowImageInfoPage.qml" line="26"/>
        <source>Return</source>
        <translatorcomment>返回</translatorcomment>
        <translation>ལོག་གཏོངས།</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/ShowImageInfoPage.qml" line="119"/>
        <source>Regional identification</source>
        <translatorcomment>识别区域</translatorcomment>
        <translation>ཤེས་རྟོགས་ཀྱི་ས་ཁུལ།</translation>
    </message>
</context>
<context>
    <name>StartMemoryBtn</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="25"/>
        <source>MemoryMap</source>
        <translatorcomment>记忆地图</translatorcomment>
        <translation>དྲན་རྟགས་ཀྱི་ས་ཁྲ།</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="31"/>
        <source>Memory map, allowing you to bid farewell to the pain of searching and experience memories like photos</source>
        <translatorcomment>记忆地图，让你告别寻找之苦，以照片记忆般的体验</translatorcomment>
        <translation>དྲན་རྟགས་ཀྱི་ས་ཁྲ། ཁྱོད་ཀྱིས་འཚོལ་བའི་སྡུག་བསྔགས་ལས་ཁ་བྲལ་བྱེད་ པར་རིས་ཀྱི་དྲན་རྟགས་ལྟ་བའི་ཉམས་མྱོང་བྱེད་</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="39"/>
        <source> Quickly retrieve everything from your computer!</source>
        <translatorcomment>快速找回电脑中的一切！</translatorcomment>
        <translation> གློག་ཀླད་ནང་གི་ཐམས་ཅད་མགྱོགས་པོར་སླར་ཐོབ་！</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="66"/>
        <source>Start</source>
        <translatorcomment>开始记忆</translatorcomment>
        <translation>དྲན་རྟགས་འགོ་འཛུགས།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBar.qml" line="22"/>
        <source>Option</source>
        <translatorcomment>选项</translatorcomment>
        <translation>གདམ་ཁ་</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBar.qml" line="32"/>
        <source>Minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation>ཆུང་དུ་བཏགས་</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBar.qml" line="39"/>
        <source>Close</source>
        <translatorcomment>关闭</translatorcomment>
        <translation>བཀག་སྡོམ།</translation>
    </message>
</context>
<context>
    <name>TitleBarMenu</name>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="20"/>
        <source>Help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation>རོགས་རམ།</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="21"/>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="44"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>སྐོར་གཞིག</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="22"/>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="41"/>
        <source>Exit</source>
        <translatorcomment>退出</translatorcomment>
        <translation>ཕྱིར་ཐོན།</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="18"/>
        <location filename="../memorymapui/trayicon.cpp" line="65"/>
        <source>MemoryMap</source>
        <translatorcomment>记忆地图</translatorcomment>
        <translation>དྲན་རྟགས་ཀྱི་ས་ཁྲ།</translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="23"/>
        <source>Open Memory Map</source>
        <translatorcomment>打开记忆地图</translatorcomment>
        <translation>དྲན་པའི་ས་ཁྲ་ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="25"/>
        <location filename="../memorymapui/trayicon.cpp" line="70"/>
        <source>Start Memorizing</source>
        <translatorcomment>开始记忆</translatorcomment>
        <translation>དྲན་རྟགས་འགོ་འཛུགས།</translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="26"/>
        <source>Exit</source>
        <translatorcomment>退出</translatorcomment>
        <translation>ཕྱིར་ཐོན།</translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="58"/>
        <location filename="../memorymapui/trayicon.cpp" line="78"/>
        <source>MemoryMap-Remembering...</source>
        <translatorcomment>记忆地图-记忆中...</translatorcomment>
        <translation>དྲན་པའི་ས་ཁྲ། དྲན་པའི་ནང་...</translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="62"/>
        <location filename="../memorymapui/trayicon.cpp" line="76"/>
        <source>Pause Memory</source>
        <translatorcomment>暂停记忆</translatorcomment>
        <translation>དྲན་པ་མཚམས་འཇོག</translation>
    </message>
</context>
<context>
    <name>VideoPreviewPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/VideoPreviewPage.qml" line="44"/>
        <source>Previous Page</source>
        <translatorcomment>上一页</translatorcomment>
        <translation>སྔོན་གྱི་ཤོག་</translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/VideoPreviewPage.qml" line="78"/>
        <source>Next Page</source>
        <translatorcomment>下一页</translatorcomment>
        <translation>རྗེས་ཤོག་</translation>
    </message>
</context>
<context>
    <name>VideoReader</name>
    <message>
        <location filename="../memorymapui/videoreader.cpp" line="210"/>
        <source>today</source>
        <translatorcomment>今天</translatorcomment>
        <translation>དღེས།</translation>
    </message>
    <message>
        <location filename="../memorymapui/videoreader.cpp" line="216"/>
        <source>1 day ago</source>
        <translatorcomment>昨天</translatorcomment>
        <translation>ཁ་སྔོན་།</translation>
    </message>
    <message>
        <location filename="../memorymapui/videoreader.cpp" line="222"/>
        <source>2 days ago</source>
        <translatorcomment>前天</translatorcomment>
        <translation>ཁ་སྔོན་ཉིན་</translation>
    </message>
</context>
<context>
    <name>WindowSettings</name>
    <message>
        <location filename="../windowsettings.cpp" line="19"/>
        <location filename="../windowsettings.cpp" line="30"/>
        <source>MemoryMap</source>
        <translatorcomment>记忆地图</translatorcomment>
        <translation>དྲན་རྟགས་ཀྱི་ས་ཁྲ།</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="8"/>
        <source>MemoryMap Window</source>
        <translatorcomment>记忆地图</translatorcomment>
        <translation>དྲན་པའི་ས་ཁྲ་སྒེའུ་ཀ</translation>
    </message>
</context>
</TS>
