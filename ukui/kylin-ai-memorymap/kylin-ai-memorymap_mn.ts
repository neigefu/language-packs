<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AboutPage.qml" line="46"/>
        <source>MemoryMap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AboutPage.qml" line="51"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AboutPage.qml" line="57"/>
        <source>Service &amp; Support</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AllTextBox</name>
    <message>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="282"/>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="315"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="283"/>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="322"/>
        <location filename="../memorymapui/ui/qml/AllTextBox.qml" line="327"/>
        <source>SelectAll</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AppProgramIcon</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/AppProgramIcon.qml" line="19"/>
        <source>MemoryMap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CloseBtnPopUp</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="58"/>
        <source>Please select the state after closing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="71"/>
        <source>Minimize to tray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="82"/>
        <source>Exit program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="104"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/CloseBtnPopUp.qml" line="114"/>
        <source>sure</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MemoryMapBackend</name>
    <message>
        <location filename="../memorymapbackend.cpp" line="68"/>
        <source>MemoryMap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchBox</name>
    <message>
        <location filename="../memorymapui/ui/qml/SearchBox.qml" line="23"/>
        <source>Enter your memory fragments...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchResultPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/SearchResultPage.qml" line="23"/>
        <source>Text Matching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/SearchResultPage.qml" line="25"/>
        <source>Image Matching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/SearchResultPage.qml" line="34"/>
        <source>We couldn&apos;t find the results you were looking for</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowImageInfoPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/ShowImageInfoPage.qml" line="26"/>
        <source>Return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/ShowImageInfoPage.qml" line="119"/>
        <source>Regional identification</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartMemoryBtn</name>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="25"/>
        <source>MemoryMap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="31"/>
        <source>Memory map, allowing you to bid farewell to the pain of searching and experience memories like photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="39"/>
        <source> Quickly retrieve everything from your computer!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/basecomponent/StartMemoryBtn.qml" line="66"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBar.qml" line="22"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBar.qml" line="32"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBar.qml" line="39"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBarMenu</name>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="20"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="21"/>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="44"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="22"/>
        <location filename="../memorymapui/ui/qml/TitleBarMenu.qml" line="41"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="18"/>
        <location filename="../memorymapui/trayicon.cpp" line="65"/>
        <source>MemoryMap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="23"/>
        <source>Open Memory Map</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="25"/>
        <location filename="../memorymapui/trayicon.cpp" line="70"/>
        <source>Start Memorizing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="26"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="58"/>
        <location filename="../memorymapui/trayicon.cpp" line="78"/>
        <source>MemoryMap-Remembering...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/trayicon.cpp" line="62"/>
        <location filename="../memorymapui/trayicon.cpp" line="76"/>
        <source>Pause Memory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoPreviewPage</name>
    <message>
        <location filename="../memorymapui/ui/qml/VideoPreviewPage.qml" line="44"/>
        <source>Previous Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/ui/qml/VideoPreviewPage.qml" line="78"/>
        <source>Next Page</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VideoReader</name>
    <message>
        <location filename="../memorymapui/videoreader.cpp" line="210"/>
        <source>today</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/videoreader.cpp" line="216"/>
        <source>1 day ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../memorymapui/videoreader.cpp" line="222"/>
        <source>2 days ago</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WindowSettings</name>
    <message>
        <location filename="../windowsettings.cpp" line="19"/>
        <location filename="../windowsettings.cpp" line="30"/>
        <source>MemoryMap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../main.qml" line="8"/>
        <source>MemoryMap Window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
