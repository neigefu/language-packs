<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Batchinstall</name>
    <message>
        <source>OK</source>
        <translation type="obsolete">确定</translation>
    </message>
    <message>
        <location filename="../qt/batch-install.cpp" line="28"/>
        <source>Wait</source>
        <translatorcomment>等待</translatorcomment>
        <translation>等待</translation>
    </message>
    <message>
        <location filename="../qt/batch-install.cpp" line="89"/>
        <source>deb version :</source>
        <translatorcomment>包版本：</translatorcomment>
        <translation>套件版本：</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-installer</source>
        <translation type="obsolete">安装器</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="320"/>
        <source>installer</source>
        <translation>安裝器</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="526"/>
        <source>view log</source>
        <translatorcomment>查看日志</translatorcomment>
        <translation>查看紀錄</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="467"/>
        <location filename="../qt/kreinstall.cpp" line="529"/>
        <location filename="../qt/kreinstall.cpp" line="657"/>
        <source>determine</source>
        <translatorcomment>确定</translatorcomment>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="606"/>
        <source>Installation log</source>
        <translatorcomment>安装日志</translatorcomment>
        <translation>安裝紀錄</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="908"/>
        <location filename="../qt/kreinstall.cpp" line="1968"/>
        <source>one-click install</source>
        <translation>一鍵安裝</translation>
    </message>
    <message>
        <source>The same version is already installed</source>
        <translatorcomment>已安装相同的版本</translatorcomment>
        <translation type="vanished">已安装相同的版本</translation>
    </message>
    <message>
        <source>Continue Install</source>
        <translatorcomment>  继续安装  </translatorcomment>
        <translation type="vanished">重新安装</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translatorcomment>    取消    </translatorcomment>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Click the &quot;add&quot; button to select the software package for installation</source>
        <translatorcomment>点击&quot;添加&quot;按钮选择软件包进行安装</translatorcomment>
        <translation type="vanished">点击&quot;添加&quot;按钮选择软件包进行安装</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="963"/>
        <source>Add</source>
        <translatorcomment>添加</translatorcomment>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1096"/>
        <source>In the installation...</source>
        <translation>安裝中...</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="../qt/main.cpp" line="50"/>
        <source>kylin-installer is already running!</source>
        <translatorcomment>安装器已在运行！</translatorcomment>
        <translation>安裝器已在運行！</translation>
    </message>
</context>
<context>
    <name>debInfoWidget</name>
    <message>
        <location filename="../qt/debinfowidget.cpp" line="304"/>
        <source>deb name:</source>
        <translation>包名：</translation>
    </message>
    <message>
        <location filename="../qt/debinfowidget.cpp" line="55"/>
        <location filename="../qt/debinfowidget.cpp" line="217"/>
        <source>deb version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <source>deb name:%1</source>
        <translation type="vanished">包名：</translation>
    </message>
    <message>
        <source>version:%1</source>
        <translation type="obsolete">版本</translation>
    </message>
</context>
<context>
    <name>kreInstall</name>
    <message>
        <location filename="../qt/kreinstall.cpp" line="86"/>
        <location filename="../qt/kreinstall.cpp" line="109"/>
        <location filename="../qt/kreinstall.cpp" line="122"/>
        <location filename="../qt/kreinstall.cpp" line="139"/>
        <location filename="../qt/kreinstall.cpp" line="161"/>
        <location filename="../qt/kreinstall.cpp" line="2351"/>
        <location filename="../qt/kreinstall.cpp" line="2365"/>
        <source>open file error</source>
        <translation>打開檔案錯誤</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="111"/>
        <location filename="../qt/kreinstall.cpp" line="141"/>
        <source>The software package architecture does not match the native architecture</source>
        <translation>軟體包架構與本機體系架構不符</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="124"/>
        <source>The package format is not Debian format and cannot be opened.</source>
        <translatorcomment>软件包格式非Debian格式，无法打开。</translatorcomment>
        <translation>軟體包格式非Debian格式，無法打開。</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="90"/>
        <location filename="../qt/kreinstall.cpp" line="113"/>
        <location filename="../qt/kreinstall.cpp" line="126"/>
        <location filename="../qt/kreinstall.cpp" line="143"/>
        <location filename="../qt/kreinstall.cpp" line="165"/>
        <location filename="../qt/kreinstall.cpp" line="303"/>
        <location filename="../qt/kreinstall.cpp" line="2040"/>
        <location filename="../qt/kreinstall.cpp" line="2051"/>
        <location filename="../qt/kreinstall.cpp" line="2355"/>
        <location filename="../qt/kreinstall.cpp" line="2369"/>
        <location filename="../qt/kreinstall.cpp" line="2553"/>
        <location filename="../qt/kreinstall.cpp" line="2636"/>
        <location filename="../qt/kreinstall.cpp" line="2663"/>
        <location filename="../qt/kreinstall.cpp" line="2957"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="88"/>
        <source> file does not exist </source>
        <translatorcomment>文件不存在</translatorcomment>
        <translation> 檔不存在 </translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="338"/>
        <source>install:</source>
        <translation>安裝到：</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="vanished">安装方式：</translation>
    </message>
    <message>
        <source>General Installation</source>
        <translation type="vanished">普通安装</translation>
    </message>
    <message>
        <source>Senior Installation</source>
        <translation type="vanished">高级安装</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1180"/>
        <source>permissions:</source>
        <translation>安裝權限：</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1224"/>
        <source>x11</source>
        <translation>x伺服器</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1226"/>
        <source>systemdbus</source>
        <translation>系統總線</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1228"/>
        <source>sessiondbus</source>
        <translation>用戶總線</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1230"/>
        <source>network</source>
        <translation>網路</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1232"/>
        <source>ipc</source>
        <translation>進程間通信</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1234"/>
        <source>cups</source>
        <translation>列印服務</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1236"/>
        <source>pulseaudio</source>
        <translation>聲音</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1274"/>
        <source>install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="384"/>
        <source>installing!</source>
        <translation>安裝中...</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="163"/>
        <source>Failed to add. This package is inconsistent with the native schema!!!</source>
        <translatorcomment>添加失败，此包与本机架构不符！！！</translatorcomment>
        <translation>添加失敗，此包與本機架構不符!!</translation>
    </message>
    <message>
        <source>install success!</source>
        <translation type="vanished">安装成功！</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="458"/>
        <source>install success</source>
        <translatorcomment>安装成功</translatorcomment>
        <translation>安裝成功</translation>
    </message>
    <message>
        <source>install Fail!</source>
        <translation type="vanished">安装失败！</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="704"/>
        <source>Install</source>
        <translatorcomment>安装</translatorcomment>
        <translation>安裝</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="705"/>
        <source>Add</source>
        <translatorcomment>添加</translatorcomment>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="706"/>
        <source>View log</source>
        <translatorcomment>查看日志</translatorcomment>
        <translation>查看紀錄</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="707"/>
        <location filename="../qt/kreinstall.cpp" line="790"/>
        <source>Return</source>
        <translatorcomment>返回</translatorcomment>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="522"/>
        <location filename="../qt/kreinstall.cpp" line="2880"/>
        <source>install Fail</source>
        <translatorcomment>安装失败</translatorcomment>
        <translation>安裝失敗</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="407"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="422"/>
        <source>Help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="663"/>
        <source>Return home</source>
        <translatorcomment>返回首页</translatorcomment>
        <translation>返回首頁</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="791"/>
        <source>Determine</source>
        <translatorcomment>确定</translatorcomment>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="820"/>
        <source>Installation is complete</source>
        <translatorcomment>安装完成</translatorcomment>
        <translation>安裝完成</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="953"/>
        <source>Click the &quot;add&quot; button to select the</source>
        <translatorcomment>点击&quot;添加&quot;按钮选择</translatorcomment>
        <translation>點擊「添加」按鈕選擇</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="954"/>
        <source>software package for installation</source>
        <translatorcomment>软件包进行安装</translatorcomment>
        <translation>軟體包進行安裝</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2036"/>
        <location filename="../qt/kreinstall.cpp" line="2047"/>
        <location filename="../qt/kreinstall.cpp" line="2072"/>
        <location filename="../qt/kreinstall.cpp" line="2953"/>
        <source>install error!</source>
        <translatorcomment>安装错误！</translatorcomment>
        <translation>安裝錯誤！</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1743"/>
        <location filename="../qt/kreinstall.cpp" line="1747"/>
        <location filename="../qt/kreinstall.cpp" line="1753"/>
        <location filename="../qt/kreinstall.cpp" line="2077"/>
        <location filename="../qt/kreinstall.cpp" line="2081"/>
        <location filename="../qt/kreinstall.cpp" line="2087"/>
        <source>The current version of the software is already installed on the system</source>
        <translatorcomment>当前版本已在系统上安装</translatorcomment>
        <translation>當前版本已在系統上安裝</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="298"/>
        <location filename="../qt/kreinstall.cpp" line="2548"/>
        <source>Open file error
Please check whether the naming 
conforms to the specification
 &quot;pakgename_version_arch.deb&quot; </source>
        <translatorcomment>无法打开文件
请检查命名是否符合规范 “包名_版本_架构.deb”</translatorcomment>
        <translation>無法打開檔
請檢查命名是否符合規範 「包名_版本_架構.deb” </translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1757"/>
        <location filename="../qt/kreinstall.cpp" line="2091"/>
        <source>Cancel installation</source>
        <translatorcomment>取消安装</translatorcomment>
        <translation>取消安裝</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="1760"/>
        <location filename="../qt/kreinstall.cpp" line="2094"/>
        <source>continue installation</source>
        <translatorcomment>继续安装</translatorcomment>
        <translation>繼續安裝</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2038"/>
        <location filename="../qt/kreinstall.cpp" line="2049"/>
        <location filename="../qt/kreinstall.cpp" line="2955"/>
        <source>dpkg is being occupied, please wait and try again</source>
        <translation>dpkg正在被佔用，請稍候再試</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2353"/>
        <source>Kylin mobile operating environment is not installed!!!</source>
        <translatorcomment>麒麟移动运行环境未安装！！！</translatorcomment>
        <translation>麒麟移動運行環境未安裝!!</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2367"/>
        <source>Kylin mobile running environment has not started, unable to install APK files!!!</source>
        <translatorcomment>麒麟移动运行环境未启动，无法安装apk文件！！！</translatorcomment>
        <translation>麒麟移動運行環境未啟動，無法安裝apk檔!!</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2573"/>
        <location filename="../qt/kreinstall.cpp" line="2581"/>
        <location filename="../qt/kreinstall.cpp" line="2590"/>
        <source>In the list </source>
        <translatorcomment>列表中</translatorcomment>
        <translation>清單中 </translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2574"/>
        <location filename="../qt/kreinstall.cpp" line="2582"/>
        <location filename="../qt/kreinstall.cpp" line="2591"/>
        <source> already exists </source>
        <translatorcomment>已存在</translatorcomment>
        <translation> 已存在 </translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2575"/>
        <location filename="../qt/kreinstall.cpp" line="2583"/>
        <location filename="../qt/kreinstall.cpp" line="2592"/>
        <source> version, currently to be added </source>
        <translatorcomment> 版本，当前将要添加 </translatorcomment>
        <translation> 版本，當前將要添加 </translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2576"/>
        <location filename="../qt/kreinstall.cpp" line="2584"/>
        <location filename="../qt/kreinstall.cpp" line="2593"/>
        <source> of </source>
        <translatorcomment> 的 </translatorcomment>
        <translation> 的 </translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2577"/>
        <location filename="../qt/kreinstall.cpp" line="2585"/>
        <location filename="../qt/kreinstall.cpp" line="2594"/>
        <source> version, select replace?</source>
        <translatorcomment> 版本，是否选择替换？</translatorcomment>
        <translation> 版本，是否選擇替換？</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2598"/>
        <source>Cancel</source>
        <translatorcomment>取消</translatorcomment>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2601"/>
        <source>Replace</source>
        <translatorcomment>替换</translatorcomment>
        <translation>替換</translation>
    </message>
    <message>
        <source>add error!</source>
        <translatorcomment>添加错误！</translatorcomment>
        <translation type="vanished">添加错误！</translation>
    </message>
    <message>
        <location filename="../qt/kreinstall.cpp" line="2623"/>
        <location filename="../qt/kreinstall.cpp" line="2627"/>
        <location filename="../qt/kreinstall.cpp" line="2632"/>
        <location filename="../qt/kreinstall.cpp" line="2650"/>
        <location filename="../qt/kreinstall.cpp" line="2654"/>
        <location filename="../qt/kreinstall.cpp" line="2659"/>
        <source>The file already exists in the installation list</source>
        <translatorcomment>已存在安装列表</translatorcomment>
        <translation>已存在安裝清單</translation>
    </message>
</context>
<context>
    <name>titleWidget</name>
    <message>
        <source>kylin-installer</source>
        <translation type="vanished">安装器</translation>
    </message>
    <message>
        <location filename="../qt/titlewidget.cpp" line="27"/>
        <source>Installer</source>
        <translation>安裝器</translation>
    </message>
    <message>
        <location filename="../qt/titlewidget.cpp" line="73"/>
        <source>About</source>
        <translatorcomment>关于</translatorcomment>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../qt/titlewidget.cpp" line="75"/>
        <source>Help</source>
        <translatorcomment>帮助</translatorcomment>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../qt/titlewidget.cpp" line="77"/>
        <source>Quit</source>
        <translatorcomment>退出</translatorcomment>
        <translation>退出</translation>
    </message>
</context>
</TS>
