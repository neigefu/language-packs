<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QObject</name>
    <message>
        <source>kylin installation device</source>
        <translation type="vanished">卸载器</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/single_window.cpp" line="24"/>
        <source>warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/single_window.cpp" line="24"/>
        <source>The last software is being uninstalled,Please uninstall later!</source>
        <translation>上一個軟體正在卸載中，請稍後卸載！</translation>
    </message>
    <message>
        <source>The last software is being uninstalled. Please uninstall later!</source>
        <translation type="vanished">应用程序正在运行,请关闭后进行卸载</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/single_window.cpp" line="26"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>uninstaller</source>
        <translation type="vanished">卸载器</translation>
    </message>
</context>
<context>
    <name>debInfoWidget</name>
    <message>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="84"/>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="284"/>
        <source>deb name:</source>
        <translation>德布姓名：</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="96"/>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="288"/>
        <source>deb version:</source>
        <translation>DEB 版本：</translation>
    </message>
</context>
<context>
    <name>main_window</name>
    <message>
        <source>kylin-uninstaller</source>
        <translation type="vanished">麒麟卸载器</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="75"/>
        <source>uninstaller</source>
        <translation>卸載程式</translation>
    </message>
    <message>
        <source>Are you sure you want to uninstall  &quot;</source>
        <translatorcomment>你确定要卸载</translatorcomment>
        <translation type="vanished">你确定要卸载</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="186"/>
        <source>Are you sure you want to uninstall </source>
        <translatorcomment>你确定要卸载</translatorcomment>
        <translation>是否確實要卸載 </translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="219"/>
        <source>Cancel</source>
        <translatorcomment>取消</translatorcomment>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="223"/>
        <source>Uninstall</source>
        <translatorcomment>卸载</translatorcomment>
        <translation>卸載</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="329"/>
        <source>install error!</source>
        <translation></translation>
    </message>
    <message>
        <source>Failed to upgrade. Please uninstall again</source>
        <translatorcomment>提权失败，请重新卸载.</translatorcomment>
        <translation type="vanished">提权失败，请重新卸载.</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="333"/>
        <source>Determine</source>
        <translatorcomment>确定</translatorcomment>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="331"/>
        <source>Uninstall Failed</source>
        <translatorcomment>卸载失败</translatorcomment>
        <translation>卸載失敗</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="367"/>
        <source>uninstall</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>App is running, please uninstall after closing</source>
        <translation type="vanished">应用程序正在运行，请关闭后进行卸载</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="521"/>
        <source>uninstalling......</source>
        <translation>卸載...</translation>
    </message>
</context>
<context>
    <name>parts_result</name>
    <message>
        <location filename="../uninstall_qt/parts_result.cpp" line="43"/>
        <source>uninstall success</source>
        <translation>卸載成功</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/parts_result.cpp" line="45"/>
        <source>uninstall fail</source>
        <translation>卸載失敗</translation>
    </message>
</context>
<context>
    <name>parts_title</name>
    <message>
        <source>kylin installation device</source>
        <translation type="vanished">麒麟卸载器</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/parts_title.cpp" line="42"/>
        <source>uninstaller</source>
        <translation>卸載程式</translation>
    </message>
</context>
</TS>
