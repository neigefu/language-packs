<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <source>kylin installation device</source>
        <translation type="vanished">卸载器</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/single_window.cpp" line="24"/>
        <source>warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/single_window.cpp" line="24"/>
        <source>The last software is being uninstalled,Please uninstall later!</source>
        <translation>མཇུག་མཐར་མཉེན་ཆས་སྒྲིག་སྦྱོར་བྱེད་བཞིན་ཡོད། ཅུང་ཙམ་འགོར་རྗེས་སྒྲིག་སྦྱོར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>The last software is being uninstalled. Please uninstall later!</source>
        <translation type="vanished">应用程序正在运行,请关闭后进行卸载</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/single_window.cpp" line="26"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <source>uninstaller</source>
        <translation type="vanished">卸载器</translation>
    </message>
</context>
<context>
    <name>debInfoWidget</name>
    <message>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="84"/>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="284"/>
        <source>deb name:</source>
        <translation>འགན་གཙང་ལེན་པའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="96"/>
        <location filename="../uninstall_qt/debinfowidget.cpp" line="288"/>
        <source>deb version:</source>
        <translation>པར་གཞི།</translation>
    </message>
</context>
<context>
    <name>main_window</name>
    <message>
        <source>kylin-uninstaller</source>
        <translation type="vanished">麒麟卸载器</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="75"/>
        <source>uninstaller</source>
        <translation>མར་ལེན་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <source>Are you sure you want to uninstall  &quot;</source>
        <translatorcomment>你确定要卸载</translatorcomment>
        <translation type="vanished">你确定要卸载</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="186"/>
        <source>Are you sure you want to uninstall </source>
        <translatorcomment>你确定要卸载</translatorcomment>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་བསུབ་དགོས། </translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="219"/>
        <source>Cancel</source>
        <translatorcomment>取消</translatorcomment>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="223"/>
        <source>Uninstall</source>
        <translatorcomment>卸载</translatorcomment>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="329"/>
        <source>install error!</source>
        <translation></translation>
    </message>
    <message>
        <source>Failed to upgrade. Please uninstall again</source>
        <translatorcomment>提权失败，请重新卸载.</translatorcomment>
        <translation type="vanished">提权失败，请重新卸载.</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="333"/>
        <source>Determine</source>
        <translatorcomment>确定</translatorcomment>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="331"/>
        <source>Uninstall Failed</source>
        <translatorcomment>卸载失败</translatorcomment>
        <translation>སྒྲིག་སྦྱོར་བྱས་ནས་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="367"/>
        <source>uninstall</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>App is running, please uninstall after closing</source>
        <translation type="vanished">应用程序正在运行，请关闭后进行卸载</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/main_window.cpp" line="521"/>
        <source>uninstalling......</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ......</translation>
    </message>
</context>
<context>
    <name>parts_result</name>
    <message>
        <location filename="../uninstall_qt/parts_result.cpp" line="43"/>
        <source>uninstall success</source>
        <translation>གྲུབ་འབྲས་ཐོབ་པ་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/parts_result.cpp" line="45"/>
        <source>uninstall fail</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ནས་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>parts_title</name>
    <message>
        <source>kylin installation device</source>
        <translation type="vanished">麒麟卸载器</translation>
    </message>
    <message>
        <location filename="../uninstall_qt/parts_title.cpp" line="42"/>
        <source>uninstaller</source>
        <translation>ཕབ་ལེན་མི་བྱེད་པ།</translation>
    </message>
</context>
</TS>
