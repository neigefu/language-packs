<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Core</name>
    <message>
        <location filename="../src/controller/core/core.cpp" line="653"/>
        <source>Add</source>
        <translation>添加图片</translation>
    </message>
</context>
<context>
    <name>Information</name>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="20"/>
        <source>Info</source>
        <translation>信息</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="25"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="32"/>
        <source>Type</source>
        <translation>格式</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="38"/>
        <source>Capacity</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="44"/>
        <source>Size</source>
        <translation>尺寸</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="50"/>
        <source>Color</source>
        <translation>颜色空间</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="56"/>
        <source>Created</source>
        <translation>创建时间</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="62"/>
        <source>Modified</source>
        <translation>修改时间</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Storage size</source>
        <translation type="vanished">大小</translation>
    </message>
    <message>
        <source>Pixel Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Color Space</source>
        <translation type="vanished">颜色空间</translation>
    </message>
    <message>
        <source>Create Time</source>
        <translation type="vanished">创建时间</translation>
    </message>
    <message>
        <source>Revise Time</source>
        <translation type="vanished">修改时间</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <source>Kylin Photo Viewer</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="30"/>
        <location filename="../src/view/kyview.cpp" line="1341"/>
        <source>Pictures</source>
        <translation>看图</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="690"/>
        <location filename="../src/view/kyview.cpp" line="838"/>
        <location filename="../src/view/kyview.cpp" line="995"/>
        <source>full srceen</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="1342"/>
        <source>Version:</source>
        <translation>版本:</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="1343"/>
        <source>A system picture tool that can quickly open common formats. It provides zoom,flip and other processing simplely.</source>
        <translation>看图是一款系统图片查看工具，可快速打开浏览常见格式的图片。同时也提供了缩放、翻转等简单的图片处理，操作简单方便。</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="633"/>
        <location filename="../src/view/kyview.cpp" line="694"/>
        <location filename="../src/view/kyview.cpp" line="835"/>
        <location filename="../src/view/kyview.cpp" line="1001"/>
        <source>recovery</source>
        <translation>还原</translation>
    </message>
</context>
<context>
    <name>OCRResultWidget</name>
    <message>
        <location filename="../src/view/ocrresultwidget.cpp" line="14"/>
        <source>OCR recognition...</source>
        <translation>正在识别...</translation>
    </message>
    <message>
        <location filename="../src/view/ocrresultwidget.cpp" line="22"/>
        <source>No text dected</source>
        <translation>未检测到文本</translation>
    </message>
</context>
<context>
    <name>OpenImage</name>
    <message>
        <location filename="../src/view/openimage.cpp" line="20"/>
        <source>Load picture</source>
        <translation>载入图片</translation>
    </message>
    <message>
        <location filename="../src/view/openimage.cpp" line="91"/>
        <source>Open Image</source>
        <translation>打开图片</translation>
    </message>
    <message>
        <location filename="../src/view/openimage.cpp" line="92"/>
        <source>Image Files(</source>
        <translation>文件类型(</translation>
    </message>
    <message>
        <source>打开图片</source>
        <translation type="vanished">Open Image</translation>
    </message>
    <message>
        <source>文件类型(</source>
        <translation type="vanished">Image Files(</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="68"/>
        <source>Pictures</source>
        <translation>看图</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="40"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="41"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="42"/>
        <source>Set Desktop Wallpaper</source>
        <translation>设置为桌面壁纸</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="43"/>
        <source>Set Lock Wallpaper</source>
        <translation>设置为锁屏壁纸</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="44"/>
        <source>Print</source>
        <translation>打印</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="45"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="46"/>
        <source>Show in File</source>
        <translation>在文件夹中显示</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="47"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="48"/>
        <source>Markup</source>
        <translation>标注</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="63"/>
        <source>Next</source>
        <translation>下一张</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="67"/>
        <source>Previous</source>
        <translation>上一张</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="98"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="99"/>
        <location filename="../src/view/showimagewidget.cpp" line="116"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="118"/>
        <source>Export</source>
        <translation>导出txt</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="438"/>
        <source>OCR recognition...</source>
        <translation>正在识别...</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="504"/>
        <location filename="../src/view/showimagewidget.cpp" line="531"/>
        <location filename="../src/view/showimagewidget.cpp" line="538"/>
        <location filename="../src/view/showimagewidget.cpp" line="545"/>
        <location filename="../src/view/showimagewidget.cpp" line="965"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="504"/>
        <source>save file failed!</source>
        <translation>保存文件失败！</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="531"/>
        <source>save fail.name cannot begin with &quot;.&quot; </source>
        <translation>保存失败。不能以&quot;.&quot;命名 </translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="965"/>
        <source>是否保存对此图片的更改？</source>
        <translation>是否保存对此图片的更改？</translation>
    </message>
    <message>
        <source>save fail.name begins with &quot;.&quot; </source>
        <translation type="vanished">保存失败。</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="538"/>
        <location filename="../src/view/showimagewidget.cpp" line="545"/>
        <source>the file name is illegal</source>
        <translation>非法文件名</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Kylin Photo Viewer</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="154"/>
        <source>Pictures</source>
        <translation>看图</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="41"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="51"/>
        <source>full screen</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="58"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="150"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="298"/>
        <location filename="../src/view/titlebar.cpp" line="304"/>
        <location filename="../src/view/titlebar.cpp" line="330"/>
        <location filename="../src/view/titlebar.cpp" line="333"/>
        <location filename="../src/view/titlebar.cpp" line="336"/>
        <location filename="../src/view/titlebar.cpp" line="340"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="299"/>
        <source>This file will be hidden(the file whose name begins with &quot;.&quot; will be the hidden property file.)</source>
        <translation>此文件将被隐藏（文件名以“.”开头的将会成为隐藏属性文件）!</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="304"/>
        <source>the file name is illegal</source>
        <translation>非法文件名</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="330"/>
        <source>File does not exist (or has been deleted)!</source>
        <translation>文件不存在(已被删除)！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="333"/>
        <source>This name has been occupied, please choose another！</source>
        <translation>此名称已被占用，请选取其他名称！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="337"/>
        <source>This is a read-only file, please modify the permissions before operation！</source>
        <translation>此为只读文件，请修改权限后操作！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="340"/>
        <source>Other error, rename failed！</source>
        <translation>其他错误，重命名失败！</translation>
    </message>
    <message>
        <source>full srceen</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>recovery</source>
        <translation type="vanished">还原</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/view/toolbar.cpp" line="21"/>
        <source>Zoom out</source>
        <translation>缩小视图</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="32"/>
        <source>View scale</source>
        <translation>视图比例</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="40"/>
        <source>Zoom in</source>
        <translation>放大视图</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="49"/>
        <source>Life size</source>
        <translation>原尺寸</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="56"/>
        <source>Window widget</source>
        <translation>适应窗口</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="89"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="900"/>
        <source>Rorate left</source>
        <translation>向左旋转</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="912"/>
        <source>Rorate right</source>
        <translation>向右旋转</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="68"/>
        <source>Flip horizontally</source>
        <translation>水平镜像</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="74"/>
        <source>Flip vertically</source>
        <translation>垂直镜像</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="81"/>
        <source>Crop</source>
        <translation>裁剪</translation>
    </message>
    <message>
        <source>Tailoring</source>
        <translation type="vanished">裁剪</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="113"/>
        <source>Sidebar</source>
        <translation>侧边栏</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="119"/>
        <source>Get info</source>
        <translation>信息</translation>
    </message>
    <message>
        <source>Original size</source>
        <translation type="vanished">原始尺寸</translation>
    </message>
    <message>
        <source>Adaptive widget</source>
        <translation type="vanished">图片适应窗口</translation>
    </message>
    <message>
        <source>Rorate</source>
        <translation type="vanished">旋转</translation>
    </message>
    <message>
        <source>Horizontal mirror</source>
        <translation type="vanished">水平镜像</translation>
    </message>
    <message>
        <source>Vertical mirror</source>
        <translation type="vanished">垂直镜像</translation>
    </message>
    <message>
        <source>Thumbnail</source>
        <translation type="vanished">侧栏</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="126"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Service &amp; Support Team: </source>
        <translation type="vanished">服务与支持团队： </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="30"/>
        <location filename="../src/view/menumodule.cpp" line="220"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="31"/>
        <source>A system picture tool that can quickly open common formats. It provides zoom,flip and other processing simplely.</source>
        <translation>看图是一款系统图片查看工具，可快速打开浏览常见格式的图片。同时也提供了缩放、翻转等简单的图片处理，操作简单方便。</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="36"/>
        <source>menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <source>Open..</source>
        <translation type="vanished">打开..</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="57"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <source>Open...</source>
        <translation type="vanished">打开...</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="26"/>
        <location filename="../src/view/menumodule.cpp" line="267"/>
        <location filename="../src/view/menumodule.cpp" line="276"/>
        <source>Service &amp; Support: </source>
        <translation>服务与支持团队： </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="54"/>
        <location filename="../src/view/menumodule.cpp" line="105"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="60"/>
        <location filename="../src/view/menumodule.cpp" line="103"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="63"/>
        <location filename="../src/view/menumodule.cpp" line="101"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="66"/>
        <location filename="../src/view/menumodule.cpp" line="99"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="171"/>
        <location filename="../src/view/menumodule.cpp" line="189"/>
        <source>Pictures</source>
        <translation>看图</translation>
    </message>
    <message>
        <source>kylin photo view</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="178"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Kylin Photo View</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
</context>
</TS>
