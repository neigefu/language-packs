<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Core</name>
    <message>
        <location filename="../src/controller/core/core.cpp" line="653"/>
        <source>Add</source>
        <translation>加</translation>
    </message>
</context>
<context>
    <name>Information</name>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="20"/>
        <source>Info</source>
        <translation>資訊</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="25"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="32"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="38"/>
        <source>Capacity</source>
        <translation>能力</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="44"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="50"/>
        <source>Color</source>
        <translation>顏色</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="56"/>
        <source>Created</source>
        <translation>創建</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="62"/>
        <source>Modified</source>
        <translation>改 性</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Storage size</source>
        <translation type="vanished">大小</translation>
    </message>
    <message>
        <source>Pixel Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Color Space</source>
        <translation type="vanished">颜色空间</translation>
    </message>
    <message>
        <source>Create Time</source>
        <translation type="vanished">创建时间</translation>
    </message>
    <message>
        <source>Revise Time</source>
        <translation type="vanished">修改时间</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <source>Kylin Photo Viewer</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="30"/>
        <location filename="../src/view/kyview.cpp" line="1341"/>
        <source>Pictures</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="690"/>
        <location filename="../src/view/kyview.cpp" line="838"/>
        <location filename="../src/view/kyview.cpp" line="995"/>
        <source>full srceen</source>
        <translation>全 srceen</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="1342"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="1343"/>
        <source>A system picture tool that can quickly open common formats. It provides zoom,flip and other processing simplely.</source>
        <translation>可以快速打開常見格式的系統圖片工具。它提供縮放，翻轉和其他簡單的處理。</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="633"/>
        <location filename="../src/view/kyview.cpp" line="694"/>
        <location filename="../src/view/kyview.cpp" line="835"/>
        <location filename="../src/view/kyview.cpp" line="1001"/>
        <source>recovery</source>
        <translation>恢復</translation>
    </message>
</context>
<context>
    <name>OCRResultWidget</name>
    <message>
        <location filename="../src/view/ocrresultwidget.cpp" line="14"/>
        <source>OCR recognition...</source>
        <translation>OCR 識別...</translation>
    </message>
    <message>
        <location filename="../src/view/ocrresultwidget.cpp" line="22"/>
        <source>No text dected</source>
        <translation>無文本</translation>
    </message>
</context>
<context>
    <name>OpenImage</name>
    <message>
        <location filename="../src/view/openimage.cpp" line="20"/>
        <source>Load picture</source>
        <translation>載入圖片</translation>
    </message>
    <message>
        <location filename="../src/view/openimage.cpp" line="91"/>
        <source>Open Image</source>
        <translation>打開圖像</translation>
    </message>
    <message>
        <location filename="../src/view/openimage.cpp" line="92"/>
        <source>Image Files(</source>
        <translation>影像檔案（</translation>
    </message>
    <message>
        <source>打开图片</source>
        <translation type="vanished">Open Image</translation>
    </message>
    <message>
        <source>文件类型(</source>
        <translation type="vanished">Image Files(</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="68"/>
        <source>Pictures</source>
        <translation>圖片</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="40"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="41"/>
        <source>Rename</source>
        <translation>重新命名</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="42"/>
        <source>Set Desktop Wallpaper</source>
        <translation>設置桌面壁紙</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="43"/>
        <source>Set Lock Wallpaper</source>
        <translation>設置鎖定壁紙</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="44"/>
        <source>Print</source>
        <translation>列印</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="45"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="46"/>
        <source>Show in File</source>
        <translation>在文件中顯示</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="47"/>
        <source>Save as</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="48"/>
        <source>Markup</source>
        <translation>標記</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="63"/>
        <source>Next</source>
        <translation>下一個</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="67"/>
        <source>Previous</source>
        <translation>以前</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="98"/>
        <source>Save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="99"/>
        <location filename="../src/view/showimagewidget.cpp" line="116"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="118"/>
        <source>Export</source>
        <translation>出口</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="438"/>
        <source>OCR recognition...</source>
        <translation>OCR 識別...</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="504"/>
        <location filename="../src/view/showimagewidget.cpp" line="531"/>
        <location filename="../src/view/showimagewidget.cpp" line="538"/>
        <location filename="../src/view/showimagewidget.cpp" line="545"/>
        <location filename="../src/view/showimagewidget.cpp" line="965"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="504"/>
        <source>save file failed!</source>
        <translation>儲存檔案失敗！</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="531"/>
        <source>save fail.name cannot begin with &quot;.&quot; </source>
        <translation>保存 fail.name 不能以“.” </translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="965"/>
        <source>是否保存对此图片的更改？</source>
        <translation>是否保存對此圖片的更改？</translation>
    </message>
    <message>
        <source>save fail.name begins with &quot;.&quot; </source>
        <translation type="vanished">保存失败。</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="538"/>
        <location filename="../src/view/showimagewidget.cpp" line="545"/>
        <source>the file name is illegal</source>
        <translation>檔名是非法的</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Kylin Photo Viewer</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="154"/>
        <source>Pictures</source>
        <translation>圖片</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="41"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="51"/>
        <source>full screen</source>
        <translation>全螢幕</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="58"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="150"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="298"/>
        <location filename="../src/view/titlebar.cpp" line="304"/>
        <location filename="../src/view/titlebar.cpp" line="330"/>
        <location filename="../src/view/titlebar.cpp" line="333"/>
        <location filename="../src/view/titlebar.cpp" line="336"/>
        <location filename="../src/view/titlebar.cpp" line="340"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="299"/>
        <source>This file will be hidden(the file whose name begins with &quot;.&quot; will be the hidden property file.)</source>
        <translation>此檔案將被隱藏（名稱以“.”開頭的檔將是隱藏的屬性檔。</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="304"/>
        <source>the file name is illegal</source>
        <translation>檔名是非法的</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="330"/>
        <source>File does not exist (or has been deleted)!</source>
        <translation>檔不存在（或已被刪除）！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="333"/>
        <source>This name has been occupied, please choose another！</source>
        <translation>這個名字已經被佔用了，請再選一個！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="337"/>
        <source>This is a read-only file, please modify the permissions before operation！</source>
        <translation>這是一個唯讀檔，操作前請修改許可權！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="340"/>
        <source>Other error, rename failed！</source>
        <translation>其他錯誤，重命名失敗！</translation>
    </message>
    <message>
        <source>full srceen</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>recovery</source>
        <translation type="vanished">还原</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/view/toolbar.cpp" line="21"/>
        <source>Zoom out</source>
        <translation>縮小</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="32"/>
        <source>View scale</source>
        <translation>查看比例</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="40"/>
        <source>Zoom in</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="49"/>
        <source>Life size</source>
        <translation>真人大小</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="56"/>
        <source>Window widget</source>
        <translation>視窗小部件</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="89"/>
        <source>OCR</source>
        <translation>光學字元識別</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="900"/>
        <source>Rorate left</source>
        <translation>左羅拉特</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="912"/>
        <source>Rorate right</source>
        <translation>流浪權</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="68"/>
        <source>Flip horizontally</source>
        <translation>水平翻轉</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="74"/>
        <source>Flip vertically</source>
        <translation>垂直翻轉</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="81"/>
        <source>Crop</source>
        <translation>作物</translation>
    </message>
    <message>
        <source>Tailoring</source>
        <translation type="vanished">裁剪</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="113"/>
        <source>Sidebar</source>
        <translation>側 欄</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="119"/>
        <source>Get info</source>
        <translation>獲取資訊</translation>
    </message>
    <message>
        <source>Original size</source>
        <translation type="vanished">原始尺寸</translation>
    </message>
    <message>
        <source>Adaptive widget</source>
        <translation type="vanished">图片适应窗口</translation>
    </message>
    <message>
        <source>Rorate</source>
        <translation type="vanished">旋转</translation>
    </message>
    <message>
        <source>Horizontal mirror</source>
        <translation type="vanished">水平镜像</translation>
    </message>
    <message>
        <source>Vertical mirror</source>
        <translation type="vanished">垂直镜像</translation>
    </message>
    <message>
        <source>Thumbnail</source>
        <translation type="vanished">侧栏</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="126"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Service &amp; Support Team: </source>
        <translation type="vanished">服务与支持团队： </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="30"/>
        <location filename="../src/view/menumodule.cpp" line="220"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="31"/>
        <source>A system picture tool that can quickly open common formats. It provides zoom,flip and other processing simplely.</source>
        <translation>可以快速打開常見格式的系統圖片工具。它提供縮放，翻轉和其他簡單的處理。</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="36"/>
        <source>menu</source>
        <translation>功能表</translation>
    </message>
    <message>
        <source>Open..</source>
        <translation type="vanished">打开..</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="57"/>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <source>Open...</source>
        <translation type="vanished">打开...</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="26"/>
        <location filename="../src/view/menumodule.cpp" line="267"/>
        <location filename="../src/view/menumodule.cpp" line="276"/>
        <source>Service &amp; Support: </source>
        <translation>服務與支援： </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="54"/>
        <location filename="../src/view/menumodule.cpp" line="105"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="60"/>
        <location filename="../src/view/menumodule.cpp" line="103"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="63"/>
        <location filename="../src/view/menumodule.cpp" line="101"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="66"/>
        <location filename="../src/view/menumodule.cpp" line="99"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="171"/>
        <location filename="../src/view/menumodule.cpp" line="189"/>
        <source>Pictures</source>
        <translation>圖片</translation>
    </message>
    <message>
        <source>kylin photo view</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="178"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Kylin Photo View</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
</context>
</TS>
