<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../mainwindow.cpp" line="46"/>
        <source>Reboot</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="48"/>
        <source>Later</source>
        <translation>稍后</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="54"/>
        <source>System-Upgrade</source>
        <translation>系统更新</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="57"/>
        <source>ukui-control-center-upgrade</source>
        <translation>设置-升级</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="58"/>
        <source>It&apos;s suggested to reboot.</source>
        <translation>建议现在进行重启实现更新。</translation>
    </message>
</context>
</TS>
