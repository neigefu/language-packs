<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AppUpdateWid</name>
    <message>
        <location filename="../src/appupdate.cpp" line="296"/>
        <source>Cancel failed,Being installed</source>
        <translatorcomment>取消失败，安装中</translatorcomment>
        <translation>ཕམ་ཉེས་བྱུང་ནས་སྒྲིག་སྦྱོར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="315"/>
        <source>Being installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="376"/>
        <source>Download succeeded!</source>
        <translation>ཕབ་ལེན་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="466"/>
        <location filename="../src/appupdate.cpp" line="468"/>
        <location filename="../src/appupdate.cpp" line="469"/>
        <source>Update succeeded , It is recommended that you restart later!</source>
        <translation>གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་སོང་། ཁྱེད་ཀྱིས་རྗེས་སུ་ཡང་བསྐྱར་འགོ་འཛུགས་རྒྱུའི་གྲོས་འགོ་འདོན་རྒྱུ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="151"/>
        <location filename="../src/appupdate.cpp" line="152"/>
        <location filename="../src/appupdate.cpp" line="157"/>
        <location filename="../src/appupdate.cpp" line="173"/>
        <source>Version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="347"/>
        <location filename="../src/appupdate.cpp" line="350"/>
        <location filename="../src/appupdate.cpp" line="351"/>
        <source>Download finished,it is recommended that you restart later to use the new version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="354"/>
        <location filename="../src/appupdate.cpp" line="738"/>
        <source>reboot</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="358"/>
        <location filename="../src/appupdate.cpp" line="361"/>
        <location filename="../src/appupdate.cpp" line="362"/>
        <location filename="../src/appupdate.cpp" line="473"/>
        <location filename="../src/appupdate.cpp" line="475"/>
        <location filename="../src/appupdate.cpp" line="476"/>
        <source>Update succeeded , It is recommended that you log out later and log in again!</source>
        <translation>གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བ་རེད། ཁྱེད་ཀྱིས་རྗེས་སུ་ཐོ་འགོད་བྱས་ནས་ཡང་བསྐྱར་ཐོ་འགོད་བྱ་རྒྱུའི་གྲོས་འགོ་འདོན་རྒྱུ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="380"/>
        <location filename="../src/appupdate.cpp" line="386"/>
        <location filename="../src/appupdate.cpp" line="479"/>
        <location filename="../src/appupdate.cpp" line="484"/>
        <source>Update succeeded!</source>
        <translation>གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="397"/>
        <location filename="../src/appupdate.cpp" line="495"/>
        <location filename="../src/appupdate.cpp" line="603"/>
        <location filename="../src/appupdate.cpp" line="619"/>
        <location filename="../src/appupdate.cpp" line="641"/>
        <source>Update has been canceled!</source>
        <translation>གསར་སྒྱུར་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="408"/>
        <location filename="../src/appupdate.cpp" line="415"/>
        <location filename="../src/appupdate.cpp" line="432"/>
        <location filename="../src/appupdate.cpp" line="506"/>
        <location filename="../src/appupdate.cpp" line="513"/>
        <source>Update failed!</source>
        <translation>གསར་སྒྱུར་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="410"/>
        <location filename="../src/appupdate.cpp" line="508"/>
        <source>Failure reason:</source>
        <translation>ཕམ་ཉེས་བྱུང་བའི་རྒྱུ་རྐྱེན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="530"/>
        <source>There are unresolved dependency conflicts in this update，Please select update all</source>
        <translation>གསར་སྒྱུར་འདིའི་ནང་དུ་ཐག་གཅོད་བྱས་མེད་པའི་གཞན་རྟེན་རང་བཞིན་གྱི་འགལ་བ་ཡོད་པས། ཚང་མ་གསར་སྒྱུར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="531"/>
        <location filename="../src/appupdate.cpp" line="765"/>
        <source>Prompt information</source>
        <translation>མགྱོགས་མྱུར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="533"/>
        <source>Update ALL</source>
        <translation>ཚང་མ་གསར་སྒྱུར་བྱ་</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="534"/>
        <location filename="../src/appupdate.cpp" line="630"/>
        <location filename="../src/appupdate.cpp" line="768"/>
        <location filename="../src/appupdate.cpp" line="795"/>
        <location filename="../src/appupdate.cpp" line="849"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="581"/>
        <source>No Content.</source>
        <translation>ནང་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="591"/>
        <source>There are </source>
        <translation>དེ་རུ་ཡོད་པ་ནི། </translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="591"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation> ཐུམ་སྒྲིལ་མེད་པར་བཟོ་རྒྱུ་རེད། ཁྱེད་ཀྱིས་དང་ལེན་བྱེད་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="722"/>
        <source>Cumulative updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation type="vanished">ཐུམ་སྒྲིལ་མེད་པར་བཟོ་རྒྱུ་རེད། ཁྱེད་ཀྱིས་དང་ལེན་བྱེད་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="723"/>
        <source>version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <source>The update stopped because of low battery.</source>
        <translation type="vanished">གློག་སྨན་དམའ་བའི་རྐྱེན་གྱིས་གསར་སྒྱུར་བྱེད་མཚམས་བཞག་པ་རེད།</translation>
    </message>
    <message>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation type="vanished">མ་ལག་གསར་སྒྱུར་བྱེད་པར་གློག་སྨན་གྱི་སྒུལ་ཤུགས་50%ལས་མི་ཉུང་བའི་བླང་བྱ་བཏོན་ཡོད།</translation>
    </message>
    <message>
        <source>pkg will be uninstall!</source>
        <translation type="vanished">个软件包将被卸载！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="69"/>
        <location filename="../src/appupdate.cpp" line="406"/>
        <location filename="../src/appupdate.cpp" line="504"/>
        <location filename="../src/appupdate.cpp" line="604"/>
        <location filename="../src/appupdate.cpp" line="620"/>
        <location filename="../src/appupdate.cpp" line="642"/>
        <location filename="../src/appupdate.cpp" line="753"/>
        <location filename="../src/appupdate.cpp" line="800"/>
        <location filename="../src/appupdate.cpp" line="955"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="61"/>
        <source>details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="102"/>
        <location filename="../src/appupdate.cpp" line="172"/>
        <source>Update log</source>
        <translation>གསར་སྒྱུར་ཉིན་ཐོ།</translation>
    </message>
    <message>
        <source>Newest:</source>
        <translation type="vanished">最新：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="182"/>
        <source>Download size:</source>
        <translation>ཕབ་ལེན་གྱི་གཞི་ཁྱོན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="183"/>
        <source>Install size:</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་གཞི་ཁྱོན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="209"/>
        <source>Current version:</source>
        <translation>ད་ལྟའི་པར་གཞི་ནི།</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="764"/>
        <source>A single update will not automatically backup the system, if you want to backup, please click Update All.</source>
        <translation>གསར་སྒྱུར་རྐྱང་པ་ཞིག་གིས་མ་ལག་རང་འགུལ་གྱིས་རྗེས་གྲབས་བྱེད་མི་སྲིད། གལ་ཏེ་ཁྱོད་ཀྱིས་རྗེས་གྲབས་བྱེད་འདོད་ན་གསར་སྒྱུར་ཚང་མ་མནན་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="767"/>
        <source>Do not backup, continue to update</source>
        <translation>རྗེས་གྲབས་མི་བྱེད་པར་མུ་མཐུད་དུ་གསར་སྒྱུར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="771"/>
        <source>This time will no longer prompt</source>
        <translation>ཐེངས་འདིར་ཡང་བསྐྱར་སྐུལ་འདེད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="853"/>
        <source>Ready to update</source>
        <translatorcomment>准备更新</translatorcomment>
        <translation>གསར་སྒྱུར་བྱེད་པར་གྲ་སྒྲིག་</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="931"/>
        <source>downloaded</source>
        <translatorcomment>已下载</translatorcomment>
        <translation>ཕབ་ལེན་བྱས་པ།</translation>
    </message>
    <message>
        <source>Ready to install</source>
        <translation type="vanished">准备安装</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="937"/>
        <location filename="../src/appupdate.cpp" line="940"/>
        <source>downloading</source>
        <translation>ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="937"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>རྩིས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1016"/>
        <source>No content.</source>
        <translation>ནང་དོན་གང་ཡང་མེད།</translation>
    </message>
</context>
<context>
    <name>HistoryUpdateListWig</name>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="102"/>
        <source>Success</source>
        <translation>ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="108"/>
        <source>Failed</source>
        <translation>ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/backup.cpp" line="138"/>
        <source>system upgrade new backup</source>
        <translation>མ་ལག་རིམ་སྤར་གྱི་རྗེས་གྲབས་དཔུང་ཁག་</translation>
    </message>
    <message>
        <location filename="../src/backup.cpp" line="139"/>
        <source>system upgrade increment backup</source>
        <translation>མ་ལག་རིམ་སྤར་འཕར་སྣོན་གྱི་རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="78"/>
        <source>Advanced Option</source>
        <translation>མཐོ་རིམ་འདེམས་ཚན།</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="106"/>
        <source>Server address settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="109"/>
        <source>If internal services, change the server address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="123"/>
        <source>Port  ID </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="132"/>
        <source>Address</source>
        <translation>ཤག་གནས།</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="197"/>
        <source>reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="203"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="206"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="336"/>
        <source>OK</source>
        <translation type="unfinished">འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="333"/>
        <source>Modification failed!</source>
        <translation>ར་སྤྲོད་ཕམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="334"/>
        <source>Attention</source>
        <translation>དོ་སྣང་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>TabWid</name>
    <message>
        <location filename="../src/tabwidget.cpp" line="72"/>
        <location filename="../src/tabwidget.cpp" line="190"/>
        <location filename="../src/tabwidget.cpp" line="1359"/>
        <location filename="../src/tabwidget.cpp" line="1409"/>
        <location filename="../src/tabwidget.cpp" line="1711"/>
        <location filename="../src/tabwidget.cpp" line="1880"/>
        <location filename="../src/tabwidget.cpp" line="1971"/>
        <location filename="../src/tabwidget.cpp" line="2193"/>
        <location filename="../src/tabwidget.cpp" line="2256"/>
        <location filename="../src/tabwidget.cpp" line="2585"/>
        <source>Check Update</source>
        <translation>ཞིབ་བཤེར་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>initializing</source>
        <translation type="vanished">初始化中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="262"/>
        <location filename="../src/tabwidget.cpp" line="701"/>
        <location filename="../src/tabwidget.cpp" line="1382"/>
        <location filename="../src/tabwidget.cpp" line="1727"/>
        <location filename="../src/tabwidget.cpp" line="1857"/>
        <location filename="../src/tabwidget.cpp" line="2134"/>
        <location filename="../src/tabwidget.cpp" line="2273"/>
        <location filename="../src/tabwidget.cpp" line="2304"/>
        <location filename="../src/tabwidget.cpp" line="2635"/>
        <source>UpdateAll</source>
        <translation>གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Your system is the latest!</source>
        <translation type="vanished">您的系统已是最新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="914"/>
        <location filename="../src/tabwidget.cpp" line="1450"/>
        <source>No Information!</source>
        <translation>དཔྱད་གཞིའི་ཡིག་ཆ་མེད།</translation>
    </message>
    <message>
        <source>Last refresh:</source>
        <translation type="vanished">检查时间：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="205"/>
        <source>Downloading and installing updates...</source>
        <translation>ཕབ་ལེན་དང་སྒྲིག་སྦྱོར་བྱས་པའི་གནས་ཚུལ་གསར་བ་</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation type="vanished">立即更新</translation>
    </message>
    <message>
        <source>Cancel update</source>
        <translation type="vanished">取消更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="242"/>
        <location filename="../src/tabwidget.cpp" line="662"/>
        <source>Being updated...</source>
        <translation>གསར་སྒྱུར་བྱེད་བཞིན་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="253"/>
        <location filename="../src/tabwidget.cpp" line="1395"/>
        <source>Updatable app detected on your system!</source>
        <translation>ཁྱེད་ཚོའི་མ་ལག་ནང་དུ་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་ཉེར་སྤྱོད་གོ་རིམ་གསར་པ་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="280"/>
        <source>The backup restore partition could not be found. The system will not be backed up in this update!</source>
        <translation>རྗེས་གྲབས་སླར་གསོ་བྱེད་པའི་ཆ་ཤས་རྙེད་མི་ཐུབ། ཐེངས་འདིའི་གསར་སྒྱུར་ཁྲོད་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱེད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="284"/>
        <source>Kylin backup restore tool is doing other operations, please update later.</source>
        <translation>ཅིན་ལིན་གྱི་རྗེས་གྲབས་ཡོ་བྱད་སླར་གསོ་བྱེད་པའི་ཡོ་བྱད་ཀྱིས་ད་དུང་ལས་ཀ་གཞན་དག་བྱེད་བཞིན་ཡོད་པས། རྗེས་སུ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="289"/>
        <source>The source manager configuration file is abnormal, the system temporarily unable to update!</source>
        <translation>འབྱུང་ཁུངས་ཀྱི་སྤྱི་གཉེར་བའི་བཀོད་སྒྲིག་ཡིག་ཆ་རྒྱུན་ལྡན་མིན་པས་མ་ལག་གནས་སྐབས་སུ་གསར་སྒྱུར་བྱེད་ཐབས་བྲལ་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="294"/>
        <source>Backup already, no need to backup again.</source>
        <translation>རྗེས་གྲབས་བྱས་ཟིན་པས་སླར་ཡང་རྗེས་གྲབས་བྱེད་དགོས་དོན་མེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="303"/>
        <source>Start backup,getting progress</source>
        <translation>རྗེས་གྲབས་ལས་དོན་སྤེལ་འགོ་བཙུགས་ཏེ་ཡར་རྒྱས་ཡོང</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="319"/>
        <source>Kylin backup restore tool does not exist, this update will not backup the system!</source>
        <translation>ཅིན་ལིན་གྱི་རྗེས་གྲབས་སླར་གསོ་ཡོ་བྱད་མེད་པས། གསར་སྒྱུར་འདིས་མ་ལག་ལ་རྗེས་གྲབས་བྱེད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="228"/>
        <location filename="../src/tabwidget.cpp" line="335"/>
        <location filename="../src/tabwidget.cpp" line="341"/>
        <location filename="../src/tabwidget.cpp" line="362"/>
        <location filename="../src/tabwidget.cpp" line="667"/>
        <location filename="../src/tabwidget.cpp" line="1585"/>
        <location filename="../src/tabwidget.cpp" line="1635"/>
        <location filename="../src/tabwidget.cpp" line="1652"/>
        <location filename="../src/tabwidget.cpp" line="1739"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="351"/>
        <source>Calculated</source>
        <translation>རྩིས་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="436"/>
        <source>There are unresolved dependency conflicts in this update，Please contact the administrator!</source>
        <translation>གསར་སྒྱུར་འདིའི་ནང་དུ་ཐག་གཅོད་བྱས་མེད་པའི་གཞན་རྟེན་རང་བཞིན་གྱི་འགལ་བ་ཡོད་པས། དོ་དམ་པར་འབྲེལ་གཏུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="90"/>
        <location filename="../src/tabwidget.cpp" line="111"/>
        <location filename="../src/tabwidget.cpp" line="359"/>
        <location filename="../src/tabwidget.cpp" line="437"/>
        <location filename="../src/tabwidget.cpp" line="1736"/>
        <source>Prompt information</source>
        <translation>མགྱོགས་མྱུར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="180"/>
        <location filename="../src/tabwidget.cpp" line="1362"/>
        <location filename="../src/tabwidget.cpp" line="1412"/>
        <location filename="../src/tabwidget.cpp" line="1891"/>
        <location filename="../src/tabwidget.cpp" line="2056"/>
        <location filename="../src/tabwidget.cpp" line="2211"/>
        <location filename="../src/tabwidget.cpp" line="2258"/>
        <source>Your system is the latest:</source>
        <translation>ཁྱོད་ཀྱི་མ་ལག་ནི་པར་གཞི་ཆེས་གསར་བ་ཡིན</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="439"/>
        <source>Sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="633"/>
        <location filename="../src/tabwidget.cpp" line="636"/>
        <source>In the download</source>
        <translation>ཕབ་ལེན་བྱེད་པའི་ཁྲོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="633"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>རྩིས་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>In the install...</source>
        <translation type="vanished">安装中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="654"/>
        <source>Backup complete.</source>
        <translation>རྗེས་གྲབས་ལས་ཀ་ལེགས་འགྲུབ་བྱུང་བ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="672"/>
        <source>System is backing up...</source>
        <translation>མ་ལག་གིས་རྒྱབ་སྐྱོར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="690"/>
        <source>Backup interrupted, stop updating!</source>
        <translation>རྗེས་གྲབས་བྱེད་མཚམས་བཞག་ནས་གསར་སྒྱུར་བྱེད་མཚམས་འཇོག་དགོས།</translation>
    </message>
    <message>
        <source>Backup finished!</source>
        <translation type="vanished">备份完成！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="709"/>
        <source>Kylin backup restore tool exception:</source>
        <translation>ཅིན་ལིན་གྱི་རྗེས་གྲབས་ཡོ་བྱད་སླར་གསོ་བྱེད་པའི་ཡོ་བྱད་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="709"/>
        <source>There will be no backup in this update!</source>
        <translation>གསར་སྒྱུར་འདིའི་ནང་དུ་རྗེས་གྲབས་དཔུང་ཁག་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="707"/>
        <source>The status of backup completion is abnormal</source>
        <translatorcomment>备份完成状态异常</translatorcomment>
        <translation>རྗེས་གྲབས་ལེགས་འགྲུབ་བྱུང་བའི་གནས་ཚུལ་ནི་རྒྱུན་ལྡན་མིན་པ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="830"/>
        <source>Getting update list</source>
        <translation>གསར་སྒྱུར་གྱི་མིང་ཐོ་ཐོབ་པ།</translation>
    </message>
    <message>
        <source>Software source update successed: </source>
        <translatorcomment>软件源更新成功： </translatorcomment>
        <translation type="vanished">མཉེན་ཆས་ཀྱི་འབྱུང་ཁུངས་གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བ </translation>
    </message>
    <message>
        <source>Software source update failed: </source>
        <translation type="vanished">མཉེན་ཆས་ཀྱི་འབྱུང་ཁུངས་གསར་སྒྱུར་ལ་ཕམ་ཉེས་བྱུང </translation>
    </message>
    <message>
        <source>Update software source :</source>
        <translation type="vanished">更新软件源进度：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="236"/>
        <location filename="../src/tabwidget.cpp" line="976"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="358"/>
        <source>There are unresolved dependency conflicts in this update，Please select Dist-upgrade</source>
        <translatorcomment>本次更新存在无法解决的依赖冲突，请选择全盘更新</translatorcomment>
        <translation>ཐེངས་འདིའི་གསར་སྒྱུར་ཁྲོད་དུ་ཐག་གཅོད་བྱས་མེད་པའི་གཞན་རྟེན་རང་བཞིན་གྱི་འགལ་བ་ཡོད་པས། ཁྱེད་ཀྱིས་Dist-upgradeབདམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="361"/>
        <source>Dist-upgrade</source>
        <translatorcomment>全盘更新</translatorcomment>
        <translation>རྒྱང་རིང་གི་རིམ་པ་འཕར་བ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="520"/>
        <location filename="../src/tabwidget.cpp" line="521"/>
        <source>The system is downloading the update!</source>
        <translation>མ་ལག་གིས་གསར་སྒྱུར་ཕབ་ལེན་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="527"/>
        <location filename="../src/tabwidget.cpp" line="528"/>
        <source>Downloading the updates...</source>
        <translation>གནས་ཚུལ་གསར་པ་ཕབ་ལེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="532"/>
        <location filename="../src/tabwidget.cpp" line="533"/>
        <source>Installing the updates...</source>
        <translation>གསར་སྒྱུར་སྒྲིག་སྦྱོར་བྱེད་པ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="625"/>
        <location filename="../src/tabwidget.cpp" line="1636"/>
        <location filename="../src/tabwidget.cpp" line="1653"/>
        <source>In the install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པའི་ཁྲོད་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="862"/>
        <location filename="../src/tabwidget.cpp" line="1667"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="870"/>
        <location filename="../src/tabwidget.cpp" line="1912"/>
        <location filename="../src/tabwidget.cpp" line="2078"/>
        <location filename="../src/tabwidget.cpp" line="2160"/>
        <source>Network exception, unable to check for updates!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="876"/>
        <location filename="../src/tabwidget.cpp" line="1917"/>
        <location filename="../src/tabwidget.cpp" line="2083"/>
        <location filename="../src/tabwidget.cpp" line="2165"/>
        <source>No room to backup,upgrade failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="882"/>
        <location filename="../src/tabwidget.cpp" line="1923"/>
        <location filename="../src/tabwidget.cpp" line="2089"/>
        <location filename="../src/tabwidget.cpp" line="2171"/>
        <source>Battery level is below 50%,and upgrade failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="887"/>
        <source>Checking update failed! </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="888"/>
        <location filename="../src/tabwidget.cpp" line="1931"/>
        <location filename="../src/tabwidget.cpp" line="2096"/>
        <location filename="../src/tabwidget.cpp" line="2178"/>
        <source>Error Code: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="957"/>
        <source>The system is checking update :</source>
        <translation>མ་ལག་གིས་གནས་ཚུལ་གསར་བར་ཞིབ་བཤེར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Download Limit(Kb/s)</source>
        <translation type="vanished">下载限速(Kb/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1244"/>
        <source>View history</source>
        <translation>ལོ་རྒྱུས་ལ་ལྟ་ཞིབ་</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1085"/>
        <source>Update Settings</source>
        <translation>གསར་སྒྱུར་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1100"/>
        <source>Allowed to renewable notice</source>
        <translation>བསྐྱར་སྤྱོད་རུང་བའི་བརྡ་ཐོ་བཏང་ཆོག།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1116"/>
        <source>Backup current system before updates all</source>
        <translation>ཚང་མ་གསར་སྒྱུར་མ་བྱས་གོང་ལ་ད་ལྟ་སྤྱོད་བཞིན་</translation>
    </message>
    <message>
        <source>Download Limit(KB/s)</source>
        <translation type="vanished">下载限速(KB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1140"/>
        <source>It will be avaliable in the next download.</source>
        <translation>ཐེངས་རྗེས་མར་ཕབ་ལེན་བྱས་ན་འགན་འཁྲི་འཁུར་དགོས།</translation>
    </message>
    <message>
        <source>Automatically download and install updates</source>
        <translation type="vanished">自动下载和安装更新</translation>
    </message>
    <message>
        <source>After it is turned on, the system will automatically download and install updates when there is an available network and available backup and restore partitions.</source>
        <translation type="vanished">开启后，当有可用网络和可用备份和恢复分区时，系统会自动下载和安装更新。</translation>
    </message>
    <message>
        <source>Upgrade during poweroff</source>
        <translation type="vanished">关机检测更新</translation>
    </message>
    <message>
        <source>Download Limit(kB/s)</source>
        <translation type="vanished">下载限速(kB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1168"/>
        <source>The system will automatically updates when there is an available network and backup.</source>
        <translation>མ་ལག་འདི་ལ་ད་ཡོད་ཀྱི་དྲ་རྒྱ་དང་རྗེས་གྲབས་ཡོད་པའི་སྐབས་སུ་རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་སྲིད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1318"/>
        <source>Ready to install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པར་གྲ་སྒྲིག་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="188"/>
        <location filename="../src/tabwidget.cpp" line="261"/>
        <location filename="../src/tabwidget.cpp" line="899"/>
        <location filename="../src/tabwidget.cpp" line="952"/>
        <location filename="../src/tabwidget.cpp" line="1372"/>
        <location filename="../src/tabwidget.cpp" line="1392"/>
        <location filename="../src/tabwidget.cpp" line="1423"/>
        <location filename="../src/tabwidget.cpp" line="1460"/>
        <location filename="../src/tabwidget.cpp" line="1951"/>
        <location filename="../src/tabwidget.cpp" line="2116"/>
        <location filename="../src/tabwidget.cpp" line="2228"/>
        <source>Last Checked:</source>
        <translation>མཇུག་མཐར་ཞིབ་བཤེར་བྱས་པ་གཤམ་</translation>
    </message>
    <message>
        <source>Your system is the latest:V10sp1-</source>
        <translation type="vanished">你的系统已是最新版本：V10sp1-</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="181"/>
        <location filename="../src/tabwidget.cpp" line="254"/>
        <location filename="../src/tabwidget.cpp" line="892"/>
        <location filename="../src/tabwidget.cpp" line="945"/>
        <location filename="../src/tabwidget.cpp" line="1365"/>
        <location filename="../src/tabwidget.cpp" line="1385"/>
        <location filename="../src/tabwidget.cpp" line="1416"/>
        <location filename="../src/tabwidget.cpp" line="1944"/>
        <location filename="../src/tabwidget.cpp" line="2109"/>
        <location filename="../src/tabwidget.cpp" line="2221"/>
        <source>No information!</source>
        <translation>དཔྱད་གཞིའི་ཡིག་ཆ་མེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1132"/>
        <source>Download Limit</source>
        <translation>ཕབ་ལེན་གྱི་བཅད་གྲངས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1247"/>
        <source>Advanced</source>
        <translation>སྔོན་ཐོན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1539"/>
        <location filename="../src/tabwidget.cpp" line="1564"/>
        <source>Dependency conflict exists in this update,need to be completely repaired!</source>
        <translation>གསར་སྒྱུར་འདིའི་ཁྲོད་དུ་གཞན་རྟེན་རང་བཞིན་གྱི་འགལ་བ་ཡོད་པས་རྦད་དེ་ཉམས་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1541"/>
        <source>There are </source>
        <translation>དེ་རུ་ཡོད་པ་ནི། </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1541"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation> ཐུམ་སྒྲིལ་མེད་པར་བཟོ་རྒྱུ་རེད། ཁྱེད་ཀྱིས་དང་ལེན་བྱེད་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1566"/>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation>ཐུམ་སྒྲིལ་མེད་པར་བཟོ་རྒྱུ་རེད། ཁྱེད་ཀྱིས་དང་ལེན་བྱེད་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1577"/>
        <source>trying to reconnect </source>
        <translation>ཐབས་བརྒྱ་ཇུས་སྟོང་གིས་འབྲེལ་མཐུད་བྱེད་པ། </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1577"/>
        <source> times</source>
        <translation> དུས་རབས་</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>Auto-Update is backing up......</source>
        <translatorcomment>自动更新进程正在备份中......</translatorcomment>
        <translation type="vanished">རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་པར་རྒྱབ་སྐྱོར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1603"/>
        <source>The updater is NOT start</source>
        <translation>གསར་སྒྱུར་བྱེད་མཁན་གྱིས་གསར་སྒྱུར་བྱེད་འགོ་ཚུགས་མེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1621"/>
        <source>The progress is updating...</source>
        <translation>འཕེལ་རིམ་གསར་སྒྱུར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1631"/>
        <location filename="../src/tabwidget.cpp" line="1648"/>
        <source>The progress is installing...</source>
        <translation>འཕེལ་རིམ་སྒྲིག་སྦྱོར་བྱེད་བཞིན་པའི་སྒང་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1662"/>
        <source>The updater is busy！</source>
        <translation>གསར་སྒྱུར་བྱེད་མཁན་དེ་བྲེལ་བ་ཧ་ཅང་ཆེ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1680"/>
        <location filename="../src/tabwidget.cpp" line="1723"/>
        <source>Updating the software source</source>
        <translation>མཉེན་ཆས་ཀྱི་འབྱུང་ཁུངས་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="114"/>
        <location filename="../src/tabwidget.cpp" line="410"/>
        <location filename="../src/tabwidget.cpp" line="422"/>
        <location filename="../src/tabwidget.cpp" line="1706"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1735"/>
        <source>Please back up the system before all updates to avoid unnecessary losses</source>
        <translation>གསར་སྒྱུར་མ་བྱས་གོང་ལ་མ་ལག་ཕྱིར་འཐེན་བྱས་ནས་དགོས་མེད་ཀྱི་གྱོང་གུན་མི་ཡོང་བ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1740"/>
        <source>Only Update</source>
        <translation>གསར་སྒྱུར་ཁོ་ན་བྱེད་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1741"/>
        <source>Back And Update</source>
        <translation>རྒྱབ་ཕྱོགས་དང་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1904"/>
        <location filename="../src/tabwidget.cpp" line="2070"/>
        <location filename="../src/tabwidget.cpp" line="2151"/>
        <location filename="../src/tabwidget.cpp" line="2275"/>
        <source>update has been canceled!</source>
        <translation>གསར་སྒྱུར་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1886"/>
        <location filename="../src/tabwidget.cpp" line="1979"/>
        <source>This update has been completed！</source>
        <translatorcomment>本次更新已完成！</translatorcomment>
        <translation>གསར་སྒྱུར་འདི་ལེགས་འགྲུབ་བྱུང་སོང་།</translation>
    </message>
    <message>
        <source>Your system is the latest:V10</source>
        <translation type="vanished">ཁྱེད་ཚོའི་མ་ལག་ནི་ཆེས་གསར་བ་ཡིན། V10</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="227"/>
        <source>Keeping update</source>
        <translation>གསར་སྒྱུར་རྒྱུན་འཁྱོངས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="229"/>
        <source>It is recommended to back up the system before all updates to avoid unnecessary losses!</source>
        <translation>གསར་སྒྱུར་མ་བྱས་གོང་ལ་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱས་ནས་དགོས་མེད་ཀྱི་གྱོང་གུན་མི་ཡོང་བ་བྱ་རྒྱུའི་གྲོས་འགོ་བཏོན་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="408"/>
        <source>Insufficient disk space to download updates!</source>
        <translation>ཕབ་ལེན་བྱས་པའི་གསར་སྒྱུར་གྱི་བར་སྟོང་མི་འདང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="491"/>
        <source>supposed</source>
        <translation>ཚོད་དཔག་བྱས་པ་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="562"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="564"/>
        <source>min</source>
        <translation>ཟིན་བྲིས་གནད་བསྡུས།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="566"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1167"/>
        <source>Automatically updates</source>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་</translation>
    </message>
    <message>
        <source>Advanced Option</source>
        <translation type="vanished">ཚད་མཐོའི་གདམ་ཁ་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="418"/>
        <source>The update stopped because of low battery.</source>
        <translation>གློག་སྨན་དམའ་བའི་རྐྱེན་གྱིས་གསར་སྒྱུར་བྱེད་མཚམས་བཞག་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="419"/>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation>མ་ལག་གསར་སྒྱུར་བྱེད་པར་གློག་སྨན་གྱི་སྒུལ་ཤུགས་50%ལས་མི་ཉུང་བའི་བླང་བྱ་བཏོན་ཡོད།</translation>
    </message>
    <message>
        <source>Part of the update failed!</source>
        <translation type="vanished">གསར་སྒྱུར་གྱི་ཆ་ཤས་ཤིག་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>Failure reason:</source>
        <translatorcomment>失败原因：</translatorcomment>
        <translation type="vanished">ཕམ་ཉེས་བྱུང་བའི་རྒྱུ་རྐྱེན་ནི།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1972"/>
        <source>Finish the download!</source>
        <translation>ཕབ་ལེན་བྱས་ཚར་སོང་།</translation>
    </message>
    <message>
        <source>The system has download the update!</source>
        <translation type="vanished">མ་ལག་གིས་གསར་སྒྱུར་ཕབ་ལེན་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>It&apos;s need to reboot to make the install avaliable</source>
        <translation type="vanished">སྒྲིག་སྦྱོར་བྱས་ནས་སྒྲིག་སྦྱོར་བྱས་ན་འགྲིག་གི་རེད།</translation>
    </message>
    <message>
        <source>Reboot notification</source>
        <translation type="vanished">བསྐྱར་དུ་བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <source>Reboot rightnow</source>
        <translation type="vanished">གཡས་ཕྱོགས་པ་བསྐྱར་དུ་ཐོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="93"/>
        <source>Later</source>
        <translation>རྗེས་སུ།</translation>
    </message>
    <message>
        <source>Part of the update success!</source>
        <translation type="vanished">གསར་སྒྱུར་ལེགས་འགྲུབ་བྱུང་བའི་ཆ་ཤས་ཤིག་རེད།</translation>
    </message>
    <message>
        <source>All the update has been downloaded.</source>
        <translation type="vanished">གསར་སྒྱུར་ཚང་མ་ཕབ་ལེན་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2316"/>
        <source>An important update is in progress, please wait.</source>
        <translation>གལ་འགངས་ཆེ་བའི་གནས་ཚུལ་གསར་བ་ཞིག་ད་ལྟ་སྤེལ་བཞིན་ཡོད།རེ་སྒུག་</translation>
    </message>
    <message>
        <source>plase clean up your disk or expand the backup space</source>
        <translation type="vanished">ཁྱེད་ལ་གཙང་བཤེར་འཁོར་གཞོང་བར་སྟོང་འམ་རྒྱ་བསྐྱེད་རྗེས་གྲབས།འཛུགས་སྐྲུན་ཁག། </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2336"/>
        <source>insufficient backup space</source>
        <translation>གསོག་ཉར་ཁམས་མི་འདང་བ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2344"/>
        <source>backup failed</source>
        <translation>གསོག་ཉར་ཕམ་ཁ། </translation>
    </message>
    <message>
        <source>Failed to write configuration file, this update will not back up the system!</source>
        <translation type="vanished">བཀོད་སྒྲིག་ཡིག་ཆ་འབྲི་མ་ཐུབ་ན། གསར་སྒྱུར་འདིས་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱེད་མི་སྲིད།</translation>
    </message>
    <message>
        <source>Insufficient backup space, this update will not backup your system!</source>
        <translation type="vanished">རྗེས་གྲབས་ཀྱི་བར་སྟོང་མི་འདང་བས། གསར་སྒྱུར་འདིས་ཁྱེད་ཚོའི་མ་ལག་ལ་རྗེས་གྲབས་བྱེད་མི་སྲིད།</translation>
    </message>
    <message>
        <source>Kylin backup restore tool could not find the UUID, this update will not backup the system!</source>
        <translation type="vanished">ཅིན་ལིན་གྱི་རྗེས་གྲབས་ཡོ་བྱད་ཀྱིས་UUIDམ་རྙེད་པས། གསར་སྒྱུར་འདིས་མ་ལག་ལ་རྗེས་གྲབས་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>The backup restore partition is abnormal. You may not have a backup restore partition.For more details,see /var/log/backup.log</source>
        <translation type="vanished">备份还原分区异常，您可能没有备份还原分区。更多详细信息，可以参看/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1766"/>
        <location filename="../src/tabwidget.cpp" line="2352"/>
        <source>Calculating Capacity...</source>
        <translation>ནུས་པ་རྩིས་རྒྱག་པ...</translation>
    </message>
    <message>
        <source>The system backup partition is not detected. Do you want to continue updating?</source>
        <translation type="vanished">未检测到系统备份还原分区，是否继续更新？</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2475"/>
        <source>Calculating</source>
        <translation>རྩིས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2465"/>
        <location filename="../src/tabwidget.cpp" line="2488"/>
        <source>The system is updating...</source>
        <translation>ལམ་ལུགས་གསར་སྒྱུར་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="92"/>
        <source>Reboot</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1992"/>
        <location filename="../src/tabwidget.cpp" line="2204"/>
        <source>The system has download the update，and you are suggested to reboot to use the new version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1685"/>
        <location filename="../src/tabwidget.cpp" line="1993"/>
        <location filename="../src/tabwidget.cpp" line="2205"/>
        <source>reboot rightnow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1703"/>
        <source>Reboot failed!</source>
        <translation>བསྐྱར་སློང་ཕམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1930"/>
        <location filename="../src/tabwidget.cpp" line="2095"/>
        <location filename="../src/tabwidget.cpp" line="2177"/>
        <source>Update failed! </source>
        <translation>གསར་སྒྱུར་ལ་ཕམ་ཉེས་བྱུང་བ་རེད། </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2543"/>
        <source>Auto-Update progress is installing new file：</source>
        <translatorcomment>系统自动更新功能正在安装新文件：</translatorcomment>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་པའི་འཕེལ་རིམ་ནི་ཡིག་ཆ་གསར་པ་སྒྲིག་སྦྱོར་བྱེད་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2553"/>
        <source>Auto-Update progress finished!</source>
        <translatorcomment>系统自动更新完成！</translatorcomment>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་གྱི་འཕེལ་རིམ་ལེགས་འགྲུབ་བྱུང་</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2561"/>
        <source>Auto-Update progress fail in backup!</source>
        <translatorcomment>自动更新安装时备份失败！</translatorcomment>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་པའི་འཕེལ་རིམ་དེ་རྗེས་གྲབས་ལས་དོན་ལ་ཕམ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2584"/>
        <source>Failed in updating because of broken environment.</source>
        <translation>ཁོར་ཡུག་གཏོར་བཤིག་བཏང་བའི་རྐྱེན་གྱིས་གསར་སྒྱུར་བྱེད་མ་ཐུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2596"/>
        <source>It&apos;s fixing up the environment...</source>
        <translation>དེས་ཁོར་ཡུག་ཞིག་གསོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>UpdateDbus</name>
    <message>
        <location filename="../src/updatedbus.cpp" line="139"/>
        <source>System-Upgrade</source>
        <translation>མ་ལག་རིམ་སྤར།</translation>
    </message>
    <message>
        <location filename="../src/updatedbus.cpp" line="142"/>
        <source>ukui-control-center-update</source>
        <translation>ukui-control-center-update</translation>
    </message>
</context>
<context>
    <name>UpdateLog</name>
    <message>
        <location filename="../src/updatelog.cpp" line="23"/>
        <source>Update log</source>
        <translation>གསར་སྒྱུར་ཉིན་ཐོ།</translation>
    </message>
</context>
<context>
    <name>UpdateSource</name>
    <message>
        <location filename="../src/updatesource.cpp" line="64"/>
        <source>Connection failed, please reconnect!</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པས་ཡང་བསྐྱར་འབྲེལ་མཐུད་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>Upgrade</name>
    <message>
        <location filename="../upgrade.cpp" line="11"/>
        <source>Upgrade</source>
        <translation>རིམ་པ་སྤར་བ།</translation>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="65"/>
        <source>View history</source>
        <translation>ལོ་རྒྱུས་ལ་ལྟ་ཞིབ་</translation>
        <extra-contents_path>/Upgrade/View history</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="67"/>
        <source>Update Settings</source>
        <translation>གསར་སྒྱུར་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/Upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="69"/>
        <source>Allowed to renewable notice</source>
        <translation>བསྐྱར་སྤྱོད་རུང་བའི་བརྡ་ཐོ་བཏང་ཆོག།</translation>
        <extra-contents_path>/Upgrade/Allowed to renewable notice</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="71"/>
        <source>Automatically download and install updates</source>
        <translation>རང་འགུལ་གྱིས་ཕབ་ལེན་དང་སྒྲིག་སྦྱོར་བྱས་པའི་གནས་</translation>
        <extra-contents_path>/Upgrade/Automatically download and install updates</extra-contents_path>
    </message>
</context>
<context>
    <name>dependencyfixdialog</name>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="26"/>
        <source>show details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ་གསལ་བཤད་</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="35"/>
        <source>uninstall and update</source>
        <translation>སྒྲིག་སྦྱོར་དང་གསར་སྒྱུར་མ་བྱས་པ།</translation>
    </message>
    <message>
        <source>uninstall</source>
        <translation type="vanished">移除</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="37"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>fixbrokeninstalldialog</name>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="80"/>
        <source>We need to fix up the environment!</source>
        <translation>ང་ཚོས་ཁོར་ཡུག་ཞིག་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="86"/>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation>དེ་རུ་ཐུམ་སྒྲིལ་ཁ་ཤས་སྒྲིག་སྦྱོར་བྱས་ནས་གསར་སྒྱུར་ལེགས་འགྲུབ་བྱེད་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="95"/>
        <source>The following packages will be uninstalled:</source>
        <translation>གཤམ་གསལ་གྱི་ཐུམ་སྒྲིལ་དེ་དག་སྒྲིག་སྦྱོར་བྱས་མེད་པ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="120"/>
        <source>PKG Details</source>
        <translation>PKGཡི་ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="130"/>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="397"/>
        <source>details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="134"/>
        <source>Keep</source>
        <translation>ཉར་ཚགས་ཡག་པོ་</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="138"/>
        <source>Remove</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="294"/>
        <source>Attention on update</source>
        <translation>གསར་སྒྱུར་ལ་དོ་སྣང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="385"/>
        <source>back</source>
        <translation>རྒྱབ་ཕྱོགས་སུ་ཕྱིར་</translation>
    </message>
</context>
<context>
    <name>fixupdetaillist</name>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="59"/>
        <source>No content.</source>
        <translation>ནང་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="115"/>
        <source>Update Details</source>
        <translation>གནས་ཚུལ་ཞིབ་ཕྲ་གསར་སྒྱུར་བྱ་</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="549"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
</context>
<context>
    <name>m_updatelog</name>
    <message>
        <location filename="../src/m_updatelog.cpp" line="58"/>
        <source>No content.</source>
        <translation>ནང་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <source>Update Details</source>
        <translation type="vanished">གནས་ཚུལ་ཞིབ་ཕྲ་གསར་སྒྱུར་བྱ་</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="113"/>
        <source>No Contents</source>
        <translation>གནས་སྐབས་མེད་པའི་ནང་དོན། </translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="558"/>
        <location filename="../src/m_updatelog.cpp" line="594"/>
        <source>Search content</source>
        <translation>འཚོལ་བཤེར་གྱི་ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="641"/>
        <source>History Log</source>
        <translation>ལོ་རྒྱུས་ཀྱི་ཟིན་ཐོ།</translation>
    </message>
</context>
<context>
    <name>updatedeleteprompt</name>
    <message>
        <source>Dependency conflict exists in this update!</source>
        <translation type="vanished">本次更新存在依赖冲突！</translation>
    </message>
    <message>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation type="vanished">将卸载部分软件包以完成更新！</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="78"/>
        <source>The following packages will be uninstalled:</source>
        <translation>གཤམ་གསལ་གྱི་ཐུམ་སྒྲིལ་དེ་དག་སྒྲིག་སྦྱོར་བྱས་མེད་པ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="97"/>
        <source>PKG Details</source>
        <translation>PKGཡི་ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="241"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="105"/>
        <source>Keep</source>
        <translation>ཉར་ཚགས་ཡག་པོ་</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="109"/>
        <source>Remove</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Update Prompt</source>
        <translation type="vanished">更新提示</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
</context>
</TS>
