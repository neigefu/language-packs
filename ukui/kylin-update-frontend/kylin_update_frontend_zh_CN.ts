<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AppUpdateWid</name>
    <message>
        <location filename="../src/appupdate.cpp" line="302"/>
        <source>Cancel failed,Being installed</source>
        <translatorcomment>正在安装更新，无法取消。</translatorcomment>
        <translation>正在安装更新，无法取消。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="323"/>
        <source>Being installed</source>
        <translation>系统正在安装更新。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="390"/>
        <source>Download succeeded!</source>
        <translation>下载完成。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="482"/>
        <location filename="../src/appupdate.cpp" line="484"/>
        <location filename="../src/appupdate.cpp" line="485"/>
        <source>Update succeeded , It is recommended that you restart later!</source>
        <translation>更新成功，请稍后重启系统。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="161"/>
        <location filename="../src/appupdate.cpp" line="162"/>
        <location filename="../src/appupdate.cpp" line="167"/>
        <location filename="../src/appupdate.cpp" line="183"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="356"/>
        <location filename="../src/appupdate.cpp" line="359"/>
        <location filename="../src/appupdate.cpp" line="360"/>
        <source>Download finished,it is recommended that you restart later to use the new version.</source>
        <translation>更新下载已完成，请重启系统完成安装。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="365"/>
        <location filename="../src/appupdate.cpp" line="393"/>
        <location filename="../src/appupdate.cpp" line="813"/>
        <source>reboot</source>
        <translation>立即重启</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="371"/>
        <location filename="../src/appupdate.cpp" line="374"/>
        <location filename="../src/appupdate.cpp" line="375"/>
        <location filename="../src/appupdate.cpp" line="489"/>
        <location filename="../src/appupdate.cpp" line="491"/>
        <location filename="../src/appupdate.cpp" line="492"/>
        <source>Update succeeded , It is recommended that you log out later and log in again!</source>
        <translation>更新成功，建议稍后注销后重新登录！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="398"/>
        <location filename="../src/appupdate.cpp" line="404"/>
        <location filename="../src/appupdate.cpp" line="495"/>
        <location filename="../src/appupdate.cpp" line="500"/>
        <source>Update succeeded!</source>
        <translation>更新成功。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="413"/>
        <location filename="../src/appupdate.cpp" line="511"/>
        <location filename="../src/appupdate.cpp" line="633"/>
        <location filename="../src/appupdate.cpp" line="649"/>
        <location filename="../src/appupdate.cpp" line="672"/>
        <location filename="../src/appupdate.cpp" line="1390"/>
        <source>Update has been canceled!</source>
        <translation>本次更新已取消。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="424"/>
        <location filename="../src/appupdate.cpp" line="430"/>
        <location filename="../src/appupdate.cpp" line="447"/>
        <location filename="../src/appupdate.cpp" line="522"/>
        <location filename="../src/appupdate.cpp" line="531"/>
        <source>Update failed!</source>
        <translation>更新异常！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="427"/>
        <location filename="../src/appupdate.cpp" line="525"/>
        <source>Failure reason:</source>
        <translation>异常原因：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="548"/>
        <source>Install detect error</source>
        <translation>更新检测错误</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="627"/>
        <location filename="../src/appupdate.cpp" line="1308"/>
        <source>Prepare to backup</source>
        <translation>备份准备中。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1319"/>
        <source>backup progress:</source>
        <translation>备份进度：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1344"/>
        <source>backup finished</source>
        <translation>备份完成。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1357"/>
        <source>backup failed</source>
        <translation>备份异常。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1359"/>
        <source>backup failed,continue upgrade?</source>
        <translation>无法备份，是否继续更新？</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1363"/>
        <source>Continue to Update</source>
        <translation>继续更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="561"/>
        <source>There are unresolved dependency conflicts in this update，Please select update all</source>
        <translation>本次更新出现异常，请选择“全部更新”尝试修复。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="562"/>
        <location filename="../src/appupdate.cpp" line="845"/>
        <location filename="../src/appupdate.cpp" line="1360"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="564"/>
        <source>Update ALL</source>
        <translation>全部更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="565"/>
        <location filename="../src/appupdate.cpp" line="661"/>
        <location filename="../src/appupdate.cpp" line="848"/>
        <location filename="../src/appupdate.cpp" line="880"/>
        <location filename="../src/appupdate.cpp" line="934"/>
        <location filename="../src/appupdate.cpp" line="1362"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="612"/>
        <source>No Content.</source>
        <translation>适用于当前系统的累积更新。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="619"/>
        <source>There are </source>
        <translation>本次更新出现异常，将卸载</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="619"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation>个软件包尝试修复，是否继续？</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="772"/>
        <location filename="../src/appupdate.cpp" line="795"/>
        <source>Cumulative updates</source>
        <translation>适用于当前系统的累积更新。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="756"/>
        <location filename="../src/appupdate.cpp" line="773"/>
        <location filename="../src/appupdate.cpp" line="796"/>
        <source>version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="74"/>
        <location filename="../src/appupdate.cpp" line="422"/>
        <location filename="../src/appupdate.cpp" line="520"/>
        <location filename="../src/appupdate.cpp" line="634"/>
        <location filename="../src/appupdate.cpp" line="650"/>
        <location filename="../src/appupdate.cpp" line="673"/>
        <location filename="../src/appupdate.cpp" line="832"/>
        <location filename="../src/appupdate.cpp" line="885"/>
        <location filename="../src/appupdate.cpp" line="959"/>
        <location filename="../src/appupdate.cpp" line="1073"/>
        <location filename="../src/appupdate.cpp" line="1391"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="66"/>
        <source>details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="112"/>
        <location filename="../src/appupdate.cpp" line="182"/>
        <source>Update log</source>
        <translation>更新日志</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="192"/>
        <source>Download size:</source>
        <translation>下载大小：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="193"/>
        <source>Install size:</source>
        <translation>安装大小：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="219"/>
        <source>Current version:</source>
        <translation>当前版本：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="844"/>
        <source>A single update will not automatically backup the system, if you want to backup, please click Update All.</source>
        <translation>单个更新不会自动备份系统，如需备份，请点击全部更新。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="847"/>
        <source>Do not backup, continue to update</source>
        <translation>不备份，继续更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="851"/>
        <source>This time will no longer prompt</source>
        <translation>本次更新不再提示</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="936"/>
        <source>Ready to update</source>
        <translatorcomment>准备更新</translatorcomment>
        <translation>准备更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="958"/>
        <source>The updater is busy！</source>
        <translation>其他应用正在安装软件包，请稍后再试</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1049"/>
        <source>downloaded</source>
        <translatorcomment>已下载</translatorcomment>
        <translation>已下载</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1055"/>
        <location filename="../src/appupdate.cpp" line="1058"/>
        <source>downloading</source>
        <translation>下载中</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1055"/>
        <source>calculating</source>
        <translatorcomment>检测中</translatorcomment>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1138"/>
        <source>No content.</source>
        <translation>适用于当前系统的累积更新</translation>
    </message>
</context>
<context>
    <name>DateTimeUtils</name>
    <message>
        <location filename="../src/AddFunction/datetimeutils.cpp" line="41"/>
        <source>No information!</source>
        <translation>无可用更新</translation>
    </message>
</context>
<context>
    <name>DeletePkgListWig</name>
    <message>
        <location filename="../src/deletepkglistwig.cpp" line="85"/>
        <source>No Content.</source>
        <translation>适用于当前系统的累积更新</translation>
    </message>
</context>
<context>
    <name>HistoryUpdateListWig</name>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="116"/>
        <source>Success</source>
        <translation>更新成功</translation>
    </message>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="122"/>
        <source>Failed</source>
        <translation>更新失败</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="94"/>
        <source>Advanced Option</source>
        <translation>高级选项</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="131"/>
        <source>Server address settings</source>
        <translation>服务器地址设置</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="73"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="87"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="134"/>
        <source>If internal services, change the server address.</source>
        <translation>如果想使用内部更新服务，可以更换服务器地址和端口</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="148"/>
        <source>Port  ID </source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="157"/>
        <source>Address</source>
        <translation>服务器地址</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="222"/>
        <source>update period</source>
        <translation>自动下载更新周期</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="229"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="241"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="542"/>
        <source>1 day</source>
        <translation>一天</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="229"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="546"/>
        <source>7 days</source>
        <translation>七天</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="229"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="249"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="524"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="550"/>
        <source>1 month</source>
        <translation>一个月</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="229"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="253"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="554"/>
        <source>3 months</source>
        <translation>三个月</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="229"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="257"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="261"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="558"/>
        <source>half a year</source>
        <translation>半年</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="229"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="237"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="562"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="571"/>
        <source>never</source>
        <translation>从不</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="234"/>
        <source>a month</source>
        <translation>一个月</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="245"/>
        <source>7 day</source>
        <translation>七天</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="279"/>
        <source>during the work time,there&apos;s no download</source>
        <translation>在指定时段内，不会自动下载更新</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="308"/>
        <source>work time</source>
        <translation>指定时间段</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="310"/>
        <source>to</source>
        <translation>至</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="380"/>
        <source>reset</source>
        <translation>恢复默认</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="386"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="389"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="607"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="604"/>
        <source>Modification failed!</source>
        <translation>修改失败，请尝试重启系统。</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="605"/>
        <source>Attention</source>
        <translation>提示</translation>
    </message>
</context>
<context>
    <name>TabWid</name>
    <message>
        <location filename="../src/tabwidget.cpp" line="126"/>
        <location filename="../src/tabwidget.cpp" line="1741"/>
        <location filename="../src/tabwidget.cpp" line="2146"/>
        <location filename="../src/tabwidget.cpp" line="2673"/>
        <location filename="../src/tabwidget.cpp" line="3251"/>
        <location filename="../src/tabwidget.cpp" line="3318"/>
        <location filename="../src/tabwidget.cpp" line="3663"/>
        <source>Check Update</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1764"/>
        <location filename="../src/tabwidget.cpp" line="2054"/>
        <location filename="../src/tabwidget.cpp" line="2162"/>
        <location filename="../src/tabwidget.cpp" line="2335"/>
        <location filename="../src/tabwidget.cpp" line="2972"/>
        <location filename="../src/tabwidget.cpp" line="3335"/>
        <location filename="../src/tabwidget.cpp" line="3367"/>
        <location filename="../src/tabwidget.cpp" line="3475"/>
        <location filename="../src/tabwidget.cpp" line="3739"/>
        <source>UpdateAll</source>
        <translation>全部更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1289"/>
        <location filename="../src/tabwidget.cpp" line="1822"/>
        <location filename="../src/tabwidget.cpp" line="4223"/>
        <source>No Information!</source>
        <translation>系统未更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1787"/>
        <location filename="../src/tabwidget.cpp" line="1791"/>
        <source>Updatable app detected on your system!</source>
        <translation>检测到系统有可更新的应用。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="4082"/>
        <source>Start backup,getting progress</source>
        <translation>等待备份中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="359"/>
        <location filename="../src/tabwidget.cpp" line="365"/>
        <location filename="../src/tabwidget.cpp" line="390"/>
        <location filename="../src/tabwidget.cpp" line="2023"/>
        <location filename="../src/tabwidget.cpp" line="2072"/>
        <location filename="../src/tabwidget.cpp" line="2089"/>
        <location filename="../src/tabwidget.cpp" line="2206"/>
        <location filename="../src/tabwidget.cpp" line="2520"/>
        <location filename="../src/tabwidget.cpp" line="3117"/>
        <location filename="../src/tabwidget.cpp" line="3494"/>
        <location filename="../src/tabwidget.cpp" line="3511"/>
        <location filename="../src/tabwidget.cpp" line="3540"/>
        <location filename="../src/tabwidget.cpp" line="3914"/>
        <location filename="../src/tabwidget.cpp" line="3936"/>
        <location filename="../src/tabwidget.cpp" line="4154"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="375"/>
        <source>Calculated</source>
        <translation>计算完成</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="694"/>
        <source>There are unresolved dependency conflicts in this update，Please contact the administrator!</source>
        <translation>本次更新存在异常，请联系管理员！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="206"/>
        <location filename="../src/tabwidget.cpp" line="227"/>
        <location filename="../src/tabwidget.cpp" line="387"/>
        <location filename="../src/tabwidget.cpp" line="695"/>
        <location filename="../src/tabwidget.cpp" line="2203"/>
        <location filename="../src/tabwidget.cpp" line="2518"/>
        <location filename="../src/tabwidget.cpp" line="3115"/>
        <location filename="../src/tabwidget.cpp" line="3912"/>
        <location filename="../src/tabwidget.cpp" line="3934"/>
        <location filename="../src/tabwidget.cpp" line="4152"/>
        <source>Prompt information</source>
        <translation>提示信息</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1744"/>
        <location filename="../src/tabwidget.cpp" line="2379"/>
        <location filename="../src/tabwidget.cpp" line="2700"/>
        <location filename="../src/tabwidget.cpp" line="3272"/>
        <location filename="../src/tabwidget.cpp" line="3320"/>
        <source>Your system is the latest:</source>
        <translation>系统已是最新：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="697"/>
        <source>Sure</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="882"/>
        <location filename="../src/tabwidget.cpp" line="885"/>
        <source>In the download</source>
        <translation>下载中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="882"/>
        <source>calculating</source>
        <translatorcomment>检测中</translatorcomment>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1012"/>
        <source>Getting update list</source>
        <translation>正在获取更新内容</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3570"/>
        <location filename="../src/tabwidget.cpp" line="3625"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="386"/>
        <source>There are unresolved dependency conflicts in this update，Please select Dist-upgrade</source>
        <translatorcomment>本次更新出现异常，请选择“全盘更新”。</translatorcomment>
        <translation>本次更新出现异常，请选择“全盘更新”。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="389"/>
        <source>Dist-upgrade</source>
        <translatorcomment>全盘更新</translatorcomment>
        <translation>全盘更新</translation>
    </message>
    <message>
        <source>Install detect error:</source>
        <translation type="vanished">安装前检测错误：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="770"/>
        <location filename="../src/tabwidget.cpp" line="771"/>
        <source>The system is downloading the update!</source>
        <translation>正在下载更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="777"/>
        <location filename="../src/tabwidget.cpp" line="778"/>
        <source>Downloading the updates...</source>
        <translation>正在下载更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="782"/>
        <location filename="../src/tabwidget.cpp" line="783"/>
        <source>Installing the updates...</source>
        <translation>正在安装更新......</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2073"/>
        <location filename="../src/tabwidget.cpp" line="2090"/>
        <location filename="../src/tabwidget.cpp" line="3495"/>
        <location filename="../src/tabwidget.cpp" line="3512"/>
        <source>In the install</source>
        <translation>正在下载补丁并自动更新中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1055"/>
        <location filename="../src/tabwidget.cpp" line="2103"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1332"/>
        <source>The system is checking update :</source>
        <translation>正在检测更新：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1617"/>
        <source>View history</source>
        <translation>更新历史</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1432"/>
        <source>Update Settings</source>
        <translation>更新设置</translation>
        <extra-contents_path>/upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1447"/>
        <source>Allowed to renewable notice</source>
        <translation>有可更新内容时通知</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1474"/>
        <source>It will be avaliable in the next download.</source>
        <translation>开启后会在下次下载时进行限速</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1496"/>
        <source>The system will automatically updates when there is an available network and backup.</source>
        <translation>网络可用时，若存在备份，系统会自动下载并安装更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1698"/>
        <source>Ready to install</source>
        <translation>准备安装更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1327"/>
        <location filename="../src/tabwidget.cpp" line="1754"/>
        <location filename="../src/tabwidget.cpp" line="1774"/>
        <location filename="../src/tabwidget.cpp" line="1832"/>
        <location filename="../src/tabwidget.cpp" line="3877"/>
        <source>Last Checked:</source>
        <translation>上次检查时间：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="136"/>
        <source>Version Verify</source>
        <translation>版本检测</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="138"/>
        <source>The updatable content is inconsistent with the current version. Do you want to continue?</source>
        <translation>可更新内容与当前版本不匹配，您是否要继续更新？</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="139"/>
        <source>Continuing to update may cause system abnormalities.</source>
        <translation>继续更新可能导致系统异常。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="142"/>
        <source>Accept</source>
        <translation>继续更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="143"/>
        <source>Reject</source>
        <translation>取消更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="157"/>
        <source>interface is error!</source>
        <translation>更新接口存在异常！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1320"/>
        <location filename="../src/tabwidget.cpp" line="1747"/>
        <location filename="../src/tabwidget.cpp" line="1767"/>
        <source>No information!</source>
        <translation>尚未更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1351"/>
        <source>SystemUpdate</source>
        <translation>系统更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1466"/>
        <source>Download Limit</source>
        <translation>下载限速</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1495"/>
        <source>Automatically download updates</source>
        <translation>自动下载更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1515"/>
        <source>Accept beta version and </source>
        <translation>优先体验最新版本，并</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1517"/>
        <source>to help improving the system.</source>
        <translation>协助改进系统</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1519"/>
        <source>provide feedback</source>
        <translation>提供反馈</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1620"/>
        <source>Advanced</source>
        <translation>高级选项</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1978"/>
        <location filename="../src/tabwidget.cpp" line="2004"/>
        <source>Dependency conflict exists in this update,need to be completely repaired!</source>
        <translation>本次更新存在异常，需要全盘修复以完成更新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1980"/>
        <source>There are </source>
        <translation>本次更新存在异常，将卸载</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1980"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation>个软件包以完成本次更新。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2006"/>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation>个软件包将被卸载，请确认是否接受！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2014"/>
        <source>trying to reconnect </source>
        <translation>重新尝试连接 </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2014"/>
        <source> times</source>
        <translation>次数</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2689"/>
        <location filename="../src/tabwidget.cpp" line="3262"/>
        <source>Updates ready，please reboot.</source>
        <translation>下载完成，需要重启系统进行更新。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2517"/>
        <location filename="../src/tabwidget.cpp" line="3114"/>
        <source>Install failed,you can restore your system</source>
        <translation>安装失败，可以尝试还原系统。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2521"/>
        <location filename="../src/tabwidget.cpp" line="3118"/>
        <location filename="../src/tabwidget.cpp" line="3937"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>updates ready，please reboot.</source>
        <translation type="vanished">下载完成，需要重启系统进行更新。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3911"/>
        <source>Please make sure your important files are saved before restore</source>
        <translation>还原前，确保重要文件已备份到其他存储设备</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3915"/>
        <source>Continue to Restore</source>
        <translation>继续还原</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2532"/>
        <location filename="../src/tabwidget.cpp" line="3129"/>
        <location filename="../src/tabwidget.cpp" line="3922"/>
        <source>restoring the system,please wait...</source>
        <translation>正在还原系统，请稍候...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="469"/>
        <location filename="../src/tabwidget.cpp" line="478"/>
        <location filename="../src/tabwidget.cpp" line="487"/>
        <location filename="../src/tabwidget.cpp" line="496"/>
        <location filename="../src/tabwidget.cpp" line="505"/>
        <location filename="../src/tabwidget.cpp" line="514"/>
        <location filename="../src/tabwidget.cpp" line="523"/>
        <location filename="../src/tabwidget.cpp" line="532"/>
        <location filename="../src/tabwidget.cpp" line="541"/>
        <location filename="../src/tabwidget.cpp" line="550"/>
        <location filename="../src/tabwidget.cpp" line="559"/>
        <location filename="../src/tabwidget.cpp" line="568"/>
        <location filename="../src/tabwidget.cpp" line="577"/>
        <location filename="../src/tabwidget.cpp" line="586"/>
        <location filename="../src/tabwidget.cpp" line="595"/>
        <location filename="../src/tabwidget.cpp" line="604"/>
        <location filename="../src/tabwidget.cpp" line="613"/>
        <location filename="../src/tabwidget.cpp" line="622"/>
        <location filename="../src/tabwidget.cpp" line="631"/>
        <location filename="../src/tabwidget.cpp" line="640"/>
        <location filename="../src/tabwidget.cpp" line="649"/>
        <location filename="../src/tabwidget.cpp" line="658"/>
        <location filename="../src/tabwidget.cpp" line="667"/>
        <location filename="../src/tabwidget.cpp" line="1065"/>
        <location filename="../src/tabwidget.cpp" line="1074"/>
        <location filename="../src/tabwidget.cpp" line="1083"/>
        <location filename="../src/tabwidget.cpp" line="1092"/>
        <location filename="../src/tabwidget.cpp" line="1101"/>
        <location filename="../src/tabwidget.cpp" line="1110"/>
        <location filename="../src/tabwidget.cpp" line="1119"/>
        <location filename="../src/tabwidget.cpp" line="1128"/>
        <location filename="../src/tabwidget.cpp" line="1137"/>
        <location filename="../src/tabwidget.cpp" line="1146"/>
        <location filename="../src/tabwidget.cpp" line="1155"/>
        <location filename="../src/tabwidget.cpp" line="1164"/>
        <location filename="../src/tabwidget.cpp" line="1173"/>
        <location filename="../src/tabwidget.cpp" line="1182"/>
        <location filename="../src/tabwidget.cpp" line="1191"/>
        <location filename="../src/tabwidget.cpp" line="1200"/>
        <location filename="../src/tabwidget.cpp" line="1209"/>
        <location filename="../src/tabwidget.cpp" line="1218"/>
        <location filename="../src/tabwidget.cpp" line="1227"/>
        <location filename="../src/tabwidget.cpp" line="1236"/>
        <location filename="../src/tabwidget.cpp" line="1245"/>
        <location filename="../src/tabwidget.cpp" line="1254"/>
        <location filename="../src/tabwidget.cpp" line="1263"/>
        <location filename="../src/tabwidget.cpp" line="2407"/>
        <location filename="../src/tabwidget.cpp" line="2416"/>
        <location filename="../src/tabwidget.cpp" line="2425"/>
        <location filename="../src/tabwidget.cpp" line="2434"/>
        <location filename="../src/tabwidget.cpp" line="2443"/>
        <location filename="../src/tabwidget.cpp" line="2452"/>
        <location filename="../src/tabwidget.cpp" line="2461"/>
        <location filename="../src/tabwidget.cpp" line="2470"/>
        <location filename="../src/tabwidget.cpp" line="2479"/>
        <location filename="../src/tabwidget.cpp" line="2488"/>
        <location filename="../src/tabwidget.cpp" line="2497"/>
        <location filename="../src/tabwidget.cpp" line="2506"/>
        <location filename="../src/tabwidget.cpp" line="2538"/>
        <location filename="../src/tabwidget.cpp" line="2548"/>
        <location filename="../src/tabwidget.cpp" line="2562"/>
        <location filename="../src/tabwidget.cpp" line="2571"/>
        <location filename="../src/tabwidget.cpp" line="2580"/>
        <location filename="../src/tabwidget.cpp" line="2589"/>
        <location filename="../src/tabwidget.cpp" line="2598"/>
        <location filename="../src/tabwidget.cpp" line="2607"/>
        <location filename="../src/tabwidget.cpp" line="2616"/>
        <location filename="../src/tabwidget.cpp" line="2625"/>
        <location filename="../src/tabwidget.cpp" line="2634"/>
        <location filename="../src/tabwidget.cpp" line="2643"/>
        <location filename="../src/tabwidget.cpp" line="2729"/>
        <location filename="../src/tabwidget.cpp" line="2738"/>
        <location filename="../src/tabwidget.cpp" line="2747"/>
        <location filename="../src/tabwidget.cpp" line="2756"/>
        <location filename="../src/tabwidget.cpp" line="2765"/>
        <location filename="../src/tabwidget.cpp" line="2774"/>
        <location filename="../src/tabwidget.cpp" line="2783"/>
        <location filename="../src/tabwidget.cpp" line="2792"/>
        <location filename="../src/tabwidget.cpp" line="2801"/>
        <location filename="../src/tabwidget.cpp" line="2810"/>
        <location filename="../src/tabwidget.cpp" line="2819"/>
        <location filename="../src/tabwidget.cpp" line="2828"/>
        <location filename="../src/tabwidget.cpp" line="2837"/>
        <location filename="../src/tabwidget.cpp" line="2846"/>
        <location filename="../src/tabwidget.cpp" line="2855"/>
        <location filename="../src/tabwidget.cpp" line="2864"/>
        <location filename="../src/tabwidget.cpp" line="2873"/>
        <location filename="../src/tabwidget.cpp" line="2882"/>
        <location filename="../src/tabwidget.cpp" line="2891"/>
        <location filename="../src/tabwidget.cpp" line="2900"/>
        <location filename="../src/tabwidget.cpp" line="2909"/>
        <location filename="../src/tabwidget.cpp" line="2918"/>
        <location filename="../src/tabwidget.cpp" line="2927"/>
        <location filename="../src/tabwidget.cpp" line="3004"/>
        <location filename="../src/tabwidget.cpp" line="3013"/>
        <location filename="../src/tabwidget.cpp" line="3022"/>
        <location filename="../src/tabwidget.cpp" line="3031"/>
        <location filename="../src/tabwidget.cpp" line="3040"/>
        <location filename="../src/tabwidget.cpp" line="3049"/>
        <location filename="../src/tabwidget.cpp" line="3058"/>
        <location filename="../src/tabwidget.cpp" line="3067"/>
        <location filename="../src/tabwidget.cpp" line="3076"/>
        <location filename="../src/tabwidget.cpp" line="3085"/>
        <location filename="../src/tabwidget.cpp" line="3094"/>
        <location filename="../src/tabwidget.cpp" line="3103"/>
        <location filename="../src/tabwidget.cpp" line="3147"/>
        <location filename="../src/tabwidget.cpp" line="3156"/>
        <location filename="../src/tabwidget.cpp" line="3165"/>
        <location filename="../src/tabwidget.cpp" line="3174"/>
        <location filename="../src/tabwidget.cpp" line="3183"/>
        <location filename="../src/tabwidget.cpp" line="3192"/>
        <location filename="../src/tabwidget.cpp" line="3201"/>
        <location filename="../src/tabwidget.cpp" line="3210"/>
        <location filename="../src/tabwidget.cpp" line="3219"/>
        <location filename="../src/tabwidget.cpp" line="3228"/>
        <source>Update exception!</source>
        <translation>更新异常！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="470"/>
        <location filename="../src/tabwidget.cpp" line="479"/>
        <location filename="../src/tabwidget.cpp" line="488"/>
        <location filename="../src/tabwidget.cpp" line="497"/>
        <location filename="../src/tabwidget.cpp" line="506"/>
        <location filename="../src/tabwidget.cpp" line="515"/>
        <location filename="../src/tabwidget.cpp" line="524"/>
        <location filename="../src/tabwidget.cpp" line="533"/>
        <location filename="../src/tabwidget.cpp" line="542"/>
        <location filename="../src/tabwidget.cpp" line="551"/>
        <location filename="../src/tabwidget.cpp" line="560"/>
        <location filename="../src/tabwidget.cpp" line="569"/>
        <location filename="../src/tabwidget.cpp" line="578"/>
        <location filename="../src/tabwidget.cpp" line="587"/>
        <location filename="../src/tabwidget.cpp" line="596"/>
        <location filename="../src/tabwidget.cpp" line="605"/>
        <location filename="../src/tabwidget.cpp" line="614"/>
        <location filename="../src/tabwidget.cpp" line="623"/>
        <location filename="../src/tabwidget.cpp" line="632"/>
        <location filename="../src/tabwidget.cpp" line="641"/>
        <location filename="../src/tabwidget.cpp" line="650"/>
        <location filename="../src/tabwidget.cpp" line="659"/>
        <location filename="../src/tabwidget.cpp" line="668"/>
        <location filename="../src/tabwidget.cpp" line="1066"/>
        <location filename="../src/tabwidget.cpp" line="1075"/>
        <location filename="../src/tabwidget.cpp" line="1084"/>
        <location filename="../src/tabwidget.cpp" line="1093"/>
        <location filename="../src/tabwidget.cpp" line="1102"/>
        <location filename="../src/tabwidget.cpp" line="1111"/>
        <location filename="../src/tabwidget.cpp" line="1120"/>
        <location filename="../src/tabwidget.cpp" line="1129"/>
        <location filename="../src/tabwidget.cpp" line="1138"/>
        <location filename="../src/tabwidget.cpp" line="1147"/>
        <location filename="../src/tabwidget.cpp" line="1156"/>
        <location filename="../src/tabwidget.cpp" line="1165"/>
        <location filename="../src/tabwidget.cpp" line="1174"/>
        <location filename="../src/tabwidget.cpp" line="1183"/>
        <location filename="../src/tabwidget.cpp" line="1192"/>
        <location filename="../src/tabwidget.cpp" line="1201"/>
        <location filename="../src/tabwidget.cpp" line="1210"/>
        <location filename="../src/tabwidget.cpp" line="1219"/>
        <location filename="../src/tabwidget.cpp" line="1228"/>
        <location filename="../src/tabwidget.cpp" line="1237"/>
        <location filename="../src/tabwidget.cpp" line="1246"/>
        <location filename="../src/tabwidget.cpp" line="1255"/>
        <location filename="../src/tabwidget.cpp" line="1264"/>
        <location filename="../src/tabwidget.cpp" line="2408"/>
        <location filename="../src/tabwidget.cpp" line="2417"/>
        <location filename="../src/tabwidget.cpp" line="2426"/>
        <location filename="../src/tabwidget.cpp" line="2435"/>
        <location filename="../src/tabwidget.cpp" line="2444"/>
        <location filename="../src/tabwidget.cpp" line="2453"/>
        <location filename="../src/tabwidget.cpp" line="2462"/>
        <location filename="../src/tabwidget.cpp" line="2471"/>
        <location filename="../src/tabwidget.cpp" line="2480"/>
        <location filename="../src/tabwidget.cpp" line="2489"/>
        <location filename="../src/tabwidget.cpp" line="2498"/>
        <location filename="../src/tabwidget.cpp" line="2507"/>
        <location filename="../src/tabwidget.cpp" line="2539"/>
        <location filename="../src/tabwidget.cpp" line="2549"/>
        <location filename="../src/tabwidget.cpp" line="2563"/>
        <location filename="../src/tabwidget.cpp" line="2572"/>
        <location filename="../src/tabwidget.cpp" line="2581"/>
        <location filename="../src/tabwidget.cpp" line="2590"/>
        <location filename="../src/tabwidget.cpp" line="2599"/>
        <location filename="../src/tabwidget.cpp" line="2608"/>
        <location filename="../src/tabwidget.cpp" line="2617"/>
        <location filename="../src/tabwidget.cpp" line="2626"/>
        <location filename="../src/tabwidget.cpp" line="2635"/>
        <location filename="../src/tabwidget.cpp" line="2644"/>
        <location filename="../src/tabwidget.cpp" line="2730"/>
        <location filename="../src/tabwidget.cpp" line="2739"/>
        <location filename="../src/tabwidget.cpp" line="2748"/>
        <location filename="../src/tabwidget.cpp" line="2757"/>
        <location filename="../src/tabwidget.cpp" line="2766"/>
        <location filename="../src/tabwidget.cpp" line="2775"/>
        <location filename="../src/tabwidget.cpp" line="2784"/>
        <location filename="../src/tabwidget.cpp" line="2793"/>
        <location filename="../src/tabwidget.cpp" line="2802"/>
        <location filename="../src/tabwidget.cpp" line="2811"/>
        <location filename="../src/tabwidget.cpp" line="2820"/>
        <location filename="../src/tabwidget.cpp" line="2829"/>
        <location filename="../src/tabwidget.cpp" line="2838"/>
        <location filename="../src/tabwidget.cpp" line="2847"/>
        <location filename="../src/tabwidget.cpp" line="2856"/>
        <location filename="../src/tabwidget.cpp" line="2865"/>
        <location filename="../src/tabwidget.cpp" line="2874"/>
        <location filename="../src/tabwidget.cpp" line="2883"/>
        <location filename="../src/tabwidget.cpp" line="2892"/>
        <location filename="../src/tabwidget.cpp" line="2901"/>
        <location filename="../src/tabwidget.cpp" line="2910"/>
        <location filename="../src/tabwidget.cpp" line="2919"/>
        <location filename="../src/tabwidget.cpp" line="2928"/>
        <location filename="../src/tabwidget.cpp" line="3005"/>
        <location filename="../src/tabwidget.cpp" line="3014"/>
        <location filename="../src/tabwidget.cpp" line="3023"/>
        <location filename="../src/tabwidget.cpp" line="3032"/>
        <location filename="../src/tabwidget.cpp" line="3041"/>
        <location filename="../src/tabwidget.cpp" line="3050"/>
        <location filename="../src/tabwidget.cpp" line="3059"/>
        <location filename="../src/tabwidget.cpp" line="3068"/>
        <location filename="../src/tabwidget.cpp" line="3077"/>
        <location filename="../src/tabwidget.cpp" line="3086"/>
        <location filename="../src/tabwidget.cpp" line="3095"/>
        <location filename="../src/tabwidget.cpp" line="3104"/>
        <location filename="../src/tabwidget.cpp" line="3148"/>
        <location filename="../src/tabwidget.cpp" line="3157"/>
        <location filename="../src/tabwidget.cpp" line="3166"/>
        <location filename="../src/tabwidget.cpp" line="3175"/>
        <location filename="../src/tabwidget.cpp" line="3184"/>
        <location filename="../src/tabwidget.cpp" line="3193"/>
        <location filename="../src/tabwidget.cpp" line="3202"/>
        <location filename="../src/tabwidget.cpp" line="3211"/>
        <location filename="../src/tabwidget.cpp" line="3220"/>
        <location filename="../src/tabwidget.cpp" line="3229"/>
        <location filename="../src/tabwidget.cpp" line="3661"/>
        <source>Click here to diagnose the issue</source>
        <translation>点击此处诊断问题</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="472"/>
        <location filename="../src/tabwidget.cpp" line="1068"/>
        <location filename="../src/tabwidget.cpp" line="2410"/>
        <location filename="../src/tabwidget.cpp" line="2732"/>
        <location filename="../src/tabwidget.cpp" line="3007"/>
        <source>:Unable to access server, please try again later.</source>
        <translation>：无法访问服务器，请稍后再试。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="481"/>
        <location filename="../src/tabwidget.cpp" line="1077"/>
        <location filename="../src/tabwidget.cpp" line="2419"/>
        <location filename="../src/tabwidget.cpp" line="2741"/>
        <location filename="../src/tabwidget.cpp" line="3016"/>
        <source>:Access to source management server timed out. Please try again later.</source>
        <translation>：访问服务器超时，请稍后再试。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="490"/>
        <location filename="../src/tabwidget.cpp" line="1086"/>
        <location filename="../src/tabwidget.cpp" line="2428"/>
        <location filename="../src/tabwidget.cpp" line="2750"/>
        <location filename="../src/tabwidget.cpp" line="3025"/>
        <source>:Please check your network connection and try again.</source>
        <translation>：无法访问服务器，请稍后再试。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="499"/>
        <location filename="../src/tabwidget.cpp" line="1095"/>
        <location filename="../src/tabwidget.cpp" line="2437"/>
        <location filename="../src/tabwidget.cpp" line="2759"/>
        <location filename="../src/tabwidget.cpp" line="3034"/>
        <source>:The kylin-update-desktop-config configuration package is missing from the source.</source>
        <translation>：更新环境存在异常，仓库源中缺少kylin-update-desktop-config软件包。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="508"/>
        <location filename="../src/tabwidget.cpp" line="517"/>
        <location filename="../src/tabwidget.cpp" line="535"/>
        <location filename="../src/tabwidget.cpp" line="589"/>
        <location filename="../src/tabwidget.cpp" line="652"/>
        <location filename="../src/tabwidget.cpp" line="1104"/>
        <location filename="../src/tabwidget.cpp" line="1113"/>
        <location filename="../src/tabwidget.cpp" line="1131"/>
        <location filename="../src/tabwidget.cpp" line="1185"/>
        <location filename="../src/tabwidget.cpp" line="1248"/>
        <location filename="../src/tabwidget.cpp" line="2446"/>
        <location filename="../src/tabwidget.cpp" line="2455"/>
        <location filename="../src/tabwidget.cpp" line="2473"/>
        <location filename="../src/tabwidget.cpp" line="2565"/>
        <location filename="../src/tabwidget.cpp" line="2628"/>
        <location filename="../src/tabwidget.cpp" line="2768"/>
        <location filename="../src/tabwidget.cpp" line="2777"/>
        <location filename="../src/tabwidget.cpp" line="2795"/>
        <location filename="../src/tabwidget.cpp" line="2849"/>
        <location filename="../src/tabwidget.cpp" line="2912"/>
        <location filename="../src/tabwidget.cpp" line="3043"/>
        <location filename="../src/tabwidget.cpp" line="3052"/>
        <location filename="../src/tabwidget.cpp" line="3070"/>
        <location filename="../src/tabwidget.cpp" line="3150"/>
        <location filename="../src/tabwidget.cpp" line="3213"/>
        <location filename="../src/tabwidget.cpp" line="3676"/>
        <source>:There is an exception in updating the environment.</source>
        <translation>：更新环境存在异常，关键目录检测未通过。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="526"/>
        <location filename="../src/tabwidget.cpp" line="1122"/>
        <location filename="../src/tabwidget.cpp" line="2464"/>
        <location filename="../src/tabwidget.cpp" line="2786"/>
        <location filename="../src/tabwidget.cpp" line="3061"/>
        <source>:The update policy is corrupt.</source>
        <translation>：更新策略损坏。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="544"/>
        <location filename="../src/tabwidget.cpp" line="1140"/>
        <location filename="../src/tabwidget.cpp" line="2482"/>
        <location filename="../src/tabwidget.cpp" line="2804"/>
        <location filename="../src/tabwidget.cpp" line="3079"/>
        <source>:Unable to download software repository information.</source>
        <translation>：无法下载软件仓库信息，检查您的网络连接后再尝试。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="553"/>
        <location filename="../src/tabwidget.cpp" line="1149"/>
        <location filename="../src/tabwidget.cpp" line="2491"/>
        <location filename="../src/tabwidget.cpp" line="2813"/>
        <location filename="../src/tabwidget.cpp" line="3088"/>
        <source>:There is an exception in updating the environment and the package list cannot be loaded.</source>
        <translation>：更新环境存在异常，无法载入软件包列表。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="562"/>
        <location filename="../src/tabwidget.cpp" line="1158"/>
        <location filename="../src/tabwidget.cpp" line="2500"/>
        <location filename="../src/tabwidget.cpp" line="2822"/>
        <location filename="../src/tabwidget.cpp" line="3097"/>
        <source>:The updated patch package does not match the system version exactly.</source>
        <translation>：更新补丁与系统版本不完全匹配。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="571"/>
        <location filename="../src/tabwidget.cpp" line="1167"/>
        <location filename="../src/tabwidget.cpp" line="2509"/>
        <location filename="../src/tabwidget.cpp" line="2831"/>
        <location filename="../src/tabwidget.cpp" line="3106"/>
        <source>:There is an exception in updating the environment, and the critical directory detection did not pass.</source>
        <translation>：更新环境存在异常，关键目录检测未通过。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="580"/>
        <location filename="../src/tabwidget.cpp" line="1176"/>
        <location filename="../src/tabwidget.cpp" line="2541"/>
        <location filename="../src/tabwidget.cpp" line="2551"/>
        <location filename="../src/tabwidget.cpp" line="2840"/>
        <source>:There is an exception in updating the environment, and the software package cannot be installed or removed.</source>
        <translation>：更新环境存在异常，无法安装或移除软件包。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="598"/>
        <location filename="../src/tabwidget.cpp" line="1194"/>
        <location filename="../src/tabwidget.cpp" line="2574"/>
        <location filename="../src/tabwidget.cpp" line="2858"/>
        <location filename="../src/tabwidget.cpp" line="3159"/>
        <source>:The patch needs to remove necessary components from the system</source>
        <translation>：更新补丁与系统环境存在异常，补丁需要移除系统必要的组件。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="607"/>
        <location filename="../src/tabwidget.cpp" line="1203"/>
        <location filename="../src/tabwidget.cpp" line="2583"/>
        <location filename="../src/tabwidget.cpp" line="2867"/>
        <location filename="../src/tabwidget.cpp" line="3168"/>
        <source>:Unable to download the patch pack. Please check your network connection and try again.</source>
        <translation>：无法下载补丁包，检查您的网络连接后再尝试。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="616"/>
        <location filename="../src/tabwidget.cpp" line="1212"/>
        <location filename="../src/tabwidget.cpp" line="2592"/>
        <location filename="../src/tabwidget.cpp" line="2876"/>
        <location filename="../src/tabwidget.cpp" line="3177"/>
        <source>:Insufficient disk space, please clean the disk before upgrading and updating.</source>
        <translation>：磁盘空间不足，请清理磁盘后进行升级更新。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="625"/>
        <location filename="../src/tabwidget.cpp" line="1221"/>
        <location filename="../src/tabwidget.cpp" line="2601"/>
        <location filename="../src/tabwidget.cpp" line="2885"/>
        <location filename="../src/tabwidget.cpp" line="3186"/>
        <source>:The software package format is abnormal and the read failed.</source>
        <translation>：补丁格式异常。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="634"/>
        <location filename="../src/tabwidget.cpp" line="1230"/>
        <location filename="../src/tabwidget.cpp" line="2610"/>
        <location filename="../src/tabwidget.cpp" line="2894"/>
        <location filename="../src/tabwidget.cpp" line="3195"/>
        <source>:Battery power is low.</source>
        <translation>：电池电量低。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="643"/>
        <location filename="../src/tabwidget.cpp" line="1239"/>
        <location filename="../src/tabwidget.cpp" line="2619"/>
        <location filename="../src/tabwidget.cpp" line="2903"/>
        <location filename="../src/tabwidget.cpp" line="3204"/>
        <source>:The patch format is abnormal.</source>
        <translation>：补丁格式异常。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="661"/>
        <location filename="../src/tabwidget.cpp" line="1257"/>
        <location filename="../src/tabwidget.cpp" line="2637"/>
        <location filename="../src/tabwidget.cpp" line="2921"/>
        <location filename="../src/tabwidget.cpp" line="3222"/>
        <source>:The system upgrade was interrupted abnormally, and the system has automatically rolled back to the pre upgrade state.</source>
        <translation>：系统升级异常中断，系统已自动回退至升级前状态。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="670"/>
        <location filename="../src/tabwidget.cpp" line="1266"/>
        <location filename="../src/tabwidget.cpp" line="2646"/>
        <location filename="../src/tabwidget.cpp" line="2930"/>
        <location filename="../src/tabwidget.cpp" line="3231"/>
        <source>Unknown error!</source>
        <translation>未知错误！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1539"/>
        <source>Start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2056"/>
        <location filename="../src/tabwidget.cpp" line="3477"/>
        <source>Other progress is updating,please retry later.</source>
        <translation>其他程序正在更新，请稍后再试</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2117"/>
        <location filename="../src/tabwidget.cpp" line="2691"/>
        <location filename="../src/tabwidget.cpp" line="3263"/>
        <source>Reboot right now</source>
        <translation>立即重启</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2554"/>
        <location filename="../src/tabwidget.cpp" line="3139"/>
        <source>Update failed</source>
        <translation>更新失败</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3443"/>
        <location filename="../src/tabwidget.cpp" line="3772"/>
        <source>Prepare to backup</source>
        <translation>备份准备中。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3933"/>
        <source>backuping system,please wait</source>
        <translation>有正在进行的系统备份，请稍后还原</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3967"/>
        <source>feedback UI end failed!</source>
        <translation>用户反馈界面异常，请确认麒麟管家是否正常安装</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3968"/>
        <location filename="../src/tabwidget.cpp" line="3993"/>
        <source>System-Upgrade</source>
        <translation>系统更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3971"/>
        <location filename="../src/tabwidget.cpp" line="3996"/>
        <source>ukui-control-center-update</source>
        <translation>设置-更新提示</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3992"/>
        <source>feedback UI start failed!</source>
        <translation>用户反馈界面异常。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="4057"/>
        <source>self-update finished,ukui-control-center will restart in %1 seconds</source>
        <translation>更新相关组件自升级完成，控制面板将在%1秒后重新启动</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="61"/>
        <location filename="../src/tabwidget.cpp" line="4093"/>
        <source>backuping</source>
        <translation>正在备份</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="4133"/>
        <source>backup finished</source>
        <translation>备份完成</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="4151"/>
        <source>backup failed,continue upgrade?</source>
        <translation>无法备份，是否继续更新？</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="4155"/>
        <source>Continue to Update</source>
        <translation>继续更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2042"/>
        <source>The updater is NOT start</source>
        <translation>更新程序未启动，建议重启计算机！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2059"/>
        <location filename="../src/tabwidget.cpp" line="3480"/>
        <source>The progress is updating...</source>
        <translation>正在检查更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2069"/>
        <location filename="../src/tabwidget.cpp" line="2085"/>
        <location filename="../src/tabwidget.cpp" line="3490"/>
        <location filename="../src/tabwidget.cpp" line="3507"/>
        <source>The progress is installing...</source>
        <translation>正在下载并安装更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2099"/>
        <location filename="../src/tabwidget.cpp" line="3521"/>
        <location filename="../src/tabwidget.cpp" line="3573"/>
        <location filename="../src/tabwidget.cpp" line="3621"/>
        <source>The updater is busy！</source>
        <translation>其他应用正在安装软件包，请稍后再试</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2113"/>
        <location filename="../src/tabwidget.cpp" line="2158"/>
        <source>Updating the software source</source>
        <translation>正在更新软件源</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="230"/>
        <location filename="../src/tabwidget.cpp" line="440"/>
        <location filename="../src/tabwidget.cpp" line="452"/>
        <location filename="../src/tabwidget.cpp" line="2142"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1787"/>
        <source>current step:%1/%2</source>
        <translation>当前更新阶段:%1/%2</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2202"/>
        <source>Please back up the system before all updates to avoid unnecessary losses</source>
        <translation>更新前备份系统，以防数据丢失。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2207"/>
        <source>Only Update</source>
        <translation>直接更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2208"/>
        <source>Back And Update</source>
        <translation>备份并更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="169"/>
        <location filename="../src/tabwidget.cpp" line="179"/>
        <location filename="../src/tabwidget.cpp" line="2330"/>
        <location filename="../src/tabwidget.cpp" line="2392"/>
        <location filename="../src/tabwidget.cpp" line="2715"/>
        <location filename="../src/tabwidget.cpp" line="2988"/>
        <location filename="../src/tabwidget.cpp" line="3337"/>
        <location filename="../src/tabwidget.cpp" line="3659"/>
        <location filename="../src/tabwidget.cpp" line="3742"/>
        <source>update has been canceled!</source>
        <translation>本次更新已取消！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2374"/>
        <source>This update has been completed！</source>
        <translatorcomment>本次更新已完成！</translatorcomment>
        <translation>本次更新已完成！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="438"/>
        <source>Insufficient disk space to download updates!</source>
        <translation>磁盘空间不足，无法下载更新！请清理磁盘空间后再试。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="748"/>
        <source>supposed</source>
        <translation>预计占用</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="811"/>
        <source>s</source>
        <translation>秒</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="813"/>
        <source>min</source>
        <translation>分钟</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="815"/>
        <source>h</source>
        <translation>小时</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1537"/>
        <source>Rollback to previous version</source>
        <translation>回退到上一个版本</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="448"/>
        <source>The update stopped because of low battery.</source>
        <translation>电池电量较低，系统更新已终止！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="449"/>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation>系统更新需要电池电量不低于50%时进行。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2678"/>
        <location filename="../src/tabwidget.cpp" line="2690"/>
        <location filename="../src/tabwidget.cpp" line="3266"/>
        <source>Finish the download!</source>
        <translation>下载完成</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="209"/>
        <source>Later</source>
        <translation>稍后安装</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="4146"/>
        <source>backup failed</source>
        <translation>备份异常</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3535"/>
        <source>Calculating</source>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3460"/>
        <location filename="../src/tabwidget.cpp" line="3552"/>
        <source>The system is updating...</source>
        <translation>正在准备更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="208"/>
        <source>Reboot</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2139"/>
        <source>Reboot failed!</source>
        <translation>重启失败！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="3686"/>
        <source>It&apos;s fixing up the environment...</source>
        <translation>更新环境修正中，请稍后</translation>
    </message>
</context>
<context>
    <name>UpdateDbus</name>
    <message>
        <location filename="../src/updatedbus.cpp" line="118"/>
        <source>System-Upgrade</source>
        <translation>系统更新</translation>
    </message>
    <message>
        <location filename="../src/updatedbus.cpp" line="121"/>
        <source>ukui-control-center-update</source>
        <translation>设置-更新提示</translation>
    </message>
</context>
<context>
    <name>UpdateLog</name>
    <message>
        <location filename="../src/updatelog.cpp" line="20"/>
        <source>Update log</source>
        <translation>更新日志</translation>
    </message>
</context>
<context>
    <name>UpdateSource</name>
    <message>
        <location filename="../src/updatesource.cpp" line="64"/>
        <source>Connection failed, please reconnect!</source>
        <translation>连接失败，请重新连接！</translation>
    </message>
</context>
<context>
    <name>Upgrade</name>
    <message>
        <location filename="../upgrade.cpp" line="28"/>
        <source>Upgrade</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="88"/>
        <source>Accept beta version and provide feedback to help improving the system.</source>
        <translation>接受Beta版本更新，并提供反馈以协助改进系统</translation>
        <extra-contents_path>/Upgrade/Accept beta version and provide feedback to help improving the system.</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="90"/>
        <source>Download Limit</source>
        <translation>下载限速</translation>
        <extra-contents_path>/Upgrade/Download Limit</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="92"/>
        <source>View history</source>
        <translation>更新历史</translation>
        <extra-contents_path>/Upgrade/View history</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="94"/>
        <source>Advanced</source>
        <translation>高级选项</translation>
        <extra-contents_path>/Upgrade/Advanced</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="96"/>
        <source>AutomaticUpdate</source>
        <translation>自动更新</translation>
        <extra-contents_path>/Upgrade/AutomaticUpdate</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="84"/>
        <source>Update Settings</source>
        <translation>更新设置</translation>
        <extra-contents_path>/Upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="82"/>
        <source>SystemUpdate</source>
        <translation>系统更新</translation>
        <extra-contents_path>/Upgrade/SystemUpdate</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="86"/>
        <source>Allowed to renewable notice</source>
        <translation>有更新应用时通知</translation>
        <extra-contents_path>/Upgrade/Allowed to renewable notice</extra-contents_path>
    </message>
</context>
<context>
    <name>dependencyfixdialog</name>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="26"/>
        <source>show details</source>
        <translation>了解详情</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="35"/>
        <source>uninstall and update</source>
        <translation>卸载并更新</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="37"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>fixbrokeninstalldialog</name>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="80"/>
        <source>We need to fix up the environment!</source>
        <translation>需要修复更新环境，可尝试如下方法：</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="86"/>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation>将卸载部分软件包，以完成更新。</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="95"/>
        <source>The following packages will be uninstalled:</source>
        <translation>下列软件包将被卸载：</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="120"/>
        <source>PKG Details</source>
        <translation>软件包详情</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="130"/>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="408"/>
        <source>details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="134"/>
        <source>Keep</source>
        <translation>取消更新</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="138"/>
        <source>Remove</source>
        <translation>继续更新</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="294"/>
        <source>Attention on update</source>
        <translation>更新提示</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="349"/>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="350"/>
        <source>signal error</source>
        <translation>信号错误</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="396"/>
        <source>back</source>
        <translation>收起</translation>
    </message>
</context>
<context>
    <name>fixupdetaillist</name>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="54"/>
        <source>No content.</source>
        <translation>暂无内容。</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="110"/>
        <source>Update Details</source>
        <translation>更新详情</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="542"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
</context>
<context>
    <name>m_updatelog</name>
    <message>
        <location filename="../src/m_updatelog.cpp" line="64"/>
        <source>No content.</source>
        <translation>暂无内容</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="171"/>
        <source>no content</source>
        <translation>暂无内容</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="665"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="669"/>
        <location filename="../src/m_updatelog.cpp" line="676"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="652"/>
        <source>History Log</source>
        <translation>更新历史记录</translation>
    </message>
</context>
<context>
    <name>updatedeleteprompt</name>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="76"/>
        <source>The following packages will be uninstalled:</source>
        <translation>下列软件包将被卸载，请确认是否继续更新：</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="95"/>
        <source>PKG Details</source>
        <translation>软件包详情</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="239"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="103"/>
        <source>Keep</source>
        <translation>取消更新</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="107"/>
        <source>Remove</source>
        <translation>继续更新</translation>
    </message>
</context>
</TS>
