<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AppUpdateWid</name>
    <message>
        <location filename="../src/appupdate.cpp" line="296"/>
        <source>Cancel failed,Being installed</source>
        <translatorcomment>正在安装，无法取消</translatorcomment>
        <translation>取消失敗，正在安裝</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="315"/>
        <source>Being installed</source>
        <translation>正在安裝</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="376"/>
        <source>Download succeeded!</source>
        <translation>下載成功！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="466"/>
        <location filename="../src/appupdate.cpp" line="468"/>
        <location filename="../src/appupdate.cpp" line="469"/>
        <source>Update succeeded , It is recommended that you restart later!</source>
        <translation>更新成功，建議稍後重啟！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="151"/>
        <location filename="../src/appupdate.cpp" line="152"/>
        <location filename="../src/appupdate.cpp" line="157"/>
        <location filename="../src/appupdate.cpp" line="173"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="347"/>
        <location filename="../src/appupdate.cpp" line="350"/>
        <location filename="../src/appupdate.cpp" line="351"/>
        <source>Download finished,it is recommended that you restart later to use the new version.</source>
        <translation>下載完成，建議您稍後重新啟動以使用新版本。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="354"/>
        <location filename="../src/appupdate.cpp" line="738"/>
        <source>reboot</source>
        <translation>重新啟動</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="358"/>
        <location filename="../src/appupdate.cpp" line="361"/>
        <location filename="../src/appupdate.cpp" line="362"/>
        <location filename="../src/appupdate.cpp" line="473"/>
        <location filename="../src/appupdate.cpp" line="475"/>
        <location filename="../src/appupdate.cpp" line="476"/>
        <source>Update succeeded , It is recommended that you log out later and log in again!</source>
        <translation>更新成功，建議稍後註銷再登錄！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="380"/>
        <location filename="../src/appupdate.cpp" line="386"/>
        <location filename="../src/appupdate.cpp" line="479"/>
        <location filename="../src/appupdate.cpp" line="484"/>
        <source>Update succeeded!</source>
        <translation>更新成功！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="397"/>
        <location filename="../src/appupdate.cpp" line="495"/>
        <location filename="../src/appupdate.cpp" line="603"/>
        <location filename="../src/appupdate.cpp" line="619"/>
        <location filename="../src/appupdate.cpp" line="641"/>
        <source>Update has been canceled!</source>
        <translation>更新已被取消！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="408"/>
        <location filename="../src/appupdate.cpp" line="415"/>
        <location filename="../src/appupdate.cpp" line="432"/>
        <location filename="../src/appupdate.cpp" line="506"/>
        <location filename="../src/appupdate.cpp" line="513"/>
        <source>Update failed!</source>
        <translation>更新失敗！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="410"/>
        <location filename="../src/appupdate.cpp" line="508"/>
        <source>Failure reason:</source>
        <translation>失敗原因：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="530"/>
        <source>There are unresolved dependency conflicts in this update，Please select update all</source>
        <translation>此更新中存在未解決的依賴衝突，請選擇全部更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="531"/>
        <location filename="../src/appupdate.cpp" line="765"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="533"/>
        <source>Update ALL</source>
        <translation>全部更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="534"/>
        <location filename="../src/appupdate.cpp" line="630"/>
        <location filename="../src/appupdate.cpp" line="768"/>
        <location filename="../src/appupdate.cpp" line="795"/>
        <location filename="../src/appupdate.cpp" line="849"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="581"/>
        <source>No Content.</source>
        <translation>暫無內容。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="591"/>
        <source>There are </source>
        <translation>有 </translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="591"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation> 要移除的包裹，請確認是否接受！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="722"/>
        <source>Cumulative updates</source>
        <translation>累積更新</translation>
    </message>
    <message>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation type="vanished">个软件包将被卸载，请确认是否接受！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="723"/>
        <source>version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <source>Reboot failed!</source>
        <translation type="vanished">重启失败！</translation>
    </message>
    <message>
        <source>The update stopped because of low battery.</source>
        <translation type="vanished">电池电量较低，系统更新已终止。</translation>
    </message>
    <message>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation type="vanished">系统更新需要电池电量不低于50%时进行。</translation>
    </message>
    <message>
        <source>pkg will be uninstall!</source>
        <translation type="vanished">个软件包将被卸载！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="69"/>
        <location filename="../src/appupdate.cpp" line="406"/>
        <location filename="../src/appupdate.cpp" line="504"/>
        <location filename="../src/appupdate.cpp" line="604"/>
        <location filename="../src/appupdate.cpp" line="620"/>
        <location filename="../src/appupdate.cpp" line="642"/>
        <location filename="../src/appupdate.cpp" line="753"/>
        <location filename="../src/appupdate.cpp" line="800"/>
        <location filename="../src/appupdate.cpp" line="955"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="61"/>
        <source>details</source>
        <translation>詳</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="102"/>
        <location filename="../src/appupdate.cpp" line="172"/>
        <source>Update log</source>
        <translation>更新日誌</translation>
    </message>
    <message>
        <source>Newest:</source>
        <translation type="vanished">最新：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="182"/>
        <source>Download size:</source>
        <translation>下載大小：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="183"/>
        <source>Install size:</source>
        <translation>安裝尺寸：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="209"/>
        <source>Current version:</source>
        <translation>目前版本：</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="764"/>
        <source>A single update will not automatically backup the system, if you want to backup, please click Update All.</source>
        <translation>單次更新不會自動備份系統，如果要備份，請按兩下全部更新」。</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="767"/>
        <source>Do not backup, continue to update</source>
        <translation>不備份，繼續更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="771"/>
        <source>This time will no longer prompt</source>
        <translation>此時間將不再提示</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="853"/>
        <source>Ready to update</source>
        <translatorcomment>准备更新</translatorcomment>
        <translation>準備更新</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="931"/>
        <source>downloaded</source>
        <translatorcomment>已下载</translatorcomment>
        <translation>下載</translation>
    </message>
    <message>
        <source>Ready to install</source>
        <translation type="vanished">准备安装</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="937"/>
        <location filename="../src/appupdate.cpp" line="940"/>
        <source>downloading</source>
        <translation>下載</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="937"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1016"/>
        <source>No content.</source>
        <translation>沒有內容。</translation>
    </message>
</context>
<context>
    <name>HistoryUpdateListWig</name>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="102"/>
        <source>Success</source>
        <translation>成功</translation>
    </message>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="108"/>
        <source>Failed</source>
        <translation>失敗</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/backup.cpp" line="138"/>
        <source>system upgrade new backup</source>
        <translation>系統升級新備份</translation>
    </message>
    <message>
        <location filename="../src/backup.cpp" line="139"/>
        <source>system upgrade increment backup</source>
        <translation>系統升級增量備份</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="78"/>
        <source>Advanced Option</source>
        <translation>高級選項</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="106"/>
        <source>Server address settings</source>
        <translation>伺服器地址設置</translation>
    </message>
    <message>
        <source>If there are internal services, you can change the server address.</source>
        <translation type="vanished">如果想使用内部服务，可以更换服务器地址</translation>
    </message>
    <message>
        <source>Protocal</source>
        <translation type="vanished">协议</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="109"/>
        <source>If internal services, change the server address.</source>
        <translation>如果是內部服務，請更改伺服器位址。</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="123"/>
        <source>Port  ID </source>
        <translation>埠標識 </translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="132"/>
        <source>Address</source>
        <translation>位址</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="197"/>
        <source>reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="203"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="206"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="336"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="333"/>
        <source>Modification failed!</source>
        <translation>修改失敗！</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="334"/>
        <source>Attention</source>
        <translation>注意力</translation>
    </message>
</context>
<context>
    <name>TabWid</name>
    <message>
        <location filename="../src/tabwidget.cpp" line="72"/>
        <location filename="../src/tabwidget.cpp" line="190"/>
        <location filename="../src/tabwidget.cpp" line="1359"/>
        <location filename="../src/tabwidget.cpp" line="1409"/>
        <location filename="../src/tabwidget.cpp" line="1711"/>
        <location filename="../src/tabwidget.cpp" line="1880"/>
        <location filename="../src/tabwidget.cpp" line="1971"/>
        <location filename="../src/tabwidget.cpp" line="2193"/>
        <location filename="../src/tabwidget.cpp" line="2256"/>
        <location filename="../src/tabwidget.cpp" line="2585"/>
        <source>Check Update</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <source>initializing</source>
        <translation type="vanished">初始化中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="262"/>
        <location filename="../src/tabwidget.cpp" line="701"/>
        <location filename="../src/tabwidget.cpp" line="1382"/>
        <location filename="../src/tabwidget.cpp" line="1727"/>
        <location filename="../src/tabwidget.cpp" line="1857"/>
        <location filename="../src/tabwidget.cpp" line="2134"/>
        <location filename="../src/tabwidget.cpp" line="2273"/>
        <location filename="../src/tabwidget.cpp" line="2304"/>
        <location filename="../src/tabwidget.cpp" line="2635"/>
        <source>UpdateAll</source>
        <translation>全部更新</translation>
    </message>
    <message>
        <source>Your system is the latest!</source>
        <translation type="vanished">您的系统已是最新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="914"/>
        <location filename="../src/tabwidget.cpp" line="1450"/>
        <source>No Information!</source>
        <translation>暫無資訊！</translation>
    </message>
    <message>
        <source>Last refresh:</source>
        <translation type="vanished">检查时间：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="205"/>
        <source>Downloading and installing updates...</source>
        <translation>下載與安裝更新...</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation type="vanished">立即更新</translation>
    </message>
    <message>
        <source>Cancel update</source>
        <translation type="vanished">取消更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="242"/>
        <location filename="../src/tabwidget.cpp" line="662"/>
        <source>Being updated...</source>
        <translation>更新中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="253"/>
        <location filename="../src/tabwidget.cpp" line="1395"/>
        <source>Updatable app detected on your system!</source>
        <translation>在您的系統上檢測到可更新的應用程式！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="280"/>
        <source>The backup restore partition could not be found. The system will not be backed up in this update!</source>
        <translation>找不到備份還原分區。此更新中不會備份系統！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="284"/>
        <source>Kylin backup restore tool is doing other operations, please update later.</source>
        <translation>麒麟備份還原工具正在做其他操作，請稍後更新。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="289"/>
        <source>The source manager configuration file is abnormal, the system temporarily unable to update!</source>
        <translation>源管理員配置檔異常，系統暫時無法更新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="294"/>
        <source>Backup already, no need to backup again.</source>
        <translation>已經備份，無需再次備份。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="303"/>
        <source>Start backup,getting progress</source>
        <translation>開始備份，獲取進度</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="319"/>
        <source>Kylin backup restore tool does not exist, this update will not backup the system!</source>
        <translation>麒麟備份還原工具不存在，此更新不會備份系統！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="228"/>
        <location filename="../src/tabwidget.cpp" line="335"/>
        <location filename="../src/tabwidget.cpp" line="341"/>
        <location filename="../src/tabwidget.cpp" line="362"/>
        <location filename="../src/tabwidget.cpp" line="667"/>
        <location filename="../src/tabwidget.cpp" line="1585"/>
        <location filename="../src/tabwidget.cpp" line="1635"/>
        <location filename="../src/tabwidget.cpp" line="1652"/>
        <location filename="../src/tabwidget.cpp" line="1739"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="351"/>
        <source>Calculated</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="436"/>
        <source>There are unresolved dependency conflicts in this update，Please contact the administrator!</source>
        <translation>本次更新存在未解決的依賴衝突，請聯繫管理員！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="90"/>
        <location filename="../src/tabwidget.cpp" line="111"/>
        <location filename="../src/tabwidget.cpp" line="359"/>
        <location filename="../src/tabwidget.cpp" line="437"/>
        <location filename="../src/tabwidget.cpp" line="1736"/>
        <source>Prompt information</source>
        <translation>提示資訊</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="180"/>
        <location filename="../src/tabwidget.cpp" line="1362"/>
        <location filename="../src/tabwidget.cpp" line="1412"/>
        <location filename="../src/tabwidget.cpp" line="1891"/>
        <location filename="../src/tabwidget.cpp" line="2056"/>
        <location filename="../src/tabwidget.cpp" line="2211"/>
        <location filename="../src/tabwidget.cpp" line="2258"/>
        <source>Your system is the latest:</source>
        <translation>您的系統是最新的：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="439"/>
        <source>Sure</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="633"/>
        <location filename="../src/tabwidget.cpp" line="636"/>
        <source>In the download</source>
        <translation>在下載中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="633"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>計算</translation>
    </message>
    <message>
        <source>In the install...</source>
        <translation type="vanished">安装中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="654"/>
        <source>Backup complete.</source>
        <translation>備份完成。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="672"/>
        <source>System is backing up...</source>
        <translation>系統正在備份...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="690"/>
        <source>Backup interrupted, stop updating!</source>
        <translation>備份中斷，停止更新！</translation>
    </message>
    <message>
        <source>Backup finished!</source>
        <translation type="vanished">备份完成！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="709"/>
        <source>Kylin backup restore tool exception:</source>
        <translation>麒麟備份還原工具異常：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="709"/>
        <source>There will be no backup in this update!</source>
        <translation>此更新中將沒有備份！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="707"/>
        <source>The status of backup completion is abnormal</source>
        <translatorcomment>备份完成状态异常</translatorcomment>
        <translation>備份完成狀態異常</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="830"/>
        <source>Getting update list</source>
        <translation>獲取更新清單</translation>
    </message>
    <message>
        <source>Software source update successed: </source>
        <translatorcomment>软件源更新成功： </translatorcomment>
        <translation type="vanished">软件源更新成功： </translation>
    </message>
    <message>
        <source>Software source update failed: </source>
        <translation type="vanished">软件源更新失败： </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="236"/>
        <location filename="../src/tabwidget.cpp" line="976"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="358"/>
        <source>There are unresolved dependency conflicts in this update，Please select Dist-upgrade</source>
        <translatorcomment>本次更新存在无法解决的依赖冲突，请选择全盘更新</translatorcomment>
        <translation>此更新存在未解決的依賴衝突，請選擇 Dist-upgrade</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="361"/>
        <source>Dist-upgrade</source>
        <translatorcomment>全盘更新</translatorcomment>
        <translation>區域升級</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="520"/>
        <location filename="../src/tabwidget.cpp" line="521"/>
        <source>The system is downloading the update!</source>
        <translation>系統正在下載更新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="527"/>
        <location filename="../src/tabwidget.cpp" line="528"/>
        <source>Downloading the updates...</source>
        <translation>下載更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="532"/>
        <location filename="../src/tabwidget.cpp" line="533"/>
        <source>Installing the updates...</source>
        <translation>安裝更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="625"/>
        <location filename="../src/tabwidget.cpp" line="1636"/>
        <location filename="../src/tabwidget.cpp" line="1653"/>
        <source>In the install</source>
        <translation>在安裝</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="862"/>
        <location filename="../src/tabwidget.cpp" line="1667"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="957"/>
        <source>The system is checking update :</source>
        <translation>系統正在檢查更新：</translation>
    </message>
    <message>
        <source>Download Limit(Kb/s)</source>
        <translation type="vanished">下载限速(Kb/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1244"/>
        <source>View history</source>
        <translation>查看歷史記錄</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1085"/>
        <source>Update Settings</source>
        <translation>更新設置</translation>
        <extra-contents_path>/upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1100"/>
        <source>Allowed to renewable notice</source>
        <translation>允許更新通知</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1116"/>
        <source>Backup current system before updates all</source>
        <translation>在更新所有系統之前備份當前系統</translation>
    </message>
    <message>
        <source>Download Limit(KB/s)</source>
        <translation type="vanished">下载限速(KB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1140"/>
        <source>It will be avaliable in the next download.</source>
        <translation>它將在下次下載中可用。</translation>
    </message>
    <message>
        <source>Automatically download and install updates</source>
        <translation type="vanished">自动下载和安装更新</translation>
    </message>
    <message>
        <source>After it is turned on, the system will automatically download and install updates when there is an available network and available backup and restore partitions.</source>
        <translation type="vanished">开启后，当有可用网络和可用备份和恢复分区时，系统会自动下载和安装更新。</translation>
    </message>
    <message>
        <source>Upgrade during poweroff</source>
        <translation type="vanished">关机检测更新</translation>
    </message>
    <message>
        <source>Download Limit(kB/s)</source>
        <translation type="vanished">下载限速(kB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1168"/>
        <source>The system will automatically updates when there is an available network and backup.</source>
        <translation>當有可用的網路和備份時，系統將自動更新。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1318"/>
        <source>Ready to install</source>
        <translation>準備安裝</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="188"/>
        <location filename="../src/tabwidget.cpp" line="261"/>
        <location filename="../src/tabwidget.cpp" line="899"/>
        <location filename="../src/tabwidget.cpp" line="952"/>
        <location filename="../src/tabwidget.cpp" line="1372"/>
        <location filename="../src/tabwidget.cpp" line="1392"/>
        <location filename="../src/tabwidget.cpp" line="1423"/>
        <location filename="../src/tabwidget.cpp" line="1460"/>
        <location filename="../src/tabwidget.cpp" line="1951"/>
        <location filename="../src/tabwidget.cpp" line="2116"/>
        <location filename="../src/tabwidget.cpp" line="2228"/>
        <source>Last Checked:</source>
        <translation>上次檢查時間：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="181"/>
        <location filename="../src/tabwidget.cpp" line="254"/>
        <location filename="../src/tabwidget.cpp" line="892"/>
        <location filename="../src/tabwidget.cpp" line="945"/>
        <location filename="../src/tabwidget.cpp" line="1365"/>
        <location filename="../src/tabwidget.cpp" line="1385"/>
        <location filename="../src/tabwidget.cpp" line="1416"/>
        <location filename="../src/tabwidget.cpp" line="1944"/>
        <location filename="../src/tabwidget.cpp" line="2109"/>
        <location filename="../src/tabwidget.cpp" line="2221"/>
        <source>No information!</source>
        <translation>暫無資訊！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1132"/>
        <source>Download Limit</source>
        <translation>下載限制</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1247"/>
        <source>Advanced</source>
        <translation>高深</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1539"/>
        <location filename="../src/tabwidget.cpp" line="1564"/>
        <source>Dependency conflict exists in this update,need to be completely repaired!</source>
        <translation>本次更新存在依賴衝突，需要徹底修復！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1541"/>
        <source>There are </source>
        <translation>有 </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1541"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation> 要移除的包裹，請確認是否接受！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1566"/>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation>包裹要被移除了，請確認是否接受！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1577"/>
        <source>trying to reconnect </source>
        <translation>嘗試重新連接 </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1577"/>
        <source> times</source>
        <translation> 次</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>Auto-Update is backing up......</source>
        <translatorcomment>自动更新进程正在备份中......</translatorcomment>
        <translation type="vanished">自动更新进程正在备份中......</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1603"/>
        <source>The updater is NOT start</source>
        <translation>更新程式未啟動</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1621"/>
        <source>The progress is updating...</source>
        <translation>進度正在更新中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1631"/>
        <location filename="../src/tabwidget.cpp" line="1648"/>
        <source>The progress is installing...</source>
        <translation>進度正在安裝中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1662"/>
        <source>The updater is busy！</source>
        <translation>更新程式繁忙！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1680"/>
        <location filename="../src/tabwidget.cpp" line="1723"/>
        <source>Updating the software source</source>
        <translation>更新軟體源</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="114"/>
        <location filename="../src/tabwidget.cpp" line="410"/>
        <location filename="../src/tabwidget.cpp" line="422"/>
        <location filename="../src/tabwidget.cpp" line="1706"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1735"/>
        <source>Please back up the system before all updates to avoid unnecessary losses</source>
        <translation>請在所有更新前備份系統，以避免不必要的損失</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1740"/>
        <source>Only Update</source>
        <translation>僅更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1741"/>
        <source>Back And Update</source>
        <translation>返回和更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1904"/>
        <location filename="../src/tabwidget.cpp" line="2070"/>
        <location filename="../src/tabwidget.cpp" line="2151"/>
        <location filename="../src/tabwidget.cpp" line="2275"/>
        <source>update has been canceled!</source>
        <translation>更新已被取消！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1886"/>
        <location filename="../src/tabwidget.cpp" line="1979"/>
        <source>This update has been completed！</source>
        <translatorcomment>本次更新已完成！</translatorcomment>
        <translation>此更新已完成！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="227"/>
        <source>Keeping update</source>
        <translation>保持更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="229"/>
        <source>It is recommended to back up the system before all updates to avoid unnecessary losses!</source>
        <translation>建議在所有更新前備份系統，以避免不必要的損失！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="408"/>
        <source>Insufficient disk space to download updates!</source>
        <translation>磁碟空間不足，無法下載更新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="491"/>
        <source>supposed</source>
        <translation>假定的</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="562"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="564"/>
        <source>min</source>
        <translation>最小</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="566"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="870"/>
        <location filename="../src/tabwidget.cpp" line="1912"/>
        <location filename="../src/tabwidget.cpp" line="2078"/>
        <location filename="../src/tabwidget.cpp" line="2160"/>
        <source>Network exception, unable to check for updates!</source>
        <translation>網路異常，無法檢查更新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="887"/>
        <source>Checking update failed! </source>
        <translation>檢查更新失敗！ </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="888"/>
        <location filename="../src/tabwidget.cpp" line="1931"/>
        <location filename="../src/tabwidget.cpp" line="2096"/>
        <location filename="../src/tabwidget.cpp" line="2178"/>
        <source>Error Code: </source>
        <translation>錯誤代碼： </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1167"/>
        <source>Automatically updates</source>
        <translation>自動更新</translation>
    </message>
    <message>
        <source>Advanced Option</source>
        <translation type="vanished">高级选项</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="418"/>
        <source>The update stopped because of low battery.</source>
        <translation>由於電池電量不足，更新停止。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="419"/>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation>系統更新要求電池電量不低於50%</translation>
    </message>
    <message>
        <source>Part of the update failed!</source>
        <translation type="vanished">更新失败！</translation>
    </message>
    <message>
        <source>Failure reason:</source>
        <translatorcomment>失败原因：</translatorcomment>
        <translation type="vanished">失败原因：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1972"/>
        <source>Finish the download!</source>
        <translation>完成下載！</translation>
    </message>
    <message>
        <source>The system has download the update!</source>
        <translation type="vanished">系统完成更新内容下载</translation>
    </message>
    <message>
        <source>It&apos;s need to reboot to make the install avaliable</source>
        <translation type="vanished">更新下载已完成，是否安装更新。</translation>
    </message>
    <message>
        <source>Reboot notification</source>
        <translation type="vanished">重启提示</translation>
    </message>
    <message>
        <source>Reboot rightnow</source>
        <translation type="vanished">重启并安装</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="93"/>
        <source>Later</source>
        <translation>後</translation>
    </message>
    <message>
        <source>Part of the update success!</source>
        <translation type="vanished">部分软件包更新成功！</translation>
    </message>
    <message>
        <source>All the update has been downloaded.</source>
        <translation type="vanished">所有的更新内容已经被下载</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2316"/>
        <source>An important update is in progress, please wait.</source>
        <translation>重要的更新正在進行中，請稍候。</translation>
    </message>
    <message>
        <source>plase clean up your disk or expand the backup space</source>
        <translation type="vanished">请清理磁盘空间或扩大备份分区</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2336"/>
        <source>insufficient backup space</source>
        <translation>備份空間不足</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2344"/>
        <source>backup failed</source>
        <translation>備份失敗</translation>
    </message>
    <message>
        <source>Failed to write configuration file, this update will not back up the system!</source>
        <translation type="vanished">写入配置文件失败，本次更新不会备份系统！</translation>
    </message>
    <message>
        <source>Insufficient backup space, this update will not backup your system!</source>
        <translation type="vanished">备份空间不足，本次更新不会备份系统！</translation>
    </message>
    <message>
        <source>Kylin backup restore tool could not find the UUID, this update will not backup the system!</source>
        <translation type="vanished">麒麟备份还原工具无法找到UUID，本次更新不会备份系统！</translation>
    </message>
    <message>
        <source>The backup restore partition is abnormal. You may not have a backup restore partition.For more details,see /var/log/backup.log</source>
        <translation type="vanished">备份还原分区异常，您可能没有备份还原分区。更多详细信息，可以参看/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1766"/>
        <location filename="../src/tabwidget.cpp" line="2352"/>
        <source>Calculating Capacity...</source>
        <translation>計算容量...</translation>
    </message>
    <message>
        <source>The system backup partition is not detected. Do you want to continue updating?</source>
        <translation type="vanished">未检测到系统备份还原分区，是否继续更新？</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2475"/>
        <source>Calculating</source>
        <translation>計算</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2465"/>
        <location filename="../src/tabwidget.cpp" line="2488"/>
        <source>The system is updating...</source>
        <translation>系統正在更新...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="92"/>
        <source>Reboot</source>
        <translation>重新啟動</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1992"/>
        <location filename="../src/tabwidget.cpp" line="2204"/>
        <source>The system has download the update，and you are suggested to reboot to use the new version.</source>
        <translation>系統已下載更新，建議您重新啟動以使用新版本。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1685"/>
        <location filename="../src/tabwidget.cpp" line="1993"/>
        <location filename="../src/tabwidget.cpp" line="2205"/>
        <source>reboot rightnow</source>
        <translation>立即重啟</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1703"/>
        <source>Reboot failed!</source>
        <translation>重新啟動失敗！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1930"/>
        <location filename="../src/tabwidget.cpp" line="2095"/>
        <location filename="../src/tabwidget.cpp" line="2177"/>
        <source>Update failed! </source>
        <translation>更新失敗！ </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="876"/>
        <location filename="../src/tabwidget.cpp" line="1917"/>
        <location filename="../src/tabwidget.cpp" line="2083"/>
        <location filename="../src/tabwidget.cpp" line="2165"/>
        <source>No room to backup,upgrade failed.</source>
        <translation>沒有備份空間，升級失敗。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="882"/>
        <location filename="../src/tabwidget.cpp" line="1923"/>
        <location filename="../src/tabwidget.cpp" line="2089"/>
        <location filename="../src/tabwidget.cpp" line="2171"/>
        <source>Battery level is below 50%,and upgrade failed.</source>
        <translation>電池電量低於 50%，升級失敗。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2543"/>
        <source>Auto-Update progress is installing new file：</source>
        <translatorcomment>系统自动更新功能正在安装新文件：</translatorcomment>
        <translation>自動更新進度正在安裝新檔案：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2553"/>
        <source>Auto-Update progress finished!</source>
        <translatorcomment>系统自动更新完成！</translatorcomment>
        <translation>自動更新進度已完成！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2561"/>
        <source>Auto-Update progress fail in backup!</source>
        <translatorcomment>自动更新安装时备份失败！</translatorcomment>
        <translation>自動更新進度在備份中失敗！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2584"/>
        <source>Failed in updating because of broken environment.</source>
        <translation>由於環境損壞，更新失敗。</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2596"/>
        <source>It&apos;s fixing up the environment...</source>
        <translation>它正在修復環境...</translation>
    </message>
</context>
<context>
    <name>UpdateDbus</name>
    <message>
        <location filename="../src/updatedbus.cpp" line="139"/>
        <source>System-Upgrade</source>
        <translation>系統升級</translation>
    </message>
    <message>
        <location filename="../src/updatedbus.cpp" line="142"/>
        <source>ukui-control-center-update</source>
        <translation>Ukui-control-center-update</translation>
    </message>
</context>
<context>
    <name>UpdateLog</name>
    <message>
        <location filename="../src/updatelog.cpp" line="23"/>
        <source>Update log</source>
        <translation>更新日誌</translation>
    </message>
</context>
<context>
    <name>UpdateSource</name>
    <message>
        <location filename="../src/updatesource.cpp" line="64"/>
        <source>Connection failed, please reconnect!</source>
        <translation>連接失敗，請重新連接！</translation>
    </message>
</context>
<context>
    <name>Upgrade</name>
    <message>
        <location filename="../upgrade.cpp" line="11"/>
        <source>Upgrade</source>
        <translation>升級</translation>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="65"/>
        <source>View history</source>
        <translation>查看歷史記錄</translation>
        <extra-contents_path>/Upgrade/View history</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="67"/>
        <source>Update Settings</source>
        <translation>更新設置</translation>
        <extra-contents_path>/Upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="69"/>
        <source>Allowed to renewable notice</source>
        <translation>允許更新通知</translation>
        <extra-contents_path>/Upgrade/Allowed to renewable notice</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="71"/>
        <source>Automatically download and install updates</source>
        <translation>自動下載並安裝更新</translation>
        <extra-contents_path>/Upgrade/Automatically download and install updates</extra-contents_path>
    </message>
</context>
<context>
    <name>dependencyfixdialog</name>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="26"/>
        <source>show details</source>
        <translation>顯示詳情</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="35"/>
        <source>uninstall and update</source>
        <translation>卸載和更新</translation>
    </message>
    <message>
        <source>uninstall</source>
        <translation type="vanished">移除</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="37"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>fixbrokeninstalldialog</name>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="80"/>
        <source>We need to fix up the environment!</source>
        <translation>我們需要修復環境！</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="86"/>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation>將卸載一些軟體包以完成更新！</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="95"/>
        <source>The following packages will be uninstalled:</source>
        <translation>將卸載以下套件：</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="120"/>
        <source>PKG Details</source>
        <translation>包裝詳情</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="130"/>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="397"/>
        <source>details</source>
        <translation>詳</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="134"/>
        <source>Keep</source>
        <translation>保持</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="138"/>
        <source>Remove</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="294"/>
        <source>Attention on update</source>
        <translation>更新注意事項</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="385"/>
        <source>back</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>fixupdetaillist</name>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="59"/>
        <source>No content.</source>
        <translation>沒有內容。</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="115"/>
        <source>Update Details</source>
        <translation>更新詳細資訊</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="549"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
</context>
<context>
    <name>m_updatelog</name>
    <message>
        <location filename="../src/m_updatelog.cpp" line="58"/>
        <source>No content.</source>
        <translation>沒有內容。</translation>
    </message>
    <message>
        <source>Update Details</source>
        <translation type="vanished">更新详情</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="113"/>
        <source>No Contents</source>
        <translation>暫無內容</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="558"/>
        <location filename="../src/m_updatelog.cpp" line="594"/>
        <source>Search content</source>
        <translation>搜索內容</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="641"/>
        <source>History Log</source>
        <translation>歷史日誌</translation>
    </message>
</context>
<context>
    <name>updatedeleteprompt</name>
    <message>
        <source>Dependency conflict exists in this update!</source>
        <translation type="vanished">本次更新存在依赖冲突！</translation>
    </message>
    <message>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation type="vanished">将卸载部分软件包以完成更新！</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="78"/>
        <source>The following packages will be uninstalled:</source>
        <translation>將卸載以下套件：</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="97"/>
        <source>PKG Details</source>
        <translation>包裝詳情</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="241"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="105"/>
        <source>Keep</source>
        <translation>保持</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="109"/>
        <source>Remove</source>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Update Prompt</source>
        <translation type="vanished">更新提示</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
</context>
</TS>
