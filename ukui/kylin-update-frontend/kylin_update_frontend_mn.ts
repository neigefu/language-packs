<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AppUpdateWid</name>
    <message>
        <location filename="../src/appupdate.cpp" line="296"/>
        <source>Cancel failed,Being installed</source>
        <translatorcomment>取消失败，安装中</translatorcomment>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="315"/>
        <source>Being installed</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="376"/>
        <source>Download succeeded!</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠴᠢᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="466"/>
        <location filename="../src/appupdate.cpp" line="468"/>
        <location filename="../src/appupdate.cpp" line="469"/>
        <source>Update succeeded , It is recommended that you restart later!</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠪᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠵᠢ ᠵᠦᠪᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="151"/>
        <location filename="../src/appupdate.cpp" line="152"/>
        <location filename="../src/appupdate.cpp" line="157"/>
        <location filename="../src/appupdate.cpp" line="173"/>
        <source>Version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="347"/>
        <location filename="../src/appupdate.cpp" line="350"/>
        <location filename="../src/appupdate.cpp" line="351"/>
        <source>Download finished,it is recommended that you restart later to use the new version.</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ᠂ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="354"/>
        <location filename="../src/appupdate.cpp" line="738"/>
        <source>reboot</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="358"/>
        <location filename="../src/appupdate.cpp" line="361"/>
        <location filename="../src/appupdate.cpp" line="362"/>
        <location filename="../src/appupdate.cpp" line="473"/>
        <location filename="../src/appupdate.cpp" line="475"/>
        <location filename="../src/appupdate.cpp" line="476"/>
        <source>Update succeeded , It is recommended that you log out later and log in again!</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠪᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠩᠰᠠᠨ ᠡᠴᠡ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠨ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠢ ᠵᠦᠪᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="380"/>
        <location filename="../src/appupdate.cpp" line="386"/>
        <location filename="../src/appupdate.cpp" line="479"/>
        <location filename="../src/appupdate.cpp" line="484"/>
        <source>Update succeeded!</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="397"/>
        <location filename="../src/appupdate.cpp" line="495"/>
        <location filename="../src/appupdate.cpp" line="603"/>
        <location filename="../src/appupdate.cpp" line="619"/>
        <location filename="../src/appupdate.cpp" line="641"/>
        <source>Update has been canceled!</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠦᠬᠡᠢᠰᠬᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="408"/>
        <location filename="../src/appupdate.cpp" line="415"/>
        <location filename="../src/appupdate.cpp" line="432"/>
        <location filename="../src/appupdate.cpp" line="506"/>
        <location filename="../src/appupdate.cpp" line="513"/>
        <source>Update failed!</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="410"/>
        <location filename="../src/appupdate.cpp" line="508"/>
        <source>Failure reason:</source>
        <translation>ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠰᠢᠯᠳᠠᠭᠠᠨ:</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="530"/>
        <source>There are unresolved dependency conflicts in this update，Please select update all</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠳᠦᠰᠢᠭᠯᠡᠯ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠵᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="531"/>
        <location filename="../src/appupdate.cpp" line="765"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="533"/>
        <source>Update ALL</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="534"/>
        <location filename="../src/appupdate.cpp" line="630"/>
        <location filename="../src/appupdate.cpp" line="768"/>
        <location filename="../src/appupdate.cpp" line="795"/>
        <location filename="../src/appupdate.cpp" line="849"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="581"/>
        <source>No Content.</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠤᠷᠠᠮᠳᠤᠯ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ.</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="591"/>
        <source>There are </source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠳᠦᠰᠢᠭᠯᠡᠯ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠨ᠎ᠠ </translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="591"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation> ᠨᠢᠭᠡ ᠰᠤᠹᠲ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠳᠠᠭᠤᠰᠬᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="722"/>
        <source>Cumulative updates</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠬᠤᠷᠠᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation type="vanished">ᠨᠢᠭᠡ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="723"/>
        <source>version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
    <message>
        <source>The update stopped because of low battery.</source>
        <translation type="vanished">ᠳ᠋ᠢᠶᠠᠨ ᠤ᠋ᠤᠢ ᠵᠢᠨ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠡᠯᠢᠶᠡᠳ ᠳᠤᠤᠷ᠎ᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠨᠢᠭᠡᠨᠳᠡ ᠦᠬᠡᠢᠰᠬᠡᠭᠳᠡᠪᠡ.</translation>
    </message>
    <message>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠳ᠋ᠢᠶᠠᠨ ᠤ᠋ᠤᠢ ᠵᠢᠨ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ50% ᠡᠴᠡ ᠳᠤᠤᠷ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>pkg will be uninstall!</source>
        <translation type="vanished">个软件包将被卸载！</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="69"/>
        <location filename="../src/appupdate.cpp" line="406"/>
        <location filename="../src/appupdate.cpp" line="504"/>
        <location filename="../src/appupdate.cpp" line="604"/>
        <location filename="../src/appupdate.cpp" line="620"/>
        <location filename="../src/appupdate.cpp" line="642"/>
        <location filename="../src/appupdate.cpp" line="753"/>
        <location filename="../src/appupdate.cpp" line="800"/>
        <location filename="../src/appupdate.cpp" line="955"/>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="61"/>
        <source>details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠭᠦᠢ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="102"/>
        <location filename="../src/appupdate.cpp" line="172"/>
        <source>Update log</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <source>Newest:</source>
        <translation type="vanished">最新：</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="182"/>
        <source>Download size:</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="183"/>
        <source>Install size:</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="209"/>
        <source>Current version:</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="764"/>
        <source>A single update will not automatically backup the system, if you want to backup, please click Update All.</source>
        <translation>ᠳᠠᠩ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠨᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠦᠬᠡᠢ᠂ ᠬᠡᠷᠪᠡ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠳᠠ ᠪᠤᠯ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="767"/>
        <source>Do not backup, continue to update</source>
        <translation>ᠨᠦᠬᠡᠴᠯᠡᠡᠬᠦ ᠦᠬᠡᠢ᠂ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="771"/>
        <source>This time will no longer prompt</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="853"/>
        <source>Ready to update</source>
        <translatorcomment>准备更新</translatorcomment>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠪᠡᠷ ᠪᠡᠯᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="931"/>
        <source>downloaded</source>
        <translatorcomment>已下载</translatorcomment>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Ready to install</source>
        <translation type="vanished">准备安装</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="937"/>
        <location filename="../src/appupdate.cpp" line="940"/>
        <source>downloading</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="937"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>ᠪᠤᠳᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/appupdate.cpp" line="1016"/>
        <source>No content.</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠤᠷᠠᠮᠳᠤᠯ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ.</translation>
    </message>
</context>
<context>
    <name>HistoryUpdateListWig</name>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="102"/>
        <source>Success</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/historyupdatelistwig.cpp" line="108"/>
        <source>Failed</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/backup.cpp" line="138"/>
        <source>system upgrade new backup</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠵᠤ ᠰᠢᠨ᠎ᠡ ᠨᠦᠬᠡᠴᠡ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/backup.cpp" line="139"/>
        <source>system upgrade increment backup</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠵᠤ ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠨᠡᠮᠡᠭᠳᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="78"/>
        <source>Advanced Option</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="106"/>
        <source>Server address settings</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="109"/>
        <source>If internal services, change the server address.</source>
        <translation>ᠳᠤᠳᠤᠷ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠬᠦᠰᠡᠪᠡᠯ᠂ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠡᠷ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠰᠤᠯᠢᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="123"/>
        <source>Port  ID </source>
        <translation>ᠫᠤᠷᠲ </translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="132"/>
        <source>Address</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="197"/>
        <source>reset</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="203"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="206"/>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="336"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="333"/>
        <source>Modification failed!</source>
        <translation>ᠵᠠᠰᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/SecondaryWidget/setwidget.cpp" line="334"/>
        <source>Attention</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TabWid</name>
    <message>
        <location filename="../src/tabwidget.cpp" line="72"/>
        <location filename="../src/tabwidget.cpp" line="190"/>
        <location filename="../src/tabwidget.cpp" line="1359"/>
        <location filename="../src/tabwidget.cpp" line="1409"/>
        <location filename="../src/tabwidget.cpp" line="1711"/>
        <location filename="../src/tabwidget.cpp" line="1880"/>
        <location filename="../src/tabwidget.cpp" line="1971"/>
        <location filename="../src/tabwidget.cpp" line="2193"/>
        <location filename="../src/tabwidget.cpp" line="2256"/>
        <location filename="../src/tabwidget.cpp" line="2585"/>
        <source>Check Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>initializing</source>
        <translation type="vanished">初始化中</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="262"/>
        <location filename="../src/tabwidget.cpp" line="701"/>
        <location filename="../src/tabwidget.cpp" line="1382"/>
        <location filename="../src/tabwidget.cpp" line="1727"/>
        <location filename="../src/tabwidget.cpp" line="1857"/>
        <location filename="../src/tabwidget.cpp" line="2134"/>
        <location filename="../src/tabwidget.cpp" line="2273"/>
        <location filename="../src/tabwidget.cpp" line="2304"/>
        <location filename="../src/tabwidget.cpp" line="2635"/>
        <source>UpdateAll</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Your system is the latest!</source>
        <translation type="vanished">您的系统已是最新！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="914"/>
        <location filename="../src/tabwidget.cpp" line="1450"/>
        <source>No Information!</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠳᠡᠬᠡ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Last refresh:</source>
        <translation type="vanished">检查时间：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="205"/>
        <source>Downloading and installing updates...</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation type="vanished">立即更新</translation>
    </message>
    <message>
        <source>Cancel update</source>
        <translation type="vanished">取消更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="242"/>
        <location filename="../src/tabwidget.cpp" line="662"/>
        <source>Being updated...</source>
        <translation>ᠶᠠᠭ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="253"/>
        <location filename="../src/tabwidget.cpp" line="1395"/>
        <source>Updatable app detected on your system!</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠮᠡᠳᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="280"/>
        <source>The backup restore partition could not be found. The system will not be backed up in this update!</source>
        <translation>ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠭᠰᠡᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="284"/>
        <source>Kylin backup restore tool is doing other operations, please update later.</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠤᠳᠤ ᠪᠡᠷ ᠪᠤᠰᠤᠳ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="289"/>
        <source>The source manager configuration file is abnormal, the system temporarily unable to update!</source>
        <translation>ᠡᠬᠢ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ᠂ ᠳᠦᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="294"/>
        <source>Backup already, no need to backup again.</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠨᠦᠬᠡᠴᠡᠯᠡᠪᠡ᠂ ᠳᠠᠬᠢᠵᠤ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠴᠢᠬᠤᠯᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="303"/>
        <source>Start backup,getting progress</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠨᠦᠬᠡᠴᠡᠯᠡᠪᠡ᠂ ᠤᠳᠤ ᠶᠠᠭ ᠠᠬᠢᠴᠠ ᠵᠢ ᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="319"/>
        <source>Kylin backup restore tool does not exist, this update will not backup the system!</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ UUID ᠢ᠋/ ᠵᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="228"/>
        <location filename="../src/tabwidget.cpp" line="335"/>
        <location filename="../src/tabwidget.cpp" line="341"/>
        <location filename="../src/tabwidget.cpp" line="362"/>
        <location filename="../src/tabwidget.cpp" line="667"/>
        <location filename="../src/tabwidget.cpp" line="1585"/>
        <location filename="../src/tabwidget.cpp" line="1635"/>
        <location filename="../src/tabwidget.cpp" line="1652"/>
        <location filename="../src/tabwidget.cpp" line="1739"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="351"/>
        <source>Calculated</source>
        <translation>ᠪᠤᠳᠤᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="436"/>
        <source>There are unresolved dependency conflicts in this update，Please contact the administrator!</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠳᠦᠰᠢᠭᠯᠡᠯ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="90"/>
        <location filename="../src/tabwidget.cpp" line="111"/>
        <location filename="../src/tabwidget.cpp" line="359"/>
        <location filename="../src/tabwidget.cpp" line="437"/>
        <location filename="../src/tabwidget.cpp" line="1736"/>
        <source>Prompt information</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="439"/>
        <source>Sure</source>
        <translation>ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="633"/>
        <location filename="../src/tabwidget.cpp" line="636"/>
        <source>In the download</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="633"/>
        <source>calculating</source>
        <translatorcomment>计算中</translatorcomment>
        <translation>ᠪᠤᠳᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>In the install...</source>
        <translation type="vanished">安装中...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="654"/>
        <source>Backup complete.</source>
        <translation>ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="672"/>
        <source>System is backing up...</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="690"/>
        <source>Backup interrupted, stop updating!</source>
        <translation>ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠶᠠᠪᠤᠴᠠ ᠳᠠᠰᠤᠯᠠᠭᠳᠠᠪᠠ᠂ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠪᠡᠨ ᠵᠤᠭᠰᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Backup finished!</source>
        <translation type="vanished">备份完成！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="709"/>
        <source>Kylin backup restore tool exception:</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ:</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="709"/>
        <source>There will be no backup in this update!</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="707"/>
        <source>The status of backup completion is abnormal</source>
        <translatorcomment>备份完成状态异常</translatorcomment>
        <translation>ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="830"/>
        <source>Getting update list</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠤᠳᠤ ᠶᠠᠭ ᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Software source update successed: </source>
        <translatorcomment>软件源更新成功： </translatorcomment>
        <translation type="vanished">ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠡᠬᠢ ᠵᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ: </translation>
    </message>
    <message>
        <source>Software source update failed: </source>
        <translation type="vanished">ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠡᠬᠢ ᠵᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ: </translation>
    </message>
    <message>
        <source>Update software source :</source>
        <translation type="vanished">更新软件源进度：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="236"/>
        <location filename="../src/tabwidget.cpp" line="976"/>
        <source>Update</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="358"/>
        <source>There are unresolved dependency conflicts in this update，Please select Dist-upgrade</source>
        <translatorcomment>本次更新存在无法解决的依赖冲突，请选择全盘更新</translatorcomment>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠳᠦᠰᠢᠭᠯᠡᠯ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠪᠦᠬᠦ ᠲᠠᠪᠠᠭ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠵᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="361"/>
        <source>Dist-upgrade</source>
        <translatorcomment>全盘更新</translatorcomment>
        <translation>ᠪᠦᠬᠦ ᠲᠠᠪᠠᠭ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="520"/>
        <location filename="../src/tabwidget.cpp" line="521"/>
        <source>The system is downloading the update!</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="527"/>
        <location filename="../src/tabwidget.cpp" line="528"/>
        <source>Downloading the updates...</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="532"/>
        <location filename="../src/tabwidget.cpp" line="533"/>
        <source>Installing the updates...</source>
        <translation>ᠶᠠᠭ ᠤᠭᠰᠠᠷᠴᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="625"/>
        <location filename="../src/tabwidget.cpp" line="1636"/>
        <location filename="../src/tabwidget.cpp" line="1653"/>
        <source>In the install</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="862"/>
        <location filename="../src/tabwidget.cpp" line="1667"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="870"/>
        <location filename="../src/tabwidget.cpp" line="1912"/>
        <location filename="../src/tabwidget.cpp" line="2078"/>
        <location filename="../src/tabwidget.cpp" line="2160"/>
        <source>Network exception, unable to check for updates!</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ᠂ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="876"/>
        <location filename="../src/tabwidget.cpp" line="1917"/>
        <location filename="../src/tabwidget.cpp" line="2083"/>
        <location filename="../src/tabwidget.cpp" line="2165"/>
        <source>No room to backup,upgrade failed.</source>
        <translation>ᠨᠦᠭᠡᠴᠡ ᠵᠢᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠬᠡᠢ᠂ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="882"/>
        <location filename="../src/tabwidget.cpp" line="1923"/>
        <location filename="../src/tabwidget.cpp" line="2089"/>
        <location filename="../src/tabwidget.cpp" line="2171"/>
        <source>Battery level is below 50%,and upgrade failed.</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ ᠵᠢᠨ ᠴᠠᠬᠢᠯᠭᠠᠨ 50% ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ᠂ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="887"/>
        <source>Checking update failed! </source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ! </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="888"/>
        <location filename="../src/tabwidget.cpp" line="1931"/>
        <location filename="../src/tabwidget.cpp" line="2096"/>
        <location filename="../src/tabwidget.cpp" line="2178"/>
        <source>Error Code: </source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋: </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="957"/>
        <source>The system is checking update :</source>
        <translation>ᠶᠠᠭ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ:</translation>
    </message>
    <message>
        <source>Download Limit(Kb/s)</source>
        <translation type="vanished">下载限速(Kb/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1244"/>
        <source>View history</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠲᠡᠤᠬᠡ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1085"/>
        <source>Update Settings</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1100"/>
        <source>Allowed to renewable notice</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠵᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠦᠶ᠎ᠡ ᠵᠢᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1116"/>
        <source>Backup current system before updates all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Download Limit(KB/s)</source>
        <translation type="vanished">下载限速(KB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1140"/>
        <source>It will be avaliable in the next download.</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠬᠤᠢᠨ᠎ᠠ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠤᠳᠠᠭ᠎ᠠ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>Automatically download and install updates</source>
        <translation type="vanished">自动下载和安装更新</translation>
    </message>
    <message>
        <source>After it is turned on, the system will automatically download and install updates when there is an available network and available backup and restore partitions.</source>
        <translation type="vanished">开启后，当有可用网络和可用备份和恢复分区时，系统会自动下载和安装更新。</translation>
    </message>
    <message>
        <source>Upgrade during poweroff</source>
        <translation type="vanished">关机检测更新</translation>
    </message>
    <message>
        <source>Download Limit(kB/s)</source>
        <translation type="vanished">下载限速(kB/s)</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1168"/>
        <source>The system will automatically updates when there is an available network and backup.</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠬᠡᠷᠪᠡ ᠨᠦᠬᠡᠴᠡ ᠲᠠᠢ ᠪᠤᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠢᠨ ᠵᠡᠷᠬᠡᠴᠡᠬᠡ ᠤᠭᠰᠠᠷᠴᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠳᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1318"/>
        <source>Ready to install</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠪᠡᠷ ᠪᠡᠯᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="188"/>
        <location filename="../src/tabwidget.cpp" line="261"/>
        <location filename="../src/tabwidget.cpp" line="899"/>
        <location filename="../src/tabwidget.cpp" line="952"/>
        <location filename="../src/tabwidget.cpp" line="1372"/>
        <location filename="../src/tabwidget.cpp" line="1392"/>
        <location filename="../src/tabwidget.cpp" line="1423"/>
        <location filename="../src/tabwidget.cpp" line="1460"/>
        <location filename="../src/tabwidget.cpp" line="1951"/>
        <location filename="../src/tabwidget.cpp" line="2116"/>
        <location filename="../src/tabwidget.cpp" line="2228"/>
        <source>Last Checked:</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <source>Your system is the latest:V10sp1-</source>
        <translation type="vanished">你的系统已是最新版本：V10sp1-</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="181"/>
        <location filename="../src/tabwidget.cpp" line="254"/>
        <location filename="../src/tabwidget.cpp" line="892"/>
        <location filename="../src/tabwidget.cpp" line="945"/>
        <location filename="../src/tabwidget.cpp" line="1365"/>
        <location filename="../src/tabwidget.cpp" line="1385"/>
        <location filename="../src/tabwidget.cpp" line="1416"/>
        <location filename="../src/tabwidget.cpp" line="1944"/>
        <location filename="../src/tabwidget.cpp" line="2109"/>
        <location filename="../src/tabwidget.cpp" line="2221"/>
        <source>No information!</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠳᠡᠬᠡ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1132"/>
        <source>Download Limit</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠬᠤᠷᠳᠤᠴᠠ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1247"/>
        <source>Advanced</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1539"/>
        <location filename="../src/tabwidget.cpp" line="1564"/>
        <source>Dependency conflict exists in this update,need to be completely repaired!</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠳᠦᠰᠢᠭᠯᠡᠯ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠪᠦᠬᠦ ᠲᠠᠪᠠᠭ ᠢ᠋ ᠰᠡᠯᠪᠢᠨ ᠵᠠᠰᠠᠵᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1541"/>
        <source>There are </source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠳᠦᠰᠢᠭᠯᠡᠯ ᠤ᠋ᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠨ᠎ᠠ </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1541"/>
        <source> packages going to be removed,Please confirm whether to accept!</source>
        <translation> ᠨᠢᠭᠡ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠳᠠᠭᠤᠰᠬᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1566"/>
        <source>packages are going to be removed,Please confirm whether to accept!</source>
        <translation>ᠨᠢᠭᠡ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1577"/>
        <source>trying to reconnect </source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1577"/>
        <source> times</source>
        <translation> ᠤᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
    <message>
        <source>Auto-Update is backing up......</source>
        <translatorcomment>自动更新进程正在备份中......</translatorcomment>
        <translation type="vanished">ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠠᠬᠢᠴᠠ ᠶᠠᠭ ᠨᠦᠬᠡᠴᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ......</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1603"/>
        <source>The updater is NOT start</source>
        <translation>ᠠᠷᠤ ᠲᠠᠪᠴᠠᠩ ᠤ᠋ᠨ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠢ᠋ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠭᠡᠳᠦᠶ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1621"/>
        <source>The progress is updating...</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠢᠨ ᠵᠡᠷᠬᠡᠴᠡᠬᠡ ᠤᠭᠰᠠᠷᠴᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1631"/>
        <location filename="../src/tabwidget.cpp" line="1648"/>
        <source>The progress is installing...</source>
        <translation>ᠶᠠᠭ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠢᠨ ᠵᠡᠷᠬᠡᠴᠡᠬᠡ ᠤᠭᠰᠠᠷᠴᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1662"/>
        <source>The updater is busy！</source>
        <translation>ᠠᠷᠤ ᠲᠠᠪᠴᠠᠩ ᠤ᠋ᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠶᠠᠭ ᠡᠵᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ！</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1680"/>
        <location filename="../src/tabwidget.cpp" line="1723"/>
        <source>Updating the software source</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠡᠬᠢ ᠶᠠᠭ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>The battery is below 50% and the update cannot be downloaded</source>
        <translation type="vanished">电池电量低于 50%，无法下载更新</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="114"/>
        <location filename="../src/tabwidget.cpp" line="410"/>
        <location filename="../src/tabwidget.cpp" line="422"/>
        <location filename="../src/tabwidget.cpp" line="1706"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1735"/>
        <source>Please back up the system before all updates to avoid unnecessary losses</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ᠂ ᠬᠤᠬᠢᠷᠠᠯ ᠭᠠᠷᠬᠤ ᠡᠴᠡ ᠰᠡᠷᠬᠡᠢᠯᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1740"/>
        <source>Only Update</source>
        <translation>ᠰᠢᠭ᠋ᠤᠳ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1741"/>
        <source>Back And Update</source>
        <translation>ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1904"/>
        <location filename="../src/tabwidget.cpp" line="2070"/>
        <location filename="../src/tabwidget.cpp" line="2151"/>
        <location filename="../src/tabwidget.cpp" line="2275"/>
        <source>update has been canceled!</source>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠦᠬᠡᠢᠰᠬᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1886"/>
        <location filename="../src/tabwidget.cpp" line="1979"/>
        <source>This update has been completed！</source>
        <translatorcomment>本次更新已完成！</translatorcomment>
        <translation>ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠰᠬᠠᠪᠠ！</translation>
    </message>
    <message>
        <source>Your system is the latest:V10</source>
        <translation type="vanished">ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠪᠠᠢᠨ᠎ᠠ:V10</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="227"/>
        <source>Keeping update</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="229"/>
        <source>It is recommended to back up the system before all updates to avoid unnecessary losses!</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ᠂ ᠬᠤᠬᠢᠷᠠᠯ ᠭᠠᠷᠬᠤ ᠡᠴᠡ ᠰᠡᠷᠬᠡᠢᠯᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="408"/>
        <source>Insufficient disk space to download updates!</source>
        <translation>ᠳ᠋ᠢᠰᠺ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠵᠠᠶ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠭᠡᠢ ᠂ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="491"/>
        <source>supposed</source>
        <translation>ᠪᠠᠷᠤᠭᠴᠠᠯᠠᠭᠰᠠᠨ ᠡᠵᠡᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="562"/>
        <source>s</source>
        <translation>ᠰᠸᠺᠦᠨ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="564"/>
        <source>min</source>
        <translation>ᠮᠢᠨᠦ᠋ᠲ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="566"/>
        <source>h</source>
        <translation>ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1167"/>
        <source>Automatically updates</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="418"/>
        <source>The update stopped because of low battery.</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᠤ᠋ᠤᠢ ᠵᠢᠨ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠡᠯᠢᠶᠡᠳ ᠳᠤᠤᠷ᠎ᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠨᠢᠭᠡᠨᠳᠡ ᠦᠬᠡᠢᠰᠬᠡᠭᠳᠡᠪᠡ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="419"/>
        <source>The system update requires that the battery power is not less than 50%</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠳ᠋ᠢᠶᠠᠨ ᠤ᠋ᠤᠢ ᠵᠢᠨ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ 50% ᠡᠴᠡ ᠳᠤᠤᠷ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Part of the update failed!</source>
        <translation type="vanished">ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Failure reason:</source>
        <translatorcomment>失败原因：</translatorcomment>
        <translation type="vanished">ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠰᠢᠯᠳᠠᠭᠠᠨ:</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1972"/>
        <source>Finish the download!</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ!</translation>
    </message>
    <message>
        <source>The system has download the update!</source>
        <translation type="vanished">ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ!</translation>
    </message>
    <message>
        <source>It&apos;s need to reboot to make the install avaliable</source>
        <translation type="vanished">ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ᠂ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠤᠭᠰᠠᠷᠬᠤ ᠤᠤ</translation>
    </message>
    <message>
        <source>Reboot notification</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠰᠠᠨᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Reboot rightnow</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="93"/>
        <source>Later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Part of the update success!</source>
        <translation type="vanished">ᠬᠡᠰᠡᠭ ᠪᠦᠯᠦᠭ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>All the update has been downloaded.</source>
        <translation type="vanished">ᠪᠦᠬᠦᠢᠯᠡ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠪᠠ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2316"/>
        <source>An important update is in progress, please wait.</source>
        <translation>ᠶᠠᠭ ᠨᠢᠭᠡ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠴᠢᠬᠤᠯᠠ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠶᠠᠪᠤᠭᠳᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <source>Failed to write configuration file, this update will not back up the system!</source>
        <translation type="vanished">ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠵᠢᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Insufficient backup space, this update will not backup your system!</source>
        <translation type="vanished">ᠨᠦᠬᠡᠴᠡ ᠵᠢᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Kylin backup restore tool could not find the UUID, this update will not backup the system!</source>
        <translation type="vanished">ᠴᠢ ᠯᠢᠨ ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ UUID ᠢ᠋/ ᠵᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠳᠤᠰ ᠤᠳᠠᠭ᠎ᠠ ᠵᠢᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠳ᠋ᠤ᠌ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>The backup restore partition is abnormal. You may not have a backup restore partition.For more details,see /var/log/backup.log</source>
        <translation type="vanished">备份还原分区异常，您可能没有备份还原分区。更多详细信息，可以参看/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1766"/>
        <location filename="../src/tabwidget.cpp" line="2352"/>
        <source>Calculating Capacity...</source>
        <translation>ᠪᠤᠳᠤᠬᠤ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ...</translation>
    </message>
    <message>
        <source>The system backup partition is not detected. Do you want to continue updating?</source>
        <translation type="vanished">未检测到系统备份还原分区，是否继续更新？</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2475"/>
        <source>Calculating</source>
        <translation>ᠪᠤᠳᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2465"/>
        <location filename="../src/tabwidget.cpp" line="2488"/>
        <source>The system is updating...</source>
        <translation>ᠶᠠᠭ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠪᠡᠷ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="92"/>
        <source>Reboot</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="180"/>
        <location filename="../src/tabwidget.cpp" line="1362"/>
        <location filename="../src/tabwidget.cpp" line="1412"/>
        <location filename="../src/tabwidget.cpp" line="1891"/>
        <location filename="../src/tabwidget.cpp" line="2056"/>
        <location filename="../src/tabwidget.cpp" line="2211"/>
        <location filename="../src/tabwidget.cpp" line="2258"/>
        <source>Your system is the latest:</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠨ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1685"/>
        <location filename="../src/tabwidget.cpp" line="1993"/>
        <location filename="../src/tabwidget.cpp" line="2205"/>
        <source>reboot rightnow</source>
        <translation>ᠳᠠᠷᠤᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1703"/>
        <source>Reboot failed!</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1930"/>
        <location filename="../src/tabwidget.cpp" line="2095"/>
        <location filename="../src/tabwidget.cpp" line="2177"/>
        <source>Update failed! </source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ! </translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="1992"/>
        <location filename="../src/tabwidget.cpp" line="2204"/>
        <source>The system has download the update，and you are suggested to reboot to use the new version.</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠪᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2336"/>
        <source>insufficient backup space</source>
        <translation>ᠨᠦᠭᠡᠴᠡ ᠵᠢᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2344"/>
        <source>backup failed</source>
        <translation>ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2543"/>
        <source>Auto-Update progress is installing new file：</source>
        <translatorcomment>系统自动更新功能正在安装新文件：</translatorcomment>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠤᠭᠠᠳ ᠰᠢᠨ᠎ᠡ ᠹᠠᠢᠯ ᠢ᠋ ᠶᠠᠭ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ：</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2553"/>
        <source>Auto-Update progress finished!</source>
        <translatorcomment>系统自动更新完成！</translatorcomment>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2561"/>
        <source>Auto-Update progress fail in backup!</source>
        <translatorcomment>自动更新安装时备份失败！</translatorcomment>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠤᠭᠰᠠᠷᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠨᠦᠬᠡᠴᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2584"/>
        <source>Failed in updating because of broken environment.</source>
        <translation>ᠤᠷᠴᠢᠨ ᠳᠤᠭᠤᠷᠢᠨ ᠡᠪᠳᠡᠭᠳᠡᠭᠰᠡᠨ ᠡᠴᠡ ᠪᠤᠯᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/tabwidget.cpp" line="2596"/>
        <source>It&apos;s fixing up the environment...</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠤᠷᠴᠢᠨ ᠳᠤᠭᠤᠷᠢᠨ ᠢ᠋ ᠵᠠᠰᠠᠨ ᠰᠢᠨᠡᠳᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
</context>
<context>
    <name>UpdateDbus</name>
    <message>
        <location filename="../src/updatedbus.cpp" line="139"/>
        <source>System-Upgrade</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/updatedbus.cpp" line="142"/>
        <source>ukui-control-center-update</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ- ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
</context>
<context>
    <name>UpdateLog</name>
    <message>
        <location filename="../src/updatelog.cpp" line="23"/>
        <source>Update log</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
</context>
<context>
    <name>UpdateSource</name>
    <message>
        <location filename="../src/updatesource.cpp" line="64"/>
        <source>Connection failed, please reconnect!</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠴᠦᠷᠬᠡᠯᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>Upgrade</name>
    <message>
        <location filename="../upgrade.cpp" line="11"/>
        <source>Upgrade</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="65"/>
        <source>View history</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠲᠡᠤᠬᠡ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
        <extra-contents_path>/Upgrade/View history</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="67"/>
        <source>Update Settings</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Upgrade/Update Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="69"/>
        <source>Allowed to renewable notice</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠵᠢ ᠮᠡᠳᠡᠭᠳᠡᠬᠦ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ</translation>
        <extra-contents_path>/Upgrade/Allowed to renewable notice</extra-contents_path>
    </message>
    <message>
        <location filename="../upgrade.cpp" line="71"/>
        <source>Automatically download and install updates</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠪᠠ ᠤᠭᠰᠠᠷᠴᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
        <extra-contents_path>/Upgrade/Automatically download and install updates</extra-contents_path>
    </message>
</context>
<context>
    <name>dependencyfixdialog</name>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="26"/>
        <source>show details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="35"/>
        <source>uninstall and update</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>uninstall</source>
        <translation type="vanished">移除</translation>
    </message>
    <message>
        <location filename="../src/dependencyfixdialog.cpp" line="37"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>fixbrokeninstalldialog</name>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="80"/>
        <source>We need to fix up the environment!</source>
        <translation>ᠤᠷᠴᠢᠨ ᠳᠤᠭᠤᠷᠢᠨ ᠢ᠋ ᠵᠠᠰᠠᠨ ᠰᠢᠨᠡᠳᠬᠡᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="86"/>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation>ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠬᠤ ᠬᠡᠰᠡᠭ ᠪᠦᠯᠦᠭ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠰᠢᠨᠡᠴᠢᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="95"/>
        <source>The following packages will be uninstalled:</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠳ᠋ᠡᠬᠢ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠨ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="120"/>
        <source>PKG Details</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭᠠᠨ ᠤ᠋ ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="130"/>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="397"/>
        <source>details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠭᠦᠢ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="134"/>
        <source>Keep</source>
        <translation>ᠳᠡᠪᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="138"/>
        <source>Remove</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="294"/>
        <source>Attention on update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/fixbrokeninstalldialog.cpp" line="385"/>
        <source>back</source>
        <translation>ᠬᠤᠷᠢᠶᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>fixupdetaillist</name>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="59"/>
        <source>No content.</source>
        <translation>ᠳᠦᠷ ᠳᠡᠯᠬᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="115"/>
        <source>Update Details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/fixupdetaillist.cpp" line="549"/>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>m_updatelog</name>
    <message>
        <location filename="../src/m_updatelog.cpp" line="58"/>
        <source>No content.</source>
        <translation>ᠳᠦᠷ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>Update Details</source>
        <translation type="vanished">ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="113"/>
        <source>No Contents</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠳ᠋ᠤ᠌ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="558"/>
        <location filename="../src/m_updatelog.cpp" line="594"/>
        <source>Search content</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/m_updatelog.cpp" line="641"/>
        <source>History Log</source>
        <translation>ᠳᠡᠤᠬᠡᠨ ᠰᠢᠨᠡᠴᠢᠯᠡᠯ</translation>
    </message>
</context>
<context>
    <name>updatedeleteprompt</name>
    <message>
        <source>Dependency conflict exists in this update!</source>
        <translation type="vanished">本次更新存在依赖冲突！</translation>
    </message>
    <message>
        <source>There will be uninstall some packages to complete the update!</source>
        <translation type="vanished">将卸载部分软件包以完成更新！</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="78"/>
        <source>The following packages will be uninstalled:</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠳ᠋ᠡᠬᠢ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠨ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="97"/>
        <source>PKG Details</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠪᠠᠭᠯᠠᠭᠠᠨ ᠤ᠋ ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="241"/>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>details</source>
        <translation type="vanished">详情</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="105"/>
        <source>Keep</source>
        <translation>ᠳᠡᠪᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/updatedeleteprompt.cpp" line="109"/>
        <source>Remove</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Update Prompt</source>
        <translation type="vanished">更新提示</translation>
    </message>
    <message>
        <source>back</source>
        <translation type="vanished">收起</translation>
    </message>
</context>
</TS>
