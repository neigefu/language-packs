<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QObject</name>
    <message>
        <location filename="../desktopnotify.cpp" line="23"/>
        <location filename="../mainwindow.cpp" line="25"/>
        <location filename="../rebootinstallnotify.cpp" line="18"/>
        <source>Reboot</source>
        <translation>重新啟動</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="25"/>
        <location filename="../mainwindow.cpp" line="27"/>
        <location filename="../rebootinstallnotify.cpp" line="20"/>
        <source>Later</source>
        <translation>後</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="33"/>
        <location filename="../mainwindow.cpp" line="35"/>
        <location filename="../mainwindow.cpp" line="69"/>
        <location filename="../mainwindow.cpp" line="97"/>
        <location filename="../mainwindow.cpp" line="125"/>
        <location filename="../rebootinstallnotify.cpp" line="28"/>
        <source>System-Upgrade</source>
        <translation>系統升級</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="36"/>
        <location filename="../mainwindow.cpp" line="38"/>
        <location filename="../mainwindow.cpp" line="72"/>
        <location filename="../mainwindow.cpp" line="100"/>
        <location filename="../mainwindow.cpp" line="128"/>
        <location filename="../rebootinstallnotify.cpp" line="31"/>
        <source>ukui-control-center-upgrade</source>
        <translation>烏奎-控制-中心-升級</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="73"/>
        <source>Reboot failed because battery power is lower than 50%.</source>
        <translation>重新啟動失敗，因為電池電量低於50%。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="101"/>
        <source>Backup room is poor.</source>
        <translation>備份室很差。</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="129"/>
        <source>Reboot failed.</source>
        <translation>重新啟動失敗。</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="37"/>
        <location filename="../mainwindow.cpp" line="39"/>
        <location filename="../rebootinstallnotify.cpp" line="32"/>
        <source>It&apos;s suggested to reboot.</source>
        <translation>建議重新啟動。</translation>
    </message>
</context>
</TS>
