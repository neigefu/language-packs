<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../desktopnotify.cpp" line="23"/>
        <location filename="../mainwindow.cpp" line="25"/>
        <location filename="../rebootinstallnotify.cpp" line="18"/>
        <source>Reboot</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="25"/>
        <location filename="../mainwindow.cpp" line="27"/>
        <location filename="../rebootinstallnotify.cpp" line="20"/>
        <source>Later</source>
        <translation>རྗེས་སུ།</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="33"/>
        <location filename="../mainwindow.cpp" line="35"/>
        <location filename="../mainwindow.cpp" line="69"/>
        <location filename="../mainwindow.cpp" line="97"/>
        <location filename="../mainwindow.cpp" line="125"/>
        <location filename="../rebootinstallnotify.cpp" line="28"/>
        <source>System-Upgrade</source>
        <translation>མ་ལག་རིམ་སྤར།</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="36"/>
        <location filename="../mainwindow.cpp" line="38"/>
        <location filename="../mainwindow.cpp" line="72"/>
        <location filename="../mainwindow.cpp" line="100"/>
        <location filename="../mainwindow.cpp" line="128"/>
        <location filename="../rebootinstallnotify.cpp" line="31"/>
        <source>ukui-control-center-upgrade</source>
        <translation>ukui-control-center-update</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="73"/>
        <source>Reboot failed because battery power is lower than 50%.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="101"/>
        <source>Backup room is poor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="129"/>
        <source>Reboot failed.</source>
        <translation>བསྐྱར་སློང་ཕམ་པ།</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="37"/>
        <location filename="../mainwindow.cpp" line="39"/>
        <location filename="../rebootinstallnotify.cpp" line="32"/>
        <source>It&apos;s suggested to reboot.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
