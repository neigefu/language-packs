<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../desktopnotify.cpp" line="24"/>
        <location filename="../desktopnotify.cpp" line="139"/>
        <location filename="../mainwindow.cpp" line="27"/>
        <location filename="../mainwindow.cpp" line="263"/>
        <location filename="../rebootinstallnotify.cpp" line="24"/>
        <source>Reboot</source>
        <translation>立即重启</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="26"/>
        <location filename="../desktopnotify.cpp" line="141"/>
        <location filename="../mainwindow.cpp" line="29"/>
        <location filename="../mainwindow.cpp" line="265"/>
        <location filename="../rebootinstallnotify.cpp" line="26"/>
        <source>Later</source>
        <translation>稍后重启</translation>
    </message>
    <message>
        <location filename="../checkpower.cpp" line="26"/>
        <location filename="../checkpower.cpp" line="64"/>
        <location filename="../checkpower.cpp" line="99"/>
        <location filename="../checkpower.cpp" line="127"/>
        <location filename="../checkpower.cpp" line="155"/>
        <location filename="../desktopnotify.cpp" line="36"/>
        <location filename="../desktopnotify.cpp" line="63"/>
        <location filename="../desktopnotify.cpp" line="150"/>
        <location filename="../mainwindow.cpp" line="39"/>
        <location filename="../mainwindow.cpp" line="66"/>
        <location filename="../mainwindow.cpp" line="93"/>
        <location filename="../mainwindow.cpp" line="176"/>
        <location filename="../mainwindow.cpp" line="204"/>
        <location filename="../mainwindow.cpp" line="232"/>
        <location filename="../mainwindow.cpp" line="274"/>
        <location filename="../rebootinstallnotify.cpp" line="33"/>
        <source>System-Upgrade</source>
        <translation>系统更新</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="40"/>
        <location filename="../desktopnotify.cpp" line="67"/>
        <location filename="../desktopnotify.cpp" line="154"/>
        <location filename="../mainwindow.cpp" line="43"/>
        <location filename="../mainwindow.cpp" line="70"/>
        <location filename="../mainwindow.cpp" line="97"/>
        <location filename="../mainwindow.cpp" line="278"/>
        <location filename="../rebootinstallnotify.cpp" line="37"/>
        <source>After restarting, the new system will be used.</source>
        <translation>将在重启后使用新系统。</translation>
    </message>
    <message>
        <location filename="../checkpower.cpp" line="102"/>
        <location filename="../mainwindow.cpp" line="179"/>
        <source>Low battery level, system update terminated</source>
        <translation>电池电量过低，系统更新已终止。</translation>
    </message>
    <message>
        <location filename="../checkpower.cpp" line="103"/>
        <location filename="../mainwindow.cpp" line="180"/>
        <source>System updates are carried out when the battery level is not less than 50%.</source>
        <translation>系统更新在电池电量不低于50%时进行。</translation>
    </message>
    <message>
        <location filename="../checkpower.cpp" line="130"/>
        <location filename="../mainwindow.cpp" line="207"/>
        <source>The system disk space is insufficient to complete the update.</source>
        <translation>系统磁盘空间不足，无法完成更新。</translation>
    </message>
    <message>
        <source>ukui-control-center-upgrade</source>
        <translation type="vanished">设置-更新</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="39"/>
        <location filename="../desktopnotify.cpp" line="66"/>
        <location filename="../desktopnotify.cpp" line="153"/>
        <location filename="../mainwindow.cpp" line="42"/>
        <location filename="../mainwindow.cpp" line="69"/>
        <location filename="../mainwindow.cpp" line="96"/>
        <location filename="../mainwindow.cpp" line="277"/>
        <location filename="../rebootinstallnotify.cpp" line="36"/>
        <source>Update download completed, do you want to restart the system?</source>
        <translation>更新下载完成,是否重启系统？</translation>
    </message>
    <message>
        <source>Reboot failed because battery power is lower than 50%.</source>
        <translation type="vanished">电池电量低于50%，升级失败。</translation>
    </message>
    <message>
        <source>Backup room is poor.</source>
        <translation type="vanished">系统磁盘空间不足，无法完成更新。</translation>
    </message>
    <message>
        <location filename="../checkpower.cpp" line="29"/>
        <location filename="../checkpower.cpp" line="30"/>
        <location filename="../checkpower.cpp" line="67"/>
        <location filename="../checkpower.cpp" line="158"/>
        <location filename="../mainwindow.cpp" line="235"/>
        <source>Reboot failed.</source>
        <translation>重启失败！</translation>
    </message>
    <message>
        <source>It&apos;s suggested to reboot.</source>
        <translation type="vanished">建议立即重启进行系统更新</translation>
    </message>
</context>
</TS>
