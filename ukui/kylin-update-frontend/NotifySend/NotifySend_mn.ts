<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <location filename="../desktopnotify.cpp" line="23"/>
        <location filename="../mainwindow.cpp" line="25"/>
        <location filename="../rebootinstallnotify.cpp" line="18"/>
        <source>Reboot</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="25"/>
        <location filename="../mainwindow.cpp" line="27"/>
        <location filename="../rebootinstallnotify.cpp" line="20"/>
        <source>Later</source>
        <translation>ᠪᠠᠢᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="33"/>
        <location filename="../mainwindow.cpp" line="35"/>
        <location filename="../mainwindow.cpp" line="69"/>
        <location filename="../mainwindow.cpp" line="97"/>
        <location filename="../mainwindow.cpp" line="125"/>
        <location filename="../rebootinstallnotify.cpp" line="28"/>
        <source>System-Upgrade</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="36"/>
        <location filename="../mainwindow.cpp" line="38"/>
        <location filename="../mainwindow.cpp" line="72"/>
        <location filename="../mainwindow.cpp" line="100"/>
        <location filename="../mainwindow.cpp" line="128"/>
        <location filename="../rebootinstallnotify.cpp" line="31"/>
        <source>ukui-control-center-upgrade</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ - ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="73"/>
        <source>Reboot failed because battery power is lower than 50%.</source>
        <translation>ᠳ᠋ᠢᠶᠠᠨ ᡂᠢ ᠵᠢᠨ ᠴᠠᠬᠢᠯᠭᠠᠨ 50% ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ᠂ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="101"/>
        <source>Backup room is poor.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳ᠋ᠢᠰᠺ ᠤ᠋ᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠬᠡᠢ᠂ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="129"/>
        <source>Reboot failed.</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../desktopnotify.cpp" line="37"/>
        <location filename="../mainwindow.cpp" line="39"/>
        <location filename="../rebootinstallnotify.cpp" line="32"/>
        <source>It&apos;s suggested to reboot.</source>
        <translation>ᠳᠠᠷᠤᠢ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠵᠢ ᠵᠦᠪᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
</context>
</TS>
