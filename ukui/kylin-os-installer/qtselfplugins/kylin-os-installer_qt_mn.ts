<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2316"/>
        <source>Services</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide %1</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Others</source>
        <translation>ᠪᠤᠰᠤᠳ ᠢ᠋ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preferences...</source>
        <translation>ᠤᠨᠴᠠ ᠳᠤᠷᠠᠰᠢᠯ ᠤ᠋ᠨ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠳᠠ...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit %1</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ %1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About %1</source>
        <translation>ᠲᠤᠬᠠᠢ %1</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/audiooutput.cpp" line="+375"/>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt; ᠠᠦ᠋ᠳᠢᠤ᠋ ᠵᠢᠨ ᠳᠠᠬᠢᠵᠤ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ &lt;b&gt;%1&lt;/b&gt; ᠠᠵᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ.&lt;br/&gt; ᠪᠤᠴᠠᠵᠤ ᠦᠩᠬᠦᠷᠢᠭᠡᠳ &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation>&lt;html&gt; ᠠᠦ᠋ᠳᠢᠤ᠋ ᠵᠢᠨ ᠳᠠᠬᠢᠵᠤ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠪᠡᠷ ᠰᠤᠯᠢᠬᠤ &lt;b&gt;%1&lt;/b&gt;，&lt;br/&gt; ᠲᠡᠷᠡ ᠨᠢ ᠰᠠᠶᠢᠬᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠵᠤᠬᠢᠬᠤ ᠨᠡᠩ ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ ᠳᠦᠪᠰᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠪᠡ.&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation>&apos;%1&apos; ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳ᠋ᠤ᠌ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>CloseButton</name>
    <message>
        <location filename="../src/gui/widgets/qtabbar.cpp" line="+2251"/>
        <source>Close Tab</source>
        <translation>ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠵᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/phononnamespace.cpp" line="+55"/>
        <source>Notifications</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Music</source>
        <translation>ᠳᠠᠭᠤᠤ ᠬᠦᠭᠵᠢᠮ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video</source>
        <translation>ᠸᠢᠳᠢᠤ᠋</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication</source>
        <translation>ᠮᠡᠳᠡᠭᠡᠯᠡᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Games</source>
        <translation>ᠳᠤᠭᠯᠠᠭᠠᠮ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Accessibility</source>
        <translation>ᠰᠠᠭᠠᠳ ᠦᠬᠡᠢ ᠤᠷᠴᠢᠨ</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/backend.cpp" line="+171"/>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ ᠄ ᠦᠵᠡᠬᠦ ᠳᠦ ᠲᠠgstreamer0.10-plugins-good ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠶᠢ ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠦᠬᠡᠢ.
     ᠵᠠᠷᠢᠮ ᠸᠢᠳᠢᠤ᠋ ᠶᠢᠨ ᠤᠨᠴᠤᠭᠤᠢ ᠰᠢᠨᠵᠢ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ: ᠦᠵᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠲᠠ ᠰᠠᠭᠤᠷᠢ GStreamer ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠢ᠋ ᠵᠢ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.
            ᠪᠦᠬᠦᠢᠯᠡ ᠠᠦ᠋ᠳᠢᠤ᠋ ᠬᠢᠬᠡᠳ ᠸᠢᠳᠢᠤ᠋ ᠵᠢᠨ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠪᠦᠷ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/mediaobject.cpp" line="+90"/>
        <source>Cannot start playback. 

Check your Gstreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠵᠤ ᠳᠠᠯᠪᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ. 

ᠲᠠ Gstreamer ᠢ᠋/ ᠵᠢ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ ᠵᠢᠴᠢ
libgstreamer-plugins-base ᠢ᠋/ ᠵᠢ ᠯᠠᠪᠳᠠᠢ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ ᠪᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation>ᠨᠢᠭᠡ ᠺᠤᠳ᠋ ᠳᠠᠢᠯᠤᠭᠤᠷ ᠳᠤᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠠ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠰᠢᠭ᠋ ᠺᠤᠳ᠋ ᠳᠠᠢᠯᠤᠭᠤᠷ ᠢ᠋ ᠤᠭᠰᠠᠷᠴᠤ ᠡᠨᠡ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ: %0</translation>
    </message>
    <message>
        <location line="+676"/>
        <location line="+8"/>
        <location line="+15"/>
        <location line="+9"/>
        <location line="+6"/>
        <location line="+19"/>
        <location line="+335"/>
        <location line="+24"/>
        <source>Could not open media source.</source>
        <translation>ᠵᠠᠭᠤᠴᠢᠯᠠᠭᠤᠷ ᠤ᠋ᠨ ᠡᠬᠢ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>Invalid source type.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠡᠬᠢ ᠡᠬᠦᠰᠪᠦᠷᠢ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ.</translation>
    </message>
    <message>
        <location line="+377"/>
        <source>Could not locate media source.</source>
        <translation>ᠵᠠᠭᠤᠴᠢᠯᠠᠭᠤᠷ ᠤ᠋ᠨ ᠡᠬᠢ ᠪᠡᠷ ᠪᠠᠢᠷᠢ ᠳᠤᠭᠳᠠᠭᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not open audio device. The device is already in use.</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ. ᠳᠤᠰ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠠᠭ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Could not decode media source.</source>
        <translation>ᠺᠤᠳ᠋ ᠳᠠᠢᠯᠠᠵᠤ ᠦᠯᠦ ᠪᠤᠯᠬᠤ ᠵᠠᠭᠤᠴᠢᠯᠠᠭᠤᠷ ᠤ᠋ᠨ ᠡᠬᠢ.</translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+42"/>
        <location line="+18"/>
        <source>Volume: %1%</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ: %1%</translation>
    </message>
    <message>
        <location line="-15"/>
        <location line="+18"/>
        <location line="+54"/>
        <source>Use this slider to adjust the volume. The leftmost position is 0%, the rightmost is %1%</source>
        <translation>ᠲᠠ ᠲᠤᠰ ᠭᠤᠯᠭᠤᠬᠤ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ᠂ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠵᠡᠬᠦᠨ ᠲᠠᠯ᠎ᠠ ᠨᠢ 0%, ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠷᠠᠭᠤᠨ ᠲᠠᠯ᠎ᠠ ᠨᠢ %1%</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="+481"/>
        <source>%1, %2 not defined</source>
        <translation>%1, %2 ᠳᠤᠮᠢᠶᠠᠯᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Ambiguous %1 not handled</source>
        <translation>ᠳᠤᠳᠤᠷᠬᠠᠢ ᠪᠤᠰᠤ %1 ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠳᠡᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="+285"/>
        <source>True</source>
        <translation>ᠦᠨᠡᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>ᠬᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location line="+505"/>
        <source>Insert</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="+864"/>
        <source>Copy or Move a File</source>
        <translation>ᠨᠢᠭᠡ ᠹᠠᠢᠯ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠬᠤ ᠪᠤᠶᠤ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Read: %1</source>
        <translation>ᠤᠩᠰᠢᠬᠤ: %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+30"/>
        <source>Write: %1</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ: %1</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+1575"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-157"/>
        <location line="+49"/>
        <location line="+2153"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_mac.cpp" line="+110"/>
        <source>All Files (*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ(*)</translation>
    </message>
    <message>
        <location line="-2089"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attributes</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+2031"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="-1991"/>
        <source>Look &amp;in:</source>
        <translation>ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠡᠪᠴᠢᠶ᠎ᠡ ᠵᠢᠨ ᠡᠷᠢᠬᠦ:</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1981"/>
        <location line="+16"/>
        <source>File &amp;name:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ(&amp;N)：</translation>
    </message>
    <message>
        <location line="-1996"/>
        <source>File &amp;type:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ(&amp;T)：</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Back</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠢᠭᠡ ᠳᠡᠰ ᠳᠡᠰᠢ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Create New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List View</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Detail View</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Preview File Info</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Preview File Contents</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Read-write</source>
        <translation>ᠤᠩᠰᠢᠵᠤ ᠪᠢᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read-only</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write-only</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠪᠢᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inaccessible</source>
        <translation>ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Symlink to File</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Special</source>
        <translation>ᠤᠨᠴᠤᠭᠤᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dir</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Special</source>
        <translation>ᠤᠨᠴᠤᠭᠤᠢ</translation>
    </message>
    <message>
        <location line="+704"/>
        <location line="+2100"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+337"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-1990"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+84"/>
        <source>Save As</source>
        <translation>ᠦᠭᠡᠷ᠎ᠡ ᠭᠠᠵᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+642"/>
        <location line="+5"/>
        <location line="+355"/>
        <source>&amp;Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ(&amp;O)</translation>
    </message>
    <message>
        <location line="-357"/>
        <location line="+341"/>
        <source>&amp;Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ（&amp;S）</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>&amp;Rename</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ(&amp;R)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ(&amp;D)</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>R&amp;eload</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ(&amp;E)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Sort by &amp;Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠵᠢᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;N)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by &amp;Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;S)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort by &amp;Date</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠡᠷ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ(&amp;D)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Unsorted</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ(&amp;U)</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Sort</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show &amp;hidden files</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ(&amp;H)</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>the file</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the symlink</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete %1</source>
        <translation>ᠬᠠᠰᠤᠬᠤ%1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; ᠲᠠ%1，“%2” ᠢ᠋/ ᠵᠢᠨ ᠯᠠᠪᠳᠠᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?&lt;/qt&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Yes</source>
        <translation>ᠳᠡᠢᠮᠤ(&amp;Y)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;No</source>
        <translation>ᠪᠢᠰᠢ(&amp;N)</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>New Folder 1</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder %1</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ%1</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Find Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+108"/>
        <source>Directories</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Directory:</source>
        <translation>ᠭᠠᠷᠴᠠᠭ:</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+1110"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="-1109"/>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>ᠹᠠᠢᠯ%1
ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭ᠎ᠠ ᠦᠬᠡᠢ.
ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠬᠢᠬᠡᠳ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="-289"/>
        <source>All Files (*.*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ (*.*)</translation>
    </message>
    <message>
        <location line="+375"/>
        <source>Open </source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ </translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Select a Directory</source>
        <translation>ᠨᠢᠭᠡ ᠭᠠᠷᠴᠠᠭ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="+130"/>
        <location line="+10"/>
        <source>Could not read directory
%1</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠤᠩᠰᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ
%1</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Could not create directory
%1</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ
%1</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Could not remove file or directory
%1</source>
        <translation>ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ
%1</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Could not rename
%1
to
%2</source>
        <translation>%1
ᠢ᠋/ ᠵᠢᠨ
%2
ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Could not open
%1</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ
%1</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Could not write
%1</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="+2051"/>
        <source>Line up</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Customize...</source>
        <translation>ᠳᠤᠷ᠎ᠠ ᠎ᠪᠠᠷ ᠳᠤᠭ᠍ᠳᠠᠭᠠᠭ᠍ᠰᠠᠨ...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <location filename="../src/qt3support/network/q3networkprotocol.cpp" line="+854"/>
        <source>Operation stopped by the user</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠬᠤᠷᠢᠭᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3progressdialog.cpp" line="+224"/>
        <location line="+61"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="+189"/>
        <location line="+814"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Apply</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Defaults</source>
        <translation>ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="+5429"/>
        <source>&amp;Undo</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ(&amp;U)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Redo</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠬᠢᠬᠦ(&amp;R)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cu&amp;t</source>
        <translation>ᠡᠰᠬᠡᠬᠦ(&amp;T)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ(&amp;C)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ(&amp;P)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="+246"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore up</source>
        <translation>ᠳᠡᠬᠡᠭᠰᠢ ᠪᠡᠨ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore down</source>
        <translation>ᠳᠤᠷᠤᠭᠰᠢ ᠪᠡᠨ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Contains commands to manipulate the window</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠴᠤᠩᠬᠤᠨ ᠤᠤ ᠵᠠᠷᠯᠢᠭ ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a minimized back to normal</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠡ ᠴᠣᠩᠬᠣ ᠵᠢ ᠡᠩ ᠤ᠋ᠨ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Moves the window out of the way</source>
        <translation>ᠴᠤᠩᠬᠤ ᠵᠢᠨ ᠭᠠᠳᠠᠨ᠎ᠠ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a maximized window back to normal</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠨᠢᠭᠡ ᠴᠣᠩᠬᠣ ᠵᠢ ᠡᠩ ᠤ᠋ᠨ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Makes the window full screen</source>
        <translation>ᠴᠤᠩᠬᠤ ᠵᠢ ᠪᠦᠳᠦᠨ ᠳᠡᠯᠭᠡᠴᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Closes the window</source>
        <translation>ᠴᠤᠩᠬᠤ ᠵᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>ᠴᠣᠩᠬᠣᠨ ᠤ᠋ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠤᠯᠤᠨ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠵᠠᠰᠠᠨ ᠰᠡᠯᠪᠢᠬᠦ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠣᠨᠣᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <location filename="../src/qt3support/widgets/q3toolbar.cpp" line="+692"/>
        <source>More...</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="+386"/>
        <location line="+260"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>ᠭᠡᠷ᠎ᠡ `%1&apos; ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-260"/>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>ᠭᠡᠷ᠎ᠡ`%1&apos; ᠭᠠᠷᠴᠠᠭ ᠢ ᠵᠢᠭᠰᠠᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>ᠭᠡᠷ᠎ᠡ`%1&apos; ᠰᠢᠨ᠎ᠡ ᠭᠠᠷᠴᠠᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>ᠭᠡᠷ᠎ᠡ`%1&apos; ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>ᠭᠡᠷ᠎ᠡ`%1&apos; ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠭᠠᠷᠴᠠᠭ ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>ᠭᠡᠷ᠎ᠡ`%1&apos; ᠹᠠᠢᠯ ᠢ ᠤᠯᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>ᠭᠡᠷ᠎ᠡ`%1&apos; ᠹᠠᠢᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+243"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>ᠭᠡᠷ᠎ᠡ`%1&apos; ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠭᠠᠷᠴᠠᠭ ᠢ ᠺᠤᠫᠢᠳᠠᠬᠤ ᠡᠰᠡᠪᠡᠯ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+237"/>
        <location line="+1"/>
        <source>(unknown)</source>
        <translation>( ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="+177"/>
        <source>&amp;Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ(&amp;C)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; ᠳᠡᠭᠡᠷᠡᠬᠢ ᠠᠯᠬᠤᠮ(&amp;B)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Next &gt;</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠠᠯᠬᠤᠮ(&amp;N) &gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ(&amp;F)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ(&amp;H)</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../src/network/socket/qabstractsocket.cpp" line="+868"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+615"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+657"/>
        <location line="+26"/>
        <source>Host not found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+50"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+3"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+4"/>
        <source>Connection refused</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Connection timed out</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠴᠠᠭ ᠡᠴᠡᠵᠦ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="-547"/>
        <location line="+787"/>
        <location line="+208"/>
        <source>Operation on socket is not supported</source>
        <translation>Socket ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Socket operation timed out</source>
        <translation>ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>Socket is not connected</source>
        <translation>ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠴᠥᠷᠬᠡᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-8"/>
        <source>Network unreachable</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="+1199"/>
        <source>&amp;Step up</source>
        <translation>ᠨᠡᠮᠡᠭᠳᠡᠬᠦ(&amp;S)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Step &amp;down</source>
        <translation>ᠬᠠᠰᠤᠭᠳᠠᠬᠤ(&amp;D)</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ(&amp;S)</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+376"/>
        <source>Activate</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="+352"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>&apos;%1&apos; ᠢ᠋/ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠳ᠋ᠤ᠌ Qt %2 ᠱᠠᠭᠠᠷᠳᠠᠨ᠎ᠠ, ᠵᠥᠪᠬᠡᠨ Qt %3 ᠢ᠋/ ᠵᠢ ᠡᠷᠢᠵᠤ ᠣᠯᠤᠭᠰᠠᠨ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incompatible Qt Library Error</source>
        <translation>ᠠᠭᠤᠰᠤᠯᠴᠠᠬᠤ ᠦᠬᠡᠢ Qt ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+2"/>
        <source>Activates the program&apos;s main window</source>
        <translation>ᠡᠨᠡᠬᠦ ᠫᠷᠤᠭᠷᠡᠮ ᠤ᠋ᠨ ᠭᠤᠤᠯ ᠴᠤᠩᠬᠤ ᠵᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <location filename="../src/activeqt/container/qaxselect.ui"/>
        <source>Select ActiveX Control</source>
        <translation>ActiveX ᠡᠵᠡᠮᠳᠡᠯ ᠲᠤᠨᠤᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ(&amp;C)</translation>
    </message>
    <message>
        <location/>
        <source>COM &amp;Object:</source>
        <translation>COM ᠳ᠋ᠦᠢᠰᠢᠶᠠᠩ(&amp;O)：</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+114"/>
        <source>Uncheck</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠢ᠋ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Check</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle</source>
        <translation>ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="+1253"/>
        <source>Hu&amp;e:</source>
        <translation>ᠦᠩᠬᠡ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ(&amp;E)：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Sat:</source>
        <translation>ᠦᠩᠬᠡ ᠵᠢᠨ ᠬᠠᠨᠤᠴᠠ (&amp;S)：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Val:</source>
        <translation>ᠬᠡᠷᠡᠯᠳᠦᠴᠡ(&amp;V)：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Red:</source>
        <translation>ᠤᠯᠠᠭᠠᠨ(&amp;R)：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Green:</source>
        <translation>ᠨᠤᠭᠤᠭᠠᠨ(&amp;G)：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bl&amp;ue:</source>
        <translation>ᠬᠦᠭᠡ(&amp;U)：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A&amp;lpha channel:</source>
        <translation>Alpha ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠠᠮ(&amp;A)：</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Select Color</source>
        <translation>ᠦᠩᠬᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>&amp;Basic colors</source>
        <translation>ᠦᠨᠳᠦᠰᠦᠨ ᠦᠩᠬᠡ(&amp;B)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Custom colors</source>
        <translation>ᠳᠤᠷ᠎ᠠ ᠪᠡᠷ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠦᠩᠬᠡ(&amp;C)</translation>
    </message>
    <message>
        <source>Cursor at %1, %2
Press ESC to cancel</source>
        <translation>ᠺᠸᠰᠸ ᠨᠢ %1, %2 ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ᠌ ᠪᠤᠢ
ESC ᠢ᠋/ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>ᠳᠤᠷ᠎ᠠ ᠪᠡᠷ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠦᠩᠬᠡ ᠳ᠋ᠤ᠌ ᠨᠡᠮᠡᠬᠦ(&amp;A)</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">选择颜色</translation>
    </message>
    <message>
        <source>&amp;Pick Screen Color</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠤ᠋ ᠦᠩᠬᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠵᠤ ᠠᠪᠬᠤ(&amp;P)</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+1771"/>
        <location line="+65"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="+544"/>
        <source>False</source>
        <translation>ᠬᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True</source>
        <translation>ᠦᠨᠡᠨ</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+0"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：权限被拒绝</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：已经存在</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：不存在</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：资源耗尽了</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：未知错误 %2</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="+119"/>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: ᠳᠠᠷᠤᠪᠴᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: ᠳᠠᠷᠤᠪᠴᠢ ᠵᠣᠬᠢᠶᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation>%1: ftok ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+1276"/>
        <source>Unable to connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+303"/>
        <source>Unable to commit transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to set autocommit</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠳᠤᠰᠢᠶᠠᠬᠤ ᠪᠡᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location line="-1043"/>
        <location line="+243"/>
        <source>Unable to execute statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-206"/>
        <source>Unable to prepare statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Unable to bind variable</source>
        <translation>ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠬᠡᠮᠵᠢᠭᠳᠡᠬᠦᠨ ᠢ᠋ ᠤᠶᠠᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Unable to fetch record %1</source>
        <translation>ᠳᠡᠮᠳᠡᠭᠯᠡᠯ %1 ᠢ᠋/ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to fetch next</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠡ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to fetch first</source>
        <translation>ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠢ᠋ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="+2295"/>
        <source>AM</source>
        <translation>ᠳᠡᠢᠮᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>am</source>
        <translation>ᠳᠡᠢᠮᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PM</source>
        <translation>ᠦᠳᠡ ᠵᠢᠨ ᠬᠤᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>pm</source>
        <translation>ᠦᠳᠡ ᠵᠢᠨ ᠬᠤᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+951"/>
        <source>QDial</source>
        <translation>qDial</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SpeedoMeter</source>
        <translation>speedoMeter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SliderHandle</source>
        <translation>sliderHandle</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="+597"/>
        <source>What&apos;s This?</source>
        <translation>ᠡᠨᠡ ᠶᠠᠭᠤ ᠪᠤᠢ?</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>Done</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="+1866"/>
        <location line="+464"/>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+561"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+3"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ（&amp;S）</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ(&amp;C)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ&amp;C</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Apply</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reset</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Don&apos;t Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Discard</source>
        <translation>ᠳᠠᠶᠠᠭᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Yes</source>
        <translation>ᠳᠡᠢᠮᠤ(&amp;Y)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes to &amp;All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠵᠢᠡᠷ ᠵᠢᠨᠨ(&amp;A)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;No</source>
        <translation>ᠪᠢᠰᠢ(&amp;N)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>N&amp;o to All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠵᠢᠡᠷ ᠵᠢᠨᠨ ᠪᠢᠰᠢ(&amp;O)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abort</source>
        <translation>ᠤᠷᠬᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠲᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ignore</source>
        <translation>ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore Defaults</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Close without Saving</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠵᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>&amp;OK</source>
        <translation>OK(&amp;O)</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+453"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>ᠲᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Date Modified</source>
        <translation>ᠡᠳᠤᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠵᠢ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="+1239"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dock</source>
        <translation>ᠲᠸᠺᠰᠲ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Float</source>
        <translation>ᠬᠥᠪᠥᠮᠡᠯ</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-537"/>
        <source>More</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>ᠨᠡᠩ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="+192"/>
        <source>Debug Message:</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning:</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Fatal Error:</source>
        <translation>ᠠᠮᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ ᠪᠤᠷᠤᠭᠤ:</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>&amp;Show this message again</source>
        <translation>ᠳᠤᠰ ᠮᠡᠳᠡᠭᠡ ᠶᠢ ᠳᠠᠬᠢᠵᠤ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠦᠬᠡᠢ(&amp;S)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;OK</source>
        <translation>OK(&amp;O)</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../src/corelib/io/qfile.cpp" line="+708"/>
        <location line="+141"/>
        <source>Destination file exists</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="-108"/>
        <source>Cannot remove source file</source>
        <translation>cannot remove source file</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Cannot open %1 for input</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot open for output</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failure to write block</source>
        <translation>ᠬᠡᠰᠡᠭ ᠢ᠋ ᠪᠢᠴᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot create %1 for output</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+515"/>
        <location line="+444"/>
        <source>All Files (*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ(*)</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Directories</source>
        <translation>ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+50"/>
        <location line="+1471"/>
        <location line="+75"/>
        <source>&amp;Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ(&amp;O)</translation>
    </message>
    <message>
        <location line="-1596"/>
        <location line="+50"/>
        <source>&amp;Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ（&amp;S）</translation>
    </message>
    <message>
        <location line="-730"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location line="+1515"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>%1 ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ.
ᠲᠠ ᠲᠡᠬᠦᠨ ᠢ ᠰᠤᠯᠢᠬᠤ ᠰᠠᠨᠠᠭ᠎ᠠ ᠲᠠᠢ ᠤᠤ ?</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>ᠹᠠᠢᠯ%1 ᠢ
ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.
ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠵᠦᠪ ᠹᠠᠢᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+402"/>
        <source>My Computer</source>
        <translation>ᠮᠢᠨᠤ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <source>%1 File</source>
        <extracomment>%1 is a file name suffix, for example txt</extracomment>
        <translation>%1 ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 KiB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-1504"/>
        <source>&amp;Rename</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ(&amp;R)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ(&amp;D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show &amp;hidden files</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ(&amp;H)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Back</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Parent Directory</source>
        <translation>ᠳᠡᠭᠡᠳᠦ ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>List View</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Detail View</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Files of type:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+6"/>
        <location line="+648"/>
        <source>Directory:</source>
        <translation>ᠭᠠᠷᠴᠠᠭ:</translation>
    </message>
    <message>
        <location line="+794"/>
        <location line="+862"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>ᠭᠠᠷᠴᠠᠭ%1 ᠢ
ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.
ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠵᠦᠪ ᠭᠠᠷᠴᠠᠭ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location line="-218"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&apos;%1&apos; ᠨᠢ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢ ᠪᠢᠴᠢᠨ᠎ᠡ.
ᠲᠠ ᠮᠦᠨ ᠬᠠᠰᠤᠬᠤ ᠰᠠᠨᠠᠭ᠎ᠠ ᠲᠠᠢ ᠤᠤ?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>ᠲᠠ ᠨᠡᠬᠡᠷᠡᠨ&apos;%1&apos; ᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>ᠲᠠ ᠨᠡᠬᠡᠷᠡᠨ&apos;%1&apos; ᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not delete directory.</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ ᠬᠠᠰᠤᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+407"/>
        <source>Recent Places</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤᠨ ᠤᠢᠷ᠎ᠠ ᠭᠠᠵᠠᠷ</translation>
    </message>
    <message>
        <location line="-2550"/>
        <source>Save As</source>
        <translation>ᠦᠭᠡᠷ᠎ᠡ ᠭᠠᠵᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="+411"/>
        <source>Drive</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>File</source>
        <translation>ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-4"/>
        <source>Find Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Show </source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Forward</source>
        <translation>ᠠᠬᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+1970"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="-1963"/>
        <source>&amp;New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ(&amp;N)</translation>
    </message>
    <message>
        <location line="+656"/>
        <location line="+38"/>
        <source>&amp;Choose</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ(&amp;C)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qsidebar.cpp" line="+418"/>
        <source>Remove</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-687"/>
        <location line="+652"/>
        <source>File &amp;name:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ(&amp;N)：</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Look in:</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ:</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Create New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_win.cpp" line="+160"/>
        <source>All Files (*.*)</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ (*.*)</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="+744"/>
        <source>%1 TB</source>
        <translation>%1 TB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 bytes</source>
        <translation>%1 ᠪᠸᠲ᠋ᠠ</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Invalid filename</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠹᠠᠢᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt; ᠨᠡᠷᠡᠢᠳᠦᠯ &quot;%1&quot; ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ᠂ &lt;/b&gt;&lt;p&gt; ᠲᠠ ᠦᠭᠡᠷ᠎ᠡ ᠨᠢᠭᠡ ᠴᠦᠬᠡᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠬᠤ ᠪᠤᠶᠤ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>ᠲᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date Modified</source>
        <translation>ᠡᠳᠤᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠵᠢ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="+234"/>
        <source>My Computer</source>
        <translation>ᠮᠢᠨᠤ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Computer</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>%1 KiB</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <location filename="../src/gui/text/qfontdatabase.cpp" line="+90"/>
        <location line="+1176"/>
        <source>Normal</source>
        <translation>ᠡᠨᠭ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location line="-1173"/>
        <location line="+12"/>
        <location line="+1149"/>
        <source>Bold</source>
        <translation>ᠪᠦᠳᠦᠭᠦᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location line="-1158"/>
        <location line="+1160"/>
        <source>Demi Bold</source>
        <translation>ᠬᠠᠭᠠᠰ ᠪᠦᠳᠦᠬᠦᠨ ᠲᠢᠭ</translation>
    </message>
    <message>
        <location line="-1157"/>
        <location line="+18"/>
        <location line="+1135"/>
        <source>Black</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠲᠢᠭ</translation>
    </message>
    <message>
        <location line="-1145"/>
        <source>Demi</source>
        <translation>ᠬᠠᠭᠠᠰ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1145"/>
        <source>Light</source>
        <translation>ᠬᠦᠩᠭᠡᠨ ᠲᠢᠭ</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1007"/>
        <source>Italic</source>
        <translation>ᠢᠲ᠋ᠠᠯᠢ ᠲᠢᠭ</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1006"/>
        <source>Oblique</source>
        <translation>ᠵᠢᠰᠢᠬᠦᠦ ᠲᠢᠭ</translation>
    </message>
    <message>
        <location line="+705"/>
        <source>Any</source>
        <translation>ᠳᠤᠷ᠎ᠠ ᠵᠢᠨ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Latin</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Greek</source>
        <translation>ᠭᠷᠧᠭ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cyrillic</source>
        <translation>ᠺᠢᠷᠢᠯ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Armenian</source>
        <translation>ᠠᠷᠮᠧᠨᠢᠶ᠎ᠠ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hebrew</source>
        <translation>ᠢᠪᠷᠠᠶ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Arabic</source>
        <translation>ᠠᠷᠠᠪ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Syriac</source>
        <translation>ᠰᠢᠷᠢᠶ᠎ᠠ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thaana</source>
        <translation>ᠮᠠᠯᠳᠠᠶᠢᠹ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Devanagari</source>
        <translation>ᠰᠠᠨᠰᠺᠷᠢᠲ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bengali</source>
        <translation>ᠪᠧᠩᠭᠠᠯ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gurmukhi</source>
        <translation>ᠪᠦᠨᠵᠸᠫᠸᠢᠨ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gujarati</source>
        <translation>ᠡᠷᠲᠡᠨ ᠦ ᠬᠢᠯᠠᠲ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Oriya</source>
        <translation>ᠣᠷᠢᠶᠠᠨ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tamil</source>
        <translation>ᠳᠠᠮᠢᠯ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Telugu</source>
        <translation>ᠲᠠᠢᠢᠯᠦ᠋ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kannada</source>
        <translation>ᠡᠸᠨᠠᠷᠳ᠋ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Malayalam</source>
        <translation>ᠮᠠᠯᠠᠶᠠᠷᠠᠮ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sinhala</source>
        <translation>ᠪᠧᠷᠢᠶᠠ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thai</source>
        <translation>ᠲᠠᠢᠯᠠᠨᠳ᠋ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lao</source>
        <translation>ᠯᠠᠤᠰ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tibetan</source>
        <translation>ᠳᠦᠪᠡᠳ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Myanmar</source>
        <translation>ᠪᠢᠷᠮᠠ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Georgian</source>
        <translation>ᠭᠷᠦ᠋ᠽᠢᠶᠠ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Khmer</source>
        <translation>ᠬᠦᠮᠸᠷᠸᠢᠨ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Simplified Chinese</source>
        <translation>ᠳᠦᠬᠦᠮ ᠬᠢᠲᠠᠳ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Traditional Chinese</source>
        <translation>ᠡᠷᠳᠡᠨ ᠤ᠋ ᠬᠢᠳᠠᠳ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Japanese</source>
        <translation>ᠶᠠᠫᠤᠨ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Korean</source>
        <translation>ᠰᠤᠯᠤᠩᠭᠤᠰ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vietnamese</source>
        <translation>ᠸᠢᠶᠸᠲ᠋ᠨᠠᠮ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol</source>
        <translation>ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ogham</source>
        <translation>ᠤᠺᠠᠸᠢᠨ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Runic</source>
        <translation>ᠡᠷᠲᠡᠨ ᠦ ᠤᠮᠠᠷᠠᠳᠤ ᠡᠧᠦ᠋ᠷᠦᠫᠠ ᠬᠡᠯᠡ</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="+772"/>
        <source>&amp;Font</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠲᠢᠭ(&amp;F)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font st&amp;yle</source>
        <translation>ᠲᠢᠭ ᠦᠨ ᠬᠡᠪ ᠨᠠᠮᠪᠠ(&amp;Y)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ(&amp;S)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Effects</source>
        <translation>ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stri&amp;keout</source>
        <translation>ᠬᠠᠰᠤᠬᠤ ᠱᠤᠭᠤᠮ(&amp;K)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Underline</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ ᠵᠢᠷᠤᠭᠠᠰᠤ(&amp;U)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sample</source>
        <translation>ᠪᠤᠳᠠᠳᠤ ᠵᠢᠰᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wr&amp;iting System</source>
        <translation>ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠰᠢᠰᠲ᠋ᠧᠮ(&amp;I)</translation>
    </message>
    <message>
        <location line="-604"/>
        <location line="+247"/>
        <source>Select Font</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠲᠢᠭ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../src/network/access/qftp.cpp" line="+826"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+683"/>
        <source>Not connected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+65"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+65"/>
        <source>Host %1 not found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ %1 ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+4"/>
        <source>Connection refused to host %1</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ %1 ᠳᠤ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection timed out to host %1</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ %1 ᠶᠢᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠴᠠᠭ ᠠᠴᠠ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+104"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+102"/>
        <location line="+1451"/>
        <source>Connected to host %1</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ %1 ᠳᠤ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+219"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-1290"/>
        <source>Connection refused for data connection</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠡᠴᠡ ᠪᠤᠯᠵᠤ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+29"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+195"/>
        <location line="+728"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+889"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+77"/>
        <source>Connecting to host failed:
%1</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠳᠤ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Login failed:
%1</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Listing directory failed:
%1</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ ᠵᠢᠭᠰᠠᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Changing directory failed:
%1</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Downloading file failed:
%1</source>
        <translation>ᠹᠠᠢᠯ ᠢ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Uploading file failed:
%1</source>
        <translation>ᠹᠠᠢᠯ ᠢ ᠳᠠᠮᠵᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing file failed:
%1</source>
        <translation>ᠹᠠᠢᠯ ᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Creating directory failed:
%1</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing directory failed:
%1</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+25"/>
        <location line="+250"/>
        <source>Connection closed</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-11"/>
        <source>Host %1 found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ%1 ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to %1 closed</source>
        <translation>%1 ᠳᠤ ᠬᠦᠷᠬᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connected to host</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠳᠤ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2248"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>QT_LAYOUT_DIRECTION</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_p.h" line="+183"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_unix.cpp" line="+178"/>
        <location line="+9"/>
        <location line="+64"/>
        <location line="+31"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="+180"/>
        <location line="+9"/>
        <location line="+40"/>
        <location line="+27"/>
        <source>Host not found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-44"/>
        <location line="+39"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-34"/>
        <location line="+29"/>
        <source>Unknown address type</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠬᠠᠶ᠋ᠢᠭ ᠤᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-19"/>
        <location line="+27"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="+1574"/>
        <location line="+820"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+1160"/>
        <location line="+567"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="-568"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="-370"/>
        <source>Request aborted</source>
        <translation>ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠲᠠᠶᠠᠭᠳᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+579"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+381"/>
        <source>No server set to connect to</source>
        <translation>ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+164"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+56"/>
        <source>Wrong content length</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠠᠭᠤᠯᠭ᠎ᠠ ᠶᠢᠨ ᠤᠷᠳᠤ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+4"/>
        <source>Server closed connection unexpectedly</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠡᠷ ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Unknown authentication method</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠪᠡᠶ᠎ᠡ ᠵᠢᠨ ᠭᠠᠷᠤᠯ ᠢ᠋ ᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+183"/>
        <source>Error writing response to device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳ᠋ᠤ᠌ ᠬᠠᠷᠢᠭᠤ ᠵᠢ ᠪᠢᠴᠢᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+876"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+38"/>
        <source>Connection refused</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-304"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Host %1 not found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ %1 ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+20"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+10"/>
        <location line="+19"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>HTTP request failed</source>
        <translation>HTTP ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+69"/>
        <source>Invalid HTTP response header</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ HTTP ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠠᠭᠠᠯ ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+48"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+40"/>
        <location line="+47"/>
        <source>Invalid HTTP chunked body</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ HTTP ᠤ᠋ᠨ/ ᠵᠢᠨ ᠲᠤᠮᠤ ᠪᠡᠶᠡᠳᠦ</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="+294"/>
        <source>Host %1 found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ%1 ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host %1</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ %1 ᠳᠤ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection to %1 closed</source>
        <translation>%1 ᠳᠤ ᠬᠦᠷᠬᠦ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Host found</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠳᠤ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-22"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Connection closed</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-135"/>
        <source>Proxy authentication required</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Authentication required</source>
        <translation>ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Connection refused (or timed out)</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠲᠡᠪᠴᠢᠭᠳᠡᠪᠡ(ᠡᠰᠡᠪᠡᠯ ᠴᠠᠭ ᠠᠴᠠ ᠬᠡᠳᠦᠷᠡᠪᠡ)</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+6"/>
        <source>Proxy requires authentication</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host requires authentication</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data corrupted</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown protocol specified</source>
        <translation>ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠬᠡᠯᠡᠯᠴᠡᠬᠡᠷ ᠨᠢ ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SSL handshake failed</source>
        <translation>SSL ᠭᠠᠷ ᠠᠳᠬᠤᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-2263"/>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation>HTTPS ᠴᠦᠷᠬᠡᠯᠡᠭᠡ SSL ᠢ᠋/ ᠵᠢ ᠱᠠᠭᠠᠷᠳᠠᠨ᠎ᠠ᠂ ᠬᠡᠪᠡᠴᠤ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠨ ᠤᠷᠴᠢᠭᠤᠯᠵᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="-89"/>
        <source>Did not receive HTTP response from proxy</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢᠨ HTTP ᠬᠠᠷᠢᠭᠤ ᠲᠤᠰᠬᠠᠯ ᠢ᠋ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error parsing authentication request from proxy</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠤ᠋ᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯᠳᠠ ᠵᠢ ᠵᠠᠳᠠᠯᠬᠤ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Authentication required</source>
        <translation>ᠬᠡᠷᠡᠴᠢᠯᠡᠯ ᠢ᠋ ᠱᠠᠭᠠᠷᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Proxy denied connection</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠵᠢ ᠳᠡᠪᠴᠢᠪᠡ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error communicating with HTTP proxy</source>
        <translation>HTTP ᠤᠷᠤᠯᠠᠭᠴᠢ ᠲᠠᠢ ᠬᠤᠯᠪᠤᠭ᠎ᠠ ᠪᠠᠷᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Proxy server not found</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection refused</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy server connection timed out</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection closed prematurely</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠬᠡᠳᠦ ᠡᠷᠳᠡ ᠬᠠᠭᠠᠭᠠᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="+1428"/>
        <source>Error opening database</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠶᠢᠨ ᠬᠦᠮᠦᠷᠭᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Could not start transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to commit transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to rollback transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location line="-1097"/>
        <source>Unable to create BLOB</source>
        <translation>BLOB ᠶᠢ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to write BLOB</source>
        <translation>BLOB ᠶᠢ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to open BLOB</source>
        <translation>BLOB ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to read BLOB</source>
        <translation>BLOB ᠶᠢ ᠤᠩᠰᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+189"/>
        <source>Could not find array</source>
        <translation>ᠲᠤᠭᠠᠨ ᠪᠦᠯᠦᠭ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Could not get array data</source>
        <translation>ᠲᠤᠭᠠᠨ ᠪᠦᠯᠦᠭ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Could not get query info</source>
        <translation>ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠬᠤ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not start transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Could not allocate statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not prepare statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+7"/>
        <source>Could not describe input statement</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢ ᠳᠦᠷᠰᠦᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not describe statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠳᠦᠷᠰᠦᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unable to close statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unable to execute query</source>
        <translation>ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Could not fetch next item</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠦᠷᠦᠯ ᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Could not get statement info</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠶᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="+1869"/>
        <source>Permission denied</source>
        <translation>ᠡᠷᠬᠡ ᠲᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Too many open files</source>
        <translation>ᠬᠡᠳᠦ ᠤᠯᠠᠨ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No such file or directory</source>
        <translation>ᠲᠤᠰ ᠹᠠᠢᠯ ᠪᠤᠶᠤ ᠭᠠᠷᠴᠠᠭ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No space left on device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠤᠷᠤᠨ ᠵᠠᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qiodevice.cpp" line="+1536"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="+242"/>
        <source>XIM</source>
        <translation>XIM</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>XIM input method</source>
        <translation>XIM ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Windows input method</source>
        <translation>Windows ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mac OS X input method</source>
        <translation>Mac OS ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qinputdialog.cpp" line="+223"/>
        <source>Enter a value:</source>
        <translation>ᠨᠢᠭᠡ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠤᠷᠤᠭᠤᠯᠬᠤ:</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys： 不能载入%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys：不能卸载%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: 符号“%1”在%2（%3）没有被定义</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="+378"/>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation>ᠳᠤᠰᠬᠠᠬᠤ ᠦᠬᠡᠢ %1&apos;: %2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>&apos;%1&apos; ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠦᠯᠦ ᠪᠤᠯᠬᠤ ᠲᠤᠰᠬᠠᠯ &apos;%1&apos;: %2</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ &apos;%1&apos; ᠨᠢ ᠠᠭᠤᠰᠤᠯᠴᠠᠬᠤ ᠦᠬᠡᠢ Qt ᠬᠦᠮᠦᠷᠬᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡ . (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation>ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠣᠨᠣᠭ &apos;%1&apos; ᠨᠢ ᠠᠭᠤᠰᠤᠯᠴᠠᠬᠤ ᠥᠬᠡᠢ Qt ᠬᠦᠮᠦᠷᠬᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡ᠂ ᠬᠦᠰᠡᠨ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠤᠢ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠳᠠᠷᠤᠪᠴᠢ ᠨᠢ &quot;%2&quot;, ᠬᠡᠪᠡᠴᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠨᠢ ᠬᠠᠷᠢᠨ &quot;%3&quot;</translation>
    </message>
    <message>
        <location line="+340"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="-377"/>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+280"/>
        <source>The shared library was not found.</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠬᠤᠪᠢᠶᠠᠯᠴᠠᠬᠤ ᠬᠦᠮᠦᠷᠬᠡ ᠵᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>ᠹᠠᠢᠯ &apos;%1&apos; ᠨᠢ ᠬᠦᠴᠦᠨ ᠲᠠᠢ Qt ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ &apos;%1&apos; ᠠᠭᠤᠰᠤᠯᠴᠠᠬᠤ ᠦᠬᠡᠢ Qt ᠬᠦᠮᠦᠷᠭᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡ. ( ᠬᠦᠮᠦᠷᠭᠡ ᠵᠢᠨ ᠳᠤᠷᠰᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠡᠪᠯᠡᠯ ᠬᠢᠬᠡᠳ ᠨᠡᠢᠳᠡᠯᠡᠬᠦ ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠬᠤᠯᠢᠯᠳᠤᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.)</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_unix.cpp" line="+209"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+99"/>
        <source>Cannot load library %1: %2</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠬᠦᠮᠦᠷᠬᠡ %1: %2</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+26"/>
        <source>Cannot unload library %1: %2</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ ᠬᠦᠮᠦᠷᠬᠡ %1: %2</translation>
    </message>
    <message>
        <location line="+31"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+15"/>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation>%2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠢ ᠳᠡᠮᠳᠡᠭ &quot;%1&quot; in ᠢ᠋/ ᠵᠢ ᠵᠠᠳᠠᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ: %3</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="+2680"/>
        <source>&amp;Undo</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ(&amp;U)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ(&amp;R)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>ᠡᠰᠬᠡᠬᠦ(&amp;T)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ(&amp;C)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ(&amp;P)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <location filename="../src/network/socket/qlocalserver.cpp" line="+226"/>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="+231"/>
        <source>%1: Name error</source>
        <translation>%1: ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="-8"/>
        <source>%1: Permission denied</source>
        <translation>%1: ᠡᠷᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: Address in use</source>
        <translation>%1: ᠬᠠᠶ᠋ᠢᠭ ᠶᠠᠭ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠵᠦ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../src/network/socket/qlocalserver_win.cpp" line="+158"/>
        <source>%1: Unknown error %2</source>
        <translation>%1: ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ%2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <location filename="../src/network/socket/qlocalsocket_tcp.cpp" line="+132"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+134"/>
        <source>%1: Connection refused</source>
        <translation>%1: ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠲᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Remote closed</source>
        <translation>%1: ᠠᠯᠤᠰ ᠡᠵᠡᠮᠳᠡᠯ ᠢ᠋ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+80"/>
        <location line="+43"/>
        <source>%1: Invalid name</source>
        <translation>%1: ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket access error</source>
        <translation>%1: ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠠᠢᠯᠴᠢᠯᠠᠯ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket resource error</source>
        <translation>%1: ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket operation timed out</source>
        <translation>%1: ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Datagram too large</source>
        <translation>%1: ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠶᠢᠨ ᠮᠡᠳᠡᠬᠦᠯᠦᠯᠳᠡ ᠬᠡᠳᠦ ᠶᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="-48"/>
        <source>%1: Connection error</source>
        <translation>%1: ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: The socket operation is not supported</source>
        <translation>%1: ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: Unknown error</source>
        <translation>%1: ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+4"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+10"/>
        <source>%1: Unknown error %2</source>
        <translation>%1: ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ%2</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="+1231"/>
        <source>Unable to open database &apos;</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠶᠢᠨ ᠬᠦᠮᠦᠷᠬᠡ ᠶᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Unable to begin transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location line="-922"/>
        <source>Unable to fetch data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠶᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Unable to execute query</source>
        <translation>ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to store result</source>
        <translation>ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+190"/>
        <location line="+8"/>
        <source>Unable to prepare statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unable to reset statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢ ᠳᠠᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unable to bind value</source>
        <translation>ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠢ᠋ ᠤᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to execute statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+21"/>
        <source>Unable to bind outvalues</source>
        <translation>ᠭᠠᠳᠠᠷ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠢ᠋ ᠤᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Unable to store statement results</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>Unable to execute next query</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢᠭᠡ ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to store next result</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢᠭᠡ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <location filename="../src/gui/widgets/qmdiarea.cpp" line="+290"/>
        <source>(Untitled)</source>
        <translation>( ᠨᠡᠷᠡᠢᠳᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ)</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="+280"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Restore Down</source>
        <translation>ᠳᠤᠷᠤᠭᠰᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+707"/>
        <source>&amp;Restore</source>
        <translation>ᠰᠡᠷᠭᠦᠭᠡᠬᠦ(&amp;R)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Move</source>
        <translation>ᠱᠢᠯᠵᠢᠬᠦᠯᠬᠦ᠌(&amp;M)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ(&amp;S)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mi&amp;nimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ(&amp;N)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ(&amp;X)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on &amp;Top</source>
        <translation>ᠶᠡᠷᠦᠳᠡᠬᠡᠨ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠡᠮᠦᠨ᠎ᠡ (&amp;T)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ(&amp;C)</translation>
    </message>
    <message>
        <location line="-787"/>
        <source>- [%1]</source>
        <translation>- [%1]</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unshade</source>
        <translation>ᠬᠠᠯᠬᠠᠯᠠᠬᠤ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shade</source>
        <translation>ᠬᠠᠯᠬᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Restore</source>
        <translation>ᠰᠡᠷᠭᠦᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Menu</source>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="+157"/>
        <location line="+225"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="-224"/>
        <location line="+225"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location line="-223"/>
        <location line="+225"/>
        <location line="+51"/>
        <source>Execute</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠭᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="obsolete">配置</translation>
    </message>
    <message>
        <source>Preference</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">选项</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="obsolete">设置</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="obsolete">安装</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="obsolete">关于%1</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">关于Qt</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="obsolete">退出%1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="-1111"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="-853"/>
        <location line="+852"/>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="-52"/>
        <location line="+8"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location line="+509"/>
        <source>About Qt</source>
        <translation>Qt ᠶᠢᠨ ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用的是Qt %1版。&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="-1605"/>
        <source>Show Details...</source>
        <translation>ᠨᠠᠷᠢᠨ ᠪᠠᠢᠳᠠᠯ ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Details...</source>
        <translation>ᠨᠠᠷᠢᠨ ᠪᠠᠢᠳᠠᠯ ᠢ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ...</translation>
    </message>
    <message>
        <location line="+1570"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Qt&lt;/h3&gt; &lt;p&gt; ᠡᠨᠡ ᠫᠷᠦᠭᠷᠠᠮ ᠳ᠋ᠤ᠌ Qt ᠬᠡᠪᠯᠡᠯ ᠨᠢ %1 ᠢ᠋/ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃&lt;/p&gt;&lt;p&gt;Qt ᠪᠣᠯ ᠲᠠᠪᠴᠠᠩ ᠠᠯᠤᠰᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠡᠭᠡᠭᠡᠯᠲᠡ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ C+ ᠪᠠᠭᠠᠵᠢ ᠪᠣᠯᠤᠨ᠎ᠠ᠃&lt;/p&gt;&lt;p&gt;Qt ᠨᠢ MS&amp;nbsp;Widoos᠂ ᠮᠧᠺ &amp;nbsp;OS&amp;nbsp; X᠂ Linuf ᠪᠣᠯᠣᠨ ᠠᠯᠢᠪᠠ ᠭᠣᠣᠯ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ ᠤ᠋ Unix ᠬᠤᠪᠢᠷᠠᠯᠲᠠ ᠵᠢᠨ ᠳᠠᠩ ᠭᠠᠭᠴᠠ ᠢᠷᠡᠯᠲᠡ ᠵᠢᠨ ᠰᠢᠯᠵᠢᠭᠦᠯᠵᠦ ᠪᠣᠯᠬᠤ ᠴᠢᠨᠠᠷ ᠢ᠋ ᠬᠠᠩᠭᠠᠨ᠎ᠠ᠃ Qt ᠮᠠᠶᠢᠭ ᠤ᠋ᠨ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ᠂ ᠵᠢᠱᠢᠶᠡᠯᠡᠪᠡᠯ Qt for Embd Lo Qt for ᠸᠢᠨᠳ᠋ᠧᠰ CE ᠃&lt;/p&gt;&lt;p&gt;Qt ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠭᠤᠷᠪᠠᠨ ᠵᠦᠢᠯ ᠤ᠋ᠨ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠯᠲᠠ ᠲᠠᠢ᠂ ᠵᠣᠷᠢᠯᠭ᠎ᠠ ᠨᠢ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠴᠡᠭᠡ ᠵᠢ ᠬᠠᠩᠭᠠᠬᠤ ᠵᠢᠨ ᠲᠥᠯᠥᠭᠡ ᠶᠤᠮ ᠃&lt;/p&gt; ᠮᠠᠨ ᠤ᠋ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ ᠤ᠋ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠤ᠋ᠨ ᠭᠡᠷ᠎ᠡ ᠵᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠡᠷᠬᠡ ᠣᠯᠭᠣᠭᠰᠠᠨ Qt ᠨᠢ ᠲᠠ ᠭᠤᠷᠪᠠᠳᠠᠬᠢ ᠡᠲᠡᠭᠡᠳ ᠲᠡᠶ ᠶᠠᠮᠠᠷᠪᠠ ᠢᠷᠡᠯᠲᠡ ᠵᠢᠨ ᠺᠣᠳ᠋ ᠢ᠋ ᠬᠠᠮᠲᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠪᠤᠶᠤ GNULPL ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ2.1 ᠪᠤᠶᠤ GNU GPL ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ 3.0 ᠵᠤᠷᠪᠤᠰ ᠤ᠋ᠨ ᠲᠤᠰᠬᠠᠶ / ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ ᠤ᠋ ᠰᠤᠹᠲ ᠤ᠋ᠨ ᠨᠡᠭᠡᠭᠡᠯᠲᠡ ᠵᠢ ᠵᠢᠷᠤᠮᠯᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠃&lt;/p&gt;GNU LGPL 2.1 ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠪᠡᠯ᠂ &lt;p&gt;Qt ᠨᠢ Q ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠫᠷᠦᠭᠷᠠᠮ ( ᠲᠤᠰᠬᠠᠶ ᠪᠤᠶᠤ ᠡᠬᠢ ᠰᠤᠷᠪᠤᠯᠵᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ) ᠤ᠋ᠨ ᠨᠡᠭᠡᠭᠡᠯᠲᠡ ᠳ᠋ᠤ᠌ ᠲᠣᠬᠢᠷᠠᠨ᠎ᠠ᠂ ᠤᠷᠢᠲᠠᠯ ᠨᠦᠭᠦᠴᠡᠯ ᠨᠢ ᠲᠠ GNU LGPL 2.1 ᠬᠡᠪᠯᠡᠯ ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠵ ᠳᠡᠢᠯᠬᠦ ᠵᠤᠷᠪᠤᠰ ᠪᠠᠳᠠᠭ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃&lt;/p&gt;&lt;p&gt;GNU ᠲᠤᠩ ᠣᠯᠠᠨ ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡ ᠤ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ ᠤ᠋ᠨ 3.0 ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠢ᠋ ᠦᠨᠳᠦᠰᠦᠯᠡᠪᠡᠯ Qt ᠨᠢ Qt ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠫᠷᠦᠭᠷᠠᠮ ᠤ᠋ᠨ ᠨᠡᠭᠡᠭᠡᠯᠲᠡ ᠳ᠋ᠤ᠌ ᠲᠣᠬᠢᠷᠠᠨ᠎ᠠ᠂ ᠬᠡᠷᠪᠡ ᠲᠠ ᠡᠢᠮᠦᠷᠬᠦ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠫᠷᠦᠭᠷᠠᠮ ᠪᠤᠯ GNU GPP ᠢ᠋/ ᠵᠢ ᠬᠦᠯᠢᠶᠡᠬᠦ ᠵᠢ ᠬᠦᠰᠡᠵᠦ ᠪᠠᠢᠨ᠎ᠠ᠃ L3.0 ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠤ᠋ᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠢ᠋ ᠤᠶᠠᠯᠳᠤᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠤᠶᠤ ᠲᠠ GNU GPL 3.0 ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠵᠦᠢᠯ ᠵᠤᠷᠪᠤᠰ ᠢ᠋ ᠵᠢᠷᠤᠮᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃&lt;/p&gt;&lt;p&gt;&lt;a href=《http://qt.nokia.com/products/licensing》 ᠢ᠋/ ᠵᠢ &gt;qt.nokia.com/products/licensing&lt;/a&gt; Qt ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠤ᠋ᠨ ᠲᠣᠪᠴᠢᠯᠠᠯ ᠢ᠋ ᠤᠢᠯᠠᠭᠠᠭᠠᠷᠠᠢ᠃&lt;/p&gt;&lt;p&gt; ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠵᠢᠨ ᠥᠮᠴᠢᠯᠡᠯ (C) 2012 ᠣᠨ ᠤ᠋ ᠨᠣᠺᠢᠶᠠ ᠺᠣᠮᠫᠠᠨᠢ ᠪᠣᠯᠤᠨ/ ᠪᠤᠶᠤ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠺᠣᠮᠫᠠᠨᠢ᠃&lt;/p&gt;&lt;p&gt;Qt ᠪᠣᠯ ᠨᠣᠺᠢᠶᠠ ᠵᠢᠨ ᠦᠢᠯᠡᠳᠬᠦᠨ᠃ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠵᠢ &lt;a href=《http://qt.nokia.com/》 &gt;qt.nokia.com ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠵᠤ ᠦᠵᠡᠬᠡᠷᠡᠢ.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokiae.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt也有用于嵌入式Linux和Windows CE的版本。&lt;/p&gt;&lt;p&gt;Qt是Nokia的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用了Qt %1开源版本。&lt;/p&gt;&lt;p&gt;Qt开源版本只用于开源应用程序的开发。如果要开发私有（闭源）软件，你需要一个商业的Qt协议。&lt;/p&gt;&lt;p&gt;有关Qt协议的概览，请参考&lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt Embedded.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt对于嵌入式平台也是可用的，在嵌入式平台上它被称为Qt Embedded。&lt;/p&gt;&lt;p&gt;Qt是Trolltech的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontext.cpp" line="+88"/>
        <source>Select IM</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠶᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="+95"/>
        <source>Multiple input method switcher</source>
        <translation>ᠤᠯᠠᠨ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠰᠤᠯᠢᠯᠴᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation>ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠴᠣᠩᠬᠣᠨ ᠲᠣᠨᠣᠭ ᠤ᠋ᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠤᠷᠠᠬᠢ ᠲᠣᠪᠶᠣᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢᠨ ᠰᠤᠯᠢᠭᠤᠷ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qnativesocketengine.cpp" line="+206"/>
        <source>The remote host closed the connection</source>
        <translation>ᠠᠯᠤᠰ ᠡᠵᠡᠮᠰᠢᠯ ᠤ᠋ᠨ ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠡᠨᠡᠬᠦ ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network operation timed out</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠴᠠᠭ ᠠᠴᠠ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Out of resources</source>
        <translation>ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠪᠠᠷᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unsupported socket operation</source>
        <translation>ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Protocol type not supported</source>
        <translation>ᠭᠡᠷ᠎ᠡ ᠶᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid socket descriptor</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠳᠦᠷᠰᠦᠯᠡᠯ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Network unreachable</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Permission denied</source>
        <translation>ᠡᠷᠬᠡ ᠲᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection timed out</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection refused</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The bound address is already in use</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠬᠠᠶᠢᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is not available</source>
        <translation>ᠲᠤᠰ ᠬᠠᠶ᠋ᠢᠭ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is protected</source>
        <translation>ᠲᠤᠰ ᠬᠠᠶᠢᠭ ᠬᠠᠮᠠᠭᠠᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to send a message</source>
        <translation>ᠨᠢᠭᠡ ᠮᠡᠳᠡᠬᠡ ᠵᠠᠩᠬᠢ ᠢᠯᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to receive a message</source>
        <translation>ᠨᠢᠭᠡ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠠᠪᠴᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to write</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network error</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Another socket is already listening on the same port</source>
        <translation>ᠥᠭᠡᠷ᠎ᠡ ᠨᠢᠭᠡ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡ ᠦᠵᠦᠬᠦᠷ ᠢ᠋ ᠬᠢᠨᠠᠨ ᠰᠣᠨᠣᠰᠴᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation>ᠪᠦᠭᠯᠡᠷᠡᠬᠦ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to initialize broadcast socket</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>IPv6 ᠳᠡᠮᠵᠢᠬᠦ ᠥᠬᠡᠢ ᠳᠠᠪᠴᠠᠩ ᠳᠡᠭᠡᠷ᠎ᠡ IPv6 ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠡᠷ ᠤᠷᠤᠯᠳᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Host unreachable</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠳᠤ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Datagram was too large to send</source>
        <translation>ᠬᠡᠳᠦ ᠲᠤᠮᠤ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠮᠡᠳᠡᠬᠦᠯᠦᠯᠳᠡ ᠵᠢ ᠢᠯᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Operation on non-socket</source>
        <translation>ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠪᠤᠰᠤ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠳ᠋ᠤ᠌</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>The proxy type is invalid for this operation</source>
        <translation>ᠡᠨᠡᠬᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠤᠷᠤᠯᠠᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesscachebackend.cpp" line="+65"/>
        <source>Error opening %1</source>
        <translation>%1 ᠶᠢ ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳᠦ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessfilebackend.cpp" line="+99"/>
        <source>Request for opening non-local file %1</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠪᠤᠰᠤ %1 ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error opening %1: %2</source>
        <translation>%1 ᠶᠢ ᠢ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ: %2</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Write error writing to %1: %2</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ: %2</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot open %1: Path is a directory</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ: ᠵᠠᠮ ᠮᠦᠷ ᠨᠢ ᠨᠢᠭᠡᠨ ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Read error reading from %1: %2</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠤᠩᠰᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ: %2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessftpbackend.cpp" line="+165"/>
        <source>No suitable proxy found</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠮᠵᠢᠳᠠᠢ ᠤᠷᠤᠯᠠᠭᠴᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot open %1: is a directory</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠤᠩᠰᠢᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ: ᠨᠢᠭᠡᠨ ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Logging in to %1 failed: authentication required</source>
        <translation>%1 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ: ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Error while downloading %1: %2</source>
        <translation>%1 ᠶᠢ ᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠦᠶ᠎ᠡ ᠶᠢᠨ ᠠᠯᠳᠠᠭ᠎ᠠ: %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error while uploading %1: %2</source>
        <translation>%1 ᠶᠢ ᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠦᠶ᠎ᠡ ᠶᠢᠨ ᠠᠯᠳᠠᠭ᠎ᠠ: %2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessHttpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesshttpbackend.cpp" line="+597"/>
        <source>No suitable proxy found</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠮᠵᠢᠳᠠᠢ ᠤᠷᠤᠯᠠᠭᠴᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <location line="+128"/>
        <source>Error downloading %1 - server replied: %2</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ - ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ: %2</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkreplyimpl.cpp" line="+68"/>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation>ᠬᠡᠯᠡᠯᠴᠡᠬᠡᠷ &quot;%1&quot; ᠨᠢ ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <location line="+432"/>
        <location line="+22"/>
        <source>Operation canceled</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠨᠢᠬᠡᠨᠳᠡ ᠦᠬᠡᠢᠰᠭᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="+2069"/>
        <source>Unable to logon</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-144"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>Unable to begin transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to rollback transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location line="-963"/>
        <location line="+161"/>
        <location line="+15"/>
        <source>Unable to bind column for batch execute</source>
        <translation>ᠡᠨᠳᠡ ᠬᠦᠢᠴᠡᠳᠬᠡᠭᠳᠡᠵᠤ ᠪᠤᠢ ᠮᠦᠷ ᠢ᠋ ᠤᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to execute batch statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>Unable to goto next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠨᠢᠭᠡ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Unable to alloc statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to prepare statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unable to bind value</source>
        <translation>ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠢ᠋ ᠤᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">不能执行选择语句</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to execute statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="+1785"/>
        <source>Unable to connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ - ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠫᠷᠤᠭᠷᠡᠮ ᠪᠦᠬᠦᠢᠯᠡ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢᠨ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>Unable to disable autocommit</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠳᠤᠰᠢᠶᠠᠬᠤ ᠵᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to enable autocommit</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠳᠤᠰᠢᠶᠠᠬᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location line="-1218"/>
        <location line="+349"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult::reset: &apos;SQL_CURSOR_STATIC&apos; ᠢ᠋/ ᠵᠢ ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠪᠤᠯᠠᠭᠠᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠲᠠ ODBC ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠤ᠋ᠨ ᠫᠷᠤᠭᠷᠡᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location line="-332"/>
        <location line="+626"/>
        <source>Unable to execute statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠵᠢᠬᠦᠯᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-555"/>
        <source>Unable to fetch next</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠡ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+279"/>
        <source>Unable to prepare statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Unable to bind variable</source>
        <translation>ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠵᠢ ᠤᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+194"/>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-475"/>
        <location line="+578"/>
        <source>Unable to fetch last</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ ᠬᠢ ᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-672"/>
        <source>Unable to fetch</source>
        <translation>ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Unable to fetch first</source>
        <translation>ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠢ᠋ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch previous</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢ ᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/gui/util/qdesktopservices_mac.cpp" line="+165"/>
        <source>Home</source>
        <translation>ᠭᠡᠷ</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdatabackend.cpp" line="+74"/>
        <source>Operation not supported on %1</source>
        <translation>%1 ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid URI: %1</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢURI: %1</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdebugpipebackend.cpp" line="+175"/>
        <source>Write error writing to %1: %2</source>
        <translation>%1 ᠶᠢ ᠢ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤ:%2</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Read error reading from %1: %2</source>
        <translation>%1 ᠶᠢ ᠢ ᠤᠩᠰᠢᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤ: %2</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Socket error on %1: %2</source>
        <translation>%1 ᠳᠡᠭᠡᠷᠡᠬᠢ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠦᠰᠦᠭ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ: %2</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation>ᠠᠯᠤᠰ ᠡᠵᠡᠮᠰᠢᠯ ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠬᠡᠳᠦ ᠡᠷᠲᠡ %1 ᠳᠡᠭᠡᠷᠡᠬᠢ ᠲᠤᠰ ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠬᠠᠭᠠᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Protocol error: packet of size 0 received</source>
        <translation>ᠬᠡᠯᠡᠯᠴᠡᠬᠡᠷ ᠪᠤᠷᠤᠭᠤ: ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠨᠢ 0 ᠪᠤᠯᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/network/kernel/qhostinfo.cpp" line="+177"/>
        <location line="+57"/>
        <source>No host name given</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠳᠤᠭᠳᠠᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="+1195"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>ᠬᠡᠮᠵᠢᠭᠳᠡᠯ</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="+763"/>
        <source>Unable to connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Could not begin transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Could not commit transaction</source>
        <translation>ᠬᠡᠷᠡᠭ ᠶᠠᠪᠤᠳᠠᠯ ᠢ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Could not rollback transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+358"/>
        <source>Unable to subscribe</source>
        <translation>ᠵᠠᠬᠢᠶᠠᠯᠠᠨ ᠤᠩᠰᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Unable to unsubscribe</source>
        <translation>ᠵᠠᠬᠢᠶᠠᠯᠠᠨ ᠤᠩᠰᠢᠬᠤ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location line="-1058"/>
        <source>Unable to create query</source>
        <translation>ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠶᠢ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+374"/>
        <source>Unable to prepare statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="+304"/>
        <source>Centimeters (cm)</source>
        <translation>ᠰᠠᠨᠲ᠋ᠢᠮᠧᠲ᠋ᠷ(cm)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Millimeters (mm)</source>
        <translation>ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ(mm)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Inches (in)</source>
        <translation>ᠢᠨᠴ(in)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Points (pt)</source>
        <translation>ᠴᠡᠭ (pt)</translation>
    </message>
    <message>
        <source>Pica (P̸)</source>
        <translation>ᠫᠠᠢᠺ (P̸)</translation>
    </message>
    <message>
        <source>Didot (DD)</source>
        <translation>ᠳ᠋ᠢᠲᠦᠢ (DD)</translation>
    </message>
    <message>
        <source>Cicero (CC)</source>
        <translation>ᠰᠢᠰᠧᠯ (CC)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupwidget.ui"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
    <message>
        <location/>
        <source>Paper</source>
        <translation>ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Page size:</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location/>
        <source>Width:</source>
        <translation>ᠦᠷᠬᠡᠨ:</translation>
    </message>
    <message>
        <location/>
        <source>Height:</source>
        <translation>ᠦᠨᠳᠦᠷ:</translation>
    </message>
    <message>
        <location/>
        <source>Paper source:</source>
        <translation>ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠢᠷᠡᠯᠳᠡ:</translation>
    </message>
    <message>
        <location/>
        <source>Orientation</source>
        <translation>ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location/>
        <source>Portrait</source>
        <translation>ᠬᠦᠨᠳᠡᠯᠡᠨ</translation>
    </message>
    <message>
        <location/>
        <source>Landscape</source>
        <translation>ᠭᠤᠤᠯᠳᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Reverse landscape</source>
        <translation>ᠭᠤᠤᠯᠳᠤ ᠵᠢᠨ ᠡᠰᠡᠷᠬᠦ</translation>
    </message>
    <message>
        <location/>
        <source>Reverse portrait</source>
        <translation>ᠭᠤᠤᠯᠳᠤ ᠵᠢᠨ ᠡᠰᠡᠷᠬᠦ</translation>
    </message>
    <message>
        <location/>
        <source>Margins</source>
        <translation>ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location/>
        <source>top margin</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location/>
        <source>left margin</source>
        <translation>ᠵᠡᠬᠦᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location/>
        <source>right margin</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location/>
        <source>bottom margin</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location/>
        <source>Page Layout</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location/>
        <source>Pages per sheet:</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ ᠪᠦᠷᠢ ᠵᠢᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location/>
        <source>Page order:</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ:</translation>
    </message>
</context>
<context>
    <name>QCupsJobWidget</name>
    <message>
        <location/>
        <source>Job Control</source>
        <translation>ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠡᠵᠡᠮᠳᠡᠯ</translation>
    </message>
    <message>
        <location/>
        <source>Scheduled printing:</source>
        <translation>ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠴᠠᠭ ᠲᠤ᠌ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location/>
        <source>Print Immediately</source>
        <translation>ᠳᠠᠷᠤᠢ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Hold Indefinitely</source>
        <translation>ᠳᠠᠷᠤᠢ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Day (06:00 to 17:59)</source>
        <translation>ᠡᠳᠦᠷ (06:00 to 17:59)</translation>
    </message>
    <message>
        <location/>
        <source>Night (18:00 to 05:59)</source>
        <translation>ᠦᠳᠡᠰᠢ (18:00 to 05:59)</translation>
    </message>
    <message>
        <location/>
        <source>Second Shift (16:00 to 23:59)</source>
        <translation>ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠠᠩᠬᠢ (16:00 to 23:59)</translation>
    </message>
    <message>
        <location/>
        <source>Third Shift (00:00 to 07:59)</source>
        <translation>ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠠᠩᠬᠢ (00:00 to 07:59)</translation>
    </message>
    <message>
        <location/>
        <source>Weekend (Saturday to Sunday)</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯᠴᠢ ( ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠵᠢᠷᠭᠤᠭᠠᠨ ᠡᠴᠡ ᠭᠠᠷᠠᠭ ᠤ᠋ᠨ ᠡᠳᠦᠷ)</translation>
    </message>
    <message>
        <location/>
        <source>Specific Time</source>
        <translation>ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location/>
        <source>Billing information:</source>
        <translation>ᠰᠦᠢᠳᠬᠡᠯ ᠳᠡᠮᠳᠡᠭᠯᠡᠬᠦ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ:</translation>
    </message>
    <message>
        <location/>
        <source>Job priority:</source>
        <translation>ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ ᠳᠡᠰ:</translation>
    </message>
    <message>
        <location/>
        <source>Banner Pages</source>
        <translation>ᠬᠦᠨᠳᠡᠯᠡᠨ ᠪᠢᠴᠢᠬᠡᠰᠦ ᠵᠢᠨ ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Start:</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ:</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location/>
        <source>Standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location/>
        <source>Unclassified</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location/>
        <source>Confidential</source>
        <translation>ᠨᠢᠭᠤᠴᠠ</translation>
    </message>
    <message>
        <location/>
        <source>Classified</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠪᠠ</translation>
    </message>
    <message>
        <location/>
        <source>Secret</source>
        <translation>ᠨᠢᠭᠤᠴᠠ</translation>
    </message>
    <message>
        <location/>
        <source>Top Secret</source>
        <translation>ᠮᠠᠰᠢ ᠨᠢᠭᠤᠴᠠ</translation>
    </message>
    <message>
        <location/>
        <source>End:</source>
        <translation>ᠳᠠᠭᠤᠰᠬᠤ:</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+24"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>The plugin was not loaded.</source>
        <translation>ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠠᠴᠢᠶᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../src/gui/painting/qprinterinfo_unix.cpp" line="+98"/>
        <source>locally connected</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠨᠢᠭᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+225"/>
        <source>Aliases: %1</source>
        <translation>ᠪᠤᠰᠤᠳ ᠨᠡᠷ᠎ᠡ: %1</translation>
    </message>
    <message>
        <location line="+223"/>
        <location line="+199"/>
        <source>unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="+375"/>
        <source>Print all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print selection</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print range</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠡᠪᠴᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation>A0 (841 x 1189 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1 (594 x 841 mm)</source>
        <translation>A1 (594 x 841 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2 (420 x 594 mm)</source>
        <translation>A2 (420 x 594 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3 (297 x 420 mm)</source>
        <translation>A3 (297 x 420 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation>A4 (210 x 297 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ᠂ 8.26 x 11.7 ᠢᠨᠴ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 (148 x 210 mm)</source>
        <translation>A5 (148 x 210 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6 (105 x 148 mm)</source>
        <translation>A6 (105 x 148 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7 (74 x 105 mm)</source>
        <translation>A7 (74 x 105 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8 (52 x 74 mm)</source>
        <translation>A8 (52 x 74 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9 (37 x 52 mm)</source>
        <translation>A9 (37 x 52 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation>B0 (1000 x 1414 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation>B1 (707 x 1000 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2 (500 x 707 mm)</source>
        <translation>B2 (500 x 707 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3 (353 x 500 mm)</source>
        <translation>B3 (353 x 500 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4 (250 x 353 mm)</source>
        <translation>B4 (250 x 353 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation>B5 (176 x 250 ᠮᠢᠯᠢᠮᠸᠲᠷ, 6.93 x 9.84 ᠢᠨᠴ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6 (125 x 176 mm)</source>
        <translation>B6 (125 x 176 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7 (88 x 125 mm)</source>
        <translation>B7 (88 x 125 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8 (62 x 88 mm)</source>
        <translation>B8 (62 x 88 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9 (44 x 62 mm)</source>
        <translation>B9 (44 x 62 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10 (31 x 44 mm)</source>
        <translation>B10 (31 x 44 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E (163 x 229 mm)</source>
        <translation>C5E (163 x 229 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE (110 x 220 mm)</source>
        <translation>DLE (110 x 220 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation>ᠵᠠᠰᠠᠭ ᠵᠠᠬᠢᠷᠭᠠᠨ (7.5 x 10 ᠢᠨᠴ᠂ 191 x 254 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (210 x 330 mm)</source>
        <translation>Folio (210 x 330 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation>Ledger (432 x 279 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation>ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ (8.5 x 14 ᠢᠨᠴ᠂ 216 x 356 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation>Letter (8.5 x 11 ᠢᠨᠴ᠂ 216 x 279 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation>Tabloid (279 x 432 ᠮᠢᠯᠢᠮᠧᠲ᠋ᠷ)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation>ᠠᠮᠸᠷᠢᠺᠡ ᠵᠢᠨ ᠡᠩ ᠤ᠋ᠨ 10 ᠨᠤᠮᠸᠷᠳᠤ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠤᠭᠤᠳᠠ (105 x 241 ᠮᠢᠯᠢᠮᠸᠲᠷ)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+268"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qabstractprintdialog.cpp" line="+110"/>
        <location line="+13"/>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="-2"/>
        <source>Print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠠᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-357"/>
        <source>Print To File ...</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠡᠭᠡᠳ ᠹᠠᠢᠯ ᠳ᠋ᠤ᠌...</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>ᠹᠠᠢᠯ %1 ᠢ᠋/ ᠵᠢᠨ ᠪᠢᠴᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.
ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>%1 ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.
ᠲᠠ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="-210"/>
        <source>File exists</source>
        <translation>ᠹᠠᠢᠯ ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; ᠲᠠ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ ᠤᠤ?&lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-8"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>%1 ᠨᠢ ᠭᠠᠷᠴᠠᠭ.
ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+1"/>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation>&apos; ᠹᠤᠤᠮ&apos; ᠤ᠋ᠨ ᠲᠣᠭᠠᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ &apos; ᠬᠦᠷᠬᠦ&apos; ᠵᠢᠨ ᠲᠣᠭᠠᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠡᠴᠡ ᠶᠡᠭᠡ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="-232"/>
        <source>A0</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E</source>
        <translation>C5E</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE</source>
        <translation>ᠳ᠋ᠸ ᠯᠠᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive</source>
        <translation>ᠪᠤᠳᠤᠯᠭ᠎ᠠ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠪᠢᠴᠢᠭ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio</source>
        <translation>ᠡᠰᠡᠷᠬᠦᠯᠵᠤ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal</source>
        <translation>ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠵᠢᠨ ᠳᠤᠰᠬᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid</source>
        <translation>ᠵᠢᠵᠢᠭ ᠰᠤᠨᠢᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope</source>
        <translation>ᠠᠮᠸᠷᠢᠺᠡ ᠵᠢᠨ ᠡᠩ ᠤ᠋ᠨ 10 ᠨᠤᠮᠸᠷᠳᠦ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠤᠭᠤᠳᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Folio ᠴᠠᠭᠠᠰᠤ (8.27 x 13 in)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-522"/>
        <location line="+68"/>
        <source>&amp;Options &gt;&gt;</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ (&amp;O) &gt;&gt;</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>&amp;Print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠠᠬᠦ (&amp;P)</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>&amp;Options &lt;&lt;</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ (&amp;O) &lt;&lt;</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>Print to File (PDF)</source>
        <translation>ᠹᠠᠢᠯ (PDF) ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print to File (Postscript)</source>
        <translation>ᠹᠠᠢᠯ (Postscript) ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Local file</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write %1 file</source>
        <translation>%1 ᠹᠠᠢᠯ ᠢ᠋ ᠪᠢᠴᠢᠭᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qabstractpagesetupdialog.cpp" line="+68"/>
        <location line="+12"/>
        <source>Page Setup</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpreviewdialog.cpp" line="+252"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Print Preview</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ ᠢ᠋ ᠤᠷᠢᠳᠴᠢᠯᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Next page</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous page</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First page</source>
        <translation>ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last page</source>
        <translation>ᠬᠠᠮᠤᠭ ᠰᠡᠭᠦᠯ ᠤ᠋ᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Fit width</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠬᠤ ᠦᠷᠬᠡᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fit page</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠬᠤ ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Zoom in</source>
        <translation>ᠶᠡᠬᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom out</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Portrait</source>
        <translation>ᠭᠤᠤᠯᠳᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Landscape</source>
        <translation>ᠬᠦᠨᠳᠡᠯᠡᠨ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show single page</source>
        <translation>ᠳ᠋ᠠᠩ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show facing pages</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show overview of all pages</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠤᠪᠴᠢ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠠᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page setup</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Export to PDF</source>
        <translation>PDF ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export to PostScript</source>
        <translation>PostScript ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertieswidget.ui"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
    <message>
        <location/>
        <source>Page</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Advanced</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <location filename="../src/gui/dialogs/qprintsettingsoutput.ui"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
    <message>
        <location/>
        <source>Copies</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Print range</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠡᠪᠴᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location/>
        <source>Print all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Pages from</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠡᠨᠳᠡ ᠡᠴᠡ</translation>
    </message>
    <message>
        <location/>
        <source>to</source>
        <translation>ᠡᠴᠡ</translation>
    </message>
    <message>
        <location/>
        <source>Pages</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠲᠣᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location/>
        <source>Page Set:</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ:</translation>
    </message>
    <message>
        <location/>
        <source>Selection</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Output Settings</source>
        <translation>ᠭᠠᠷᠭᠠᠯᠳᠠ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Copies:</source>
        <translation>ᠨᠦᠭᠡᠴᠡ:</translation>
    </message>
    <message>
        <location/>
        <source>Collate</source>
        <translation>ᠬᠠᠷᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>Reverse</source>
        <translation>ᠡᠰᠡᠷᠬᠦ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location/>
        <source>Color Mode</source>
        <translation>ᠥᠩᠭᠡ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>ᠦᠩᠭᠡ ᠪᠣᠳᠣᠭ</translation>
    </message>
    <message>
        <location/>
        <source>Grayscale</source>
        <translation>ᠴᠠᠢᠪᠤᠷ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location/>
        <source>Duplex Printing</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠰᠡᠭ ᠵᠢᠡᠷ ᠫᠠᠷᠢᠨᠲᠸᠷᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location/>
        <source>Long side</source>
        <translation>ᠤᠷᠳᠤ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location/>
        <source>Short side</source>
        <translation>ᠪᠤᠭᠤᠨᠢ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintwidget.ui"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
    <message>
        <location/>
        <source>Printer</source>
        <translation>ᠫᠠᠷᠢᠨᠲᠸᠷ</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ (&amp;N):</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;roperties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ (&amp;R)</translation>
    </message>
    <message>
        <location/>
        <source>Location:</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ:</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>ᠤᠷᠢᠳᠴᠢᠯᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location/>
        <source>Type:</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location/>
        <source>Output &amp;file:</source>
        <translation>ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ (&amp;F)：</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <location filename="../src/corelib/io/qprocess_unix.cpp" line="+475"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+147"/>
        <source>Could not open input redirection for reading</source>
        <translation>ᠤᠩᠰᠢᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳᠠᠬᠢᠨ ᠴᠢᠭᠯᠡᠬᠦᠯᠬᠦ ᠵᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+36"/>
        <source>Could not open output redirection for writing</source>
        <translation>ᠪᠢᠴᠢᠬᠦ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳᠠᠬᠢᠨ ᠴᠢᠭᠯᠡᠬᠦᠯᠬᠦ ᠵᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+235"/>
        <source>Resource error (fork failure): %1</source>
        <translation>ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠪᠤᠷᠤᠭᠤ (fork ᠢᠯᠠᠭᠳᠠᠪᠠ): %1</translation>
    </message>
    <message>
        <location line="+259"/>
        <location line="+53"/>
        <location line="+74"/>
        <location line="+67"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+422"/>
        <location line="+50"/>
        <location line="+75"/>
        <location line="+42"/>
        <location line="+54"/>
        <source>Process operation timed out</source>
        <translation>ᠠᠬᠢᠴᠠ ᠵᠢᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠯᠳᠡ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess.cpp" line="+533"/>
        <location line="+52"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-211"/>
        <location line="+50"/>
        <source>Error reading from process</source>
        <translation>ᠠᠬᠢᠴᠠ ᠡᠴᠡ ᠤᠩᠰᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+779"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+140"/>
        <source>Error writing to process</source>
        <translation>ᠠᠬᠢᠴᠠ ᠳ᠋ᠤ᠌ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="-709"/>
        <source>Process crashed</source>
        <translation>ᠠᠬᠢᠴᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠭᠠᠯᠵᠠᠭᠤᠷᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+912"/>
        <source>No program defined</source>
        <translation>no program defined</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-341"/>
        <source>Process failed to start</source>
        <translation>ᠠᠬᠢᠴᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprogressdialog.cpp" line="+182"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠭᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="-8"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <location line="+12"/>
        <source>Check</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠰᠠᠨ</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="+64"/>
        <source>no error occurred</source>
        <translation>ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disabled feature used</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠤᠭᠰᠠᠨ ᠤᠨᠴᠠᠭᠠᠢ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad char class syntax</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠬᠡᠯᠡᠨ ᠵᠦᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad lookahead syntax</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠬᠡᠯᠡᠨ ᠵᠦᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad repetition syntax</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠳᠠᠪᠬᠤᠴᠠᠬᠤ ᠬᠡᠯᠡᠨ ᠵᠦᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid octal value</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠨᠠᠢ᠍ᠮᠠ ᠪᠡᠷ ᠳᠠᠪᠰᠢᠬᠤ ᠲᠤᠭᠠᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing left delim</source>
        <translation>ᠵᠡᠬᠦᠨ ᠬᠤᠪᠢᠢᠶᠠᠷᠢᠯᠠᠬᠤ ᠳᠮᠡᠳᠡᠭ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end</source>
        <translation>ᠰᠠᠨᠠᠮᠰᠠᠷ ᠦᠬᠡᠢ ᠳᠡᠬᠦᠰᠬᠡᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>met internal limit</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯᠳᠠ ᠳ᠋ᠤ᠌ ᠤᠴᠠᠷᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="+396"/>
        <source>Error to open database</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠬᠦᠮᠦᠷᠬᠡ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to begin transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback Transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location line="-323"/>
        <source>Unable to fetch results</source>
        <translation>ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ᠋ ᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Unable to execute statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="+528"/>
        <source>Error opening database</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠬᠦᠮᠦᠷᠭᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error closing database</source>
        <translation>ᠬᠠᠭᠠᠭᠰᠠᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠬᠦᠮᠦᠷᠭᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to begin transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to commit transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to rollback transaction</source>
        <translation>ᠤᠴᠢᠷ ᠶᠠᠪᠤᠳᠠᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location line="-400"/>
        <location line="+66"/>
        <location line="+8"/>
        <source>Unable to fetch row</source>
        <translation>ᠮᠦᠷ ᠢ᠋ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Unable to execute statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to reset statement</source>
        <translation>ᠦᠬᠦᠯᠡᠪᠦᠷᠢ ᠵᠢ ᠳᠠᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Unable to bind parameters</source>
        <translation>ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠢ᠋ ᠤᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter count mismatch</source>
        <translation>ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="-208"/>
        <source>No query</source>
        <translation>ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="+448"/>
        <source>Scroll here</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠡᠳ ᠡᠨᠳᠡ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left edge</source>
        <translation>ᠵᠡᠬᠦᠨ ᠬᠦᠪᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>ᠤᠷᠤᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠬᠦᠪᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>ᠢᠷᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page left</source>
        <translation>ᠵᠡᠬᠦᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+143"/>
        <source>Page up</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+4"/>
        <source>Page down</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scroll left</source>
        <translation>ᠵᠡᠭᠦᠨᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>ᠳᠡᠭᠡᠭᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>ᠳᠤᠷᠤᠭᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-6"/>
        <source>Line up</source>
        <translation>ᠳᠡᠬᠡᠭᠰᠢ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Position</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Line down</source>
        <translation>ᠳᠤᠷᠤᠭᠰᠢ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory.cpp" line="+207"/>
        <source>%1: unable to set key on lock</source>
        <translation>%1: ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>%1: create size is less then 0</source>
        <translation>%1: ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ 0 ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+168"/>
        <location filename="../src/corelib/kernel/qsharedmemory_p.h" line="+148"/>
        <source>%1: unable to lock</source>
        <translation>%1: ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1: unable to unlock</source>
        <translation>%1: ᠳᠤᠭᠳᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_unix.cpp" line="+78"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+87"/>
        <source>%1: permission denied</source>
        <translation>%1: ᠡᠷᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-22"/>
        <source>%1: already exists</source>
        <translation>%1: ᠨᠢᠬᠡᠨᠳᠡ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: doesn&apos;t exists</source>
        <translation>%1: ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: out of resources</source>
        <translation>%1:ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>%1: ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ %2</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>%1: key is empty</source>
        <translation>%1: ᠳᠠᠷᠤᠪᠴᠢ ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1: unix key file doesn&apos;t exists</source>
        <translation>%1: unix ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢᠨ ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1: ftok failed</source>
        <translation>%1: ftok ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+51"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+15"/>
        <source>%1: unable to make key</source>
        <translation>%1: ᠳᠠᠷᠤᠪᠴᠢ ᠵᠣᠬᠢᠶᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1: system-imposed size restrictions</source>
        <translation>%1: ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠬᠡᠮᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1: not attached</source>
        <translation>%1: ᠨᠡᠮᠡᠯᠳᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-27"/>
        <source>%1: invalid size</source>
        <translation>%1: ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>%1: key error</source>
        <translation>%1: ᠳᠠᠷᠤᠪᠴᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>%1: size query failed</source>
        <translation>%1: ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="+373"/>
        <source>Space</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Esc</source>
        <translation>esc</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab</source>
        <translation>ᠱᠣᠰᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backtab</source>
        <translation>ᠠᠷᠤ ᠮᠥᠷ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backspace</source>
        <translation>Backspace</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter</source>
        <translation>ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>ᠳ᠋ᠸ ᠯᠠᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pause</source>
        <translation>ᠲᠦᠷ ᠵᠤᠭᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠠᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SysReq</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home</source>
        <translation>ᠭᠡᠷ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>End</source>
        <translation>ᠲᠡᠭᠦᠰᠬᠡᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>ᠵᠡᠭᠦᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Up</source>
        <translation>ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Down</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgUp</source>
        <translation>pgUp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgDown</source>
        <translation>pgDown</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CapsLock</source>
        <translation>ᠲᠤᠮᠤ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NumLock</source>
        <translation>numLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ScrollLock</source>
        <translation>scrollLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Back</source>
        <translation>ᠤᠬᠤᠷᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward</source>
        <translation>ᠵᠥᠪ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>ᠵᠣᠭᠰᠣᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Refresh</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Down</source>
        <translation>ᠳᠠᠭᠤ ᠵᠢ ᠪᠠᠭᠠᠰᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Mute</source>
        <translation>ᠳᠠᠭᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Up</source>
        <translation>ᠳᠠᠭᠤ ᠵᠢ ᠶᠡᠬᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Boost</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ ᠳᠠᠭᠤ ᠵᠢ ᠨᠡᠮᠡᠭᠳᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Up</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ ᠳᠠᠭᠤ ᠵᠢ ᠶᠡᠬᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Down</source>
        <translation>ᠳᠠᠭᠤ ᠵᠢ ᠪᠠᠭᠠᠰᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Up</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠠᠭᠤ ᠵᠢ ᠶᠡᠬᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Down</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠠᠭᠤ ᠵᠢ ᠪᠠᠭᠠᠰᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Play</source>
        <translation>ᠮᠸᠳᠢᠶᠠ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠦᠭᠴᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Stop</source>
        <translation>ᠮᠸᠳᠢᠶᠠ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Previous</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠮᠸᠳᠢᠶᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠮᠸᠳᠢᠶᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Record</source>
        <translation>ᠮᠸᠳᠢᠶᠠ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Favorites</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠳᠤᠷᠠᠳᠠᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>ᠪᠦᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Standby</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>URL ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Mail</source>
        <translation>ᠢᠮᠸᠯ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Media</source>
        <translation>ᠮᠸᠳᠢᠶᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (0)</source>
        <translation>(0) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (1)</source>
        <translation>(1) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (2)</source>
        <translation>(2) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (3)</source>
        <translation>(3) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (4)</source>
        <translation>(4) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (5)</source>
        <translation>(5) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (6)</source>
        <translation>(6) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (7)</source>
        <translation>(7) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (8)</source>
        <translation>(8) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (9)</source>
        <translation>(9) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (A)</source>
        <translation>(A) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (B)</source>
        <translation>(B) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (C)</source>
        <translation>(C) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (D)</source>
        <translation>(D) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (E)</source>
        <translation>(E) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (F)</source>
        <translation>(F) ᠢ᠋/ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print Screen</source>
        <translation>print Screen</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Up</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Down</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caps Lock</source>
        <translation>ᠲᠤᠮᠤ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Num Lock</source>
        <translation>num Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number Lock</source>
        <translation>number Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Lock</source>
        <translation>scroll Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Escape</source>
        <translation>ᠳᠤᠲᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Request</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>ᠦᠬᠡᠢ/ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Context1</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠤᠷᠠᠬᠢ ᠹᠠᠢᠯ 1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context2</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠤᠷᠠᠬᠢ ᠹᠠᠢᠯ 2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context3</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠤᠷᠠᠬᠢ ᠹᠠᠢᠯ 3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context4</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠤᠷᠠᠬᠢ ᠹᠠᠢᠯ 4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call</source>
        <translation>ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangup</source>
        <translation>ᠡᠯᠬᠦᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip</source>
        <translation>ᠤᠷᠪᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+527"/>
        <location line="+122"/>
        <source>Ctrl</source>
        <translation>ctrl</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+125"/>
        <source>Shift</source>
        <translation>shift</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+122"/>
        <source>Alt</source>
        <translation>alt</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+117"/>
        <source>Meta</source>
        <translation>meta</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>F%1</source>
        <translation>F%1</translation>
    </message>
    <message>
        <location line="-720"/>
        <source>Home Page</source>
        <translation>ᠭᠤᠤᠯ ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+151"/>
        <source>Page left</source>
        <translation>ᠵᠡᠬᠦᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Position</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-67"/>
        <source>Connection to proxy refused</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠵᠢ ᠳᠡᠪᠴᠢᠪᠡ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to proxy closed prematurely</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢᠨ ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠬᠡᠳᠦ ᠡᠷᠲᠡ ᠬᠠᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Proxy host not found</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Connection to proxy timed out</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy authentication failed</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢ ᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Proxy authentication failed: %1</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢ ᠬᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ: %1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>SOCKS version 5 protocol error</source>
        <translation>SOCKS ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ 5 ᠭᠡᠷ᠎ᠡ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>General SOCKSv5 server failure</source>
        <translation>ᠪᠠᠢᠩᠭᠤ ᠵᠢᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ SOCKSv5 ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>TTL expired</source>
        <translation>TTL ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠦᠩᠬᠡᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>SOCKSv5 command not supported</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ SOCKSv5 ᠵᠠᠷᠯᠢᠭ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address type not supported</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ ᠬᠠᠶᠢᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ SOCKSv5 ᠤᠷᠤᠯᠠᠭᠴᠢ᠂ ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋ 0x%1</translation>
    </message>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">连接到套接字服务器的时候，Socks5超时错误</translation>
    </message>
    <message>
        <location line="+685"/>
        <source>Network operation timed out</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-574"/>
        <source>More</source>
        <translation>ᠨᠡᠩ ᠣᠯᠠᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>ᠨᠡᠩ ᠴᠦᠬᠡᠨ</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="+890"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete this record?</source>
        <translation>ᠲᠤᠰ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>Yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>No</source>
        <translation>ᠦᠭᠡᠢ/ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Insert</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Update</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save edits?</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Confirm</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel your edits?</source>
        <translation>ᠲᠠᠨ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠵᠢᠨᠨ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ ᠤᠤ?</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../src/network/ssl/qsslsocket_openssl.cpp" line="+569"/>
        <source>Unable to write data: %1</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠪᠢᠴᠢᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ: %1</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Error while reading: %1</source>
        <translation>ᠤᠩᠰᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ: %1</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error during SSL handshake: %1</source>
        <translation>SSL ᠬᠤᠯᠪᠤᠭᠳᠠᠭᠰᠠᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤ: %1</translation>
    </message>
    <message>
        <location line="-524"/>
        <source>Error creating SSL context (%1)</source>
        <translation>SSL ᠤ᠋/ ᠵᠢᠨ/ ᠤ᠋ᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠤᠷᠠᠬᠢ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤ (%1)</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠬᠤᠭᠤᠰᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ (%1)</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Error creating SSL session, %1</source>
        <translation>SSL ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤ᠂ %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Error creating SSL session: %1</source>
        <translation>SSL ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤ: %1</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ ᠦᠬᠡᠢ ᠦᠨᠡᠮᠯᠡᠯ ᠢ᠋ ᠬᠠᠩᠭᠠᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ᠂%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading local certificate, %1</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠦᠨᠡᠮᠯᠡᠯ ᠢ᠋ ᠠᠴᠢᠶᠠᠯᠠᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ᠂ %1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error loading private key, %1</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠠᠴᠢᠶᠠᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Private key does not certificate public key, %1</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠳᠠᠷᠤᠪᠴᠢ ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠪᠠᠳᠤᠯᠭᠠᠵᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂%1</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="-41"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+66"/>
        <source>%1: out of resources</source>
        <translation>%1:ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location line="-13"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+4"/>
        <source>%1: permission denied</source>
        <translation>%1: ᠡᠷᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: already exists</source>
        <translation>%1: ᠨᠢᠬᠡᠨᠳᠡ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: does not exist</source>
        <translation>%1: ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+3"/>
        <source>%1: unknown error %2</source>
        <translation>%1: ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ %2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="+584"/>
        <source>Unable to open connection</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to use database</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠬᠦᠮᠦᠷᠬᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="-326"/>
        <source>Scroll Left</source>
        <translation>ᠵᠡᠭᠦᠨᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../src/network/socket/qtcpserver.cpp" line="+282"/>
        <source>Operation on socket is not supported</source>
        <translation>socket ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+1973"/>
        <source>&amp;Undo</source>
        <translation>ᠪᠤᠴᠠᠬᠤ (&amp;U)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Redo</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ (&amp;R)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cu&amp;t</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ (&amp;T)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ (&amp;C)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copy &amp;Link Location</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠬᠤ (&amp;L)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ (&amp;P)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭ᠋ᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+254"/>
        <location line="+6"/>
        <source>Press</source>
        <translation>ᠳᠠᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+8"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <location filename="../src/network/socket/qudpsocket.cpp" line="+169"/>
        <source>This platform does not support IPv6</source>
        <translation>ᠲᠤᠰ ᠲᠠᠪᠴᠠᠩ IPv6 ᠢ᠋/ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠶ</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="+386"/>
        <source>Undo</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Redo</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../src/gui/util/qundoview.cpp" line="+101"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt; ᠬᠣᠭᠣᠰᠣᠨ&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="+834"/>
        <source>Undo</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Redo</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+884"/>
        <source>LRM Left-to-right mark</source>
        <translation>LRM ᠵᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠳᠡᠮᠳᠡᠭ ᠳᠠᠯᠪᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLM Right-to-left mark</source>
        <translation>RLM ᠪᠠᠷᠠᠭᠤᠨ ᠡᠴᠡ ᠵᠡᠬᠦᠨᠰᠢ ᠳᠡᠮᠳᠡᠭ ᠳᠠᠯᠪᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJ ᠦᠷᠬᠡᠴᠡ ᠦᠬᠡᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJ ᠦᠷᠬᠡᠴᠡ ᠦᠬᠡᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦᠷ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWSP Zero width space</source>
        <translation>ZWSP ᠦᠷᠬᠡᠴᠡ ᠦᠬᠡᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation>LRE ᠵᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠡᠬᠢᠯᠡᠵᠤ ᠰᠢᠬᠢᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation>RLE ᠪᠠᠷᠠᠭᠤᠨ ᠡᠴᠡ ᠵᠡᠬᠦᠨᠰᠢ ᠡᠬᠢᠯᠡᠵᠤ ᠰᠢᠬᠢᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRO Start of left-to-right override</source>
        <translation>LRO ᠵᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠡᠬᠢᠯᠡᠵᠤ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLO Start of right-to-left override</source>
        <translation>RLO ᠪᠠᠷᠠᠭᠤᠨ ᠡᠴᠡ ᠵᠡᠬᠦᠨᠰᠢ ᠡᠬᠢᠯᠡᠵᠤ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF Pop directional formatting</source>
        <translation>PDF ᠦᠰᠦᠷᠴᠤ ᠭᠠᠷᠬᠤ ᠴᠢᠭᠯᠡᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert Unicode control character</source>
        <translation>Unicode ᠡᠵᠡᠮᠳᠡᠬᠦ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/FrameLoaderClientQt.cpp" line="+692"/>
        <source>Request cancelled</source>
        <translation>ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠦᠬᠡᠢᠰᠬᠡᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Request blocked</source>
        <translation>ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠰᠠᠭᠠᠳᠠᠭᠤᠯᠤᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot show URL</source>
        <translation>URL ᠢ᠋/ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frame load interruped by policy change</source>
        <translation>ᠪᠤᠳᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠬᠢᠭᠰᠡᠨ ᠡᠴᠡ ᠪᠤᠯᠵᠤ ᠲᠤᠯᠭᠠᠭᠤᠷᠢ ᠵᠢᠨ ᠠᠴᠢᠶᠠᠯᠠᠯᠳᠠ ᠵᠢ ᠳᠠᠰᠤᠯᠪᠠ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot show mimetype</source>
        <translation>mimetype ᠢ᠋/ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File does not exist</source>
        <translation>ᠹᠠᠢᠯ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/network/qt/QNetworkReplyHandler.cpp" line="+382"/>
        <source>Bad HTTP request</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ HTTP ᠭᠤᠶᠤᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/Localizations.cpp" line="+42"/>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation>ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation>ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation>ᠡᠨᠡ ᠪᠣᠯ ᠨᠢᠭᠡᠨ ᠬᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ᠂ ᠬᠠᠢᠬᠤ ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠦᠭᠡ ᠪᠡᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ: </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation>ᠹᠠᠢᠯ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤ ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠤᠩᠬᠤᠨ ᠳ᠋ᠤ᠌ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation>ᠵᠢᠷᠤᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation>ᠵᠢᠷᠤᠭ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation>ᠵᠢᠷᠤᠭ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation>ᠬᠠᠪᠢᠰᠤ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation>ᠵᠣᠭᠰᠣᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation>ᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation>ᠳᠠᠭᠠᠮᠠᠭᠯᠠᠯ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation>ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation>ᠳᠤᠯᠢ ᠪᠢᠴᠢᠭ ᠲᠤ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation>ᠳᠤᠯᠢ ᠪᠢᠴᠢᠭ ᠡᠴᠡ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠡ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation>ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation>ᠠᠪᠢᠶᠠᠯᠠᠵᠤ ᠪᠢᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>ᠠᠪᠢᠶᠠᠯᠠᠵᠤ ᠪᠢᠴᠢᠬᠦ ᠬᠢᠬᠡᠳ ᠬᠡᠯᠡᠨ ᠵᠦᠢ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>ᠠᠪᠢᠶᠠᠯᠠᠵᠤ ᠪᠢᠴᠢᠬᠦ ᠬᠢᠬᠡᠳ ᠬᠡᠯᠡᠨ ᠵᠦᠢ ᠵᠢ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠯᠲᠠ ᠵᠢ ᠠᠪᠢᠶᠠᠴᠢᠯᠠᠨ ᠪᠢᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation>ᠪᠢᠴᠢᠭᠯᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠪᠢᠶᠠᠯᠠᠵᠤ ᠪᠢᠴᠢᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation>ᠬᠡᠯᠡᠨ ᠵᠦᠢ ᠬᠢᠬᠡᠳ ᠠᠪᠢᠶᠠᠯᠠᠵᠤ ᠪᠢᠴᠢᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation>ᠪᠦᠳᠦᠭᠦᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation>ᠵᠢᠰᠢᠬᠦᠦ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation>ᠳᠤᠤᠷ᠎ᠠ ᠵᠢᠷᠤᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation>ᠪᠤᠷᠤ ᠳᠦᠯᠦᠪ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation>ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Text Direction</source>
        <comment>Text direction context sub-menu item</comment>
        <translation>ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠴᠢᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation>ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LTR</source>
        <comment>Left to Right context menu item</comment>
        <translation>LTR</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>RTL</source>
        <comment>Right to Left context menu item</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation>ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ ᠦᠶ᠎ᠡ ᠵᠢᠨ ᠬᠠᠢᠯᠳᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation>ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ ᠦᠶ᠎ᠡ ᠵᠢᠨ ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation>ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ ᠦᠶ᠎ᠡ ᠵᠢᠨ ᠬᠠᠢᠯᠳᠠ ᠵᠢ ᠪᠠᠯᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation>%1 (%2x%3 ᠫᠢᠺᠰᠸᠯ)</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/InspectorClientQt.cpp" line="+185"/>
        <source>Web Inspector - %2</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠴᠢ - %2</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/ScrollbarQt.cpp" line="+58"/>
        <source>Scroll here</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠡᠳ ᠡᠨᠳᠡ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Left edge</source>
        <translation>ᠵᠡᠬᠦᠨ ᠬᠦᠪᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>ᠤᠷᠤᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠬᠦᠪᠡᠬᠡ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>ᠢᠷᠤᠭᠠᠷ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page left</source>
        <translation>ᠵᠡᠬᠦᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scroll left</source>
        <translation>ᠵᠡᠭᠦᠨᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>ᠳᠡᠭᠡᠭᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>ᠳᠤᠷᠤᠭᠰᠢ ᠦᠩᠬᠦᠷᠢᠬᠦ</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/FileChooserQt.cpp" line="+45"/>
        <source>%n file(s)</source>
        <comment>number of chosen file</comment>
        <translation>
            <numerusform>%n ᠹᠠᠢᠯ</numerusform>
            <numerusform>%n ᠹᠠᠢᠯ ᠤ᠋ᠳ</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/Api/qwebpage.cpp" line="+1322"/>
        <source>JavaScript Alert - %1</source>
        <translation>JavaScript ᠰᠡᠷᠡᠮᠵᠢᠬᠦᠯᠦᠯ t - %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>JavaScript Confirm - %1</source>
        <translation>JavaScript ᠪᠠᠳᠤᠯᠠᠬᠤ - %1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>JavaScript Prompt - %1</source>
        <translation>JavaScript ᠰᠠᠨᠠᠭᠤᠯᠬᠤ - %1</translation>
    </message>
    <message>
        <location line="+333"/>
        <source>Move the cursor to the next character</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous character</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next word</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠠᠩ ᠦᠬᠡᠰ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous word</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠩ ᠦᠬᠡᠰ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next line</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠮᠥᠷ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous line</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠮᠥᠷ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the line</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠮᠥᠷ ᠤ᠋ᠨ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the line</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠮᠥᠷ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the block</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ᠋ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the block</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ᠋ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the document</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠹᠠᠢᠯ ᠤ᠋ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the document</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠺᠸᠰᠸ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠡᠴᠤᠰ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select all</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭ᠋ᠤᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next character</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous character</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠳᠡᠭᠡᠷᠡᠭᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next word</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠳᠠᠩ ᠦᠬᠡᠰ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous word</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠩ ᠦᠬᠡᠰ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next line</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠮᠥᠷ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous line</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠳᠡᠭᠡᠷᠡᠭᠢ ᠮᠥᠷ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the line</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠮᠥᠷ ᠤ᠋ᠨ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the line</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠮᠥᠷ ᠤ᠋ᠨ ᠡᠴᠤᠰ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the block</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ᠋ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the block</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ᠋ ᠡᠴᠤᠰ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the document</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the document</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠠᠳ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the start of the word</source>
        <translation>ᠬᠠᠰᠤᠭᠠᠳ ᠳᠠᠩ ᠦᠬᠡᠰ ᠤ᠋ᠨ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the end of the word</source>
        <translation>ᠬᠠᠰᠤᠭᠠᠳ ᠳᠠᠩ ᠦᠬᠡᠰ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Insert a new paragraph</source>
        <translation>insert a new paragraph</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert a new line</source>
        <translation>insert a new line</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../src/gui/kernel/qwhatsthis.cpp" line="+522"/>
        <source>What&apos;s This?</source>
        <translation>ᠡᠨᠡ ᠶᠠᠭᠤ ᠪᠤᠢ ?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/gui/kernel/qwidget.cpp" line="+5326"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="+637"/>
        <source>Go Back</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Commit</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Done</source>
        <translation>ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠢᠭᠡ ᠠᠯᠬᠤᠮ (&amp;B)</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠬᠤ (&amp;F)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠮᠵᠢ (&amp;H)</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Next</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠵᠢᠨ ᠠᠯᠬᠤᠮ (&amp;N)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Next &gt;</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠵᠢᠨ ᠠᠯᠬᠤᠮ (&amp;N) &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="+1094"/>
        <source>&amp;Restore</source>
        <translation>ᠰᠡᠷᠭᠦᠭᠡᠬᠦ (&amp;R)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Move</source>
        <translation>ᠱᠢᠯᠵᠢᠬᠦᠯᠬᠦ᠌ (&amp;M)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>ᠬᠡᠮᠵᠢᠶ᠎ᠡ (&amp;S)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mi&amp;nimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ （&amp;N）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ （&amp;X）</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ (&amp;C)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stay on &amp;Top</source>
        <translation>ᠶᠡᠷᠦᠳᠡᠬᠡᠨ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠡᠮᠦᠨ᠎ᠡ (&amp;T)</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1059"/>
        <source>Sh&amp;ade</source>
        <translation>ᠡᠪᠬᠡᠬᠦ (&amp;A)</translation>
    </message>
    <message>
        <location line="-278"/>
        <location line="+60"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="-1837"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore Down</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location line="+2053"/>
        <source>&amp;Unshade</source>
        <translation>ᠳᠡᠯᠬᠡᠬᠦ᠌ (&amp;U)</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../src/xml/sax/qxml.cpp" line="+58"/>
        <source>no error occurred</source>
        <translation>ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error triggered by consumer</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠡᠴᠡ ᠡᠬᠦᠳᠦᠯᠳᠡᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end of file</source>
        <translation>ᠰᠠᠨᠠᠮᠰᠠᠷ ᠥᠬᠡᠢ ᠪᠡᠷ ᠹᠠᠢᠯ ᠵᠤᠭᠰᠤᠭᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>more than one document type definition</source>
        <translation>ᠨᠢᠭᠡ ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠤ᠋ᠨ ᠳᠤᠭᠳᠠᠯ ᠡᠴᠡ ᠢᠯᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing element</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠢ᠋ ᠵᠠᠳᠠᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tag mismatch</source>
        <translation>ᠳᠡᠮᠳᠡᠭ ᠳᠠᠯᠪᠢᠬᠤ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing content</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠵᠠᠳᠠᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected character</source>
        <translation>ᠰᠠᠨᠠᠮᠰᠠᠷ ᠥᠬᠡᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid name for processing instruction</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>version expected while reading the XML declaration</source>
        <translation>XML ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠵᠢ ᠤᠩᠰᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠬᠡᠪᠯᠡᠯ ᠬᠦᠯᠢᠶᠡᠭᠳᠡᠵᠤ ᠪᠤᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>wrong value for standalone declaration</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠳᠤᠰᠠᠭᠠᠷ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>XML ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠵᠢ ᠤᠩᠰᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠪᠤᠶᠤ ᠳᠤᠰᠠᠭᠠᠷ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠬᠦᠯᠢᠶᠡᠭᠳᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>XML ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠵᠢ ᠤᠩᠰᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠳᠤᠰᠠᠭᠠᠷ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠬᠦᠯᠢᠶᠡᠭᠳᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing document type definition</source>
        <translation>ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠤ᠋ᠨ ᠳᠤᠭᠳᠠᠯ ᠢ᠋ ᠵᠠᠳᠠᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>letter is expected</source>
        <translation>ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠬᠦᠯᠢᠶᠡᠭᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing comment</source>
        <translation>ᠳᠠᠢᠯᠪᠤᠷᠢ ᠵᠢ ᠵᠠᠳᠠᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing reference</source>
        <translation>ᠯᠠᠪᠯᠠᠯᠳᠠ ᠵᠢ ᠵᠠᠳᠠᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>DTD ᠳᠤᠮᠳᠠ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠵᠠᠳᠠᠯᠤᠯᠳᠠ ᠵᠢᠨ ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠪᠤᠳᠠᠳᠤ ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠳᠤᠮᠳᠠ ᠭᠠᠳᠠᠷ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠪᠤᠳᠠᠳᠤ ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>DTD ᠳᠤᠮᠳᠠ ᠭᠠᠳᠠᠷ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠪᠤᠳᠠᠳᠤ ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unparsed entity reference in wrong context</source>
        <translation>ᠵᠠᠳᠠᠯᠤᠯᠳᠠ ᠥᠬᠡᠢ ᠪᠤᠷᠤᠭᠤ ᠳᠡᠭᠡᠷ᠎ᠡ ᠳᠤᠤᠷᠠᠬᠢ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠪᠤᠳᠠᠳᠤ ᠯᠠᠪᠯᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive entities</source>
        <translation>ᠰᠢᠬᠢᠳᠬᠡᠬᠦ ᠪᠡᠶᠡᠳᠦ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error in the text declaration of an external entity</source>
        <translation>ᠭᠠᠳᠠᠷ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠪᠤᠳᠠᠳᠤ ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠳ᠋ᠤ᠌ ᠠᠯᠳᠠᠭ᠎ᠠ ᠪᠤᠢ</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="+592"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+1769"/>
        <source>Extra content at end of document.</source>
        <translation>ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠬᠤᠪᠢ ᠵᠢᠨ ᠭᠠᠳᠠᠨᠠᠬᠢ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠤᠢ.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Invalid entity value.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠪᠡᠶᠡᠳᠦ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ.</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Invalid XML character.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ XML ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ.</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ ᠳ᠋ᠤ᠌ &apos;]]&gt;&apos; ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠪᠠᠢᠬᠤ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+309"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠭᠰᠡᠨ ᠣᠷᠣᠨ ᠵᠠᠢ &apos;%1&apos; ᠤ᠋ᠨ\ ᠵᠢᠨ ᠤᠭᠳᠤᠪᠤᠷᠢ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Attribute redefined.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>ᠨᠡᠢᠳᠡ ᠵᠢᠨ ᠳᠡᠮᠳᠡᠭ ᠲᠠᠢ ᠲᠸᠺᠰᠲ ᠳᠤᠮᠳᠠ ᠰᠠᠨᠠᠭᠠᠨ ᠡᠴᠡ ᠭᠠᠳᠠᠭᠤᠷ &apos;%1&apos; ᠪᠤᠢ.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Invalid XML version string.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ XML ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported XML version.</source>
        <translation>ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠥᠬᠡᠢ XML ᠬᠡᠪᠯᠡᠯ.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>%1 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Encoding %1 is unsupported</source>
        <translation>ᠺᠣᠳ᠋ %1 ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Standalone accepts only yes or no.</source>
        <translation>ᠳᠤᠰᠠᠭᠠᠷ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠨᠢ ᠵᠥᠪᠬᠡᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation>XML ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠳ᠋ᠤ᠌ ᠬᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Premature end of document.</source>
        <translation>ᠲᠸᠺᠰᠲ ᠬᠡᠳᠦ ᠡᠷᠲᠡ ᠳᠠᠭᠤᠰᠪᠠ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid document.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠲᠸᠺᠰᠲ.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Expected </source>
        <translation>ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ </translation>
    </message>
    <message>
        <location line="+11"/>
        <source>, but got &apos;</source>
        <translation>, ᠬᠡᠪᠡᠴᠤ ᠬᠦᠷᠳᠡᠭᠰᠡᠨ ᠨᠢ &apos;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unexpected &apos;</source>
        <translation>ᠰᠠᠨᠠᠮᠰᠠᠷ ᠥᠬᠡᠢ &apos;</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Expected character data.</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-995"/>
        <source>Recursive entity detected.</source>
        <translation>ᠰᠢᠬᠢᠳᠬᠡᠭᠰᠡᠨ ᠪᠡᠶᠡᠳᠦ ᠵᠢ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠤᠯᠪᠠ.</translation>
    </message>
    <message>
        <location line="+516"/>
        <source>Start tag expected.</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠤᠢ ᠳᠡᠮᠳᠡᠭ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>XML declaration not at start of document.</source>
        <translation>XML ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠡᠬᠢᠯᠡᠯ ᠤ᠋ᠨ ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ᠌ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation>ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠤ᠋ᠨ ᠪᠡᠶᠡᠳᠦ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠳ᠋ᠤ᠌ NDATA ᠪᠤᠢ.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>%1 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid processing instruction name.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-521"/>
        <location line="+12"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+164"/>
        <location line="+53"/>
        <source>Illegal namespace declaration.</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠵᠠᠢ ᠵᠢᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+15"/>
        <source>Invalid XML name.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ XML ᠨᠡᠷᠡᠢᠳᠦᠯ.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Opening and ending tag mismatch.</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ ᠳᠡᠮᠳᠡᠭ ᠪᠤᠯᠤᠨ ᠳᠡᠬᠦᠰᠬᠡᠬᠦ ᠳᠡᠮᠳᠡᠭ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>&apos;%1&apos; ᠪᠡᠶᠡᠳᠦ ᠵᠢᠨ ᠡᠰᠢ ᠳᠠᠳᠠᠯᠳᠠ ᠵᠢ ᠵᠠᠳᠠᠯᠤᠭᠰᠠᠨ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+61"/>
        <location line="+40"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>&apos;%1&apos; ᠪᠡᠶᠡᠳᠦ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠳ᠋ᠤ᠌ ᠬᠢ ᠭᠠᠳᠠᠷ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠪᠤᠳᠠᠳᠤ &apos;%1&apos; ᠵᠢᠨ ᠡᠰᠢᠯᠡᠯ.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid character reference.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠡᠰᠢ ᠳᠠᠳᠠᠯᠳᠠ.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-75"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-823"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation>ᠵᠥᠪ ᠪᠤᠰᠤ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠲᠠᠢ ᠳᠠᠭᠠᠷᠠᠯᠳᠤᠪᠠ.</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>ᠳᠤᠰᠠᠭᠠᠷ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠵᠢᠨ ᠬᠠᠭᠤᠷᠮᠠᠭ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠡᠷᠬᠡᠪᠰᠢ ᠺᠣᠳ᠋ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠢᠯᠡᠷᠡᠬᠦ.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+562"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠨᠡᠢᠳᠡ ᠵᠢᠨ PUBLIC ᠳᠡᠮᠳᠡᠭ.</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreebuilder.cpp" line="+205"/>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation>%2 ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠲᠠᠢ %1 ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠰᠠᠨ.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation>ᠨᠢᠭᠡᠨ %1 ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ %2 ᠵᠢᠡᠷ ᠪᠡᠷ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠣᠯᠭᠠᠭᠰᠠᠨ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ ᠬᠡᠪᠡᠴᠤ %3 ᠨᠢ ᠬᠠᠷᠢᠨ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qiodevicedelegate.cpp" line="+84"/>
        <source>Network timeout.</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠡᠷᠪᠡ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+320"/>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation>ᠡᠯᠧᠮᠧᠨ᠋ᠲ ᠨᠢ %1 ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ᠋ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠂ ᠤᠴᠢᠷ ᠨᠢ ᠲᠡᠷᠡ ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠡᠯᠧᠮᠧᠨ᠋ᠲ ᠡᠴᠡ ᠭᠠᠳᠠᠨ᠎ᠠ ᠢᠯᠡᠷᠡᠵᠡᠢ.</translation>
    </message>
    <message>
        <source>Attribute element %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="obsolete">属性元素 %1 不能被串行化，因为它出现在最顶层。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractdatetime.cpp" line="+80"/>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation>%1 ᠵᠢᠯ ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ᠂ ᠤᠴᠢᠷ ᠨᠢ %2 ᠡᠴᠡ ᠡᠬᠢᠯᠡᠬᠦ ᠶᠤᠰᠤᠳᠠᠢ.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation>%1 ᠡᠳᠦᠷ ᠨᠢ %2..%3 ᠬᠦᠷᠢᠶᠡᠨ ᠡᠴᠡ ᠭᠠᠳᠠᠨ᠎ᠠ ᠪᠤᠢ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation>%1 ᠰᠠᠷ᠎ᠠ ᠨᠢ %2..%3 ᠬᠦᠷᠢᠶᠡᠨ ᠡᠴᠡ ᠭᠠᠳᠠᠨ᠎ᠠ ᠪᠤᠢ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation>ᠠᠰᠬᠠᠷᠠᠯ: ᠳ᠋ᠠᠢᠲ᠋ᠠ %1 ᠢ᠋ \ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Day %1 is invalid for month %2.</source>
        <translation>%1 ᠡᠳᠦᠷ ᠨᠢ %2 ᠰᠠᠷ᠎ᠠ ᠳ᠋ᠤ᠌ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation>ᠴᠠᠭ 24:%1:%2.%3 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ᠂ ᠴᠠᠭ ᠨᠢ 24᠂ ᠬᠡᠪᠡᠴᠦ ᠮᠢᠨᠦ᠋ᠲ᠂ ᠰᠸᠺᠦ᠋ᠨ᠋ᠲ᠂ ᠮᠢᠨᠢᠰᠸᠺᠦ᠋ᠨ᠋ᠲ ᠨᠢ ᠪᠦᠷ 0 ᠪᠢᠰᠢ; </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation>ᠴᠠᠭ %1:%2:%3.%4 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation>ᠠᠰᠬᠠᠷᠠᠯ: ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠢᠯᠡᠷᠬᠦᠯᠦᠭᠳᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractduration.cpp" line="+99"/>
        <location line="+15"/>
        <source>At least one component must be present.</source>
        <translation>ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠳᠤᠨᠤᠭᠯᠠᠯ ᠢᠯᠡᠷᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation>ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠤᠨᠤᠭᠯᠠᠯ ᠡᠷᠬᠡᠪᠰᠢ ᠡᠨᠡ ᠬᠦ᠌ %1 ᠵᠠᠭᠠᠭ ᠤ᠋ᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠢᠯᠡᠷᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractfloatmathematician.cpp" line="+64"/>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠲᠣᠭᠠᠨ ᠤ᠋ ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠲᠣᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ᠂ %1, ᠪᠠᠰᠠ %2 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation>ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠡᠳᠦᠬᠡᠷ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠲᠣᠭ᠎ᠠ᠂ %1, ᠬᠢᠵᠠᠭᠠᠷ ᠥᠬᠡᠢ%2 ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation>ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠢᠨ ᠬᠣᠶᠠᠳᠤᠭᠠᠷ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠲᠣᠭ᠎ᠠ᠂ %1, ᠳᠡᠭ (%2) ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qanyuri_p.h" line="+132"/>
        <source>%1 is not a valid value of type %2.</source>
        <translation>%1 ᠨᠢ %2 ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomiccasters_p.h" line="+223"/>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation>%2 ᠡᠴᠡ ᠰᠢᠳᠡᠭᠡᠳ %1 ᠬᠦᠷᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠤᠭ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ %3 ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicmathematicians.cpp" line="+65"/>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation>ᠪᠦᠬᠦᠯᠢ ᠳᠤᠭᠠᠨ ᠤ᠋ ᠬᠠᠰᠤᠬᠤ (%1) ᠲᠡᠭ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ (%2) ᠨᠢ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation>ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ (%1) ᠲᠡᠭ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ (%2) ᠨᠢ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation>ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ (%1) ᠲᠡᠭ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ (%2) ᠨᠢ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+122"/>
        <location line="+32"/>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1 ᠪᠣᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠡᠴᠡ %2( ᠨᠢᠭᠡ ᠲᠣᠭᠠᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠢᠰᠢ) ᠢ᠋\ ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1 ᠪᠣᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠡᠴᠡ %2 ᠪᠤᠶᠤ %3 (ᠡᠶᠡᠷᠡᠬᠦ ᠰᠦᠬᠡᠷᠬᠦ ᠳᠡᠭ) ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1 ᠪᠣᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠡᠴᠡ %2 ᠪᠤᠶᠤ %3 (ᠡᠶᠡᠷᠡᠬᠦ ᠰᠦᠬᠡᠷᠬᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠥᠬᠡᠢ) ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicvalue.cpp" line="+79"/>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1 ᠪᠣᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠨᠢᠭᠡᠨ ᠬᠦᠴᠦᠨ ᠲᠠᠢ ᠪᠦᠸᠯ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qboolean.cpp" line="+78"/>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation>ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ Boolean ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠬᠤᠶᠠᠷ ᠪᠤᠶᠤ ᠨᠡᠩ ᠤᠯᠠᠨ ᠠᠲ᠋ᠤᠮ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠠᠭᠤᠯᠠᠭᠰᠠᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ᠋ ᠪᠤᠳᠤᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qderivedinteger_p.h" line="+402"/>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %2 ᠪᠣᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠤ᠋ᠨ %1 ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ (%3) ᠡᠴᠡ ᠳᠠᠪᠠᠵᠠᠢ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation>%2 ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ %1 ᠨᠢ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ (%3) ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠭᠰᠡᠨ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qhexbinary.cpp" line="+91"/>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation>ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠨᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠡᠳᠦᠨ ᠳᠡᠭᠰᠢ ᠲᠣᠭ᠎ᠠ ᠪᠠᠭᠲᠠᠨ᠎ᠠ᠂ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ %2 ᠡᠢᠮᠤ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 is not valid as a value of type %2.</source>
        <translation>%1 ᠨᠢ %2 ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qarithmeticexpression.cpp" line="+207"/>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠳᠡᠮᠳᠡᠭ %1 ᠨᠢ %2 ᠬᠡᠯᠪᠡᠷᠢ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠳᠡᠮᠳᠡᠭ %1 ᠢ᠋/ ᠵᠢ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠨᠢ %2 ᠪᠤᠯᠤᠨ %3 ᠪᠤᠯᠬᠤ ᠠᠲ᠋ᠣᠮ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠶ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qattributenamevalidator.cpp" line="+66"/>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠪᠤᠳᠤᠯᠳᠠ ᠵᠢᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳᠤᠮᠳᠠᠬᠢ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ URI ᠨᠢ %1 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠪᠤᠳᠤᠯᠳᠠ ᠵᠢᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ %2 ᠢ᠋/ ᠵᠢ ᠠᠭᠤᠯᠠᠭᠰᠠᠨ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ URI ᠢ᠋/ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastas.cpp" line="+88"/>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation>ᠰᠢᠳᠡᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠪᠤᠷᠤᠭᠤ᠂ ᠬᠦᠰᠡᠨ ᠬᠦᠯᠢᠶᠡᠭᠳᠡᠵᠤ ᠪᠤᠢ ᠨᠢ %1, ᠬᠤᠷᠢᠶᠠᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠨᠢ %2.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation>ᠰᠢᠳᠡᠭᠡᠳ %1 ᠪᠤᠶᠤ ᠳᠡᠬᠦᠨ ᠤ᠋ ᠡᠬᠦᠰᠬᠡᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠡᠬᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ ᠳᠦᠷᠦᠯ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠢᠡ᠂ ᠡᠰᠡᠪᠡᠯ ᠲᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %2 ᠨᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastingplatform.cpp" line="+134"/>
        <source>No casting is possible with %1 as the target type.</source>
        <translation>%1 ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠬᠠᠷᠠᠯᠳᠠ ᠪᠤᠯᠭᠠᠭᠰᠠᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠰᠢᠳᠡᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation>%1 ᠡᠴᠡ ᠰᠢᠳᠡᠬᠡᠳ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠴᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠰᠢᠳᠡᠵᠤ ᠭᠠᠷᠭᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠤᠴᠢᠷ ᠨᠢ ᠳᠡᠷᠡ ᠨᠢ ᠨᠢᠭᠡᠨ ᠬᠡᠢᠰᠪᠦᠷᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ᠂ ᠪᠠᠰᠠ ᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠤᠯᠵᠤ ᠪᠤᠳᠠᠳᠠᠢ ᠵᠢᠰᠢᠶᠡᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠨᠢ %2 ᠪᠣᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ %1 ᠢ᠋/ ᠵᠢ ᠰᠢᠳᠡᠭᠡᠳ %3 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠴᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation>%2 ᠡᠴᠡ ᠰᠢᠳᠡᠭᠡᠳ %1 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ: %3</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcommentconstructor.cpp" line="+67"/>
        <source>A comment cannot contain %1</source>
        <translation>ᠳᠠᠢᠯᠪᠤᠷᠢ ᠳ᠋ᠤ᠌ %1 ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>A comment cannot end with a %1.</source>
        <translation>ᠳᠠᠢᠯᠪᠤᠷᠢ %1 ᠵᠢᠡᠷ \ ᠪᠡᠷ ᠳᠡᠬᠦᠰᠴᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomparisonplatform.cpp" line="+167"/>
        <source>No comparisons can be done involving the type %1.</source>
        <translation>%1 ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠬᠠᠷᠢᠴᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation>ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠨᠢ %2 ᠪᠤᠯᠤᠨ %3 ᠪᠤᠯᠬᠤ ᠠᠲ᠋ᠣᠮ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠤ᠋ᠨ ᠳᠤᠮᠳᠠ᠂ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠳᠡᠮᠳᠡᠭ %1 ᠢ᠋/ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠶ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qdocumentcontentvalidator.cpp" line="+86"/>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠤ᠋ᠨ ᠴᠡᠭ ᠨᠢ ᠨᠢᠭᠡᠨ ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠴᠡᠭ ᠤ᠋ᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠴᠡᠭ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃ ᠡᠢᠮᠤ ᠡᠴᠡ᠂ ᠡᠨᠡᠬᠦ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠪᠠᠢᠷᠢᠯᠠᠯ ᠳᠤᠬᠢᠷᠠᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qexpressionfactory.cpp" line="+169"/>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠬᠦᠮᠦᠷᠭᠡ ᠵᠢᠨ ᠮᠤᠳᠦ᠋ᠯ ᠰᠢᠭ᠋ᠤᠳ ᠦᠨᠡᠯᠡᠭᠳᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠲᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ ᠭᠤᠤᠯ ᠮᠤᠳᠦ᠋ᠯ ᠵᠢᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠤᠷᠤᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>No template by name %1 exists.</source>
        <translation>%1 ᠨᠡᠷ᠎ᠡ ᠲᠠᠢ ᠬᠡᠪ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qgenericpredicate.cpp" line="+106"/>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation>ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1 ᠪᠤᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠮᠠᠭᠠᠳᠯᠠᠭᠳᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃ ᠨᠢᠭᠡᠨ ᠮᠠᠭᠠᠳᠯᠠᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠭᠠᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠡᠰᠡᠪᠡᠯ ᠨᠢᠭᠡᠨ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ Boolean ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠪᠠᠢᠷᠢ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠮᠠᠭᠠᠳᠯᠠᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ ᠳ᠋ᠠᠩ ᠲᠤᠭᠠᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠢ᠋ ᠦᠨᠡᠯᠡᠭᠰᠡᠨ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qncnameconstructor_p.h" line="+113"/>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, is %2 invalid.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠵᠠᠷᠯᠢᠭ ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳᠤᠮᠳᠠᠬᠢ ᠭᠠᠷᠴᠠᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ ᠶᠠᠮᠠᠷᠪᠠ ᠳᠤᠮᠤ ᠵᠢᠵᠢᠭ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠬᠤᠯᠢᠯᠳᠤᠭᠰᠠᠨ %1 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃ ᠡᠢᠮᠤ ᠡᠴᠡ᠂ %2 ᠨᠢ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠵᠠᠷᠯᠢᠭ ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳᠤᠮᠳᠠᠬᠢ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ ᠭᠠᠷᠴᠠᠭ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠢᠰᠢ᠃ ᠲᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ %2 ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ ᠵᠢᠰᠢᠶ᠎ᠠ ᠨᠢ %3.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qpath.cpp" line="+109"/>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠵᠢᠮ ᠳ᠋ᠤ᠌ ᠬᠢ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ ᠨᠢᠭᠡ ᠠᠯᠬᠤᠮ ᠡᠷᠬᠡᠪᠰᠢ ᠴᠡᠭ ᠬᠢᠬᠡᠳ ᠠᠲ᠋ᠤᠮ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠠᠭᠳᠠᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ ᠲᠡᠷᠡ ᠨᠢ ᠬᠤᠶᠠᠭᠤᠯᠠ ᠵᠢᠨ ᠨᠢᠭᠡᠨ ᠬᠠᠮᠳᠤᠯᠢᠭ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qprocessinginstructionconstructor.cpp" line="+84"/>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠳ᠋ᠤ᠌ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠠᠭᠤᠯᠠᠭᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor.cpp" line="+82"/>
        <source>No namespace binding exists for the prefix %1</source>
        <translation>ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠪᠢ ᠳ᠋ᠤ᠌᠂ ᠤᠶᠠᠭᠰᠠᠨ ᠨᠡᠷᠡᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor_p.h" line="+156"/>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation>%2 ᠳᠤᠮᠳᠠᠬᠢ ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠪᠢ ᠳ᠋ᠤ᠌᠂ ᠤᠶᠠᠭᠰᠠᠨ ᠨᠡᠷᠡᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+69"/>
        <source>%1 is an invalid %2</source>
        <translation>%1 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ %2</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/xmlpatterns/functions/qabstractfunctionfactory.cpp" line="+77"/>
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1 ᠨᠢ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ %n ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠲᠠᠢ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ %2 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ.</numerusform>
            <numerusform>%1 ᠨᠢ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ %n ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠤ᠋ᠳ ᠲᠠᠢ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ %2 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>%1 ᠨᠢ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ %n ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ %2 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ.</numerusform>
            <numerusform>%1 ᠨᠢ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ %n ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠤ᠋ᠳ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ %2 ᠨᠢ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qaggregatefns.cpp" line="+120"/>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation>%1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠫᠠᠷᠢᠨᠲᠸᠷ ᠨᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %2 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃ ᠲᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠭ᠎ᠠ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ xs:yearMonthDuration ᠡᠰᠡᠬᠦᠯ᠎ᠡ xs:dayTimeDuration.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1ᠤ᠋ᠨ/ ᠵᠢᠨ ᠨᠢᠭᠡᠳᠦᠬᠡᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %2 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ. ᠳᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %3, %4, ᠡᠰᠡᠪᠡᠯ %5 ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>%1ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %2 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ. ᠳᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %3, %4, ᠡᠰᠡᠪᠡᠯ %5 ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qassemblestringfns.cpp" line="+88"/>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation>%1 ᠨᠢ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ XML 1.0 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcomparingaggregator.cpp" line="+197"/>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation>%1ᠤ᠋ᠨ/ ᠵᠢᠨ ᠨᠢᠭᠡᠳᠦᠬᠡᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %2 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qdatetimefn.cpp" line="+86"/>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation>ᠬᠡᠷᠪᠡ ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠪᠦᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠰᠢᠯᠵᠢᠯᠳᠡ ᠲᠠᠢ ᠪᠤᠯ (zone offset)᠂ ᠲᠡᠳᠡᠭᠡᠷ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠳᠠᠯᠢ ᠨᠢᠭᠡᠨ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠰᠢᠯᠵᠢᠯᠳᠡ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ %1 ᠪᠤᠯᠤᠨ %2 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠰᠢᠯᠵᠢᠯᠳᠡ ᠨᠢ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qerrorfn.cpp" line="+61"/>
        <source>%1 was called.</source>
        <translation>%1 ᠨᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠪᠡ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternmatchingfns.cpp" line="+94"/>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation>%1 ᠨᠢ %2 ᠪᠤᠶᠤ %3 ᠲᠤ᠌\ ᠳ᠋ᠤ᠌ ᠳᠠᠭᠠᠭᠳᠠᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠨ ᠤ᠋ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠰᠣᠯᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation>ᠡᠨᠡᠬᠦ ᠰᠤᠯᠢᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠨ ᠳ᠋ᠤ᠌᠂ %1 ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠭᠳᠠᠬᠤ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠲᠤᠭ᠎ᠠ ᠳᠠᠭᠠᠯᠳᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation>ᠡᠨᠡᠬᠦ ᠰᠤᠯᠢᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶᠡᠨ ᠳ᠋ᠤ᠌᠂ %1 ᠵᠦᠪᠬᠡᠨ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠭᠳᠠᠭᠰᠠᠨ ᠲᠤᠰ ᠪᠡᠶ᠎ᠡ ᠪᠤᠶᠤ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠨ᠎ᠡ᠂ ᠬᠠᠷᠢᠨ %3 ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternplatform.cpp" line="+92"/>
        <source>%1 matches newline characters</source>
        <translation>%1 ᠨᠢ ᠮᠥᠷ ᠰᠣᠯᠢᠬᠤ ᠳᠡᠮᠳᠡᠭ ᠲᠠᠢ ᠠᠪᠴᠠᠯᠳᠤᠪᠠ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation>%1 ᠪᠤᠯᠤᠨ %2 ᠨᠢ ᠨᠢᠭᠡ ᠮᠥᠷ ᠤ᠋ᠨ ᠳᠤᠯᠤᠭᠠᠢ᠂ ᠰᠡᠬᠦᠯ ᠲᠠᠢ ᠠᠪᠴᠠᠯᠳᠤᠪᠠ.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Matches are case insensitive</source>
        <translation>ᠠᠪᠴᠠᠯᠳᠤᠯ ᠨᠢ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠢᠴᠢᠯᠬᠡ ᠵᠢᠨ ᠮᠡᠳᠡᠷᠡᠮᠡᠳᠡᠬᠡᠢ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠭᠳᠡᠪᠡ᠂ ᠳᠡᠳᠡᠨᠡᠷ ᠨᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠬᠦ ᠡᠴᠡ ᠭᠠᠳᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation>%1 ᠨᠢ ᠵᠦᠪᠬᠡᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠬᠢ ᠨᠢᠭᠡᠨ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠵᠠᠭᠪᠤᠷ: %2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation>%1 ᠨᠢ ᠵᠦᠪᠬᠡᠨ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠬᠢ ᠨᠢᠭᠡᠨ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠳᠡᠮᠳᠡᠭ᠃ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ ᠳᠡᠮᠳᠡᠭ ᠨᠢ:</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+17"/>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation>ᠬᠡᠷᠪᠡ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠨᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠡᠰᠡᠪᠡᠯ ᠲᠡᠭ ᠤᠷᠳᠤ ᠲᠠᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ( ᠨᠡᠷᠡᠢᠳᠦᠭᠰᠡᠨ ᠦᠬᠡᠢ ᠤᠷᠤᠨ ᠵᠠᠢ) ᠪᠤᠯ᠂ ᠤᠭᠳᠤᠪᠤᠷᠢ ᠵᠢᠨ ᠳᠤᠭᠳᠠᠭᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃ ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠭᠳᠠᠭᠠᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencefns.cpp" line="+347"/>
        <source>It will not be possible to retrieve %1.</source>
        <translation>%1 ᠢ᠋ \ ᠵᠢ ᠣᠯᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcontextnodechecker.cpp" line="+54"/>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation>ᠹᠦᠩᠽᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠤ᠋ᠨ ᠦᠨᠳᠦᠰᠦᠨ ᠴᠡᠭ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡ ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠴᠡᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ %2 ᠨᠢ ᠨᠢᠭᠡᠨ ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠴᠡᠭ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencegeneratingfns.cpp" line="+279"/>
        <source>The default collection is undefined</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠴᠤᠭᠯᠠᠭᠤᠯᠭ᠎ᠠ (collection) ᠨᠢ ᠳᠤᠭᠳᠠᠭᠠᠭᠳᠠᠭ᠎ᠠ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be retrieved</source>
        <translation>%1 ᠢ᠋ \ ᠵᠢ ᠣᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qstringvaluefns.cpp" line="+252"/>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation>ᠳᠦᠷᠢᠮᠵᠢᠭᠰᠡᠨ (no normalization) ᠹᠤᠤᠮ %1 ᠢ᠋/ ᠵᠢᠨ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ. ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠹᠤᠤᠮ ᠨᠢ %2, %3, %4, ᠪᠤᠯᠤᠨ %5᠂ ᠵᠢᠴᠢ ᠦᠬᠡᠢ᠂ ᠵᠢᠰᠢᠶ᠎ᠠ ᠨᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ( ᠳᠦᠷᠢᠮᠵᠢᠭᠰᠡᠨ ᠦᠬᠡᠢ).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qtimezonefns.cpp" line="+87"/>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation>ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠰᠢᠯᠵᠢᠯᠳᠡ (zone offset) ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ %1..%2 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ %3 ᠨᠢ ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠡᠪᠴᠢᠶ᠎ᠡ ᠡᠴᠡ ᠭᠠᠳᠠᠨ᠎ᠠ ᠪᠤᠢ.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 is not a whole number of minutes.</source>
        <translation>%1 ᠨᠢ ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠨ ᠪᠦᠳᠦᠨ ᠲᠣᠭ᠎ᠠ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qcardinalityverifier.cpp" line="+58"/>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation>ᠬᠡᠷᠡᠭᠰᠡᠬᠦ ᠬᠦᠰᠦᠨᠦᠭ ᠤ᠋ᠨ ᠵᠠᠢ ᠵᠢᠨ ᠬᠠᠷᠢᠴᠠᠭ᠎ᠠ ᠨᠢ %1; ᠬᠠᠷᠢᠨ ᠤᠯᠤᠭᠰᠠᠨ ᠬᠦᠰᠦᠨᠦᠭ ᠤ᠋ᠨ ᠵᠠᠢ ᠵᠢᠨ ᠬᠠᠷᠢᠴᠠᠭ᠎ᠠ ᠨᠢ %2.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qitemverifier.cpp" line="+67"/>
        <source>The item %1 did not match the required type %2.</source>
        <translation>%1 ᠳᠦᠷᠦᠯ ᠪᠤᠯᠤᠨ ᠱᠠᠭᠠᠷᠳᠠᠭᠳᠠᠬᠤ %2 ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+379"/>
        <location line="+7253"/>
        <source>%1 is an unknown schema type.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠳᠦᠰᠦᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ.</translation>
    </message>
    <message>
        <location line="-6971"/>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation>ᠦᠭᠡᠷ᠎ᠡ ᠨᠢᠭᠡ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠨᠢ ᠠᠰᠠᠭᠤᠨ ᠯᠠᠪᠯᠠᠬᠤ ᠤᠳᠤᠷᠢᠳᠬᠠᠯ ᠦᠬᠡᠨ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>The initialization of variable %1 depends on itself</source>
        <translation>ᠬᠤᠪᠢᠰᠤᠭᠴᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠤᠯᠳᠠ ᠨᠢ ᠳᠡᠬᠦᠨ ᠤ᠋ ᠲᠤᠰ ᠪᠡᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠳᠦᠰᠢᠭᠯᠡᠯᠴᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>No variable by name %1 exists</source>
        <translation>%1 ᠨᠡᠷᠡᠢᠳᠦᠯ ᠲᠠᠢ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qparsercontext.cpp" line="+93"/>
        <source>The variable %1 is unused</source>
        <translation>ᠬᠤᠪᠢᠰᠤᠭᠴᠢ %1 ᠨᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠭᠰᠡᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+2841"/>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠥᠬᠡᠢ ᠬᠡᠪᠯᠡᠯ %1᠂ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ XQuery ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠪᠯᠡᠯ ᠨᠢ 1.0.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation>ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠠᠷᠭ᠎ᠠ %1 ᠨᠢ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ᠃ ᠲᠡᠷᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠵᠦᠪᠬᠡᠨ ᠯᠠᠲ᠋ᠢᠨ ᠦᠰᠦᠭ ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ᠂ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠠᠭᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠢᠬᠤ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠡᠷᠬᠡᠪᠰᠢ ᠵᠦᠪ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ %2 ᠲᠠᠢ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>No function with signature %1 is available</source>
        <translation>ᠭᠠᠷᠤᠢ ᠤ᠋ᠨ ᠦᠰᠦᠭ ᠵᠢᠷᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠹᠦᠩᠽᠢ</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+10"/>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠹᠦᠩᠽᠢ᠂ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠪᠠ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠡᠮᠦᠨ᠎ᠡ ᠢᠯᠡᠷᠡᠭᠰᠡᠨ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation>ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠹᠦᠩᠽᠢ᠂ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠪᠠ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠡᠮᠦᠨ᠎ᠡ ᠢᠯᠡᠷᠡᠭᠰᠡᠨ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation>ᠮᠤᠳᠦ᠋ᠯ ᠤ᠋ᠨ ᠤᠷᠤᠭᠤᠯᠤᠯᠳᠠ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠹᠦᠩᠽᠢ᠂ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠪᠠ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠡᠮᠦᠨ᠎ᠡ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation>ᠳᠠᠪᠬᠤᠷ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ ᠤᠭᠳᠤᠪᠤᠷᠢ %1.</translation>
    </message>
    <message>
        <source>Only the prefix %1 can be declared to bind the namespace %2. By default, it is already bound to the prefix %1.</source>
        <translation type="obsolete">至于前缀 %1 可以被声明为和命名空间 %2 绑定。默认情况下，它已经被绑定到前缀 %1。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation>ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠣᠷᠣᠰᠢᠯ ᠳ᠋ᠤ᠌ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation>ᠨᠢᠭᠡ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠤᠭᠳᠤᠪᠤᠷᠢ ᠲᠠᠢ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠳ᠋ᠤ᠌ ᠠᠶᠠᠳᠠᠯ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+171"/>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation>ᠳᠦᠰᠦᠯ (Schema Import) ᠤᠨᠴᠠᠭᠠᠢ ᠴᠢᠨᠠᠷ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ %1 ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation>%1 ᠤ᠋ᠨ \ ᠵᠢᠨ ᠬᠠᠷᠠᠯᠳᠠ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠵᠠᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The module import feature is not supported</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠥᠬᠡᠢ ᠮᠤᠳᠦ᠋ᠯ ᠣᠨᠴᠠᠭᠠᠢ ᠴᠢᠨᠠᠷ ᠢ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>A variable by name %1 has already been declared in the prolog.</source>
        <translation type="obsolete">名称为 %1 的变量已经在序言中声明过了。</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>No value is available for the external variable by name %1.</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ %1 ᠪᠤᠯᠬᠤ ᠭᠠᠳᠠᠷ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠨᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty(try the predefined prefix %1 which exists for cases like this)</source>
        <translation type="obsolete">用户定义的函数的命名空间不能为空(请试试预定义的前缀 %1，它就是用于这种情况的)。</translation>
    </message>
    <message>
        <location line="-4154"/>
        <source>A construct was encountered which only is allowed in XQuery.</source>
        <translation>ᠵᠦᠪᠬᠡᠨ XQuery ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠬᠦ ᠴᠤᠭᠴᠠᠯᠠᠪᠤᠷᠢ ᠵᠢᠨ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠲᠠᠢ ᠳᠠᠭᠠᠷᠠᠯᠳᠤᠪᠠ.</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>A template by name %1 has already been declared.</source>
        <translation>ᠬᠡᠪ %1 ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location line="+3581"/>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation>ᠠᠯᠢᠪᠠ ᠪᠤᠰᠤᠳ ᠵᠠᠭᠪᠤᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠳ᠋ᠤ᠌ ᠵᠠᠩᠬᠢᠯᠠᠭ᠎ᠠ ᠦᠬᠡᠢ %1 ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The value of attribute %1 must of type %2, which %3 isn&apos;t.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ %2 ᠪᠠᠢᠬᠤ᠂ᠬᠡᠪᠡᠴᠤ %3 ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>The prefix %1 can not be bound. By default, it is already bound to the namespace %2.</source>
        <translation>ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠢ᠋/ ᠵᠢᠨ ᠤᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃ ᠠᠶᠠᠳᠠᠯ᠂ ᠲᠡᠷᠡ ᠨᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠤᠷᠤᠨ ᠵᠠᠢ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠤᠶᠠᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location line="+312"/>
        <source>A variable by name %1 has already been declared.</source>
        <translation>ᠬᠤᠪᠢᠰᠤᠭᠴᠢ%1 ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation>ᠶᠠᠩᠵᠤ ᠵᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ ᠵᠢᠨ ᠹᠦᠩᠽᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡ ᠤᠭᠳᠤᠪᠤᠷᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠲᠠᠢ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠹᠦᠩᠽᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ( ᠡᠢᠮᠦᠷᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠡᠴᠡ ᠪᠤᠯᠵᠤ ᠤᠷᠤᠰᠢᠬᠤ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠤᠷᠤᠰᠢᠨ᠎ᠠ)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation>ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ %1 ᠢ᠋/ ᠵᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠦᠯᠡᠳᠡᠭᠡᠭᠰᠡᠨ; ᠡᠢᠮᠤ ᠡᠴᠡ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠹᠦᠩᠽᠢ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠤᠭᠳᠤᠪᠤᠷᠢ %2 ᠢ᠋/ ᠵᠢ ᠳᠤᠷᠰᠢᠵᠤ ᠦᠵᠡᠬᠡᠷᠡᠢ᠂ ᠲᠡᠷᠡ ᠨᠢ ᠡᠢᠮᠦᠷᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠳᠡᠭ.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠢᠭᠡᠨ ᠬᠦᠮᠦᠷᠭᠡ ᠵᠢᠨ ᠮᠤᠳᠦ᠋ᠯ ᠳ᠋ᠤ᠌ ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠹᠦᠩᠽᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠰ ᠮᠤᠳᠦ᠋ᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠲᠠᠢ ᠢᠵᠢᠯ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ ᠳᠠᠷᠤᠢ᠂ ᠲᠡᠷᠡ ᠨᠢ %1 ᠪᠠᠢᠬᠤ ᠶᠤᠰᠤᠳᠠᠢ᠂ ᠬᠠᠷᠢᠨ %2 ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>A function already exists with the signature %1.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠭᠠᠷ ᠤ᠋ᠨ ᠦᠰᠦᠭ ᠲᠠᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠹᠦᠩᠽᠢ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation>ᠭᠠᠳᠠᠷ ᠹᠦᠩᠽᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ᠃ ᠪᠦᠬᠦᠢᠯᠡ ᠳᠡᠮᠵᠢᠬᠦ ᠹᠦᠩᠽᠢ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠰᠢᠭ᠋ᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠨ᠎ᠡ᠂ ᠳᠡᠳᠡᠨᠡᠷ ᠢ᠋ ᠭᠠᠳᠠᠷ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠬᠡᠵᠤ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>An argument by name %1 has already been declared. Every argument name must be unique.</source>
        <translation>%1 ᠨᠡᠷᠡᠢᠳᠦᠯ ᠲᠠᠢ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠳᠠᠪᠠ᠃ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠪᠦᠷᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠡᠷᠬᠡᠪᠰᠢ ᠴᠤᠷᠢ ᠵᠢᠨ ᠭᠠᠭᠴᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation>ᠹᠦᠩᠽᠢ %1 ᠨᠢ ᠶᠠᠩᠵᠤ ᠵᠢᠨ ᠠᠪᠴᠠᠯᠳᠤᠯ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠵᠢᠨ ᠯᠠᠪᠯᠠᠬᠤ ᠪᠠ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation>XSL-T ᠶᠠᠩᠵᠤ ᠳ᠋ᠤ᠌ ᠹᠦᠩᠽᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠨᠢᠭᠡᠳᠦᠬᠡᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠡᠷᠬᠡᠪᠰᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠪᠠᠢᠵᠤ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation>XSL-T ᠶᠠᠩᠵᠤ ᠳ᠋ᠤ᠌ ᠹᠦᠩᠽᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠨᠢᠭᠡᠳᠦᠬᠡᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠡᠷᠬᠡᠪᠰᠢ ᠦᠰᠦᠭ ᠪᠤᠶᠤ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠵᠢᠨ ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation>XSL-T ᠶᠠᠩᠵᠤ ᠳ᠋ᠤ᠌ ᠹᠦᠩᠽᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation>XSL-T ᠶᠠᠩᠵᠤ ᠳ᠋ᠤ᠌ ᠵᠦᠪᠬᠡᠨ ᠹᠦᠩᠽᠢ %1 ᠪᠤᠯᠤᠨ %2 ᠢ᠋/ ᠵᠢ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ %3 ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation>XSL-T ᠳᠡᠭᠡᠭᠰᠢ ᠬᠠᠷᠠᠬᠤ ᠳ᠋ᠤ᠌ %1 ᠳᠡᠩᠭᠡᠯᠢᠭ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠵᠦᠪᠬᠡᠨ ᠳᠡᠩᠭᠡᠯᠢᠭ %2 ᠪᠤᠶᠤ %3 ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 is an invalid template mode name.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠨ ᠤ᠋ ᠬᠡᠪ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation>for ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠤᠶᠠᠭᠰᠠᠨ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠲᠤᠰ ᠪᠠᠢᠷᠢᠨ ᠤ᠋ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠲᠠᠢ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠃ ᠡᠢᠮᠤ ᠡᠴᠡ᠂ ᠡᠨᠡ ᠬᠤᠶᠠᠷ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠪᠢᠰᠤᠭᠴᠢ ᠲᠠᠢ ᠵᠦᠷᠢᠴᠡᠯᠳᠦᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+758"/>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation>ᠳᠦᠰᠦᠯ ᠤ᠋ᠨ ᠪᠠᠳᠤᠯᠠᠭᠠᠵᠢᠭᠤᠯᠬᠤ ᠤᠨᠴᠠᠭᠠᠢ ᠴᠢᠨᠠᠷ (Schema Validation Feature) ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ᠃ ᠡᠢᠮᠤ ᠡᠴᠡ᠂ %1 ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠮᠠᠭᠠᠳ.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation>ᠶᠠᠮᠠᠷᠪᠠ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠨ ᠤᠷᠴᠢᠭᠤᠯᠵᠤ ᠵᠢᠭᠠᠪᠤᠷᠢᠯᠠᠭᠰᠠᠨ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ (pragma expressions)᠃ ᠡᠢᠮᠤ ᠡᠴᠡ᠂ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠬᠡᠷᠡᠭᠳᠡᠢ (fallback expression)</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation>ᠬᠡᠪ ᠪᠦᠷᠢ ᠵᠢᠨ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠪᠦᠷᠢ ᠴᠤᠷᠢ ᠵᠢᠨ ᠭᠠᠭᠴᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ; %1 ᠳᠠᠪᠬᠤᠴᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation>ᠡᠨᠡ %1 ᠳᠡᠩᠭᠡᠯᠢᠭ ᠨᠢ XQuery ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+1150"/>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠬᠠᠤᠯᠢ ᠶᠣᠰᠣᠨ ᠤ᠋ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="-7029"/>
        <source>%1 is not a valid numeric literal.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ ᠲᠣᠭᠠᠨ ᠤ᠋ ᠤᠳᠬ᠎ᠠ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+6165"/>
        <source>No function by name %1 is available.</source>
        <translation>%1 ᠨᠡᠷᠡᠢᠳᠦᠯ ᠲᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠹᠦᠩᠼ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation>ᠡᠨᠡᠬᠦ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ URI ᠨᠢ ᠨᠢᠭᠡ ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠤᠶᠠᠭᠳᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠳᠡᠷᠡ ᠨᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 is an invalid namespace URI.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠣᠷᠣᠨ ᠵᠠᠢ URI.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>It is not possible to bind to the prefix %1</source>
        <translation>ᠡᠨᠡᠬᠦ ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠤᠶᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ %1 ᠨᠢ ᠵᠦᠪᠬᠡᠨ %2 ᠲᠠᠢ ᠤᠶᠠᠨ᠎ᠠ ( ᠡᠢᠮᠦᠷᠬᠦ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ).</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠨᠢ ᠵᠦᠪᠬᠡᠨ %2 ᠲᠠᠢ ᠤᠶᠠᠨ᠎ᠠ ( ᠬᠡᠷᠪᠡ ᠡᠢᠮᠤ ᠪᠠᠢᠳᠠᠯ ᠲᠠᠢ ᠪᠤᠯ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ).</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation>ᠬᠤᠶᠠᠷ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠤ᠋ᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠨᠢ ᠠᠳᠠᠯᠢ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠪᠡ: %1.</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation>ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ URI ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ ᠡᠩ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠪᠢᠳᠡᠬᠦᠮᠵᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>An attribute by name %1 has already appeared on this element.</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ %1 ᠪᠤᠯᠬᠤ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠲᠤᠰ ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠲᠤ᠌ ᠨᠢᠭᠡᠨᠳᠡ ᠢᠯᠡᠷᠡᠭᠰᠡᠨ.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠰᠢᠭ᠋ᠤᠳ ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠤ᠋ᠨ ᠴᠤᠭᠴᠠᠯᠠᠪᠤᠷᠢ ᠰᠠᠢᠨ ᠪᠦᠷᠢᠯᠳᠦᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠃ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠠᠷᠤ ᠳ᠋ᠤ᠌ %2 ᠳᠠᠭᠠᠯᠳᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>The name %1 does not refer to any schema type.</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ %1 ᠶᠠᠮᠠᠷᠪᠠ ᠳᠦᠰᠦᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠵᠢᠭᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation>%1 ᠨᠢ ᠨᠢᠭᠡᠨ ᠪᠤᠳᠤᠯᠢᠶᠠᠨᠳᠠᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ᠃ ᠪᠤᠳᠤᠯᠢᠶᠠᠨᠳᠠᠢ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠳ᠋ᠤ᠌ ᠰᠢᠳᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠃ ᠡᠢᠮᠤ ᠡᠴᠡ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠰᠢᠳᠡᠭᠰᠡᠨ ᠬᠡᠬᠦ ᠮᠡᠳᠦ ᠡᠢᠮᠦᠷᠬᠦ ᠠᠲ᠋ᠤᠮ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠪᠠᠰᠠ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation>%1 ᠠᠲ᠋ᠤᠮ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠪᠢᠰᠢ᠂ ᠵᠦᠪᠬᠡᠨ ᠰᠢᠳᠡᠭᠡᠳ ᠠᠲ᠋ᠤᠮ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠬᠡᠬᠦ.</translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction. Therefore this name test will never match.</source>
        <translation type="obsolete">%1 不是处理指令的有效名称。因此这个名称测试永远不会匹配。</translation>
    </message>
    <message>
        <location line="+145"/>
        <location line="+71"/>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation>%1 ᠨᠢ ᠬᠦᠷᠢᠶᠡᠨ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠢ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠤ᠋ᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠯᠳᠠ ᠪᠢᠰᠢ᠃ ᠳᠦᠰᠦᠯ ᠤ᠋ᠨ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠤᠨᠴᠠᠭᠠᠢ ᠴᠢᠨᠠᠷ ᠨᠢ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ ᠵᠢ ᠠᠩᠬᠠᠷᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠦᠷᠬᠡᠳᠭᠡᠭᠰᠡᠨ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠳᠤᠮᠳᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qcardinality.cpp" line="+55"/>
        <source>empty</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or one</source>
        <translation>ᠲᠡᠭ ᠪᠢᠰᠢ ᠪᠣᠯ ᠨᠢᠭᠡ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>exactly one</source>
        <translation>ᠪᠤᠳᠠᠳᠠᠢ ᠪᠡᠷ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>one or more</source>
        <translation>ᠨᠢᠭᠡ ᠪᠤᠶᠤ ᠨᠡᠩ ᠣᠯᠠᠨ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or more</source>
        <translation>ᠲᠡᠭ ᠪᠤᠶᠤ ᠨᠡᠩ ᠣᠯᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qtypechecker.cpp" line="+63"/>
        <source>Required type is %1, but %2 was found.</source>
        <translation>ᠱᠠᠭᠠᠷᠳᠠᠭᠳᠠᠬᠤ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1᠂ ᠬᠡᠪᠡᠴᠤ ᠡᠷᠢᠵᠤ ᠣᠯᠤᠭᠰᠠᠨ ᠨᠢ %2.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation>%1 ᠢ᠋ \ ᠵᠢ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠬᠦᠯᠦᠭᠡᠳ %2 ᠪᠣᠯᠭᠠᠬᠤ ᠨᠢ ᠣᠨᠣᠪᠴᠢ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠬᠣᠬᠢᠷᠠᠯ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>The focus is undefined.</source>
        <translation>ᠳᠦᠪᠯᠡᠷᠡᠯ ᠴᠡᠭ ᠢ᠋ ᠳᠤᠭᠳᠠᠭ᠎ᠠ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qoutputvalidator.cpp" line="+86"/>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation>ᠶᠠᠮᠠᠷᠪᠠ ᠪᠤᠰᠤᠳ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠤ᠋ᠨ ᠴᠡᠭ ᠤ᠋ᠨ ᠠᠷᠤ ᠳ᠋ᠤ᠌ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠨᠡᠮᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>An attribute by name %1 has already been created.</source>
        <translation>ᠨᠢᠭᠡᠨ %1 ᠨᠡᠷᠡᠢᠳᠦᠯ ᠲᠠᠢ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠭᠤᠯᠤᠭᠳᠠᠪᠠ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qxpathhelper_p.h" line="+120"/>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation>Unicode ᠺᠤᠳ ᠤ᠋ᠨ ᠴᠡᠭ ᠢ᠋ ᠬᠠᠷᠭᠤᠭᠤᠯᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ (Unicode Codepoint Collation is supported)(%1). %2 ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+60"/>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ ᠮᠦᠷ ᠰᠤᠯᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠤᠴᠢᠷ ᠨᠢ ᠲᠡᠷᠡ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠤᠷᠤᠢ ᠵᠢᠨ ᠳᠠᠪᠬᠤᠷᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreeresourceloader.cpp" line="+314"/>
        <source>%1 is an unsupported encoding.</source>
        <translation>%1 ᠨᠢ ᠳᠡᠮᠵᠢᠭᠳᠡᠬᠦ ᠥᠬᠡᠢ ᠺᠣᠳ᠋.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation>%1 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠺᠤᠳ᠋ %2 ᠢ᠋/ ᠵᠢ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ ᠳᠤᠮᠳᠠ ᠨᠠᠢ᠍ᠮᠠ ᠪᠡᠷ ᠳᠡᠪᠰᠢᠬᠦ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠢ᠋ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation>ᠺᠤᠳ᠋ %3 ᠤ᠋ᠨ/ ᠵᠢᠨ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠬᠦ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠴᠡᠭ %1 ᠨᠢ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ XML ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qapplytemplate.cpp" line="+119"/>
        <source>Ambiguous rule match.</source>
        <translation>ᠪᠠᠯᠠᠷᠬᠠᠢ ᠳᠦᠷᠢᠮ ᠠᠪᠴᠠᠯᠳᠤᠪᠠ.</translation>
    </message>
    <message>
        <source>In a namespace constructor, the value for a namespace value cannot be an empty string.</source>
        <translation type="obsolete">在一个命名空间构造中，命名空间的值不能为空字符串。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomputednamespaceconstructor.cpp" line="+69"/>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation>ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠹᠦᠩᠽᠢ ᠳ᠋ᠤ᠌᠂ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠬᠤᠭᠤᠰᠤᠨ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation>ᠤᠭᠳᠤᠪᠤᠷᠢ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠦᠴᠦᠨ ᠪᠦᠬᠦᠢ %1 ᠪᠠᠢᠵᠤ᠂ ᠬᠠᠷᠢᠨ %2 ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The prefix %1 cannot be bound.</source>
        <translation>ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠤᠶᠠᠭᠳᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠤᠭᠳᠤᠪᠤᠷᠢ %1 ᠢ᠋/ ᠵᠢ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠤᠶᠠᠬᠤ᠂ ᠡᠰᠡᠷᠬᠦ ᠪᠡᠷ ᠪᠠᠰᠠ ᠠᠳᠠᠯᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qevaluationcache.cpp" line="+117"/>
        <source>Circularity detected</source>
        <translation>ᠲᠣᠭᠣᠷᠢᠭ ᠲᠤ᠌ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠬᠦᠷᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qtemplate.cpp" line="+145"/>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation>ᠱᠠᠭᠠᠷᠳᠠᠭᠳᠠᠬᠤ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠨᠢ %1᠂ ᠬᠡᠪᠡᠴᠤ ᠬᠠᠷᠠᠭᠠᠯᠵᠠᠭᠰᠠᠨ %2 ᠬᠠᠩᠭᠠᠵᠤ ᠦᠭᠬᠦᠭᠰᠡᠨ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation>ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ %1 ᠢ᠋ \ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠮᠵᠢᠭᠤᠯᠪᠠ᠂ ᠬᠡᠪᠡᠴᠤ ᠬᠠᠷᠠᠭᠠᠯᠵᠠᠭᠰᠠᠨ %2 ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qunparsedtextfn.cpp" line="+65"/>
        <source>The URI cannot have a fragment</source>
        <translation>URI ᠬᠡᠰᠡᠭ ᠪᠠᠳᠠᠭ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+519"/>
        <source>Element %1 is not allowed at this location.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠡᠨᠡ ᠪᠠᠢᠷᠢᠯᠠᠯ ᠳ᠋ᠤ᠌ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Text nodes are not allowed at this location.</source>
        <translation>ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠴᠡᠭ ᠡᠨᠡ ᠪᠠᠢᠷᠢᠯᠠᠯ ᠳ᠋ᠤ᠌ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Parse error: %1</source>
        <translation>ᠵᠠᠳᠠᠯᠤᠯᠳᠠ ᠪᠤᠷᠤᠭᠤ: %1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation>XSL-T ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ %1 ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ᠂ ᠬᠠᠷᠢᠨ %2 ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation>XSL-T 1.0 ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ ᠲᠤ᠌ 2.0 ᠶᠠᠩᠵᠤ ᠵᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ ᠶᠠᠪᠤᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Unknown XSL-T attribute %1.</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ XSL-T ᠤ᠋ᠨ \ ᠵᠢᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠪᠤᠯᠤᠨ %2 ᠨᠢ ᠬᠠᠷᠢᠯᠴᠠᠨ ᠬᠠᠷᠰᠢᠯᠳᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation>ᠨᠢᠭᠡᠨ ᠳᠦᠬᠦᠮᠴᠢᠯᠡᠭᠰᠡᠨ ᠶᠠᠩᠵᠤ ᠵᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ ᠵᠢᠨ ᠮᠤᠳᠦ᠋ᠯ ᠳ᠋ᠤ᠌᠂ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠡᠷᠬᠡᠪᠰᠢ ᠤᠷᠤᠰᠢᠵᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation>ᠬᠡᠷᠪᠡ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %2 ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠯ᠂ ᠲᠡᠷᠡ ᠨᠢ ᠮᠦᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %3 ᠪᠤᠶᠤ %4 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %2 ᠡᠰᠡᠬᠦᠯ᠎ᠡ %3 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠤᠮᠳᠠᠬᠢ ᠨᠢᠭᠡ ᠨᠢ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %2 ᠤ᠋ᠨ/ ᠵᠢᠨ %1 ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠳᠤᠮᠳᠠ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠨᠢᠭᠡ ᠵᠠᠭᠪᠤᠷ ᠠᠪᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qmaintainingreader.cpp" line="+183"/>
        <source>Attribute %1 cannot appear on the element %2. Only the standard attributes can appear.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %2 ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠵᠦᠪᠬᠡᠨ ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠰᠠᠶᠢ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Attribute %1 cannot appear on the element %2. Only %3 is allowed, and the standard attributes.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %2 ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠵᠦᠪᠬᠡᠨ %3 ᠪᠤᠯᠤᠨ ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠰᠠᠶᠢ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, %4, and the standard attributes.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %2 ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠵᠦᠪᠬᠡᠨ %3᠂%4 ᠪᠤᠯᠤᠨ ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠰᠠᠶᠢ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, and the standard attributes.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %2 ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠵᠦᠪᠬᠡᠨ %3 ᠪᠤᠯᠤᠨ ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠳᠤ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠰᠠᠶᠢ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>XSL-T attributes on XSL-T elements must be in the null namespace, not in the XSL-T namespace which %1 is.</source>
        <translation>XSL-T ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠳ᠋ᠤ᠌ ᠬᠢ XSL-T ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠢ᠋ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠤᠭᠤᠰᠤᠨ (null) ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠳ᠋ᠤ᠌ ᠳᠠᠯᠪᠢᠨ᠎ᠠ᠂ ᠬᠠᠷᠢᠨ XSL-T ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠳ᠋ᠤ᠌ ᠪᠢᠰᠢ᠂ %1 ᠨᠢ ᠬᠠᠷᠢᠨ ᠳᠡᠢᠮᠤ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The attribute %1 must appear on element %2.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠬᠦ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The element with local name %1 does not exist in XSL-T.</source>
        <translation>ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠨᠢ XSL-T ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+123"/>
        <source>Element %1 must come last.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one %1-element must occur before %2.</source>
        <translation>ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ %2 ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠢᠯᠡᠷᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Only one %1-element can appear.</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠨᠢᠭᠡ %1 ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠢᠯᠡᠷᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>At least one %1-element must occur inside %2.</source>
        <translation>ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ %2 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠤᠳᠤᠷ᠎ᠠ ᠢᠯᠡᠷᠡᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠴᠤᠭᠴᠠᠯᠠᠪᠤᠷᠢ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠨ %2 ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠡᠰᠡᠪᠡᠯ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠴᠤᠭᠴᠠᠯᠠᠪᠤᠷᠢ ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation>ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠬᠡᠷᠡᠭ ᠲᠠᠢ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌᠂ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠪᠤᠶᠤ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠴᠤᠭᠴᠠᠯᠠᠪᠤᠷᠢ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠬᠠᠩᠭᠠᠭᠰᠠᠨ ᠠᠶᠠᠳᠠᠯ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>Element %1 cannot have children.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+434"/>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠨᠢ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠴᠤᠭᠴᠠᠯᠠᠪᠤᠷᠢ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+9"/>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠨᠢ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠤᠴᠢᠷ ᠨᠢ ᠲᠡᠷᠡ ᠪᠤᠯ %3 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation>ᠹᠦᠩᠽᠢ ᠵᠢᠨ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠳ᠋ᠤ᠌ ᠬᠢ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ ᠨᠢ ᠰᠤᠪᠠᠭ (tunnel) ᠵᠢᠡᠷ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠳᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation>ᠲᠤᠰ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ Schema ᠢ᠋/ ᠵᠢ ᠮᠡᠳᠡᠷᠡᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ %1 ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation>ᠳᠡᠷᠢᠬᠦᠨ ᠵᠡᠷᠭᠡ ᠵᠢᠨ ᠶᠠᠩᠵᠤ ᠵᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ ᠵᠢᠨ ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠡᠷᠡᠯᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠳᠤᠮᠳᠠ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ᠂ ᠬᠠᠷᠢᠨ %1 ᠨᠢ ᠳᠡᠢᠮᠤ ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation>ᠡᠯᠸᠮᠸᠨ᠋ᠲ %2 ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠢ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ %3 ᠡᠰᠡᠬᠦᠯ᠎ᠡ %4᠂ ᠬᠠᠷᠢᠨ %5 ᠪᠢᠰᠢ.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠤ᠋ᠨ \ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠨᠢ %2 ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ %1 ᠵᠦᠪᠬᠡᠨ ᠡᠮᠦᠨ᠎ᠡ %2 ᠡᠯᠸᠮᠸᠨ᠋ᠲ ᠲᠤ᠌ ᠢᠯᠡᠷᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation>%2 ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠡᠯᠸᠮᠸᠨ᠋ᠲ %1 ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+67"/>
        <source>Muted</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠭᠤᠤ ᠥᠬᠡᠢ ᠪᠣᠯᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+15"/>
        <source>Volume: %1%</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ: %1%</translation>
    </message>
</context>
<context>
    <name>WebCore::PlatformScrollbar</name>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">滚动到这里</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">左边缘</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">顶部</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">右边缘</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">底部</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">左一页</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">上一页</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">右一页</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">下一页</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">向左滚动</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">向上滚动</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">向右滚动</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">向下滚动</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <source>OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>ᠳᠡᠢᠮᠤ (&amp;Y)</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>ᠪᠥᠬᠥᠳᠡ ᠮᠥᠨ (&amp;A)</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>ᠪᠢᠰᠢ (&amp;N)</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>ᠪᠥᠬᠥᠳᠡ ᠪᠢᠰᠢ (&amp;O)</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠲᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>ᠲᠠᠶᠠᠭᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢ᠋ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <source>&amp;OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ (&amp;O)</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ (&amp;S)</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ (&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ (&amp;C)</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>ᠪᠤᠴᠠᠬᠤ (&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ (&amp;R)</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ (&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ (&amp;C)</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠬᠤ (&amp;L)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ (&amp;P)</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>Printer Properties</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Job Options</source>
        <translation>ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <source>Page Setup Conflicts</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠤᠭᠳᠠᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ</translation>
    </message>
    <message>
        <source>There are conflicts in page setup options. Do you want to fix them?</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠮᠦᠷᠬᠦᠯᠳᠦᠬᠡᠨ ᠲᠠᠢ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠢ᠋ ᠤᠯᠵᠤ ᠢᠯᠡᠷᠬᠦᠯᠪᠡ᠂ ᠵᠠᠰᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Advanced Option Conflicts</source>
        <translation>ᠳᠡᠷᠢᠬᠦᠨ ᠤ᠋ ᠳᠠᠪᠬᠤᠷᠭ᠎ᠠ ᠵᠢᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ ᠮᠦᠷᠬᠦᠯᠳᠦᠭᠡᠨ</translation>
    </message>
    <message>
        <source>There are conflicts in some advanced options. Do you want to fix them?</source>
        <translation>ᠵᠠᠷᠢᠮ ᠳᠡᠷᠢᠬᠦᠨ ᠤ᠋ ᠳᠠᠪᠬᠤᠷᠭ᠎ᠠ ᠵᠢᠨ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠳ᠋ᠤ᠌ ᠮᠦᠷᠬᠦᠯᠳᠦᠭᠡᠨ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠠᠰᠠᠬᠤ ᠤᠤ?</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>Left to Right, Top to Bottom</source>
        <translation>ᠵᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ᠂ ᠳᠡᠭᠡᠷ᠎ᠡ ᠡᠴᠡ ᠳᠣᠷᠣᠭᠰᠢ</translation>
    </message>
    <message>
        <source>Left to Right, Bottom to Top</source>
        <translation>ᠵᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ᠂ ᠳᠤᠤᠷ᠎ᠡ ᠡᠴᠡ ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <source>Right to Left, Bottom to Top</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠡᠴᠡ ᠵᠡᠬᠦᠨᠰᠢ᠂ ᠳᠤᠤᠷ᠎ᠡ ᠡᠴᠡ ᠳᠡᠭᠡᠭᠰᠢ</translation>
    </message>
    <message>
        <source>Right to Left, Top to Bottom</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠡᠴᠡ ᠵᠡᠬᠦᠨᠰᠢ᠂ ᠳᠡᠭᠡᠷ᠎ᠡ ᠡᠴᠡ ᠳᠣᠷᠣᠭᠰᠢ</translation>
    </message>
    <message>
        <source>Bottom to Top, Left to Right</source>
        <translation>ᠳᠤᠤᠷ᠎ᠡ ᠡᠴᠡ ᠳᠡᠭᠡᠭᠰᠢ᠂ ᠵᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ</translation>
    </message>
    <message>
        <source>Bottom to Top, Right to Left</source>
        <translation>ᠳᠤᠤᠷ᠎ᠡ ᠡᠴᠡ ᠳᠡᠭᠡᠭᠰᠢ᠂ ᠪᠠᠷᠠᠭᠤᠨ ᠡᠴᠡ ᠵᠡᠬᠦᠨᠰᠢ</translation>
    </message>
    <message>
        <source>Top to Bottom, Left to Right</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠡᠴᠡ ᠳᠣᠷᠣᠭᠰᠢ᠂ ᠵᠡᠬᠦᠨ ᠡᠴᠡ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ</translation>
    </message>
    <message>
        <source>Top to Bottom, Right to Left</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠡᠴᠡ ᠳᠣᠷᠣᠭᠰᠢ᠂ ᠪᠠᠷᠠᠭᠤᠨ ᠡᠴᠡ ᠵᠡᠬᠦᠨᠰᠢ</translation>
    </message>
    <message>
        <source>1 (1x1)</source>
        <translation>1 (1x1)</translation>
    </message>
    <message>
        <source>2 (2x1)</source>
        <translation>2 (2x1)</translation>
    </message>
    <message>
        <source>4 (2x2)</source>
        <translation>4 (2x2)</translation>
    </message>
    <message>
        <source>6 (2x3)</source>
        <translation>6 (2x3)</translation>
    </message>
    <message>
        <source>9 (3x3)</source>
        <translation>9 (3x3)</translation>
    </message>
    <message>
        <source>16 (4x4)</source>
        <translation>16 (4x4)</translation>
    </message>
    <message>
        <source>All Pages</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠨᠢᠭᠤᠷ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Odd Pages</source>
        <translation>ᠰᠤᠨᠳᠤᠭᠤᠢ ᠲᠣᠭᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <source>Even Pages</source>
        <translation>ᠲᠡᠭᠰᠢ ᠲᠣᠭᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>QPageSize</name>
    <message>
        <source>A0</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 9</translation>
    </message>
    <message>
        <source>A10</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ 10</translation>
    </message>
    <message>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 in)</source>
        <translation>Executive ᠴᠠᠭᠠᠰᠤ (7.5 x 10 in)</translation>
    </message>
    <message>
        <source>Executive (7.25 x 10.5 in)</source>
        <translation>Executive ᠴᠠᠭᠠᠰᠤ (7.25 x 10.5 in)</translation>
    </message>
    <message>
        <source>Folio (8.27 x 13 in)</source>
        <translation>Folio ᠴᠠᠭᠠᠰᠤ (8.27 x 13 in)</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation>ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠵᠢᠨ ᠳᠤᠰᠬᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Letter / ANSI A</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ / ANSI A</translation>
    </message>
    <message>
        <source>Tabloid / ANSI B</source>
        <translation>ᠵᠢᠵᠢᠭ ᠰᠣᠨᠢᠨ / ANSI B</translation>
    </message>
    <message>
        <source>Ledger / ANSI B</source>
        <translation>ᠳᠦᠷᠦᠯᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ ᠳᠠᠩᠰᠠ / ANSI B</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>A3 Extra</source>
        <translation>A3 ᠶᠡᠬᠡᠰᠬᠡᠭᠰᠡᠨ</translation>
    </message>
    <message>
        <source>A4 Extra</source>
        <translation>A4 ᠶᠡᠬᠡᠰᠬᠡᠭᠰᠡᠨ</translation>
    </message>
    <message>
        <source>A4 Plus</source>
        <translation>A4 ᠣᠨᠴᠠ ᠲᠣᠮᠣ</translation>
    </message>
    <message>
        <source>A4 Small</source>
        <translation>A4 ᠵᠢᠵᠢᠭ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <source>A5 Extra</source>
        <translation>A5 ᠶᠡᠬᠡᠰᠬᠡᠭᠰᠡᠨ</translation>
    </message>
    <message>
        <source>B5 Extra</source>
        <translation>B5 ᠶᠡᠬᠡᠰᠬᠡᠭᠰᠡᠨ</translation>
    </message>
    <message>
        <source>JIS B0</source>
        <translation>B0 (JIS)</translation>
    </message>
    <message>
        <source>JIS B1</source>
        <translation>B1 (JIS)</translation>
    </message>
    <message>
        <source>JIS B2</source>
        <translation>B2 (JIS)</translation>
    </message>
    <message>
        <source>JIS B3</source>
        <translation>B3 (JIS)</translation>
    </message>
    <message>
        <source>JIS B4</source>
        <translation>B4 (JIS)</translation>
    </message>
    <message>
        <source>JIS B5</source>
        <translation>B5 (JIS)</translation>
    </message>
    <message>
        <source>JIS B6</source>
        <translation>B6 (JIS)</translation>
    </message>
    <message>
        <source>JIS B7</source>
        <translation>B7 (JIS)</translation>
    </message>
    <message>
        <source>JIS B8</source>
        <translation>B8 (JIS)</translation>
    </message>
    <message>
        <source>JIS B9</source>
        <translation>B9 (JIS)</translation>
    </message>
    <message>
        <source>JIS B10</source>
        <translation>B10 (JIS)</translation>
    </message>
    <message>
        <source>ANSI C</source>
        <translation>ansic</translation>
    </message>
    <message>
        <source>ANSI D</source>
        <translation>ansid</translation>
    </message>
    <message>
        <source>ANSI E</source>
        <translation>ansie</translation>
    </message>
    <message>
        <source>Legal Extra</source>
        <translation>ᠬᠠᠤᠯᠢ ᠴᠠᠭᠠᠵᠠ ᠵᠢᠨ ᠳᠤᠰᠬᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠴᠠᠭᠠᠰᠤ᠂ ᠶᠡᠬᠡᠰᠬᠡᠭᠰᠡᠨ</translation>
    </message>
    <message>
        <source>Letter Extra</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ᠂ ᠶᠡᠬᠡᠰᠬᠡᠭᠰᠡᠨ</translation>
    </message>
    <message>
        <source>Letter Plus</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ ᠣᠨᠴᠠ ᠲᠣᠮᠣ</translation>
    </message>
    <message>
        <source>Letter Small</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ ᠵᠢᠵᠢᠭ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <source>Tabloid Extra</source>
        <translation>ᠵᠢᠵᠢᠭ ᠰᠣᠨᠢᠨ ᠶᠡᠬᠡᠰᠬᠡᠭᠰᠡᠨ</translation>
    </message>
    <message>
        <source>Architect A</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ A</translation>
    </message>
    <message>
        <source>Architect B</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ B</translation>
    </message>
    <message>
        <source>Architect C</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ C</translation>
    </message>
    <message>
        <source>Architect D</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ D</translation>
    </message>
    <message>
        <source>Architect E</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ E</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>ᠲᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <source>Quarto</source>
        <translation>Quarto ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Statement</source>
        <translation>Statement ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Super A</source>
        <translation>super A</translation>
    </message>
    <message>
        <source>Super B</source>
        <translation>super B</translation>
    </message>
    <message>
        <source>Postcard</source>
        <translation>ᠢᠯᠡ ᠵᠠᠬᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <source>Double Postcard</source>
        <translation>ᠬᠤᠤᠰ ᠢᠯᠡ ᠵᠠᠬᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <source>PRC 16K</source>
        <translation>PRC 16K</translation>
    </message>
    <message>
        <source>PRC 32K</source>
        <translation>PRC 32K</translation>
    </message>
    <message>
        <source>PRC 32K Big</source>
        <translation>PRC 32K(Big)</translation>
    </message>
    <message>
        <source>Fan-fold US (14.875 x 11 in)</source>
        <translation>ᠠᠮᠧᠷᠢᠺᠡFan-fold ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Fan-fold German (8.5 x 12 in)</source>
        <translation>ᠭᠧᠷᠮᠡᠨFan-fold ᠴᠠᠭᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Fan-fold German Legal (8.5 x 13 in)</source>
        <translation>ᠭᠧᠷᠮᠡᠨ ᠦ ᠬᠠᠤᠯᠢ ᠳᠤ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠳᠤᠰᠬᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠦ ᠴᠠᠭᠠᠰᠤFan-fold</translation>
    </message>
    <message>
        <source>Envelope B4</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢB4</translation>
    </message>
    <message>
        <source>Envelope B5</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢB5</translation>
    </message>
    <message>
        <source>Envelope B6</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢB6</translation>
    </message>
    <message>
        <source>Envelope C0</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC0</translation>
    </message>
    <message>
        <source>Envelope C1</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC1</translation>
    </message>
    <message>
        <source>Envelope C2</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC2</translation>
    </message>
    <message>
        <source>Envelope C3</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC3</translation>
    </message>
    <message>
        <source>Envelope C4</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC4</translation>
    </message>
    <message>
        <source>Envelope C5</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC5</translation>
    </message>
    <message>
        <source>Envelope C6</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC6</translation>
    </message>
    <message>
        <source>Envelope C65</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC65</translation>
    </message>
    <message>
        <source>Envelope C7</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢC7</translation>
    </message>
    <message>
        <source>Envelope DL</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢDL</translation>
    </message>
    <message>
        <source>Envelope US 9</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢUS 9</translation>
    </message>
    <message>
        <source>Envelope US 10</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢUS 10</translation>
    </message>
    <message>
        <source>Envelope US 11</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢUS 11</translation>
    </message>
    <message>
        <source>Envelope US 12</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢUS 12</translation>
    </message>
    <message>
        <source>Envelope US 14</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢUS 14</translation>
    </message>
    <message>
        <source>Envelope Monarch</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢMonarch</translation>
    </message>
    <message>
        <source>Envelope Personal</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPersonal</translation>
    </message>
    <message>
        <source>Envelope Chou 3</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢChou 3</translation>
    </message>
    <message>
        <source>Envelope Chou 4</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢChou 4</translation>
    </message>
    <message>
        <source>Envelope Invite</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠤᠭᠤᠲᠠ ᠤᠷᠢᠯᠭ᠎ᠠ ᠵᠢᠨ ᠪᠢᠴᠢᠭ</translation>
    </message>
    <message>
        <source>Envelope Italian</source>
        <translation>ᠢᠲ᠋ᠠᠯᠢ ᠮᠠᠶ᠋ᠢᠭ ᠤᠨ ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢ</translation>
    </message>
    <message>
        <source>Envelope Kaku 2</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢKaku 2</translation>
    </message>
    <message>
        <source>Envelope Kaku 3</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢKaku 3</translation>
    </message>
    <message>
        <source>Envelope PRC 1</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 1</translation>
    </message>
    <message>
        <source>Envelope PRC 2</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 2</translation>
    </message>
    <message>
        <source>Envelope PRC 3</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 3</translation>
    </message>
    <message>
        <source>Envelope PRC 4</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 4</translation>
    </message>
    <message>
        <source>Envelope PRC 5</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 5</translation>
    </message>
    <message>
        <source>Envelope PRC 6</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 6</translation>
    </message>
    <message>
        <source>Envelope PRC 7</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 7</translation>
    </message>
    <message>
        <source>Envelope PRC 8</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 8</translation>
    </message>
    <message>
        <source>Envelope PRC 9</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 9</translation>
    </message>
    <message>
        <source>Envelope PRC 10</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢPRC 10</translation>
    </message>
    <message>
        <source>Envelope You 4</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤᠨ ᠲᠤᠭᠳᠤᠢYou #4</translation>
    </message>
</context>
</TS>
