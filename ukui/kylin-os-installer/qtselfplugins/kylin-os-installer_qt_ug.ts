<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2316"/>
        <source>Services</source>
        <translation>مۇلازىمەتلەر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide %1</source>
        <translation>يوشۇرۇش ٪1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Others</source>
        <translation>باشقىلارنى يوشۇر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show All</source>
        <translation>ھەممىنى كۆرسەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preferences...</source>
        <translation>ئېتىبارلار...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit %1</source>
        <translation>٪1 دىن چېكىنىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About %1</source>
        <translation>تەخمىنەن ٪1</translation>
    </message>
</context>
<context>
    <name>AudioOutput</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/audiooutput.cpp" line="+375"/>
        <source>&lt;html&gt;The audio playback device &lt;b&gt;%1&lt;/b&gt; does not work.&lt;br/&gt;Falling back to &lt;b&gt;%2&lt;/b&gt;.&lt;/html&gt;</source>
        <translation>&lt;html&gt; ئاۋاز قويۇش ئۈسكۈنىسى &lt;b&gt;٪1&lt;/b&gt; ئىشلىمەيدۇ. &lt;br/&gt; قايتىپ كەلدى &lt;b&gt;٪2&lt;/b&gt;. &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>&lt;html&gt;Switching to the audio playback device &lt;b&gt;%1&lt;/b&gt;&lt;br/&gt;which just became available and has higher preference.&lt;/html&gt;</source>
        <translation>&lt;html&gt; ئاۋاز قويۇش ئۈسكۈنىسىگە ئالماشتۇرۇش &lt;b&gt;٪1&lt;/b&gt;&lt;br/&gt;بۇ ئەپ ئەمدىلا بارلىققا كېلىپ تېخىمۇ يۇقىرى ئېتىبارغا ئىگە. &lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Revert back to device &apos;%1&apos;</source>
        <translation>&apos;٪1&apos; ئۈسكۈنىگە قايتىش</translation>
    </message>
</context>
<context>
    <name>CloseButton</name>
    <message>
        <location filename="../src/gui/widgets/qtabbar.cpp" line="+2251"/>
        <source>Close Tab</source>
        <translation>تاختا تاقاش</translation>
    </message>
</context>
<context>
    <name>Phonon::</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/phononnamespace.cpp" line="+55"/>
        <source>Notifications</source>
        <translation>ئۇقتۇرۇشلار</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Music</source>
        <translation>ناخشا-مۇزىكا</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Video</source>
        <translation>ۋىدېئو</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Communication</source>
        <translation>ئالاقە</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Games</source>
        <translation>ئويۇنلار</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Accessibility</source>
        <translation>زىيارەت قىلىش</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::Backend</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/backend.cpp" line="+171"/>
        <source>Warning: You do not seem to have the package gstreamer0.10-plugins-good installed.
          Some video features have been disabled.</source>
        <translation>دىققەت: Gstreamer0.10-قىستۇرمىلار ياخشى قاچىلانغاندەك قىلمايسىز.
          بىر قىسىم سىن ئىقتىدارلىرى چەكلەنگەن.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: You do not seem to have the base GStreamer plugins installed.
          All audio and video support has been disabled</source>
        <translation>دىققەت: ئاساسى GStreamer قىستۇرمىلىرى قاچىلانمىغاندەك قىلىدۇ.
          بارلىق ئۈن-سىن قوللىغۇچىلار چەكلەنگەن</translation>
    </message>
</context>
<context>
    <name>Phonon::Gstreamer::MediaObject</name>
    <message>
        <location filename="../src/3rdparty/phonon/gstreamer/mediaobject.cpp" line="+90"/>
        <source>Cannot start playback. 

Check your Gstreamer installation and make sure you 
have libgstreamer-plugins-base installed.</source>
        <translation>قويۇشنى باشلىغىلى بولمايدۇ. 

Gstreamer قاچىلاشنى تەكشۈرۈپ، ئۆزىڭىزنىڭ 
libgstreamer-plugins-base قاچىلانغان.</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>A required codec is missing. You need to install the following codec(s) to play this content: %0</source>
        <translation>كېرەكلىك كود كودى يوقاپ كەتتى. بۇ مەزمۇننى قويۇش ئۈچۈن تۆۋەندىكى codec(s) نى قاچىلىشىڭىز كېرەك: ٪0</translation>
    </message>
    <message>
        <location line="+676"/>
        <location line="+8"/>
        <location line="+15"/>
        <location line="+9"/>
        <location line="+6"/>
        <location line="+19"/>
        <location line="+335"/>
        <location line="+24"/>
        <source>Could not open media source.</source>
        <translation>مېدىئا مەنبەسىنى ئاچقىلى بولمىدى.</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>Invalid source type.</source>
        <translation>ئىناۋەتسىز مەنبە تۈرى.</translation>
    </message>
    <message>
        <location line="+377"/>
        <source>Could not locate media source.</source>
        <translation>مېدىئا مەنبەسىنى تاپالمىدى.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not open audio device. The device is already in use.</source>
        <translation>ئاۋاز ئۈسكۈنىسىنى ئاچقىلى بولمىدى. بۇ ئۈسكۈنە ئاللىقاچان ئىشلىتىلىپ بولغان.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Could not decode media source.</source>
        <translation>مېدىئا مەنبەسىنى كودلىغىلى بولمىدى.</translation>
    </message>
</context>
<context>
    <name>Phonon::VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+42"/>
        <location line="+18"/>
        <source>Volume: %1%</source>
        <translation>ھەجىم: ٪1٪</translation>
    </message>
    <message>
        <location line="-15"/>
        <location line="+18"/>
        <location line="+54"/>
        <source>Use this slider to adjust the volume. The leftmost position is 0%, the rightmost is %1%</source>
        <translation>بۇ سىيرىلغۇچتىن پايدىلىنىپ ئاۋاز مىقدارىنى تەڭشەڭ. ئەڭ سول تەرەپتىكى ئورۇن ٪0، ئەڭ ئوڭ تەرەپ ٪1</translation>
    </message>
</context>
<context>
    <name>Q3Accel</name>
    <message>
        <location filename="../src/qt3support/other/q3accel.cpp" line="+481"/>
        <source>%1, %2 not defined</source>
        <translation>٪1، ٪2 ئېنىقلىما</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Ambiguous %1 not handled</source>
        <translation>بىر تەرەپ قىلىنمىغان ٪1</translation>
    </message>
</context>
<context>
    <name>Q3DataTable</name>
    <message>
        <location filename="../src/qt3support/sql/q3datatable.cpp" line="+285"/>
        <source>True</source>
        <translation>راست</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>يالغان</translation>
    </message>
    <message>
        <location line="+505"/>
        <source>Insert</source>
        <translation>قىستۇرۇش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Update</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
</context>
<context>
    <name>Q3FileDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog.cpp" line="+864"/>
        <source>Copy or Move a File</source>
        <translation>ھۆججەت كۆچۈرۈش ياكى يۆتكەش</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Read: %1</source>
        <translation>ئوقۇلدى: ٪1</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+30"/>
        <source>Write: %1</source>
        <translation>يازما: ٪1</translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+1575"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location line="-157"/>
        <location line="+49"/>
        <location line="+2153"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_mac.cpp" line="+110"/>
        <source>All Files (*)</source>
        <translation>بارلىق ھۆججەتلەر (*)</translation>
    </message>
    <message>
        <location line="-2089"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <translation>تۈرى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation>چېسلا</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attributes</source>
        <translation>خاسلىق</translation>
    </message>
    <message>
        <location line="+35"/>
        <location line="+2031"/>
        <source>&amp;OK</source>
        <translation>&amp; OK</translation>
    </message>
    <message>
        <location line="-1991"/>
        <source>Look &amp;in:</source>
        <translation>قاراڭ:</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1981"/>
        <location line="+16"/>
        <source>File &amp;name:</source>
        <translation>ھۆججەت &amp; نامى:</translation>
    </message>
    <message>
        <location line="-1996"/>
        <source>File &amp;type:</source>
        <translation>ھۆججەت &amp; تۈرى:</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Back</source>
        <translation>كەينىگە يېنىش</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>One directory up</source>
        <translation>بىر مۇندەرىجە يۇقىرىغا</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Create New Folder</source>
        <translation>يېڭى ھۆججەت قىسقۇچ قۇرۇش</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List View</source>
        <translation>تىزىملىك كۆرۈش</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Detail View</source>
        <translation>تەپسىلى كۆرۈڭ</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Preview File Info</source>
        <translation>ھۆججەت ئۇچۇرىنى ئالدىن كۆرۈش</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Preview File Contents</source>
        <translation>ھۆججەت مەزمۇنىنى ئالدىن كۆرۈش</translation>
    </message>
    <message>
        <location line="+88"/>
        <source>Read-write</source>
        <translation>ئوقۇشلۇق- يېزىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Read-only</source>
        <translation>پەقەت ئوقۇش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write-only</source>
        <translation>پەقەت يېزىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inaccessible</source>
        <translation>يىتەرسىز</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Symlink to File</source>
        <translation>ھۆججەتكە Symlink</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Directory</source>
        <translation>Symlink to مۇندەرىجىگە</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Symlink to Special</source>
        <translation>ئالاھىدە ئۇلانمىغا Symlink</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File</source>
        <translation>ھۆججەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dir</source>
        <translation>Dir</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Special</source>
        <translation>ئالاھىدە</translation>
    </message>
    <message>
        <location line="+704"/>
        <location line="+2100"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+337"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location line="-1990"/>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="+84"/>
        <source>Save As</source>
        <translation>As نى ساقلاش</translation>
    </message>
    <message>
        <location line="+642"/>
        <location line="+5"/>
        <location line="+355"/>
        <source>&amp;Open</source>
        <translation>ئاچ&amp;ئاچ</translation>
    </message>
    <message>
        <location line="-357"/>
        <location line="+341"/>
        <source>&amp;Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location line="-334"/>
        <source>&amp;Rename</source>
        <translation>&amp;قايتا ئىسىم</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>R&amp;eload</source>
        <translation>R&amp; eload</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Sort by &amp;Name</source>
        <translation>ئىسىم بويىچە رەتلەش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sort by &amp;Size</source>
        <translation>چوڭ-كىچىكلىكى بويىچە تۈرگە ئايرىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sort by &amp;Date</source>
        <translation>ۋاقىت بويىچە رەتلەش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Unsorted</source>
        <translation>&amp;ماس كەلمىگەن</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Sort</source>
        <translation>تۈرگە ئايرىش</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show &amp;hidden files</source>
        <translation>يوشۇرۇن ھۆججەتلەرنى كۆرسىتىش</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>the file</source>
        <translation>ھۆججەت</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the directory</source>
        <translation>مۇندەرىجە</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>the symlink</source>
        <translation>سىم ئۇلانمىسى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete %1</source>
        <translation>٪1 ئۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Are you sure you wish to delete %1 &quot;%2&quot;?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; ٪1 «٪2» نى ئۆچۈرەمسىز؟ &lt;/qt&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Yes</source>
        <translation>&amp;Evet</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;No</source>
        <translation>&amp;ياق</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>New Folder 1</source>
        <translation>يېڭى ھۆججەت قىسقۇچ 1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder</source>
        <translation>يېڭى ھۆججەت قىسقۇچ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>New Folder %1</source>
        <translation>يېڭى ھۆججەت قىسقۇچ ٪1</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>Find Directory</source>
        <translation>مۇندەرىجىنى تېپىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+108"/>
        <source>Directories</source>
        <translation>مۇندەرىجىلەر</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Directory:</source>
        <translation>مۇندەرىجە:</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+1110"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location line="-1109"/>
        <source>%1
File not found.
Check path and filename.</source>
        <translation>%1
ھۆججەت تېپىلمىدى.
يول ۋە ھۆججەت نامىنى تەكشۈرىدۇ.</translation>
    </message>
    <message>
        <location filename="../src/qt3support/dialogs/q3filedialog_win.cpp" line="-289"/>
        <source>All Files (*.*)</source>
        <translation>بارلىق ھۆججەتلەر (*.*)</translation>
    </message>
    <message>
        <location line="+375"/>
        <source>Open </source>
        <translation>ئېچىش </translation>
    </message>
    <message>
        <location line="+155"/>
        <source>Select a Directory</source>
        <translation>مۇندەرىجە تاللاش</translation>
    </message>
</context>
<context>
    <name>Q3LocalFs</name>
    <message>
        <location filename="../src/qt3support/network/q3localfs.cpp" line="+130"/>
        <location line="+10"/>
        <source>Could not read directory
%1</source>
        <translation>مۇندەرىجىنى ئوقۇغىلى بولمىدى
%1</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Could not create directory
%1</source>
        <translation>مۇندەرىجە قۇرالمىدى
%1</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Could not remove file or directory
%1</source>
        <translation>ھۆججەت ياكى مۇندەرىجىنى چىقىرىۋېتىشكە بولمىدى
%1</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Could not rename
%1
to
%2</source>
        <translation>قايتا ئىسىم قويغىلى بولمىدى
%1
تو
%2</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Could not open
%1</source>
        <translation>ئېچىلمىدى
%1</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Could not write
%1</source>
        <translation>يېزىلمىدى
%1</translation>
    </message>
</context>
<context>
    <name>Q3MainWindow</name>
    <message>
        <location filename="../src/qt3support/widgets/q3mainwindow.cpp" line="+2051"/>
        <source>Line up</source>
        <translation>ئۆچرەتتە تۇرۇش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Customize...</source>
        <translation>خاسلاشتۇر...</translation>
    </message>
</context>
<context>
    <name>Q3NetworkProtocol</name>
    <message>
        <location filename="../src/qt3support/network/q3networkprotocol.cpp" line="+854"/>
        <source>Operation stopped by the user</source>
        <translation>ئىشلەتكۈچى توختىغان مەشغۇلات</translation>
    </message>
</context>
<context>
    <name>Q3ProgressDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3progressdialog.cpp" line="+224"/>
        <location line="+61"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>Q3TabDialog</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3tabdialog.cpp" line="+189"/>
        <location line="+814"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location line="-356"/>
        <source>Apply</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Defaults</source>
        <translation>كۆڭۈلدىكى سۆز</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>Q3TextEdit</name>
    <message>
        <location filename="../src/qt3support/text/q3textedit.cpp" line="+5429"/>
        <source>&amp;Undo</source>
        <translation>&amp;Undo</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Redo</source>
        <translation>&amp;قايتا</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cu&amp;t</source>
        <translation>Cu&amp;T</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Paste</source>
        <translation>چاپلاش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation>تازىلاش</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+2"/>
        <source>Select All</source>
        <translation>ھەممىنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>Q3TitleBar</name>
    <message>
        <location filename="../src/plugins/accessible/compat/q3complexwidgets.cpp" line="+246"/>
        <source>System</source>
        <translation>سىستېما</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore up</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore down</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximize</source>
        <translation>ئەڭ چوڭ چەككە</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Contains commands to manipulate the window</source>
        <translation>كۆزنەكنى باشقۇرۇش بۇيرۇقى بار</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a minimized back to normal</source>
        <translation>كىچىكلىتىشنى نورمال ھالەتكە ئەكىلىپ قويىدۇ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Moves the window out of the way</source>
        <translation>كۆزنەكنى يولدىن ئېلىپ چىقىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Puts a maximized window back to normal</source>
        <translation>ئەڭ چوڭ بولغان كۆزنەكنى نورمال ئەھۋالغا قايتۇرۇش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Makes the window full screen</source>
        <translation>كۆزنەكنى تولۇق ئېكران قىلىدۇ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Closes the window</source>
        <translation>دېرىزىنى تاقاش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Displays the name of the window and contains controls to manipulate it</source>
        <translation>كۆزنەك نامىنى كۆرسىتىپ، كونتىروللاشنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
</context>
<context>
    <name>Q3ToolBar</name>
    <message>
        <location filename="../src/qt3support/widgets/q3toolbar.cpp" line="+692"/>
        <source>More...</source>
        <translation>تېخىمۇ كۆپ...</translation>
    </message>
</context>
<context>
    <name>Q3UrlOperator</name>
    <message>
        <location filename="../src/qt3support/network/q3urloperator.cpp" line="+386"/>
        <location line="+260"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; is not supported</source>
        <translation>&apos;٪1&apos; كېلىشىمى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="-260"/>
        <source>The protocol `%1&apos; does not support listing directories</source>
        <translation>&apos;٪1&apos; كېلىشىمى تىزىملىك مۇندەرىجىسىنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support creating new directories</source>
        <translation>&apos;٪1&apos; كېلىشىمى يېڭى مۇندەرىجىلەرنى قۇرۇشنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support removing files or directories</source>
        <translation>&apos;٪1&apos; كېلىشىمى ھۆججەت ياكى مۇندەرىجىلەرنى ئېلىۋېتىشنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support renaming files or directories</source>
        <translation>&apos;٪1&apos; كېلىشىمى ھۆججەت ياكى مۇندەرىجە نامىنى ئۆزگەرتىشنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support getting files</source>
        <translation>&apos;٪1&apos; كېلىشىمى ھۆججەت ئېلىشنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The protocol `%1&apos; does not support putting files</source>
        <translation>&apos;٪1&apos; كېلىشىم ھۆججەتلەرنى قويۇشنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+243"/>
        <location line="+4"/>
        <source>The protocol `%1&apos; does not support copying or moving files or directories</source>
        <translation>&apos;٪1&apos; كېلىشىمى ھۆججەت ياكى مۇندەرىجە كۆچۈرۈش ۋە يۆتكەشنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+237"/>
        <location line="+1"/>
        <source>(unknown)</source>
        <translation>(ئېنىق ئەمەس)</translation>
    </message>
</context>
<context>
    <name>Q3Wizard</name>
    <message>
        <location filename="../src/qt3support/dialogs/q3wizard.cpp" line="+177"/>
        <source>&amp;Cancel</source>
        <translation>ۋازكەچى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp; كەينى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Next &gt;</source>
        <translation>كېيىنكى &gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Finish</source>
        <translation>&amp;تاماملاش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Help</source>
        <translation>ياردەم</translation>
    </message>
</context>
<context>
    <name>QAbstractSocket</name>
    <message>
        <location filename="../src/network/socket/qabstractsocket.cpp" line="+868"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+615"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+657"/>
        <location line="+26"/>
        <source>Host not found</source>
        <translation>رىياسەتچى تېپىلمىدى</translation>
    </message>
    <message>
        <location line="+50"/>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="+3"/>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="+4"/>
        <source>Connection refused</source>
        <translation>ئۇلىنىش رەت قىلىندى</translation>
    </message>
    <message>
        <location line="+141"/>
        <source>Connection timed out</source>
        <translation>ئۇلىنىش ۋاقتى ۋاقتسىز</translation>
    </message>
    <message>
        <location line="-547"/>
        <location line="+787"/>
        <location line="+208"/>
        <source>Operation on socket is not supported</source>
        <translation>Socket دىكى مەشغۇلات قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>Socket operation timed out</source>
        <translation>Socket مەشغۇلات ۋاقتى ئۇزىراپ كەتتى</translation>
    </message>
    <message>
        <location line="+380"/>
        <source>Socket is not connected</source>
        <translation>Socket ئۇلانمىدى</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-8"/>
        <source>Network unreachable</source>
        <translation>تورغا چىققىلى بولمايدۇ</translation>
    </message>
</context>
<context>
    <name>QAbstractSpinBox</name>
    <message>
        <location filename="../src/gui/widgets/qabstractspinbox.cpp" line="+1199"/>
        <source>&amp;Step up</source>
        <translation>&amp;قەدەم</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Step &amp;down</source>
        <translation>قەدەم &amp; پەس</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Select All</source>
        <translation>ھەممىنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+376"/>
        <source>Activate</source>
        <translation>قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="+352"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>ئىجرا قىلىشقا بولىدىغان &apos;٪1&apos; Qt ٪2 نى تەلەپ قىلىدۇ، تېپىلغان Qt ٪3.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incompatible Qt Library Error</source>
        <translation>بىردەك بولمىغان QT ئامبىرى خاتالىقى</translation>
    </message>
    <message>
        <location filename="../src/gui/accessible/qaccessibleobject.cpp" line="+2"/>
        <source>Activates the program&apos;s main window</source>
        <translation>پروگرامما ئاساسىي كۆزنىكى قوزغىتىلىدۇ</translation>
    </message>
</context>
<context>
    <name>QAxSelect</name>
    <message>
        <location filename="../src/activeqt/container/qaxselect.ui"/>
        <source>Select ActiveX Control</source>
        <translation>ActiveX كونتىروللاشنى تاللاڭ</translation>
    </message>
    <message>
        <location/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Cancel</source>
        <translation>ۋازكەچى</translation>
    </message>
    <message>
        <location/>
        <source>COM &amp;Object:</source>
        <translation>COM &amp; لايىق:</translation>
    </message>
</context>
<context>
    <name>QCheckBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+114"/>
        <source>Uncheck</source>
        <translation>تەكشۈرۈشتىن ئايرىلدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Check</source>
        <translation>تەكشۈرۈش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Toggle</source>
        <translation>قاڭقىش</translation>
    </message>
</context>
<context>
    <name>QColorDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qcolordialog.cpp" line="+1253"/>
        <source>Hu&amp;e:</source>
        <translation>خۇ ئېر:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Sat:</source>
        <translation>&amp; Sat:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Val:</source>
        <translation>ۋەل:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Red:</source>
        <translation>&amp; قىزىل :</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Green:</source>
        <translation>&amp; يېشىل:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bl&amp;ue:</source>
        <translation>Bl&amp;ue:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A&amp;lpha channel:</source>
        <translation>A&amp; Lpha قانىلى:</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Select Color</source>
        <translation>رەڭ تاللاش</translation>
    </message>
    <message>
        <location line="+137"/>
        <source>&amp;Basic colors</source>
        <translation>&amp; ئاساسىي رەڭلەر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Custom colors</source>
        <translation>خاس رەڭلەر</translation>
    </message>
    <message>
        <source>Cursor at %1, %2
Press ESC to cancel</source>
        <translation>٪1، ٪2 دە كۇرەڭگۈر
ئەمەلدىن قالدۇرۇش ئۈچۈن ESC نى بېسىپ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Add to Custom Colors</source>
        <translation>خاس رەڭلەرگە قوش</translation>
    </message>
    <message>
        <source>Select color</source>
        <translation type="obsolete">选择颜色</translation>
    </message>
    <message>
        <source>&amp;Pick Screen Color</source>
        <translation>ئېكران رەڭگىنى تاللاش</translation>
    </message>
</context>
<context>
    <name>QComboBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+1771"/>
        <location line="+65"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qitemeditorfactory.cpp" line="+544"/>
        <source>False</source>
        <translation>يالغان</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True</source>
        <translation>راست</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="+0"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <source>%1: permission denied</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：权限被拒绝</translation>
    </message>
    <message>
        <source>%1: already exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：已经存在</translation>
    </message>
    <message>
        <source>%1: doesn&apos;t exists</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：不存在</translation>
    </message>
    <message>
        <source>%1: out of resources</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：资源耗尽了</translation>
    </message>
    <message>
        <source>%1: unknown error %2</source>
        <comment>QSystemSemaphore</comment>
        <translation type="obsolete">%1：未知错误 %2</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="+119"/>
        <source>%1: key is empty</source>
        <comment>QSystemSemaphore</comment>
        <translation>٪1: ئاچقۇچ بوش</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: unable to make key</source>
        <comment>QSystemSemaphore</comment>
        <translation>٪1: ئاچقۇچ ياساپ بولالمىدى</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1: ftok failed</source>
        <comment>QSystemSemaphore</comment>
        <translation>٪1: ftok مەغلۇپ بولدى</translation>
    </message>
</context>
<context>
    <name>QDB2Driver</name>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+1276"/>
        <source>Unable to connect</source>
        <translation>ئۇلىنالمىدى</translation>
    </message>
    <message>
        <location line="+303"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>قايتۇرۇۋېلىش سودىسى</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to set autocommit</source>
        <translation>ئاپتوماتىك پۇل ئەۋەتىشنى تەڭشىگىلى بولمايدۇ</translation>
    </message>
</context>
<context>
    <name>QDB2Result</name>
    <message>
        <location line="-1043"/>
        <location line="+243"/>
        <source>Unable to execute statement</source>
        <translation>باياناتنى ئىجرا قىلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="-206"/>
        <source>Unable to prepare statement</source>
        <translation>بايانات تەييارلىغىلى بولماس</translation>
    </message>
    <message>
        <location line="+196"/>
        <source>Unable to bind variable</source>
        <translation>ئۆزگەرگۈچى مىقدارنى باغلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Unable to fetch record %1</source>
        <translation>٪1 خاتىرىسىنى ئېلىۋېلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to fetch next</source>
        <translation>كېيىنكى قەدەمدە ئالغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to fetch first</source>
        <translation>ئاۋۋال ئالغىلى بولمايدۇ</translation>
    </message>
</context>
<context>
    <name>QDateTimeEdit</name>
    <message>
        <location filename="../src/gui/widgets/qdatetimeedit.cpp" line="+2295"/>
        <source>AM</source>
        <translation>AM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>am</source>
        <translation>am</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>PM</source>
        <translation>PM</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>pm</source>
        <translation>pm</translation>
    </message>
</context>
<context>
    <name>QDial</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+951"/>
        <source>QDial</source>
        <translation>QDial</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SpeedoMeter</source>
        <translation>SpeedoMeter</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>SliderHandle</source>
        <translation>SliderHandle</translation>
    </message>
</context>
<context>
    <name>QDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qdialog.cpp" line="+597"/>
        <source>What&apos;s This?</source>
        <translation>ئۇ نېمۇ?</translation>
    </message>
    <message>
        <location line="-115"/>
        <source>Done</source>
        <translation>تۈگىدى</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="+1866"/>
        <location line="+464"/>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+561"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../src/gui/widgets/qdialogbuttonbox.cpp" line="+3"/>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Cancel</source>
        <translation>ۋازكەچى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Close</source>
        <translation>يېپىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Apply</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reset</source>
        <translation>قايتا تىڭشى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Don&apos;t Save</source>
        <translation>ساقلىماڭ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Discard</source>
        <translation>ۋازكەس</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Yes</source>
        <translation>&amp;Evet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Yes to &amp;All</source>
        <translation>شۇنداق &amp; ھەممىگە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;No</source>
        <translation>&amp;ياق</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>N&amp;o to All</source>
        <translation>N&amp; O نىڭ ھەممىسىگە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Save All</source>
        <translation>ھەممىنى ساقلىۋېلىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Abort</source>
        <translation>توختىت</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Retry</source>
        <translation>قايتا قايتا تىرشىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ignore</source>
        <translation>نەزەردىن ساقىت قىلىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restore Defaults</source>
        <translation>كۆڭۈلدىكىلەرنى ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="-29"/>
        <source>Close without Saving</source>
        <translation>ساقلىماي تۇرۇپ يېپىش</translation>
    </message>
    <message>
        <location line="-27"/>
        <source>&amp;OK</source>
        <translation>&amp; OK</translation>
    </message>
</context>
<context>
    <name>QDirModel</name>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+453"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>مېھرىبانلىق</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>تۈرى</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Date Modified</source>
        <translation>چېسلا ئۆزگەرتىلگەن ۋاقتى</translation>
    </message>
</context>
<context>
    <name>QDockWidget</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblewidgets.cpp" line="+1239"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dock</source>
        <translation>پىرىستان</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Float</source>
        <translation>لەيلى</translation>
    </message>
</context>
<context>
    <name>QDoubleSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-537"/>
        <source>More</source>
        <translation>تېخىمۇ كۆپ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>ئاز</translation>
    </message>
</context>
<context>
    <name>QErrorMessage</name>
    <message>
        <location filename="../src/gui/dialogs/qerrormessage.cpp" line="+192"/>
        <source>Debug Message:</source>
        <translation>Debug تەبلىغ ئۇچۇرى:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning:</source>
        <translation>دىققەت:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Fatal Error:</source>
        <translation>ئەجەللىك خاتالىق:</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>&amp;Show this message again</source>
        <translation>بۇ ئۇچۇرنى يەنە بىر قېتىم كۆرسىتىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;OK</source>
        <translation>&amp; OK</translation>
    </message>
</context>
<context>
    <name>QFile</name>
    <message>
        <location filename="../src/corelib/io/qfile.cpp" line="+708"/>
        <location line="+141"/>
        <source>Destination file exists</source>
        <translation>نىشان ھۆججەت مەۋجۇت</translation>
    </message>
    <message>
        <location line="-108"/>
        <source>Cannot remove source file</source>
        <translation>مەنبە ھۆججەتنى چىقىرىۋېتىشكە بولمايدۇ</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Cannot open %1 for input</source>
        <translation>كىرگۈزۈش ئۈچۈن ٪1 نى ئاچقىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Cannot open for output</source>
        <translation>چىقىرىش ئۈچۈن ئېچىشقا بولمايدۇ</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failure to write block</source>
        <translation>خەت يېزىش توسۇش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot create %1 for output</source>
        <translation>چىقىرىش ئۈچۈن ٪1 قۇرغىلى بولمايدۇ</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+515"/>
        <location line="+444"/>
        <source>All Files (*)</source>
        <translation>بارلىق ھۆججەتلەر (*)</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Directories</source>
        <translation>مۇندەرىجىلەر</translation>
    </message>
    <message>
        <location line="-3"/>
        <location line="+50"/>
        <location line="+1471"/>
        <location line="+75"/>
        <source>&amp;Open</source>
        <translation>ئاچ&amp;ئاچ</translation>
    </message>
    <message>
        <location line="-1596"/>
        <location line="+50"/>
        <source>&amp;Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <location line="-730"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location line="+1515"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>٪1 ئاللىقاچان مەۋجۇت.
ئالماشتۇرغۇڭىز بارمۇ؟</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1
File not found.
Please verify the correct file name was given.</source>
        <translation>%1
ھۆججەت تېپىلمىدى.
توغرا ھۆججەت نامى بېرىلگەن دەپ دەلىللەڭ.</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qdirmodel.cpp" line="+402"/>
        <source>My Computer</source>
        <translation>مېنىڭ كومپيۇتېرىم</translation>
    </message>
    <message>
        <source>%1 File</source>
        <extracomment>%1 is a file name suffix, for example txt</extracomment>
        <translation>٪1 ھۆججەت</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>٪1 KiB</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-1504"/>
        <source>&amp;Rename</source>
        <translation>&amp;قايتا ئىسىم</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show &amp;hidden files</source>
        <translation>يوشۇرۇن ھۆججەتلەرنى كۆرسىتىش</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Back</source>
        <translation>كەينىگە يېنىش</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Parent Directory</source>
        <translation>ئانا مۇندەرىجىسى</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>List View</source>
        <translation>تىزىملىك كۆرۈش</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Detail View</source>
        <translation>تەپسىلى كۆرۈڭ</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Files of type:</source>
        <translation>تىپ ھۆججەتلىرى:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+6"/>
        <location line="+648"/>
        <source>Directory:</source>
        <translation>مۇندەرىجە:</translation>
    </message>
    <message>
        <location line="+794"/>
        <location line="+862"/>
        <source>%1
Directory not found.
Please verify the correct directory name was given.</source>
        <translation>%1
مۇندەرىجە تېپىلمىدى.
توغرا مۇندەرىجە نامى بېرىلگەن دەپ دەلىللەڭ.</translation>
    </message>
    <message>
        <location line="-218"/>
        <source>&apos;%1&apos; is write protected.
Do you want to delete it anyway?</source>
        <translation>&apos;٪1&apos; يىزىش قوغدىلىدۇ.
ئىشقىلىپ ئۆچۈرەمسىز؟</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are sure you want to delete &apos;%1&apos;?</source>
        <translation>&apos;٪1&apos; نى ئۆچۈرەمسىز؟</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Are you sure you want to delete &apos;%1&apos;?</source>
        <translation>&apos;٪1&apos; نى ئۆچۈرەمسىز؟</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>ھۆججەت قىسقۇچ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Could not delete directory.</source>
        <translation>مۇندەرىجىنى ئۆچۈرەلمىدى.</translation>
    </message>
    <message>
        <location line="+407"/>
        <source>Recent Places</source>
        <translation>يېقىنقى جايلار</translation>
    </message>
    <message>
        <location line="-2550"/>
        <source>Save As</source>
        <translation>As نى ساقلاش</translation>
    </message>
    <message>
        <location filename="../src/gui/itemviews/qfileiconprovider.cpp" line="+411"/>
        <source>Drive</source>
        <translation>قوزغىتىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>File</source>
        <translation>ھۆججەت</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unknown</source>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-4"/>
        <source>Find Directory</source>
        <translation>مۇندەرىجىنى تېپىش</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Show </source>
        <translation>كۆرسەت </translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Forward</source>
        <translation>ئالدىغا</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="+1970"/>
        <source>New Folder</source>
        <translation>يېڭى ھۆججەت قىسقۇچ</translation>
    </message>
    <message>
        <location line="-1963"/>
        <source>&amp;New Folder</source>
        <translation>&amp;يېڭى ھۆججەت قىسقۇچ</translation>
    </message>
    <message>
        <location line="+656"/>
        <location line="+38"/>
        <source>&amp;Choose</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qsidebar.cpp" line="+418"/>
        <source>Remove</source>
        <translation>چىقىرىۋېتىش</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.cpp" line="-687"/>
        <location line="+652"/>
        <source>File &amp;name:</source>
        <translation>ھۆججەت &amp; نامى:</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog.ui"/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Look in:</source>
        <translation>كىرىپ كۆرۈڭ:</translation>
    </message>
    <message>
        <location/>
        <location filename="../src/gui/dialogs/qfiledialog_wince.ui"/>
        <source>Create New Folder</source>
        <translation>يېڭى ھۆججەت قىسقۇچ قۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfiledialog_win.cpp" line="+160"/>
        <source>All Files (*.*)</source>
        <translation>بارلىق ھۆججەتلەر (*.*)</translation>
    </message>
</context>
<context>
    <name>QFileSystemModel</name>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel.cpp" line="+744"/>
        <source>%1 TB</source>
        <translation>٪1 TB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation>٪1 GB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation>٪1 MB</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation>٪1 KB</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 bytes</source>
        <translation>٪1 بايىت</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Invalid filename</source>
        <translation>ئىناۋەتسىز ھۆججەت نامى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;b&gt;The name &quot;%1&quot; can not be used.&lt;/b&gt;&lt;p&gt;Try using another name, with fewer characters or no punctuations marks.</source>
        <translation>&lt;b&gt;«٪1» نامىنى ئىشلىتىشكە بولمايدۇ.&lt;/b&gt; &lt;p&gt;باشقا ئىسىم ئىشلىتىشنى سىناپ بېقىڭ، ھەرپ سانى ئاز بولسۇن ياكى تىنىش بەلگىلىرى يوق.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Kind</source>
        <comment>Match OS X Finder</comment>
        <translation>مېھرىبانلىق</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Type</source>
        <comment>All other platforms</comment>
        <translation>تۈرى</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Date Modified</source>
        <translation>چېسلا ئۆزگەرتىلگەن ۋاقتى</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qfilesystemmodel_p.h" line="+234"/>
        <source>My Computer</source>
        <translation>مېنىڭ كومپيۇتېرىم</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Computer</source>
        <translation>كومپيۇتېر</translation>
    </message>
    <message>
        <source>%1 KiB</source>
        <translation>٪1 KiB</translation>
    </message>
</context>
<context>
    <name>QFontDatabase</name>
    <message>
        <location filename="../src/gui/text/qfontdatabase.cpp" line="+90"/>
        <location line="+1176"/>
        <source>Normal</source>
        <translation>نورمال</translation>
    </message>
    <message>
        <location line="-1173"/>
        <location line="+12"/>
        <location line="+1149"/>
        <source>Bold</source>
        <translation>قاپ يۈرەك</translation>
    </message>
    <message>
        <location line="-1158"/>
        <location line="+1160"/>
        <source>Demi Bold</source>
        <translation>Demi Bold</translation>
    </message>
    <message>
        <location line="-1157"/>
        <location line="+18"/>
        <location line="+1135"/>
        <source>Black</source>
        <translation>قارا</translation>
    </message>
    <message>
        <location line="-1145"/>
        <source>Demi</source>
        <translation>Demi</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+1145"/>
        <source>Light</source>
        <translation>نۇر</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1007"/>
        <source>Italic</source>
        <translation>ئىتاچى</translation>
    </message>
    <message>
        <location line="-1004"/>
        <location line="+1006"/>
        <source>Oblique</source>
        <translation>Oblique</translation>
    </message>
    <message>
        <location line="+705"/>
        <source>Any</source>
        <translation>ھەرقانداق</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Latin</source>
        <translation>لاتىنچە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Greek</source>
        <translation>گرىك تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cyrillic</source>
        <translation>كىرىل يېزىقى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Armenian</source>
        <translation>ئارمىنىيە تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hebrew</source>
        <translation>ئىبراي تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Arabic</source>
        <translation>ئەرەبچە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Syriac</source>
        <translation>سۇرىيە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thaana</source>
        <translation>تاھانا</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Devanagari</source>
        <translation>Devanagari</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bengali</source>
        <translation>بېنگال تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gurmukhi</source>
        <translation>گۈلمۇھەممەد</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gujarati</source>
        <translation>گۇجاراتى تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Oriya</source>
        <translation>ئورىيا</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tamil</source>
        <translation>تامىل تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Telugu</source>
        <translation>تېلۇگۇ تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Kannada</source>
        <translation>كاننادا تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Malayalam</source>
        <translation>مالايالام تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Sinhala</source>
        <translation>سىنجار</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Thai</source>
        <translation>تايلاند تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lao</source>
        <translation>لائوس</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Tibetan</source>
        <translation>زاڭزۇ تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Myanmar</source>
        <translation>بىرما</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Georgian</source>
        <translation>گرۇزىيە تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Khmer</source>
        <translation>Khmer</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Simplified Chinese</source>
        <translation>ئاددىيلاشتۇرۇلغان خەنزۇچە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Traditional Chinese</source>
        <translation>ئەنئەنىۋى جۇڭگولۇقلار</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Japanese</source>
        <translation>ياپون تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Korean</source>
        <translation>كورىيە تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Vietnamese</source>
        <translation>ۋيېتنام تىلى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Symbol</source>
        <translation>بەلگە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ogham</source>
        <translation>ئوخام</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Runic</source>
        <translation>Runic</translation>
    </message>
</context>
<context>
    <name>QFontDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qfontdialog.cpp" line="+772"/>
        <source>&amp;Font</source>
        <translation>خەت نۇسخىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font st&amp;yle</source>
        <translation>خەت نۇسخىسى st&amp;yle</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Effects</source>
        <translation>تەسىرى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stri&amp;keout</source>
        <translation>Stri&amp;Keout</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Underline</source>
        <translation>ئاستىغا سىزىق</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sample</source>
        <translation>ئەۋرىشكە</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wr&amp;iting System</source>
        <translation>Wr&amp; iting سىستېمىسى</translation>
    </message>
    <message>
        <location line="-604"/>
        <location line="+247"/>
        <source>Select Font</source>
        <translation>خەت نۇسخىسىنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../src/network/access/qftp.cpp" line="+826"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+683"/>
        <source>Not connected</source>
        <translation>ئۇلانمىدى</translation>
    </message>
    <message>
        <location line="+65"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+65"/>
        <source>Host %1 not found</source>
        <translation>ساھىپخان ٪1 تېپىلمىدى</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+4"/>
        <source>Connection refused to host %1</source>
        <translation>ئۇلىنىش ٪1نى ساھىپخانغا بېرىشنى رەت قىلدى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection timed out to host %1</source>
        <translation>ئۇلىنىش ۋاقتى ٪1 نى ساھىپخانغا ۋاقتسىز قىلدى</translation>
    </message>
    <message>
        <location line="+104"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+102"/>
        <location line="+1451"/>
        <source>Connected to host %1</source>
        <translation>٪1 ساھىپخانغا ئۇلاندى</translation>
    </message>
    <message>
        <location line="+219"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-1290"/>
        <source>Connection refused for data connection</source>
        <translation>ئۇلىنىش سانلىق مەلۇمات ئۇلاشنى رەت قىلدى</translation>
    </message>
    <message>
        <location line="+178"/>
        <location line="+29"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+195"/>
        <location line="+728"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location line="+889"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+77"/>
        <source>Connecting to host failed:
%1</source>
        <translation>ساھىپخانغا ئۇلىنىش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Login failed:
%1</source>
        <translation>كىرىش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Listing directory failed:
%1</source>
        <translation>تىزىملىك مۇندەرىجىسى مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Changing directory failed:
%1</source>
        <translation>مۇندەرىجىنى ئۆزگەرتىش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Downloading file failed:
%1</source>
        <translation>ھۆججەت چۈشۈرۈش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Uploading file failed:
%1</source>
        <translation>ھۆججەت يۈكلەش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing file failed:
%1</source>
        <translation>ھۆججەتنى ئېلىۋېتىش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Creating directory failed:
%1</source>
        <translation>مۇندەرىجە قۇرۇش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+3"/>
        <source>Removing directory failed:
%1</source>
        <translation>مۇندەرىجىنى ئېلىۋېتىش مەغلۇپ بولدى:
%1</translation>
    </message>
    <message>
        <location line="+28"/>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="+25"/>
        <location line="+250"/>
        <source>Connection closed</source>
        <translation>ئۇلاش تاقالدى</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3ftp.cpp" line="-11"/>
        <source>Host %1 found</source>
        <translation>ساھىپخان ٪1 تېپىلدى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to %1 closed</source>
        <translation>٪1 گە ئۇلىنىش يېپىلدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host found</source>
        <translation>ساھىپخان تېپىلدى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connected to host</source>
        <translation>ساھىپخانغا ئۇلانغان</translation>
    </message>
</context>
<context>
    <name>QGuiApplication</name>
    <message>
        <location filename="../src/gui/kernel/qapplication.cpp" line="+2248"/>
        <source>QT_LAYOUT_DIRECTION</source>
        <comment>Translate this string to the string &apos;LTR&apos; in left-to-right languages or to &apos;RTL&apos; in right-to-left languages (such as Hebrew and Arabic) to get proper widget layout.</comment>
        <translation>QT_LAYOUT_DIRECTION</translation>
    </message>
</context>
<context>
    <name>QHostInfo</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_p.h" line="+183"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
</context>
<context>
    <name>QHostInfoAgent</name>
    <message>
        <location filename="../src/network/kernel/qhostinfo_unix.cpp" line="+178"/>
        <location line="+9"/>
        <location line="+64"/>
        <location line="+31"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="+180"/>
        <location line="+9"/>
        <location line="+40"/>
        <location line="+27"/>
        <source>Host not found</source>
        <translation>رىياسەتچى تېپىلمىدى</translation>
    </message>
    <message>
        <location line="-44"/>
        <location line="+39"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-34"/>
        <location line="+29"/>
        <source>Unknown address type</source>
        <translation>نامەلۇم ئادرېس تۈرى</translation>
    </message>
    <message>
        <location line="+8"/>
        <location filename="../src/network/kernel/qhostinfo_win.cpp" line="-19"/>
        <location line="+27"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
</context>
<context>
    <name>QHttp</name>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="+1574"/>
        <location line="+820"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+1160"/>
        <location line="+567"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location line="-568"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="-370"/>
        <source>Request aborted</source>
        <translation>ئىلتىماسنى بىكار قىلىش</translation>
    </message>
    <message>
        <location line="+579"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+381"/>
        <source>No server set to connect to</source>
        <translation>ئۇلاشقا بېكىتىلگەن مۇلازىمىتېر يوق</translation>
    </message>
    <message>
        <location line="+164"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+56"/>
        <source>Wrong content length</source>
        <translation>خاتا مەزمۇن ئۇزۇنلۇقى</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+4"/>
        <source>Server closed connection unexpectedly</source>
        <translation>مۇلازىمىتېر يېپىلغان ئۇلىنىش ئويلىمىغان يەردىن</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>Unknown authentication method</source>
        <translation>نامەلۇم دەلىللەش ئۇسۇلى</translation>
    </message>
    <message>
        <location line="+183"/>
        <source>Error writing response to device</source>
        <translation>ئۈسكۈنىگە ئىنكاس يېزىش خاتالىقى</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+876"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+38"/>
        <source>Connection refused</source>
        <translation>ئۇلىنىش رەت قىلىندى</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-304"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-4"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Host %1 not found</source>
        <translation>ساھىپخان ٪1 تېپىلمىدى</translation>
    </message>
    <message>
        <location line="+20"/>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+10"/>
        <location line="+19"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>HTTP request failed</source>
        <translation>HTTP ئىلتىماسى مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location line="+73"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+69"/>
        <source>Invalid HTTP response header</source>
        <translation>HTTP ئىنكاس باش قىسمى ئىناۋەتسىز</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+48"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+40"/>
        <location line="+47"/>
        <source>Invalid HTTP chunked body</source>
        <translation>ئىناۋەتسىز HTTP دۆۋىلەنگەن جەسەت</translation>
    </message>
    <message>
        <location filename="../src/qt3support/network/q3http.cpp" line="+294"/>
        <source>Host %1 found</source>
        <translation>ساھىپخان ٪1 تېپىلدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host %1</source>
        <translation>٪1 ساھىپخانغا ئۇلاندى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection to %1 closed</source>
        <translation>٪1 گە ئۇلىنىش يېپىلدى</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Host found</source>
        <translation>ساھىپخان تېپىلدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connected to host</source>
        <translation>ساھىپخانغا ئۇلانغان</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="-22"/>
        <location filename="../src/qt3support/network/q3http.cpp" line="+3"/>
        <source>Connection closed</source>
        <translation>ئۇلاش تاقالدى</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-135"/>
        <source>Proxy authentication required</source>
        <translation>Proxy دەلىللەش زۆرۈر</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Authentication required</source>
        <translation>دەلىللەش زۆرۈر</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Connection refused (or timed out)</source>
        <translation>ئۇلىنىش رەت قىلىندى (ياكى ۋاقىت ئۇلاندى)</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttpnetworkconnection.cpp" line="+6"/>
        <source>Proxy requires authentication</source>
        <translation>Proxy دەلىللەشنى تەلەپ قىلىدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Host requires authentication</source>
        <translation>ساھىپخان دەلىللەشنى تەلەپ قىلىدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Data corrupted</source>
        <translation>سانلىق مەلۇماتلار بۇزۇلغان</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown protocol specified</source>
        <translation>نامەلۇم كېلىشىم بەلگىلەنگەن</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>SSL handshake failed</source>
        <translation>SSL قول ئېلىشىپ كۆرۈشۈش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location filename="../src/network/access/qhttp.cpp" line="-2263"/>
        <source>HTTPS connection requested but SSL support not compiled in</source>
        <translation>HTTPS ئۇلىنىش ئىلتىماس قىلىندى ئەمما SSL قوللاشنى رەتلىمىدى</translation>
    </message>
</context>
<context>
    <name>QHttpSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qhttpsocketengine.cpp" line="-89"/>
        <source>Did not receive HTTP response from proxy</source>
        <translation>proxy دىن HTTP ئىنكاسىنى تاپشۇرۇۋالمىدى</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Error parsing authentication request from proxy</source>
        <translation>proxy نىڭ دەلىللەش تەلىپىنى پارسلاش خاتالىقى</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Authentication required</source>
        <translation>دەلىللەش زۆرۈر</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Proxy denied connection</source>
        <translation>Proxy ئۇلىنىشنى ئىنكار قىلدى</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error communicating with HTTP proxy</source>
        <translation>HTTP proxy بىلەن ئالاقە قىلىش خاتالىقى</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Proxy server not found</source>
        <translation>Proxy مۇلازىمېتىرى تېپىلمىدى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection refused</source>
        <translation>Proxy ئۇلىنىش رەت قىلىندى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy server connection timed out</source>
        <translation>Proxy مۇلازىمېتىرى ئۇلىنىش ۋاقتى ۋاقتسىز</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Proxy connection closed prematurely</source>
        <translation>Proxy ئۇلاشنى بالدۇر يېپىش</translation>
    </message>
</context>
<context>
    <name>QIBaseDriver</name>
    <message>
        <location filename="../src/sql/drivers/ibase/qsql_ibase.cpp" line="+1428"/>
        <source>Error opening database</source>
        <translation>سانداننى ئېچىش خاتالىقى</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Could not start transaction</source>
        <translation>سودىنى باشلىغىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unable to rollback transaction</source>
        <translation>قايتۇرۇۋېلىش سودىسى</translation>
    </message>
</context>
<context>
    <name>QIBaseResult</name>
    <message>
        <location line="-1097"/>
        <source>Unable to create BLOB</source>
        <translation>BLOB نى قۇرغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to write BLOB</source>
        <translation>BLOB نى يېزىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Unable to open BLOB</source>
        <translation>BLOB نى ئاچقىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unable to read BLOB</source>
        <translation>BLOB نى ئوقۇغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+125"/>
        <location line="+189"/>
        <source>Could not find array</source>
        <translation>Array نى تاپالمىدى</translation>
    </message>
    <message>
        <location line="-157"/>
        <source>Could not get array data</source>
        <translation>Array سانلىق مەلۇماتىغا ئېرىشەلمىدى</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Could not get query info</source>
        <translation>سۈرۈشتۈرۈش ئۇچۇرىنى ئالالمىدى</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not start transaction</source>
        <translation>سودىنى باشلىغىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Could not allocate statement</source>
        <translation>بايانات ئاجراتمىدى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not prepare statement</source>
        <translation>بايانات تەييارلىغىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+7"/>
        <source>Could not describe input statement</source>
        <translation>خەت كىرگۈزگۈچنى تەسۋىرلىگىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Could not describe statement</source>
        <translation>باياناتنى تەسۋىرلىگىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unable to close statement</source>
        <translation>باياناتنى تاقاپ بولالمىدى</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unable to execute query</source>
        <translation>سۈرۈشتۈرۈشنى ئىجرا قىلالمىدى</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Could not fetch next item</source>
        <translation>كېيىنكى تۈرنى ئالالمىدى</translation>
    </message>
    <message>
        <location line="+160"/>
        <source>Could not get statement info</source>
        <translation>بايانات ئۇچۇرىنى ئالالمىدى</translation>
    </message>
</context>
<context>
    <name>QIODevice</name>
    <message>
        <location filename="../src/corelib/global/qglobal.cpp" line="+1869"/>
        <source>Permission denied</source>
        <translation>ئىجازەت ئىنكار قىلدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Too many open files</source>
        <translation>ھۆججەتلەرنى بەك كۆپ ئېچىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No such file or directory</source>
        <translation>بۇنداق ھۆججەت ياكى مۇندەرىجە يوق</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>No space left on device</source>
        <translation>ئۈسكۈنە ئۈستىدە بوشلۇق قالمىدى</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qiodevice.cpp" line="+1536"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
</context>
<context>
    <name>QInputContext</name>
    <message>
        <location filename="../src/gui/inputmethod/qinputcontextfactory.cpp" line="+242"/>
        <source>XIM</source>
        <translation>XIM</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>XIM input method</source>
        <translation>XIM كىرگۈزگۈچ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Windows input method</source>
        <translation>Windows كىرگۈزگۈچ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Mac OS X input method</source>
        <translation>Mac OS X كىرگۈزگۈچ</translation>
    </message>
</context>
<context>
    <name>QInputDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qinputdialog.cpp" line="+223"/>
        <source>Enter a value:</source>
        <translation>بىر قىممەت كىرگۈزۈڭ:</translation>
    </message>
</context>
<context>
    <name>QLibrary</name>
    <message>
        <source>QLibrary::load_sys: Cannot load %1 (%2)</source>
        <translation type="obsolete">QLibrary::load_sys： 不能载入%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::unload_sys: Cannot unload %1 (%2)</source>
        <translation type="obsolete">QLibrary::unload_sys：不能卸载%1 (%2)</translation>
    </message>
    <message>
        <source>QLibrary::resolve_sys: Symbol &quot;%1&quot; undefined in %2 (%3)</source>
        <translation type="obsolete">QLibrary::resolve_sys: 符号“%1”在%2（%3）没有被定义</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary.cpp" line="+378"/>
        <source>Could not mmap &apos;%1&apos;: %2</source>
        <translation>mmap &apos;٪1&apos;: ٪2</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Plugin verification data mismatch in &apos;%1&apos;</source>
        <translation>قىستۇرما دەلىللەش سانلىق مەلۇماتى &apos;٪1&apos; دىكى ماس كەلمەسلىك</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Could not unmap &apos;%1&apos;: %2</source>
        <translation>&apos;٪1&apos;: ٪2 نى باشلىغىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (%2.%3.%4) [%5]</source>
        <translation>قىستۇرما &apos;٪1&apos; گە ماس كەلمەيدىغان QT ئامبىرى ئىشلىتىلىدۇ. (%2.%3.%4) [%5]</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. Expected build key &quot;%2&quot;, got &quot;%3&quot;</source>
        <translation>قىستۇرما &apos;٪1&apos; گە ماس كەلمەيدىغان QT ئامبىرى ئىشلىتىلىدۇ. مۆلچەرلەنگەن قۇرۇلۇش ئاچقۇچى &quot;٪2&quot;، &quot;٪3&quot;كە ئېرىشتى</translation>
    </message>
    <message>
        <location line="+340"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location line="-377"/>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+280"/>
        <source>The shared library was not found.</source>
        <translation>ئورتاق كۇتۇپخانا تېپىلمىدى.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The file &apos;%1&apos; is not a valid Qt plugin.</source>
        <translation>&apos;٪1&apos; ھۆججىتى ئىناۋەتلىك Qt قىستۇرمىسى ئەمەس.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>The plugin &apos;%1&apos; uses incompatible Qt library. (Cannot mix debug and release libraries.)</source>
        <translation>قىستۇرما &apos;٪1&apos; گە ماس كەلمەيدىغان QT ئامبىرى ئىشلىتىلىدۇ. (كۇتۇپخانىنى ئارىلاشتۇرغىلى ۋە قويۇپ بېرەلمەيدۇ.)</translation>
    </message>
    <message>
        <location filename="../src/corelib/plugin/qlibrary_unix.cpp" line="+209"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+99"/>
        <source>Cannot load library %1: %2</source>
        <translation>٪1 ئامبىرىنى قاچىلىغىلى بولمىدى: ٪2</translation>
    </message>
    <message>
        <location line="+16"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+26"/>
        <source>Cannot unload library %1: %2</source>
        <translation>٪1 ئامبىرىنى چۈشۈرگىلى بولمايدۇ: ٪2</translation>
    </message>
    <message>
        <location line="+31"/>
        <location filename="../src/corelib/plugin/qlibrary_win.cpp" line="+15"/>
        <source>Cannot resolve symbol &quot;%1&quot; in %2: %3</source>
        <translation>٪2 دا &quot;٪1&quot; بەلگىسىنى ھەل قىلغىلى بولمىدى: ٪3</translation>
    </message>
</context>
<context>
    <name>QLineEdit</name>
    <message>
        <location filename="../src/gui/widgets/qlineedit.cpp" line="+2680"/>
        <source>&amp;Undo</source>
        <translation>&amp;Undo</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Redo</source>
        <translation>&amp;قايتا</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cu&amp;t</source>
        <translation>Cu&amp;T</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Paste</source>
        <translation>چاپلاش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>ھەممىنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>QLocalServer</name>
    <message>
        <location filename="../src/network/socket/qlocalserver.cpp" line="+226"/>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="+231"/>
        <source>%1: Name error</source>
        <translation>٪1: ئىسىم خاتالىقى</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalserver_unix.cpp" line="-8"/>
        <source>%1: Permission denied</source>
        <translation>٪1: ئىجازەت رەت قىلىنىدۇ</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1: Address in use</source>
        <translation>٪1: ئىشلىتىلىش ئورنى ئادرېسى</translation>
    </message>
    <message>
        <location line="+5"/>
        <location filename="../src/network/socket/qlocalserver_win.cpp" line="+158"/>
        <source>%1: Unknown error %2</source>
        <translation>٪1: نامەلۇم خاتالىق ٪2</translation>
    </message>
</context>
<context>
    <name>QLocalSocket</name>
    <message>
        <location filename="../src/network/socket/qlocalsocket_tcp.cpp" line="+132"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+134"/>
        <source>%1: Connection refused</source>
        <translation>٪1: ئۇلىنىش رەت قىلىندى</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Remote closed</source>
        <translation>٪1: يىراقتىن تاقالدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+80"/>
        <location line="+43"/>
        <source>%1: Invalid name</source>
        <translation>٪1: ئىناۋەتسىز ئىسىم</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket access error</source>
        <translation>٪1: Socket زىيارەت خاتالىقى</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket resource error</source>
        <translation>٪1: Socket بايلىق خاتالىقى</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Socket operation timed out</source>
        <translation>٪1: Socket مەشغۇلاتىنىڭ ۋاقتى ئۆچۈپ قالدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: Datagram too large</source>
        <translation>٪1: سانلىق مەلۇمات گرامماتىكىسى بەك چوڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="-48"/>
        <source>%1: Connection error</source>
        <translation>٪1: ئۇلىنىش خاتالىقى</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+3"/>
        <source>%1: The socket operation is not supported</source>
        <translation>٪1: سوكېت مەشخۇلاتى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: Unknown error</source>
        <translation>٪1: نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location filename="../src/network/socket/qlocalsocket_unix.cpp" line="+4"/>
        <location filename="../src/network/socket/qlocalsocket_win.cpp" line="+10"/>
        <source>%1: Unknown error %2</source>
        <translation>٪1: نامەلۇم خاتالىق ٪2</translation>
    </message>
</context>
<context>
    <name>QMYSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/mysql/qsql_mysql.cpp" line="+1231"/>
        <source>Unable to open database &apos;</source>
        <translation>سانداننى ئاچالمىدى &apos;</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to connect</source>
        <translation>ئۇلىنالمىدى</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>Unable to begin transaction</source>
        <translation>سودىنى باشلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>قايتۇرۇۋېلىش سودىسى</translation>
    </message>
</context>
<context>
    <name>QMYSQLResult</name>
    <message>
        <location line="-922"/>
        <source>Unable to fetch data</source>
        <translation>سانلىق مەلۇماتنى ئېلىۋېلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Unable to execute query</source>
        <translation>سۈرۈشتۈرۈشنى ئىجرا قىلالمىدى</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to store result</source>
        <translation>نەتىجە ساقلاشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+190"/>
        <location line="+8"/>
        <source>Unable to prepare statement</source>
        <translation>بايانات تەييارلىغىلى بولماس</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Unable to reset statement</source>
        <translation>باياناتنى قايتا باشلىيالمىدۇق</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unable to bind value</source>
        <translation>قىممىتىنى باغلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Unable to execute statement</source>
        <translation>باياناتنى ئىجرا قىلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+21"/>
        <source>Unable to bind outvalues</source>
        <translation>دىپلوماتنى باغلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="-12"/>
        <source>Unable to store statement results</source>
        <translation>بايانات نەتىجىسىنى ساقلاشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="-253"/>
        <source>Unable to execute next query</source>
        <translation>كېيىنكى سۈرۈشتۈرۈشنى ئىجرا قىلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unable to store next result</source>
        <translation>كېيىنكى نەتىجىنى ساقلاشقا ئامالسىز</translation>
    </message>
</context>
<context>
    <name>QMdiArea</name>
    <message>
        <location filename="../src/gui/widgets/qmdiarea.cpp" line="+290"/>
        <source>(Untitled)</source>
        <translation>(نامسىز)</translation>
    </message>
</context>
<context>
    <name>QMdiSubWindow</name>
    <message>
        <location filename="../src/gui/widgets/qmdisubwindow.cpp" line="+280"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location line="-18"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Restore Down</source>
        <translation>پەسكە ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="+707"/>
        <source>&amp;Restore</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Move</source>
        <translation>يۆتكەش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mi&amp;nimize</source>
        <translation>Mi&amp;nimize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>ما&amp;ximize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on &amp;Top</source>
        <translation>چوققا &amp; ئۈستى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close</source>
        <translation>يېپىش</translation>
    </message>
    <message>
        <location line="-787"/>
        <source>- [%1]</source>
        <translation>- [%1]</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Maximize</source>
        <translation>ئەڭ چوڭ چەككە</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unshade</source>
        <translation>تەۋرەنمەس</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shade</source>
        <translation>سايە</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Restore</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Menu</source>
        <translation>تىزىملىك</translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/qaccessiblemenu.cpp" line="+157"/>
        <location line="+225"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location line="-224"/>
        <location line="+225"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <location line="-223"/>
        <location line="+225"/>
        <location line="+51"/>
        <source>Execute</source>
        <translation>ئىجرا قىلىش</translation>
    </message>
</context>
<context>
    <name>QMenuBar</name>
    <message>
        <source>About</source>
        <translation type="obsolete">关于</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="obsolete">配置</translation>
    </message>
    <message>
        <source>Preference</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="obsolete">选项</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="obsolete">设置</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation type="obsolete">安装</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation type="obsolete">关于%1</translation>
    </message>
    <message>
        <source>About Qt</source>
        <translation type="obsolete">关于Qt</translation>
    </message>
    <message>
        <source>Preferences</source>
        <translation type="obsolete">首选项</translation>
    </message>
    <message>
        <source>Quit %1</source>
        <translation type="obsolete">退出%1</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../src/gui/dialogs/qmessagebox.cpp" line="-1111"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location line="-853"/>
        <location line="+852"/>
        <location filename="../src/gui/dialogs/qmessagebox.h" line="-52"/>
        <location line="+8"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location line="+509"/>
        <source>About Qt</source>
        <translation>Qt ھەققىدە</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用的是Qt %1版。&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="-1605"/>
        <source>Show Details...</source>
        <translation>تەپسىلاتىنى كۆرسەت...</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Details...</source>
        <translation>تەپسىلاتىنى يوشۇرۇش...</translation>
    </message>
    <message>
        <location line="+1570"/>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is available under three different licensing options designed to accommodate the needs of our various users.&lt;/p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of the GNU LGPL version 2.1 or GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU LGPL version 2.1 is appropriate for the development of Qt applications (proprietary or open source) provided you can comply with the terms and conditions of the GNU LGPL version 2.1.&lt;/p&gt;&lt;p&gt;Qt licensed under the GNU General Public License version 3.0 is appropriate for the development of Qt applications where you wish to use such applications in combination with software subject to the terms of the GNU GPL version 3.0 or where you are otherwise willing to comply with the terms of the GNU GPL version 3.0.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot;&gt;qt.nokia.com/products/licensing&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;چ چ ھەققىدە&lt;/h3&gt;&lt;p&gt;بۇ پروگرامما ٪1 QT نۇسخىسىنى ئىشلىتىدۇ.&lt;/p&gt; &lt;p&gt;Qt بولسا سۇپا ھالقىغان ئەپ ئېچىش ئۈچۈن C++ قورال قورالى.&lt;/p&gt; &lt;p&gt;Qt MS&amp;nbsp;Windows، Mac&amp;nbsp;OS&amp;nbsp;X، Linux قاتارلىق بارلىق ئاساسلىق سودا Unix ۋارىيانتلىرى ئارقىلىق يەككە مەنبەلىك كۆچۈش ئىقتىدارىنى تەمىنلەيدۇ. Qt نى Windows CE ئۈچۈن Embedded Linux ۋە Qt ئۈچۈن Qt قىلىپ قاچىلىغان ئۈسكۈنىلەرگە ئىشلىتىشكە بولىدۇ.&lt;/p&gt; &lt;p&gt;Qt ھەر خىل ئابونتلىرىمىزنىڭ ئېھتىياجىغا سىغدۇرۇش ئۈچۈن لايىھەلەنگەن ئۈچ خىل ئىجازەت تاللانمىسى ئاستىدا تەمىنلىنىدۇ.&lt;/p&gt; بىزنىڭ سودا كىنىشكىسى كېلىشىمىمىز بويىچە ئىجازەتنامە ئالغان QT ئۈچىنچى تەرەپ بىلەن ھەرقانداق مەنبە كودنى ئورتاقلىشىشنى خالىمايدىغان ياكى باشقا ئەھۋالدا GNU LGPL نەشرىنىڭ 2.1 ياكى GNU GPL نەشرىنىڭ 3.0 نۇسخىسىدىكى شەرتلەرگە ئۇيغۇن كەلمەيدىغان خوجىلىق/سودا يۇمشاق دېتالىنى ئېچىشقا ماس كېلىدۇ.&lt;/p&gt; &lt;p&gt;GNU LGPL نۇسقىسى 2.1 نەشرى ئاستىدا ئىجازەتلەنگەن QT ئەپ دېتالى (خوجىلىق ياكى ئوچۇق مەنبە) نىڭ تەرەققىياتىغا ماس كىلىدۇ، ئەگەر سىز GNU LGPL نۇسقىسىنىڭ 2.1 نۇسقىسىدىكى شەرت ۋە شەرتلەرگە رىئايە قىلسىڭىز بولىدۇ.&lt;/p&gt; &lt;p&gt;GNU ئادەتتىكى ئاممىۋى ئىجازەتنامە نۇسخىسى 3.0 گە تەۋە بولغان QT ئەپ دېتالى GNU GPL نەشرى 3.0 نەشرىنىڭ شەرتىگە ئاساسەن يۇمشاق دېتاللار بىلەن بىرلەشتۈرۈپ ئىشلىتىشنى خالايدىغان ياكى بولمىسا GNU GPL نەشرى 3.0 نەشرىنىڭ شەرتىگە رىئايە قىلىشنى خالايدىغان QT ئەپلىرىنى ئېچىشقا ماس كىلىدۇ.&lt;/p&gt; &lt;p&gt;Qt ئىجازەت &lt; ھەققىدە قىسقىچە چۈشەنچە بېرىش ئۈچۈن &lt;a href=&quot;http://qt.nokia.com/products/licensing&quot; &gt;qt.nokia.com/products/licensing&lt;/a&gt;&lt;/p&gt; كۆرۈڭ. &lt;p&gt;Copyright (C) 2012 نوكىيا Corporation ۋە/ياكى ئۇنىڭ قوشۇمچە شىركىتى(-ies)گە تەۋە.&lt;/p&gt; &lt;p&gt;Qt بولسا نوكىيا مەھسۇلاتى. &lt; href=«http://qt.nokia.com/» &gt;qt.nokia.com&lt;/a&gt; تەپسىلاتىنى كۆرۈڭ.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt for Embedded Linux and Qt for Windows CE.&lt;/p&gt;&lt;p&gt;Qt is a Nokia product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokiae.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt也有用于嵌入式Linux和Windows CE的版本。&lt;/p&gt;&lt;p&gt;Qt是Nokia的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;This program uses Qt Open Source Edition version %1.&lt;/p&gt;&lt;p&gt;Qt Open Source Edition is intended for the development of Open Source applications. You need a commercial Qt license for development of proprietary (closed source) applications.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;p&gt;这个程序使用了Qt %1开源版本。&lt;/p&gt;&lt;p&gt;Qt开源版本只用于开源应用程序的开发。如果要开发私有（闭源）软件，你需要一个商业的Qt协议。&lt;/p&gt;&lt;p&gt;有关Qt协议的概览，请参考&lt;a href=&quot;http://qt.nokia.com/company/model/&quot;&gt;qt.nokia.com/company/model/&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;%1&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across MS&amp;nbsp;Windows, Mac&amp;nbsp;OS&amp;nbsp;X, Linux, and all major commercial Unix variants. Qt is also available for embedded devices as Qt Embedded.&lt;/p&gt;&lt;p&gt;Qt is a Trolltech product. See &lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation type="obsolete">&lt;h3&gt;关于Qt&lt;/h3&gt;%1&lt;p&gt;Qt是一个用于跨平台应用程序开发的C++工具包。&lt;/p&gt;&lt;p&gt;对于MS&amp;nbsp;Windows、Mac&amp;nbsp;OS&amp;nbsp;X、Linux和所有主流商业Unix，Qt提供了单一源程序的可移植性。Qt对于嵌入式平台也是可用的，在嵌入式平台上它被称为Qt Embedded。&lt;/p&gt;&lt;p&gt;Qt是Trolltech的产品。有关更多信息，请参考&lt;a href=&quot;http://qt.nokia.com/&quot;&gt;qt.nokia.com&lt;/a&gt;。&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>QMultiInputContext</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontext.cpp" line="+88"/>
        <source>Select IM</source>
        <translation>IM نى تاللاش</translation>
    </message>
</context>
<context>
    <name>QMultiInputContextPlugin</name>
    <message>
        <location filename="../src/plugins/inputmethods/imsw-multi/qmultiinputcontextplugin.cpp" line="+95"/>
        <source>Multiple input method switcher</source>
        <translation>كۆپ خىل كىرگۈزگۈچ ئالماشتۇرغۇچ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Multiple input method switcher that uses the context menu of the text widgets</source>
        <translation>تېكىست ۋىكىپېدىيەلىرىنىڭ مەزمۇن تىزىملىكىنى ئىشلىتىدىغان كۆپ خىل كىرگۈزگۈچ ئالماشتۇرغۇچ</translation>
    </message>
</context>
<context>
    <name>QNativeSocketEngine</name>
    <message>
        <location filename="../src/network/socket/qnativesocketengine.cpp" line="+206"/>
        <source>The remote host closed the connection</source>
        <translation>يىراقتىكى رىياسەتچى ئۇلاشنى تاقىدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network operation timed out</source>
        <translation>تور مەشخۇلاتى ۋاقتى ئۇزىدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Out of resources</source>
        <translation>مەنبەدىن چىقىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unsupported socket operation</source>
        <translation>قوللىمايدىغان سوكېت مەشغۇلاتى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Protocol type not supported</source>
        <translation>كېلىشىم تىپى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid socket descriptor</source>
        <translation>ئىناۋەتسىز socket دېكتورى</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Network unreachable</source>
        <translation>تورغا چىققىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Permission denied</source>
        <translation>ئىجازەت ئىنكار قىلدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection timed out</source>
        <translation>ئۇلىنىش ۋاقتى ۋاقتسىز</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connection refused</source>
        <translation>ئۇلىنىش رەت قىلىندى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The bound address is already in use</source>
        <translation>باغلانغان ئادرېس ئىشلىتىلىپ بولدى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is not available</source>
        <translation>ئادرىسى يوق</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The address is protected</source>
        <translation>ئادرېس قوغدىلىدۇ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to send a message</source>
        <translation>ئۇچۇر يوللىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to receive a message</source>
        <translation>ئۇچۇر تاپشۇرۇۋالغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to write</source>
        <translation>يېزىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Network error</source>
        <translation>تور خاتالىقى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Another socket is already listening on the same port</source>
        <translation>يەنە بىر سوپۇن ئاللىقاچان ئوخشاش بىر پورتتا ئاڭلاۋاتىدۇ</translation>
    </message>
    <message>
        <location line="-66"/>
        <source>Unable to initialize non-blocking socket</source>
        <translation>توسۇمايدىغان سوكنى دەسلەپتە قوزغىتىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to initialize broadcast socket</source>
        <translation>ئاڭلىتىش دىسكىلىرىنى قوزغىتىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Attempt to use IPv6 socket on a platform with no IPv6 support</source>
        <translation>IPv6 قوللىغۇچىسى يوق سۇپىدا IPv6 socket نى ئىشلىتىشكە ئۇرۇنۇش</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Host unreachable</source>
        <translation>ساھىبخانغا ئۇلاشقىلى بولمايدىغان</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Datagram was too large to send</source>
        <translation>Datagram بەك چوڭ بولۇپ كەتسە يوللىسا بولماپتۇ</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Operation on non-socket</source>
        <translation>سوپىتسىز مەشخۇلات</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>The proxy type is invalid for this operation</source>
        <translation>proxy تۈرى بۇ مەشغۇلات ئۈچۈن ئىناۋەتسىز</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessCacheBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesscachebackend.cpp" line="+65"/>
        <source>Error opening %1</source>
        <translation>٪1 نى ئېچىش خاتالىقى</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFileBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessfilebackend.cpp" line="+99"/>
        <source>Request for opening non-local file %1</source>
        <translation>يەرلىك بولمىغان ھۆججەتنى ٪1 ئېچىشنى تەلەپ قىلىش</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error opening %1: %2</source>
        <translation>٪1 نى ئېچىش خاتالىقى: ٪2</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Write error writing to %1: %2</source>
        <translation>خاتالىق يېزىش ٪1: ٪2</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot open %1: Path is a directory</source>
        <translation>٪1 نى ئاچقىلى بولمايدۇ: يول مۇندەرىجە</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Read error reading from %1: %2</source>
        <translation>خاتالىقنى ٪1 دىن ئوقۇش: ٪2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessFtpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccessftpbackend.cpp" line="+165"/>
        <source>No suitable proxy found</source>
        <translation>ماس proxy تېپىلمىدى</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot open %1: is a directory</source>
        <translation>٪1 نى ئاچقىلى بولمايدۇ: مۇندەرىجە</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Logging in to %1 failed: authentication required</source>
        <translation>٪1 كە كىرىش مەغلۇپ بولدى: دەلىللەش زۆرۈر</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Error while downloading %1: %2</source>
        <translation>٪1 نى چۈشۈرگەندە خاتالىق كۆرۈلدى: ٪2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Error while uploading %1: %2</source>
        <translation>٪1 يۈكلەشتە خاتالىق كۆرۈلدى: ٪2</translation>
    </message>
</context>
<context>
    <name>QNetworkAccessHttpBackend</name>
    <message>
        <location filename="../src/network/access/qnetworkaccesshttpbackend.cpp" line="+597"/>
        <source>No suitable proxy found</source>
        <translation>ماس proxy تېپىلمىدى</translation>
    </message>
</context>
<context>
    <name>QNetworkReply</name>
    <message>
        <location line="+128"/>
        <source>Error downloading %1 - server replied: %2</source>
        <translation>٪1 نى چۈشۈرۈش خاتالىقى - مۇلازىمىتېر جاۋاب: ٪2</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkreplyimpl.cpp" line="+68"/>
        <source>Protocol &quot;%1&quot; is unknown</source>
        <translation>«٪1» كېلىشىمى نامەلۇم</translation>
    </message>
</context>
<context>
    <name>QNetworkReplyImpl</name>
    <message>
        <location line="+432"/>
        <location line="+22"/>
        <source>Operation canceled</source>
        <translation>مەشغۇلات ئەمەلدىن قالدۇرۇلدى</translation>
    </message>
</context>
<context>
    <name>QOCIDriver</name>
    <message>
        <location filename="../src/sql/drivers/oci/qsql_oci.cpp" line="+2069"/>
        <source>Unable to logon</source>
        <translation>كاشىلا چىقالمىدى</translation>
    </message>
    <message>
        <location line="-144"/>
        <source>Unable to initialize</source>
        <comment>QOCIDriver</comment>
        <translation>باشلانمىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+215"/>
        <source>Unable to begin transaction</source>
        <translation>سودىنى باشلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to rollback transaction</source>
        <translation>قايتۇرۇۋېلىش سودىسى</translation>
    </message>
</context>
<context>
    <name>QOCIResult</name>
    <message>
        <location line="-963"/>
        <location line="+161"/>
        <location line="+15"/>
        <source>Unable to bind column for batch execute</source>
        <translation>تۈركۈمگە بۆلۈپ ئىجرا قىلىش ئۈچۈن تۈۋرۈك باغلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to execute batch statement</source>
        <translation>تۈركۈم بايانىنى ئىجرا قىلغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+302"/>
        <source>Unable to goto next</source>
        <translation>كېيىنكى قەدەمدە تۇتالمىدىم</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Unable to alloc statement</source>
        <translation>alloc باياناتىنى دىگىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to prepare statement</source>
        <translation>بايانات تەييارلىغىلى بولماس</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unable to bind value</source>
        <translation>قىممىتىنى باغلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <source>Unable to execute select statement</source>
        <translation type="obsolete">不能执行选择语句</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to execute statement</source>
        <translation>باياناتنى ئىجرا قىلىشقا ئامالسىز</translation>
    </message>
</context>
<context>
    <name>QODBCDriver</name>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="+1785"/>
        <source>Unable to connect</source>
        <translation>ئۇلىنالمىدى</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Unable to connect - Driver doesn&apos;t support all needed functionality</source>
        <translation>ئۇلىنالمىدى - قوزغاتقان بارلىق زۆرۈر ئىقتىدارلارنى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>Unable to disable autocommit</source>
        <translation>ئاپتوماتىك يوللاشنى بىكار قىلغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback transaction</source>
        <translation>قايتۇرۇۋېلىش سودىسى</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to enable autocommit</source>
        <translation>ئاپتولنى يوللاشنى قوزغىتالمىدى</translation>
    </message>
</context>
<context>
    <name>QODBCResult</name>
    <message>
        <location line="-1218"/>
        <location line="+349"/>
        <source>QODBCResult::reset: Unable to set &apos;SQL_CURSOR_STATIC&apos; as statement attribute. Please check your ODBC driver configuration</source>
        <translation>QODBCResult::reset: &apos;SQL_CURSOR_STATIC&apos; نى بايانات خاسلىقىغا بەلگىلەشكە بولمايدۇ. ODBC شوپۇر سەپلىمىسىنى تەكشۈرۈپ بېقىڭ</translation>
    </message>
    <message>
        <location line="-332"/>
        <location line="+626"/>
        <source>Unable to execute statement</source>
        <translation>باياناتنى ئىجرا قىلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="-555"/>
        <source>Unable to fetch next</source>
        <translation>كېيىنكى قەدەمدە ئالغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+279"/>
        <source>Unable to prepare statement</source>
        <translation>بايانات تەييارلىغىلى بولماس</translation>
    </message>
    <message>
        <location line="+268"/>
        <source>Unable to bind variable</source>
        <translation>ئۆزگەرگۈچى مىقدارنى باغلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/db2/qsql_db2.cpp" line="+194"/>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-475"/>
        <location line="+578"/>
        <source>Unable to fetch last</source>
        <translation>ئاخىرقى قېتىم ئالغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/sql/drivers/odbc/qsql_odbc.cpp" line="-672"/>
        <source>Unable to fetch</source>
        <translation>ئەكەلگىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Unable to fetch first</source>
        <translation>ئاۋۋال ئالغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unable to fetch previous</source>
        <translation>بۇرۇننى ئالغىلى بولمايدۇ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/gui/util/qdesktopservices_mac.cpp" line="+165"/>
        <source>Home</source>
        <translation>ئۆي</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdatabackend.cpp" line="+74"/>
        <source>Operation not supported on %1</source>
        <translation>٪1 دا مەشغۇلات قوللىمىدى</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid URI: %1</source>
        <translation>ئىناۋەتسىز URI: ٪1</translation>
    </message>
    <message>
        <location filename="../src/network/access/qnetworkaccessdebugpipebackend.cpp" line="+175"/>
        <source>Write error writing to %1: %2</source>
        <translation>خاتالىق يېزىش ٪1: ٪2</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Read error reading from %1: %2</source>
        <translation>خاتالىقنى ٪1 دىن ئوقۇش: ٪2</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Socket error on %1: %2</source>
        <translation>٪1 دا socket خاتالىقى: ٪2</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remote host closed the connection prematurely on %1</source>
        <translation>يىراقتىن رىياسەتچى ٪1 نىڭ ئۇلىنىشنى بالدۇر يېپىپ قالدى</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Protocol error: packet of size 0 received</source>
        <translation>كېلىشىم خاتالىقى: 0 چوڭلۇقتىكى بوغچىسى تاپشۇرۇۋېلىندى</translation>
    </message>
    <message>
        <location filename="../src/network/kernel/qhostinfo.cpp" line="+177"/>
        <location line="+57"/>
        <source>No host name given</source>
        <translation>رىياسەتچى ئىسمى بېرىلمىدى</translation>
    </message>
</context>
<context>
    <name>QPPDOptionsModel</name>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="+1195"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>قىممەت</translation>
    </message>
</context>
<context>
    <name>QPSQLDriver</name>
    <message>
        <location filename="../src/sql/drivers/psql/qsql_psql.cpp" line="+763"/>
        <source>Unable to connect</source>
        <translation>ئۇلىنالمىدى</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Could not begin transaction</source>
        <translation>سودىنى باشلىغىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Could not commit transaction</source>
        <translation>سودا قىلالمىدى</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Could not rollback transaction</source>
        <translation>قايتۇرۇۋىلىش سودىسى بولمىدى</translation>
    </message>
    <message>
        <location line="+358"/>
        <source>Unable to subscribe</source>
        <translation>مۇشتەرى بولالمىدى</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Unable to unsubscribe</source>
        <translation>مۇشتەرىلىكتىن ئايرىلالمىدى</translation>
    </message>
</context>
<context>
    <name>QPSQLResult</name>
    <message>
        <location line="-1058"/>
        <source>Unable to create query</source>
        <translation>سۈرۈشتۈرۈشنى قۇرغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+374"/>
        <source>Unable to prepare statement</source>
        <translation>بايانات تەييارلىغىلى بولماس</translation>
    </message>
</context>
<context>
    <name>QPageSetupWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="+304"/>
        <source>Centimeters (cm)</source>
        <translation>سانتىمېتىر (cm)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Millimeters (mm)</source>
        <translation>مىللىمېتىر (mm)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Inches (in)</source>
        <translation>دىسكاز (in)</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Points (pt)</source>
        <translation>نۇقتىلار (pt)</translation>
    </message>
    <message>
        <source>Pica (P̸)</source>
        <translation>پىكا (P̸)</translation>
    </message>
    <message>
        <source>Didot (DD)</source>
        <translation>دىدوت (DD)</translation>
    </message>
    <message>
        <source>Cicero (CC)</source>
        <translation>چەرچەن (CC)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupwidget.ui"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location/>
        <source>Paper</source>
        <translation>قەغەز</translation>
    </message>
    <message>
        <location/>
        <source>Page size:</source>
        <translation>بەت چوڭلۇقى:</translation>
    </message>
    <message>
        <location/>
        <source>Width:</source>
        <translation>كەڭلىكى:</translation>
    </message>
    <message>
        <location/>
        <source>Height:</source>
        <translation>ئېگىزلىكى:</translation>
    </message>
    <message>
        <location/>
        <source>Paper source:</source>
        <translation>قەغەز مەنبەسى:</translation>
    </message>
    <message>
        <location/>
        <source>Orientation</source>
        <translation>يۆلىنىش</translation>
    </message>
    <message>
        <location/>
        <source>Portrait</source>
        <translation>پورترېت</translation>
    </message>
    <message>
        <location/>
        <source>Landscape</source>
        <translation>مەنزىرە رايونى</translation>
    </message>
    <message>
        <location/>
        <source>Reverse landscape</source>
        <translation>تەتۈر مەنزىرە</translation>
    </message>
    <message>
        <location/>
        <source>Reverse portrait</source>
        <translation>تەتۈر پورترېت</translation>
    </message>
    <message>
        <location/>
        <source>Margins</source>
        <translation>Margins</translation>
    </message>
    <message>
        <location/>
        <source>top margin</source>
        <translation>يۇقىرى ئۈستۈنلۈك</translation>
    </message>
    <message>
        <location/>
        <source>left margin</source>
        <translation>سول margin</translation>
    </message>
    <message>
        <location/>
        <source>right margin</source>
        <translation>ئوڭ ئۈستۈنلۈك</translation>
    </message>
    <message>
        <location/>
        <source>bottom margin</source>
        <translation>تۆۋەن چېكى</translation>
    </message>
    <message>
        <location/>
        <source>Page Layout</source>
        <translation>بەت بەت ئورۇنلاشتۇرۇشى</translation>
    </message>
    <message>
        <location/>
        <source>Pages per sheet:</source>
        <translation>بەتلەر ھەر ۋاراقتا:</translation>
    </message>
    <message>
        <location/>
        <source>Page order:</source>
        <translation>بەت تەرتىپى:</translation>
    </message>
</context>
<context>
    <name>QCupsJobWidget</name>
    <message>
        <location/>
        <source>Job Control</source>
        <translation>خىزمەت كونترول قىلىش</translation>
    </message>
    <message>
        <location/>
        <source>Scheduled printing:</source>
        <translation>بەلگىلەنگەن باسما:</translation>
    </message>
    <message>
        <location/>
        <source>Print Immediately</source>
        <translation>دەرھال بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location/>
        <source>Hold Indefinitely</source>
        <translation>مۇددەتسىز تۇتۇپ تۇرۇش</translation>
    </message>
    <message>
        <location/>
        <source>Day (06:00 to 17:59)</source>
        <translation>كۈنى (06:00 - 17:59)</translation>
    </message>
    <message>
        <location/>
        <source>Night (18:00 to 05:59)</source>
        <translation>كېچە (18:00 - 05:59)</translation>
    </message>
    <message>
        <location/>
        <source>Second Shift (16:00 to 23:59)</source>
        <translation>ئىككىنچى ئىسمېنا (16:00 - 23:59)</translation>
    </message>
    <message>
        <location/>
        <source>Third Shift (00:00 to 07:59)</source>
        <translation>ئۈچىنچى ئىسمېنا (00:00 - 07:59)</translation>
    </message>
    <message>
        <location/>
        <source>Weekend (Saturday to Sunday)</source>
        <translation>ھەپتە ئاخىرىدا (شەنبەدىن يەكشەنبىگىچە)</translation>
    </message>
    <message>
        <location/>
        <source>Specific Time</source>
        <translation>كونكرېت ۋاقتى</translation>
    </message>
    <message>
        <location/>
        <source>Billing information:</source>
        <translation>ھىسابات ئۇچۇرلىرى:</translation>
    </message>
    <message>
        <location/>
        <source>Job priority:</source>
        <translation>خىزمەت ئاددىسى:</translation>
    </message>
    <message>
        <location/>
        <source>Banner Pages</source>
        <translation>&quot;Banner&quot; تۈردىكى بەتلەر</translation>
    </message>
    <message>
        <location/>
        <source>Start:</source>
        <translation>باشلىنىش:</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>يوق</translation>
    </message>
    <message>
        <location/>
        <source>Standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <location/>
        <source>Unclassified</source>
        <translation>كىنىشكىسىز</translation>
    </message>
    <message>
        <location/>
        <source>Confidential</source>
        <translation>مەخپىيەتلىك</translation>
    </message>
    <message>
        <location/>
        <source>Classified</source>
        <translation>تۈرگە ئايرىش</translation>
    </message>
    <message>
        <location/>
        <source>Secret</source>
        <translation>مەخپىيەتلىك</translation>
    </message>
    <message>
        <location/>
        <source>Top Secret</source>
        <translation>ئەڭ مەخپىيەتلىك</translation>
    </message>
    <message>
        <location/>
        <source>End:</source>
        <translation>ئاخىر:</translation>
    </message>
</context>
<context>
    <name>QPluginLoader</name>
    <message>
        <location filename="../src/corelib/plugin/qpluginloader.cpp" line="+24"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>The plugin was not loaded.</source>
        <translation>قىستۇرما قاچىلانمىدى.</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <location filename="../src/gui/painting/qprinterinfo_unix.cpp" line="+98"/>
        <source>locally connected</source>
        <translation>يەرلىك تۇتاشتۇرۇلغان</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+225"/>
        <source>Aliases: %1</source>
        <translation>باشقا ئىسىملار: ٪1</translation>
    </message>
    <message>
        <location line="+223"/>
        <location line="+199"/>
        <source>unknown</source>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="+375"/>
        <source>Print all</source>
        <translation>ھەممە بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print selection</source>
        <translation>بېسىپ چىقىرىش تاللانما</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print range</source>
        <translation>بېسىپ چىقىرىش دائىرىسى</translation>
    </message>
    <message>
        <location line="-48"/>
        <source>A0 (841 x 1189 mm)</source>
        <translation>A0 (841 × 1189 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1 (594 x 841 mm)</source>
        <translation>A1 (594 × 841 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2 (420 x 594 mm)</source>
        <translation>A2 (420 × 594 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3 (297 x 420 mm)</source>
        <translation>A3 (297 × 420 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4 (210 x 297 mm, 8.26 x 11.7 inches)</source>
        <translation>A4 (210 × 297 مىللىمېتىر، 8.26 × 11.7 دىيۇملۇق)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5 (148 x 210 mm)</source>
        <translation>A5 (148 × 210 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6 (105 x 148 mm)</source>
        <translation>A6 (105 × 148 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7 (74 x 105 mm)</source>
        <translation>A7 (74 × 105 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8 (52 x 74 mm)</source>
        <translation>A8 (52 × 74 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9 (37 x 52 mm)</source>
        <translation>A9 (37 × 52 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0 (1000 x 1414 mm)</source>
        <translation>B0 (1000 × 1414 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1 (707 x 1000 mm)</source>
        <translation>B1 (707 × 1000 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2 (500 x 707 mm)</source>
        <translation>B2 (500 × 707 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3 (353 x 500 mm)</source>
        <translation>B3 (353 × 500 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4 (250 x 353 mm)</source>
        <translation>B4 (250 × 353 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5 (176 x 250 mm, 6.93 x 9.84 inches)</source>
        <translation>B5 (176 × 250 مىللىمېتىر، 6.93 × 9.84 ديۇملۇق)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6 (125 x 176 mm)</source>
        <translation>B6 (125 × 176 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7 (88 x 125 mm)</source>
        <translation>B7 (88 × 125 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8 (62 x 88 mm)</source>
        <translation>B8 (62 × 88 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9 (44 x 62 mm)</source>
        <translation>B9 (44 × 62 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10 (31 x 44 mm)</source>
        <translation>B10 (31 × 44 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E (163 x 229 mm)</source>
        <translation>C5E (163 × 229 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE (110 x 220 mm)</source>
        <translation>DLE (110 × 220 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive (7.5 x 10 inches, 191 x 254 mm)</source>
        <translation>ئىجرائىيە ئەمەلدارى (7.5 × 10 ديۇم، 191 × 254 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (210 x 330 mm)</source>
        <translation>فولىئو (210 × 330 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger (432 x 279 mm)</source>
        <translation>لىدېر (432 × 279 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal (8.5 x 14 inches, 216 x 356 mm)</source>
        <translation>قانۇنچىلىق (8.5 × 14 ديۇم، 216 × 356 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter (8.5 x 11 inches, 216 x 279 mm)</source>
        <translation>ھەرپ (8.5 × 11 ديۇم، 216 × 279 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid (279 x 432 mm)</source>
        <translation>مەتتا (279 × 432 مىللىمېتىر)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope (105 x 241 mm)</source>
        <translation>ئامېرىكا ئورتاق #10 كونۋېرت (105 × 241 مىللىمېتىر)</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+268"/>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qabstractprintdialog.cpp" line="+110"/>
        <location line="+13"/>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="-2"/>
        <source>Print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-357"/>
        <source>Print To File ...</source>
        <translation>ھۆججەتكە بېسىپ چىقىرىش ...</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>File %1 is not writable.
Please choose a different file name.</source>
        <translation>٪1 ھۆججىتى يېزىلمىدى.
ئوخشاش بولمىغان ھۆججەت نامىنى تاللاڭ.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 already exists.
Do you want to overwrite it?</source>
        <translation>٪1 ئاللىقاچان مەۋجۇت.
سىز ئۇنى ھەددىدىن زىيادە يېزىپ قويامسىز؟</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_qws.cpp" line="-210"/>
        <source>File exists</source>
        <translation>ھۆججەت مەۋجۇت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;qt&gt;Do you want to overwrite it?&lt;/qt&gt;</source>
        <translation>&lt;qt&gt; سىز ئۇنى ھەددىدىن زىيادە يېزىپ قويامسىز؟ &lt;/qt&gt;</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-8"/>
        <source>%1 is a directory.
Please choose a different file name.</source>
        <translation>٪1 بولسا مۇندەرىجە.
ئوخشاش بولمىغان ھۆججەت نامىنى تاللاڭ.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_win.cpp" line="+1"/>
        <source>The &apos;From&apos; value cannot be greater than the &apos;To&apos; value.</source>
        <translation>&apos;From&apos; قىممىتى &apos;تو&apos; قىممىتىدىن چوڭ بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qpagesetupdialog_unix.cpp" line="-232"/>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A8</source>
        <translation>A8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C5E</source>
        <translation>C5E</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DLE</source>
        <translation>DLE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Executive</source>
        <translation>ئىجرائىيە ئەمەلدارى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio</source>
        <translation>Folio</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ledger</source>
        <translation>لىدېر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Legal</source>
        <translation>قانۇن-قانۇن</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Letter</source>
        <translation>خىتتاي</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tabloid</source>
        <translation>ژىگىت-ژىگىت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>US Common #10 Envelope</source>
        <translation>ئامېرىكا ئورتاق #10 كونۋېرت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Folio (8.27 x 13 in)</source>
        <translation>فولىئو (8.27 × 13 in)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom</source>
        <translation>ئۆرپ-ئادەت</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintdialog_unix.cpp" line="-522"/>
        <location line="+68"/>
        <source>&amp;Options &gt;&gt;</source>
        <translation>&gt;&gt; تاللانمىلار</translation>
    </message>
    <message>
        <location line="-63"/>
        <source>&amp;Print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>&amp;Options &lt;&lt;</source>
        <translation>تاللانمىلار &lt;&lt;</translation>
    </message>
    <message>
        <location line="+253"/>
        <source>Print to File (PDF)</source>
        <translation>ھۆججەتكە بېسىپ چىقىرىش (PDF)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print to File (Postscript)</source>
        <translation>ھۆججەتكە بېسىپ چىقىرىش (يازماscript)</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Local file</source>
        <translation>يەرلىك ھۆججەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Write %1 file</source>
        <translation>٪1 ھۆججەت يېزىش</translation>
    </message>
</context>
<context>
    <name>QPrintPreviewDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qabstractpagesetupdialog.cpp" line="+68"/>
        <location line="+12"/>
        <source>Page Setup</source>
        <translation>بەت تەڭشەش</translation>
    </message>
    <message>
        <location filename="../src/gui/dialogs/qprintpreviewdialog.cpp" line="+252"/>
        <source>%1%</source>
        <translation>%1%</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>Print Preview</source>
        <translation>بېسىپ چىقىرىش كۆرۈنمە يۈزى</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Next page</source>
        <translation>كېيىنكى بەت &gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous page</source>
        <translation>ئالدىنقى بەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First page</source>
        <translation>بىرىنجى بەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last page</source>
        <translation>ئاخىرقى بەت</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Fit width</source>
        <translation>Fit كەڭلىكى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Fit page</source>
        <translation>ماس بەت</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Zoom in</source>
        <translation>چوڭايت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom out</source>
        <translation>چوڭايتماق</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Portrait</source>
        <translation>پورترېت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Landscape</source>
        <translation>مەنزىرە رايونى</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Show single page</source>
        <translation>يەككە بەتنى كۆرسەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show facing pages</source>
        <translation>يۈز بەتلەرنى كۆرسىتىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show overview of all pages</source>
        <translation>بارلىق بەتلەرنىڭ قىسقىچە چۈشەندۈرۈشىنى كۆرسىتىش</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page setup</source>
        <translation>بەت تەڭشەش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Export to PDF</source>
        <translation>PDF غا ئېكسپورت قىلىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Export to PostScript</source>
        <translation>PostScript گە ئېكسپورت قىلىش</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintpropertieswidget.ui"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location/>
        <source>Page</source>
        <translation>بەت</translation>
    </message>
    <message>
        <location/>
        <source>Advanced</source>
        <translation>ئىلغار</translation>
    </message>
</context>
<context>
    <name>QPrintSettingsOutput</name>
    <message>
        <location filename="../src/gui/dialogs/qprintsettingsoutput.ui"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location/>
        <source>Copies</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location/>
        <source>Print range</source>
        <translation>بېسىپ چىقىرىش دائىرىسى</translation>
    </message>
    <message>
        <location/>
        <source>Print all</source>
        <translation>ھەممە بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location/>
        <source>Pages from</source>
        <translation>&quot;بەتلەر&quot; دىن</translation>
    </message>
    <message>
        <location/>
        <source>to</source>
        <translation>تو</translation>
    </message>
    <message>
        <location/>
        <source>Pages</source>
        <translation>&quot;بەتلەر&quot;</translation>
    </message>
    <message>
        <location/>
        <source>Page Set:</source>
        <translation>بەت توپلىمى:</translation>
    </message>
    <message>
        <location/>
        <source>Selection</source>
        <translation>تاللانما</translation>
    </message>
    <message>
        <location/>
        <source>Output Settings</source>
        <translation>چىقىرىش تەڭشەكلىرى</translation>
    </message>
    <message>
        <location/>
        <source>Copies:</source>
        <translation>كۆچۈرۈلگەنلەر:</translation>
    </message>
    <message>
        <location/>
        <source>Collate</source>
        <translation>كوللاگېن</translation>
    </message>
    <message>
        <location/>
        <source>Reverse</source>
        <translation>تەتۈر</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>تاللانمىلار</translation>
    </message>
    <message>
        <location/>
        <source>Color Mode</source>
        <translation>رەڭ شەكلى</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>رەڭ</translation>
    </message>
    <message>
        <location/>
        <source>Grayscale</source>
        <translation>كۈل رەڭ</translation>
    </message>
    <message>
        <location/>
        <source>Duplex Printing</source>
        <translation>قوش قوش تىللىق باسما</translation>
    </message>
    <message>
        <location/>
        <source>None</source>
        <translation>يوق</translation>
    </message>
    <message>
        <location/>
        <source>Long side</source>
        <translation>ئۇزۇن يان</translation>
    </message>
    <message>
        <location/>
        <source>Short side</source>
        <translation>قىسقا يان</translation>
    </message>
</context>
<context>
    <name>QPrintWidget</name>
    <message>
        <location filename="../src/gui/dialogs/qprintwidget.ui"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>&amp; نامى:</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;roperties</source>
        <translation>P&amp; ئارغامچا</translation>
    </message>
    <message>
        <location/>
        <source>Location:</source>
        <translation>ئورنى:</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>كۆرۈنمە يۈزى</translation>
    </message>
    <message>
        <location/>
        <source>Type:</source>
        <translation>تۈرى:</translation>
    </message>
    <message>
        <location/>
        <source>Output &amp;file:</source>
        <translation>چىقىرىش &amp; ھۆججەت:</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>...</translation>
    </message>
</context>
<context>
    <name>QProcess</name>
    <message>
        <location filename="../src/corelib/io/qprocess_unix.cpp" line="+475"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+147"/>
        <source>Could not open input redirection for reading</source>
        <translation>ئوقۇش ئۈچۈن كىرگۈزۈشنى قايتا يوللاشنى ئاچالمىدى</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+36"/>
        <source>Could not open output redirection for writing</source>
        <translation>يېزىش ئۈچۈن چىقىرىش قايتا يوللاشنى ئاچالمىدى</translation>
    </message>
    <message>
        <location line="+235"/>
        <source>Resource error (fork failure): %1</source>
        <translation>بايلىق خاتالىقى (fork failure): ٪1</translation>
    </message>
    <message>
        <location line="+259"/>
        <location line="+53"/>
        <location line="+74"/>
        <location line="+67"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+422"/>
        <location line="+50"/>
        <location line="+75"/>
        <location line="+42"/>
        <location line="+54"/>
        <source>Process operation timed out</source>
        <translation>جەريان مەشخۇلات ۋاقتى ئۇزىدى</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess.cpp" line="+533"/>
        <location line="+52"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-211"/>
        <location line="+50"/>
        <source>Error reading from process</source>
        <translation>جەرياندىن كىتاب ئوقۇش خاتالىقى</translation>
    </message>
    <message>
        <location line="+47"/>
        <location line="+779"/>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="+140"/>
        <source>Error writing to process</source>
        <translation>يېزىشنى بىر تەرەپ قىلىش خاتالىقى</translation>
    </message>
    <message>
        <location line="-709"/>
        <source>Process crashed</source>
        <translation>جەريانى چۈشۈپ كەتتى</translation>
    </message>
    <message>
        <location line="+912"/>
        <source>No program defined</source>
        <translation>بەلگىلەنگەن پروگرامما يوق</translation>
    </message>
    <message>
        <location filename="../src/corelib/io/qprocess_win.cpp" line="-341"/>
        <source>Process failed to start</source>
        <translation>جەريان باشلانمىدى</translation>
    </message>
</context>
<context>
    <name>QProgressDialog</name>
    <message>
        <location filename="../src/gui/dialogs/qprogressdialog.cpp" line="+182"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
</context>
<context>
    <name>QPushButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="-8"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
</context>
<context>
    <name>QRadioButton</name>
    <message>
        <location line="+12"/>
        <source>Check</source>
        <translation>تەكشۈرۈش</translation>
    </message>
</context>
<context>
    <name>QRegExp</name>
    <message>
        <location filename="../src/corelib/tools/qregexp.cpp" line="+64"/>
        <source>no error occurred</source>
        <translation>خاتالىق يۈز بەرمىدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>disabled feature used</source>
        <translation>ئىشلىتىلگەن چەكلەنگەن ئىقتىدار</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad char class syntax</source>
        <translation>ناچار char سىنىپى سىنتاكسىس</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad lookahead syntax</source>
        <translation>ناچار قارا كۆزلۈك سىنتاكسىسلار</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>bad repetition syntax</source>
        <translation>ناچار تەكرارلاش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid octal value</source>
        <translation>ئىناۋەتسىز سەككىزلىك قىممەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>missing left delim</source>
        <translation>يوقاپ كەتكەن سول دېلىم</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end</source>
        <translation>كۈتۈلمىگەن ئاخىرلىشىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>met internal limit</source>
        <translation>ئىچكى چەكنى قاندۇردى</translation>
    </message>
</context>
<context>
    <name>QSQLite2Driver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite2/qsql_sqlite2.cpp" line="+396"/>
        <source>Error to open database</source>
        <translation>سانداننى ئېچىش خاتالىقى</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to begin transaction</source>
        <translation>سودىنى باشلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unable to rollback Transaction</source>
        <translation>قايتۇرغىلى بولمايدىغان سودا</translation>
    </message>
</context>
<context>
    <name>QSQLite2Result</name>
    <message>
        <location line="-323"/>
        <source>Unable to fetch results</source>
        <translation>نەتىجە ئېلىپ كەلگىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+147"/>
        <source>Unable to execute statement</source>
        <translation>باياناتنى ئىجرا قىلىشقا ئامالسىز</translation>
    </message>
</context>
<context>
    <name>QSQLiteDriver</name>
    <message>
        <location filename="../src/sql/drivers/sqlite/qsql_sqlite.cpp" line="+528"/>
        <source>Error opening database</source>
        <translation>سانداننى ئېچىش خاتالىقى</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error closing database</source>
        <translation>سانداننى تاقاش خاتالىقى</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to begin transaction</source>
        <translation>سودىنى باشلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to commit transaction</source>
        <translation>سودىنى بىجىرەلمەسلىك</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unable to rollback transaction</source>
        <translation>قايتۇرۇۋېلىش سودىسى</translation>
    </message>
</context>
<context>
    <name>QSQLiteResult</name>
    <message>
        <location line="-400"/>
        <location line="+66"/>
        <location line="+8"/>
        <source>Unable to fetch row</source>
        <translation>قۇرنى ئېلىۋېلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Unable to execute statement</source>
        <translation>باياناتنى ئىجرا قىلىشقا ئامالسىز</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Unable to reset statement</source>
        <translation>باياناتنى قايتا باشلىيالمىدۇق</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Unable to bind parameters</source>
        <translation>پارامېتىرلارنى باغلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Parameter count mismatch</source>
        <translation>پارامېتىر سانى ماسلاشماسلىق</translation>
    </message>
    <message>
        <location line="-208"/>
        <source>No query</source>
        <translation>سۈرۈشتۈرۈش يوق</translation>
    </message>
</context>
<context>
    <name>QScrollBar</name>
    <message>
        <location filename="../src/gui/widgets/qscrollbar.cpp" line="+448"/>
        <source>Scroll here</source>
        <translation>بۇ يەرنى سىيرىل</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Left edge</source>
        <translation>سول گىرۋىكى</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>ئەڭ ئۈستى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>ئوڭ گىرۋىكى</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>ئاستىنقى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page left</source>
        <translation>بەت سول بەت</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+143"/>
        <source>Page up</source>
        <translation>بەت قىلىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>بەت ئوڭ</translation>
    </message>
    <message>
        <location line="+0"/>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+4"/>
        <source>Page down</source>
        <translation>بەتنى تۆۋەن قىلىش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scroll left</source>
        <translation>سولغا سىيرىلماق</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>سىيرىلما</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>ئوڭغا سىيرىلماق</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>سىيرىلماق</translation>
    </message>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-6"/>
        <source>Line up</source>
        <translation>ئۆچرەتتە تۇرۇش</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Position</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Line down</source>
        <translation>تىزىلغان سىزىق</translation>
    </message>
</context>
<context>
    <name>QSharedMemory</name>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory.cpp" line="+207"/>
        <source>%1: unable to set key on lock</source>
        <translation>٪1: ئاچقۇچنى قۇلۇپقا بەلگىلەشكە ئامالسىز</translation>
    </message>
    <message>
        <location line="+81"/>
        <source>%1: create size is less then 0</source>
        <translation>٪1: يارىتىش كۆلىمى ئاز بولسا 0</translation>
    </message>
    <message>
        <location line="+168"/>
        <location filename="../src/corelib/kernel/qsharedmemory_p.h" line="+148"/>
        <source>%1: unable to lock</source>
        <translation>٪1: قۇلۇپلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1: unable to unlock</source>
        <translation>٪1: قۇلۇپ ئېچىشقا ئامالسىز</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_unix.cpp" line="+78"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+87"/>
        <source>%1: permission denied</source>
        <translation>٪1: ئىجازەت رەت قىلىنىدۇ</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-22"/>
        <source>%1: already exists</source>
        <translation>٪1: ئاللىبۇرۇن مەۋجۇت</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: doesn&apos;t exists</source>
        <translation>٪1: مەۋجۇت ئەمەس</translation>
    </message>
    <message>
        <location line="+6"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+9"/>
        <source>%1: out of resources</source>
        <translation>٪1: بايلىق قالمىدى</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+7"/>
        <source>%1: unknown error %2</source>
        <translation>٪1: نامەلۇم خاتالىق ٪2</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>%1: key is empty</source>
        <translation>٪1: ئاچقۇچ بوش</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1: unix key file doesn&apos;t exists</source>
        <translation>٪1: unix كونۇپكا ھۆججىتى مەۋجۇت ئەمەس</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1: ftok failed</source>
        <translation>٪1: ftok مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location line="+51"/>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="+15"/>
        <source>%1: unable to make key</source>
        <translation>٪1: ئاچقۇچ ياساپ بولالمىدى</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1: system-imposed size restrictions</source>
        <translation>٪1: سىستېما قويغان چوڭلۇق چەكلىمىسى</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1: not attached</source>
        <translation>٪1: قوشۇلمىدى</translation>
    </message>
    <message>
        <location filename="../src/corelib/kernel/qsharedmemory_win.cpp" line="-27"/>
        <source>%1: invalid size</source>
        <translation>٪1: ئىناۋەتسىز چوڭلۇق</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>%1: key error</source>
        <translation>٪1: ئاچقۇچلۇق خاتالىق</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>%1: size query failed</source>
        <translation>٪1: چوڭلۇقتىكى سۈرۈشتۈرۈش مەغلۇپ بولدى</translation>
    </message>
</context>
<context>
    <name>QShortcut</name>
    <message>
        <location filename="../src/gui/kernel/qkeysequence.cpp" line="+373"/>
        <source>Space</source>
        <translation>ئالەم بوشلۇقى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Esc</source>
        <translation>Esc</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab</source>
        <translation>تاختا</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backtab</source>
        <translation>كەينىگە چېكىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backspace</source>
        <translation>ئارقا بوشلۇق</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Return</source>
        <translation>قايتىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter</source>
        <translation>كىر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ins</source>
        <translation>Ins</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pause</source>
        <translation>توختاش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SysReq</source>
        <translation>SysReq</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Home</source>
        <translation>ئۆي</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>End</source>
        <translation>ئاخىرلىشىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Left</source>
        <translation>سول</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Up</source>
        <translation>يۇقىرىغا</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right</source>
        <translation>توغرىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Down</source>
        <translation>تۆۋەنگە</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgUp</source>
        <translation>PgUp</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PgDown</source>
        <translation>PgDown</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CapsLock</source>
        <translation>CapsLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NumLock</source>
        <translation>NumLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ScrollLock</source>
        <translation>ScrollLock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Menu</source>
        <translation>تىزىملىك</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Back</source>
        <translation>كەينىگە يېنىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward</source>
        <translation>ئالدىغا</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop</source>
        <translation>توختاش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Refresh</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Down</source>
        <translation>ئاۋازنى تۆۋەنگە چۈشۈر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Mute</source>
        <translation>ئاۋاز چوڭلۇقى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Volume Up</source>
        <translation>ئاۋاز چوڭلۇقى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Boost</source>
        <translation>Bass Boost</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Up</source>
        <translation>Bass Up</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bass Down</source>
        <translation>باسى پەس</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Up</source>
        <translation>تىترىمەك</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Treble Down</source>
        <translation>تىترەپ چۈشۈش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Play</source>
        <translation>Media Play</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Stop</source>
        <translation>Media Stop</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Previous</source>
        <translation>مېدىيا ئالدىنقى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Next</source>
        <translation>كېيىنكى مېدىيا</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Media Record</source>
        <translation>مېدىيا خاتىرىسى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Favorites</source>
        <translation>ياخشى كۆرىدىغانلار</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search</source>
        <translation>ئىزدە</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Standby</source>
        <translation>Standby</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>URL نى ئېچىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Mail</source>
        <translation>پوچتا يوللانمىسىنى قوزغىتىۋىتىلدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch Media</source>
        <translation>Media نى قوزغىتىۋىتىلدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (0)</source>
        <translation>قوزغىتىشى (0)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (1)</source>
        <translation>قويۇپ بېرىش (1)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (2)</source>
        <translation>قويۇپ بېرىش (2)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (3)</source>
        <translation>قويۇپ بېرىش (3)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (4)</source>
        <translation>قوزغىتىشى (4)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (5)</source>
        <translation>قويۇپ بېرىش (5)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (6)</source>
        <translation>قويۇپ بېرىش (6)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (7)</source>
        <translation>قويۇپ بېرىش (7)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (8)</source>
        <translation>قويۇپ بېرىش (8)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (9)</source>
        <translation>قويۇپ بېرىش (9)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (A)</source>
        <translation>قويۇپ بېرىش (A)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (B)</source>
        <translation>قويۇپ بېرىش (ب)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (C)</source>
        <translation>قويۇپ بېرىش (C)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (D)</source>
        <translation>قويۇپ بېرىش (D)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (E)</source>
        <translation>قويۇپ بېرىش (E)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Launch (F)</source>
        <translation>قويۇپ بېرىش (F)</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Print Screen</source>
        <translation>بېسىپ چىقىرىش ئېكرانى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Up</source>
        <translation>بەت Up</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page Down</source>
        <translation>بەتنى تۆۋەن قىلىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Caps Lock</source>
        <translation>چوڭ يېزىق قۇلۇبى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Num Lock</source>
        <translation>Num Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Number Lock</source>
        <translation>نومۇر قۇلۇپى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll Lock</source>
        <translation>Scroll Lock</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Insert</source>
        <translation>قىستۇرۇش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Escape</source>
        <translation>تۈرمىدىن قېچىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System Request</source>
        <translation>سىستېما ئىلتىماسى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Select</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Context1</source>
        <translation>1- مەزمۇن</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context2</source>
        <translation>2- مەزمۇن</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context3</source>
        <translation>3- مەزمۇن</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Context4</source>
        <translation>4- مەزمۇن</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Call</source>
        <translation>دەۋەت قىلىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hangup</source>
        <translation>ئېسىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip</source>
        <translation>ئۆركەش</translation>
    </message>
    <message>
        <location line="+527"/>
        <location line="+122"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+125"/>
        <source>Shift</source>
        <translation>Shift</translation>
    </message>
    <message>
        <location line="-124"/>
        <location line="+122"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+117"/>
        <source>Meta</source>
        <translation>مېتا</translation>
    </message>
    <message>
        <location line="-25"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>F%1</source>
        <translation>F٪ 1</translation>
    </message>
    <message>
        <location line="-720"/>
        <source>Home Page</source>
        <translation>باش بەت &gt;</translation>
    </message>
</context>
<context>
    <name>QSlider</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="+151"/>
        <source>Page left</source>
        <translation>بەت سول بەت</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>بەت قىلىش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Position</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page right</source>
        <translation>بەت ئوڭ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>بەتنى تۆۋەن قىلىش</translation>
    </message>
</context>
<context>
    <name>QSocks5SocketEngine</name>
    <message>
        <location filename="../src/network/socket/qsocks5socketengine.cpp" line="-67"/>
        <source>Connection to proxy refused</source>
        <translation>proxy ئۇلىنىش رەت قىلىندى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection to proxy closed prematurely</source>
        <translation>Proxy غا ئۇلىنىش بالدۇر تاقىلىپ قالدى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Proxy host not found</source>
        <translation>Proxy نىڭ رىياسەتچىسى تېپىلمىدى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Connection to proxy timed out</source>
        <translation>Proxy غا ئۇلاش ۋاقتى توشمىدى</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Proxy authentication failed</source>
        <translation>Proxy دەلىللەش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Proxy authentication failed: %1</source>
        <translation>Proxy دەلىللەش مەغلۇپ بولدى:٪1</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>SOCKS version 5 protocol error</source>
        <translation>SOCKS نەشرى 5 كېلىشىم خاتالىقى</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>General SOCKSv5 server failure</source>
        <translation>ئادەتتىكى SOCKSv5 مۇلازىمىتېر مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Connection not allowed by SOCKSv5 server</source>
        <translation>SOCKSv5 مۇلازىمېتىرى ئارقىلىق ئۇلىنىش قىلىشقا بولمايدۇ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>TTL expired</source>
        <translation>TTL نىڭ ۋاقتى ئۆتۈپ كەتتى</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>SOCKSv5 command not supported</source>
        <translation>SOCKSv5 بۇيرۇقى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address type not supported</source>
        <translation>ئادرېس تىپى قوللىمايدۇ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unknown SOCKSv5 proxy error code 0x%1</source>
        <translation>نامەلۇم SOCKSv5 proxy خاتالىق كودى 0x٪1</translation>
    </message>
    <message>
        <source>Socks5 timeout error connecting to socks server</source>
        <translation type="obsolete">连接到套接字服务器的时候，Socks5超时错误</translation>
    </message>
    <message>
        <location line="+685"/>
        <source>Network operation timed out</source>
        <translation>تور مەشخۇلاتى ۋاقتى ئۇزىدى</translation>
    </message>
</context>
<context>
    <name>QSpinBox</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/rangecontrols.cpp" line="-574"/>
        <source>More</source>
        <translation>تېخىمۇ كۆپ</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Less</source>
        <translation>ئاز</translation>
    </message>
</context>
<context>
    <name>QSql</name>
    <message>
        <location filename="../src/qt3support/sql/q3sqlmanager_p.cpp" line="+890"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete this record?</source>
        <translation>بۇ خاتىرىنى ئۆچۈرۋېتەمدۇ؟</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location line="-51"/>
        <location line="+16"/>
        <location line="+36"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location line="-44"/>
        <source>Insert</source>
        <translation>قىستۇرۇش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Update</source>
        <translation>يېڭىلاش</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Save edits?</source>
        <translation>تەھرىرلەشنى ساقلاش كېرەكمۇ؟</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cancel your edits?</source>
        <translation>تەھرىرلىكنى ئەمەلدىن قالدۇرۇۋېتەمدۇ؟</translation>
    </message>
</context>
<context>
    <name>QSslSocket</name>
    <message>
        <location filename="../src/network/ssl/qsslsocket_openssl.cpp" line="+569"/>
        <source>Unable to write data: %1</source>
        <translation>سانلىق مەلۇمات يېزىشقا ئامالسىز: ٪1</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Error while reading: %1</source>
        <translation>ئوقۇش جەريانىدا خاتالىق: ٪1</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Error during SSL handshake: %1</source>
        <translation>SSL قول ئېلىشىپ كۆرۈشۈش جەريانىدا خاتالىق كۆرۈلدى: ٪1</translation>
    </message>
    <message>
        <location line="-524"/>
        <source>Error creating SSL context (%1)</source>
        <translation>SSL نىڭ مەزمۇننى قۇرۇش خاتالىقى (٪1)</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Invalid or empty cipher list (%1)</source>
        <translation>ئىناۋەتسىز ياكى بوش شىفىر تىزىملىكى (٪1)</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Error creating SSL session, %1</source>
        <translation>SSL session, ٪1 نى قۇرۇش خاتالىقى</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Error creating SSL session: %1</source>
        <translation>SSL session قۇرۇش خاتالىقى: ٪1</translation>
    </message>
    <message>
        <location line="-61"/>
        <source>Cannot provide a certificate with no key, %1</source>
        <translation>ئاچقۇچسىز كىنىشكا تەمىنلىگىلى بولمايدۇ،٪1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading local certificate, %1</source>
        <translation>يەرلىك كىنىشكىنى قاچىلاش خاتالىقى، ٪1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error loading private key, %1</source>
        <translation>شەخسىي كۇنۇپكا قاچىلاشتا خاتالىق كۆرۈلدى، ٪1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Private key does not certificate public key, %1</source>
        <translation>شەخسىي ئاچقۇچ ئاشكارا ئاچقۇچىنى كىنىشكا قىلمايدۇ،٪1</translation>
    </message>
</context>
<context>
    <name>QSystemSemaphore</name>
    <message>
        <location filename="../src/corelib/kernel/qsystemsemaphore_unix.cpp" line="-41"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+66"/>
        <source>%1: out of resources</source>
        <translation>٪1: بايلىق قالمىدى</translation>
    </message>
    <message>
        <location line="-13"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+4"/>
        <source>%1: permission denied</source>
        <translation>٪1: ئىجازەت رەت قىلىنىدۇ</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: already exists</source>
        <translation>٪1: ئاللىبۇرۇن مەۋجۇت</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1: does not exist</source>
        <translation>٪1: مەۋجۇت ئەمەس</translation>
    </message>
    <message>
        <location line="+9"/>
        <location filename="../src/corelib/kernel/qsystemsemaphore_win.cpp" line="+3"/>
        <source>%1: unknown error %2</source>
        <translation>٪1: نامەلۇم خاتالىق ٪2</translation>
    </message>
</context>
<context>
    <name>QTDSDriver</name>
    <message>
        <location filename="../src/sql/drivers/tds/qsql_tds.cpp" line="+584"/>
        <source>Unable to open connection</source>
        <translation>ئۇلىنىشنى ئاچالمىدى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unable to use database</source>
        <translation>سانداننى ئىشلىتىشكە ئامالسىز</translation>
    </message>
</context>
<context>
    <name>QTabBar</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/complexwidgets.cpp" line="-326"/>
        <source>Scroll Left</source>
        <translation>سولغا سىيرىل</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll Right</source>
        <translation>ئوڭغا سىيرىلىڭ</translation>
    </message>
</context>
<context>
    <name>QTcpServer</name>
    <message>
        <location filename="../src/network/socket/qtcpserver.cpp" line="+282"/>
        <source>Operation on socket is not supported</source>
        <translation>Socket دىكى مەشغۇلات قوللىمايدۇ</translation>
    </message>
</context>
<context>
    <name>QTextControl</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+1973"/>
        <source>&amp;Undo</source>
        <translation>&amp;Undo</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Redo</source>
        <translation>&amp;قايتا</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cu&amp;t</source>
        <translation>Cu&amp;T</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copy &amp;Link Location</source>
        <translation>كۆچۈرۈش &gt; ئورنى</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>چاپلاش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select All</source>
        <translation>ھەممىنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>QToolButton</name>
    <message>
        <location filename="../src/plugins/accessible/widgets/simplewidgets.cpp" line="+254"/>
        <location line="+6"/>
        <source>Press</source>
        <translation>بېسىپ</translation>
    </message>
    <message>
        <location line="-4"/>
        <location line="+8"/>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
</context>
<context>
    <name>QUdpSocket</name>
    <message>
        <location filename="../src/network/socket/qudpsocket.cpp" line="+169"/>
        <source>This platform does not support IPv6</source>
        <translation>بۇ سۇپا IPv6 نى قوللىمايدۇ</translation>
    </message>
</context>
<context>
    <name>QUndoGroup</name>
    <message>
        <location filename="../src/gui/util/qundogroup.cpp" line="+386"/>
        <source>Undo</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Redo</source>
        <translation>قايتا-قايتا</translation>
    </message>
</context>
<context>
    <name>QUndoModel</name>
    <message>
        <location filename="../src/gui/util/qundoview.cpp" line="+101"/>
        <source>&lt;empty&gt;</source>
        <translation>&lt;empty&gt;</translation>
    </message>
</context>
<context>
    <name>QUndoStack</name>
    <message>
        <location filename="../src/gui/util/qundostack.cpp" line="+834"/>
        <source>Undo</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Redo</source>
        <translation>قايتا-قايتا</translation>
    </message>
</context>
<context>
    <name>QUnicodeControlCharacterMenu</name>
    <message>
        <location filename="../src/gui/text/qtextcontrol.cpp" line="+884"/>
        <source>LRM Left-to-right mark</source>
        <translation>LRM سولدىن ئوڭغا بەلگىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLM Right-to-left mark</source>
        <translation>RLM ئوڭدىن سولغا بەلگىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWJ Zero width joiner</source>
        <translation>ZWJ Zero كەڭلىك قوشقۇچى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWNJ Zero width non-joiner</source>
        <translation>ZWNJ Zero كەڭلىكتە غەيرىي قوشۇلغۇچى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ZWSP Zero width space</source>
        <translation>ZWSP نۆل كەڭلىك بوشلۇقى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRE Start of left-to-right embedding</source>
        <translation>LRE Start ئوڭدىن سولغا قوشۇۋېلىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLE Start of right-to-left embedding</source>
        <translation>RLE باشلانسا ئوڭدىن سولغا قوشۇۋېلىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LRO Start of left-to-right override</source>
        <translation>LRO باشلان تورى سولدىن ئوڭغا بېسىپ كېتىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>RLO Start of right-to-left override</source>
        <translation>RLO Start ئوڭدىن سولغا بېسىپ كېتىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PDF Pop directional formatting</source>
        <translation>PDF Pop يۆنىلىشلىك فورماتلاش</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Insert Unicode control character</source>
        <translation>Unicode كونتىروللاش خاراكتېرىنى قىستۇرۇش</translation>
    </message>
</context>
<context>
    <name>QWebFrame</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/FrameLoaderClientQt.cpp" line="+692"/>
        <source>Request cancelled</source>
        <translation>ئىلتىماس بىكار قىلىندى</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Request blocked</source>
        <translation>تەلەپ چەكلەنگەن</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot show URL</source>
        <translation>URL كۆرسەتكىلى بولمىدى</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Frame load interruped by policy change</source>
        <translation>سىياسەت ئۆزگەرتىش ئارقىلىق ئۆز ئارا رامكا قاچىلاش</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot show mimetype</source>
        <translation>مىmetype نى كۆرسەتكىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File does not exist</source>
        <translation>ھۆججەت مەۋجۇت ئەمەس</translation>
    </message>
</context>
<context>
    <name>QWebPage</name>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/network/qt/QNetworkReplyHandler.cpp" line="+382"/>
        <source>Bad HTTP request</source>
        <translation>ناچار HTTP تەلىپى</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/Localizations.cpp" line="+42"/>
        <source>Submit</source>
        <comment>default label for Submit buttons in forms on web pages</comment>
        <translation>تاپشۇرماق</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Submit</source>
        <comment>Submit (input element) alt text for &lt;input&gt; elements with no alt, title, or value</comment>
        <translation>تاپشۇرماق</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reset</source>
        <comment>default label for Reset buttons in forms on web pages</comment>
        <translation>قايتا تىڭشى</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>This is a searchable index. Enter search keywords: </source>
        <comment>text that appears at the start of nearly-obsolete web pages in the form of a &apos;searchable index&apos;</comment>
        <translation>بۇ ئىزدەشكە بولىدىغان كۆرسەتكۈچ. ئىزدەش ھالقىلىق سۆزنى كىرگۈزۈڭ: </translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Choose File</source>
        <comment>title for file button used in HTML forms</comment>
        <translation>ھۆججەت تاللاش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No file selected</source>
        <comment>text to display in file button used in HTML forms when no file is selected</comment>
        <translation>ھۆججەت تاللىمىدى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open in New Window</source>
        <comment>Open in New Window context menu item</comment>
        <translation>يېڭى كۆزنەكتە ئېچىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Link...</source>
        <comment>Download Linked File context menu item</comment>
        <translation>ئۇلىنىشنى ساقلاپ قېلىش...</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Link</source>
        <comment>Copy Link context menu item</comment>
        <translation>ئۇلانما كۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Image</source>
        <comment>Open Image in New Window context menu item</comment>
        <translation>رەسىمنى ئېچىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Image</source>
        <comment>Download Image context menu item</comment>
        <translation>سۈرەتنى ساقلىۋېلىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy Image</source>
        <comment>Copy Link context menu item</comment>
        <translation>رەسىم كۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Frame</source>
        <comment>Open Frame in New Window context menu item</comment>
        <translation>رامكا ئېچىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy</source>
        <comment>Copy context menu item</comment>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Back</source>
        <comment>Back context menu item</comment>
        <translation>قايت</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go Forward</source>
        <comment>Forward context menu item</comment>
        <translation>ئالدىغا ماڭ</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stop</source>
        <comment>Stop context menu item</comment>
        <translation>توختاش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Reload</source>
        <comment>Reload context menu item</comment>
        <translation>قايتا قاچىلاش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cut</source>
        <comment>Cut context menu item</comment>
        <translation>كېسىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Paste</source>
        <comment>Paste context menu item</comment>
        <translation>چاپلاش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No Guesses Found</source>
        <comment>No Guesses Found context menu item</comment>
        <translation>پەرەز تېپىلمىدى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Spelling context menu item</comment>
        <translation>نەزەردىن ساقىت قىلىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add To Dictionary</source>
        <comment>Learn Spelling context menu item</comment>
        <translation>لۇغەتكە قوشۇش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Search The Web</source>
        <comment>Search The Web context menu item</comment>
        <translation>ئىزدە تورى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Look Up In Dictionary</source>
        <comment>Look Up in Dictionary context menu item</comment>
        <translation>لۇغەتتىن يۇقىرىغا نەزەر</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Link</source>
        <comment>Open Link context menu item</comment>
        <translation>ئۇلىنىشنى ئېچىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Ignore</source>
        <comment>Ignore Grammar context menu item</comment>
        <translation>نەزەردىن ساقىت قىلىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Spelling</source>
        <comment>Spelling and Grammar context sub-menu item</comment>
        <translation>ئىملا قائىدىسى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Show Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>ئىملا ۋە گرامماتىكىنى كۆرسەت</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide Spelling and Grammar</source>
        <comment>menu item title</comment>
        <translation>ئىملا ۋە گرامماتىكىنى يوشۇرۇش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling</source>
        <comment>Check spelling context menu item</comment>
        <translation>ئىملا تەكشۈرۈش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Spelling While Typing</source>
        <comment>Check spelling while typing context menu item</comment>
        <translation>خەت بېسىش جەريانىدا ئىملا تەكشۈرۈش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Check Grammar With Spelling</source>
        <comment>Check grammar with spelling context menu item</comment>
        <translation>ئىملا قائىدىسى بىلەن گىرامماتىكىنى تەكشۈرۈش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Fonts</source>
        <comment>Font context sub-menu item</comment>
        <translation>خەت نۇسخىلىرى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Bold</source>
        <comment>Bold context menu item</comment>
        <translation>قاپ يۈرەك</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Italic</source>
        <comment>Italic context menu item</comment>
        <translation>ئىتاچى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Underline</source>
        <comment>Underline context menu item</comment>
        <translation>ئاستى سىزىق</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Outline</source>
        <comment>Outline context menu item</comment>
        <translation>تىزىس</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Direction</source>
        <comment>Writing direction context sub-menu item</comment>
        <translation>يۆنىلىش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Text Direction</source>
        <comment>Text direction context sub-menu item</comment>
        <translation>تېكىست يۆنىلىشى</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Default</source>
        <comment>Default writing direction context menu item</comment>
        <translation>كۆڭۈلدىكى سۆز</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>LTR</source>
        <comment>Left to Right context menu item</comment>
        <translation>LTR</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>RTL</source>
        <comment>Right to Left context menu item</comment>
        <translation>RTL</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Inspect</source>
        <comment>Inspect Element context menu item</comment>
        <translation>تەكشۈرۈش</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>No recent searches</source>
        <comment>Label for only item in menu that appears when clicking on the search field image, when no searches have been performed</comment>
        <translation>يېقىنقى ئىزدىنىشلەر يوق</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Recent searches</source>
        <comment>label for first item in the menu that appears when clicking on the search field image, used as embedded menu title</comment>
        <translation>يېقىنقى ئىزدىنىشلەر</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Clear recent searches</source>
        <comment>menu item in Recent Searches menu that empties menu&apos;s contents</comment>
        <translation>يېقىنقى ئىزدىنىشلەرنى ئېنىقلى</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Unknown</source>
        <comment>Unknown filesize FTP directory listing item</comment>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 (%2x%3 pixels)</source>
        <comment>Title string for images</comment>
        <translation>٪1 (٪2x٪3 پىكسېل)</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/WebCoreSupport/InspectorClientQt.cpp" line="+185"/>
        <source>Web Inspector - %2</source>
        <translation>تور تەكشۈرگۈچى - ٪2</translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/ScrollbarQt.cpp" line="+58"/>
        <source>Scroll here</source>
        <translation>بۇ يەرنى سىيرىل</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Left edge</source>
        <translation>سول گىرۋىكى</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Top</source>
        <translation>ئەڭ ئۈستى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Right edge</source>
        <translation>ئوڭ گىرۋىكى</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Bottom</source>
        <translation>ئاستىنقى</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Page left</source>
        <translation>بەت سول بەت</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page up</source>
        <translation>بەت قىلىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page right</source>
        <translation>بەت ئوڭ</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Page down</source>
        <translation>بەتنى تۆۋەن قىلىش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Scroll left</source>
        <translation>سولغا سىيرىلماق</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll up</source>
        <translation>سىيرىلما</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scroll right</source>
        <translation>ئوڭغا سىيرىلماق</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Scroll down</source>
        <translation>سىيرىلماق</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/3rdparty/webkit/WebCore/platform/qt/FileChooserQt.cpp" line="+45"/>
        <source>%n file(s)</source>
        <comment>number of chosen file</comment>
        <translation>
            <numerusform>٪n ھۆججەت</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/3rdparty/webkit/WebKit/qt/Api/qwebpage.cpp" line="+1322"/>
        <source>JavaScript Alert - %1</source>
        <translation>JavaScript ئاگاھلاندۇرۇشى - ٪1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>JavaScript Confirm - %1</source>
        <translation>JavaScript جەزملەش - ٪1</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>JavaScript Prompt - %1</source>
        <translation>JavaScript ئەسكەرتمىسى - ٪1</translation>
    </message>
    <message>
        <location line="+333"/>
        <source>Move the cursor to the next character</source>
        <translation>مۇلازورنى كېيىنكى پېرسوناژغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous character</source>
        <translation>مۇلازورنى ئالدىنقى پېرسوناژغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next word</source>
        <translation>مۇلازورنى كېيىنكى سۆزگە يۆتكەيدۇ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous word</source>
        <translation>مۇلازورنى ئالدىنقى سۆزگە يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the next line</source>
        <translation>مۇلازورنى كېيىنكى قۇرغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the previous line</source>
        <translation>مۇلازورنى ئالدىنقى قۇرغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the line</source>
        <translation>مۇلازورنى قۇرنىڭ باشلىنىشىغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the line</source>
        <translation>مۇلازورنى قۇرنىڭ ئاخىرىغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the block</source>
        <translation>مۇلازورنى بۆلەكنىڭ باشلىنىش نۇقتىسىغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the block</source>
        <translation>مۇلازورنى بۆلەكنىڭ ئاخىرىغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the start of the document</source>
        <translation>ئىملاچىنى ھۆججەتنىڭ باشلىنىش نۇقتىسىغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Move the cursor to the end of the document</source>
        <translation>مۇلازورنى ھۆججەتنىڭ ئاخىرىغا يۆتكەش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select all</source>
        <translation>ھەممىنى تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next character</source>
        <translation>كېيىنكى پېرسوناژغا تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous character</source>
        <translation>ئالدىنقى ھەرپنى تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next word</source>
        <translation>كېيىنكى سۆزگە تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous word</source>
        <translation>ئالدىنقى سۆزگە تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the next line</source>
        <translation>كېيىنكى قۇرغا تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the previous line</source>
        <translation>ئالدىنقى قۇرغا تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the line</source>
        <translation>قۇرنىڭ باشلىنىشىغا تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the line</source>
        <translation>قۇرنىڭ ئاخىرىغا تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the block</source>
        <translation>بۆلەكنى باشلاشنى تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the block</source>
        <translation>بۆلەكنىڭ ئاخىرىغا تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the start of the document</source>
        <translation>ھۆججەتنىڭ باشلىنىش نۇقتىسىنى تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Select to the end of the document</source>
        <translation>ھۆججەتنىڭ ئاخىرىغا تاللاڭ</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the start of the word</source>
        <translation>سۆزنىڭ باشلانمىغا ئۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete to the end of the word</source>
        <translation>سۆزنىڭ ئاخىرىغا ئۆچۈرۈش</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Insert a new paragraph</source>
        <translation>يېڭى ئابزاس كىرگۈزۈش</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert a new line</source>
        <translation>يېڭى قۇر قىستۇرۇش</translation>
    </message>
</context>
<context>
    <name>QWhatsThisAction</name>
    <message>
        <location filename="../src/gui/kernel/qwhatsthis.cpp" line="+522"/>
        <source>What&apos;s This?</source>
        <translation>ئۇ نېمۇ?</translation>
    </message>
</context>
<context>
    <name>QWidget</name>
    <message>
        <location filename="../src/gui/kernel/qwidget.cpp" line="+5326"/>
        <source>*</source>
        <translation>*</translation>
    </message>
</context>
<context>
    <name>QWizard</name>
    <message>
        <location filename="../src/gui/dialogs/qwizard.cpp" line="+637"/>
        <source>Go Back</source>
        <translation>قايت</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Continue</source>
        <translation>داۋامى بار</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Commit</source>
        <translation>ئىش قىلىش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Done</source>
        <translation>تۈگىدى</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="obsolete">退出</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp; كەينى</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Finish</source>
        <translation>&amp;تاماملاش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>&amp;Next</source>
        <translation>كېيىنكى</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Next &gt;</source>
        <translation>كېيىنكى &gt;</translation>
    </message>
</context>
<context>
    <name>QWorkspace</name>
    <message>
        <location filename="../src/gui/widgets/qworkspace.cpp" line="+1094"/>
        <source>&amp;Restore</source>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Move</source>
        <translation>يۆتكەش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Mi&amp;nimize</source>
        <translation>Mi&amp;nimize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ma&amp;ximize</source>
        <translation>ما&amp;ximize</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Close</source>
        <translation>يېپىش</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stay on &amp;Top</source>
        <translation>چوققا &amp; ئۈستى</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1059"/>
        <source>Sh&amp;ade</source>
        <translation>Sh&amp;ade</translation>
    </message>
    <message>
        <location line="-278"/>
        <location line="+60"/>
        <source>%1 - [%2]</source>
        <translation>%1 - [%2]</translation>
    </message>
    <message>
        <location line="-1837"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Restore Down</source>
        <translation>پەسكە ئەسلىگە كەلتۈرۈش</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location line="+2053"/>
        <source>&amp;Unshade</source>
        <translation>&amp;تەۋرەنمەسلىك</translation>
    </message>
</context>
<context>
    <name>QXml</name>
    <message>
        <location filename="../src/xml/sax/qxml.cpp" line="+58"/>
        <source>no error occurred</source>
        <translation>خاتالىق يۈز بەرمىدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error triggered by consumer</source>
        <translation>ئىستېمالچىلار قوزغاتقان خاتالىق</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected end of file</source>
        <translation>كۈتۈلمىگەن ھۆججەتنىڭ ئاخىرى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>more than one document type definition</source>
        <translation>بىردىن ئارتۇق ھۆججەت تىپى ئېنىقلىمىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing element</source>
        <translation>ئېلېمېنتنى پارلاۋاتقاندا خاتالىق كۆرۈلدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tag mismatch</source>
        <translation>خەتكۈچ ماس كېلىشمەسلىك</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing content</source>
        <translation>مەزمۇننى ئىزاھلاش جەريانىدا خاتالىق كۆرۈلدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unexpected character</source>
        <translation>كۈتۈلمىگەن خاراكتېر</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>invalid name for processing instruction</source>
        <translation>بىر تەرەپ قىلىش كۆرسەتمىسىنىڭ ئىناۋەتسىز نامى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>version expected while reading the XML declaration</source>
        <translation>XML خىتابنامىسىنى ئوقۇش بىلەن بىرگە مۆلچەرلەنگەن نۇسخىسى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>wrong value for standalone declaration</source>
        <translation>يەككە خىتابنامىنىڭ خاتا قىممىتى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>encoding declaration or standalone declaration expected while reading the XML declaration</source>
        <translation>XML خىتابنامىسىنى ئوقۇش بىلەن بىرگە مۆلچەرلەنگەن كودلاش خىتابنامىسى ياكى يەككە خىتابنامە</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>standalone declaration expected while reading the XML declaration</source>
        <translation>يەككە خىتابنامە XML خىتابنامىسىنى ئوقۇش بىلەن بىر ۋاقىتتا مۆلچەرلەنگەن</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing document type definition</source>
        <translation>ھۆججەت تىپى ئېنىقلىمىسىنى پارلاۋاتقاندا خاتالىق كۆرۈلدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>letter is expected</source>
        <translation>ھەرپ مۆلچەرلەنگەن</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing comment</source>
        <translation>ئىنكاسنى ئىزاھلاشتا خاتالىق كۆرۈلدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error occurred while parsing reference</source>
        <translation>پايدىلىنىش ماتېرىيالىنى ئىزاھلاش جەريانىدا خاتالىق كۆرۈلدى</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>internal general entity reference not allowed in DTD</source>
        <translation>DTD دا ئىچكى ئومۇمىي گەۋدە پايدىلىنىشىغا يول قويۇلمايدۇ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in attribute value</source>
        <translation>خاسلىق قىممىتىدە سىرتقى parsed general entity پايدىلىنىشقا يول قويۇلمايدۇ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>external parsed general entity reference not allowed in DTD</source>
        <translation>DTD دا سىرتقى parsed ئادەتتىكى ئورۇن پايدىلىنىشىغا يول قويۇلمايدۇ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>unparsed entity reference in wrong context</source>
        <translation>خاتا مۇلاھىشتە تەڭشەلمىگەن مۇددىھ پايدىلىنىش</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>recursive entities</source>
        <translation>قايتىلانما شەيدالار</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>error in the text declaration of an external entity</source>
        <translation>تاشقى شەيئىنىڭ تېكىست خىتابنامىسىدىكى خاتالىق</translation>
    </message>
</context>
<context>
    <name>QXmlStream</name>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="+592"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+1769"/>
        <source>Extra content at end of document.</source>
        <translation>ھۆججەت ئاخىرلاشقاندا ئارتۇق مەزمۇن.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>Invalid entity value.</source>
        <translation>ئىناۋەتسىز شەيدا قىممىتى .</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Invalid XML character.</source>
        <translation>ئىناۋەتسىز XML پېرسوناژى.</translation>
    </message>
    <message>
        <location line="+259"/>
        <source>Sequence &apos;]]&gt;&apos; not allowed in content.</source>
        <translation>&apos;]]&gt;&apos;نى مەزمۇندا رەتكە تىزىپ قويۇشقا بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+309"/>
        <source>Namespace prefix &apos;%1&apos; not declared</source>
        <translation>&apos;٪1&apos; namespace prefix ئېلان قىلىنمىدى</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Attribute redefined.</source>
        <translation>خاسلىق قايتا بېكىتىلدى.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Unexpected character &apos;%1&apos; in public id literal.</source>
        <translation>ئاممىۋى كىملىكتىكى كۈتۈلمىگەن مىجەز «٪1»</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Invalid XML version string.</source>
        <translation>ئىناۋەتسىز XML نەشرى تىزمىسى.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Unsupported XML version.</source>
        <translation>قوللىمايدىغان XML نۇسخىسى.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>%1 is an invalid encoding name.</source>
        <translation>٪1 ئىناۋەتسىز كودلاش نامى.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Encoding %1 is unsupported</source>
        <translation>٪1 كودلاش قوللىمايۋاتىدۇ</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Standalone accepts only yes or no.</source>
        <translation>تايانچ پەقەت شۇنداق ياكى ياق دەپلا قوبۇل قىلىدۇ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid attribute in XML declaration.</source>
        <translation>XML خىتابنامىسىدىكى ئىناۋەتسىز خاسلىق.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Premature end of document.</source>
        <translation>ھۆججىتىنىڭ بالدۇر ئاخىرلىشىشى</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Invalid document.</source>
        <translation>ئىناۋەتسىز ھۆججەت.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Expected </source>
        <translation>مۆلچەرلەنگەن </translation>
    </message>
    <message>
        <location line="+11"/>
        <source>, but got &apos;</source>
        <translation>، بىراق ئېرىشكەن &apos;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unexpected &apos;</source>
        <translation>كۈتۈلمىگەن &apos;</translation>
    </message>
    <message>
        <location line="+210"/>
        <source>Expected character data.</source>
        <translation>مۆلچەرلەنگەن خاراكتېر سانلىق مەلۇماتى .</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-995"/>
        <source>Recursive entity detected.</source>
        <translation>قايتىلانغان شەيدا بايقالدى.</translation>
    </message>
    <message>
        <location line="+516"/>
        <source>Start tag expected.</source>
        <translation>باشلان تورىغا خەت مۆلچەرلەنگەن.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>XML declaration not at start of document.</source>
        <translation>ھۆججەت باشلانمىغان XML خىتابنامىسى.</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>NDATA in parameter entity declaration.</source>
        <translation>پارامېتىرلىق گەۋدە بويىچە ستاتتا .</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>%1 is an invalid processing instruction name.</source>
        <translation>٪1 ئىناۋەتسىز بىر تەرەپ قىلىش كۆرسەتمە نامى.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid processing instruction name.</source>
        <translation>ئىناۋەتسىز بىر تەرەپ قىلىش كۆرسەتمە نامى .</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-521"/>
        <location line="+12"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+164"/>
        <location line="+53"/>
        <source>Illegal namespace declaration.</source>
        <translation>قانۇنغا خىلاپ ئىسىم-فامىلە خىتابنامىسى</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+15"/>
        <source>Invalid XML name.</source>
        <translation>ئىناۋەتسىز XML نامى.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Opening and ending tag mismatch.</source>
        <translation>خەتكۈچ ماسلاشماسلىقنى ئېچىش ۋە ئاخىرلاشتۇرۇش.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Reference to unparsed entity &apos;%1&apos;.</source>
        <translation>&apos;٪1&apos; نى تەڭشىمىگەن تېررىتورىيەنىڭ پايدىلىنىش ماتېرىيالى.</translation>
    </message>
    <message>
        <location line="-13"/>
        <location line="+61"/>
        <location line="+40"/>
        <source>Entity &apos;%1&apos; not declared.</source>
        <translation>&apos;٪1&apos; نى ئېلان قىلمىدى.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Reference to external entity &apos;%1&apos; in attribute value.</source>
        <translation>خاسلىق قىممىتىدىكى تاشقى گەۋدە &apos;٪1&apos; گە پايدىلانغان.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Invalid character reference.</source>
        <translation>ئىناۋەتسىز خاراكتىرلىك پايدىلىنىش .</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream.cpp" line="-75"/>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="-823"/>
        <source>Encountered incorrectly encoded content.</source>
        <translation>خاتا كودلانغان مەزمۇنغا يولۇقتى.</translation>
    </message>
    <message>
        <location line="+274"/>
        <source>The standalone pseudo attribute must appear after the encoding.</source>
        <translation>مۇقەددىمە pseudo خاسلىق كودلاشتىن كېيىن چوقۇم پەيدا بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/corelib/xml/qxmlstream_p.h" line="+562"/>
        <source>%1 is an invalid PUBLIC identifier.</source>
        <translation>٪1 ئىناۋەتسىز بولغان PUBLIC پەرقلەندۈرگۈچ.</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreebuilder.cpp" line="+205"/>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation>٪2 قىممىتى بار ٪1 خاسلىق ئاللىقاچان ئېلان قىلىندى.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation>٪1 خاسلىقتا چوقۇم ئىناۋەتلىك ٪2 قىممىتى بولۇشى كېرەك، بۇ ٪3 ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qiodevicedelegate.cpp" line="+84"/>
        <source>Network timeout.</source>
        <translation>تور ۋاقىت ئۇزىتى.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+320"/>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation>٪1 ئېلېمېنتى ھۆججەت ئېلېمېنتى سىرتىدا پەيدا بولغانلىقى ئۈچۈن قاتارغا كىرگۈزگىلى بولمايدۇ.</translation>
    </message>
    <message>
        <source>Attribute element %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation type="obsolete">属性元素 %1 不能被串行化，因为它出现在最顶层。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractdatetime.cpp" line="+80"/>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation>يىل ٪1 ئىناۋەتسىز، چۈنكى ٪2 بىلەن باشلىنىدۇ.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation>٪1 كۈنى ٪2..٪3 دائىرىسىنىڭ سىرتىدا.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation>ئاي ٪1 ٪2..٪3 دائىرىسىنىڭ سىرتىدا.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation>ئېشىپ كېتىش: ٪1 نىڭ چېسلاغا ۋەكىللىك قىلالمايدۇ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Day %1 is invalid for month %2.</source>
        <translation>٪1 كۈنى ٪2 ئايلىق ئىناۋەتسىز.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation>ۋاقتى 24:٪1:٪2.٪3 ئىناۋەتسىز. سائەت 24، ئەمما مىنۇت، سېكۇنت، مىللىسېكۇنتنىڭ ھەممىسى 0 ئەمەس؛ </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation>ۋاقىت ٪1:٪2:٪3.٪4 ئىناۋەتسىز.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation>ئېشىپ كېتىش: چېسلاغا ۋەكىللىك قىلغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractduration.cpp" line="+99"/>
        <location line="+15"/>
        <source>At least one component must be present.</source>
        <translation>ئەڭ ئاز دېگەندە بىر دېتال ھازىر بولىشى كېرەك.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation>ئەڭ ئاز بولغاندا بىر قېتىم دېتال ٪1 تىن كېيىن پەيدا بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qabstractfloatmathematician.cpp" line="+64"/>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation>بىر دىئاگنورلۇق بۆلمىدە ئوپېرا قويۇلمايدۇ، ٪1 نى ٪2 گە ئايرىشقا بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation>ئىنتېگرال پارچىسىدىكى تۇنجى ئوپېرا ،٪1 چەكسىزلىك بولالمايدۇ (٪2).</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation>بىر بۆلۈمدىكى ئىككىنچى ئوپېرا نۆلگە بولالمايدۇ (٪2).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qanyuri_p.h" line="+132"/>
        <source>%1 is not a valid value of type %2.</source>
        <translation>٪1 تىپىنىڭ ئىناۋەتلىك قىممىتى ٪2 ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomiccasters_p.h" line="+223"/>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation>٪2 دىن ٪1 كە تاشلىغاندا، مەنبە قىممىتى ٪3 بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicmathematicians.cpp" line="+65"/>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation>ئىنتېگرال پارچىسى (٪1) نۆلگە (٪2) ئېنىقلانمىغان.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation>بۆلۈنۈش (٪1) نۆلگە ئاساسەن (٪2) ئېنىقسىز.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation>مودېل بۆلۈنمىسى (٪1) نۆلگە ئاساسەن (٪2) ئېنىقلانمىغان.</translation>
    </message>
    <message>
        <location line="+122"/>
        <location line="+32"/>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation>٪1 تۈرىنىڭ قىممىتىنى ٪2 (ئا-سان ئەمەس) دەپ بۆلۈشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation>٪1 تۈرىنىڭ قىممىتىنى ٪2 ياكى ٪3 (+ياكى نۆلدىن تۆۋەن نۆلگە) بۆلۈشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation>٪1 تىپىنىڭ قىممىتىنى ٪2 ياكى ٪3 (+ياكى نۆلدىن تۆۋەن چەكسىزلىك) كۆپەيتىشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qatomicvalue.cpp" line="+79"/>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation>٪1 تىپىدىكى بىر قىممەتتە ئۈنۈملۈك بوئولا قىممىتىگە ئىگە بولغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qboolean.cpp" line="+78"/>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation>ئۈنۈملۈك بوئولانىڭ قىممىتىنى ئىككى ياكى ئۇنىڭدىن ئارتۇق ئاتوم قىممىتىنى ئۆز ئىچىگە ئالغان تەرتىپ بويىچە ھېسابلاشقا بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qderivedinteger_p.h" line="+402"/>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation>٪1 تۈردىكى ٪2 نىڭ قىممىتى ئەڭ يۇقىرى چەكتىن ئېشىپ (٪3).</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation>٪1 تۈردىكى ٪2 نىڭ قىممىتى ئەڭ تۆۋەن (٪3).</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/data/qhexbinary.cpp" line="+91"/>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation>٪1 تۈردىكى قىممەتتە چوقۇم تەكشى ساننى ئۆز ئىچىگە ئالىدۇ. ٪2 نىڭ قىممىتى يوق.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 is not valid as a value of type %2.</source>
        <translation>٪1 ٪2 تىپىنىڭ قىممىتى سۈپىتىدە كۈچكە ئىگە ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qarithmeticexpression.cpp" line="+207"/>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation>٪1 تىجارەتچى ٪2 تىپلىقنى ئىشلىتىشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation>٪1 تىجارەتچى ٪2 ۋە ٪3 تىپلىق ئاتوم قىممەتلىرىنى ئىشلىتىشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qattributenamevalidator.cpp" line="+66"/>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation>ھېسابلانغان خاسلىق نامىدىكى ئىسىم بوشلۇقى URI ٪1 بولالمايدۇ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation>ھېسابلانغان خاسلىق نامىغا ٪2 يەرلىك نامى قوشۇلغان ئىسىم بوشلۇقى URI ٪1 گە ئىگە بولالمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastas.cpp" line="+88"/>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation>٪1 نى كۈتكەن cast نىڭ تىپىدىكى خاتالىق ٪2 نى تاپشۇرۇۋالدى.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation>٪1 كە ياكى ئۇنىڭدىن ھاسىل بولغان تىپلارغا تاشلىغاندا، مەنبە قىممىتى چوقۇم ئوخشاش تۈردىكى بولۇشى كېرەك، بولمىسا چوقۇم بىر ھەرپلىك نۇسقىسى بولۇشى كېرەك. ٪2 نىڭ تىپىغا رۇخسەت قىلىنمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcastingplatform.cpp" line="+134"/>
        <source>No casting is possible with %1 as the target type.</source>
        <translation>٪1 بىلەن نىشان تىپى بولۇش سۈپىتى بىلەن قۇيۇش مۇمكىن ئەمەس.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation>٪1 دىن ٪2 كە تاشلىغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation>٪1 كە تاشلاش مۇمكىن ئەمەس، چۈنكى ئۇ ئابستراكت تىپى بولۇپ، شۇڭا ھەرگىزمۇ كۆزنى يۇمۇپ ئاچقىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation>٪1 تۈردىكى ٪2 تىن ٪3 گىچە بولغان قىممەتنى تاشلىغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation>٪1 دىن ٪2 كە تاشلىغاندا مەغلۇپ بولۇش: ٪3</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcommentconstructor.cpp" line="+67"/>
        <source>A comment cannot contain %1</source>
        <translation>ئىنكاس ٪1 نى ئۆز ئىچىگە ئالالمايدۇ</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>A comment cannot end with a %1.</source>
        <translation>ئىنكاس ٪1 بىلەن ئاخىرلىشالمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomparisonplatform.cpp" line="+167"/>
        <source>No comparisons can be done involving the type %1.</source>
        <translation>٪1 نىڭ تۈرىگە چېتىشلىق ھېچقانداق سېلىشتۇرما قىلىشقا بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation>٪1 لىك ئاتوم قىممىتى ٪2 بىلەن ٪3 نىڭ ئاتوم قىممەتلىرى ئارىسىدا مەشغۇلات قىلغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qdocumentcontentvalidator.cpp" line="+86"/>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation>خاسلىق تۈگۈنى ھۆججەت تۈگۈنىنىڭ بالىسى بولالمايدۇ. شۇڭا ٪1 خاسلىق ئورنىدىن قالمىدى.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qexpressionfactory.cpp" line="+169"/>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation>بىر كۇتۇپخانا مودېلىنى بىۋاستە باھالاشقا بولمايدۇ. چوقۇم ئاساسلىق مودېلدىن ئىمپورت قىلىنىشى كېرەك.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>No template by name %1 exists.</source>
        <translation>٪1 ئىسمى بويىچە قېلىپ يوق</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qgenericpredicate.cpp" line="+106"/>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation>٪1 نىڭ تىپىنىڭ قىممىتىنى باستۇرغىلى بولمايدۇ. بىر مۇستەققىللىقتا چوقۇم رەقەملىك تۈر ياكى ئۈنۈملۈك بوئولا قىممىتى تىپى بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation>ئورۇن بەلگىلەش ئىقتىدارى چوقۇم بىر رەقەملىك قىممەتنى باھالىشى كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qncnameconstructor_p.h" line="+113"/>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, is %2 invalid.</source>
        <translation>بىر تەرەپ قىلىش كۆرسەتمىسىدىكى نىشان نامى چوڭ- كىچىك ھەر قانداق بىر بىرىكمىدە ٪1 بولمايدۇ. شۇڭا ٪2 ئىناۋەتسىز.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation>٪1 بىر تەرەپ قىلىش كۆرسەتمىسىدىكى ئىناۋەتلىك نىشان نامى ئەمەس. چوقۇم ٪2 قىممىتى بولۇشى كېرەك، مەسىلەن :٪3.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qpath.cpp" line="+109"/>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation>بىر يولدا ئەڭ ئاخىرقى قەدەمدە چوقۇم تۈگۈن ياكى ئاتوم قىممەت قارىشى بولۇشى كېرەك. بۇ ئىككىسىنىڭ ئارىسىنى ئارىلاشتۇرۇپ قويۇشقا بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qprocessinginstructionconstructor.cpp" line="+84"/>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation>بىر تەرەپ قىلىش كۆرسەتمىسىنىڭ سانلىق مەلۇماتى ٪1 تىزمىسىنى ئۆز ئىچىگە ئالالمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor.cpp" line="+82"/>
        <source>No namespace binding exists for the prefix %1</source>
        <translation>٪1 ئالدىنقى شەرتى ئۈچۈن ئىسىم بوشلۇقى باغلاش مەۋجۇت ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qqnameconstructor_p.h" line="+156"/>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation>٪2 دىكى ئالدىنقى شەرت ٪1 ئۈچۈن ئىسىم بوشلۇقى باغلاش مەۋجۇت ئەمەس</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+69"/>
        <source>%1 is an invalid %2</source>
        <translation>٪1 ئىناۋەتسىز ٪2</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/xmlpatterns/functions/qabstractfunctionfactory.cpp" line="+77"/>
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>٪1 ئەڭ كۆپ ٪n دەلىللەشنى ئېلىپ بارىدۇ. ٪2 شۇنىڭ ئۈچۈن ئىناۋەتسىز.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>٪1 ھېچ بولمىغاندا ٪n دەلىللەشنى تەلەپ قىلىدۇ. ٪2 شۇنىڭ ئۈچۈن ئىناۋەتسىز.</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qaggregatefns.cpp" line="+120"/>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation>٪1 نىڭ بىرىنچى دەلىلى ٪2 نىڭ تىپى بولالمايدۇ. چوقۇم رەقەملىك تىپى، xs:yearMonthDuration ياكى xs:dayTimeDuration.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>٪1 نىڭ بىرىنچى دەلىلى ٪2 نىڭ تىپى بولالمايدۇ. چوقۇم ٪3، ٪4، ياكى ٪5 تىپلىق بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>٪1 نىڭ ئىككىنچى دەلىلى ٪2 نىڭ تىپى بولالمايدۇ. چوقۇم ٪3، ٪4، ياكى ٪5 تىپلىق بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qassemblestringfns.cpp" line="+88"/>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation>٪1 ئىناۋەتلىك XML 1.0 ھەرىپى ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcomparingaggregator.cpp" line="+197"/>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation>٪1 نىڭ بىرىنچى دەلىلى ٪2 نىڭ تىپى بولالمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qdatetimefn.cpp" line="+86"/>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation>ئەگەر ھەر ئىككى قىممەتتە رايون تەڭپۇڭلۇقى بولسا، چوقۇم ئوخشاش رايون تەڭپۇڭلۇقى بولۇشى كېرەك. ٪1 ۋە ٪2 ئوخشاش ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qerrorfn.cpp" line="+61"/>
        <source>%1 was called.</source>
        <translation>٪1 دەپ چاقىرىلدى.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternmatchingfns.cpp" line="+94"/>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation>٪1 نى ئالماشتۇرۇش تىزلىكى ئاخىرلاشقاندا ئەمەس بەلكى ٪2 ياكى ٪3 نى ئەگەشتۈرۈپ قويۇش كېرەك.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation>ئالماشتۇرۇلغان تىزلىقتا ٪1 نى قاچلىمىغاندا ئەڭ ئاز دېگەندە بىر خانىلىق سان بويىچە مېڭىش كېرەك.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation>ئالماشتۇرۇلغان تىزىلما ئىچىدە ٪1 پەقەت ئۆزى ياكى ٪2 نى قېچىشقا بولىدۇ، ٪3 نى ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qpatternplatform.cpp" line="+92"/>
        <source>%1 matches newline characters</source>
        <translation>٪1 يېڭى سىزىق ھەرپلىرى بىلەن ماس</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation>٪1 ۋە ٪2 بىر قۇرنىڭ باشلىنىش ۋە ئاخىرلىشىشىغا ماس كېلىدۇ.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Matches are case insensitive</source>
        <translation>سەرەڭگە بولسا مۇلايىملىق</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation>پېرسوناژلار سىنىپىدا كۆرۈنگەندىن باشقا، ئاق ئالەم ھەرپلىرى چىقىرىۋېتىلدى</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation>٪1 ئىناۋەتسىز مۇنتىزىم ئىپادىلەش شەكلى: ٪2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation>٪1 ئادەتتىكى چىراي ئىپادىلىرى ئۈچۈن ئىناۋەتسىز بايراق. كۈچكە ئىگە بايراقلار:</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qqnamefns.cpp" line="+17"/>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation>ئەگەر بىرىنچى مۇلاھىزىدە بوش تەرتىپ ياكى نۆل ئۇزۇنلۇقتىكى تىزما (ئىسىم تەۋەلىكى يوق) بولسا، ئالدىدىكىنى بېكىتىشكە بولمايدۇ. ٪1 ئالدىنقى شەرتى كۆرسىتىلدى.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencefns.cpp" line="+347"/>
        <source>It will not be possible to retrieve %1.</source>
        <translation>٪1 نى قايتۇرۇپ ئالغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qcontextnodechecker.cpp" line="+54"/>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation>٪1 نىڭ فۇنكسىيەسى ئۈچۈن 2-مۇلاھىزىنىڭ يىلتىز تۈگۈنى چوقۇم ھۆججەت تۈگۈنى بولۇشى كېرەك. ٪2 ھۆججەت تۈگۈنى ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qsequencegeneratingfns.cpp" line="+279"/>
        <source>The default collection is undefined</source>
        <translation>كۆڭۈلدىكى توپلاش جەدۋىلى ئېنىق ئەمەس</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be retrieved</source>
        <translation>٪1 نى قايتۇرۇپ ئالغىلى بولمىدى</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qstringvaluefns.cpp" line="+252"/>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation>٪1 نورماللاشتۇرۇش جەدۋىلى قوللىمايدۇ. قوللايدىغان جەدۋەللەر ٪2، ٪3، ٪4 ۋە ٪5 تىن ئىبارەت بولۇپ، ھېچقايسىسى بوش تىزمىسى (نورماللىشىش يوق) دۇر.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qtimezonefns.cpp" line="+87"/>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation>بىر بەلۋاغ تىزلىكى ٪1..٪2 دائىرىسىدە بولۇشى كېرەك. ٪3 دائىرىسىدىن چىقىپ كەتتى.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 is not a whole number of minutes.</source>
        <translation>٪1 پۈتۈن مىنۇتلار ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qcardinalityverifier.cpp" line="+58"/>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation>زۆرۈر بولغان چوڭلۇق دەرىجىسى ٪1؛ ٪2 لىك ئوردېنغا ئېرىشتى.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/janitors/qitemverifier.cpp" line="+67"/>
        <source>The item %1 did not match the required type %2.</source>
        <translation>٪1 تۈرى زۆرۈر بولغان ٪2 تۈرىگە ماس كەلمىدى.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+379"/>
        <location line="+7253"/>
        <source>%1 is an unknown schema type.</source>
        <translation>٪1 نامەلۇم سىخېما تىپى.</translation>
    </message>
    <message>
        <location line="-6971"/>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation>query prolog دا پەقەت ٪1 جاكارلاش يۈز بىرىدۇ.</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>The initialization of variable %1 depends on itself</source>
        <translation>٪1 ئۆزگەرگۈچى مىقدارنىڭ باشلاندۇرۇشى ئۆزىگە باغلىق</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>No variable by name %1 exists</source>
        <translation>٪1 نامىغا ئاساسەن ئۆزگەرگۈچى مىقدار مەۋجۇت ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qparsercontext.cpp" line="+93"/>
        <source>The variable %1 is unused</source>
        <translation>٪1 ئۆزگەرگۈچى مىقدار ئىشلىتىلمىگەن</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qquerytransformparser.cpp" line="+2841"/>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation>٪1 نەشرى قوللىمايدۇ. قوللايدىغان XQuery نۇسخىسى 1.0.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation>كودلاش ٪1 ئىناۋەتسىز. چوقۇم پەقەت لاتىن ھەرپلىرىنى ئۆز ئىچىگە ئالىدۇ، چوقۇم ئاق بوشلۇقنى ئۆز ئىچىگە ئالماسلىقى، ٪2 نىڭ مۇنتىزىم ئىپادىلەش شەكلىگە ماس كېلىشى كېرەك.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>No function with signature %1 is available</source>
        <translation>٪1 ئىمزا قويۇش ئىقتىدارى يوق</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+10"/>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation>بىر كۆڭۈلدىكى ئىسىم بوشلۇقى خىتابنامىسى چوقۇم فۇنكسىيە، ئۆزگىرىش ۋە تاللاش خىتابنامىسىدىن بۇرۇن يۈز بېرىشى كېرەك.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation>ئىسىم-فامىلە خىتابنامىسى چوقۇم فۇنكسىيە، ئۆزگىرىش ۋە تاللاش خىتابنامىسىدىن بۇرۇن يۈز بېرىشى كېرەك.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation>مودېل ئىمپورتى چوقۇم فۇنكسىيە، ئۆزگىرىش، تاللانما خىتابنامىسىدىن بۇرۇن يۈز بېرىشى كېرەك.</translation>
    </message>
    <message>
        <location line="+200"/>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation>٪1 نىڭ ئالدى قوشۇمچىسىنى قايتا ئېنىقلىغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <source>Only the prefix %1 can be declared to bind the namespace %2. By default, it is already bound to the prefix %1.</source>
        <translation type="obsolete">至于前缀 %1 可以被声明为和命名空间 %2 绑定。默认情况下，它已经被绑定到前缀 %1。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation>Prefix ٪1 ئاللىبۇرۇن Prolog دا ئېلان قىلىندى.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation>تاللاش نامىدا چوقۇم بىر ئالدىنقى شەرت بولۇشى كېرەك. تاللانمىلار ئۈچۈن كۆڭۈلدىكىدەك ئىسىم بوشلۇقى يوق.</translation>
    </message>
    <message>
        <location line="+171"/>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation>Schema Import ئىقتىدارى قوللىمايدۇ، شۇڭا ٪1 خىتابنامىلەر يۈز بەرمەيدۇ.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation>٪1 نىڭ نىشان نام بوشلۇقىنى بوش قويۇشقا بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The module import feature is not supported</source>
        <translation>مودېل ئىمپورت ئىقتىدارى قوللىمايدۇ</translation>
    </message>
    <message>
        <source>A variable by name %1 has already been declared in the prolog.</source>
        <translation type="obsolete">名称为 %1 的变量已经在序言中声明过了。</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>No value is available for the external variable by name %1.</source>
        <translation>تاشقى ئۆزگىرىش ٪1 نامى بىلەن تەمىنلىگىلى بولمايدۇ.</translation>
    </message>
    <message>
        <source>The namespace for a user defined function cannot be empty(try the predefined prefix %1 which exists for cases like this)</source>
        <translation type="obsolete">用户定义的函数的命名空间不能为空(请试试预定义的前缀 %1，它就是用于这种情况的)。</translation>
    </message>
    <message>
        <location line="-4154"/>
        <source>A construct was encountered which only is allowed in XQuery.</source>
        <translation>پەقەت XQuery دىلا رۇخسەت قىلىنغان بىر قۇرلۇش بىلەن ئۇچرىشىپ قالدى.</translation>
    </message>
    <message>
        <location line="+118"/>
        <source>A template by name %1 has already been declared.</source>
        <translation>٪1 گە ئاساسەن قېلىپ ئېلان قىلىندى.</translation>
    </message>
    <message>
        <location line="+3581"/>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation>٪1 ئاچقۇچلۇق سۆز باشقا شەكىل نامى بىلەن پەيدا بولالمايدۇ.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The value of attribute %1 must of type %2, which %3 isn&apos;t.</source>
        <translation>٪1 خاسلىق قىممىتى ٪2 نىڭ تىپىغا تەۋە بولۇشى كېرەك، بۇ ٪3 ئەمەس.</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>The prefix %1 can not be bound. By default, it is already bound to the namespace %2.</source>
        <translation>ئالدىنقى شەرت ٪1 نى باغلاپ بولغىلى بولمايدۇ. كۆڭۈلدىكىدەك بولغاندا ئاللىقاچان ٪2 ئىسىم بوشلۇقىغا باغلانغان.</translation>
    </message>
    <message>
        <location line="+312"/>
        <source>A variable by name %1 has already been declared.</source>
        <translation>٪1 نامىغا ئاساسەن ئۆزگەرگۈچى مىقدار ئاللىقاچان ئېلان قىلىندى.</translation>
    </message>
    <message>
        <location line="+135"/>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation>ئۇسلۇب ھۆججىتى ئىقتىدارىدا چوقۇم ئالدىن بەلگىلەنگەن ئىسىم بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation>ئىشلەتكۈچى بېكىتىلگەن فۇنكسىيەنىڭ نامى بوش بولالمايدۇ (بۇنداق ئەھۋاللاردا مەۋجۇت بولغان ئالدىن بەلگىلەنگەن ٪1 لىك ئالدىن بەلگىلەنگەن ئالدىنقى شەرتنى سىناپ كۆرۈڭ)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation>ئىسىم بوشلۇقى ٪1 ساقلانغان؛ شۇڭا ئىشلەتكۈچى بېكىتىلگەن فۇنكسىيەلەرنى ئىشلەتمەسلىكى مۇمكىن. بۇ ئەھۋاللار ئۈچۈن مەۋجۇت بولغان ئالدىن بەلگىلەنگەن ئالدىنقى شەرت ٪2 نى سىناپ كۆرۈڭ.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation>ئامبار مودېلىدا ئىشلەتكۈچى بېكىتىلگەن فۇنكسىيەنىڭ ئىسىم بوشلۇقى مودېل ئىسىم بوشلۇقى بىلەن تەڭ بولۇشى كېرەك. باشقىچە قىلىپ ئېيتقاندا ٪ 2 نىڭ ئورنىغا ٪1 بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>A function already exists with the signature %1.</source>
        <translation>ئىمزا ٪1 بىلەن بىر فۇنكسىيە مەۋجۇت.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation>ھېچقانداق سىرتقى فۇنكسىيە قوللىمايدۇ. قوللايدىغان بارلىق ئىقتىدارلارنى بىۋاستە ئىشلىتىشكە بولىدۇ، ئاۋال ئۇلارنى سىرتقى ھالەتكە ئېلان قىلمايلا</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>An argument by name %1 has already been declared. Every argument name must be unique.</source>
        <translation>٪1 نامى بىلەن دەلىللەش ئاللىقاچان ئېلان قىلىندى. ھەر بىر مۇنازىرە نامى چوقۇم ئۆزگىچە بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation>٪1 فۇنكسىيەسى بىر ئەندىزىنىڭ ئىچكى قىسمىغا ماسلاشتۇرۇش ئۈچۈن ئىشلىتىلگەندە، مۇلاھىزىدە ئۆزگىرىشچان پايدىلىنىش ماتېرىيالى ياكى ھەرپلىك ھەرپلەر بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation>XSL-T ئەندىزىسىدە ،٪1 نىڭ فۇنكسىيەسى ئۈچۈن بىرىنچى دەلىللەشتە، ماسلاشتۇرۇش ئۈچۈن ئىشلەتكەندە چوقۇم بىر تال دىتال بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation>بىر XSL-T ئەندىزىسىدە ،٪1 لىك فۇنكسىيەنىڭ بىرىنچى دەلىلى چوقۇم ماسلاشتۇرۇش ئۈچۈن ئىشلەتكەندە، ھەر خىل ياكى ئۆزگەرگۈچى پايدىلىنىش ئۇسۇلى بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation>XSL-T قېلىپىدا ٪1 ئىقتىدارى ئۈچىنچى دەلىللەشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation>XSL-T ئەندىزىسىدە پەقەت ٪1 ۋە ٪2 فونكىسىيىسىنىلا ماسلاشتۇرۇشقا ئىشلەتكىلى بولىدۇ.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation>XSL-T ئەندىزىسىدە ٪1 ئوقىنى ئىشلىتىشكە بولمايدۇ، پەقەت ئوق ٪2 ياكى ٪3 بولسا بولىدۇ.</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 is an invalid template mode name.</source>
        <translation>٪1 ئىناۋەتسىز قېلىپ شەكلى نامى.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation>ئىپادىلەش ئۈچۈن باغلانغان بىر ئۆزگەرگۈچى مىقدارنىڭ نامى ئورۇنلۇق ئۆزگىرىشتىن پەرقلىق بولۇشى كېرەك. شۇنىڭ بىلەن ٪1 دەپ ئاتىلىدىغان ئىككى ئۆزگىرىش سوقۇلۇپ كەتتى.</translation>
    </message>
    <message>
        <location line="+758"/>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation>Schema دەلىللەش ئىقتىدارى قوللىمايدۇ. شۇڭا ٪1 چىراي ئىپادىسى ئىشلىتىلمەسلىكى مۇمكىن.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation>Pragma چىراي ئىپادىلىرىنىڭ ھېچقايسىسى قوللاشقا ئېرىشەلمەيدۇ. شۇڭا، كەينىگە قايتىدىغان چىراي ئىپادىسى چوقۇم ھازىر بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation>ھەر بىر قېلىپ پارامېتىرىنىڭ ئىسمى چوقۇم ئۆزگىچە بولۇشى كېرەك؛ ٪1 تەكرارلانغان.</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation>٪1 ئوق XQuery دا قوللىمايدىكەن</translation>
    </message>
    <message>
        <location line="+1150"/>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation>٪1 بىر تەرەپ قىلىش كۆرسەتمىسىنىڭ كۈچكە ئىگە نامى ئەمەس.</translation>
    </message>
    <message>
        <location line="-7029"/>
        <source>%1 is not a valid numeric literal.</source>
        <translation>٪1 ئىناۋەتلىك رەقەملىك لېكتور ئەمەس.</translation>
    </message>
    <message>
        <location line="+6165"/>
        <source>No function by name %1 is available.</source>
        <translation>٪1 نامى بويىچە فۇنكسىيە يوق.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation>ئىسىم بوشلۇقى URI ئالدىنقى قوشۇمچىسىنى باغلىغاندا بوش URI بولالمايدۇ،٪1.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 is an invalid namespace URI.</source>
        <translation>٪1 ئىناۋەتسىز ئىسىم بوشلۇقى URI.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>It is not possible to bind to the prefix %1</source>
        <translation>٪1 ئالدىنقى شەرتكە باغلاش مۇمكىن ئەمەس</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>ئىسىم-فامىلىسى ٪1 پەقەت ٪2 گە باغلانغىلى بولىدۇ (ھەر ئىككىلا ئەھۋالدا ئالدىن ئىلان قىلىنغان).</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>Prefix ٪1 نى پەقەت ٪2 گە باغلىغىلى بولىدۇ (ھەر ئىككى ئەھۋالدا ئالدىن ئىلان قىلىنغان).</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation>ئىككى ئىسىم بوشلۇقى خىتابنامىسىنىڭ خاسلىقى ئوخشاش ئىسىمغا ئىگە: ٪1.</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation>ئىسىم-فامىلە URI  چوقۇم تۇراقلىق بولۇشى ھەمدە قاپلانغان چىراي ئىپادىسىنى ئىشلىتىشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>An attribute by name %1 has already appeared on this element.</source>
        <translation>٪1 نامى بويىچە خاسلىق بۇ ئېلېمېنتتا ئاللىقاچان پەيدا بولدى.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation>بىۋاسىتە ئېلېمېنت قۇراشتۇرغۇچى ياخشى شەكىللەنمەيدۇ. ٪1 ٪2 بىلەن ئاخىرلىشىدۇ.</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>The name %1 does not refer to any schema type.</source>
        <translation>٪1 نامى ھېچقانداق چەمبىرىك تۈرىنى كۆرسەتمەيدۇ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation>٪1 مۇرەككەپ تىپى. مۇرەككەپ تىپلارغا قۇيۇش مۇمكىن ئەمەس. بىراق، ٪2 ئەسەرلىرى قاتارلىق ئاتوم تىپلىرىغا تاشلانغان.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation>٪1 ئاتوم تىپى ئەمەس. تاشلاش پەقەت ئاتوم تىپلىرىغا بولىدۇ.</translation>
    </message>
    <message>
        <source>%1 is not a valid name for a processing-instruction. Therefore this name test will never match.</source>
        <translation type="obsolete">%1 不是处理指令的有效名称。因此这个名称测试永远不会匹配。</translation>
    </message>
    <message>
        <location line="+145"/>
        <location line="+71"/>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation>٪1 دائىرىسىدىكى خاسلىق خىتابنامىسىدە يوق. ئەسكەرتىش: چەمبىك ئىمپورت ئىقتىدارى قوللىمايدۇ.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation>كېڭەيتىلگەن ئىپادىلەش نامى چوقۇم ئىسىم بوشلۇقىدا بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qcardinality.cpp" line="+55"/>
        <source>empty</source>
        <translation>بوش</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or one</source>
        <translation>نۆل ياكى بىر</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>exactly one</source>
        <translation>مۇتلەق بىر</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>one or more</source>
        <translation>بىر ياكى بىر نەچچە</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or more</source>
        <translation>نۆل ياكى ئۇنىڭدىنمۇ كۆپ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/type/qtypechecker.cpp" line="+63"/>
        <source>Required type is %1, but %2 was found.</source>
        <translation>زۆرۈر بولغان تىپى ٪1، ئەمما ٪2 تېپىلدى.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation>٪1 تىن ٪2 كە ئۆسۈش ئېنىقسىزلىقنى كەلتۈرۈپ چىقىرىشى مۇمكىن.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>The focus is undefined.</source>
        <translation>قىزىق نۇقتا ئېنىقلانمىغان.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qoutputvalidator.cpp" line="+86"/>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation>باشقا ھەر خىل تۈگۈندىن كېيىن خاسلىق قوشقىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>An attribute by name %1 has already been created.</source>
        <translation>٪1 نامىغا ئاساسەن خاسلىق ئاللىقاچان بارلىققا كەلگەن.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/utils/qxpathhelper_p.h" line="+120"/>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation>پەقەت Unicode Codepoint كولاسىنى قوللايدۇ(٪1). ٪2 قوللىمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/api/qxmlserializer.cpp" line="+60"/>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation>خاسلىق ٪1 نى ئەڭ يۇقىرى پەللىدە كۆرۈنگەچكە قاتارغا كىرگۈزگىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/acceltree/qacceltreeresourceloader.cpp" line="+314"/>
        <source>%1 is an unsupported encoding.</source>
        <translation>٪1 قوللىمايدىغان كودلاش.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation>٪1 تەلەپ قىلىنغان كودلاشتا تەلەپ قىلىنماي قالغان ئوكتۇتلار ٪2 نى ئۆز ئىچىگە ئالىدۇ.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation>٪2 كودلاش ٪3 ئارقىلىق يۈز بەرگەن ٪1 كود نۇقتىسى ئىناۋەتسىز XML ھەرپىدۇر.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qapplytemplate.cpp" line="+119"/>
        <source>Ambiguous rule match.</source>
        <translation>ئەدەپ-قائىدىلەر سەرەڭگە</translation>
    </message>
    <message>
        <source>In a namespace constructor, the value for a namespace value cannot be an empty string.</source>
        <translation type="obsolete">在一个命名空间构造中，命名空间的值不能为空字符串。</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qcomputednamespaceconstructor.cpp" line="+69"/>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation>ئىسىم-فامىلە بوشلۇقىنى ياسىغۇچىدا ئىسىم-فامىلىسىنىڭ قىممىتى قۇرۇق تىزمىسى بولالمايدۇ.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation>ئالدى قوشۇلغۇچى ئىناۋەتلىك ٪1 بولۇشى كېرەك، بۇ ٪2 ئەمەس.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The prefix %1 cannot be bound.</source>
        <translation>٪1 ئالدىنقى شەرتنى باغلاپ بولغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation>پەقەت ئالدىنقى شەرت ٪1 نى ٪2 گە باغلىغىلى بولىدۇ ۋە ئەكىسچە بولىدۇ.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qevaluationcache.cpp" line="+117"/>
        <source>Circularity detected</source>
        <translation>ئايلانما تەكشۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/expr/qtemplate.cpp" line="+145"/>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation>٪1 پارامېتىرى تەلەپ قىلىنىدۇ، لېكىن مۇناسىپ ٪2 تەمىنلىمەيدۇ.</translation>
    </message>
    <message>
        <location line="-71"/>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation>٪1 پارامېتىرى ماقۇللاندى، لېكىن مۇناسىپ ٪2 مەۋجۇت ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/functions/qunparsedtextfn.cpp" line="+65"/>
        <source>The URI cannot have a fragment</source>
        <translation>URI دا پارچىلانسا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+519"/>
        <source>Element %1 is not allowed at this location.</source>
        <translation>٪1 ئېلېمېنتى بۇ ئورۇنغا يول قويۇلمايدۇ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Text nodes are not allowed at this location.</source>
        <translation>بۇ ئورۇندا تېكىست تۈگۈنىگە يول قويۇلمايدۇ.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Parse error: %1</source>
        <translation>تەقلىد قىلىش خاتالىقى: ٪1</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation>XSL-T نەشر خاسلىق قىممىتى ٪1 لىك تىپنىڭ قىممىتى بولۇشى كېرەك، بۇ ٪2 ئەمەس.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation>2.0 بىر تەرەپ قىلغۇچ بىلەن XSL-T 1.0 ئۇسلۇب ھۆججىتىنى ئىجرا قىلىدۇ.</translation>
    </message>
    <message>
        <location line="+108"/>
        <source>Unknown XSL-T attribute %1.</source>
        <translation>نامەلۇم XSL-T خاسلىق ٪1.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation>خاسلىق ٪1 ۋە ٪2 نى ئۆز-ئارا ئۆز-ئارا خاسلاشتۇرىدۇ.</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation>ئاددىيلاشتۇرۇلغان ئۇسلۇب ھۆججىتى مودېلىدا ،خاسلىق ٪1 چوقۇم مەۋجۇت بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation>٪1 ئېلېمېنتى ٪2 خاسلىق بولمىسا، ٪3 ياكى ٪4 خاسلىققا ئىگە بولالمايدۇ.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation>٪1 ئېلېمېنتىدا ٪2 ياكى ٪3 خاسلىقلاردىن ئاز دېگەندە بىرسى بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation>٪2 ئېلېمېنتىدا ٪1 خاسلىق ئىچىدە ئاز دېگەندە بىر خىل ھالەتنى بېكىتىش كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qmaintainingreader.cpp" line="+183"/>
        <source>Attribute %1 cannot appear on the element %2. Only the standard attributes can appear.</source>
        <translation>خاسلىق ٪1 ٪2 ئېلېمېنتىدا كۆرۈنەلمەيدۇ. پەقەت ئۆلچەملىك خاسلىقلا كۆرۈنەلەيدۇ.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Attribute %1 cannot appear on the element %2. Only %3 is allowed, and the standard attributes.</source>
        <translation>خاسلىق ٪1 ٪2 ئېلېمېنتىدا كۆرۈنەلمەيدۇ. پەقەت ٪3 گە يول قويۇلىدۇ، ئۆلچەملىك خاسلىق بولىدۇ.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, %4, and the standard attributes.</source>
        <translation>خاسلىق ٪1 ٪2 ئېلېمېنتىدا كۆرۈنەلمەيدۇ. ئىجازەت : ٪3، ٪4، ئۆلچەملىك خاسلىق.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, and the standard attributes.</source>
        <translation>خاسلىق ٪1 ٪2 ئېلېمېنتىدا كۆرۈنەلمەيدۇ. ئىجازەت :٪3، ئۆلچەملىك خاسلىق.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>XSL-T attributes on XSL-T elements must be in the null namespace, not in the XSL-T namespace which %1 is.</source>
        <translation>XSL-T ئېلېمېنتلىرىدىكى XSL-T خاسلىقلىرى چوقۇم Null نامى بوشلۇقىدا بولۇشى كېرەك، ٪1 بولغان XSL-T ئىسىم تەۋەلىكىدە ئەمەس.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The attribute %1 must appear on element %2.</source>
        <translation>٪1 خاسلىق ٪2 ئېلېمېنتىدا كۆرۈنۈشى كېرەك.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The element with local name %1 does not exist in XSL-T.</source>
        <translation>٪1 يەرلىك نامى بار ئېلېمېنت XSL-T دا مەۋجۇت ئەمەس.</translation>
    </message>
    <message>
        <location filename="../src/xmlpatterns/parser/qxslttokenizer.cpp" line="+123"/>
        <source>Element %1 must come last.</source>
        <translation>٪1 ئېلېمېنتى چوقۇم ئاخىرقى قېتىم كېلىدۇ.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one %1-element must occur before %2.</source>
        <translation>ئەڭ ئاز دېگەندە ٪1 ئېلېمېنت ٪2 تىن بۇرۇن يۈز بېرىشى كېرەك.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Only one %1-element can appear.</source>
        <translation>پەقەت ٪1 ئېلېمېنتلار كۆرۈنەلەيدۇ.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>At least one %1-element must occur inside %2.</source>
        <translation>ئەڭ ئاز دېگەندە ٪1 ئېلېمېنت ٪2 ئىچىدە يۈز بېرىشى كېرەك.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation>٪1 خاسلىق ٪2 دا مەۋجۇت بولۇپ تۇرغاندا، بىر تەرتىپلىك قۇرۇلغۇنى ئىشلەتكىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation>٪1 ئېلېمېنتىدا ٪2 خاسلىق ياكى تەرتىپلىك قۇرۇلغۇچ بولۇشى كېرەك.</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation>پارامېتىر تەلەپ قىلىنسا، ٪1 خاسلىق ياكى تەرتىپلىك قۇرۇلغۇچ ئارقىلىق سۈكۈتتىكى قىممەت تەمىنلىگىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>Element %1 cannot have children.</source>
        <translation>٪1 ئېلېمېنتى بالىلىق بولالمايدۇ.</translation>
    </message>
    <message>
        <location line="+434"/>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation>٪1 ئېلېمېنتىدا بىر تەرتىپلىك قۇرۇلما بولالمايدۇ.</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+9"/>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation>٪1 خاسلىق ٪2 دا، ٪3 نىڭ كىچىك بالىسىدا كۆرۈنەلمەيدۇ.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation>فۇنكسىيەدىكى پارامېتىرنى تونېل دەپ جاكارلاشقا بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+149"/>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation>بۇ بىر تەرەپ قىلغۇچ Schema تونۇمايدۇ، شۇڭا ٪1 نى ئىشلىتىشكە بولمايدۇ.</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation>ئەڭ يۇقىرى سەۋىيە ئۇسلۇب ھۆججىتى ئېلېمېنتلىرى چوقۇم null بولمىغان ئىسىم بوشلۇقىدا بولۇشى كېرەك، بۇ ٪1 ئەمەس.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation>٪2 ئېلېمېنتىدىكى ٪1 خاسلىق قىممىتى ٪3 ياكى ٪4 بولۇشى كېرەك، ٪5 ئەمەس.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation>خاسلىق ٪1 ٪2 قىممىتىگە ئىگە بولالمايدۇ.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation>٪1 خاسلىق پەقەت ئالدىنقى ٪2 ئېلېمېنتىدا كۆرۈنەلەيدۇ.</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation>ئەڭ ئاز بولغاندا ٪1 ئېلېمېنت ٪2 نىڭ بالىدەك كۆرۈنىدۇ.</translation>
    </message>
</context>
<context>
    <name>VolumeSlider</name>
    <message>
        <location filename="../src/3rdparty/phonon/phonon/volumeslider.cpp" line="+67"/>
        <source>Muted</source>
        <translation>ئاۋازسىز</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+15"/>
        <source>Volume: %1%</source>
        <translation>ھەجىم: ٪1٪</translation>
    </message>
</context>
<context>
    <name>WebCore::PlatformScrollbar</name>
    <message>
        <source>Scroll here</source>
        <translation type="obsolete">滚动到这里</translation>
    </message>
    <message>
        <source>Left edge</source>
        <translation type="obsolete">左边缘</translation>
    </message>
    <message>
        <source>Top</source>
        <translation type="obsolete">顶部</translation>
    </message>
    <message>
        <source>Right edge</source>
        <translation type="obsolete">右边缘</translation>
    </message>
    <message>
        <source>Bottom</source>
        <translation type="obsolete">底部</translation>
    </message>
    <message>
        <source>Page left</source>
        <translation type="obsolete">左一页</translation>
    </message>
    <message>
        <source>Page up</source>
        <translation type="obsolete">上一页</translation>
    </message>
    <message>
        <source>Page right</source>
        <translation type="obsolete">右一页</translation>
    </message>
    <message>
        <source>Page down</source>
        <translation type="obsolete">下一页</translation>
    </message>
    <message>
        <source>Scroll left</source>
        <translation type="obsolete">向左滚动</translation>
    </message>
    <message>
        <source>Scroll up</source>
        <translation type="obsolete">向上滚动</translation>
    </message>
    <message>
        <source>Scroll right</source>
        <translation type="obsolete">向右滚动</translation>
    </message>
    <message>
        <source>Scroll down</source>
        <translation type="obsolete">向下滚动</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <source>OK</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <source>Save All</source>
        <translation>ھەممىنى ساقلىۋېلىش</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ئېچىش</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;Evet</translation>
    </message>
    <message>
        <source>Yes to &amp;All</source>
        <translation>شۇنداق &amp; ھەممىگە</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&amp;ياق</translation>
    </message>
    <message>
        <source>N&amp;o to All</source>
        <translation>N&amp; O نىڭ ھەممىسىگە</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>توختىت</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>قايتا قايتا تىرشىش</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>نەزەردىن ساقىت قىلىش</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>Discard</source>
        <translation>ۋازكەس</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>قايتا تىڭشى</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>كۆڭۈلدىكىلەرنى ئەسلىگە كەلتۈرۈش</translation>
    </message>
</context>
<context>
    <name>QGnomeTheme</name>
    <message>
        <source>&amp;OK</source>
        <translation>&amp; OK</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>ساقلاش</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>ۋازكەچى</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>يېپىش</translation>
    </message>
    <message>
        <source>Close without Saving</source>
        <translation>ساقلىماي تۇرۇپ يېپىش</translation>
    </message>
</context>
<context>
    <name>QWidgetTextControl</name>
    <message>
        <source>&amp;Undo</source>
        <translation>&amp;Undo</translation>
    </message>
    <message>
        <source>&amp;Redo</source>
        <translation>&amp;قايتا</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation>Cu&amp;T</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <source>Copy &amp;Link Location</source>
        <translation>كۆچۈرۈش &gt; ئورنى</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation>چاپلاش</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>ھەممىنى تاللاڭ</translation>
    </message>
</context>
<context>
    <name>QPrintPropertiesDialog</name>
    <message>
        <source>Printer Properties</source>
        <translation>پرىنتېرلاش خاسلىقلىرى</translation>
    </message>
    <message>
        <source>Job Options</source>
        <translation>خىزمەت تاللانمىلىرى</translation>
    </message>
    <message>
        <source>Page Setup Conflicts</source>
        <translation>بەت تەڭشەش توقۇنۇشلىرى</translation>
    </message>
    <message>
        <source>There are conflicts in page setup options. Do you want to fix them?</source>
        <translation>بەت تەسىس قىلىش تاللانمىلىرىدا توقۇنۇشلار بار. سىز ئۇلارنى ئوڭشاپ قويامسىز؟</translation>
    </message>
    <message>
        <source>Advanced Option Conflicts</source>
        <translation>ئىلغار تاللانما توقۇنۇشلىرى</translation>
    </message>
    <message>
        <source>There are conflicts in some advanced options. Do you want to fix them?</source>
        <translation>بىر قىسىم ئىلغار تاللاشلاردا توقۇنۇش يۈز بېرىدۇ. سىز ئۇلارنى ئوڭشاپ قويامسىز؟</translation>
    </message>
</context>
<context>
    <name>QPrintDialog</name>
    <message>
        <source>Left to Right, Top to Bottom</source>
        <translation>سولدىن ئوڭغا، ئۈستىدىن ئوڭغا</translation>
    </message>
    <message>
        <source>Left to Right, Bottom to Top</source>
        <translation>سولدىن ئوڭغا، ئاستىدىن ئۈستىگە</translation>
    </message>
    <message>
        <source>Right to Left, Bottom to Top</source>
        <translation>ئوڭدىن سولغا، ئاستىدىن ئۈستىگە</translation>
    </message>
    <message>
        <source>Right to Left, Top to Bottom</source>
        <translation>ئوڭدىن سولغا، يۇقىرىدىن تۆۋەنگە</translation>
    </message>
    <message>
        <source>Bottom to Top, Left to Right</source>
        <translation>ئاستىدىن ئۈستىگە، سولدىن ئوڭغا</translation>
    </message>
    <message>
        <source>Bottom to Top, Right to Left</source>
        <translation>ئاستىدىن ئۈستىگە، ئوڭدىن سولغا</translation>
    </message>
    <message>
        <source>Top to Bottom, Left to Right</source>
        <translation>ئۈستىدىن ئوڭغا، سولدىن ئوڭغا</translation>
    </message>
    <message>
        <source>Top to Bottom, Right to Left</source>
        <translation>يۇقىرىدىن تۆۋەنگە، ئوڭدىن سولغا</translation>
    </message>
    <message>
        <source>1 (1x1)</source>
        <translation>1(1-توم)</translation>
    </message>
    <message>
        <source>2 (2x1)</source>
        <translation>2-توم (2-توم 1-سان)</translation>
    </message>
    <message>
        <source>4 (2x2)</source>
        <translation>4(2-توم)</translation>
    </message>
    <message>
        <source>6 (2x3)</source>
        <translation>6(2-توم 3-سان)</translation>
    </message>
    <message>
        <source>9 (3x3)</source>
        <translation>9(3x3)</translation>
    </message>
    <message>
        <source>16 (4x4)</source>
        <translation>16- سۈرە نىسا (4-توم 4-، 4-، 4- سۈرە)</translation>
    </message>
    <message>
        <source>All Pages</source>
        <translation>بارلىق بەتلەر</translation>
    </message>
    <message>
        <source>Odd Pages</source>
        <translation>&quot;غەلىتە بەتلەر&quot;</translation>
    </message>
    <message>
        <source>Even Pages</source>
        <translation>ھەتتا بەتلەر</translation>
    </message>
</context>
<context>
    <name>QPageSize</name>
    <message>
        <source>A0</source>
        <translation>A0</translation>
    </message>
    <message>
        <source>A1</source>
        <translation>A1</translation>
    </message>
    <message>
        <source>A2</source>
        <translation>A2</translation>
    </message>
    <message>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <source>A6</source>
        <translation>A6</translation>
    </message>
    <message>
        <source>A7</source>
        <translation>A7</translation>
    </message>
    <message>
        <source>A8</source>
        <translation>A8</translation>
    </message>
    <message>
        <source>A9</source>
        <translation>A9</translation>
    </message>
    <message>
        <source>A10</source>
        <translation>A10</translation>
    </message>
    <message>
        <source>B0</source>
        <translation>B0</translation>
    </message>
    <message>
        <source>B1</source>
        <translation>B1</translation>
    </message>
    <message>
        <source>B2</source>
        <translation>B2</translation>
    </message>
    <message>
        <source>B3</source>
        <translation>B3</translation>
    </message>
    <message>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <source>B7</source>
        <translation>B7</translation>
    </message>
    <message>
        <source>B8</source>
        <translation>B8</translation>
    </message>
    <message>
        <source>B9</source>
        <translation>B9</translation>
    </message>
    <message>
        <source>B10</source>
        <translation>B10</translation>
    </message>
    <message>
        <source>Executive (7.5 x 10 in)</source>
        <translation>ئىجرائىيە ئەمەلدارى (7.5 x 10 in)</translation>
    </message>
    <message>
        <source>Executive (7.25 x 10.5 in)</source>
        <translation>ئىجرائىيە ئەمەلدارى (7.25 × 10.5 in)</translation>
    </message>
    <message>
        <source>Folio (8.27 x 13 in)</source>
        <translation>فولىئو (8.27 × 13 in)</translation>
    </message>
    <message>
        <source>Legal</source>
        <translation>قانۇن-قانۇن</translation>
    </message>
    <message>
        <source>Letter / ANSI A</source>
        <translation>خەت / ANSI A</translation>
    </message>
    <message>
        <source>Tabloid / ANSI B</source>
        <translation>ژىگىت / ANSI B</translation>
    </message>
    <message>
        <source>Ledger / ANSI B</source>
        <translation>لىدېر / ANSI B</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>ئۆرپ-ئادەت</translation>
    </message>
    <message>
        <source>A3 Extra</source>
        <translation>A3 Extra</translation>
    </message>
    <message>
        <source>A4 Extra</source>
        <translation>A4 ئارتۇق</translation>
    </message>
    <message>
        <source>A4 Plus</source>
        <translation>A4 Plus</translation>
    </message>
    <message>
        <source>A4 Small</source>
        <translation>A4 كىچىك</translation>
    </message>
    <message>
        <source>A5 Extra</source>
        <translation>A5 Extra</translation>
    </message>
    <message>
        <source>B5 Extra</source>
        <translation>B5 ئارتۇق</translation>
    </message>
    <message>
        <source>JIS B0</source>
        <translation>JIS B0</translation>
    </message>
    <message>
        <source>JIS B1</source>
        <translation>JIS B1</translation>
    </message>
    <message>
        <source>JIS B2</source>
        <translation>JIS B2</translation>
    </message>
    <message>
        <source>JIS B3</source>
        <translation>JIS B3</translation>
    </message>
    <message>
        <source>JIS B4</source>
        <translation>JIS B4</translation>
    </message>
    <message>
        <source>JIS B5</source>
        <translation>JIS B5</translation>
    </message>
    <message>
        <source>JIS B6</source>
        <translation>JIS B6</translation>
    </message>
    <message>
        <source>JIS B7</source>
        <translation>JIS B7</translation>
    </message>
    <message>
        <source>JIS B8</source>
        <translation>JIS B8</translation>
    </message>
    <message>
        <source>JIS B9</source>
        <translation>JIS B9</translation>
    </message>
    <message>
        <source>JIS B10</source>
        <translation>JIS B10</translation>
    </message>
    <message>
        <source>ANSI C</source>
        <translation>ANSI C</translation>
    </message>
    <message>
        <source>ANSI D</source>
        <translation>ANSI D</translation>
    </message>
    <message>
        <source>ANSI E</source>
        <translation>ANSI E</translation>
    </message>
    <message>
        <source>Legal Extra</source>
        <translation>قانۇندىن ئارتۇق</translation>
    </message>
    <message>
        <source>Letter Extra</source>
        <translation>خەت- چەك قوشۇمچىلىرى</translation>
    </message>
    <message>
        <source>Letter Plus</source>
        <translation>خىتتاي Plus</translation>
    </message>
    <message>
        <source>Letter Small</source>
        <translation>كىچىك ھەرپ</translation>
    </message>
    <message>
        <source>Tabloid Extra</source>
        <translation>Tabloid Extra</translation>
    </message>
    <message>
        <source>Architect A</source>
        <translation>بىناكار ئا</translation>
    </message>
    <message>
        <source>Architect B</source>
        <translation>بىناكار ب</translation>
    </message>
    <message>
        <source>Architect C</source>
        <translation>بىناكار C</translation>
    </message>
    <message>
        <source>Architect D</source>
        <translation>بىناكار D</translation>
    </message>
    <message>
        <source>Architect E</source>
        <translation>بىناكار E</translation>
    </message>
    <message>
        <source>Note</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <source>Quarto</source>
        <translation>Quarto</translation>
    </message>
    <message>
        <source>Statement</source>
        <translation>بايانات</translation>
    </message>
    <message>
        <source>Super A</source>
        <translation>دەرىجىدىن تاشقىرى A</translation>
    </message>
    <message>
        <source>Super B</source>
        <translation>دەرىجىدىن تاشقىرى ب</translation>
    </message>
    <message>
        <source>Postcard</source>
        <translation>ئاتكىرىتكا</translation>
    </message>
    <message>
        <source>Double Postcard</source>
        <translation>قوش ئاتكىرىتكا</translation>
    </message>
    <message>
        <source>PRC 16K</source>
        <translation>PRC 16K</translation>
    </message>
    <message>
        <source>PRC 32K</source>
        <translation>PRC 32 K</translation>
    </message>
    <message>
        <source>PRC 32K Big</source>
        <translation>PRC 32 K چوڭ</translation>
    </message>
    <message>
        <source>Fan-fold US (14.875 x 11 in)</source>
        <translation>يەلپۈگۈچ ئامېرىكا (14.875 × 11 in)</translation>
    </message>
    <message>
        <source>Fan-fold German (8.5 x 12 in)</source>
        <translation>يەلپۈگۈچسىمان گېرمانىيە (8.5 x 12 in)</translation>
    </message>
    <message>
        <source>Fan-fold German Legal (8.5 x 13 in)</source>
        <translation>فەن بىتچىت گېرمانىيە قانۇنشۇناسلىقى (8.5 x 13 in)</translation>
    </message>
    <message>
        <source>Envelope B4</source>
        <translation>بولاق B4</translation>
    </message>
    <message>
        <source>Envelope B5</source>
        <translation>بولاق B5</translation>
    </message>
    <message>
        <source>Envelope B6</source>
        <translation>بولاق B6</translation>
    </message>
    <message>
        <source>Envelope C0</source>
        <translation>كونۋېرت C0</translation>
    </message>
    <message>
        <source>Envelope C1</source>
        <translation>كونۋېرت C1</translation>
    </message>
    <message>
        <source>Envelope C2</source>
        <translation>كونۋېرت C2</translation>
    </message>
    <message>
        <source>Envelope C3</source>
        <translation>كونۋېرت C3</translation>
    </message>
    <message>
        <source>Envelope C4</source>
        <translation>كونۋېرت C4</translation>
    </message>
    <message>
        <source>Envelope C5</source>
        <translation>كونۋېرت C5</translation>
    </message>
    <message>
        <source>Envelope C6</source>
        <translation>كونۋېرت C6</translation>
    </message>
    <message>
        <source>Envelope C65</source>
        <translation>كونۋېرت C65</translation>
    </message>
    <message>
        <source>Envelope C7</source>
        <translation>كونۋېرت C7</translation>
    </message>
    <message>
        <source>Envelope DL</source>
        <translation>كونۋېرت DL</translation>
    </message>
    <message>
        <source>Envelope US 9</source>
        <translation>ئامرىكا 9-نومۇرلۇق كونۋېرت</translation>
    </message>
    <message>
        <source>Envelope US 10</source>
        <translation>ئامرىكا 10-نومۇرلۇق كونۋېرت</translation>
    </message>
    <message>
        <source>Envelope US 11</source>
        <translation>ئامرىكا 11-نومۇرلۇق كونۋېرت</translation>
    </message>
    <message>
        <source>Envelope US 12</source>
        <translation>ئامرىكا 12-نومۇرلۇق كونۋېرت</translation>
    </message>
    <message>
        <source>Envelope US 14</source>
        <translation>ئامرىكا 14-نومۇرلۇق كونۋېرت</translation>
    </message>
    <message>
        <source>Envelope Monarch</source>
        <translation>كونۋېرت پادىشاھى</translation>
    </message>
    <message>
        <source>Envelope Personal</source>
        <translation>كونۋېرت شەخسى</translation>
    </message>
    <message>
        <source>Envelope Chou 3</source>
        <translation>چيۇ 3-نومۇرلۇق كونۋېرت</translation>
    </message>
    <message>
        <source>Envelope Chou 4</source>
        <translation>كونۋېرت Chou 4</translation>
    </message>
    <message>
        <source>Envelope Invite</source>
        <translation>كونۋېرت تەكلىپ قىلىش</translation>
    </message>
    <message>
        <source>Envelope Italian</source>
        <translation>ئىتاليانچە كونۋېرت</translation>
    </message>
    <message>
        <source>Envelope Kaku 2</source>
        <translation>كونۋېرت كاكۇ 2</translation>
    </message>
    <message>
        <source>Envelope Kaku 3</source>
        <translation>كونۋېرت كاكۇ 3</translation>
    </message>
    <message>
        <source>Envelope PRC 1</source>
        <translation>كونۋېرت PRC 1</translation>
    </message>
    <message>
        <source>Envelope PRC 2</source>
        <translation>كونۋېرت PRC 2</translation>
    </message>
    <message>
        <source>Envelope PRC 3</source>
        <translation>بولاق PRC 3</translation>
    </message>
    <message>
        <source>Envelope PRC 4</source>
        <translation>كونۋېرت PRC 4</translation>
    </message>
    <message>
        <source>Envelope PRC 5</source>
        <translation>كونۋېرت PRC 5</translation>
    </message>
    <message>
        <source>Envelope PRC 6</source>
        <translation>كونۋېرت PRC 6</translation>
    </message>
    <message>
        <source>Envelope PRC 7</source>
        <translation>كونۋېرت PRC 7</translation>
    </message>
    <message>
        <source>Envelope PRC 8</source>
        <translation>كونۋېرت PRC 8</translation>
    </message>
    <message>
        <source>Envelope PRC 9</source>
        <translation>كونۋېرت PRC 9</translation>
    </message>
    <message>
        <source>Envelope PRC 10</source>
        <translation>كونۋېرت PRC 10</translation>
    </message>
    <message>
        <source>Envelope You 4</source>
        <translation>بولاق سىز 4</translation>
    </message>
</context>
</TS>
