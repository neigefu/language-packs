<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="mn" sourcelanguage="en_AS">
<context>
    <name>DiskInfoView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="300"/>
        <source>as data disk</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠳ᠋ᠢᠰᠺ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="302"/>
        <source>System Partitions Size:</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠲᠣᠭᠣᠷᠢᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="316"/>
        <source>Data Partition Size:</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢᠨ ᠲᠣᠭᠣᠷᠢᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="333"/>
        <source>Encrypted Partition</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::CreatePartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="444"/>
        <source>OK</source>
        <translatorcomment>确定</translatorcomment>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="446"/>
        <source>Used to:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="450"/>
        <source>Mount point</source>
        <translatorcomment>挂载点：</translatorcomment>
        <translation>ᠡᠯᠬᠦᠬᠦ ᠴᠡᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="452"/>
        <source>Location for the new partition</source>
        <translatorcomment>新分区的位置：</translatorcomment>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="447"/>
        <source>Create Partition</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="358"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>ᠡᠯᠬᠦᠬᠦ ᠴᠡᠭ ᠨᠢ &apos;/&apos; ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="296"/>
        <source>The disk can only create one root partition!</source>
        <translation>ᠢᠵᠠᠭᠤᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠢ᠋ ᠭᠠᠭᠴᠠ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠬᠤᠵᠢᠮ ᠨᠢ ᠳᠠᠬᠢᠵᠤ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠢᠵᠠᠭᠤᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="320"/>
        <source>The disk can only create one boot partition!</source>
        <translation>boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠢ᠋ ᠭᠠᠭᠴᠠ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠬᠤᠵᠢᠮ ᠨᠢ ᠳᠠᠬᠢᠵᠤ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="336"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠨᠢ ᠵᠦᠪᠬᠡᠨ ext4 ᠪᠤᠶᠤ ext3 ᠪᠠᠢᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="346"/>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="546"/>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation>boot Хуваалт нь түүнээс бага байж болохгүй 2GiB</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="380"/>
        <source>Mount point repeat:</source>
        <translation>Хүлүүн цэг давт:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="453"/>
        <source>End of this space</source>
        <translation>ᠦᠯᠡᠳᠡᠵᠤ ᠪᠤᠢ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="454"/>
        <source>Beginning of this space</source>
        <translatorcomment>剩余空间头部</translatorcomment>
        <translation>ᠦᠯᠡᠳᠡᠵᠤ ᠪᠤᠢ ᠤᠷᠤᠨ ᠵᠠᠢ ᠵᠢᠨ ᠡᠮᠦᠨᠡᠬᠢ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="456"/>
        <source>Type for the new partition:</source>
        <translatorcomment>新分区的类型：</translatorcomment>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="522"/>
        <source>Recommended efi partition size is between 256MiB and 2GiB.</source>
        <translation>EFI ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠨᠢ 256MiB ᠡᠴᠡ 2GiB ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>Recommended boot partition size is between 1GiB and 2GiB.
Note that the size must be greater than 500MiB.</source>
        <translation type="obsolete">boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠵᠢ 1GiB ᠡᠴᠡ 2GiB ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠪᠠᠢᠯᠭᠠᠬᠤ ᠵᠢ ᠵᠦᠪᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.
500MiB ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="548"/>
        <source>Root partition size is greater than 15GiB, 
but Huawei machines require greater than 25GiB.</source>
        <translation>ᠢᠵᠠᠭᠤᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ 15GiB ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠶᠤᠰᠤᠳᠠᠢ,
ᠬᠤᠸᠠ ᠸᠸᠢ ᠮᠠᠰᠢᠨ 25GiB ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠵᠢ ᠱᠠᠭᠠᠷᠳᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="566"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="462"/>
        <source>Size(MiB)</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ (MiB)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="457"/>
        <source>Logical</source>
        <translatorcomment>逻辑分区</translatorcomment>
        <translation>ᠯᠤᠬᠢᠭ ᠤ᠋ᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="458"/>
        <source>Primary</source>
        <translatorcomment>主分区</translatorcomment>
        <translation>ᠭᠤᠤᠯ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="250"/>
        <source>remove this partition?</source>
        <translation>ᠲᠤᠰ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠢ᠋ ᠯᠠᠪᠳᠠᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="252"/>
        <source>This is a system partition,remove this partition?</source>
        <translation>ᠲᠤᠰ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠨᠢ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠦ᠋ᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠂ ᠲᠤᠰ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠬᠠᠰᠤᠬᠤ᠎ᠪᠠᠷ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="348"/>
        <source>Device for boot loader path:</source>
        <translation>ᠬᠦᠳᠦᠯᠦᠨ ᠵᠢᠯᠤᠭᠤᠳᠴᠤ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠤ᠋ᠨ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="349"/>
        <source>Revert</source>
        <translatorcomment>还原</translatorcomment>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitiondelegate</name>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="81"/>
        <source>#%1 partition on the device %2 will be created.
</source>
        <translation>#%1 ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ %2 ᠳᠡᠭᠡᠷ᠎ᠡ ᠪᠠᠢᠭᠤᠯᠤᠭᠳᠠᠨ᠎ᠠ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="85"/>
        <source>#%1 partition on the device %2 will be mounted %3.
</source>
        <translation>#%1 ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ %2 ᠳᠡᠭᠡᠷ᠎ᠡ %3 ᠪᠤᠯᠵᠤ ᠡᠯᠬᠦᠭᠳᠡᠨ᠎ᠡ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="90"/>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="99"/>
        <source>#%1 partition on the device %2 will be formated %3.
</source>
        <translation>#%1 ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ %2 ᠳᠡᠭᠡᠷ᠎ᠡ %3 ᠪᠤᠯᠵᠤ ᠬᠡᠯᠪᠡᠷᠢᠵᠢᠬᠦᠯᠦᠭᠳᠡᠨ᠎ᠡ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="95"/>
        <source>#%1 partition on the device %2 will be deleted.
</source>
        <translation>#%1 ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ %2 ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳ᠋ᠤ᠌ ᠤᠰᠠᠳᠬᠠᠭᠳᠠᠬᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="103"/>
        <source>#%1 partition  on the device %2 will be mounted %3.
</source>
        <translation>#%1 ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ %2 ᠳᠡᠭᠡᠷ᠎ᠡ %3 ᠪᠤᠯᠵᠤ ᠡᠯᠬᠦᠭᠳᠡᠨ᠎ᠡ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="118"/>
        <source>%1 GPT new partition table will be created.
</source>
        <translation>%1 GPT ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ ᠪᠠᠢᠭᠤᠯᠤᠭᠳᠠᠨ᠎ᠠ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="120"/>
        <source>%1 MsDos new partition table will be created.
</source>
        <translation>%1 MsDos ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ ᠪᠠᠢᠭᠤᠯᠤᠭᠳᠠᠨ᠎ᠠ.
</translation>
    </message>
</context>
<context>
    <name>KInstaller::FinishedFrame</name>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="80"/>
        <source>Installation Finished</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="81"/>
        <source>Restart</source>
        <translation>ᠤᠳᠤ ᠪᠡᠷ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>KInstaller::FullPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="200"/>
        <source>This action will  use the original old account as the default account. Please make sure you remember the password of the original account!</source>
        <translation>ᠲᠤᠰ ᠰᠤᠩᠭᠤᠯᠳᠠ ᠨᠢ ᠤᠤᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠴᠢ ᠭᠠᠷᠴᠠᠭ ᠤ᠋ᠨ ᠳᠤᠤᠷᠠᠬᠢ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢᠨ ᠬᠠᠮᠳᠤ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠳᠡᠬᠢ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠴᠤ᠌ ᠦᠯᠡᠳᠡᠬᠡᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ᠃ ᠬᠡᠪᠯᠡᠯ ᠠᠪᠴᠠᠯᠳᠤᠭᠰᠠᠨ ᠪᠠᠢᠳᠠᠯ ᠳ᠋ᠤ᠌ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠪᠤᠯᠤᠨ᠎ᠠ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠠᠶᠠᠳᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ᠃ ᠲᠤᠰ ᠰᠤᠩᠭᠤᠯᠳᠠ ᠵᠢ ᠰᠤᠩᠭᠤᠪᠠᠯ ᠰᠢᠨ᠎ᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠵᠢ ᠳᠠᠬᠢᠵᠤ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠦᠬᠡᠢ᠂ ᠲᠠ ᠡᠳᠡᠭᠡᠷ ᠳᠠᠩᠰᠠ ᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠳᠠᠢ ᠳᠡᠮᠳᠡᠭᠯᠡᠭᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="292"/>
        <source>The current installation mode enforces factory backup configuration and cannot be unchecked</source>
        <translation>ᠣᠳᠣᠬᠢ ᠰᠠᠭᠤᠯᠭᠠᠬᠤ ᠫᠠᠲ᠋ᠧᠨ ᠦᠢᠯᠡᠳᠪᠦᠷᠢ᠎ᠡᠴᠡ ᠭᠠᠷᠬᠤ ᠪᠡᠯᠡᠳᠬᠡᠯ ᠢ ᠠᠯᠪᠠᠲᠠᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠂ ᠰᠣᠩᠭᠣᠬᠤ ᠢ ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="326"/>
        <source>Please choose custom way to install, disk size less than 50G!</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠪᠡᠷ ᠤᠭᠰᠠᠷᠠᠭᠠᠷᠠᠢ᠂ ᠳ᠋ᠢᠰᠺ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠨᠢ 50G ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ !</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="327"/>
        <source>Full disk encryption</source>
        <translation>ᠪᠦᠬᠦ ᠳ᠋ᠢᠰᠺ ᠲᠤ᠌ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠠᠭᠠᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="328"/>
        <source>lvm</source>
        <translation>ᠯᠤᠬᠢᠭ ᠤ᠋ᠨ ᠡᠪᠬᠡᠮᠡᠯ (lvm)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="329"/>
        <source>Factory backup</source>
        <translation>ᠦᠢᠯᠡᠳᠪᠦᠷᠢ ᠡᠴᠡ ᠭᠠᠷᠬᠤ ᠨᠦᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="330"/>
        <source>save history date</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠪᠤᠯᠤᠨ ᠳᠡᠬᠦᠨ ᠤ᠋ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠦᠯᠡᠳᠡᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="504"/>
        <source>By default, SSDS serve as system disks and HDDS serve as user data disks</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠤᠭᠰᠠᠷᠠᠬᠤ᠂ SSD ᠳ᠋ᠢᠰᠺ ᠨᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤᠮᠠᠢ ᠳ᠋ᠢᠰᠺ ᠪᠤᠯᠵᠤ᠂ HDD ᠳ᠋ᠢᠰᠺ ᠨᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠳ᠋ᠢᠰᠺ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>KInstaller::ImgFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="213"/>
        <source>Select install way</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="214"/>
        <source>install from ghost</source>
        <translation>Ghost ᠡᠴᠡ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="215"/>
        <source>install from live</source>
        <translation>live ᠡᠴᠡ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="129"/>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="216"/>
        <source>open file</source>
        <translation>ᠹᠠᠢᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="217"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallerMainWidget</name>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="318"/>
        <source>About to exit the installer, restart the computer.</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ ᠡᠴᠡ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ᠂ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠨᠡᠬᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="320"/>
        <source>About to exit the installer and return to the trial interface.</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ ᠡᠴᠡ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ᠂ ᠳᠤᠷᠰᠢᠬᠤ ᠵᠤᠯᠭᠠᠭᠤᠷ ᠲᠤ᠌ ᠪᠤᠴᠠᠵᠤ ᠤᠴᠢᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="383"/>
        <source>Installer is about to exit and the computer will be shut down.</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠫᠷᠦᠭᠷᠡᠮ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ᠂ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠬᠠᠭᠠᠭᠳᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="684"/>
        <source>back</source>
        <translation>ᠡᠮᠦᠨ᠎ᠡ ᠠᠯᠬᠤᠮ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="661"/>
        <source>quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="638"/>
        <source>keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingFrame</name>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="151"/>
        <source>Log</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠡᠳᠦᠷ ᠦ᠋ᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="150"/>
        <source>The system is being installed, please do not turn off the computer</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠶᠠᠭ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠢ᠋ ᠪᠢᠳᠡᠭᠡᠢ ᠬᠠᠭᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="189"/>
        <source>Install failed</source>
        <translation>ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="191"/>
        <source>Sorry, KylinOS cannot continue the installation. Please feed back the error log below so that we can better solve the problem for you.</source>
        <translation>ᠠᠭᠤᠴᠢᠯᠠᠭᠠᠷᠠᠢ᠂ KylinOS ᠢ᠋/ ᠵᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠤᠬᠰᠠᠷᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠳᠤᠤᠷᠠᠬᠢ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠬᠠᠷᠢᠭᠤ ᠮᠡᠳᠡᠬᠦᠯᠪᠡᠯ ᠪᠢᠳᠡ ᠲᠠᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠨᠡᠩ ᠰᠠᠢᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠤ ᠴᠢᠳᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="198"/>
        <source>Restart</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingOEMConfigFrame</name>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="98"/>
        <source>Progressing system configuration</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="174"/>
        <source>OEM Error</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠢᠯᠠᠭᠳᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="180"/>
        <source>Restart</source>
        <translation>ᠣᠳᠣ ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠵᠦ ᠪᠠᠢ᠌ᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>KInstaller::LanguageFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="250"/>
        <source>Select Language</source>
        <translation>ᠦᠭᠡ ᠬᠡᠯᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="251"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::LicenseFrame</name>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="192"/>
        <source>Read License Agreement</source>
        <translation>ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠭᠡᠷ᠎ᠡ ᠵᠢ ᠤᠩᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="193"/>
        <source>I have read and agree to the terms of the agreement</source>
        <translation>ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠭᠡᠷ᠎ᠡ ᠵᠢᠨ ᠵᠤᠷᠪᠤᠰ ᠢ᠋ ᠪᠢ ᠨᠢᠬᠡᠨᠳᠡ ᠤᠩᠰᠢᠭ᠋ᠰᠠᠨ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="195"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::MainPartFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="848"/>
        <source>Custom install</source>
        <translatorcomment>自定义安装</translatorcomment>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠤᠭᠰᠠᠷᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="477"/>
        <source>Coexist Install</source>
        <translation>ᠵᠡᠷᠭᠡᠴᠡᠭᠡ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠭᠰᠠᠷᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="546"/>
        <source>Confirm Custom Installation</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠳᠤᠳᠤᠷᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠤᠭᠰᠠᠷᠠᠯᠳᠠ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="311"/>
        <source>Formatted the whole disk</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠳ᠋ᠢᠰᠺ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="567"/>
        <source>Start Installation</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="375"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
swap partition #6 of  %1 as swap;
</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ EFI, vfat ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ boot, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ /, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠦᠷᠪᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ backup, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠪᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ data, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠭᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ swap, swap ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="320"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠨᠢ EFI ᠪᠠᠢᠵᠤ ᠂ vfat ᠪᠣᠯᠭᠠᠨ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 %1 ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ 2᠎ᠳ᠋ᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠨᠢ boot ᠂ ext4 ᠭᠡᠵᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 %1 ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠦᠯᠡᠳᠡᠪᠦᠷᠢ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ 1vm ᠪᠣᠯᠭᠠᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 lvmᠳᠣᠲᠣᠭᠠᠳᠤ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ / ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠂ ext4 ᠪᠡᠷ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 lvmᠳᠣᠲᠣᠷ᠎ᠠ backup ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠂ ext4 ᠪᠡᠷ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 lvmᠳᠣᠲᠣᠷ᠎ᠠ data ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠂ ext4 ᠪᠡᠷ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 / ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠳᠤᠮᠳᠠ swap_file ᠹᠠᠢᠯ᠎ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠬᠤ ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="330"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ EFI, vfat ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ boot, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ /, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠦᠷᠪᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ backup, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠪᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ data, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠭᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ swap, swap ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="340"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠨᠢ boot ᠂ ext4 ᠭᠡᠵᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 %1 ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠣᠶᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠨᠢ extend ;
 %1 ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠦᠯᠡᠳᠡᠪᠦᠷᠢ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ 1vm ᠪᠣᠯᠭᠠᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 lvmᠳᠣᠲᠣᠭᠠᠳᠤ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ / ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠂ ext4 ᠪᠡᠷ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 lvmᠳᠣᠲᠣᠷ᠎ᠠ backup ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠂ ext4 ᠪᠡᠷ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 lvmᠳᠣᠲᠣᠷ᠎ᠠ data ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠂ ext4 ᠪᠡᠷ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ;
 / ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠳᠤᠮᠳᠠ swap_file ᠹᠠᠢᠯ᠎ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠬᠤ ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="351"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ EFI, vfat ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ boot, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ /, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠦᠷᠪᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ backup, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠪᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ data, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠭᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ swap, swap ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="365"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ EFI, vfat ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ boot, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ /, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠦᠷᠪᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ backup, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠪᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ data, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠭᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ swap, swap ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="385"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ EFI, vfat ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ boot, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠭᠤᠷᠪᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ /, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠦᠷᠪᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ backup, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠪᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ data, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠭᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ swap, swap ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="396"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
swap partition #8 of  %1 as swap;
</source>
        <translation>%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ boot, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠤᠶᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ extend;
%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠪᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ /, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠢᠷᠭᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ backup, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠤᠯᠤᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ data, ext4 ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
%1 ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠨᠠᠢ᠍ᠮᠠᠳᠤᠭᠠᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠨᠢ swap, swap ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="417"/>
        <source>will it be formatted, continue?</source>
        <translation>ᠹᠤᠷᠮᠠᠲ᠋ᠯᠠᠭᠳᠠᠬᠤ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠬᠦ ᠦᠦ ? ︖</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="419"/>
        <source>The disk which was set as a data disk will be formatted</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠢᠰᠺ᠎ᠢ᠋ᠶ᠋ᠡᠷ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠳ᠋ᠢᠰᠺ᠎ᠦ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠨᠢ ᠹᠤᠷᠮᠠᠲ᠋ᠯᠠᠭᠳᠠᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="421"/>
        <source>Continue</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="434"/>
        <source> set to data device;
</source>
        <translation> ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠳᠠᠪᠠᠭ ᠪᠣᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ;
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="507"/>
        <source>bootloader type of Legacy should not contain EFI partition!</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ Legacy ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠠᠷᠭ᠎ᠠ ᠦᠶᠡᠰ EFI ᠠᠭᠤᠯᠠᠭᠳᠠᠬᠤ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="554"/>
        <source>Confirm the above operations</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="680"/>
        <source>No created of </source>
        <translation>ᠴᠣᠭᠴᠠᠯᠠᠭᠠᠳᠤᠢ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="700"/>
        <source> partition
</source>
        <translation> ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="708"/>
        <source>InvalidId
</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="714"/>
        <source>Boot filesystem invalid
</source>
        <translation>Boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠤ᠋ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ
</translation>
    </message>
    <message>
        <source>the EFI partition size should be set between 256MB and 2GB
</source>
        <translation type="obsolete">Efi ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠵᠢ 256MB ᠡᠴᠡ 2GB ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠶᠤᠰᠤᠳᠠᠢ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="732"/>
        <source>Root partition size is greater than 15GiB,
but Huawei machines require greater than 25GiB.
</source>
        <translation>ᠢᠵᠠᠭᠤᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ 15GiB ᠡᠴᠡ ᠶᠡᠬᠡ,
ᠬᠤᠸᠠ ᠸᠸᠢ ᠮᠠᠰᠢᠨ 25GiB ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠵᠢ ᠱᠠᠭᠠᠷᠳᠠᠨ᠎ᠠ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="720"/>
        <source>Boot partition too small
</source>
        <translation>Boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠬᠡᠳᠦ ᠪᠠᠭ᠎ᠠ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="310"/>
        <source>Confirm Full Installation</source>
        <translation>ᠪᠦᠬᠦ ᠳ᠋ᠢᠰᠺ ᠲᠤ᠌ ᠤᠭᠰᠠᠷᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="621"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2.</source>
        <translation>ᠵᠢᠯᠤᠭᠤᠳᠤᠨ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠤ᠋ᠨ ᠠᠷᠭ᠎ᠠ %1 ᠨᠢ ᠳ᠋ᠢᠰᠺ ᠤ᠋ᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ
%2 ᠲᠠᠢ ᠢᠵᠢᠯ ᠪᠤᠰᠤ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="624"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="629"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2, cannot have efi partition.</source>
        <translation>ᠵᠢᠯᠤᠭᠤᠳᠤᠨ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠤ᠋ᠨ ᠠᠷᠭ᠎ᠠ %1 ᠨᠢ ᠳ᠋ᠢᠰᠺ ᠤ᠋ᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ
%2 ᠲᠠᠢ ᠢᠵᠢᠯ ᠪᠤᠰᠤ᠂ efi ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="717"/>
        <source>Boot partition not in the first partition
</source>
        <translation>Boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠨᠢᠭᠡᠳᠦᠬᠡᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ
</translation>
    </message>
    <message>
        <source>No boot partition
</source>
        <translation type="obsolete">boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ
</translation>
    </message>
    <message>
        <source>No Efi partition
</source>
        <translation type="obsolete">Efi ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="726"/>
        <source>Only one EFI partition is allowed
</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠨᠢᠭᠡ EFI ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠪᠠᠢᠬᠤ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠨ᠎ᠡ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="729"/>
        <source>Efi partition number invalid
</source>
        <translation>Efi ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ ᠤ ᠳ᠋ᠤᠭᠠᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ
</translation>
    </message>
    <message>
        <source>No root partition
</source>
        <translation type="obsolete">ᠢᠵᠠᠭᠤᠷ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ
</translation>
    </message>
    <message>
        <source>No backup partition
</source>
        <translation type="obsolete">ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="748"/>
        <source>This machine not support EFI partition
</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠡᠨᠡ ᠮᠠᠰᠢᠨ EFI ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="847"/>
        <source>Full install</source>
        <translation>ᠪᠦᠬᠦ ᠳ᠋ᠢᠰᠺ ᠲᠤ᠌ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="711"/>
        <source>Partition too small
</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠬᠡᠳᠦ ᠪᠠᠭ᠎ᠠ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="723"/>
        <source>the EFI partition size should be set between 256MiB and 2GiB
</source>
        <translation>Efi ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠣᠭᠣᠷᠢᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠵᠢ 256MB ᠡᠴᠡ 2GB ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠶᠤᠰᠤᠳᠠᠢ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="736"/>
        <source>BackUp partition too small
</source>
        <translation>ᠨᠦᠬᠡᠴᠡ ᠵᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠳᠦ ᠪᠠᠭ᠎ᠠ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="739"/>
        <source>Repeated mountpoint
</source>
        <translation>ᠳᠠᠪᠬᠤᠷᠳᠠᠵᠤ ᠡᠯᠬᠦᠭᠰᠡᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="751"/>
        <source>The filesystem type FAT16 or FAT32 is not fully-function filesystem,
except EFI partition, other partition not proposed</source>
        <translation>FAT16 ᠪᠤᠯᠤᠨFAT32 ᠨᠢ ᠪᠦᠷᠢᠨ ᠪᠤᠰᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ,
EFIᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠡᠴᠡ ᠭᠠᠳᠠᠨ᠎ᠠ᠂ ᠪᠤᠰᠤᠳ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠬᠡᠢ ᠪᠠᠢᠬᠤ ᠵᠢ ᠵᠦᠪᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="786"/>
        <source>If the </source>
        <translation>ᠴᠣᠭᠴᠠᠯᠠᠭᠠᠳᠤᠢ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="796"/>
        <source>If the /swap partition is not created,computers cannot be woken up after the system goes to sleep. </source>
        <translation>/ swap ᠬᠤᠪᠢᠶᠠᠷᠢ ᠣᠷᠣᠨ᠎ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠤᠭᠠᠳᠤᠢ ᠪᠣᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠢᠴᠡᠭᠡᠯᠡᠭᠰᠡᠨ᠎ᠦ᠌ ᠳᠠᠷᠠᠭ᠎ᠠ ᠺᠤᠮᠫᠢᠤᠢᠲ᠋ᠧᠷ᠎ᠢ᠋ ᠬᠡᠪ᠎ᠦ᠋ᠨ ᠪᠠᠢᠳᠠᠯ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠰᠡᠷᠡᠭᠡᠵᠦ ᠳᠡᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠮᠠᠭᠠᠳ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="804"/>
        <source>If the /data partition is not created,user data cannot be backed up. </source>
        <translation>/ data ᠣᠷᠣᠨ᠎ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠥᠬᠡᠢ ᠪᠣᠯ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ᠎ᠶ᠋ᠢ ᠪᠡᠯᠡᠳᠬᠡᠬᠦ᠎ᠶ᠋ᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="810"/>
        <source> partitions are not created,do you want to go to the next step?</source>
        <translation> ᠣᠷᠣᠨ ᠬᠤᠪᠢᠶᠠᠬᠤ ᠂ ᠳᠠᠷᠠᠭᠠᠴᠢ᠎ᠶ᠋ᠢᠨ ᠠᠯᠬᠤᠮ᠎ᠢ᠋ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠬᠦ ᠦᠦ ? ︖</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="846"/>
        <source>Choose Installation Method</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠠᠷᠭ᠎ᠠ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="593"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="606"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="850"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="906"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::MiddleFrameManager</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/middleframemanager.cpp" line="44"/>
        <source>next</source>
        <translatorcomment>下一步</translatorcomment>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::ModifyPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="46"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="206"/>
        <source>unused</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="192"/>
        <source>Format partition.</source>
        <translation>ᠳᠤᠰ ᠬᠤᠪᠢᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="193"/>
        <source>OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="195"/>
        <source>Used to:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="196"/>
        <source>Modify Partition</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠣᠭᠣᠷᠢᠭ ᠢ᠋ ᠵᠠᠰᠠᠨ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="325"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠣᠭᠣᠷᠢᠭ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠵᠥᠪᠬᠡᠨ ext4 ᠡᠰᠡᠪᠡᠯ ext3!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="335"/>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation>boot Хуваалт нь түүнээс бага байж болохгүй 2GiB</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="363"/>
        <source>The disk can only create one boot partition!</source>
        <translation>boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠢ᠋ ᠭᠠᠭᠴᠠ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠬᠤᠵᠢᠮ ᠨᠢ ᠳᠠᠬᠢᠵᠤ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ boot ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="391"/>
        <source>The disk can only create one root partition!</source>
        <translation>ᠦᠨᠳᠦᠰᠦᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠣᠭᠣᠷᠢᠭ ᠢ᠋ ᠵᠥᠪᠬᠡᠨ ᠨᠢᠭᠡ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠳᠠᠷᠠᠭᠠᠪᠠᠷ ᠳᠠᠬᠢᠨ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠦᠨᠳᠦᠰᠦᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠣᠭᠣᠷᠢᠭ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="407"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>ᠡᠯᠬᠦᠬᠦ ᠴᠡᠭ ᠨᠢ &apos;/&apos; ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="429"/>
        <source>Mount point repeat:</source>
        <translation>Хүлүүн цэг давт:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="532"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="201"/>
        <source>Mount point</source>
        <translation>ᠡᠯᠬᠦᠬᠦ ᠴᠡᠭ</translation>
    </message>
</context>
<context>
    <name>KInstaller::PrepareInstallFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/prepareinstallframe.cpp" line="109"/>
        <source>Check it and click [Start Installation]</source>
        <translatorcomment>勾选后点击[开始安装]</translatorcomment>
        <translation>ᠭᠤᠬᠠᠳᠠᠵᠤ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠲᠤᠪᠴᠢᠳᠠᠭᠠᠷᠠᠢ [ ᠡᠬᠢᠯᠡᠵᠤ ᠤᠭᠰᠠᠷᠬᠤ]</translation>
    </message>
</context>
<context>
    <name>KInstaller::SlideShow</name>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="295"/>
        <source>Multi platform support, suitable for mainstream CPU platforms at home and abroad.</source>
        <translation>ᠤᠯᠠᠨ ᠳᠠᠪᠴᠠᠩ ᠳᠡᠮᠵᠢᠨ᠎ᠡ᠂ ᠤᠯᠤᠰ ᠤ᠋ᠨ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠭᠠᠳᠠᠭᠠᠳᠤ ᠵᠢᠨ ᠭᠤᠤᠯ ᠤᠷᠤᠰᠬᠠᠯ ᠪᠤᠯᠬᠤ CPU ᠳᠠᠪᠴᠠᠩ ᠳ᠋ᠤ᠌ ᠳᠤᠬᠢᠷᠠᠮᠵᠢᠳᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="296"/>
        <source>Ukui desktop environment meets personalized needs.</source>
        <translation>UKUI ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠤᠷᠴᠢᠨ᠂ ᠦᠪᠡᠷᠮᠢᠴᠡ ᠱᠠᠭᠠᠷᠳᠠᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠠᠩᠭᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="297"/>
        <source>The system is updated and upgraded more conveniently.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠰᠢᠨᠡᠴᠢᠯᠡᠭᠳᠡᠵᠤ ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠰᠡᠨ ᠵᠢᠡᠷ ᠤᠯᠠᠮ ᠳᠦᠬᠦᠮ ᠳᠦᠳᠡ ᠪᠤᠯᠪᠠ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="298"/>
        <source>Security center, all-round protection of data security.</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠤ᠋ ᠳᠦᠪ᠂ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠢ᠋ ᠪᠦᠬᠦ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠬᠠᠮᠠᠭᠠᠯᠠᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="299"/>
        <source>Software store, featured software recommendation.</source>
        <translation>ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ᠂ ᠰᠣᠩᠭᠣᠳᠠᠭ ᠰᠣᠹᠲ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>Kirin software store, featured software recommendation.</source>
        <translation type="obsolete">kirin software store, featured software recommendation.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="300"/>
        <source>Provide technical support and professional after-sales service.</source>
        <translation>ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠳᠡᠮᠵᠢᠯᠬᠡ᠂ ᠳᠤᠰᠬᠠᠢ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠠᠩᠭᠠᠨ᠎ᠠ.</translation>
    </message>
</context>
<context>
    <name>KInstaller::TableWidgetView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="111"/>
        <source>This action will delect all partition,are you sure?</source>
        <translation>ᠤᠳᠤ ᠪᠠᠢᠭ᠎ᠠ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠤ᠋ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠭᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠯᠠᠪᠳᠠᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <source>yes</source>
        <translation>ᠳᠡᠢᠮᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="327"/>
        <source>no</source>
        <translation>ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="167"/>
        <source>Create partition table</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠣᠭᠣᠷᠢᠭ ᠤ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="321"/>
        <source>freespace</source>
        <translation>ᠰᠤᠯᠠ ᠰᠡᠯᠡᠬᠦᠦ</translation>
    </message>
</context>
<context>
    <name>KInstaller::TimeZoneFrame</name>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="119"/>
        <source>Select Timezone</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠤᠷᠤᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="236"/>
        <source>Start Configuration</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="127"/>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="240"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::UserFrame</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="139"/>
        <source>Start Configuration</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="143"/>
        <source>Start Installation</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="133"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>KInstaller::appcheckframe</name>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="96"/>
        <source>Start Installation</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="89"/>
        <source>Choose your app</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ ᠪᠡᠨ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="90"/>
        <source>Select the application you want to install to install with one click when using the system.</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠤᠭᠰᠠᠷᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ ᠪᠡᠨ ᠰᠤᠩᠭᠤᠵᠤ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠨᠢᠭᠡ ᠯᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="93"/>
        <source>Start Configuration</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
</context>
<context>
    <name>KInstaller::conferquestion</name>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="105"/>
        <source>confer_mainTitle</source>
        <translation>ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="106"/>
        <source>You can reset your password by answering a security question when you forget your password. Make sure you remember the answer.</source>
        <translation>ᠲᠠ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠡᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠯᠳᠠ ᠵᠢ ᠬᠠᠷᠢᠭᠤᠯᠬᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠳᠠᠬᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠬᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠪᠡᠨ ᠬᠠᠷᠢᠭᠤᠯᠳᠠᠭ ᠵᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠴᠡᠭᠡᠵᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="109"/>
        <source>Which city were you born in?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠭᠰᠡᠨ ᠨᠤᠳᠤᠭ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="110"/>
        <source>What is your childhood nickname?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠪᠠᠭ᠎ᠠ ᠨᠠᠰᠤᠨ ᠤ᠋ ᠡᠩᠬᠦᠷᠡᠢ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="111"/>
        <source>Which middle school did you graduate from?</source>
        <translation>ᠲᠠ ᠠᠯᠢ ᠳᠤᠮᠳᠠᠳᠤ ᠰᠤᠷᠭᠠᠭᠤᠯᠢ ᠡᠴᠡ ᠳᠡᠬᠦᠰᠦᠭᠰᠡᠨ ᠪᠤᠢ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="112"/>
        <source>What is your father&apos;s name?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠠᠪᠤ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="113"/>
        <source>What is your mother&apos;s name?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠡᠵᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="114"/>
        <source>When is your spouse&apos;s birthday?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠨᠢ ᠨᠦᠬᠦᠷ ᠤ᠋ᠨ ᠳᠦᠷᠦᠭᠰᠡᠨ ᠡᠳᠦᠷ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="115"/>
        <source>What&apos;s your favorite animal?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠮᠤᠭ ᠡᠴᠡ ᠳᠤᠭᠤᠳᠠᠭ ᠠᠮᠢᠳᠠᠨ?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="119"/>
        <source>question%1:</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ%1:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="120"/>
        <source>answer%1:</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠳᠠ%1:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="126"/>
        <source>set later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>KInstaller::modeselect</name>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="62"/>
        <source>Register account</source>
        <translation>ᠳᠠᠩᠰᠠ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="68"/>
        <source>Register immediately</source>
        <translation>ᠳᠠᠷᠤᠢ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="76"/>
        <source>Register later</source>
        <translation>ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="77"/>
        <source>After the system installation is completed, an account will be created when entering the system.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠤᠭᠰᠠᠷᠠᠴᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳ᠋ᠤ᠌ ᠤᠷᠤᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠳᠠᠩᠰᠠ ᠪᠠᠢᠭᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
</context>
<context>
    <name>KInstaller::securityquestions</name>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="404"/>
        <source>Security Questions</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠠᠶᠤᠯ ᠦᠭᠡᠶ ᠶᠢᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="405"/>
        <source>You can reset your password by answering security questions when you forget it, please make sure to remember the answer</source>
        <translation>ᠲᠠ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢᠶᠠᠨ ᠮᠠᠷᠲᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳᠦ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠦ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ ᠬᠠᠷᠢᠭᠤᠯᠬᠤ ᠪᠠᠷ ᠳᠠᠮᠵᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠳᠠᠬᠢᠨ ᠲᠠᠯᠪᠢᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠂ ᠬᠠᠷᠢᠭᠤᠯᠲᠠ ᠪᠠᠨ ᠴᠡᠭᠡᠵᠢᠯᠡᠵᠦ ᠴᠢᠳᠠᠬᠤ ᠶᠢ ᠦᠨᠡᠨᠬᠦ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="406"/>
        <source>Question 1</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ 1</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="407"/>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="409"/>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="411"/>
        <source>Answer:</source>
        <translation>ᠬᠠᠷᠢᠭᠤᠯᠲᠠ ᠄</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="408"/>
        <source>Question 2</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ 2</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="410"/>
        <source>Question 3</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ 3</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="412"/>
        <source>Set up later</source>
        <translation>ᠪᠠᠭᠠᠰᠤ ᠶᠢᠨ ᠳᠠᠷᠠᠭ᠎ᠠ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>KInstaller::userregisterwidiget</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="442"/>
        <source>Two password entries are inconsistent!</source>
        <translation>ᠬᠤᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="obsolete">your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="673"/>
        <source>Password strength:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠬᠦᠴᠦᠯᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="494"/>
        <source>Hostname should be more than 0 bits in length.</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠤᠷᠳᠤ ᠨᠢ 1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="496"/>
        <source>Hostname should be no more than 64 bits in length.</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠤᠷᠳᠤ ᠨᠢ 64 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="498"/>
        <source>Hostname only letters,numbers,hyphen and dot notation are allowed</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠳ᠋ᠤ᠌ ᠵᠥᠪᠬᠡᠨ ᠠᠪᠢᠶ᠎ᠠ᠂ ᠲᠣᠭ᠎ᠠ᠂ ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠳᠡᠮᠳᠡᠭ ᠪᠠ ᠴᠡᠭ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠠᠭᠳᠠᠬᠤ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="500"/>
        <source>Hostname must start with a number or a letter</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠡᠬᠢᠨ ᠳ᠋ᠤ᠌ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠪᠢᠶ᠎ᠠ ᠪᠤᠶᠤ ᠲᠣᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="502"/>
        <source>Hostname must end with a number or a letter</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠰᠡᠬᠦᠯ ᠳ᠋ᠤ᠌ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠪᠢᠶ᠎ᠠ ᠪᠤᠶᠤ ᠲᠣᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="504"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠳ᠋ᠤ᠌ ᠬᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠ ᠴᠡᠭ ᠳᠡᠮᠳᠡᠭ ᠨᠢ ᠵᠠᠯᠭᠠᠯᠳᠤᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ &apos; . &apos;</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="506"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ ᠳ᠋ᠤ᠌ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠭᠰᠡᠨ &apos; . &apos; ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="669"/>
        <source>Create User</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="670"/>
        <source>username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="671"/>
        <source>hostname</source>
        <translation>ᠭᠤᠤᠯ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="672"/>
        <source>new password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="674"/>
        <source>enter the password again</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="675"/>
        <source>Automatic login on boot</source>
        <translation>ᠮᠠᠰᠢᠨ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="680"/>
        <source>After logging into the system, it is recommended that you set biometric passwords and password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠦ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠲᠠᠨ ᠳᠤ 《 ᠲᠡᠮᠳᠡᠭᠯᠡᠬᠦ ᠰᠣᠩᠭᠣᠯᠲᠠ 》 ᠳᠤ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ ᠤᠨ ᠣᠨᠴᠠᠯᠢᠭ ᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠪᠣᠯᠣᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠦᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠦ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠭᠠᠷᠭᠠᠵᠤ ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠦ ᠨᠡᠩ ᠰᠠᠶᠢᠨ ᠣᠶᠢᠯᠠᠭᠠᠯᠲᠠ ᠣᠯᠬᠤ ᠶᠢ ᠰᠠᠨᠠᠭᠤᠯᠵᠠᠶ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="682"/>
        <source>After logging into the system, it is recommended that you set password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠦ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠲᠠᠨ ᠳᠤ 《 ᠲᠡᠮᠳᠡᠭᠯᠡᠬᠦ ᠰᠣᠩᠭᠣᠯᠲᠠ 》 ᠳᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠦᠨ ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠦ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠭᠠᠷᠭᠠᠵᠤ ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠦ ᠨᠡᠩ ᠰᠠᠶᠢᠨ ᠣᠶᠢᠯᠠᠭᠠᠯᠲᠠ ᠣᠯᠬᠤ ᠶᠢ ᠰᠠᠨᠠᠭᠤᠯᠵᠠᠶ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="685"/>
        <source>After logging into the system, it is recommended that you set the biometric password in [Settings - Login Options] to have a better experience in the system.</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠦ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠂ ᠲᠠᠨ ᠳᠤ 《 ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠬᠦ ᠰᠣᠩᠭᠣᠯᠲᠠ 》 ᠳᠤ ᠠᠮᠢᠳᠤ ᠪᠣᠳᠠᠰ ᠤᠨ ᠣᠨᠴᠠᠯᠢᠭ ᠤᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠭᠠᠷᠭᠠᠵᠤ ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠳᠣᠲᠣᠷ᠎ᠠ ᠨᠡᠩ ᠰᠠᠶᠢᠨ ᠲᠤᠷᠰᠢᠯᠭ᠎ᠠ ᠣᠯᠬᠤ ᠶᠢ ᠰᠠᠨᠠᠭᠤᠯᠤᠶ᠎ᠠ</translation>
    </message>
    <message>
        <source>Biometric [authentication] device detected / unified login support</source>
        <translation type="obsolete">ᠪᠢᠤᠯᠤᠬᠢ ᠤ᠋ᠨ ᠤᠨᠴᠠᠯᠢᠭ [ ᠬᠡᠷᠡᠴᠢᠯᠡᠯ] ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ / ᠨᠢᠭᠡᠳᠦᠯᠳᠡᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠵᠢ ᠳᠡᠮᠵᠢᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="724"/>
        <source>Low</source>
        <translation>ᠪᠠᠭᠤᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="737"/>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ ᠵᠡᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="748"/>
        <source>High</source>
        <translation>ᠬᠦᠴᠦᠷᠬᠡᠭ᠍</translation>
    </message>
</context>
<context>
    <name>KServer::EncryptSetFrame</name>
    <message>
        <source>password:</source>
        <translation type="obsolete">password:</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">Confirm password:</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">please keep your password properly.If you forget it,
you will not be able to access the disk data.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="40"/>
        <source>password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="41"/>
        <source>confirm password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="161"/>
        <source>The password contains less than two types of characters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠠᠳᠠᠬᠤ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ 2 ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="242"/>
        <source>The two passwords are different</source>
        <translation>ᠬᠣᠶᠠᠷ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠢᠵᠢᠯ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="268"/>
        <source>Please keep your password properly.If you forget it,you will not be able to access the disk data.</source>
        <translation>ᠲᠠ ᠥᠪᠡᠷ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠵᠢᠨᠨ ᠰᠠᠢᠳᠤᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠠᠷᠠᠢ, ᠬᠡᠷᠪᠡ ᠨᠢᠭᠤᠴᠠ ᠺᠣᠳ᠋ ᠵᠢᠨᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠪᠣᠯ ᠳ᠋ᠢᠰᠺ ᠤ᠋ᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="359"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="360"/>
        <source>OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="430"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">two password entries are inconsistent!</translation>
    </message>
</context>
<context>
    <name>KServer::MessageBox</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="199"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="201"/>
        <source>OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="295"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KServer::SetPartitionsSize</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="219"/>
        <source>Data Partitions</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="348"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="349"/>
        <source>OK</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="353"/>
        <source>System Partitions:</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠲᠣᠭᠣ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="355"/>
        <source>Data Partitions:</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="360"/>
        <source>Size should be within </source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢᠨ ᠲᠣᠭᠣᠷᠢᠭ ᠢ ᠪᠠᠭ᠎ᠠ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="360"/>
        <source>etc</source>
        <translation> </translation>
    </message>
    <message>
        <source>Size range from </source>
        <translation type="obsolete">ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢᠨ ᠲᠣᠭᠣᠷᠢᠭ ᠢ ᠪᠠᠭ᠎ᠠ ᠪᠠᠶᠢᠯᠭᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠶ ᠃ </translation>
    </message>
    <message>
        <source> to </source>
        <translation type="obsolete"> ᠠᠢ ᠂ ᠶᠡᠬᠡ ᠪᠠᠢ᠌ᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃ </translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="488"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../src/plugins/VirtualKeyboard/src/keyboardwidget.ui" line="29"/>
        <source>KeyboardWidget</source>
        <translation>keyboardWidget</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="9"/>
        <source>wd</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠤᠷᠤᠨ ᠤ᠋ ᠳ᠋ᠠᠢᠲ᠋ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="11"/>
        <source>seagate</source>
        <translation>ᠰᠢ ᠵᠢᠶᠸ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="13"/>
        <source>hitachi</source>
        <translation>ᠿᠢ ᠯᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="15"/>
        <source>samsung</source>
        <translation>ᠰᠠᠨ ᠰᠢᠨᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="17"/>
        <source>toshiba</source>
        <translation>ᠳ᠋ᠦᠩ ᡁᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="19"/>
        <source>fujitsu</source>
        <translation>ᠹᠦ ᠱᠢ ᠲᠦᠩ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="21"/>
        <source>maxtor</source>
        <translation>ᠮᠠᠢ ᠲᠦᠧᠧ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="23"/>
        <source>IBM</source>
        <translation>IBM</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="25"/>
        <source>excelStor</source>
        <translation>ᠢ ᠲᠦᠧᠧ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="27"/>
        <source>lenovo</source>
        <translation>ᠯᠢᠶᠠᠨ ᠰᠢᠶᠠᠩ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="29"/>
        <source>other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="31"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="303"/>
        <source>Unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="80"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="314"/>
        <source>Freespace</source>
        <translation>ᠰᠤᠯᠠ ᠰᠡᠯᠡᠬᠦᠦ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="294"/>
        <source>kylin data partition</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠳ᠋ᠢᠰᠺ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="301"/>
        <source>Swap partition</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠤᠷᠤᠨ ᠢ᠋ ᠰᠤᠯᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/main.cpp" line="158"/>
        <source>Show debug informations</source>
        <translation>ᠲᠤᠷᠰᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠬᠠᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="191"/>
        <source>Extended partition %1 has 
</source>
        <translation>ᠦᠷᠬᠡᠳᠬᠡᠯ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ %1 ᠪᠠᠢᠨ᠎ᠠ 
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="193"/>
        <source>Create new partition %1,%2
</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ %1,%2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="196"/>
        <source>Create new partition %1,%2,%3
</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ %1,%2,%3
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="201"/>
        <source>Delete partition %1
</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ %1
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="205"/>
        <source>Format %1 partition, %2
</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ %1,%2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="208"/>
        <source>Format partition %1,%2,%3
</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ %1,%2,%3
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="213"/>
        <source>%1 partition mountPoint %2
</source>
        <translation>%1 ᠬᠤᠪᠢᠶᠠᠷᠢ ᠲᠤᠭᠤᠷᠢᠭ ᠤ᠋ᠨ ᠡᠯᠬᠦᠬᠦ ᠴᠡᠭ %2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="217"/>
        <source>New Partition Table %1
</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠬᠦᠰᠦᠨᠦᠭᠳᠦ %1
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="220"/>
        <source>Reset size %1 partition
</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ %1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠵᠢ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="30"/>
        <source>KCommand::m_cmdInstance is not init.</source>
        <translation>ᠵᠠᠷᠯᠢᠭ ::m_cmdInstance ᠢ᠋/ ᠵᠢ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠤᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="67"/>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="99"/>
        <source>WorkingPath is not found. 
</source>
        <translation>ᠠᠵᠢᠯ ᠤ᠋ᠨ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ. 
</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="91"/>
        <source>Shell file is empty, does not continue. 
</source>
        <translation>shell ᠹᠠᠢᠯ ᠬᠤᠭᠤᠰᠤᠨ᠂ ᠡᠬᠦᠷᠬᠡ ᠵᠤᠭᠰᠤᠪᠠ. 
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>device</source>
        <translation>ᠲᠦᠭᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>type</source>
        <translation>ᠲᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>mounted</source>
        <translation>ᠡᠯᠬᠦᠬᠦ ᠴᠡᠭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>used</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>system</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>format</source>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="69"/>
        <source>Memory allocation error when setting</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ ᠵᠢᠨ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠯᠳᠠ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="73"/>
        <source>Memory allocation error</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠮᠵᠢ ᠵᠢᠨ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠯᠳᠠ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="75"/>
        <source>The password is the same as the old one</source>
        <translation>ᠳᠤᠰ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠤᠯ ᠤ᠋ᠨ ᠬᠢ ᠲᠠᠢ ᠪᠡᠨ ᠠᠳᠠᠯᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="77"/>
        <source>The password is a palindrome</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ ᠨᠢᠭᠡ ᠤᠷᠴᠢᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="79"/>
        <source>The password differs with case changes only</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠵᠦᠪᠬᠡᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠬᠤᠪᠢᠷᠠᠯᠳᠠ ᠵᠢ ᠪᠠᠭᠳᠠᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="81"/>
        <source>The password is too similar to the old one</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ ᠤᠤᠯ ᠤ᠋ᠨ ᠬᠢ ᠲᠠᠢ ᠳᠡᠩᠳᠡᠬᠦᠦ ᠠᠳᠠᠯᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="83"/>
        <source>The password contains the user name in some form</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠶᠠᠮᠠᠷ ᠨᠢᠭᠡ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="85"/>
        <source>The password contains words from the real name of the user in some form</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠶᠠᠮᠠᠷ ᠨᠢᠭᠡ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠨᠡᠨ ᠪᠤᠳᠠᠳᠠᠢ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="87"/>
        <source>The password contains forbidden words in some form</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠶᠠᠮᠠᠷ ᠨᠢᠭᠡ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠭᠰᠠᠨ ᠳᠠᠩ ᠦᠭᠡ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="92"/>
        <source>The password contains too few digits</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠬᠤ ᠤᠷᠤᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠬᠡᠳᠦ ᠪᠤᠭᠤᠨᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="97"/>
        <source>The password contains too few uppercase letters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠬᠤ ᠳᠤᠮᠤ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠠᠪᠢᠶ᠎ᠠ ᠬᠡᠳᠦ ᠴᠦᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="102"/>
        <source>The password contains too few lowercase letters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠬᠤ ᠳᠤᠮᠤ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠠᠪᠢᠶ᠎ᠠ ᠬᠡᠳᠦ ᠴᠦᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="107"/>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠬᠤ ᠤᠨᠴᠤᠭᠤᠢ ᠠᠪᠢᠶ᠎ᠠ ᠬᠡᠳᠦ ᠴᠦᠬᠡᠨ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="112"/>
        <source>The password is too short</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠡᠳᠦ ᠪᠤᠭᠤᠨᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="114"/>
        <source>The password is just rotated old one</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ ᠵᠦᠪᠬᠡᠨ ᠬᠠᠭᠤᠴᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠡᠰᠡᠷᠬᠦ ᠡᠷᠬᠢᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="119"/>
        <source>The password does not contain enough character classes</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠬᠠᠩᠭᠠᠯᠳᠠᠳᠠᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠪᠠᠭᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="124"/>
        <source>The password contains too many same characters consecutively</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠬᠡᠳᠦ ᠤᠯᠠᠨ ᠠᠳᠠᠯᠢ ᠳᠦᠰᠳᠡᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠭᠰᠡᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="129"/>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠬᠡᠳᠦ ᠤᠯᠠᠨ ᠠᠳᠠᠯᠢ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠭᠰᠡᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="90"/>
        <source>The password contains less than %1 digits</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠭᠰᠠᠨ ᠲᠤᠭᠠᠨ ᠤ᠋ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠨᠢ%ld ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="95"/>
        <source>The password contains less than %1 uppercase letters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠬᠤ ᠳᠤᠮᠤ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠠᠪᠢᠶ᠎ᠠ%ld ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="100"/>
        <source>The password contains less than %1 lowercase letters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠬᠤ ᠵᠢᠵᠢᠭ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠠᠪᠢᠶ᠎ᠠ%ld ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="105"/>
        <source>The password contains less than %1 non-alphanumeric characters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠪᠠᠭᠳᠠᠬᠤ ᠤᠨᠴᠤᠭᠤᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ%ld ᠤᠷᠤᠨ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="110"/>
        <source>The password is shorter than %1 characters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ %1 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="117"/>
        <source>The password contains less than %1 character classes</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠠᠭᠤᠯᠠᠭᠰᠠᠨ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠨᠢ %1 ᠳᠦᠷᠦᠯ ᠡᠴᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="122"/>
        <source>The password contains more than %1 same characters consecutively</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ %ld ᠤᠷᠤᠨ ᠡᠴᠡ ᠳᠠᠪᠠᠭᠰᠠᠨ ᠠᠳᠠᠯᠢ ᠳᠦᠰᠳᠡᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠭᠰᠡᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="127"/>
        <source>The password contains more than %1 characters of the same class consecutively</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ %ld ᠤᠷᠤᠨ ᠡᠴᠡ ᠳᠠᠪᠠᠭᠰᠠᠨ ᠠᠳᠠᠯᠢ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠭᠰᠡᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="132"/>
        <source>The password contains monotonic sequence longer than %1 characters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ %ld ᠤᠷᠤᠨ ᠡᠴᠡ ᠳᠠᠪᠠᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠨᠢᠭᠡ ᠬᠦᠭᠲᠦ ᠴᠤᠪᠤᠷᠠᠯ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="134"/>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠲᠤ᠌ ᠬᠡᠳᠦ ᠤᠷᠳᠤ ᠠᠳᠠᠯᠢ ᠬᠡᠯᠪᠡᠷᠢ ᠵᠢᠨ ᠨᠢᠭᠡ ᠬᠦᠭᠲᠦ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠴᠤᠪᠤᠷᠠᠯ ᠪᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="136"/>
        <source>No password supplied</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠬᠠᠩᠭᠠᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="138"/>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation>RNG ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ ᠡᠴᠡ ᠡᠬᠦᠰᠦᠭᠰᠡᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳᠠᠰᠢᠷᠠᠮ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ ᠵᠢ ᠤᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="140"/>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠦᠷᠢᠯᠳᠦᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ- ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠳ᠋ᠤ᠌ ᠱᠠᠭᠠᠷᠳᠠᠭᠳᠠᠬᠤ ᠡᠧᠨ᠋ᠲ᠋ᠷᠤᠫᠢ ᠳ᠋ᠤ᠌ ᠬᠦᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="143"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="145"/>
        <source>The password fails the dictionary check</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠨᠢ ᠳᠤᠯᠢ ᠪᠢᠴᠢᠭ ᠤ᠋ᠨ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠦᠩᠬᠡᠷᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="149"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="151"/>
        <source>Unknown setting</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="155"/>
        <source>Bad integer value of setting</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠪᠦᠬᠦᠯᠢ ᠲᠤᠭ᠎ᠠ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="157"/>
        <source>Bad integer value</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠪᠦᠬᠦᠯᠢ ᠳᠤᠭᠠᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="161"/>
        <source>Setting %s is not of integer type</source>
        <translation>%s ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠢ ᠪᠦᠬᠦᠯᠢ ᠲᠤᠭᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="163"/>
        <source>Setting is not of integer type</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠢ ᠪᠦᠬᠦᠯᠢ ᠲᠤᠭᠠᠨ ᠤ᠋ ᠳᠦᠷᠦᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="167"/>
        <source>Setting %s is not of string type</source>
        <translation>%s ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="169"/>
        <source>Setting is not of string type</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠨᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠬᠡᠯᠬᠢᠶ᠎ᠡ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="171"/>
        <source>Opening the configuration file failed</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="173"/>
        <source>The configuration file is malformed</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="175"/>
        <source>Fatal failure</source>
        <translation>ᠠᠮᠢᠨ ᠳ᠋ᠤ᠌ ᠳᠤᠯᠬᠤ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="177"/>
        <source>Unknown error</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="467"/>
        <source>unused</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="469"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="59"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="208"/>
        <source>kylin-data</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ</translation>
    </message>
</context>
</TS>
