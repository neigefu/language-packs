<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ky" sourcelanguage="en_AS">
<context>
    <name></name>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">Эки сырсөз жазуу бири-бирине карама-каршы!</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">Сырсөздү ыраста:</translation>
    </message>
    <message>
        <source>Kirin software store, featured software recommendation.</source>
        <translation type="obsolete">Kylin Software Store программалык камсыздоо боюнча сунуштарды камтыды.</translation>
    </message>
    <message>
        <source>password:</source>
        <translation type="obsolete">Сырсөз:</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">Сырсөзүңөрдү коопсуз сактаңыз. Сырсөзүңөрдү унутуп калсаңыз, дисктин маалыматтарына кирүү мүмкүн эмес.</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">Өчүрүү</translation>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="obsolete">Хостнамдар тамгаларды, сандарды, баса белгилерди жана дүрбөлөңдөрдү гана берет жана мындан ары 64 биттен ашык эмес.</translation>
    </message>
</context>
<context>
    <name>DiskInfoView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="267"/>
        <source>as data disk</source>
        <translation>Берилиштер дискине орнотулган</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="268"/>
        <source>System Partitions Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="273"/>
        <source>Data Partition Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="280"/>
        <source>Encrypted Partition</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::CreatePartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="444"/>
        <source>OK</source>
        <translatorcomment>确定</translatorcomment>
        <translation>Чын элеби</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="446"/>
        <source>Used to:</source>
        <translation>Үчүн:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="450"/>
        <source>Mount point</source>
        <translatorcomment>挂载点：</translatorcomment>
        <translation>Маунт-пойнт:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="452"/>
        <source>Location for the new partition</source>
        <translatorcomment>新分区的位置：</translatorcomment>
        <translation>Жаңы бөлүктүн жайгашкан жери</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="447"/>
        <source>Create Partition</source>
        <translation>Жаңы бөлүгүн түзүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="358"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>Тоо чекити &quot;/&quot; менен башталышы керек</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="296"/>
        <source>The disk can only create one root partition!</source>
        <translation>тамыр бөлүштүрүү гана түзүүгө болот, ал эми кийинчерээк түзүлгөн тамыр бөлүгү жараксыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="336"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>Бут кийим бөлүштүрүү гана ext4 же ext3 форматында болушу мүмкүн!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="320"/>
        <source>The disk can only create one boot partition!</source>
        <translation>Бут кийим бөлүштүрүү гана түзүүгө болот, ал эми кийинчерээк түзүлгөн бут кийим бөлүштүрүү жараксыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="346"/>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="546"/>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="380"/>
        <source>Mount point repeat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="453"/>
        <source>End of this space</source>
        <translation>Калган мейкиндиктин куйругу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="454"/>
        <source>Beginning of this space</source>
        <translatorcomment>剩余空间头部</translatorcomment>
        <translation>Калган космос башчысы</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="456"/>
        <source>Type for the new partition:</source>
        <translatorcomment>新分区的类型：</translatorcomment>
        <translation>Жаңы бөлүштүрүү түрү:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="522"/>
        <source>Recommended efi partition size is between 256MiB and 2GiB.</source>
        <translation>ЭФИ бөлүштүрүү көлөмү 256 МиБ жана 2 ГИБ ортосунда.</translation>
    </message>
    <message>
        <source>Recommended boot partition size is between 1GiB and 2GiB.
Note that the size must be greater than 500MiB.</source>
        <translation type="obsolete">Бут кийим бөлүштүрүү өлчөмү 1 ГИБ жана 2 ГИБ ортосунда болууга сунуш кылынат, жана 500 МиБ кем болушу мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="548"/>
        <source>Root partition size is greater than 15GiB, 
but Huawei machines require greater than 25GiB.</source>
        <translation>тамыр бөлүштүрүү көлөмү 15 ГИБ ашык болушу керек, ал эми Huawei машиналар 25 ГИБ ашык болушу керек.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="566"/>
        <source>close</source>
        <translation>Өчүрүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="462"/>
        <source>Size(MiB)</source>
        <translation>Көлөмү (МиБ)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="457"/>
        <source>Logical</source>
        <translatorcomment>逻辑分区</translatorcomment>
        <translation>Логикалык бөлүштүрүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="458"/>
        <source>Primary</source>
        <translatorcomment>主分区</translatorcomment>
        <translation>Баштапкы бөлүштүрүү</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="250"/>
        <source>remove this partition?</source>
        <translation>Сиз бул бөлүгүн жоготуп келет деп ишенесизби?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="252"/>
        <source>This is a system partition,remove this partition?</source>
        <translation>Сиз бул бөлүгүн жоготуп келет деп ишенесизби?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="348"/>
        <source>Device for boot loader path:</source>
        <translation>Жүктөөчү жол:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="349"/>
        <source>Revert</source>
        <translatorcomment>还原</translatorcomment>
        <translation>калыбына келтирүү</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitiondelegate</name>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="81"/>
        <source>#%1 partition on the device %2 will be created.
</source>
        <translation>Аппаратта #%1 бөлүгү %2 түзүлөт.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="85"/>
        <source>#%1 partition on the device %2 will be mounted %3.
</source>
        <translation>#%1 бөлүштүрүү %2 боюнча %3 катары орнотулат.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="90"/>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="99"/>
        <source>#%1 partition on the device %2 will be formated %3.
</source>
        <translation>#%1 бөлүштүрүү %2 боюнча %3 катары түзүлөт.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="95"/>
        <source>#%1 partition on the device %2 will be deleted.
</source>
        <translation>#%1 бөлүгү %2 орнотмосунда жок кылынат.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="103"/>
        <source>#%1 partition  on the device %2 will be mounted %3.
</source>
        <translation>#%1 бөлүштүрүү %2 боюнча %3 катары орнотулат.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="118"/>
        <source>%1 GPT new partition table will be created.
</source>
        <translation>%1 GPT бөлүштүрүү таблицасы түзүлөт.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="120"/>
        <source>%1 MsDos new partition table will be created.
</source>
        <translation>%1 MsDos бөлүштүрүү таблицасы түзүлөт.
</translation>
    </message>
</context>
<context>
    <name>KInstaller::FinishedFrame</name>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="80"/>
        <source>Installation Finished</source>
        <translation>Орнотуу аяктады</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="81"/>
        <source>Restart</source>
        <translation>Азыр кайра жүктөө</translation>
    </message>
</context>
<context>
    <name>KInstaller::FullPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="201"/>
        <source>This action will  use the original old account as the default account. Please make sure you remember the password of the original account!</source>
        <translation>Бул опцияны текшерүү баштапкы системанын колдонуучусунун жана каталогунун маалыматтарын, ошондой эле маалыматтарды бөлүштүрүүдөгү маалыматтарды сактайт. Версиясы дал келсе, система сиздин система активдештирүү маалыматын жана активдештирүү абалын дефолт боюнча сактайт.
Бул параметрди тандоо мындан ары жаңы колдонуучуларды түзүүгө түрткү берет, бул эсептер үчүн сырсөздөрдү эстеп калууга түрткү берет.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="293"/>
        <source>The current installation mode enforces factory backup configuration and cannot be unchecked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="327"/>
        <source>Please choose custom way to install, disk size less than 50G!</source>
        <translation>Орнотуу үчүн колдонуу ыкмасын тандап алыңыз, диск өлчөмү 50Г кем!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="328"/>
        <source>Full disk encryption</source>
        <translation>Толук диск шифрлоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="329"/>
        <source>lvm</source>
        <translation>Логикалык көлөмү (ЛВМ)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="330"/>
        <source>Factory backup</source>
        <translation>Фабрика резервдик</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="331"/>
        <source>save history date</source>
        <translation>Колдонуучуларды жана алардын маалыматтарын сактоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="481"/>
        <source>By default, SSDS serve as system disks and HDDS serve as user data disks</source>
        <translation>Дефолт боюнча, SSD диск системалык диск катары колдонулат жана HDD диск колдонуучу маалыматтар диск катары колдонулат</translation>
    </message>
</context>
<context>
    <name>KInstaller::ImgFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="213"/>
        <source>Select install way</source>
        <translation>Орнотуу жолун тандоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="214"/>
        <source>install from ghost</source>
        <translation>Елестен орнотуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="215"/>
        <source>install from live</source>
        <translation>Live-ден орнотуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="129"/>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="216"/>
        <source>open file</source>
        <translation>Файлды ачуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="217"/>
        <source>Next</source>
        <translation>Кийинки</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallerMainWidget</name>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="318"/>
        <source>About to exit the installer, restart the computer.</source>
        <translation>Орнотуудан чыгуу жана компьютериңизди кайра иштетүү.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="320"/>
        <source>About to exit the installer and return to the trial interface.</source>
        <translation>Орнотмого чыгып, сыноо экранына кайтуу.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="383"/>
        <source>Installer is about to exit and the computer will be shut down.</source>
        <translation>Орнотуу чыгууга жакын жана компьютер өчүрүлөт.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="682"/>
        <source>back</source>
        <translation>Мурунку кадам</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="659"/>
        <source>quit</source>
        <translation>чыгуу</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="636"/>
        <source>keyboard</source>
        <translation>алиптергич</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingFrame</name>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="149"/>
        <source>Log</source>
        <translation>Орнотуу журналдары</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="148"/>
        <source>The system is being installed, please do not turn off the computer</source>
        <translation>Система орнотулууда, компьютерди өчүрбөгүлө</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="187"/>
        <source>Install failed</source>
        <translation>Орнотуу ишке ашпады</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="189"/>
        <source>Sorry, KylinOS cannot continue the installation. Please feed back the error log below so that we can better solve the problem for you.</source>
        <translation>КайлинОС орнотууну уланта албай турганына өкүнөбүз. Сураныч, биз сиз үчүн көйгөйдү жакшыраак чечүү үчүн төмөндө ката кирүү пикир.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="196"/>
        <source>Restart</source>
        <translation>чыгуу</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingOEMConfigFrame</name>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="98"/>
        <source>Progressing system configuration</source>
        <translation>Системанын конфигурациясы аткарылууда</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="174"/>
        <source>OEM Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="180"/>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::LanguageFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="250"/>
        <source>Select Language</source>
        <translation>Тил тандоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="251"/>
        <source>Next</source>
        <translation>Кийинки</translation>
    </message>
</context>
<context>
    <name>KInstaller::LicenseFrame</name>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="192"/>
        <source>Read License Agreement</source>
        <translation>Лицензиялык келишимди окуп</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="193"/>
        <source>I have read and agree to the terms of the agreement</source>
        <translation>Мен окуп, келишимдин шарттарына макул болдум</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="195"/>
        <source>Next</source>
        <translation>Кийинки</translation>
    </message>
</context>
<context>
    <name>KInstaller::MainPartFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="843"/>
        <source>Custom install</source>
        <translatorcomment>自定义安装</translatorcomment>
        <translation>Колдонуучу орнотуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="472"/>
        <source>Coexist Install</source>
        <translation>Жан-тарап орнотуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="541"/>
        <source>Confirm Custom Installation</source>
        <translation>Колдонуучунун орнотулушун тастыктоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="311"/>
        <source>Formatted the whole disk</source>
        <translation>Бүт дискти форматтаңыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="562"/>
        <source>Start Installation</source>
        <translation>Орнотууну баштоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="365"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="320"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="330"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="340"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="351"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="375"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
swap partition #6 of  %1 as swap;
</source>
        <translation>%1 Аппараттын биринчи бөлүгү – ЕФИ, ал вфатка коюлат;
%1 Түзмөк боюнча экинчи бөлүгү жүктөө болуп саналат, ал экст4 белгиленген болот;
%1 Түзмөк боюнча үчүнчү бөлүгү болуп саналат /, бул экст4 үчүн белгиленген болот;
%1 Түзмөк боюнча 4-бөлүгү резервдик болуп саналат, ал экстрезивдүү белгиленген болот;
%1 Аппараттын 5-бөлүгү – бул маалыматтар, ал экст4-орунга коюлат;
%1 Түзмөк боюнча 6-бөлүгү своп болуп саналат жана алмаштыруу белгиленген болот;</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="385"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="396"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
swap partition #8 of  %1 as swap;
</source>
        <translation>%1 Түзмөктө биринчи бөлүгү жүктөө болуп саналат, ал экст4 үчүн белгиленген болот;
%1 бөлүштүрүү 2 түзмөк боюнча узартылат;
%1 Аппараттын 5-бөлүгү болуп саналат /, ал экст4 үчүн белгиленген болот;
%1 Түзмөк боюнча 6-бөлүгү резервдик болуп саналат, ал экстрезивдүү белгиленген болот;
%1 Аппараттын 7-бөлүгү – бул маалыматтар, ал экст4-орунга коюлат;
%1 Түзмөк боюнча 8-бөлүгү своп болуп саналат, ал алмаштыруу үчүн белгиленген болот;</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="417"/>
        <source>will it be formatted, continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="419"/>
        <source>The disk which was set as a data disk will be formatted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="549"/>
        <source>Confirm the above operations</source>
        <translation>Жогорудагы операцияларды тастыктагыла</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="703"/>
        <source>InvalidId
</source>
        <translation>боштук
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="709"/>
        <source>Boot filesystem invalid
</source>
        <translation>Жүктөө файлы системасы жеткиликтүү эмес
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="718"/>
        <source>the EFI partition size should be set between 256MiB and 2GiB
</source>
        <translation>ЭФИ бөлүштүрүү өлчөмү 256МБ жана 2ГБ ортосунда белгилениши керек.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="727"/>
        <source>Root partition size is greater than 15GiB,
but Huawei machines require greater than 25GiB.
</source>
        <translation>тамыр бөлүштүрүү көлөмү 15 ГИБ ашык, жана Huawei машиналар 25 Гиб ашык болушу керек.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="715"/>
        <source>Boot partition too small
</source>
        <translation>Бут кийим бөлүштүрүү өтө кичинекей
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="310"/>
        <source>Confirm Full Installation</source>
        <translation>Толук орнотуу ырастоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="421"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="434"/>
        <source> set to data device;
</source>
        <translation> Берилиштер диски катары орнотулган</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="502"/>
        <source>bootloader type of Legacy should not contain EFI partition!</source>
        <translation>Система мурас болгондо, анда ЭФИ бөлүктөрү камтыла албайт!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="616"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2.</source>
        <translation>%1 жүктөө ыкмасы %2 дискти бөлүштүрүү таблицага дал келбейт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="619"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="624"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2, cannot have efi partition.</source>
        <translation>%1 жүктөө ыкмасы %2 дискти бөлүштүрүү столго дал келбейт жана ЭФИ бөлүштүрүү колдонулбайт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="675"/>
        <source>No created of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="695"/>
        <source> partition
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="712"/>
        <source>Boot partition not in the first partition
</source>
        <translation>Бут кийим бөлүштүрүү биринчи бөлүгү болушу керек!</translation>
    </message>
    <message>
        <source>No boot partition
</source>
        <translation type="obsolete">Бут кийимдин бөлүнүшү табылган жок
</translation>
    </message>
    <message>
        <source>No Efi partition
</source>
        <translation type="obsolete">ЕФИ бөлүгү табылган жок
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="721"/>
        <source>Only one EFI partition is allowed
</source>
        <translation>ЭФИ-ге бир гана бөлүштүрүүгө уруксат берилет
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="724"/>
        <source>Efi partition number invalid
</source>
        <translation>ЭФИ бөлүштүрүү номери жеткиликтүү эмес
</translation>
    </message>
    <message>
        <source>No root partition
</source>
        <translation type="obsolete">Тамыры табылган жок.
</translation>
    </message>
    <message>
        <source>No backup partition
</source>
        <translation type="obsolete">Резервдик калыбына келтирүү бөлүгү табылган жок.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="743"/>
        <source>This machine not support EFI partition
</source>
        <translation>Азыркы учурда бул машина ЭФИ бөлүштүрүүнү колдобойт!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="791"/>
        <source>If the /swap partition is not created,computers cannot be woken up after the system goes to sleep. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="799"/>
        <source>If the /data partition is not created,user data cannot be backed up. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="842"/>
        <source>Full install</source>
        <translation>Толук орнотуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="706"/>
        <source>Partition too small
</source>
        <translation>Бөлүштүрүү өтө кичинекей
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="731"/>
        <source>BackUp partition too small
</source>
        <translation>Резервдик калыбына келтирүү бөлүштүрүү өтө аз
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="734"/>
        <source>Repeated mountpoint
</source>
        <translation>Бир нече жолу монтаждалган бөлүктөр бар
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="746"/>
        <source>The filesystem type FAT16 or FAT32 is not fully-function filesystem,
except EFI partition, other partition not proposed</source>
        <translation>FAT16 жана FAT32 толук эмес файл системалары болуп саналат.
ЕФИ бөлүктөрүн эске албаганда, бөлүштүрүүлөр сунуш кылынбайт.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="781"/>
        <source>If the </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="805"/>
        <source> partitions are not created,do you want to go to the next step?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="841"/>
        <source>Choose Installation Method</source>
        <translation>Орнотуу ыкмасын тандоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="588"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="601"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="845"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="901"/>
        <source>Next</source>
        <translation>Кийинки</translation>
    </message>
</context>
<context>
    <name>KInstaller::MiddleFrameManager</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/middleframemanager.cpp" line="44"/>
        <source>next</source>
        <translatorcomment>下一步</translatorcomment>
        <translation>Кийинки</translation>
    </message>
</context>
<context>
    <name>KInstaller::ModifyPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="46"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="206"/>
        <source>unused</source>
        <translation>Колдонулбайт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="192"/>
        <source>Format partition.</source>
        <translation>Бул бөлүштүрүү форматы.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="193"/>
        <source>OK</source>
        <translation>Чын элеби</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="195"/>
        <source>Used to:</source>
        <translation>Үчүн:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="196"/>
        <source>Modify Partition</source>
        <translation>Бөлүктөрүн өзгөртүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="429"/>
        <source>Mount point repeat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="325"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>Бут кийим бөлүштүрүү гана ext4 же ext3 форматында болушу мүмкүн!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="335"/>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="363"/>
        <source>The disk can only create one boot partition!</source>
        <translation>Бут кийим бөлүштүрүү гана түзүүгө болот, ал эми кийинчерээк түзүлгөн бут кийим бөлүштүрүү жараксыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="391"/>
        <source>The disk can only create one root partition!</source>
        <translation>тамыр бөлүштүрүү гана түзүүгө болот, ал эми кийинчерээк түзүлгөн тамыр бөлүгү жараксыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="407"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>Тоо чекити &quot;/&quot; менен башталышы керек</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="532"/>
        <source>close</source>
        <translation>Өчүрүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="201"/>
        <source>Mount point</source>
        <translation>Маунт-пойнт</translation>
    </message>
</context>
<context>
    <name>KInstaller::PrepareInstallFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/prepareinstallframe.cpp" line="109"/>
        <source>Check it and click [Start Installation]</source>
        <translatorcomment>勾选后点击[开始安装]</translatorcomment>
        <translation>Кутучаны текшерип, чыкылдатуу [Орнотууну баштоо]</translation>
    </message>
</context>
<context>
    <name>KInstaller::SlideShow</name>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="295"/>
        <source>Multi platform support, suitable for mainstream CPU platforms at home and abroad.</source>
        <translation>Көп платформалык колдоо, өлкөдөгү жана чет өлкөлөрдөгү негизги КПУ платформаларына ылайыкташтырылган.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="296"/>
        <source>Ukui desktop environment meets personalized needs.</source>
        <translation>Жеке муктаждыктарын канааттандыруу үчүн UKUI иш столунун чөйрөсү.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="297"/>
        <source>The system is updated and upgraded more conveniently.</source>
        <translation>Системаны жаңыртуу, жогорулатуу ыңгайлуураак.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="298"/>
        <source>Security center, all-round protection of data security.</source>
        <translation>Бардык аспектилер боюнча маалыматтардын коопсуздугун коргоо боюнча коопсуздук борбору.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="299"/>
        <source>Software store, featured software recommendation.</source>
        <translation>Программалык камсыздоо дүкөнүндө программалык камсыздоо боюнча сунуштар көрсөтүлдү.</translation>
    </message>
    <message>
        <source>Kirin software store, featured software recommendation.</source>
        <translation type="obsolete">Kylin Software Store программалык камсыздоо боюнча сунуштарды камтыды.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="300"/>
        <source>Provide technical support and professional after-sales service.</source>
        <translation>Техникалык колдоо, профессионалдык кийин сатуу кызматы менен камсыз кылуу.</translation>
    </message>
</context>
<context>
    <name>KInstaller::TableWidgetView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="111"/>
        <source>This action will delect all partition,are you sure?</source>
        <translation>Учурдагы бөлүштүрүү столу бошотулат беле, сиз аны боштондукка чыгара турганыңызга ишенесизби?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <source>yes</source>
        <translation>болуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="327"/>
        <source>no</source>
        <translation>эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="167"/>
        <source>Create partition table</source>
        <translation>Бөлүнгөн столду түзүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="321"/>
        <source>freespace</source>
        <translation>бош убакыт</translation>
    </message>
</context>
<context>
    <name>KInstaller::TimeZoneFrame</name>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="119"/>
        <source>Select Timezone</source>
        <translation>Убакыт белдеуин тандоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="232"/>
        <source>Start Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="127"/>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="236"/>
        <source>Next</source>
        <translation>Кийинки</translation>
    </message>
</context>
<context>
    <name>KInstaller::UserFrame</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="139"/>
        <source>Start Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="143"/>
        <source>Start Installation</source>
        <translation>Орнотууну баштоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="133"/>
        <source>Next</source>
        <translation>Кийинки</translation>
    </message>
</context>
<context>
    <name>KInstaller::appcheckframe</name>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="100"/>
        <source>Start Installation</source>
        <translation>Орнотууну баштоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="93"/>
        <source>Choose your app</source>
        <translation>Тиркемени тандоо</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="94"/>
        <source>Select the application you want to install to install with one click when using the system.</source>
        <translation>Системаны колдонууда бир чыкылдатуу менен орнотуу үчүн орнотууну каалаган тиркемени тандаңыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="97"/>
        <source>Start Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::conferquestion</name>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="105"/>
        <source>confer_mainTitle</source>
        <translation>Коопсуздук маселелери</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="106"/>
        <source>You can reset your password by answering a security question when you forget your password. Make sure you remember the answer.</source>
        <translation>Коопсуздук суроосуна жооп берүү менен аны унутуп калсаңар, сырсөзүңөрдү калыбына келтире аласыз, жоопту эстеп калганыңызга ынаныңыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="109"/>
        <source>Which city were you born in?</source>
        <translation>Сиз төрөлгөн шаардын аты?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="110"/>
        <source>What is your childhood nickname?</source>
        <translation>Балалык лақап атыңар кандай эле?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="111"/>
        <source>Which middle school did you graduate from?</source>
        <translation>Кайсы орто мектепти бүтүрдүңөр?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="112"/>
        <source>What is your father&apos;s name?</source>
        <translation>Атаңыздын аты?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="113"/>
        <source>What is your mother&apos;s name?</source>
        <translation>Сиздин эненин аты?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="114"/>
        <source>When is your spouse&apos;s birthday?</source>
        <translation>Жубайыңардын туулган күнү?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="115"/>
        <source>What&apos;s your favorite animal?</source>
        <translation>Сиздин сүйүктүү жаныбар?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="119"/>
        <source>question%1:</source>
        <translation>Суроо %1:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="120"/>
        <source>answer%1:</source>
        <translation>Жооп %1:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="126"/>
        <source>set later</source>
        <translation>Кийинчерээк орнотуу</translation>
    </message>
</context>
<context>
    <name>KInstaller::modeselect</name>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="62"/>
        <source>Register account</source>
        <translation>Эсеп түзүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="68"/>
        <source>Register immediately</source>
        <translation>Азыр жаратуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="76"/>
        <source>Register later</source>
        <translation>Кийинчерээк жаратуу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="77"/>
        <source>After the system installation is completed, an account will be created when entering the system.</source>
        <translation>Системаны орнотуу бүткөндөн кийин системага киргенде эсеп түзүлөт.</translation>
    </message>
</context>
<context>
    <name>KInstaller::securityquestions</name>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="404"/>
        <source>Security Questions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="405"/>
        <source>You can reset your password by answering security questions when you forget it, please make sure to remember the answer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="406"/>
        <source>Question 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="407"/>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="409"/>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="411"/>
        <source>Answer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="408"/>
        <source>Question 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="410"/>
        <source>Question 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="412"/>
        <source>Set up later</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::userregisterwidiget</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="442"/>
        <source>Two password entries are inconsistent!</source>
        <translation>Эки сырсөз жазуу бири-бирине карама-каршы!</translation>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="obsolete">Хостнамдар тамгаларды, сандарды, баса белгилерди жана дүрбөлөңдөрдү гана берет жана мындан ары 64 биттен ашык эмес.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="153"/>
        <source>Password strength:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="494"/>
        <source>Hostname should be more than 0 bits in length.</source>
        <translation>Хостнамдын узундугу , жок эле дегенде , 1 мүнөздүү болушу керек</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="496"/>
        <source>Hostname should be no more than 64 bits in length.</source>
        <translation>Хост атынын узундугу 64 тамгадан аша албайт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="498"/>
        <source>Hostname only letters,numbers,hyphen and dot notation are allowed</source>
        <translation>Хост аттары тамгаларды, номерлерди, туташтыргычтарды, же чекит символдорун гана камтууга уруксат берилет</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="500"/>
        <source>Hostname must start with a number or a letter</source>
        <translation>Хост аты башында кат же номери болушу керек</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="502"/>
        <source>Hostname must end with a number or a letter</source>
        <translation>Хост атынын ээрчиген бөлүгү кат же номер болушу керек</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="504"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>Хост аттарындагы дүрбөлөңдөрдү жана чекит символдорун сактоо мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="506"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>Хост аты бир катар чекит символдорун камтый албайт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="669"/>
        <source>Create User</source>
        <translation>Колдонуучуну түзүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="670"/>
        <source>username</source>
        <translation>Колдонуучунун аты</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="671"/>
        <source>hostname</source>
        <translation>хост аты</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="672"/>
        <source>new password</source>
        <translation>сырсөз</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="673"/>
        <source>enter the password again</source>
        <translation>Сырсөздү кайра киргизүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="674"/>
        <source>Automatic login on boot</source>
        <translation>Стартапта автоматтык кирүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="679"/>
        <source>After logging into the system, it is recommended that you set biometric passwords and password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="681"/>
        <source>After logging into the system, it is recommended that you set password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="684"/>
        <source>After logging into the system, it is recommended that you set the biometric password in [Settings - Login Options] to have a better experience in the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Biometric [authentication] device detected / unified login support</source>
        <translation type="obsolete">Биометриялык [аутентификация] түзмөк / бирдиктүү кирүү колдоосу аныкталды,
Сураныч, аны биометриялык башкаруу инструменттеринде / параметрлеринде орнотуу - Кирүү параметрлери.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="725"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="738"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="749"/>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KServer::EncryptSetFrame</name>
    <message>
        <source>password:</source>
        <translation type="obsolete">Сырсөз:</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">Сырсөздү ыраста:</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">Сырсөзүңөрдү коопсуз сактаңыз. Сырсөзүңөрдү унутуп калсаңыз, дисктин маалыматтарына кирүү мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="40"/>
        <source>password</source>
        <translation>сырсөз</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="161"/>
        <source>The password contains less than two types of characters</source>
        <translation>Сырсөз 2 символдук түрдөн аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="41"/>
        <source>confirm password</source>
        <translation>Сырсөздү ыраста</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="242"/>
        <source>The two passwords are different</source>
        <translation>Сырсөздөр бири - бирине карама - каршы келбейт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="268"/>
        <source>Please keep your password properly.If you forget it,you will not be able to access the disk data.</source>
        <translation>Сырсөзүңөрдү коопсуз сактаңыз, унутуп калсаңар, дискке кирүүгө мүмкүн эмес.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="359"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="360"/>
        <source>OK</source>
        <translation>Чын элеби</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="430"/>
        <source>close</source>
        <translation>Өчүрүү</translation>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">Эки сырсөз жазуу бири-бирине карама-каршы!</translation>
    </message>
</context>
<context>
    <name>KServer::MessageBox</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="199"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="201"/>
        <source>OK</source>
        <translation>Чын элеби</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="295"/>
        <source>close</source>
        <translation>Өчүрүү</translation>
    </message>
</context>
<context>
    <name>KServer::SetPartitionsSize</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="219"/>
        <source>Data Partitions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="348"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="349"/>
        <source>OK</source>
        <translation>Чын элеби</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="353"/>
        <source>System Partitions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="355"/>
        <source>Data Partitions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="359"/>
        <source>Size range from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="359"/>
        <source> to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="487"/>
        <source>close</source>
        <translation>Өчүрүү</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../src/plugins/VirtualKeyboard/src/keyboardwidget.ui" line="29"/>
        <source>KeyboardWidget</source>
        <translation>АлиптергичВиджет</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="9"/>
        <source>wd</source>
        <translation>Батыш санариптик</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="11"/>
        <source>seagate</source>
        <translation>Сигейт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="13"/>
        <source>hitachi</source>
        <translation>Хитачи</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="15"/>
        <source>samsung</source>
        <translation>Samsung</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="17"/>
        <source>toshiba</source>
        <translation>Тошиба</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="19"/>
        <source>fujitsu</source>
        <translation>Фудзицу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="21"/>
        <source>maxtor</source>
        <translation>Макстор</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="23"/>
        <source>IBM</source>
        <translation>IBM</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="25"/>
        <source>excelStor</source>
        <translation>Жогоркуга жеңил</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="27"/>
        <source>lenovo</source>
        <translation>бирикмеси</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="29"/>
        <source>other</source>
        <translation>башка</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="31"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="303"/>
        <source>Unknown</source>
        <translation>Белгисиз</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="80"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="314"/>
        <source>Freespace</source>
        <translation>бош убакыт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="294"/>
        <source>kylin data partition</source>
        <translation>Берилиштер диски</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="301"/>
        <source>Swap partition</source>
        <translation>Бөлүштүрүү алмаштыруу</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/main.cpp" line="158"/>
        <source>Show debug informations</source>
        <translation>Дебют тууралуу маалыматты чагылдырат</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="191"/>
        <source>Extended partition %1 has 
</source>
        <translation>Узартылган бөлүштүрүү %1 Ооба</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="193"/>
        <source>Create new partition %1,%2
</source>
        <translation>%1,2 жаңы бөлүктөрүн түзүү
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="196"/>
        <source>Create new partition %1,%2,%3
</source>
        <translation>%1,2,3% жаңы бөлүктөрүн түзүү
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="201"/>
        <source>Delete partition %1
</source>
        <translation>%1 бөлүштүрүлүшүн жоготуу
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="205"/>
        <source>Format %1 partition, %2
</source>
        <translation>Форматты бөлүштүрүү %1,2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="208"/>
        <source>Format partition %1,%2,%3
</source>
        <translation>Формат бөлүштүрүү %1, %2, %3
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="213"/>
        <source>%1 partition mountPoint %2
</source>
        <translation>%1 бөлүштүрүү бириктирүү пункту %2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="217"/>
        <source>New Partition Table %1
</source>
        <translation>%1 жаңы бөлүштүрүү таблицасы
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="220"/>
        <source>Reset size %1 partition
</source>
        <translation>Бөлүштүрүү %1 өлчөмүн калыбына келтирүү</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="30"/>
        <source>KCommand::m_cmdInstance is not init.</source>
        <translation>Команда инициализацияланбаган m_cmdInstance</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="67"/>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="99"/>
        <source>WorkingPath is not found. 
</source>
        <translation>Жумушчу каталог табылган жок</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="91"/>
        <source>Shell file is empty, does not continue. 
</source>
        <translation>Кабыгы файлы бош жана тапшырма токтойт.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>device</source>
        <translation>жабдуулар</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>type</source>
        <translation>түрү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>size</source>
        <translation>өлчөмү</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>mounted</source>
        <translation>Маунт-пойнт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>used</source>
        <translation>Колдонулган</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>system</source>
        <translation>система</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>format</source>
        <translation>форматы</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="69"/>
        <source>Memory allocation error when setting</source>
        <translation>Орнотуу учурунда эс бөлүштүрүү катасы пайда болгон</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="73"/>
        <source>Memory allocation error</source>
        <translation>Эс бөлүштүрүү катасы</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="75"/>
        <source>The password is the same as the old one</source>
        <translation>Бул сырсөз оригиналдуу менен бирдей</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="77"/>
        <source>The password is a palindrome</source>
        <translation>Сырсөз — палиндром</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="79"/>
        <source>The password differs with case changes only</source>
        <translation>Сырсөздөрдө жагдайдагы өзгөрүүлөр гана бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="81"/>
        <source>The password is too similar to the old one</source>
        <translation>Сырсөз түп нускасына өтө окшош</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="83"/>
        <source>The password contains the user name in some form</source>
        <translation>Сырсөз колдонуучунун атынын кандайдыр бир түрүн камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="85"/>
        <source>The password contains words from the real name of the user in some form</source>
        <translation>Сырсөз колдонуучунун чыныгы ысмынын кандайдыр бир түрүн камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="87"/>
        <source>The password contains forbidden words in some form</source>
        <translation>Сырсөз тыюу салынган сөздүн кандайдыр бир түрүн камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="92"/>
        <source>The password contains too few digits</source>
        <translation>Сырсөздө сандык символдор өтө аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="97"/>
        <source>The password contains too few uppercase letters</source>
        <translation>Сырсөздө үстүнкү тамгалар өтө аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="102"/>
        <source>The password contains too few lowercase letters</source>
        <translation>Сырсөздө ылдыйкы тамгалар өтө аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="107"/>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation>Сырсөздө өзгөчө символдор өтө аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="112"/>
        <source>The password is too short</source>
        <translation>Сырсөз өтө кыска</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="114"/>
        <source>The password is just rotated old one</source>
        <translation>Сырсөз — эски сырсөздүн өзгөрүшү</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="119"/>
        <source>The password does not contain enough character classes</source>
        <translation>Сырсөздө мүнөздүн жетиштүү түрлөрү жок</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="124"/>
        <source>The password contains too many same characters consecutively</source>
        <translation>Сырсөздө бирдей символдор өтө көп</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="129"/>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation>Сырсөздө бир типтеги бир катар символдор өтө көп</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="90"/>
        <source>The password contains less than %1 digits</source>
        <translation>Сырсөздө сандык символдордун %1 сандан аз саны бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="95"/>
        <source>The password contains less than %1 uppercase letters</source>
        <translation>Сырсөздүн үстүнкү тамгалары %1 санга караганда аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="100"/>
        <source>The password contains less than %1 lowercase letters</source>
        <translation>Сырсөз 1 % дан аз төмөнкү тамгаларды камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="105"/>
        <source>The password contains less than %1 non-alphanumeric characters</source>
        <translation>Сырсөздүн 1 % дан аз атайын символдору бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="110"/>
        <source>The password is shorter than %1 characters</source>
        <translation>Сырсөз % 1 символдорунан аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="117"/>
        <source>The password contains less than %1 character classes</source>
        <translation>Сырсөз 1 %дан аз символдук түрлөрүн камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="122"/>
        <source>The password contains more than %1 same characters consecutively</source>
        <translation>Сырсөз бир катар символдордун % 1 ашык камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="127"/>
        <source>The password contains more than %1 characters of the same class consecutively</source>
        <translation>Сырсөз бир типтеги 1 % дан ашык ырааттуу символдорду камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="132"/>
        <source>The password contains monotonic sequence longer than %1 characters</source>
        <translation>Сырсөз % 1 символдон ашык монотондук ырааттуулугун камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="134"/>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation>Сырсөз өтө узун монотоникалык мүнөздүү тартипти камтыйт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="136"/>
        <source>No password supplied</source>
        <translation>Сырсөз берилбейт</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="138"/>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation>РНГ кокусунан санды өндүрүү түзмөктөрүнөн кокусунан сандарды ала албай калды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="140"/>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation>Сырсөз мууну ишке ашпады - Орнотуу үчүн зарыл болгон энтропияга жетүү мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="143"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="145"/>
        <source>The password fails the dictionary check</source>
        <translation>Сырсөз сөздүк текшерүү ишке ашпады - Сырсөз сөздүк сөздөргө негизделет</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="149"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="151"/>
        <source>Unknown setting</source>
        <translation>Белгисиз параметр</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="155"/>
        <source>Bad integer value of setting</source>
        <translation>Туура эмес интегер орнотуу наркы</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="157"/>
        <source>Bad integer value</source>
        <translation>Туура эмес интегер орнотуу наркы</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="161"/>
        <source>Setting %s is not of integer type</source>
        <translation>%s орнотуу интегер түрү эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="163"/>
        <source>Setting is not of integer type</source>
        <translation>Орнотуу интегер түрү эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="167"/>
        <source>Setting %s is not of string type</source>
        <translation>%s орнотуу сап түрү эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="169"/>
        <source>Setting is not of string type</source>
        <translation>Орнотуу сап түрү эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="171"/>
        <source>Opening the configuration file failed</source>
        <translation>Конфигурация файлын ачуу ишке ашпады</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="173"/>
        <source>The configuration file is malformed</source>
        <translation>Конфигурация файлынын форматы туура эмес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="175"/>
        <source>Fatal failure</source>
        <translation>Өлүмгө дуушар кыла турган</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="177"/>
        <source>Unknown error</source>
        <translation>Белгисиз ката</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="467"/>
        <source>unused</source>
        <translation>Колдонулбайт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="469"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="59"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="208"/>
        <source>kylin-data</source>
        <translation>Колдонуучулардын маалыматтарын бөлүштүрүү</translation>
    </message>
</context>
</TS>
