<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ug" sourcelanguage="en_AS">
<context>
    <name></name>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">ئىككى قېتىملىق مەخپىي نومۇرنى كىرگۈزۈش بىردەك ئەمەس!</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">مەخپىي نومۇرنى جەزملەشتۈر</translation>
    </message>
    <message>
        <source>Kirin software store, featured software recommendation.</source>
        <translation type="obsolete">بولىن يۇمشاق دېتال دۇكىنى، تاللانغان يۇمشاق دېتال تەۋسىيە قىلىنىدۇ.</translation>
    </message>
    <message>
        <source>password:</source>
        <translation type="obsolete">مەخپىي نومۇر</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">مەخپىي نومۇرىڭىزنى ياخشى ساقلاڭ. ئەگەر مەخپىي نومۇرنى ئۇنتۇپ قالسىڭىز، ماگنىتلىق دىسكا سانلىق مەلۇماتىنى زىيارەت قىلغىلى بولماي</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">تاقاش</translation>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="obsolete">باش كومپيۇتېرنىڭ ئىسمى پەقەت ھەرپ، رەقەم، ئاستى سىزىق سىزىش ۋە ئۇلاش بەلگىسىلا يول قويۇلىدۇ، ئۇنىڭ ئۈستىگە ئۇزۇنلۇقى 64 خانىدىن ئاشمايدۇ.</translation>
    </message>
</context>
<context>
    <name>DiskInfoView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="267"/>
        <source>as data disk</source>
        <translation>سانلىق مەلۇمات دىسكىسى قىلىپ تەسىس قىلىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="268"/>
        <source>System Partitions Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="273"/>
        <source>Data Partition Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="280"/>
        <source>Encrypted Partition</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::CreatePartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="444"/>
        <source>OK</source>
        <translatorcomment>确定</translatorcomment>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="446"/>
        <source>Used to:</source>
        <translation>ئىشلىتىلسە</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="450"/>
        <source>Mount point</source>
        <translatorcomment>挂载点：</translatorcomment>
        <translation>يۈك ئېسىش نۇقتىسى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="452"/>
        <source>Location for the new partition</source>
        <translatorcomment>新分区的位置：</translatorcomment>
        <translation>يىڭى شۆبە رايونىنىڭ ئورنى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="447"/>
        <source>Create Partition</source>
        <translation>يېڭىدىن شۆبە رايون قۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="358"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>يۈك ئېسىش نۇقتىسىنى «/باشلىنىش كېرەك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="296"/>
        <source>The disk can only create one root partition!</source>
        <translation>يىلتىز تارماق رايونى پەقەت بىرلا بەرپا قىلغىلى بولىدۇ، كېيىن قايتا بەرپا قىلىنغان يىلتىز تارماق رايونى ئىناۋەتسىز!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="336"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>boot شۆبە رايونىنىڭ شەكلى پەقەت ext4 ياكى ext3</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="320"/>
        <source>The disk can only create one boot partition!</source>
        <translation>boot شۆبە رايونىدا پەقەت بىرلا قۇرۇلىدۇ، كېيىن قايتا قۇرۇلغان boot شۆبە رايونى ئىناۋەتسىز</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="346"/>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="546"/>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="380"/>
        <source>Mount point repeat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="453"/>
        <source>End of this space</source>
        <translation>قالغان بوشلۇقنىڭ قۇيرۇق قىسمى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="454"/>
        <source>Beginning of this space</source>
        <translatorcomment>剩余空间头部</translatorcomment>
        <translation>قالغان بوشلۇق باش قىسمى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="456"/>
        <source>Type for the new partition:</source>
        <translatorcomment>新分区的类型：</translatorcomment>
        <translation>يېڭى شۆبە رايونىنىڭ تىپى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="522"/>
        <source>Recommended efi partition size is between 256MiB and 2GiB.</source>
        <translation>شۆبە رايوننىڭ چوڭ-كىچىكلىكى 256MiBدىن 2GiB ئارىلىقىدا EFIB</translation>
    </message>
    <message>
        <source>Recommended boot partition size is between 1GiB and 2GiB.
Note that the size must be greater than 500MiB.</source>
        <translation type="obsolete">boot شۆبە رايونىنىڭ چوڭ-كىچىكلىكى 1GiBدىن 2GiB ئارىلىقىدا تەۋسىيە قىلىنىدۇ، 500MiBتىن كىچىك بولماسلىقى كېرەك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="548"/>
        <source>Root partition size is greater than 15GiB, 
but Huawei machines require greater than 25GiB.</source>
        <translation>يىلتىز شۆبە رايونىنىڭ چوڭ-كىچىكلىكى 15GiBدىن چوڭ بولۇشى كېرەك، خۇاۋېينىڭ ماشىنا تەلىپى 25GiB دىن چوڭ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="566"/>
        <source>close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="462"/>
        <source>Size(MiB)</source>
        <translation>چوڭ - كىچىكلىكى ( MiB )</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="457"/>
        <source>Logical</source>
        <translatorcomment>逻辑分区</translatorcomment>
        <translation>لوگىكىلىق رايونغا ئايرىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="458"/>
        <source>Primary</source>
        <translatorcomment>主分区</translatorcomment>
        <translation>ئاساسىي رايون</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="250"/>
        <source>remove this partition?</source>
        <translation>بۇ شۆبە رايوننى يۇيىۋېتىشنى جەزىملەشتۈرۈش ؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="252"/>
        <source>This is a system partition,remove this partition?</source>
        <translation>بۇ شۆبە رايوننى يۇيىۋېتىشنى جەزىملەشتۈرۈش ؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="348"/>
        <source>Device for boot loader path:</source>
        <translation>قاچىلاش تەرتىپى يولىغا يېتەكچىلىك قىلىش:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="349"/>
        <source>Revert</source>
        <translatorcomment>还原</translatorcomment>
        <translation>ئەسلىگە كەلتۈرۈش</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitiondelegate</name>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="81"/>
        <source>#%1 partition on the device %2 will be created.
</source>
        <translation>تارماق رايوندا ئۈسكۈنىلەردە ٪2 قۇرۇلىدۇ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="85"/>
        <source>#%1 partition on the device %2 will be mounted %3.
</source>
        <translation>1 پىرسەنت تارماق رايونى ٪2 نىڭ ئۈستىدە ٪3 لىك يۈك بېسىلىدۇ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="90"/>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="99"/>
        <source>#%1 partition on the device %2 will be formated %3.
</source>
        <translation>1 پىرسەنت تارماق رايونى ٪2 نىڭ شەكلى ٪3كە ئايلىنىدۇ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="95"/>
        <source>#%1 partition on the device %2 will be deleted.
</source>
        <translation>تارماق رايوندا ئۈسكۈنىلەر ٪2 ئوتتۇرىسىدا يۇيىۋېتىلىدۇ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="103"/>
        <source>#%1 partition  on the device %2 will be mounted %3.
</source>
        <translation>1 پىرسەنت تارماق رايونى ٪2 نىڭ ئۈستىدە ٪3 لىك يۈك بېسىلىدۇ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="118"/>
        <source>%1 GPT new partition table will be created.
</source>
        <translation>٪1 GPT رايونغا ئايرىش جەدۋىلى قۇرۇلىدۇ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="120"/>
        <source>%1 MsDos new partition table will be created.
</source>
        <translation>1 MsDos شۆبە رايون جەدۋىلى قۇرۇلىدۇ
</translation>
    </message>
</context>
<context>
    <name>KInstaller::FinishedFrame</name>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="80"/>
        <source>Installation Finished</source>
        <translation>قۇراشتۇرۇش تاماملاندى</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="81"/>
        <source>Restart</source>
        <translation>ھازىر قايتىدىن قوزغىتىش</translation>
    </message>
</context>
<context>
    <name>KInstaller::FullPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="201"/>
        <source>This action will  use the original old account as the default account. Please make sure you remember the password of the original account!</source>
        <translation>بۇ تاللاش تۈرىدە ئەسلىدىكى سىستېمىدىكى ئابونتلار ۋە ئۇنىڭ مۇندەرىجىسىدىكى سانلىق مەلۇماتلارنى ساقلاپ قالغىلى بولىدۇ شۇنىڭ بىلەن بىر ۋاقىتتا داتا شۆبە رايونىدىكى سانلىق مەلۇماتلار ساقلاپ قېلىنىدۇ. نۇسخىسى ماسلاشقان ئەھۋالدا، سىستېما سىزنىڭ سىستېمىڭىزنىڭ ئاكتىپلاشتۇرۇش ئۇچۇرى ۋە ئاكتىپلاشتۇرۇش ھالىتىنى ئېتىراپ قىلىدۇ.
بۇ تاللاش تۈرىنى تاللاش سىزنىڭ يېڭىدىن قۇرغان ئابونتىڭىزنى ئەسكەرتمەيسىز،بۇ ھېساباتنىڭ مەخپىي نومۇرىنى خاتىرىلەپ قويۇشىڭىزغا كاپالەتلىك قىلىڭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="293"/>
        <source>The current installation mode enforces factory backup configuration and cannot be unchecked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="327"/>
        <source>Please choose custom way to install, disk size less than 50G!</source>
        <translation>ئۆزى بەلگىلەش ئۇسۇلىنى تاللاپ قۇراشتۇرۇڭ، ماگنىتلىق دىسكىنىڭ چوڭ- كىچىكلىكى 50G تىن كىچىك بولىدۇ!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="328"/>
        <source>Full disk encryption</source>
        <translation>پۈتۈنلەي شىفىر قوشۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="329"/>
        <source>lvm</source>
        <translation>لوگىكىلىق توم (lvm)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="330"/>
        <source>Factory backup</source>
        <translation>زاۋۇتتىن چىقىپ زاپاس نۇسخ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="331"/>
        <source>save history date</source>
        <translation>خېرىدار ۋە ئۇنىڭ سانلىق مەلۇماتىنى ساقلاپ قېلىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="481"/>
        <source>By default, SSDS serve as system disks and HDDS serve as user data disks</source>
        <translation>ئېتىراپ قىلغاندا SSD ماگنىتلىق دىسكا سىستېما دىسكىسى بولۇش سۈپىتى بىلەن HDD ماگنىتلىق دىسكا ئابونتلارنىڭ سانلىق مەلۇمات دىسكىسى بولۇش سۈپىتى بىلەن ئورنىتىلغان</translation>
    </message>
</context>
<context>
    <name>KInstaller::ImgFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="213"/>
        <source>Select install way</source>
        <translation>ئورنىتىش يولىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="214"/>
        <source>install from ghost</source>
        <translation>Ghostدىن قۇراشتۇرماق</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="215"/>
        <source>install from live</source>
        <translation>Liveدىن قۇراشتۇرماق</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="129"/>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="216"/>
        <source>open file</source>
        <translation>ھۆججەتنى ئېچىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="217"/>
        <source>Next</source>
        <translation>كېيىنكى قەدەمدە</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallerMainWidget</name>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="318"/>
        <source>About to exit the installer, restart the computer.</source>
        <translation>پات ئارىدا قۇراشتۇرۇش پروگراممىسىدىن چېكىنىپ چىقىپ، كومپيۇتېرنى قايتىدىن قوزغىتىدۇ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="320"/>
        <source>About to exit the installer and return to the trial interface.</source>
        <translation>پات ئارىدا قۇراشتۇرۇش پروگراممىسىدىن چېكىنىپ چىقىپ، سىناپ ئىشلىتىش كۆرۈنمە يۈزىگە قايتىدۇ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="383"/>
        <source>Installer is about to exit and the computer will be shut down.</source>
        <translation>قۇراشتۇرۇش پروگراممىسى چېكىنىپ چىقىش ئالدىدا، كومپيۇتېر ئېتىك.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="682"/>
        <source>back</source>
        <translation>ئالدىنقى قەدەمدە</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="659"/>
        <source>quit</source>
        <translation>چېكىنىپ چىقماق</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="636"/>
        <source>keyboard</source>
        <translation>كۇنۇپكا تاختىسى</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingFrame</name>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="149"/>
        <source>Log</source>
        <translation>كۈندىلىك خاتىرىنى قۇراشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="148"/>
        <source>The system is being installed, please do not turn off the computer</source>
        <translation>سىستىما ئورنىتىلىۋاتىدۇ،كومپىيوتىرنى ئېتىۋەتمەڭ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="187"/>
        <source>Install failed</source>
        <translation>قۇراشتۇرۇش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="189"/>
        <source>Sorry, KylinOS cannot continue the installation. Please feed back the error log below so that we can better solve the problem for you.</source>
        <translation>ئىنتايىن خىجىلمەن kylinOSنى داۋاملىق ئورناتقىلى بولمىدى ئاستىدىكى خاتا كۈندىلىك خاتىرىنى ئىنكاس قىلىڭ ، بىزنىڭ مەسىلىڭىزنى تېخىمۇ ياخشى ھەل قىلىشىمىزغا قولايلىق يارىتىپ بېرىمەن.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="196"/>
        <source>Restart</source>
        <translation>چېكىنىپ چىقماق</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingOEMConfigFrame</name>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="98"/>
        <source>Progressing system configuration</source>
        <translation>سىستېمىنى تەقسىملەش ئېلىپ بېرىلىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="174"/>
        <source>OEM Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="180"/>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::LanguageFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="250"/>
        <source>Select Language</source>
        <translation>تىل تاللاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="251"/>
        <source>Next</source>
        <translation>كېيىنكى قەدەمدە</translation>
    </message>
</context>
<context>
    <name>KInstaller::LicenseFrame</name>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="192"/>
        <source>Read License Agreement</source>
        <translation>ئوقۇش ئىجازەتنامىسى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="193"/>
        <source>I have read and agree to the terms of the agreement</source>
        <translation>مەن كېلىشىم ماددىلىرىنى ئوقۇپ قوشۇلدۇم ھەم قوشۇلدۇم</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="195"/>
        <source>Next</source>
        <translation>كېيىنكى قەدەمدە</translation>
    </message>
</context>
<context>
    <name>KInstaller::MainPartFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="843"/>
        <source>Custom install</source>
        <translatorcomment>自定义安装</translatorcomment>
        <translation>ئۆزلۈكىدىن ئېنىقلىما بېرىش قۇراشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="472"/>
        <source>Coexist Install</source>
        <translation>تەڭ ساقلىنىپ قۇراشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="541"/>
        <source>Confirm Custom Installation</source>
        <translation>ئۆزى بەلگىلەنگەن قۇراشتۇرۇشنى جەزملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="311"/>
        <source>Formatted the whole disk</source>
        <translation>فورماتلاشقان پۈتۈن ماگنىتلىق دىسكا .</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="562"/>
        <source>Start Installation</source>
        <translation>قۇراشتۇرۇشنى باشلاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="365"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="320"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="330"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="340"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="351"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="375"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
swap partition #6 of  %1 as swap;
</source>
        <translation>٪1 ئۈسكۈنىدىكى 1- تارماق رايون EFI بولۇپ، vfat قىلىپ بېكىتىلىدۇ
٪1 ئۈسكۈنىدىكى 2 – تارماق رايون boot بولۇپ ، ext4 قىلىپ بېكىتىلىدۇ
٪1 ئۈسكۈنىدىكى 3 – تارماق رايون ( بولۇپ ، ext4 قىلىپ بېكىتىلىدۇ
٪1 ئۈسكۈنىدىكى 4- تارماق رايون backup بولۇپ ، ext4 قىلىپ تەڭشەلىدۇ
٪1 ئۈسكۈنىدىكى 5- تارماق رايون data بولۇپ، ext4 بولۇپ بېكىتىلىدۇ
٪1 ئۈسكۈنىدىكى 6- تارماق رايون swap بولۇپ، swap قىلىپ بېكىتىلىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="385"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="396"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
swap partition #8 of  %1 as swap;
</source>
        <translation>٪1 ئۈسكۈنىدىكى 1- تارماق رايون boot بولۇپ، ext4 قىلىپ بېكىتىلىدۇ
٪1 ئۈسكۈنىدىكى 2 – تارماق رايون بولسا extend
٪1 ئۈسكۈنىدىكى 5 – تارماق رايون ( بولۇپ ، ext4 ى قىلىپ بېكىتىلىدۇ
٪1 ئۈسكۈنىدىكى 6- تارماق رايون backup بولۇپ ، ext4 قىلىپ تەسىس قىلىنىدۇ
٪1 ئۈسكۈنىدىكى 7- تارماق رايون data بولۇپ، ext4 قىلىپ بېكىتىلىدۇ
(1 ئۈسكۈنىدىكى سەككىزىنچى شۆبە رايون swap بولۇپ، swap قىلىپ بېكىتىلىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="417"/>
        <source>will it be formatted, continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="419"/>
        <source>The disk which was set as a data disk will be formatted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="549"/>
        <source>Confirm the above operations</source>
        <translation>يۇقىرىقى مەشغۇلاتنى جەزملەشتۈرمەك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="703"/>
        <source>InvalidId
</source>
        <translation>ئىناۋەتسىز
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="709"/>
        <source>Boot filesystem invalid
</source>
        <translation>رايونغا بۆلۈنگەن ھۆججەت سىستېمىسىنى ئىشلەتكىلى بولمايدۇ Boot
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="718"/>
        <source>the EFI partition size should be set between 256MiB and 2GiB
</source>
        <translation>شۆبە رايوننىڭ چوڭ-كىچىكلىكىنى 256MiBدىن 2GiBغىچە ئورۇنلاشتۇرۇش كېرەك Efi</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="727"/>
        <source>Root partition size is greater than 15GiB,
but Huawei machines require greater than 25GiB.
</source>
        <translation>يىلتىز شۆبە رايونىنىڭ چوڭ-كىچىكلىكى 15GiBدىن چوڭ، خۇاۋېينىڭ ماشىنا تەلىپى 25GiB دىن چوڭ.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="715"/>
        <source>Boot partition too small
</source>
        <translation>تارماق رايون بەك كىچىك Boot
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="310"/>
        <source>Confirm Full Installation</source>
        <translation>پۈتۈن تەخسىلىك قۇراشتۇرۇشنى جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="421"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="434"/>
        <source> set to data device;
</source>
        <translation> سانلىق مەلۇمات دىسكىسى قىلىپ تەسىس قىلىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="502"/>
        <source>bootloader type of Legacy should not contain EFI partition!</source>
        <translation>سىستىما legacy قوزغىتىش ئۇسۇلى بولغاندا efi رايونىنى ئۆز ئىچىگە ئالسا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="616"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2.</source>
        <translation>قاچىلاش پروگراممىسى ئۇسۇلىنى يېتەكلەش 1 ماگنىتلىق دىسكىنى رايونغا ئايرىش جەدۋىلى بىلەن بىردەك ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="619"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="624"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2, cannot have efi partition.</source>
        <translation>قاچىلاش پروگراممىسى ئۇسۇلىنى يېتەكلەش ٪1 ماگنىتلىق دىسكا رايونغا ئايرىش جەدۋىلى بىلەن بىردەك ئەمەس، efi شۆبە رايونىنى ئىشلەتكىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="675"/>
        <source>No created of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="695"/>
        <source> partition
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="712"/>
        <source>Boot partition not in the first partition
</source>
        <translation>شۆبە رايون چوقۇم بىرىنچى شۆبە رايون بولۇشى كېرەك Boot</translation>
    </message>
    <message>
        <source>No boot partition
</source>
        <translation type="obsolete">boot تارماق رايونىنى تاپالمىدىم
</translation>
    </message>
    <message>
        <source>No Efi partition
</source>
        <translation type="obsolete">Efi تارماق رايونىنى تاپالمىدىم
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="721"/>
        <source>Only one EFI partition is allowed
</source>
        <translation>پەقەت بىر EFI تارماق رايونىنىڭ مەۋجۇت بولۇپ تۇرۇشىغا يول قويۇلىدۇ
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="724"/>
        <source>Efi partition number invalid
</source>
        <translation>شۆبە رايون نومۇرىنى ئىشلەتكىلى بولمايدۇ Efi
</translation>
    </message>
    <message>
        <source>No root partition
</source>
        <translation type="obsolete">يىلتىز تارماق رايونىنى تاپالمىدىم.
</translation>
    </message>
    <message>
        <source>No backup partition
</source>
        <translation type="obsolete">زاپاس نۇسخا تاپالمىغان رايوننى ئەسلىگە كەلتۈرۈش .
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="743"/>
        <source>This machine not support EFI partition
</source>
        <translation>نۆۋەتتە بۇ ماشىنا EFI رايونغا بۆلۈشنى قوللىمايدۇ !</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="791"/>
        <source>If the /swap partition is not created,computers cannot be woken up after the system goes to sleep. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="799"/>
        <source>If the /data partition is not created,user data cannot be backed up. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="842"/>
        <source>Full install</source>
        <translation>پۈتۈن تەخسىلىك قۇراشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="706"/>
        <source>Partition too small
</source>
        <translation>شۆبە رايون بەك كىچىك
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="731"/>
        <source>BackUp partition too small
</source>
        <translation>زاپاس نۇسخىسىنى ئەسلىگە كەلتۈرۈش رايونى بەك كىچىك
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="734"/>
        <source>Repeated mountpoint
</source>
        <translation>تەكرار يۈك بېسىش رايونى مەۋجۇت
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="746"/>
        <source>The filesystem type FAT16 or FAT32 is not fully-function filesystem,
except EFI partition, other partition not proposed</source>
        <translation>FAT16 ۋە FAT32 مۇكەممەل بولمىغان ھۆججەت سىستېمىسى بولۇپ،
EFI شۆبە رايونىدىن باشقا، باشقا تارماق رايونلاردا ئىشلىتىشكە تەكلىپ بەرمەيدۇ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="781"/>
        <source>If the </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="805"/>
        <source> partitions are not created,do you want to go to the next step?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="841"/>
        <source>Choose Installation Method</source>
        <translation>ئورنىتىش ئۇسۇلىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="588"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="601"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="845"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="901"/>
        <source>Next</source>
        <translation>كېيىنكى قەدەمدە</translation>
    </message>
</context>
<context>
    <name>KInstaller::MiddleFrameManager</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/middleframemanager.cpp" line="44"/>
        <source>next</source>
        <translatorcomment>下一步</translatorcomment>
        <translation>كېيىنكى قەدەمدە</translation>
    </message>
</context>
<context>
    <name>KInstaller::ModifyPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="46"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="206"/>
        <source>unused</source>
        <translation>ئىشلەتمەسلىك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="192"/>
        <source>Format partition.</source>
        <translation>فورماتلاشتۇرۇش بۇ رايونغا ئايرىش .</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="193"/>
        <source>OK</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="195"/>
        <source>Used to:</source>
        <translation>ئىشلىتىلسە</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="196"/>
        <source>Modify Partition</source>
        <translation>رايونغا تۈزىتىش كىرگۈزۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="429"/>
        <source>Mount point repeat:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="325"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>boot شۆبە رايونىنىڭ شەكلى پەقەت ext4 ياكى ext3</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="335"/>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="363"/>
        <source>The disk can only create one boot partition!</source>
        <translation>boot شۆبە رايونىدا پەقەت بىرلا قۇرۇلىدۇ، كېيىن قايتا قۇرۇلغان boot شۆبە رايونى ئىناۋەتسىز</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="391"/>
        <source>The disk can only create one root partition!</source>
        <translation>يىلتىز تارماق رايونى پەقەت بىرلا بەرپا قىلغىلى بولىدۇ، كېيىن قايتا بەرپا قىلىنغان يىلتىز تارماق رايونى ئىناۋەتسىز!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="407"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>يۈك ئېسىش نۇقتىسىنى «/باشلىنىش كېرەك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="532"/>
        <source>close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="201"/>
        <source>Mount point</source>
        <translation>يۈك ئېسىش نۇقتىسى</translation>
    </message>
</context>
<context>
    <name>KInstaller::PrepareInstallFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/prepareinstallframe.cpp" line="109"/>
        <source>Check it and click [Start Installation]</source>
        <translatorcomment>勾选后点击[开始安装]</translatorcomment>
        <translation>تاللىغاندىن كېيىن چېكىش [ ئورنىتىشنى باشلاش ]</translation>
    </message>
</context>
<context>
    <name>KInstaller::SlideShow</name>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="295"/>
        <source>Multi platform support, suitable for mainstream CPU platforms at home and abroad.</source>
        <translation>كۆپ سۇپا ئارقىلىق قوللاش، دۆلەت ئىچى ۋە سىرتىدىكى ئاساسلىق CPU سۇپىسىغا ماسلاشتۇرۇش كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="296"/>
        <source>Ukui desktop environment meets personalized needs.</source>
        <translation>UKUI ئۈستەل مۇھىتى خاس ئېھتىياجنى قاندۇرىدۇ UKUI</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="297"/>
        <source>The system is updated and upgraded more conveniently.</source>
        <translation>سىستېمىسى يېڭىلاندى، دەرىجىسىنى ئۆستۈرۈش تېخىمۇ قولايلىق.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="298"/>
        <source>Security center, all-round protection of data security.</source>
        <translation>بىخەتەرلىك مەركىزى سانلىق مەلۇماتلارنىڭ بىخەتەرلىكىنى ئومۇميۈزلۈك قوغداش كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="299"/>
        <source>Software store, featured software recommendation.</source>
        <translation>يۇمشاق دېتال ماگىزىنى ، تاللانغان يۇمشاق دېتال تەۋسىيە قىلىنىدۇ .</translation>
    </message>
    <message>
        <source>Kirin software store, featured software recommendation.</source>
        <translation type="obsolete">بولىن يۇمشاق دېتال دۇكىنى، تاللانغان يۇمشاق دېتال تەۋسىيە قىلىنىدۇ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="300"/>
        <source>Provide technical support and professional after-sales service.</source>
        <translation>تېخنىكا جەھەتتىن قوللاش، كەسپىي سېتىشتىن كېيىنكى مۇلازىمەت بىلەن تەمىنلەيدۇ.</translation>
    </message>
</context>
<context>
    <name>KInstaller::TableWidgetView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="111"/>
        <source>This action will delect all partition,are you sure?</source>
        <translation>ھازىر بار بولغان رايونلارغا ئايرىش جەدۋىلىنى بىكار قىلىپ، بىكار قىلىشنى بېكىتىش كېرەك؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <source>yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="327"/>
        <source>no</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="167"/>
        <source>Create partition table</source>
        <translation>رايونغا ئايرىش جەدۋىلى بەرپا قىلىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="321"/>
        <source>freespace</source>
        <translation>بىكار</translation>
    </message>
</context>
<context>
    <name>KInstaller::TimeZoneFrame</name>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="119"/>
        <source>Select Timezone</source>
        <translation>ۋاقىت رايونىنى تاللاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="232"/>
        <source>Start Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="127"/>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="236"/>
        <source>Next</source>
        <translation>كېيىنكى قەدەمدە</translation>
    </message>
</context>
<context>
    <name>KInstaller::UserFrame</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="139"/>
        <source>Start Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="143"/>
        <source>Start Installation</source>
        <translation>قۇراشتۇرۇشنى باشلاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="133"/>
        <source>Next</source>
        <translation>كېيىنكى قەدەمدە</translation>
    </message>
</context>
<context>
    <name>KInstaller::appcheckframe</name>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="100"/>
        <source>Start Installation</source>
        <translation>قۇراشتۇرۇشنى باشلاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="93"/>
        <source>Choose your app</source>
        <translation>قوللىنىشىڭىزنى تاللاڭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="94"/>
        <source>Select the application you want to install to install with one click when using the system.</source>
        <translation>سىز ئورنىتىشقا تېگىشلىك قوللىنىشنى تاللاڭ ،سىستېمىنى ئىشلەتكەندە بىر كۇنۇپكا بىلەن ئورنىتىڭ .</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="97"/>
        <source>Start Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::conferquestion</name>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="105"/>
        <source>confer_mainTitle</source>
        <translation>بىخەتەرلىك مەسىلىسى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="106"/>
        <source>You can reset your password by answering a security question when you forget your password. Make sure you remember the answer.</source>
        <translation>سىز مەخپىي نومۇرنى ئۇنتۇپ قالغاندا بىخەتەرلىك سوئالىغا جاۋاب بېرىش ئارقىلىق مەخپىي نومۇرنى قايتا قويسىڭىز بولىدۇ، جاۋابىنى چوقۇم ئەستە تۇتۇشقا كاپالەتلىك قىلىڭ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="109"/>
        <source>Which city were you born in?</source>
        <translation>سەن تۇغۇلغان شەھەرنىڭ ئىسمىمۇ؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="110"/>
        <source>What is your childhood nickname?</source>
        <translation>سېنىڭ بالىلىق چاغلىرىڭدىكى نامىڭ؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="111"/>
        <source>Which middle school did you graduate from?</source>
        <translation>سەن قايسى ئوتتۇرا مەكتەپنى پۈتتۈرگەن؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="112"/>
        <source>What is your father&apos;s name?</source>
        <translation>دادىڭىزنىڭ ئىسمى؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="113"/>
        <source>What is your mother&apos;s name?</source>
        <translation>ئاناڭنىڭ ئىسمى ؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="114"/>
        <source>When is your spouse&apos;s birthday?</source>
        <translation>جورىڭىزنىڭ تۇغۇلغان كۈنى؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="115"/>
        <source>What&apos;s your favorite animal?</source>
        <translation>سىز ئەڭ ياخشى كۆرىدىغان ھايۋان؟</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="119"/>
        <source>question%1:</source>
        <translation>مەسىلە 1:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="120"/>
        <source>answer%1:</source>
        <translation>جاۋابى ٪ 1</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="126"/>
        <source>set later</source>
        <translation>بىرئازدىن كېيىن تەسىس قىلماق</translation>
    </message>
</context>
<context>
    <name>KInstaller::modeselect</name>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="62"/>
        <source>Register account</source>
        <translation>ھىسابات قۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="68"/>
        <source>Register immediately</source>
        <translation>دەرھال بەرپا قىلىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="76"/>
        <source>Register later</source>
        <translation>بىرئازدىن كېيىن بەرپا قىلماق</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="77"/>
        <source>After the system installation is completed, an account will be created when entering the system.</source>
        <translation>سىستېما ئورنىتىلىپ بولغاندىن كېيىن، سىستېمىغا كىرگەندە ھېسابات بەرپا قىلىنىدۇ.</translation>
    </message>
</context>
<context>
    <name>KInstaller::securityquestions</name>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="404"/>
        <source>Security Questions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="405"/>
        <source>You can reset your password by answering security questions when you forget it, please make sure to remember the answer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="406"/>
        <source>Question 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="407"/>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="409"/>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="411"/>
        <source>Answer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="408"/>
        <source>Question 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="410"/>
        <source>Question 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/securityquestions.cpp" line="412"/>
        <source>Set up later</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KInstaller::userregisterwidiget</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="442"/>
        <source>Two password entries are inconsistent!</source>
        <translation>ئىككى قېتىملىق مەخپىي نومۇرنى كىرگۈزۈش بىردەك ئەمەس!</translation>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="obsolete">باش كومپيۇتېرنىڭ ئىسمى پەقەت ھەرپ، رەقەم، ئاستى سىزىق سىزىش ۋە ئۇلاش بەلگىسىلا يول قويۇلىدۇ، ئۇنىڭ ئۈستىگە ئۇزۇنلۇقى 64 خانىدىن ئاشمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="153"/>
        <source>Password strength:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="494"/>
        <source>Hostname should be more than 0 bits in length.</source>
        <translation>باش كومپيۇتېر نامىنىڭ ئۇزۇنلۇقى بىر ھەرپتىن كەم بولسا بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="496"/>
        <source>Hostname should be no more than 64 bits in length.</source>
        <translation>باش كومپيۇتېر نامىنىڭ ئۇزۇنلۇقى 64 خەتتىن ئېشىپ كەتمەسلىكى كېرەك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="498"/>
        <source>Hostname only letters,numbers,hyphen and dot notation are allowed</source>
        <translation>باش كومپيۇتېرنىڭ ئىسمى پەقەت ھەرپ، سان، ئۇلاش بەلگىسى ياكى نۇقتىلىق بەلگە بولۇشقا يول قويۇلىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="500"/>
        <source>Hostname must start with a number or a letter</source>
        <translation>باش كومپيۇتېر نامىنىڭ باش قىسمى چوقۇم ھەرپ ياكى سان بولۇشى كېرەك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="502"/>
        <source>Hostname must end with a number or a letter</source>
        <translation>باش كومپيۇتېرنىڭ ئىسمىنىڭ قۇيرۇق قىسمى چوقۇم ھەرپ ياكى سان بولۇشى كېرەك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="504"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>باش كومپيۇتېرنىڭ ئىسمىغا تۇتاشتۇرۇلغان خەت ۋە نۇقتا بەلگىلىرىنى تۇتاشتۇرغىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="506"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>باش كومپيۇتېر نامىدا ئۈزلۈكسىز نۇقتا بەلگىسى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="669"/>
        <source>Create User</source>
        <translation>خېرىدار قۇرماق</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="670"/>
        <source>username</source>
        <translation>ئابونت نامى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="671"/>
        <source>hostname</source>
        <translation>باش كومپيۇتېر نامى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="672"/>
        <source>new password</source>
        <translation>مەخپىي نومۇر</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="673"/>
        <source>enter the password again</source>
        <translation>مەخپىي نومۇرنى يەنە بىر قېتىم كىرگۈزۈ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="674"/>
        <source>Automatic login on boot</source>
        <translation>ئېچىلغاندا ئاپتوماتىك كىرىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="679"/>
        <source>After logging into the system, it is recommended that you set biometric passwords and password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="681"/>
        <source>After logging into the system, it is recommended that you set password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="684"/>
        <source>After logging into the system, it is recommended that you set the biometric password in [Settings - Login Options] to have a better experience in the system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Biometric [authentication] device detected / unified login support</source>
        <translation type="obsolete">بىئولوگىيەلىك ئالاھىدىلىكلەرنى تەكشۈرۈپ چىقىلغان « ئىسپاتلاش » ئۈسكۈنىلىرى/ بىر تۇتاش كىرىپ قوللىدى ،
بىئولوگىيەلىك ئالاھىدىلىك باشقۇرۇش قورالى/تەسىس قىلىش- تىزىملىتىش تاللاش تۈرىگە تەسىس قىلىش كېرەك.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="725"/>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="738"/>
        <source>Medium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="749"/>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KServer::EncryptSetFrame</name>
    <message>
        <source>password:</source>
        <translation type="obsolete">مەخپىي نومۇر</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">مەخپىي نومۇرنى جەزملەشتۈر</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">مەخپىي نومۇرىڭىزنى ياخشى ساقلاڭ. ئەگەر مەخپىي نومۇرنى ئۇنتۇپ قالسىڭىز، ماگنىتلىق دىسكا سانلىق مەلۇماتىنى زىيارەت قىلغىلى بولماي</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="40"/>
        <source>password</source>
        <translation>مەخپىي نومۇر</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="161"/>
        <source>The password contains less than two types of characters</source>
        <translation>مەخپىي نومۇر ئۆز ئىچىگە ئالغان ھەرپ تىپى 2 خىلدىن ئاز بولىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="41"/>
        <source>confirm password</source>
        <translation>مەخپىي نومۇرنى جەزىملەشتۈر</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="242"/>
        <source>The two passwords are different</source>
        <translation>ئىككى قېتىملىق مەخپىي نومۇر بىردەك ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="268"/>
        <source>Please keep your password properly.If you forget it,you will not be able to access the disk data.</source>
        <translation>مەخپىي نومۇرىڭىزنى ياخشى ساقلاڭ، مەخپىي نومۇرنى ئۇنتۇپ قالسىڭىز، ماگنىتلىق دىسكىنى زىيارەت قىلغىلى بولمايدۇ.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="359"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="360"/>
        <source>OK</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="430"/>
        <source>close</source>
        <translation>تاقاش</translation>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">ئىككى قېتىملىق مەخپىي نومۇرنى كىرگۈزۈش بىردەك ئەمەس!</translation>
    </message>
</context>
<context>
    <name>KServer::MessageBox</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="199"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="201"/>
        <source>OK</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="295"/>
        <source>close</source>
        <translation>تاقاش</translation>
    </message>
</context>
<context>
    <name>KServer::SetPartitionsSize</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="219"/>
        <source>Data Partitions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="348"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="349"/>
        <source>OK</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="353"/>
        <source>System Partitions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="355"/>
        <source>Data Partitions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="359"/>
        <source>Size range from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="359"/>
        <source> to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/setPartitionsSize.cpp" line="487"/>
        <source>close</source>
        <translation>تاقاش</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../src/plugins/VirtualKeyboard/src/keyboardwidget.ui" line="29"/>
        <source>KeyboardWidget</source>
        <translation>KeyboardWidget</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="9"/>
        <source>wd</source>
        <translation>غەربىي رايون سانلىق مەلۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="11"/>
        <source>seagate</source>
        <translation>شىجيې</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="13"/>
        <source>hitachi</source>
        <translation>ھىتاجى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="15"/>
        <source>samsung</source>
        <translation>سامسۇڭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="17"/>
        <source>toshiba</source>
        <translation>توشىبا</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="19"/>
        <source>fujitsu</source>
        <translation>فۇشىتۇڭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="21"/>
        <source>maxtor</source>
        <translation>مايسو</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="23"/>
        <source>IBM</source>
        <translation>IBM</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="25"/>
        <source>excelStor</source>
        <translation>يىتو</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="27"/>
        <source>lenovo</source>
        <translation>لىيەنشياڭ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="29"/>
        <source>other</source>
        <translation>باشقىسى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="31"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="303"/>
        <source>Unknown</source>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="80"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="314"/>
        <source>Freespace</source>
        <translation>بىكار</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="294"/>
        <source>kylin data partition</source>
        <translation>سانلىق مەلۇمات دىسكىسى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="301"/>
        <source>Swap partition</source>
        <translation>رايوننى ئالماشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/main.cpp" line="158"/>
        <source>Show debug informations</source>
        <translation>تەڭشەش ئۇچۇرىنى نامايان قىلىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="191"/>
        <source>Extended partition %1 has 
</source>
        <translation>شۆبە رايوننى كېڭەيتىش ٪1</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="193"/>
        <source>Create new partition %1,%2
</source>
        <translation>يېڭى شۆبە رايونى بەرپا قىلىش ٪1، ٪2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="196"/>
        <source>Create new partition %1,%2,%3
</source>
        <translation>يېڭى شۆبە رايونى قۇرۇش ٪1، ٪2، ٪3
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="201"/>
        <source>Delete partition %1
</source>
        <translation>شۆبە رايوننىڭ ٪1 نى چىقىرىپ تاشلاش
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="205"/>
        <source>Format %1 partition, %2
</source>
        <translation>فورماتلاشقان شۆبە رايونى ٪1، ٪2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="208"/>
        <source>Format partition %1,%2,%3
</source>
        <translation>فورماتلاشقان شۆبە رايونى ٪1، ٪2، ٪3
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="213"/>
        <source>%1 partition mountPoint %2
</source>
        <translation>1 شۆبە رايوندا يۈك كۆتۈرۈش نۇقتىسى 2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="217"/>
        <source>New Partition Table %1
</source>
        <translation>يىڭى رايونغا ئايرىش جەدۋىلى ٪1
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="220"/>
        <source>Reset size %1 partition
</source>
        <translation>قايتا تەسىس قىلىنغان رايونلاردا بىر چوڭ كىچىكلىك بار</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="30"/>
        <source>KCommand::m_cmdInstance is not init.</source>
        <translation>بۇيرۇق m_cmdInstance دەسلەپكى قەدەمدە بولمىدى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="67"/>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="99"/>
        <source>WorkingPath is not found. 
</source>
        <translation>خىزمەت مۇندەرىجىسى تاپالمىدى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="91"/>
        <source>Shell file is empty, does not continue. 
</source>
        <translation>ھۆججەت قۇرۇق، ۋەزىپە توختايدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>device</source>
        <translation>ئۈسكۈنىلەر</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>type</source>
        <translation>تىپى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>mounted</source>
        <translation>يۈك ئېسىش نۇقتىسى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>used</source>
        <translation>ئىشلىتىلدى</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>system</source>
        <translation>سىستىما</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>format</source>
        <translation>فورماتلاشتۇرۇش</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="69"/>
        <source>Memory allocation error when setting</source>
        <translation>تەسىس قىلغاندا ئىچكى ساقلىغۇچنى تەقسىملەش خاتالىقى يۈز بېرىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="73"/>
        <source>Memory allocation error</source>
        <translation>ئىچكى ساقلىغۇچنى تەقسىملەش خاتالىقى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="75"/>
        <source>The password is the same as the old one</source>
        <translation>بۇ مەخپىي نومۇر بىلەن ئەسلىدىكىسى ئوخشاش</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="77"/>
        <source>The password is a palindrome</source>
        <translation>مەخپىي نومۇر بىر جاۋاب قايتۇرۇش ھۆججىتى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="79"/>
        <source>The password differs with case changes only</source>
        <translation>مەخپىي نومۇر پەقەت چوڭ- كىچىك يېزىش ئۆزگىرىشىنىلا ئۆز ئىچى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="81"/>
        <source>The password is too similar to the old one</source>
        <translation>مەخپىي نومۇر بىلەن ئەسلىدىكى بەك ئوخشىشىپ كېتىدىكەن</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="83"/>
        <source>The password contains the user name in some form</source>
        <translation>مەخپىي نومۇر مەلۇم شەكىلدىكى ئابونت نامىنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="85"/>
        <source>The password contains words from the real name of the user in some form</source>
        <translation>مەخپىي نومۇر مەلۇم شەكىلدىكى خېرىدارنىڭ ھەقىقىي نامىنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="87"/>
        <source>The password contains forbidden words in some form</source>
        <translation>مەخپىي نومۇر مەلۇم شەكىلدىكى مەنئى قىلىنغان سۆزلۈكنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="92"/>
        <source>The password contains too few digits</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان سانلىق خەتلەر بەك ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="97"/>
        <source>The password contains too few uppercase letters</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان چوڭ ھەرپ بەك ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="102"/>
        <source>The password contains too few lowercase letters</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان كىچىك ھەرپ بەك ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="107"/>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان ئالاھىدە ھەرپ سانى بەك ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="112"/>
        <source>The password is too short</source>
        <translation>مەخپىي نومۇر بەك قىسقا</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="114"/>
        <source>The password is just rotated old one</source>
        <translation>مەخپىي نومۇر پەقەت كونا مەخپىي نومۇرنىڭ تەتۈر ئايلىنىشى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="119"/>
        <source>The password does not contain enough character classes</source>
        <translation>مەخپىي نومۇر يېتەرلىك ھەرپ تىپىنى ئۆز ئىچىگە ئالمىغان</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="124"/>
        <source>The password contains too many same characters consecutively</source>
        <translation>مەخپىي نومۇر بەك كۆپ ئوخشاش ئۇدا ھەرىكەتلەرنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="129"/>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation>مەخپىي نومۇر زىيادە كۆپ بولغان ئوخشاش تىپتىكى ئۈزلۈكسىز ھەرىكەتلەرنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="90"/>
        <source>The password contains less than %1 digits</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان سانلىق خەت بەلگىلىرى ٪1تىن ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="95"/>
        <source>The password contains less than %1 uppercase letters</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان چوڭ ھەرپ بىر خانىدىن ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="100"/>
        <source>The password contains less than %1 lowercase letters</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان كىچىك يېزىلغان ھەرپلەر بىر خانىلىقتىن ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="105"/>
        <source>The password contains less than %1 non-alphanumeric characters</source>
        <translation>مەخپىي نومۇرنى ئۆز ئىچىگە ئالغان ئالاھىدە ھەرپلەر ٪1 تىن ئاز بولىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="110"/>
        <source>The password is shorter than %1 characters</source>
        <translation>مەخپىي نومۇر ٪1 تىن ئاز</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="117"/>
        <source>The password contains less than %1 character classes</source>
        <translation>مەخپىي نومۇر ئۆز ئىچىگە ئالغان ھەرپ تىپى ٪1 تىن ئاز بولىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="122"/>
        <source>The password contains more than %1 same characters consecutively</source>
        <translation>مەخپىي نومۇر ٪1 تىن ئارتۇق ئوخشاش ئۇدا ھەرپلەرنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="127"/>
        <source>The password contains more than %1 characters of the same class consecutively</source>
        <translation>مەخپىي نومۇر ٪1 تىن ئاشقان ئوخشاش تۈردىكى ئۈزلۈكسىز ھەرىكەتنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="132"/>
        <source>The password contains monotonic sequence longer than %1 characters</source>
        <translation>مەخپىي نومۇر ٪1 تىن ئاشىدىغان مونوتون تەرتىپىنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="134"/>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation>مەخپىي نومۇر زىيادە ئۇزۇن مونوتون ھەرپ تەرتىپىنى ئۆز ئىچىگە ئالىدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="136"/>
        <source>No password supplied</source>
        <translation>مەخپىي نومۇر بىلەن تەمىنلەنمىگەن</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="138"/>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation>RNG دىن ئىختىيارىي ساناق ھاسىل قىلغىلى بولمايدىغان ئۈسكۈنىلەر ئىختىيارىي سانغا ئېرىشكىلى بولمايدۇ</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="140"/>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation>مەخپىي نومۇر مەغلۇپ بولۇش ھاسىل قىلىندى - تەسىس قىلىشقا ئېھتىياجلىق ئېزىشقا يەتمىگەن</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="143"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="145"/>
        <source>The password fails the dictionary check</source>
        <translation>مەخپىي نومۇر لۇغەتتىن ئۆتمىگەندە تەكشۈرۈلمىگەن - مەخپىي نومۇر لۇغەتتىكى سۆزلۈككە ئاساسەن</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="149"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="151"/>
        <source>Unknown setting</source>
        <translation>نامەلۇم تەسىس قىلىش</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="155"/>
        <source>Bad integer value of setting</source>
        <translation>خاتا پۈتۈن سان تەڭشەش قىممىتى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="157"/>
        <source>Bad integer value</source>
        <translation>خاتا پۈتۈن سان تەڭشەش قىممىتى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="161"/>
        <source>Setting %s is not of integer type</source>
        <translation>تەڭشەش ٪s پۈتۈن تىپ ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="163"/>
        <source>Setting is not of integer type</source>
        <translation>تەسىس قىلىش ھەرگىزمۇ پۈتۈن سان تىپى ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="167"/>
        <source>Setting %s is not of string type</source>
        <translation>تەسىس قىلىش ٪s ھەرپ-بەلگىلىك تىزىق تىپى ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="169"/>
        <source>Setting is not of string type</source>
        <translation>تەسىس قىلىش ھەرپ-بەلگىلىك تىزىق تىپى ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="171"/>
        <source>Opening the configuration file failed</source>
        <translation>ئېچىپ سەپلەش ھۆججىتى مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="173"/>
        <source>The configuration file is malformed</source>
        <translation>سەپلەش ھۆججىتىنىڭ شەكلى توغرا ئەمەس</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="175"/>
        <source>Fatal failure</source>
        <translation>ئەجەللىك خاتالىق</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="177"/>
        <source>Unknown error</source>
        <translation>نامەلۇم خاتالىق</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="467"/>
        <source>unused</source>
        <translation>ئىشلەتمەسلىك</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="469"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="59"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="208"/>
        <source>kylin-data</source>
        <translation>ئابونت سانلىق مەلۇماتىنى رايونغا ئايرىش</translation>
    </message>
</context>
</TS>
