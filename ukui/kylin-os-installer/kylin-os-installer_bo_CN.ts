<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="bo_CN">
<context>
    <name>DiskInfoView</name>
    <message>
        <source>as data disk</source>
        <translation>གཞི་གྲངས་སྡེར་ལ་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <source>Encrypted Partition</source>
        <translation>ཏང་ཁག།གསང་སྔགས།འཁོར་གཞོང།</translation>
    </message>
    <message>
        <source>System Partitions Size:</source>
        <translation>ཆ་ལག་ཚང་བའི་སྒོ་ནས་དབྱེ་</translation>
    </message>
    <message>
        <source>Data Partition Size:</source>
        <translation>གཞི་གྲངས་དབྱེ་འབྱེད་ས་ཁུལ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::CreatePartitionFrame</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Size(MiB)</source>
        <translation>ཆེ་ཆུང་།(MiB)</translation>
    </message>
    <message>
        <source>End of this space</source>
        <translation>བར་སྟོང་གི་མཇུག་མ་ལྷག་མ།</translation>
    </message>
    <message>
        <source>Create Partition</source>
        <translation>བགོ་ཁུལ་གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>Root partition size is greater than 15GiB, 
but Huawei machines require greater than 25GiB.</source>
        <translation>རྩ་བའི་ཡན་ལག་ཁུལ་གྱི་ཆེ་ཆུང་15GiBཡིན་པ་དང་། ཧྭ་ཝེ་འཕྲུལ་འཁོར་གྱི་རེ་བ་25GiBལས་ཆེ་བ།</translation>
    </message>
    <message>
        <source>Location for the new partition</source>
        <translation>ཁུལ་བགོས་གསར་བའི་སྡོད་གནས།</translation>
    </message>
    <message>
        <source>Logical</source>
        <translation>གཏན་ཚིགས།</translation>
    </message>
    <message>
        <source>Beginning of this space</source>
        <translation>བར་སྟོང་གི་མགོ་བོ་ལྷག་མ།</translation>
    </message>
    <message>
        <source>Mount point</source>
        <translation>འགེལ་ཚེག：</translation>
    </message>
    <message>
        <source>Primary</source>
        <translation>བདག་པོ།</translation>
    </message>
    <message>
        <source>Type for the new partition:</source>
        <translation>ཁུལ་བགོས་གསར་བའི་རིགས།:</translation>
    </message>
    <message>
        <source>Recommended efi partition size is between 256MiB and 2GiB.</source>
        <translation>EFIཁུལ་བགོས་ཀྱི་ཆེ་ཆུང་256MiBནས་2GiBབར་ཡིན།</translation>
    </message>
    <message>
        <source>Used to:</source>
        <translation>བཀོལ་སྤྱོད།:</translation>
    </message>
    <message>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>གཞི་འཛིན་ས་གཙོ་བོ་ &apos;/&apos;ཞེས་པ་དེ་འགོ་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <source>The disk can only create one root partition!</source>
        <translation>ཁབ་ལེན་ཁབ་ལེན་གྱིས་རྩ་བའི་ཆ་ཤས་གཅིག་མ་གཏོགས་བཟོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>The disk can only create one boot partition!</source>
        <translation>ཁབ་ལེན་ཁབ་ལེན་གྱིས་ལྷམ་གྱི་ཆ་ཤས་གཅིག་མ་གཏོགས་བཟོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Recommended boot partition size is between 1GiB and 2GiB.
Note that the size must be greater than 500MiB.</source>
        <translation type="obsolete">འོས་སྦྱོར་བྱས་པའི་ལྷམ་གྱི་བར་མཚམས་ཀྱི་ཆེ་ཆུང་ནི་1GiBནས་2GiBབར་ཡིན།</translation>
    </message>
    <message>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>Bootའཛུགས་སྐྲུན་ཁག་ཚད་ལྡན་ཁོ་ན་ལས་ext4འམ་ext3།</translation>
    </message>
    <message>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation>Bootཡན་ལག་དམག་ཁུལ་མི་ཆུང།2GiB </translation>
    </message>
    <message>
        <source>Mount point repeat:</source>
        <translation>བཀལ།གླང་རྫི།གནས།བསྐྱར་ཟློས།：</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitionFrame</name>
    <message>
        <source>Revert</source>
        <translation>སླར་གསོ།</translation>
    </message>
    <message>
        <source>Device for boot loader path:</source>
        <translation>བྱ་རིམ་འགྲོ་ལམ་ཁུར་སྣོན་སྣེ་ཁྲིད།:</translation>
    </message>
    <message>
        <source>remove this partition?</source>
        <translation>ཁུལ་འདི་བསུབ་རྒྱུ་གཏན་འཁེལ་བྱས།</translation>
    </message>
    <message>
        <source>This is a system partition,remove this partition?</source>
        <translation>ཁུལ་འདི་ནི་མ་ལག་ཁུལ་ཡིན་པ་རེད།་ཁུལ་འདི་གསུབ་དགོས་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitiondelegate</name>
    <message>
        <source>%1 MsDos new partition table will be created.
</source>
        <translation>%1 MsDos ཁུལ་བགོས་རེའུ་མིག་གསར་བཟོ་བྱ་རྒྱུ་རེད།
</translation>
    </message>
    <message>
        <source>%1 GPT new partition table will be created.
</source>
        <translation>%1 GPT  ཁུལ་བགོས་རེའུ་མིག་གསར་བཟོ་བྱ་རྒྱུ་རེད།
</translation>
    </message>
    <message>
        <source>#%1 partition  on the device %2 will be mounted %3.
</source>
        <translation>#%1 ཁུལ་བགོས་ %2 སྟེང་ནས་%3རུ་བཀལ་འཇུག་བྱེད་རྒྱུ།
</translation>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be mounted %3.
</source>
        <translation>#%1 ཁུལ་བགོས་ %2 སྟེང་ནས་%3རུ་བཀལ་འཇུག་བྱེད་རྒྱུ།
</translation>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be deleted.
</source>
        <translation>#%1 ཁུལ་བགོས་སྒྲིག་ཆས་ %2 སྟེང་དུ་མི་འགྱངས་པར་བསུབ་རྒྱུ་རེད།
</translation>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be formated %3.
</source>
        <translation>#%1 ཁུལ་བགོས་ %2 སྟེང་དུ་ %3རུ་རྣམ་པར་འཇོག་ངེས་ཡིན།
</translation>
    </message>
    <message>
        <source>#%1 partition on the device %2 will be created.
</source>
        <translation>#%1 ཁུལ་བགོས་སྒྲིག་ཆས་%2 སྟེང་དུ་གསར་བཟོ་བྱེད་རྒྱུ་རེད།
</translation>
    </message>
</context>
<context>
    <name>KInstaller::FinishedFrame</name>
    <message>
        <source>Installation Finished</source>
        <translation>སྒྲིག་འཇུག་བྱས་ཚར།</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
</context>
<context>
    <name>KInstaller::FullPartitionFrame</name>
    <message>
        <source>Please choose custom way to install, disk size less than 50G!</source>
        <translation>མཚན་ཉིད་རང་འཇོག་རྣམ་པའི་ངང་ནས་སྒྲིག་འཇུག་འདེམས་རོགས་གནང་།སྡུད་སྡེར་གྱི་ཆེ་ཆུང་50Gལས་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Full disk encryption</source>
        <translation>སྡེར་ཡོངས་གསང་སྣོན།</translation>
    </message>
    <message>
        <source>Factory backup</source>
        <translation>བཟོས་གྲུབ་གྲབས་ཉར།</translation>
    </message>
    <message>
        <source>This action will  use the original old account as the default account. Please make sure you remember the password of the original account!</source>
        <translation>འདེམས་བསྐོ་བྱས་པའི་རྣམ་གྲངས་དེ་སྔར་གྱི་མ་ལག་གི་སྤྱོད་མཁན་དང་དེའི་དཀར་ཆག་འོག་གི་གྲངས་གཞི་སོར་འཇོག་བྱེད་པ་དང་ཆབས་ཅིག་ཏུ་dataaཁུལ་ཁྲོད་ཀྱི་གཞི་གྲངས་སོར་འཇོག་བྱེད་སྲིད། པར་གཞི་ཆ་འགྲིག་གི་གནས་ཚུལ་འོག་མ་ལག་གིས་ཁྱེད་ཀྱི་མ་ལག་གིས་ཆ་འཕྲིན་དང་གསོན་ཤུགས་འཕེལ་བའི་རྣམ་པ་ཉར་འཛིན་བྱེད་ངེས་ཡིན།
རྣམ་གྲངས་དེ་བདམས་པ་དེས་ཁྱེད་ལ་གསར་དུ་བསྐྲུན་པའི་སྤྱོད་མཁན་ལ་གསལ་བརྡ་གཏོང་མི་སྲིད་ལ། ཁྱོད་ཀྱིས་རྩིས་ཐོ་འདི་དག་གི་གསང་བ་འགོད་པར་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>save history date</source>
        <translation>ལོ་རྒྱུས་ཀྱི་དུས་ཚོད་ཉར་</translation>
    </message>
    <message>
        <source>By default, SSDS serve as system disks and HDDS serve as user data disks</source>
        <translation>བློ་མོས་སྒྲིག་སྦྱོར་དང་། SSDགི་ཁབ་ལེན་སྡེར་མ་དེ་མ་ལག་གི་སྡེར་མར་བརྩིས་པ་དང་། HDDགྱི་ཁབ་ལེན་སྡེར་མ་དེ་སྤྱོད་མཁན་གྱི་གྲངས་གཞིའི་སྡེར་མར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>lvm</source>
        <translation>གཏན་ཚིགས་རིག་པ།(lvm)</translation>
    </message>
    <message>
        <source>The current installation mode enforces factory backup configuration and cannot be unchecked</source>
        <translation>མིག་སྔ་འི་སྒྲིག་སྦྱོར་དཔེ་དབྱིབས་ལ་བཙན་ཤེད་ཀྱིས་བཀོད་སྒྲིག་ནས་ཐོན་པའི་གྲ་སྒྲིག།སྐལ།མེད་པ་བཟོས་པ་འདེམས།</translation>
    </message>
</context>
<context>
    <name>KInstaller::ImgFrame</name>
    <message>
        <source>Select install way</source>
        <translation>སྒྲིག་འཇུག་གི་ཐབས་ལམ་འདེམ་དགོས།</translation>
    </message>
    <message>
        <source>install from ghost</source>
        <translation>Ghostནས་སྒྲིག་འཇུག</translation>
    </message>
    <message>
        <source>install from live</source>
        <translation>Liveནས་སྒྲིག་འཇུག</translation>
    </message>
    <message>
        <source>open file</source>
        <translation>ཡིག་ཆ་ཁ་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source> open file </source>
        <translation type="obsolete">ཡིག་ཆ་ཁ་འབྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallerMainWidget</name>
    <message>
        <source>back</source>
        <translation>གོམ་པ་སྔོན་མ།</translation>
    </message>
    <message>
        <source>quit</source>
        <translation>ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>About to exit the installer, restart the computer.</source>
        <translation>བྱ་རིམ་སྒྲིག་འཇུག་ནས་ཕྱིར་འབུད་བྱེད་རྒྱུ་ཡིན་པས་རྩིས་འཁོར་བསྐྱར་སློང་བྱེད་ངེས།</translation>
    </message>
    <message>
        <source>Installer is about to exit and the computer will be shut down.</source>
        <translation>བྱ་རིམ་སྒྲིག་འཇུག་ནས་ཕྱིར་འབུད་བྱེད་རྒྱུ་ཡིན་པས་རྩིས་འཁོར་ཁ་རྒྱག་ངེས་ཡིན།</translation>
    </message>
    <message>
        <source>About to exit the installer and return to the trial interface.</source>
        <translation>བྱ་རིམ་སྒྲིག་འཇུག་ནས་ཕྱིར་འབུད་བྱེད་རྒྱུ་ཡིན་པས་ཚོད་ལྟའི་མཚམས་ངོས་སུ་ཕེབས་རོགས།</translation>
    </message>
    <message>
        <source>keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingFrame</name>
    <message>
        <source>The system is being installed, please do not turn off the computer</source>
        <translation>མ་ལག་སྒྲིག་འཇུག་བྱེད་བཞིན་ཡོད་པས་རྩིས་འཁོར་ཁ་མ་རྒྱག་རོགས་གནང་།</translation>
    </message>
    <message>
        <source>Log</source>
        <translation>སྒྲིག་འཇུག་ཉིན་ཐོ།</translation>
    </message>
    <message>
        <source>Sorry, KylinOS cannot continue the installation. Please feed back the error log below so that we can better solve the problem for you.</source>
        <translation>ཐུགས་རྒྱལ་མ་བཞེངས། kylinOSམུ་མཐུད་དུ་སྒྲིག་འཇུག་བྱེད་ཐབས་མེད། གཤམ་གྱི་ནོར་འཁྲུལ་ཉིན་ཐོ་ལྡོག་སྐྱེལ་གནང་རོགས། ང་ཚོས་སྔར་ལས་ལེགས་པའི་སྒོ་ནས་ཁྱེད་ཀྱི་གནད་དོན་ཐག་གཅོད་བྱེད་ཆེད།</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>Install failed</source>
        <translation>суурилуулалт амжилтгүй болсон</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingOEMConfigFrame</name>
    <message>
        <source>Progressing system configuration</source>
        <translation>མ་ལག་སྡེབ་སྒྲིག་བྱེད་བཞིན་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <source>OEM Error</source>
        <translation>བཀོད་སྒྲིག་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>ད་ལྟ་ཡང་བསྐྱར་སྒོ་ཕྱེ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>KInstaller::LanguageFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Select Language</source>
        <translation>སྒྲིག་འགོད་སྐད་བརྡ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::LicenseFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Read License Agreement</source>
        <translation>ཆོག་མཆན་གྲོས་ཆིངས་ཀློག་པ།</translation>
    </message>
    <message>
        <source>I have read and agree to the terms of the agreement</source>
        <translation>ངས་གྲོས་ཆིངས་དོན་ཚན་ལ་ལྟ་ཀློག་བྱས་པ་མ་ཟད་མོས་མཐུན་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>KInstaller::MainPartFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Repeated mountpoint
</source>
        <translation>བསྐྱར་དུ་བཀལ་བའི་ཁུལ་ལག་ཡོད་པ།
</translation>
    </message>
    <message>
        <source>Boot partition too small
</source>
        <translation>Bootཁུལ་ལག་ཧ་ཅང་ཆུང་དྲགས་པ།
</translation>
    </message>
    <message>
        <source>Formatted the whole disk</source>
        <translation>སྡུད་སྡེར་ཡོངས་རྣམ་པར་འཇོག་པ།</translation>
    </message>
    <message>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2.</source>
        <translation>སྣེ་ཁྲིད་བྱེད་སྟངས་%1དང་སྡུད་སྡེར་ཁུལ་ལག་གི་རེའུ་མིག་རིགས་ཀྱི་རིགས་%2དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <source>Coexist Install</source>
        <translation>མཉམ་དུ་སྒྲིག་འཇུག</translation>
    </message>
    <message>
        <source>Confirm Full Installation</source>
        <translation>སྡེར་ཡོངས་སྒྲིག་འཇུག་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Custom install</source>
        <translation>སྒྲིག་འཇུག་མཚན་ཉིད་རང་འཇོག</translation>
    </message>
    <message>
        <source>Boot filesystem invalid
</source>
        <translation>Bootཁུལ་ལག་ཡིག་ཆའི་མ་ལག་བཀོལ་སྤྱོད་བྱེད་མི་རུང་།
</translation>
    </message>
    <message>
        <source>Confirm the above operations</source>
        <translation>གོང་གི་བཀོལ་སྤྱོད་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>No Efi partition
</source>
        <translation type="obsolete">Efiཁུལ་ལག་རྙེད་མ་སོང་།
</translation>
    </message>
    <message>
        <source>InvalidId
</source>
        <translation>ནུས་མེད།</translation>
    </message>
    <message>
        <source>No backup partition
</source>
        <translation type="obsolete">གྲབས་ཉར་སོར་ལོག་ཁུལ་ལག་མ་རྙེད་པ།
</translation>
    </message>
    <message>
        <source>Choose Installation Method</source>
        <translation>སྒྲིག་འཇུག་བྱ་ཐབས།</translation>
    </message>
    <message>
        <source>Partition too small
</source>
        <translation>ཁུལ་ལག་ཧ་ཅང་ཆུང་བ།
</translation>
    </message>
    <message>
        <source>Confirm Custom Installation</source>
        <translation>སྒྲིག་འཇུག་མཚན་ཉིད་རང་འཇོག་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>No root partition
</source>
        <translation type="obsolete">རྩ་བའི་ཁུལ་ལག་མ་རྙེད་པ།
</translation>
    </message>
    <message>
        <source>No boot partition
</source>
        <translation type="obsolete">bootཁུལ་ལག་མ་རྙེད་པ།
</translation>
    </message>
    <message>
        <source>Only one EFI partition is allowed
</source>
        <translation>EFIཁུལ་ལག་གཅིག་ཁོ་ན་ཡོད་ཆོག
</translation>
    </message>
    <message>
        <source>Full install</source>
        <translation>སྡེར་ཡོངས་སྒྲིག་འཇུག</translation>
    </message>
    <message>
        <source>Efi partition number invalid
</source>
        <translation>Efiཁུལ་ལག་ཨང་རྟགས་བཀོལ་སྤྱོད་བྱེད་མི་རུང་།
</translation>
    </message>
    <message>
        <source>Root partition size is greater than 15GiB,
but Huawei machines require greater than 25GiB.
</source>
        <translation>རྩ་བའི་ཡན་ལག་ཁུལ་གྱི་ཆེ་ཆུང་15GiBཡིན་པ་དང་། ཧྭ་ཝེ་འཕྲུལ་འཁོར་གྱི་རེ་བ་25GiBལས་ཆེ་བ།
</translation>
    </message>
    <message>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2, cannot have efi partition.</source>
        <translation>སྣེ་ཁྲིད་བྱེད་སྟངས་%1དང་སྡུད་སྡེར་ཁུལ་ལག་རེའུ་མིག་གི་རིགས་%2དང་མི་མཐུན་པ།
EFIཁུལ་ལག་མི་དགོས་པ།</translation>
    </message>
    <message>
        <source>BackUp partition too small
</source>
        <translation>གྲབས་ཉར་སོར་ལོག་ཁུལ་ལག་ཧ་ཅང་ཆུང་དྲགས་པ།
</translation>
    </message>
    <message>
        <source>The filesystem type FAT16 or FAT32 is not fully-function filesystem,
except EFI partition, other partition not proposed</source>
        <translation>FAT16དང་FAT32ནི་ཆ་མི་ཚང་བའི་ཡིག་ཆའི་མ་ལག་ཡིན།
EFIཁུལ་ལག་མ་གཏོགས་ཁུལ་ལག་གཞན་དག་བཀོལ་སྤྱོད་བྱེད་པར་གྲོས་འགོ་འདོན་རྒྱུ་མིན།</translation>
    </message>
    <message>
        <source>Boot partition not in the first partition
</source>
        <translation>Boottཁུལ་ནི་ངེས་པར་དུ་དབྱེ་བ་དང་པོར་དབྱེ་དགོས།</translation>
    </message>
    <message>
        <source>the EFI partition size should be set between 256MiB and 2GiB
</source>
        <translation>Efiiཁུལ་ཆེ་ཆུང་256MBནས་2GiBབར་བཀོད་སྒྲིག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>This machine not support EFI partition
</source>
        <translation>མིག་སྔར་འཕྲུལ་ཆས་འདིས་EFIIལ་རྒྱབ་སྐྱོར་མི་བྱེད་པ་རེད།</translation>
    </message>
    <message>
        <source>Start Installation</source>
        <translation>སྒྲིག་འཇུག་བྱེད་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
swap partition #6 of  %1 as swap;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་དང་པོ་EFIཡིན་པ་དང་། vfat ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་གཉིས་པ་ bootཡིན་པ། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་གསུམ་པ་/ཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་བཞི་བ་backupཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་ལྔ་བ་dataཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་དྲུག་པ་swapཡིན་པ་དང་། swap ལ་སྒྲིག་འགོད་བྱེད་པ།
</translation>
    </message>
    <message>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
swap partition #8 of  %1 as swap;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་དང་པོ་bootཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་གཉིས་པ་ extendཡིན་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་ལྔ་བ་/ཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་དྲུག་པ་backupཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་བདུན་པ་dataཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་བརྒྱད་པ་swapཡིན་པ་དང་། swap ལ་སྒྲིག་འགོད་བྱེད་པ།
</translation>
    </message>
    <message>
        <source> set to data device;
</source>
        <translation>བཙུགས་པ་དེ་གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <source>bootloader type of Legacy should not contain EFI partition!</source>
        <translation>མ་ལག་legacyའགོ་ཚུགས་བྱེད་སྟངས་ཚེ། མི་འདུ་བའི་efiའཛུགས་སྐྲུན་ཁག།</translation>
    </message>
    <message>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་དང་པོ་ནི། EFI, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། vfat;
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་གཉིས་པ་ནི། boot, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
%1 སྒྲིག་ཆས་སྟེང་གི་ལྷག་མའི་ཁུལ་ནི་། lvm;
lvm ནང་ཁུལ་དུ་ /དབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ backupདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ dataདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ swapདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། swap;</translation>
    </message>
    <message>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
swap partition of lvm as swap;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་དང་པོ་ནི། boot, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་གཉིས་པ་ནི། extend;
%1 སྒྲིག་ཆས་སྟེང་གི་ལྷག་མའི་ཁུལ་ནི་། lvm;
lvm ནང་ཁུལ་དུ་ /དབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ backupདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ dataདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ swapདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། swap;</translation>
    </message>
    <message>
        <source>No created of </source>
        <translation>མ་གཏོད། </translation>
    </message>
    <message>
        <source> partition
</source>
        <translation>ཡན་ལག་དམག་ཁུལ། </translation>
    </message>
    <message>
        <source>If the </source>
        <translation>མ་གཏོད། </translation>
    </message>
    <message>
        <source> partitions are not created,do you want to go to the next step?</source>
        <translation>ཡན་ལག་དམག་ཁུལ་མིན་མུ་མཐུད་དུ་གོམ་སྟབས་རྗེས་མ་འི།？</translation>
    </message>
    <message>
        <source>If the /swap partition is not created,computers cannot be woken up after the system goes to sleep. </source>
        <translation>མ་གཏོད།/swapཡན་ལག་དམག་ཁུལ། མ་ལག་སླེབས་པ་ཉལ་རྗེས་སྲིད་ཐབས་བྲལ་བའི་རྒྱུན་ལྡན་གྱི་བསླངས་གློག་ཀླད།</translation>
    </message>
    <message>
        <source>If the /backup partition is not created,system data cannot be backed up. </source>
        <translation type="obsolete">མ་གཏོད།/backupཡན་ལག་དམག་ཁུལ་གྱིས་བྱེད་ཐབས་བྲལ་མ་ལག་གཞི་གྲངས་གྲ་སྒྲིག།སྐལ།</translation>
    </message>
    <message>
        <source>If the /data partition is not created,user data cannot be backed up. </source>
        <translation>མ་གཏོད།/dataཡན་ལག་དམག་ཁུལ་གྱིས་ཐབས་མེད་སྒྲིག།སྐལ།སྤྱོད་མཁན་གཞི་གྲངས།</translation>
    </message>
    <message>
        <source>will it be formatted, continue?</source>
        <translation>ཡོང་ངེས་གཙང་སེལ།ཡོད་མེད་ལ་མུ་མཐུད།</translation>
    </message>
    <message>
        <source>The disk which was set as a data disk will be formatted</source>
        <translation>འཛུགས་རྒྱུ་དེ་གཞི་གྲངས་ཉོ་བའི་འཁོར་གཞོང་གཞི་གྲངས་ཀྱིས་གཙང་སེལ།ཐག་གཅོད། </translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>མུ་མཐུད།</translation>
    </message>
    <message>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of %1 as ext4;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་དང་པོ་ནི། EFI, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། vfat;
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་གཉིས་པ་ནི། boot, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
%1 སྒྲིག་ཆས་སྟེང་གི་ལྷག་མའི་ཁུལ་ནི་། lvm;
lvm ནང་ཁུལ་དུ་ /དབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ backupདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ dataདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
ནི་/འཛུགས་སྐྲུན་ཁག་ཁྲོད་གསར་གཏོད་swap_fileཡིག་ཆ།;</translation>
    </message>
    <message>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་དང་པོ་EFIཡིན་པ་དང་། vfat ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་གཉིས་པ་ bootཡིན་པ། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་གསུམ་པ་/ཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་བཞི་བ་backupཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་ལྔ་བ་dataཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
ནི་/འཛུགས་སྐྲུན་ཁག་ཁྲོད་གསར་གཏོད་swap_fileཡིག་ཆ།
</translation>
    </message>
    <message>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of %1 ;
partition be left of %1 as lvm;
/ partition of lvm as ext4;
backup partition of lvm as ext4;
data partition of lvm as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་དང་པོ་ནི། boot, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་གཉིས་པ་ནི། extend;
%1 སྒྲིག་ཆས་སྟེང་གི་ལྷག་མའི་ཁུལ་ནི་། lvm;
lvm ནང་ཁུལ་དུ་ /དབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ backupདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
lvm ནང་ཁུལ་དུ་ dataདབྱེ་ཁུལ་བཀོད་སྒྲིག་བྱས་ཡོད།, འཛུགས་བསྐྲུན་བྱེད་གྱིན་ཡོད། ext4;
ནི་/འཛུགས་སྐྲུན་ཁག་ཁྲོད་གསར་གཏོད་swap_fileཡིག་ཆ།;</translation>
    </message>
    <message>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
Create swap_file file in / partition;
</source>
        <translation>%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་དང་པོ་bootཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་གཉིས་པ་ extendཡིན་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་ལྔ་བ་/ཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་དྲུག་པ་backupཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
%1 སྒྲིག་ཆས་སྟེང་གི་ཁུལ་ལག་བདུན་པ་dataཡིན་པ་དང་། ext4 ལ་སྒྲིག་འགོད་བྱེད་པ།
ནི་/འཛུགས་སྐྲུན་ཁག་ཁྲོད་གསར་གཏོད་swap_fileཡིག་ཆ།
</translation>
    </message>
</context>
<context>
    <name>KInstaller::MiddleFrameManager</name>
    <message>
        <source>next</source>
        <translation>རྗེས་མ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::ModifyPartitionFrame</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Format partition.</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་བགོ་ཁུལ།</translation>
    </message>
    <message>
        <source>Mount point</source>
        <translation>འགེལ་ཚེག：</translation>
    </message>
    <message>
        <source>Modify Partition</source>
        <translation>ཁུལ་ལག་བཟོ་བཅོས།</translation>
    </message>
    <message>
        <source>unused</source>
        <translation>སྤྱད་ཚར།</translation>
    </message>
    <message>
        <source>Used to:</source>
        <translation>བཀོལ་སྤྱོད།:</translation>
    </message>
    <message>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>གཞི་འཛིན་ས་གཙོ་བོ་ &apos;/&apos;ཞེས་པ་དེ་འགོ་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <source>The disk can only create one boot partition!</source>
        <translation>ཁབ་ལེན་ཁབ་ལེན་གྱིས་ལྷམ་གྱི་ཆ་ཤས་གཅིག་མ་གཏོགས་བཟོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>The disk can only create one root partition!</source>
        <translation>ཁབ་ལེན་ཁབ་ལེན་གྱིས་རྩ་བའི་ཆ་ཤས་གཅིག་མ་གཏོགས་བཟོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>Bootའཛུགས་སྐྲུན་ཁག་ཚད་ལྡན་ཁོ་ན་ལས་ext4འམ་ext3།</translation>
    </message>
    <message>
        <source>Boot partition size should not be less than 2GiB.</source>
        <translation>Bootཡན་ལག་དམག་ཁུལ་མི་ཆུང།2GiB </translation>
    </message>
    <message>
        <source>Mount point repeat:</source>
        <translation>བཀལ།གླང་རྫི།གནས།བསྐྱར་ཟློས།：</translation>
    </message>
</context>
<context>
    <name>KInstaller::PrepareInstallFrame</name>
    <message>
        <source>Check it and click [Start Installation]</source>
        <translation>འདེམས་བསྐོ་བྱས་རྗེས་[སྒྲིག་འཇུག་བྱེད་འགོ་ཚུགས་པ།]གནོན་པ།</translation>
    </message>
</context>
<context>
    <name>KInstaller::SlideShow</name>
    <message>
        <source>Multi platform support, suitable for mainstream CPU platforms at home and abroad.</source>
        <translation>སྟེགས་བུ་མང་པོས་རྒྱབ་སྐྱོར་བྱས་ཏེ་རྒྱལ་ཁབ་ཕྱི་ནང་གི་གཙོ་རྒྱུན་CPUཡི་སྟེགས་བུ་དང་འཚམ་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Ukui desktop environment meets personalized needs.</source>
        <translation>UKUIགློག་ངོས་ཀྱི་ཁོར་ཡུག་གིས་རང་གཤིས་ཅན་གྱི་དགོས་མཁོ་བསྐང་དགོས།</translation>
    </message>
    <message>
        <source>The system is updated and upgraded more conveniently.</source>
        <translation>མ་ལག་གསར་སྒྱུར་དང་རིམ་པ་འཕར་བ་དེ་བས་སྟབས་བདེ་ཡོང་བ་བྱེད་</translation>
    </message>
    <message>
        <source>Security center, all-round protection of data security.</source>
        <translation>བདེ་འཇགས་ལྟེ་གནས་བཅས་སུ་ཕྱོགས་ཡོངས་ནས་གཞི་གྲངས་བདེ་འཇགས་ལ་སྲུང་སྐྱོབ་བྱེད་དགོས</translation>
    </message>
    <message>
        <source>Software store, featured software recommendation.</source>
        <translation>ཁྱད་ཆོས་ལྡན་པའི་མཉེན་ཆས་ཚོང་ཁང་། ཁྱད་ཆོས་ལྡན་པའི</translation>
    </message>
    <message>
        <source>Provide technical support and professional after-sales service.</source>
        <translation>ལག་རྩལ་གྱི་རྒྱབ་སྐྱོར་བཅས་ལ་བརྟེན་ནས་ཆེད་ལས་ཀྱི་ཞབས་ཞུ་མཁོ་འདོན་</translation>
    </message>
</context>
<context>
    <name>KInstaller::TableWidgetView</name>
    <message>
        <source>no</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <source>yes</source>
        <translation>ཡིན།</translation>
    </message>
    <message>
        <source>Create partition table</source>
        <translation>བགོ་ཁུལ་གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>freespace</source>
        <translation>ཁོམ་ལོང་།</translation>
    </message>
    <message>
        <source>This action will delect all partition,are you sure?</source>
        <translation>མིག་སྔར་ཡོད་པའི་ཁུལ་ལག་རེའུ་མིག་གཙང་སེལ་བྱེད་ངེས་ཡིན་པས་གཙང་སེལ་གཏན་འཁེལ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>       </source>
        <translation type="obsolete">       </translation>
    </message>
</context>
<context>
    <name>KInstaller::TimeZoneFrame</name>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Select Timezone</source>
        <translation>དུས་ཚོད་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Start Configuration</source>
        <translation>ཐོག་མར་བཀོད་སྒྲིག།</translation>
    </message>
</context>
<context>
    <name>KInstaller::UserFrame</name>
    <message>
        <source>Start Installation</source>
        <translation>སྒྲིག་འཇུག་བྱེད་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>རྗེས་མ།</translation>
    </message>
    <message>
        <source>Start Configuration</source>
        <translation>ཐོག་མར་བཀོད་སྒྲིག།</translation>
    </message>
</context>
<context>
    <name>KInstaller::appcheckframe</name>
    <message>
        <source>Choose your app</source>
        <translation>ཁྱོད་ཀྱི་ཉེར་སྤྱོད་གོ་རིམ་</translation>
    </message>
    <message>
        <source>Select the application you want to install to install with one click when using the system.</source>
        <translation>མ་ལག་བཀོལ་སྤྱོད་བྱེད་སྐབས་སྒྲིག་སྦྱོར་བྱེད་འདོད་པའི་ཉེར་སྤྱོད་གོ་རིམ་བདམས་ནས་སྒྲིག་སྦྱོར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Start Installation</source>
        <translation>སྒྲིག་འཇུག་བྱེད་འགོ་ཚུགས་པ།</translation>
    </message>
    <message>
        <source>Start Configuration</source>
        <translation>ཐོག་མར་བཀོད་སྒྲིག།</translation>
    </message>
</context>
<context>
    <name>KInstaller::conferquestion</name>
    <message>
        <source>Which city were you born in?</source>
        <translation>ཁྱོད་ནི་གྲོང་ཁྱེར་གང་ནས་སྐྱེས་པ་ཡིན།</translation>
    </message>
    <message>
        <source>What is your childhood nickname?</source>
        <translation>ཁྱོད་ཀྱི་བྱིས་དུས་ཀྱི་མིང་ལ་ཅི་ཟེར།</translation>
    </message>
    <message>
        <source>Which middle school did you graduate from?</source>
        <translation>ཁྱོད་ནི་སློབ་འབྲིང་གང་ནས་མཐར་ཕྱིན་པ་ཡིན།</translation>
    </message>
    <message>
        <source>What is your father&apos;s name?</source>
        <translation>ཁྱོད་ཀྱི་ཨ་ཕའི་མིང་ལ་ཅི་ཟེར།</translation>
    </message>
    <message>
        <source>What is your mother&apos;s name?</source>
        <translation>ཁྱོད་ཀྱི་ཨ་མའི་མིང་ལ་ཅི་ཟེར།</translation>
    </message>
    <message>
        <source>When is your spouse&apos;s birthday?</source>
        <translation>ཁྱོད་ཀྱི་བཟའ་ཟླའི་སྐྱེས་སྐར་ནམ་ཞིག་རེད།</translation>
    </message>
    <message>
        <source>What&apos;s your favorite animal?</source>
        <translation>ཁྱོད་ཆེས་དགའ་བའི་སྲོག་ཆགས་ནི་ཅི་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <source>set later</source>
        <translation>རྗེས་སུ་གཏན་འཁེལ་བྱས</translation>
    </message>
    <message>
        <source>confer_mainTitle</source>
        <translation>confer_mainTitle</translation>
    </message>
    <message>
        <source>You can reset your password by answering a security question when you forget your password. Make sure you remember the answer.</source>
        <translation>ཁྱོད་ཀྱིས་གསང་གྲངས་བརྗེད་རྗེས་བདེ་འཇགས་ཀྱི་གནད་དོན་ལ་ལན་བཏབ་ན་ཁྱོད་ཀྱི་གསང་གྲངས་བསྐྱར་དུ་བཀོད་སྒྲིག་བྱས་ཆོག ཁྱོད་ཀྱིས་དྲིས་ལན་ཡིད་ལ་འཛིན་པར་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>question%1:</source>
        <translation>གནད་དོན་བརྒྱ་ཆ་1:</translation>
    </message>
    <message>
        <source>answer%1:</source>
        <translation>ལན་%1:</translation>
    </message>
</context>
<context>
    <name>KInstaller::modeselect</name>
    <message>
        <source>Register account</source>
        <translation>ཐོ་འགོད་ཐོ་ཁོངས།</translation>
    </message>
    <message>
        <source>Register immediately</source>
        <translation>འཕྲལ་མར་ཐོ་འགོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Register later</source>
        <translation>རྗེས་སུ་ཐོ་འགོད་བྱ་</translation>
    </message>
    <message>
        <source>After the system installation is completed, an account will be created when entering the system.</source>
        <translation>མ་ལག་སྒྲག་སྦྱར་བྱས་ཚར་རྗེས་མ་ལག་ནང་དུ་འཛུལ་སྐབས་རྩིས་ཐོ་ཞིག་བཟོ་སྲིད།</translation>
    </message>
</context>
<context>
    <name>KInstaller::securityquestions</name>
    <message>
        <source>Security Questions</source>
        <translation>གསང་བའི་བདེ་འཇགས་ཀྱི་གནད་དོན།</translation>
    </message>
    <message>
        <source>You can reset your password by answering security questions when you forget it, please make sure to remember the answer</source>
        <translation>ཁྱེད་ཀྱིས་གསང་བའི་ཨང་གྲངས་བརྗེད་སྐབས་བདེ་འཇགས་གནད་དོན་ལ་ལན་འདེབས་པར་བརྟེན་ནས་གསང་གྲངས་བསྐྱར་སྒྲིག་བྱས་ཏེ་དྲིས་ལན་ངེས་པར་དུ་བློ་ལ་ངེས་ངེས་ཡིན།</translation>
    </message>
    <message>
        <source>Question 1</source>
        <translation>གནད་དོན་1</translation>
    </message>
    <message>
        <source>Answer:</source>
        <translation>དྲིས་ལན།</translation>
    </message>
    <message>
        <source>Question 2</source>
        <translation>གནད་དོན་2ཡོད།</translation>
    </message>
    <message>
        <source>Question 3</source>
        <translation>གནད་དོན་3ཡོད།</translation>
    </message>
    <message>
        <source>Set up later</source>
        <translation>ཅུང་ཙམ་འགོར་རྗེས་བཀོད་སྒྲིག་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>KInstaller::userregisterwidiget</name>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation>ཐེངས་གཉིས་ཀྱི་གསང་ཨང་ནང་འཇུག་ཆ་མཐུན་པ།</translation>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="obsolete">གཙོ་འཁོར་མིང་ངེས་པར་དུ་གསལ་བྱེད་དང་ཨང་གྲངས།འོག་ཐིག་&quot;_&quot;དང་མཐུད་རྟགས་&quot;-&quot;བཅས་ཡིན་དགོས་ལ།རིང་ཚད་གནས་༦༤ལས་བརྒལ་མི་ཆོག</translation>
    </message>
    <message>
        <source>Create User</source>
        <translation>སྤྱོད་མཁན་གསར་བཟོ།</translation>
    </message>
    <message>
        <source>username</source>
        <translation>སྤྱོད་མཁན་མིང་།：</translation>
    </message>
    <message>
        <source>hostname</source>
        <translation>གཙོ་འཁོར་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>new password</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <source>enter the password again</source>
        <translation>སླར་ཡང་གསང་ཨང་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Automatic login on boot</source>
        <translation>འཁོར་ཁ་འབྱེད་ནས་རང་འགུལ་ངང་ཐོ་འགོད།</translation>
    </message>
    <message>
        <source>Biometric [authentication] device detected / unified login support</source>
        <translation type="obsolete">སྐྱེ་དངོས་དབྱེ་འབྱེད་[བདེན་དཔང་]སྒྲིག་ཆས་ལ་ཞིབ་དཔྱད་ཚད་ལེན་དང་གཅིག་གྱུར་གྱི་ཐོ་འགོད་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <source>Hostname should be more than 0 bits in length.</source>
        <translation>གཙོ་ཆས་མིང་གི་རིང་ཚད་ཡིག་རྟགས་1ལས་ཉུང་མི་རུང་།</translation>
    </message>
    <message>
        <source>Hostname should be no more than 64 bits in length.</source>
        <translation>འཕྲུལ་འཁོར་ཨ་མའི་མིང་གི་རིང་ཚད་ཡིག་རྟགས་64ལས་བརྒལ་མི་ཆོག</translation>
    </message>
    <message>
        <source>Hostname only letters,numbers,hyphen and dot notation are allowed</source>
        <translation>འཕྲུལ་འཁོར་གྱི་མིང་ལ་གསལ་བྱེད་མ་གཏོགས་མི་ཆོག་པ་དང་།གྲངས་ཀ་འབྲེལ་མཐུད་རྟགས་སམ་ཚེག་མཚོན་རྟགས་།</translation>
    </message>
    <message>
        <source>Hostname must start with a number or a letter</source>
        <translation>གཙོ་ཆས་མིང་གི་མགོ་ནི་ངེས་པར་དུ་གསལ་བྱེད་དང་ཨང་ཀི་ཡིན་དགོས།</translation>
    </message>
    <message>
        <source>Hostname must end with a number or a letter</source>
        <translation>གཙོ་ཆས་ཀྱི་མིང་གི་མཇུག་ཏུ་ངེས་པར་དུ་གསལ་བྱེད་དང་ཨང་ཀི་དགོས།</translation>
    </message>
    <message>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>གཙོ་ཆས་ཀྱི་མིང་ཁྲོད་འབྲེལ་ཡིག་རྟགས་དང་ཚེག་མཚོན་རྟགས་སྦྲེལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>གཙོ་ཆས་མིང་གི་ཁྲོད་དུ་རྒྱུན་མཐུད་ཀྱི་ཚེག་མཚོན་རྟགས་མི་འདུས།</translation>
    </message>
    <message>
        <source>Password strength:</source>
        <translation>གསང་གྲངས་ཀྱི་ཚད་ནི།</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>ཉམ་ཆུང་།</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation>འབྲིང་རིམ་ཡིན།</translation>
    </message>
    <message>
        <source>High</source>
        <translation>སྟོབས་དང་ལྡན་པ།</translation>
    </message>
    <message>
        <source>After logging into the system, it is recommended that you set biometric passwords and password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation>མ་ལག་ཐོ་འགོད་བྱས་རྗེས། ཁྱོད་ཀྱིས་&quot;འདེམས་བསྐོའི་རྣམ་གྲངས་འགོད་པའི་&quot;ནང་སྐྱེ་དངོས་ཀྱི་ཁྱད་རྟགས་གསང་བ་དང་གསང་བའི་བདེ་འཇགས་གནད་དོན་འགོད་རྒྱུའི་གྲོས་འགོ་བཏོན་པ་དང་། མ་ལག་ཁྲོད་ཉམས་ལེན་སྔར་ལས་ལེགས་པ་ཐོབ་པར་བྱ་རྒྱུའི་གྲོས་འགོ་འདོན་རྒྱུ་ཡིན།</translation>
    </message>
    <message>
        <source>After logging into the system, it is recommended that you set password security questions in [Settings - Login Options] to provide a better experience in the system.</source>
        <translation>མ་ལག་ཐོ་འགོད་བྱས་རྗེས།ཁྱོད་ཀྱིས་&quot;འདེམས་བསྐོའི་རྣམ་གྲངས་འགོད་པའི་&quot;ཁྲོད་གསང་བའི་བདེ་འཇགས་གནད་དོན་བཀོད་སྒྲིག་བྱས་ཏེ་མ་ལག་ཁྲོད་ཉམས་ལེན་སྔར་ལས་ལེགས་པ་ཐོབ་པར་བྱ་རྒྱུའི་གྲོས་འགོ་འདོན་རྒྱུ་ཡིན།</translation>
    </message>
    <message>
        <source>After logging into the system, it is recommended that you set the biometric password in [Settings - Login Options] to have a better experience in the system.</source>
        <translation>མ་ལག་ཐོ་འགོད་བྱས་རྗེས།ཁྱོད་ཀྱིས་&quot;འདེམས་བསྐོའི་རྣམ་གྲངས་འགོད་པའི་&quot;ཁྲོད་སྐྱེ་དངོས་ཀྱི་ཁྱད་ཆོས་དང་གསང་བ་འགོད་རྒྱུའི་གྲོས་འགོ་བཏོན་པ་དང་། མ་ལག་ཁྲོད་ཉམས་ལེན་སྔར་ལས་ལེགས་པ་ཐོབ་པར་བྱ་རྒྱུའི་གྲོས་འགོ་འདོན་རྒྱུ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>KServer::EncryptSetFrame</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">ཁྱེད་ཀྱི་གསང་ཨང་ཉར་ཚགས་ཡག་པོ་བྱེད་རོགས། གལ་ཏེ་གསང་ཨང་བརྗེད་ན་སྡུད་སྡེར་གྱི་གཞི་གྲངས་ལ་འཚམས་འདྲི་བྱེད་ཐབས་མེད།</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">གཏན་འཁེལ་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>password:</source>
        <translation type="obsolete">གསང་ཨང་།</translation>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">ཐེངས་གཉིས་ཀྱི་གསང་ཨང་ནང་འཇུག་ཆ་མཐུན་པ།</translation>
    </message>
    <message>
        <source>password</source>
        <translation>གསང་བ།</translation>
    </message>
    <message>
        <source>The password contains less than two types of characters</source>
        <translation>ཨང་ཡིག་འདུ་བའི་ཡིག་རྟགས་འི་རིགས་དབྱིབས་ལས་ཉུང་2རིགས།</translation>
    </message>
    <message>
        <source>confirm password</source>
        <translation>ངོས་འཛིན་ཨང་ཡིག།</translation>
    </message>
    <message>
        <source>The two passwords are different</source>
        <translation>གཉིས་ཀ་ར་ཐེངས་གསང་བ་མི་མཚུངས་པ།</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,you will not be able to access the disk data.</source>
        <translation>ཁྱེད་ཀྱི་ཉར་ཚགས་ཁྱེད་གསང་བ་དཔེར་ན་བརྗེད་ཨང་ཡིག་ས་ཐབས་མེད་འཚམས་འདྲི་འཁོར་གཞོང།</translation>
    </message>
</context>
<context>
    <name>KServer::MessageBox</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ</translation>
    </message>
</context>
<context>
    <name>KServer::SetPartitionsSize</name>
    <message>
        <source>Data Partitions</source>
        <translation>གཞི་གྲངས་དབྱེ་འབྱེད་ས་ཁུལ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>System Partitions:</source>
        <translation>ཆ་ལག་ཚང་བའི་སྒོ་ནས་དབྱེ་</translation>
    </message>
    <message>
        <source>Data Partitions:</source>
        <translation>གཞི་གྲངས་དབྱེ་འབྱེད་ས་ཁུལ།</translation>
    </message>
    <message>
        <source>Size range from </source>
        <translation>གཞི་གྲངས་དབྱེ་འབྱེད་ནི་ཆུང་མི་རུང་། </translation>
    </message>
    <message>
        <source> to </source>
        <translation> དེ་ལས་ཆེ་བ་བྱེད་མི་རུང་། </translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <source>KeyboardWidget</source>
        <translation>KeyboardWidget</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>wd</source>
        <translation>ནུབ་རྒྱུད་ཀྱི་གཞི་གྲངས།</translation>
    </message>
    <message>
        <source>size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>type</source>
        <translation> རིགས་རྣམ།</translation>
    </message>
    <message>
        <source>used</source>
        <translation>སྤྱད་ཚར།</translation>
    </message>
    <message>
        <source>kylin-data</source>
        <translation>སྤྱོད་མཁན་གྱི་གཞི་གྲངས་ཁུལ་ལག</translation>
    </message>
    <message>
        <source>Format %1 partition, %2
</source>
        <translation>རྣམ་གཞག་ཅན་གྱི་ཡན་ལག་ཁུལ་{་1ཡོད་དམ། } {2
?}</translation>
    </message>
    <message>
        <source>other</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <source>The password contains more than %1 characters of the same class consecutively</source>
        <translation>གསང་ཨང་ནང་དུ་ %1 ལས་བརྒལ་བའི་རིགས་དབྱིབས་གཅིག་གི་རྒྱུན་མཐུད་ཡིག་རྟགས་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>The password contains words from the real name of the user in some form</source>
        <translation>གསང་ཨང་ནང་དུ་རྣམ་པ་ག་གེ་མོའི་སྤྱོད་མཁན་གྱི་མིང་ངོ་མ་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>The password contains less than %1 uppercase letters</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་ཡིག་འབྲི་ཆེ་བ་དེ་%1 ་ལས་ཉུང་བ།</translation>
    </message>
    <message>
        <source>The password contains less than %1 lowercase letters</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་ཡིག་འབྲི་ཆུང་བ་དེ་%1 ་ལས་ཉུང་བ།</translation>
    </message>
    <message>
        <source>Fatal failure</source>
        <translation>རབ་གནོད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>The password contains forbidden words in some form</source>
        <translation>གསང་ཨང་ནང་དུ་རྣམ་པ་ག་གེ་མོའི་བཀོལ་མི་ཆོག་པའི་མིང་རྐྱང་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>No password supplied</source>
        <translation>གསང་ཨང་མཁོ་འདོན་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>The password is shorter than %1 characters</source>
        <translation>གསང་ཨང་ %1 ཡིག་རྟགས་ལས་ཉུང་བ།</translation>
    </message>
    <message>
        <source>The password contains the user name in some form</source>
        <translation>གསང་ཨང་ནང་དུ་རྣམ་པ་ག་གེ་མོའི་སྤྱོད་མཁན་གྱི་མིང་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>The configuration file is malformed</source>
        <translation>སྡེབ་སྒྲིག་ཡིག་ཆའི་རྣམ་གཞག་མི་འགྲིག་པ།</translation>
    </message>
    <message>
        <source>Show debug informations</source>
        <translation>ཚོད་སྒྲིག་ཆ་འཕྲིན་འཆར་བ།</translation>
    </message>
    <message>
        <source>New Partition Table %1
</source>
        <translation>ཁུལ་ལག་གསར་བའི་རེའུ་མིག%1
</translation>
    </message>
    <message>
        <source>The password fails the dictionary check</source>
        <translation>གསང་ཨང་ཚིག་མཛོད་ཀྱི་ཞིབ་བཤེར་ལས་གྲོས་འཆམ་བྱུང་མེད་པ། གསང་གྲངས་ནི་ཚིག་མཛོད་ནང་གི་མིང་རྐྱང་ལ་གཞིགས་པ།</translation>
    </message>
    <message>
        <source>Swap partition</source>
        <translation>ཁུལ་ལག་བརྗེ་རེས།</translation>
    </message>
    <message>
        <source>mounted</source>
        <translation>བཀལ་ཟིན་པ།</translation>
    </message>
    <message>
        <source>Bad integer value</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཧྲིལ་གྲངས་སྒྲིག་འགོད་ཐང་།</translation>
    </message>
    <message>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation>གསང་ཨང་འགྲུབ་པར་ཕམ་པ། སྒྲིག་འགོད་མཁོ་བའི་ཁྲོབ་ཀྱི་ཚད་ལ་སླེབས་མེད།</translation>
    </message>
    <message>
        <source>The password contains less than %1 character classes</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་ཡིག་རྟགས་རིགས་%1 ་ལས་ཉུང་བ།</translation>
    </message>
    <message>
        <source>%1 partition mountPoint %2
</source>
        <translation>དབྱེ་འབྱེད་ཁུལ་གྱི་ཕབ་ལེན་བྱེད་གནས་{་1ཡིན་ནམ། } {2
?}</translation>
    </message>
    <message>
        <source>Create new partition %1,%2,%3
</source>
        <translation>ཁུལ་ལག་གསར་བ་གསར་བཟོ། %1,%2,%3
</translation>
    </message>
    <message>
        <source>Setting %s is not of string type</source>
        <translation>%sསྒྲིག་འགོད་དང་ཡིག་རྟགས་ཕྲེང་གི་རིགས་མིན་པ།</translation>
    </message>
    <message>
        <source>The password is too similar to the old one</source>
        <translation>གསང་ཨང་གསར་པ་དང་གསང་ཨང་རྙིང་པ་འདྲ་དྲགས་པ།</translation>
    </message>
    <message>
        <source>The password is too short</source>
        <translation>གསང་ཨང་གསར་པ་ཐུང་དྲགས་པ།</translation>
    </message>
    <message>
        <source>device</source>
        <translation>སྒྲིག་ཆས།(_D)</translation>
    </message>
    <message>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་དམིགས་བསལ་གྱི་ཡིག་རྟགས་ཧ་ཅང་ཉུང་བ།</translation>
    </message>
    <message>
        <source>format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Delete partition %1
</source>
        <translation>བགོ་ཁུལ་བསུབ་པ།</translation>
    </message>
    <message>
        <source>lenovo</source>
        <translation>ལན་ཞང་།</translation>
    </message>
    <message>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation>གསང་ཨང་ནང་དུ་རིང་དྲགས་པའི་རིགས་གཅིག་གི་ཡིག་རྟགས་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>system</source>
        <translation>རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <source>unused</source>
        <translation>སྤྱད་ཚར།</translation>
    </message>
    <message>
        <source>The password is the same as the old one</source>
        <translation>གསང་ཨང་གསར་པ་དང་གསང་ཨང་རྙིང་པ་གཅིག་པ་རེད།</translation>
    </message>
    <message>
        <source>The password contains too many same characters consecutively</source>
        <translation>གསང་ཨང་ནང་དུ་རྒྱུན་མཐུད་གཅིག་མཚུངས་ཀྱི་ཡིག་རྟགས་མང་དྲགས་པ།</translation>
    </message>
    <message>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation>གསང་ཨང་ནང་དུ་རིགས་དབྱིབས་གཅིག་མཚུངས་ཀྱི་མུ་འབྲེལ་ཡིག་རྟགས་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>The password is just rotated old one</source>
        <translation>གསང་ཨང་ནི་གསང་ཨང་རྙིང་བ་ལྡོག་ཕྱོགས་སུ་འཁོར་བ་ཙམ་རེད།</translation>
    </message>
    <message>
        <source>samsung</source>
        <translation>སམ་ཤིན།</translation>
    </message>
    <message>
        <source>The password is a palindrome</source>
        <translation>གསང་ཨང་ནི་ཀུན་འཁོར་ཡིན་པ།</translation>
    </message>
    <message>
        <source>seagate</source>
        <translation>ཞི་ཅེ།</translation>
    </message>
    <message>
        <source>Bad integer value of setting</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཧྲིལ་གྲངས་སྒྲིག་འགོད་ཐང་།</translation>
    </message>
    <message>
        <source>The password contains monotonic sequence longer than %1 characters</source>
        <translation>གསང་ཨང་ནང་དུ་%1ལས་བརྒལ་བའི་ཡིག་རྟགས་ཀྱི་སྣ་རྐྱང་རིམ་སྟར་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>Setting is not of string type</source>
        <translation>སྒྲིག་འགོད་དང་ཡིག་རྟགས་ཕྲེང་གི་རིགས་མིན་པ།</translation>
    </message>
    <message>
        <source>Unknown setting</source>
        <translation>མ་ཤེས་པའི་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <source>Freespace</source>
        <translation>ཁོམ་ལོང་།</translation>
    </message>
    <message>
        <source>Memory allocation error</source>
        <translation>ནང་གསོག་བགོ་འགྲེམས་ཀྱི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>The password contains too few digits</source>
        <translation>གསང་ཨང་ནང་དུ་ཚུད་པའི་གྲངས་ཀའི་ཨང་རྟགས་གནས་ %ld ལས་ཉུང་བ།</translation>
    </message>
    <message>
        <source>The password contains less than %1 non-alphanumeric characters</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་དམིགས་བསལ་གྱི་ཡིག་རྟགས་%1ལས་ཉུང་བ།</translation>
    </message>
    <message>
        <source>The password contains more than %1 same characters consecutively</source>
        <translation>གསང་ཨང་ནང་དུ་རྒྱུན་མཐུད་གཅིག་མཚུངས་ཀྱི་ཡིག་རྟགས་%1ལས་མང་བ།</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation>གསང་ཨང་རྙོག་འཛིང་ཚད་བླང་བྱ་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <source>The password contains too few uppercase letters</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་ཡིག་འབྲི་ཆེ་བ་ཉུང་དྲགས་པ།</translation>
    </message>
    <message>
        <source>The password contains too few lowercase letters</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་ཡིག་འབྲི་ཆུང་བ་ཉུང་དྲགས་པ།</translation>
    </message>
    <message>
        <source>Create new partition %1,%2
</source>
        <translation>ཁུལ་ལག་གསར་བ་གསར་བཟོ། %1,%2
</translation>
    </message>
    <message>
        <source>Setting is not of integer type</source>
        <translation>སྒྲིག་འགོད་དང་ཧྲིལ་གྲངས་རིགས་མིན་པ།</translation>
    </message>
    <message>
        <source>Opening the configuration file failed</source>
        <translation>སྡེབ་སྒྲིག་ཡིག་ཆ་ཁ་འབྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>The password contains less than %1 digits</source>
        <translation>གསང་ཨང་ནང་དུ་འདུས་པའི་གྲངས་ཀའི་ཡིག་རྟགས་གནས་%1ལས་ཉུང་བ།</translation>
    </message>
    <message>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation>སྐབས་བསྟུན་གྲངས་སྐྱེད་གྲུབ་ཀྱི་སྒྲིག་ཆས་RNGལས་སྐབས་བསྟུན་གྲངས་ཐོབ་ཐབས་མེད།</translation>
    </message>
    <message>
        <source>Setting %s is not of integer type</source>
        <translation>%s སྒྲིག་འགོད་དང་ཧྲིལ་གྲངས་རིགས་མིན་པ།</translation>
    </message>
    <message>
        <source>Format partition %1,%2,%3
</source>
        <translation>ཁུལ་ལག་རྣམ་པར་འཇོག་པ། %1,%2,%3
</translation>
    </message>
    <message>
        <source>The password does not contain enough character classes</source>
        <translation>གསང་ཨང་ནང་དུ་ཡིག་རྟགས་ཀྱི་རིགས་འདང་ངེས་ཤིག་འདུས་མེད།</translation>
    </message>
    <message>
        <source>Memory allocation error when setting</source>
        <translation>སྒྲིག་འགོད་བྱེད་སྐབས་ནང་གསོག་བགོ་བཤའི་ནོར་འཁྲུལ་བྱུང་བ།</translation>
    </message>
    <message>
        <source>The password differs with case changes only</source>
        <translation>གསང་ཨང་ནང་དུ་ཡིག་འབྲི་ཆེ་ཆུང་བསྒྱུར་བཅོས་ཁོ་ན་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>KCommand::m_cmdInstance is not init.</source>
        <translation>བཀའ་རྒྱ་m_cmdInstance་ཐོག་མ་ཉིད་ནས་འགྱུར་མེད།</translation>
    </message>
    <message>
        <source>WorkingPath is not found. 
</source>
        <translation>ལས་ཀའི་དཀར་ཆག་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <source>Shell file is empty, does not continue. 
</source>
        <translation>ཡིག་ཆ་སྟོང་པར་བཞག་ནས་ལས་འགན་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <source>hitachi</source>
        <translation>ཉིན་རེ་བཞིན་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <source>toshiba</source>
        <translation>ཏུང་ཀྲི།</translation>
    </message>
    <message>
        <source>fujitsu</source>
        <translation>ཧྥུ་ཧྲི་ཤར་གཏོང་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>maxtor</source>
        <translation></translation>
    </message>
    <message>
        <source>IBM</source>
        <translation>IBM</translation>
    </message>
    <message>
        <source>excelStor</source>
        <translation>གསར་གཏོད་བྱེད་སླ་</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>མ་ཤེས་པ།</translation>
    </message>
    <message>
        <source>Extended partition %1 has 
</source>
        <translation>ཡན་ལག་ཁུལ་གྱི་བརྒྱ་ཆ་1འཕར་ཡོད།</translation>
    </message>
    <message>
        <source>Reset size %1 partition
</source>
        <translation>དབྱེ་འབྱེད་ཁུལ་གྱི་ཆེ་ཆུང་དང་ཆེ་ཆུང་ཁག་གཅིག་བསྐྱར་སྒྲིག་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>kylin data partition</source>
        <translation>གཞི་གྲངས་སྡེར་མ།</translation>
    </message>
</context>
</TS>
