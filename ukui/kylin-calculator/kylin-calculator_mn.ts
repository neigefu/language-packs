<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ᠎ᠲᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>ᠵᠡᠭᠦᠨ ᠬᠠᠭᠠᠯᠲᠠ ᠳᠤᠲᠠᠭᠤ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠬᠡᠲᠦᠷᠬᠡᠢ ᠶᠡᠬᠡ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤ ᠲᠣᠭ᠎ᠠ ᠳᠤᠲᠠᠭᠳᠠᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>ᠵᠢᠯᠣᠭᠣᠳᠬᠤ ᠲᠡᠮᠳᠡᠭ᠎ᠢ᠋ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠭᠴᠢ ᠲᠣᠭ᠎ᠠ ᠨᠢ 0 ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠲᠣᠭ᠎ᠠ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>ᠰᠣᠯᠢᠨ ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>Calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <location filename="../src/mainwindow.cpp" line="1337"/>
        <location filename="../src/mainwindow.cpp" line="1353"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1335"/>
        <source>calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="268"/>
        <source>Copy</source>
        <translation>ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="269"/>
        <source>Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="923"/>
        <source>input too long</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠤᠯᠲᠠ ᠬᠡᠳᠦ ᠤᠷᠲᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="75"/>
        <location filename="../src/mainwindow.cpp" line="1337"/>
        <location filename="../src/mainwindow.cpp" line="1357"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1370"/>
        <source>exchange rate</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="801"/>
        <location filename="../src/mainwindow.cpp" line="805"/>
        <source>Error!</source>
        <translation>ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="809"/>
        <location filename="../src/mainwindow.cpp" line="1552"/>
        <source>Input error!</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠤᠯᠲᠠ ᠨᠢ ᠬᠡᠳᠦ ᠤᠷᠲᠤ !</translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="71"/>
        <location filename="../src/programmer/programmodel.cpp" line="236"/>
        <location filename="../src/programmer/programmodel.cpp" line="320"/>
        <source>Input error!</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="402"/>
        <source>ShowBinary</source>
        <translation>ᠦᠵᠡᠭᠦᠯᠬᠦ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="408"/>
        <source>HideBinary</source>
        <translation>ᠨᠢᠭᠤᠭᠳᠠᠮᠠᠯ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Calculator</source>
        <translation type="vanished">计算器</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Standard</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ — ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="292"/>
        <source>Scientific</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="58"/>
        <location filename="../src/titlebar.cpp" line="73"/>
        <location filename="../src/titlebar.cpp" line="259"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="59"/>
        <location filename="../src/titlebar.cpp" line="271"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="293"/>
        <source>Exchange Rate</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ — ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="294"/>
        <source>Programmer</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ — ᠫᠷᠦᠭᠷᠠᠮᠴᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>StayTop</source>
        <translation>ᠣᠷᠣᠢ᠎ᠳ᠋ᠤ᠌ ᠲᠠᠯᠪᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="430"/>
        <source>Restore</source>
        <translation>ᠠᠩᠭᠢᠵᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="123"/>
        <location filename="../src/titlebar.cpp" line="320"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="124"/>
        <location filename="../src/titlebar.cpp" line="321"/>
        <location filename="../src/titlebar.cpp" line="421"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠶᠡᠭᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="125"/>
        <location filename="../src/titlebar.cpp" line="322"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="57"/>
        <location filename="../src/programmer/toolbar.cpp" line="203"/>
        <location filename="../src/programmer/toolbar.cpp" line="206"/>
        <source>ShowBinary</source>
        <translation>ᠦᠵᠡᠭᠦᠯᠬᠦ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="204"/>
        <location filename="../src/programmer/toolbar.cpp" line="205"/>
        <source>HideBinary</source>
        <translation>ᠨᠢᠭᠤᠭᠳᠠᠮᠠᠯ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="91"/>
        <source>Rate update</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠰᠢᠨᠡᠳᠬᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="106"/>
        <location filename="../src/toolmodel.cpp" line="217"/>
        <source>Chinese Yuan</source>
        <translation>ᠠᠷᠠᠳ᠎ᠤ᠋ᠨ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="114"/>
        <location filename="../src/toolmodel.cpp" line="244"/>
        <source>US Dollar</source>
        <translation>ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="211"/>
        <source>UAE Dirham</source>
        <translation>ᠠᠷᠠᠪ᠎ᠤ᠋ᠨ ᠬᠣᠯᠪᠣᠭᠠᠲᠤ ᠠᠬᠠᠮᠠᠳᠲᠤ ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠳ᠋ᠢᠯᠠᠮ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="211"/>
        <source>Argentinian peso</source>
        <translation>ᠠᠷᠭᠸᠨ᠋ᠲ᠋ᠢᠨ᠎ᠤ᠋ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="212"/>
        <source>Australian Dollar</source>
        <translation>ᠠᠦ᠋ᠰᠲ᠋ᠷᠠᠯᠢᠶ᠎ᠠ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="212"/>
        <source>Bulgarian Lev</source>
        <translation>ᠪᠣᠯᠭᠠᠷᠢᠶ᠎ᠠ ᠷᠸᠸᠹ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="213"/>
        <source>Bahraini Dinar</source>
        <translation>ᠪᠠᠭᠠᠷᠢᠨ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="213"/>
        <source>Brunei Dollar</source>
        <translation>ᠪᠷᠦᠨ᠋ᠸᠢ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="214"/>
        <source>Brazilian Real</source>
        <translation>ᠪᠠᠰᠢᠯᠧᠶᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="214"/>
        <source>Bahaman Dollar</source>
        <translation>ᠪᠠᠬᠠᠮᠠᠮᠠ ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="215"/>
        <source>Botswana Pula</source>
        <translation>ᠪᠤᠼᠸᠠᠨᠠᠫᠦ᠋ᠯᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="215"/>
        <source>Canadian Dollar</source>
        <translation>ᠺᠠᠨᠠᠳᠠ᠎ᠶ᠋ᠢᠨ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="216"/>
        <source>CFA Franc</source>
        <translation>ᠳᠤᠮᠳᠠᠳᠤ ᠠᠹᠷᠢᠺᠠ᠎ᠶ᠋ᠢᠨ ᠹᠷᠠᠩᠺ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="216"/>
        <source>Swiss Franc</source>
        <translation>ᠰᠸᠢᠰ᠎ᠦ᠋ᠨ ᠹᠷᠠᠩᠺ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="217"/>
        <source>Chilean Peso</source>
        <translation>ᠴᠢᠯᠢ ᠪᠢ ᠰᠦᠸᠧ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="218"/>
        <source>Colombian Peso</source>
        <translation>ᠺᠣᠯᠣᠮᠪᠢᠶ᠎ᠠ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="218"/>
        <source>Czech Koruna</source>
        <translation>ᠴᠧᠺ ᠺᠯᠠᠩ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="219"/>
        <source>Danish Krone</source>
        <translation>ᠳ᠋ᠠᠨᠮᠠᠷᠺ ᠺᠷᠤᠨᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="219"/>
        <source>Dominican peso</source>
        <translation>ᠳᠤᠮᠢᠨᠢᠺᠠ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="220"/>
        <source>Algerian Dinar</source>
        <translation>ᠠᠯᠵᠧᠷᠢᠶ᠎ᠠ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="220"/>
        <source>Estonian Kroon</source>
        <translation>ᠡᠧᠰᠲ᠋ᠤᠨᠢᠶ᠎ᠠ ᠺᠷᠤᠨᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="221"/>
        <source>Egyptian pound</source>
        <translation>ᠶᠧᠵᠢᠫᠲ᠎ᠦ᠋ᠨ ᠫᠦᠨᠳ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="221"/>
        <source>Euro</source>
        <translation>ᠧᠦ᠋ᠷᠣᠫᠠ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="222"/>
        <source>Fijian dollar</source>
        <translation>ᠹᠧᠢ ᠵᠢᠢ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="222"/>
        <source>Pound Sterling</source>
        <translation>ᠫᠦᠨᠳ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="223"/>
        <source>Guatemalan Quetzal</source>
        <translation>ᠭᠤᠸᠠᠲ᠋ᠸᠮᠠᠯᠠᠭᠲ᠋ᠠᠢ ᠴᠠᠯᠠᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="223"/>
        <source>Hong Kong Dollar</source>
        <translation>ᠭᠠᠷᠠᠮ᠎ᠤ᠋ᠨ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="224"/>
        <source>Croatian Kuna</source>
        <translation>ᠺᠷᠤᠲ᠋ᠢᠶ᠎ᠠ ᠺᠤᠨᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="224"/>
        <source>Hungarian Forint</source>
        <translation>ᠬᠠᠩᠭᠠᠷᠢ᠎ᠶ᠋ᠢᠨ ᠹᠦ ᠯᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="225"/>
        <source>Indonesian Rupiah</source>
        <translation>ᠢᠨᠳᠣᠨᠧᠽᠢ ᠷᠦᠫᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="225"/>
        <source>Israeli New Shekel</source>
        <translation>ᠢᠰᠷᠸᠯ ᠰᠢᠨ᠎ᠡ ᠰᠢᠺᠧᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="226"/>
        <source>Indian Rupee</source>
        <translation>ᠡᠨᠡᠳᠬᠡᠭ᠎ᠦ᠋ᠨ ᠷᠦᠪᠪᠢ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="226"/>
        <source>Iranian Rial</source>
        <translation>ᠢᠷᠡᠨ ᠷᠢᠶᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="227"/>
        <source>Icelandic Krona</source>
        <translation>ᠠᠢᠰᠯᠠᠨᠳ᠋ ᠺᠯᠠᠩ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="227"/>
        <source>Japanese Yen</source>
        <translation>ᠶᠠᠫᠣᠨ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="228"/>
        <source>South Korean Won</source>
        <translation>ᠬᠠᠨ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="228"/>
        <source>Kuwaiti Dinar</source>
        <translation>ᠺᠦᠸᠠᠶᠢᠲ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="229"/>
        <source>Kazakhstani Tenge</source>
        <translation>ᠺᠠᠽᠠᠭ ᠲᠠᠨ ᠲᠡᠭᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="229"/>
        <source>Sri Lankan Rupee</source>
        <translation>ᠰᠷᠢ ᠯᠠᠨᠺᠠ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="230"/>
        <source>Lithuanian Litas</source>
        <translation>ᠯᠢᠲ᠋ᠣ᠎ᠠ ᠯᠢᠲ᠋ᠣ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="230"/>
        <source>Latvian Lats</source>
        <translation>ᠯᠠᠲ᠋ᠧᠢᠶ᠎ᠠ ᠯᠠᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="231"/>
        <source>Libyan Dinar</source>
        <translation>ᠯᠢᠪᠢᠶ᠎ᠠ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="231"/>
        <source>Mauritian Rupee</source>
        <translation>ᠮᠠᠦ᠋ᠷᠢᠲ᠋ᠢᠦ᠋ᠰ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="232"/>
        <source>Maldivian Rupee</source>
        <translation>ᠮᠠᠯᠳᠠᠢᠹ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="232"/>
        <source>Mexican Peso</source>
        <translation>ᠮᠧᠺᠰᠢᠺᠦ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="233"/>
        <source>Malaysian Ringgit</source>
        <translation>ᠮᠠᠯᠠᠢᠰᠢᠶ᠎ᠠ ᠷᠢᠨᠲ᠋ᠧ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="233"/>
        <source>Norwegian Krone</source>
        <translation>ᠨᠤᠷᠸᠠᠢ ᠺᠸᠷᠦ᠋ᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="234"/>
        <source>Nepalese Rupee</source>
        <translation>ᠪᠠᠯᠪᠤ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="234"/>
        <source>New Zealand Dollar</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠽᠢᠯᠠᠨᠳ᠋ ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="235"/>
        <source>Omani Rial</source>
        <translation>ᠠᠮᠠᠮᠠᠨ ᠷᠢᠶᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="235"/>
        <source>Panamanian balbos</source>
        <translation>ᠫᠠᠨᠠᠮᠠ ᠫᠣᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="236"/>
        <source>Peruvian Nuevo Sol</source>
        <translation>ᠫᠧᠷᠥ᠋᠎ᠶ᠋ᠢᠨ ᠰᠢᠨ᠎ᠡ ᠰᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="236"/>
        <source>Philippine Peso</source>
        <translation>ᠹᠢᠯᠢᠫᠢᠨ᠎ᠦ᠌ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="237"/>
        <source>Pakistani Rupee</source>
        <translation>ᠫᠠᠺᠢᠰᠲ᠋ᠠᠨ᠎ᠤ᠋ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="237"/>
        <source>Polish Zloty</source>
        <translation>ᠫᠣᠯᠠᠨᠼ ᠷᠣᠯᠲ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="238"/>
        <source>Paraguayan Guaran</source>
        <translation>ᠫᠠᠷᠠᠭᠤᠧ᠋ ᠷᠠᠨᠢ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="238"/>
        <source>Qatari Riyal</source>
        <translation>ᠺᠠᠲ᠋ᠠᠷᠢᠶ᠎ᠠ (᠎ᠺᠠᠲ᠋ᠠᠷᠢᠶ᠎ᠠ )</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="239"/>
        <source>New Romanian Leu</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠷᠤᠮᠠᠨᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="239"/>
        <source>Russian Rouble</source>
        <translation>ᠣᠷᠣᠰ᠎ᠤ᠋ᠨ ᠷᠦᠪᠯᠢ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="240"/>
        <source>Saudi Riyal</source>
        <translation>ᠰᠠᠦ᠋ᠲ᠋ᠷᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="240"/>
        <source>Swedish Krona</source>
        <translation>ᠰᠸᠧᠳᠧᠨ ᠺᠸᠷᠦ᠋ᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="241"/>
        <source>Singapore Dollar</source>
        <translation>ᠰᠢᠩᠭᠠᠫᠦᠷ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="241"/>
        <source>Thai Baht</source>
        <translation>ᠲᠠᠢ ᠵᠣ᠌</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="242"/>
        <source>Tunisian Dinar</source>
        <translation>ᠲᠦᠨᠢᠰ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="242"/>
        <source>New Turkish Lira</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠲᠤᠷᠴᠢ ᠯᠢᠷᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="243"/>
        <source>T&amp;T Dollar (TTD)</source>
        <translation>ᠲᠷᠢᠨᠢᠳᠠᠳ᠋ ᠪᠠ ᠲᠤᠪᠠᠭᠤ᠋ ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="243"/>
        <source>Taiwan Dollar</source>
        <translation>ᠲᠠᠢᠸᠠᠨ᠎ᠤ᠋ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="244"/>
        <source>Ukrainian Hryvnia</source>
        <translation>ᠦᠺᠷᠠᠢᠨ ᠭᠷᠢᠹᠨᠠ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="245"/>
        <source>Uruguayan Peso</source>
        <translation>ᠦᠷᠤᠢᠭᠠᠧᠠᠢᠶᠢᠨ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="245"/>
        <source>Venezuelan Bolívar</source>
        <translation>ᠸᠧᠨ᠋ᠸᠽᠦ᠋ᠸᠯᠠ ᠪᠤᠯᠢᠸᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="246"/>
        <source>South African Rand</source>
        <translation>ᠡᠮᠦᠨᠡᠲᠦ ᠠᠹᠷᠢᠺᠠ᠎ᠶ᠋ᠢᠨ ᠷᠠᠨᠳ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="509"/>
        <source>Error!</source>
        <translation>ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>ᠫᠷᠦᠭᠷᠠᠮᠴᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠄ </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ ᠪᠣᠯ qt5᠎ᠤ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠰᠠᠭᠤᠷᠢᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠡᠨ ᠬᠥᠩᠭᠡᠨ ᠳᠡᠰ᠎ᠦ᠋ᠨ ᠪᠣᠳᠣᠭᠤᠷ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠬᠠᠩᠭᠠᠵᠤ ᠂ ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨᠴᠢ᠎ᠪᠠᠷ ᠪᠣᠳᠣᠬᠤ ᠪᠠ ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ᠎ᠶ᠋ᠢ ᠰᠣᠯᠢᠨ ᠪᠣᠳᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
</context>
</TS>
