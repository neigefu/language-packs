<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>མཚོན་ཚུལ་སྟོང་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>མཚོན་ཚུལ་འཆོལ་བ།</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>གཡོན་རྟགས་མེད།</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>གྲངས་ཐང་ཆེ་སོང་།</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>བཀོལ་སྤྱོད་ཀྱི་གྲངས་ཀ་ཉུང་བ།</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>བཀོལ་སྤྱོད་རྟགས་ལ་མཚན་ཉིད་བཞག་མེད།</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>གྲངས་ཀ་ལས་0འགྲུབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>གཡས་སྤྱོད་གྲངས་ནོར་བ།</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>ཚད་གཞི་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>ཚན་རིག་གི་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>ཚད་གཞི་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>ཚན་རིག་གི་རང་བཞིན།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <location filename="../src/mainwindow.cpp" line="1337"/>
        <location filename="../src/mainwindow.cpp" line="1353"/>
        <source>standard</source>
        <translation>ཚད་གཞི་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1335"/>
        <source>calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="268"/>
        <source>Copy</source>
        <translation>འདྲ་བཟོ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="269"/>
        <source>Paste</source>
        <translation>སྦྱར་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="923"/>
        <source>input too long</source>
        <translation>ནང་འཇུག་རིང་དྲགས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="75"/>
        <location filename="../src/mainwindow.cpp" line="1337"/>
        <location filename="../src/mainwindow.cpp" line="1357"/>
        <source>scientific</source>
        <translation>ཚན་རིག་གི་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1370"/>
        <source>exchange rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="801"/>
        <location filename="../src/mainwindow.cpp" line="805"/>
        <source>Error!</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="809"/>
        <location filename="../src/mainwindow.cpp" line="1552"/>
        <source>Input error!</source>
        <translation>ནོར་འཁྲུལ་ནང་འཇུག་བྱོས།</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>ནང་འཇུག་རིང་དྲགས་སོང་།</translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="71"/>
        <location filename="../src/programmer/programmodel.cpp" line="236"/>
        <location filename="../src/programmer/programmodel.cpp" line="320"/>
        <source>Input error!</source>
        <translation>ནོར་འཁྲུལ་ནང་འཇུག་བྱོས།</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="402"/>
        <source>ShowBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་མངོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="408"/>
        <source>HideBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་སྦས་སྐུང་།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Calculator</source>
        <translation type="vanished">计算器</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Standard</source>
        <translation>རྩིས་ཆས།ཚད་ལྡན་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="292"/>
        <source>Scientific</source>
        <translation>རྩིས་ཆས།ཚན་རིག་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="58"/>
        <location filename="../src/titlebar.cpp" line="73"/>
        <location filename="../src/titlebar.cpp" line="259"/>
        <source>standard</source>
        <translation>ཚད་གཞི་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="59"/>
        <location filename="../src/titlebar.cpp" line="271"/>
        <source>scientific</source>
        <translation>ཚན་རིག་གི་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="293"/>
        <source>Exchange Rate</source>
        <translation>རྩིས་ཆས།འཛའ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="294"/>
        <source>Programmer</source>
        <translation>རྩིས་ཆས།བྱ་རིམ་པའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>StayTop</source>
        <translation>རྩེ་མོ།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="430"/>
        <source>Restore</source>
        <translation>སྔར་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="123"/>
        <location filename="../src/titlebar.cpp" line="320"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="124"/>
        <location filename="../src/titlebar.cpp" line="321"/>
        <location filename="../src/titlebar.cpp" line="421"/>
        <source>Maximize</source>
        <translation>ཆེས་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="125"/>
        <location filename="../src/titlebar.cpp" line="322"/>
        <source>Close</source>
        <translation>སྒོ་གཏན་རོགས།</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="57"/>
        <location filename="../src/programmer/toolbar.cpp" line="203"/>
        <location filename="../src/programmer/toolbar.cpp" line="206"/>
        <source>ShowBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་མངོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="204"/>
        <location filename="../src/programmer/toolbar.cpp" line="205"/>
        <source>HideBinary</source>
        <translation>གཉིས་གོང་འགྲིལ་ལུགས་སྦས་སྐུང་།</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="91"/>
        <source>Rate update</source>
        <translation>དངུལ་བརྗེས་འཛའ་ཐང་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="106"/>
        <location filename="../src/toolmodel.cpp" line="217"/>
        <source>Chinese Yuan</source>
        <translation>མི་དམངས་ཤོག་སྒོར་སྐོར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="114"/>
        <location filename="../src/toolmodel.cpp" line="244"/>
        <source>US Dollar</source>
        <translation>མེ་སྒོར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="211"/>
        <source>UAE Dirham</source>
        <translation>ཨ་རབ་མཉམ་འབྲེལ་ཚོ་དཔོན་རྒྱལ་ཁབ་ཀྱི་ཏི་ལའོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="211"/>
        <source>Argentinian peso</source>
        <translation>ཨ་ཀེན་ཐིན་གྱི་པི་སོའོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="212"/>
        <source>Australian Dollar</source>
        <translation>ཨོ་སི་ཁྲུ་ལི་ཡའི་སྒོར་མོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="212"/>
        <source>Bulgarian Lev</source>
        <translation>པུར་ཀ་རི་ཡའི་ལེ་ཧྥུ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="213"/>
        <source>Bahraini Dinar</source>
        <translation>པ་ལིན་ཏེ་ནར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="213"/>
        <source>Brunei Dollar</source>
        <translation>ཝུན་ལའེ་ཡོན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="214"/>
        <source>Brazilian Real</source>
        <translation>པར་ཙིར་གྱི་ལེ་ཡར་གྱིས།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="214"/>
        <source>Bahaman Dollar</source>
        <translation>པ་ཧ་མ་ཡོན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="215"/>
        <source>Botswana Pula</source>
        <translation>པོ་ཚི་ཝ་ན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="215"/>
        <source>Canadian Dollar</source>
        <translation>ཁ་ན་ཏའི་སྒོར་མོ་ཆེན་པོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="216"/>
        <source>CFA Franc</source>
        <translation>ཀྲུང་གོ་དང་ཧྥེ་གླིང་གི་ཧྥ་སྒོར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="216"/>
        <source>Swiss Franc</source>
        <translation>སུའེ་ཙེར་གྱི་ཧྥ་སྒོར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="217"/>
        <source>Chilean Peso</source>
        <translation>ཀྲི་ལི་པིས་སོའོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="218"/>
        <source>Colombian Peso</source>
        <translation>ཁོ་ལོམ་པི་ཡ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="218"/>
        <source>Czech Koruna</source>
        <translation>ཅེ་ཁེ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="219"/>
        <source>Danish Krone</source>
        <translation>བསྟན་མེ་ཁུ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="219"/>
        <source>Dominican peso</source>
        <translation>མདོ་སྨིས་ཅ་པའེ་སོའོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="220"/>
        <source>Algerian Dinar</source>
        <translation>ཨར་ཅི་རི་ཡ།ཏི་ནར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="220"/>
        <source>Estonian Kroon</source>
        <translation>ཨེ་ཧྲ་ནི་ཡ་ཁེ་ལང་།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="221"/>
        <source>Egyptian pound</source>
        <translation>ཨེ་ཅིབ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="221"/>
        <source>Euro</source>
        <translation>ཡོབ་སྒོར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="222"/>
        <source>Fijian dollar</source>
        <translation>ཧྥེ་ཅི་ཡོན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="222"/>
        <source>Pound Sterling</source>
        <translation>དབྱིན་སྒོར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="223"/>
        <source>Guatemalan Quetzal</source>
        <translation>བེ་ཏེ་མ་ལ་ཀེ་ཁྲ་འར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="223"/>
        <source>Hong Kong Dollar</source>
        <translation>ཤང་ཀང་གི་དངུལ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="224"/>
        <source>Croatian Kuna</source>
        <translation>ཁ་རེ་ཤེ་ཡའི་ཁུའུ་ན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="224"/>
        <source>Hungarian Forint</source>
        <translation>ཧང་ཀ་རི་ཁའི་ཧྥུ་ལིན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="225"/>
        <source>Indonesian Rupiah</source>
        <translation>ཧིན་རྡུ་ཉི་ཞི་ཡའི་ལུ་པིས།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="225"/>
        <source>Israeli New Shekel</source>
        <translation>དབྱི་སི་རལ་གྱི་ཞེ་ཁུ་འར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="226"/>
        <source>Indian Rupee</source>
        <translation>ཧིན་རྡུ་ལོའུ་པི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="226"/>
        <source>Iranian Rial</source>
        <translation>ཨི་རན་ལི་ཡར་གྱིས།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="227"/>
        <source>Icelandic Krona</source>
        <translation>པིན་ཏའོ་ཁུ་ལང་།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="227"/>
        <source>Japanese Yen</source>
        <translation>འཇར་པན་སྒོར་མོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="228"/>
        <source>South Korean Won</source>
        <translation>ཧན་ཡོན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="228"/>
        <source>Kuwaiti Dinar</source>
        <translation>ཁུ་ཝེ་ཐེ་ཏིས་ན་འར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="229"/>
        <source>Kazakhstani Tenge</source>
        <translation>ཧ་སག་སི་ཐན་གྱི་ཐེང་ཀེ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="229"/>
        <source>Sri Lankan Rupee</source>
        <translation>སེ་རི་ལི་ལན་ཁ་ལོ་པི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="230"/>
        <source>Lithuanian Litas</source>
        <translation>ལི་ཐའོ་ཝན།ལི་ཐེ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="230"/>
        <source>Latvian Lats</source>
        <translation>ལ་ཐོ་ཝེ་ཡ་ལ་ཐི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="231"/>
        <source>Libyan Dinar</source>
        <translation>ལི་པི་ཡ་ཏི་ནཱ་འར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="231"/>
        <source>Mauritian Rupee</source>
        <translation>མའོ་ལི་ཆོ་སི།ལུའུ་པི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="232"/>
        <source>Maldivian Rupee</source>
        <translation>ཨཱར་ཏི་ཝེ་ལོའུ་པི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="232"/>
        <source>Mexican Peso</source>
        <translation>མོ་ཞི་ཁོའི་པིས་སོའོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="233"/>
        <source>Malaysian Ringgit</source>
        <translation>མ་ལེ་ཞི་ཡ་ཅི་ཐུ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="233"/>
        <source>Norwegian Krone</source>
        <translation>ནོར་ཝེ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="234"/>
        <source>Nepalese Rupee</source>
        <translation>ནེ་པ་ལ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="234"/>
        <source>New Zealand Dollar</source>
        <translation>ཞིན་ཞི་ལན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="235"/>
        <source>Omani Rial</source>
        <translation>ཨ་མན་ལི་ཡ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="235"/>
        <source>Panamanian balbos</source>
        <translation>ཕ་ན་མ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="236"/>
        <source>Peruvian Nuevo Sol</source>
        <translation>ཕེ་རུའི་ཤིན་སོའོ་འར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="236"/>
        <source>Philippine Peso</source>
        <translation>ཧྥེ་ལི་པིན་པེ་སའོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="237"/>
        <source>Pakistani Rupee</source>
        <translation>པ་ཀི་སི་ཐན།ལུའུ་པི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="237"/>
        <source>Polish Zloty</source>
        <translation>པོ་ལན་ཙི་ལོ་ཐི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="238"/>
        <source>Paraguayan Guaran</source>
        <translation>པཱ་ལ་ཀུའེ།ཀྭ་ལ་ཉི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="238"/>
        <source>Qatari Riyal</source>
        <translation>ཁ་ཐར་ལིས་ཡ་འར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="239"/>
        <source>New Romanian Leu</source>
        <translation>རོ་མ་ཉི་ཡ་གསར་པའི་ལེ་དབྱི།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="239"/>
        <source>Russian Rouble</source>
        <translation>ཨུ་རུ་སིའི་ལུའུ་པུའུ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="240"/>
        <source>Saudi Riyal</source>
        <translation>ཧྲ་ཐི་ལི་ཡེར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="240"/>
        <source>Swedish Krona</source>
        <translation>རུའེ་ཏན་ཁེ་ལང་།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="241"/>
        <source>Singapore Dollar</source>
        <translation>སིང་ག་ཕོར་གྱི་སྒོར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="241"/>
        <source>Thai Baht</source>
        <translation>ཐེ་ཀྲུའུ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="242"/>
        <source>Tunisian Dinar</source>
        <translation>ཐུ་ནེ་སི་ཏི་ནར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="242"/>
        <source>New Turkish Lira</source>
        <translation>ཞིའར་ཆི་ལི་ལ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="243"/>
        <source>T&amp;T Dollar (TTD)</source>
        <translation>ཐི་ལི་ཉི་ཏ་དང་ཐོ་པ་ཀི་ཡོན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="243"/>
        <source>Taiwan Dollar</source>
        <translation>ཐའེ་དངུལ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="244"/>
        <source>Ukrainian Hryvnia</source>
        <translation>ཝུའུ་ཁི་ལན་ཀུ་ལི་ཧྥུ་ན།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="245"/>
        <source>Uruguayan Peso</source>
        <translation>ཝུ་ར་ཀུའེ།པི་སོའོ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="245"/>
        <source>Venezuelan Bolívar</source>
        <translation>ཝེ་ནེ་ཙུའེ་ལའི་པོ་ལི་ཝེ་ཝར།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="246"/>
        <source>South African Rand</source>
        <translation>ནན་ཧྥེ་ལན་ཐེ།</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="509"/>
        <source>Error!</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>གདམ་ག</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>ཚད་གཞི་ཅན།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>ཚན་རིག་གི་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>བྱ་རིམ་པའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>བརྗོད་གཞི།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>རོགས་རམ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>པར་གཞི། </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>རྩིས་ཆས་ནི་qut5གསར་སྤེལ་བྱས་པར་གཞིགས་པའི་ཡང་བའི་རིམ་པའི་རྩིས་ཆས་ཤིག་ཡིན་ཞིང་།ཚད་གཞི་ལྟར་རྩིས་རྒྱག་རྒྱུ་དང་།ཚན་རིག་དང་མཐུན་པའི་རྩིས་རྒྱག་དང་དངུལ་བརྗེས་འཛའ་ཐང་ལྟར་རྩིས་རྒྱག་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
</context>
</TS>
