<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>表達式為空！</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>表達式錯誤！</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>缺少左括弧！</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>數值過大！</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>缺少操作數！</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>未定義操作符！</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>除數不能為0！</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>右操作數錯誤！</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>計算機</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>標準型</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>科學型</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>換算器</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>匯率</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>標準型</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>科學型</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <source>Calculator</source>
        <translation>計算機</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1102"/>
        <location filename="../src/mainwindow.cpp" line="1337"/>
        <location filename="../src/mainwindow.cpp" line="1353"/>
        <source>standard</source>
        <translation>標準型</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1335"/>
        <source>calculator</source>
        <translation>計算機</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="268"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="269"/>
        <source>Paste</source>
        <translation>粘貼</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="923"/>
        <source>input too long</source>
        <translation>輸入過長</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="75"/>
        <location filename="../src/mainwindow.cpp" line="1337"/>
        <location filename="../src/mainwindow.cpp" line="1357"/>
        <source>scientific</source>
        <translation>科學型</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1370"/>
        <source>exchange rate</source>
        <translation>匯率</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="801"/>
        <location filename="../src/mainwindow.cpp" line="805"/>
        <source>Error!</source>
        <translation>錯誤！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="809"/>
        <location filename="../src/mainwindow.cpp" line="1552"/>
        <source>Input error!</source>
        <translation>輸入錯誤！</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>輸入過長！</translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="71"/>
        <location filename="../src/programmer/programmodel.cpp" line="236"/>
        <location filename="../src/programmer/programmodel.cpp" line="320"/>
        <source>Input error!</source>
        <translation>輸入錯誤！</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="402"/>
        <source>ShowBinary</source>
        <translation>顯示二進位</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="408"/>
        <source>HideBinary</source>
        <translation>隱藏二進位</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Calculator</source>
        <translation type="vanished">计算器</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Standard</source>
        <translation>計算機—標準型</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="292"/>
        <source>Scientific</source>
        <translation>計算機—科學型</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="58"/>
        <location filename="../src/titlebar.cpp" line="73"/>
        <location filename="../src/titlebar.cpp" line="259"/>
        <source>standard</source>
        <translation>標準型</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="59"/>
        <location filename="../src/titlebar.cpp" line="271"/>
        <source>scientific</source>
        <translation>科學型</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="293"/>
        <source>Exchange Rate</source>
        <translation>計算機—匯率</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="294"/>
        <source>Programmer</source>
        <translation>計算機—程式師型</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>StayTop</source>
        <translation>置頂</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="430"/>
        <source>Restore</source>
        <translation>還原</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="123"/>
        <location filename="../src/titlebar.cpp" line="320"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="124"/>
        <location filename="../src/titlebar.cpp" line="321"/>
        <location filename="../src/titlebar.cpp" line="421"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="125"/>
        <location filename="../src/titlebar.cpp" line="322"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="57"/>
        <location filename="../src/programmer/toolbar.cpp" line="203"/>
        <location filename="../src/programmer/toolbar.cpp" line="206"/>
        <source>ShowBinary</source>
        <translation>顯示二進位</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="204"/>
        <location filename="../src/programmer/toolbar.cpp" line="205"/>
        <source>HideBinary</source>
        <translation>隱藏二進位</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="91"/>
        <source>Rate update</source>
        <translation>匯率更新</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="106"/>
        <location filename="../src/toolmodel.cpp" line="217"/>
        <source>Chinese Yuan</source>
        <translation>人民幣</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="114"/>
        <location filename="../src/toolmodel.cpp" line="244"/>
        <source>US Dollar</source>
        <translation>美元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="211"/>
        <source>UAE Dirham</source>
        <translation>阿聯酋迪拉姆</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="211"/>
        <source>Argentinian peso</source>
        <translation>阿根廷比索</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="212"/>
        <source>Australian Dollar</source>
        <translation>澳大利亞元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="212"/>
        <source>Bulgarian Lev</source>
        <translation>保加利亞列弗</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="213"/>
        <source>Bahraini Dinar</source>
        <translation>巴林第納爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="213"/>
        <source>Brunei Dollar</source>
        <translation>汶萊元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="214"/>
        <source>Brazilian Real</source>
        <translation>巴西雷亞爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="214"/>
        <source>Bahaman Dollar</source>
        <translation>巴哈馬元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="215"/>
        <source>Botswana Pula</source>
        <translation>波札那普拉</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="215"/>
        <source>Canadian Dollar</source>
        <translation>加拿大元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="216"/>
        <source>CFA Franc</source>
        <translation>中非法郎</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="216"/>
        <source>Swiss Franc</source>
        <translation>瑞士法郎</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="217"/>
        <source>Chilean Peso</source>
        <translation>智利比索</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="218"/>
        <source>Colombian Peso</source>
        <translation>哥倫比亞比索</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="218"/>
        <source>Czech Koruna</source>
        <translation>捷克克朗</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="219"/>
        <source>Danish Krone</source>
        <translation>丹麥克朗</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="219"/>
        <source>Dominican peso</source>
        <translation>多明尼加比索</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="220"/>
        <source>Algerian Dinar</source>
        <translation>阿爾及利亞第納爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="220"/>
        <source>Estonian Kroon</source>
        <translation>愛沙尼亞克朗</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="221"/>
        <source>Egyptian pound</source>
        <translation>埃及鎊</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="221"/>
        <source>Euro</source>
        <translation>歐元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="222"/>
        <source>Fijian dollar</source>
        <translation>斐濟元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="222"/>
        <source>Pound Sterling</source>
        <translation>英鎊</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="223"/>
        <source>Guatemalan Quetzal</source>
        <translation>瓜地馬拉格查爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="223"/>
        <source>Hong Kong Dollar</source>
        <translation>港幣</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="224"/>
        <source>Croatian Kuna</source>
        <translation>克羅埃西亞庫納</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="224"/>
        <source>Hungarian Forint</source>
        <translation>匈牙利福林</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="225"/>
        <source>Indonesian Rupiah</source>
        <translation>印尼盧比</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="225"/>
        <source>Israeli New Shekel</source>
        <translation>以色列新謝克爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="226"/>
        <source>Indian Rupee</source>
        <translation>印度盧比</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="226"/>
        <source>Iranian Rial</source>
        <translation>伊朗里亞爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="227"/>
        <source>Icelandic Krona</source>
        <translation>冰島克朗</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="227"/>
        <source>Japanese Yen</source>
        <translation>日元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="228"/>
        <source>South Korean Won</source>
        <translation>韓元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="228"/>
        <source>Kuwaiti Dinar</source>
        <translation>科威特第納爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="229"/>
        <source>Kazakhstani Tenge</source>
        <translation>哈薩克斯坦騰格</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="229"/>
        <source>Sri Lankan Rupee</source>
        <translation>斯里蘭卡盧比</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="230"/>
        <source>Lithuanian Litas</source>
        <translation>立陶宛立特</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="230"/>
        <source>Latvian Lats</source>
        <translation>拉脫維亞拉特</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="231"/>
        <source>Libyan Dinar</source>
        <translation>利比亞第納爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="231"/>
        <source>Mauritian Rupee</source>
        <translation>模里西斯盧比</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="232"/>
        <source>Maldivian Rupee</source>
        <translation>瑪律地夫盧比</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="232"/>
        <source>Mexican Peso</source>
        <translation>墨西哥比索</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="233"/>
        <source>Malaysian Ringgit</source>
        <translation>馬來西亞林吉特</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="233"/>
        <source>Norwegian Krone</source>
        <translation>挪威克朗</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="234"/>
        <source>Nepalese Rupee</source>
        <translation>尼泊爾盧比</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="234"/>
        <source>New Zealand Dollar</source>
        <translation>紐西蘭元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="235"/>
        <source>Omani Rial</source>
        <translation>阿曼里亞爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="235"/>
        <source>Panamanian balbos</source>
        <translation>巴拿馬巴波亞</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="236"/>
        <source>Peruvian Nuevo Sol</source>
        <translation>秘魯新索爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="236"/>
        <source>Philippine Peso</source>
        <translation>菲律賓比索</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="237"/>
        <source>Pakistani Rupee</source>
        <translation>巴基斯坦盧比</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="237"/>
        <source>Polish Zloty</source>
        <translation>波蘭茲羅提</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="238"/>
        <source>Paraguayan Guaran</source>
        <translation>巴拉圭瓜拉尼</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="238"/>
        <source>Qatari Riyal</source>
        <translation>卡達里亞爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="239"/>
        <source>New Romanian Leu</source>
        <translation>新羅馬尼亞列伊</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="239"/>
        <source>Russian Rouble</source>
        <translation>俄羅斯盧布</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="240"/>
        <source>Saudi Riyal</source>
        <translation>沙特里亞爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="240"/>
        <source>Swedish Krona</source>
        <translation>瑞典克朗</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="241"/>
        <source>Singapore Dollar</source>
        <translation>新加坡元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="241"/>
        <source>Thai Baht</source>
        <translation>泰銖</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="242"/>
        <source>Tunisian Dinar</source>
        <translation>突尼西亞第納爾</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="242"/>
        <source>New Turkish Lira</source>
        <translation>新土爾其里拉</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="243"/>
        <source>T&amp;T Dollar (TTD)</source>
        <translation>特立尼達和多巴哥元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="243"/>
        <source>Taiwan Dollar</source>
        <translation>台幣</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="244"/>
        <source>Ukrainian Hryvnia</source>
        <translation>烏克蘭格裡夫納</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="245"/>
        <source>Uruguayan Peso</source>
        <translation>烏拉圭比索</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="245"/>
        <source>Venezuelan Bolívar</source>
        <translation>委內瑞拉玻利瓦爾省</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="246"/>
        <source>South African Rand</source>
        <translation>南非蘭特</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="509"/>
        <source>Error!</source>
        <translation>錯誤！</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>標準型</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>科學型</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>匯率</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>程序師型</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>計算機是一款基於qt5開發的輕量級計算機，提供標準計算，科學計算和匯率換算。</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>計算機</translation>
    </message>
</context>
</TS>
