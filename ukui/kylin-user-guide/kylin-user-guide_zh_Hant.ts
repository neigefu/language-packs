<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: white;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>服務與支援 ： &lt;a style=&apos;color： white;&apos; href=&apos;mailto：//support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: black;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>服務與支援 ： &lt;a style=&apos;color： black;&apos; href=&apos;mailto：//support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</translation>
    </message>
    <message>
        <source>VERSION</source>
        <translation>版本</translation>
    </message>
</context>
<context>
    <name>AndroidTextWidget</name>
    <message>
        <source>Android applications can also be used on computers</source>
        <translation>安卓應用程式也可以在計算機上使用</translation>
    </message>
    <message>
        <source>Unlimited work and entertainment</source>
        <translation>無限的工作和娛樂</translation>
    </message>
</context>
<context>
    <name>AppStoreTextWidget</name>
    <message>
        <source>New Software Store</source>
        <translation>新軟體商店</translation>
    </message>
    <message>
        <source>Clear classification and more convenient synchronization</source>
        <translation>分類清晰，同步更方便</translation>
    </message>
</context>
<context>
    <name>ApplicationTextWidget</name>
    <message>
        <source>Redesign 5 apps</source>
        <translation>重新設計 5 個應用程式</translation>
    </message>
    <message>
        <source>It is more convenient to chat, look at pictures, find documents and see the weather. You can also know your computer status at any time</source>
        <translation>聊天，看圖片，查找文檔和查看天氣更方便。您也可以隨時瞭解您的電腦狀態</translation>
    </message>
</context>
<context>
    <name>ClickSwitchTextWidget</name>
    <message>
        <source>New style and multiple modes
The visual experience is very different</source>
        <translation>新風格和多種模式
視覺體驗大不相同</translation>
    </message>
    <message>
        <source>Topic Switch
Multiple themes, free choice</source>
        <translation>主題切換
多種主題，自由選擇</translation>
    </message>
</context>
<context>
    <name>GuideWidget</name>
    <message>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>Reduction</source>
        <translation>減少</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <source>Go back</source>
        <translation>回去</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Option</source>
        <translation>選擇</translation>
    </message>
    <message>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Manual one-stop help for the use of this machine software</source>
        <translation type="vanished">用户手册提供本机软件使用的一站式帮助</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation type="obsolete">&lt;p&gt;服务与支持团队 : &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HandBookTextWidget</name>
    <message>
        <source>More new functions are here</source>
        <translation>更多新功能在這裡</translation>
    </message>
    <message>
        <source>Ok, I got it.</source>
        <translation>好的，我知道了。</translation>
    </message>
</context>
<context>
    <name>KylinIdTextWidget</name>
    <message>
        <source>Register Kirin ID
Synchronize important content</source>
        <translation>註冊麒麟身份證
同步重要內容</translation>
    </message>
    <message>
        <source>Sync your configuration and app download records</source>
        <translation>同步配置和應用下載記錄</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-new-function-introduction</source>
        <translation>麒麟-新功能-介紹</translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Introduction to new functions</source>
        <translation>新功能介紹</translation>
    </message>
</context>
<context>
    <name>WelcomeTextWidget</name>
    <message>
        <source>Control panel, more clear classification
Notification center, no more missing messages</source>
        <translation>控制面板，分類更清晰
通知中心，不再丟失消息</translation>
    </message>
    <message>
        <source>Hello，
Designed for you, new desktop</source>
        <translation>你好
專為您設計，全新桌面</translation>
    </message>
</context>
</TS>
