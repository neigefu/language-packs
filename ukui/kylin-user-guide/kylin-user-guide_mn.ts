<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: white;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>《 p 》 ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠠ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠪᠦᠯᠬᠦᠮ ᠄ 《 astyle= (᠎color: white ; shref = mailto : / / support@kylinos . cn &apos; support@kylinos . cn＜/ a〉 〈 / p 〉</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: black;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>《 p 》 ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠠ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠪᠦᠯᠬᠦᠮ ᠄ 《 astyle=color: black ; shref = mailto : / / support@kylinos . cn &apos; support@kylinos . cn＜/ a〉 〈 / p 〉</translation>
    </message>
    <message>
        <source>VERSION</source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠨᠣᠮᠧᠷ</translation>
    </message>
</context>
<context>
    <name>AndroidTextWidget</name>
    <message>
        <source>Android applications can also be used on computers</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠳᠡᠭᠡᠷ᠎ᠡ ᠴᠦ᠍ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠣᠯᠤᠨ᠎ᠠ
 ᠠᠨ ᠵᠦᠸᠧ ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ</translation>
    </message>
    <message>
        <source>Unlimited work and entertainment</source>
        <translation>ᠠᠵᠢᠯ ᠂ ᠵᠤᠭ᠎ᠠ ᠴᠡᠩᠭᠡᠯ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠬᠤ ᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>AppStoreTextWidget</name>
    <message>
        <source>New Software Store</source>
        <translation>ᠴᠦ ᠰᠢᠨ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ᠎ᠤ᠋ᠨ ᠬᠤᠳᠠᠯᠳᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Clear classification and more convenient synchronization</source>
        <translation>ᠲᠥᠷᠥᠯ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠯ ᠨᠢ ᠨᠡᠩ ᠲᠣᠳᠣᠷᠬᠠᠢ ᠂ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠯ ᠨᠢ ᠨᠡᠩ ᠳᠥᠭᠥᠮ</translation>
    </message>
</context>
<context>
    <name>ApplicationTextWidget</name>
    <message>
        <source>Redesign 5 apps</source>
        <translation>5 ᠬᠡᠷᠡᠭᠯᠡᠯᠲᠡ᠎ᠶ᠋ᠢ ᠳᠠᠬᠢᠨ ᠲᠥᠯᠥᠪᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>It is more convenient to chat, look at pictures, find documents and see the weather. You can also know your computer status at any time</source>
        <translation>ᠵᠣᠭᠣᠭᠳᠠᠬᠤ ᠂ ᠵᠢᠷᠤᠭ ᠦᠵᠡᠬᠦ ᠂ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠡᠷᠢᠬᠦ ᠂ ᠠᠭᠤᠷ ᠠᠮᠢᠰᠬᠤᠯ᠎ᠢ᠋ ᠦᠵᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠨᠡᠩ ᠳᠥᠭᠥᠮ
 ᠪᠠᠰᠠ ᠴᠠᠭ ᠢᠮᠠᠭᠲᠠ ᠣᠢᠯᠠᠭᠠᠵᠤ ᠳᠡᠢᠯᠦᠨ᠎ᠡ ᠂ ᠴᠢᠨᠦ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ᠎ᠦ᠋ᠨ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
</context>
<context>
    <name>ClickSwitchTextWidget</name>
    <message>
        <source>New style and multiple modes
The visual experience is very different</source>
        <translation>ᠪᠦᠷᠢᠨ ᠰᠢᠨ᠎ᠡ ᠬᠡᠪ ᠨᠠᠮᠪᠠ᠎ᠶ᠋ᠢᠨ ᠣᠯᠠᠨ ᠵᠦᠢᠯ᠎ᠦ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ
 ᠬᠠᠷᠠᠭᠠᠨ ᠰᠠᠷᠠᠯ᠎ᠤ᠋ᠨ ᠮᠡᠳᠡᠷᠡᠮᠵᠢ ᠶᠡᠬᠡ ᠢᠯᠭᠠᠭ᠎ᠠ᠎ᠲᠠᠢ</translation>
    </message>
    <message>
        <source>Topic Switch
Multiple themes, free choice</source>
        <translation>ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ ᠰᠣᠯᠢᠬᠤ
 ᠣᠯᠠᠨ ᠪᠠᠳᠠᠭ ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ ᠂ ᠴᠢᠯᠥᠭᠡᠲᠡᠢ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
</context>
<context>
    <name>GuideWidget</name>
    <message>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠶᠡᠭᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>Reduction</source>
        <translation>ᠠᠩᠭᠢᠵᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢᠨ ᠭᠠᠷ ᠳᠡᠪᠲᠡᠷ</translation>
    </message>
    <message>
        <source>Go back</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Option</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Manual one-stop help for the use of this machine software</source>
        <translation type="vanished">用户手册提供本机软件使用的一站式帮助</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation type="obsolete">&lt;p&gt;服务与支持团队 : &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HandBookTextWidget</name>
    <message>
        <source>More new functions are here</source>
        <translation>ᠨᠡᠩ ᠣᠯᠠᠨ ᠰᠢᠨ᠎ᠡ ᠴᠢᠳᠠᠮᠵᠢ ᠨᠢ ᠪᠦᠷ ᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Ok, I got it.</source>
        <translation>ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠪᠢ ᠮᠡᠳᠡᠯ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>KylinIdTextWidget</name>
    <message>
        <source>Register Kirin ID
Synchronize important content</source>
        <translation>ᠪᠢᠯᠢᠭᠲᠦ ᠭᠥᠷᠥᠭᠡᠰᠦ᠎ᠶ᠋ᠢᠨ ID ᠳᠠᠩᠰᠠᠯᠠᠬᠤ
 ᠴᠢᠬᠤᠯᠠ ᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Sync your configuration and app download records</source>
        <translation>ᠲᠠᠨ᠎ᠤ᠋ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠬᠠᠮᠲᠤ ᠂ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠠᠭᠤᠯᠭᠠᠯ᠎ᠲᠠᠢ ᠬᠠᠮᠲᠤ ᠶᠠᠪᠤᠭᠳᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-new-function-introduction</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Introduction to new functions</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠢᠳᠠᠮᠵᠢ᠎ᠶ᠋ᠢᠨ ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>WelcomeTextWidget</name>
    <message>
        <source>Control panel, more clear classification
Notification center, no more missing messages</source>
        <translation>ᠫᠡᠨᠧᠯ᠎ᠢ᠋ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠂ ᠲᠥᠷᠥᠯ ᠬᠤᠪᠢᠶᠠᠬᠤ ᠨᠢ ᠨᠡᠩ ᠲᠣᠳᠣᠷᠬᠠᠢ
 ᠲᠥᠪ᠎ᠲᠦ᠍ ᠮᠡᠳᠡᠭᠳᠡ ᠂ ᠴᠢᠮᠡᠭᠡ᠎ᠶ᠋ᠢ ᠳᠠᠬᠢᠵᠤ ᠠᠯᠳᠠᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Hello，
Designed for you, new desktop</source>
        <translation>ᠲᠠ ᠰᠠᠢᠨ ᠤᠤ ? ᠃
 ᠴᠢᠮ᠎ᠠ᠎ᠳ᠋ᠤ᠌ ᠲᠥᠯᠥᠪᠯᠡᠵᠦ ᠥᠭᠬᠦᠶ᠎ᠡ ᠂ ᠰᠢᠨ᠎ᠡ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
</context>
</TS>
