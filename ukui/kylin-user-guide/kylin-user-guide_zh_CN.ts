<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutWidget</name>
    <message>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: white;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>&lt;p&gt;服务与支持团队 : &lt;a style=&apos;color: white;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: black;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation>&lt;p&gt;服务与支持团队 : &lt;a style=&apos;color: black;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <source>VERSION</source>
        <translation>版本号</translation>
    </message>
</context>
<context>
    <name>AndroidTextWidget</name>
    <message>
        <source>Android applications can also be used on computers</source>
        <translation>电脑上也能使用
安卓应用</translation>
    </message>
    <message>
        <source>Unlimited work and entertainment</source>
        <translation>工作，娱乐不设限</translation>
    </message>
</context>
<context>
    <name>AppStoreTextWidget</name>
    <message>
        <source>New Software Store</source>
        <translation>全新软件商店</translation>
    </message>
    <message>
        <source>Clear classification and more convenient synchronization</source>
        <translation>分类更清晰，同步更方便</translation>
    </message>
</context>
<context>
    <name>ApplicationTextWidget</name>
    <message>
        <source>Redesign 5 apps</source>
        <translation>重新设计5个应用</translation>
    </message>
    <message>
        <source>It is more convenient to chat, look at pictures, find documents and see the weather. You can also know your computer status at any time</source>
        <translation>聊天、看图、找文件、看天气更方便
还能随时了解，你的电脑状态</translation>
    </message>
</context>
<context>
    <name>ClickSwitchTextWidget</name>
    <message>
        <source>New style and multiple modes
The visual experience is very different</source>
        <translation>全新风格 多种模式
视觉体验 大有不同</translation>
    </message>
    <message>
        <source>Topic Switch
Multiple themes, free choice</source>
        <translation>主题切换
多款主题，自由选择</translation>
    </message>
</context>
<context>
    <name>GuideWidget</name>
    <message>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>Reduction</source>
        <translation>还原</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>用户手册</translation>
    </message>
    <message>
        <source>Go back</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Option</source>
        <translation>选项</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Manual one-stop help for the use of this machine software</source>
        <translation type="vanished">用户手册提供本机软件使用的一站式帮助</translation>
    </message>
    <message>
        <source>Service &amp; Support : &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;</source>
        <translation type="obsolete">&lt;p&gt;服务与支持团队 : &lt;a style=&apos;color: #464646;&apos; href=&apos;mailto://support@kylinos.cn&apos;&gt;support@kylinos.cn&lt;/a&gt;&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>HandBookTextWidget</name>
    <message>
        <source>More new functions are here</source>
        <translation>更多新功能都在这里</translation>
    </message>
    <message>
        <source>Ok, I got it.</source>
        <translation>好的，我知道了</translation>
    </message>
</context>
<context>
    <name>KylinIdTextWidget</name>
    <message>
        <source>Register Kirin ID
Synchronize important content</source>
        <translation>注册麒麟ID
同步重要内容</translation>
    </message>
    <message>
        <source>Sync your configuration and app download records</source>
        <translation>同步你的配置，和应用下载记录</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-new-function-introduction</source>
        <translation>新功能介绍</translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Introduction to new functions</source>
        <translation>新功能介绍</translation>
    </message>
</context>
<context>
    <name>WelcomeTextWidget</name>
    <message>
        <source>Control panel, more clear classification
Notification center, no more missing messages</source>
        <translation>控制面板，分类更加清晰
通知中心，不再错过消息</translation>
    </message>
    <message>
        <source>Hello，
Designed for you, new desktop</source>
        <translation>你好，
为你设计，全新桌面</translation>
    </message>
</context>
</TS>
