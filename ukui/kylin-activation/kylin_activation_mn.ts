<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ActivateSucceedWidget</name>
    <message>
        <location filename="../activatesucceedwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../activatesucceedwidget.ui" line="220"/>
        <source>Successful Activation</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../activatesucceedwidget.ui" line="316"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ActivationWidget</name>
    <message>
        <location filename="../activation.ui" line="14"/>
        <source>Activate/Extended service</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ/ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡᠷ ᠵᠢ ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="26"/>
        <location filename="../activation.cpp" line="148"/>
        <source>serial number:</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ:</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="39"/>
        <location filename="../activation.cpp" line="150"/>
        <source>License:</source>
        <translation>ᠳᠠᠩᠰᠠᠯᠠᠭᠰᠠᠨ ᠨᠤᠮᠸᠷ:</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="78"/>
        <location filename="../activation.cpp" line="162"/>
        <location filename="../activation.cpp" line="309"/>
        <source>Code activation</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ/ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="91"/>
        <location filename="../activation.cpp" line="155"/>
        <source>Scan code acquisition</source>
        <translation>ᠰᠢᠷᠪᠢᠵᠤ ᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="104"/>
        <location filename="../activation.cpp" line="153"/>
        <source>Activation URL:</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋ ᠤ᠋ᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="196"/>
        <location filename="../activation.cpp" line="158"/>
        <source>Please fill in the activation code or insert Ukey:</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋ ᠢ᠋ ᠳᠠᠭᠯᠠᠬᠤ ᠪᠤᠶᠤ Ukey ᠢ᠋/ ᠵᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ:</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="209"/>
        <location filename="../activation.cpp" line="164"/>
        <location filename="../activation.cpp" line="406"/>
        <location filename="../activation.cpp" line="459"/>
        <source>Ukey activation</source>
        <translation>Ukey ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ/ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠤᠷᠳᠤᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="222"/>
        <location filename="../activation.cpp" line="152"/>
        <source>Customer:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠪᠠᠢᠭᠤᠯᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="235"/>
        <location filename="../activation.ui" line="248"/>
        <location filename="../activation.cpp" line="491"/>
        <location filename="../activation.cpp" line="528"/>
        <source>Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="281"/>
        <source>Qr code</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="110"/>
        <source>Dbus Error!</source>
        <translation>Dbus ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="178"/>
        <source>Has been activated, the expiration time:</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ᠂ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠤ᠋ ᠳᠠᠭᠤᠰᠬᠤ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="182"/>
        <source>Has expired, the expiration time:</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠢᠭᠡᠨᠳᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ᠂ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠤ᠋ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠳᠠᠭᠤᠰᠪᠠ:</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="185"/>
        <source>Your system is not activated, please activate!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠬᠠᠷᠠᠬᠠᠨ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ᠂ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="197"/>
        <location filename="../activation.cpp" line="215"/>
        <location filename="../activation.cpp" line="252"/>
        <source>File system test failure! (error code#</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ! ( ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋#</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="260"/>
        <location filename="../activation.cpp" line="372"/>
        <location filename="../activation.cpp" line="422"/>
        <source>Activation...</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="270"/>
        <source>Activation is empty!  </source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ!  </translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="275"/>
        <location filename="../activation.cpp" line="286"/>
        <location filename="../activation.cpp" line="555"/>
        <source>Invalid activation code</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="292"/>
        <location filename="../activation.cpp" line="390"/>
        <location filename="../activation.cpp" line="440"/>
        <location filename="../activation.cpp" line="543"/>
        <source>Activation is successful, reboot the system!</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ/ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠤᠷᠳᠤᠳᠬᠠᠪᠠ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="293"/>
        <location filename="../activation.cpp" line="391"/>
        <location filename="../activation.cpp" line="441"/>
        <location filename="../activation.cpp" line="544"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="304"/>
        <location filename="../activation.cpp" line="403"/>
        <location filename="../activation.cpp" line="455"/>
        <source>!(error code#</source>
        <translation>!(ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋#</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="379"/>
        <location filename="../activation.cpp" line="384"/>
        <location filename="../activation.cpp" line="427"/>
        <location filename="../activation.cpp" line="432"/>
        <source>Ukey not inserted or invalid Ukey</source>
        <translation>Ukey ᠢ᠋/ ᠵᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ Ukey</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="470"/>
        <location filename="../activation.cpp" line="517"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="487"/>
        <source>Serial number needs to be greater than 7 digits!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ 7 ᠤᠷᠤᠨ ᠲᠠᠢ ᠲᠤᠭ᠎ᠠ ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="498"/>
        <source>Please enter the correct serial number!</source>
        <translation>ᠵᠦᠪ ᠤᠨᠤᠪᠴᠢᠳᠠᠢ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
</context>
<context>
    <name>DeactivateWidget</name>
    <message>
        <location filename="../deactivatewidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="77"/>
        <location filename="../deactivatewidget.ui" line="314"/>
        <source>Deactivate</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="124"/>
        <source>Please enter the reason for the deactivation and click deactivation button to complete deactivation.</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠭᠰᠡᠨ ᠰᠢᠯᠳᠠᠭᠠᠨ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ᠂ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ ᠳᠠᠷᠤᠭᠤᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <source>Please enter the reason for the deactivation and click deactivation button to complete deactivation</source>
        <translation type="vanished">请输入取消原因，并点击取消激活按钮完成取消激活</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="189"/>
        <source>Reason for deactivation:</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠭᠰᠡᠨ ᠰᠢᠯᠳᠠᠭᠠᠨ:</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="289"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="54"/>
        <source>Deactivation successs!</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="58"/>
        <source>Deactivation fail!</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="62"/>
        <source>The administrator refused!</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠳᠡᠪᠴᠢᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="65"/>
        <source>Requires an administrator to confirm!</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠪᠡᠷ ᠪᠠᠳᠤᠯᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="88"/>
        <location filename="../deactivatewidget.cpp" line="143"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="142"/>
        <source>No quota, please contact the administrator!</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠭᠰᠠᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ᠂ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
</context>
<context>
    <name>DropWidget</name>
    <message>
        <location filename="../dropwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dropwidget.ui" line="155"/>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../dropwidget.ui" line="186"/>
        <source>click Add Or drag and drop the authorization file here</source>
        <translation>ᠨᠡᠮᠡᠬᠦ ᠳᠠᠷᠤᠭᠤᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ ᠪᠤᠶᠤ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠡᠨᠳᠡ ᠴᠢᠷᠴᠤ ᠠᠪᠴᠢᠷᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="vanished">选择授权文件</translation>
    </message>
    <message>
        <source>Or drag and drop the authorization file here</source>
        <translation type="vanished">或者拖拽授权文件到此处</translation>
    </message>
    <message>
        <source>Please import .kyinfo and LICENSE files</source>
        <translation type="vanished">请导入.kyinfo和LICENSE文件</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="167"/>
        <source>Invalid filename: </source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="167"/>
        <source>, please import kyinfo and LICENSE text files.</source>
        <translation>, kyinfo ᠪᠤᠯᠤᠨ LICENSE ᠤ᠋ᠨ/ ᠵᠢᠨ ᠲᠸᠺᠰᠲ ᠹᠠᠢᠯ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="168"/>
        <location filename="../dropwidget.cpp" line="217"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="216"/>
        <source>Please import kyinfo and LICENSE text files.</source>
        <translation>kyinfo ᠪᠤᠯᠤᠨ LICENSE ᠤ᠋ᠨ/ ᠵᠢᠨ ᠲᠸᠺᠰᠲ ᠹᠠᠢᠯ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <source>, please import kyinfo and LICENSE text files</source>
        <translation type="vanished">，请导入.kyinfo和LICENSE文本文件</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="203"/>
        <source>Existing similar filename: </source>
        <translation>ᠠᠳᠠᠯᠢᠪᠳᠤᠷ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ: </translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="203"/>
        <source>, whether to overwrite?</source>
        <translation>, ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Please import kyinfo and LICENSE text files</source>
        <translation type="vanished">请导入.kyinfo和LICENSE文本文件</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
</context>
<context>
    <name>FileRepeatDialog</name>
    <message>
        <source>File repeat</source>
        <translation type="vanished">文件重复</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="vanished">覆盖</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>&quot;</source>
        <translation type="vanished">“</translation>
    </message>
    <message>
        <source>&quot; file already exists, whether to overwrite?</source>
        <translation type="vanished">”文件已存在，是否覆盖？</translation>
    </message>
</context>
<context>
    <name>ImportLicenseFileWidget</name>
    <message>
        <location filename="../importlicensefilewidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="74"/>
        <location filename="../importlicensefilewidget.cpp" line="225"/>
        <source>Import license file</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="121"/>
        <source>Please select the authorization file and click the import button to complete the import.</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠵᠤ᠂ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="529"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="557"/>
        <location filename="../importlicensefilewidget.cpp" line="226"/>
        <source>import</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="138"/>
        <location filename="../importlicensefilewidget.cpp" line="314"/>
        <source>!(error code#</source>
        <translation>!(ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋#</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="139"/>
        <location filename="../importlicensefilewidget.cpp" line="161"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="160"/>
        <source>Not site license file!</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠨᠢ ᠳᠠᠯᠠᠪᠠᠢ ᠵᠢᠨ ᠪᠤᠰᠤ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ!</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="217"/>
        <source>Please select the authorization file you want to import, and then click the Activate button.</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠵᠤ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="218"/>
        <source>Import site license file</source>
        <translation>ᠳᠠᠯᠠᠪᠠᠢ ᠵᠢᠨ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="219"/>
        <source>Activate</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="224"/>
        <source>Please select the authorization file you want to import, and then click the import button.</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠵᠤ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="316"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠲᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Imported successfully!</source>
        <translation type="vanished">导入成功！</translation>
    </message>
    <message>
        <source>The system architecture information does not match. Import it again!</source>
        <translation type="vanished">系统架构信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The system version information does not match. Import it again!</source>
        <translation type="vanished">系统版本信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>Imported successfully, reboot the system!</source>
        <translation type="vanished">导入成功，请重启系统！</translation>
    </message>
    <message>
        <source>The authorization files is abnormal, please re-import!</source>
        <translation type="vanished">授权文件异常，请重新导入！</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="220"/>
        <source>Tips</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="220"/>
        <source>It is currently in livecd mode, please activate it after installing the system!</source>
        <translation>ᠲᠡᠷᠡ ᠣᠳᠣᠬᠠᠨ ᠳ᠋ᠤ᠌ livecd ᠵᠠᠭᠪᠤᠷ ᠪᠠᠢᠨ᠎ᠠ᠂ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠨ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>QrCodeActivationWidget</name>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation>form</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="77"/>
        <source>Sweep the code to activate</source>
        <translation>ᠺᠤᠳ᠋ ᠰᠢᠷᠪᠢᠵᠤ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Connection timed out, please click retry button to activate.</source>
        <translation type="vanished">连接超时，请点击“重试”按钮完成激活。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="402"/>
        <source>Serial number:</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ:</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="519"/>
        <source>Activation code:</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋:</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="694"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="719"/>
        <source>Activate</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="34"/>
        <source>Activating in progress</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="86"/>
        <source>Please scan the qr code below, and according to the prompt to complete the activation operation on mobile devices.</source>
        <translation>ᠳᠣᠣᠷᠠᠬᠢ ᠬᠣᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠢᠷᠪᠢᠵᠤ᠂ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷ᠎ᠡ ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠵᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="107"/>
        <source>Unable to connect to the network, please scan the QR code below, enter the obtained serial number and activation code to activate.</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠳᠤᠤᠷᠠᠬᠢ ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠢᠷᠪᠢᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ ᠪᠤᠯᠤᠨ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="135"/>
        <source>Activation timed out! Please complete the activation on the mobile device, and then click retry button.</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠯᠳᠡ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ! ᠰᠢᠯᠵᠢᠮᠡᠯ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷ᠎ᠡ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠵᠤ ᠳᠠᠭᠤᠰᠬᠠᠭᠠᠷᠠᠢ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠠᠰᠢᠬᠤ ᠳᠠᠷᠤᠭᠤᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="293"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>Please scan the qr code below and follow the instructions on your mobile device.</source>
        <translation type="vanished">请扫描下方二维码，在移动设备上根据提示操作。</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">重试</translation>
    </message>
    <message>
        <source>Please scan the qr code below, confirm activation on mobile devices, and click the &quot;activate&quot; button to complete the activation.</source>
        <translation type="vanished">请扫描下方二维码，在移动设备上确认激活，并点击“激活”按钮完成激活。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="233"/>
        <location filename="../qrcodeactivationwidget.cpp" line="241"/>
        <source>Invalid activation code</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Activation is successful, reboot the system!</source>
        <translation type="vanished">激活/延长服务成功,请重启系统！</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="251"/>
        <source>!(error code#</source>
        <translation>!(ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋#</translation>
    </message>
    <message>
        <source>Please confirm activation on your mobile device!</source>
        <translation type="vanished">请在移动设备上确认激活！</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="292"/>
        <source>Invalid serial number!</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ!</translation>
    </message>
    <message>
        <source>The system has been activated!</source>
        <translation type="vanished">系统已经激活!</translation>
    </message>
</context>
<context>
    <name>SecretKeyActivationWidget</name>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="35"/>
        <source>Form</source>
        <translation>form</translation>
    </message>
    <message>
        <source>Enter the product secret key</source>
        <translation type="vanished">输入产品秘钥</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="77"/>
        <source>Enter the product secret key to acitvate</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="127"/>
        <source>The product secret key usually comes in the accompanying DVD box and consists of 20 digits and capital letters.</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠨᠢ ᠶᠡᠷᠦᠳᠡᠭᠡᠨ ᠢᠵᠢ DVD ᠪᠤᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠬᠠᠢᠷᠴᠠᠭ ᠲᠤ᠌ ᠪᠠᠢᠳᠠᠭ᠂20 ᠤᠷᠤᠨ ᠲᠠᠢ ᠲᠤᠭ᠎ᠠ ᠪᠤᠯᠤᠨ ᠲᠤᠮᠤ ᠪᠢᠴᠢᠯᠭᠡ ᠵᠢᠨ ᠠᠪᠢᠶ᠎ᠠ ᠪᠡᠷ ᠪᠦᠷᠢᠯᠳᠦᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="304"/>
        <source>Activating in progress</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="425"/>
        <source>Activate</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Activation</source>
        <translation type="vanished">激活</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="397"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="119"/>
        <source>Invalid activation code</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Activation is successful, reboot the system!</source>
        <translation type="vanished">激活/延长服务成功,请重启系统！</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="127"/>
        <source>!(error code#</source>
        <translation>!(ᠪᠤᠷᠤᠭᠤ ᠺᠤᠳ᠋#</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="176"/>
        <source>The request parameters are incomplete</source>
        <translation>ᠭᠤᠶᠤᠴᠢᠯᠠᠭᠰᠠᠨ ᠫᠠᠷᠠᠮᠸᠲᠷ ᠪᠦᠷᠢᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="179"/>
        <source>The service serial number is wrong</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="182"/>
        <source>The product key and system version do not match</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠬᠢᠬᠡᠳ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠡᠪᠯᠡᠯ ᠠᠪᠴᠠᠯᠳᠤᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="185"/>
        <source>The product key has been used</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="188"/>
        <source>Incorrect product key input</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="191"/>
        <source>Hardware code parsing failed</source>
        <translation>ᠬᠠᠷᠳ᠋ᠸᠠᠢᠷ ᠤ᠋ᠨ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠳᠠᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="194"/>
        <source>Server error</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="209"/>
        <location filename="../secretkeyactivationwidget.cpp" line="232"/>
        <source>Close</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="226"/>
        <source>Please enter 20-digit product key</source>
        <translation>20 ᠤᠷᠤᠨ ᠲᠠᠢ ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>no activation record</source>
        <translation type="vanished">无激活记录</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="202"/>
        <source>Unable to connect to the network</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="221"/>
        <source>Product key is not correct</source>
        <translation>ᠦᠢᠯᠡᠳᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ ᠵᠥᠪ ᠪᠢᠰᠢ</translation>
    </message>
</context>
<context>
    <name>SecretKeyOfflineActivationWidget</name>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation>form</translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="202"/>
        <source>Offline activation</source>
        <translation>offline activation</translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="249"/>
        <source>Please scan the QR code below and enter the captcha displayed by the mobile device.</source>
        <translation>please scan the QR code below and enter the captcha displayed by the mobile device.</translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="458"/>
        <source>Serial number (8 bits):</source>
        <translation>serial number (8 bits):</translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="598"/>
        <source>Activation</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="623"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>SystemActivationMainWindow</name>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="35"/>
        <source>System activation</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="312"/>
        <source>Select activation mode</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="392"/>
        <source>Product secret key</source>
        <translation>ᠪᠦᠳᠦᠬᠡᠭᠳᠡᠬᠦᠨ ᠤ᠋ ᠨᠢᠭᠤᠴᠠ ᠳᠦᠯᠬᠢᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="552"/>
        <location filename="../systemactivationmainwindow.cpp" line="172"/>
        <source>Scan QR code to avtivate</source>
        <translation>ᠺᠤᠳ᠋ ᠰᠢᠷᠪᠢᠵᠦ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="632"/>
        <source>Site authorization</source>
        <translation>ᠳᠠᠯᠠᠪᠠᠢ ᠵᠢᠨ ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="472"/>
        <source>UKEY</source>
        <translation>ukey</translation>
    </message>
    <message>
        <source>Qr code</source>
        <translation type="vanished">二维码</translation>
    </message>
    <message>
        <source>Import license file</source>
        <translation type="vanished">授权文件导入</translation>
    </message>
    <message>
        <source>ukey</source>
        <translation type="vanished">ukey</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="46"/>
        <source>Deactivate</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <source>tips</source>
        <translation type="vanished">ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="27"/>
        <source>Dbus Error!</source>
        <translation>Dbus ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="47"/>
        <source>Modifying license file</source>
        <translation>ᠡᠷᠬᠡ ᠤᠯᠭᠤᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>!(error code#</source>
        <translation type="vanished">！(错误码#</translation>
    </message>
    <message>
        <source>Please import it again!</source>
        <translation type="vanished">请重新导入！</translation>
    </message>
    <message>
        <source>The authorization files fail to be detected. Import it again!</source>
        <translation type="vanished">授权文件检测失败，请重新导入！</translation>
    </message>
    <message>
        <source>The system architecture information does not match. Import it again!</source>
        <translation type="vanished">系统架构信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The system version information does not match. Import it again!</source>
        <translation type="vanished">系统版本信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The authorization files test failed!</source>
        <translation type="vanished">授权文件检测失败!</translation>
    </message>
    <message>
        <source>Authorization files do not match system version!</source>
        <translation type="vanished">授权文件与系统版本不匹配!</translation>
    </message>
    <message>
        <source>The authorization files is abnormal, please re-import!</source>
        <translation type="vanished">授权文件异常，请重新导入！</translation>
    </message>
    <message>
        <source>Invalid activation code</source>
        <translation type="vanished">无效激活码</translation>
    </message>
</context>
<context>
    <name>UkeyActivationWidget</name>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation>form</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="74"/>
        <source>Ukey activation</source>
        <translation>Ukey ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="290"/>
        <source>Activating in progress</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="402"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">ᠳᠠᠬᠢᠨ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="21"/>
        <source>Ukey not recognized, please insert Ukey to activate.</source>
        <translation>Ukey ᠢ᠋/ ᠵᠢ ᠳᠠᠨᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ Ukey ᠢ᠋/ ᠵᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠵᠤ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="26"/>
        <location filename="../ukeyactivationwidget.cpp" line="30"/>
        <source>Ukey not inserted or invalid Ukey</source>
        <translation>Ukey ᠢ᠋/ ᠵᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ Ukey</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="35"/>
        <source>Ukey activated successfully!</source>
        <translation>Ukep ᠢ᠋/ ᠵᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="427"/>
        <source>Activate</source>
        <translation>ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="177"/>
        <source>please insert Ukey to activate.</source>
        <translation>Ukey ᠢ᠋/ ᠵᠢ ᠬᠠᠪᠴᠢᠭᠤᠯᠵᠤ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <source>Ukey detection!</source>
        <translation type="vanished">Ukey检测!</translation>
    </message>
</context>
</TS>
