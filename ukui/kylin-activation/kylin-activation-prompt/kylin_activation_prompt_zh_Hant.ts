<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="64"/>
        <source>No longer prompt</source>
        <translation>不再提示</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Activate now</source>
        <translation>立即啟動</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <location filename="main.cpp" line="71"/>
        <source>System activation</source>
        <translation>系統啟動</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>The current system is not activated, please activate the system first!</source>
        <translation>當前系統未啟動，請先激活系統！</translation>
    </message>
</context>
</TS>
