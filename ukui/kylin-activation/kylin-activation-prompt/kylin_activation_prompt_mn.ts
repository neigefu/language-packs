<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="226"/>
        <source>No longer prompt</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="main.cpp" line="236"/>
        <source>Activate now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="main.cpp" line="238"/>
        <location filename="main.cpp" line="241"/>
        <source>System activation</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="main.cpp" line="242"/>
        <source>The current system is not activated, please activate the system first!</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ᠂ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠦᠬᠡᠷᠡᠢ!</translation>
    </message>
</context>
</TS>
