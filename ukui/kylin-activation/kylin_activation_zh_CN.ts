<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ActivateSucceedWidget</name>
    <message>
        <location filename="../activatesucceedwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../activatesucceedwidget.ui" line="220"/>
        <source>Successful Activation</source>
        <translation>激活成功</translation>
    </message>
    <message>
        <location filename="../activatesucceedwidget.ui" line="316"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>ActivationWidget</name>
    <message>
        <location filename="../activation.ui" line="14"/>
        <source>Activate/Extended service</source>
        <translation>激活/延长服务</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="26"/>
        <location filename="../activation.cpp" line="148"/>
        <source>serial number:</source>
        <translation>序列号：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="39"/>
        <location filename="../activation.cpp" line="150"/>
        <source>License:</source>
        <translation>注册码：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="78"/>
        <location filename="../activation.cpp" line="162"/>
        <location filename="../activation.cpp" line="307"/>
        <source>Code activation</source>
        <translation>激活/延长服务</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="91"/>
        <location filename="../activation.cpp" line="155"/>
        <source>Scan code acquisition</source>
        <translation>扫码获取</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="104"/>
        <location filename="../activation.cpp" line="153"/>
        <source>Activation URL:</source>
        <translation>激活码网址：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="196"/>
        <location filename="../activation.cpp" line="158"/>
        <source>Please fill in the activation code or insert Ukey:</source>
        <translation>请填写激活码或插入Ukey后进行激活：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="209"/>
        <location filename="../activation.cpp" line="164"/>
        <location filename="../activation.cpp" line="402"/>
        <location filename="../activation.cpp" line="453"/>
        <source>Ukey activation</source>
        <translation>Ukey激活/延长服务</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="222"/>
        <location filename="../activation.cpp" line="152"/>
        <source>Customer:</source>
        <translation>客户单位：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="235"/>
        <location filename="../activation.ui" line="248"/>
        <location filename="../activation.cpp" line="485"/>
        <location filename="../activation.cpp" line="522"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="281"/>
        <source>Qr code</source>
        <translation>二维码</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="110"/>
        <location filename="../activation.cpp" line="270"/>
        <location filename="../activation.cpp" line="292"/>
        <location filename="../activation.cpp" line="302"/>
        <location filename="../activation.cpp" line="388"/>
        <location filename="../activation.cpp" line="399"/>
        <location filename="../activation.cpp" line="436"/>
        <location filename="../activation.cpp" line="449"/>
        <location filename="../activation.cpp" line="481"/>
        <location filename="../activation.cpp" line="492"/>
        <location filename="../activation.cpp" line="537"/>
        <location filename="../activation.cpp" line="547"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="110"/>
        <source>Dbus Error!</source>
        <translation>Dbus 异常！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="178"/>
        <source>Has been activated, the expiration time:</source>
        <translation>您的系统已激活,技术服务截止日期：</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="182"/>
        <source>Has expired, the expiration time:</source>
        <translation>您的系统已激活，技术服务已到期：</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="185"/>
        <source>Your system is not activated, please activate!</source>
        <translation>您的系统还未激活，请激活！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="197"/>
        <location filename="../activation.cpp" line="215"/>
        <location filename="../activation.cpp" line="252"/>
        <source>File system test failure! (error code#</source>
        <translation>系统文件检测失败！(错误码#</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="260"/>
        <location filename="../activation.cpp" line="370"/>
        <location filename="../activation.cpp" line="418"/>
        <source>Activation...</source>
        <translation>激活中...</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="270"/>
        <source>Activation is empty!  </source>
        <translation>激活码为空！  </translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="275"/>
        <location filename="../activation.cpp" line="286"/>
        <location filename="../activation.cpp" line="547"/>
        <source>Invalid activation code</source>
        <translation>无效激活码</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="292"/>
        <location filename="../activation.cpp" line="388"/>
        <location filename="../activation.cpp" line="436"/>
        <location filename="../activation.cpp" line="537"/>
        <source>Activation is successful, reboot the system!</source>
        <translation>激活/延长服务成功,请重启系统！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="302"/>
        <location filename="../activation.cpp" line="399"/>
        <location filename="../activation.cpp" line="449"/>
        <source>!(error code#</source>
        <translation>！(错误码#</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="377"/>
        <location filename="../activation.cpp" line="382"/>
        <location filename="../activation.cpp" line="423"/>
        <location filename="../activation.cpp" line="428"/>
        <source>Ukey not inserted or invalid Ukey</source>
        <translation>未插入Ukey或无效Ukey</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="464"/>
        <location filename="../activation.cpp" line="511"/>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="481"/>
        <source>Serial number needs to be greater than 7 digits!</source>
        <translation>序列号需要大于7位数！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="492"/>
        <source>Please enter the correct serial number!</source>
        <translation> 请输入正确的序列号！</translation>
    </message>
</context>
<context>
    <name>DeactivateWidget</name>
    <message>
        <location filename="../deactivatewidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="77"/>
        <location filename="../deactivatewidget.ui" line="314"/>
        <source>Deactivate</source>
        <translation>取消激活</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="124"/>
        <source>Please enter the reason for the deactivation and click deactivation button to complete deactivation.</source>
        <translation>请输入取消原因，并点击取消激活按钮完成取消激活。</translation>
    </message>
    <message>
        <source>Please enter the reason for the deactivation and click deactivation button to complete deactivation</source>
        <translation type="vanished">请输入取消原因，并点击取消激活按钮完成取消激活</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="189"/>
        <source>Reason for deactivation:</source>
        <translation>取消激活原因：</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="289"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="80"/>
        <location filename="../deactivatewidget.cpp" line="110"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="50"/>
        <source>Deactivation successs!</source>
        <translation>取消激活成功！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="54"/>
        <source>Deactivation fail!</source>
        <translation>取消激活失败！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="58"/>
        <source>The administrator refused!</source>
        <translation>管理员拒绝！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="61"/>
        <source>Requires an administrator to confirm!</source>
        <translation>需要管理员确认！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="110"/>
        <source>No quota, please contact the administrator!</source>
        <translation>配额已用完，请联系管理员！</translation>
    </message>
</context>
<context>
    <name>DropWidget</name>
    <message>
        <location filename="../dropwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dropwidget.ui" line="155"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../dropwidget.ui" line="186"/>
        <source>click Add Or drag and drop the authorization file here</source>
        <translation>点击“添加”或拖拽授权文件到此</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="vanished">选择授权文件</translation>
    </message>
    <message>
        <source>Or drag and drop the authorization file here</source>
        <translation type="vanished">或者拖拽授权文件到此处</translation>
    </message>
    <message>
        <source>Please import .kyinfo and LICENSE files</source>
        <translation type="vanished">请导入.kyinfo和LICENSE文件</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="166"/>
        <location filename="../dropwidget.cpp" line="213"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="166"/>
        <source>Invalid filename: </source>
        <translation>无效文件名：</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="166"/>
        <source>, please import kyinfo and LICENSE text files.</source>
        <translation>，请导入kyinfo和LICENSE文本文件。</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="213"/>
        <source>Please import kyinfo and LICENSE text files.</source>
        <translation>请导入kyinfo和LICENSE文本文件。</translation>
    </message>
    <message>
        <source>, please import kyinfo and LICENSE text files</source>
        <translation type="vanished">，请导入.kyinfo和LICENSE文本文件</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="200"/>
        <source>Existing similar filename: </source>
        <translation>存在相似文件名：</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="200"/>
        <source>, whether to overwrite?</source>
        <translation>，是否覆盖？</translation>
    </message>
    <message>
        <source>Please import kyinfo and LICENSE text files</source>
        <translation type="vanished">请导入.kyinfo和LICENSE文本文件</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
</context>
<context>
    <name>FileRepeatDialog</name>
    <message>
        <source>File repeat</source>
        <translation type="vanished">文件重复</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="vanished">覆盖</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>&quot;</source>
        <translation type="vanished">“</translation>
    </message>
    <message>
        <source>&quot; file already exists, whether to overwrite?</source>
        <translation type="vanished">”文件已存在，是否覆盖？</translation>
    </message>
</context>
<context>
    <name>ImportLicenseFileWidget</name>
    <message>
        <location filename="../importlicensefilewidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="74"/>
        <location filename="../importlicensefilewidget.cpp" line="219"/>
        <source>Import license file</source>
        <translation>授权文件导入</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="121"/>
        <source>Please select the authorization file and click the import button to complete the import.</source>
        <translation>请选择授权文件，并点击导入按钮完成导入。</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="529"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="554"/>
        <location filename="../importlicensefilewidget.cpp" line="220"/>
        <source>import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="137"/>
        <location filename="../importlicensefilewidget.cpp" line="156"/>
        <location filename="../importlicensefilewidget.cpp" line="301"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="137"/>
        <location filename="../importlicensefilewidget.cpp" line="301"/>
        <source>!(error code#</source>
        <translation>！(错误码#</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="156"/>
        <source>Not site license file!</source>
        <translation>导入文件非场地授权文件!</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="211"/>
        <source>Please select the authorization file you want to import, and then click the Activate button.</source>
        <translation>选择需要导入的授权文件，然后点击“激活”。</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="212"/>
        <source>Import site license file</source>
        <translation>导入场地授权文件</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="213"/>
        <source>Activate</source>
        <translation>激活</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="218"/>
        <source>Please select the authorization file you want to import, and then click the import button.</source>
        <translation>选择需要导入的授权文件，然后点击“导入”。</translation>
    </message>
    <message>
        <source>Imported successfully!</source>
        <translation type="vanished">导入成功！</translation>
    </message>
    <message>
        <source>The system architecture information does not match. Import it again!</source>
        <translation type="vanished">系统架构信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The system version information does not match. Import it again!</source>
        <translation type="vanished">系统版本信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>Imported successfully, reboot the system!</source>
        <translation type="vanished">导入成功，请重启系统！</translation>
    </message>
    <message>
        <source>The authorization files is abnormal, please re-import!</source>
        <translation type="vanished">授权文件异常，请重新导入！</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="79"/>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="79"/>
        <source>It is currently in livecd mode, please activate it after installing the system!</source>
        <translation> 当前为livecd模式，请安装完系统后再进行激活！ </translation>
    </message>
</context>
<context>
    <name>QrCodeActivationWidget</name>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="77"/>
        <source>Sweep the code to activate</source>
        <translation>扫码激活</translation>
    </message>
    <message>
        <source>Connection timed out, please click retry button to activate.</source>
        <translation type="vanished">连接超时，请点击“重试”按钮完成激活。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="376"/>
        <source>Serial number:</source>
        <translation>序列号:</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="493"/>
        <source>Activation code:</source>
        <translation>激活码：</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="668"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="693"/>
        <source>Activate</source>
        <translation>激活</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="34"/>
        <source>Activating in progress</source>
        <translation>正在激活</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="110"/>
        <source>Please scan the qr code below, and according to the prompt to complete the activation operation on mobile devices.</source>
        <translation>请扫描下方二维码，并在移动设备上根据提示完成激活操作。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="136"/>
        <source>Unable to connect to the network, please scan the QR code below, enter the obtained serial number and activation code to activate.</source>
        <translation>无法连接网络，请扫描下方二维码，输入获取的序列号和激活码激活。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="165"/>
        <source>Activation timed out! Please complete the activation on the mobile device, and then click retry button.</source>
        <translation>激活超时！请在移动设备上完成激活，然后点击重试按钮。</translation>
    </message>
    <message>
        <source>Please scan the qr code below and follow the instructions on your mobile device.</source>
        <translation type="vanished">请扫描下方二维码，在移动设备上根据提示操作。</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">重试</translation>
    </message>
    <message>
        <source>Please scan the qr code below, confirm activation on mobile devices, and click the &quot;activate&quot; button to complete the activation.</source>
        <translation type="vanished">请扫描下方二维码，在移动设备上确认激活，并点击“激活”按钮完成激活。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="259"/>
        <location filename="../qrcodeactivationwidget.cpp" line="270"/>
        <source>Invalid activation code</source>
        <translation>无效激活码</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="285"/>
        <location filename="../qrcodeactivationwidget.cpp" line="325"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Activation is successful, reboot the system!</source>
        <translation type="vanished">激活/延长服务成功,请重启系统！</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="285"/>
        <source>!(error code#</source>
        <translation>！(错误码#</translation>
    </message>
    <message>
        <source>Please confirm activation on your mobile device!</source>
        <translation type="vanished">请在移动设备上确认激活！</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="325"/>
        <source>Invalid serial number!</source>
        <translation>无效序列号!</translation>
    </message>
    <message>
        <source>The system has been activated!</source>
        <translation type="vanished">系统已经激活!</translation>
    </message>
</context>
<context>
    <name>SecretKeyActivationWidget</name>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Enter the product secret key</source>
        <translation type="vanished">输入产品秘钥</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="77"/>
        <source>Enter the product secret key to acitvate</source>
        <translation>输入产品密钥激活</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="127"/>
        <source>The product secret key usually comes in the accompanying DVD box and consists of 20 digits and capital letters.</source>
        <translation>产品秘钥通常在配套DVD包装盒中，由20位数字和大写字母组成。</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="304"/>
        <source>Activating in progress</source>
        <translation>正在激活</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="422"/>
        <source>Activate</source>
        <translation>激活</translation>
    </message>
    <message>
        <source>Activation</source>
        <translation type="vanished">激活</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="397"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="123"/>
        <source>Invalid activation code</source>
        <translation>无效激活码</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="135"/>
        <location filename="../secretkeyactivationwidget.cpp" line="216"/>
        <location filename="../secretkeyactivationwidget.cpp" line="237"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Activation is successful, reboot the system!</source>
        <translation type="vanished">激活/延长服务成功,请重启系统！</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="135"/>
        <source>!(error code#</source>
        <translation>！(错误码#</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="184"/>
        <source>The request parameters are incomplete</source>
        <translation>请求参数不完整</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="187"/>
        <source>The service serial number is wrong</source>
        <translation>服务序列号错误</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="190"/>
        <source>The product key and system version do not match</source>
        <translation>产品密钥和系统版本不匹配</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="193"/>
        <source>The product key has been used</source>
        <translation>产品密钥已被使用</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="196"/>
        <source>Incorrect product key input</source>
        <translation>产品密钥输入错误</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="199"/>
        <source>Hardware code parsing failed</source>
        <translation>硬件编码解析失败</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="202"/>
        <source>Server error</source>
        <translation>服务器错误</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="232"/>
        <source>Please enter 20-digit product key</source>
        <translation>请输入20位产品秘钥</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>no activation record</source>
        <translation type="vanished">无激活记录</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="210"/>
        <source>Unable to connect to the network</source>
        <translation>无法连接网络</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="227"/>
        <source>Product key is not correct</source>
        <translation>产品密钥不正确</translation>
    </message>
</context>
<context>
    <name>SecretKeyOfflineActivationWidget</name>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="202"/>
        <source>Offline activation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="249"/>
        <source>Please scan the QR code below and enter the captcha displayed by the mobile device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="458"/>
        <source>Serial number (8 bits):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="598"/>
        <source>Activation</source>
        <translation>激活</translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="623"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>SystemActivationMainWindow</name>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="35"/>
        <source>System activation</source>
        <translation>系统激活</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="312"/>
        <source>Select activation mode</source>
        <translation>选择激活方式</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="392"/>
        <source>Product secret key</source>
        <translation>产品密钥</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="472"/>
        <source>Scan QR code to avtivate</source>
        <translation>扫码激活</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="552"/>
        <source>Site authorization</source>
        <translation>场地授权</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="632"/>
        <source>UKEY</source>
        <translation></translation>
    </message>
    <message>
        <source>Qr code</source>
        <translation type="vanished">二维码</translation>
    </message>
    <message>
        <source>Import license file</source>
        <translation type="vanished">授权文件导入</translation>
    </message>
    <message>
        <source>ukey</source>
        <translation type="vanished">ukey</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="45"/>
        <source>Deactivate</source>
        <translation>取消激活</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="28"/>
        <source>tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="28"/>
        <source>Dbus Error!</source>
        <translation>Dbus 异常！</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="46"/>
        <source>Modifying license file</source>
        <translation>修改授权文件</translation>
    </message>
    <message>
        <source>!(error code#</source>
        <translation type="vanished">！(错误码#</translation>
    </message>
    <message>
        <source>Please import it again!</source>
        <translation type="vanished">请重新导入！</translation>
    </message>
    <message>
        <source>The authorization files fail to be detected. Import it again!</source>
        <translation type="vanished">授权文件检测失败，请重新导入！</translation>
    </message>
    <message>
        <source>The system architecture information does not match. Import it again!</source>
        <translation type="vanished">系统架构信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The system version information does not match. Import it again!</source>
        <translation type="vanished">系统版本信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The authorization files test failed!</source>
        <translation type="vanished">授权文件检测失败!</translation>
    </message>
    <message>
        <source>Authorization files do not match system version!</source>
        <translation type="vanished">授权文件与系统版本不匹配!</translation>
    </message>
    <message>
        <source>The authorization files is abnormal, please re-import!</source>
        <translation type="vanished">授权文件异常，请重新导入！</translation>
    </message>
    <message>
        <source>Invalid activation code</source>
        <translation type="vanished">无效激活码</translation>
    </message>
</context>
<context>
    <name>UkeyActivationWidget</name>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="74"/>
        <source>Ukey activation</source>
        <translation>Ukey激活</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="290"/>
        <source>Activating in progress</source>
        <translation>正在激活</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="402"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="143"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="99"/>
        <source>Ukey not recognized, please insert Ukey to activate.</source>
        <translation>未识别到Ukey，请插入Ukey进行激活。</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="105"/>
        <location filename="../ukeyactivationwidget.cpp" line="110"/>
        <source>Ukey not inserted or invalid Ukey</source>
        <translation>未插入Ukey或无效Ukey</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="121"/>
        <source>Ukey activated successfully!</source>
        <translation>Ukey激活成功！</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="427"/>
        <location filename="../ukeyactivationwidget.cpp" line="195"/>
        <source>Activate</source>
        <translation>激活</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="208"/>
        <source>please insert Ukey to activate.</source>
        <translation>请插入Ukey进行激活。</translation>
    </message>
    <message>
        <source>Ukey detection!</source>
        <translation type="vanished">Ukey检测!</translation>
    </message>
</context>
</TS>
