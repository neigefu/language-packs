<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>ActivateSucceedWidget</name>
    <message>
        <location filename="../activatesucceedwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../activatesucceedwidget.ui" line="220"/>
        <source>Successful Activation</source>
        <translation>成功啟動</translation>
    </message>
    <message>
        <location filename="../activatesucceedwidget.ui" line="316"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>ActivationWidget</name>
    <message>
        <location filename="../activation.ui" line="14"/>
        <source>Activate/Extended service</source>
        <translation>啟動/擴展服務</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="26"/>
        <location filename="../activation.cpp" line="148"/>
        <source>serial number:</source>
        <translation>序號：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="39"/>
        <location filename="../activation.cpp" line="150"/>
        <source>License:</source>
        <translation>許可證：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="78"/>
        <location filename="../activation.cpp" line="162"/>
        <location filename="../activation.cpp" line="307"/>
        <source>Code activation</source>
        <translation>代碼啟動</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="91"/>
        <location filename="../activation.cpp" line="155"/>
        <source>Scan code acquisition</source>
        <translation>掃碼採集</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="104"/>
        <location filename="../activation.cpp" line="153"/>
        <source>Activation URL:</source>
        <translation>啟動網址：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="196"/>
        <location filename="../activation.cpp" line="158"/>
        <source>Please fill in the activation code or insert Ukey:</source>
        <translation>請填寫啟動碼或插入Ukey：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="209"/>
        <location filename="../activation.cpp" line="164"/>
        <location filename="../activation.cpp" line="402"/>
        <location filename="../activation.cpp" line="453"/>
        <source>Ukey activation</source>
        <translation>Ukey 啟動</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="222"/>
        <location filename="../activation.cpp" line="152"/>
        <source>Customer:</source>
        <translation>客戶：</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="235"/>
        <location filename="../activation.ui" line="248"/>
        <location filename="../activation.cpp" line="485"/>
        <location filename="../activation.cpp" line="522"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../activation.ui" line="281"/>
        <source>Qr code</source>
        <translation>二維碼</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="110"/>
        <location filename="../activation.cpp" line="270"/>
        <location filename="../activation.cpp" line="292"/>
        <location filename="../activation.cpp" line="302"/>
        <location filename="../activation.cpp" line="388"/>
        <location filename="../activation.cpp" line="399"/>
        <location filename="../activation.cpp" line="436"/>
        <location filename="../activation.cpp" line="449"/>
        <location filename="../activation.cpp" line="481"/>
        <location filename="../activation.cpp" line="492"/>
        <location filename="../activation.cpp" line="537"/>
        <location filename="../activation.cpp" line="547"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="110"/>
        <source>Dbus Error!</source>
        <translation>dbus 錯誤！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="178"/>
        <source>Has been activated, the expiration time:</source>
        <translation>已啟動，到期時間：</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="182"/>
        <source>Has expired, the expiration time:</source>
        <translation>已過期，過期時間：</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="185"/>
        <source>Your system is not activated, please activate!</source>
        <translation>您的系統未啟動，請啟動！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="197"/>
        <location filename="../activation.cpp" line="215"/>
        <location filename="../activation.cpp" line="252"/>
        <source>File system test failure! (error code#</source>
        <translation>檔案系統測試失敗！（錯誤代碼#</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="260"/>
        <location filename="../activation.cpp" line="370"/>
        <location filename="../activation.cpp" line="418"/>
        <source>Activation...</source>
        <translation>啟動。。。</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="270"/>
        <source>Activation is empty!  </source>
        <translation>啟動為空！  </translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="275"/>
        <location filename="../activation.cpp" line="286"/>
        <location filename="../activation.cpp" line="547"/>
        <source>Invalid activation code</source>
        <translation>啟動碼無效</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="292"/>
        <location filename="../activation.cpp" line="388"/>
        <location filename="../activation.cpp" line="436"/>
        <location filename="../activation.cpp" line="537"/>
        <source>Activation is successful, reboot the system!</source>
        <translation>啟動成功，重新啟動系統！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="302"/>
        <location filename="../activation.cpp" line="399"/>
        <location filename="../activation.cpp" line="449"/>
        <source>!(error code#</source>
        <translation>!（錯誤代碼#</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="377"/>
        <location filename="../activation.cpp" line="382"/>
        <location filename="../activation.cpp" line="423"/>
        <location filename="../activation.cpp" line="428"/>
        <source>Ukey not inserted or invalid Ukey</source>
        <translation>Ukey 未插入或 Ukey 無效</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="464"/>
        <location filename="../activation.cpp" line="511"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="481"/>
        <source>Serial number needs to be greater than 7 digits!</source>
        <translation>序列號必須大於 7 位！</translation>
    </message>
    <message>
        <location filename="../activation.cpp" line="492"/>
        <source>Please enter the correct serial number!</source>
        <translation>請輸入正確的序列號！</translation>
    </message>
</context>
<context>
    <name>DeactivateWidget</name>
    <message>
        <location filename="../deactivatewidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="77"/>
        <location filename="../deactivatewidget.ui" line="314"/>
        <source>Deactivate</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="124"/>
        <source>Please enter the reason for the deactivation and click deactivation button to complete deactivation.</source>
        <translation>請輸入停用原因，然後按兩下停用按鈕以完成停用。</translation>
    </message>
    <message>
        <source>Please enter the reason for the deactivation and click deactivation button to complete deactivation</source>
        <translation type="vanished">请输入取消原因，并点击取消激活按钮完成取消激活</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="189"/>
        <source>Reason for deactivation:</source>
        <translation>停用原因：</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.ui" line="289"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="80"/>
        <location filename="../deactivatewidget.cpp" line="110"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="50"/>
        <source>Deactivation successs!</source>
        <translation>停用成功！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="54"/>
        <source>Deactivation fail!</source>
        <translation>停用失敗！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="58"/>
        <source>The administrator refused!</source>
        <translation>管理員拒絕了！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="61"/>
        <source>Requires an administrator to confirm!</source>
        <translation>需要管理員確認！</translation>
    </message>
    <message>
        <location filename="../deactivatewidget.cpp" line="110"/>
        <source>No quota, please contact the administrator!</source>
        <translation>沒有配額，請聯繫管理員！</translation>
    </message>
</context>
<context>
    <name>DropWidget</name>
    <message>
        <location filename="../dropwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../dropwidget.ui" line="155"/>
        <source>Add</source>
        <translation>加</translation>
    </message>
    <message>
        <location filename="../dropwidget.ui" line="186"/>
        <source>click Add Or drag and drop the authorization file here</source>
        <translation>按下添加或將授權檔拖放到此處</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="vanished">选择授权文件</translation>
    </message>
    <message>
        <source>Or drag and drop the authorization file here</source>
        <translation type="vanished">或者拖拽授权文件到此处</translation>
    </message>
    <message>
        <source>Please import .kyinfo and LICENSE files</source>
        <translation type="vanished">请导入.kyinfo和LICENSE文件</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="166"/>
        <location filename="../dropwidget.cpp" line="213"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="166"/>
        <source>Invalid filename: </source>
        <translation>檔案名無效： </translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="166"/>
        <source>, please import kyinfo and LICENSE text files.</source>
        <translation>，請匯入 kyinfo 和許可證文本檔。</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="213"/>
        <source>Please import kyinfo and LICENSE text files.</source>
        <translation>請匯入 kyinfo 和許可證文字檔。</translation>
    </message>
    <message>
        <source>, please import kyinfo and LICENSE text files</source>
        <translation type="vanished">，请导入.kyinfo和LICENSE文本文件</translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="200"/>
        <source>Existing similar filename: </source>
        <translation>現有的類似檔名稱： </translation>
    </message>
    <message>
        <location filename="../dropwidget.cpp" line="200"/>
        <source>, whether to overwrite?</source>
        <translation>，是否覆蓋？</translation>
    </message>
    <message>
        <source>Please import kyinfo and LICENSE text files</source>
        <translation type="vanished">请导入.kyinfo和LICENSE文本文件</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
</context>
<context>
    <name>FileRepeatDialog</name>
    <message>
        <source>File repeat</source>
        <translation type="vanished">文件重复</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="vanished">覆盖</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>&quot;</source>
        <translation type="vanished">“</translation>
    </message>
    <message>
        <source>&quot; file already exists, whether to overwrite?</source>
        <translation type="vanished">”文件已存在，是否覆盖？</translation>
    </message>
</context>
<context>
    <name>ImportLicenseFileWidget</name>
    <message>
        <location filename="../importlicensefilewidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="74"/>
        <location filename="../importlicensefilewidget.cpp" line="219"/>
        <source>Import license file</source>
        <translation>匯入許可證檔</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="121"/>
        <source>Please select the authorization file and click the import button to complete the import.</source>
        <translation>請選擇授權檔，點擊導入按鈕完成導入。</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="529"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.ui" line="554"/>
        <location filename="../importlicensefilewidget.cpp" line="220"/>
        <source>import</source>
        <translation>進口</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="137"/>
        <location filename="../importlicensefilewidget.cpp" line="156"/>
        <location filename="../importlicensefilewidget.cpp" line="301"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="137"/>
        <location filename="../importlicensefilewidget.cpp" line="301"/>
        <source>!(error code#</source>
        <translation>!（錯誤代碼#</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="156"/>
        <source>Not site license file!</source>
        <translation>不是網站許可證檔！</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="211"/>
        <source>Please select the authorization file you want to import, and then click the Activate button.</source>
        <translation>請選擇要導入的授權檔，然後按下「啟動」 按鈕。</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="212"/>
        <source>Import site license file</source>
        <translation>匯入網站許可檔</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="213"/>
        <source>Activate</source>
        <translation>啟動</translation>
    </message>
    <message>
        <location filename="../importlicensefilewidget.cpp" line="218"/>
        <source>Please select the authorization file you want to import, and then click the import button.</source>
        <translation>請選擇要導入的授權檔，然後按下導入按鈕。</translation>
    </message>
    <message>
        <source>Imported successfully!</source>
        <translation type="vanished">导入成功！</translation>
    </message>
    <message>
        <source>The system architecture information does not match. Import it again!</source>
        <translation type="vanished">系统架构信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The system version information does not match. Import it again!</source>
        <translation type="vanished">系统版本信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>Imported successfully, reboot the system!</source>
        <translation type="vanished">导入成功，请重启系统！</translation>
    </message>
    <message>
        <source>The authorization files is abnormal, please re-import!</source>
        <translation type="vanished">授权文件异常，请重新导入！</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="79"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="79"/>
        <source>It is currently in livecd mode, please activate it after installing the system!</source>
        <translation>它目前處於livecd模式，請在安裝系統后啟動它！</translation>
    </message>
</context>
<context>
    <name>QrCodeActivationWidget</name>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="77"/>
        <source>Sweep the code to activate</source>
        <translation>掃碼啟動</translation>
    </message>
    <message>
        <source>Connection timed out, please click retry button to activate.</source>
        <translation type="vanished">连接超时，请点击“重试”按钮完成激活。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="376"/>
        <source>Serial number:</source>
        <translation>序號：</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="493"/>
        <source>Activation code:</source>
        <translation>啟動碼：</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="668"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.ui" line="693"/>
        <source>Activate</source>
        <translation>啟動</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="34"/>
        <source>Activating in progress</source>
        <translation>正在啟動</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="110"/>
        <source>Please scan the qr code below, and according to the prompt to complete the activation operation on mobile devices.</source>
        <translation>請掃描下方二維碼，並根據提示在行動裝置上完成啟動操作。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="136"/>
        <source>Unable to connect to the network, please scan the QR code below, enter the obtained serial number and activation code to activate.</source>
        <translation>無法連接網路，請掃描下方二維碼，輸入獲取的序列號和啟動碼進行啟動。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="165"/>
        <source>Activation timed out! Please complete the activation on the mobile device, and then click retry button.</source>
        <translation>啟動超時！請在行動裝置上完成啟動，然後按下重試按鈕。</translation>
    </message>
    <message>
        <source>Please scan the qr code below and follow the instructions on your mobile device.</source>
        <translation type="vanished">请扫描下方二维码，在移动设备上根据提示操作。</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="vanished">重试</translation>
    </message>
    <message>
        <source>Please scan the qr code below, confirm activation on mobile devices, and click the &quot;activate&quot; button to complete the activation.</source>
        <translation type="vanished">请扫描下方二维码，在移动设备上确认激活，并点击“激活”按钮完成激活。</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="259"/>
        <location filename="../qrcodeactivationwidget.cpp" line="270"/>
        <source>Invalid activation code</source>
        <translation>啟動碼無效</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="285"/>
        <location filename="../qrcodeactivationwidget.cpp" line="325"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <source>Activation is successful, reboot the system!</source>
        <translation type="vanished">激活/延长服务成功,请重启系统！</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="285"/>
        <source>!(error code#</source>
        <translation>!（錯誤代碼#</translation>
    </message>
    <message>
        <source>Please confirm activation on your mobile device!</source>
        <translation type="vanished">请在移动设备上确认激活！</translation>
    </message>
    <message>
        <location filename="../qrcodeactivationwidget.cpp" line="325"/>
        <source>Invalid serial number!</source>
        <translation>序列號無效！</translation>
    </message>
    <message>
        <source>The system has been activated!</source>
        <translation type="vanished">系统已经激活!</translation>
    </message>
</context>
<context>
    <name>SecretKeyActivationWidget</name>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="35"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Enter the product secret key</source>
        <translation type="vanished">输入产品秘钥</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="77"/>
        <source>Enter the product secret key to acitvate</source>
        <translation>輸入要啟動的產品金鑰</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="127"/>
        <source>The product secret key usually comes in the accompanying DVD box and consists of 20 digits and capital letters.</source>
        <translation>產品密鑰通常裝在隨附的 DVD 盒中，由 20 位數位和大寫字母組成。</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="304"/>
        <source>Activating in progress</source>
        <translation>正在啟動</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="422"/>
        <source>Activate</source>
        <translation>啟動</translation>
    </message>
    <message>
        <source>Activation</source>
        <translation type="vanished">激活</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.ui" line="397"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="123"/>
        <source>Invalid activation code</source>
        <translation>啟動碼無效</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="135"/>
        <location filename="../secretkeyactivationwidget.cpp" line="216"/>
        <location filename="../secretkeyactivationwidget.cpp" line="237"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <source>Activation is successful, reboot the system!</source>
        <translation type="vanished">激活/延长服务成功,请重启系统！</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="135"/>
        <source>!(error code#</source>
        <translation>!（錯誤代碼#</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="184"/>
        <source>The request parameters are incomplete</source>
        <translation>請求參數不完整</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="187"/>
        <source>The service serial number is wrong</source>
        <translation>服務序列號錯誤</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="190"/>
        <source>The product key and system version do not match</source>
        <translation>產品金鑰和系統版本不匹配</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="193"/>
        <source>The product key has been used</source>
        <translation>產品金鑰已被使用</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="196"/>
        <source>Incorrect product key input</source>
        <translation>產品金鑰輸入不正確</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="199"/>
        <source>Hardware code parsing failed</source>
        <translation>硬體代碼解析失敗</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="202"/>
        <source>Server error</source>
        <translation>伺服器錯誤</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="232"/>
        <source>Please enter 20-digit product key</source>
        <translation>請輸入 20 位產品金鑰</translation>
    </message>
    <message>
        <source>error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>no activation record</source>
        <translation type="vanished">无激活记录</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="210"/>
        <source>Unable to connect to the network</source>
        <translation>無法連接到網路</translation>
    </message>
    <message>
        <location filename="../secretkeyactivationwidget.cpp" line="227"/>
        <source>Product key is not correct</source>
        <translation>產品金鑰不正確</translation>
    </message>
</context>
<context>
    <name>SecretKeyOfflineActivationWidget</name>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="202"/>
        <source>Offline activation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="249"/>
        <source>Please scan the QR code below and enter the captcha displayed by the mobile device.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="458"/>
        <source>Serial number (8 bits):</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="598"/>
        <source>Activation</source>
        <translation>啟動</translation>
    </message>
    <message>
        <location filename="../secretkeyofflineactivationwidget.ui" line="623"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>SystemActivationMainWindow</name>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="35"/>
        <source>System activation</source>
        <translation>系統啟動</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="312"/>
        <source>Select activation mode</source>
        <translation>選擇啟動模式</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="392"/>
        <source>Product secret key</source>
        <translation>產品金鑰</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="472"/>
        <source>Scan QR code to avtivate</source>
        <translation>掃描二維碼進行驗證</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="552"/>
        <source>Site authorization</source>
        <translation>網站授權</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.ui" line="632"/>
        <source>UKEY</source>
        <translation></translation>
    </message>
    <message>
        <source>Qr code</source>
        <translation type="vanished">二维码</translation>
    </message>
    <message>
        <source>Import license file</source>
        <translation type="vanished">授权文件导入</translation>
    </message>
    <message>
        <source>ukey</source>
        <translation type="vanished">ukey</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="45"/>
        <source>Deactivate</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="28"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="28"/>
        <source>Dbus Error!</source>
        <translation>dbus 錯誤！</translation>
    </message>
    <message>
        <location filename="../systemactivationmainwindow.cpp" line="46"/>
        <source>Modifying license file</source>
        <translation>修改許可證檔</translation>
    </message>
    <message>
        <source>!(error code#</source>
        <translation type="vanished">！(错误码#</translation>
    </message>
    <message>
        <source>Please import it again!</source>
        <translation type="vanished">请重新导入！</translation>
    </message>
    <message>
        <source>The authorization files fail to be detected. Import it again!</source>
        <translation type="vanished">授权文件检测失败，请重新导入！</translation>
    </message>
    <message>
        <source>The system architecture information does not match. Import it again!</source>
        <translation type="vanished">系统架构信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The system version information does not match. Import it again!</source>
        <translation type="vanished">系统版本信息不匹配，请重新导入！</translation>
    </message>
    <message>
        <source>The authorization files test failed!</source>
        <translation type="vanished">授权文件检测失败!</translation>
    </message>
    <message>
        <source>Authorization files do not match system version!</source>
        <translation type="vanished">授权文件与系统版本不匹配!</translation>
    </message>
    <message>
        <source>The authorization files is abnormal, please re-import!</source>
        <translation type="vanished">授权文件异常，请重新导入！</translation>
    </message>
    <message>
        <source>Invalid activation code</source>
        <translation type="vanished">无效激活码</translation>
    </message>
</context>
<context>
    <name>UkeyActivationWidget</name>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="74"/>
        <source>Ukey activation</source>
        <translation>Ukey 啟動</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="290"/>
        <source>Activating in progress</source>
        <translation>正在啟動</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="402"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="143"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="99"/>
        <source>Ukey not recognized, please insert Ukey to activate.</source>
        <translation>無法識別 Ukey，請插入 Ukey 進行啟動。</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="105"/>
        <location filename="../ukeyactivationwidget.cpp" line="110"/>
        <source>Ukey not inserted or invalid Ukey</source>
        <translation>Ukey 未插入或 Ukey 無效</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="121"/>
        <source>Ukey activated successfully!</source>
        <translation>Ukey啟動成功！</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.ui" line="427"/>
        <location filename="../ukeyactivationwidget.cpp" line="195"/>
        <source>Activate</source>
        <translation>啟動</translation>
    </message>
    <message>
        <location filename="../ukeyactivationwidget.cpp" line="208"/>
        <source>please insert Ukey to activate.</source>
        <translation>請插入 Ukey 以啟動。</translation>
    </message>
    <message>
        <source>Ukey detection!</source>
        <translation type="vanished">Ukey检测!</translation>
    </message>
</context>
</TS>
