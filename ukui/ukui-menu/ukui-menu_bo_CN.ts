<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>MainViewWidget</name>
    <message>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="211"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="250"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="438"/>
        <source>Search</source>
        <translation>འཚོལ་བ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="29"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="66"/>
        <source>Office</source>
        <translation>གཞུང་ལས།དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="30"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="67"/>
        <source>Development</source>
        <translation>གོང་འཕེལ་</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="31"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="68"/>
        <source>Image</source>
        <translation>པར་རིས།།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="32"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="69"/>
        <source>Video</source>
        <translation>བརྙན་ཟློས།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="33"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="70"/>
        <source>Internet</source>
        <translation>སྦྲེལ་རེས་དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="34"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="71"/>
        <source>Game</source>
        <translation>རོལ་རྩེད།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="35"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="72"/>
        <source>Education</source>
        <translation>སློབ་གསོ།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="36"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="73"/>
        <source>Social</source>
        <translation>འབྲེལ་འདྲིས།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="37"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="74"/>
        <source>System</source>
        <translation>རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="38"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="75"/>
        <source>Safe</source>
        <translation>सुरक्षा</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="39"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="76"/>
        <source>Others</source>
        <translation>གཞན་དག</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="199"/>
        <source>Pin to all</source>
        <translation>མཉེན་ཆས་ཆ་ཚང་། དུ་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="202"/>
        <source>Unpin from all</source>
        <translation>མཉེན་ཆས་ཆ་ཚང་། ལས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="206"/>
        <source>Pin to taskbar</source>
        <translation>འགན་བྱང་དུ་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="209"/>
        <source>Unpin from taskbar</source>
        <translation>འགན་བྱང་ནས་ལེན་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="212"/>
        <source>Add to desktop shortcuts</source>
        <translation>ཅོག་ངོས་སུ་མྱུར་འཐེབ་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="223"/>
        <source>Uninstall</source>
        <translation>ལྷུ་གཏོར</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="313"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="379"/>
        <source>Switch User</source>
        <translation>སྤྱོད་མཁན་བརྗེ་རེས།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="321"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="391"/>
        <source>Hibernate</source>
        <translation>མལ་གསོ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="329"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="403"/>
        <source>Suspend</source>
        <translation>གཉིད་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="334"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="414"/>
        <source>Lock Screen</source>
        <translation>བརྙན་ཡོལ་ཟྭ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="341"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="425"/>
        <source>Log Out</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="347"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="442"/>
        <source>Update and restart</source>
        <translation>གསར་སྒྱུར་གལ་ཆེ་མཉམ་དུ་འཛིན་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="353"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="458"/>
        <source>Update and shut down</source>
        <translation>གསར་སྒྱུར་བྱས་པར་མ་ཟད་།ཁ་བརྒྱབ་།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="363"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="437"/>
        <source>Restart</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="371"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="453"/>
        <source>Shut Down</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="392"/>
        <source>&lt;p&gt;Turn off the computer, but the application will remain open. When you turn on the computer, you can return to the state where you left&lt;/p&gt;</source>
        <translation>&lt;p&gt;གློག་ཀླད་སྒོ་རྒྱག་དགོས།འོན་ཀྱང་བཀོལ་སྤྱོད་ཀྱིས་རྒྱུན་པར་ཁ་ཕྱེ་བའི་རྣམ་པ་སྲུང་འཛིན་བྱེད་ཐུབ། གློག་ཀླད་ཁ་ཕྱེ་སྐབས།རང་ཁ་བྲལ་བའི་རྣམ་པ་སླར་གསོ་བྱེད་ཐུབ།&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="404"/>
        <source>&lt;p&gt;The computer stays on, but consumes less power. The application will remain open all the time, which can quickly wake up the computer and restore it to the state where you left it&lt;/p&gt;</source>
        <translation>&lt;p&gt;གློག་ཀླད་ཀྱི་སྒོ་འབྱེད་པའི་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱས་ཡོད།འོན་ཀྱང་གློག་མང་པོ་མི་དགོས། བཀོལ་སྤྱོད།མུ་མཐུད་དུ་ཁ་ཕྱེ་བའི་རྣམ་པ་རྒྱུན་འཁྱོངས་བྱེད་ཐུབ།མགྱོགས་མྱུར་ངང་གློག་ཀླད་སད་ནས་ཁྱོད་ཁ་བྲལ་བའི་རྣམ་པ་ལ་སླེབས་ཐུབ།&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="426"/>
        <source>&lt;p&gt;The current user logs out of the system, ends his session and returns to the login interface&lt;/p&gt;</source>
        <translation>&lt;p&gt;མིག་སྔར་སྤྱོད་མཁན་གྱིས་མ་ལག་ཁྲོད་ནས་ཐོ་ཁོངས་ནས་སུབ་སྟེ་དེའི་ཁ་བརྡའི་མཇུག་སྒྲིལ།&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="438"/>
        <source>&lt;p&gt;Close all applications, turn off the computer, and then turn it on again&lt;/p&gt;</source>
        <translation>&lt;p&gt;བེད་སྤྱོད་ཡོད་ཚད་ཁ་རྒྱགགློག་ཀླད་ཁ་རྒྱགདེ་ནས་ཡང་བསྐྱར་གློག་ཀླད་ཁ་ཕྱེ།&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="454"/>
        <source>&lt;p&gt;Close all applications, and then turn off the computer&lt;/p&gt;</source>
        <translation>&lt;p&gt;བེད་སྤྱོད་ཡོད་ཚད་སྒོ་རྒྱག་པ་དང་།དེ་ནས་གློག་ཀླད་སྒོ་རྒྱག་དགོས་།&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="504"/>
        <source>Personalize this list</source>
        <translation>མགོ་རྩོམ་གདམ་བྱང་གི་རིམ་བྱང་སྒྲིག་འགོད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="174"/>
        <source>All</source>
        <translation>ཡོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="176"/>
        <source>Letter</source>
        <translation>གསལ་བྱེད།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="178"/>
        <source>Function</source>
        <translation>བྱེད་ལས་</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="197"/>
        <source>Personal</source>
        <translation>མི་སྒེར།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="199"/>
        <source>Trash</source>
        <translation>སྙིགས་སྒམ།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="201"/>
        <source>Computer</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="203"/>
        <source>Settings</source>
        <translation>སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="205"/>
        <source>Power</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="638"/>
        <source>Max</source>
        <translation>ཆེས་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="692"/>
        <source>Min</source>
        <translation>ཆུང་དུ་འགྲོ་བ་</translation>
    </message>
</context>
<context>
    <name>UkuiMenuGui</name>
    <message>
        <location filename="../ukui-menu-gui.cpp" line="76"/>
        <source>Show main window</source>
        <translation></translation>
    </message>
</context>
</TS>
