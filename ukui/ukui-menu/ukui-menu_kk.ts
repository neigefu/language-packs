<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>MainViewWidget</name>
    <message>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="211"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="250"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="438"/>
        <source>Search</source>
        <translation>Іздеу</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="29"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="66"/>
        <source>Office</source>
        <translation>Office</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="30"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="67"/>
        <source>Development</source>
        <translation>Даму</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="31"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="68"/>
        <source>Image</source>
        <translation>Кескін</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="32"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="69"/>
        <source>Video</source>
        <translation>Бейне</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="33"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="70"/>
        <source>Internet</source>
        <translation>Интернет</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="34"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="71"/>
        <source>Game</source>
        <translation>Ойын</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="35"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="72"/>
        <source>Education</source>
        <translation>Білім беру</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="36"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="73"/>
        <source>Social</source>
        <translation>Әлеуметтік</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="37"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="74"/>
        <source>System</source>
        <translation>Жүйе</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="38"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="75"/>
        <source>Safe</source>
        <translation>Қауіпсіз</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="39"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="76"/>
        <source>Others</source>
        <translation>Басқалары</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="199"/>
        <source>Pin to all</source>
        <translation>Барлығына бекіту</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="202"/>
        <source>Unpin from all</source>
        <translation>Барлығынан басу</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="206"/>
        <source>Pin to taskbar</source>
        <translation>Тапсырмалар тақтасына бекіту</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="209"/>
        <source>Unpin from taskbar</source>
        <translation>Тапсырмалар тақтасынан шығару</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="212"/>
        <source>Add to desktop shortcuts</source>
        <translation>Жұмыс үстелі пернелер тіркесімдеріне қосу</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="313"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="379"/>
        <source>Switch User</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="329"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="403"/>
        <source>Suspend</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="371"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="453"/>
        <source>Shut Down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="392"/>
        <source>&lt;p&gt;Turn off the computer, but the application will remain open. When you turn on the computer, you can return to the state where you left&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="404"/>
        <source>&lt;p&gt;The computer stays on, but consumes less power. The application will remain open all the time, which can quickly wake up the computer and restore it to the state where you left it&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="426"/>
        <source>&lt;p&gt;The current user logs out of the system, ends his session and returns to the login interface&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="438"/>
        <source>&lt;p&gt;Close all applications, turn off the computer, and then turn it on again&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="454"/>
        <source>&lt;p&gt;Close all applications, and then turn off the computer&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="223"/>
        <source>Uninstall</source>
        <translation>Жою</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="321"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="391"/>
        <source>Hibernate</source>
        <translation>Хибернате</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="334"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="414"/>
        <source>Lock Screen</source>
        <translation>Экранды құлыптау</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="341"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="425"/>
        <source>Log Out</source>
        <translation>Журналға жазу</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="347"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="442"/>
        <source>Update and restart</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="353"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="458"/>
        <source>Update and shut down</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="363"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="437"/>
        <source>Restart</source>
        <translation>Қайта іске қосу</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="504"/>
        <source>Personalize this list</source>
        <translation>Бұл тізімді жекелендіру</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="174"/>
        <source>All</source>
        <translation>Барлығы</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="176"/>
        <source>Letter</source>
        <translation>Хат</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="178"/>
        <source>Function</source>
        <translation>Функция</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="197"/>
        <source>Personal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="199"/>
        <source>Trash</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="201"/>
        <source>Computer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="203"/>
        <source>Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="205"/>
        <source>Power</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="638"/>
        <source>Max</source>
        <translation>Макс</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="692"/>
        <source>Min</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>UkuiMenuGui</name>
    <message>
        <location filename="../ukui-menu-gui.cpp" line="76"/>
        <source>Show main window</source>
        <translation></translation>
    </message>
</context>
</TS>
