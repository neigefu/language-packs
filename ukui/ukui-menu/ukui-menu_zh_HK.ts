<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>MainViewWidget</name>
    <message>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="211"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="250"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="438"/>
        <source>Search</source>
        <translation>搜索應用</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="29"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="66"/>
        <source>Office</source>
        <translation>辦公</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="30"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="67"/>
        <source>Development</source>
        <translation>開發</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="31"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="68"/>
        <source>Image</source>
        <translation>圖像</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="32"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="69"/>
        <source>Video</source>
        <translation>影音</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="33"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="70"/>
        <source>Internet</source>
        <translation>網路</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="34"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="71"/>
        <source>Game</source>
        <translation>遊戲</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="35"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="72"/>
        <source>Education</source>
        <translation>教育</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="36"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="73"/>
        <source>Social</source>
        <translation>社交</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="37"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="74"/>
        <source>System</source>
        <translation>系統</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="38"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="75"/>
        <source>Safe</source>
        <translation>安全</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="39"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="76"/>
        <source>Others</source>
        <translation>其它</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="199"/>
        <source>Pin to all</source>
        <translation>固定到所有應用</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="202"/>
        <source>Unpin from all</source>
        <translation>從所有應用取消固定</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="206"/>
        <source>Pin to taskbar</source>
        <translation>固定到任務列</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="209"/>
        <source>Unpin from taskbar</source>
        <translation>從任務列取消固定</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="212"/>
        <source>Add to desktop shortcuts</source>
        <translation>添加到桌面快捷方式</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="223"/>
        <source>Uninstall</source>
        <translation>卸載</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="313"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="379"/>
        <source>Switch User</source>
        <translation>切換使用者</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="321"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="391"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="329"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="403"/>
        <source>Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="334"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="414"/>
        <source>Lock Screen</source>
        <translation>鎖定螢幕</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="341"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="425"/>
        <source>Log Out</source>
        <translation>註銷</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="347"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="442"/>
        <source>Update and restart</source>
        <translation>更新並重啟</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="353"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="458"/>
        <source>Update and shut down</source>
        <translation>更新並關機</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="363"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="437"/>
        <source>Restart</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="371"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="453"/>
        <source>Shut Down</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="392"/>
        <source>&lt;p&gt;Turn off the computer, but the application will remain open. When you turn on the computer, you can return to the state where you left&lt;/p&gt;</source>
        <translation>&lt;p&gt;關閉電腦，但是應用會一直保持打開狀態，當打開電腦時，可以恢復到你離開的狀態&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="404"/>
        <source>&lt;p&gt;The computer stays on, but consumes less power. The application will remain open all the time, which can quickly wake up the computer and restore it to the state where you left it&lt;/p&gt;</source>
        <translation>&lt;p&gt;電腦保持開機狀態，但耗電較少，應用會一直保持打開狀態，可快速喚醒電腦並恢復到你離開的狀態&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="426"/>
        <source>&lt;p&gt;The current user logs out of the system, ends his session and returns to the login interface&lt;/p&gt;</source>
        <translation>&lt;p&gt;當前使用者從系統中註銷，結束其會話並返回登錄介面&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="438"/>
        <source>&lt;p&gt;Close all applications, turn off the computer, and then turn it on again&lt;/p&gt;</source>
        <translation>&lt;p&gt;關閉所有應用，關閉電腦，然後重新打開電腦&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="454"/>
        <source>&lt;p&gt;Close all applications, and then turn off the computer&lt;/p&gt;</source>
        <translation>&lt;p&gt;關閉所有應用，然後關閉電腦&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="504"/>
        <source>Personalize this list</source>
        <translation>設置開始功能表顯示清單</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="174"/>
        <source>All</source>
        <translation>所有軟體</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="176"/>
        <source>Letter</source>
        <translation>字母排序</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="178"/>
        <source>Function</source>
        <translation>功能分類</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="197"/>
        <source>Personal</source>
        <translation>個人</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="199"/>
        <source>Trash</source>
        <translation>回收站</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="201"/>
        <source>Computer</source>
        <translation>計算機</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="203"/>
        <source>Settings</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="205"/>
        <source>Power</source>
        <translation>電源</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="638"/>
        <source>Max</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="692"/>
        <source>Min</source>
        <translation>縮小</translation>
    </message>
</context>
<context>
    <name>UkuiMenuGui</name>
    <message>
        <location filename="../ukui-menu-gui.cpp" line="76"/>
        <source>Show main window</source>
        <translation>顯示主視窗</translation>
    </message>
</context>
</TS>
