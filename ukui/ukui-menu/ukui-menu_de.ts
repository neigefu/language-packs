<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>MainViewWidget</name>
    <message>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="211"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="250"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="438"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="29"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="66"/>
        <source>Office</source>
        <translation>Büro</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="30"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="67"/>
        <source>Development</source>
        <translation>Entwicklung</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="31"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="68"/>
        <source>Image</source>
        <translation>Bild</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="32"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="69"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="33"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="70"/>
        <source>Internet</source>
        <translation>Internet</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="34"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="71"/>
        <source>Game</source>
        <translation>Spiel</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="35"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="72"/>
        <source>Education</source>
        <translation>Bildung</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="36"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="73"/>
        <source>Social</source>
        <translation>Sozial</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="37"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="74"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="38"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="75"/>
        <source>Safe</source>
        <translation>Sicher</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="39"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="76"/>
        <source>Others</source>
        <translation>Andere</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="199"/>
        <source>Pin to all</source>
        <translation>An alle anheften</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="202"/>
        <source>Unpin from all</source>
        <translation>Lösen Sie sich von allen</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="206"/>
        <source>Pin to taskbar</source>
        <translation>An Taskleiste anheften</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="209"/>
        <source>Unpin from taskbar</source>
        <translation>Von der Taskleiste lösen</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="212"/>
        <source>Add to desktop shortcuts</source>
        <translation>Zu Desktop-Verknüpfungen hinzufügen</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="223"/>
        <source>Uninstall</source>
        <translation>Deinstallieren</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="313"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="379"/>
        <source>Switch User</source>
        <translation>Benutzer wechseln</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="321"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="391"/>
        <source>Hibernate</source>
        <translation>Überwintern</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="329"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="403"/>
        <source>Suspend</source>
        <translation>Aufhängen</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="334"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="414"/>
        <source>Lock Screen</source>
        <translation>Sperrbildschirm</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="341"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="425"/>
        <source>Log Out</source>
        <translation>Ausloggen</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="347"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="442"/>
        <source>Update and restart</source>
        <translation>Aktualisieren und Neustart</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="353"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="458"/>
        <source>Update and shut down</source>
        <translation>Aktualisieren und Herunterfahren</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="363"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="437"/>
        <source>Restart</source>
        <translation>Neustarten</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="371"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="453"/>
        <source>Shut Down</source>
        <translation>Herunterfahren</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="392"/>
        <source>&lt;p&gt;Turn off the computer, but the application will remain open. When you turn on the computer, you can return to the state where you left&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="404"/>
        <source>&lt;p&gt;The computer stays on, but consumes less power. The application will remain open all the time, which can quickly wake up the computer and restore it to the state where you left it&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="426"/>
        <source>&lt;p&gt;The current user logs out of the system, ends his session and returns to the login interface&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="438"/>
        <source>&lt;p&gt;Close all applications, turn off the computer, and then turn it on again&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="454"/>
        <source>&lt;p&gt;Close all applications, and then turn off the computer&lt;/p&gt;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="504"/>
        <source>Personalize this list</source>
        <translation>Personalisieren Sie diese Liste</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="174"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="176"/>
        <source>Letter</source>
        <translation>Brief</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="178"/>
        <source>Function</source>
        <translation>Funktion</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="197"/>
        <source>Personal</source>
        <translation>Persönlich</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="199"/>
        <source>Trash</source>
        <translation>Müll</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="201"/>
        <source>Computer</source>
        <translation>Computer</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="203"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="205"/>
        <source>Power</source>
        <translation>Macht</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="638"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="692"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
</context>
<context>
    <name>UkuiMenuGui</name>
    <message>
        <location filename="../ukui-menu-gui.cpp" line="76"/>
        <source>Show main window</source>
        <translation>Hauptfenster anzeigen</translation>
    </message>
</context>
</TS>
