<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>MainViewWidget</name>
    <message>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="211"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="250"/>
        <location filename="../src/userinterface/MainViewWidget/mainviewwidget.cpp" line="438"/>
        <source>Search</source>
        <translation>ᠡᠷᠢᠵᠦ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="29"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="66"/>
        <source>Office</source>
        <translation>ᠠᠯᠪᠠᠨ ᠠᠵᠢᠯᠯᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="30"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="67"/>
        <source>Development</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠨ ᠬᠥᠭᠵᠢᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="31"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="68"/>
        <source>Image</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="32"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="69"/>
        <source>Video</source>
        <translation>ᠺᠢᠨᠣ᠋ ᠶᠢᠨ ᠳᠠᠭᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="33"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="70"/>
        <source>Internet</source>
        <translation>ᠲᠣᠣᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="34"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="71"/>
        <source>Game</source>
        <translation>ᠲᠣᠭᠯᠠᠭᠠᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="35"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="72"/>
        <source>Education</source>
        <translation>ᠰᠤᠷᠭᠠᠨ ᠬᠦᠮᠦᠵᠢᠯ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="36"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="73"/>
        <source>Social</source>
        <translation>ᠨᠡᠶᠢᠭᠡᠮ ᠦᠨ ᠬᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="37"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="74"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="38"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="75"/>
        <source>Safe</source>
        <translation>ᠠᠮᠤᠷ ᠲᠦᠪᠰᠢᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/FunctionWidget/functionbuttonwidget.cpp" line="39"/>
        <location filename="../src/userinterface/Interface/ukuimenuinterface.cpp" line="76"/>
        <source>Others</source>
        <translation>ᠪᠤᠰᠤᠳ ᠃</translation>
    </message>
</context>
<context>
    <name>RightClickMenu</name>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="199"/>
        <source>Pin to all</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡᠨ ᠳᠦ ᠲᠣᠭᠲᠠᠪᠤᠷᠢᠵᠢᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="202"/>
        <source>Unpin from all</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡᠨ ᠡᠴᠡ ᠲᠣᠭᠲᠠᠮᠠᠯ ᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="206"/>
        <source>Pin to taskbar</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠳᠤ ᠲᠣᠭᠲᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="209"/>
        <source>Unpin from taskbar</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠠᠴᠠ ᠲᠣᠭᠲᠠᠮᠠᠯ ᠢ ᠦᠭᠡᠶᠢᠰᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="212"/>
        <source>Add to desktop shortcuts</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠭᠠᠳᠠᠷᠭᠤ ᠳᠤ ᠨᠡᠮᠡᠬᠦ ᠲᠦᠷᠭᠡᠨ ᠠᠷᠭ᠎ᠠ ᠮᠠᠶᠢᠭ ᠨᠡᠮᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="223"/>
        <source>Uninstall</source>
        <translation>ᠠᠴᠢᠶ᠎ᠠ ᠪᠠᠭᠤᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="313"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="379"/>
        <source>Switch User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ ᠰᠣᠯᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="321"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="391"/>
        <source>Hibernate</source>
        <translation>ᠤᠨᠲᠠᠵᠤ ᠠᠮᠠᠷᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="329"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="403"/>
        <source>Suspend</source>
        <translation>ᠤᠨᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="334"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="414"/>
        <source>Lock Screen</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ ᠶᠢ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="341"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="425"/>
        <source>Log Out</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="347"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="442"/>
        <source>Update and restart</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠶᠢ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠳᠠᠬᠢᠨ ᠡᠭᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="353"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="458"/>
        <source>Update and shut down</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠪᠠᠶᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ ᠶᠢ ᠰᠢᠨᠡᠳᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="363"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="437"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="371"/>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="453"/>
        <source>Shut Down</source>
        <translation>ᠪᠠᠶᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="392"/>
        <source>&lt;p&gt;Turn off the computer, but the application will remain open. When you turn on the computer, you can return to the state where you left&lt;/p&gt;</source>
        <translation>&lt;p&gt;ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ ᠂ ᠭᠡᠪᠡᠴᠦ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠨᠢ ᠨᠡᠶᠢᠳᠡᠮ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠷᠢᠮᠲᠠᠯᠠᠵᠤ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳᠦ ᠂ ᠲᠠᠨ ᠤ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠵᠦ ᠳᠡᠶᠢᠯᠦᠨ&lt;/p&gt;᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="404"/>
        <source>&lt;p&gt;The computer stays on, but consumes less power. The application will remain open all the time, which can quickly wake up the computer and restore it to the state where you left it&lt;/p&gt;</source>
        <translation>&lt;p&gt;ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠨᠢ ᠮᠠᠰᠢᠨ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠲᠤᠯᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠂ ᠭᠡᠪᠡᠴᠦ ᠴᠠᠬᠢᠯᠭᠠᠨ ᠬᠣᠷᠣᠭᠳᠠᠭᠤᠯᠬᠤ ᠨᠢ ᠨᠡᠯᠢᠶᠡᠳ ᠪᠠᠭ᠎ᠠ ᠂ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠦ ᠬᠤᠷᠠᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠪᠠᠲᠤᠯᠠᠵᠤ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠲᠦᠷᠭᠡᠨ ᠰᠡᠷᠢᠭᠡᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠲᠠᠨ ᠤ ᠰᠠᠯᠤᠭᠰᠠᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢᠶᠠᠨ ᠰᠡᠷᠭᠦᠭᠡᠨ&lt;/p&gt;᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="426"/>
        <source>&lt;p&gt;The current user logs out of the system, ends his session and returns to the login interface&lt;/p&gt;</source>
        <translation>&lt;p&gt;ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠰᠢᠰᠲ᠋ᠧᠮ ᠡᠴᠡ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠵᠤ ᠂ ᠲᠡᠭᠦᠨ ᠦ ᠬᠤᠷᠠᠯ ᠤᠨ ᠦᠭᠡ ᠪᠡᠨ ᠳᠠᠭᠤᠰᠬᠠᠬᠤ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠲᠡᠮᠳᠡᠭᠯᠡᠭᠰᠡᠨ ᠨᠢᠭᠤᠷ ᠲᠤ ᠪᠤᠴᠠᠵᠠᠶ&lt;/p&gt; ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="438"/>
        <source>&lt;p&gt;Close all applications, turn off the computer, and then turn it on again&lt;/p&gt;</source>
        <translation>&lt;p&gt;ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠳᠠᠬᠢᠨ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ&lt;/p&gt; ᠃</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="454"/>
        <source>&lt;p&gt;Close all applications, and then turn off the computer&lt;/p&gt;</source>
        <translation>&lt;p&gt;ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠵᠤ ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠢ ᠬᠠᠭᠠᠬᠤ&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/rightclickmenu/rightclickmenu.cpp" line="504"/>
        <source>Personalize this list</source>
        <translation>ᠵᠠᠭᠤᠰᠢ ᠶᠢᠨ ᠨᠡᠷᠡᠰ ᠦᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>SideBarWidget</name>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="174"/>
        <source>All</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="176"/>
        <source>Letter</source>
        <translation>ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠵᠢᠭ᠌ᠰᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="178"/>
        <source>Function</source>
        <translation>ᠴᠢᠳᠠᠮᠵᠢ ᠶᠢᠨ ᠲᠥᠷᠥᠯ ᠬᠤᠪᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="197"/>
        <source>Personal</source>
        <translation>ᠬᠣᠪᠢ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="199"/>
        <source>Trash</source>
        <translation>ᠡᠭᠡᠭᠦᠯᠦᠨ ᠬᠤᠷᠢᠶᠠᠬᠤ ᠥᠷᠲᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="201"/>
        <source>Computer</source>
        <translation>ᠺᠣᠮᠫᠢᠦᠢᠲ᠋ᠧᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="203"/>
        <source>Settings</source>
        <translation>ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="205"/>
        <source>Power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="638"/>
        <source>Max</source>
        <translation>ᠶᠡᠬᠡᠰᠬᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../src/userinterface/SideBarWidget/sidebarwidget.cpp" line="692"/>
        <source>Min</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>UkuiMenuGui</name>
    <message>
        <location filename="../ukui-menu-gui.cpp" line="76"/>
        <source>Show main window</source>
        <translation>ᠭᠣᠣᠯ ᠴᠣᠩᠬᠣ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
</TS>
