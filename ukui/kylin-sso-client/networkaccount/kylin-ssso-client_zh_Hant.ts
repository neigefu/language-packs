<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>CloudSyncUI::GlobalVariant</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败项！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../firstpage.cpp" line="20"/>
        <source>Service is not valid for private server</source>
        <translation>服務對私人伺服器無效</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="100"/>
        <source>Sync your settings across the other devices!</source>
        <translation>在其他設備之間同步您的設置！</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="110"/>
        <source>Network is not accessible</source>
        <translation>網路無法訪問</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="111"/>
        <source>Ensure internet accessibility</source>
        <translation>確保互聯網可訪問性</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="134"/>
        <source>Sign In</source>
        <translation>登錄</translation>
    </message>
</context>
<context>
    <name>FrameItem</name>
    <message>
        <source>Auto-sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="77"/>
        <source>Some items sync failed!</source>
        <translation>某些專案同步失敗！</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="78"/>
        <source>Watting for sync!</source>
        <translation>同步的瓦特！</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="128"/>
        <source>Auto-Sync</source>
        <translation>自動同步</translation>
    </message>
</context>
<context>
    <name>FrameList</name>
    <message>
        <location filename="../framelist.cpp" line="22"/>
        <source>Wallpaper</source>
        <translation>壁紙</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="25"/>
        <source>ScreenSaver</source>
        <translation>屏幕保護程式</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="28"/>
        <source>Peony</source>
        <translation>牡丹</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="31"/>
        <source>Power</source>
        <translation>權力</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="34"/>
        <source>Themes</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="37"/>
        <source>Touchpad</source>
        <translation>觸控板</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="40"/>
        <source>Quick Launch</source>
        <translation>快速啟動</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="43"/>
        <source>Panel</source>
        <translation>面板</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="46"/>
        <source>Mouse</source>
        <translation>鼠</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="49"/>
        <source>Menu</source>
        <translation>功能表</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="52"/>
        <source>Font</source>
        <translation>字體</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="55"/>
        <source>Keyboard</source>
        <translation>鍵盤</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="58"/>
        <source>Datetime</source>
        <translation>日期時間</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="61"/>
        <source>Avatar</source>
        <translation>化身</translation>
    </message>
</context>
<context>
    <name>HeaderModel</name>
    <message>
        <location filename="../headermodel.cpp" line="25"/>
        <source>Change Password</source>
        <translation>更改密碼</translation>
    </message>
    <message>
        <location filename="../headermodel.cpp" line="26"/>
        <source>Sign Out</source>
        <translation>登出</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation>您的電子郵件/姓名/電話</translation>
    </message>
</context>
<context>
    <name>MCodeWidget</name>
    <message>
        <location filename="../mcodewidget.cpp" line="33"/>
        <source>SongTi</source>
        <translation>松提</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="98"/>
        <source>Forget?</source>
        <translation>忘記？</translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="97"/>
        <source>Kylin ID</source>
        <translation>麒麟身份證</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="103"/>
        <source>Remember it</source>
        <translation>記住它</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="244"/>
        <source>Your password</source>
        <translation>您的密碼</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="248"/>
        <source>Pass login</source>
        <translation>通行證登錄</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="249"/>
        <source>Phone login</source>
        <translation>電話登錄</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="250"/>
        <location filename="../maindialog.cpp" line="632"/>
        <location filename="../maindialog.cpp" line="713"/>
        <location filename="../maindialog.cpp" line="747"/>
        <source>Login</source>
        <translation>登錄</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="251"/>
        <location filename="../maindialog.cpp" line="732"/>
        <source>Send</source>
        <translation>發送</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="245"/>
        <source>Please wait</source>
        <translation>請稍候</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="104"/>
        <source>Register</source>
        <translation>註冊</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="246"/>
        <source>Your code</source>
        <translation>您的代碼</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="247"/>
        <source>Your phone number here</source>
        <translation>您的電話號碼在這裡</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="278"/>
        <location filename="../maindialog.cpp" line="548"/>
        <source>Please move slider to right place</source>
        <translation>請將滑塊移動到正確的位置</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="738"/>
        <source>%1s left</source>
        <translation>剩餘 %1 秒</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="931"/>
        <source>User stop verify Captcha</source>
        <translation>使用者停止驗證驗證碼</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="932"/>
        <source>Parsing data failed!</source>
        <translation>解析數據失敗！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="933"/>
        <location filename="../maindialog.cpp" line="940"/>
        <source>No response data!</source>
        <translation>沒有回應數據！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="934"/>
        <source>Timeout!</source>
        <translation>超時！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="935"/>
        <source>Server internal error!</source>
        <translation>伺服器內部錯誤！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="936"/>
        <location filename="../maindialog.cpp" line="944"/>
        <location filename="../maindialog.cpp" line="946"/>
        <source>Phone number error!</source>
        <translation>電話號碼錯誤！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="938"/>
        <location filename="../maindialog.cpp" line="948"/>
        <source>Pictrure has expired!</source>
        <translation>皮特魯爾已過期！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="939"/>
        <source>User deleted!</source>
        <translation>使用者已刪除！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="958"/>
        <source>Please check account status!</source>
        <translation>請檢查帳戶狀態！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="943"/>
        <source>Your are reach the limit!</source>
        <translation>你已經達到了極限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="945"/>
        <source>Please check your code!</source>
        <translation>請檢查您的代碼！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="949"/>
        <source>Pictrure blocked!</source>
        <translation>皮特魯爾被封鎖了！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="950"/>
        <source>Illegal code!</source>
        <translation>非法代碼！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="951"/>
        <source>Phone code is expired!</source>
        <translation>電話代碼已過期！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="952"/>
        <source>Failed attemps limit reached!</source>
        <translation>已達到失敗的試探限制！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="954"/>
        <source>Slider validate error</source>
        <translation>滑塊驗證錯誤</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="955"/>
        <source>Phone code error!</source>
        <translation>電話代碼錯誤！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="956"/>
        <source>Code can not be empty!</source>
        <translation>代碼不能為空！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="957"/>
        <source>MCode can not be empty!</source>
        <translation>MCode不能為空！</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="937"/>
        <source>No network!</source>
        <translation>沒有網路！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="941"/>
        <source>Phone number exsists!</source>
        <translation>電話號碼存在！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="942"/>
        <source>Wrong phone number format!</source>
        <translation>錯誤的電話號碼格式！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="947"/>
        <source>Send sms Limited!</source>
        <translation>發送簡訊有限！</translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="953"/>
        <source>Wrong account or password!</source>
        <translation>錯誤的帳戶或密碼！</translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="959"/>
        <source>Unsupported operation!</source>
        <translation>不支援的操作！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="960"/>
        <source>Unsupported Client Type!</source>
        <translation>不支援的客戶端類型！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="961"/>
        <source>Please check your input!</source>
        <translation>請檢查您的輸入！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="963"/>
        <source>Process failed</source>
        <translation>進程失敗</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="95"/>
        <location filename="../mainwidget.cpp" line="522"/>
        <location filename="../mainwidget.h" line="105"/>
        <source>Disconnected</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="144"/>
        <source>Sync processing!</source>
        <translation>同步處理！</translation>
    </message>
    <message>
        <source>Waiting for service start...</source>
        <translation type="vanished">等待服务准备完毕...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="275"/>
        <source>Auto-sync</source>
        <translation>自動同步</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="280"/>
        <source>Cloud Account</source>
        <translation>雲帳戶</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="339"/>
        <location filename="../mainwidget.cpp" line="589"/>
        <source>Failed to sync!</source>
        <translation>同步失敗！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="357"/>
        <source>We get some trouble when service start</source>
        <translation>服務啟動時遇到一些麻煩</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="386"/>
        <source>Make sure installed cloud sync!</source>
        <translation>確保已安裝雲同步！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="537"/>
        <source>Waitting for sync complete!</source>
        <translation>等待同步完成！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="567"/>
        <source>Cloud Sync</source>
        <translation>雲同步</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="570"/>
        <source>Kylin Account</source>
        <translation>麒麟帳戶</translation>
    </message>
    <message>
        <source>Sync!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>networkaccount</name>
    <message>
        <location filename="../networkaccount.cpp" line="30"/>
        <source>Cloud Account</source>
        <translation>雲帳戶</translation>
    </message>
</context>
</TS>
