<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>CloudSyncUI::GlobalVariant</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败项！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../firstpage.cpp" line="20"/>
        <source>Service is not valid for private server</source>
        <translation>ᠬᠤᠪᠢᠵᠢᠭᠤᠯᠬᠤ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠳᠠ ᠡᠬᠦᠯᠡᠨ ᠳᠠᠩᠰᠠ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="100"/>
        <source>Sync your settings across the other devices!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ᠂ ᠦᠪᠡᠷᠮᠢᠴᠡ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠪᠤᠰᠤᠳ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳ᠋ᠤ᠌ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="110"/>
        <source>Network is not accessible</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="111"/>
        <source>Ensure internet accessibility</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠡᠬᠦᠯᠡᠨ ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="134"/>
        <source>Sign In</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>FrameItem</name>
    <message>
        <source>Auto-sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="77"/>
        <source>Some items sync failed!</source>
        <translation>ᠬᠡᠰᠡᠭ ᠪᠦᠯᠦᠭ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ ᠳᠦᠷᠦᠯ ᠢ᠋ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="78"/>
        <source>Watting for sync!</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ ᠪᠡᠷ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="128"/>
        <source>Auto-Sync</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>FrameList</name>
    <message>
        <location filename="../framelist.cpp" line="22"/>
        <source>Wallpaper</source>
        <translation>ᠰᠢᠷᠡᠬᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠠᠷᠤ ᠦᠵᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="25"/>
        <source>ScreenSaver</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ ᠪᠠ ᠳᠡᠯᠬᠡᠴᠡ ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="28"/>
        <source>Peony</source>
        <translation>ᠹᠠᠢᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="31"/>
        <source>Power</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠤ᠋ ᠡᠬᠢ ᠡᠬᠦᠰᠪᠦᠷᠢ ᠵᠢ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="34"/>
        <source>Themes</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="37"/>
        <source>Touchpad</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠬᠠᠪᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="40"/>
        <source>Quick Launch</source>
        <translation>ᠳᠦᠷᠬᠡᠨ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠪᠠᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="43"/>
        <source>Panel</source>
        <translation>ᠡᠬᠦᠷᠬᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="46"/>
        <source>Mouse</source>
        <translation>ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="49"/>
        <source>Menu</source>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="52"/>
        <source>Font</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="55"/>
        <source>Keyboard</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠤ᠋ᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="58"/>
        <source>Datetime</source>
        <translation>ᠴᠠᠭ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="61"/>
        <source>Avatar</source>
        <translation>ᠲᠤᠯᠤᠭᠠᠢ ᠵᠢᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
</context>
<context>
    <name>HeaderModel</name>
    <message>
        <location filename="../headermodel.cpp" line="25"/>
        <source>Change Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../headermodel.cpp" line="26"/>
        <source>Sign Out</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation>ᠢᠮᠸᠯ/ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ/ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ</translation>
    </message>
</context>
<context>
    <name>MCodeWidget</name>
    <message>
        <location filename="../mcodewidget.cpp" line="33"/>
        <source>SongTi</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="98"/>
        <source>Forget?</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠡᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="97"/>
        <source>Kylin ID</source>
        <translation>kylin ID</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="103"/>
        <source>Remember it</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠴᠡᠬᠡᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="244"/>
        <source>Your password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="248"/>
        <source>Pass login</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="249"/>
        <source>Phone login</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="250"/>
        <location filename="../maindialog.cpp" line="632"/>
        <location filename="../maindialog.cpp" line="713"/>
        <location filename="../maindialog.cpp" line="747"/>
        <source>Login</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="251"/>
        <location filename="../maindialog.cpp" line="732"/>
        <source>Send</source>
        <translation>ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="245"/>
        <source>Please wait</source>
        <translation>ᠶᠠᠭ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="104"/>
        <source>Register</source>
        <translation>ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="246"/>
        <source>Your code</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="247"/>
        <source>Your phone number here</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="278"/>
        <location filename="../maindialog.cpp" line="548"/>
        <source>Please move slider to right place</source>
        <translation>ᠭᠤᠯᠭᠤᠬᠤ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠮᠵᠢᠳᠠᠢ ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ᠌ ᠭᠤᠯᠭᠤᠭᠤᠯᠵᠤ ᠠᠪᠠᠴᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="738"/>
        <source>%1s left</source>
        <translation>%1 ᠰᠸᠺᠦᠨ᠋ᠲ ᠦᠯᠡᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="931"/>
        <source>User stop verify Captcha</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="932"/>
        <source>Parsing data failed!</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠵᠠᠳᠠᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="933"/>
        <location filename="../maindialog.cpp" line="940"/>
        <source>No response data!</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ ᠳ᠋ᠠᠢᠲ᠋ᠠ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="934"/>
        <source>Timeout!</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠴᠠᠭ ᠬᠡᠳᠦᠷᠡᠪᠡ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="935"/>
        <source>Server internal error!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠵᠢᠨ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="936"/>
        <location filename="../maindialog.cpp" line="944"/>
        <location filename="../maindialog.cpp" line="946"/>
        <source>Phone number error!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="938"/>
        <location filename="../maindialog.cpp" line="948"/>
        <source>Pictrure has expired!</source>
        <translation>ᠵᠢᠷᠤᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ ᠦᠩᠬᠡᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="939"/>
        <source>User deleted!</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠩᠰᠠ ᠪᠡᠨ ᠬᠠᠰᠤᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="958"/>
        <source>Please check account status!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠪᠠᠢᠳᠠᠯ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="943"/>
        <source>Your are reach the limit!</source>
        <translation>ᠳᠤᠰ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠤᠰ ᠡᠳᠦᠷ ᠲᠤ᠌ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠠᠪᠬᠤ ᠤᠬᠤᠷᠢ ᠮᠡᠳᠡᠬᠡᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠦᠢᠴᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="945"/>
        <source>Please check your code!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠪᠤᠰᠤᠳ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="949"/>
        <source>Pictrure blocked!</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠡᠪᠳᠡᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="950"/>
        <source>Illegal code!</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="951"/>
        <source>Phone code is expired!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="952"/>
        <source>Failed attemps limit reached!</source>
        <translation>ᠳᠤᠷᠰᠢᠭᠰᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠬᠡᠳᠦ ᠤᠯᠠᠨ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="954"/>
        <source>Slider validate error</source>
        <translation>ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠵᠢᠡᠷ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="955"/>
        <source>Phone code error!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="956"/>
        <source>Code can not be empty!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="957"/>
        <source>MCode can not be empty!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="937"/>
        <source>No network!</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="941"/>
        <source>Phone number exsists!</source>
        <translation>ᠳᠤᠰ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="942"/>
        <source>Wrong phone number format!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="947"/>
        <source>Send sms Limited!</source>
        <translation>ᠢᠯᠡᠬᠡᠬᠦ ᠤᠬᠤᠷᠢ ᠮᠡᠳᠡᠭᠡᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠬᠦᠢᠴᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="953"/>
        <source>Wrong account or password!</source>
        <translation>ᠳᠠᠩᠰᠠ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="959"/>
        <source>Unsupported operation!</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠦᠷ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="960"/>
        <source>Unsupported Client Type!</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="961"/>
        <source>Please check your input!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="963"/>
        <source>Process failed</source>
        <translation>ᠢᠯᠡᠬᠡᠭᠰᠡᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="95"/>
        <location filename="../mainwidget.cpp" line="522"/>
        <location filename="../mainwidget.h" line="105"/>
        <source>Disconnected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="144"/>
        <source>Sync processing!</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>Waiting for service start...</source>
        <translation type="vanished">等待服务准备完毕...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="275"/>
        <source>Auto-sync</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="280"/>
        <source>Cloud Account</source>
        <translation>ᠡᠬᠦᠯᠡᠨ ᠳᠠᠩᠰᠠ</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="339"/>
        <location filename="../mainwidget.cpp" line="589"/>
        <source>Failed to sync!</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ ᠶᠠᠪᠤᠴᠠ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="357"/>
        <source>We get some trouble when service start</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ ᠳ᠋ᠤ᠌ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="386"/>
        <source>Make sure installed cloud sync!</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠵᠢ ᠨᠢᠬᠡᠨᠳ ᠤᠭᠰᠠᠷᠰᠠᠨ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="537"/>
        <source>Waitting for sync complete!</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠳᠠᠭᠤᠰᠤᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="567"/>
        <source>Cloud Sync</source>
        <translation>ᠡᠬᠦᠯᠡᠨ ᠳᠠᠩᠰᠠ</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="570"/>
        <source>Kylin Account</source>
        <translation>ᠡᠬᠦᠯᠡᠨ ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Sync!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>networkaccount</name>
    <message>
        <location filename="../networkaccount.cpp" line="30"/>
        <source>Cloud Account</source>
        <translation>ᠡᠬᠦᠯᠡᠨ ᠳᠠᠩᠰᠠ</translation>
    </message>
</context>
</TS>
