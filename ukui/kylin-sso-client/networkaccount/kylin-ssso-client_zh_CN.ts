<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CloudSyncUI::GlobalVariant</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败项！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../firstpage.cpp" line="20"/>
        <source>Service is not valid for private server</source>
        <translation>私有化部署不支持云帐户</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="100"/>
        <source>Sync your settings across the other devices!</source>
        <translation>同步您的帐户数据以及个性化设置到其他设备！</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="110"/>
        <source>Network is not accessible</source>
        <translation>网络不可用</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="111"/>
        <source>Ensure internet accessibility</source>
        <translation>连接网络后才能使用云帐户功能</translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="134"/>
        <source>Sign In</source>
        <translation>登录</translation>
    </message>
</context>
<context>
    <name>FrameItem</name>
    <message>
        <source>Auto-sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="77"/>
        <source>Some items sync failed!</source>
        <translation>部分同步项同步失败！</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="78"/>
        <source>Watting for sync!</source>
        <translation>等候同步！</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="128"/>
        <source>Auto-Sync</source>
        <translation>自动同步</translation>
    </message>
</context>
<context>
    <name>FrameList</name>
    <message>
        <location filename="../framelist.cpp" line="22"/>
        <source>Wallpaper</source>
        <translation>桌面背景</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="25"/>
        <source>ScreenSaver</source>
        <translation>锁屏和屏保</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="28"/>
        <source>Peony</source>
        <translation>文件管理器</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="31"/>
        <source>Power</source>
        <translation>电源管理</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="34"/>
        <source>Themes</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="37"/>
        <source>Touchpad</source>
        <translation>触摸板</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="40"/>
        <source>Quick Launch</source>
        <translation>快速启动栏</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="43"/>
        <source>Panel</source>
        <translation>任务栏</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="46"/>
        <source>Mouse</source>
        <translation>鼠标</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="49"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="52"/>
        <source>Font</source>
        <translation>字体</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="55"/>
        <source>Keyboard</source>
        <translation>键盘</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="58"/>
        <source>Datetime</source>
        <translation>时间格式</translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="61"/>
        <source>Avatar</source>
        <translation>头像</translation>
    </message>
</context>
<context>
    <name>HeaderModel</name>
    <message>
        <location filename="../headermodel.cpp" line="25"/>
        <source>Change Password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../headermodel.cpp" line="26"/>
        <source>Sign Out</source>
        <translation>退出登录</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation>邮箱/用户名/手机号码</translation>
    </message>
</context>
<context>
    <name>MCodeWidget</name>
    <message>
        <location filename="../mcodewidget.cpp" line="33"/>
        <source>SongTi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="98"/>
        <source>Forget?</source>
        <translation>忘记密码?</translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="97"/>
        <source>Kylin ID</source>
        <translation>Kylin ID</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="103"/>
        <source>Remember it</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="244"/>
        <source>Your password</source>
        <translation>请输入密码</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="248"/>
        <source>Pass login</source>
        <translation>密码登录</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="249"/>
        <source>Phone login</source>
        <translation>手机登录</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="250"/>
        <location filename="../maindialog.cpp" line="632"/>
        <location filename="../maindialog.cpp" line="713"/>
        <location filename="../maindialog.cpp" line="747"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="251"/>
        <location filename="../maindialog.cpp" line="732"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="245"/>
        <source>Please wait</source>
        <translation>正在登录</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="104"/>
        <source>Register</source>
        <translation>注册</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="246"/>
        <source>Your code</source>
        <translation>请输入验证码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="247"/>
        <source>Your phone number here</source>
        <translation>请输入手机号码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="278"/>
        <location filename="../maindialog.cpp" line="548"/>
        <source>Please move slider to right place</source>
        <translation>请滑动滑块到合适的位置</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="738"/>
        <source>%1s left</source>
        <translation>剩%1秒</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="931"/>
        <source>User stop verify Captcha</source>
        <translation>用户给停止验证验证码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="932"/>
        <source>Parsing data failed!</source>
        <translation>解析数据失败!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="933"/>
        <location filename="../maindialog.cpp" line="940"/>
        <source>No response data!</source>
        <translation>无响应数据!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="934"/>
        <source>Timeout!</source>
        <translation>登录超时，请重试！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="935"/>
        <source>Server internal error!</source>
        <translation>服务器内部错误!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="936"/>
        <location filename="../maindialog.cpp" line="944"/>
        <location filename="../maindialog.cpp" line="946"/>
        <source>Phone number error!</source>
        <translation>请检查您的手机号码！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="938"/>
        <location filename="../maindialog.cpp" line="948"/>
        <source>Pictrure has expired!</source>
        <translation>图片已经过期！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="939"/>
        <source>User deleted!</source>
        <translation>用户已经销户！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="958"/>
        <source>Please check account status!</source>
        <translation>请检查您的用户状态！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="943"/>
        <source>Your are reach the limit!</source>
        <translation>该手机当日接收短信次数达到上限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="945"/>
        <source>Please check your code!</source>
        <translation>手机号码其他错误！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="949"/>
        <source>Pictrure blocked!</source>
        <translation>图片格式损坏!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="950"/>
        <source>Illegal code!</source>
        <translation>非法参数！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="951"/>
        <source>Phone code is expired!</source>
        <translation>验证码已经失效！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="952"/>
        <source>Failed attemps limit reached!</source>
        <translation>尝试次数太多！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="954"/>
        <source>Slider validate error</source>
        <translation>滑动验证码验证失败</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="955"/>
        <source>Phone code error!</source>
        <translation>手机验证码错误！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="956"/>
        <source>Code can not be empty!</source>
        <translation>验证码不能为空！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="957"/>
        <source>MCode can not be empty!</source>
        <translation>验证码不能为空！</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="937"/>
        <source>No network!</source>
        <translation>网络不可达!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="941"/>
        <source>Phone number exsists!</source>
        <translation>手机号码已存在！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="942"/>
        <source>Wrong phone number format!</source>
        <translation>手机号码格式错误!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="947"/>
        <source>Send sms Limited!</source>
        <translation>发送短信达到限制！</translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="953"/>
        <source>Wrong account or password!</source>
        <translation>账号或者密码错误!</translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="959"/>
        <source>Unsupported operation!</source>
        <translation>暂不支持该操作!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="960"/>
        <source>Unsupported Client Type!</source>
        <translation>不支持的客户端类型!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="961"/>
        <source>Please check your input!</source>
        <translation>请检查您的输入!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="963"/>
        <source>Process failed</source>
        <translation>发送请求失败</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="95"/>
        <location filename="../mainwidget.cpp" line="522"/>
        <location filename="../mainwidget.h" line="105"/>
        <source>Disconnected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="144"/>
        <source>Sync processing!</source>
        <translation>同步中！</translation>
    </message>
    <message>
        <source>Waiting for service start...</source>
        <translation type="vanished">等待服务准备完毕...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="275"/>
        <source>Auto-sync</source>
        <translation>自动同步</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="280"/>
        <source>Cloud Account</source>
        <translation>云帐户</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="339"/>
        <location filename="../mainwidget.cpp" line="589"/>
        <source>Failed to sync!</source>
        <translation>同步过程发生了错误！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="357"/>
        <source>We get some trouble when service start</source>
        <translation>服务启动存在问题</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="386"/>
        <source>Make sure installed cloud sync!</source>
        <translation>确保同步服务已经安装！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="537"/>
        <source>Waitting for sync complete!</source>
        <translation>等待同步结束自动退出！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="567"/>
        <source>Cloud Sync</source>
        <translation>云帐户</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="570"/>
        <source>Kylin Account</source>
        <translation>云帐户消息</translation>
    </message>
    <message>
        <source>Sync!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>networkaccount</name>
    <message>
        <location filename="../networkaccount.cpp" line="30"/>
        <source>Cloud Account</source>
        <translation>云帐户</translation>
    </message>
</context>
</TS>
