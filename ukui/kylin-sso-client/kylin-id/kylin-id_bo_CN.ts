<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation>ཡིག་སྒམ།སྤྱོད་མཁན་གྱི་མིང་།ཁ་པར་ཨང་གྲངས།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="130"/>
        <source>Forget?</source>
        <translation>གསང་གྲངས་བརྗེད་པ།</translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="129"/>
        <source>Kylin ID</source>
        <translation>ཅིན་ལིནID</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>Remember it</source>
        <translation>གསང་གྲངས་ཡིད་ལ་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="305"/>
        <source>Your password</source>
        <translation>ཁྱེད་ཀྱི་གསང་གྲངས་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="309"/>
        <source>Pass login</source>
        <translation>གསང་གྲངས་ཐོ་འགོད་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="310"/>
        <source>Phone login</source>
        <translation>ཁ་པར་གྱི་ཐོ་འགོད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="683"/>
        <location filename="../mainwindow.cpp" line="770"/>
        <location filename="../mainwindow.cpp" line="838"/>
        <source>Login</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="789"/>
        <source>Send</source>
        <translation>སྐུར་སྐྱེལ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>Please wait</source>
        <translation>ཐོ་འགོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="136"/>
        <source>Register</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="307"/>
        <source>Your code</source>
        <translation>ར་སྤྲོད་ཨང་གྲངས་སྣོན་རོགས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="308"/>
        <source>Your phone number here</source>
        <translation>ཁ་པར་ཨང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="339"/>
        <location filename="../mainwindow.cpp" line="592"/>
        <source>Please move slider to right place</source>
        <translation>འདྲེད་འགུལ་འདྲེད་ལེབ་འོས་འཚམ་གྱི་གནས་སུ་འཇོག་རོགས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="795"/>
        <source>%1s left</source>
        <translation>%1སྐར་ཆ་ལྷག་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1086"/>
        <source>User stop verify Captcha</source>
        <translation>སྤྱོད་མཁན་གྱིས་ར་སྤྲོད་ཨང་གྲངས་མཚམས་བཞག</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>Parsing data failed!</source>
        <translation>གཞི་གྲངས་ལ་དབྱེ་ཞིབ་བྱས་ཀྱང་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <location filename="../mainwindow.cpp" line="1095"/>
        <source>No response data!</source>
        <translation>ཚུར་སྣང་ལན་ཆ་མེད་པའི་གཞི་གྲངས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1089"/>
        <source>Timeout!</source>
        <translation>ཐོ་འགོད་དུས་ཚོད་ལས་བརྒལ་འདུག་ཡང་བསྐྱར་ཚོད་ལྟ་བྱོས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1090"/>
        <source>Server internal error!</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ནང་ཁུལ་གྱི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1091"/>
        <location filename="../mainwindow.cpp" line="1099"/>
        <location filename="../mainwindow.cpp" line="1101"/>
        <source>Phone number error!</source>
        <translation>ཞིབ་བཤེར་བྱས་པའི་ཁ་པར་ཨང་གྲངས་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1093"/>
        <location filename="../mainwindow.cpp" line="1103"/>
        <source>Pictrure has expired!</source>
        <translation>ཚ་ལུ་མ་དུས་ལས་ཡོལ་ཟིན།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1094"/>
        <source>User deleted!</source>
        <translation>སྤྱོད་མཁན་གྱིས་བསུབ་སོང་།</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1098"/>
        <source>Your are reach the limit!</source>
        <translation>ཁྱེད་ཚོ་མཐར་སོན་པའི་ཚད་ལ་སླེབས་ཡོད།</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">请检查您的手机号码！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1100"/>
        <source>Please check your code!</source>
        <translation>ཁྱེད་ཀྱི་ཚབ་རྟགས་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1104"/>
        <source>Pictrure blocked!</source>
        <translation>འདྲ་པར་གྱི་རྣམ་པ་ཆག་སོང་།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1105"/>
        <source>Illegal code!</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་ཚབ་རྟགས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1106"/>
        <source>Phone code is expired!</source>
        <translation>ཁ་པར་གྱི་ཨང་གྲངས་དུས་ལས་ཡོལ་ཟིན།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1107"/>
        <source>Failed attemps limit reached!</source>
        <translation>ཕམ་ཁ་བྱུང་བའི་ཚོད་འཛིན་གྱི་ཚད་ལ་སླེབས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1109"/>
        <source>Slider validate error</source>
        <translation>འདྲེད་བརྡར་གྱིས་ནོར་འཁྲུལ་ལ་ཚོད་དཔག་བྱས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1110"/>
        <source>Phone code error!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1111"/>
        <source>Code can not be empty!</source>
        <translation>ཚབ་རྟགས་ནི་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1112"/>
        <source>MCode can not be empty!</source>
        <translation>ཨང་ཀི་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1113"/>
        <source>Please check account status!</source>
        <translation>ཁྱེད་ཀྱིས་རྩིས་ཐོའི་གནས་ཚུལ་ལ་ཞིབ་བཤེར་བྱེད་པའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1092"/>
        <source>No network!</source>
        <translation>དྲ་རྒྱ་མེད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>Phone number exsists!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཕྱིར་འབུད་བྱེད་མཁན།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1097"/>
        <source>Wrong phone number format!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཀྱི་རྣམ་གཞག་ནོར་སོང་།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1102"/>
        <source>Send sms Limited!</source>
        <translation>འཕྲིན་ཐུང་བསྐུར་ན་ཚད་བཀག་ལ་ཐོན་ཡོད།</translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1108"/>
        <source>Wrong account or password!</source>
        <translation>ནོར་འཁྲུལ་གྱི་རྩིས་ཐོ་དང་ཡང་ན་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1114"/>
        <source>Unsupported operation!</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་བྱ་སྤྱོད་ཅིག་རེད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1115"/>
        <source>Unsupported Client Type!</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཚོང་འགྲུལ་གྱི་རིགས་དབྱིབས་ཤིག་རེད།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1116"/>
        <source>Please check your input!</source>
        <translation>ཁྱེད་ཀྱི་ནང་དོན་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Process failed</source>
        <translation>བརྒྱུད་རིམ་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>WechatLogin</name>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="114"/>
        <source>Register</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="123"/>
        <source>Login</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="137"/>
        <location filename="../wechat/wechatlogin.cpp" line="468"/>
        <source>Scan Code for Quick Login</source>
        <translation>མགྱོགས་མྱུར་ངང་ཐོ་འགོད་བྱེད་པའི་ཨང་གྲངས་ལ་ཞིབ་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="299"/>
        <location filename="../wechat/wechatlogin.cpp" line="347"/>
        <source>Fail to Get QRcode,Please Retry</source>
        <translation>རྩ་གཉིས་རྟགས་རིས་ཐོབ་ནས་ཕམ་སོང་།ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="313"/>
        <source>QRcode Invaild,Please Click to Refresh</source>
        <translation>རྩ་གཉིས་རྟགས་རིས་གོ་མི་ཆོད་པར་གྱུར་འདུག་གསར་སྒྱུར་མནན་རོགས།</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="327"/>
        <source>Success</source>
        <translation>བཤར་ཕབ་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="424"/>
        <source>Not register yet, click to register</source>
        <translation>སྤྱོད་མཁན་གྱིས་ཐོ་འགོད་བྱས་མེད་པས་སྔོན་ལ་ཐོ་འགོད་མནན་རོགས།</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="445"/>
        <source>Click login button after register successful</source>
        <translation>ཐོ་འགོད་བྱས་རྗེས་ཐོ་འགོད་གཅུས་སྒོ་མནན་ནས་ཐོ་འགོད་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
</context>
</TS>
