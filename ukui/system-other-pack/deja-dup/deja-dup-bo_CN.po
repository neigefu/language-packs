msgid ""
msgstr ""
"Project-Id-Version: deja-dup\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"PO-Revision-Date: 2018-04-03 12:00+0000\n"
"Last-Translator: Kakurady Drakenar <kakurady@gmail.com>\n"
"Language-Team: Chinese (Simplified) <zh_CN@li.org>\n"
"Language: bo_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Launchpad (build 2e26c9bbd21cdca248baaea29aeffb920afcc32a)\n"
"X-Launchpad-Export-Date: 2020-04-16 19:12+0000\n"

msgid "%1$s on %2$s"
msgstr "%1$s， %2$sརུ་གནས།"

msgid "%s on Amazon S3"
msgstr "Amazon S3 ཡི་ %s"

msgid "%s on Rackspace Cloud Files"
msgstr "Rackspace སྤྲིན་ཡིག་ཆའི་ %s"

msgid "A backup automatically starts every day."
msgstr " ཉིན་མ་རེར་གྲབས་ཉར་རེ་བྱེད།"

msgid "A backup automatically starts every week."
msgstr " གཟའ་འཁོར་རེར་གྲབས་ཉར་རེ་བྱེད།"

msgid "Add"
msgstr "སྣོན་པ་"

msgid "Amazon S3 Access Key ID"
msgstr "Amazon S3 གསང་ལྡེ་ ID ལྟ་སྤྱོད།"

msgid ""
"An optional folder name to store files in. This folder will be created in "
"the chosen bucket."
msgstr ""
"ཡིག་ཆའི་ཡིག་ཁུག་མིང་ཉར་བར་སྤྱོད། (འདེམས་རུང་།) "
"བདམས་པའི་བསོག་དུམ་དུ་ཡིག་ཁུག་གསར་བཟོ་བྱེད།"

msgid "At least a year"
msgstr "མ་མཐའ་ཡང་ལོ་གཅིག"

msgid "At least six months"
msgstr "མ་མཐའ་ཡང་ཟླ་དྲུག"

msgid "Authentication URL"
msgstr " URL ར་སྤྲོད།"

msgid "Back Up"
msgstr "གྲབས་ཉར།"

msgid "Back Up Now…"
msgstr "མྱུར་དུ་གྲབས་ཉར།..."

msgid "Backing Up…"
msgstr "གྲབས་ཉར་བྱེད་བཞིན་ཡོད།..."

msgid "Backing up:"
msgstr "གྲབས་ཉར།："

msgid "Backing up…"
msgstr "གྲབས་ཉར་བྱེད་བཞིན་ཡོད།..."

msgid "Backup Failed"
msgstr "གྲབས་ཉར་ཕམ་པ།"

msgid "Backup Finished"
msgstr "གྲབས་ཉར་ལེགས་འགྲུབ།"

msgid "Backup Monitor"
msgstr "སོ་ལྟ་ཆས་གྲབས་ཉར།"

msgid "Backup completed"
msgstr "གྲབས་ཉར་ལེགས་འགྲུབ།"

msgid "Backup encryption password"
msgstr "གསང་སྡོམ་གསང་ཨང་གྲབས་ཉར།"

msgid "Backup location"
msgstr "གྲབས་ཉར་གནས་ས།"

msgid "Backup location ‘%s’ does not exist."
msgstr "གྲབས་ཉར་གནས་ས“%s”མེད།"

msgid "Backup will begin when a network connection becomes available."
msgstr "གྲབས་ཉར་ནི་དྲ་བའི་འབྲེལ་མཐུད་སྤྱོད་རུང་སྐབས་སུ་མགོ་རྩོམ།"

msgid "Backups"
msgstr "གྲབས་ཉར།"

msgid "Bad encryption password."
msgstr "ནུས་མེད་ཀྱི་གསང་སྡོམ་གསང་ཨང་།"

msgid "Categories"
msgstr "རིགས་དབྱེ།"

msgid "Checking for Backups…"
msgstr "གྲབས་ཉར་ཡོད་མེད་ཞིབ་བཤེར་བྱེད་པ།..."

msgid "Checking for backups…"
msgstr "གྲབས་ཉར་ཡོད་མེད་ཞིབ་བཤེར་བྱེད་པ།..."

msgid "Choose Folder"
msgstr "ཡིག་ཁུག་འདེམ་པ།"

msgid "Choose destination for restored files"
msgstr "ཡིག་ཆ་སླར་གསོའི་དམིགས་ཡུལ་གནས་འདེམ་པ།"

msgid "Choose folders"
msgstr "ཡིག་ཁུག"

msgid "Cleaning up…"
msgstr "གཙང་བཤེར་བྱེད་པ།..."

msgid "Co_ntinue"
msgstr "མུ་མཐུད།(_N)"

msgid "Computer name changed"
msgstr "རྩིས་འཁོར་མིང་བཅོས་ཟིན།"

msgid "Confir_m password"
msgstr "གསང་ཨང་གཏན་འཁེལ།(_M)"

msgid "Connect _anonymously"
msgstr "མིང་གསང་འབྲེལ་མཐུད།(_A)"

msgid "Connect as u_ser"
msgstr "སྤྱོད་མཁན་ཐོབ་ཐང་གིས་འབྲེལ་མཐུད་(_U)"

msgid "Connect to Amazon S3"
msgstr " Amazon S3 ལ་འབྲེལ་མཐུད།"

msgid "Connect to Rackspace Cloud Files"
msgstr "Rackspace སྤྲིན་ཡིག་ཆའི་ཞབས་ཞུ་ལ་འབྲེལ་མཐུད།"

msgid "Connect to Server"
msgstr "ཞབས་ཞུ་ཆས་ལ་འབྲེལ་མཐུད།"

msgid ""
"Could not back up the following files.  Please make sure you are able to "
"open them."
msgstr ""
"གཤམ་གྱི་ཡིག་ཆ་གྲབས་ཉར་བྱེད་མི་ཐུབ། "
"ཁྱེད་ཀྱིས་ཡིག་ཆ་འདི་དག་ཁ་འབྱེད་ཐུབ་པ་གཏན་འཁེལ་བྱེདཞ"

msgid "Could not display %s"
msgstr "%s མངོན་མི་ཐུབ།"

msgid ""
"Could not restore the following files.  Please make sure you are able to "
"write to them."
msgstr ""
"གཤམ་གྱི་ཡིག་ཆ་སླར་གསོ་མི་ཐུབ། ཁྱེད་ཀྱིས་དེ་དག་ལ་འབྲི་ཐུབ་པ་གཏན་འཁེལ་བྱེད།"

msgid "Could not restore ‘%s’: File not found in backup"
msgstr "“%s” སླར་གསོ་མི་ཐུབ།：གྲབས་ཉར་དུ་ཡིག་ཆ་འདི་མི་རྙེད།"

msgid "Could not restore ‘%s’: Not a valid file location"
msgstr "“%s” ཕྱིར་ལོག་མི་ཐུབ།：ནུས་ལྡན་གྱི་ཡིག་ཆའི་གནས་ས་མིན།"

msgid "Could not understand duplicity version ‘%s’."
msgstr " duplicity ཡི་པར་གཞི།：%s གཏན་མི་འཁེལ།"

msgid "Could not understand duplicity version."
msgstr "duplicity ཡི་པར་གཞི་གཏན་མི་འཁེལ།"

msgid ""
"Creating a fresh backup to protect against backup corruption.  This will "
"take longer than normal."
msgstr ""
"གྲབས་ཉར་ཀུན་གསར་ཞིག་གསར་བཟོ་བྱས་ནས་གྲབས་ཉར་བརླག་པ་འགོག "
"འདི་ནི་རྒྱུན་ལྡན་གྱི་གྲབས་ཉར་ལས་དུས་ལྷག་ཏུ་རིང་།"

msgid "Creating the first backup.  This may take a while."
msgstr "གྲབས་ཉར་དང་པོ་གསར་བཟོ་བྱེད་བཞིན་ཡོད། འདི་ལ་དུས་ཅུང་ཙམ་དགོས།"

msgid "Day"
msgstr "ཚེས་གྲངས།"

msgid "Directory does not exist"
msgstr "དཀར་ཆག་མེད་པ།"

msgid "Déjà Dup Backup Tool"
msgstr "Déjà Dup གྲབས་ཉར་ལག་ཆ།"

msgid ""
"Déjà Dup needs to occasionally make fresh full backups. This is the number "
"of days to wait between full backups."
msgstr ""
"Déjà Dup མཚམས་རེས་ཡང་བསྐྱར་ཆ་ཚང་གྲབས་ཉར་བྱེད་དགོས། "
"འདི་ནི་ཆ་ཚང་གྲབས་ཉར་བར་གྱི་ཉིན་གྲངས་ཡིན།"

msgid "E_ncryption password"
msgstr "གསང་སྡོམ་གསང་ཨང་(_N)"

msgid "Encryption Password Needed"
msgstr "གསང་སྡོམ་གསང་ཨང་ནང་འཇུག་རོགས།"

msgid "Error reading file ‘%s’."
msgstr "ཡིག་ཆ་“%s”ཀློག་སྐབས་ནོར་འཁྲུལ་བྱུང་།"

msgid "Error writing file ‘%s’."
msgstr "ཡིག་ཆ་“%s” འབྲི་སྐབས་ནོར་འཁྲུལ་བྱུང་།"

msgid "Failed with an unknown error."
msgstr "མི་ཤེས་པའི་ནོར་འཁྲུལ་བྱུང་པས་ཕམ་པ།"

msgid "File"
msgstr "ཡིག་ཆ།"

msgid "Folder"
msgstr "ཡིག་ཁུག"

msgid "Folders to ignore"
msgstr "སྣང་མེད་དུ་གཏོང་དགོས་པའི་ཡིག་ཁུག"

msgid "Folders to save"
msgstr "ཉར་དགོས་པའི་ཡིག་ཁུག"

msgid "Forever"
msgstr "གཏན་དུ།"

msgid "Google Cloud Storage"
msgstr "ཀུ་ཀེ་སྤྲིན་གསོག་འཇོག"

msgid "Google Cloud Storage Access Key ID"
msgstr "ཀུ་ཀེ་སྤྲིན་གསོག་འཇོག་གསང་ལྡེ་ ID ལྟ་སྤྱོད།"

msgid "Google Drive"
msgstr "Google Drive"

msgid "Home"
msgstr "རྩིས་ཆས།"

msgid "Home (%s)"
msgstr "དཀར་ཆག་གཙོ་བོ་(%s)"

msgid "How long to keep backup files"
msgstr "གྲབས་ཉར་ཡིག་ཆའི་ཉར་ཡུན།"

msgid "How long to wait between full backups"
msgstr "ཁ་ཚང་གྲབས་ཉར་བར་ཆོད་དུས་ཚོད།"

msgid "How often to periodically back up"
msgstr "དུས་ངེས་གྲབས་ཉར་གྱི་ཟློས་ཕྱོད།"

msgid "Icon"
msgstr "རིས་རྟགས"

msgid "Immediately start a backup"
msgstr "ལམ་སེང་གྲབས་ཉར་མགོ་རྩོམ།"

msgid "Incrementally backs up, letting you restore from any particular backup"
msgstr "ཚད་འཕར་ཅན་གྱི་གྲབས་ཉར། ཁྱེད་ཀྱིས་གྲབས་ཉར་གནས་གང་རུང་ནས་སླར་གསོ་ལ་བདེ།"

msgid "Install Packages"
msgstr "མཉེན་ཆས་ཐུམ་སྒྲིག་འཇུག"

msgid "Integrates well into your GNOME desktop"
msgstr " GNOME ཅོག་ངོས་ནང་འདྲེས།"

msgid "Invalid ID."
msgstr "ནུས་མེད་ཀྱི ID"

msgid "Invalid secret key."
msgstr "ནུས་མེད་ཀྱི་གསང་ལྡེ།"

msgid "Keep your files safe by backing up regularly"
msgstr "དུས་ངེས་ཀྱི་གྲབས་ཉར་གྱིས་ཡིག་ཆ་བདེ་འཇགས་སྲུང་བ།"

msgid "Keep your important documents safe from disaster"
msgstr "ཁྱེད་ཀྱི་ཡིག་ཆ་གལ་ཆེ་སྲུང་སྐྱོབ།"

msgid "Last backup was today."
msgstr "ཉེ་ཆར་གྱི་གྲབས་ཉར་ནི་དེ་རིང་ཡིན།"

msgid "Last backup was yesterday."
msgstr "ཉེ་ཆར་གྱི་གྲབས་ཉར་ནི་ཁ་སང་ཡིན།"

msgid "Last seen"
msgstr "ཐེངས་སྔོན་གྱི་ལྟ་ཞིབ་དུས་ཚོད།"

msgid "Listing files…"
msgstr "ཡིག་ཆ་འགྲེམས་བཞིན་ཡོད།..."

msgid "Local Folder"
msgstr "མིག་སྔའི་དཀར་ཆག་"

msgid "Location not available"
msgstr "གནས་ས་སྤྱོད་མི་རུང་།"

msgid "Name"
msgstr "མིང་།"

msgid "Next backup is today."
msgstr "ཐེངས་ཕྱི་མའི་གྲབས་ཉར་དུས་ཚོད་ནི་དེ་རིང་ཡིན།"

msgid "Next backup is tomorrow."
msgstr "ཐེངས་ཕྱི་མའི་གྲབས་ཉར་དུས་ཚོད་ནི་སང་ཉིན་ཡིན།"

msgid "No backup files found"
msgstr "གྲབས་ཉར་ཡིག་ཆ་མ་རྙེད།"

msgid "No backup scheduled."
msgstr "འཆར་གཞི་མེད་པའི་གྲབས་ཉར།"

msgid "No backups to restore"
msgstr "སླར་གསོ་ཐུབ་པའི་གྲབས་ཉར་མེད།"

msgid "No directory provided"
msgstr "དཀར་ཆག་མཁོ་འདོན་མེད།"

msgid "No recent backups."
msgstr "ཉེ་ཆར་གྲབས་ཉར་བྱས་པ་མེད།"

msgid "No space left in ‘%s’."
msgstr "“%s”ཐོག་ཏུ་བར་སྟོང་ལྷག་མ་མེད།"

msgid "No space left."
msgstr "བར་སྟོང་ལྷག་མ་མེད། "

msgid ""
"Not all files were successfully backed up.  See dialog for more details."
msgstr ""
"ཡིག་ཆ་ཚང་མ་གྲབས་ཉར་ལེགས་འགྲུབ་བྱུང་མེད། "
"ཞིབ་ཕྲའི་གནས་ཚུལ་ཇེ་མང་གླེང་སྒྲོམ་ལ་ལྟོས།"

msgid ""
"Old backups will be deleted earlier if the storage location is low on space."
msgstr "གསོག་འཇོག་བར་སྟོང་མི་འདང་། གྲབས་ཉར་རྙིང་པ་འདོར་བ་ཡིན།"

msgid "Only one directory can be shown at once"
msgstr "ཐེངས་གཅིག་ལ་དཀར་ཆག་གཅིག་མངོན་པ།"

msgid "Original location"
msgstr "ཐོག་མའི་གནས་ས།"

msgid "Overview"
msgstr "རགས་ལྟ།"

msgid "Paused (no network)"
msgstr "སྐབས་སྡོད་བྱས་པ།(དྲ་བ་མེད།)"

msgid "Permission denied"
msgstr "ལྟ་སྤྱོད་ཟློག་པ།"

msgid "Permission denied when trying to create ‘%s’."
msgstr "་“%s” གསར་བཟོ་ཚོད་ལྟ་བྱེད་སྐབས་ལྟ་སྤྱོད་ཟློག་པ།"

msgid "Permission denied when trying to delete ‘%s’."
msgstr "“%s” འདོར་བ་ཚོད་ལྟ་བྱེད་སྐབས་དབང་ཚད་ཟློག་པ།"

msgid "Permission denied when trying to read ‘%s’."
msgstr "“%s” ཀློག་པ་ཚོད་ལྟ་བྱེད་སྐབས་དབང་ཚད་ཟློག་པ།"

msgid "Prefix"
msgstr "སྔོན་འཇུག"

msgid "Preparing…"
msgstr "གྲ་སྒྲིག་བྱེད་བཞིན་ཡོད།..."

msgid "Rackspace Cloud Files"
msgstr "Rackspace སྤྲིན་ཡིག་ཆ།"

msgid "Remove"
msgstr "བསུབ་པ།"

msgid "Require Password?"
msgstr "གསང་ཨང་དགོས་སམ།"

msgid "Restore"
msgstr "ཕྱིར་ལོག"

msgid "Restore Failed"
msgstr "སླར་གསོ་ཕམ་པ།"

msgid "Restore Finished"
msgstr "སླར་གསོ་ལེགས་འགྲུབ།"

msgid "Restore From When?"
msgstr "དུས་ནམ་ནས་སླར་གསོ་བྱེད་དགོས།"

msgid "Restore From Where?"
msgstr "གང་ནས་སླར་གསོ་བྱེད་དགོས།"

msgid "Restore Missing Files…"
msgstr "བོར་བའི་ཡིག་ཆ་ཕྱིར་གསོ།..."

msgid "Restore Test"
msgstr "ཚོད་ལེན་ཕྱིར་གསོ།"

msgid "Restore _folder"
msgstr "ཡིག་ཁུག་སླར་གསོ།(_F)"

msgid "Restore date"
msgstr "སླར་གསོ་དུས་ཚེས།"

msgid "Restore deleted files"
msgstr "འདོར་བའི་ཡིག་ཆ་སླར་གསོ།"

msgid "Restore deleted files from backup"
msgstr "གྲབས་ཉར་ལས་འདོར་བའི་ཡིག་ཆ་སླར་གསོ།"

msgid "Restore files to _original locations"
msgstr "ཡིག་ཆ་ཐོག་མའི་གནས་སར་སླར་གསོ།(_O)"

msgid "Restore folder"
msgstr "ཡིག་ཁུག་སླར་གསོ།"

msgid "Restore given files"
msgstr "གཏན་འཁེལ་ཡིག་ཆ་སླར་གསོ།"

msgid "Restore to Where?"
msgstr "གང་ལ་སླར་གསོ་དགོས།"

msgid "Restore to _specific folder"
msgstr "སྟོན་པའི་ཡིག་ཁུག་ཏུ་སླར་གསོ།(_S)"

msgid "Restore which Files?"
msgstr "ཡིག་ཆ་གང་དག་སླར་གསོ།"

msgid "Restore…"
msgstr "ཕྱིར་ལོག..."

msgid "Restoring files…"
msgstr "ཡིག་ཆ་སླར་གསོ་པ།..."

msgid "Restoring:"
msgstr "སླར་གསོ།："

msgid "Restoring…"
msgstr "སླར་གསོ་བྱེད་པ།..."

msgid "S3 Access Key I_D"
msgstr "S3 གསང་ལྡེ་ I_Dལྟ་སྤྱོད།"

msgid "S3 bucket name is not available."
msgstr "S3 གསོག་དུམ་མིང་སྤྱོད་མི་རུང་།"

msgid "S_how API access key"
msgstr "API ལྟ་སྤྱོད་གསང་ལྡེ་མངོན་པ།(_H)"

msgid "S_how password"
msgstr "གསང་ཨང་མངོན་པ།(_H)"

msgid "S_how secret access key"
msgstr "ལྟ་སྤྱོད་གསང་ལྡེ་མངོན་པ།(_H)"

msgid "Scanning finished"
msgstr "བཤར་འབེབས་ལེགས་འགྲུབ་བྱུང་།"

msgid "Scanning for files from up to a day ago…"
msgstr "ཉིན་གཅིག་ནང་གི་ཡིག་ཆ་ལ་བཤར་འབེབས་བྱེད་བཞིན་ཡོད།..."

msgid "Scanning for files from up to a month ago…"
msgstr "ཟླ་གཅིག་ནང་གི་ཡིག་ཆ་ལ་བཤར་འབེབས་བྱེད་བཞིན་ཡོད།..."

msgid "Scanning for files from up to a week ago…"
msgstr "གཟའ་གཅིག་ནང་གི་ཡིག་ཆ་ལ་བཤར་འབེབས་བྱེད་བཞིན་ཡོད།..."

msgid "Scanning:"
msgstr "བཤར་འབེབས།："

msgid "Scanning…"
msgstr "བཤར་འབེབས་བྱེད་བཞིན་ཡོད།..."

msgid "Scheduled backup delayed"
msgstr "འཆར་གཞི་གྲབས་ཉར་ཕྱིར་འགྱངས་པ།"

msgid "Schedules regular backups"
msgstr "རྒྱུན་ལྡན་དུས་ངེས་གྲབས་ཉར།"

msgid "Scheduling"
msgstr "བཀོད་གཏོང་བྱེད་པ།"

msgid "Securely encrypts and compresses your data"
msgstr "བདེ་འཇགས་ཀྱིས་ཁྱེད་ཀྱི་གཞི་གྲངས་གསང་སྡོམ་ཞིང་བཙིར་གནོན་བྱེད་པ།"

msgid "Show version"
msgstr "པར་གཞིའི་ཆ་འཕྲིན་འཆར་བ།"

msgid "Starting scheduled backup"
msgstr "འཆར་གཞི་གྲབས་ཉར་མགོ་རྩོམ།"

msgid "Storage location"
msgstr "གསོག་འཇོག་གནས་ས།"

msgid "Summary"
msgstr "གནད་བསྡུས།"

msgid "Test every two _months"
msgstr "ཟླ 2 རེར་ཚོད་ལྟ་ཐེངས་རེ་བྱེད།(_M)"

msgid "The Amazon S3 bucket name to use"
msgstr "སྤྱོད་དགོས་པའི་ Amazon S3 གསོག་འཇོག་དུམ་མིང་།"

msgid "The Amazon S3 folder"
msgstr "Amazon S3 ཡིག་ཁུག"

msgid "The Google Cloud Storage bucket name to use"
msgstr "ཀུ་ཀེ་སྤྲིན་གསོག་འཇོག་གིས་སྤྱོད་པའི་གསོག་འཇོག་དུམ་མིང་།"

msgid "The Google Cloud Storage folder"
msgstr "ཀུ་ཀེའི་སྤྲིན་གསོག་འཇོག་ཡིག་ཁུག"

msgid "The OpenStack Swift container"
msgstr "OpenStack Swift སྣོད་ཆས།"

msgid "The Rackspace Cloud Files container"
msgstr "Rackspace སྤྲིན་ཡིག་ཆ་སྣོད་ཆས།"

msgid "The folder where backups are stored"
msgstr "གསོག་འཇོག་གྲབས་ཉར་གྱི་ཡིག་ཁུག"

msgid "The last time Déjà Dup backed up"
msgstr "Déjà Dup གྲབས་ཉར་སྔོན་མའི་དུས་ཚོད།"

msgid ""
"The last time Déjà Dup checked whether it should prompt about backing up"
msgstr ""
"ཐེངས་སྔོན་Déjà Dup "
"ཞིབ་བཤེར་སྔོན་བཀོད་ཀྱིས་ཁྱེད་ལ་གྲབས་ཉར་དུས་ཚོད་དྲན་སྐུལ་བྱེད་མིན།"

msgid ""
"The last time Déjà Dup checked whether it should prompt about your password"
msgstr ""
"ཐེངས་སྔོན་Déjà Dup "
"ཞིབ་བཤེར་སྔོན་བཀོད་ཀྱིས་ཁྱེད་ལ་གསང་ཨང་གཏན་འཁེལ་གྱི་དུས་ཚོད་དྲན་སྐུལ་བྱེད་མིན།"

msgid "The last time Déjà Dup restored"
msgstr "Déjà Dup ཐེངས་སྔོན་ཕྱིར་ལོག་དུས་ཚོད།"

msgid ""
"The last time Déjà Dup successfully completed a backup. This time should be "
"in ISO 8601 format."
msgstr ""
"Déjà Dup ཐེངས་སྔོན་གྲབས་ཉར་ལེགས་འགྲུབ་ཀྱི་དུས་ཚོད། དུས་ཚོད་འདི་ནི་ ISO 8601 "
"རྣམ་གཞག་ཡིན་དགོས།"

msgid ""
"The last time Déjà Dup successfully completed a restore. This time should be"
" in ISO 8601 format."
msgstr ""
"Déjà Dup ཐེངས་སྔོན་གྲབས་ཉར་ལེགས་འགྲུབ་ཀྱི་དུས་ཚོད། དུས་ཚོད་འདི་ནི་ ISO 8602 "
"རྣམ་གཞག་ཡིན་དགོས།"

msgid "The last time Déjà Dup was run"
msgstr "ཐེངས་སྔོན་ Déjà Dup འཁོར་སྐྱོད་ཀྱི་དུས་ཚོད།"

msgid ""
"The last time Déjà Dup was successfully run. This time should be in ISO 8601"
" format."
msgstr ""
"ཐེངས་སྔོན་ Déjà Dup འཁོར་སྐྱོད་ལེགས་འགྲུབ་ཀྱི་དུས་ཚོད། དུས་ཚོད་འདི་ནི་ ISO "
"8601 རྣམ་གཞག་ཡིན་དགོས།"

msgid "The number of days between backups."
msgstr "གྲབས་ཉར་གྱི་བར་ཆོད་དུས་ཚོད།"

msgid ""
"The type of backup location. If ‘auto’, a default will be chosen based on "
"what is available."
msgstr ""
"གྲབས་ཉར་གནས་སའི་རིགས་གྲས། གལ་ཏེ་auto ལ་བཀོད་ན། "
"སྤྱོད་རུང་གི་གནས་ས་རང་སྤྱོད་བྱེད།"

msgid ""
"This is the Authentication URL, or keystone URL for the OpenStack service"
msgstr "འདི་ནི་URL ར་སྤྲོད་ཡང་ན་ OpenStack ཞབས་ཞུའི་ keystone URL"

msgid "This is your tenant for the OpenStack Swift service."
msgstr "འདི་ནི་ཁྱེད་ཀྱི་ OpenStack Swift ཞབས་ཞུའི་གླ་ཐེམ་ཡིན།"

msgid "This is your username for the OpenStack Swift service."
msgstr "འདི་ནི་ཁྱེད་ཀྱི་ OpenStack Swift ཞབས་ཞུའི་སྤྱོད་མིང་ཡིན།"

msgid "This is your username for the Rackspace Cloud Files service."
msgstr "འདི་ནི་ཁྱེད་ Rackspace སྤྲིན་ཡིག་ཆའི་ཞབས་ཞུའི་སྤྱོད་མཁན་མིང་ཡིན།"

msgid "Trash"
msgstr "སྙིགས་སྒམ།"

msgid "Type of location to store backup"
msgstr "གྲབས་ཉར་གསོག་འཇོག་གནས་སའི་རིགས་གྲས།"

msgid "Uploading…"
msgstr "ཡར་སྐྱེལ་བཞིན་ཡོད།..."

msgid "Verifying backup…"
msgstr "གྲབས་ཉར་ར་སྤྲོད་བྱེད་བཞིན་ཡོད།..."

msgid "Waiting for a network connection…"
msgstr "དྲ་བ་འབྲེལ་མཐུད་སྒུག་པ།..."

msgid "Waiting for ‘%s’ to become connected…"
msgstr "“%s”འབྲེལ་མཐུད་གསར་བཟོ་སྒུག་པ།..."

msgid "WebDAV"
msgstr "WebDAV"

msgid "Week"
msgstr "གཟའ།"

msgid "Whether to automatically back up on a regular schedule."
msgstr "གཏན་པའི་དུས་ཚོད་རེའུ་མིག་ལྟར་གྲབས་ཉར་རང་བྱེད་བྱ་དགོས་མིན།"

msgid "Whether to periodically back up"
msgstr "དུས་ངེས་གྲབས་ཉར་བྱེད་དགོས་མིན།"

msgid "Whether to request the root password"
msgstr "རྩ་བའི་གསང་ཨང་འདོན་དགོས་མིན།"

msgid ""
"Whether to request the root password when backing up from or restoring to "
"system folders."
msgstr ""
"རྒྱུད་ཁོངས་ཡིག་ཁུག་གྲབས་ཉར་ཡང་ན་རྒྱུད་ཁོངས་ཡིག་ཁུག་ཏུ་སླར་གསོ་སྐབས་ root "
"སྤྱོད་མཁན་གསང་ཨང་མཁོ་འདོན་རེ་ཞུ་འདོན་དགོས་མིན།"

msgid "You may use the %s button to browse for existing backups."
msgstr "ཁྱེད་ཀྱིས་ %s མཐེབ་གནོན་གྱིས་ཡོད་པའི་གྲབས་ཉར་ལྟ་བ།"

msgid "You must provide a directory, not a file"
msgstr "ཁྱེད་ཀྱིས་ངེས་པར་དུ་དཀར་ཆག་མཁོ་འདོན་དགོས། ཡིག་ཆ་མིན།"

msgid ""
"You should <a href=''>enable</a> automatic backups or use the %s button to "
"start one now."
msgstr ""
"ཁྱེད་ཀྱིས་<a href=''>སྒྲིག་འགོད་</a>གྲབས་ཉར་རང་བྱེད་ཡང་ན་ %s "
"མཐེབ་གནོན་གྱིས་ལམ་སེང་གྲབས་ཉར་ཐེངས་གཅིག་བྱེད།"

msgid ""
"You will need your password to restore your files. You might want to write "
"it down."
msgstr "ཡིག་ཆ་ཕྱིར་ལོག་པ་ལ་གསང་ཨང་དགོས། ཁྱེད་ཀྱིས་ཕལ་ཆེར་དེ་བཤུ་དགོས།"

msgid "Your Amazon S3 Access Key Identifier. This acts as your S3 username."
msgstr ""
"ཁྱེད་ཀྱི་Amazon S3 ལྟ་སྤྱོད་ཀྱི་གསང་ལྡེ་མཚོན་རྟགས། ཚན་འདི་ནི་ཁྱེད་ཀྱི་ S3 "
"སྤྱོད་མཁན་མིང་བྱ་ཐུབ།"

msgid "Your Amazon Web Services account is not signed up for the S3 service."
msgstr "ཁྱེད་ཀྱི་ Amazon Web Services རྩིས་ཐོས་S3 ཞབས་ཞུ་ཐོ་བཀོད་མེད།"

msgid ""
"Your Google Cloud Storage Access Key Identifier. This acts as your Google "
"Cloud Storage username."
msgstr ""
"ཁྱེད་ཀྱི་ཀུ་ཀེ་སྤྲིན་གསོག་འཇོག་ལྟ་སྤྱོད་ཀྱི་གསང་ལྡེ་མཚོན་རྟགས། "
"ཚན་འདི་ཁྱེད་ཀྱི་ཀུ་ཀེ་སྤྲིན་གསོག་འཇོག་གི་སྤྱོད་མཁན་མིང་བྱ་ཐུབ།"

msgid "Your OpenStack tenant"
msgstr "ཁྱེད་ OpenStack ཡི་གླ་ཐེམ།"

msgid "Your OpenStack username"
msgstr "ཁྱེད་ OpenStack ཡི་སྤྱོད་མཁན་མིང་།"

msgid "Your Rackspace username"
msgstr "ཁྱེད་ཀྱི་ Rackspace སྤྱོད་མཁན་མིང་།"

msgid ""
"Your backup appears to be corrupted.  You should delete the backup and try "
"again."
msgstr ""
"ཁྱེད་ཀྱི་གྲབས་ཉར་བརླགས་པ་འདྲ། གྲབས་ཉར་འདི་འདོར་ནས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད།"

msgid "Your files were successfully backed up and tested."
msgstr "ཁྱེད་ཀྱི་ཡིག་ཆ་གྲབས་ཉར་ལེགས་གྲུབ་ནས་ཚོད་ལྟ་ལོན་པ།"

msgid "Your files were successfully restored."
msgstr "ཁྱེད་ཀྱི་ཡིག་ཆ་སླར་གསོ་ལེགས་འགྲུབ་བྱུང་།"

msgid "_API access key"
msgstr "_API ལྟ་སྤྱོད་གསང་ལྡེ།"

msgid "_Access key ID"
msgstr "ལྟ་སྤྱོད་གསང་ལྡེ། ID(_A)"

msgid "_Add"
msgstr "སྣོན་པ། (A)"

msgid "_Allow restoring without a password"
msgstr "གསང་ཨང་མ་སྤྱོད་ནས་ཕྱིར་ལོག་པ་སླར་གསོ།(_A)"

msgid "_Automatic backup"
msgstr "གྲབས་ཉར་རང་བྱེད།(_A)"

msgid "_Back"
msgstr "ཕྱིར་ནུར།(_B)"

msgid "_Back Up Now…"
msgstr "ལམ་སེང་གྲབས་ཉར།(_B)"

msgid "_Backup location"
msgstr "གྲབས་ཉར་གནས་ས།(_B)"

msgid "_Cancel"
msgstr "དོར་བ།(_C)"

msgid "_Choose Folder…"
msgstr "ཡིག་ཁུག་འདེམ་པ།(_C)..."

msgid "_Close"
msgstr "ཁ་རྒྱག (C)"

msgid "_Container"
msgstr "སྣོད་ཆས།(_C)"

msgid "_Date"
msgstr "དུས་ཚེས།(_D)"

msgid "_Details"
msgstr "ཞིབ་ཕྲ།(_D)"

msgid "_Domain"
msgstr "ཁོངས།(_D)"

msgid "_Don't Show Again"
msgstr "གསལ་སྟོན་མི་བྱེད།(_D)"

msgid "_Every"
msgstr "ཉིན་རེར།(_E)"

msgid "_Folder"
msgstr "ཡིག་ཁུག(_F)"

msgid "_Forward"
msgstr "མདུན་སྐྱོད།(_F)"

msgid "_Help"
msgstr "རོགས་རམ།(_H)"

msgid "_Keyboard Shortcuts"
msgstr "མྱུར་མཐེབ།(_K)"

msgid "_OK"
msgstr "གཏན་འཁེལ།"

msgid "_Open Backup Settings"
msgstr "གྲབས་ཉར་སྒྲིག་འགོད་ཁ་འབྱེད།(_O)"

msgid "_Password"
msgstr "གསང་ཨང་།(_P)"

msgid "_Password-protect your backup"
msgstr "གསང་ཨང་གིས་ཁྱེད་ཀྱི་གྲབས་ཉར་སྲུང་སྐྱོབ།(_P)"

msgid "_Remember API access key"
msgstr " API ལྟ་སྤྱོད་གསང་ལྡེ་ཡིད་དུ་འཛིན།(_R)"

msgid "_Remember password"
msgstr " གསང་ཨང་བློར་ངེས།(_R)"

msgid "_Remember secret access key"
msgstr "ལྟ་སྤྱོད་གསང་ལྡེ་ཡིད་དུ་འཛིན།(_R)"

msgid "_Remove"
msgstr "སྤོ་བ།（R)"

msgid "_Restore"
msgstr "སླར་གསོ།(_R)"

msgid "_Restore…"
msgstr "སླར་གསོ།(_R)..."

msgid "_Resume Later"
msgstr "ཅུང་ཙམ་ནས་མུ་མཐུད།(_R)"

msgid "_Secret access key"
msgstr "གསང་ལྡེ་སྒེར་ལྡེ་ལྟ་སྤྱོད།(_S)"

msgid "_Show password"
msgstr "གསང་ཨང་མངོན་པ།(_S)"

msgid "_Storage location"
msgstr "གསོག་འཇོག་གནས་ས།(_S)"

msgid "_Username"
msgstr "སྤྱོད་མཁན་མིང་།(_U)"

msgctxt "back up is verb"
msgid "Back Up"
msgstr "གྲབས་ཉར།"

msgctxt "back up is verb"
msgid "_Back Up"
msgstr "གྲབས་ཉར།(_B)"

msgid "déjà;deja;dup;"
msgstr "déjà;deja;dup;གྲབས་ཉར།"

msgctxt "shortcut window"
msgid "General"
msgstr "རྒྱུན་སྲོལ།"

msgctxt "shortcut window"
msgid "Help"
msgstr "རོགས་རམ།"

msgctxt "shortcut window"
msgid "Quit"
msgstr "ཕྱིར་འབུད།"

msgid "translator-credits"
msgstr ""
"He Qiangqiang <carton@linux.net.cn>, 2002\n"
"Funda Wang <fundawang@linux.net.cn>, 2003\n"
"2004བདུད་རྩི<rhythm.gan@gmail.com>, 2009\n"
"Fan Qijiang <fqj1994@linux.com>,2010"

msgctxt "verb"
msgid "_Install"
msgstr "སྒྲིག་འཇུག་(_I)"

msgctxt "verb"
msgid "_Keep"
msgstr "རྣམ་པ་སྔོན་མ་ལྟར་འཇོག་པ།(_K)"

msgctxt "verb"
msgid "_Test"
msgstr "ཚོད་ལེན།(_T)"
