msgid ""
msgstr ""
"Project-Id-Version: Ubuntu Xenial Tibetan\n"
"Report-Msgid-Bugs-To: https://github.com/ubuntukylin-weblate/xenial-tibetan\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2021-04-26 17:31+0000\n"
"Last-Translator: Burgess Chang <register@brsvh.org>\n"
"Language-Team: Tibetan <https://weblate.ubuntukylin.com/projects/xenial-tibetan/ubuntu-help/bo_CN/>\n"
"Language: bo_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.6\n"

msgid "Access files"
msgstr "ལྟ་སྤྱོད་ཡིག་ཆ།"

msgid "Automatic"
msgstr "རང་འགུལ།"

msgid "Automatic (DHCP)"
msgstr "རང་འགུལ།(DHCP)\""

msgid "Automatic (DHCP) addresses only"
msgstr "རང་འགུལ།(DHCP)ཤག་གནས་ཁོ་ན།"

msgid "Available to all users"
msgstr "སྤྱོད་མཁན་ཚང་མར་སྤྱོད་རུང་།"

msgid "Backing up"
msgstr "གྲབས་ཉར།"

msgid "Backups"
msgstr "གྲབས་ཉར།"

msgid "Battery settings"
msgstr "གློག་རྫས་སྒྲིག་འགོད།"

msgid "Behavior"
msgstr "སྤྱོད་པ།"

msgid "Bluetooth"
msgstr "སོ་སྔོན།"

msgid "By Modification Date"
msgstr "བཟོ་བཅོས་དུས་ཚོད་ལྟར།"

msgid "By Name"
msgstr "མིང་ལྟར།"

msgid "By Size"
msgstr "ཆེ་ཆུང་ལྟར།"

msgid "By Type"
msgstr "རིགས་གྲས་ལྟར།"

msgid "Calibration"
msgstr "དག་བཅོས།"

msgid "Change the date and time"
msgstr "ཟླ་ཚེས་དང་དུས་ཚོད་བཟོ་བཅོས།"

msgid "Change the keyring password"
msgstr "གསང་ལྡེའི་གདུབ་ཀྱི་གསང་ཨང་བཟོ་བཅོས།"

msgid "Character map"
msgstr "ཡིག་རྟགས་རྟེན་འཕྲོའི་རེའུ་མིག"

msgid "Choose a secure password"
msgstr "གསང་ཨང་ཁ་སྣོན།"

msgid "Cloned MAC address"
msgstr "MAC ཤག་གནས་རྒྱུད་བཤུས།"

msgid "Color management"
msgstr "ཚོན་མདོག་དོ་དམ།"

msgid "Color profiles"
msgstr "ཚོན་མདོག་སྡེབ་སྒྲིག་ཡིག་ཆ།"

msgid "Connect automatically"
msgstr "རང་འགུལ་འབྲེལ་མཐུད།"

msgid "Contacts"
msgstr "འབྲེལ་གཏུག་པ།"

msgid "Contents"
msgstr "ནང་དོན།"

msgid "Create and delete files"
msgstr "ཡིག་ཆ་གསར་བཟོ་དང་སུབ་པ།"

msgid "Customization"
msgstr "རང་ངཟོས།"

msgid "Description"
msgstr "ཞིབ་བརྗོད།"

msgid "Desktop"
msgstr "ཅོག་ངོས།"

msgid "Destinations"
msgstr "དམིགས་ས།"

msgid "Disable touchpad while typing"
msgstr "ནང་འཇུག་སྐབས་རེག་པང་སྤྱད་མི་ཆོག"

msgid "Disabled"
msgstr "བཀག་པ།"

msgid "Documents"
msgstr "ཡིག་ཚགས།།"

msgid "FTP (with login)"
msgstr "FTP (ཐོ་འགོད་དགོས།)"

msgid "Facebook"
msgstr "Facebook"

msgid "Files"
msgstr "ཡིག་ཆ།"

msgid "Filters"
msgstr "ཡིག་ཆ།"

msgid "Flickr"
msgstr "Flickr"

msgid "Folders"
msgstr "དཀར་ཆག"

msgid "For example:"
msgstr "དཔེར་ན།"

msgid "Google"
msgstr "Google"

msgid "Group"
msgstr "ཚོགས་པ།"

msgid "Hardware"
msgstr "སྲ་ཆས།"

msgid "Hidden files"
msgstr "སྦེད་པའི་ཡིག་ཆ།"

msgid "IPv4 Settings"
msgstr "IPv4 སྒྲིག་འགོད།"

msgid "IPv6 Settings"
msgstr "IPv6 སྒྲིག་འགོད།"

msgid "Keyboard"
msgstr "མཐེབ་གཞོང་།"

msgid "Keyboard shortcuts"
msgstr "མཐེབ་གཞོང་མྱུར་མཐེབ།"

msgid "Landscape"
msgstr "འཕྲེད་ཕྱོགས།"

msgid "Language Support"
msgstr "སྐད་བརྡའི་རྒྱབ་སྐྱོར།"

msgid "Link-Local Only"
msgstr "རང་སའི་འབྲེལ་མཐུད་ཁོ་ན།"

msgid "List files only"
msgstr "ཡིག་ཆ་ཁོ་ན་འགོད་ཐུབ།"

msgid "List view"
msgstr "རེའུ་མིག་གི་མཐོང་རིས།"

msgid "Location"
msgstr "གནས་ས།"

msgid "Lock the screen"
msgstr "བརྙན་ཡོལ་དམིགས་འཛིན།"

msgid "MIME Type"
msgstr "སྨྱན་གཟུགས་ཀྱི་རིགས།"

msgid "MTU"
msgstr "བརྒྱུད་གཏོང་སྡེ་ཚན་ཆ་ཤོས།(MTU)"

msgid "Manage user accounts"
msgstr "སྤྱོད་མཁན་གྱི་རྩིས་ཐེམ་དོ་དམ།"

msgid "Manual"
msgstr "ལག་སྒུལ།"

msgid "Middle-click"
msgstr "དཀྱིལ་མཐེབ་ཆིག་རྡེབ།"

msgid "Mode"
msgstr "རྣམ་པ།"

msgid "Modified"
msgstr "བཅོས་པ།"

msgid "Mouse"
msgstr "ཙིག་མདའ།"

msgid "Mouse tips"
msgstr "ཙིག་རྟགས།"

msgid "Music players"
msgstr "གླུ་གཞས་གཏོང་ཆས།"

msgid "Name"
msgstr "མིང་།"

msgid "Network problems"
msgstr "དྲ་བའི་གནད་དོན།"

msgid "None"
msgstr "བདེ་འཇགས་སྲུང་སྐྱོང་མེད་པ།"

msgid "Notifications"
msgstr "བརྡ་ཐོ།"

msgid "Owner"
msgstr "བདག་པོ།"

msgid "Permissions"
msgstr "དབང་ཚད།"

msgid "Photos"
msgstr "འདྲ་པར།"

msgid "Port"
msgstr "མཐུད་ཁ།"

msgid "Portrait"
msgstr "གཞུང་ཕྱོགས།"

msgid "Presence"
msgstr "ཕྱི་ཚུལ།"

msgid "Previews"
msgstr "སྔོན་ལྟ།"

msgid "Printing"
msgstr "པར་འདེབས།"

msgid "Privileges"
msgstr "དབང་ཚད།"

msgid "Problems"
msgstr "གནད་དོན།"

msgid "Public FTP"
msgstr "ཡོངས་བསྒྲགས། FTP"

msgid "Questions"
msgstr "གནད་དོན།"

msgid "Resize"
msgstr "དབྱིབས་སྒྱུར་སྒྲིག་སྲོལ།"

msgid "Resolution"
msgstr "འབྱེད་ཕྱོད།"

msgid "Restart the computer."
msgstr "རྩིས་འཁོར་བསྐྱར་སློང་། "

msgid "Reverse"
msgstr "ལྡོག་ཕྱོགས།"

msgid "Reverse landscape"
msgstr "གཞུང་དུ་སྐོར་སློག"

msgid "Reverse portrait"
msgstr "གཞུང་དུ་སྐོར་སློག"

msgid "Rotation"
msgstr "འཁྱིལ་འཁོར།"

msgid "SSH"
msgstr "SSH"

msgid "Save a search"
msgstr "འཚོལ་བཤེར་ཉར་ཚགས།"

msgid "Screenshots"
msgstr "བཅད་རིས།"

msgid "Search"
msgstr "འཚོལ་བ།"

msgid "Search for files"
msgstr "འཚོལ་བཤེར་ཡིག་ཆ།"

msgid "Security"
msgstr "བདེ་འཇགས།"

msgid "Set up a printer"
msgstr "པར་འཁོར་སྒྲིག་འགོད།"

msgid "Settings"
msgstr "སྒྲིག་འགོད།"

msgid "Setup"
msgstr "སྒྲིག་འཇུག"

msgid "Sharing"
msgstr "མཉམ་སྤྱོད།"

msgid "Show hidden and backup files"
msgstr "གབ་པའི་ཡིག་ཆ་དང་གྲབས་ཉར་ཡིག་ཆ་འཆར་བ།"

msgid "Size"
msgstr "ཆེ་ཆུང་།"

msgid "Software"
msgstr "མཉེན་ཆས།"

msgid "Sound"
msgstr "སྒྲ་ནུས།"

msgid "Startup Applications"
msgstr "ཉེར་སྤྱོད་བྱ་རིམ་འགོ་སློང་།"

msgid "Suspend"
msgstr "འགེལ་འཇོག"

msgid "Switch between workspaces"
msgstr "ལས་ཁུལ་བར་བརྗེ་བ།"

msgid "System settings"
msgstr "རྒྱུད་ཁོངས་སྒྲིག་འགོད།"

msgid "Take a screenshot"
msgstr "པར་གཏུབ།"

msgid "The Desktop"
msgstr "ཅོག་ངོས།"

msgid "Time &amp; date"
msgstr "དུས་ཚོད་དང་ཚེས་གྲངས།"

msgid "Tips"
msgstr "གསལ་འདེབས།"

msgid "Trash"
msgstr "སྙིགས་སྒམ།"

msgid "Type"
msgstr "རིགས་གྲས་"

msgid "Unity Search"
msgstr "བཤེར་འཚོལ།"

msgid "Users"
msgstr "སྤྱོད་མཁན་གྱི ID"

msgid "Using the command line"
msgstr "བཀའ་བརྡའི་ཕྲེང་སྤྱོད་པ།"

msgid "Videos"
msgstr "བརྙན་ལམ།"

msgid "Volume"
msgstr "སྒྲ་ཚད།"

msgid "Web Browsers"
msgstr "Web མིག་བཤར་ཆས།"

msgid "Welcome to Ubuntu"
msgstr "Ubuntuབེད་སྤྱོད་པར་དགའ་བསུ་ཞུ།"

msgid "Windows"
msgstr "སྒེའུ་ཁུང་།"

msgid "Windows share"
msgstr "Windowsམཉམ་སྤྱོད།"

msgid "Wireless Networking"
msgstr "སྐུད་མེད་དྲ་རྒྱ།"

msgid "Workspaces"
msgstr "ལས་ཁུལ།"

msgid "c"
msgstr "c"

msgid "translator-credits"
msgstr ""
"He Qiangqiang <carton@linux.net.cn>, 2002\n"
"Funda Wang <fundawang@linux.net.cn>, 2003\n"
"2004བདུད་རྩི<rhythm.gan@gmail.com>, 2009\n"
"Fan Qijiang <fqj1994@linux.com>,2010"
