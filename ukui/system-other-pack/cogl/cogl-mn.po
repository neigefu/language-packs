msgid ""
msgstr ""
"Project-Id-Version: cogl master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug."
"cgi?product=cogl&keywords=I18N+L10N&component=general\n"
"PO-Revision-Date: 2023-06-01 12:22+0000\n"
"Last-Translator: KevinDuan <duankaiwen@kylinos.com>\n"
"Language-Team: Mongolian <http://weblate.openkylin.top/projects/test-for-mn/"
"cogl/mn/>\n"
"Language: mn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.1-dev\n"
"X-Launchpad-Export-Date: 2020-04-16 19:43+0000\n"

msgid "Add wire outlines for all geometry"
msgstr "ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠭᠧᠦᠮᠧᠲ᠋ᠷ ᠦᠨ ᠵᠢᠷᠤᠭ ᠲᠤ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠤᠲᠠᠰᠤᠨ ᠤ ᠬᠦᠷᠳᠦ ᠨᠡᠮᠡᠵᠦ ᠥᠭ᠍ᠬᠦ"

msgid "Add wire outlines for all rectangular geometry"
msgstr ""
"ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠹᠣᠰᠹᠣᠷ ᠬᠡᠯᠪᠡᠷᠢᠲᠦ ᠭᠧᠦᠮᠧᠲ᠋ᠷ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠳᠦ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠤᠲᠠᠰᠤᠨ ᠤ ᠬᠦᠷᠳᠦ "
"ᠨᠡᠮᠡᠵᠦ ᠥᠭ᠍ᠬᠦ"

msgid "Additional environment variables:"
msgstr "ᠪᠤᠰᠤᠳ ᠣᠷᠴᠢᠨ ᠲᠣᠭᠣᠷᠢᠨ ᠤ ᠬᠤᠪᠢᠷᠠᠯᠲᠠ"

msgid "Cogl Options"
msgstr "Cogl ᠰᠣᠩᠭᠣᠯᠲᠠ ᠃"

msgid "Cogl Specialist"
msgstr "ᠬᠤᠷᠴᠢᠨᠠᠷ ᠲᠤᠰᠬᠠᠢ ᠮᠡᠷᠭᠡᠵᠢᠯᠲᠡᠨ᠃"

msgid "Cogl Tracing"
msgstr "Cogl Tracee ( ᠤᠳᠠᠭᠠᠰᠢᠷᠠᠯ )"

msgid "Cogl debugging flags to set"
msgstr "ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ Cogl ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢᠨ ᠲᠡᠮᠳᠡᠭ ᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃"

msgid "Cogl debugging flags to unset"
msgstr ""
"ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ Cogl ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠲᠤᠷᠰᠢᠬᠤ ᠲᠡᠮᠳᠡᠭ ᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠬᠤ "
"ᠬᠡᠷᠡᠭᠲᠡᠶ᠃"

msgid "CoglObject references"
msgstr "CoglObject ᠡᠰᠢ ᠲᠠᠲᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃"

msgid "Comma-separated list of GL extensions to pretend are disabled"
msgstr ""
"ᠱᠣᠭ ᠢᠶᠡᠷ ᠰᠠᠯᠭᠠᠭᠰᠠᠨ GL ᠪᠠᠷ ᠢᠶᠠᠨ ᠥᠷᠭᠡᠳᠬᠡᠬᠦ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠪᠡᠷ ᠬᠠᠭᠤᠷᠮᠠᠭ ᠢᠶᠠᠷ "
"ᠬᠤᠪᠴᠠᠰᠤᠯᠠᠬᠤ ᠶᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠴᠠᠭᠠᠵᠠᠯᠠᠵᠠᠢ ᠃"

msgid "Debug CoglBlendString parsing"
msgstr "CogleedString ᠵᠠᠳᠠᠯᠤᠯᠲᠠ ᠶᠢ ᠲᠤᠷᠰᠢᠨ᠎ᠠ ᠃"

msgid "Debug offscreen support"
msgstr "ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠭᠠᠳᠠᠨᠠᠬᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠲᠤᠷᠰᠢᠬᠤ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠦᠵᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃"

msgid "Debug ref counting issues for CoglObjects"
msgstr "CoglObjects ᠶᠢᠨ ᠡᠰᠢᠯᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠲᠣᠭ᠎ᠠ ᠶᠢᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ ᠲᠤᠷᠰᠢᠨ ᠲᠤᠷᠰᠢᠨ᠎ᠠ ᠃"

msgid "Debug texture atlas management"
msgstr "ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠵᠢᠷᠤᠭ ᠢ ᠲᠥᠪᠯᠡᠷᠡᠭᠦᠯᠦᠨ ᠬᠠᠮᠢᠶᠠᠷᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃"

msgid "Disable GL Pixel Buffers"
msgstr "GL ᠳᠦᠷᠰᠦᠲᠦ ᠪᠣᠰᠣᠭ᠎ᠠ ᠵᠠᠪᠰᠠᠷ ᠤᠨ ᠣᠷᠣᠨ ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable GL Vertex Buffers"
msgstr "GL ᠶᠢᠨ ᠣᠷᠣᠢ ᠶᠢ ᠠᠭᠠᠵᠢᠮ ᠢᠶᠠᠷ ᠵᠠᠪᠰᠠᠷᠯᠠᠬᠤ ᠣᠷᠣᠨ ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable GLSL"
msgstr "GLSL ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable Journal batching"
msgstr "ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢᠶᠡᠷ ᠰᠢᠶᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable arbfp"
msgstr "arbfp ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable batching of geometry in the Cogl Journal."
msgstr ""
"Cogl ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠦᠨ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠭᠧᠦᠮᠧᠲ᠋ᠷ ᠦᠨ ᠪᠥᠭᠡᠮ ᠢᠶᠡᠷ ᠰᠢᠶᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠶᠢ "
"ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable blending"
msgstr "ᠬᠣᠯᠢᠯᠳᠤᠭᠤᠯᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable fallback caches for arbfp and glsl programs"
msgstr ""
"arbxp ᠪᠣᠯᠣᠨ glsl ᠫᠷᠦᠭᠷᠠᠮ ᠤᠨ ᠡᠭᠡᠭᠦᠯᠦᠨ ᠤᠬᠤᠷᠢᠵᠤ ᠤᠬᠤᠷᠢᠵᠤ ᠤᠬᠤᠷᠢᠭᠤᠯᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ "
"ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable fixed"
msgstr "ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ ᠲᠣᠭᠲᠠᠮᠠᠯ ᠃"

msgid "Disable non-power-of-two textures"
msgstr "ᠬᠣᠶᠠᠳᠠᠬᠢ ᠤᠳᠠᠭᠠᠨ ᠤ ᠪᠤᠰᠤ ᠵᠢᠵᠢᠭ ᠶᠣᠰᠣ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid ""
"Disable optimization for reading 1px for simple scenes of opaque rectangles"
msgstr "ᠲᠤᠩᠭᠠᠴᠠ ᠦᠭᠡᠢ ᠳᠦᠭᠦᠮ ᠦᠵᠡᠭᠳᠡᠯ ᠢ ᠤᠩᠰᠢᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ᠃"

msgid "Disable program caches"
msgstr "ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ ᠬᠣᠶᠢᠰᠢᠯᠠᠭᠤᠯᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable read pixel optimization"
msgstr "ᠳᠦᠷᠰᠦᠲᠦ ᠵᠢᠷᠤᠭᠲᠤ ᠪᠣᠳᠠᠰ ᠢ ᠤᠩᠰᠢᠵᠤ ᠰᠢᠯᠢᠳᠡᠭᠵᠢᠭᠦᠯᠬᠦ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable sharing the texture atlas between text and images"
msgstr ""
"ᠪᠢᠴᠢᠭ᠌ ᠪᠣᠯᠣᠨ ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ ᠪᠠᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠶᠢ "
"ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable software clipping"
msgstr "ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠢᠶᠠᠷ ᠯᠣᠭᠢᠭ ᠬᠠᠢ᠌ᠴᠢᠯᠠᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable software rect transform"
msgstr "ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable texture atlasing"
msgstr "ᠬᠣᠷᠢᠭ᠎ᠠ ᠶᠢᠨ ᠵᠢᠷᠤᠭ ᠢᠶᠠᠷ ᠵᠠᠳᠠᠯᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable texturing"
msgstr "ᠬᠣᠷᠢᠭ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable texturing any primitives"
msgstr "ᠶᠠᠮᠠᠷᠪᠠ ᠰᠠᠭᠤᠷᠢ ᠲᠥᠭᠥᠷᠢᠭ ᠦᠨ ᠨᠡᠬᠡᠮᠡᠯ ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable use of ARB fragment programs"
msgstr "ARB ᠬᠡᠰᠡᠭ ᠦᠨ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable use of GLSL"
msgstr "GLSL ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable use of OpenGL pixel buffer objects"
msgstr "OpegL ᠳᠦᠷᠰᠦᠲᠦ ᠪᠣᠰᠣᠨ ᠤ ᠵᠠᠪᠰᠠᠷᠲᠤ ᠣᠷᠣᠨ ᠤ ᠡᠲᠡᠭᠡᠳ ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable use of OpenGL vertex buffer objects"
msgstr ""
"OpegL ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠣᠷᠣᠢ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠢ ᠠᠭᠠᠵᠢᠮ ᠢᠶᠠᠷ ᠣᠷᠣᠨ ᠤ ᠡᠲᠡᠭᠡᠳ ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable use of blending"
msgstr "ᠬᠣᠯᠢᠯᠳᠤᠭᠤᠯᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable use of texture atlasing"
msgstr "ᠬᠣᠷᠢᠭ᠎ᠠ ᠶᠢᠨ ᠵᠢᠷᠤᠭ ᠢᠶᠠᠷ ᠵᠠᠳᠠᠯᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disable use of the fixed function pipeline backend"
msgstr "ᠲᠣᠭᠲᠠᠮᠠᠯ ᠹᠦᠩᠺᠼᠢ ᠬᠣᠭᠣᠯᠠᠶ ᠶᠢᠨ ᠠᠷᠤ ᠲᠠᠯ᠎ᠠ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Disables Cogl's attempts to clip some rectangles in software."
msgstr ""
"Cogl ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠵᠤ ᠵᠥᠭᠡᠯᠡᠨ ᠲᠣᠨᠣᠭ ᠲᠤ ᠵᠠᠷᠢᠮ ᠹᠣᠰᠹᠣᠷ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢ ᠬᠠᠢ᠌"
"ᠴᠢᠯᠠᠬᠤ ᠶᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Dump atlas images"
msgstr "ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢ ᠡᠷᠭᠢᠭᠦᠯᠦᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃"

msgid "Dump texture atlas changes to an image file"
msgstr "ᠵᠢᠷᠤᠭ ᠢ ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦᠲᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠤ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠨ᠎ᠡ ᠃"

msgid "Enables all non-behavioural debug options"
msgstr ""
"ᠪᠤᠶ ᠪᠦᠬᠦᠢ ᠦᠢᠯᠡ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠶᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠲᠤᠷᠰᠢᠬᠤ ᠰᠣᠩᠭᠣᠯᠲᠠ ᠶᠢ ᠡᠭᠢᠯᠡᠨ "
"ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠃"

msgid "Logs information about how Cogl is implementing clipping"
msgstr ""
"Cogl ᠬᠠᠶᠢᠴᠢᠯᠠᠬᠤ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠶᠢ ᠬᠡᠷᠬᠢᠨ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠬᠦ ᠲᠤᠬᠠᠶ "
"ᠲᠡᠮᠳᠡᠭᠯᠡᠯ"

msgid "Outline rectangles"
msgstr "ᠬᠦᠷᠳᠦᠨ ᠳᠦ ᠪᠤᠯᠠᠭ ᠬᠡᠯᠪᠡᠷᠢ ᠲᠡᠢ ᠃"

msgid "Override the GL version that Cogl will assume the driver supports"
msgstr ""
"Cogl ᠬᠠᠮᠤᠷᠴᠤ ᠬᠠᠭᠤᠷᠮᠠᠭ ᠢᠶᠠᠷ ᠬᠥᠳᠡᠯᠭᠡᠭᠦᠷ ᠪᠣᠯᠭᠠᠬᠤ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠤᠨ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠶᠢ "
"ᠳᠡᠮᠵᠢᠬᠦ GL ᠬᠡᠪᠯᠡᠯ ᠢ ᠬᠠᠮᠤᠷᠤᠨ᠎ᠠ ᠃"

msgid "Root Cause"
msgstr "ᠦᠨᠳᠦᠰᠦᠨ ᠦ ᠡᠬᠢ ᠰᠤᠷᠪᠤᠯᠵᠢ"

msgid "Show Cogl options"
msgstr "Cogl ᠰᠣᠩᠭᠣᠯᠲᠠ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ ᠃"

msgid "Show generated ARBfp/GLSL source code"
msgstr "ᠡᠭᠦᠰᠦᠭᠰᠡᠨ ARBxy/GLSLL ᠶᠢᠨ ᠢᠷᠡᠯᠲᠡ ᠶᠢᠨ ᠺᠣᠳ᠋ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ ᠃"

msgid "Show how geometry is being batched in the journal"
msgstr ""
"ᠶᠠᠫᠣᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠳᠡᠬᠢ ᠪᠥᠭᠡᠮ ᠢᠶᠡᠷ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠶ ᠃"

msgid "Show source"
msgstr "ᠢᠷᠡᠯᠲᠡ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ ᠃"

msgid "Show wireframes"
msgstr "ᠤᠲᠠᠰᠤᠨ ᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃"

msgid "Special debug values:"
msgstr "ᠣᠨᠴᠣᠭᠣᠶ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠄"

msgid "Supported debug values:"
msgstr "ᠳᠡᠮᠵᠢᠯᠭᠡ ᠶᠢᠨ ᠲᠤᠷᠰᠢᠯᠲᠠ ᠶᠢᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠄"

msgid "Trace Atlas Textures"
msgstr "ᠵᠢᠷᠤᠭ ᠤᠨ ᠴᠤᠭᠯᠠᠭᠤᠯᠬᠤ ᠵᠢᠷᠤᠭ ᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Trace Batching"
msgstr "ᠮᠥᠷᠳᠡᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠵᠦ ᠰᠢᠶᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃"

msgid "Trace Blend Strings"
msgstr "ᠮᠥᠷᠳᠡᠬᠦ ᠦᠰᠦᠭ ᠨᠢ ᠲᠡᠮᠲᠡᠭ ᠲᠡᠢ ᠨᠡᠢ᠌ᠴᠡᠨ᠎ᠡ ᠃"

msgid "Trace CoglTexturePixmap backend"
msgstr "CoglTextureeimap ᠶᠢᠨ ᠠᠷᠤ ᠲᠠᠯ᠎ᠠ ᠶᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Trace Journal"
msgstr "ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Trace Misc Drawing"
msgstr "ᠮᠥᠷᠳᠡᠨ ᠰᠤᠯᠠᠷᠠᠭᠤᠯᠬᠤ ᠵᠦᠢᠯ ᠦᠨ ᠵᠢᠷᠤᠭ ᠵᠢᠷᠤᠬᠤ"

msgid "Trace Pango Renderer"
msgstr "ᠫᠠᠨ ᠭᠧ ᠶᠢᠨ ᠤᠬᠢᠶᠠᠯᠭ᠎ᠠ ᠶᠢᠨ ᠪᠠᠭᠠᠵᠢ ᠶᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Trace Texture Slicing"
msgstr "ᠮᠥᠷᠳᠡᠨ ᠣᠷᠣᠮᠰᠢᠭᠤᠯᠬᠤ ᠹᠢᠯᠢᠮ ᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Trace all matrix manipulation"
msgstr "ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠹᠣᠰᠹᠣᠷ ᠢᠶᠠᠷ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃"

msgid "Trace clipping"
msgstr "ᠮᠥᠷᠳᠡᠨ ᠬᠠᠢ᠌ᠴᠢᠯᠠ ᠃"

msgid "Trace matrices"
msgstr "ᠣᠷᠣᠮ ᠤᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ ᠹᠣᠰᠹᠣᠷ ᠤᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠃"

msgid "Trace offscreen support"
msgstr "ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠭᠠᠳᠠᠨ᠎ᠠ ᠮᠥᠷᠳᠡᠨ ᠳᠡᠮᠵᠢᠨ᠎ᠡ ᠃"

msgid "Trace performance concerns"
msgstr "ᠮᠥᠷᠳᠡᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠶᠢᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠃"

msgid "Trace some OpenGL"
msgstr "ᠵᠠᠷᠢᠮ OpeegL ᠶᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Trace some misc drawing operations"
msgstr "ᠵᠠᠷᠢᠮ ᠰᠤᠯᠠ ᠵᠦᠢᠯ ᠢ ᠮᠥᠷᠳᠡᠨ ᠦᠢᠯᠡᠳᠴᠦ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃"

msgid "Trace the Cogl Pango renderer"
msgstr "Cogl Pango ᠤᠬᠢᠶᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠶᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Trace the Cogl texture pixmap backend"
msgstr "Cogl ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦᠲᠦ ᠵᠢᠷᠤᠭ ᠢ ᠮᠥᠷᠳᠡᠨ᠎ᠡ ᠃"

msgid "Traces some select OpenGL calls"
msgstr "ᠵᠠᠷᠢᠮ ᠰᠤᠩᠭ᠋ᠤᠭᠳᠠᠭᠰᠠᠨ OpegL ᠶᠢ ᠮᠥᠷᠳᠡᠵᠦ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃"

msgid "Tries to highlight sub-optimal Cogl usage."
msgstr ""
"ᠳᠡᠯᠭᠡᠴᠡ ᠨᠢ ᠤᠳᠠᠭ᠎ᠠ ᠶᠢᠨ ᠰᠢᠯᠢᠳᠡᠭ Cogl ᠶᠢ ᠲᠣᠪᠣᠶᠢᠯᠭᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠶᠢ ᠲᠣᠪᠣᠶᠢᠯᠭᠠᠨ᠎ᠠ ᠃"

msgid "Use the GPU to transform rectangular geometry"
msgstr ""
"GPU ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠹᠣᠰᠹᠣᠷ ᠬᠡᠯᠪᠡᠷᠢᠲᠦ ᠭᠧᠦᠮᠧᠲ᠋ᠷ ᠦᠨ ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃"

msgid "View all the geometry passing through the journal"
msgstr ""
"ᠲᠡᠩᠭᠡᠯᠢᠭ ᠢᠶᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠬᠦᠵᠦᠭᠦᠦ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠠᠯᠢᠪᠠ ᠭᠧᠦᠮᠧᠲ᠋ᠷ ᠦᠨ ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢ "
"ᠦᠵᠡᠨ᠎ᠡ ᠃"

msgid "Visualize"
msgstr "ᠦᠵᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃"

msgid "debug the creation of texture slices"
msgstr "ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠣᠷᠣᠮ ᠤᠨ ᠬᠡᠷᠴᠢᠮ ᠢ ᠡᠭᠦᠳᠦᠨ ᠪᠠᠶᠢᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃"
