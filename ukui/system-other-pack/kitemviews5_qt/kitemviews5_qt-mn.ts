<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>KTreeWidgetSearchLine</name>
    <message>
        <source>Search Columns</source>
        <translation>ᠡᠷᠢᠯᠲᠡ ᠶᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <source>Search...</source>
        <translation>ᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>All Visible Columns</source>
        <translation>ᠠᠯᠢᠪᠠ ᠬᠠᠷᠠᠭᠳᠠᠬᠤ ᠡᠩᠨᠡᠭᠡᠨ ᠳᠦ ᠃</translation>
    </message>
</context>
<context>
    <name>KFilterProxySearchLine</name>
    <message>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ ᠃</translation>
    </message>
</context>
</TS>
