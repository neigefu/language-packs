<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>KTreeWidgetSearchLine</name>
    <message>
        <source>Search Columns</source>
        <translation>འཚོལ་བཤེར་ཆེད་སྒྲིག་ལེ་ཚན།</translation>
    </message>
    <message>
        <source>Search...</source>
        <translation>འཚོལ་ཞིབ།...</translation>
    </message>
    <message>
        <source>All Visible Columns</source>
        <translation>མཐོང་ཐུབ་པའི་ཀ་བ་ཡོད་ཚད།</translation>
    </message>
</context>
<context>
    <name>KFilterProxySearchLine</name>
    <message>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
</context>
</TS>
