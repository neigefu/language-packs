<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>KStandardGuiItem</name>
    <message>
        <source>No</source>
        <translation>ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>《ᠲᠣᠭᠲᠠᠭᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>ᠡᠰᠡ ᠲᠡᠭᠡᠪᠡᠯ</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>ᠨᠡᠮᠡ ᠃</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>ᠲᠡᠢ᠌ᠮᠦ ᠡ ᠃</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>ᠲᠡᠢ᠌ᠮᠦ ᠡ ᠃</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>ᠵᠣᠭᠰᠣ ᠃</translation>
    </message>
    <message>
        <source>Test</source>
        <translation>ᠰᠢᠯᠭᠠᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation>ᠡᠶ᠎ᠡ ᠨᠠᠶᠢᠷᠠᠮᠳᠠᠭᠤ ᠪᠤᠴᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠯᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <source>Discard changes</source>
        <translation>ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠪᠡᠨ ᠪᠣᠯᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Reset all items to their default values</source>
        <translation>ᠪᠤᠶ ᠪᠦᠬᠦᠢ ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠦᠨ ᠳᠠᠬᠢᠨ ᠳᠠᠪᠬᠤᠷ ᠢ ᠲᠡᠭᠦᠨ ᠦ ᠳᠤᠭᠤᠶᠢᠳᠠᠮ ᠤᠨ ᠮᠡᠳᠡᠷᠡᠯ ᠦᠨ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠪᠣᠯᠭᠠᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Close the current document.</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠳᠠᠩᠰᠠ ᠡᠪᠬᠡᠮᠡᠯ ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Apply changes</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <source>Save file with another name</source>
        <translation>ᠪᠤᠰᠤᠳ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Clear the input in the edit field</source>
        <translation>ᠨᠠᠢ᠌ᠷᠠᠭᠤᠯᠤᠭᠴᠢ ᠦᠰᠦᠭ᠍ ᠦᠨ ᠬᠡᠰᠡᠭ᠍ ᠦᠨ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠶᠢ ᠠᠷᠢᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>ᠭᠤᠶᠤᠴᠢᠯᠠᠯ᠃</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>ᠡᠭᠦᠳᠡ ᠶᠢ ᠬᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation>ᠡᠶ᠎ᠡ ᠨᠠᠶᠢᠷᠠᠮᠳᠠᠭᠤ ᠴᠢᠬᠤᠯᠠ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤ</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠠ ᠠᠵᠢᠯ ᠁</translation>
    </message>
    <message>
        <source>Clear input</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠶᠢ ᠠᠷᠢᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>C&amp;lear</source>
        <translation>ᠴ ᠯᠢᠯ ᠃</translation>
    </message>
    <message>
        <source>Opens the print dialog to print the current document</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠣᠳᠣᠬᠠᠨ ᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠳᠠᠩᠰᠠ ᠡᠪᠬᠡᠮᠡᠯ ᠢ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>When you click &lt;b&gt;Apply&lt;/b&gt;, the settings will be handed over to the program, but the dialog will not be closed.
Use this to try different settings.</source>
        <translation>ᠲᠠ ᠳᠠᠩ ᠴᠣᠬᠢᠵᠤ 〔 X15X 〕 ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ 〔 X23X 〕 ᠶᠢ ᠳᠠᠭᠠᠷᠢᠬᠤ ᠦᠶ᠎ᠡ ᠳᠦ ᠂ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠫᠷᠤᠭ᠍ᠷᠠᠮ ᠳᠤ ᠰᠢᠯᠵᠢᠭᠦᠯᠦᠨ ᠲᠤᠰᠢᠶᠠᠨ᠎ᠠ ᠂ ᠭᠡᠪᠡᠴᠦ ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠠᠭᠠᠬᠤ ᠦᠭᠡᠢ ᠃
ᠡᠨᠡ ᠰᠣᠩᠭᠣᠯᠲᠠ ᠶᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠳᠦ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠲᠤᠷᠰᠢᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Close the current window or document</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ ᠴᠣᠩᠬᠣ ᠪᠤᠶᠤ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Confi&amp;gure...</source>
        <translation>ᠺᠠᠩ ᠹᠧᠢ ᠁</translation>
    </message>
    <message>
        <source>Close the current window.</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠤ ᠴᠣᠩᠬᠣ ᠶᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>&amp;Do Not Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠦᠭᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>ᠬᠠᠰᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Enter Administrator Mode</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠲᠤ ᠣᠷᠣᠬᠤ</translation>
    </message>
    <message>
        <source>C&amp;ontinue</source>
        <translation>C&amp;ontinue</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>᠁ ᠨᠡᠭᠡᠭᠡᠨ ᠁</translation>
    </message>
    <message>
        <source>Show help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠯᠭ᠎ᠠ ᠦᠵᠡᠭᠦᠯᠦᠭᠰᠡᠨ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎</translation>
    </message>
    <message>
        <source>&amp;Close Document</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠦᠨ ᠳᠠᠩᠰᠠ ᠡᠪᠬᠡᠮᠡᠯ ᠢ ᠬᠠᠭᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>ᠴᠢᠳᠠᠪᠬᠢ ᠃</translation>
    </message>
    <message>
        <source>Pressing this button will discard all recent changes made in this dialog.</source>
        <translation>ᠡᠨᠡ ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠳᠠᠷᠤᠬᠤ ᠨᠢ ᠡᠨᠡᠬᠦ ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠣᠶᠢᠷ᠎ᠠ ᠳᠤ ᠬᠢᠭ᠍ᠰᠡᠨ ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠯᠲᠡ ᠪᠡᠨ ᠲᠠᠶᠠᠭᠳᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Go forward one step</source>
        <translation>ᠤᠷᠤᠭᠰᠢ ᠨᠢᠭᠡ ᠠᠯᠬᠤᠮ ᠶᠠᠪᠤ ᠃</translation>
    </message>
    <message>
        <source>When you click &lt;b&gt;Administrator Mode&lt;/b&gt; you will be prompted for the administrator (root) password in order to make changes which require root privileges.</source>
        <translation>ᠲᠠ ᠳᠠᠩ ᠴᠣᠬᠢᠯᠲᠠ ᠥᠭ᠍ᠬᠦ ᠦᠶ᠎ᠡ ᠳᠦ ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠨᠢ ᠲᠠᠨ ᠤ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ( root ) ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠶᠢ ᠰᠠᠨᠠᠭᠤᠯᠵᠤ ᠂ root ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠦᠨ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠬᠢᠬᠦ ᠳᠦ ᠳᠥᠭᠥᠮ ᠦᠵᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>ᠪᠠ ᠳᠠᠷᠤᠮᠠᠯ ᠁</translation>
    </message>
    <message>
        <source>Do not save data</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠦᠭᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Quit application</source>
        <translation>ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢ ᠪᠤᠴᠠᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>&amp;Insert</source>
        <translation>ᠬᠠᠳᠬᠤᠨ ᠣᠷᠣᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation>ᠬᠠᠶᠠ ᠃</translation>
    </message>
    <message>
        <source>Administrator &amp;Mode...</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠪᠠ ᠵᠠᠭᠪᠤᠷ ᠤᠨ ᠁</translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation>ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠦᠨ᠎ᠡ ᠬᠠᠩᠰᠢ ᠲᠠᠢ ᠪᠠᠨ ᠮᠡᠳᠡᠷᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Go back one step</source>
        <translation>ᠬᠣᠢ᠌ᠰᠢ ᠨᠢᠭᠡ ᠠᠯᠬᠤᠮ ᠤᠬᠤᠷᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Save data</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠶᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Delete item(s)</source>
        <translation>ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠢ ᠬᠠᠰᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Overwrite</source>
        <translation>ᠪᠦᠷᠬᠦᠬᠦ᠃</translation>
    </message>
    <message>
        <source>&amp;Forward</source>
        <translation>ᠡᠶ᠎ᠡ ᠲᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠶᠠᠪᠤᠭᠤᠯᠬᠤ᠃</translation>
    </message>
    <message>
        <source>Continue operation</source>
        <translation>ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Reset configuration</source>
        <translation>ᠳᠠᠬᠢᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>&amp;Close Window</source>
        <translation>ᠴᠣᠩᠬᠣ ᠶᠢ ᠬᠠᠭᠠᠪᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KCharSelectData</name>
    <message>
        <source>Lao</source>
        <translation>ᠯᠠᠤᠰ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>NKo</source>
        <translation>NKo</translation>
    </message>
    <message>
        <source>Vai</source>
        <translation>ᠸᠠ ᠢ ᠃</translation>
    </message>
    <message>
        <source>Cham</source>
        <translation>ᠵᠠᠯᠠᠭᠤᠷ᠃</translation>
    </message>
    <message>
        <source>Lisu</source>
        <translation>ᠷᠧᠨ ᠹᠧᠩ ᠦᠨᠳᠦᠰᠦᠲᠡᠨ᠃</translation>
    </message>
    <message>
        <source>Thai</source>
        <translation>ᠲᠠᠶ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <source>CJK Compatibility Ideographs</source>
        <translation>ᠳᠤᠮᠲᠠᠲᠦ ᠶᠠᠫᠤᠨ ᠺᠤᠷᠢᠶᠠ ᠶᠢᠨ ᠪᠠᠭᠲᠠᠭᠠᠮᠵᠢᠲᠤ ᠴᠢᠨᠠᠷ ᠤᠨ ᠢᠯᠡᠳᠬᠡᠯ ᠤᠨ ᠰᠠᠨᠠᠭ᠎ᠠ ᠶᠢ ᠬᠠᠪᠰᠤᠷᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Cyrillic Extended-A</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠯᠢᠷᠸᠧᠨ ᠦ ᠥᠷᠭᠡᠳᠴᠡ ᠠ ᠃</translation>
    </message>
    <message>
        <source>Cyrillic Extended-B</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠯᠢᠷᠸᠧᠨ ᠦ ᠥᠷᠭᠡᠳᠬᠡᠯᠲᠡ —ᠪ᠃</translation>
    </message>
    <message>
        <source>Cyrillic Extended-C</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠯᠢᠷᠸᠧᠨ ᠦ ᠥᠷᠭᠡᠳᠬᠡᠯᠲᠡ —C ᠃</translation>
    </message>
    <message>
        <source>Bamum</source>
        <translation>ᠪᠠᠮᠤ᠃</translation>
    </message>
    <message>
        <source>Batak</source>
        <translation>ᠪᠠᠲ᠋ᠠᠺ ᠃</translation>
    </message>
    <message>
        <source>Buhid</source>
        <translation>ᠪᠦᠰᠢᠳ᠋ ᠃</translation>
    </message>
    <message>
        <source>Dingbats</source>
        <translation>ᠳ᠋ᠢᠩ ᠪᠢᠶᠣᠤ ᠃</translation>
    </message>
    <message>
        <source>Khmer</source>
        <translation>ᠥᠨᠳᠥᠷ ᠬᠥᠪᠥᠩ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Limbu</source>
        <translation>ᠯᠢᠨ ᠪᠦ᠋᠃</translation>
    </message>
    <message>
        <source>Ogham</source>
        <translation>ᠧᠦᠢᠷᠦᠫᠡ ᠃</translation>
    </message>
    <message>
        <source>Oriya</source>
        <translation>ᠣᠷᠢᠶ᠎ᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ ᠃</translation>
    </message>
    <message>
        <source>Runic</source>
        <translation>ᠹᠦ ᠸᠧᠨ ᠃</translation>
    </message>
    <message>
        <source>Tamil</source>
        <translation>ᠲᠠᠮᠢᠯ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <source>IPA Extensions</source>
        <translation>ᠣᠯᠠᠨ ᠤᠯᠤᠰ ᠤᠨ ᠳᠠᠭᠤᠨ ᠲᠡᠮᠳᠡᠭ ᠦᠨ ᠥᠷᠭᠡᠳᠬᠦ ᠶᠢ ᠥᠷᠭᠡᠳᠬᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <source>Miscellaneous Mathematical Symbols-A</source>
        <translation>ᠰᠤᠯᠠ ᠲᠣᠭᠠᠨ ᠤ ᠤᠬᠠᠭᠠᠨ ᠤ ᠲᠡᠮᠳᠡᠭ — A ᠃</translation>
    </message>
    <message>
        <source>Miscellaneous Mathematical Symbols-B</source>
        <translation>ᠰᠤᠯᠠ ᠲᠣᠭᠠᠨ ᠤ ᠤᠬᠠᠭᠠᠨ ᠤ ᠲᠡᠮᠳᠡᠭ — B ᠃</translation>
    </message>
    <message>
        <source>Halfwidth and Fullwidth Forms</source>
        <translation>ᠬᠠᠭᠠᠰ ᠥᠨᠴᠥᠭ᠍ ᠪᠣᠯᠣᠨ ᠪᠥᠬᠥ ᠥᠨᠴᠥᠭ᠍ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Hiragana</source>
        <translation>ᠬᠠᠭᠤᠷᠮᠠᠭ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Cyrillic</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨᠰᠢᠷ ᠃</translation>
    </message>
    <message>
        <source>Punctuation, Other</source>
        <translation>ᠴᠡᠭ ᠦᠨ ᠲᠡᠮᠳᠡᠭ ᠂ ᠪᠤᠰᠤᠳ ᠃</translation>
    </message>
    <message>
        <source>Punctuation, Close</source>
        <translation>ᠴᠡᠭ ᠲᠡᠮᠲᠡᠭ ᠢ ᠬᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Enclosed Alphanumeric Supplement</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠤᠭᠤᠯᠤᠨ ᠦᠰᠦᠭ ᠦᠨ ᠴᠠᠭᠠᠨ ᠲᠣᠭᠠᠨ ᠤ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Unified Canadian Aboriginal Syllabics</source>
        <translation>ᠺᠠᠨᠠᠳᠠ ᠶᠢᠨ ᠨᠤᠲᠤᠭ ᠳᠡᠪᠢᠰᠬᠡᠷ ᠦᠨ ᠨᠢᠭᠡᠳᠦᠯᠲᠡᠢ ᠠᠪᠢᠶ᠎ᠠ ᠶᠢᠨ ᠪᠠᠶᠠᠷ ᠃</translation>
    </message>
    <message>
        <source>High Private Use Surrogates</source>
        <translation>ᠦᠨᠳᠦᠷ ᠬᠤᠪᠢ ᠬᠦᠮᠦᠨ ᠣᠷᠣᠯᠠᠬᠤ ᠡᠳ᠋ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <source>Number Forms</source>
        <translation>ᠲᠣᠭᠠᠴᠢᠯᠠᠭᠰᠠᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>African Scripts</source>
        <translation>ᠠᠹᠷᠢᠺᠠ ᠲᠢᠪ ᠦᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠃</translation>
    </message>
    <message>
        <source>American Scripts</source>
        <translation>ᠠᠮᠧᠷᠢᠺᠠ ᠮᠠᠶᠢᠭ ᠤᠨ ᠵᠦᠴᠦᠭᠡ ᠶᠢᠨ ᠳᠡᠪᠲᠡᠷ ᠃</translation>
    </message>
    <message>
        <source>Kannada</source>
        <translation>ᠺᠠᠨᠠᠯᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Transport and Map Symbols</source>
        <translation>ᠵᠠᠮ ᠬᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠪᠣᠯᠣᠨ ᠭᠠᠵᠠᠷ ᠤᠨ ᠵᠢᠷᠤᠭ ᠤᠨ ᠲᠡᠮᠳᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Mark, Spacing Combining</source>
        <translation>ᠲᠡᠮᠳᠡᠭ᠍ ᠂ ᠵᠠᠢ ᠶᠢᠨ ᠨᠡᠢ᠌ᠯᠡᠮᠡᠯ ᠃</translation>
    </message>
    <message>
        <source>Letter, Lowercase</source>
        <translation>ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠂ ᠪᠢᠴᠢᠬᠠᠨ ᠪᠢᠴᠢᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Unified Canadian Aboriginal Syllabics Extended</source>
        <translation>ᠺᠠᠨᠠᠳᠠ ᠶᠢᠨ ᠨᠤᠲᠤᠭ ᠳᠡᠪᠢᠰᠬᠡᠷ ᠦᠨ ᠨᠢᠭᠡᠳᠦᠯᠲᠡᠢ ᠠᠶ᠎ᠠ ᠥᠷᠭᠡᠳᠴᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Block Elements</source>
        <translation>ᠬᠡᠰᠡᠭ᠍ ᠡᠯᠧᠮᠧᠨᠲ᠋ ᠃</translation>
    </message>
    <message>
        <source>Georgian Extended</source>
        <translation>ᠭᠷᠦᠢᠽᠢᠶᠠ ᠬᠡᠯᠡᠨ ᠦ ᠥᠷᠭᠡᠳᠴᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Syriac Supplement</source>
        <translation>ᠰᠢᠷᠢᠶ᠎ᠠ ᠬᠡᠯᠡ ᠪᠡᠷ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks for Symbols</source>
        <translation>ᠲᠡᠮᠳᠡᠭ᠍ ᠦᠨ ᠳᠠᠭᠤᠨ ᠤ ᠲᠡᠮᠳᠡᠭ᠍ ᠢ ᠨᠡᠢ᠌ᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Currency Symbols</source>
        <translation>ᠵᠣᠭᠣᠰ ᠤᠨ ᠲᠡᠮᠳᠡᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>Separator, Paragraph</source>
        <translation>ᠮᠢᠨᠦᠢᠲ᠋ ᠦᠨ ᠲᠤᠰᠠᠭᠠᠷᠯᠠᠯ ᠳᠤ ᠨᠡᠢ᠌ᠴᠡᠵᠦ ᠂ ᠬᠡᠰᠡᠭ᠍ ᠤᠨᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Separator, Line</source>
        <translation>ᠮᠢᠨᠦᠢᠲ᠋ ᠦᠨ ᠬᠣᠪᠴᠠᠰᠤ ᠨᠢ ᠲᠡᠮᠲᠡᠭ ᠂ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Other, Format</source>
        <translation>ᠪᠤᠰᠤᠳ ᠂ ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Malayalam</source>
        <translation>ᠮᠠᠯᠠᠶᠢᠯᠠᠮ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <source>Vertical Forms</source>
        <translation>ᠡᠭᠴᠡ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Geometric Shapes Extended</source>
        <translation>ᠭᠧᠦᠮᠧᠲ᠋ᠷ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ ᠥᠷᠭᠡᠳᠴᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Mark, Enclosing</source>
        <translation>ᠲᠡᠮᠳᠡᠭ᠍ ᠂ ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Chess Symbols</source>
        <translation>ᠣᠯᠠᠨ ᠤᠯᠤᠰ ᠤᠨ ᠰᠢᠲᠠᠷ᠎ᠠ ᠶᠢᠨ ᠲᠡᠮᠳᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Kangxi Radicals</source>
        <translation>ᠡᠩᠬᠡ ᠰᠢ ᠶᠢᠨ ᠬᠡᠯᠲᠡᠰ ᠦᠨ ᠲᠦᠷᠦᠭᠦᠦ ᠃</translation>
    </message>
    <message>
        <source>Cyrillic Supplement</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠯᠢᠷᠸᠧᠨ ᠨᠥᠬᠥᠪᠥᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Mandaic</source>
        <translation>ᠮᠧᠳᠠᠺ ᠃</translation>
    </message>
    <message>
        <source>Samaritan</source>
        <translation>ᠰᠠᠮᠠᠷᠢᠶ᠎ᠠ ᠶᠢᠨ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>Other, Private Use</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠤ ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ᠃</translation>
    </message>
    <message>
        <source>Miscellaneous Technical</source>
        <translation>ᠪᠤᠰᠤᠳ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠃</translation>
    </message>
    <message>
        <source>Phonetic Extensions</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ ᠥᠷᠭᠡᠳᠬᠦ ᠶᠢ ᠥᠷᠭᠡᠳᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Sundanese Supplement</source>
        <translation>ᠲᠡᠭᠦᠨ ᠦ ᠬᠡᠯᠡᠨ ᠦ ᠨᠥᠬᠥᠪᠥᠷᠢ ᠨᠦᠬᠦᠪᠦᠷᠢᠯᠡᠬᠦ ᠶᠢ ᠠᠯᠢᠪᠠ ᠃</translation>
    </message>
    <message>
        <source>Latin-1 Supplement</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡ —1 ᠨᠦᠬᠦᠪᠦᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Control Pictures</source>
        <translation>ᠵᠢᠷᠤᠭ ᠢ ᠡᠵᠡᠮᠳᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Mark, Non-Spacing</source>
        <translation>ᠲᠡᠮᠳᠡᠭ᠍ ᠂ ᠵᠠᠢ᠌ᠯᠠᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Miscellaneous Symbols and Pictographs</source>
        <translation>ᠰᠤᠯᠠ ᠲᠦᠷᠦᠯ ᠤᠨ ᠲᠡᠮᠲᠡᠭ ᠪᠠ ᠳᠦᠷᠢ ᠳᠦᠷᠰᠦ ᠶᠢᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠃</translation>
    </message>
    <message>
        <source>CJK Symbols and Punctuation</source>
        <translation>ᠳᠤᠮᠳᠠᠳᠤ ᠶᠠᠫᠤᠨ ᠺᠣᠷᠢᠶᠠ ᠶᠢᠨ ᠲᠡᠮᠳᠡᠭ ᠪᠣᠯᠣᠨ ᠴᠡᠭ ᠦᠨ ᠲᠡᠮᠳᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Arabic</source>
        <translation>ᠠᠷᠠᠪ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <source>Arrows</source>
        <translation>ᠰᠤᠮᠤᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Central Asian Scripts</source>
        <translation>ᠬᠢᠲᠠᠳ ᠠᠼᠢᠶ᠎ᠠ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌᠃</translation>
    </message>
    <message>
        <source>Coptic</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ ᠲᠦᠭᠡᠮᠡᠯᠵᠢᠭᠦᠯᠬᠦ ᠣᠨᠴᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Sundanese</source>
        <translation>ᠲᠡᠭᠦᠨ ᠦ ᠬᠡᠯᠡ ᠶᠢ ᠠᠭᠤᠴᠢᠯᠠᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Other, Control</source>
        <translation>ᠪᠤᠰᠤᠳ ᠂ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>Mongolian</source>
        <translation>ᠮᠣᠩᠭ᠋ᠣᠯ ᠬᠡᠯᠡ᠃</translation>
    </message>
    <message>
        <source>Hebrew</source>
        <translation>ᠰᠢᠪᠦᠢᠸᠧ ᠬᠡᠯᠡ ᠢᠷᠡᠪᠡ᠃</translation>
    </message>
    <message>
        <source>Myanmar</source>
        <translation>ᠪᠢᠷᠮᠠ ᠃</translation>
    </message>
    <message>
        <source>Punctuation, Initial Quote</source>
        <translation>ᠴᠡᠭ ᠲᠡᠮᠲᠡᠭ ᠂ ᠠᠩᠬ᠎ᠠ ᠲᠠᠲᠠᠬᠤ ᠨᠣᠮᠧᠷ ᠃</translation>
    </message>
    <message>
        <source>Letterlike Symbols</source>
        <translation>ᠲᠦᠷᠦᠯ ᠤᠨ ᠦᠰᠦᠭ ᠤᠨ ᠴᠠᠭᠠᠨ ᠲᠡᠮᠲᠡᠭ᠃</translation>
    </message>
    <message>
        <source>Vedic Extensions</source>
        <translation>ᠦᠨᠦᠰᠦ ᠲᠢᠭ ᠥᠷᠭᠡᠳᠪᠡ ᠃</translation>
    </message>
    <message>
        <source>Kanbun</source>
        <translation>ᠲᠠᠯᠬ᠎ᠠ ᠶᠢ ᠬᠠᠷᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Basic Latin</source>
        <translation>ᠦᠨᠳᠦᠰᠦᠨ ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Lepcha</source>
        <translation>ᠯᠠᠶᠢᠫᠣᠨ ᠤ ᠨᠡᠶᠢᠳᠡᠮ ᠪᠠᠶᠢᠴᠠᠭᠠᠯᠲᠠ᠃</translation>
    </message>
    <message>
        <source>Symbol, Other</source>
        <translation>ᠲᠡᠮᠳᠡᠭ ᠂ ᠪᠤᠰᠤᠳ ᠃</translation>
    </message>
    <message>
        <source>Arabic Extended-A</source>
        <translation>ᠠᠷᠠᠪ ᠬᠡᠯᠡᠨ ᠦ ᠥᠷᠭᠡᠳᠬᠡᠯᠲᠡ ᠠ᠃</translation>
    </message>
    <message>
        <source>Rejang</source>
        <translation>ᠲᠠᠲᠠᠵᠤ ᠥᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>East Asian Scripts</source>
        <translation>ᠵᠡᠭᠦᠨ ᠠᠽᠢᠶ᠎ᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠃</translation>
    </message>
    <message>
        <source>Tai Le</source>
        <translation>ᠲᠠᠢ᠌ᠯᠧᠨ ᠃</translation>
    </message>
    <message>
        <source>Telugu</source>
        <translation>ᠲᠠᠢ ᠯᠦ ᠭᠦᠢ ᠬᠡᠯᠡ᠃</translation>
    </message>
    <message>
        <source>CJK Compatibility</source>
        <translation>ᠳᠤᠮᠲᠠᠲᠦ ᠶᠠᠫᠤᠨ ᠺᠤᠷᠢᠶᠠ ᠶᠢᠨ ᠪᠠᠭᠲᠠᠭᠠᠮᠵᠢᠲᠤ ᠴᠢᠨᠠᠷ ᠲᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Thaana</source>
        <translation>ᠲᠠᠨᠠ ᠃</translation>
    </message>
    <message>
        <source>Syriac</source>
        <translation>ᠡᠷᠲᠡᠨ ᠰᠢᠷᠢᠶ᠎ᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Cherokee Supplement</source>
        <translation>ᠨᠣᠷᠺᠢ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠬᠦ ᠡᠳ ᠢ ᠬᠡᠷᠴᠢᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Latin Extended Additional</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡᠨ ᠦ ᠥᠷᠭᠡᠳᠬᠡᠯᠲᠡ ᠶᠢᠨ ᠨᠡᠮᠡᠯᠲᠡ ᠃</translation>
    </message>
    <message>
        <source>Symbol, Currency</source>
        <translation>ᠲᠡᠮᠳᠡᠭ ᠂ ᠵᠣᠭᠣᠰ ᠃</translation>
    </message>
    <message>
        <source>Other, Surrogate</source>
        <translation>ᠪᠤᠰᠤᠳ ᠂ ᠣᠷᠣᠯᠠᠨ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>Domino Tiles</source>
        <translation>ᠳ᠋ᠧᠮᠢᠨᠣ᠋ ᠶᠢᠨ ᠸᠠᠭᠠᠷ ᠲᠣᠭᠣᠰᠬ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Punctuation, Connector</source>
        <translation>ᠴᠡᠭ ᠲᠡᠮᠲᠡᠭ ᠂ ᠵᠠᠯᠭᠠᠭᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Private Use Area</source>
        <translation>ᠬᠤᠪᠢ ᠬᠥᠮᠦᠨ ᠦ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠣᠷᠣᠨ ᠃</translation>
    </message>
    <message>
        <source>Indonesia and Oceania Scripts</source>
        <translation>ᠢᠨᠵᠠᠨᠠᠰᠢ ᠠᠽᠢᠶ᠎ᠠ ᠪᠣᠯᠣᠨ ᠠᠲ᠋ᠯᠠᠨᠲ᠋ ᠤᠨ ᠬᠥᠯ ᠦᠨ ᠳᠡᠪᠲᠡᠷ ᠃</translation>
    </message>
    <message>
        <source>Supplemental Symbols and Pictographs</source>
        <translation>ᠲᠡᠮᠳᠡᠭ ᠪᠣᠯᠤᠨ ᠳᠦᠷᠢ ᠲᠥᠷᠬᠦ ᠶᠢᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Geometric Shapes</source>
        <translation>ᠭᠧᠦᠮᠧᠲ᠋ᠷ ᠬᠡᠯᠪᠡᠷᠢ ᠳᠦᠷᠰᠦ ᠃</translation>
    </message>
    <message>
        <source>Variation Selectors</source>
        <translation>ᠬᠤᠪᠢᠰᠬᠠᠭᠤᠷ ᠤᠨ ᠰᠤᠩᠭ᠋ᠤᠭᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Bopomofo</source>
        <translation>ᠪᠦᠸᠸᠫᠦᠢᠸᠧ ᠮᠦᠸᠧ ᠹᠦ᠋ ᠃</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation>ᠢᠯᠡᠷᠡᠯ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Specials</source>
        <translation>ᠣᠨᠴᠠ ᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Ethiopic Supplement</source>
        <translation>ᠧᠽᠢᠦᠢᠫᠢᠶ᠎ᠠ ᠶᠢᠨ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠬᠦ ᠨᠦᠬᠦᠪᠦᠷᠢᠯᠡᠬᠦ ᠨᠦᠬᠦᠪᠦᠷᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Separator, Space</source>
        <translation>ᠮᠢᠨᠦᠢᠲ᠋ ᠦᠨ ᠬᠣᠭᠣᠰᠣᠨ ᠳᠤ ᠨᠡᠢ᠌ᠴᠡᠵᠦ ᠂ ᠬᠣᠭᠣᠰᠣᠨ ᠃</translation>
    </message>
    <message>
        <source>Hangul Jamo Extended-A</source>
        <translation>ᠬᠠᠨ ᠸᠧᠨ ᠵᠢᠶᠠ ᠮᠦᠸᠧ ᠠ ᠥᠷᠭᠡᠳᠴᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Hangul Jamo Extended-B</source>
        <translation>ᠬᠠᠨ ᠸᠧᠨ ᠵᠢᠶᠠᠮᠦᠢᠸᠧ B ᠦᠷᠭᠡᠳᠴᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>High Surrogates</source>
        <translation>ᠳᠡᠭᠡᠳᠦ ᠣᠷᠣᠯᠠᠨ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>Greek Extended</source>
        <translation>ᠭ᠌ᠷᠧᠭ᠌ ᠬᠡᠯᠡ ᠥᠷᠭᠡᠳᠴᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Letter, Titlecase</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠂ ᠭᠠᠷᠴᠠᠭ ᠤᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠᠷ ᠪᠢᠴᠢᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Myanmar Extended-A</source>
        <translation>ᠪᠢᠷᠮᠠ ᠶᠢᠨ ᠦᠷᠭᠡᠳᠭᠡᠯ ᠠ᠃</translation>
    </message>
    <message>
        <source>Myanmar Extended-B</source>
        <translation>ᠪᠢᠷᠮᠠ ᠶᠢᠨ ᠦᠷᠭᠡᠳᠭᠡᠯ —B ᠃</translation>
    </message>
    <message>
        <source>Phonetic Extensions Supplement</source>
        <translation>ᠠᠪᠢᠶ᠎ᠠ ᠶᠢᠨ ᠥᠷᠭᠡᠳᠬᠡᠯᠲᠡ ᠶᠢᠨ ᠨᠦᠬᠦᠪᠦᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Alphabetic Presentation Forms</source>
        <translation>ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢᠨ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢᠶᠠᠷ ᠵᠢᠭ᠌ᠰᠠᠭᠠᠭᠰᠠᠨ ᠦᠵᠡᠭᠦᠯᠦᠯᠭᠡ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Number, Decimal Digit</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠂ ᠠᠷᠪᠠ ᠶᠢ ᠦᠢᠯᠡᠳᠦᠭᠰᠡᠨ ᠲᠣᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Tai Tham</source>
        <translation>ᠲᠠᠨᠢᠯᠴᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Tai Viet</source>
        <translation>ᠲᠠᠢ᠌ᠵᠠᠨ ᠬᠡᠳᠦᠢ ᠃</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks Extended</source>
        <translation>ᠨᠡᠶᠢᠯᠡᠭᠦᠯᠦᠨ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠠᠭᠤᠨ ᠤ ᠲᠡᠮᠳᠡᠭ ᠢ ᠥᠷᠭᠡᠳᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Tagbanwa</source>
        <translation>ᠰᠤᠪᠠᠭᠸᠠ ᠃</translation>
    </message>
    <message>
        <source>Miscellaneous Symbols</source>
        <translation>ᠰᠤᠯᠠ ᠵᠤᠷᠪᠤᠰ ᠤᠨ ᠲᠡᠮᠳᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Yi Radicals</source>
        <translation>ᠢ ᠦᠨᠳᠦᠰᠦᠲᠡᠨ ᠦ ᠬᠡᠯᠲᠡᠰ ᠦᠨ ᠲᠦᠷᠦᠭᠦᠦ ᠃</translation>
    </message>
    <message>
        <source>Middle Eastern Scripts</source>
        <translation>ᠳᠤᠮᠳᠠᠳᠤ ᠳᠣᠷᠣᠨᠠᠲᠤ ᠶᠢᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠃</translation>
    </message>
    <message>
        <source>General Punctuation</source>
        <translation>ᠡᠩ ᠦᠨ ᠴᠡᠭ ᠦᠨ ᠲᠡᠮᠳᠡᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>Balinese</source>
        <translation>ᠪᠠᠯᠢ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Combining Diacritics</source>
        <translation>ᠠᠶ᠎ᠠ ᠶᠢᠨ ᠲᠡᠮᠳᠡᠭ ᠢ ᠨᠡᠶᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Devanagari</source>
        <translation>ᠸᠧᠨ ᠸᠧᠨ ᠃</translation>
    </message>
    <message>
        <source>Braille Patterns</source>
        <translation>ᠰᠤᠬᠤᠷ ᠪᠢᠴᠢᠬᠦ ᠵᠢᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <source>Saurashtra</source>
        <translation>ᠰᠣᠯᠠᠱᠲ᠋ᠯᠠ ᠃</translation>
    </message>
    <message>
        <source>Katakana</source>
        <translation>ᠬᠠᠭᠤᠷᠮᠠᠭ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Mathematical Symbols</source>
        <translation>ᠲᠣᠭᠠᠨ ᠤ ᠤᠬᠠᠭᠠᠨ ᠤ ᠲᠡᠮᠳᠡᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>Bengali</source>
        <translation>ᠪᠠᠨᢉᠭᠠᠯᠢ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <source>Javanese</source>
        <translation>ᠸᠠᠭᠠᠷ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Yi Syllables</source>
        <translation>ᠠᠪᠢᠶ᠎ᠠ ᠶᠢᠨ ᠪᠠᠶᠠᠷ᠃</translation>
    </message>
    <message>
        <source>Devanagari Extended</source>
        <translation>ᠸᠠᠨ ᠸᠧᠨ ᠥᠷᠭᠡᠵᠢᠪᠡ ᠃</translation>
    </message>
    <message>
        <source>Kayah Li</source>
        <translation>ᠯᠢ ᠴᠢᠶᠠᠨ ᠴᠢᠶᠠᠨ ᠃</translation>
    </message>
    <message>
        <source>Number, Letter</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠂ ᠦᠰᠦᠭ ᠦᠨ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Armenian</source>
        <translation>ᠠᠷᠮᠧᠨ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <source>Other, Not Assigned</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠤ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠭᠰᠠᠨ ᠦᠭᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Ethiopic Extended</source>
        <translation>ᠧᠽᠢᠦᠢᠫᠢᠶ᠎ᠠ ᠥᠷᠭᠡᠳᠴᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Ethiopic Extended-A</source>
        <translation>ᠧᠽᠢᠦᠢᠫᠢᠶ᠎ᠠ ᠠ ᠥᠷᠭᠡᠳᠴᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Optical Character Recognition</source>
        <translation>ᠭᠡᠷᠡᠯ ᠦᠨ ᠦᠰᠦᠭ ᠢ ᠢᠯᠭᠠᠬᠤ ᠳᠤ ᠴᠡᠭᠡᠵᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Yijing Hexagram Symbols</source>
        <translation>ᠠᠮᠠᠷᠬᠠᠨ ᠦᠵᠡᠮᠵᠢ ᠶᠢᠨ ᠵᠢᠷᠭᠤᠭᠠᠨ ᠥᠨᠴᠥᠭ᠍ᠲᠦ ᠣᠳᠣᠨ ᠲᠡᠮᠳᠡᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>Sinhala</source>
        <translation>ᠪᠧᠷᠢᠶᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Mahjong Tiles</source>
        <translation>ᠮᠠᠵᠢᠶᠠᠩ ᠫᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>New Tai Lue</source>
        <translation>ᠰᠢᠨ ᠲᠠᠢ ᠵᠠᠮ᠃</translation>
    </message>
    <message>
        <source>Arabic Supplement</source>
        <translation>ᠠᠷᠠᠪ ᠬᠡᠯᠡ ᠪᠡᠷ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Enclosed CJK Letters and Months</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠤᠮᠳᠠᠳᠤ ᠶᠠᠫᠤᠨ ᠺᠣᠷᠢᠶᠠ ᠶᠢᠨ ᠵᠠᠬᠢᠳᠠᠯ ᠪᠠ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Supplemental Punctuation</source>
        <translation>ᠴᠡᠭ ᠲᠡᠮᠲᠡᠭ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Tagalog</source>
        <translation>ᠲᠠᠺᠠᠷ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Miscellaneous Symbols and Arrows</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠡᠮᠲᠡᠭ ᠪᠠ ᠰᠤᠮᠤᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>&lt;Private Use High Surrogate&gt;</source>
        <translation>&lt;私人使用高代理&gt;᠃</translation>
    </message>
    <message>
        <source>Letter, Uppercase</source>
        <translation>ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠂ ᠶᠡᠬᠡ ᠪᠢᠴᠢᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Ethiopic</source>
        <translation>ᠧᠽᠢᠦᠢᠫᠢᠶ᠎ᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Tibetan</source>
        <translation>ᠲᠦᠪᠡᠳ ᠬᠡᠯᠡ᠃</translation>
    </message>
    <message>
        <source>Combining Half Marks</source>
        <translation>ᠨᠡᠢ᠌ᠯᠡᠭᠡᠳ ᠬᠠᠭᠠᠰ ᠬᠣᠪᠢᠶᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>CJK Strokes</source>
        <translation>ᠳᠤᠮᠳᠠᠳᠤ ᠶᠠᠫᠤᠨ ᠺᠣᠷᠢᠶᠠ ᠶᠢᠨ ᠪᠢᠷ ᠦᠨ ᠵᠢᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>ᠲᠡᠮᠳᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Punctuation, Dash</source>
        <translation>ᠴᠡᠭ ᠦᠨ ᠲᠡᠮᠳᠡᠭ᠂ ᠡᠪᠳᠡᠷᠡᠭᠰᠡᠨ ᠳ᠋ᠤᠭᠠᠷ ᠢ ᠡᠪᠳᠡᠬᠦ᠃</translation>
    </message>
    <message>
        <source>Punctuation, Open</source>
        <translation>ᠴᠡᠭ ᠲᠡᠮᠲᠡᠭ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>&lt;Private Use&gt;</source>
        <translation>&lt;私人使用&gt;᠃</translation>
    </message>
    <message>
        <source>Khmer Symbols</source>
        <translation>ᠥᠨᠳᠥᠷ ᠬᠥᠪᠥᠩ ᠦᠨ ᠲᠡᠮᠳᠡᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>Mathematical Operators</source>
        <translation>ᠲᠣᠭᠠᠨ ᠤ ᠤᠬᠠᠭᠠᠨ ᠤ ᠲᠡᠭᠡᠭᠡᠪᠦᠷᠢ ᠶᠢᠨ ᠪᠣᠳᠣᠯᠲᠠ ᠨᠢ ᠨᠡᠢ᠌ᠴᠡᠨ᠎</translation>
    </message>
    <message>
        <source>Non-printable</source>
        <translation>ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>CJK Unified Ideographs</source>
        <translation>ᠳᠤᠮᠳᠠᠳᠤ ᠶᠠᠫᠤᠨ ᠺᠣᠷᠢᠶᠠ ᠶᠢᠨ ᠨᠢᠭᠡᠳᠦᠯᠲᠡᠢ ᠢᠯᠡᠳᠬᠡᠯ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠢᠶᠡᠨ ᠨᠢᠭᠡᠳᠬᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Symbol, Math</source>
        <translation>ᠲᠡᠮᠳᠡᠭ᠍ ᠂ ᠲᠣᠭᠠᠨ ᠤ ᠤᠬᠠᠭᠠᠨ ᠃</translation>
    </message>
    <message>
        <source>South Asian Scripts</source>
        <translation>ᠡᠮᠦᠨᠡᠲᠦ ᠠᠽᠢᠶ᠎ᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠃</translation>
    </message>
    <message>
        <source>Supplemental Arrows-A</source>
        <translation>ᠰᠤᠮᠤᠨ ᠲᠣᠯᠣᠭᠠᠢ — A ᠶᠢ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Supplemental Arrows-B</source>
        <translation>ᠰᠤᠮᠤᠨ ᠲᠣᠯᠣᠭᠠᠢ — B ᠶᠢ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Supplemental Arrows-C</source>
        <translation>ᠰᠤᠮᠤᠨ ᠲᠣᠯᠣᠭᠠᠢ C ᠶᠢ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Hangul Compatibility Jamo</source>
        <translation>ᠬᠠᠨ ᠸᠧᠨ ᠢ ᠨᠡᠢ᠌ᠯᠡᠭᠦᠯᠬᠦ ᠴᠢᠨᠠᠷ ᠲᠠᠢ ᠵᠧᠮᠧᠨ ᠃</translation>
    </message>
    <message>
        <source>Superscripts and Subscripts</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠭᠠᠭᠠᠴᠢᠯᠠᠯᠲᠠ ᠪᠠ ᠳᠠᠭᠠᠭᠠᠴᠢᠯᠠᠯᠲᠠ ᠶᠢ ᠪᠠᠭᠤᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Number, Other</source>
        <translation>ᠨᠣᠮᠧᠷ ᠂ ᠪᠤᠰᠤᠳ ᠃</translation>
    </message>
    <message>
        <source>CJK Unified Ideographs Extension A</source>
        <translation>ᠳᠤᠮᠲᠠᠲᠦ ᠶᠠᠫᠤᠨ ᠺᠤᠷᠢᠶᠠ ᠶᠢᠨ ᠨᠢᠬᠡᠲᠦᠯᠲᠡᠢ ᠢᠯᠡᠷᠬᠡᠶᠢᠯᠡᠭᠰᠡᠨ ᠨᠢ ᠠ ᠶᠢ ᠦᠷᠬᠡᠳᠬᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Hangul Jamo</source>
        <translation>ᠬᠠᠨ ᠸᠧᠨ ᠵᠢᠶᠠᠮᠦᠢ ᠃</translation>
    </message>
    <message>
        <source>Bopomofo Extended</source>
        <translation>ᠪᠦᠸᠸᠫᠦᠢᠸᠧ ᠮᠦᠸᠧ ᠹᠦ᠋ ᠥᠷᠭᠡᠳᠴᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Box Drawing</source>
        <translation>ᠬᠠᠢ᠌ᠷᠴᠠᠭ ᠤᠨ ᠵᠢᠷᠤᠭ ᠃</translation>
    </message>
    <message>
        <source>Buginese</source>
        <translation>ᠪᠦᠵᠢ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Gujarati</source>
        <translation>ᠭᠣᠿᠠᠷᠠᠲ᠋ᠢ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <source>Katakana Phonetic Extensions</source>
        <translation>ᠹᠢᠯᠢᠮ ᠦᠨ ᠬᠠᠭᠤᠷᠮᠠᠭ ᠨᠡᠷᠡᠲᠦ ᠠᠪᠢᠶ᠎ᠠ ᠥᠷᠭᠡᠳᠴᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Spacing Modifier Letters</source>
        <translation>ᠵᠠᠢ ᠨᠢ ᠵᠠᠰᠠᠯ ᠤᠨ ᠲᠡᠮᠲᠡᠭ ᠨᠢ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢ ᠵᠠᠰᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Latin Extended-A</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡᠨ ᠦ ᠥᠷᠭᠡᠳᠴᠡ — A ᠃</translation>
    </message>
    <message>
        <source>Latin Extended-B</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡ ᠥᠷᠭᠡᠳᠴᠡᠢ — B ᠃</translation>
    </message>
    <message>
        <source>Latin Extended-C</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡ ᠥᠷᠭᠡᠳᠴᠦ —C ᠃</translation>
    </message>
    <message>
        <source>Latin Extended-D</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡᠨ ᠦ ᠥᠷᠭᠡᠳᠴᠡ — D ᠃</translation>
    </message>
    <message>
        <source>Latin Extended-E</source>
        <translation>ᠯᠠᠲ᠋ᠢᠨ ᠬᠡᠯᠡ ᠥᠷᠭᠡᠳᠴᠡᠢ — E ᠃</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>ᠮᠡᠳᠡᠬᠦ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Meetei Mayek Extensions</source>
        <translation>ᠮᠧᠶ ᠲᠠᠢᠮᠠᠶᠢᠺ ᠥᠷᠭᠡᠳᠴᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Georgian</source>
        <translation>ᠴᠢᠶᠣᠤ ᡁᠢ ᠶᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Arabic Presentation Forms-A</source>
        <translation>ᠠᠷᠠᠪ ᠬᠡᠯᠡᠨ ᠦ ᠲᠣᠭᠯᠠᠯᠲᠠ ᠶᠢᠨ ᠢᠯᠡᠳᠬᠡᠯ ᠭᠧ ᠭᠧ A ᠃</translation>
    </message>
    <message>
        <source>Arabic Presentation Forms-B</source>
        <translation>ᠠᠷᠠᠪ ᠬᠡᠯᠡᠨ ᠦ ᠲᠣᠭᠯᠠᠯᠲᠠ ᠭᠧ ᠪ ᠃</translation>
    </message>
    <message>
        <source>Gurmukhi</source>
        <translation>ᠡᠷᠳᠡᠮᠦᠵᠢ</translation>
    </message>
    <message>
        <source>&lt;not assigned&gt;</source>
        <translation>&lt;未分配&gt;᠃</translation>
    </message>
    <message>
        <source>Letter, Other</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠂ ᠪᠤᠰᠤᠳ ᠃</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks Supplement</source>
        <translation>ᠬᠤᠪᠢᠷᠠᠯᠲᠠ ᠶᠢᠨ ᠲᠡᠮᠳᠡᠭ ᠢ ᠨᠡᠶᠢᠯᠡᠭᠦᠯᠦᠨ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Enclosed Ideographic Supplement</source>
        <translation>ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠦᠨ ᠨᠥᠬᠥᠪᠥᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Symbol, Modifier</source>
        <translation>ᠲᠡᠮᠳᠡᠭ᠂ ᠵᠠᠰᠠᠯ ᠤᠨ ᠲᠡᠮᠳᠡᠭ᠂ ᠵᠠᠰᠠᠯ ᠤᠨ ᠲᠡᠮᠳᠡᠭ᠃</translation>
    </message>
    <message>
        <source>&lt;noncharacter&gt;</source>
        <translation>&lt;noncharacter&gt;</translation>
    </message>
    <message>
        <source>Symbols and Pictographs Extended-A</source>
        <translation>ᠲᠡᠮᠲᠡᠭ ᠪᠠ ᠳᠦᠷᠢ ᠳᠦᠷᠬᠦ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠦᠨ ᠥᠷᠭᠡᠳᠬᠦ — A ᠃</translation>
    </message>
    <message>
        <source>Ornamental Dingbats</source>
        <translation>ᠳ᠋ᠢᠩ ᠢᠮᠠᠭ᠎ᠠ ᠶᠢ ᠰᠣᠨᠢᠷᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Alchemical Symbols</source>
        <translation>ᠠᠯᠲᠠ ᠪᠣᠯᠪᠠᠰᠤᠷᠠᠭᠤᠯᠬᠤ ᠮᠡᠰᠡᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>Hangul Syllables</source>
        <translation>ᠬᠠᠨ ᠸᠧᠨ ᠳᠠᠭᠤᠨ ᠤ ᠪᠠᠶᠠᠷ᠃</translation>
    </message>
    <message>
        <source>Modifier Tone Letters</source>
        <translation>ᠠᠪᠢᠶ᠎ᠠ ᠶᠢᠨ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢ ᠵᠠᠰᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks</source>
        <translation>ᠳᠠᠭᠤ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠲᠡᠮᠳᠡᠭ ᠢ ᠨᠡᠶᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>&lt;Non Private Use High Surrogate&gt;</source>
        <translation>&lt;非私人使用高代理&gt;᠃</translation>
    </message>
    <message>
        <source>Ideographic Description Characters</source>
        <translation>ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠢ ᠳᠦᠷᠰᠦᠯᠡᠨ ᠲᠣᠭᠠᠴᠢᠬᠤ ᠶᠢ ᠢᠯᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>CJK Radicals Supplement</source>
        <translation>ᠳᠤᠮᠲᠠᠲᠦ ᠶᠠᠫᠤᠨ ᠺᠤᠷᠢᠶᠠ ᠶᠢᠨ ᠴᠢᠯᠦᠬᠡᠲᠦ ᠰᠠᠭᠤᠷᠢ ᠶᠢᠨ ᠨᠦᠭᠦᠪᠦᠷᠢᠯᠡᠭᠦ ᠡᠮ ᠃</translation>
    </message>
    <message>
        <source>Ol Chiki</source>
        <translation>ᠣᠷᠴᠢᠺ ᠃</translation>
    </message>
    <message>
        <source>Supplemental Mathematical Operators</source>
        <translation>ᠲᠣᠭᠠᠨ ᠤ ᠤᠬᠠᠭᠠᠨ ᠤ ᠲᠡᠭᠡᠭᠡᠪᠦᠷᠢ ᠶᠢᠨ ᠪᠣᠳᠣᠯᠲᠠ ᠶᠢᠨ ᠨᠡᠶᠢᠴᠡᠬᠦ ᠶᠢ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Phonetic Symbols</source>
        <translation>ᠠᠪᠢᠶ᠎ᠠ ᠶᠢᠨ ᠲᠡᠮᠳᠡᠭ᠍ ᠃</translation>
    </message>
    <message>
        <source>Playing Cards</source>
        <translation>ᠫᠠᠢ ᠨᠠᠭᠠᠳᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>Glagolitic</source>
        <translation>ᠭᠯᠠᠭᠤᠸᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Phags-pa</source>
        <translation>ᠫᠠᠭᠰᠫᠠ ᠃</translation>
    </message>
    <message>
        <source>Hanunoo</source>
        <translation>ᠬᠠᠴᠢ ᠴᠢᠷᠮᠠᠢ᠌ᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Meetei Mayek</source>
        <translation>ᠮᠧᠢ ᠲᠠᠢ ᠮᠠᠶᠧᠺ ᠃</translation>
    </message>
    <message>
        <source>Cherokee</source>
        <translation>ᠴᠧᠯᠦᠸᠧᠵᠢ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Punctuation, Final Quote</source>
        <translation>ᠴᠡᠭ ᠦᠨ ᠲᠡᠮᠳᠡᠭ ᠂ ᠬᠠᠮᠤᠭ ᠰᠡᠭᠦᠯ ᠳᠦ ᠦᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>CJK Compatibility Forms</source>
        <translation>ᠳᠤᠮᠲᠠᠲᠦ ᠶᠠᠫᠤᠨ ᠺᠤᠷᠢᠶᠠ ᠶᠢᠨ ᠪᠠᠭᠲᠠᠭᠠᠮᠵᠢᠲᠤ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠪᠠᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Georgian Supplement</source>
        <translation>ᠭᠷᠦᠢᠽᠢᠶᠠ ᠬᠡᠯᠡ ᠪᠡᠷ ᠨᠥᠬᠥᠪᠥᠷᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Syloti Nagri</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠯᠤᠳᠢ ᠨᠠᠭᠯᠢ ᠃</translation>
    </message>
    <message>
        <source>Greek and Coptic</source>
        <translation>ᠭ᠌ᠷᠧᠭ᠌ ᠬᠡᠯᠡ ᠪᠣᠯᠣᠨ ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ ᠢ ᠲᠦᠭᠡᠮᠡᠯᠵᠢᠭᠦᠯᠬᠦ ᠣᠨᠴᠠ ᠬᠡᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Enclosed Alphanumerics</source>
        <translation>ᠪᠢᠲᠡᠭᠦᠮᠵᠢᠯᠡᠭᠰᠡᠨ ᠮᠠᠶᠢᠭ ᠤᠨ ᠦᠰᠦᠭ ᠦᠨ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢᠨ ᠲᠣᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Letter, Modifier</source>
        <translation>ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠂ ᠵᠠᠰᠠᠯ ᠤᠨ ᠲᠡᠮᠲᠡᠭ ᠃</translation>
    </message>
    <message>
        <source>Common Indic Number Forms</source>
        <translation>ᠦᠷᠭᠦᠯᠵᠢ ᠲᠣᠬᠢᠶᠠᠯᠳᠤᠳᠠᠭ ᠡᠨᠡᠳᠬᠡᠭ ᠦᠨ ᠲᠣᠭ᠎ᠠ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Low Surrogates</source>
        <translation>ᠣᠷᠣᠯᠠᠨ ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>&lt;Low Surrogate&gt;</source>
        <translation>&lt;低代孕&gt;᠃</translation>
    </message>
    <message>
        <source>European Scripts</source>
        <translation>ᠧᠦᠢᠷᠦᠫᠡ ᠲᠢᠪ ᠦᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠃</translation>
    </message>
    <message>
        <source>Tifinagh</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠬᠤ ᠦᠭᠡᠢ᠃</translation>
    </message>
    <message>
        <source>Southeast Asian Scripts</source>
        <translation>ᠵᠡᠭᠦᠨ ᠡᠮᠦᠨᠡᠲᠦ ᠠᠽᠢᠶ᠎ᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ᠌ ᠃</translation>
    </message>
    <message>
        <source>Small Form Variants</source>
        <translation>ᠵᠢᠵᠢᠭ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ ᠬᠤᠪᠢᠷᠠᠯᠲᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KEditListWidget</name>
    <message>
        <source>&amp;Add</source>
        <translation>ᠨᠡᠮᠡᠯᠲᠡ ᠃</translation>
    </message>
    <message>
        <source>Move &amp;Up</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦ ᠪᠠ ᠳᠡᠭᠡᠭᠰᠢᠯᠡᠬᠦ ᠰᠢᠯᠵᠢᠯᠲᠡ</translation>
    </message>
    <message>
        <source>Move &amp;Down</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦ ᠪᠠ ᠳᠣᠷᠣᠭᠰᠢ ᠰᠢᠯᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>ᠬᠠᠰᠤᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>KFontChooser</name>
    <message>
        <source>Bold</source>
        <translation>ᠵᠣᠷᠢᠭᠲᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation>ᠦᠰᠦᠭ ᠄</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠄</translation>
    </message>
    <message>
        <source>Here you can choose the font to be used.</source>
        <translation>ᠡᠨᠳᠡ ᠂ ᠲᠠ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠦᠰᠦᠭ᠍ ᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Change font family?</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠰᠢᠷᠢᠰ ᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <source>The Quick Brown Fox Jumps Over The Lazy Dog</source>
        <translation>ᠮᠢᠨ ᠵᠢᠶᠧ ᠶᠢᠨ ᠬᠦᠷᠡᠩ ᠦᠨ ᠦᠨᠡᠭᠡ ᠠᠯᠵᠠᠭᠤ ᠨᠣᠬᠠᠢ ᠶᠢ ᠦᠰᠦᠷᠦᠨ ᠭᠠᠷᠴᠤ ᠥᠩᠭᠡᠷᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>This sample text illustrates the current settings. You may edit it to test special characters.</source>
        <translation>ᠡᠨᠡ ᠵᠢᠱᠢᠶ᠎ᠡ ᠶᠢᠨ ᠲᠣᠪᠬᠢᠮᠠᠯ ᠨᠢ ᠣᠳᠣᠬᠠᠨ ᠤ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠲᠣᠳᠣᠷᠬᠠᠶᠢᠯᠠᠭᠰᠠᠨ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃ ᠲᠠ ᠲᠡᠭᠦᠨ ᠳᠦ ᠨᠠᠶᠢᠷᠠᠭᠤᠯᠤᠭᠴᠢ ᠬᠢᠵᠦ ᠣᠨᠴᠠᠭᠠᠢ ᠦᠰᠦᠭ᠍ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠢ ᠰᠢᠯᠭᠠᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Change font style?</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠶᠠᠩᠵᠤ ᠶᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <source>Here you can choose the font style to be used.</source>
        <translation>ᠡᠨᠳᠡ ᠂ ᠲᠠ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠦᠰᠦᠭ᠍ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠬᠢ ᠃</translation>
    </message>
    <message>
        <source>Bold Italic</source>
        <translation>ᠪᠦᠳᠦᠭᠦᠨ ᠪᠡᠶ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>ᠬᠡᠪ ᠦᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠃</translation>
    </message>
    <message>
        <source>Oblique</source>
        <translation>ᠬᠠᠵᠠᠭᠤ᠃</translation>
    </message>
    <message>
        <source>Font style</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Here you can switch between fixed font size and font size to be calculated dynamically and adjusted to changing environment (e.g. widget dimensions, paper size).</source>
        <translation>ᠡᠨᠳᠡ ᠂ ᠲᠠ ᠲᠣᠭᠲᠠᠮᠠᠯ ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠠ ᠬᠥᠳᠡᠯᠦᠩᠭᠦᠶ ᠪᠣᠳᠣᠬᠤ ᠦᠰᠦᠭ ᠦᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠶᠢᠨ ᠬᠣᠭᠣᠷᠣᠨᠳᠣ ᠰᠣᠯᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠪᠥᠭᠡᠳ ᠲᠠᠰᠤᠷᠠᠯᠲᠠ ᠦᠭᠡᠢ ᠬᠤᠪᠢᠷᠠᠯᠲᠠ ᠶᠢᠨ ᠣᠷᠴᠢᠨ ᠲᠣᠭᠣᠷᠢᠨ ( ᠵᠢᠱᠢᠶᠡᠯᠡᠪᠡᠯ ᠵᠢᠵᠢᠭ ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠲᠣᠨᠣᠭ ᠤᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠂ ᠴᠠᠭᠠᠰᠤᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ) ᠶᠢ ᠦᠨᠳᠦᠰᠦᠯᠡᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠬᠢᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Font style:</source>
        <translation>ᠦᠰᠦᠭ</translation>
    </message>
    <message>
        <source>Requested Font</source>
        <translation>ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠤᠨ ᠦᠰᠦᠭ ᠦᠨ ᠴᠣᠭᠴᠠ ᠃</translation>
    </message>
    <message>
        <source>Font size&lt;br /&gt;&lt;i&gt;fixed&lt;/i&gt; or &lt;i&gt;relative&lt;/i&gt;&lt;br /&gt;to environment</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ 〔 X9X 〕 〔 X15X 〕 ᠲᠣᠭᠲᠠᠮᠠᠯ 〔 X23X 〕 ᠪᠤᠶᠤ 〔 X31X 〕 ᠬᠠᠷᠢᠴᠠᠩᠭᠤᠶ 〔 X42X 〕 ᠬᠠᠷᠢᠴᠠᠩᠭᠤᠶ 〔 X42X 〕 ᠣᠷᠴᠢᠨ ᠲᠣᠭᠣᠷᠢᠨ ᠳᠤ ᠬᠦᠷᠴᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Here you can choose the font family to be used.</source>
        <translation>ᠡᠨᠳᠡ ᠂ ᠲᠠ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠦᠰᠦᠭ᠍ ᠦᠨ ᠰᠢᠷᠢᠰ ᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Enable this checkbox to change the font style settings.</source>
        <translation>ᠡᠭᠦᠨ ᠢ ᠡᠬᠢᠯᠡᠨ ᠰᠣᠩᠭᠣᠬᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡᠯ ᠦᠰᠦᠭ ᠦᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Change font size?</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠶᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message>
        <source>Relative</source>
        <translation>ᠬᠠᠷᠢᠴᠠᠩᠭᠤᠶ ᠃</translation>
    </message>
    <message>
        <source>Enable this checkbox to change the font size settings.</source>
        <translation>ᠡᠭᠦᠨ ᠢ ᠡᠬᠢᠯᠡᠨ ᠰᠣᠩᠭᠣᠬᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡᠯ ᠦᠰᠦᠭ ᠦᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠶᠢᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Enable this checkbox to change the font family settings.</source>
        <translation>ᠡᠭᠦᠨ ᠢ ᠡᠬᠢᠯᠡᠨ ᠰᠣᠩᠭᠣᠬᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠪᠡᠯ ᠦᠰᠦᠭ ᠦᠨ ᠰᠢᠷᠢᠰ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Here you can choose the font size to be used.</source>
        <translation>ᠡᠨᠳᠡ ᠂ ᠲᠠ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠦᠰᠦᠭ᠍ ᠦᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠶᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KAssistantDialog</name>
    <message>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢᠭᠡ ᠨᠢ ᠃</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation>ᠡᠶ᠎ᠡ ᠨᠠᠶᠢᠷᠠᠮᠳᠠᠭᠤ ᠪᠤᠴᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation>ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <source>Go back one step</source>
        <translation>ᠬᠣᠢ᠌ᠰᠢ ᠨᠢᠭᠡ ᠠᠯᠬᠤᠮ ᠤᠬᠤᠷᠢᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KActionSelector</name>
    <message>
        <source>&amp;Available:</source>
        <translation>ᠬᠡᠷᠡᠭ᠍ᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠄</translation>
    </message>
    <message>
        <source>&amp;Selected:</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠰᠤᠩᠭ᠋ᠤᠵᠠᠢ᠄</translation>
    </message>
</context>
<context>
    <name>KCharSelect</name>
    <message>
        <source>&amp;Back</source>
        <translation>ᠡᠶ᠎ᠡ ᠨᠠᠶᠢᠷᠠᠮᠳᠠᠭᠤ ᠪᠤᠴᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>XML decimal entity:</source>
        <translation>XML ᠠᠷᠪᠠ ᠶᠢ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠪᠠᠶᠢᠭᠤᠯᠤᠯ ᠤᠨ ᠪᠣᠳᠠᠲᠤ ᠴᠣᠭᠴᠠ ᠄</translation>
    </message>
    <message>
        <source>CJK Ideograph Information</source>
        <translation>ᠳᠤᠮᠲᠠᠲᠦ ᠶᠠᠫᠤᠨ ᠺᠤᠷᠢᠶᠠ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ ᠤᠨ ᠮᠡᠲᠡᠬᠡ ᠵᠠᠩᠭᠢ ᠶᠢ ᠰᠠᠨᠠᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Mandarin Pronunciation: </source>
        <translation>ᠨᠡᠢ᠌ᠲᠡᠯᠢᠭ ᠬᠡᠯᠡᠨ ᠦ ᠠᠪᠢᠶᠠᠯᠠᠬᠤ ᠨᠢ ᠄ </translation>
    </message>
    <message>
        <source>Next in History</source>
        <translation>ᠲᠡᠦᠬᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢᠭᠡ ᠨᠢ ᠃</translation>
    </message>
    <message>
        <source>Select a category</source>
        <translation>ᠨᠢᠭᠡ ᠲᠥᠷᠥᠯ ᠢᠯᠭᠠᠯ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Decomposition:</source>
        <translation>ᠵᠠᠳᠠᠯᠤᠯᠲᠠ ᠄</translation>
    </message>
    <message>
        <source>Alias names:</source>
        <translation>ᠥᠭᠡᠷ᠎ᠡ ᠨᠡᠷ᠎ᠡ ᠄</translation>
    </message>
    <message>
        <source>Unicode category: </source>
        <translation>ᠨᠢᠭᠡᠳᠦᠯᠲᠡᠢ ᠨᠣᠮᠧᠷ ᠤᠨ ᠲᠥᠷᠥᠯ ᠢᠯᠭᠠᠯ </translation>
    </message>
    <message>
        <source>Equivalents:</source>
        <translation>ᠲᠡᠩᠴᠡᠭᠦᠦ ᠦᠨ᠎ᠡ ᠶᠢᠨ ᠡᠳ᠋ ᠄</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠄ </translation>
    </message>
    <message>
        <source>Notes:</source>
        <translation>ᠲᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <source>UTF-8:</source>
        <translation>UTF-8：</translation>
    </message>
    <message>
        <source>Tang Pronunciation: </source>
        <translation>ᠲᠠᠩ ᠹᠠ ᠠᠪᠢᠶᠠᠯᠠᠬᠤ ᠨᠢ </translation>
    </message>
    <message>
        <source>Previous in History</source>
        <translation>ᠲᠡᠦᠬᠡᠨ ᠡᠴᠡ ᠡᠮᠦᠨᠡᠬᠢ ᠰᠠᠯᠠᠭᠠᠨ ᠤ ᠰᠠᠯᠠᠭᠠᠨ᠃</translation>
    </message>
    <message>
        <source>See also:</source>
        <translation>ᠠᠰᠲᠠᠭᠠᠨ ᠯᠠᠪᠯᠠᠯᠲᠠ ᠪᠣᠯᠭᠠᠭᠠᠷᠠᠢ ᠄</translation>
    </message>
    <message>
        <source>Korean Pronunciation: </source>
        <translation>ᠺᠣᠷᠢᠶᠠ ᠬᠡᠯᠡᠨ ᠦ ᠠᠪᠢᠶ᠎ᠠ ᠄ </translation>
    </message>
    <message>
        <source>Japanese Kun Pronunciation: </source>
        <translation>ᠶᠠᠫᠤᠨ ᠬᠡᠯᠡᠨ ᠦ ᠺᠦᠨ ᠦ ᠠᠪᠢᠶᠠᠯᠠᠯ </translation>
    </message>
    <message>
        <source>Character:</source>
        <translation>ᠦᠰᠦᠭ</translation>
    </message>
    <message>
        <source>General Character Properties</source>
        <translation>ᠡᠩ ᠦᠨ ᠦᠰᠦᠭ ᠦᠨ ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠃</translation>
    </message>
    <message>
        <source>UTF-16: </source>
        <translation>UTF-16： </translation>
    </message>
    <message>
        <source>Next Character in History</source>
        <translation>ᠲᠡᠦᠬᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢᠭᠡ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>Various Useful Representations</source>
        <translation>ᠡᠯ᠎ᠡ ᠵᠦᠢᠯ ᠦᠨ ᠬᠡᠷᠡᠭᠴᠡᠭᠡ ᠲᠡᠢ ᠢᠯᠡᠷᠬᠡᠢ᠌ᠯᠡᠯᠲᠡ ᠲᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Block: </source>
        <translation>ᠬᠡᠰᠡᠭ </translation>
    </message>
    <message>
        <source>C octal escaped UTF-8: </source>
        <translation>C8 ᠶᠢᠨ ᠳᠦᠷᠢᠮ ᠦᠨ ᠬᠤᠪᠢᠷᠠᠭᠤᠯᠤᠯᠲᠠ ᠶᠢᠨ ᠤᠳᠬ᠎ᠠ UTF-8 ᠄ </translation>
    </message>
    <message>
        <source>Annotations and Cross References</source>
        <translation>ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠬᠤ ᠪᠠ ᠰᠣᠯᠪᠣᠭᠠᠯᠠᠵᠤ ᠡᠰᠢ ᠲᠠᠲᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Cantonese Pronunciation: </source>
        <translation>ᠭᠤᠸᠠᠩᠳᠦᠢᠩ ᠬᠡᠯᠡᠨ ᠦ ᠠᠪᠢᠶᠠᠯᠠᠬᠤ </translation>
    </message>
    <message>
        <source>Select a block to be displayed</source>
        <translation>ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠬᠡᠰᠡᠭ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Enter a search term or character here</source>
        <translation>ᠡᠨᠳᠡ ᠡᠷᠢᠬᠦ ᠦᠭᠡ ᠪᠤᠶᠤ ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠢ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Set font</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Approximate equivalents:</source>
        <translation>ᠣᠢ᠌ᠷ᠎ᠠ ᠲᠡᠩᠴᠡᠭᠦᠦ ᠦᠨ᠎ᠡ ᠶᠢᠨ ᠪᠣᠳᠠᠰ</translation>
    </message>
    <message>
        <source>Set font size</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Find...</source>
        <translation>ᠡᠷᠢᠬᠦ ᠁</translation>
    </message>
    <message>
        <source>&amp;Forward</source>
        <translation>ᠡᠶ᠎ᠡ ᠲᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠶᠠᠪᠤᠭᠤᠯᠬᠤ᠃</translation>
    </message>
    <message>
        <source>Previous Character in History</source>
        <translation>ᠲᠡᠦᠬᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠠᠭᠤᠴᠢᠨ ᠬᠥᠮᠥᠨ ᠃</translation>
    </message>
    <message>
        <source>Japanese On Pronunciation: </source>
        <translation>ᠶᠠᠫᠤᠨ ᠬᠡᠯᠡᠨ ᠦ ᠠᠪᠢᠶᠠᠯᠠᠯ </translation>
    </message>
    <message>
        <source>Definition in English: </source>
        <translation>ᠠᠩᠭ᠌ᠯᠢ ᠬᠡᠯᠡᠨ ᠦ ᠲᠣᠮᠢᠶᠠᠯᠠᠯ </translation>
    </message>
</context>
<context>
    <name>KDatePicker</name>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Next month</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Select the current day</source>
        <translation>ᠲᠤᠰ ᠡᠳᠦᠷ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Previous year</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠢᠭᠡ ᠵᠢᠯ ᠃</translation>
    </message>
    <message>
        <source>Week %1</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ 1 ᠃</translation>
    </message>
    <message>
        <source>Previous month</source>
        <translation>ᠲᠦᠷᠦᠭᠦᠦ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Select a month</source>
        <translation>ᠨᠢᠭᠡ ᠰᠠᠷ᠎ᠠ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Next year</source>
        <translation>ᠢᠷᠡᠬᠦ ᠵᠢᠯ᠃</translation>
    </message>
    <message>
        <source>Select a year</source>
        <translation>ᠵᠢᠯ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Select a week</source>
        <translation>ᠨᠢᠭᠡ ᠭᠠᠷᠠᠭ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KMessageBox</name>
    <message>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
    <message>
        <source>Sorry</source>
        <translation>ᠡᠪ ᠦᠭᠡᠢᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Do not show this message again</source>
        <translation>ᠡᠨᠡ ᠴᠢᠮᠡᠭᠡ ᠶᠢ ᠳᠠᠬᠢᠨ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Do not ask again</source>
        <translation>ᠪᠢᠲᠡᠭᠡᠢ ᠠᠰᠠᠭᠤ ᠃</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ᠃</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>ᠨᠠᠷᠢᠨ ᠃</translation>
    </message>
    <message>
        <source>Question</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠃</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭ᠍ᠬᠦ</translation>
    </message>
</context>
<context>
    <name>KFontRequester</name>
    <message>
        <source>Preview of the &quot;%1&quot; font</source>
        <translation>《1》 ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠦᠵᠡᠯᠲᠡ᠃</translation>
    </message>
    <message>
        <source>Choose Font...</source>
        <translation>ᠦᠰᠦᠭ ᠰᠣᠩᠭᠣᠬᠤ ᠁</translation>
    </message>
    <message>
        <source>Preview of the selected font</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>This is a preview of the &quot;%1&quot; font. You can change it by clicking the &quot;Choose Font...&quot; button.</source>
        <translation>ᠡᠨᠡ ᠪᠣᠯ 《 1 》 ᠦᠰᠦᠭ ᠦᠨ ᠦᠰᠦᠭ ᠦᠨ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠦᠵᠡᠯᠲᠡ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃ ᠲᠠ ᠳᠠᠩ ᠴᠣᠬᠢᠬᠤ ᠪᠠᠷ ᠳᠠᠮᠵᠢᠨ 《 ᠦᠰᠦᠭ᠍ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠁ 》 ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠳᠠᠷᠤ ᠃</translation>
    </message>
    <message>
        <source>This is a preview of the selected font. You can change it by clicking the &quot;Choose Font...&quot; button.</source>
        <translation>ᠡᠨᠡ ᠪᠣᠯ ᠦᠰᠦᠭ᠍ ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠤᠷᠢᠳᠴᠢᠯᠠᠨ ᠦᠵᠡᠯᠲᠡ ᠮᠥᠨ ᠃ ᠲᠠ ᠳᠠᠩ ᠴᠣᠬᠢᠬᠤ ᠪᠠᠷ ᠳᠠᠮᠵᠢᠨ 《 ᠦᠰᠦᠭ᠍ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢ ᠰᠣᠩᠭᠣᠵᠤ ᠪᠣᠯᠣᠨ᠎ᠠ ᠁ 》 ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠳᠠᠷᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>FontHelpers</name>
    <message>
        <source>Serif</source>
        <translation>ᠬᠠᠨᠲᠠᠭᠠᠵᠠ ᠶᠢᠨ ᠤᠲᠠᠰᠤ᠃</translation>
    </message>
    <message>
        <source>Sans Serif</source>
        <translation>ᠬᠠᠨᠲᠠᠭᠠᠵᠠ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Monospace</source>
        <translation>ᠥᠷᠭᠡᠨ ᠪᠠᠢ᠌ᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>KDateComboBox</name>
    <message>
        <source>Today</source>
        <translation>ᠥᠨᠥᠳᠥᠷ ᠃</translation>
    </message>
    <message>
        <source>The date you entered is invalid</source>
        <translation>ᠲᠠᠨ ᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>No Date</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠶᠢ ᠨᠢ ᠳᠣᠢᠷ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Tomorrow</source>
        <translation>ᠮᠠᠷᠭᠠᠰᠢ ᠃</translation>
    </message>
    <message>
        <source>Next Month</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Last Month</source>
        <translation>ᠲᠦᠷᠦᠭᠦᠦ ᠰᠠᠷ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Date cannot be later than %1</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ 1 ᠡᠴᠡ ᠬᠣᠵᠢᠮ ᠪᠠᠶᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Last Week</source>
        <translation>ᠲᠦᠷᠦᠭᠦᠦ ᠭᠠᠷᠠᠭ ᠃</translation>
    </message>
    <message>
        <source>Last Year</source>
        <translation>ᠨᠢᠳᠤᠨᠤᠨ ᠵᠢᠯ ᠃</translation>
    </message>
    <message>
        <source>Yesterday</source>
        <translation>ᠥᠴᠥᠭᠡᠳᠦᠷ ᠃</translation>
    </message>
    <message>
        <source>Date cannot be earlier than %1</source>
        <translation>ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ ᠡᠷᠲᠡ ᠡᠴᠡ 1 ᠡᠴᠡ ᠡᠷᠲᠡ ᠪᠠᠶᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Next Week</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠭᠠᠷᠠᠭ ᠃</translation>
    </message>
    <message>
        <source>Next Year</source>
        <translation>ᠢᠷᠡᠬᠦ ᠵᠢᠯ᠃</translation>
    </message>
</context>
<context>
    <name>KLed</name>
    <message>
        <source>LED off</source>
        <translation>ᠳ᠋ᠧᠩ ᠤᠨᠲᠠᠷᠠᠬᠤ ᠶᠢ ᠵᠢᠭᠠᠪᠤᠷᠢᠯᠠ ᠃</translation>
    </message>
    <message>
        <source>LED on</source>
        <translation>ᠵᠢᠭᠠᠪᠤᠷᠢ ᠶᠢᠨ ᠳ᠋ᠧᠩ ᠠᠰᠠᠭᠠᠬᠤ ᠶᠢ ᠵᠢᠭᠠᠪᠤᠷᠢᠯᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KPixmapRegionSelectorDialog</name>
    <message>
        <source>Please click and drag on the image to select the region of interest:</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢ ᠴᠣᠬᠢᠬᠤ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠰᠣᠨᠢᠷᠬᠠᠯ ᠲᠠᠢ ᠣᠷᠣᠨ ᠢ ᠰᠣᠩᠭᠣᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠄</translation>
    </message>
    <message>
        <source>Select Region of Image</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦᠲᠦ ᠣᠷᠣᠨ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Change the visibility of the password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠬᠠᠷᠠᠭᠳᠠᠬᠤ ᠴᠢᠨᠠᠷ ᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>KPixmapRegionSelectorWidget</name>
    <message>
        <source>Rotate &amp;Counterclockwise</source>
        <translation>ᠡᠷᠭᠢᠯᠲᠡ ᠪᠠ ᠡᠰᠡᠷᠭᠦ ᠴᠠᠭ ᠤᠨ ᠵᠡᠭᠦᠦ ᠃</translation>
    </message>
    <message>
        <source>Image Operations</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦ ᠶᠢ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>&amp;Rotate Clockwise</source>
        <translation>ᠴᠠᠭ ᠤᠨ ᠵᠡᠭᠦᠦ ᠳᠠᠭᠠᠵᠤ ᠡᠷᠭᠢᠯᠳᠦᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>KMessageWidget</name>
    <message>
        <source>&amp;Close</source>
        <translation>ᠡᠭᠦᠳᠡ ᠶᠢ ᠬᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <source>Close message</source>
        <translation>ᠴᠢᠮᠡᠭᠡ ᠶᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KPasswordDialog</name>
    <message>
        <source>Supply a password below.</source>
        <translation>ᠳᠣᠣᠷ᠎ᠠ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠬᠠᠩᠭ᠋ᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Use this password:</source>
        <translation>ᠡᠨᠡ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠶ᠎ᠡ ᠄</translation>
    </message>
    <message>
        <source>No password, use anonymous (or &amp;guest) login</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠪᠠᠶᠢᠬᠤ ᠦᠭᠡᠢ ᠂ ᠨᠡᠷ᠎ᠡ ( ᠡᠰᠡᠬᠦᠯ᠎ᠡ &amp;guest ) ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠃</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠄</translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation>ᠣᠷᠣᠨ ᠪᠦᠰᠡ ᠄</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠴᠡᠭᠡᠵᠢᠯᠡ ᠃</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>Supply a username and password below.</source>
        <translation>ᠳᠣᠣᠷ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠪᠠ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠠᠩᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KDateTimeEdit</name>
    <message>
        <source>Floating</source>
        <translation>ᠬᠥᠪᠪᠥᠮᠡᠯ ᠃</translation>
    </message>
    <message>
        <source>The entered date and time is after the maximum allowed date and time.</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠠ ᠴᠠᠭ ᠨᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠬᠠᠮᠤᠭ ᠤᠨ ᠶᠡᠬᠡ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠠ ᠴᠠᠭ ᠠᠴᠠ ᠬᠣᠯᠠ ᠣᠷᠣᠰᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>The entered date and time is before the minimum allowed date and time.</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠠ ᠴᠠᠭ ᠨᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠬᠠᠮᠤᠭ ᠤᠨ ᠪᠠᠭ᠎ᠠ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠪᠠ ᠴᠠᠭ ᠠᠴᠠ ᠡᠷᠲᠡ ᠪᠠᠶᠢᠳᠠᠭ ᠃</translation>
    </message>
</context>
<context>
    <name>KMimeTypeChooser</name>
    <message>
        <source>Mime Type</source>
        <translation>ᠶᠠᠨ ᠵᠦᠴᠦᠭᠡ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <source>Patterns</source>
        <translation>ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>ᠰᠢᠭᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Click this button to display the familiar KDE mime type editor.</source>
        <translation>ᠡᠨᠡ ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠳᠠᠭᠠᠷᠢᠵᠤ ᠮᠡᠳᠡᠭᠰᠡᠨ KDE ᠠᠬ᠎ᠠ ᠵᠦᠴᠦᠭᠡ ᠶᠢᠨ ᠲᠥᠷᠥᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢᠨ ᠨᠠᠶᠢᠷᠠᠭᠤᠯᠤᠭᠴᠢ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>&amp;Edit...</source>
        <translation>ᠨᠠᠶᠢᠷᠠᠭᠤᠯᠤᠨ ᠁</translation>
    </message>
</context>
<context>
    <name>KToggleFullScreenAction</name>
    <message>
        <source>Display the window in full screen</source>
        <translation>ᠪᠦᠬᠦ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠴᠣᠩᠬᠣ ᠶᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Exit Full Screen</source>
        <translation>ᠪᠦᠬᠦ ᠳᠡᠯᠬᠡᠴᠡ ᠡᠴᠡ ᠭᠠᠷᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Exit F&amp;ull Screen Mode</source>
        <translation>ᠹᠯᠧ ᠳᠡᠯᠪᠡᠷᠡᠬᠦ ᠵᠠᠭᠪᠤᠷ ᠠᠴᠠ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation>ᠪᠦᠬᠦ ᠳᠡᠯᠪᠡᠴᠢᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>F&amp;ull Screen Mode</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Exit full screen mode</source>
        <translation>ᠪᠦᠬᠦ ᠳᠡᠯᠬᠡᠴᠡ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠠᠴᠠ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KNewPasswordDialog</name>
    <message>
        <source>Passwords do not match.</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Password is empty.</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ ᠃</translation>
    </message>
    <message>
        <source>The password you have entered has a low strength. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols as well as letters.

Would you like to use this password anyway?</source>
        <translation>ᠲᠠᠨ ᠤ ᠣᠷᠣᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠦᠨ ᠬᠦᠴᠦᠯᠡᠴᠡ ᠨᠡᠯᠢᠶᠡᠳ ᠳᠣᠣᠷ᠎ᠠ ᠃ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠬᠦᠴᠦᠯᠡᠴᠡ ᠶᠢ ᠳᠡᠭᠡᠭ᠍ᠰᠢᠯᠡᠭᠦᠯᠬᠦ ᠬᠡᠷᠡᠭ᠍ᠲᠡᠢ ᠂ ᠲᠤᠷᠰᠢᠵᠤ ᠦᠵᠡᠭᠡᠷᠡᠢ ᠄
 — ᠨᠡᠩ ᠤᠷᠲᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃
 — ᠲᠣᠮᠣ ᠪᠢᠴᠢᠯᠭᠡ ᠪᠠ ᠵᠢᠵᠢᠭ ᠦᠰᠦᠭ᠍ ᠢ ᠬᠣᠯᠢᠯᠳᠤᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠨ᠎ᠡ
 — ᠲᠣᠭ᠎ᠠ ᠪᠤᠶᠤ ᠲᠡᠮᠲᠡᠭ ᠵᠢᠴᠢ ᠦᠰᠦᠭ ᠦᠨ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃

ᠡᠨᠡ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠦᠦ ?</translation>
    </message>
    <message numerus="yes">
        <source>Password must be at least %n character(s) long.</source>
        <translation>
            <numerusform>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠤᠷᠲᠤ ᠶᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ ᠠᠳᠠᠭ ᠲᠠᠭᠠᠨ n ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠪᠠᠶᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Passwords match.</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Low Password Strength</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠦᠨ ᠬᠦᠴᠦᠯᠡᠴᠡ ᠳᠣᠣᠷ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KSqueezedTextLabel</name>
    <message>
        <source>&amp;Copy Full Text</source>
        <translation>ᠪᠦᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠶᠢ ᠪᠠᠭᠤᠯᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>KTimeComboBox</name>
    <message>
        <source>Time cannot be later than %1</source>
        <translation>ᠴᠠᠭ ᠢ ᠨᠢ 1 ᠡᠴᠡ ᠬᠣᠯᠠ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Time cannot be earlier than %1</source>
        <translation>ᠴᠠᠭ ᠨᠢ ᠡᠷᠲᠡ ᠡᠴᠡ 1 ᠡᠴᠡ ᠡᠷᠲᠡ ᠪᠠᠶᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>The time you entered is invalid</source>
        <translation>ᠲᠠᠨ ᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠴᠠᠭ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>KNewPasswordWidget</name>
    <message>
        <source>Password:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠄</translation>
    </message>
    <message>
        <source>Password strength &amp;meter:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠦᠨ ᠬᠦᠴᠦᠯᠡᠴᠡ ᠪᠠ ᠬᠡᠮᠵᠢᠭᠦᠷᠯᠢᠭ</translation>
    </message>
    <message>
        <source>The password strength meter gives an indication of the security of the password you have entered. To improve the strength of the password, try:&lt;ul&gt;&lt;li&gt;using a longer password;&lt;/li&gt;&lt;li&gt;using a mixture of upper- and lower-case letters;&lt;/li&gt;&lt;li&gt;using numbers or symbols, such as #, as well as letters.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠬᠦᠴᠦᠯᠡᠴᠡ ᠲᠠᠨ ᠤ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠦᠨ ᠠᠶᠤᠯᠭᠦᠶ ᠴᠢᠨᠠᠷ ᠢ ᠵᠢᠭᠠᠪᠤᠷᠢᠯᠠᠶ᠎ᠠ ᠃ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠬᠦᠴᠦᠯᠡᠴᠡ ᠶᠢ ᠳᠡᠭᠡᠭ᠍ᠰᠢᠯᠡᠭᠦᠯᠬᠦ ᠬᠡᠷᠡᠭ᠍ᠲᠡᠢ ᠂ 〔 X143X 〔 X147X 〕 ᠨᠡᠩ ᠤᠷᠲᠤ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠪᠣᠯᠪᠠᠤ ᠃ &lt;/li&gt; 〔 X180X 〕 ᠲᠣᠮᠣ ᠪᠢᠴᠢᠯᠭᠡ ᠪᠣᠯᠣᠨ ᠪᠢᠴᠢᠬᠠᠨ ᠦᠰᠦᠭ᠍ ᠦᠨ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠬᠣᠯᠢᠯᠳᠤᠯ &lt;/li&gt; 〔 X238X 〕 ᠲᠣᠭ᠎ᠠ ᠪᠤᠶᠤ ᠲᠡᠮᠳᠡᠭ ( ᠵᠢᠱᠢᠶᠡᠯᠡᠪᠡᠯ # ) ᠵᠢᠴᠢ ᠦᠰᠦᠭ ᠦᠨ ᠴᠠᠭᠠᠨ ᠲᠣᠯᠣᠭᠠᠢ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃ &lt;/li&gt; &lt;/ul&gt;</translation>
    </message>
    <message>
        <source>&amp;Verify:</source>
        <translation>ᠡᠭᠦᠨ ᠦ ᠬᠠᠮᠲᠤ ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠨᠢ ᠄</translation>
    </message>
</context>
<context>
    <name>KColorCombo</name>
    <message>
        <source>Custom...</source>
        <translation>ᠳᠠᠳᠬᠠᠯ ᠃</translation>
    </message>
</context>
<context>
    <name>KCharSelectItemModel</name>
    <message>
        <source>In decimal</source>
        <translation>ᠠᠷᠪᠠᠲᠤ ᠶᠢᠨ ᠳᠦᠷᠢᠮ ᠃</translation>
    </message>
    <message>
        <source>Unicode code point:</source>
        <translation>ᠨᠢᠭᠡᠳᠦᠯᠲᠡᠢ ᠨᠤᠮᠸᠷ ᠤᠨ ᠪᠠᠶᠢᠷᠢ</translation>
    </message>
</context>
</TS>
