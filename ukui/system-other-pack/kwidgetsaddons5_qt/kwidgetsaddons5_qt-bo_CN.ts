<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KStandardGuiItem</name>
    <message>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation>&amp; ཆོག</translation>
    </message>
    <message>
        <source>&amp;No</source>
        <translation>&gt; མི་ཆོག</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <source>&amp;Yes</source>
        <translation>&amp;རེད།</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Test</source>
        <translation>ཚད་ལེན་ཚོད་ལྟ།</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation>ཕྱིར་ནུར་བྱས་པ།</translation>
    </message>
    <message>
        <source>&amp;Find</source>
        <translation>&gt; བཙལ་ནས་རྙེད་པ</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>རོགས་རམ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation>ལས་གནས་ནས་ཕྱིར་འཐེན་བྱ</translation>
    </message>
    <message>
        <source>&amp;Save</source>
        <translation>&amp;ཉར་ཚགས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Discard changes</source>
        <translation>བསྒྱུར་བཅོས་དོར་བ།</translation>
    </message>
    <message>
        <source>Reset all items to their default values</source>
        <translation>རྣམ་གྲངས་ཚང་མ་དེའི་ཁ་ཆད་དང་འགལ་བའི་རིན་ཐང་དུ་བསྐྱར་དུ་</translation>
    </message>
    <message>
        <source>Close the current document.</source>
        <translation>ད་ལྟ་སྤྱོད་བཞིན་པའི་ཡིག་ཆ་དེ་སྒོ་</translation>
    </message>
    <message>
        <source>Apply changes</source>
        <translation>འགྱུར་ལྡོག་བེད་སྤྱོད་བྱ་དགོས</translation>
    </message>
    <message>
        <source>Save file with another name</source>
        <translation>མིང་གཞན་ཞིག་གིས་ཡིག་ཆ་ཉར་ཚགས་བྱེད་</translation>
    </message>
    <message>
        <source>Clear the input in the edit field</source>
        <translation>རྩོམ་སྒྲིག་ཁྱབ་ཁོངས་ནང་གི་མ་དངུལ་ཁ་གསལ་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>&amp;Apply</source>
        <translation>»རེ་ཞུ།</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>&amp;Reset</source>
        <translation>&gt; བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ</translation>
    </message>
    <message>
        <source>Save &amp;As...</source>
        <translation>གསོག་འཇོག ་བྱེད་པ་</translation>
    </message>
    <message>
        <source>Clear input</source>
        <translation>ནང་འཇུག་ཁ་གསལ་བཟོ་དགོས</translation>
    </message>
    <message>
        <source>C&amp;lear</source>
        <translation>གཙང་སེལ་</translation>
    </message>
    <message>
        <source>Opens the print dialog to print the current document</source>
        <translation>པར་སྐྲུན་གྱི་གླེང་མོལ་ཁ་ཕྱེ་ནས་ད་ལྟའི་ཡིག་ཆ་དཔར་དགོས།</translation>
    </message>
    <message>
        <source>When you click &lt;b&gt;Apply&lt;/b&gt;, the settings will be handed over to the program, but the dialog will not be closed.
Use this to try different settings.</source>
        <translation>&lt;b&gt;Apply&lt;/b&gt;ལ་ཞིབ་འཇུག་བྱེད་སྐབས་སྒྲིག་བཀོད་དེ་གོ་རིམ་ལ་རྩིས་སྤྲོད་བྱ་རྒྱུ་ཡིན་མོད། འོན་ཀྱང་གླེང་མོལ་དེ་སྒོ་རྒྱག་མི་སྲིད།
འདི་བཀོལ་ནས་སྒྲིག་གཞི་མི་འདྲ་བར་ཚོད་ལྟ་བྱས།</translation>
    </message>
    <message>
        <source>Close the current window or document</source>
        <translation>མིག་སྔའི་སྒེའུ་ཁུང་ངམ་ཡིག་ཆ་བཀག་སྡོམ་བྱེད་པ</translation>
    </message>
    <message>
        <source>Confi&amp;gure...</source>
        <translation>ཁུང་ཧྥེ་ཀུང་སི་ ...</translation>
    </message>
    <message>
        <source>Close the current window.</source>
        <translation>མིག་སྔའི་སྒེའུ་ཁུང་གི་སྒོ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <source>&amp;Do Not Save</source>
        <translation>&gt; ཉར་ཚགས་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Enter Administrator Mode</source>
        <translation>སྲིད་འཛིན་དོ་དམ་བྱེད་སྟངས་ནང་</translation>
    </message>
    <message>
        <source>C&amp;ontinue</source>
        <translation>མུ་མཐུད་དུ་རྒྱུན་</translation>
    </message>
    <message>
        <source>&amp;Open...</source>
        <translation>&amp; ཁ་ཕྱེ་</translation>
    </message>
    <message>
        <source>Show help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>&amp;Close Document</source>
        <translation>སྒོ་རྒྱག་པའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>ངོ་བོ་དང་གཟུགས་</translation>
    </message>
    <message>
        <source>Pressing this button will discard all recent changes made in this dialog.</source>
        <translation>མཐེབ་གཅུས་འདི་མནན་ན་གླེང་མོལ་འདིའི་ཁྲོད་ཀྱི་ཆེས་ཉེ་བའི་འགྱུར་ལྡོག་ཡོད་ཚད་འདོར་སྲིད།</translation>
    </message>
    <message>
        <source>Go forward one step</source>
        <translation>གོམ་གང་མདུན་དུ་སྤོས་ནས་མདུན་དུ</translation>
    </message>
    <message>
        <source>When you click &lt;b&gt;Administrator Mode&lt;/b&gt; you will be prompted for the administrator (root) password in order to make changes which require root privileges.</source>
        <translation>&lt;b&gt;དོ་དམ་བྱེད་སྟངས།&lt;/b&gt;ལ་མཐེབ་གནོན་བྱེད་སྐབས་ཁྱོད་ཀྱིས་དོ་དམ་པ་(root)གསང་གྲངས་ལ་སྐུལ་འདེད་བྱས་ནས་རྩ་བའི་དམིགས་བསལ་དབང་ཆ་དགོས་པའི་འགྱུར་ལྡོག་འབྱུང་སྲིད།</translation>
    </message>
    <message>
        <source>&amp;Print...</source>
        <translation>&gt; པར་སྐྲུན་</translation>
    </message>
    <message>
        <source>Do not save data</source>
        <translation>གཞི་གྲངས་ཉར་ཚགས་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Quit application</source>
        <translation>རེ་ཞུ་ཕྱིར་འཐེན་བྱ་</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>&gt; མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation>&amp;བསུབ་པ།</translation>
    </message>
    <message>
        <source>&amp;Insert</source>
        <translation>&gt; འཇུག་པ།</translation>
    </message>
    <message>
        <source>&amp;Discard</source>
        <translation>བསྐྱུར་བ།</translation>
    </message>
    <message>
        <source>Administrator &amp;Mode...</source>
        <translation>སྲིད་འཛིན་དོ་དམ་པ་དང་དཔེ་དབྱིབས་</translation>
    </message>
    <message>
        <source>&amp;Defaults</source>
        <translation>&amp;ཁ་ཆད་དང་འགལ་བ།</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Go back one step</source>
        <translation>ཕྱིར་ནུར་བྱས་ནས་གོམ་པ་གཅིག་</translation>
    </message>
    <message>
        <source>Save data</source>
        <translation>གཞི་གྲངས་གྲོན་ཆུང་བྱེད་</translation>
    </message>
    <message>
        <source>Delete item(s)</source>
        <translation>རྣམ་གྲངས་(s)བསུབ་པ།</translation>
    </message>
    <message>
        <source>&amp;Overwrite</source>
        <translation>་་་</translation>
    </message>
    <message>
        <source>&amp;Forward</source>
        <translation>མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <source>Continue operation</source>
        <translation>མུ་མཐུད་དུ་འཁོར་སྐྱོད་</translation>
    </message>
    <message>
        <source>Reset configuration</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>&amp;Close Window</source>
        <translation>སྒེའུ་ཁུང་གི་སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>KCharSelectData</name>
    <message>
        <source>Lao</source>
        <translation>ལའོ་ཨའོ།</translation>
    </message>
    <message>
        <source>NKo</source>
        <translation>NKo</translation>
    </message>
    <message>
        <source>Vai</source>
        <translation>ཝེ་ཡ།</translation>
    </message>
    <message>
        <source>Cham</source>
        <translation>ཞ་མུའུ།</translation>
    </message>
    <message>
        <source>Lisu</source>
        <translation>ལི་སུའུ།</translation>
    </message>
    <message>
        <source>Thai</source>
        <translation>ཐཡེ་ལྣེཌ་ཡི་སྐད།</translation>
    </message>
    <message>
        <source>CJK Compatibility Ideographs</source>
        <translation>CJKཡི་མཐུན་འཕྲོད་རང་བཞིན་གྱི་Ideographs</translation>
    </message>
    <message>
        <source>Cyrillic Extended-A</source>
        <translation>ཞི་ལ་ལི་ཡི་རིང་ཚད་ཇེ་རིང་དུ་བཏང་བ།</translation>
    </message>
    <message>
        <source>Cyrillic Extended-B</source>
        <translation>ཞི་ལ་ལི་ཡི་རིང་ཚད་B</translation>
    </message>
    <message>
        <source>Cyrillic Extended-C</source>
        <translation>ཞི་ལ་ལི་ཡི་རིང་ཚད་ཇེ་རིང་དུ་བཏང་།</translation>
    </message>
    <message>
        <source>Bamum</source>
        <translation>པ་མུའུ་</translation>
    </message>
    <message>
        <source>Batak</source>
        <translation>པ་ཐེ་ཁེ།</translation>
    </message>
    <message>
        <source>Buhid</source>
        <translation>པུ་ཧི་ཏི།</translation>
    </message>
    <message>
        <source>Dingbats</source>
        <translation>ཏེན་མག་གི་འཐབ་འཛིང་པ།</translation>
    </message>
    <message>
        <source>Khmer</source>
        <translation>ཁའེ་མའེ་ཨར།</translation>
    </message>
    <message>
        <source>Limbu</source>
        <translation>ལི་མོའུ།</translation>
    </message>
    <message>
        <source>Ogham</source>
        <translation>ཨའོ་ཀེ་ཧན།</translation>
    </message>
    <message>
        <source>Oriya</source>
        <translation>ཨོ་ལི་ཡ།</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
    </message>
    <message>
        <source>Runic</source>
        <translation>བྲོས་བྱོལ་དུ་སོང་བ།</translation>
    </message>
    <message>
        <source>Tamil</source>
        <translation>ཏ་མིལ་སྐད།</translation>
    </message>
    <message>
        <source>IPA Extensions</source>
        <translation>IPAཁ་ལོ་བའི་ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <source>Miscellaneous Mathematical Symbols-A</source>
        <translation>རྩིས་རིག་གི་མཚོན་རྟགས་གཞན་དག-A</translation>
    </message>
    <message>
        <source>Miscellaneous Mathematical Symbols-B</source>
        <translation>རྩིས་རིག་གི་མཚོན་རྟགས་གཞན་དག-B</translation>
    </message>
    <message>
        <source>Halfwidth and Fullwidth Forms</source>
        <translation>ཞེང་ཚད་ཕྱེད་ཀ་དང་ཞེང་ཚད་ཧྲིལ་པོའི་རེའུ་མིག</translation>
    </message>
    <message>
        <source>Hiragana</source>
        <translation>ཧོ་ལ་ཀ་ན་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Cyrillic</source>
        <translation>ཁེ་ལའེ་ལི་ཆི།</translation>
    </message>
    <message>
        <source>Punctuation, Other</source>
        <translation>ཚེག་རྡར་བྱས་ནས་ཚེག་རྡར་བྱས་ཏེ་ཚེག་</translation>
    </message>
    <message>
        <source>Punctuation, Close</source>
        <translation>ཚེག་རྡར་བྱས་ནས་སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Enclosed Alphanumeric Supplement</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་ཨར་ཧྥ་གྲངས་ཀའི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Unified Canadian Aboriginal Syllabics</source>
        <translation>གཅིག་གྱུར་གྱི་ཁ་ན་ཏའི་ཨ་པུའུ་ལིན་གྱི་སློབ་ཁྲིད་རྩ་གནད།</translation>
    </message>
    <message>
        <source>High Private Use Surrogates</source>
        <translation>སྒེར་གྱིས་བཀོལ་སྤྱོད་བྱེད་པའི་ཚབ་བྱེད་མི་སྣས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Number Forms</source>
        <translation>ཨང་ཀིའི་རེའུ་མིག</translation>
    </message>
    <message>
        <source>African Scripts</source>
        <translation>གླིང་ཆེན་ཨ་ཧྥེ་རི་ཁའི་ཡི་གེ</translation>
    </message>
    <message>
        <source>American Scripts</source>
        <translation>ཨ་རིའི་འཁྲབ་གཞུང་།</translation>
    </message>
    <message>
        <source>Kannada</source>
        <translation>ཀ་ན་ཌ་སྐད།</translation>
    </message>
    <message>
        <source>Transport and Map Symbols</source>
        <translation>སྐྱེལ་འདྲེན་དང་ས་བཀྲའི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Mark, Spacing Combining</source>
        <translation>མཱ་ཁི་དང་བར་ཐག་ཟུང་འབྲེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Letter, Lowercase</source>
        <translation>འཕྲིན་ཡིག་དང་འོག་རིམ།</translation>
    </message>
    <message>
        <source>Unified Canadian Aboriginal Syllabics Extended</source>
        <translation>གཅིག་གྱུར་གྱི་ཁ་ན་ཏའི་ཨ་པུའུ་ལིན་གྱི་སློབ་ཁྲིད་རྩ་གནད་ཇེ་རིང་དུ་བཏང་།</translation>
    </message>
    <message>
        <source>Block Elements</source>
        <translation>བཀག་སྡོམ་བྱེད་པའི་རྒྱུ་རྐྱེན</translation>
    </message>
    <message>
        <source>Georgian Extended</source>
        <translation>ཀེ་ལུའུ་ཅི་ཡ་ཡིས་དུས་འགྱངས་བྱས</translation>
    </message>
    <message>
        <source>Syriac Supplement</source>
        <translation>སི་རི་ཡའི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks for Symbols</source>
        <translation>མཚོན་རྟགས་ཀྱི་ནད་རྟགས་དང་ཟུང་འབྲེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Currency Symbols</source>
        <translation>དངུལ་ལོར་གྱི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Separator, Paragraph</source>
        <translation>སོ་སོར་བཀར་ནས་ནང་གསེས་དོན་ཚན</translation>
    </message>
    <message>
        <source>Separator, Line</source>
        <translation>སོ་སོར་བཀར་ནས་སྐུད་པ་</translation>
    </message>
    <message>
        <source>Other, Format</source>
        <translation>གཞན་ཞིག་ནི་རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Malayalam</source>
        <translation>མ་ལེ་ཡ་ལམ་སྐད།</translation>
    </message>
    <message>
        <source>Vertical Forms</source>
        <translation>གཞུང་ཕྱོགས་ཀྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Geometric Shapes Extended</source>
        <translation>དབྱིབས་རྩིས་ཀྱི་དབྱིབས་ཇེ་རིང་དུ་བཏང་།</translation>
    </message>
    <message>
        <source>Mark, Enclosing</source>
        <translation>མཱ་ཁི་དང་བཀག་སྡོམ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Chess Symbols</source>
        <translation>མིག་མངས་ཀྱི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Kangxi Radicals</source>
        <translation>ཁམས་པའི་དྲག་སྐྱོད་ཕྱོགས་ཁག</translation>
    </message>
    <message>
        <source>Cyrillic Supplement</source>
        <translation>ཁེ་ལའེ་ཨེར་གྱི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Mandaic</source>
        <translation>སྤྱི་སྐད།</translation>
    </message>
    <message>
        <source>Samaritan</source>
        <translation>ས་མ་ལ་ཡ།</translation>
    </message>
    <message>
        <source>Other, Private Use</source>
        <translation>གཞན་ཡང་སྒེར་གྱིས་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Miscellaneous Technical</source>
        <translation>ལག་རྩལ་སྣ་མང་ཅན།</translation>
    </message>
    <message>
        <source>Phonetic Extensions</source>
        <translation>སྒྲ་རིག་པའི་ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <source>Sundanese Supplement</source>
        <translation>ཕུན་སུམ་ཚོགས་པའི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Latin-1 Supplement</source>
        <translation>ལ་ཏིང་སྐད་ཀྱི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Control Pictures</source>
        <translation>ཚོད་འཛིན་པར་རིས།</translation>
    </message>
    <message>
        <source>Mark, Non-Spacing</source>
        <translation>མཚོན་རྟགས། བར་སྟོང་མིན་པ།</translation>
    </message>
    <message>
        <source>Miscellaneous Symbols and Pictographs</source>
        <translation>ནོར་འཁྲུལ་གྱི་མཚོན་རྟགས་དང་རི་མོའི་པར་རིས།</translation>
    </message>
    <message>
        <source>CJK Symbols and Punctuation</source>
        <translation>CJKཡི་མཚོན་རྟགས་དང་ཚེག་རྡར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Arabic</source>
        <translation>ཨ་རབ་བྷི་ཡའི་སྐད།</translation>
    </message>
    <message>
        <source>Arrows</source>
        <translation>མདའ་མོ།</translation>
    </message>
    <message>
        <source>Central Asian Scripts</source>
        <translation>ཨེ་ཤ་ཡ་དབུས་མའི་འཁྲབ་གཞུང་།</translation>
    </message>
    <message>
        <source>Coptic</source>
        <translation>ཉེན་རྟོག་པ།</translation>
    </message>
    <message>
        <source>Sundanese</source>
        <translation>ཕུན་སུམ་ཚོགས་པའི་མི།</translation>
    </message>
    <message>
        <source>Other, Control</source>
        <translation>གཞན་ཞིག་ནི་ཚོད་འཛིན་བྱེད་</translation>
    </message>
    <message>
        <source>Mongolian</source>
        <translation>མོན་གྷོ་ལི་ཡའི་སྐད།</translation>
    </message>
    <message>
        <source>Hebrew</source>
        <translation>ཧིབ་བྷུ་རུ་སྐད།</translation>
    </message>
    <message>
        <source>Myanmar</source>
        <translation>འབར་མ།</translation>
    </message>
    <message>
        <source>Punctuation, Initial Quote</source>
        <translation>ཚེག་རྡར་བྱས་ནས་ཐོག་མའི་སྐད་ཆ་དྲངས་པ།</translation>
    </message>
    <message>
        <source>Letterlike Symbols</source>
        <translation>ཡི་གེ་དང་འདྲ་བའི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Vedic Extensions</source>
        <translation>དུས་འགྱངས་བྱ་རྒྱུའི་ཁྱབ་ཁོངས།</translation>
    </message>
    <message>
        <source>Kanbun</source>
        <translation>ཁན་པུན།</translation>
    </message>
    <message>
        <source>Basic Latin</source>
        <translation>གཞི་རྩའི་ལ་ཏིང་སྐད།</translation>
    </message>
    <message>
        <source>Lepcha</source>
        <translation>ལེ་ཕུའུ་ཁྲ།</translation>
    </message>
    <message>
        <source>Symbol, Other</source>
        <translation>མཚོན་རྟགས་གཞན་དག</translation>
    </message>
    <message>
        <source>Arabic Extended-A</source>
        <translation>ཨ་རབ་ཀྱི་སྐད་ཀྱི་རྒྱ་སྐྱེད་A</translation>
    </message>
    <message>
        <source>Rejang</source>
        <translation>རེ་ཅང་།</translation>
    </message>
    <message>
        <source>East Asian Scripts</source>
        <translation>ཨེ་ཤ་ཡ་ཤར་མའི་འཁྲབ་གཞུང་།</translation>
    </message>
    <message>
        <source>Tai Le</source>
        <translation>ཐའེ་ལེ།</translation>
    </message>
    <message>
        <source>Telugu</source>
        <translation>ཏེལ་ལི་གུ་སྐད།</translation>
    </message>
    <message>
        <source>CJK Compatibility</source>
        <translation>CJKཡི་མཐུན་འཕྲོད་</translation>
    </message>
    <message>
        <source>Thaana</source>
        <translation>ས་ན་ཡ།</translation>
    </message>
    <message>
        <source>Syriac</source>
        <translation>སི་རི་ཡ།</translation>
    </message>
    <message>
        <source>Cherokee Supplement</source>
        <translation>Cherokee ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Latin Extended Additional</source>
        <translation>ལ་ཏིང་སྐད་ཀྱི་ཟུར་སྣོན་ཁ་སྣོན</translation>
    </message>
    <message>
        <source>Symbol, Currency</source>
        <translation>མཚོན་རྟགས། དངུལ་ལོར།</translation>
    </message>
    <message>
        <source>Other, Surrogate</source>
        <translation>གཞན་ཞིག་ནི་ཚབ་བྱེད་མི་2</translation>
    </message>
    <message>
        <source>Domino Tiles</source>
        <translation>ཏོ་སྨི་ནོ་སོ་ཕག</translation>
    </message>
    <message>
        <source>Punctuation, Connector</source>
        <translation>ཚེག་རྡར་བྱས་ནས་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Private Use Area</source>
        <translation>སྒེར་གྱི་བཀོལ་སྤྱོད་ཁུལ།</translation>
    </message>
    <message>
        <source>Indonesia and Oceania Scripts</source>
        <translation>ཧིན་རྡུ་ཉི་ཞི་ཡ་དང་རྒྱ་མཚོ་ཆེན་མོའི་ཡི་གེ།</translation>
    </message>
    <message>
        <source>Supplemental Symbols and Pictographs</source>
        <translation>ཁ་གསབ་ཀྱི་མཚོན་རྟགས་དང་རི་མོ།</translation>
    </message>
    <message>
        <source>Geometric Shapes</source>
        <translation>དབྱིབས་རྩིས་ཀྱི་དབྱིབས་གཞི།</translation>
    </message>
    <message>
        <source>Variation Selectors</source>
        <translation>འགྱུར་ལྡོག་གདམ་གསེས་བྱེད་མཁན།</translation>
    </message>
    <message>
        <source>Bopomofo</source>
        <translation>པོ་པོ་མོ་ཧྥུ།</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation>བརྙན་རིས་རིག་པ།</translation>
    </message>
    <message>
        <source>Specials</source>
        <translation>ཁྱད་ལྡན་ཐོན་རྫས།</translation>
    </message>
    <message>
        <source>Ethiopic Supplement</source>
        <translation>མི་ཆོས་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Separator, Space</source>
        <translation>ཁ་གྱེས་འཕྲུལ་ཆས། བར་སྟོང་།</translation>
    </message>
    <message>
        <source>Hangul Jamo Extended-A</source>
        <translation>ཧང་ག་ལ་ཅ་མའོ་ཡིས་དུས་འགྱངས་བྱས་པའི་A</translation>
    </message>
    <message>
        <source>Hangul Jamo Extended-B</source>
        <translation>ཧང་ག་ལ་ཅ་མའོ་ཡིས་དུས་འགྱངས་བྱས་པའི་B</translation>
    </message>
    <message>
        <source>High Surrogates</source>
        <translation>མཐོ་རིམ་ཉེན་རྟོག་པ།</translation>
    </message>
    <message>
        <source>Greek Extended</source>
        <translation>ཀེ་རི་སེའི་རྒྱ་སྐྱེད་</translation>
    </message>
    <message>
        <source>Letter, Titlecase</source>
        <translation>འཕྲིན་ཡིག་དང་ཁ་བྱང་།</translation>
    </message>
    <message>
        <source>Myanmar Extended-A</source>
        <translation>འབར་མའི་ཁྱབ་གདལ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Myanmar Extended-B</source>
        <translation>འབར་མའི་ཁྱབ་གདལ་B</translation>
    </message>
    <message>
        <source>Phonetic Extensions Supplement</source>
        <translation>སྒྲ་རིག་པའི་ཁྱབ་གདལ་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Alphabetic Presentation Forms</source>
        <translation>དབྱངས་གསལ་གྱི་ངོ་སྤྲོད་རེའུ་མིག</translation>
    </message>
    <message>
        <source>Number, Decimal Digit</source>
        <translation>ཨང་ཀི། གྲངས་ཀ་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Tai Tham</source>
        <translation>ཐའེ</translation>
    </message>
    <message>
        <source>Tai Viet</source>
        <translation>ཐའེ་ཝེ་ཐེ།</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks Extended</source>
        <translation>ཏའེ་ཁེ་ཡི་མཚོན་རྟགས་དང་ཟུང་འབྲེལ་བྱས་ནས་ཇེ་རིང་དུ་བཏང་།</translation>
    </message>
    <message>
        <source>Tagbanwa</source>
        <translation>ཐ་པན་ཝ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Miscellaneous Symbols</source>
        <translation>མཚོན་རྟགས་གཞན་དག</translation>
    </message>
    <message>
        <source>Yi Radicals</source>
        <translation>དབྱི་དབྱི་སི་ལན་རྒྱལ་ཁབ་ཅེས་</translation>
    </message>
    <message>
        <source>Middle Eastern Scripts</source>
        <translation>དབུས་ཤར་གྱི་འཁྲབ་གཞུང་།</translation>
    </message>
    <message>
        <source>General Punctuation</source>
        <translation>སྤྱིར་བཏང་གི་ཚེག་རྡར་བྱས་ནས་ཚེག་</translation>
    </message>
    <message>
        <source>Balinese</source>
        <translation>པ་ལིན་སི།</translation>
    </message>
    <message>
        <source>Combining Diacritics</source>
        <translation>གཅིན་སྙི་ཟ་ཁུའི་ནད་དང་ཟུང་འབྲེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Devanagari</source>
        <translation>ཏེ་ཝན་ན་ཀ་ལི།</translation>
    </message>
    <message>
        <source>Braille Patterns</source>
        <translation>ལོང་བའི་དཔེ་དབྱིབས་</translation>
    </message>
    <message>
        <source>Saurashtra</source>
        <translation>ས་ལ་ཧྲི་ཐེ་ལ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Katakana</source>
        <translation>ཁ་ཐ་ཁ་ན་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Mathematical Symbols</source>
        <translation>རྩིས་རིག་གི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Bengali</source>
        <translation>བྷེང་གྷ་ལིའི་སྐད།</translation>
    </message>
    <message>
        <source>Javanese</source>
        <translation>ཅ་ཝེ་སི།</translation>
    </message>
    <message>
        <source>Yi Syllables</source>
        <translation>དབྱིས་རིགས་ཀྱི་སྒྲ་ཚིགས་</translation>
    </message>
    <message>
        <source>Devanagari Extended</source>
        <translation>ཏི་ཝན་ན་ཅ་ལེ་ཡིས་དུས་འགྱངས་བྱས།</translation>
    </message>
    <message>
        <source>Kayah Li</source>
        <translation>ཁའེ་ཡ་ལི།</translation>
    </message>
    <message>
        <source>Number, Letter</source>
        <translation>ཨང་གྲངས། འཕྲིན་ཡིག</translation>
    </message>
    <message>
        <source>Armenian</source>
        <translation>ཨར་མེ་ནི་ཡའི་སྐད།</translation>
    </message>
    <message>
        <source>Other, Not Assigned</source>
        <translation>གཞན་ཞིག་ནི་བགོ་བཤའ་མ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <source>Ethiopic Extended</source>
        <translation>མི་ཆོས་རིང་དུ་བསྲིངས་པ།</translation>
    </message>
    <message>
        <source>Ethiopic Extended-A</source>
        <translation>མི་ཆོས་རིང་དུ་བསྲིངས་པའི་A</translation>
    </message>
    <message>
        <source>Optical Character Recognition</source>
        <translation>འོད་རིག་གི་མི་སྣའི་ངོས་འཛིན།</translation>
    </message>
    <message>
        <source>Yijing Hexagram Symbols</source>
        <translation>དབྱི་ཅིན་གྱི་དཔེ་རིས་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Sinhala</source>
        <translation>ཞིན་ཧ་ལ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Mahjong Tiles</source>
        <translation>མ་ཅང་སོ་ཕག</translation>
    </message>
    <message>
        <source>New Tai Lue</source>
        <translation>ཐའེ་ལའེ་གསར་པ།</translation>
    </message>
    <message>
        <source>Arabic Supplement</source>
        <translation>ཨ་རབ་ཀྱི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Enclosed CJK Letters and Months</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་CJKའཕྲིན་ཡིག་དང་ཟླ་བ་འགའ།</translation>
    </message>
    <message>
        <source>Supplemental Punctuation</source>
        <translation>ཁ་གསབ་ཀྱི་ཚེག་རྡར་བྱས་ནས་ཚེག་རྡར་བྱས</translation>
    </message>
    <message>
        <source>Tagalog</source>
        <translation>ཐ་ཅ་ལོ་ཀི།</translation>
    </message>
    <message>
        <source>Miscellaneous Symbols and Arrows</source>
        <translation>མཚོན་རྟགས་དང་མདའ་མོ་གཞན་དག</translation>
    </message>
    <message>
        <source>&lt;Private Use High Surrogate&gt;</source>
        <translation>&lt;བདག་སྤྱོད་བྱེད་པར་ཚད་མཐོའི་ཚབ་བྱེད་མི་&gt;</translation>
    </message>
    <message>
        <source>Letter, Uppercase</source>
        <translation>ཡི་གེ་དང་སྟེང་གི་ཡི་གེ།</translation>
    </message>
    <message>
        <source>Ethiopic</source>
        <translation>མི་ཆོས་རིང་རིགས།</translation>
    </message>
    <message>
        <source>Tibetan</source>
        <translation>བོད་ཡིག</translation>
    </message>
    <message>
        <source>Combining Half Marks</source>
        <translation>ཕྱེད་ཀའི་རྟགས་དང་ཟུང་འབྲེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>CJK Strokes</source>
        <translation>ཀྲུང་གོ་དང་ལ་ཞི་བདེ་རྒྱ་མཚོ་ཆེ་མོ་</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Punctuation, Dash</source>
        <translation>ཚེག་རྡར་བྱས་ནས་ཚེག་རྡར་བྱས་ཏེ་ཚེག་</translation>
    </message>
    <message>
        <source>Punctuation, Open</source>
        <translation>ཚེག་རྡར་བྱས་ནས་སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>&lt;Private Use&gt;</source>
        <translation>&lt;བེད་སྤྱོད་བྱ་རྒྱུ་དེ་ཡིན&gt;</translation>
    </message>
    <message>
        <source>Khmer Symbols</source>
        <translation>ཁའེ་མའེ་ཡི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Mathematical Operators</source>
        <translation>རྩིས་རིག་གི་ལས་ཀ་བྱེད་མཁན།</translation>
    </message>
    <message>
        <source>Non-printable</source>
        <translation>པར་སྐྲུན་བྱེད་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>CJK Unified Ideographs</source>
        <translation>CJKགཉིས་གཅིག་གྱུར་གྱི་Ideographs</translation>
    </message>
    <message>
        <source>Symbol, Math</source>
        <translation>མཚོན་རྟགས། རྩིས་རིག</translation>
    </message>
    <message>
        <source>South Asian Scripts</source>
        <translation>ཨེ་ཤེ་ཡ་ལྷོ་མའི་འཁྲབ་གཞུང་།</translation>
    </message>
    <message>
        <source>Supplemental Arrows-A</source>
        <translation>ཁ་གསབ་ཀྱི་མདའ་མོ་A</translation>
    </message>
    <message>
        <source>Supplemental Arrows-B</source>
        <translation>ཁ་གསབ་ཀྱི་མདའ་མོ་B</translation>
    </message>
    <message>
        <source>Supplemental Arrows-C</source>
        <translation>ཁ་གསབ་ཀྱི་མདའ་མོ་-C</translation>
    </message>
    <message>
        <source>Hangul Compatibility Jamo</source>
        <translation>ཧང་ག་རི་དང་ཕན་ཚུན་མཐུན་པའི་ཅ་མའོ་</translation>
    </message>
    <message>
        <source>Superscripts and Subscripts</source>
        <translation>ཚད་བརྒལ་ཡི་གེ་དང་ཡན་ལག་ཡི་གེ།</translation>
    </message>
    <message>
        <source>Number, Other</source>
        <translation>ཨང་གྲངས་གཞན་དག</translation>
    </message>
    <message>
        <source>CJK Unified Ideographs Extension A</source>
        <translation>CJKགཉིས་གཅིག་གྱུར་གྱི་པར་རིས་ཁྱབ་གདལ་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Hangul Jamo</source>
        <translation>ཧང་ག་ལ་ཅའོ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Bopomofo Extended</source>
        <translation>པོ་པོ་མོ་ཧྥུ་ཡིས་དུས་འགྱངས་བྱས་པ།</translation>
    </message>
    <message>
        <source>Box Drawing</source>
        <translation>སྒམ་གྱི་རི་མོ།</translation>
    </message>
    <message>
        <source>Buginese</source>
        <translation>འབུ་ཕྲ་གསོད་པ།</translation>
    </message>
    <message>
        <source>Gujarati</source>
        <translation>གུ་ཇུ་ར་ཐིའི་སྐད།</translation>
    </message>
    <message>
        <source>Katakana Phonetic Extensions</source>
        <translation>ཁ་ཐ་ཁ་ན་སྒྲ་རིག་པའི་ཁྱབ་གདལ།</translation>
    </message>
    <message>
        <source>Spacing Modifier Letters</source>
        <translation>བར་ཐག་རིང་བའི་བཟོ་བཅོས་རྒྱག་མཁན་གྱི་འཕྲིན་ཡིག</translation>
    </message>
    <message>
        <source>Latin Extended-A</source>
        <translation>ལ་ཏིང་སྐད་ཀྱི་ཁྱབ་གདལ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Latin Extended-B</source>
        <translation>ལ་ཏིང་སྐད་ཀྱི་ཁྱབ་གདལ་B</translation>
    </message>
    <message>
        <source>Latin Extended-C</source>
        <translation>ལ་ཏིང་སྐད་ཀྱི་ཁྱབ་གདལ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Latin Extended-D</source>
        <translation>ལ་ཏིང་སྐད་ཀྱི་ཁྱབ་གདལ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>Latin Extended-E</source>
        <translation>ལ་ཏིང་སྐད་ཀྱི་ཁྱབ་གདལ་གཏོང་བའི་E</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <source>Meetei Mayek Extensions</source>
        <translation>མའེ་མའེ་ཁེ་དུས་འགྱངས་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <source>Georgian</source>
        <translation>ཇོར་ཡི་ཡའི་སྐད།</translation>
    </message>
    <message>
        <source>Arabic Presentation Forms-A</source>
        <translation>ཨ་རབ་ཀྱི་སྐད་ཀྱི་ངོ་སྤྲོད་རེའུ་མིག-A</translation>
    </message>
    <message>
        <source>Arabic Presentation Forms-B</source>
        <translation>ཨ་རབ་ཀྱི་སྐད་ཀྱི་ངོ་སྤྲོད་རེའུ་མིག-B</translation>
    </message>
    <message>
        <source>Gurmukhi</source>
        <translation>ཀུའུ་མུའུ་ཆི།</translation>
    </message>
    <message>
        <source>&lt;not assigned&gt;</source>
        <translation>&lt;གོ་བགོས་བྱས་མེད&gt;</translation>
    </message>
    <message>
        <source>Letter, Other</source>
        <translation>འཕྲིན་ཡིག་གཞན་དག</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks Supplement</source>
        <translation>བཤལ་རྟགས་ཀྱི་ཁ་གསབ་དང་ཟུང་འབྲེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Enclosed Ideographic Supplement</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་ཡི་བརྙན་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Symbol, Modifier</source>
        <translation>མཚོན་རྟགས། བཟོ་བཅོས་རྒྱག་མཁན།</translation>
    </message>
    <message>
        <source>&lt;noncharacter&gt;</source>
        <translation>&lt;ཁྱད་ཆོས་མེད་པའི་མི།&gt;</translation>
    </message>
    <message>
        <source>Symbols and Pictographs Extended-A</source>
        <translation>མཚོན་རྟགས་དང་རི་མོའི་རིང་ཚད་ཇེ་རིང་དུ་བཏང་།</translation>
    </message>
    <message>
        <source>Ornamental Dingbats</source>
        <translation>རྒྱན་སྤྲས་པའི་ཏིན་པ་སི།</translation>
    </message>
    <message>
        <source>Alchemical Symbols</source>
        <translation>གསེར་བཙོས་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Hangul Syllables</source>
        <translation>ཧང་ག་རིའི་སྒྲ་ཚིགས།</translation>
    </message>
    <message>
        <source>Modifier Tone Letters</source>
        <translation>བཟོ་བཅོས་རྒྱག་མཁན་གྱི་སྒྲ་གདངས་འཕྲིན་</translation>
    </message>
    <message>
        <source>Combining Diacritical Marks</source>
        <translation>གཉིས་ལྡན་གྱི་མཚོན་རྟགས་ཟུང་འབྲེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>&lt;Non Private Use High Surrogate&gt;</source>
        <translation>&lt;མི་སྒེར་གྱིས་ཚད་མཐོའི་ཚབ་བྱེད་མི་&gt;</translation>
    </message>
    <message>
        <source>Ideographic Description Characters</source>
        <translation>གཟུགས་བརྙན་གྱི་གསལ་བཤད་ཡི་གེ</translation>
    </message>
    <message>
        <source>CJK Radicals Supplement</source>
        <translation>CJK དྲག་སྐྱོད་ཕྱོགས་ཁག་གི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Ol Chiki</source>
        <translation>ཨོ་ཨར་ཆི་ཅི།</translation>
    </message>
    <message>
        <source>Supplemental Mathematical Operators</source>
        <translation>ཁ་གསབ་རྩིས་རིག་གི་ལས་ཀ་བྱེད་མཁན།</translation>
    </message>
    <message>
        <source>Phonetic Symbols</source>
        <translation>སྒྲ་རིག་པའི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Playing Cards</source>
        <translation>ཤོག་ཕེ་རྩེ་བ།</translation>
    </message>
    <message>
        <source>Glagolitic</source>
        <translation>འཁྱགས་རོམ་གྱི་ནད།</translation>
    </message>
    <message>
        <source>Phags-pa</source>
        <translation>Phags-pa</translation>
    </message>
    <message>
        <source>Hanunoo</source>
        <translation>ཧན་ནུའུ།</translation>
    </message>
    <message>
        <source>Meetei Mayek</source>
        <translation>མའེ་མའེ་ཁེ།</translation>
    </message>
    <message>
        <source>Cherokee</source>
        <translation>ཞའོ་ཁི་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Punctuation, Final Quote</source>
        <translation>ཚེག་རྡར་བྱས་ནས་མཐའ་མའི་སྐད་ཆ་དྲངས་པ།</translation>
    </message>
    <message>
        <source>CJK Compatibility Forms</source>
        <translation>CJKཡི་མཐུན་འཕྲོད་རེའུ་མིག</translation>
    </message>
    <message>
        <source>Georgian Supplement</source>
        <translation>ཀེ་ལུའུ་ཅི་ཡའི་ཁ་གསབ།</translation>
    </message>
    <message>
        <source>Syloti Nagri</source>
        <translation>སི་ལོ་ཐི་ན་ཀི་ལི་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Greek and Coptic</source>
        <translation>ཀེ་རི་སེའི་སྐད་དང་ཁའོ་ཕུའུ་ཆི།</translation>
    </message>
    <message>
        <source>Enclosed Alphanumerics</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་དབྱངས་གསལ་ཡི་གེའི་གྲངས་ཀ།</translation>
    </message>
    <message>
        <source>Letter, Modifier</source>
        <translation>འཕྲིན་ཡིག་དང་བཟོ་བཅོས་རྒྱག་མཁན།</translation>
    </message>
    <message>
        <source>Common Indic Number Forms</source>
        <translation>ཐུན་མོང་གི་ཨང་ཀིའི་རེའུ་མིག</translation>
    </message>
    <message>
        <source>Low Surrogates</source>
        <translation>ཚབ་བྱེད་མི་སྣས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>&lt;Low Surrogate&gt;</source>
        <translation>&lt;མན་གྱི་ཚབ་བྱེད་མི་&gt;</translation>
    </message>
    <message>
        <source>European Scripts</source>
        <translation>ཡོ་རོབ་གླིང་གི་འཁྲབ་གཞུང་།</translation>
    </message>
    <message>
        <source>Tifinagh</source>
        <translation>ཐི་ཧྥེ་ན་ཀེ་ཡིས་བཤད་རྒྱུར།</translation>
    </message>
    <message>
        <source>Southeast Asian Scripts</source>
        <translation>ཨེ་ཤ་ཡ་ཤར་ལྷོའི་ས་ཁུལ་གྱི་ཡི་</translation>
    </message>
    <message>
        <source>Small Form Variants</source>
        <translation>རྣམ་པ་ཆུང་བའི་འགྱུར་ལྡོག</translation>
    </message>
</context>
<context>
    <name>KEditListWidget</name>
    <message>
        <source>&amp;Add</source>
        <translation>&gt; ཁ་སྣོན་</translation>
    </message>
    <message>
        <source>Move &amp;Up</source>
        <translation>འགུལ་སྐྱོད ་བྱེད་པ །</translation>
    </message>
    <message>
        <source>Move &amp;Down</source>
        <translation>གནས་སྤོ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>&amp;Remove</source>
        <translation>&gt; མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>KFontChooser</name>
    <message>
        <source>Bold</source>
        <translation>སྤོབས་པ་ཆེ་བ།</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>ཡིག་གཟུགས།</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Font:</source>
        <translation>ཡིག་གཟུགས་ནི།</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>ཆེ་ཆུང་ནི།</translation>
    </message>
    <message>
        <source>Here you can choose the font to be used.</source>
        <translation>འདི་ནས་ཁྱེད་ཚོས་བཀོལ་སྤྱོད་བྱེད་དགོས་པའི་ཡིག་གཟུགས་བདམས་ཆོག</translation>
    </message>
    <message>
        <source>Change font family?</source>
        <translation>ཡིག་གཟུགས་ཁྱིམ་ཚང་ལ་འགྱུར་ལྡོག་གཏོང་</translation>
    </message>
    <message>
        <source>The Quick Brown Fox Jumps Over The Lazy Dog</source>
        <translation>མྱུར་ཚད་མགྱོགས་པའི་ཁམ་མདོག་གི་ཝ་སི་ཡིས་སྒྱིད་ལུག་གི་ཁྱི་ལས་བརྒལ།</translation>
    </message>
    <message>
        <source>This sample text illustrates the current settings. You may edit it to test special characters.</source>
        <translation>དཔེ་མཚོན་ཡི་གེ་འདིས་མིག་སྔའི་སྒྲིག་བཀོད་གསལ་བཤད་བྱས་ཡོད། ཁྱེད་ཚོས་རྩོམ་སྒྲིག་བྱས་ནས་དམིགས་བསལ་གྱི་མི་སྣ་ཚོད་ལྟ་བྱས་ཆོག</translation>
    </message>
    <message>
        <source>Change font style?</source>
        <translation>ཡིག་གཟུགས་ཀྱི་རྣམ་པ་བསྒྱུར་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Here you can choose the font style to be used.</source>
        <translation>འདི་ནས་ཁྱེད་ཚོས་བཀོལ་སྤྱོད་བྱེད་དགོས་པའི་ཡིག་གཟུགས་ཀྱི་རྣམ་པ་བདམས་ཆོག</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>དབྱི་སི་ལན་ཆོས་ཁང་།</translation>
    </message>
    <message>
        <source>Bold Italic</source>
        <translation>བློ་ཁོག་ཆེན་པོས་དབྱི་སི་ལན་ཆོས་ཁང</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>རྒྱུན་ལྡན་གྱི་གནས་</translation>
    </message>
    <message>
        <source>Oblique</source>
        <translation>གསེག་པ།</translation>
    </message>
    <message>
        <source>Font style</source>
        <translation>ཡིག་གཟུགས་ཀྱི་ཉམས་འགྱུར།</translation>
    </message>
    <message>
        <source>Here you can switch between fixed font size and font size to be calculated dynamically and adjusted to changing environment (e.g. widget dimensions, paper size).</source>
        <translation>འདིར་ཁྱེད་ཚོས་གཏན་འཇགས་ཀྱི་ཡིག་གཟུགས་ཆེ་ཆུང་དང་ཡིག་གཟུགས་ཀྱི་ཆེ་ཆུང་བར་དུ་བརྗེ་རེས་བྱས་ནས་འགུལ་རྣམ་གྱིས་རྩིས་བརྒྱབ་སྟེ་འགྱུར་ལྡོག་འབྱུང་བཞིན་པའི་ཁོར་ཡུག་ལ་ལེགས་སྒྲིག་བྱས་ཆོག</translation>
    </message>
    <message>
        <source>Font style:</source>
        <translation>ཡིག་གཟུགས་ཀྱི་ཉམས་འགྱུར་ནི།</translation>
    </message>
    <message>
        <source>Requested Font</source>
        <translation>རེ་འདུན་ཞུས་པའི་ཡིག་གཟུགས།</translation>
    </message>
    <message>
        <source>Font size&lt;br /&gt;&lt;i&gt;fixed&lt;/i&gt; or &lt;i&gt;relative&lt;/i&gt;&lt;br /&gt;to environment</source>
        <translation>ཡིག་གཟུགས་ཆེ་ཆུང་།&lt;br /&gt;&lt;i&gt;fixed&lt;/i&gt;,&lt;i&gt;བལྟོས་བཅས་&lt;/i&gt;&lt;br /&gt;&lt;br /&gt;ནས་ཁོར་ཡུག་ལ་ཁ་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <source>Here you can choose the font family to be used.</source>
        <translation>འདི་ནས་ཁྱེད་ཚོས་བཀོལ་སྤྱོད་བྱེད་དགོས་པའི་ཡིག་གཟུགས་ཁྱིམ་ཚང་བདམས་ཆོག</translation>
    </message>
    <message>
        <source>Enable this checkbox to change the font style settings.</source>
        <translation>ཞིབ་བཤེར་སྒམ་འདིས་ཡིག་གཟུགས་ཀྱི་རྣམ་པ་བསྒྱུར་ཐུབ་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Change font size?</source>
        <translation>ཡིག་གཟུགས་ཀྱི་ཆེ་ཆུང་ལ་འགྱུར་ལྡོག་</translation>
    </message>
    <message>
        <source>Relative</source>
        <translation>བལྟོས་བཅས་ཀྱི་མི་</translation>
    </message>
    <message>
        <source>Enable this checkbox to change the font size settings.</source>
        <translation>ཞིབ་བཤེར་སྒམ་འདིས་ཡིག་གཟུགས་ཆེ་ཆུང་གི་སྒྲིག་བཀོད་ལ་འགྱུར་ལྡོག་གཏོང་ཐུབ།</translation>
    </message>
    <message>
        <source>Enable this checkbox to change the font family settings.</source>
        <translation>ཞིབ་བཤེར་སྒམ་འདིས་ཡིག་གཟུགས་ཁྱིམ་ཚང་གི་སྒྲིག་བཀོད་ལ་འགྱུར་ལྡོག་གཏོང་ཐུབ།</translation>
    </message>
    <message>
        <source>Here you can choose the font size to be used.</source>
        <translation>འདི་ནས་ཁྱེད་ཚོས་བཀོལ་སྤྱོད་བྱེད་དགོས་པའི་ཡིག་གཟུགས་ཀྱི་ཆེ་ཆུང་བདམས་ཆོག</translation>
    </message>
</context>
<context>
    <name>KAssistantDialog</name>
    <message>
        <source>Next</source>
        <translation>གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <source>&amp;Back</source>
        <translation>ཕྱིར་ནུར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <source>Go back one step</source>
        <translation>ཕྱིར་ནུར་བྱས་ནས་གོམ་པ་གཅིག་</translation>
    </message>
</context>
<context>
    <name>KActionSelector</name>
    <message>
        <source>&amp;Available:</source>
        <translation>&gt; སྤྱོད་གོ་ཆོད་པ་སྟེ།</translation>
    </message>
    <message>
        <source>&amp;Selected:</source>
        <translation>བདམས་ཟིན་པ་གཤམ་གསལ།</translation>
    </message>
</context>
<context>
    <name>KCharSelect</name>
    <message>
        <source>&amp;Back</source>
        <translation>ཕྱིར་ནུར་བྱས་པ།</translation>
    </message>
    <message>
        <source>XML decimal entity:</source>
        <translation>XMLཡི་ཆེས་ཆུང་བའི་དངོས་གཟུགས་ནི།</translation>
    </message>
    <message>
        <source>CJK Ideograph Information</source>
        <translation>CJK Ideograph ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Mandarin Pronunciation: </source>
        <translation>སྤྱི་སྐད་ཀྱི་སྒྲ་གདངས་ནི། </translation>
    </message>
    <message>
        <source>Next in History</source>
        <translation>ལོ་རྒྱུས་ཐོག་གི་རྗེས་མ།</translation>
    </message>
    <message>
        <source>Select a category</source>
        <translation>རིགས་ཤིག་བདམས་པ།</translation>
    </message>
    <message>
        <source>Decomposition:</source>
        <translation>དབྱེ་འབྱེད་ནི།</translation>
    </message>
    <message>
        <source>Alias names:</source>
        <translation>ཨ་ལ་སི་ཡི་མིང་ནི།</translation>
    </message>
    <message>
        <source>Unicode category: </source>
        <translation>Unicode category: </translation>
    </message>
    <message>
        <source>Equivalents:</source>
        <translation>རིན་མཉམ་དངོས་ཟོག་ནི།</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation>མིང་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <source>Notes:</source>
        <translation>ཟིན་བྲིས་སུ་བཀོད་པ་གཤམ</translation>
    </message>
    <message>
        <source>UTF-8:</source>
        <translation>UTF-8:</translation>
    </message>
    <message>
        <source>Tang Pronunciation: </source>
        <translation>ཐང་གི་སྒྲ་གདངས་ནི། </translation>
    </message>
    <message>
        <source>Previous in History</source>
        <translation>ལོ་རྒྱུས་ཐོག་གི་སྔོན་ཆད།</translation>
    </message>
    <message>
        <source>See also:</source>
        <translation>ད་དུང་གསལ་བཤད་བྱ་རྒྱུ་སྟེ།</translation>
    </message>
    <message>
        <source>Korean Pronunciation: </source>
        <translation>ཁྲའོ་ཞན་གྱི་སྒྲ་གདངས་ནི། </translation>
    </message>
    <message>
        <source>Japanese Kun Pronunciation: </source>
        <translation>འཇར་པན་གྱི་ཁུན་གྱི་སྒྲ་གདངས་ནི། </translation>
    </message>
    <message>
        <source>Character:</source>
        <translation>གཤིས་ཀ་ནི།</translation>
    </message>
    <message>
        <source>General Character Properties</source>
        <translation>སྤྱིར་བཏང་གི་གཤིས་ཀའི་ངོ་བོ</translation>
    </message>
    <message>
        <source>UTF-16: </source>
        <translation>UTF-16: </translation>
    </message>
    <message>
        <source>Next Character in History</source>
        <translation>ལོ་རྒྱུས་ཐོག་གི་མི་སྣ་རྗེས་མ།</translation>
    </message>
    <message>
        <source>Various Useful Representations</source>
        <translation>སྤྱོད་གོ་ཆོད་པའི་མཚོན་བྱེད་སྣ་ཚོགས</translation>
    </message>
    <message>
        <source>Block: </source>
        <translation>བཀག་སྡོམ་བྱེད་པ་སྟེ། </translation>
    </message>
    <message>
        <source>C octal escaped UTF-8: </source>
        <translation>C octal བྲོས་བྱོལ་དུ་སོང་བའི་UTF-8: </translation>
    </message>
    <message>
        <source>Annotations and Cross References</source>
        <translation>མཆན་འགྲེལ། རྒྱ་གྲམ་དམར་པོའི་དཔྱད་གཞིའི་ཡིག་རིགས།</translation>
    </message>
    <message>
        <source>Cantonese Pronunciation: </source>
        <translation>སྐད་ཡིག་གི་སྒྲ་གདངས་ནི། </translation>
    </message>
    <message>
        <source>Select a block to be displayed</source>
        <translation>འགྲེམས་སྟོན་བྱེད་དགོས་པའི་སྲང་ལམ་ཞིག་བདམས་པ།</translation>
    </message>
    <message>
        <source>Enter a search term or character here</source>
        <translation>འདི་ནས་འཚོལ་ཞིབ་ཀྱི་ཐ་སྙད་ཅིག་གམ་ཡང་ན་ཡི་གེ</translation>
    </message>
    <message>
        <source>Set font</source>
        <translation>ཡིག་གཟུགས་གཏན་འཁེལ་བྱ་དགོས</translation>
    </message>
    <message>
        <source>Approximate equivalents:</source>
        <translation>ཧ་ལམ་འདྲ་མཚུངས་ཡིན་པ་སྟེ།</translation>
    </message>
    <message>
        <source>Set font size</source>
        <translation>ཡིག་གཟུགས་ཀྱི་ཆེ་ཆུང་གཏན་འཁེལ</translation>
    </message>
    <message>
        <source>&amp;Find...</source>
        <translation>བཙལ་ནས་རྙེད་བྱུང་།</translation>
    </message>
    <message>
        <source>&amp;Forward</source>
        <translation>མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <source>Previous Character in History</source>
        <translation>ལོ་རྒྱུས་ཐོག་གི་སྔོན་ཆད་ཀྱི་གཤིས་</translation>
    </message>
    <message>
        <source>Japanese On Pronunciation: </source>
        <translation>འཇར་པན་གྱི་སྒྲ་གདངས་སྐོར། </translation>
    </message>
    <message>
        <source>Definition in English: </source>
        <translation>དབྱིན་ཡིག་གི་མཚན་ཉིད་ནི། </translation>
    </message>
</context>
<context>
    <name>KDatePicker</name>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Next month</source>
        <translation>ཟླ་བ་རྗེས་མ།</translation>
    </message>
    <message>
        <source>Select the current day</source>
        <translation>ད་ལྟའི་ཉིན་མོ་འདེམས་སྒྲུག་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Previous year</source>
        <translation>ལོ་སྔོན་མ།</translation>
    </message>
    <message>
        <source>Week %1</source>
        <translation>གཟའ་འཁོར་རེར་%1</translation>
    </message>
    <message>
        <source>Previous month</source>
        <translation>ཟླ་སྔོན་མ།</translation>
    </message>
    <message>
        <source>Select a month</source>
        <translation>ཟླ་བ་གཅིག་གི་ནང་དུ་</translation>
    </message>
    <message>
        <source>Next year</source>
        <translation>སང་ལོར།</translation>
    </message>
    <message>
        <source>Select a year</source>
        <translation>ལོ་གཅིག་བདམས་པ།</translation>
    </message>
    <message>
        <source>Select a week</source>
        <translation>གཟའ་འཁོར་གཅིག་གི་ནང་བདམས་</translation>
    </message>
</context>
<context>
    <name>KMessageBox</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Sorry</source>
        <translation>དགོངས་དག</translation>
    </message>
    <message>
        <source>Do not show this message again</source>
        <translation>ཆ་འཕྲིན་འདི་སླར་ཡང་མི་མངོན་པ།</translation>
    </message>
    <message>
        <source>Do not ask again</source>
        <translation>སླར་ཡང་བླང་བྱ་འདོན་མི་རུང་།</translation>
    </message>
    <message>
        <source>Information</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <source>Question</source>
        <translation>གནད་དོན།</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
</context>
<context>
    <name>KFontRequester</name>
    <message>
        <source>Preview of the &quot;%1&quot; font</source>
        <translation>&quot;%1&quot;ཡིག་གཟུགས་ཀྱི་སྔོན་བརྡ།</translation>
    </message>
    <message>
        <source>Choose Font...</source>
        <translation>ཡིག་གཟུགས་འདེམས་པ...</translation>
    </message>
    <message>
        <source>Preview of the selected font</source>
        <translation>བདམས་ཟིན་པའི་ཡིག་གཟུགས་ཀྱི་སྔོན་བརྡ།</translation>
    </message>
    <message>
        <source>This is a preview of the &quot;%1&quot; font. You can change it by clicking the &quot;Choose Font...&quot; button.</source>
        <translation>འདི་ནི་&quot;%1&quot;ཡིག་གཟུགས་ཀྱི་སྔོན་བརྡ་ཡིན། ཁྱོད་ཀྱིས་&quot;གདམ་གསེས་ཡིག་གཟུགས་&quot;ལ་བརྟེན་ནས་འགྱུར་ལྡོག་གཏོང་ཐུབ།... མཐེབ་གཅུས།</translation>
    </message>
    <message>
        <source>This is a preview of the selected font. You can change it by clicking the &quot;Choose Font...&quot; button.</source>
        <translation>འདི་ནི་བདམས་ཟིན་པའི་ཡིག་གཟུགས་ཀྱི་སྔོན་བརྡ་ཡིན། ཁྱོད་ཀྱིས་&quot;གདམ་གསེས་ཡིག་གཟུགས་&quot;ལ་བརྟེན་ནས་འགྱུར་ལྡོག་གཏོང་ཐུབ།... མཐེབ་གཅུས།</translation>
    </message>
</context>
<context>
    <name>FontHelpers</name>
    <message>
        <source>Serif</source>
        <translation>སེ་ལེ་ཧྥུའུ།</translation>
    </message>
    <message>
        <source>Sans Serif</source>
        <translation>སན་སི་སེ་ལེ་ཧྥུའུ།</translation>
    </message>
    <message>
        <source>Monospace</source>
        <translation>གཅིག་རྐྱང་གི་བར་སྟོང་།</translation>
    </message>
</context>
<context>
    <name>KDateComboBox</name>
    <message>
        <source>Today</source>
        <translation>དེ་རིང་།</translation>
    </message>
    <message>
        <source>The date you entered is invalid</source>
        <translation>ཁྱེད་ཀྱིས་ནང་འཇུག་བྱས་པའི་དུས་ཚོད་ནི་གོ་མི་ཆོད་པ་ཡིན།</translation>
    </message>
    <message>
        <source>No Date</source>
        <translation>དུས་ཚོད་མེད་པ།</translation>
    </message>
    <message>
        <source>Tomorrow</source>
        <translation>སང་ཉིན།</translation>
    </message>
    <message>
        <source>Next Month</source>
        <translation>ཟླ་བ་རྗེས་མ།</translation>
    </message>
    <message>
        <source>Last Month</source>
        <translation>ཟླ་སྔོན་མ།</translation>
    </message>
    <message>
        <source>Date cannot be later than %1</source>
        <translation>དུས་ཚོད་ནི་%1ལས་ཕྱི་མི་རུང་།</translation>
    </message>
    <message>
        <source>Last Week</source>
        <translation>བདུན་ཕྲག་སྔོན་མ།</translation>
    </message>
    <message>
        <source>Last Year</source>
        <translation>ན་ནིང་།</translation>
    </message>
    <message>
        <source>Yesterday</source>
        <translation>ཁ་སང་།</translation>
    </message>
    <message>
        <source>Date cannot be earlier than %1</source>
        <translation>དུས་ཚོད་ནི་%1ལས་སྔ་མི་རུང་།</translation>
    </message>
    <message>
        <source>Next Week</source>
        <translation>བདུན་ཕྲག་རྗེས་མ།</translation>
    </message>
    <message>
        <source>Next Year</source>
        <translation>སང་ལོར།</translation>
    </message>
</context>
<context>
    <name>KLed</name>
    <message>
        <source>LED off</source>
        <translation>LED OFF</translation>
    </message>
    <message>
        <source>LED on</source>
        <translation>LED ON</translation>
    </message>
</context>
<context>
    <name>KPixmapRegionSelectorDialog</name>
    <message>
        <source>Please click and drag on the image to select the region of interest:</source>
        <translation>ཁྱོད་ཀྱིས་པར་རིས་སྟེང་ནས་སྤྲོ་སྣང་ལྡན་པའི་ས་ཁོངས་བདམས་ཏེ།</translation>
    </message>
    <message>
        <source>Select Region of Image</source>
        <translation>པར་རིས་ཀྱི་ས་ཁོངས་བདམས་པ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Change the visibility of the password</source>
        <translation>གསང་གྲངས་ཀྱི་མཐོང་ཚད་ལ་འགྱུར་ལྡོག་གཏོང་དགོས།</translation>
    </message>
</context>
<context>
    <name>KPixmapRegionSelectorWidget</name>
    <message>
        <source>Rotate &amp;Counterclockwise</source>
        <translation>འཁོར་སྐྱོད་དང་ལྡོག་ཕྱོགས་སུ་འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Image Operations</source>
        <translation>པར་རིས་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <source>&amp;Rotate Clockwise</source>
        <translation>དུས་ཚོད་ལྟར་འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>KMessageWidget</name>
    <message>
        <source>&amp;Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Close message</source>
        <translation>ཆ་འཕྲིན་ཉེ་བར་བཅར་བ</translation>
    </message>
</context>
<context>
    <name>KPasswordDialog</name>
    <message>
        <source>Supply a password below.</source>
        <translation>གཤམ་གྱི་གསང་གྲངས་ཤིག་མཁོ་སྤྲོད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Use this password:</source>
        <translation>གསང་གྲངས་འདི་བེད་སྤྱོད་བྱ་རྒྱུ་སྟེ།</translation>
    </message>
    <message>
        <source>No password, use anonymous (or &amp;guest) login</source>
        <translation>གསང་གྲངས་མེད་ན་མིང་མ་བཀོད་པའི་(ཡང་ན་མགྲོན་པོ་)ཐོ་འགོད་བྱེད་ཐབས་སྤྱོད་དགོས།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>གསང་གྲངས་ནི།</translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation>ཁྱབ་ཁོངས་ནི།</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation>གསང་གྲངས་ཡིད་ལ་འཛིན་དགོས</translation>
    </message>
    <message>
        <source>Username:</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་ནི།</translation>
    </message>
    <message>
        <source>Supply a username and password below.</source>
        <translation>གཤམ་གྱི་སྤྱོད་མཁན་གྱི་མིང་དང་གསང་གྲངས་མཁོ་འདོན་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KDateTimeEdit</name>
    <message>
        <source>Floating</source>
        <translation>ཆུ་ངོས་སུ་གཡེང་བ།</translation>
    </message>
    <message>
        <source>The entered date and time is after the maximum allowed date and time.</source>
        <translation>ནང་འཇུག་བྱས་པའི་དུས་ཚོད་དང་དུས་ཚོད་ནི་ཆོག་མཆན་ཐོབ་པའི་དུས་ཚོད་དང་དུས་ཚོད་རིང་ཤོས་ཀྱི་རྗེས་སུ་ཡིན།</translation>
    </message>
    <message>
        <source>The entered date and time is before the minimum allowed date and time.</source>
        <translation>ནང་འཇུག་བྱས་པའི་དུས་ཚོད་དང་དུས་ཚོད་ནི་ཆེས་དམའ་བའི་ཆོག་མཆན་ཐོབ་པའི་དུས་ཚོད་དང་དུས་ཚོད་ཀྱི་སྔོན་དུ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>KMimeTypeChooser</name>
    <message>
        <source>Mime Type</source>
        <translation>ལད་མོ་བྱེད་པའི་རིགས་</translation>
    </message>
    <message>
        <source>Patterns</source>
        <translation>དཔེ་དབྱིབས་</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>དཔྱད་གཏམ་བརྗོད་པ།</translation>
    </message>
    <message>
        <source>Click this button to display the familiar KDE mime type editor.</source>
        <translation>མཐེབ་གཅུས་འདི་མནན་ནས་རྒྱུས་མངའ་ཡོད་པའི་KDE ལད་མོ་བྱེད་པའི་རིགས་དབྱིབས་ཀྱི་རྩོམ་སྒྲིག་པ་མངོན་པར</translation>
    </message>
    <message>
        <source>&amp;Edit...</source>
        <translation>རྩོམ་སྒྲིག...</translation>
    </message>
</context>
<context>
    <name>KToggleFullScreenAction</name>
    <message>
        <source>Display the window in full screen</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་བོའི་ནང་དུ་སྒེའུ་ཁུང་མངོན་པ།</translation>
    </message>
    <message>
        <source>Exit Full Screen</source>
        <translation>ཕྱིར་འབུད་བྱས་པའི་བརྙན་ཤེལ་</translation>
    </message>
    <message>
        <source>Exit F&amp;ull Screen Mode</source>
        <translation>ཕྱིར་འབུད་F&amp;ullབརྟག་ངོས་ཀྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Full Screen</source>
        <translation>བརྙན་ཤེལ་ཆ་ཚང་།</translation>
    </message>
    <message>
        <source>F&amp;ull Screen Mode</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་པོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Exit full screen mode</source>
        <translation>ཕྱིར་འབུད་བྱས་པའི་བརྙན་ཤེལ་གྱི་རྣམ་པ</translation>
    </message>
</context>
<context>
    <name>KNewPasswordDialog</name>
    <message>
        <source>Passwords do not match.</source>
        <translation>གསང་གྲངས་དང་མི་མཐུན་པ་</translation>
    </message>
    <message>
        <source>Password is empty.</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་རེད།</translation>
    </message>
    <message>
        <source>The password you have entered has a low strength. To improve the strength of the password, try:
 - using a longer password;
 - using a mixture of upper- and lower-case letters;
 - using numbers or symbols as well as letters.

Would you like to use this password anyway?</source>
        <translation>ཁྱེད་ཀྱིས་ནང་འཇུག་བྱས་པའི་གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཧ་ཅང་ཞན། གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆེ་རུ་གཏོང་ཆེད་ཚོད་ལྟ་ཞིག་བྱ་རྒྱུ་སྟེ།
 - དུས་ཡུན་ཅུང་རིང་བའི་གསང་གྲངས་བེད་སྤྱོད་བྱ་རྒྱུ།
 -གོང་འོག་གི་ཡི་གེ་མཉམ་བསྲེས་བྱས་པ།
 -གྲངས་ཀ་དང་མཚོན་རྟགས་དང་དེ་བཞིན་ཡི་གེ་བེད་སྤྱོད་བྱ་དགོས།

གང་ལྟར་ཁྱོད་ཀྱིས་གསང་གྲངས་འདི་བཀོལ་ན་ཆོག་གམ།</translation>
    </message>
    <message numerus="yes">
        <source>Password must be at least %n character(s) long.</source>
        <translation>
            <numerusform>གསང་གྲངས་ནི་ངེས་པར་དུ་མ་མཐར་ཡང་%nཡི་ཡི་གེ་རིང་པོ་ཡིན་དགོས།</numerusform>
        </translation>
    </message>
    <message>
        <source>Passwords match.</source>
        <translation>གསང་གྲངས་ཆ་འགྲིག་རེད།</translation>
    </message>
    <message>
        <source>Low Password Strength</source>
        <translation>གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་དམའ་བ།</translation>
    </message>
</context>
<context>
    <name>KSqueezedTextLabel</name>
    <message>
        <source>&amp;Copy Full Text</source>
        <translation>&amp;འདྲ་བཤུས་ཆ་ཚང་།</translation>
    </message>
</context>
<context>
    <name>KTimeComboBox</name>
    <message>
        <source>Time cannot be later than %1</source>
        <translation>དུས་ཚོད་ནི་%1ལས་ཕྱི་མི་རུང་།</translation>
    </message>
    <message>
        <source>Time cannot be earlier than %1</source>
        <translation>དུས་ཚོད་ནི་%1ལས་སྔ་མི་རུང་།</translation>
    </message>
    <message>
        <source>The time you entered is invalid</source>
        <translation>ཁྱེད་ཚོ་ནང་དུ་འཛུལ་བའི་དུས་ཚོད་ནི་གོ་མི་ཆོད་པ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>KNewPasswordWidget</name>
    <message>
        <source>Password:</source>
        <translation>གསང་གྲངས་ནི།</translation>
    </message>
    <message>
        <source>Password strength &amp;meter:</source>
        <translation>གསང་བའི་སྟོབས་ཤུགས་དང་ཚད་འཇལ་ཆས།</translation>
    </message>
    <message>
        <source>The password strength meter gives an indication of the security of the password you have entered. To improve the strength of the password, try:&lt;ul&gt;&lt;li&gt;using a longer password;&lt;/li&gt;&lt;li&gt;using a mixture of upper- and lower-case letters;&lt;/li&gt;&lt;li&gt;using numbers or symbols, such as #, as well as letters.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>གསང་གྲངས་ཀྱི་ཤུགས་ཚད་འཇལ་ཆས་ཀྱིས་ཁྱེད་ཀྱིས་ནང་འཇུག་བྱས་པའི་གསང་གྲངས་ཀྱི་བདེ་འཇགས་རང་བཞིན་གསལ་བཤད་བྱས་ཡོད། གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆེ་རུ་གཏོང་ཆེད།&lt;ul&gt;&lt;li&gt;&lt;li&gt;དུས་ཡུན་ཅུང་རིང་བའི་གསང་གྲངས་བཀོལ་སྤྱོད་བྱེད་པ། &lt;/li&gt; &lt;li&gt;གོང་འོག་གི་ཡི་གེ་མཉམ་བསྲེས་བྱས་ནས་བཀོལ་སྤྱོད་བྱས་པ། &lt;/li&gt; &lt;li&gt;ཨང་ཀི་དང་མཚོན་རྟགས་བེད་སྤྱོད་བྱས་པ་དཔེར་ན་#དང་ཡི་གེ་སོགས་ཡིན། &lt;/li&gt; &lt;/ul&gt;</translation>
    </message>
    <message>
        <source>&amp;Verify:</source>
        <translation>&gt; ཞིབ་བཤེར་བྱ་རྒྱུ་སྟེ།</translation>
    </message>
</context>
<context>
    <name>KColorCombo</name>
    <message>
        <source>Custom...</source>
        <translation>གོམས་སྲོལ་ ...</translation>
    </message>
</context>
<context>
    <name>KCharSelectItemModel</name>
    <message>
        <source>In decimal</source>
        <translation>བཅུ་ཆ་གཅིག་གི་ནང་དུ།</translation>
    </message>
    <message>
        <source>Unicode code point:</source>
        <translation>Unicode code point:</translation>
    </message>
</context>
</TS>
