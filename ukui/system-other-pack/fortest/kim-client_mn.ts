<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AuthWidget</name>
    <message>
        <source>Step 1</source>
        <translation>ᠠᠯᠬᠤᠮ ᠳᠠᠷᠠᠭ᠎ᠠ 1᠃</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>ᠠᠯᠬᠤᠮ ᠳᠠᠷᠠᠭ᠎ᠠ 2 ᠃</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ ᠡᠷᠦᠬᠡ ᠃</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠃</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠠᠯᠬᠤᠮ ᠳᠤ ᠃</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠯᠬᠤᠮ ᠃</translation>
    </message>
    <message>
        <source>Authention</source>
        <translation type="vanished">账号认证</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ᠃</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>ConfigWidget</name>
    <message>
        <source>Step 1</source>
        <translation>ᠠᠯᠬᠤᠮ ᠳᠠᠷᠠᠭ᠎ᠠ 1᠃</translation>
    </message>
    <message>
        <source>Step 2</source>
        <translation>ᠠᠯᠬᠤᠮ ᠳᠠᠷᠠᠭ᠎ᠠ 2 ᠃</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation type="vanished">基础配置</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠤ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Server</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠠᠯᠬᠤᠮ ᠳᠤ ᠃</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠯᠬᠤᠮ ᠃</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <source>Hostname can not be empty!</source>
        <translation>ᠵᠣᠴᠢᠨ ᠡᠷᠦᠬᠡ ᠶᠢᠨ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠡᠵᠡᠨ ᠮᠠᠰᠢᠨ ᠤ ᠨᠡᠷ᠎ᠡ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠶᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Please input server address!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠦ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠬᠠᠶᠢᠭ ᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠢᠷᠡᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
    <message>
        <source>Server not found!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠦᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Please enter a valid server hostname or IP address!</source>
        <translation type="vanished">请输入有效的服务器主机名！</translation>
    </message>
    <message>
        <source>Searching for server...</source>
        <translation>ᠶᠠᠭ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠢ ᠡᠷᠢᠵᠦ ᠁</translation>
    </message>
    <message>
        <source>The client and server time is inconsistent, please correct the time first!</source>
        <translation>ᠵᠣᠴᠢᠨ ᠡᠷᠦᠬᠡ ᠪᠣᠯᠣᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠦ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠴᠠᠭ ᠨᠢ ᠠᠳᠠᠯᠢ ᠦᠭᠡᠢ ᠂ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠲᠠᠭᠠᠷᠠᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <source>Get infomation from server failed!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠦᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠭᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Config</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ᠃</translation>
    </message>
    <message>
        <source>Auth</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Ip address is not allowed!</source>
        <translation>IP ᠬᠠᠶᠢᠭ ᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠵᠦ ᠬᠡᠪᠴᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠬᠦ ᠶᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Server not found, please make sure your input is correct and check your dns</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠡᠷᠢᠵᠦ ᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠂ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠭᠠᠵᠠᠷ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠵᠥᠪ ᠮᠥᠷᠲᠡᠭᠡᠨ ᠦᠵᠦᠭᠦᠷ ᠲᠦ ᠨᠢᠭᠡᠨᠲᠡ DNS ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠶᠢ ᠦᠨᠡᠨᠬᠦ ᠪᠠᠲᠤᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Host name is the same as server!</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠤ ᠨᠡᠷ᠎ᠡ ᠨᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠦᠨ ᠬᠠᠶᠢᠭ ᠲᠠᠢ ᠠᠳᠠᠯᠢ ᠦᠭᠡᠢ !</translation>
    </message>
</context>
<context>
    <name>ConfirmWidget</name>
    <message>
        <source>Finished</source>
        <translation>ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <source>Server IP</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ IP ᠃</translation>
    </message>
    <message>
        <source>Server Name</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠤ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠠᠯᠬᠤᠮ ᠳᠤ ᠃</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠨ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠃</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>ᠬᠦᠷᠢᠶᠡᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠂ ᠪᠢᠲᠡᠭᠡᠢ ᠬᠠᠭᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <source>Enroll failed:%1</source>
        <translation>ᠬᠡᠪᠴᠢᠶᠡᠨ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠯ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <source>Enroll failed, please check host.</source>
        <translation>ᠬᠡᠪᠴᠢᠶᠡᠨ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠪᠠᠯ ᠂ ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠢ᠌ᠴᠠᠭᠠᠭᠠᠷᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Enroll failed, please check server.</source>
        <translation>ᠬᠡᠪᠴᠢᠶᠡᠨ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠯ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠪᠠᠯ ᠂ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠬᠡᠪ ᠦᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠤᠨ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ ᠃</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>FinishedWidget</name>
    <message>
        <source>Reboot</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Enroll to domain succeed</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠨ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠳᠦ ᠠᠮᠵᠢᠯᠲᠠ ᠲᠠᠶ ᠣᠷᠣᠯᠴᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation>ᠤᠬᠤᠷᠢᠨ ᠭᠠᠷᠴᠤ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠡᠩᠬᠡᠷᠡᠭᠦᠯᠦᠯ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠪᠣᠯᠬᠣᠷ ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠠᠶᠢᠬᠤ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ᠃</translation>
    </message>
</context>
<context>
    <name>InstalledWidget</name>
    <message>
        <source>Server IP</source>
        <translation type="vanished">服务器地址</translation>
    </message>
    <message>
        <source>Server name</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠃</translation>
    </message>
    <message>
        <source>Hostname</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠤ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Unenroll</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠵᠤ ᠬᠠᠮᠢᠶᠠᠷᠤᠨ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠡᠴᠡ ᠭᠠᠷᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <source>Dbus called failed:%1</source>
        <translation>Dbus ᠵᠠᠯᠭᠠᠯᠲᠠ ᠶᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠳᠦ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <source>Unenrolled:%1</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠨ ᠭᠠᠷᠭᠠᠬᠤ ᠶᠢ ᠬᠠᠮᠢᠶᠠᠷᠤᠨ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <source>Unenrolled failed: %1</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠨ ᠭᠠᠷᠭᠠᠵᠤ ᠬᠠᠮᠢᠶᠠᠷᠤᠨ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <source>Are you sure to unenroll?</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠨ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠡᠴᠡ ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ ?</translation>
    </message>
    <message>
        <source>Unenrolling, please not close this application</source>
        <translation>ᠶᠠᠭ ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠬᠤ ᠭᠠᠵᠠᠷ ᠠᠴᠠ ᠬᠠᠮᠢᠶᠠᠷᠴᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠂ ᠪᠢᠲᠡᠭᠡᠢ ᠬᠠᠭᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Configuration</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ᠃</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>LoadingWidget</name>
    <message>
        <source>loading...</source>
        <translation>ᠠᠮᠢ ᠲᠡᠮᠡᠴᠡᠨ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ ᠁</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kim Client</source>
        <translation>ᠬᠦᠷᠢᠶᠡᠨ ᠳᠦ ᠵᠣᠴᠢᠨ ᠡᠷᠦᠬᠡ ᠶᠢᠨ ᠦᠵᠦᠭᠦᠷ ᠢ ᠨᠡᠮᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Caution</source>
        <translation>ᠠᠩᠬᠠᠷ ᠃</translation>
    </message>
    <message>
        <source>In progress now, please do not close the app!</source>
        <translation>ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠢᠨ᠎ᠠ᠂ ᠪᠢᠲᠡᠭᠡᠢ ᠬᠠᠭᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠪᠠᠭ᠎ᠠ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PwdChangeWidget</name>
    <message>
        <source>Password expired, change password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ ᠬᠡᠲᠦᠷᠡᠵᠦ ᠂ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠲᠤ ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <source>User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠃</translation>
    </message>
    <message>
        <source>Old password</source>
        <translation>ᠤᠤᠯ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠃</translation>
    </message>
    <message>
        <source>New password</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠃</translation>
    </message>
    <message>
        <source>Reenter password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation type="obsolete">上一步</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="obsolete">加入管控</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠢᠷᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Password change success.</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠵᠠᠰᠠᠭᠠᠳ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Insufficient permissions</source>
        <translation>ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠭᠡᠶ ᠪᠠᠶᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>The current user does not have permissions, contact your administrator.</source>
        <translation>ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠦᠭᠡᠶ ᠪᠣᠯ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠲᠠᠶ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ᠃</translation>
    </message>
    <message>
        <source>Server connection failed, please check the configuration!</source>
        <translation type="vanished">服务端连接失败，请检查参数配置！</translation>
    </message>
    <message>
        <source>User authentication failed, please check the account and password!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠢᠯᠠᠭᠳᠠᠯ ᠢ ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡᠷᠡᠢ ᠂ ᠳᠠᠩᠰᠠᠨ ᠤ ᠨᠣᠮᠧᠷ ᠪᠣᠯᠣᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠪᠠᠢ᠌ᠴᠠᠭᠠᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <source>Query for user failed, please check the configuration!</source>
        <translation type="vanished">用户信息查询失败，请检查连接配置！</translation>
    </message>
    <message>
        <source>User password expires, please change the password and try again!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠤᠨ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ ᠬᠡᠲᠦᠷᠡᠵᠡᠢ ᠂ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠵᠠᠰᠠᠭᠠᠳ ᠳᠠᠬᠢᠨ ᠲᠤᠷᠰᠢᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <source>Search for configure failed, please check the server!</source>
        <translation type="vanished">系统信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>User rights are insufficient, please contact the administrator!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠭᠡᠢ᠂ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Hostname[%1] has already registered, please change the name or delete the record!</source>
        <translation>ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠤ ᠨᠡᠷ᠎ᠡ 〔 1〕 ᠨᠢᠭᠡᠨᠲᠡ ᠬᠦᠷᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠵᠡᠢ ᠂ ᠭᠣᠣᠯ ᠮᠠᠰᠢᠨ ᠤ ᠨᠡᠷ᠎ᠡ ᠪᠡᠨ ᠰᠣᠯᠢᠬᠤ ᠪᠤᠶᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠦ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠯ ᠢ ᠬᠠᠰᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <source>Query for replica server failed, please check the server configuration!</source>
        <translation type="vanished">副本服务器信息查询失败，请检查服务器配置！</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <source>Invalid format!</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠂ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠳ᠋ᠠᠶᠢᠲ᠋ᠠ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ !</translation>
    </message>
    <message>
        <source>Request error: %1</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <source>Request error: counts error!</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠄ ᠪᠤᠴᠠᠵᠤ ᠢᠷᠡᠬᠦ ᠲᠣᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠶᠢ ᠲᠣᠬᠢᠷᠠᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Please input username!</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠢᠷᠡᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <source>Please input password!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠢᠷᠡᠭᠡᠷᠡᠢ !</translation>
    </message>
    <message>
        <source>Server request error:%1</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠦᠷ ᠦᠨ ᠪᠤᠷᠤᠭᠤ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠄ 1 ᠃</translation>
    </message>
    <message>
        <source>Authentication is abnormal, check the service status!</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠨᠢ ᠬᠡᠪ ᠦᠨ ᠪᠤᠰᠤ ᠂ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠢ ᠪᠠᠢ᠌ᠴᠠᠭᠠᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <source>authenting, please wait...</source>
        <translation>ᠭᠡᠷᠡᠴᠢᠯᠡᠭᠡ ᠬᠢᠬᠦ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ ᠳᠤᠮᠳᠠ ᠵᠢᠭᠠᠬᠠᠨ ᠤ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠁</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>Please input new password!</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠭᠠᠷᠭᠠᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <source>Confirmed password and new password do not match!</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠪᠣᠯᠣᠨ ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠨᠢ ᠠᠳᠠᠯᠢ ᠦᠭᠡᠢ ᠭᠡᠵᠦ ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ !</translation>
    </message>
</context>
<context>
    <name>RecoveryWidget</name>
    <message>
        <source>Recover</source>
        <translation>ᠰᠡᠷᠭᠦᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source>exit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>The last operation did not complete successfully, please recover!</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ ᠤᠳᠠᠭᠠᠨ ᠤ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠠᠵᠢᠯ ᠠᠮᠵᠢᠯᠲᠠ ᠲᠠᠢ ᠳᠠᠭᠤᠰᠤᠭᠠᠳᠤᠢ ᠂ ᠰᠡᠷᠭᠦᠭᠡᠷᠡᠢ !</translation>
    </message>
</context>
<context>
    <name>UnenrollAuthWidget</name>
    <message>
        <source>Please input username and password which has authority to unjoin the domain, and the click the continue button.</source>
        <translation>ᠲᠠ ᠶᠠᠭ ᠪᠤᠴᠠᠭᠠᠬᠤ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠡᠴᠡ ᠭᠠᠷᠬᠤ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠢ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠵᠦ ᠪᠠᠶᠢᠨ᠎ᠠ᠂ ᠪᠤᠴᠠᠭᠠᠬᠤ ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠢ ᠪᠦᠷᠢᠳᠦᠭᠦᠯᠦᠭᠰᠡᠨ ᠳᠠᠩᠰᠠᠨ ᠤ ᠨᠣᠮᠧᠷ ᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ᠂ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠦᠭᠡᠷᠡᠢ᠃</translation>
    </message>
    <message>
        <source>Input username</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠤ ᠨᠣᠮᠧᠷ ᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠢᠷᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Input password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠢᠷᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Domain:</source>
        <translation>ᠣᠷᠣᠨ ᠪᠦᠰᠡ ᠄</translation>
    </message>
</context>
<context>
    <name>UnenrollFinishedWidget</name>
    <message>
        <source>Unenroll from domain succeed</source>
        <translation type="obsolete">退出成功</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠡᠩᠭᠡᠷᠡᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Prompt</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <source>Reboot failed, please check if there are others user login.</source>
        <translation>ᠳᠠᠬᠢᠨ ᠰᠡᠩᠬᠡᠷᠡᠭᠦᠯᠦᠯ ᠢᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠪᠣᠯᠬᠣᠷ ᠣᠳᠣᠬᠠᠨ ᠳᠤ ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠠᠶᠢᠬᠤ ᠡᠰᠡᠬᠦ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠬᠤ ᠪᠣᠯᠪᠠᠤ᠃</translation>
    </message>
    <message>
        <source>You have already unrolled from %1 domain</source>
        <translation>ᠲᠠ ᠨᠢᠭᠡᠨᠲᠡ 1 ᠪᠦᠰᠡ ᠡᠴᠡ ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠴᠠᠢ᠃</translation>
    </message>
    <message>
        <source>Unenroll from domain succeed, will restart now.</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠬᠤ ᠭᠠᠵᠠᠷ ᠲᠤ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠵᠤ ᠂ ᠤᠳᠠᠯ ᠦᠭᠡᠢ ᠳᠠᠬᠢᠨ ᠡᠭᠢᠯᠡᠬᠦ ᠭᠡᠵᠦ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>UnenrollInfoWidget</name>
    <message>
        <source>Unenrolling, please do not close this application</source>
        <translation>ᠶᠠᠭ ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠬᠤ ᠭᠠᠵᠠᠷ ᠠᠴᠠ ᠬᠠᠮᠢᠶᠠᠷᠴᠤ ᠪᠠᠶᠢᠨ᠎ᠠ ᠂ ᠪᠢᠲᠡᠭᠡᠢ ᠬᠠᠭᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠷᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Before you exist, please make sure that the local user is enabled and you have the password of this user.</source>
        <translation>ᠣᠷᠣᠨ ᠠᠴᠠ ᠪᠠᠨ ᠡᠮᠦᠨ᠎ᠡ ᠂ ᠪᠠᠭ᠎ᠠ ᠠᠴᠠ ᠪᠠᠨ ᠬᠠᠪᠰᠤᠷᠤᠭᠴᠢ ᠲᠠᠨ ᠤ ᠦᠵᠦᠭᠦᠷ ᠦᠨ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠪᠠᠶᠢᠳᠠᠯ ᠳᠤ ᠪᠠᠶᠢᠬᠤ ᠶᠢ ᠨᠤᠲᠠᠯᠠᠬᠤ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠲᠠ ᠲᠤᠰ ᠭᠠᠵᠠᠷ ᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢᠳ ᠦᠨ ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠢ ᠮᠡᠳᠡᠪᠡᠯ ᠰᠠᠶ ᠢ ᠡᠨᠡ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠲᠦ ᠭᠠᠷᠴᠤ ᠳᠡᠶᠢᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Click the ok button to continue</source>
        <translation>ᠭᠠᠭᠴᠠ ᠴᠣᠬᠢᠯᠲᠠ ᠥᠭᠭᠦᠭᠰᠡᠨ 《ᠲᠣᠭᠲᠠᠭᠠᠬᠤ》 ᠶᠢ ᠦᠷᠭᠦᠯᠵᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
</context>
</TS>
