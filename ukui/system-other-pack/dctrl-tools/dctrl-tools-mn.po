msgid ""
msgstr ""
"Project-Id-Version: dctrl-tools\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"PO-Revision-Date: 2023-06-24 10:22+0000\n"
"Last-Translator: bolor2022 <759085099@qq.com>\n"
"Language-Team: Mongolian <http://weblate.openkylin.top/projects/test-for-mn/"
"dctrl-tools/mn/>\n"
"Language: mn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.1-dev\n"
"X-Launchpad-Export-Date: 2020-04-16 18:32+0000\n"

msgid "-I requires at least one instance of -s"
msgstr "— I ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠠᠳᠠᠭ ᠲᠤ᠌ ᠪᠡᠨ ᠨᠢᠭᠡ —s ᠪᠣᠳᠠᠲᠤ ᠵᠢᠱᠢᠶ᠎ᠡ ᠬᠡᠷᠡᠭᠰᠡᠨ᠎ᠡ"

msgid "A pattern is mandatory"
msgstr "ᠵᠠᠭᠪᠤᠷ ᠪᠣᠯ ᠵᠠᠪᠠᠯ ᠬᠡᠷᠡᠭᠰᠡᠬᠦ ᠶᠤᠮ"

msgid "Append the specified column."
msgstr "ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠵᠢ ᠨᠡᠮᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ."

msgid "Attempt mmapping input files"
msgstr "ᠺᠢᠨᠣ᠋ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠲᠤᠷᠰᠢᠨ᠎ᠠ"

msgid "DELIM"
msgstr "ᠳ᠋ᠧᠯᠢᠮ"

msgid "Debug option parsing."
msgstr "ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠰᠣᠩᠭᠣᠬᠤ ᠵᠦᠢᠯ ᠤ᠋ᠨ ᠵᠠᠳᠠᠯᠤᠯᠲᠠ."

msgid "Do an exact match."
msgstr "ᠭᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠪᠦᠷᠢᠮᠦᠰᠦᠨ ᠵᠣᠬᠢᠴᠠᠭᠤᠯᠤᠨ᠎ᠠ."

msgid "Do not print a table heading"
msgstr "ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠵᠢᠨ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠦᠭᠡᠢ"

msgid "FIELD"
msgstr "ᠲᠠᠷᠢᠶᠠᠨ ᠭᠠᠵᠠᠷ"

msgid "FIELD,FIELD,..."
msgstr "ᠬᠦᠷᠢᠶ᠎ᠡ ᠬᠡᠪᠴᠢᠶ᠎ᠡ ᠂ ᠬᠦᠷᠢᠶ᠎ᠡ ,..."

msgid "Ignore case when looking for a match."
msgstr "ᠲᠣᠬᠢᠷᠠᠬᠤ ᠵᠦᠢᠯ ᠢ᠋ ᠡᠷᠢᠬᠦ ᠦᠶᠡᠰ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠪᠢᠴᠢᠬᠦ ᠵᠢ ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠪᠠ."

msgid "Ignore parse errors"
msgstr "ᠪᠤᠷᠤᠭᠤ ᠶᠢ ᠤᠮᠳᠤᠭᠠᠢ᠌ᠯᠠ ᠃"

msgid "LEVEL"
msgstr "ᠬᠢᠷᠢ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠃"

msgid "Match only whole package names (this implies -e)"
msgstr "ᠵᠥᠪᠬᠡᠨ ᠪᠦᠬᠦᠶᠢᠯᠡ ᠪᠣᠭᠴᠣ ᠶᠢᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠢ ᠲᠠᠭᠠᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ( ᠡᠨᠡ ᠨᠢ — e )"

msgid "PATTERN"
msgstr "ᠵᠠᠭᠪᠤᠷ ᠃"

msgid "Parse error in field."
msgstr "ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠵᠠᠳᠠᠯᠤᠯᠲᠠ ᠪᠤᠷᠤᠭᠤ."

msgid "Print out the copyright license."
msgstr "ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠵᠢᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠯ ᠢ᠋ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠨ᠎ᠠ."

msgid "Print unpairable records from the indicated file (either 1 or 2)"
msgstr ""
"ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ (1 ᠪᠤᠶᠤ 2) ᠠᠴᠠ ᠲᠣᠬᠢᠷᠠᠯᠴᠠᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠰᠢ ᠦᠭᠡᠶ "
"ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ ᠳᠠᠷᠤᠮᠠᠯᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃"

msgid "Regard the pattern as an extended POSIX regular expression."
msgstr "ᠲᠤᠰ ᠵᠠᠭᠪᠤᠷ ᠢ᠋ ᠥᠷᠭᠡᠳᠬᠡᠭᠰᠡᠨ ᠫᠥᠰ IX ᠵᠢᠩᠬᠢᠨᠢ ᠢᠯᠡᠳᠬᠡᠬᠦ ᠬᠡᠯᠪᠡᠷᠢ ᠪᠡᠷ ᠦᠵᠡᠳᠡᠭ."

msgid "Restrict pattern matching to the FIELDs given."
msgstr "ᠵᠠᠭᠪᠤᠷ ᠢ᠋ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠢ᠋ ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠪᠣᠯᠭᠠᠨ᠎ᠠ."

msgid "Set debugging level to LEVEL."
msgstr "ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠳᠡᠰ ᠢ᠋ ᠨᠢ LEVEL ᠪᠣᠯᠭᠠᠨ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ."

msgid "Show only paragraphs that do not match."
msgstr "ᠵᠥᠪᠬᠡᠨ ᠲᠣᠬᠢᠷᠠᠬᠤ ᠦᠭᠡᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠪᠠᠢ᠌ᠳᠠᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ."

msgid "Show only the body of these fields from the matching paragraphs."
msgstr "ᠵᠥᠪᠬᠡᠨ ᠬᠡᠰᠡᠭ ᠲᠤ᠌ ᠡᠳᠡᠭᠡᠷ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠭᠣᠣᠯ ᠵᠣᠬᠢᠶᠠᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ."

msgid "Show only the count of matching paragraphs."
msgstr "ᠵᠥᠪᠬᠡᠨ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠲᠣᠭ᠎ᠠ ᠵᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠡᠢ."

msgid "Show those fields that have NOT been selected with -s"
msgstr "ᠲᠡᠳᠡᠭᠡᠷ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠳᠦᠢ —s ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠢ ᠬᠠᠷᠠᠭᠤᠯᠤᠭᠠᠳᠤᠢ ᠃"

msgid "Specify a delimiter."
msgstr "ᠮᠢᠨᠦ᠋ᠲ ᠤ᠋ᠨ ᠲᠡᠮᠲᠡᠭ ᠢ᠋ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ."

msgid "Specify sort keys."
msgstr "ᠵᠢᠭᠰᠠᠭᠠᠬᠤ ᠳᠡᠰ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠢ᠋ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ."

msgid "Specify the common join field"
msgstr "ᠣᠯᠠᠨ ᠨᠡᠶᠢᠲᠡ ᠶᠢᠨ ᠬᠣᠯᠪᠣᠯᠲᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠦᠨ ᠬᠡᠰᠡᠭ ᠢ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃"

msgid "Specify the format of the output file"
msgstr "ᠭᠠᠳᠠᠭᠰᠢ ᠭᠠᠷᠭᠠᠬᠤ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠶᠢ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃"

msgid "Specify the join field to use for the first file"
msgstr ""
"ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠣᠯᠪᠣᠭᠠᠯᠠᠨ ᠵᠣᠯᠭᠠᠭᠤᠯᠬᠤ ᠦᠰᠦᠭ ᠦᠨ ᠬᠡᠰᠡᠭ ᠢ ᠵᠢᠭᠠᠨ "
"ᠲᠣᠭᠲᠠᠭᠠᠵᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃"

msgid "Specify the join field to use for the second file"
msgstr ""
"ᠬᠣᠶᠠᠳᠤᠭᠠᠷ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠣᠯᠪᠣᠭᠠᠯᠠᠨ ᠵᠣᠯᠭᠠᠭᠤᠯᠬᠤ ᠦᠰᠦᠭ ᠦᠨ ᠬᠡᠰᠡᠭ᠍ ᠢ ᠵᠢᠭᠠᠨ "
"ᠲᠣᠭᠲᠠᠭᠠᠵᠤ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠬᠡᠷᠡᠭ᠍ᠲᠡᠢ ᠃"

msgid "Specify the pattern to search for"
msgstr "ᠡᠷᠢᠬᠦ ᠵᠠᠭᠪᠤᠷ ᠢ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠃"

msgid "Suppress field names when showing specified fields."
msgstr ""
"ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠬᠡᠰᠡᠭ ᠤ᠋"
"ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠵᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ."

msgid "Test for version number equality."
msgstr "ᠬᠡᠮᠵᠢᠨ ᠲᠤᠷᠰᠢᠭᠰᠠᠨ ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠨᠣᠮᠧᠷ ᠨᠢ ᠠᠳᠠᠯᠢ ᠡᠰᠡᠬᠦ ᠵᠢ ᠬᠡᠮᠵᠢᠨ᠎ᠡ."

msgid "This is a shorthand for -FPackage."
msgstr "ᠡᠨᠡ ᠪᠣᠯ FPackage ᠤ᠋ᠨ/ ᠵᠢᠨ ᠲᠣᠪᠴᠢ ᠪᠢᠴᠢᠯᠲᠡ ᠪᠣᠯᠣᠨ᠎ᠠ."

msgid "This is a shorthand for -FSource:Package."
msgstr "ᠡᠨᠡ ᠪᠣᠯ ᠹᠰᠺᠣᠷᠺᠠ᠄ ᠫᠠᠺᠢᠰ ᠤ᠋ᠨ ᠲᠣᠪᠴᠢ ᠪᠢᠴᠢᠯᠲᠡ ᠪᠣᠯᠣᠨ᠎ᠠ."

msgid "Version number comparison: <=."
msgstr "ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠺᠤᠳ᠋ ᠨᠢ ᠬᠠᠷᠢᠴᠠᠭᠤᠯᠪᠠᠯ: <=."

msgid "Version number comparison: >=."
msgstr "ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠺᠤᠳ᠋ ᠨᠢ ᠬᠠᠷᠢᠴᠠᠭᠤᠯᠪᠠᠯ: >=."

msgid "bad multibyte character"
msgstr "ᠪᠤᠷᠤᠭᠤ ᠣᠯᠠᠨ ᠦᠰᠦᠭ ᠦᠨ ᠦᠰᠦᠭ ᠦᠨ ᠲᠡᠮᠲᠡᠭ ᠃"

msgid "cannot join a stream with itself"
msgstr "ᠥᠪᠡᠷ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠲᠡᠢ ᠪᠡᠨ ᠬᠠᠮᠲᠤ ᠤᠷᠤᠰᠭᠠᠯ ᠳᠤ ᠣᠷᠣᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠃"

msgid "cannot suppress field names when showing whole paragraphs"
msgstr ""
"ᠪᠦᠬᠦ ᠬᠡᠰᠡᠭ ᠨᠢᠭᠤᠭᠳᠠᠬᠤ ᠦᠶ᠎ᠡ ᠳᠦ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠬᠡᠰᠡᠭ ᠦᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠵᠤ "
"ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃"

msgid "expected a colon"
msgstr "ᠵᠢᠱᠢᠭᠰᠡᠨ ᠬᠠᠭᠤᠷᠮᠠᠭ ᠨᠣᠮᠧᠷ ᠃"

msgid "expected either '1.' or '2.' at the start of the field specification"
msgstr ""
"ᠵᠢᠱᠢᠯᠲᠡ ᠪᠡᠷ ᠦᠰᠦᠭ ᠦᠨ ᠬᠡᠰᠡᠭ ᠦᠨ ᠵᠢᠷᠤᠮᠵᠢᠯ ᠡᠬᠢᠯᠡᠬᠦ ᠦᠶ᠎ᠡ ᠳᠦ 《 1.》 ᠪᠤᠶᠤ 《 2᠁ 》 "
"ᠭᠡᠵᠦ ᠵᠢᠱᠢᠵᠦ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃"

msgid "grep-dctrl -- grep Debian control files"
msgstr "Grep-dctrl -- grep Debian control files"

msgid "internal error"
msgstr "ᠳᠣᠲᠣᠭᠠᠳᠤ ᠪᠤᠷᠤᠭᠤ ᠃"

msgid "invalid column length"
msgstr "ᠵᠢᠭᠰᠠᠭᠠᠭᠰᠠᠨ ᠤᠷᠲᠤ ᠶᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠃"

msgid "invalid key flag"
msgstr "ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠮᠠᠩᠬᠠᠨ ᠲᠡᠮᠳᠡᠭ ᠃"

msgid "is a block device, skipping"
msgstr "ᠬᠡᠰᠡᠭ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠂ ᠦᠰᠦᠷᠦᠨ ᠭᠠᠷᠴᠤ ᠥᠩᠭᠡᠷᠡᠵᠡᠢ ᠃"

msgid "is a directory, skipping"
msgstr "ᠨᠢᠭᠡ ᠭᠠᠷᠴᠠᠭ ᠂ ᠦᠰᠦᠷᠦᠨ ᠭᠠᠷᠴᠠᠭ ᠲᠠᠢ ᠃"

msgid "is a socket, skipping"
msgstr "ᠨᠢᠭᠡ ᠢᠵᠢ ᠦᠰᠦᠭ ᠵᠠᠯᠭᠠᠵᠤ ᠦᠰᠦᠭ᠍ ᠵᠠᠯᠭᠠᠵᠤ ᠦᠰᠦᠭ᠍ ᠲᠡᠢ ᠂ ᠦᠰᠦᠷᠴᠦ ᠥᠩᠭᠡᠷᠡᠵᠡᠢ ᠃"

msgid "join-dctrl -- join two Debian control files"
msgstr "joinndctrl ᠬᠣᠶᠠᠷ Debiae ᠶᠢ ᠬᠣᠯᠪᠣᠨ᠎ᠠ ᠃"

msgid "malformed argument to '-a'"
msgstr "《a》 ᠶᠢᠨ ᠪᠤᠯᠵᠣᠯᠲᠤ ᠲᠣᠭ᠎ᠠ ᠶᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠨᠢ ᠵᠥᠪ ᠪᠤᠰᠤ ᠪᠠᠶᠢᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃"

msgid "missing ')' in command line"
msgstr "ᠵᠠᠷᠯᠢᠭ ᠪᠠᠩᠭᠢ ᠶᠢᠨ ᠳᠤᠮᠳᠠ 《 ) ᠳᠤᠲᠠᠭᠳᠠᠬᠤ ᠶᠢ ᠵᠠᠷᠯᠢᠭᠳᠠᠵᠠᠢ ᠃"

msgid "missing '.' in output field specification"
msgstr "ᠭᠠᠷᠭᠠᠬᠤ ᠦᠰᠦᠭ ᠦᠨ ᠬᠡᠰᠡᠭ ᠦᠨ ᠵᠢᠷᠤᠮᠵᠢᠯ ᠳᠤ 《ᠳᠤᠲᠠᠭᠳᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ》"

msgid "need exactly two input files"
msgstr "ᠶᠠᠭ ᠰᠠᠢ᠌ᠬᠠᠨ ᠬᠣᠶᠠᠷ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠡᠷᠡᠭ᠍ᠰᠡᠨ᠎ᠡ ᠃"

msgid "no such log level '%s'"
msgstr "ᠡᠢ᠌ᠮᠦ ᠡᠳᠦᠷ ᠦᠨ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠯ ᠦᠨ ᠳᠡᠰ 〈 s 〉 ᠪᠠᠢ᠌ᠬᠤ ᠦᠭᠡᠢ ᠃"

msgid "sort-dctrl -- sort Debian control files"
msgstr "sortdctrl Debiae ᠡᠵᠡᠮᠰᠢᠬᠦ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠃"

msgid "tbl-dctrl -- tabularize Debian control files"
msgstr "tbldctrl Debbian ᠶᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃"

msgid "the join field of the first file has already been specified"
msgstr ""
"ᠨᠢᠭᠡᠨᠲᠡ ᠨᠢᠭᠡᠳᠦᠭᠡᠷ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠣᠯᠪᠣᠯᠲᠠ ᠶᠢᠨ ᠦᠰᠦᠭ ᠢ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠵᠠᠢ ᠃"

msgid "the join field of the second file has already been specified"
msgstr ""
"ᠬᠣᠶᠠᠳᠤᠭᠠᠷ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠣᠯᠪᠣᠯᠳᠤᠭᠰᠠᠨ ᠦᠰᠦᠭ᠍ ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠵᠢᠭᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠵᠠᠢ "
"᠃"

msgid "too many file names"
msgstr "ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠬᠡᠲᠦᠷᠬᠡᠢ ᠠᠷ"

msgid "too many output fields"
msgstr "ᠭᠠᠷᠭᠠᠬᠤ ᠦᠰᠦᠭ ᠬᠡᠲᠦᠷᠬᠡᠢ ᠠᠷᠪᠢᠨ ᠃"

msgid "unknown file type, skipping"
msgstr "ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠨᠢ ᠦᠰᠦᠷᠴᠦ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃"

msgid "warning: expected a colon"
msgstr "ᠰᠡᠷᠡᠮᠵᠢᠭᠦᠯᠦᠯ ᠄ ᠵᠢᠱᠢᠯᠲᠡ ᠳᠦ ᠬᠠᠭᠤᠷᠮᠠᠭ ᠨᠣᠮᠧᠷ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃"
