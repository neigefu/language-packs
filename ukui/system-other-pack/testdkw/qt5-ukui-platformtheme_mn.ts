<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>KyFileDialog</name>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="146"/>
        <source>Go Back</source>
        <translation>ttttttttttttttttttttttttttttttttttttttttttttttt</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="153"/>
        <source>Go Forward</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="159"/>
        <source>Cd Up</source>
        <translation>ᠭᠡᠷᠡᠯ ᠫᠠᠨᠰᠠ ᠥᠭᠡᠳᠡᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="166"/>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="174"/>
        <source>View Type</source>
        <translation>ᠬᠠᠷᠠᠭ᠎ᠠ ᠵᠢᠷᠤᠭ ᠤᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="184"/>
        <source>Sort Type</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠬᠤ ᠲᠥᠷᠥᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="777"/>
        <source>Name</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="780"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="781"/>
        <location filename="../widget/kyfiledialog.cpp" line="790"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="785"/>
        <source>Save as</source>
        <translation>ᠠᠰᠲᠠᠭᠠᠨ ᠬᠠᠳᠠᠭᠠᠯᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="787"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠶᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠠᠪᠤᠳᠠᠷ ᠢ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠶᠢᠭᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="789"/>
        <source>Save</source>
        <translation>ᠠᠪᠤᠷᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="824"/>
        <location filename="../widget/kyfiledialog.cpp" line="826"/>
        <source>Directories</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1010"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭ᠍ᠬᠦ</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1010"/>
        <source>exist, are you sure replace?</source>
        <translation>ᠣᠷᠣᠰᠢᠵᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠂ ᠲᠠ ᠣᠷᠣᠯᠠᠬᠤ ᠪᠠᠷ ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ ᠤᠤ ?</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1013"/>
        <source>ok</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1022"/>
        <source>no</source>
        <translation>ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1452"/>
        <source>NewFolder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠬᠠᠪᠤᠳᠴᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1601"/>
        <source>Undo</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1608"/>
        <source>Redo</source>
        <translation>ᠳᠠᠬᠢᠨ ᠬᠢᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1805"/>
        <source>warn</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢ ᠥᠭ᠍ᠬᠦ</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1805"/>
        <source>This operation is not supported.</source>
        <translation>ᠡᠨᠡ ᠠᠵᠢᠯ ᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1924"/>
        <source>Open File</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1925"/>
        <source>Save File</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1938"/>
        <source>All Files (*)</source>
        <translation>ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ( * )</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>MainWindow</source>
        <translation>ᠭᠣᠣᠯ ᠴᠣᠩᠬᠣ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test open</source>
        <translation>ᠬᠡᠮᠵᠢᠨ ᠲᠤᠷᠰᠢᠵᠤ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>selected uri</source>
        <translation>ᠰᠣᠩᠭᠣᠨ ᠲᠣᠭᠲᠠᠭᠠᠭᠰᠠᠨ URI ᠃</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test show</source>
        <translation>ᠬᠡᠮᠵᠢᠨ ᠦᠵᠡᠭᠦᠯᠦᠯᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test exec</source>
        <translation>ᠬᠡᠮᠵᠢᠨ ᠲᠤᠷᠰᠢᠵᠤ ᠬᠡᠷᠡᠭᠵᠢᠭᠦᠯᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ᠃</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test save</source>
        <translation>ᠬᠡᠮᠵᠢᠨ ᠲᠤᠷᠰᠢᠵᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test static open</source>
        <translation>ᠬᠡᠮᠵᠢᠨ ᠲᠤᠷᠰᠢᠵᠤ ᠨᠠᠮ ᠵᠢᠮ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>PushButton</source>
        <translation>ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠳᠠᠷᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>use auto highlight icon</source>
        <translation>ᠠᠦᠢᠲ᠋ᠣ᠋ ᠲᠣᠪᠣᠶᠢᠮ᠎ᠠ ᠳᠡᠯᠭᠡᠴᠡ ᠶᠢᠨ ᠵᠢᠷᠤᠭ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>highlightOnly</source>
        <translation>ᠵᠥᠪᠬᠡᠨ ᠲᠣᠪᠣᠶᠢᠮ᠎ᠠ ᠢᠯᠡᠷᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>bothDefaultAndHighlight</source>
        <translation>ᠬᠣᠶᠠᠷ ᠤᠨ ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠯᠲᠠ ᠪᠣᠯᠣᠨ ᠲᠣᠪᠣᠶᠢᠮ᠎ᠠ ᠢᠯᠡᠷᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>RadioButton</source>
        <translation>ᠳᠠᠩ ᠰᠣᠩᠭᠣᠬᠤ ᠳᠠᠷᠤᠭᠤᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>style</source>
        <translation>ᠬᠡᠪ ᠨᠠᠮᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>icon</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤᠨ ᠲᠡᠮᠳᠡᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>menu opacity</source>
        <translation>ᠵᠠᠭᠤᠰᠢ ᠶᠢᠨ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠲᠤᠩᠭᠠᠯᠠᠭᠴᠠ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>font</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../widget/message-box.h" line="190"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>ᠭᠦᠢᠴᠡᠳᠬᠡᠵᠦ ᠪᠣᠯᠬᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ 〈 1】 Qt% 2 ᠬᠡᠷᠡᠭᠰᠡᠵᠦ ᠂ Qt% 3 ᠶᠢ ᠡᠷᠢᠵᠦ ᠣᠯᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/message-box.h" line="192"/>
        <source>Incompatible Qt Library Error</source>
        <translation>ᠪᠠᠭᠲᠠᠭᠠᠬᠤ ᠦᠭᠡᠢ Qt ᠬᠦᠮᠦᠷᠭᠡ ᠪᠤᠷᠤᠭᠤ ᠃</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../widget/message-box.cpp" line="405"/>
        <location filename="../widget/message-box.cpp" line="1075"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../widget/message-box.cpp" line="461"/>
        <location filename="../widget/message-box.cpp" line="866"/>
        <location filename="../widget/message-box.cpp" line="1376"/>
        <source>Show Details...</source>
        <translation>ᠨᠠᠷᠢᠨ ᠪᠠᠶᠢᠳᠠᠯ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠦ ᠁</translation>
    </message>
    <message>
        <location filename="../widget/message-box.cpp" line="866"/>
        <location filename="../widget/message-box.cpp" line="1376"/>
        <source>Hide Details...</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ ᠨᠢᠭᠤᠵᠤ ᠁</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="19"/>
        <source>File Name</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="23"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠬᠤ ᠡᠳᠦᠷ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠳᠤ ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="27"/>
        <source>File Type</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="31"/>
        <source>File Size</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="35"/>
        <source>Original Path</source>
        <translation>ᠪᠠᠯᠠᠷ ᠵᠠᠮ ᠤᠨ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="44"/>
        <source>Descending</source>
        <translation>ᠪᠠᠭᠤᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="49"/>
        <source>Ascending</source>
        <translation>ᠳᠡᠭᠡᠭ᠍ᠰᠢᠯᠡᠪᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="55"/>
        <source>Use global sorting</source>
        <translation>ᠪᠦᠬᠦ ᠲᠠᠯ᠎ᠠ ᠶᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠲᠠ ᠶᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="75"/>
        <source>List View</source>
        <translation>ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢᠨ ᠵᠢᠷᠤᠭ ᠢ ᠵᠢᠭᠰᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="76"/>
        <source>Icon View</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠷᠰᠦᠲᠦ ᠵᠢᠷᠤᠭ ᠃</translation>
    </message>
</context>
<context>
    <name>UKUI::TabWidget::DefaultSlideAnimatorFactory</name>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="49"/>
        <source>Default Slide</source>
        <translation>ᠬᠡᠢ ᠳ᠋ᠧᠩ ᠦᠨ ᠨᠤᠭᠤᠭ᠎ᠠ ᠶᠢ ᠳᠤᠪ ᠳᠤᠭᠤᠢ ᠲᠠᠨᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="50"/>
        <source>Let tab widget switch with a slide animation.</source>
        <translation>ᠰᠣᠩᠭᠣᠯᠲᠠ ᠶᠢᠨ ᠺᠠᠷᠲ ᠤᠨ ᠵᠢᠵᠢᠭ ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠲᠣᠨᠣᠭ ᠢ ᠬᠡᠢ ᠳ᠋ᠧᠩ ᠦᠨ ᠺᠠᠷᠲ᠋ᠣᠨ ᠵᠢᠷᠤᠭ ᠪᠣᠯᠭᠠᠨ ᠰᠣᠯᠢᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
</TS>
