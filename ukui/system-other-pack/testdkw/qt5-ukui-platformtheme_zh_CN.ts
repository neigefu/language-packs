<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="2523"/>
        <source>Open File</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="2524"/>
        <source>Save File</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="2537"/>
        <source>All Files (*)</source>
        <translation>所有(*)</translation>
    </message>
</context>
<context>
    <name>KyNativeFileDialog</name>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="192"/>
        <source>Go Back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="199"/>
        <source>Go Forward</source>
        <translation>前进</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="205"/>
        <source>Cd Up</source>
        <translation>向上</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="212"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="220"/>
        <source>View Type</source>
        <translation>视图类型</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="231"/>
        <source>Sort Type</source>
        <translation>排序类型</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="236"/>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="348"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="250"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="345"/>
        <source>Restore</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="941"/>
        <source>Name</source>
        <translation>文件名</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="944"/>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="1532"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="945"/>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="954"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="949"/>
        <source>Save as</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="951"/>
        <source>New Folder</source>
        <translation>新建文件夹</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="953"/>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="1536"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="989"/>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="990"/>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="992"/>
        <source>Directories</source>
        <translation>目录</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="1181"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="1181"/>
        <source>exist, are you sure replace?</source>
        <translation>已存在，是否替换？</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="1858"/>
        <source>NewFolder</source>
        <translation>新建文件夹</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="2146"/>
        <source>Undo</source>
        <translation>撤销</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="2153"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="2350"/>
        <source>warn</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/kyfiledialog.cpp" line="2350"/>
        <source>This operation is not supported.</source>
        <translation>不支持此操作。</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="166"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../widget/messagebox/message-box.h" line="190"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/messagebox/message-box.h" line="192"/>
        <source>Incompatible Qt Library Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="423"/>
        <location filename="../widget/messagebox/message-box.cpp" line="1106"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="479"/>
        <location filename="../widget/messagebox/message-box.cpp" line="897"/>
        <location filename="../widget/messagebox/message-box.cpp" line="1516"/>
        <source>Show Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/messagebox/message-box.cpp" line="897"/>
        <location filename="../widget/messagebox/message-box.cpp" line="1516"/>
        <source>Hide Details...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="19"/>
        <source>File Name</source>
        <translation>文件名称</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="23"/>
        <source>Modified Date</source>
        <translation>修改日期</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="27"/>
        <source>File Type</source>
        <translation>文件类型</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="31"/>
        <source>File Size</source>
        <translation>文件大小</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="35"/>
        <source>Original Path</source>
        <translation>原始路径</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="44"/>
        <source>Descending</source>
        <translation>降序</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="49"/>
        <source>Ascending</source>
        <translation>升序</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="55"/>
        <source>Use global sorting</source>
        <translation>使用全局排序</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="75"/>
        <source>List View</source>
        <translation>列表视图</translation>
    </message>
    <message>
        <location filename="../widget/filedialog/ui_kyfiledialog.cpp" line="76"/>
        <source>Icon View</source>
        <translation>图标视图</translation>
    </message>
</context>
<context>
    <name>UKUI::TabWidget::DefaultSlideAnimatorFactory</name>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="50"/>
        <source>Default Slide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="51"/>
        <source>Let tab widget switch with a slide animation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
