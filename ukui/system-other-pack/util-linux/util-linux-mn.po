msgid ""
msgstr ""
"Project-Id-Version: util-linux-2.29-rc2\n"
"Report-Msgid-Bugs-To: util-linux@vger.kernel.org\n"
"PO-Revision-Date: 2023-06-23 14:22+0000\n"
"Last-Translator: bolor2022 <759085099@qq.com>\n"
"Language-Team: Mongolian <http://weblate.openkylin.top/projects/test-for-mn/"
"util-linux/mn/>\n"
"Language: mn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.1-dev\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Crowdin-File: util-linux-2.29-rc2.zh_CN.po\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-Project: util-linux\n"
"X-Launchpad-Export-Date: 2020-04-16 16:51+0000\n"

msgid "%s from %s\n"
msgstr "s ᠨᠢ ᠡᠭᠦᠨ ᠡᠴᠡ s ᠃\n"

msgid "%s requires an argument"
msgstr "s ᠳᠤ ᠨᠢᠭᠡ ᠪᠤᠯᠵᠣᠯᠲᠤ ᠲᠣᠭ᠎ᠠ ᠬᠡᠷᠡᠭᠰᠡᠨ᠎ᠡ ᠃"

msgid "%s unchanged"
msgstr "s ᠶᠢ ᠥᠭᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠦᠭᠡᠶ᠃"

msgid "%s: not found"
msgstr "s ᠄ ᠡᠷᠢᠵᠦ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠭᠡᠶ ᠃"

msgid "(unknown)"
msgstr "( ᠦᠯᠦ ᠮᠡᠳᠡᠭᠳᠡᠬᠦ )"

msgid "Attributes:"
msgstr "ᠰᠢᠨᠵᠢ ᠴᠢᠨᠠᠷ ᠄"

msgid "Boot"
msgstr "ᠭᠤᠲᠤᠯ ᠃"

msgid "Bootable"
msgstr "ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃"

msgid "CPU time"
msgstr "ᠲᠥᠪ ᠦᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠦᠷ ᠦᠨ ᠴᠠᠭ ᠃"

msgid "Cannot open %s"
msgstr "s ᠶᠢ ᠨᠡᠭᠡᠭᠡᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃"

msgid "Clear"
msgstr "ᠲᠣᠳᠣᠷᠬᠠᠢ ᠮᠡᠳᠡᠨ᠎ᠡ ᠃"

msgid "Compaq diagnostics"
msgstr "ᠺᠠᠩ ᠪᠠᠢ ᠣᠨᠣᠰᠢᠯᠠᠯᠲᠠ ᠃"

msgid "Correct"
msgstr "ᠵᠥᠪ ᠃"

msgid "Delete"
msgstr "ᠬᠠᠰᠤᠨ᠎ᠠ᠃"

msgid "Description"
msgstr "ᠳᠦᠷᠰᠦᠯᠡᠨ ᠲᠣᠭᠠᠴᠢᠬᠤ ᠃"

msgid "Device"
msgstr "ᠲᠣᠨᠣᠭ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠃"

msgid "Device:"
msgstr "ᠲᠣᠨᠣᠭ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ"

msgid "Disk"
msgstr "ᠫᠠᠨᠰᠠ ᠃"

msgid "Done.\n"
msgstr "ᠬᠢ.\n"

msgid "EFI System"
msgstr "ᠴᠠᠬᠢᠯᠭᠠᠨ ᠰᠦᠷᠴᠢᠬᠦ ᠰᠢᠰᠲ᠋ᠧᠮ ᠃"

msgid "Empty"
msgstr "ᠬᠣᠭᠣᠰᠣᠨ ᠃"

msgid "End"
msgstr "ᠲᠡᠭᠦᠰᠬᠡᠯ"

msgid "Extended"
msgstr "ᠥᠷᠭᠡᠳᠬᠡᠨ᠎ᠡ᠃"

msgid "Filename"
msgstr "ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠃"

msgid "Flags"
msgstr "ᠲᠡᠮᠳᠡᠭ ᠃"

msgid "Flags:"
msgstr "ᠲᠡᠮᠳᠡᠭ ᠄"

msgid "Generic"
msgstr "ᠨᠡᠢ᠌ᠲᠡ ᠳᠦᠨᠢ ᠬᠡᠷᠡᠭ᠍ᠯᠡᠬᠦ ᠃"

msgid "Group name"
msgstr "ᠳᠤᠭᠤᠶᠢᠯᠠᠩ ᠤᠨ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠃"

msgid "Help"
msgstr "ᠬᠠᠪᠰᠤᠷᠤᠨ᠎ᠠ ᠃"

msgid "Hidden FAT12"
msgstr "ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ FAT12 ᠃"

msgid "Hidden FAT16"
msgstr "ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ FAT16 ᠃"

msgid "Hidden FAT16 <32M"
msgstr "ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ ᠮᠠᠶᠢᠭ ᠤᠨ FAT16 <32M ᠃"

msgid "Hidden HPFS/NTFS"
msgstr "ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ HPFS /NTFS ᠃"

msgid "Hidden W95 FAT16 (LBA)"
msgstr "ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ W95 ᠹᠠᠲ᠋16 (LBA)"

msgid "Hidden W95 FAT32"
msgstr "ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ W95 FAT32 ᠃"

msgid "Hidden W95 FAT32 (LBA)"
msgstr "ᠨᠢᠭᠤᠭᠳᠠᠭᠰᠠᠨ W95 FAT32 (LBA)"

msgid "ID"
msgstr "ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠦᠨᠡᠮᠯᠡᠯ᠃"

msgid "Id"
msgstr "ᠨᠣᠮᠧᠷ ᠃"

msgid "Key"
msgstr "ᠪᠢ?"

msgid "Linux"
msgstr "Linuf ᠭᠠᠷᠴᠠᠭ ᠃"

msgid "Linux LVM"
msgstr "Linux LVM"

msgid "Linux RAID"
msgstr "Linux RAID"

msgid "Linux extended"
msgstr "Linuuf ᠥᠷᠭᠡᠳᠪᠡ ᠃"

msgid "Messages"
msgstr "ᠴᠢᠮᠡᠭᠡ᠃"

msgid "Microsoft LDM data"
msgstr "ᠮᠢᠺᠷᠤᠹᠧᠲ᠋ LDM ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠃"

msgid "Microsoft LDM metadata"
msgstr "ᠮᠢᠺᠷᠤᠹᠧᠲ᠋ LDM ᠲᠥᠭᠥᠷᠢᠭ ᠦᠨ ᠲᠣᠭ᠎ᠠ ᠪᠠᠷᠢᠮᠲᠠ ᠃"

msgid "Misc"
msgstr "ᠰᠤᠯᠠ ᠵᠦᠢᠯ ᠃"

msgid "Model:"
msgstr "ᠬᠡᠯᠪᠡᠷᠢ ᠄"

msgid "Name"
msgstr "ᠨᠡᠷ᠎ᠡ"

msgid "New"
msgstr "ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠨᠡᠮᠡᠭᠳᠡᠭᠦᠯᠬᠦ ᠴᠢᠳᠠᠮᠵᠢ ᠶᠢ ᠰᠢᠨ᠎ᠡ"

msgid "New name"
msgstr "ᠰᠢᠨ᠎ᠡ ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠃"

msgid "Not enough arguments"
msgstr "ᠭᠦᠢᠴᠡᠳ ᠪᠠᠷᠢᠮᠲᠠ ᠪᠠᠶᠢᠬᠤ ᠦᠭᠡᠢ ᠃"

msgid "Not set"
msgstr "ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠭᠡᠶ᠃"

msgid "Number of sectors"
msgstr "ᠲᠣᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠃"

msgid "Office"
msgstr "ᠠᠯᠪᠠᠨ ᠭᠡᠷ᠃"

msgid "Owner"
msgstr "ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠥᠮᠥᠨ ᠃"

msgid "PPC PReP Boot"
msgstr "PPC PReP ᠡᠬᠢᠯᠡᠵᠡᠶ ᠃"

msgid "Password: "
msgstr "ᠨᠢᠭᠤᠴᠠ ᠨᠣᠮᠧᠷ ᠄ "

msgid "Permissions"
msgstr "ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ"

msgid "Quit"
msgstr "ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ"

msgid "Resource"
msgstr "ᠡᠬᠢ ᠪᠠᠶᠠᠯᠢᠭ ᠃"

msgid "SGI"
msgstr "ᠣᠯᠠᠨ ᠤᠯᠤᠰ ᠤᠨ ᠦᠨ᠎ᠡ ᠡᠭᠦᠳᠦᠨ ᠪᠦᠲᠦᠭᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ ᠤ ᠨᠡᠶᠢᠭᠡᠮᠯᠢᠭ ᠃"

msgid "Script"
msgstr "ᠬᠥᠯ ᠦᠨ ᠳᠡᠪᠲᠡᠷ ᠃"

msgid "Set"
msgstr "ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ ᠃"

msgid "Single"
msgstr "ᠬᠠᠭᠤᠳᠠᠰᠤ ᠃"

msgid "Size"
msgstr "ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠃"

msgid "Slice"
msgstr "ᠺᠢᠨᠣ᠋ ᠃"

msgid "Solaris boot"
msgstr "Solaris boot"

msgid "Sort"
msgstr "ᠵᠢᠭᠰᠠᠭᠠᠬᠤ ᠃"

msgid "Start"
msgstr "ᠡᠬᠢᠯᠡᠪᠡ ᠃"

msgid "Status"
msgstr "ᠪᠠᠶᠢᠷᠢ ᠰᠠᠭᠤᠷᠢ ᠃"

msgid "Sun"
msgstr "ᠨᠠᠷᠠ"

msgid "Type"
msgstr "ᠲᠦᠷᠦᠯ ᠬᠡᠯᠪᠡᠷᠢ᠃"

msgid "Unknown"
msgstr "ᠮᠡᠳᠡᠬᠦ ᠦᠭᠡᠢ ᠃"

msgid "Unmark"
msgstr "ᠲᠡᠮᠳᠡᠭ ᠢ ᠨᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ ᠃"

msgid "Use"
msgstr "ᠬᠡᠷᠡᠭ᠍ᠯᠡᠨ᠎ᠡ ᠃"

msgid "Used"
msgstr "ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ᠃"

msgid "User ID"
msgstr "ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠲᠡᠮᠳᠡᠭ"

msgid "User name"
msgstr "ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠃"

msgid "Username"
msgstr "ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ ᠃"

msgid "Write"
msgstr "ᠪᠢᠴᠢᠬᠦ"

msgid "Y"
msgstr "Y"

msgid "bad arguments"
msgstr "ᠮᠠᠭᠤᠬᠠᠢ ᠣᠨᠣᠯ ᠃"

msgid "bytes"
msgstr "ᠦᠰᠦᠭ ᠦᠨ ᠪᠠᠶᠠᠷ᠃"

msgid "can't find %s"
msgstr "ᠡᠷᠢᠵᠦ ᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃"

msgid "cannot fork"
msgstr "ᠰᠠᠯᠭᠠᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠃"

msgid "cannot read %s"
msgstr "〈 s ᠶᠢ ᠤᠩᠰᠢᠵᠤ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃"

msgid "cannot seek"
msgstr "ᠡᠷᠢᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃"

msgid "cannot write %s"
msgstr "ᠡᠭᠦᠨ ᠳᠦ ᠪᠢᠴᠢᠵᠦ ᠳᠡᠶᠢᠯᠬᠦ ᠦᠭᠡᠢ ᠃"

msgid "change"
msgstr "ᠬᠣᠪᠢᠷᠠᠨ᠎ᠠ᠃"

msgid "connect"
msgstr "ᠵᠠᠯᠭᠠᠬᠤ ᠃"

msgid "detached"
msgstr "ᠬᠡᠲᠦᠷᠡᠲᠡᠯ᠎ᠡ ᠃"

msgid "done\n"
msgstr "ᠬᠢ\n"

msgid "failed"
msgstr "ᠢᠯᠠᠭᠳᠠᠯ ᠃"

msgid "files"
msgstr "ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠃"

msgid "flush buffers"
msgstr "ᠤᠬᠢᠶᠠᠬᠤ ᠰᠢᠩᠭᠡᠨ ᠢ ᠤᠬᠢᠶᠠᠬᠤ"

msgid "full"
msgstr "ᠳᠦᠭᠦᠷᠡᠩ ᠃"

msgid "gid"
msgstr "GID"

msgid "horizontal"
msgstr "ᠬᠢᠷᠢ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠃"

msgid "internal error"
msgstr "ᠳᠣᠲᠣᠭᠠᠳᠤ ᠪᠤᠷᠤᠭᠤ ᠃"

msgid "key"
msgstr "..key"

msgid "local"
msgstr "ᠲᠤᠬᠠᠢ ᠶᠢᠨ ᠭᠠᠵᠠᠷ ᠃"

msgid "locked"
msgstr "ᠣᠨᠢᠰᠤ᠃"

msgid "login: "
msgstr "ᠲᠡᠮᠳᠡᠭᠯᠡᠯ "

msgid "messages"
msgstr "ᠴᠢᠮᠡᠭᠡ᠃"

msgid "move"
msgstr "ᠰᠢᠯᠵᠢᠯᠲᠡ ᠃"

msgid "no"
msgstr "ᠦᠭᠡᠢ ᠃"

msgid "none"
msgstr "ᠦᠭᠡᠢ"

msgid "open failed"
msgstr "ᠢᠯᠠᠭᠳᠠᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃"

msgid "owner"
msgstr "ᠪᠤᠢ ᠪᠥᠬᠥᠢ ᠬᠥᠮᠥᠨ ᠃"

msgid "permission denied"
msgstr "ᠡᠷᠬᠡ ᠮᠡᠳᠡᠯ ᠨᠢ ᠲᠡᠪᠴᠢᠭ᠍ᠳᠡᠪᠡ ᠃"

msgid "perms"
msgstr "ᠠᠭᠠᠰᠢᠯᠠ ᠃"

msgid "primary"
msgstr "ᠭᠣᠤᠯᠳᠠᠭᠤ᠃"

msgid "recv"
msgstr "ᠲᠠᠨᠢᠯᠴᠠᠭᠤᠯᠬᠤ ᠃"

msgid "seconds"
msgstr "ᠰᠧᠺᠦᠢᠨᠳ᠋ ᠃"

msgid "seek error"
msgstr "ᠪᠤᠷᠤᠭᠤ ᠡᠷᠢᠨ᠎ᠡ ᠃"

msgid "seek failed"
msgstr "ᠡᠷᠢᠬᠦ ᠵᠠᠮ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠃"

msgid "send"
msgstr "ᠶᠠᠪᠤᠭᠤᠯᠤᠨ᠎ᠠ ᠃"

msgid "size"
msgstr "ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠃"

msgid "socket"
msgstr "ᠤᠭᠯᠠᠭᠤᠷ ᠃"

msgid "status"
msgstr "ᠪᠠᠶᠢᠷᠢ ᠰᠠᠭᠤᠷᠢ ᠃"

msgid "too many arguments"
msgstr "ᠪᠤᠯᠵᠣᠯᠲᠤ ᠲᠣᠭ᠎ᠠ ᠳᠡᠩᠳᠡᠭᠦᠦ ᠠᠷᠪᠢᠨ"

msgid "total"
msgstr "ᠶᠡᠷᠦᠩᠬᠡᠢ ᠳᠦ"

msgid "uid"
msgstr "UID"

msgid "unable to fork"
msgstr "ᠰᠠᠯᠭᠠᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠃"

msgid "unknown"
msgstr "ᠮᠡᠳᠡᠬᠦ ᠦᠭᠡᠢ ᠃"

msgid "unlimited"
msgstr "ᠬᠢᠵᠠᠭᠠᠷ ᠦᠭᠡᠢ ᠃"

msgid "value"
msgstr "ᠦᠨ᠎ᠡ ᠥᠷᠲᠡᠭ᠍ ᠃"

msgid "version"
msgstr "ᠬᠡᠪᠯᠡᠯ ᠃"

msgid "vertical"
msgstr "ᠡᠭᠴᠡ ᠃"

msgid "write"
msgstr "ᠪᠢᠴᠢᠬᠦ"

msgid "write error"
msgstr "ᠪᠤᠷᠤᠭᠤ ᠪᠢᠴᠢᠵᠦ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃"

msgid "write failed"
msgstr "ᠢᠯᠠᠭᠳᠠᠯ ᠳᠤ ᠪᠢᠴᠢᠵᠦ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ ᠃"

msgid "yes"
msgstr "ᠲᠡᠢ᠌ᠮᠦ ᠡ ᠃"
