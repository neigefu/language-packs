<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BasePopupTitle</name>
    <message>
        <source>Font Viewer</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BasePreviewArea</name>
    <message>
        <source>Enter Text Content For Preview</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠤᠷᠤᠭᠤᠯᠵᠤ ᠦᠵᠡᠬᠡᠷᠡᠢ</translation>
    </message>
</context>
<context>
    <name>BaseSearchEdit</name>
    <message>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ</translation>
    </message>
</context>
<context>
    <name>BlankPage</name>
    <message>
        <source>No Font</source>
        <translation>ᠲᠦᠷ ᠦᠭᠡᠢ ᠦᠰᠦᠭ ᠦᠨ ᠲᠢᠭ</translation>
    </message>
    <message>
        <source>No Search Results</source>
        <translation>ᠨᠡᠩᠵᠢᠯᠲᠡ ᠶᠢᠨ ᠳ᠋ᠦᠩ ᠦᠭ᠋ᠡᠢ</translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <source>none</source>
        <translation>ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>FontListView</name>
    <message>
        <source>Add Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Apply Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Remove Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Export Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel Collection</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠢ ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Check Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠦ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Add Fonts</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>font(*.ttf *.fon *.ttc *.afm)</source>
        <translation type="vanished">字体文件(*.ttf *.fon *.ttc *.afm)</translation>
    </message>
    <message>
        <source>Export Fonts</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Build the core strength of Chinese operating system</source>
        <translation>ᠳᠤᠮᠳᠠᠳᠤ ᠤᠯᠤᠰ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠭᠤᠤᠯᠯᠠᠬᠤ ᠬᠦᠴᠦᠨ ᠢ᠋ ᠪᠤᠢ ᠪᠤᠯᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>font(*.ttf *.otf)</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠲᠠᠮᠢᠷ ᠤᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ( .ttf*otf )</translation>
    </message>
</context>
<context>
    <name>FunctionWid</name>
    <message>
        <source>All Font</source>
        <translation>ᠪᠦᠬᠦ ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <source>System Font</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦ᠋ᠨ ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <source>User Font</source>
        <translation>ᠮᠢᠨᠤ ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌</translation>
    </message>
    <message>
        <source>Collection Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <source>Font Viewer</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>PopupAbout</name>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Font Viewer is a tool to help users install and organize management; After installation, the font can be applied to self-developed applications, third-party pre installed applications and user self installed applications.</source>
        <translation type="vanished">字体管理器是一款帮助用户安装和组织管理的一款工具；安装后字体可运用至自研应用、第三方预装应用和用户自装应用。</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持：</translation>
    </message>
    <message>
        <source>Font Viewer</source>
        <translation type="vanished">字体管理器</translation>
    </message>
</context>
<context>
    <name>PopupFontInfo</name>
    <message>
        <source>FontName:</source>
        <translation>ᠨᠡᠷᠡᠶᠢᠳᠦᠯ ᠄</translation>
    </message>
    <message>
        <source>FontSeries:</source>
        <translation>ᠰᠢᠷᠢᠰ ᠄</translation>
    </message>
    <message>
        <source>FontStyle:</source>
        <translation>ᠶᠠᠩᠵᠤ</translation>
    </message>
    <message>
        <source>FontType:</source>
        <translation>ᠲᠥᠷᠥᠯ ᠵᠦᠢᠯ ᠄</translation>
    </message>
    <message>
        <source>FontVersion:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <source>FontPath:</source>
        <translation>ᠪᠠᠶᠢᠷᠢ</translation>
    </message>
    <message>
        <source>FontCopyright:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠦᠨ ᠡᠷᠬᠡ</translation>
    </message>
    <message>
        <source>FontTrademark:</source>
        <translation>ᠲᠠᠸᠠᠷ ᠤᠨ ᠲᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>PopupInstallFail</name>
    <message>
        <source>There is a problem with the font file. Installation failed!</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠤ᠋ᠨ ᠹᠠᠢᠯ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠲᠠᠢ᠂ ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>PopupInstallSuccess</name>
    <message>
        <source>already installed </source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠤᠭᠰᠠᠷᠠᠪᠠ </translation>
    </message>
    <message>
        <source> fonts!</source>
        <translation> ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌!</translation>
    </message>
    <message>
        <source> fonts!  </source>
        <translation type="vanished">款字体！</translation>
    </message>
</context>
<context>
    <name>PopupMessage</name>
    <message>
        <source>Do you want to install the selected font?</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ ᠦᠰᠦᠭ ᠰᠣᠩᠭᠣᠬᠤ ᠪᠡᠶᠡᠲᠦ ᠶᠢ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠤᠤ ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>ᠮᠥᠨ</translation>
    </message>
    <message>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ</translation>
    </message>
</context>
<context>
    <name>PopupRemove</name>
    <message>
        <source>Are you sure you want to remove this font?</source>
        <translation>ᠲᠤᠰ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠯᠠᠪᠳᠠᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>PopupTips</name>
    <message>
        <source>Installing new fonts takes effect after restarting!</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠦᠰᠦᠭ ᠦᠨ ᠲᠢᠭ ᠤᠭᠰᠠᠷᠠᠬᠤ ᠳᠤ ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠦᠴᠦᠨ ᠲᠡᠶ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Do not show again</source>
        <translation>ᠳᠠᠬᠢᠨ ᠦᠵᠡᠭᠦᠯᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Font Viewer</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Add Font</source>
        <translation>ᠦᠰᠦᠭ ᠦ᠋ᠨ ᠲᠢᠭ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>ᠰᠣᠩᠬ᠋ᠣᠬᠳ᠋ᠠᠬᠣᠨ</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠄ </translation>
    </message>
    <message>
        <source>Font Viewer is a tool to help users install and organize management; After installation, the font can be applied to self-developed applications, third-party pre installed applications and user self installed applications.</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠨᠭ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ ᠪᠤᠯ ᠨᠢᠭᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳ᠋ᠤ᠌ ᠳᠤᠰᠠᠯᠠᠵᠤ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠢ ᠤᠭᠰᠠᠷᠴᠤ ᠵᠤᠬᠢᠶᠠᠨ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠪᠤᠯᠤᠨ᠎ᠠ᠃ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠢ᠋ ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠰᠤᠳᠤᠯᠤᠨ ᠪᠦᠳᠦᠭᠡᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ᠂ ᠭᠤᠷᠪᠠᠳᠠᠭᠴᠢ ᠡᠳᠡᠭᠡᠳ ᠤ᠋ᠨ ᠪᠡᠯᠡᠳᠬᠡᠯ ᠤᠭᠰᠠᠷᠬᠤ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠪᠤᠯᠤᠨ᠎ᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>Font Viewer</source>
        <translation>ᠦᠰᠦᠭ ᠤ᠋ᠨ ᠲᠢᠭ᠌ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <source>none</source>
        <translation>ᠦᠭᠡᠢ</translation>
    </message>
</context>
</TS>
