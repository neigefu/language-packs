<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BasePopupTitle</name>
    <message>
        <source>Font Viewer</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>BasePreviewArea</name>
    <message>
        <source>Enter Text Content For Preview</source>
        <translation>ཡི་གེའི་ནང་དོན་ནང་འཇུག་བྱས་ནས་སྔོན་ལྟ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>BaseSearchEdit</name>
    <message>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ</translation>
    </message>
</context>
<context>
    <name>BlankPage</name>
    <message>
        <source>No Font</source>
        <translation>གནས་སྐབས་ཡིག་གཟུགས་མེད་པ།</translation>
    </message>
    <message>
        <source>No Search Results</source>
        <translation>དེ་དང་འབྲེལ་བ་མེད་པའི་ཡིག་གཟུགས།</translation>
    </message>
</context>
<context>
    <name>External</name>
    <message>
        <source>none</source>
        <translation>མེད།</translation>
    </message>
</context>
<context>
    <name>FontListView</name>
    <message>
        <source>Add Font</source>
        <translation>ཡིག་གཟུགས་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <source>Apply Font</source>
        <translation>ཡིག་གཟུགས་བཀོལ་སྤྱོད་བྱེད་པ</translation>
    </message>
    <message>
        <source>Remove Font</source>
        <translation>ཡིག་གཟུགས་མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>Export Font</source>
        <translation>ཡིག་གཟུགས་ཕྱིར་འདྲེན།</translation>
    </message>
    <message>
        <source>Cancel Collection</source>
        <translation>བསྡུ་ལེན་མེད་པར་བཟོ</translation>
    </message>
    <message>
        <source>Check Font</source>
        <translation>ཡིག་གཟུགས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation>ཡིག་གཟུགས་བསྡུ་ཉར།</translation>
    </message>
    <message>
        <source>Add Fonts</source>
        <translation>ཡིག་གཟུགས་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>font(*.ttf *.fon *.ttc *.afm)</source>
        <translation type="vanished">字体文件(*.ttf *.fon *.ttc *.afm)</translation>
    </message>
    <message>
        <source>Export Fonts</source>
        <translation>ཡིག་གཟུགས་ཕྱིར་གཏོང་།</translation>
    </message>
    <message>
        <source>Build the core strength of Chinese operating system</source>
        <translation>ཀྲུང་གོའི་བཀོལ་སྤྱོད་མ་ལག་གི་ལྟེ་བའི་སྟོབས་ཤུགས་འཛུགས་སྐྱོང་བྱ</translation>
    </message>
    <message>
        <source>font(*.ttf *.otf)</source>
        <translation>ཡིག་གཟུགས་ཡིག་ཆ། (.ttf པ། )</translation>
    </message>
</context>
<context>
    <name>FunctionWid</name>
    <message>
        <source>All Font</source>
        <translation>ཡིག་གཟུགས་ཡོད་ཚད།</translation>
    </message>
    <message>
        <source>System Font</source>
        <translation>མ་ལག་གི་ཡིག་གཟུགས</translation>
    </message>
    <message>
        <source>User Font</source>
        <translation>སྤྱོད་མཁན་གྱི་ཡིག་གཟུགས</translation>
    </message>
    <message>
        <source>Collection Font</source>
        <translation>འཚོལ་སྡུད་བྱས་པའི་ཡིག་གཟུགས།</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <source>Font Viewer</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཆས།</translation>
    </message>
</context>
<context>
    <name>PopupAbout</name>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Font Viewer is a tool to help users install and organize management; After installation, the font can be applied to self-developed applications, third-party pre installed applications and user self installed applications.</source>
        <translation type="vanished">字体管理器是一款帮助用户安装和组织管理的一款工具；安装后字体可运用至自研应用、第三方预装应用和用户自装应用。</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持：</translation>
    </message>
    <message>
        <source>Font Viewer</source>
        <translation type="vanished">字体管理器</translation>
    </message>
</context>
<context>
    <name>PopupFontInfo</name>
    <message>
        <source>FontName:</source>
        <translation>མིང་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <source>FontSeries:</source>
        <translation>རིམ་པ་འདི་ལྟ་</translation>
    </message>
    <message>
        <source>FontStyle:</source>
        <translation>བཟོ་དབྱིབས་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <source>FontType:</source>
        <translation>སྣ་ཁ་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <source>FontVersion:</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <source>FontPath:</source>
        <translation>གོ་གནས་གཤམ་གསལ།</translation>
    </message>
    <message>
        <source>FontCopyright:</source>
        <translation>པར་དབང་འདི་ལྟར།</translation>
    </message>
    <message>
        <source>FontTrademark:</source>
        <translation>ཚོང་རྟགས་</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>PopupInstallFail</name>
    <message>
        <source>There is a problem with the font file. Installation failed!</source>
        <translation>ཡིག་གཟུགས་ཡིག་ཆ་ལ་གནད་དོན་ཡོད། སྒྲིག་སྦྱོར་བྱས་ཀྱང་ཕམ་སོང་།</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation>ངོས་འཛིན་བྱས་པ།</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>PopupInstallSuccess</name>
    <message>
        <source>already installed </source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ཟིན་པ། </translation>
    </message>
    <message>
        <source> fonts!</source>
        <translation> ཡིག་གཟུགས་རིགས་ཤིག</translation>
    </message>
    <message>
        <source> fonts!  </source>
        <translation type="vanished">款字体！</translation>
    </message>
</context>
<context>
    <name>PopupMessage</name>
    <message>
        <source>Do you want to install the selected font?</source>
        <translation>ཡིག་གཟུགས་སྒྲིག་སྦྱོར་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>དེ་ནི་རེད།</translation>
    </message>
    <message>
        <source>No</source>
        <translation>དེ་ལྟར་མ་བྱས་</translation>
    </message>
</context>
<context>
    <name>PopupRemove</name>
    <message>
        <source>Are you sure you want to remove this font?</source>
        <translation>ཁྱོད་ཀྱིས་དངོས་གནས་ཡིག་གཟུགས་འདི་མེད་པར་བཟོ་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>PopupTips</name>
    <message>
        <source>Installing new fonts takes effect after restarting!</source>
        <translation>ཡིག་གཟུགས་གསར་བ་སྒྲིག་སྦྱོར་བྱེད་པར་གློག་ཀླད་ཀྱི་ནུས་པ་ཐོན་དགོས།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>དངོས་སུ་ཁས་ལེན་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <source>Do not show again</source>
        <translation>ད་ནས་བཟུང་གསལ་པོར་མངོན་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>Font Viewer</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཡོ་བྱད།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Add Font</source>
        <translation>ཡིག་གཟུགས་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་དུ་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>རྣམ་གྲངས་འདེམས་པ།</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>སླར་གསོ།</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>པར་གཞི། </translation>
    </message>
    <message>
        <source>Font Viewer is a tool to help users install and organize management; After installation, the font can be applied to self-developed applications, third-party pre installed applications and user self installed applications.</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཡོ་བྱད་ནི་སྤྱོད་མཁན་ལ་སྒྲིག་སྦྱོར་དང་རྩ་འཛུགས་དོ་དམ་བྱེད་རོགས་བྱེད་པའི་ལག་ཆ་ཞིག་ཡིན། སྒྲིག་སྦྱོར་བྱས་རྗེས་ཀྱི་ཡིག་གཟུགས་དེ་རང་ཉིད་ཞིབ་འཇུག་བེད་སྤྱོད་དང་ཕྱོགས་གསུམ་པའི་སྔོན་སྒྲིག་ཉེར་སྤྱོད་དང་བེད་སྤྱོད་བྱེད་མཁན་རང་གིས་ཉེར་སྤྱོད་བྱས་ཆོག</translation>
    </message>
    <message>
        <source>Font Viewer</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>none</source>
        <translation>མེད།</translation>
    </message>
</context>
</TS>
