<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AppLauncher</name>
    <message>
        <location filename="../src/tools/launcher/applaunchertool.cpp" line="+46"/>
        <source>App Launcher</source>
        <translation>应用启动器</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Choose an app to open the capture</source>
        <translation>选择一个应用打开此截图</translation>
    </message>
</context>
<context>
    <name>AppLauncherWidget</name>
    <message>
        <location filename="../src/tools/launcher/applauncherwidget.cpp" line="+55"/>
        <source>Open With</source>
        <translation>打开</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Launch in terminal</source>
        <translation>在终端中启动</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Keep open after selection</source>
        <translation>选择后保持此窗口打开</translation>
    </message>
    <message>
        <location line="+27"/>
        <location line="+12"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to launch in terminal.</source>
        <translation>无法在终端中启动。</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>Unable to write in</source>
        <translation>无法写入</translation>
    </message>
</context>
<context>
    <name>ArrowTool</name>
    <message>
        <location filename="../src/tools/arrow/arrowtool.cpp" line="+103"/>
        <source>Arrow</source>
        <translation>箭头</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Set the Arrow as the paint tool</source>
        <translation>选择箭头作为绘画工具</translation>
    </message>
</context>
<context>
    <name>AudioRecordTool</name>
    <message>
        <location filename="../src/tools/audio_record/audio_record_tool.cpp" line="+16"/>
        <location line="+26"/>
        <source>AudioRecord</source>
        <translation>音频录制</translation>
    </message>
</context>
<context>
    <name>BlurTool</name>
    <message>
        <source>Blur</source>
        <translation type="vanished">模糊</translation>
    </message>
    <message>
        <location filename="../src/tools/blur/blurtool.cpp" line="+59"/>
        <source>blur</source>
        <translation>模糊</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Set Blur as the paint tool</source>
        <translation>选择模糊作为绘画工具</translation>
    </message>
</context>
<context>
    <name>CaptureButton</name>
    <message>
        <source>Option</source>
        <translatorcomment>选项</translatorcomment>
        <translation type="vanished">选项</translation>
    </message>
    <message>
        <location filename="../src/widgets/capture/capturebutton.cpp" line="+48"/>
        <source>option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Save</source>
        <translatorcomment>保存</translatorcomment>
        <translation>保存</translation>
    </message>
</context>
<context>
    <name>CaptureLauncher</name>
    <message>
        <source>&lt;b&gt;Capture Mode&lt;/b&gt;</source>
        <translation type="vanished">&lt;b&gt;捕获模式&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/capturelauncher.cpp" line="+74"/>
        <source>Rectangular Region</source>
        <translation>框选区域</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Full Screen (All Monitors)</source>
        <translation>全屏截图</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Take shot</source>
        <translation>开始截图</translation>
    </message>
    <message>
        <source>No Delay</source>
        <translation type="vanished">无延迟</translation>
    </message>
    <message>
        <location line="-47"/>
        <source>Screenshot</source>
        <translation>截图</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Capture Mode</source>
        <translation>捕获模式</translation>
    </message>
    <message>
        <location line="+23"/>
        <source> second</source>
        <translation> 秒</translation>
    </message>
    <message>
        <location line="+0"/>
        <source> seconds</source>
        <translation> 秒</translation>
    </message>
    <message>
        <source>Take new screenshot</source>
        <translation type="vanished">获取新屏幕截图</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Area:</source>
        <translation>区域：</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delay:</source>
        <translation>延迟：</translation>
    </message>
</context>
<context>
    <name>CaptureWidget</name>
    <message>
        <location filename="../src/widgets/capture/capturewidget.cpp" line="+113"/>
        <source>Unable to capture screen</source>
        <translatorcomment>无法捕获屏幕</translatorcomment>
        <translation>无法捕获屏幕</translation>
    </message>
    <message>
        <location line="+205"/>
        <source>save as</source>
        <translatorcomment>另存为</translatorcomment>
        <translation>另存为</translation>
    </message>
    <message>
        <source>save as ....</source>
        <translatorcomment>另存为....</translatorcomment>
        <translation type="obsolete">另存为....</translation>
    </message>
    <message>
        <location line="+154"/>
        <source>%1 , %2</source>
        <translation>%1 , %2</translation>
    </message>
    <message>
        <source>Select an area with the mouse, or press Esc to exit.
Press Enter to capture the screen.
Press Right Click to show the color picker.
Use the Mouse Wheel to change the thickness of your tool.
Press Space to open the side panel.</source>
        <translation type="vanished">用鼠标选择一个区域,或按 Esc 退出。
按 Enter 键捕捉屏幕。
按住鼠标右键显示颜色选择器。
使用鼠标滚轮来改变绘画工具的宽度。
按下空格键以打开侧边面板。</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>%1 * %2</source>
        <translation>%1 * %2</translation>
    </message>
    <message>
        <source>Open Directory</source>
        <translation type="vanished">选择目录</translation>
    </message>
</context>
<context>
    <name>CircleTool</name>
    <message>
        <location filename="../src/tools/circle/circletool.cpp" line="+57"/>
        <source>Circle</source>
        <translation>圆环</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Set the Circle as the paint tool</source>
        <translation>选择圆环作为绘画工具</translation>
    </message>
</context>
<context>
    <name>ConfigWindow</name>
    <message>
        <location filename="../src/config/configwindow.cpp" line="+42"/>
        <source>Configuration</source>
        <translation>配置</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Interface</source>
        <translation>界面</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Filename Editor</source>
        <translation>文件名编辑器</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
</context>
<context>
    <name>Controller</name>
    <message>
        <location filename="../src/core/controller.cpp" line="+258"/>
        <source>&amp;Take Screenshot</source>
        <translation>进行截图(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Open Launcher</source>
        <translation type="vanished">打开启动器(&amp;O)</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Open Screenshot Option</source>
        <translation>打开截图选项(&amp;O)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Configuration</source>
        <translation>配置(&amp;C)</translation>
    </message>
    <message>
        <source>&amp;Information</source>
        <translation type="vanished">信息(&amp;I)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;About</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Quit</source>
        <translation>退出(&amp;Q)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;ShortCut</source>
        <translation>快捷键(&amp;S)</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Screenshot</source>
        <translation>截图</translation>
    </message>
    <message>
        <source>Kylin-Screenshot</source>
        <translation type="vanished">麒麟截图</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Unable to use kylin-screenshot</source>
        <translatorcomment>麒麟截图已禁用</translatorcomment>
        <translation>麒麟截图已禁用</translation>
    </message>
</context>
<context>
    <name>CopyTool</name>
    <message>
        <location filename="../src/tools/copy/copytool.cpp" line="+53"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy the selection into the clipboard</source>
        <translation>复制选择到剪贴板</translation>
    </message>
</context>
<context>
    <name>CursorRecordTool</name>
    <message>
        <location filename="../src/tools/cursor_record/cursor_record_tool.cpp" line="+16"/>
        <location line="+26"/>
        <source>CursorRecord</source>
        <translation>游标记录</translation>
    </message>
</context>
<context>
    <name>Cut</name>
    <message>
        <location filename="../src/tools/cut/cut.cpp" line="+47"/>
        <source>Cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>cut</source>
        <translation>剪切</translation>
    </message>
</context>
<context>
    <name>CutTool</name>
    <message>
        <source>cut</source>
        <translation type="vanished">截图模式</translation>
    </message>
</context>
<context>
    <name>DBusUtils</name>
    <message>
        <location filename="../src/utils/dbusutils.cpp" line="+42"/>
        <source>Unable to connect via DBus</source>
        <translation>无法通过 DBus 进行连接</translation>
    </message>
</context>
<context>
    <name>ExitTool</name>
    <message>
        <location filename="../src/tools/exit/exittool.cpp" line="+47"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Leave the capture screen</source>
        <translation>离开屏幕捕获</translation>
    </message>
</context>
<context>
    <name>FileNameEditor</name>
    <message>
        <location filename="../src/config/filenameeditor.cpp" line="+35"/>
        <source>Edit the name of your captures:</source>
        <translation>编辑您的截图名称:</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit:</source>
        <translation>编辑器:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preview:</source>
        <translation>预览:</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Saves the pattern</source>
        <translation>保存样式</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reset</source>
        <translation>恢复</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restores the saved pattern</source>
        <translation>恢复保存的样式</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Deletes the name</source>
        <translation>删除这个名字</translation>
    </message>
</context>
<context>
    <name>FollowMouseRecordTool</name>
    <message>
        <location filename="../src/tools/follow_mouse_record/follow_mouse_record_tool.cpp" line="+14"/>
        <location line="+26"/>
        <source>FollowMouseRecord</source>
        <translation>跟随鼠标记录</translation>
    </message>
</context>
<context>
    <name>Font_Options</name>
    <message>
        <location filename="../src/widgets/capture/font_options.cpp" line="+110"/>
        <source>StrikeOut</source>
        <translatorcomment>删除线</translatorcomment>
        <translation>删除线</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Underline</source>
        <translatorcomment>下划线</translatorcomment>
        <translation>下划线</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Bold</source>
        <translatorcomment>粗体</translatorcomment>
        <translation>粗体</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Italic</source>
        <translatorcomment>斜体</translatorcomment>
        <translation>斜体</translation>
    </message>
</context>
<context>
    <name>Font_Options2</name>
    <message>
        <location filename="../src/widgets/capture/font_options2.cpp" line="+122"/>
        <source>StrikeOut</source>
        <translatorcomment>删除线</translatorcomment>
        <translation>删除线</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Underline</source>
        <translatorcomment>下划线</translatorcomment>
        <translation>下划线</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bold</source>
        <translatorcomment>粗体</translatorcomment>
        <translation>粗体</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Italic</source>
        <translatorcomment>斜体</translatorcomment>
        <translation>斜体</translation>
    </message>
</context>
<context>
    <name>GeneneralConf</name>
    <message>
        <location filename="../src/config/geneneralconf.cpp" line="+131"/>
        <source>Show help message</source>
        <translation>显示帮助文档</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show the help message at the beginning in the capture mode.</source>
        <translation>在捕获之前显示帮助信息。</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+4"/>
        <source>Show desktop notifications</source>
        <translation>显示桌面通知</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Show tray icon</source>
        <translation>显示托盘图标</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Show the systemtray icon</source>
        <translation>显示任务栏图标</translation>
    </message>
    <message>
        <location line="-85"/>
        <location line="+106"/>
        <source>Import</source>
        <translation>导入</translation>
    </message>
    <message>
        <location line="-99"/>
        <location line="+8"/>
        <location line="+22"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location line="-30"/>
        <source>Unable to read file.</source>
        <translation>无法读取文件。</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+22"/>
        <source>Unable to write file.</source>
        <translation>无法写入文件。</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>Save File</source>
        <translation>保存到文件</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Confirm Reset</source>
        <translation>确定重置</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to reset the configuration?</source>
        <translation>你确定你想要重置配置？</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Configuration File</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Export</source>
        <translation>导出</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Launch at startup</source>
        <translation>开机时启动</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Launch Kylin-screenshot</source>
        <translation>启动麒麟-截图</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Close after capture</source>
        <translation>捕获后关闭</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Close after taking a screenshot</source>
        <translation>获取屏幕截图后关闭</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Copy URL after upload</source>
        <translation>上传后复制URL</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copy URL and close window after upload</source>
        <translation>复制URL并在上传后关闭窗口</translation>
    </message>
</context>
<context>
    <name>ImgurUploader</name>
    <message>
        <location filename="../src/tools/imgur/imguruploader.cpp" line="+47"/>
        <source>Upload to Imgur</source>
        <translation>上传到Imgur</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Uploading Image</source>
        <translation>正在上传</translation>
    </message>
    <message>
        <location line="+86"/>
        <source>Copy URL</source>
        <translation>复制链接</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open URL</source>
        <translation>打开链接</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete image</source>
        <translation>删除图像</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Image to Clipboard.</source>
        <translation>保存文件到剪贴板。</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+13"/>
        <source>Unable to open the URL.</source>
        <translation>无法打开此链接。</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>URL copied to clipboard.</source>
        <translation>复制链接到剪贴板。</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Screenshot copied to clipboard.</source>
        <translation>截图复制到剪贴板。</translation>
    </message>
</context>
<context>
    <name>ImgurUploaderTool</name>
    <message>
        <location filename="../src/tools/imgur/imguruploadertool.cpp" line="+47"/>
        <source>Image Uploader</source>
        <translation>上传图片</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Upload the selection to Imgur</source>
        <translation>上传选择到 Imgur</translation>
    </message>
</context>
<context>
    <name>InfoWindow</name>
    <message>
        <source>About</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <source>SPACEBAR</source>
        <translation type="vanished">空格</translation>
    </message>
    <message>
        <source>Right Click</source>
        <translation type="vanished">右键</translation>
    </message>
    <message>
        <source>Mouse Wheel</source>
        <translation type="vanished">鼠标滑轮</translation>
    </message>
    <message>
        <source>Move selection 1px</source>
        <translation type="vanished">移动选择 1 px</translation>
    </message>
    <message>
        <source>Resize selection 1px</source>
        <translation type="vanished">调整选择大小 1 px</translation>
    </message>
    <message>
        <source>Quit capture</source>
        <translation type="vanished">退出捕获</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation type="vanished">复制到剪贴板</translation>
    </message>
    <message>
        <source>Save selection as a file</source>
        <translation type="vanished">将选择保存为文件</translation>
    </message>
    <message>
        <source>Undo the last modification</source>
        <translation type="vanished">撤消上次修改</translation>
    </message>
    <message>
        <source>Toggle visibility of sidebar with options of the selected tool</source>
        <translation type="vanished">切换侧边栏可见性</translation>
    </message>
    <message>
        <source>Show color picker</source>
        <translation type="vanished">显示颜色选择器</translation>
    </message>
    <message>
        <source>Change the tool&apos;s thickness</source>
        <translation type="vanished">改变工具的厚度</translation>
    </message>
    <message>
        <source>Capturn Full Screen</source>
        <translation type="vanished">全屏截图</translation>
    </message>
    <message>
        <source>Capture Top Screen</source>
        <translation type="vanished">截取窗口的截图</translation>
    </message>
    <message>
        <source>Capture Screen selection</source>
        <translation type="vanished">截取一个区域的截图</translation>
    </message>
    <message>
        <source>&lt;u&gt;&lt;b&gt;sysShortcuts&lt;/b&gt;&lt;/u&gt;</source>
        <translation type="vanished">&lt;u&gt;&lt;b&gt;系统快捷键&lt;/b&gt;&lt;/u&gt;</translation>
    </message>
    <message>
        <source>Available System shortcuts.</source>
        <translation type="vanished">可截图的系统快捷键</translation>
    </message>
    <message>
        <source>Key</source>
        <translation type="vanished">键</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">描述</translation>
    </message>
    <message>
        <source>&lt;u&gt;&lt;b&gt;License&lt;/b&gt;&lt;/u&gt;</source>
        <translation type="vanished">&lt;u&gt;&lt;b&gt;许可证&lt;/b&gt;&lt;/u&gt;</translation>
    </message>
    <message>
        <source>&lt;u&gt;&lt;b&gt;Version&lt;/b&gt;&lt;/u&gt;</source>
        <translation type="vanished">&lt;u&gt;&lt;b&gt;版本&lt;/b&gt;&lt;/u&gt;</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation type="vanished">截图</translation>
    </message>
    <message>
        <source>Kylin-ScreenShot</source>
        <translatorcomment>麒麟截图</translatorcomment>
        <translation type="vanished">麒麟截图</translation>
    </message>
    <message>
        <source>Compiled with Qt</source>
        <translatorcomment>Qt编译版本</translatorcomment>
        <translation type="vanished">Qt编译版本</translation>
    </message>
    <message>
        <source>&lt;u&gt;&lt;b&gt;Shortcuts&lt;/b&gt;&lt;/u&gt;</source>
        <translation type="vanished">&lt;u&gt;&lt;b&gt;快捷键&lt;/b&gt;&lt;/u&gt;</translation>
    </message>
    <message>
        <source>Available shortcuts in the screen capture mode.</source>
        <translation type="vanished">屏幕捕捉模式中的可用快捷键。</translation>
    </message>
</context>
<context>
    <name>LineTool</name>
    <message>
        <location filename="../src/tools/line/linetool.cpp" line="+54"/>
        <source>Line</source>
        <translation>直线</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Set the Line as the paint tool</source>
        <translation>将直线设置为绘画工具</translation>
    </message>
</context>
<context>
    <name>Logger</name>
    <message>
        <location filename="../src/record/AV/FastResampler.cpp" line="+127"/>
        <source>Error: Resample ratio is out of range!</source>
        <translation>错误：重新采样比率超出范围！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: Drift ratio is out of range!</source>
        <translation>错误：漂移比超出范围！</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Resample ratio is %1 (was %2).</source>
        <translation>重新采样比率为%1（以前为%2）。</translation>
    </message>
    <message>
        <location filename="../src/record/AV/FastScaler.cpp" line="+126"/>
        <source>Warning: Pixel format is not supported (%1 -&gt; %2), using swscale instead. This is not a problem, but performance will be worse.</source>
        <translation>警告：不支持像素格式（%1-&gt;%2），请改用swscale。这不是问题，但性能会更差。</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error: Can&apos;t get swscale context:</source>
        <comment>Don&apos;t translate &apos;swscale&apos;</comment>
        <translation>错误：无法获取swscale上下文：</translation>
    </message>
    <message>
        <location line="+22"/>
        <location line="+25"/>
        <location line="+25"/>
        <location line="+24"/>
        <location line="+22"/>
        <location line="+23"/>
        <source>Warning: Memory is not properly aligned for SSE, using fallback converter instead. This is not a problem, but performance will be worse.</source>
        <comment>Don&apos;t translate &apos;fallback&apos;</comment>
        <translation>警告： 内存没有为SSE正确对齐，使用回退转换器代替。这不是一个问题，但性能会变差。</translation>
    </message>
    <message>
        <location filename="../src/record/AV/input/PulseAudioInput.cpp" line="+34"/>
        <source>Error: pa_mainloop_prepare failed!</source>
        <comment>Don&apos;t translate &apos;pa_mainloop_prepare&apos;</comment>
        <translation>错误：pa_mainloopprepare失败！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: pa_mainloop_poll failed!</source>
        <comment>Don&apos;t translate &apos;pa_mainloop_poll&apos;</comment>
        <translation>错误：pa_mainloop_poll失败！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: pa_mainloop_dispatch failed!</source>
        <comment>Don&apos;t translate &apos;pa_mainloop_dispatch&apos;</comment>
        <translation>错误：pa_mainloop_dispatch失败！</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Error: Could not create main loop!</source>
        <translation>错误： 无法创建主循环!</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error: Could not create context!</source>
        <translation>错误： 无法创建上下文!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Could not connect! Reason: %1
It is possible that your system doesn&apos;t use PulseAudio. Try using the ALSA backend instead.</source>
        <translation>错误：无法连接！原因：%1
您的系统可能没有使用PulseAudio。请尝试使用ALSA后端。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: Could not connect! Reason: %1
It is possible that your system doesn&apos;t use PulseAudio.</source>
        <translation>错误： 无法连接! 原因是：%1
有可能是你的系统没有使用PulseAudio。</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Error: Connection attempt failed! Reason: %1</source>
        <translation>错误： 连接尝试失败! 原因是：%1</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Error: Could not create stream! Reason: %1</source>
        <translation>错误： 无法创建流! 原因是：%1</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error: Could not connect stream! Reason: %1</source>
        <translation>错误： 无法连接流! 原因是：%1</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error: Stream connection attempt failed! Reason: %1</source>
        <translation>错误： 流连接尝试失败! 原因是：%1</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Found source: [%1] %2</source>
        <translation>找到来源： [%1] %2</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Generating source list ...</source>
        <translation>正在生成源列表...</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error: Could not get names of sources! Reason: %1</source>
        <translation>错误： 无法获得来源的名称! 原因：%1</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Error: Could not get source info! Reason: %1</source>
        <translation>错误： 无法获得源信息! 原因：%1</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Stream is a monitor.</source>
        <translation>Stream是一个监视器。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stream is not a monitor.</source>
        <translation>Stream不是监视器。</translation>
    </message>
    <message>
        <location line="+31"/>
        <location filename="../src/record/AV/input/X11Input.cpp" line="+428"/>
        <source>Input thread started.</source>
        <translation>输入线程已启动。</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Error: pa_stream_peek failed!</source>
        <comment>Don&apos;t translate &apos;pa_stream_peek&apos;</comment>
        <translation>错误：pa_stream_eek失败！</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Warning: Audio source was suspended. The current segment will be stopped until the source is resumed.</source>
        <translation>警告：音频源已挂起。当前段将停止，直到恢复源。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Warning: Stream was moved to a different source.</source>
        <translation>警告： Stream被转移到一个不同的来源。</translation>
    </message>
    <message>
        <location line="+7"/>
        <location filename="../src/record/AV/input/X11Input.cpp" line="+113"/>
        <source>Input thread stopped.</source>
        <translation>输入线程停止。</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/record/AV/input/X11Input.cpp" line="+4"/>
        <source>Exception &apos;%1&apos; in input thread.</source>
        <translation>输入线程中出现异常&apos;%1&apos;。</translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../src/record/AV/input/X11Input.cpp" line="+3"/>
        <source>Unknown exception in input thread.</source>
        <translation>输入线程中的未知异常。</translation>
    </message>
    <message>
        <location filename="../src/record/AV/input/X11Input.cpp" line="-495"/>
        <source>Error: Unsupported X11 image pixel format!</source>
        <translation>错误： 不支持的X11图像像素格式!</translation>
    </message>
    <message>
        <location line="+124"/>
        <location filename="../src/record/AV/output/VideoEncoder.cpp" line="+96"/>
        <source>Error: Width or height is zero!</source>
        <translation>错误： 宽度或高度为零！</translation>
    </message>
    <message>
        <location line="+4"/>
        <location filename="../src/record/AV/output/VideoEncoder.cpp" line="+4"/>
        <source>Error: Width or height is too large, the maximum width and height is %1!</source>
        <translation>错误： 宽度或高度太大，最大宽度和高度是%1！</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Stopping input thread ...</source>
        <translation>停止输入线程...</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Error: Can&apos;t open X display!</source>
        <comment>Don&apos;t translate &apos;display&apos;</comment>
        <translation>错误：无法打开X显示器！</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Using X11 shared memory.</source>
        <translation>使用X11共享内存。</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Not using X11 shared memory.</source>
        <translation>没有使用X11共享内存。</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning: XFixes is not supported by X server, the cursor has been hidden.</source>
        <comment>Don&apos;t translate &apos;XFixes&apos;</comment>
        <translation>警告：X服务器不支持XFixes，光标已被隐藏。</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error: Can&apos;t create shared image!</source>
        <translation>错误：无法创建共享图像！</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Can&apos;t get shared memory!</source>
        <translation>错误： 无法获得共享内存!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Can&apos;t attach to shared memory!</source>
        <translation>错误： 不能连接到共享内存!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Can&apos;t attach server to shared memory!</source>
        <translation>错误： 不能将服务器连接到共享内存！</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Warning: Xinerama is not supported by X server, multi-monitor support may not work properly.</source>
        <comment>Don&apos;t translate &apos;Xinerama&apos;</comment>
        <translation>警告： X服务器不支持Xinerama，多显示器支持可能无法正常工作。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Warning: No monitors detected, multi-monitor support may not work properly.</source>
        <translation>警告： 没有检测到显示器，多显示器支持可能无法正常工作。</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Error: Invalid screen bounding box!</source>
        <translation>错误：屏幕边界框无效！</translation>
    </message>
    <message>
        <location line="+116"/>
        <source>Error: Can&apos;t get image (using shared memory)!
    Usually this means the recording area is not completely inside the screen. Or did you change the screen resolution?</source>
        <translation>错误：无法获取图像（使用共享内存）！
         通常这意味着记录区域不完全在屏幕内。或者你改变了屏幕分辨率？</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error: Can&apos;t get image (not using shared memory)!
    Usually this means the recording area is not completely inside the screen. Or did you change the screen resolution?</source>
        <translation>错误：无法获取图像（未使用共享内存）！
通常这意味着记录区域不完全在屏幕内。或者你改变了屏幕分辨率？</translation>
    </message>
    <message>
        <location filename="../src/record/AV/output/AudioEncoder.cpp" line="+98"/>
        <source>Error: Channel count is zero.</source>
        <translation>错误：通道计数为零。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: Sample rate is zero.</source>
        <translation>错误： 采样率为零。</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Using sample format %1.</source>
        <translation>使用样本格式%1。</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Error: Encoder requires an unsupported sample format!</source>
        <translation>错误：编码器需要不支持的样本格式！</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Error: Sending of audio frame failed!</source>
        <translation>错误： 发送音频帧失败！</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Error: Receiving of audio packet failed!</source>
        <translation>错误： 接收音频数据包失败！</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+22"/>
        <source>Error: Encoding of audio frame failed!</source>
        <translation>错误： 音频帧的编码失败！</translation>
    </message>
    <message>
        <location filename="../src/record/AV/output/BaseEncoder.cpp" line="+30"/>
        <location line="+9"/>
        <source>Error: Option &apos;%1&apos; could not be parsed!</source>
        <translation>错误： 选项&apos;%1&apos;不能被解析！</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Stopping encoder thread ...</source>
        <translation>停止编码器线程...</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Error: Can&apos;t open codec!</source>
        <translation>错误： 无法打开编解码器!</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Warning: Codec option &apos;%1&apos; was not recognised!</source>
        <translation>警告： 编解码器选项&apos;%1&apos;未被识别！</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Encoder thread started.</source>
        <translation>编码器主题开始。</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Flushing encoder ...</source>
        <translation>冲洗编码器...</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Encoder thread stopped.</source>
        <translation>编码器线程停止。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exception &apos;%1&apos; in encoder thread.</source>
        <translation>编码器线程中出现异常&apos;%1&apos;。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown exception in encoder thread.</source>
        <translation>编码器线程中的未知异常。</translation>
    </message>
    <message>
        <location filename="../src/record/AV/output/Muxer.cpp" line="+72"/>
        <source>Stopping encoders ...</source>
        <translation>停止编码器...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Waiting for muxer thread to stop ...</source>
        <translation>正在等待多路复用器线程停止...</translation>
    </message>
    <message>
        <location line="+23"/>
        <location line="+24"/>
        <source>Error: Can&apos;t copy parameters to stream!</source>
        <translation>错误： 不能将参数复制到流中！</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Error: Can&apos;t write header!</source>
        <comment>Don&apos;t translate &apos;header&apos;</comment>
        <translation>错误：无法写入标头！</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Finishing encoders ...</source>
        <translation>正在完成编码器...</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Error: Can&apos;t find chosen output format!</source>
        <translation>错误：找不到所选的输出格式！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Using format %1 (%2).</source>
        <translation>使用格式%1（%2）。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error: Can&apos;t allocate format context!</source>
        <translation>错误：无法分配格式上下文！</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error: Can&apos;t open output file!</source>
        <translation>错误：无法打开输出文件！</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Error: Can&apos;t write trailer, continuing anyway.</source>
        <comment>Don&apos;t translate &apos;trailer&apos;</comment>
        <translation>错误：无法写入预告片，仍在继续。</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Error: Can&apos;t find codec!</source>
        <translation>错误： 找不到编解码器！</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Using codec %1 (%2).</source>
        <translation>使用编解码器%1 (%2)。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Error: Can&apos;t create new stream!</source>
        <translation>错误：无法创建新流！</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error: Can&apos;t create new codec context!</source>
        <translation>错误：无法创建新的编解码器上下文！</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Error: Can&apos;t get codec context defaults!</source>
        <translation>错误：无法获取编解码器上下文默认值！</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Warning: This codec is considered experimental by libav/ffmpeg.</source>
        <translation>警告： 这个编解码器被libav/ffmpeg认为是实验性的。</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Muxer thread started.</source>
        <translation>Muxer线程已启动。</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Error: Can&apos;t write frame to muxer!</source>
        <translation>错误： 无法将帧写入多路复用器！</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Muxer thread stopped.</source>
        <translation>Muxer线程已停止。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exception &apos;%1&apos; in muxer thread.</source>
        <translation>多路复用器线程中出现异常“%1”。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown exception in muxer thread.</source>
        <translation>复用器线程中出现未知异常。</translation>
    </message>
    <message>
        <location filename="../src/record/AV/output/OutputManager.cpp" line="+69"/>
        <source>Stopping fragment thread ...</source>
        <translation>停止碎片线程...</translation>
    </message>
    <message>
        <location line="+278"/>
        <source>Fragment thread started.</source>
        <translation>碎片线开始。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Next fragment ...</source>
        <translation>下一个片段...</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Finishing ...</source>
        <translation>完成...</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Fragment thread stopped.</source>
        <translation>片段线程停止。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exception &apos;%1&apos; in fragment thread.</source>
        <translation>片段线程中出现异常&apos;%1&apos;。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown exception in fragment thread.</source>
        <translation>片段线程中的未知异常。</translation>
    </message>
    <message>
        <location filename="../src/record/AV/output/Synchronizer.cpp" line="+220"/>
        <source>Stopping synchronizer thread ...</source>
        <translation>停止同步器线程...</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Warning: Received video frame with non-monotonic timestamp.</source>
        <translation>警告：接收到具有非单调时间戳的视频帧。</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Warning: Video buffer overflow, some frames will be lost. The audio input seems to be too slow.</source>
        <translation>警告：视频缓冲区溢出，一些帧将丢失。音频输入似乎太慢。</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Warning: Received audio samples with non-monotonic timestamp.</source>
        <translation>警告：接收到具有非单调时间戳的音频样本。</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Warning: Too many audio samples, dropping samples to keep the audio in sync with the video.</source>
        <translation>警告： 太多的音频样本，丢掉样本以保持音频与视频同步。</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Warning: Not enough audio samples, inserting silence to keep the audio in sync with the video.</source>
        <translation>警告： 没有足够的音频样本，插入沉默以保持音频与视频的同步性。</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Warning: Audio input is more than 2% too slow!</source>
        <translation>警告：音频输入太慢超过2%！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Warning: Audio input is more than 2% too fast!</source>
        <translation>警告： 音频输入超过2%的速度!</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Warning: Audio buffer overflow, starting new segment to keep the audio in sync with the video (some video and/or audio may be lost). The video input seems to be too slow.</source>
        <translation>警告： 音频缓冲区溢出，开始新的片段以保持音频与视频同步（一些视频和/或音频可能会丢失）。视频输入似乎太慢了。</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Warning: Received hole in audio stream, inserting silence to keep the audio in sync with the video.</source>
        <translation>警告： 接收到音频流中的漏洞，插入沉默以保持音频与视频同步。</translation>
    </message>
    <message>
        <location line="+307"/>
        <source>Synchronizer thread started.</source>
        <translation>同步器线程已启动。</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Synchronizer thread stopped.</source>
        <translation>同步器线程已停止。</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Exception &apos;%1&apos; in synchronizer thread.</source>
        <translation>同步器线程中出现异常“%1”。</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown exception in synchronizer thread.</source>
        <translation>同步器线程中出现未知异常。</translation>
    </message>
    <message>
        <location filename="../src/record/AV/output/VideoEncoder.cpp" line="+4"/>
        <source>Error: Width or height is not an even number!</source>
        <translation>错误：宽度或高度不是偶数！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: Frame rate is zero!</source>
        <translation>错误：帧速率为零！</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Using pixel format %1.</source>
        <translation>使用像素格式%1。</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Error: The pixel format is not supported by the codec!</source>
        <translation>错误：编解码器不支持像素格式！</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Error: Sending of video frame failed!</source>
        <translation>错误： 发送视频帧失败!</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Error: Receiving of video packet failed!</source>
        <translation>错误： 接收视频数据包失败！</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+21"/>
        <source>Error: Encoding of video frame failed!</source>
        <translation>错误： 视频帧的编码失败！</translation>
    </message>
    <message>
        <location filename="../src/record/common/CPUFeatures.cpp" line="+42"/>
        <source>CPU features</source>
        <translation>CPU功能</translation>
    </message>
    <message>
        <location filename="../src/common/CommandLineOptions.cpp" line="+76"/>
        <source>Error: Command-line option &apos;%1&apos; requires a value!</source>
        <translation>错误：命令行选项“%1”需要一个值！</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error: Command-line option &apos;%1&apos; does not take a value!</source>
        <translation>错误：命令行选项“%1”没有取值！</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Error: Unknown command-line option &apos;%1&apos;!</source>
        <translation>错误：未知的命令行选项“%1”！</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error: Unknown command-line argument &apos;%1&apos;!</source>
        <translation>错误：未知的命令行参数“%1”！</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Error: Can&apos;t create .ssr directory!</source>
        <translation>错误：无法创建.ssr目录！</translation>
    </message>
</context>
<context>
    <name>LuPing</name>
    <message>
        <location filename="../src/tools/luping/luping.cpp" line="+47"/>
        <source>LuPing</source>
        <translation>录屏</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>luping</source>
        <translation>录屏</translation>
    </message>
</context>
<context>
    <name>MarkerTool</name>
    <message>
        <location filename="../src/tools/marker/markertool.cpp" line="+53"/>
        <source>Marker</source>
        <translation>标记</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Set the Marker as the paint tool</source>
        <translation>将标记设置为绘画工具</translation>
    </message>
</context>
<context>
    <name>MoveTool</name>
    <message>
        <location filename="../src/tools/move/movetool.cpp" line="+46"/>
        <source>Move</source>
        <translation>移动</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Move the selection area</source>
        <translation>移动选择区域</translation>
    </message>
</context>
<context>
    <name>MySaveDialog</name>
    <message>
        <source>Portable Network Graphic file (PNG) (*.png)</source>
        <translation type="vanished">PNG 格式 (*.png)</translation>
    </message>
    <message>
        <source>BMP file (*.bmp)</source>
        <translation type="vanished">BMP格式 （*.bmp）</translation>
    </message>
    <message>
        <source>JPEG file (*.jpg)</source>
        <translation type="vanished">JPEG 格式 （*.jpg）</translation>
    </message>
    <message>
        <location filename="../src/utils/mysavedialog.cpp" line="+33"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>OptionRecordTool</name>
    <message>
        <location filename="../src/tools/option_record/option_record_tool.cpp" line="+32"/>
        <source>OptionRecord</source>
        <translation>选项记录</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>options record tool</source>
        <translation>选项记录工具</translation>
    </message>
</context>
<context>
    <name>Options</name>
    <message>
        <location filename="../src/tools/options/options.cpp" line="+49"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>options tool</source>
        <translation>截图保存配置</translation>
    </message>
</context>
<context>
    <name>PencilTool</name>
    <message>
        <location filename="../src/tools/pencil/penciltool.cpp" line="+47"/>
        <source>Pencil</source>
        <translation>铅笔</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Set the Pencil as the paint tool</source>
        <translation>将铅笔设置为绘画工具</translation>
    </message>
</context>
<context>
    <name>PinTool</name>
    <message>
        <location filename="../src/tools/pin/pintool.cpp" line="+49"/>
        <source>Pin Tool</source>
        <translation>贴图工具</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Pin image on the desktop  exit with Esc</source>
        <translation>在桌面上固定图像  按Esc键退出</translation>
    </message>
    <message>
        <source>Pin image on the desktop</source>
        <translation type="vanished">在桌面上固定图像</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/utils/screenshotsaver.cpp" line="+138"/>
        <location filename="../src/widgets/capture/capturewidget.cpp" line="+1180"/>
        <source>Save Error</source>
        <translation>保存错误</translation>
    </message>
    <message>
        <location line="-75"/>
        <location line="+22"/>
        <location line="+37"/>
        <location filename="../src/widgets/capture/capturewidget.cpp" line="-6"/>
        <source>Capture saved as </source>
        <translation>捕获已保存为 </translation>
    </message>
    <message>
        <location line="-75"/>
        <source>Capture saved to clipboard</source>
        <translation>捕获已保存至剪贴板</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+21"/>
        <location line="+47"/>
        <location filename="../src/widgets/capture/capturewidget.cpp" line="+3"/>
        <source>Error trying to save as </source>
        <translation>尝试另存为时出错 </translation>
    </message>
    <message>
        <location line="-6"/>
        <source>file name can not contains &apos;/&apos;</source>
        <translatorcomment>保存文件名称中不能包含字符&apos;/&apos;</translatorcomment>
        <translation>保存文件名称中不能包含字符&apos;/&apos;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>can not save file as hide file</source>
        <translatorcomment>不能将文件保存为隐藏文件</translatorcomment>
        <translation>不能将文件保存为隐藏文件</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>can not save  because filename too long</source>
        <translation>由于文件名太长而无法保存</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="+97"/>
        <source>kylin-screenshot</source>
        <translatorcomment>麒麟截图</translatorcomment>
        <translation>麒麟截图</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+159"/>
        <location line="+163"/>
        <location line="+26"/>
        <location line="+31"/>
        <source>Unable to connect via DBus</source>
        <translation>无法通过DBus进行连接</translation>
    </message>
    <message>
        <location line="-363"/>
        <source>Kylin-Screenshot</source>
        <translatorcomment>麒麟截图</translatorcomment>
        <translation>麒麟截图</translation>
    </message>
    <message>
        <location filename="../src/tools/launcher/openwithprogram.cpp" line="+38"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to write in</source>
        <translation>无法写入</translation>
    </message>
    <message>
        <location filename="../src/tools/imgur/imguruploader.cpp" line="-99"/>
        <source>URL copied to clipboard.</source>
        <translatorcomment>复制链接到剪贴板。</translatorcomment>
        <translation>复制链接到剪贴板。</translation>
    </message>
</context>
<context>
    <name>QPlatformTheme</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>RectangleTool</name>
    <message>
        <location filename="../src/tools/rectangle/rectangletool.cpp" line="+51"/>
        <source>Rectangle</source>
        <translation>实心矩形</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Set the Rectangle as the paint tool</source>
        <translation>将实心矩形设置为绘画工具</translation>
    </message>
</context>
<context>
    <name>RedoTool</name>
    <message>
        <location filename="../src/tools/redo/redotool.cpp" line="+46"/>
        <source>Redo</source>
        <translation>重做</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Redo the next modification</source>
        <translation>重做上次修改</translation>
    </message>
</context>
<context>
    <name>SaveAsTool</name>
    <message>
        <location filename="../src/tools/save/saveastool.cpp" line="+51"/>
        <source>saveas</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Save the capture.....</source>
        <translatorcomment>另存为....</translatorcomment>
        <translation>另存为....</translation>
    </message>
</context>
<context>
    <name>SaveTool</name>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <location filename="../src/tools/save/savetool.cpp" line="+49"/>
        <source>save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Save the capture</source>
        <translation>保存捕获</translation>
    </message>
</context>
<context>
    <name>Save_Location</name>
    <message>
        <location filename="../src/widgets/capture/save_location.cpp" line="+71"/>
        <source>save location</source>
        <translatorcomment>存储位置</translatorcomment>
        <translation>存储位置</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>save type</source>
        <translatorcomment>存储格式</translatorcomment>
        <translation>存储格式</translation>
    </message>
</context>
<context>
    <name>Save_Location2</name>
    <message>
        <location filename="../src/widgets/capture/save_location2.cpp" line="+72"/>
        <source>save location</source>
        <translatorcomment>存储位置</translatorcomment>
        <translation>存储位置</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>save type</source>
        <translatorcomment>存储格式</translatorcomment>
        <translation>存储格式</translation>
    </message>
</context>
<context>
    <name>ScreenCap</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>ScreenCapExit</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>ScreenCapOption</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>ScreenCapStart</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>ScreenCut</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>ScreenGrabber</name>
    <message>
        <location filename="../src/utils/screengrabber.cpp" line="+91"/>
        <source>Unable to capture screen</source>
        <translation>无法捕获屏幕</translation>
    </message>
</context>
<context>
    <name>ScreenMouse</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>ScreenMouseCursor</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>ScreenVoice</name>
    <message>
        <source>options tool</source>
        <translation type="obsolete">截图保存配置</translation>
    </message>
</context>
<context>
    <name>SelectionTool</name>
    <message>
        <location filename="../src/tools/selection/selectiontool.cpp" line="+55"/>
        <source>Rectangular Selection</source>
        <translation>矩形选择</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Set the rectangle as the paint tool</source>
        <translation>将矩形选择设置为绘画工具</translation>
    </message>
    <message>
        <source>Set Selection as the paint tool</source>
        <translation type="vanished">将矩形选择设置为绘画工具</translation>
    </message>
</context>
<context>
    <name>ShortCutWidget</name>
    <message>
        <location filename="../src/widgets/shortcutwidget.cpp" line="+11"/>
        <source>Screenshot</source>
        <translation>截图</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>ShortCut</source>
        <translation>快捷键</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Keypress</source>
        <translation>按键</translation>
    </message>
    <message>
        <source>Keypr</source>
        <translation type="vanished">按键</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Mouse Wheel</source>
        <translation>鼠标滑轮</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Capturn Full Screen</source>
        <translation>全屏截图</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Capture Top Screen</source>
        <translation>截取窗口的截图</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Capture Screen selection</source>
        <translation>截取一个区域的截图</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Move selection 1px</source>
        <translation>移动选择 1 px</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resize selection 1px</source>
        <translation>调整选择大小 1 px</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit capture</source>
        <translation>退出捕获</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy to clipboard</source>
        <translation>复制到剪贴板</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save selection as a file</source>
        <translation>将选择保存为文件</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Undo the last modification</source>
        <translation>撤消上次修改</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change the tool&apos;s thickness</source>
        <translation>改变工具的厚度</translation>
    </message>
</context>
<context>
    <name>SidePanelWidget</name>
    <message>
        <location filename="../src/widgets/panel/sidepanelwidget.cpp" line="+67"/>
        <source>Active thickness:</source>
        <translation>当前宽度：</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Active color:</source>
        <translation>活动颜色：</translation>
    </message>
    <message>
        <location line="+116"/>
        <source>Press ESC to cancel</source>
        <translation>按下 ESC 键以取消</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Grab Color</source>
        <translation>获取颜色</translation>
    </message>
</context>
<context>
    <name>SizeIndicatorTool</name>
    <message>
        <location filename="../src/tools/sizeindicator/sizeindicatortool.cpp" line="+40"/>
        <source>Selection Size Indicator</source>
        <translation>选择尺寸指示</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Show the dimensions of the selection (X Y)</source>
        <translation>显示选择的尺寸 (X Y)</translation>
    </message>
</context>
<context>
    <name>StartRecordTool</name>
    <message>
        <location filename="../src/tools/start_record/start_record_tool.cpp" line="+28"/>
        <location line="+19"/>
        <source>StartRecord</source>
        <translation>开始记录</translation>
    </message>
</context>
<context>
    <name>StrftimeChooserWidget</name>
    <message>
        <location filename="../src/config/strftimechooserwidget.cpp" line="+47"/>
        <source>Century (00-99)</source>
        <translation>世纪（00-99）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Year (00-99)</source>
        <translation>年（00-99）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Year (2000)</source>
        <translation>年（2000）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Month Name (jan)</source>
        <translation>月（1月 - 12月）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Month Name (january)</source>
        <translation>月（一月 - 十二月）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Month (01-12)</source>
        <translation>月 (01-12)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Week Day (1-7)</source>
        <translation>周内的日（1-7）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Week (01-53)</source>
        <translation>周（01-53）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day Name (mon)</source>
        <translation>星期（一 - 七）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day Name (monday)</source>
        <translation>星期（星期一 - 星期日）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day (01-31)</source>
        <translation>天（01-31）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day of Month (1-31)</source>
        <translation>一月中的某天（1-31）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day (001-366)</source>
        <translation>天（001-366）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time (%H-%M-%S)</source>
        <translation>时间（%H-%M-%S）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time (%H-%M)</source>
        <translation>时间（%H-%M）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hour (00-23)</source>
        <translation>小时（00-23）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hour (01-12)</source>
        <translation>小时（01-12）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minute (00-59)</source>
        <translation>分钟（00-59）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Second (00-59)</source>
        <translation>秒（00-59）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Full Date (%m/%d/%y)</source>
        <translation>完整日期（%m/%d/%y）</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Full Date (%Y-%m-%d)</source>
        <translation>完整日期（%Y-%m-%d）</translation>
    </message>
</context>
<context>
    <name>SyncDiagram</name>
    <message>
        <location filename="../src/record/AV/output/Synchronizer.cpp" line="-675"/>
        <source>Video in</source>
        <translation>视频输入</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio in</source>
        <translation>音频输入</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video out</source>
        <translation>视频输出</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio out</source>
        <translation>音频输出</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <source>Kylin-ScreenShot Info</source>
        <translation type="vanished">麒麟截图 消息</translation>
    </message>
    <message>
        <source>kylin-screenshot</source>
        <translation type="vanished">麒麟截图</translation>
    </message>
    <message>
        <location filename="../src/utils/systemnotification.cpp" line="+50"/>
        <source>ScreenShot Info</source>
        <translation>截图 消息</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Screenshot</source>
        <translation>截图</translation>
    </message>
    <message>
        <source>screenshot</source>
        <translation type="vanished">截图</translation>
    </message>
</context>
<context>
    <name>TextConfig</name>
    <message>
        <location filename="../src/tools/text/textconfig.cpp" line="+48"/>
        <source>StrikeOut</source>
        <translation>删除线</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Underline</source>
        <translation>下划线</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Bold</source>
        <translation>粗体</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Italic</source>
        <translation>斜体</translation>
    </message>
</context>
<context>
    <name>TextTool</name>
    <message>
        <source>Text</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <location filename="../src/tools/text/texttool.cpp" line="+77"/>
        <source>text</source>
        <translation>文本</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Add text to your capture</source>
        <translation>在您的捕获中添加文本</translation>
    </message>
</context>
<context>
    <name>UIcolorEditor</name>
    <message>
        <location filename="../src/config/uicoloreditor.cpp" line="+30"/>
        <source>UI Color Editor</source>
        <translation>用户界面颜色编辑器</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Change the color moving the selectors and see the changes in the preview buttons.</source>
        <translation>移动颜色选择并在预览按钮查看。</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Select a Button to modify it</source>
        <translation>选择一个按钮以进行修改</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Main Color</source>
        <translation>主色</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Click on this button to set the edition mode of the main color.</source>
        <translation>点击按钮设置主色。</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Contrast Color</source>
        <translation>对比色</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Click on this button to set the edition mode of the contrast color.</source>
        <translation>点击按钮设置对比色。</translation>
    </message>
</context>
<context>
    <name>UndoTool</name>
    <message>
        <source>Undo</source>
        <translation type="vanished">撤消</translation>
    </message>
    <message>
        <location filename="../src/tools/undo/undotool.cpp" line="+52"/>
        <source>undo</source>
        <translation>撤消</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Undo the last modification</source>
        <translation>撤消上次修改</translation>
    </message>
</context>
<context>
    <name>VisualsEditor</name>
    <message>
        <location filename="../src/config/visualseditor.cpp" line="+52"/>
        <source>Opacity of area outside selection:</source>
        <translation>选中区域之外的不透明度：</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Button Selection</source>
        <translation>按钮选择</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
</context>
<context>
    <name>infoWidget</name>
    <message>
        <location filename="../src/widgets/infowidget.cpp" line="+38"/>
        <source>screenshot</source>
        <translation>截图</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>version:v1.0.0</source>
        <translation>版本:v1.0.0</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Screenshot is an easy to use application.that supports the basic screenshot function,but also provides the draw rectangle tool, draw a circle tool, blur, add annotations, add text and other functions</source>
        <translation>截图是一款易用的应用程序。该程序支持基础的截图功能之外，还提供了绘制矩形工具、绘制圆形工具、模糊、添加标注、添加文字等功能。</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>SUPPORT:%1</source>
        <translation>服务与支持团队:%1</translation>
    </message>
    <message>
        <source>Service and support teams:</source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
</context>
<context>
    <name>luping</name>
    <message>
        <source>luping</source>
        <translation type="vanished">录屏模式</translation>
    </message>
</context>
<context>
    <name>mypopup</name>
    <message>
        <location filename="../src/record/mypopup.cpp" line="+83"/>
        <location line="+3"/>
        <location line="+3"/>
        <location line="+3"/>
        <location line="+47"/>
        <source>%1 files</source>
        <comment>This appears in the file dialog, e.g. &apos;MP4 files&apos;</comment>
        <translation>%1文件</translation>
    </message>
    <message>
        <location line="-44"/>
        <location line="+7"/>
        <location line="+8"/>
        <source>Other...</source>
        <translation>其他...</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Uncompressed</source>
        <translation>未压缩</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Error: Could not find any suitable container in libavformat!</source>
        <translation>错误：找不到任何合适的libavformat容器！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: Could not find any suitable video codec in libavcodec!</source>
        <translation>错误：在libavcodec中找不到任何合适的视频编解码器！</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: Could not find any suitable audio codec in libavcodec!</source>
        <translation>错误：在libavcodec中找不到任何合适的音频编解码器！</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>All screens: %1x%2</source>
        <comment>This appears in the screen selection combobox</comment>
        <translation>所有屏幕：%1x%2</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Screen %1: %2x%3 at %4,%5</source>
        <comment>This appears in the screen selection combobox</comment>
        <translation>屏幕%1:%2x%3，位于%4，%5</translation>
    </message>
    <message>
        <location line="+217"/>
        <location line="+8"/>
        <source>not installed</source>
        <translation>未安装</translation>
    </message>
    <message>
        <location line="-6"/>
        <location line="+8"/>
        <source>not supported by container</source>
        <translation>容器不支持</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Screen %1</source>
        <comment>This appears in the screen labels</comment>
        <translation>屏幕%1</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Save recording as</source>
        <translation>将录音保存为</translation>
    </message>
    <message>
        <location filename="../src/record/mypopup.ui" line="+14"/>
        <source>Form</source>
        <translation>表格</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>store location</source>
        <translation>存储位置</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>container</source>
        <translation>容器</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>audio byte rate</source>
        <translation>音频字节率</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>video frate</source>
        <translation>视频</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>video res</source>
        <translation>视频分辨率</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>video codec</source>
        <translation>视频编解码器</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>audio codec</source>
        <translation>音讯编解码器</translation>
    </message>
</context>
<context>
    <name>ssrtools</name>
    <message>
        <location filename="../src/record/ssrtools.ui" line="+14"/>
        <source>Form</source>
        <translation>表格</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>VideoFrate</source>
        <translation>视频</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Width</source>
        <translation>宽度</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Left</source>
        <translation>左边</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Top</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Height</source>
        <translation>身高</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>select rectangle</source>
        <translation>选择矩形</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>select window</source>
        <translation>选择窗口</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Information</source>
        <translation>信息</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Total time:</source>
        <translation>总时间：</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+16"/>
        <source>TextLabel</source>
        <translation>文本标签</translation>
    </message>
    <message>
        <location line="-107"/>
        <source>FPS in:</source>
        <translation>FPS在：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>FPS out:</source>
        <translation>FPS输出：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Size in:</source>
        <translation>尺寸在：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Size out:</source>
        <translation>尺寸超出：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>File name:</source>
        <translation>文件名称：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>File size:</source>
        <translation>文件大小：</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Bit rate:</source>
        <translation>比特率：</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>fullscreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>fixed</source>
        <translation>固定的</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>cursor</source>
        <translation>光标</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>record cursor</source>
        <translation>记录光标</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>enable audio</source>
        <translation>启用音频</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>option</source>
        <translation>选项</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>start</source>
        <translation>开始</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../src/record/ssrtools.cpp" line="+320"/>
        <source>Starting page ...</source>
        <translation>起始页...</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Started page.</source>
        <translation>开始的一页。</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Stopping page ...</source>
        <translation>停止页面...</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Stopped page.</source>
        <translation>停止的页面。</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Starting output ...</source>
        <translation>开始输出...</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Started output.</source>
        <translation>开始输出。</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+71"/>
        <source>Error: Something went wrong during initialization.</source>
        <translation>错误：初始化过程中出现问题。</translation>
    </message>
    <message>
        <location line="-60"/>
        <source>Stopping output ...</source>
        <translation>停止输出...</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Stopped output.</source>
        <translation>停止了输出。</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Starting input ...</source>
        <translation>开始输入...</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Started input.</source>
        <translation>开始了输入。</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Stopping input ...</source>
        <translation>停止输入...</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Stopped input.</source>
        <translation>停止了输入。</translation>
    </message>
    <message>
        <location line="+587"/>
        <source>Encoding remaining data ...</source>
        <translation>正在编码剩余数据...</translation>
    </message>
    <message>
        <location line="+78"/>
        <source>Pause recording</source>
        <translation>暂停录音</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start recording</source>
        <translation>开始录音</translation>
    </message>
</context>
</TS>
