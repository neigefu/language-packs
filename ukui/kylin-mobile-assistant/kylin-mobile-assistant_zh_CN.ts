<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BaseDevice</name>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="409"/>
        <source>Control Devices Supported</source>
        <translation>已支持反控设备</translation>
    </message>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="413"/>
        <source>Control device not supported</source>
        <translation>未支持反控设备</translation>
    </message>
</context>
<context>
    <name>ConnectedTitleWin</name>
    <message>
        <location filename="../ui/view/connectedtitlewin.cpp" line="28"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedtitlewin.cpp" line="29"/>
        <location filename="../ui/view/connectedtitlewin.cpp" line="102"/>
        <source>Mobile Window</source>
        <translation>手机投屏</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedtitlewin.cpp" line="30"/>
        <location filename="../ui/view/connectedtitlewin.cpp" line="97"/>
        <source>Disconnect</source>
        <translation>断开连接</translation>
    </message>
</context>
<context>
    <name>CopyToComputerButtonWidget</name>
    <message>
        <location filename="../ui/storagelist/copytocomputerbuttonwidget.cpp" line="23"/>
        <source>Copy</source>
        <translation>复制到本机</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/copytocomputerbuttonwidget.cpp" line="46"/>
        <source>Choose folder</source>
        <translation>选择文件夹</translation>
    </message>
</context>
<context>
    <name>InitConnectWin</name>
    <message>
        <location filename="../ui/initconnectwin/initconnectwin.cpp" line="14"/>
        <source>ScanCode</source>
        <translation>扫码连接</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/initconnectwin.cpp" line="15"/>
        <source>USBConnect</source>
        <translation>usb连接</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/initconnectwin.cpp" line="22"/>
        <source>The function is under development, please look forward to it.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>InterfaceWin</name>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="26"/>
        <source>No network detected</source>
        <translation>未检测到网络</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="74"/>
        <source>Use the mobile app to scan this code</source>
        <translation>打开手机app扫描二维码</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="78"/>
        <source>Connect the mobile phone and computer to the same network,open the</source>
        <translation>将手机和电脑接入同一网络，打开</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="81"/>
        <source>mobile phone app</source>
        <translation>手机app</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="81"/>
        <source>and scan the QR code.</source>
        <translation>扫描此二维码</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="97"/>
        <source>Scan this code with your mobile browser</source>
        <translation>使用手机浏览器扫描此码</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="102"/>
        <source>Please scan this QR code with your mobile browser to download the app</source>
        <translation>请使用手机浏览器扫描二维码以下载app</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="257"/>
        <location filename="../ui/mainwindow.cpp" line="765"/>
        <source>mobile-assistant</source>
        <translation>手机助手</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="454"/>
        <source>Not currently connected, please connect</source>
        <translation>当前未连接，请连接</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="605"/>
        <source>Please install kylin-assistant on the Android terminal!</source>
        <translation>请在手机端下载麒麟手机助手app！</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="619"/>
        <source>Connection error</source>
        <translation>连接失败</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="748"/>
        <source>file download failed</source>
        <translation>文件下载失败</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="755"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="755"/>
        <source>Peony access can be error-prone</source>
        <translation>文件管理器访问可能出错</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="757"/>
        <source>Umount failed</source>
        <translation>卸载失败</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="765"/>
        <source>Version:</source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="767"/>
        <source>Mobile Assistant is an interconnection tool of Android device and Kirin operating system, which supports Android file synchronization, file transfer, screen mirroring and other functions, which is simple and fast to operate</source>
        <translation>手机助手是一款Android设备和麒麟操作系统的互通互联工具，支持Android文件同步、文件传输、屏幕镜像等功能，操作简单快捷。</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="845"/>
        <source>QQPicture</source>
        <translation>QQ图片</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="848"/>
        <source>QQVideo</source>
        <translation>QQ视频</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="851"/>
        <source>QQMusic</source>
        <translation>QQ音乐</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="854"/>
        <source>QQDocument</source>
        <translation>QQ文档</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="857"/>
        <source>WeChatPicture</source>
        <translation>微信图片</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="860"/>
        <source>WeChatVideo</source>
        <translation>微信视频</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="863"/>
        <source>WeChatMusic</source>
        <translation>微信音乐</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="866"/>
        <source>WeChatDocument</source>
        <translation>微信文档</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="869"/>
        <location filename="../ui/mainwindow.cpp" line="1028"/>
        <source>Picture</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="872"/>
        <location filename="../ui/mainwindow.cpp" line="1030"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="875"/>
        <location filename="../ui/mainwindow.cpp" line="1032"/>
        <source>Music</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="878"/>
        <location filename="../ui/mainwindow.cpp" line="1034"/>
        <source>Document</source>
        <translation>文档</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="881"/>
        <location filename="../ui/mainwindow.cpp" line="1036"/>
        <source>WeChat</source>
        <translation>微信</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="884"/>
        <location filename="../ui/mainwindow.cpp" line="1038"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1066"/>
        <location filename="../ui/mainwindow.cpp" line="1077"/>
        <source>Uploaded to</source>
        <translation>上传至</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1106"/>
        <source>Downloaded to</source>
        <translation>下载到</translation>
    </message>
</context>
<context>
    <name>MobileFileList</name>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="61"/>
        <source>Mobile file list</source>
        <translation>手机文件列表</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="69"/>
        <source>Mobile storage</source>
        <translation>移动存储</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="138"/>
        <source>Picture</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="142"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="146"/>
        <source>Music</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="150"/>
        <source>Document</source>
        <translation>文档</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="154"/>
        <source>WeChat</source>
        <translation>微信</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="158"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="169"/>
        <source>Mobile</source>
        <translation>手机</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="170"/>
        <source>All File</source>
        <translation>所有文件</translation>
    </message>
</context>
<context>
    <name>MobileFileListItem</name>
    <message>
        <location filename="../ui/view/mobilefilelistitem.cpp" line="64"/>
        <source>items</source>
        <translation>项</translation>
    </message>
</context>
<context>
    <name>MobileFileListView</name>
    <message>
        <location filename="../ui/classificationlist/mobilefilelistview.cpp" line="82"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
</context>
<context>
    <name>MobileFileSecondWidget</name>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="73"/>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="172"/>
        <source>Mobile file list</source>
        <translation>手机文件列表</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="75"/>
        <source>WeChat</source>
        <translation>微信</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="77"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="85"/>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="203"/>
        <source>List Mode</source>
        <translation>列表模式</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="93"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="196"/>
        <source>Icon Mode</source>
        <translation>图标模式</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="228"/>
        <source>Picture</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="233"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="238"/>
        <source>Music</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="243"/>
        <source>Document</source>
        <translation>文档</translation>
    </message>
</context>
<context>
    <name>MobileFileWidget</name>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="51"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="280"/>
        <source>Icon Mode</source>
        <translation>图标模式</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="55"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="149"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="286"/>
        <source>List Mode</source>
        <translation>列表模式</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="84"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="223"/>
        <source>Mobile file list</source>
        <translation>手机文件列表</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="89"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="92"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="95"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="98"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="128"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="132"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="89"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="101"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="113"/>
        <source>Picture</source>
        <translation>图片</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="92"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="104"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="116"/>
        <source>Video</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="95"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="107"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="119"/>
        <source>Music</source>
        <translation>音乐</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="98"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="110"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="122"/>
        <source>Document</source>
        <translation>文档</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="101"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="104"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="107"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="110"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="125"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="132"/>
        <source>WeChat</source>
        <translation>微信</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="157"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
</context>
<context>
    <name>MobileStorageListView</name>
    <message>
        <location filename="../ui/storagelist/mobilestoragelistview.cpp" line="76"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
</context>
<context>
    <name>MobileStorageViewWidget</name>
    <message>
        <location filename="../ui/storagelist/mobilestorageviewwidget.cpp" line="30"/>
        <source>No file</source>
        <translation>无文件</translation>
    </message>
</context>
<context>
    <name>MobileStorageWidget</name>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="45"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="231"/>
        <source>Icon Mode</source>
        <translation>图标模式</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="49"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="97"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="236"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="293"/>
        <source>List Mode</source>
        <translation>列表模式</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="77"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="176"/>
        <source>Mobile file list</source>
        <translation>手机文件列表</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="78"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="183"/>
        <source>Mobile storage</source>
        <translation>手机存储</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="105"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="301"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="280"/>
        <source>Search out %1 Results</source>
        <translation>搜索%1个结果</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="810"/>
        <source>Not connected</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="878"/>
        <source>Host %1 not found</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="881"/>
        <source>Connection refused to host %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="885"/>
        <source>Connection timed out to host %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="988"/>
        <source>Connected to host %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="1205"/>
        <source>Connection refused for data connection</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="1381"/>
        <source>Unknown error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2277"/>
        <source>Connecting to host failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2280"/>
        <source>Login failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2283"/>
        <source>Listing directory failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2286"/>
        <source>Changing directory failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2289"/>
        <source>Downloading file failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2292"/>
        <source>Uploading file failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2295"/>
        <source>Removing file failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2298"/>
        <source>Creating directory failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2301"/>
        <source>Removing directory failed:
%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2328"/>
        <source>Connection closed</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../filedb/searchdatabase.cpp" line="55"/>
        <source>Database Error</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>In order to ensure the normal operation of the program, please install the VLC program first!</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RotationChart</name>
    <message>
        <location filename="../ui/initconnectwin/rotationchart.cpp" line="92"/>
        <source>No tutorial</source>
        <translation>没有教程</translation>
    </message>
</context>
<context>
    <name>ScrollSettingWidget</name>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="22"/>
        <source>Mouse sensitivity</source>
        <translation>滚轮灵敏度</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="34"/>
        <source>slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="35"/>
        <source>quick</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="40"/>
        <source>ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="41"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>TimeLineView</name>
    <message>
        <location filename="../ui/classificationlist/timelineview.cpp" line="301"/>
        <source>%1/%2/%3</source>
        <translation>%1年%2月%3日</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/timelineview.cpp" line="678"/>
        <location filename="../ui/classificationlist/timelineview.cpp" line="701"/>
        <source>No file</source>
        <translation>无文件</translation>
    </message>
</context>
<context>
    <name>Titlebar</name>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="42"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="50"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="55"/>
        <source>mobile-assistant</source>
        <translation>手机助手</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="70"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="88"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="141"/>
        <location filename="../ui/view/titlebar.cpp" line="179"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="142"/>
        <location filename="../ui/view/titlebar.cpp" line="181"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="143"/>
        <location filename="../ui/view/titlebar.cpp" line="183"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TransmissionDialog</name>
    <message>
        <location filename="../ui/view/transmissiondialog.cpp" line="16"/>
        <source>Current progress</source>
        <translation>当前进度</translation>
    </message>
</context>
<context>
    <name>UsbConnWin</name>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.cpp" line="18"/>
        <source>Complete the connection tutorial</source>
        <translation>完成连接教程</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.cpp" line="19"/>
        <source>Mobile phone model</source>
        <translation>手机型号</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="30"/>
        <source>vivo</source>
        <translation>vivo</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="31"/>
        <source>HUAWEI</source>
        <translation>华为</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="32"/>
        <source>Xiaomi</source>
        <translation>小米</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="33"/>
        <source>SAMSUNG</source>
        <translation>三星</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="34"/>
        <source>OPPO</source>
        <translation>OPPO</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="35"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
</context>
<context>
    <name>VideoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.cpp" line="794"/>
        <source>Control device not supported</source>
        <translation>当前连接不支持控制设备</translation>
    </message>
</context>
<context>
    <name>VideoTitle</name>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="40"/>
        <source>mobile-assistant</source>
        <translation>手机助手</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="119"/>
        <location filename="../projection/uibase/videotitle.cpp" line="163"/>
        <source>Hide Navigation Button</source>
        <translation>隐藏导航栏</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="120"/>
        <source>Stay on top</source>
        <translation>置顶</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="121"/>
        <location filename="../projection/uibase/videotitle.cpp" line="187"/>
        <source>FullScreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="122"/>
        <source>Mouse sensitivity</source>
        <translation>滚轮灵敏度</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="123"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="160"/>
        <source>Show Navigation Button</source>
        <translation>显示导航栏</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="173"/>
        <source>Stay On Top</source>
        <translation>置顶</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="177"/>
        <source>Cancel Stay On Top</source>
        <translation>取消置顶</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="190"/>
        <source>Cancel FullScreen</source>
        <translation>取消全屏</translation>
    </message>
</context>
<context>
    <name>videoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.ui" line="17"/>
        <source>kylin-mobile-assistant</source>
        <translation>手机助手</translation>
    </message>
</context>
</TS>
