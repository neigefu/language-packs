<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>main</name>
    <message>
        <source>ukui-notification-daemon service</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>popupItemWidget</name>
    <message>
        <source>From </source>
        <translation>來自 </translation>
    </message>
    <message>
        <source> one notification</source>
        <translation> 的1條通知</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>also</source>
        <translation>還有</translation>
    </message>
    <message>
        <source>notice</source>
        <translation>條通知</translation>
    </message>
</context>
</TS>
