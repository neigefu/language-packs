<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>main</name>
    <message>
        <source>ukui-notification-daemon service</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>popupItemWidget</name>
    <message>
        <source>From </source>
        <translation>དེ་ནས་ཡོང་བ་ཡིན། </translation>
    </message>
    <message>
        <source> one notification</source>
        <translation> དོན་ཚན་1གི་བརྡ་ཐོ།</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation>གསལ་འདེབས།</translation>
    </message>
    <message>
        <source>also</source>
        <translation>ད་དུང་ཡོད།</translation>
    </message>
    <message>
        <source>notice</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
</context>
</TS>
