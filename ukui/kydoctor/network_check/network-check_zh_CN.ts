<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>DHCPCheck</name>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="31"/>
        <source>DHCP Config</source>
        <translation>DHCP服务</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="34"/>
        <source>DHCP service</source>
        <translation>DHCP服务</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="35"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="95"/>
        <source>Are DHCP config right?</source>
        <translation>DHCP配置是否正确？</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>Checking DHCP config</source>
        <translation>检查DHCP中</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>Checking</source>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="72"/>
        <source>DHCP RUNNING RIGHT</source>
        <translation>DHCP运行正确</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="72"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="82"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="77"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="77"/>
        <source>The DHCP assigned incorrect IP address</source>
        <translation>DHCP分配了错误的IP</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="82"/>
        <source>DHCP IS OFF, NO CHECK</source>
        <translation>DHCP已关闭，无需检测</translation>
    </message>
</context>
<context>
    <name>DNSCheck</name>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="29"/>
        <source>DNS Config</source>
        <translation>DNS服务</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="33"/>
        <source>DNS service</source>
        <translation>DNS服务</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="34"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="50"/>
        <source>Are DNS config right?</source>
        <translation>DNS配置是否正确</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <source>Checking DNS config</source>
        <translation>检查DNS配置中</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="117"/>
        <source>All DNS checks failed</source>
        <translation>所有DNS检测失败</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="69"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="117"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="69"/>
        <source>The DNS service is not working properly</source>
        <translation>DNS服务工作异常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="105"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="176"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="189"/>
        <source>DNS service is working properly</source>
        <translation>DNS服务工作正常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="105"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="176"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="189"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>HWCheck</name>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="32"/>
        <source>Hardware connection</source>
        <translation>硬件连接</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="33"/>
        <location filename="../HWCheck/hwcheck.cpp" line="100"/>
        <source>Are network card OK and cable connected?</source>
        <translation>网卡和网线是否连接正常？</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="34"/>
        <source>HardWare</source>
        <translation>网络硬件</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="67"/>
        <source>Checking NetWork HardWares</source>
        <translation>检查网络硬件中</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="67"/>
        <source>Checking</source>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>NetWork HardWares are OK,Primary Wired.</source>
        <translation>网络硬件正常，首选项为有线连接</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <location filename="../HWCheck/hwcheck.cpp" line="76"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="76"/>
        <source>NetWork HardWares are OK,Primary Wireless.</source>
        <translation>网络硬件正常，首选项为无线连接</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="82"/>
        <source>No available network is connected</source>
        <translation>未连接可用网络</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="86"/>
        <source>No hardware is available</source>
        <translation>无可用硬件设备</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="82"/>
        <location filename="../HWCheck/hwcheck.cpp" line="86"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
</context>
<context>
    <name>HostCheck</name>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="36"/>
        <source>Host File</source>
        <translation>Host文件</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="40"/>
        <location filename="../HostCheck/hostcheck.cpp" line="328"/>
        <source>Are Host File config right?</source>
        <translation>host文件配置是否正常</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="39"/>
        <source>host file</source>
        <translation>host文件</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="62"/>
        <source>No Host file
</source>
        <translation>没有 Host 文件
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="79"/>
        <source>The file has unwhitespace lines</source>
        <translation>文件中有未加空格的行</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="80"/>
        <source>The file has unwhitespace lines
</source>
        <translation>文件中有未加空格的行
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="89"/>
        <location filename="../HostCheck/hostcheck.cpp" line="107"/>
        <location filename="../HostCheck/hostcheck.cpp" line="216"/>
        <source>Ipv4 localhost error</source>
        <translation>Ipv4 localhost 错误</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="90"/>
        <location filename="../HostCheck/hostcheck.cpp" line="108"/>
        <location filename="../HostCheck/hostcheck.cpp" line="217"/>
        <source>Ipv4 localhost error
</source>
        <translation>Ipv4 localhost 错误
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="116"/>
        <source>Ipv6 localhost error</source>
        <translation>Ipv6 localhost 错误</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="117"/>
        <source>Ipv6 localhost error
</source>
        <translation>Ipv6 localhost 错误
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="125"/>
        <source>Ipv6 localnet error</source>
        <translation>Ipv6 localnet 错误</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="126"/>
        <source>Ipv6 localnet error
</source>
        <translation>Ipv6 localnet 错误
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="134"/>
        <source>Ipv6 mcastsprefix error</source>
        <translation>Ipv6 mcastsprefix 错误</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="135"/>
        <source>Ipv6 mcastsprefix error
</source>
        <translation>Ipv6 mcastsprefix 错误
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="143"/>
        <source>Ipv6 nodes error</source>
        <translation>Ipv6 nodes 错误</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="144"/>
        <source>Ipv6 nodes error
</source>
        <translation>Ipv6 nodes 错误
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="152"/>
        <source>Ipv6 routers error</source>
        <translation>Ipv6 routers 错误</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="153"/>
        <source>Ipv6 routers error
</source>
        <translation>Ipv6 routers 错误
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="180"/>
        <source>The Hosts mapping added by the user does not meet specifications</source>
        <translation>用户添加的Hosts映射不符合规范</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="181"/>
        <source>The Hosts mapping added by the user does not meet specifications
</source>
        <translation>用户添加的Hosts映射不符合规范
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="221"/>
        <source>Ipv4 localPChost error</source>
        <translation>Ipv4 localPChost 错误</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="222"/>
        <source>Ipv4 localPChost error
</source>
        <translation>Ipv4 localPChost 错误
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="302"/>
        <source>Checking Host Files</source>
        <translation>检查Host文件中</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="302"/>
        <source>Checking</source>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="307"/>
        <source>Hosts Files are OK</source>
        <translation>host文件正常</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="307"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="310"/>
        <location filename="../HostCheck/hostcheck.cpp" line="314"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="314"/>
        <source>The Hosts file is abnormal, please fix it</source>
        <translation>本机Hosts文件异常，请修复</translation>
    </message>
</context>
<context>
    <name>IPCheck</name>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="36"/>
        <source>IP address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="37"/>
        <location filename="../IPCheck/ipcheck.cpp" line="145"/>
        <source>Are IP config right?</source>
        <translation>IP配置是否正常</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="38"/>
        <source>IP Config</source>
        <translation>IP配置</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="113"/>
        <source>Checking IP config</source>
        <translation>IP配置检测中</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="113"/>
        <source>Checking</source>
        <translation>检测中</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="118"/>
        <source>DHCP ON</source>
        <translation>DHCP ON</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="118"/>
        <location filename="../IPCheck/ipcheck.cpp" line="125"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="129"/>
        <source>No wired Internet connection</source>
        <translation>没有有线网络连接</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="122"/>
        <location filename="../IPCheck/ipcheck.cpp" line="129"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="122"/>
        <source>The gateway and IP address are on different network segments</source>
        <translation>网关和IP地址不在同一网段</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="125"/>
        <source>IP CONFIG RIGHT</source>
        <translation>IP配置正常</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <location filename="../libNWDBus/src/kylin-dbus-interface.cpp" line="985"/>
        <source>Wired connection</source>
        <translation>Wired connection</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="597"/>
        <source>Hardware connection</source>
        <translation>硬件连接</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="610"/>
        <location filename="../appUI/src/mainwindow.cpp" line="774"/>
        <location filename="../appUI/src/mainwindow.cpp" line="927"/>
        <source>IP address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="616"/>
        <location filename="../appUI/src/mainwindow.cpp" line="782"/>
        <source>DHCP service</source>
        <translation>DHCP服务</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="622"/>
        <location filename="../appUI/src/mainwindow.cpp" line="790"/>
        <location filename="../appUI/src/mainwindow.cpp" line="940"/>
        <location filename="../appUI/src/mainwindow.cpp" line="965"/>
        <source>DNS service</source>
        <translation>DNS服务</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="628"/>
        <location filename="../appUI/src/mainwindow.cpp" line="757"/>
        <source>host file</source>
        <translation>host文件</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="634"/>
        <location filename="../appUI/src/mainwindow.cpp" line="916"/>
        <location filename="../appUI/src/mainwindow.cpp" line="953"/>
        <location filename="../appUI/src/mainwindow.cpp" line="979"/>
        <source>Intranet check</source>
        <translation>内网检查</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="640"/>
        <location filename="../appUI/src/mainwindow.cpp" line="916"/>
        <location filename="../appUI/src/mainwindow.cpp" line="953"/>
        <location filename="../appUI/src/mainwindow.cpp" line="979"/>
        <source>Can the computer access the Internet</source>
        <translation>电脑能否上网</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="666"/>
        <source>Network detection</source>
        <translation>网络检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="672"/>
        <source>Network hardware configuration</source>
        <translation>网络硬件配置</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="675"/>
        <location filename="../appUI/src/mainwindow.cpp" line="683"/>
        <location filename="../appUI/src/mainwindow.cpp" line="752"/>
        <location filename="../appUI/src/mainwindow.cpp" line="769"/>
        <location filename="../appUI/src/mainwindow.cpp" line="802"/>
        <location filename="../appUI/src/mainwindow.cpp" line="827"/>
        <location filename="../appUI/src/mainwindow.cpp" line="843"/>
        <location filename="../appUI/src/mainwindow.cpp" line="859"/>
        <location filename="../appUI/src/mainwindow.cpp" line="875"/>
        <location filename="../appUI/src/mainwindow.cpp" line="891"/>
        <source>%1 items were detected, %2 items were normal, and %3 items were abnormal</source>
        <translation>检测了 %1 项，%2 项正常，%3 项异常</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="687"/>
        <location filename="../appUI/src/mainwindow.cpp" line="749"/>
        <source>Network Settings</source>
        <translation>网络设置</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="693"/>
        <location filename="../appUI/src/mainwindow.cpp" line="766"/>
        <source>Network service</source>
        <translation>网络服务</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="699"/>
        <location filename="../appUI/src/mainwindow.cpp" line="799"/>
        <source>Network connectivity</source>
        <translation>网络连通性</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="706"/>
        <location filename="../appUI/src/mainwindow.cpp" line="714"/>
        <location filename="../appUI/src/mainwindow.cpp" line="722"/>
        <location filename="../appUI/src/mainwindow.cpp" line="822"/>
        <location filename="../appUI/src/mainwindow.cpp" line="838"/>
        <location filename="../appUI/src/mainwindow.cpp" line="854"/>
        <location filename="../appUI/src/mainwindow.cpp" line="932"/>
        <location filename="../appUI/src/mainwindow.cpp" line="945"/>
        <location filename="../appUI/src/mainwindow.cpp" line="956"/>
        <source>Wired network detection</source>
        <translation>有线网络检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="708"/>
        <location filename="../appUI/src/mainwindow.cpp" line="824"/>
        <source>IP address detection</source>
        <translation>IP地址检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="716"/>
        <location filename="../appUI/src/mainwindow.cpp" line="732"/>
        <location filename="../appUI/src/mainwindow.cpp" line="840"/>
        <location filename="../appUI/src/mainwindow.cpp" line="872"/>
        <source>DNS detection</source>
        <translation>DNS检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="724"/>
        <location filename="../appUI/src/mainwindow.cpp" line="740"/>
        <location filename="../appUI/src/mainwindow.cpp" line="856"/>
        <location filename="../appUI/src/mainwindow.cpp" line="888"/>
        <source>Whether to access the Intranet</source>
        <translation>能否访问内网</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="730"/>
        <location filename="../appUI/src/mainwindow.cpp" line="738"/>
        <location filename="../appUI/src/mainwindow.cpp" line="870"/>
        <location filename="../appUI/src/mainwindow.cpp" line="886"/>
        <location filename="../appUI/src/mainwindow.cpp" line="968"/>
        <location filename="../appUI/src/mainwindow.cpp" line="982"/>
        <source>Wireless network detection</source>
        <translation>无线网络检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="759"/>
        <source>The network hardware configuration fails to be detected. The network Settings cannot be detected</source>
        <translation>网络硬件配置检测失败，网络设置无法检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="776"/>
        <location filename="../appUI/src/mainwindow.cpp" line="784"/>
        <location filename="../appUI/src/mainwindow.cpp" line="792"/>
        <source>The network hardware configuration fails to be detected. The network service cannot be detected</source>
        <translation>网络硬件配置检测失败，网络服务无法检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="815"/>
        <source>The network hardware configuration fails to be detected, and the network connectivity cannot be detected</source>
        <translation>网络硬件配置检测失败，网络连通性无法检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="832"/>
        <source>The network hardware configuration fails to be detected, and the IP address cannot be detected</source>
        <translation>网络硬件配置检测失败，IP地址无法检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="848"/>
        <location filename="../appUI/src/mainwindow.cpp" line="880"/>
        <source>The network hardware configuration detection failed and DNS could not be detected</source>
        <translation>网络硬件配置检测失败，DNS无法检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="864"/>
        <location filename="../appUI/src/mainwindow.cpp" line="896"/>
        <source>The network hardware configuration fails to be checked. The access to the Intranet fails to be checked</source>
        <translation>网络硬件配置检测失败，能否访问内网无法检测</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1042"/>
        <source>InnerNet Check</source>
        <translation>InnerNet Check</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1043"/>
        <source>Check whether the intranet is smooth</source>
        <translation>Check whether the intranet is smooth</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1046"/>
        <source>Internet access</source>
        <translation>Internet access</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1047"/>
        <source>Can user browse out net?</source>
        <translation>Can user browse out net?</translation>
    </message>
</context>
<context>
    <name>NetCheck</name>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="63"/>
        <source>InnerNet Check</source>
        <translation>内网检查</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="65"/>
        <location filename="../NetCheck/netcheck.cpp" line="89"/>
        <location filename="../NetCheck/netcheck.cpp" line="91"/>
        <source>Intranet check</source>
        <translation>内网检查</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="66"/>
        <location filename="../NetCheck/netcheck.cpp" line="87"/>
        <source>Can user browse inner net?</source>
        <translation>用户能否浏览内网</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="68"/>
        <source>AccessNet Check</source>
        <translation>AccessNet Check</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="70"/>
        <location filename="../NetCheck/netcheck.cpp" line="95"/>
        <location filename="../NetCheck/netcheck.cpp" line="97"/>
        <source>Can the computer access the Internet</source>
        <translation>电脑能否上网</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="71"/>
        <location filename="../NetCheck/netcheck.cpp" line="93"/>
        <source>Can user browse out net?</source>
        <translation>Can user browse out net?</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="87"/>
        <location filename="../NetCheck/netcheck.cpp" line="93"/>
        <source>Checking</source>
        <translation>检测中</translation>
    </message>
</context>
<context>
    <name>NetCheckThread</name>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="313"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="316"/>
        <source>Extranet normal</source>
        <translation>Extranet normal</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="313"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="316"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="327"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="338"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="362"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="320"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="343"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="359"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="371"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="320"/>
        <source>Your computer cannot access the web page, there is a network exception</source>
        <translation>您的电脑无法访问网页,存在网络异常</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="327"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <source>Intranet normal</source>
        <translation>Intranet normal</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <source>Url can be accessed</source>
        <translation>Url can be accessed</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="338"/>
        <source>IP is reachable</source>
        <translation>IP is reachable</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="343"/>
        <source>No Intranet detection entries</source>
        <translation>没有内网检测条目</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <source>The specified site on the Intranet cannot be accessed</source>
        <translation>内网指定站点无法正常访问</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="359"/>
        <source>The Intranet IP address is unreachable</source>
        <translation>内网IP地址不可达</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="371"/>
        <source>The Intranet IP address is unreachable, and the specified site on the Intranet cannot be accessed</source>
        <translation>内网IP地址不可达，内网指定站点无法正常访问</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>IP is reachable，url cannot be accessed</source>
        <translation>IP is reachable, url cannot be accessed</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="362"/>
        <source>IP is unreachable，url can be accessed</source>
        <translation>IP is unreachable，url can be accessed</translation>
    </message>
</context>
<context>
    <name>ProxyCheck</name>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="38"/>
        <source>NetWork Proxy</source>
        <translation>NetWork Proxy</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="40"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="41"/>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Check whether the proxy is working?</source>
        <translation>Check whether the proxy is working?</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Checking</source>
        <translation>Checking</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="71"/>
        <source>proxy disable</source>
        <translation>proxy disable</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="74"/>
        <source>auto proxy normal</source>
        <translation>auto proxy normal</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="77"/>
        <source>auto proxy abnormal</source>
        <translation>auto proxy abnormal</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="80"/>
        <source>manual proxy normal</source>
        <translation>manual proxy normal</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="83"/>
        <source>Kylin NM</source>
        <translation>Kylin NM</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="85"/>
        <source>kylin network applet desktop message</source>
        <translation>kylin network applet desktop message</translation>
    </message>
</context>
</TS>
