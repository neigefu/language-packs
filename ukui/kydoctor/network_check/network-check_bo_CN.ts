<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>DHCPCheck</name>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="31"/>
        <source>DHCP Config</source>
        <translation>DHCPཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="34"/>
        <source>DHCP service</source>
        <translation>DHCPཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="35"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="95"/>
        <source>Are DHCP config right?</source>
        <translation>DHCPབཀོད་སྒྲིག་ཡང་དག་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>Checking DHCP config</source>
        <translation>DHCPལ་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="65"/>
        <source>Checking</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་རིང་།</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="72"/>
        <source>DHCP RUNNING RIGHT</source>
        <translation>DHCPཡི་འཁོར་སྐྱོད་ཡང་དག་པ།</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="72"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="82"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="77"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="77"/>
        <source>The DHCP assigned incorrect IP address</source>
        <translation>DHCPལ་ནོར་འཁྲུལ་གྱི་IPའགོར་ཡོད།</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="82"/>
        <source>DHCP IS OFF, NO CHECK</source>
        <translation>DHCPཡི་སྒོ་བརྒྱབ་ཟིན་པས་ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་མི་དགོས།</translation>
    </message>
</context>
<context>
    <name>DNSCheck</name>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="29"/>
        <source>DNS Config</source>
        <translation>DNSལ་ཞབས་འདེགས་ཞུ་བ།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="33"/>
        <source>DNS service</source>
        <translation>DNSལ་ཞབས་འདེགས་ཞུ་བ།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="34"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="50"/>
        <source>Are DNS config right?</source>
        <translation>DNSབཀོ་སྒྲིག་ཡང་དག་ཡིན་མིན།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <source>Checking DNS config</source>
        <translation>DNSབཀོ་སྒྲིག་ཁྲོད་ཞིབ་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་རིང་།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="117"/>
        <source>All DNS checks failed</source>
        <translation>DNSཞིབ་དཔྱད་ཚད་ལེན་ཚང་མ་ཕམ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="69"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="117"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="69"/>
        <source>The DNS service is not working properly</source>
        <translation>DNSཞིབ་ཞུའི་ལས་དོན་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="105"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="176"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="189"/>
        <source>DNS service is working properly</source>
        <translation>DNSཞིབ་ཞུའི་བྱ་བ་རྒྱུན་ལྡན་ཡིན།</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="105"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="176"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="189"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>HWCheck</name>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="32"/>
        <source>Hardware connection</source>
        <translation>མཁྲེགས་ཆས་སྦྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="33"/>
        <location filename="../HWCheck/hwcheck.cpp" line="100"/>
        <source>Are network card OK and cable connected?</source>
        <translation>དྲ་བྱང་དང་དྲ་རྒྱའི་སྐུད་པ་རྒྱུན་ལྡན་ལྟར་འབྲེལ་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="34"/>
        <source>HardWare</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས།</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="67"/>
        <source>Checking NetWork HardWares</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་ནང་ཞིབ་བཤེར་བྱ་</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="67"/>
        <source>Checking</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་རིང་།</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>NetWork HardWares are OK,Primary Wired.</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་རྒྱུན་ལྡན་ཡིན་པ་དང་། ཐོག་མར་བདམས་པ་ནི་སྐུད་ཡོད་སྦྲེལ་མཐུད་ཡིན</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <location filename="../HWCheck/hwcheck.cpp" line="76"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="76"/>
        <source>NetWork HardWares are OK,Primary Wireless.</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་རྒྱུན་ལྡན་ཡིན་པ་དང་། ཐོག་མར་སྐུད་མེད་སྦྲེལ་མཐུད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="82"/>
        <source>No available network is connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་ཆོག་པའི་དྲ་རྒྱ་མེད་</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="86"/>
        <source>No hardware is available</source>
        <translation>མཁྲེགས་ཆས་ཀྱི་སྒྲིག་ཆས་བཀོལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="82"/>
        <location filename="../HWCheck/hwcheck.cpp" line="86"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
</context>
<context>
    <name>HostCheck</name>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="36"/>
        <source>Host File</source>
        <translation>ཡིག་ཆHostཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="40"/>
        <location filename="../HostCheck/hostcheck.cpp" line="328"/>
        <source>Are Host File config right?</source>
        <translation>hostཡི་ཡིག་ཆ་བཀོད་སྒྲིག་རྒྱུན་ལྡན་ཡིན་མིན།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="39"/>
        <source>host file</source>
        <translation>ཡིག་ཆ།hostཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="62"/>
        <source>No Host file
</source>
        <translation>ཡིག་ཆ་Hostམེད།
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="79"/>
        <source>The file has unwhitespace lines</source>
        <translation>ཡིག་ཆའི་ནང་དུ་མཁའ་དབྱིངས་མེད་པའི་བྱ་སྤྱོད་ཡོད།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="80"/>
        <source>The file has unwhitespace lines
</source>
        <translation>ཡིག་ཆའི་ནང་དུ་མཁའ་དབྱིངས་མེད་པའི་བྱ་སྤྱོད་ཡོད།
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="89"/>
        <location filename="../HostCheck/hostcheck.cpp" line="107"/>
        <location filename="../HostCheck/hostcheck.cpp" line="216"/>
        <source>Ipv4 localhost error</source>
        <translation>Ipv4ཡིས་loaosst</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="90"/>
        <location filename="../HostCheck/hostcheck.cpp" line="108"/>
        <location filename="../HostCheck/hostcheck.cpp" line="217"/>
        <source>Ipv4 localhost error
</source>
        <translation>Ipv4ཡིས་loaosst
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="116"/>
        <source>Ipv6 localhost error</source>
        <translation>Ipv6ཡིས་loaosst</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="117"/>
        <source>Ipv6 localhost error
</source>
        <translation>Ipv6ཡིས་loaosst
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="125"/>
        <source>Ipv6 localnet error</source>
        <translation>Ipv61oaanett</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="126"/>
        <source>Ipv6 localnet error
</source>
        <translation>Ipv61oaanett
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="134"/>
        <source>Ipv6 mcastsprefix error</source>
        <translation>Ipv6asstsresist</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="135"/>
        <source>Ipv6 mcastsprefix error
</source>
        <translation>Ipv6asstsresist
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="143"/>
        <source>Ipv6 nodes error</source>
        <translation>Ipv6ndsཡི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="144"/>
        <source>Ipv6 nodes error
</source>
        <translation>Ipv6ndsཡི་ནོར་འཁྲུལ།
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="152"/>
        <source>Ipv6 routers error</source>
        <translation>Ipv6rressཡི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="153"/>
        <source>Ipv6 routers error
</source>
        <translation>Ipv6rressཡི་ནོར་འཁྲུལ།
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="180"/>
        <source>The Hosts mapping added by the user does not meet specifications</source>
        <translation>སྤྱོད་མཁན་གྱིས་ཁ་སྣོན་བྱས་པའི་Hossཡི་མདའ་དེ་ཚད་ལྡན་དང་མི་མཐུན་པ།།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="181"/>
        <source>The Hosts mapping added by the user does not meet specifications
</source>
        <translation>སྤྱོད་མཁན་གྱིས་ཁ་སྣོན་བྱས་པའི་Hossཡི་མདའ་དེ་ཚད་ལྡན་དང་མི་མཐུན་པ།།
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="221"/>
        <source>Ipv4 localPChost error</source>
        <translation>Ipv4ཡིས་loaapsst</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="222"/>
        <source>Ipv4 localPChost error
</source>
        <translation>Ipv4ཡིས་loaapsst
</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="302"/>
        <source>Checking Host Files</source>
        <translation>Hostཡི་ཡིག་ཆའི་ནང་དུ་ཞིབ་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="302"/>
        <source>Checking</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་རིང་།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="307"/>
        <source>Hosts Files are OK</source>
        <translation>hostཡིག་ཆ་རྒྱུན་ལྡན་རེད།</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="307"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="310"/>
        <location filename="../HostCheck/hostcheck.cpp" line="314"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="314"/>
        <source>The Hosts file is abnormal, please fix it</source>
        <translation>འཕྲུལ་ཆས་འདི་Hostsཡིག་ཆ་རྒྱུན་ལྡན་མིན་པས་ཉམས་གསོ་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>IPCheck</name>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="36"/>
        <source>IP address</source>
        <translation>IPཡི་སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="37"/>
        <location filename="../IPCheck/ipcheck.cpp" line="145"/>
        <source>Are IP config right?</source>
        <translation>IPབཀོ་སྒྲིག་རྒྱུན་ལྡན་ཡིན་མིན།</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="38"/>
        <source>IP Config</source>
        <translation>IPབཀོ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="113"/>
        <source>Checking IP config</source>
        <translation>IPབཀོད་སྒྲིག་ཞིབ་དཔྱད་ཚད་ལེན་ཁྲོད།</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="113"/>
        <source>Checking</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་རིང་།</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="118"/>
        <source>DHCP ON</source>
        <translation>DHCP ON</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="118"/>
        <location filename="../IPCheck/ipcheck.cpp" line="125"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="129"/>
        <source>No wired Internet connection</source>
        <translation>སྐུད་ཡོད་དྲ་བ་སྦྲེལ་མཐུད་བྱས་མེད།</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="122"/>
        <location filename="../IPCheck/ipcheck.cpp" line="129"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="122"/>
        <source>The gateway and IP address are on different network segments</source>
        <translation>དྲ་རྒྱའི་འགག་སྒོ་དང་IPས་གནས་དྲ་རྒྱའི་དུམ་བུ་གཅིག་ཏུ་མེད།</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="125"/>
        <source>IP CONFIG RIGHT</source>
        <translation>IPབཀོ་སྒྲིག་རྒྱུན་ལྡན་ཡིན།</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <location filename="../libNWDBus/src/kylin-dbus-interface.cpp" line="985"/>
        <source>Wired connection</source>
        <translation>Wired connection</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="597"/>
        <source>Hardware connection</source>
        <translation>མཁྲེགས་ཆས་སྦྲེལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="610"/>
        <location filename="../appUI/src/mainwindow.cpp" line="774"/>
        <location filename="../appUI/src/mainwindow.cpp" line="927"/>
        <source>IP address</source>
        <translation>IPཡི་སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="616"/>
        <location filename="../appUI/src/mainwindow.cpp" line="782"/>
        <source>DHCP service</source>
        <translation>DHCPཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="622"/>
        <location filename="../appUI/src/mainwindow.cpp" line="790"/>
        <location filename="../appUI/src/mainwindow.cpp" line="940"/>
        <location filename="../appUI/src/mainwindow.cpp" line="965"/>
        <source>DNS service</source>
        <translation>DNSལ་ཞབས་འདེགས་ཞུ་བ།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="628"/>
        <location filename="../appUI/src/mainwindow.cpp" line="757"/>
        <source>host file</source>
        <translation>ཡིག་ཆ།hostཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="634"/>
        <location filename="../appUI/src/mainwindow.cpp" line="916"/>
        <location filename="../appUI/src/mainwindow.cpp" line="953"/>
        <location filename="../appUI/src/mainwindow.cpp" line="979"/>
        <source>Intranet check</source>
        <translation>ནང་ལོགས་ཀྱི་དྲ་བར་ཞིབ་བཤེར</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="640"/>
        <location filename="../appUI/src/mainwindow.cpp" line="916"/>
        <location filename="../appUI/src/mainwindow.cpp" line="953"/>
        <location filename="../appUI/src/mainwindow.cpp" line="979"/>
        <source>Can the computer access the Internet</source>
        <translation>གློག་ཀླད་དྲ་འཇུག་བྱེད་ཐུབ་མིན།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="666"/>
        <source>Network detection</source>
        <translation>དྲ་རྒྱའི་ཞིབ་དཔྱད་ཚད་ལེན།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="672"/>
        <source>Network hardware configuration</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="675"/>
        <location filename="../appUI/src/mainwindow.cpp" line="683"/>
        <location filename="../appUI/src/mainwindow.cpp" line="752"/>
        <location filename="../appUI/src/mainwindow.cpp" line="769"/>
        <location filename="../appUI/src/mainwindow.cpp" line="802"/>
        <location filename="../appUI/src/mainwindow.cpp" line="827"/>
        <location filename="../appUI/src/mainwindow.cpp" line="843"/>
        <location filename="../appUI/src/mainwindow.cpp" line="859"/>
        <location filename="../appUI/src/mainwindow.cpp" line="875"/>
        <location filename="../appUI/src/mainwindow.cpp" line="891"/>
        <source>%1 items were detected, %2 items were normal, and %3 items were abnormal</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་རྣམ་གྲངས་1%དང་། 2%རྒྱུན་ལྡན་མིན་པ། བརྒྱ་ཆ་3བཅས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="687"/>
        <location filename="../appUI/src/mainwindow.cpp" line="749"/>
        <source>Network Settings</source>
        <translation>དྲ་རྒྱའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="693"/>
        <location filename="../appUI/src/mainwindow.cpp" line="766"/>
        <source>Network service</source>
        <translation>དྲ་རྒྱའི་ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="699"/>
        <location filename="../appUI/src/mainwindow.cpp" line="799"/>
        <source>Network connectivity</source>
        <translation>དྲ་རྒྱའི་སྦྲེལ་མཐུད་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="706"/>
        <location filename="../appUI/src/mainwindow.cpp" line="714"/>
        <location filename="../appUI/src/mainwindow.cpp" line="722"/>
        <location filename="../appUI/src/mainwindow.cpp" line="822"/>
        <location filename="../appUI/src/mainwindow.cpp" line="838"/>
        <location filename="../appUI/src/mainwindow.cpp" line="854"/>
        <location filename="../appUI/src/mainwindow.cpp" line="932"/>
        <location filename="../appUI/src/mainwindow.cpp" line="945"/>
        <location filename="../appUI/src/mainwindow.cpp" line="956"/>
        <source>Wired network detection</source>
        <translation>སྐུད་ཡོད་དྲ་རྒྱར་ཞིབ་དཔྱད་ཚད་ལེན་བྱ་</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="708"/>
        <location filename="../appUI/src/mainwindow.cpp" line="824"/>
        <source>IP address detection</source>
        <translation>IPཡི་སྡོད་གནས་ལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="716"/>
        <location filename="../appUI/src/mainwindow.cpp" line="732"/>
        <location filename="../appUI/src/mainwindow.cpp" line="840"/>
        <location filename="../appUI/src/mainwindow.cpp" line="872"/>
        <source>DNS detection</source>
        <translation>DNSལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="724"/>
        <location filename="../appUI/src/mainwindow.cpp" line="740"/>
        <location filename="../appUI/src/mainwindow.cpp" line="856"/>
        <location filename="../appUI/src/mainwindow.cpp" line="888"/>
        <source>Whether to access the Intranet</source>
        <translation>ནང་ལོགས་ཀྱི་དྲ་བར་འཚམས་འདྲི་བྱེད་ཐུབ་མིན།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="730"/>
        <location filename="../appUI/src/mainwindow.cpp" line="738"/>
        <location filename="../appUI/src/mainwindow.cpp" line="870"/>
        <location filename="../appUI/src/mainwindow.cpp" line="886"/>
        <location filename="../appUI/src/mainwindow.cpp" line="968"/>
        <location filename="../appUI/src/mainwindow.cpp" line="982"/>
        <source>Wireless network detection</source>
        <translation>སྐུད་མེད་དྲ་རྒྱའི་ཞིབ་དཔྱད་ཚད་ལེན།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="759"/>
        <source>The network hardware configuration fails to be detected. The network Settings cannot be detected</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་བཀོད་སྒྲིག་ལ་ཞིབ་དཔྱད་ཚད་ལེན་ཕམ་པ་དང་། དྲ་རྒྱའི་སྒྲིག་གཞི་ལ་ཞིབ་དཔྱད་ཚད་ལེན</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="776"/>
        <location filename="../appUI/src/mainwindow.cpp" line="784"/>
        <location filename="../appUI/src/mainwindow.cpp" line="792"/>
        <source>The network hardware configuration fails to be detected. The network service cannot be detected</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་བཀོད་སྒྲིག་ལ་ཞིབ་དཔྱད་ཚད་ལེན་ཕམ་པས་དྲ་རྒྱའི་ཞབས་ཞུ་ལ་ཞིབ་དཔྱད་ཚད་ལེན</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="815"/>
        <source>The network hardware configuration fails to be detected, and the network connectivity cannot be detected</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་བཀོད་སྒྲིག་ལ་ཞིབ་དཔྱད་ཚད་ལེན་ཕམ་པ་དང་། དྲ་རྒྱའི་སྦྲེལ་མཐུད་རང་བཞིན་ལ་ཞིབ་དཔྱད་ཚད་</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="832"/>
        <source>The network hardware configuration fails to be detected, and the IP address cannot be detected</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་བཀོད་སྒྲིག་ལ་ཞིབ་དཔྱད་ཚད་ལེན་ཕམ་པ་དང་། IPས་གནས་ལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་ཐབས་མེད།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="848"/>
        <location filename="../appUI/src/mainwindow.cpp" line="880"/>
        <source>The network hardware configuration detection failed and DNS could not be detected</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་བཀོད་སྒྲིག་ལ་ཞིབ་དཔྱད་ཚད་ལེན་ཕམ་པ་དང་། DNSལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་ཐབས་མེད།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="864"/>
        <location filename="../appUI/src/mainwindow.cpp" line="896"/>
        <source>The network hardware configuration fails to be checked. The access to the Intranet fails to be checked</source>
        <translation>དྲ་རྒྱའི་མཁྲེགས་ཆས་བཀོད་སྒྲིག་ལ་ཞིབ་དཔྱད་ཚད་ལེན་ཕམ་ཉེས་བྱུང་བས་ ནང་ཁུལ་གྱི་དྲ་རྒྱར་ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་ཐབས་མེད།</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1042"/>
        <source>InnerNet Check</source>
        <translation>InnerNet Check</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1043"/>
        <source>Check whether the intranet is smooth</source>
        <translation>Check whether the intranet is smooth</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1046"/>
        <source>Internet access</source>
        <translation>Internet access</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="1047"/>
        <source>Can user browse out net?</source>
        <translation>Can user browse out net?</translation>
    </message>
</context>
<context>
    <name>NetCheck</name>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="63"/>
        <source>InnerNet Check</source>
        <translation>ནང་ལོགས་ཀྱི་དྲ་བར་ཞིབ་བཤེར</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="65"/>
        <location filename="../NetCheck/netcheck.cpp" line="89"/>
        <location filename="../NetCheck/netcheck.cpp" line="91"/>
        <source>Intranet check</source>
        <translation>ནང་ལོགས་ཀྱི་དྲ་བར་ཞིབ་བཤེར</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="66"/>
        <location filename="../NetCheck/netcheck.cpp" line="87"/>
        <source>Can user browse inner net?</source>
        <translation>སྤྱོད་མཁན་གྱིས་ནང་དྲར་བལྟས་ན་ཆོག་གམ།</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="68"/>
        <source>AccessNet Check</source>
        <translation>AccessNet Check</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="70"/>
        <location filename="../NetCheck/netcheck.cpp" line="95"/>
        <location filename="../NetCheck/netcheck.cpp" line="97"/>
        <source>Can the computer access the Internet</source>
        <translation>གློག་ཀླད་དྲ་འཇུག་བྱེད་ཐུབ་མིན།</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="71"/>
        <location filename="../NetCheck/netcheck.cpp" line="93"/>
        <source>Can user browse out net?</source>
        <translation>Can user browse out net?</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="87"/>
        <location filename="../NetCheck/netcheck.cpp" line="93"/>
        <source>Checking</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱེད་རིང་།</translation>
    </message>
</context>
<context>
    <name>NetCheckThread</name>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="313"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="316"/>
        <source>Extranet normal</source>
        <translation>Extranet normal</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="313"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="316"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="327"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="338"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="362"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="320"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="343"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="359"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="371"/>
        <source>ERR</source>
        <translation>ERR</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="320"/>
        <source>Your computer cannot access the web page, there is a network exception</source>
        <translation>ཁྱེད་རང་གི་གློག་ཀླད་ཀྱིས་དྲ་ངོས་ལ་འཚམས་འདྲི་བྱེད་ཐབས་བྲལ་བ་དང་དྲ་རྒྱའི་རྒྱུན་ལྡན་མིན་པ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="327"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <source>Intranet normal</source>
        <translation>Intranet normal</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <source>Url can be accessed</source>
        <translation>Url can be accessed</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="338"/>
        <source>IP is reachable</source>
        <translation>IP is reachable</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="343"/>
        <source>No Intranet detection entries</source>
        <translation>ནང་གི་དྲ་རྒྱའི་ཞིབ་དཔྱད་ཚད་ལེན་གྱི་དོན་ཚན་མེད།</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <source>The specified site on the Intranet cannot be accessed</source>
        <translation>དྲ་རྒྱའི་དམིགས་འཛུགསས་ཚིགས་སུ་རྒྱུན་ལྡན་གྱི་འཚམས་འདྲི་བྱེད་ཐབས་མེད།</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="359"/>
        <source>The Intranet IP address is unreachable</source>
        <translation>ནང་དྲའི་IPས་གནས་སུ་སླེབས་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="371"/>
        <source>The Intranet IP address is unreachable, and the specified site on the Intranet cannot be accessed</source>
        <translation>ནང་དྲའི་IPས་གནས་སུ་སླེབས་མི་ཐུབ་པ་དང་། ནང་གི་དྲ་རྒྱའི་དམིགས་འཛུགསས་ཚིགས་ཀྱིས་རྒྱུན་ལྡན་གྱི་འཚམས་འདྲི་བྱེད་ཐབས་མེད།</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>IP is reachable，url cannot be accessed</source>
        <translation>IP is reachable, url cannot be accessed</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="362"/>
        <source>IP is unreachable，url can be accessed</source>
        <translation>IP is unreachable，url can be accessed</translation>
    </message>
</context>
<context>
    <name>ProxyCheck</name>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="38"/>
        <source>NetWork Proxy</source>
        <translation>NetWork Proxy</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="40"/>
        <source>Proxy</source>
        <translation>Proxy</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="41"/>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Check whether the proxy is working?</source>
        <translation>Check whether the proxy is working?</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Checking</source>
        <translation>Checking</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="71"/>
        <source>proxy disable</source>
        <translation>proxy disable</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="74"/>
        <source>auto proxy normal</source>
        <translation>auto proxy normal</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="77"/>
        <source>auto proxy abnormal</source>
        <translation>auto proxy abnormal</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="80"/>
        <source>manual proxy normal</source>
        <translation>manual proxy normal</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="83"/>
        <source>Kylin NM</source>
        <translation>Kylin NM</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="85"/>
        <source>kylin network applet desktop message</source>
        <translation>kylin network applet desktop message</translation>
    </message>
</context>
</TS>
