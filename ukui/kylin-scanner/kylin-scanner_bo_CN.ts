<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">བཤེར་ཆས་ནི་འབྲེལ་མཐུད་ལ་ཕན་པའི་བཤེར་ཆས་ཤིག་ཡིན་ཞིང་། དེ་ཡང་ཐེངས་གཅིག་རང་བཞིན་གྱི་བཟོ་བཅོས་དང་། རིག་ནུས་ཡོ་བསྲང་། ཡི་གེ་ངོས་འཛིན་བཅས་ཀྱི་ཡོ་བྱད་ཅིག་ཏུ་བརྩིས་ཆོག</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">ཞབས་ཞུ ། རྒྱབ་སྐྱོར། </translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">བཤེར་ཆས།</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting</source>
        <translation>ཞིབ་བཤེར་ཡོ་བྱད་ལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་ནས་སྒུག་རོགས།</translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation>ཞིབ་བཤེར་ཡོ་བྱད་ལ་ཞིབ་དཔྱད་ཚད་ལེན་མ་བྱས་ན། སྔོན་ལ་བརྟག་དཔྱད་ཡོ་ཆས་ལ་འབྲེལ་མཐུད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ</translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <source>Unable to read text</source>
        <translation>ཡི་གེ་ཀློག་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <source>About</source>
        <translation type="vanished">འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>གླེང་སྒྲོམ།</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཁག</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">བཤར་འབེབས་ཆས།</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">པར་གཞི།：</translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">བཤར་ཕབ་ནི་སྤྱིར་བཏང་གི་བཤར་ཕབ་དང་།མཐེབ་གཞོང་གཅིག་མཛེས་བཟོ།རིག་ལྡན་ཡོ་བསྲང་དང་ཡི་གེ་དབྱེ་འབྱེད་སོགས་ཀྱི་ནུས་པ་འདོན་སྤྲོད་བྱེད་པའི་ངོས་ཀྱི་མཛའ་མཐུན་བཤར་འབེབས་མཉེན་ཆས་ཤིག་ཡིན།</translation>
    </message>
</context>
<context>
    <name>KYCEditBarWidget</name>
    <message>
        <source>symmetry</source>
        <translation type="vanished">ཆ་འགྲིག་མིན་པའི་ཚད།:</translation>
    </message>
    <message>
        <source>rotate</source>
        <translation type="vanished">འཁོར་སྒྱུར།</translation>
    </message>
    <message>
        <source>tailor</source>
        <translation type="vanished">དྲས་གཏུབ།</translation>
    </message>
    <message>
        <source>watermark</source>
        <translation type="vanished">མིང་རྟགས།</translation>
    </message>
</context>
<context>
    <name>KYCFunctionBarWidget</name>
    <message>
        <source>Scan</source>
        <translation type="vanished">བཤར་འབེབས།</translation>
    </message>
    <message>
        <source>scan</source>
        <translation type="vanished">མིག་བཤར།…</translation>
    </message>
    <message>
        <source>Normal scanning</source>
        <translation type="vanished">སྤྱིར་བཏང་བཤར་ཕབ།</translation>
    </message>
    <message>
        <source>Beauty</source>
        <translation type="vanished">མཐེབ་གཞོང་གཅིག་གིས་མཛེས་བཟོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Rectify</source>
        <translation type="vanished">རིག་ལྡན་ཡོ་བསྲང་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <source>Text OCR</source>
        <translation type="vanished">ཡི་གེ་ངོས་འཛིན།</translation>
    </message>
</context>
<context>
    <name>KYCNoMailDialog</name>
    <message>
        <source>Close</source>
        <translation type="vanished">ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>No email client</source>
        <translation type="vanished">སྦྲག་ཟམ་མེད་པའི་ཞབས་ཞུའི་སྣེ།</translation>
    </message>
    <message>
        <source>Go to install</source>
        <translation type="vanished">ཕབ་ལེན།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>Not find email client in the system, please download and install email client firstly.</source>
        <translation type="vanished">མིག་སྔར་མ་ལག་ལ་སྦྲག་ཟམ་སྒྲིག་སྦྱོར་བྱས་མེད་པས།སྔོན་ལ་ཕབ་ལེན་དང་སྦྲག་ཟམ་སྒྲིག་སྦྱོར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KYCRunningDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">མེད་པར་བཟོ་བ</translation>
    </message>
</context>
<context>
    <name>KYCScanDisplayWidget</name>
    <message>
        <source>Please connect to a scan device firstly !</source>
        <translation type="vanished">བཤར་ཕབ་ཆས་ལ་སྦྲེལ་རོགས།</translation>
    </message>
    <message>
        <source>Try to ocr ...</source>
        <translation type="vanished">ཞིབ་བཤེར་བྱེད་སྒང་ཡིན།</translation>
    </message>
</context>
<context>
    <name>KYCScanSettingsWidget</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">རེད།</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">རང་འགུལ།</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">སྐྱ་ཚད།</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">མིང་།</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">རིགས་གྲས་</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">ཁ་མདོག</translation>
    </message>
    <message>
        <source>Currently user has no permission to modify directory </source>
        <translation type="vanished">མིག་སྔར་སྤྱོད་མཁན་ལ་དཀར་ཆག་བསྒྱུར་བའི་དབང་ཆ་མེད།</translation>
    </message>
    <message>
        <source>Store text dialog</source>
        <translation type="vanished">ཡིག་ཆའི་ཁ་བརྡའི་སྒྲོམ་གཞི་གསོག་འཇོག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">སྒྱུ་རྩལ་ཐིག་རིས།</translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">པར་རིས་ཡིག་ཆའི་མིང་གི་རིང་ཚད་ཡིག་འབྲུ་252ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <source>Colour</source>
        <translation type="vanished">ཚོན་མདངས།</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">རྣམ་གཞག</translation>
    </message>
    <message>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation type="vanished">ཡིག་ཆའི་མིང་ལ་ཡིག་རྟགས་‘/’འདུས་མི་རུང་།</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation type="vanished">དབྱེ་འབྱེད་ཚད་ཞིབ་བཤེར་བྱེད་པའི་དུས་ཚོད་ཅུང་རིང་བས་གཟབ་ནན་གྱིས་འདེམས་རོགས།</translation>
    </message>
    <message>
        <source>Paperfed</source>
        <translation type="vanished">ཤོག་བུའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Store text</source>
        <translation type="vanished">ཡིག་ཆ་གསོག་ཉར།</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">གཞན་དུ་བཅོལ་བ།</translation>
    </message>
    <message>
        <source>Scan to</source>
        <translation type="vanished">བཤར་ཕབ།</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">བཤར་འབེབས་ཆས།</translation>
    </message>
    <message>
        <source>Send email to</source>
        <translation type="vanished">སྦྲག་ཟམ་ལ་བསྐུར་བ།</translation>
    </message>
    <message>
        <source>cannot save as hidden file.</source>
        <translation type="vanished">ཡིག་ཆ་སྦས་སྐུང་བྱེད་པར་ཉར་ཚགས་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Select a directory</source>
        <translation type="vanished">དཀར་ཆག་ཅིག་བདམས་རོགས།</translation>
    </message>
    <message>
        <source>Flatbed</source>
        <translation type="vanished">ལེབ་རྣམ།</translation>
    </message>
    <message>
        <source>No available device</source>
        <translation type="vanished">བཀོལ་སྤྱོད་ཆོག་པའི་སྒྲིག་ཆས་མེད།</translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">གཞན་སྒྲོམ་དུ་བཞག་ཡོད།</translation>
    </message>
</context>
<context>
    <name>KYCSendMailDialog</name>
    <message>
        <source>Close</source>
        <translation type="vanished">ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Select email client</source>
        <translation type="vanished">སྦྲག་ཟམ་ཞབས་ཞུའི་སྣེ་འདེམས་རོགས།</translation>
    </message>
</context>
<context>
    <name>KYCTitleBarDialog</name>
    <message>
        <source>Exit</source>
        <translation type="vanished"> ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">རོགས་རམ།</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">ཆེས་ཆུང་འགྱུར།</translation>
    </message>
    <message>
        <source>mainmenu</source>
        <translation type="vanished">འདེམས་བང་གཙོ་བོ།</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">ཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">ཆེས་ཆེ་བ་ཅན།</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">བཤར་འབེབས་ཆས།</translation>
    </message>
</context>
<context>
    <name>KYCWaterMarkDialog</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>Input watermark content</source>
        <translation type="vanished">མིང་རྟགས་ཀྱི་ནང་དོན་འབྲི་རོགས།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>KYCWidget</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">རེད།</translation>
    </message>
    <message>
        <source>Scanner&apos;s parameters error, please change parameters and scanning again.</source>
        <translation type="vanished">ཞུགས་གྲངས་ཞིབ་བཤེར་བྱེད་པ་ནོར་འདུག་ཞུགས་གྲངས་བསྒྱུར་རྗེས་ཡང་བསྐྱར་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation type="vanished">ཡིག་ཆའི་མིང་ལ་ཡིག་རྟགས་‘/’འདུས་མི་རུང་།</translation>
    </message>
    <message>
        <source> already exist, do you want to overwrite it?</source>
        <translation type="vanished">ཡོད་པ་རེད།ཁྱོད་ཀྱིས་དེ་འགེབ་འདོད་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">བཤར་འབེབས་ཆས།</translation>
    </message>
    <message>
        <source>device </source>
        <translation type="vanished">སྒྲིག་ཆས།(_D)</translation>
    </message>
    <message>
        <source>Question</source>
        <translation type="vanished">གནད་དོན།</translation>
    </message>
    <message>
        <source>No available device</source>
        <translation type="vanished">བཀོལ་སྤྱོད་ཆོག་པའི་སྒྲིག་ཆས་མེད།</translation>
    </message>
    <message>
        <source>Running beauty ...</source>
        <translation type="vanished">མཐེབ་གཞོང་གཅིག་གིས་མཛེས་བཟོ་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Please load the paper and scan again.</source>
        <translation type="vanished">ཤོག་བུ་ནང་དུ་བཞག་རྗེས་ཡང་བསྐྱར་བཤར་ཕབ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>scan process is failed, please check your scanner by connect it again.</source>
        <translation type="vanished">བཤར་ཕབ་ཕམ་སོང་། ཁྱོད་ཀྱི་བཤར་ཕབ་ཆས་ཀྱི་འབྲེལ་མཐུད་གནས་ཚུལ་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished">འབྲེལ་མཐུད་ཆད་སོང་།</translation>
    </message>
    <message>
        <source>Running rectify ...</source>
        <translation type="vanished">རིག་ལྡན་ཡོ་བསྲང་བྱེད་བཞིན་པ།</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation>འབྲེལ་མཐུད་བཤེར་ཆས་ལ་ཞིབ་བཤེར་བྱེད་རོགས། ཞིབ་བཤེར་གྱི་མཐེབ་གཅུས་བརྒྱབ་ནས་ཞིབ་བཤེར་བྱེད་འགོ་ཚུགས།</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>kylin-scanner</source>
        <translation>ཆི་ལིན་བཤར་འབེབས་ཆས་</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation>དེ་རུ་བརྟག་དཔྱད་ཡོ་ཆས་གསར་བ་ཞིག་ཡོད་པ་དེས་ཡང་བསྐྱར་བརྟག་དཔྱད་ཡོ་ཆས་ཡོད་ཚད་ལ་ཞིབ་བཤེར་བྱས་ནས་ཅུང་ཙམ་སྒུགས་དང་། </translation>
    </message>
    <message>
        <source>No available device</source>
        <translation>ད་ཡོད་ཀྱི་སྒྲིག་ཆས་མེད་</translation>
    </message>
    <message>
        <source>device </source>
        <translation>སྒྲིག་ཆས། </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation> འབྲེལ་ཐག་ཆད་པ་རེད།</translation>
    </message>
    <message>
        <source>Single</source>
        <translation>ཁེར་རྐྱང་།</translation>
    </message>
    <message>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation>ནུས་པ་མེད་པའི་སྒྲུབ་བྱེད་ལ་རྩོད་གླེང་བརྗེ་བའམ་ཡང་ན་བཤེར་ཆས་གཞན་དག་བརྗེ་རོགས།</translation>
    </message>
    <message>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation>སྒྲིག་ཆས་བྲེལ་བ་ཆེ་བས་ཁྱེད་ཀྱིས་སྒུག་པའམ་ཡང་ན་གཞན་པའི་བཤེར་ཆས་བརྗེ་རོགས།</translation>
    </message>
    <message>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation>ཡིག་ཆའི་ནང་ནས་ཡིག་ཆ་འདོན་སྤྲོད་བྱེད་མཁན་གྱིས་ཡིག་ཆ་བཞག་ནས་ཡང་བསྐྱར་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Scan operation has been cancelled.</source>
        <translation>ཞིབ་བཤེར་གྱི་བྱ་སྤྱོད་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation>ཞིབ་བཤེར་བྱས་ནས་ཕམ་སོང་། ཁྱེད་ཀྱི་བཤེར་ཆས་ལ་ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་བཤེར་ཆས་གཞན་དག་བརྗེ་རོགས།</translation>
    </message>
    <message>
        <source>Running beauty ...</source>
        <translation>རྒྱུག་པའི་མཛེས་སྡུག་ ...</translation>
    </message>
    <message>
        <source>Running rectify ...</source>
        <translation>འཁོར་སྐྱོད་དག་ཐེར་ ...</translation>
    </message>
    <message>
        <source>error code:</source>
        <translation>ནོར་འཁྲུལ་ཨང་།</translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <source>No email client</source>
        <translation>གློག་རྡུལ་ཡིག་ཟམ་གྱི་མངགས་བཅོལ</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation>མ་ལག་ནང་དུ་གློག་རྡུལ་ཡིག་ཟམ་གྱི་མངགས་བཅོལ་བྱེད་མཁན་མ་རྙེད་ན། སྔོན་ལ་གློག་རྡུལ་ཡིག་ཟམ་གྱི་</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ</translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <source>Unable to read text</source>
        <translation>ཡི་གེ་ཀློག་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <source>Unable to read text</source>
        <translation>ཡི་གེ་ཀློག་མི་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation>སྐྱ་ཚད།</translation>
    </message>
    <message>
        <source> User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>ཁ་མདོག</translation>
    </message>
    <message>
        <source>User </source>
        <translation type="vanished">སྤྱོད་མཁན་ID</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation>སྒྱུ་རྩལ་ཐིག་རིས།</translation>
    </message>
    <message>
        <source>&apos;s operations. Are you continue?</source>
        <translation>སྤྱོད་མཁན་གྱིས་ད་ལྟ་བཀོལ་སྤྱོད་བྱེད་བཞིན་ཡོད་པས་མུ་མཐུད་དུ་བྱེད་དམ།</translation>
    </message>
    <message>
        <source>Transparency Adapter</source>
        <translation type="vanished">ཤོག་བུའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source> has already opened kylin-scanner, continue open will close </source>
        <translation type="vanished">མཉེན་ཆས་འདི་བཀོལ་སྤྱོད་བྱེད་བཞིན་ཡོད།ཁ་ཕྱེ་ན་སྒོ་རྒྱག་ངེས།</translation>
    </message>
    <message>
        <source>Question</source>
        <translation>གནད་དོན།</translation>
    </message>
    <message>
        <source>Current </source>
        <translation>མིག་སྔར།</translation>
    </message>
    <message>
        <source> has already opened kylin-scanner, open will close </source>
        <translation>མཉེན་ཆས་འདི་བཀོལ་སྤྱོད་བྱེད་བཞིན་ཡོད།ཁ་ཕྱེ་ན་སྒོ་རྒྱག་ངེས།</translation>
    </message>
    <message>
        <source>Flatbed</source>
        <translation>ལེབ་རྣམ།</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">ཉེན་བརྡ</translation>
    </message>
    <message>
        <source>&apos;s operations.</source>
        <translation type="vanished">ལག་བསྟར་བྱེད་བཞིན་པའི་བཀོལ་སྤྱོད་།</translation>
    </message>
    <message>
        <source>Default Type</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་རིགས</translation>
    </message>
    <message>
        <source>ADF</source>
        <translation>དམངས་གཙོའི་མནའ་མཐུན་ཚོགས་པ</translation>
    </message>
    <message>
        <source>ADF Front</source>
        <translation>ADF འཐབ་ཕྱོགས་གཅིག་གྱུར།</translation>
    </message>
    <message>
        <source>ADF Back</source>
        <translation>ADFཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>ADF Duplex</source>
        <translation>ADF གོ་ཉིས་ཆོད་</translation>
    </message>
    <message>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <source>Flatbed</source>
        <translation>ངོས་མཉམ་པ།</translation>
    </message>
    <message>
        <source>ADF</source>
        <translation>དམངས་གཙོའི་མནའ་མཐུན་ཚོགས་པ</translation>
    </message>
    <message>
        <source>ADF Front</source>
        <translation>ADF འཐབ་ཕྱོགས་གཅིག་གྱུར།</translation>
    </message>
    <message>
        <source>ADF Back</source>
        <translation>ADFཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>Default Type</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་རིགས</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <source>200 dpi</source>
        <translation>200 dpi</translation>
    </message>
    <message>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation>སྣ་མང་།</translation>
    </message>
    <message>
        <source>ADF Duplex</source>
        <translation>ADF གོ་ཉིས་ཆོད་</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <source>Scanner</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation>སྣ་མང་།</translation>
    </message>
    <message>
        <source>Number of pages scanning: </source>
        <translation>ཞིབ་བཤེར་བྱས་པའི་ཤོག་ངོས་ཀྱི་གྲངས་ཀ་གཤམ་གསལ། </translation>
    </message>
    <message>
        <source>Canceling scan，please wait a moment!</source>
        <translation>ཞིབ་བཤེར་མེད་པར་བཟོས་སོང་། ཅུང་ཙམ་སྒུགས་དང་།</translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <source>Select a directory</source>
        <translation>དཀར་ཆག་ཅིག་བདམས་པ།</translation>
    </message>
    <message>
        <source>Currently user has no permission to modify directory </source>
        <translation>མིག་སྔར་སྤྱོད་མཁན་གྱིས་དཀར་ཆག་ལ་བཟོ་བཅོས་རྒྱག་པའི་ཆོག་མཆན་མེད། </translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation>སྣ་མང་།</translation>
    </message>
    <message>
        <source>Single</source>
        <translation>ཁེར་རྐྱང་།</translation>
    </message>
    <message>
        <source>Color</source>
        <translation>ཁ་དོག</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation>ལམ་ཐིག</translation>
    </message>
    <message>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <source>1200 dpi</source>
        <translation>1200 dpi</translation>
    </message>
    <message>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation>གྲོས་ཆོད་འདི་ལ་འཁེལ་འཐག་འཕྲུལ་འཁོར་གྱི་དུས་ཚོད་སྤྱད་དེ་ཞིབ་བཤེར་བྱེད་དགོས་པས་སེམས་ཆུང་ངང་གདམ་གསེས་གནང་རོགས།</translation>
    </message>
    <message>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation>&apos;/&apos;ཡི་གཤིས་ཀ་འདུས་མི་རུང་།</translation>
    </message>
    <message>
        <source>cannot save as hidden file.</source>
        <translation>སྦས་སྐུང་བྱས་པའི་ཡིག་ཆ་ལྟར་ཉར་ཚགས་བྱེད་</translation>
    </message>
    <message>
        <source>Store text</source>
        <translation>གསོག་ཉར་ཡི་གེ</translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">གླེང་མོལ་བྱས་ནས་ཉར་ཚགས་བྱེད་པ</translation>
    </message>
    <message>
        <source>Device</source>
        <translation>སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
    <message>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <source>Colour</source>
        <translation>ཁ་དོག</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>གྲོས་ཆོད།</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>གྲོན་ཆུང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Mail to</source>
        <translation>སྦྲག་ཐོག་ནས་བསྐུར་རྒྱུ།</translation>
    </message>
    <message>
        <source>Begin Scan</source>
        <translation>ཞིབ་བཤེར་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation>བཤེར་ཆས་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation>ཡིག་ཆའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>scanner01</source>
        <translation>བཤེར་ཆས་01</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation>གྲོན་ཆུང་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">ད་ཡོད་ཀྱི་བཤེར་ཆས་མེད་པ།</translation>
    </message>
    <message>
        <source>3s</source>
        <translation>3s</translation>
    </message>
    <message>
        <source>5s</source>
        <translation>5s</translation>
    </message>
    <message>
        <source>7s</source>
        <translation>7s</translation>
    </message>
    <message>
        <source>10s</source>
        <translation>10s</translation>
    </message>
    <message>
        <source>15s</source>
        <translation>15s</translation>
    </message>
    <message>
        <source>Flatbed</source>
        <translation>ངོས་མཉམ་པ།</translation>
    </message>
    <message>
        <source>ADF</source>
        <translation>དམངས་གཙོའི་མནའ་མཐུན་ཚོགས་པ</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation>སྐྱ་བོ།</translation>
    </message>
    <message>
        <source>75 dpi</source>
        <translation>75 dpi</translation>
    </message>
    <message>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <source>Resolution is empty!</source>
        <translation>གྲོས་ཆོད་ནི་སྟོང་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="obsolete">པར་རིས་ཡིག་ཆའི་མིང་གི་རིང་ཚད་ཡིག་འབྲུ་252ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <source>tips</source>
        <translation>གསལ་འདེབས་བྱེད་ཐབས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>The file </source>
        <translation>ཡིག་ཆ། </translation>
    </message>
    <message>
        <source> already exists, do you want to overwrite it?</source>
        <translation> ཡོད་ཟིན་པས་ཁྱོད་ཀྱིས་དེ་ལས་བརྒལ་ན་འདོད་དམ།</translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>གཞན་ཉར་</translation>
    </message>
    <message>
        <source>Path without access rights: </source>
        <translation>བདམས་པའི་ཐབས་ལམ་ཁྱེད་རང་ལ་འཚམས་འདྲི་བྱེད་པའི་དབང་ཚད་མེད་།</translation>
    </message>
    <message>
        <source>File path that does not exist: </source>
        <translation>ཡིག་ཆའི་འགྲོ་ལམ་གནས་མེད།</translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <source>Multiple</source>
        <translation>སྣ་མང་།</translation>
    </message>
    <message>
        <source>3s</source>
        <translation>3s</translation>
    </message>
    <message>
        <source>5s</source>
        <translation>5s</translation>
    </message>
    <message>
        <source>7s</source>
        <translation>7s</translation>
    </message>
    <message>
        <source>10s</source>
        <translation>10s</translation>
    </message>
    <message>
        <source>15s</source>
        <translation>15s</translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <source>Select email client</source>
        <translation>གློག་རྡུལ་ཡིག་ཟམ་གྱི་མངགས་བཅོལ</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <source>Running beauty ...</source>
        <translation>རྒྱུག་པའི་མཛེས་སྡུག་ ...</translation>
    </message>
    <message>
        <source>Running rectify ...</source>
        <translation>འཁོར་སྐྱོད་དག་ཐེར་ ...</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <source>kylin-scanner</source>
        <translation>ཆི་ལིན་བཤར་འབེབས་ཆས་</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <source>Option</source>
        <translation>བསལ་འདེམས་ཀྱི་དབང</translation>
    </message>
    <message>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation>ད་ལྟ་སྤྱོད་བཞིན་པའི་ཡིག་ཆ་ཉར་ཚགས་བྱས་མེད། ཁྱོད་ཀྱིས་དེ་ཉར་ཚགས་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Straight &amp;Exit</source>
        <translation>ཐད་ཀར་ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>&amp;Save Exit</source>
        <translation>&amp;ཕྱིར་འཐེན་བྱེད་པར་སྐྱོབ་དགོས།</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>པར་གཞི་</translation>
    </message>
    <message>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>ཡིས་ཁྱབ་ཆུང་དྲ་རྒྱའི་ཁྲོད་དུ་ཡིག་སྨར་ཁ་བརྡ་དང་ཡིག་ཆ་བརྒྱུད་གཏོང་གི་བྱེད་ནུས་མཁོ་འདོན་བྱས། ཞབས་ཞུ་ཆས་འཛུགས་མི་དགོས། མི་མང་པོས་དུས་གཅིག་ཏུ་སྤེལ་རེས་དང་མཉམ་དུ་གཏོང་ལེན་བྱེད་པར་རྒྱབ་སྐྱོར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <source>Beauty</source>
        <translation>མཛེས་པ།</translation>
    </message>
    <message>
        <source>Rectify</source>
        <translation>དག་ཐེར་ཡོ་བསྲང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>OCR</source>
        <translation>ཨོ་སི་ཁྲུ་ལི་ཡ།</translation>
    </message>
    <message>
        <source>Crop</source>
        <translation>ལོ་ཏོག</translation>
    </message>
    <message>
        <source>Rotate</source>
        <translation>འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Mirror</source>
        <translation>མེ་ལོང་།</translation>
    </message>
    <message>
        <source>Watermark</source>
        <translation>ཆུ་རྟགས།</translation>
    </message>
    <message>
        <source>ZoomOut</source>
        <translation>ཆེ་རུ་གཏོང་བ།</translation>
    </message>
    <message>
        <source>ZoomIn</source>
        <translation>ཆེ་རུ་གཏོང་བ།</translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>Scanner has been disconnect.</source>
        <translation>བཤེར་ཆས་ཆད་སོང་།</translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <source>Scanner</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Add watermark</source>
        <translation>ཆུ་རྟགས་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Open file &lt;filename&gt;</source>
        <translation>ཁ་ཕྱེ་བའི་ཡིག་ཆ།&lt;ཡིག་ཆའི་མིང་།&gt;</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>Hide scan settings widget</source>
        <translation>སྦས་སྐུང་བྱས་ནས་ཞིབ་བཤེར་བྱེད་པའི་སྒྲིག་ཆས་ཀྱི་སྒྲིག་ཆས་ཆུང</translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <source>The document is in character recognition ...</source>
        <translation>ཡིག་ཆ་དེ་ནི་གཤིས་ཀར་ངོས་འཛིན་བྱེད་པའི་ཁྲོད་དུ་</translation>
    </message>
</context>
</TS>
