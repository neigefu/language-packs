<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectFailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接扫描仪</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageLeftWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="vanished">已连接扫描设备，点击按钮开始扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageRightWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation type="vanished">扫描仪设备</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation type="vanished">文件预设</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="vanished">页数</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">色彩</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">扫描至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting ...</source>
        <translation type="vanished">检测扫描设备中，请稍后 ……</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="45"/>
        <source>Detect scanners, please waiting</source>
        <translation>檢測掃描器，請稍候</translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="46"/>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation>未檢測到掃描器，請先連接掃描器！</translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="50"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
</context>
<context>
    <name>GlobalConfigSignal</name>
    <message>
        <source>kylin-scanner-images</source>
        <translation type="vanished">扫描图像</translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation>對話</translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="46"/>
        <location filename="../src/leftsuccesspagewidget.cpp" line="70"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation>連接掃描器，請按兩下掃描按鈕開始掃描。</translation>
    </message>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="48"/>
        <source>No scanner detected, plug in a new USB scanner or click refresh list to refresh the device list.</source>
        <translation>未檢測到掃描器，請插入新的 USB 掃描器或按下刷新清單以刷新設備清單。</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>kylin-scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="64"/>
        <location filename="../src/mainwidget.cpp" line="165"/>
        <source>Scanner</source>
        <translation>掃描器</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>There is a new scanner connect, please restart this application manually. </source>
        <translation type="vanished">存在新设备连接，请手动重启应用使用该新设备。</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation type="vanished">存在新设备连接，正在重新检测所有扫描设备，请稍等。</translation>
    </message>
    <message>
        <source>No available device</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>Single</source>
        <translation type="vanished">单页扫描</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="357"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation>參數無效，請更改參數或切換其他掃描器。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="357"/>
        <location filename="../src/mainwidget.cpp" line="383"/>
        <location filename="../src/mainwidget.cpp" line="386"/>
        <location filename="../src/mainwidget.cpp" line="389"/>
        <location filename="../src/mainwidget.cpp" line="393"/>
        <source>error code:</source>
        <translation>錯誤代碼：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="393"/>
        <source>Scan failed, please check your scanner or switch other scanners. If you want to continue using the scanner, click Options, refresh the list to restart the device.</source>
        <translation>掃描失敗，請檢查您的掃描器或切換其他掃描器。如果要繼續使用掃描器，請按下「選項」，刷新清單以重新啟動設備。</translation>
    </message>
    <message>
        <source>error code: </source>
        <translation type="vanished">错误码：</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="383"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation>設備繁忙，請稍候或切換其他掃描器。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="386"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation>文件進紙器從文件中取出，請放置紙張並重新掃描。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="389"/>
        <source>Scan operation has been cancelled.</source>
        <translation>掃描操作已取消。</translation>
    </message>
    <message>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation type="vanished">扫描失败，请检查当前扫描仪连接或切换到其他扫描仪。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="438"/>
        <source>Alert</source>
        <translation>警報</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="438"/>
        <source>A new Scanner has been connected.</source>
        <translation>已連接新的掃描器。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="478"/>
        <source>Running beauty ...</source>
        <translation>跑步美女...</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="497"/>
        <source>Running rectify ...</source>
        <translation>執行整流...</translation>
    </message>
</context>
<context>
    <name>NoDeviceWidget</name>
    <message>
        <location filename="../src/nodevicewidget.cpp" line="26"/>
        <source>Scanner not detected</source>
        <translation>未檢測到掃描器</translation>
    </message>
</context>
<context>
    <name>NoDriverDialog</name>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>No available drivers, do you want to manually add drivers</source>
        <translation type="vanished">无可用驱动，你想要手动添加驱动吗？</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="54"/>
        <location filename="../src/sendmail.cpp" line="77"/>
        <location filename="../src/sendmail.cpp" line="78"/>
        <source>No email client</source>
        <translation>沒有電子郵件用戶端</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="61"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="89"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation>在系統中找不到電子郵件用戶端，請先安裝電子郵件用戶端。</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="104"/>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="101"/>
        <location filename="../src/sendmail.cpp" line="102"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <source>Unable to read text</source>
        <translation type="vanished">不能识别到文本。</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="270"/>
        <source>Scanner</source>
        <translation>掃描器</translation>
    </message>
    <message>
        <source>Scanner is on working. Please refresh the device list after scanning.</source>
        <translation type="vanished">扫描仪正在工作，请在扫描完成后刷新设备列表。</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="54"/>
        <source>Refreshing list. Please wait for the refresh success information...</source>
        <translation>刷新清單。請等待刷新成功資訊...</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="60"/>
        <source>Scanner is on detecting...</source>
        <translation>掃描器正在偵測...</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="437"/>
        <source>ADF Duplex</source>
        <translation>ADF 雙工</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="572"/>
        <source>Fail to open the scanner, error code </source>
        <translation>無法打開掃描器，錯誤代碼 </translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2052"/>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <source>Illegal Naming!</source>
        <translation type="vanished">非法命名！</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="437"/>
        <location filename="../src/scansettingswidget.cpp" line="201"/>
        <location filename="../src/scansettingswidget.cpp" line="461"/>
        <source>Multiple</source>
        <translation>倍數</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="202"/>
        <source>Flatbed</source>
        <translation>平板</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>User </source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="287"/>
        <source>Question</source>
        <translation>問題</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source>Current </source>
        <translation>當前 </translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source> User</source>
        <translation> 使用者</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="285"/>
        <source> has already opened kylin-scanner, open will close </source>
        <translation> 已經打開麒麟掃描器，打開將關閉 </translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="286"/>
        <source>&apos;s operations. Are you continue?</source>
        <translation>的操作。你繼續嗎？</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="642"/>
        <source>Color</source>
        <translation>顏色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="649"/>
        <source>Gray</source>
        <translation>灰色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="655"/>
        <source>Lineart</source>
        <translation>線條藝術</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="720"/>
        <source>Default Type</source>
        <translation>默認類型</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="797"/>
        <source>Flatbed</source>
        <translation>平板</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="807"/>
        <source>ADF</source>
        <translation>澳大利亞國防軍</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="816"/>
        <source>ADF Front</source>
        <translation>ADF 前線</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="825"/>
        <source>ADF Back</source>
        <translation>ADF 返回</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="834"/>
        <source>ADF Duplex</source>
        <translation>ADF 雙工</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="883"/>
        <location filename="../src/saneobject.cpp" line="946"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="886"/>
        <location filename="../src/saneobject.cpp" line="950"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="889"/>
        <location filename="../src/saneobject.cpp" line="954"/>
        <source>1200 dpi</source>
        <translation>1200 分頻</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="892"/>
        <location filename="../src/saneobject.cpp" line="958"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="895"/>
        <location filename="../src/saneobject.cpp" line="962"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="898"/>
        <location filename="../src/saneobject.cpp" line="966"/>
        <source>200 dpi</source>
        <translation>200 分頻</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="901"/>
        <location filename="../src/saneobject.cpp" line="970"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="904"/>
        <location filename="../src/saneobject.cpp" line="974"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="907"/>
        <location filename="../src/saneobject.cpp" line="978"/>
        <source>75 dpi</source>
        <translation>75 分頻</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="924"/>
        <location filename="../src/saneobject.cpp" line="994"/>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="55"/>
        <location filename="../src/runningdialog.cpp" line="141"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="84"/>
        <location filename="../src/runningdialog.cpp" line="170"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="1978"/>
        <source>Default Type</source>
        <translation>默認類型</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1970"/>
        <source>Flatbed</source>
        <translation>平板</translation>
    </message>
    <message>
        <source>Refreshing list. Please wait for the refresh success information...</source>
        <translation type="vanished">正在刷新列表，请等待刷新成功信息...</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="409"/>
        <source>Refresh list complete.</source>
        <translation>刷新清單完成。</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1972"/>
        <source>ADF</source>
        <translation>澳大利亞國防軍</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1974"/>
        <source>ADF Front</source>
        <translation>ADF 前線</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1976"/>
        <source>ADF Back</source>
        <translation>ADF 返回</translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">黑白</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">灰度</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">彩色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1980"/>
        <source>ADF Duplex</source>
        <translation>ADF 雙工</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2056"/>
        <source>75 dpi</source>
        <translation>75 分頻</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2058"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2060"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2062"/>
        <source>200 dpi</source>
        <translation>200 分頻</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2064"/>
        <source>300 dpi</source>
        <translation>300 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2066"/>
        <source>600 dpi</source>
        <translation>600 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2068"/>
        <source>1200 dpi</source>
        <translation>1200 分頻</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2070"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2072"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="30"/>
        <location filename="../src/scandialog.cpp" line="45"/>
        <source>Scanner</source>
        <translation>掃描器</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="49"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="69"/>
        <location filename="../src/scandialog.cpp" line="169"/>
        <source>Number of pages scanning: </source>
        <translation>掃描頁數： </translation>
    </message>
    <message>
        <source>Number of pages being scanned: </source>
        <translation type="vanished">正在扫描页数：</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="81"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="138"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation>正在取消掃描，請稍等！</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="167"/>
        <source>Multiple</source>
        <translation>倍數</translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="634"/>
        <source>Scanner device</source>
        <translation>掃描器設備</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="639"/>
        <source>File settings</source>
        <translation>檔設置</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="504"/>
        <source>Device</source>
        <translation>裝置</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="118"/>
        <source>Select a directory</source>
        <translation>選擇一個目錄</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="140"/>
        <source>Currently user has no permission to modify directory </source>
        <translation>當前用戶無權修改目錄 </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="203"/>
        <source>Flatbed scan mode not support multiple scan.</source>
        <translation>平板掃描模式不支援多次掃描。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="208"/>
        <location filename="../src/scansettingswidget.cpp" line="801"/>
        <source>Multiple</source>
        <translation>倍數</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="505"/>
        <source>Pages</source>
        <translation>頁面</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="506"/>
        <source>Type</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="259"/>
        <location filename="../src/scansettingswidget.cpp" line="845"/>
        <source>Color</source>
        <translation>顏色</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="508"/>
        <source>Resolution</source>
        <translation>解析度</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="509"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="510"/>
        <source>Format</source>
        <translation>格式</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="511"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="642"/>
        <source>scanner01</source>
        <translation>掃描儀01</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="512"/>
        <source>Save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="520"/>
        <location filename="../src/scansettingswidget.cpp" line="521"/>
        <source>Mail to</source>
        <translation>郵寄至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="662"/>
        <location filename="../src/scansettingswidget.cpp" line="981"/>
        <location filename="../src/scansettingswidget.cpp" line="982"/>
        <source>Save as</source>
        <translation>另存為</translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="205"/>
        <location filename="../src/scansettingswidget.cpp" line="224"/>
        <location filename="../src/scansettingswidget.cpp" line="248"/>
        <location filename="../src/scansettingswidget.cpp" line="801"/>
        <source>Single</source>
        <translation>單</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="273"/>
        <source>4800 dpi</source>
        <translation>4800 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="274"/>
        <source>2400 dpi</source>
        <translation>2400 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="275"/>
        <source>1200 dpi</source>
        <translation>1200 分頻</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="277"/>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation>此解析度需要很長時間才能掃描，請謹慎選擇。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="304"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation>不能包含“/”字元。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="310"/>
        <location filename="../src/scansettingswidget.cpp" line="418"/>
        <source>cannot save as hidden file.</source>
        <translation>無法另存為隱藏檔。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="407"/>
        <source>Save As</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="431"/>
        <source>Path without access rights: </source>
        <translation>沒有存取權限的路徑： </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="435"/>
        <source>File path that does not exist: </source>
        <translation>不存在的檔案路徑： </translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="478"/>
        <source> already exists, do you want to overwrite it? If you are performing a multi page scan, it may cause multiple files to be overwritten. Please be cautious!</source>
        <translation> 已經存在，是否要覆蓋它？如果執行多頁掃描，可能會導致多個文件被覆蓋。請謹慎！</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="972"/>
        <location filename="../src/scansettingswidget.cpp" line="973"/>
        <source>Store text</source>
        <translation>存儲文字</translation>
    </message>
    <message>
        <source>Save as dialog</source>
        <translation type="vanished">另存为</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="477"/>
        <source>The file </source>
        <translation>該檔 </translation>
    </message>
    <message>
        <source> already exists, do you want to overwrite it?</source>
        <translation type="vanished">已存在，您想覆盖它吗？</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="480"/>
        <source>tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="507"/>
        <source>Colour</source>
        <translation>顏色</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="615"/>
        <source>Start Scan</source>
        <translation>開始掃描</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="247"/>
        <location filename="../src/scansettingswidget.cpp" line="818"/>
        <source>Flatbed</source>
        <translation>平板</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="818"/>
        <source>ADF</source>
        <translation>澳大利亞國防軍</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="845"/>
        <source>Gray</source>
        <translation>灰色</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="261"/>
        <location filename="../src/scansettingswidget.cpp" line="845"/>
        <source>Lineart</source>
        <translation>線條藝術</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="868"/>
        <source>75 dpi</source>
        <translation>75 分頻</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="868"/>
        <source>100 dpi</source>
        <translation>100 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="868"/>
        <source>150 dpi</source>
        <translation>150 dpi</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="885"/>
        <source>Resolution is empty!</source>
        <translation>解析度為空！</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="899"/>
        <source>A4</source>
        <translation>答4</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="899"/>
        <source>A5</source>
        <translation>答5</translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">扫描文档名称的长度不能超过252。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1190"/>
        <source>Scanner</source>
        <translation>掃描器</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1193"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <source>Multiple</source>
        <translation type="vanished">多页扫描</translation>
    </message>
    <message>
        <source>3s</source>
        <translation type="vanished">3 秒</translation>
    </message>
    <message>
        <source>5s</source>
        <translation type="vanished">5 秒</translation>
    </message>
    <message>
        <source>7s</source>
        <translation type="vanished">7 秒</translation>
    </message>
    <message>
        <source>10s</source>
        <translation type="vanished">10 秒</translation>
    </message>
    <message>
        <source>15s</source>
        <translation type="vanished">15 秒</translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="199"/>
        <location filename="../src/sendmail.cpp" line="223"/>
        <source>Select email client</source>
        <translation>選擇電子郵件用戶端</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="205"/>
        <source>Scanner</source>
        <translation>掃描器</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="208"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="237"/>
        <location filename="../src/sendmail.cpp" line="238"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="241"/>
        <location filename="../src/sendmail.cpp" line="242"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="52"/>
        <location filename="../src/showimagewidget.cpp" line="60"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1029"/>
        <source>Canceling...Please waiting...</source>
        <translation>取消。。。請稍候...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1040"/>
        <source>Running beauty ...</source>
        <translation>跑步美女...</translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="1073"/>
        <source>Running rectify ...</source>
        <translation>執行整流...</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="107"/>
        <source>kylin-scanner</source>
        <translation>麒麟掃描器</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="110"/>
        <location filename="../src/titlebar/titlebar.cpp" line="188"/>
        <location filename="../src/titlebar/titlebar.h" line="83"/>
        <source>Scanner</source>
        <translation>掃描器</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="173"/>
        <source>Option</source>
        <translation>選擇</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="176"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="217"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="258"/>
        <location filename="../src/titlebar/titlebar.cpp" line="140"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="305"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Maxmize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="48"/>
        <source>Refresh List</source>
        <translation>刷新清單</translation>
    </message>
    <message>
        <source>Scanner is on working. Please refresh the device list after scanning</source>
        <translation type="vanished">扫描仪正在工作，请在扫描完成后刷新设备列表。</translation>
    </message>
    <message>
        <source>Scanner is on working. Please refresh the device list after scanning.</source>
        <translation type="vanished">扫描仪正在工作，请在扫描完成后刷新设备列表。</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="65"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="69"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="71"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="72"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="82"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="137"/>
        <source>Restore</source>
        <translation>恢復</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="186"/>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation>當前文件未儲存。是否要保存它？</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="190"/>
        <source>Straight &amp;Exit</source>
        <translation>直行和退出</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="191"/>
        <source>&amp;Save Exit</source>
        <translation>&amp;儲存退出</translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="41"/>
        <source>Beauty</source>
        <translation>美</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="47"/>
        <source>Rectify</source>
        <translation>糾正</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="53"/>
        <source>OCR</source>
        <translation>光學字元識別</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="61"/>
        <source>Crop</source>
        <translation>作物</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="67"/>
        <source>Rotate</source>
        <translation>旋轉</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="73"/>
        <source>Mirror</source>
        <translation>鏡子</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="79"/>
        <source>Watermark</source>
        <translation>浮浮水印</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="98"/>
        <source>ZoomIn</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="92"/>
        <source>ZoomOut</source>
        <translation>縮小</translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>device has been disconnect.</source>
        <translation type="vanished">设备已断开！</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="77"/>
        <source>Querying scanner device. Please waitting...</source>
        <translation>正在查詢掃描程式設備。請稍候...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="98"/>
        <source>New Scanner has been Connected.</source>
        <translation>新掃描器已連接。</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="108"/>
        <source>A scanner is disconnected. If you disconnect the scanner is on scanning, click Cancel Scan or wait for the scanner to report an error message.</source>
        <translation>掃描器已斷開連接。如果斷開掃描器正在掃描的連接，請按下取消掃描或等待掃描器報告錯誤消息。</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="113"/>
        <source>Scanner is disconnect，refreshing scanner list. Please waitting...</source>
        <translation>掃描器已斷開連接，刷新掃描器清單。請稍候...</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="128"/>
        <source>Scanner list refresh complete.</source>
        <translation>掃描程式清單刷新完成。</translation>
    </message>
    <message>
        <source>Scanner has been disconnect.If you unplug the scanner USB interface during scanning, please plug and unplug the USB after a clear error message pops up.</source>
        <translation type="vanished">扫描仪断开连接！如果是在扫描仪运行过程中拔出扫描仪USB接口，则请在弹出明确错误信息后再进行USB插拔操作。</translation>
    </message>
    <message>
        <source>Scanner has been disconnect.If you unplug the scanner USB interface during the scanner operation, please plug and unplug the USB after a clear error message pops up.</source>
        <translation type="vanished">扫描仪断开连接！如果是在扫描仪运行过程中拔出扫描仪USB接口，则请在弹出明确错误信息后再进行USB插拔操作。</translation>
    </message>
    <message>
        <source>Scanner has been disconnect.</source>
        <translation type="vanished">扫描仪已断开连接！</translation>
    </message>
</context>
<context>
    <name>WaittingDialog</name>
    <message>
        <location filename="../src/waittingdialog.cpp" line="29"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/waittingdialog.cpp" line="42"/>
        <source>Searching for scanner...</source>
        <translation>搜尋掃描器...</translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="29"/>
        <source>Scanner</source>
        <translation>掃描器</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="37"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="48"/>
        <source>Add watermark</source>
        <translation>添加浮浮浮水印</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="64"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="67"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="198"/>
        <source>Open file &lt;filename&gt;</source>
        <translation>打開&lt;filename&gt;檔</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="199"/>
        <source>Filename</source>
        <translation>檔名</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="203"/>
        <source>Hide scan settings widget</source>
        <translation>隱藏掃描設置小部件</translation>
    </message>
</context>
<context>
    <name>newDeviceListPage</name>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="46"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Driver</source>
        <translation type="vanished">驱动</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="35"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="35"/>
        <source>Symbol</source>
        <translation>象徵</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="59"/>
        <source>Device List</source>
        <translation>設備清單</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="72"/>
        <location filename="../src/newdevicelistpage.cpp" line="92"/>
        <location filename="../src/newdevicelistpage.cpp" line="219"/>
        <location filename="../src/newdevicelistpage.cpp" line="276"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="73"/>
        <source>Next</source>
        <translation>下一個</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="93"/>
        <source>Before</source>
        <translation>以前</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="94"/>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="213"/>
        <source>No available drivers, do you want to manually add drivers?</source>
        <translation>沒有可用的驅動程式，是否要手動添加驅動程式？</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="217"/>
        <source>Add</source>
        <translation>加</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="255"/>
        <location filename="../src/newdevicelistpage.cpp" line="352"/>
        <source>Installing driver...</source>
        <translation>安裝驅動程式...</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="273"/>
        <source>Installation successful. Do you want to use this scanner now?</source>
        <translation>安裝成功。是否要立即使用此掃描器？</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="277"/>
        <source>Use</source>
        <translation>用</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="305"/>
        <source>Installation failed.</source>
        <translation>安裝失敗。</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="308"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="346"/>
        <source>Select a directory</source>
        <translation>選擇一個目錄</translation>
    </message>
    <message>
        <source>Searching for scanner drivers...</source>
        <translation type="vanished">正在查询扫描仪驱动...</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="159"/>
        <source>Device Name:</source>
        <translation>裝置名稱：</translation>
    </message>
    <message>
        <location filename="../src/newdevicelistpage.cpp" line="162"/>
        <source>Driver Name:</source>
        <translation>驅動程式名稱：</translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="30"/>
        <location filename="../src/showocrwidget.cpp" line="134"/>
        <location filename="../src/showocrwidget.cpp" line="142"/>
        <source>The document is in character recognition ...</source>
        <translation>文件是字元識別...</translation>
    </message>
</context>
</TS>
