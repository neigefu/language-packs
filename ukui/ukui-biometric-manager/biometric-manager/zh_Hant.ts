<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.ui" line="283"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC DemiLight&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Noto Sans CJK SC&apos;;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation> </translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="31"/>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>Biometric Manager</source>
        <translation>生物特徵管理工具</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="34"/>
        <source>Version number: </source>
        <translation>版本號： </translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="35"/>
        <location filename="../src/aboutdialog.cpp" line="48"/>
        <source>         Biometric Manager is a supporting software for managing biometric identification which is developed by Kylin team.  It mainly contains biometirc verification management, biometirc service management, biometric device&apos;s driver management and biometirc features management, etc.All functions of the software are still being perfected. Please look forward to it. </source>
        <translation>         生物特徵管理工具是由麒麟團隊開發的一款用於管理生物特徵的輔助軟體。 主要功能包括生物特徵認證管理、生物特徵服務管理、生物特徵設備驅動管理以及生物特徵的管理等功能。 各項功能目前還在不斷完善中，敬請期待。 </translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="39"/>
        <source>Service &amp; Support: </source>
        <translation>服務與支持團隊： </translation>
    </message>
    <message>
        <source>developers：</source>
        <translation type="vanished">开发者邮箱</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Contributor</source>
        <translation type="vanished">贡献者</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;         Biometric Manager is a supporting software for managing biometric identification which is developed by Kylin team.  It mainly contains biometirc verification management, biometirc service management, biometric device&apos;s driver management and biometirc features management, etc.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;       &lt;/span&gt;&lt;a name=&quot;textarea-bg-text&quot;&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;A&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;ll functions of the software are still being perfected. Please look forward to it. &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         生物特征管理工具是由麒麟团队开发的一款用于管理生物特征的辅助软件。主要功能包括生物特征认证管理、生物特征服务管理、生物特征设备驱动管理以及生物特征的管理等功能。&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;        各项功能目前还在不断完善中，敬请期待。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>AttributeWindow</name>
    <message>
        <location filename="../src/attributewindow.ui" line="14"/>
        <source>Dialog</source>
        <translation>對話框</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="48"/>
        <source>Bus Type:</source>
        <translation>匯流類型：</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="62"/>
        <source>Storage Type:</source>
        <translation>儲存類型：</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="69"/>
        <source>Identification Type:</source>
        <translation>識別類型：</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.ui" line="83"/>
        <source>Verify Type:</source>
        <translation>驗證類型：</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="42"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="44"/>
        <source>Fingervein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="46"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="48"/>
        <source>Face</source>
        <translation>人臉</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="50"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="59"/>
        <source>Hardware Verification</source>
        <translation>硬體驗證</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="61"/>
        <source>Software Verification</source>
        <translation>軟體驗證</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="63"/>
        <source>Mix Verification</source>
        <translation>混合驗證</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="65"/>
        <source>Other Verification</source>
        <translation>其他驗證</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="73"/>
        <source>Device Storage</source>
        <translation>設備存儲</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="75"/>
        <source>OS Storage</source>
        <translation>系統存儲</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="77"/>
        <source>Mix Storage</source>
        <translation>混合存儲</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="85"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="87"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="89"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="91"/>
        <source>Any</source>
        <translation>任意類型</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="93"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="101"/>
        <source>Hardware Identification</source>
        <translation>硬體識別</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="103"/>
        <source>Software Identification</source>
        <translation>軟體識別</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="105"/>
        <source>Mix Identification</source>
        <translation>混合識別</translation>
    </message>
    <message>
        <location filename="../src/attributewindow.cpp" line="107"/>
        <source>Other Identification</source>
        <translation>其他識別</translation>
    </message>
</context>
<context>
    <name>ContentPane</name>
    <message>
        <location filename="../src/contentpane.ui" line="14"/>
        <source>Form</source>
        <translation>表單</translation>
    </message>
    <message>
        <source>...</source>
        <translation type="vanished">...</translation>
    </message>
    <message>
        <source>Driver Status:</source>
        <translation type="vanished">驱动状态：</translation>
    </message>
    <message>
        <source>Device Status:</source>
        <translation type="vanished">设备状态：</translation>
    </message>
    <message>
        <source>Default Device:</source>
        <translation type="vanished">默认设备：</translation>
    </message>
    <message>
        <source>Verify Type:</source>
        <translation type="vanished">验证类型：</translation>
    </message>
    <message>
        <source>Clean</source>
        <translation type="vanished">清空</translation>
    </message>
    <message>
        <source>Biometric Type:</source>
        <translation type="obsolete">生物特征类型：</translation>
    </message>
    <message>
        <source>Device Short Name:</source>
        <translation type="vanished">设备简称：</translation>
    </message>
    <message>
        <source>Device Full Name:</source>
        <translation type="vanished">设备全称：</translation>
    </message>
    <message>
        <source>Bus Type:</source>
        <translation type="vanished">总线类型：</translation>
    </message>
    <message>
        <source>Identification Type:</source>
        <translation type="vanished">识别类型：</translation>
    </message>
    <message>
        <source>Storage Type:</source>
        <translation type="vanished">存储类型：</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation type="obsolete">状态：</translation>
    </message>
    <message>
        <source>Biometric Feature List</source>
        <translation type="obsolete">特征列表</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="vanished">录入</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <source>Verify</source>
        <translation type="vanished">验证</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空所有</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="obsolete">特征名称</translation>
    </message>
    <message>
        <source>index</source>
        <translation type="obsolete">索引</translation>
    </message>
    <message>
        <source>Device is not connected</source>
        <translation type="obsolete">设备未连接</translation>
    </message>
    <message>
        <source>Device is available</source>
        <translation type="obsolete">设备可用</translation>
    </message>
    <message>
        <source>Opened</source>
        <translation type="vanished">开</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation type="vanished">关</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="179"/>
        <source>Connected</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="179"/>
        <source>Unconnected</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="297"/>
        <source>New Feature</source>
        <translation>新的特徵</translation>
    </message>
    <message>
        <source>Please input a name for the feature:</source>
        <translation type="vanished">特征名称：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="298"/>
        <location filename="../src/contentpane.cpp" line="301"/>
        <source>name:</source>
        <translation>名稱：</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="300"/>
        <source>Rename Feature</source>
        <translation>特徵重新命名</translation>
    </message>
    <message>
        <source>Duplicate feature name</source>
        <translation type="vanished">特徵名稱重複</translation>
    </message>
    <message>
        <source>Empty feature name</source>
        <translation type="vanished">空的特徵名稱</translation>
    </message>
    <message>
        <source>feature name is too long</source>
        <translation type="vanished">特徵名稱太長</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="582"/>
        <source>Fail to delete! </source>
        <translation>刪除失敗！ </translation>
    </message>
    <message>
        <source>Delete successfully!</source>
        <translation type="vanished">刪除成功！</translation>
    </message>
    <message>
        <source>Please do not use &apos;_&apos; as the beginning of the feature name </source>
        <translation type="vanished">请不要以空格作为特征名称的开头</translation>
    </message>
    <message>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation type="vanished">請不要以空格作為特徵名稱的開頭或者結尾</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="301"/>
        <source>Please input a new</source>
        <translation>請輸入一個新的</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="378"/>
        <source>&apos;Face recognition&apos; does not support live body detection, and the verification method is risky. Do you want to continue?</source>
        <translation><“人臉識別”未支援活體檢測，驗證方式存在風險，是否繼續？/translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="380"/>
        <source>Continue</source>
        <translation>繼續錄入</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="467"/>
        <source>Confirm whether clean all the features?</source>
        <translation>確認是否清除所有特徵？</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="468"/>
        <source>Confirm Clean</source>
        <translation>確認清空</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="471"/>
        <location filename="../src/contentpane.cpp" line="474"/>
        <source>Confirm whether delete the features selected?</source>
        <translation>確認是否刪除選取的特徵？</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="472"/>
        <source>Confirm Delete</source>
        <translation>確認刪除</translation>
    </message>
    <message>
        <source>Feature Delete</source>
        <translation type="vanished">特征删除</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="514"/>
        <source>Please select the feature you want to delete.</source>
        <translation>請選擇您要刪除的功能。</translation>
    </message>
    <message>
        <source>Delete successfully</source>
        <translation type="vanished">删除成功</translation>
    </message>
    <message>
        <source>The result of delete:</source>
        <translation type="vanished">删除结果：</translation>
    </message>
    <message>
        <source>Clean Failed: </source>
        <translation type="vanished">清空失败：</translation>
    </message>
    <message>
        <source>Clean successfully</source>
        <translation type="vanished">清空成功</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="819"/>
        <source>DBus calling error</source>
        <translation>DBus調用發生錯誤</translation>
    </message>
    <message>
        <source>Delete all selected features successfully</source>
        <translation type="obsolete">所有选择的特征删除成功</translation>
    </message>
    <message>
        <source>Delete Result</source>
        <translation type="obsolete">删除结果</translation>
    </message>
    <message>
        <source>Clean Successfully</source>
        <translation type="obsolete">清空成功</translation>
    </message>
    <message>
        <source>Clean Result</source>
        <translation type="vanished">清空结果</translation>
    </message>
    <message>
        <source>Feature Verify</source>
        <translation type="vanished">特征验证</translation>
    </message>
    <message>
        <source>Please select the feature you want to verify.</source>
        <translation type="vanished">请选择您想要验证的特征</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="740"/>
        <location filename="../src/contentpane.cpp" line="788"/>
        <source>Rename Successfully</source>
        <translation>重新命名成功</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="804"/>
        <source>Rename Result</source>
        <translation>重新命名結果</translation>
    </message>
    <message>
        <source>Delete Failed</source>
        <translation type="obsolete">删除失败</translation>
    </message>
    <message>
        <source>Clean Failed</source>
        <translation type="obsolete">清空失败</translation>
    </message>
    <message>
        <source>Rename Failed</source>
        <translation type="obsolete">重命名失败</translation>
    </message>
    <message>
        <source>Fingerprint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>Fingervein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Hardware Verification</source>
        <translation type="obsolete">硬件验证</translation>
    </message>
    <message>
        <source>Software Verification</source>
        <translation type="obsolete">软件验证</translation>
    </message>
    <message>
        <source>Mix Verification</source>
        <translation type="obsolete">混合验证</translation>
    </message>
    <message>
        <source>Other Verification</source>
        <translation type="obsolete">其他验证</translation>
    </message>
    <message>
        <source>Serial</source>
        <translation type="obsolete">串口</translation>
    </message>
    <message>
        <source>USB</source>
        <translation type="obsolete">USB</translation>
    </message>
    <message>
        <source>PCIE</source>
        <translation type="obsolete">PCIE</translation>
    </message>
    <message>
        <source>Device Storage</source>
        <translation type="obsolete">设备存储</translation>
    </message>
    <message>
        <source>OS Storage</source>
        <translation type="obsolete">系统存储</translation>
    </message>
    <message>
        <source>Mix Storage</source>
        <translation type="obsolete">混合存储</translation>
    </message>
    <message>
        <source>Hardware Identification</source>
        <translation type="obsolete">硬件识别</translation>
    </message>
    <message>
        <source>Software Identification</source>
        <translation type="obsolete">软件识别</translation>
    </message>
    <message>
        <source>Mix Identification</source>
        <translation type="obsolete">混合识别</translation>
    </message>
    <message>
        <source>Other Identification</source>
        <translation type="obsolete">其他识别</translation>
    </message>
    <message>
        <source> list</source>
        <translation type="obsolete">列表</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="178"/>
        <source>List</source>
        <translation>清單</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="89"/>
        <location filename="../src/contentpane.cpp" line="474"/>
        <location filename="../src/contentpane.cpp" line="514"/>
        <location filename="../src/contentpane.cpp" line="605"/>
        <location filename="../src/contentpane.cpp" line="795"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="382"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Please input a feature name</source>
        <translation type="obsolete">请输入特征名称</translation>
    </message>
    <message>
        <source>Feature Rename</source>
        <translation type="obsolete">特征重命名</translation>
    </message>
    <message>
        <source>Please input a new name for the feature:</source>
        <translation type="vanished">請輸入特徵的新名稱：</translation>
    </message>
    <message>
        <source>Permission is required. Please authenticate yourself to continue</source>
        <translation type="obsolete">需要授权！请先进行认证以继续操作</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="obsolete">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="obsolete">D-Bus 调用错误</translation>
    </message>
    <message>
        <source>Failed to enroll</source>
        <translation type="obsolete">录入失败</translation>
    </message>
    <message>
        <source>Device encounters an error</source>
        <translation type="obsolete">设备遇到错误</translation>
    </message>
    <message>
        <source>Operation timeout</source>
        <translation type="obsolete">操作超时</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="827"/>
        <source>Device is busy</source>
        <translation>設備忙</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="831"/>
        <source>No such device</source>
        <translation>設備不存在</translation>
    </message>
    <message>
        <location filename="../src/contentpane.cpp" line="835"/>
        <source>Permission denied</source>
        <translation>沒有許可權</translation>
    </message>
    <message>
        <source>Search Result</source>
        <translation type="obsolete">搜索结果</translation>
    </message>
    <message>
        <source>Failed to get notify message</source>
        <translation type="obsolete">读取操作信息失败</translation>
    </message>
    <message>
        <source>In progress, please wait...</source>
        <translation type="obsolete">操作中，请稍后...</translation>
    </message>
    <message>
        <source>Match successfully</source>
        <translation type="obsolete">匹配成功</translation>
    </message>
    <message>
        <source>Not Match</source>
        <translation type="obsolete">不匹配</translation>
    </message>
    <message>
        <source>Failed to match</source>
        <translation type="obsolete">匹配失败</translation>
    </message>
    <message>
        <source>Found the matching features:</source>
        <translation type="obsolete">搜索到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features:
</source>
        <translation type="obsolete">搜索到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features: 
</source>
        <translation type="obsolete">查找到匹配的特征:</translation>
    </message>
    <message>
        <source>Found the matching features name: </source>
        <translation type="obsolete">搜索到匹配的特征名称：</translation>
    </message>
    <message>
        <source>No matching features Found</source>
        <translation type="obsolete">未搜索到匹配的特征</translation>
    </message>
    <message>
        <source>Found! Username: %1, Feature name: %2</source>
        <translation type="obsolete">搜索成功！用户名：%1，特征名称：%2</translation>
    </message>
    <message>
        <source>Not Found</source>
        <translation type="obsolete">未搜索到</translation>
    </message>
</context>
<context>
    <name>EnumToString</name>
    <message>
        <location filename="../src/customtype.cpp" line="101"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="103"/>
        <source>Fingervein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="105"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="107"/>
        <source>Face</source>
        <translation>人臉</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="109"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="117"/>
        <source>Hardware Verification</source>
        <translation>硬體驗證</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="119"/>
        <source>Software Verification</source>
        <translation>軟體驗證</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="121"/>
        <source>Mix Verification</source>
        <translation>混合驗證</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="123"/>
        <source>Other Verification</source>
        <translation>其他驗證</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="131"/>
        <source>Device Storage</source>
        <translation>設備存儲</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="133"/>
        <source>OS Storage</source>
        <translation>系統存儲</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="135"/>
        <source>Mix Storage</source>
        <translation>混合存儲</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="143"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="145"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="147"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="149"/>
        <source>Any</source>
        <translation>任意類型</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="151"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="159"/>
        <source>Hardware Identification</source>
        <translation>硬體識別</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="161"/>
        <source>Software Identification</source>
        <translation>軟體識別</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="163"/>
        <source>Mix Identification</source>
        <translation>混合識別</translation>
    </message>
    <message>
        <location filename="../src/customtype.cpp" line="165"/>
        <source>Other Identification</source>
        <translation>其他識別</translation>
    </message>
</context>
<context>
    <name>InputDialog</name>
    <message>
        <location filename="../src/inputdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/inputdialog.ui" line="272"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.ui" line="285"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.cpp" line="46"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.cpp" line="52"/>
        <source>feature name is too long</source>
        <translation>特徵名稱太長</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.cpp" line="54"/>
        <location filename="../src/inputdialog.cpp" line="59"/>
        <source>Duplicate feature name</source>
        <translation>特徵名稱重複</translation>
    </message>
    <message>
        <location filename="../src/inputdialog.cpp" line="66"/>
        <location filename="../src/inputdialog.cpp" line="70"/>
        <source>Empty feature name</source>
        <translation>空的特徵名稱</translation>
    </message>
    <message>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation type="vanished">請不要以空格作為特徵名稱的開頭或者結尾</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.ui" line="26"/>
        <location filename="../src/mainwindow.cpp" line="304"/>
        <location filename="../src/mainwindow.cpp" line="538"/>
        <location filename="../src/mainwindow.cpp" line="539"/>
        <source>Biometric Manager</source>
        <translation>生物特徵管理工具</translation>
    </message>
    <message>
        <source>Icon</source>
        <translation type="obsolete">图标</translation>
    </message>
    <message>
        <source>Dashboard</source>
        <translation type="obsolete">主面板</translation>
    </message>
    <message>
        <source>Authentication Management</source>
        <translation type="obsolete">认证管理</translation>
    </message>
    <message>
        <source>Biometric Authentication Status:</source>
        <translation type="obsolete">生物特征认证状态：</translation>
    </message>
    <message>
        <source>Biometric Authentication can take over system authentication processes which include Login, Screensaver, sudo/su and Polkit.</source>
        <translation type="obsolete">生物特征可接管系统认证过程，包括登录、锁屏、sudo/su 授权和 Polkit 提权。</translation>
    </message>
    <message>
        <source>Biometric authentication is enabled only when the biometric identification is opened, existing available devices are turned on and the user has enrolled the feature.</source>
        <translation type="obsolete">只用当开启了生物特征、存在可用设备而且该用户已经录入了特征才会启用生物特征认证</translation>
    </message>
    <message>
        <source>Device Driver Management</source>
        <translation type="obsolete">设备驱动管理</translation>
    </message>
    <message>
        <source>Fingerprint Devices Driver</source>
        <translation type="obsolete">指纹设备驱动</translation>
    </message>
    <message>
        <source>Fingervein Devices Driver</source>
        <translation type="obsolete">指静脉设备驱动</translation>
    </message>
    <message>
        <source>Iris Devices Driver</source>
        <translation type="obsolete">虹膜设备驱动</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="996"/>
        <location filename="../src/mainwindow.ui" line="1325"/>
        <location filename="../src/mainwindow.ui" line="1651"/>
        <location filename="../src/mainwindow.ui" line="1986"/>
        <location filename="../src/mainwindow.ui" line="2312"/>
        <source>Device Management</source>
        <translation>設備管理</translation>
    </message>
    <message>
        <source>Fingerprint Devices</source>
        <translation type="vanished">指纹设备</translation>
    </message>
    <message>
        <source>Fingervein Devices</source>
        <translation type="vanished">指静脉设备</translation>
    </message>
    <message>
        <source>Iris Devices</source>
        <translation type="vanished">虹膜设备</translation>
    </message>
    <message>
        <source>Fingerprint</source>
        <translation type="vanished">指纹</translation>
    </message>
    <message>
        <source>BiometricManager</source>
        <translation type="vanished">生物特徵管理</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="137"/>
        <location filename="../src/mainwindow.ui" line="167"/>
        <source>TextLabel</source>
        <translation>文本標籤</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2482"/>
        <source>Biometirc Manager</source>
        <translation>生物特徵管理</translation>
    </message>
    <message>
        <source>UserName</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="383"/>
        <location filename="../src/mainwindow.cpp" line="305"/>
        <location filename="../src/mainwindow.cpp" line="391"/>
        <source>Biometric</source>
        <translation>生物設備</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="750"/>
        <location filename="../src/mainwindow.cpp" line="395"/>
        <location filename="../src/mainwindow.cpp" line="1174"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="941"/>
        <location filename="../src/mainwindow.ui" line="1273"/>
        <location filename="../src/mainwindow.ui" line="1599"/>
        <location filename="../src/mainwindow.ui" line="1934"/>
        <location filename="../src/mainwindow.ui" line="2260"/>
        <source>No equipment available</source>
        <translation>無可用設備</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <location filename="../src/mainwindow.cpp" line="1174"/>
        <source>FingerVein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="397"/>
        <location filename="../src/mainwindow.cpp" line="1174"/>
        <source>Face</source>
        <translation>人臉識別</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="303"/>
        <source>Permisions</source>
        <translation>許可權設置</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="402"/>
        <source>After this function is enabled, it can be used for system authentication, including login, screen lock, system authorization, and Polkit.</source>
        <translation>開啟后可用於系統認證，包括登錄、鎖屏、系統授權和Polkit。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="467"/>
        <source>Biometrics will be used to:</source>
        <translation>生物識別將用於：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="512"/>
        <source>Login system</source>
        <translation>登錄系統</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="585"/>
        <source>Unlock the lock screen</source>
        <translation>解鎖鎖屏</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="658"/>
        <source>System authorization</source>
        <translation>系統授權</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="792"/>
        <source>Seletct Device</source>
        <translation>選擇設備</translation>
    </message>
    <message>
        <source>Fingerprint recognition is not available. No device supporting this feature was detected.</source>
        <translation type="vanished">指紋識別不可用，未檢測到支援此功能的設備。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1091"/>
        <source>Face Recognition </source>
        <translation>人臉識別 </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1133"/>
        <location filename="../src/mainwindow.ui" line="1462"/>
        <location filename="../src/mainwindow.ui" line="1791"/>
        <location filename="../src/mainwindow.ui" line="2123"/>
        <source>Select Device</source>
        <translation>選擇設備</translation>
    </message>
    <message>
        <source>Face recognition is not available. No device supporting this feature was detected.</source>
        <translation type="vanished">人臉識別不可用，未檢測到支援此功能的設備。</translation>
    </message>
    <message>
        <source>Fingervein recognition is not available. No device supporting this feature was detected.</source>
        <translation type="vanished">指靜脈識別不可用，未檢測到支援此功能的設備。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1749"/>
        <source>Iris Recognition</source>
        <translation>虹膜識別</translation>
    </message>
    <message>
        <source>Iric recognition is not available. No device supporting this feature was detected.</source>
        <translation type="vanished">虹膜識別不可用，未檢測到支援此功能的設備。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="2081"/>
        <source>Voiceprint</source>
        <translation>聲紋識別</translation>
    </message>
    <message>
        <source>Voiceprint recognition is not available. No device supporting this feature was detected.</source>
        <translation type="vanished">聲紋識別不可用，未檢測到支援此功能的設備。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="387"/>
        <source>Device&amp;Permission</source>
        <translation>設備與許可權</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>Permissions</source>
        <translation>許可權</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="394"/>
        <source>Password Setting</source>
        <translation>密碼設置</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="403"/>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <source>System components are certified using biometrics</source>
        <translation type="vanished">系统组件使用生物特征进行认证</translation>
    </message>
    <message>
        <source>Biometric Verification Status:</source>
        <translation type="obsolete">生物特征状态：</translation>
    </message>
    <message>
        <source>Closed</source>
        <translation type="vanished">关</translation>
    </message>
    <message>
        <source>     Devices Type</source>
        <translation type="obsolete">    设备类型</translation>
    </message>
    <message>
        <source>     All Devices</source>
        <translation type="obsolete">所有设备</translation>
    </message>
    <message>
        <source>Driver Not Found</source>
        <translation type="vanished">驱动未找到</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="1420"/>
        <source>Fingervein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="401"/>
        <location filename="../src/mainwindow.cpp" line="1175"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <source>Fatal Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>the biometric-authentication service was not started</source>
        <translation type="obsolete">&apos;biometric-authentication&apos;服务没有启动</translation>
    </message>
    <message>
        <source>API version is not compatible</source>
        <translation type="obsolete">API 版本不兼容</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="535"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <source>exit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>help</source>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <source>Opened</source>
        <translation type="vanished">开</translation>
    </message>
    <message>
        <source>Biometric Authentication can take over system authentication processes which include Login, LockScreen, sudo/su and Polkit</source>
        <translation type="vanished">生物特征可进行系统认证，包括登录、锁屏、sudo/su 授权和 Polkit 提权。</translation>
    </message>
    <message>
        <source>There is no any available biometric device or no features enrolled currently.</source>
        <translation type="obsolete">当前没有可用的生物特征设备，或者当前用户没有录入任何特征。</translation>
    </message>
    <message>
        <source>Process of using biometrics 1.Confirm that the device is connected 2.Set the connected device as the default 3. The biometric status is to be turned on. 4.Finally enter the fingerprint</source>
        <translation type="vanished"> 使用生物特征的流程 1.确认设备已连接 2.将已连接的设备设为默认 3.打开生物特征状态开关 4.最后录入指纹 </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="324"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="339"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="347"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Main menu</source>
        <translation type="vanished">功能表</translation>
    </message>
    <message>
        <source>Devices and Permissions</source>
        <translation type="vanished">设备与权限</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="545"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="550"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <source>Process of using biometrics 1.Confirm that the device is connected 2.Set the connected device as the default 3. The biometric status is to be turned on. 4.Finally enter the feature</source>
        <translation type="vanished">使用生物特征的流程 1.确认设备已连接 2.将已连接的设备设为默认 3.打开生物特征状态开关 4.最后录入生物特征</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="896"/>
        <location filename="../src/mainwindow.cpp" line="1436"/>
        <source>(default) </source>
        <translation>（預設） </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1335"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1335"/>
        <source>Status</source>
        <translation>狀態</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1335"/>
        <source>Driver</source>
        <translation>驅動</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1335"/>
        <source>Default</source>
        <translation>設為預設</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1533"/>
        <location filename="../src/mainwindow.cpp" line="1585"/>
        <location filename="../src/mainwindow.cpp" line="1594"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1587"/>
        <source>The service has restarted.</source>
        <translation>服務已重新啟動。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1593"/>
        <source>Service restart failure.</source>
        <translation>服務重啟失敗。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1767"/>
        <source>Connected</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1779"/>
        <source>Unconnected</source>
        <translation>斷開</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1783"/>
        <source>disable</source>
        <translation>禁用</translation>
    </message>
    <message>
        <source>The Service is stopped</source>
        <translation type="vanished">服務已關閉</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1532"/>
        <source>Fail to change device status</source>
        <translation>更改設備狀態失敗</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Select Device Type</source>
        <translation>選擇設備類型</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="530"/>
        <source>Restart Service</source>
        <translation>重啟服務</translation>
    </message>
    <message>
        <source>  Restart immediately  </source>
        <translation type="obsolete">立即重启</translation>
    </message>
    <message>
        <source>  Restart later  </source>
        <translation type="obsolete">稍后重启</translation>
    </message>
    <message>
        <source>The configuration has been modified. Restart the service immediately to make it effecitve?</source>
        <translation type="obsolete">配置修改成功，是否立即重启服务使其生效？</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">错误</translation>
    </message>
    <message>
        <source>Device is not connected</source>
        <translation type="obsolete">设备未连接</translation>
    </message>
    <message>
        <source>Warnning</source>
        <translation type="obsolete">警告</translation>
    </message>
    <message>
        <source>There is no available device or no features enrolled</source>
        <translation type="obsolete">没有可用设备或者没有录入特征</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="../src/messagedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="215"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../src/messagedialog.ui" line="231"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>PromptDialog</name>
    <message>
        <location filename="../src/promptdialog.ui" line="26"/>
        <source>Current Progress</source>
        <translation>當前進度</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.ui" line="326"/>
        <source>Continue</source>
        <translation>繼續錄入</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.ui" line="339"/>
        <location filename="../src/promptdialog.cpp" line="838"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">確認</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">确认</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="120"/>
        <location filename="../src/promptdialog.cpp" line="284"/>
        <location filename="../src/promptdialog.cpp" line="706"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Operations are in progress. Please wait...</source>
        <translation type="obsolete">操作中，请稍后...</translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="vanished">錄入</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="173"/>
        <source> Feature</source>
        <translation>特徵</translation>
    </message>
    <message>
        <source>Verify</source>
        <translation type="vanished">驗證</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="570"/>
        <source>In progress, please wait...</source>
        <translation>操作中，請稍後...</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="vanished">序列号</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="73"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="173"/>
        <location filename="../src/promptdialog.cpp" line="174"/>
        <source>Enroll </source>
        <translation>錄入 </translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="177"/>
        <source>Verify </source>
        <translation>驗證 </translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="180"/>
        <source>Search </source>
        <translation>搜索 </translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="325"/>
        <source>UserName</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="325"/>
        <location filename="../src/promptdialog.cpp" line="327"/>
        <source>FeatureName</source>
        <translation>特徵名稱</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="325"/>
        <location filename="../src/promptdialog.cpp" line="327"/>
        <source>Serial number</source>
        <translation>序號</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="395"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>需要授權！
請先進行認證後繼續</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="427"/>
        <source>Enroll successfully</source>
        <translation>錄入成功</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="469"/>
        <source>Verify successfully</source>
        <translation>驗證成功</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="473"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="516"/>
        <source>Search Result</source>
        <translation>搜尋結果</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="533"/>
        <source>No matching features Found</source>
        <translation>未搜索到匹配的特徵</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="730"/>
        <source>Keep looking straight at the camera.</source>
        <translation>保持直視鏡頭。</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="748"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus 調用錯誤</translation>
    </message>
    <message>
        <source>the window will be closed after two second</source>
        <translation type="vanished">窗口将在两秒后关闭</translation>
    </message>
    <message>
        <source>Device encounters an error</source>
        <translation type="obsolete">设备遇到错误</translation>
    </message>
    <message>
        <source>Operation timeout</source>
        <translation type="obsolete">操作超时</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="773"/>
        <source>Device is busy</source>
        <translation>設備忙</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="779"/>
        <source>No such device</source>
        <translation>設備不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">沒有許可權</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="804"/>
        <source>Failed to enroll</source>
        <translation>錄入失敗</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="808"/>
        <source>Failed to match</source>
        <translation>匹配失敗</translation>
    </message>
    <message>
        <location filename="../src/promptdialog.cpp" line="811"/>
        <source>Not Found</source>
        <translation>未搜索到</translation>
    </message>
    <message>
        <source>&lt;font size=&apos;2&apos;&gt;the window will be closed after two second&lt;/font&gt;</source>
        <translation type="vanished">&lt;font size=&apos;2&apos;&gt;窗口将在两秒后关闭&lt;/font&gt;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="52"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <source>Fatal Error</source>
        <translation type="vanished">錯誤</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="164"/>
        <source>the biometric-authentication service was not started</source>
        <translation>&apos;biometric-authentication&apos;服務沒有啟動</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="165"/>
        <location filename="../src/main.cpp" line="175"/>
        <source>OK</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="174"/>
        <source>API version is not compatible</source>
        <translation>API 版本不相容</translation>
    </message>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
</context>
<context>
    <name>TreeModel</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸特征</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="30"/>
        <source>Name</source>
        <translation>名稱</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="32"/>
        <location filename="../src/treemodel.cpp" line="34"/>
        <source>index</source>
        <translation>序列號</translation>
    </message>
    <message>
        <location filename="../src/treemodel.cpp" line="32"/>
        <source>username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <source>Empty feature name</source>
        <translation type="vanished">空的特徵名稱</translation>
    </message>
    <message>
        <source>feature name is too long</source>
        <translation type="vanished">特徵名稱太長</translation>
    </message>
    <message>
        <source>Duplicate feature name</source>
        <translation type="vanished">特徵名稱重複</translation>
    </message>
    <message>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation type="vanished">請不要以空格作為特徵名稱的開頭或者結尾</translation>
    </message>
</context>
</TS>
