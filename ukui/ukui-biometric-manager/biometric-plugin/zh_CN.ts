<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BiometricEnrollDialog</name>
    <message>
        <location filename="../biometricenroll.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="330"/>
        <source>Continue</source>
        <translation>继续录入</translation>
    </message>
    <message>
        <source>Continue adding</source>
        <translation type="vanished">继续录入</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="349"/>
        <location filename="../biometricenroll.cpp" line="499"/>
        <location filename="../biometricenroll.cpp" line="519"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="85"/>
        <location filename="../biometricenroll.cpp" line="396"/>
        <location filename="../biometricenroll.cpp" line="673"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="156"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="209"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="211"/>
        <source>Fingervein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="213"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="215"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="217"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="227"/>
        <source>Enroll </source>
        <translation>录入</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="227"/>
        <source> Feature</source>
        <translation>特征</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="230"/>
        <source>Verify </source>
        <translation>验证</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="233"/>
        <source>Search </source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="262"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>需要授权，请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="293"/>
        <location filename="../biometricenroll.cpp" line="493"/>
        <source>Enroll successfully</source>
        <translation>录入成功</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="364"/>
        <location filename="../biometricenroll.cpp" line="495"/>
        <source>Verify successfully</source>
        <translation>验证成功</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="368"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="696"/>
        <source>Keep looking straight at the camera.</source>
        <translation>保持直视镜头</translation>
    </message>
    <message>
        <source>Place your finger on the device button and remove. Repeat</source>
        <translation type="vanished">将手指放在设备按钮上再移开，重复此步骤</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="713"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus获取错误</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="736"/>
        <source>Device is busy</source>
        <translation>设备忙</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="741"/>
        <source>No such device</source>
        <translation>设备不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">权限不够</translation>
    </message>
</context>
<context>
    <name>BiometricMoreInfoDialog</name>
    <message>
        <location filename="../biometricmoreinfo.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="76"/>
        <source>Biometrics </source>
        <translation>生物识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="166"/>
        <source>Default device </source>
        <translation>默认设备</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="208"/>
        <source>Verify Type:</source>
        <translation>验证类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="215"/>
        <source>Bus Type:</source>
        <translation>总线类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="222"/>
        <source>Device Status:</source>
        <translation>设备状态：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="243"/>
        <source>Storage Type:</source>
        <translation>存储类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="250"/>
        <source>Identification Type:</source>
        <translation>验证类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Unconnected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="126"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="128"/>
        <source>Fingervein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="130"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="132"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="134"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="143"/>
        <source>Hardware Verification</source>
        <translation>硬件验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="145"/>
        <source>Software Verification</source>
        <translation>软件验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="147"/>
        <source>Mix Verification</source>
        <translation>混合验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="149"/>
        <source>Other Verification</source>
        <translation>其他验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="157"/>
        <source>Device Storage</source>
        <translation>设备存储</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="159"/>
        <source>OS Storage</source>
        <translation>系统存储</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="161"/>
        <source>Mix Storage</source>
        <translation>混合存储</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="169"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="171"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="173"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="175"/>
        <source>Any</source>
        <translation>任意类型</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="177"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="185"/>
        <source>Hardware Identification</source>
        <translation>硬件识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="187"/>
        <source>Software Identification</source>
        <translation>软件识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="189"/>
        <source>Mix Identification</source>
        <translation>混合识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="191"/>
        <source>Other Identification</source>
        <translation>其他识别</translation>
    </message>
</context>
<context>
    <name>Biometrics</name>
    <message>
        <source>Biometrics</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <location filename="../biometrics.cpp" line="36"/>
        <source>Login Options</source>
        <translation>登录选项</translation>
    </message>
</context>
<context>
    <name>BiometricsWidget</name>
    <message>
        <source>Biometric password</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Account password</source>
        <translation type="vanished">帐户密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="139"/>
        <source>Change password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="455"/>
        <source>(Fingerprint, face recognition, etc)</source>
        <translation>(指纹、人脸识别等)</translation>
    </message>
    <message>
        <source>Enable biometrics </source>
        <translation type="vanished">生物特征</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="208"/>
        <location filename="../biometricswidget.cpp" line="940"/>
        <location filename="../biometricswidget.cpp" line="1021"/>
        <source>(Can be used to log in, unlock the system, and authorize authentication)</source>
        <translation>(可用于登录、解锁系统及授权认证)</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="201"/>
        <location filename="../biometricswidget.cpp" line="1061"/>
        <source>Scan code login</source>
        <translation>扫码登录</translation>
        <extra-contents_path>/Login Options/Scan code login</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="59"/>
        <location filename="../biometricswidget.cpp" line="1057"/>
        <source>Login Options</source>
        <translation>登录选项</translation>
        <extra-contents_path>/Login Options/Login Options</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="113"/>
        <location filename="../biometricswidget.cpp" line="1059"/>
        <source>Password</source>
        <translation>密码</translation>
        <extra-contents_path>/Login Options/Password</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="228"/>
        <source>Bound wechat:</source>
        <translation>已绑定的微信:</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="263"/>
        <source>Bind</source>
        <translation>绑定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="322"/>
        <location filename="../biometricswidget.cpp" line="1069"/>
        <source>Security Key</source>
        <translation>安全密钥</translation>
        <extra-contents_path>/Login Options/SecurityKey</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="348"/>
        <source>Setup</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="448"/>
        <location filename="../biometricswidget.cpp" line="1063"/>
        <source>Biometric</source>
        <translation>生物识别</translation>
        <extra-contents_path>/Login Options/Biometric</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="580"/>
        <location filename="../biometricswidget.cpp" line="1065"/>
        <source>Type</source>
        <translation>类型</translation>
        <extra-contents_path>/Login Options/Type</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="698"/>
        <location filename="../biometricswidget.cpp" line="1067"/>
        <source>Device</source>
        <translation>设备</translation>
        <extra-contents_path>/Login Options/Device</extra-contents_path>
    </message>
    <message>
        <source>Add biometric feature</source>
        <translation type="vanished">添加生物密码</translation>
    </message>
    <message>
        <source>Disable this function</source>
        <translation type="vanished">禁用该功能</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="98"/>
        <source>Advanced Settings</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="243"/>
        <source>Standard</source>
        <translation>标准用户</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="245"/>
        <source>Admin</source>
        <translation type="unfinished">管理员</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="247"/>
        <source>root</source>
        <translation type="unfinished">Root</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="459"/>
        <location filename="../biometricswidget.cpp" line="465"/>
        <source>(default)</source>
        <translation>（默认）</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="472"/>
        <source>Add </source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="476"/>
        <location filename="../biometricswidget.cpp" line="481"/>
        <source>No available device was detected</source>
        <translation>未检测到可用设备</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="624"/>
        <source>&apos;Face recognition&apos; does not support live body detection, and the verification method is risky. Do you want to continue?</source>
        <translation>“人脸识别”未支持活体检测，验证方式存在风险，是否继续？</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="626"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="628"/>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="1006"/>
        <source>Binding WeChat</source>
        <translation>绑定微信</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">删除</translation>
    </message>
    <message>
        <source>Unbound</source>
        <translation type="vanished">未绑定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="1010"/>
        <source>Unbind</source>
        <translation>解除绑定</translation>
    </message>
</context>
<context>
    <name>ChangeFeatureName</name>
    <message>
        <location filename="../changefeaturename.ui" line="26"/>
        <source>Change Username</source>
        <translation>修改用户名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="65"/>
        <source>Change featurename</source>
        <translation>修改特证名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="322"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="vanished">特征名称</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="315"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="25"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Name already in use, change another one.</source>
        <translation type="vanished">特征名重复</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="32"/>
        <source>Duplicate feature name</source>
        <translation>特征名重复</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="34"/>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation>请不要在名称首或者末输入空格</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="36"/>
        <source>Empty feature name</source>
        <translation>名称不能为空</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="38"/>
        <source>feature name is too long</source>
        <translation>特征名称太长</translation>
    </message>
    <message>
        <source> rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="81"/>
        <source> Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="82"/>
        <source> name</source>
        <translation>名称</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <location filename="../changepwddialog.ui" line="130"/>
        <source>Change Pwd</source>
        <translation>更改密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="317"/>
        <source>Pwd type</source>
        <translation>密码类型</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="395"/>
        <source>Cur pwd</source>
        <translation>当前密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="440"/>
        <source>New pwd</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="485"/>
        <source>New pwd sure</source>
        <translation>新密码确认</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="605"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="627"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="64"/>
        <source>Change pwd</source>
        <translation>更改密码</translation>
    </message>
    <message>
        <source>Cur pwd checking!</source>
        <translation type="vanished">当前密码检查!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="172"/>
        <source>General Pwd</source>
        <translation>通用密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="178"/>
        <location filename="../changepwddialog.cpp" line="389"/>
        <source>Current Password</source>
        <translation>当前密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="179"/>
        <location filename="../changepwddialog.cpp" line="390"/>
        <location filename="../changepwddialog.cpp" line="398"/>
        <source>New Password</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="180"/>
        <location filename="../changepwddialog.cpp" line="391"/>
        <location filename="../changepwddialog.cpp" line="399"/>
        <source>New Password Identify</source>
        <translation>新密码确认</translation>
    </message>
    <message>
        <source>Authentication failed, input authtok again!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Pwd input error, re-enter!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="327"/>
        <source>Contains illegal characters!</source>
        <translation>含有非法字符！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="329"/>
        <source>Same with old pwd</source>
        <translation>与旧密码相同</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">密码长度至少大于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">密码长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to more than 5 character!</source>
        <translation type="vanished">密码长度需要大于5个字符！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="245"/>
        <location filename="../changepwddialog.cpp" line="365"/>
        <source>Inconsistency with pwd</source>
        <translation>与新密码不同</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="102"/>
        <source>Change password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="107"/>
        <location filename="../changeuserpwd.cpp" line="120"/>
        <location filename="../changeuserpwd.cpp" line="602"/>
        <source>Current Pwd</source>
        <translation>当前密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="149"/>
        <location filename="../changeuserpwd.cpp" line="160"/>
        <location filename="../changeuserpwd.cpp" line="603"/>
        <location filename="../changeuserpwd.cpp" line="611"/>
        <source>New Pwd</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="189"/>
        <location filename="../changeuserpwd.cpp" line="193"/>
        <location filename="../changeuserpwd.cpp" line="604"/>
        <location filename="../changeuserpwd.cpp" line="612"/>
        <source>Sure Pwd</source>
        <translation>确认新密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="251"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="255"/>
        <location filename="../changeuserpwd.cpp" line="422"/>
        <location filename="../changeuserpwd.cpp" line="491"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="418"/>
        <source>Same with old pwd</source>
        <translation>与旧密码相同</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="474"/>
        <source>Pwd Changed Succes</source>
        <translation>密码修改成功</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="481"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>密码输入错误,重新输入!</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="788"/>
        <source>current pwd cannot be empty!</source>
        <translation>当前密码不能为空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="793"/>
        <source>new pwd cannot be empty!</source>
        <translation>新密码不能为空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="798"/>
        <source>sure pwd cannot be empty!</source>
        <translation>确认密码不能为空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="364"/>
        <location filename="../changeuserpwd.cpp" line="675"/>
        <source>Inconsistency with pwd</source>
        <translation>与新密码不同</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="634"/>
        <source>Contains illegal characters!</source>
        <translation>含有非法字符！</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="40"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="42"/>
        <source>FingerVein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="44"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="46"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="50"/>
        <source>KCM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="52"/>
        <location filename="../biometricdeviceinfo.cpp" line="54"/>
        <source>UKey</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="48"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
</context>
<context>
    <name>PasswdCheckUtil</name>
    <message>
        <location filename="../passwdcheckutil.cpp" line="159"/>
        <source>The password is shorter than %1 characters</source>
        <translation>密码少于 %1 个字符</translation>
    </message>
    <message>
        <source>The password contains less than %ld character classes</source>
        <translation type="obsolete">密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="162"/>
        <source>The password contains less than %1 character classes</source>
        <translation>密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="165"/>
        <source>The password is the same as the old one</source>
        <translation>与旧密码相同</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="168"/>
        <source>The password contains the user name in some form</source>
        <translation>密码包含了某种形式的用户名</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="171"/>
        <source>The password differs with case changes only</source>
        <translation>密码仅包含大小写变更</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="174"/>
        <source>The password is too similar to the old one</source>
        <translation>密码与原来的太相似</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="177"/>
        <source>The password is a palindrome</source>
        <translation>密码是一个回文</translation>
    </message>
</context>
<context>
    <name>QRCodeEnrollDialog</name>
    <message>
        <location filename="../qrcodeenroll.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="102"/>
        <location filename="../qrcodeenroll.cpp" line="151"/>
        <source>Bind Wechat Account</source>
        <translation>绑定微信账号</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="112"/>
        <location filename="../qrcodeenroll.cpp" line="203"/>
        <location filename="../qrcodeenroll.cpp" line="663"/>
        <source>Please use wechat scanning code for binding.</source>
        <translation>请使用微信扫码，进行绑定。</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="202"/>
        <location filename="../qrcodeenroll.cpp" line="267"/>
        <location filename="../qrcodeenroll.cpp" line="333"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="513"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="154"/>
        <source>Verify Wechat Account</source>
        <translation>验证微信账号</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="157"/>
        <source>Search Wechat Account</source>
        <translation>搜索微信账号</translation>
    </message>
    <message>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation type="vanished">需要授权，请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="113"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="225"/>
        <source>Bind Successfully</source>
        <translation>绑定成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="261"/>
        <location filename="../qrcodeenroll.cpp" line="347"/>
        <source>Verify successfully</source>
        <translation>验证成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="270"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="344"/>
        <source>The wechat account is bound successfully!</source>
        <translation>微信账号绑定成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="396"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="518"/>
        <source>Abnormal network</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <source>Network anomalies</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="537"/>
        <location filename="../qrcodeenroll.cpp" line="543"/>
        <source>Binding failure</source>
        <translation>绑定失败</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="vanished">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="obsolete">D-Bus获取错误</translation>
    </message>
    <message>
        <source>Device is busy</source>
        <translation type="vanished">设备忙</translation>
    </message>
    <message>
        <source>No such device</source>
        <translation type="vanished">设备不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">权限不够</translation>
    </message>
</context>
<context>
    <name>SecurityKeySetDlg</name>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="119"/>
        <source>Security Key</source>
        <translation>安全密钥</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="142"/>
        <source>Please insert the security key into the USB port</source>
        <translation>请将安全密钥插入USB端口</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="161"/>
        <source>Enter security key password</source>
        <translation>输入安全密钥密码</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="168"/>
        <location filename="../securitykeysetdlg.cpp" line="901"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="210"/>
        <source>The security key password has been set, please keep it properly. To unbind the security key, click Unbind.
</source>
        <translation>安全密钥密码已设置，请妥善保管。如需解绑安全密钥请点击&quot;解绑&quot;。</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="214"/>
        <source>Unbind</source>
        <translation>解绑</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="233"/>
        <source>Bind Security Key</source>
        <translation>绑定安全密钥</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="251"/>
        <source>You will bind your security key so that you can log in to the system as &apos;%1&apos;. If you need to bind, please click &apos;OK&apos;.
</source>
        <translation>将绑定你的安全密钥以便以“%1”身份登录系统。如需绑定请点击“确定”。</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="264"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="106"/>
        <location filename="../securitykeysetdlg.cpp" line="536"/>
        <location filename="../securitykeysetdlg.cpp" line="547"/>
        <location filename="../securitykeysetdlg.cpp" line="557"/>
        <location filename="../securitykeysetdlg.cpp" line="567"/>
        <location filename="../securitykeysetdlg.cpp" line="574"/>
        <location filename="../securitykeysetdlg.cpp" line="579"/>
        <source>Security key binding failed!</source>
        <translation>安全密钥绑定失败！</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="769"/>
        <source>Security key unbinding failed!</source>
        <translation>安全密钥解绑失败！</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="268"/>
        <location filename="../securitykeysetdlg.cpp" line="783"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
</context>
</TS>
