<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>BiometricEnrollDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">對話框</translation>
    </message>
    <message>
        <source>Continue adding</source>
        <translation type="vanished">繼續錄入</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="342"/>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="361"/>
        <location filename="../biometricenroll.cpp" line="510"/>
        <location filename="../biometricenroll.cpp" line="530"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="85"/>
        <location filename="../biometricenroll.cpp" line="407"/>
        <location filename="../biometricenroll.cpp" line="684"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="159"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="212"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="214"/>
        <source>Fingervein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="216"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="218"/>
        <source>Face</source>
        <translation>人臉</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="220"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="230"/>
        <source>Enroll </source>
        <translation>錄入</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="230"/>
        <source> Feature</source>
        <translation> 特徵</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="233"/>
        <source>Verify </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="236"/>
        <source>Search </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enroll</source>
        <translation type="vanished">錄入</translation>
    </message>
    <message>
        <source>Verify</source>
        <translation type="vanished">驗證</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="265"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>需要授權，請先進行認證以繼續操作</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="296"/>
        <location filename="../biometricenroll.cpp" line="504"/>
        <source>Enroll successfully</source>
        <translation>錄入成功</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="371"/>
        <location filename="../biometricenroll.cpp" line="506"/>
        <source>Verify successfully</source>
        <translation>驗證成功</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="375"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="707"/>
        <source>Keep looking straight at the camera.</source>
        <translation>保持直視鏡頭</translation>
    </message>
    <message>
        <source>Place your finger on the device button and remove. Repeat</source>
        <translation type="vanished">将手指放在设备按钮上再移开，重复此步骤</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="724"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus獲取錯誤</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="748"/>
        <source>Device is busy</source>
        <translation>設備忙</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="753"/>
        <source>No such device</source>
        <translation>設備不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">許可權不夠</translation>
    </message>
</context>
<context>
    <name>BiometricMoreInfoDialog</name>
    <message>
        <location filename="../biometricmoreinfo.ui" line="26"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="76"/>
        <source>Biometrics </source>
        <translation>生物識別 </translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="166"/>
        <source>Default device </source>
        <translation>默認設備 </translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="208"/>
        <source>Verify Type:</source>
        <translation>驗證類型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="215"/>
        <source>Bus Type:</source>
        <translation>匯流類型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="222"/>
        <source>Device Status:</source>
        <translation>裝置狀態：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="243"/>
        <source>Storage Type:</source>
        <translation>儲存類型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="250"/>
        <source>Identification Type:</source>
        <translation>驗證類型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Connected</source>
        <translation>已連接</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Unconnected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="126"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="128"/>
        <source>Fingervein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="130"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="132"/>
        <source>Face</source>
        <translation>人臉</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="134"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="143"/>
        <source>Hardware Verification</source>
        <translation>硬體驗證</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="145"/>
        <source>Software Verification</source>
        <translation>軟體驗證</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="147"/>
        <source>Mix Verification</source>
        <translation>混合驗證</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="149"/>
        <source>Other Verification</source>
        <translation>其他驗證</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="157"/>
        <source>Device Storage</source>
        <translation>設備存儲</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="159"/>
        <source>OS Storage</source>
        <translation>系統存儲</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="161"/>
        <source>Mix Storage</source>
        <translation>混合存儲</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="169"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="171"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="173"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="175"/>
        <source>Any</source>
        <translation>任意類型</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="177"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="185"/>
        <source>Hardware Identification</source>
        <translation>硬體識別</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="187"/>
        <source>Software Identification</source>
        <translation>軟體識別</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="189"/>
        <source>Mix Identification</source>
        <translation>混合識別</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="191"/>
        <source>Other Identification</source>
        <translation>其他識別</translation>
    </message>
</context>
<context>
    <name>Biometrics</name>
    <message>
        <source>Biometrics</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <location filename="../biometrics.cpp" line="36"/>
        <source>Login Options</source>
        <translation>登錄選項</translation>
    </message>
</context>
<context>
    <name>BiometricsWidget</name>
    <message>
        <source>Biometric password</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Account password</source>
        <translation type="vanished">帐户密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="139"/>
        <source>Change password</source>
        <translation>修改密碼</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="455"/>
        <location filename="../biometricswidget.cpp" line="938"/>
        <source>(Fingerprint, face recognition, etc)</source>
        <translation>（指紋、人臉識別等）</translation>
    </message>
    <message>
        <source>Enable biometrics </source>
        <translation type="vanished">生物特征</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="208"/>
        <location filename="../biometricswidget.cpp" line="935"/>
        <location filename="../biometricswidget.cpp" line="1020"/>
        <source>(Can be used to log in, unlock the system, and authorize authentication)</source>
        <translation>（可用於登錄、解鎖系統及授權認證）</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="201"/>
        <location filename="../biometricswidget.cpp" line="930"/>
        <location filename="../biometricswidget.cpp" line="1060"/>
        <source>Scan code login</source>
        <translation>掃碼登錄</translation>
        <extra-contents_path>/Login Options/Scan code login</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="59"/>
        <location filename="../biometricswidget.cpp" line="1056"/>
        <source>Login Options</source>
        <translation>登錄選項</translation>
        <extra-contents_path>/Login Options/Login Options</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="113"/>
        <location filename="../biometricswidget.cpp" line="1058"/>
        <source>Password</source>
        <translation>密碼</translation>
        <extra-contents_path>/Login Options/Password</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="228"/>
        <source>Bound wechat:</source>
        <translation>已綁定的微信：</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="263"/>
        <source>Bind</source>
        <translation>綁定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="322"/>
        <location filename="../biometricswidget.cpp" line="1068"/>
        <source>Security Key</source>
        <translation>安全秘鑰</translation>
        <extra-contents_path>/Login Options/SecurityKey</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="348"/>
        <source>Setup</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="448"/>
        <location filename="../biometricswidget.cpp" line="1062"/>
        <source>Biometric</source>
        <translation>生物識別</translation>
        <extra-contents_path>/Login Options/Biometric</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="580"/>
        <location filename="../biometricswidget.cpp" line="1064"/>
        <source>Type</source>
        <translation>類型</translation>
        <extra-contents_path>/Login Options/Type</extra-contents_path>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="698"/>
        <location filename="../biometricswidget.cpp" line="1066"/>
        <source>Device</source>
        <translation>設備</translation>
        <extra-contents_path>/Login Options/Device</extra-contents_path>
    </message>
    <message>
        <source>Add biometric feature</source>
        <translation type="vanished">添加生物密码</translation>
    </message>
    <message>
        <source>Disable this function</source>
        <translation type="vanished">禁用该功能</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="106"/>
        <source>Advanced Settings</source>
        <translation>高級設置</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="259"/>
        <source>Standard</source>
        <translation>標準使用者</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="261"/>
        <source>Admin</source>
        <translation>管理員</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="263"/>
        <source>root</source>
        <translation>Root</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="472"/>
        <location filename="../biometricswidget.cpp" line="478"/>
        <source>(default)</source>
        <translation>（預設）</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="485"/>
        <source>Add </source>
        <translation>添加 </translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="490"/>
        <location filename="../biometricswidget.cpp" line="495"/>
        <source>No available device was detected</source>
        <translation>未檢測到可用設備</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="641"/>
        <source>&apos;Face recognition&apos; does not support live body detection, and the verification method is risky. Do you want to continue?</source>
        <translation>“人臉識別”未支援活體檢測，驗證方式存在風險，是否繼續？</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="643"/>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="645"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="171"/>
        <location filename="../biometricswidget.cpp" line="1005"/>
        <source>Binding WeChat</source>
        <translation>綁定微信</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">删除</translation>
    </message>
    <message>
        <source>Unbound</source>
        <translation type="vanished">未绑定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="1009"/>
        <source>Unbind</source>
        <translation>解除綁定</translation>
    </message>
</context>
<context>
    <name>ChangeFeatureName</name>
    <message>
        <source>Change Username</source>
        <translation type="vanished">修改使用者名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="26"/>
        <location filename="../changefeaturename.ui" line="65"/>
        <source>Change featurename</source>
        <translation>修改特證名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="328"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="vanished">特征名称</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="321"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="25"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Name already in use, change another one.</source>
        <translation type="vanished">該使用者名已存在，請更改。</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="32"/>
        <location filename="../changefeaturename.cpp" line="36"/>
        <source>Duplicate feature name</source>
        <translation>特徵名重複</translation>
    </message>
    <message>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation type="vanished">請不要在名稱首或者未輸入空格</translation>
    </message>
    <message>
        <source> rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="43"/>
        <location filename="../changefeaturename.cpp" line="49"/>
        <source>Empty feature name</source>
        <translation>空的特徵名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="45"/>
        <source>feature name is too long</source>
        <translation>特徵名過長</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="95"/>
        <location filename="../changefeaturename.cpp" line="96"/>
        <location filename="../changefeaturename.cpp" line="105"/>
        <source> Rename</source>
        <translation> 重新命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="97"/>
        <source> name</source>
        <translation> 名稱</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <location filename="../changepwddialog.ui" line="130"/>
        <source>Change Pwd</source>
        <translation>更改密碼</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="317"/>
        <source>Pwd type</source>
        <translation>密碼類型</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="395"/>
        <source>Cur pwd</source>
        <translation>當前密碼</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="440"/>
        <source>New pwd</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="485"/>
        <source>New pwd sure</source>
        <translation>新密碼確認</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="605"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="627"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="64"/>
        <source>Change pwd</source>
        <translation>更改密碼</translation>
    </message>
    <message>
        <source>Cur pwd checking!</source>
        <translation type="vanished">当前密码检查!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="172"/>
        <source>General Pwd</source>
        <translation>通用密碼</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="178"/>
        <location filename="../changepwddialog.cpp" line="389"/>
        <source>Current Password</source>
        <translation>當前密碼</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="179"/>
        <location filename="../changepwddialog.cpp" line="390"/>
        <location filename="../changepwddialog.cpp" line="398"/>
        <source>New Password</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="180"/>
        <location filename="../changepwddialog.cpp" line="391"/>
        <location filename="../changepwddialog.cpp" line="399"/>
        <source>New Password Identify</source>
        <translation>新密碼確認</translation>
    </message>
    <message>
        <source>Authentication failed, input authtok again!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Pwd input error, re-enter!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="327"/>
        <source>Contains illegal characters!</source>
        <translation>含有非法字元！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="329"/>
        <source>Same with old pwd</source>
        <translation>與舊密碼相同</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">密码长度至少大于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">密码长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to more than 5 character!</source>
        <translation type="vanished">密码长度需要大于5个字符！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="245"/>
        <location filename="../changepwddialog.cpp" line="365"/>
        <source>Inconsistency with pwd</source>
        <translation>與新密碼不同</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">關閉</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="102"/>
        <source>Change password</source>
        <translation>修改密碼</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="107"/>
        <location filename="../changeuserpwd.cpp" line="536"/>
        <source>Current Pwd</source>
        <translation>當前密碼</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="120"/>
        <source>Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="140"/>
        <location filename="../changeuserpwd.cpp" line="537"/>
        <location filename="../changeuserpwd.cpp" line="545"/>
        <source>New Pwd</source>
        <translation>新密碼</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="168"/>
        <location filename="../changeuserpwd.cpp" line="538"/>
        <location filename="../changeuserpwd.cpp" line="546"/>
        <source>Sure Pwd</source>
        <translation>確認新密碼</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="218"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="222"/>
        <location filename="../changeuserpwd.cpp" line="356"/>
        <location filename="../changeuserpwd.cpp" line="425"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="352"/>
        <source>Same with old pwd</source>
        <translation>與舊密碼相同</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="408"/>
        <source>Pwd Changed Succes</source>
        <translation>密碼修改成功&gt;</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="415"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>密碼輸入錯誤，重新輸入！</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="722"/>
        <source>current pwd cannot be empty!</source>
        <translation>當前密碼不能為空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="727"/>
        <source>new pwd cannot be empty!</source>
        <translation>新密碼不能為空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="732"/>
        <source>sure pwd cannot be empty!</source>
        <translation>確認密碼不能為空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="331"/>
        <location filename="../changeuserpwd.cpp" line="609"/>
        <source>Inconsistency with pwd</source>
        <translation>與新密碼不同</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="568"/>
        <source>Contains illegal characters!</source>
        <translation>含有非法字元！</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="40"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="42"/>
        <source>FingerVein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="44"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="46"/>
        <source>Face</source>
        <translation>人臉</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="48"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="50"/>
        <source>KCM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="52"/>
        <location filename="../biometricdeviceinfo.cpp" line="54"/>
        <source>UKey</source>
        <translation>安全秘鑰</translation>
    </message>
</context>
<context>
    <name>PasswdCheckUtil</name>
    <message>
        <location filename="../passwdcheckutil.cpp" line="159"/>
        <source>The password is shorter than %1 characters</source>
        <translation>密碼少於 %1 個字元</translation>
    </message>
    <message>
        <source>The password contains less than %ld character classes</source>
        <translation type="obsolete">密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="162"/>
        <source>The password contains less than %1 character classes</source>
        <translation>密碼包含的字元類型少於 %1 種</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="165"/>
        <source>The password is the same as the old one</source>
        <translation>與舊密碼相同</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="168"/>
        <source>The password contains the user name in some form</source>
        <translation>密碼包含了某種形式的使用者名</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="171"/>
        <source>The password differs with case changes only</source>
        <translation>密碼僅包含大小寫變更</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="174"/>
        <source>The password is too similar to the old one</source>
        <translation>密碼與原來的太相似</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="177"/>
        <source>The password is a palindrome</source>
        <translation>密碼是一個回文</translation>
    </message>
</context>
<context>
    <name>QRCodeEnrollDialog</name>
    <message>
        <source>Form</source>
        <translation type="vanished">類型</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="99"/>
        <location filename="../qrcodeenroll.cpp" line="152"/>
        <source>Bind Wechat Account</source>
        <translation>綁定微信帳號</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="109"/>
        <location filename="../qrcodeenroll.cpp" line="204"/>
        <location filename="../qrcodeenroll.cpp" line="665"/>
        <source>Please use wechat scanning code for binding.</source>
        <translation>請使用微信掃碼，進行綁定。</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="205"/>
        <location filename="../qrcodeenroll.cpp" line="270"/>
        <location filename="../qrcodeenroll.cpp" line="336"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="515"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="155"/>
        <source>Verify Wechat Account</source>
        <translation>驗證微信帳號</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="113"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="158"/>
        <source>Search Wechat Account</source>
        <translation>搜索微信帳號</translation>
    </message>
    <message>
        <source>Permission is required.Please authenticate yourself to continue</source>
        <translation type="vanished">需要授权，请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="228"/>
        <source>Bind Successfully</source>
        <translation>綁定成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="264"/>
        <location filename="../qrcodeenroll.cpp" line="350"/>
        <source>Verify successfully</source>
        <translation>驗證成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="273"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="347"/>
        <source>The wechat account is bound successfully!</source>
        <translation>微信賬號綁定成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="398"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="520"/>
        <source>Abnormal network</source>
        <translation>網路異常</translation>
    </message>
    <message>
        <source>Network anomalies</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="539"/>
        <location filename="../qrcodeenroll.cpp" line="545"/>
        <source>Binding failure</source>
        <translation>綁定失敗</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="vanished">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="obsolete">D-Bus获取错误</translation>
    </message>
    <message>
        <source>Device is busy</source>
        <translation type="vanished">设备忙</translation>
    </message>
    <message>
        <source>No such device</source>
        <translation type="vanished">设备不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">权限不够</translation>
    </message>
</context>
<context>
    <name>SecurityKeySetDlg</name>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="107"/>
        <location filename="../securitykeysetdlg.cpp" line="559"/>
        <location filename="../securitykeysetdlg.cpp" line="570"/>
        <location filename="../securitykeysetdlg.cpp" line="580"/>
        <location filename="../securitykeysetdlg.cpp" line="590"/>
        <location filename="../securitykeysetdlg.cpp" line="597"/>
        <location filename="../securitykeysetdlg.cpp" line="602"/>
        <source>Security key binding failed!</source>
        <translation>安全秘鑰綁定失敗</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="120"/>
        <source>Security Key</source>
        <translation>安全秘鑰</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="143"/>
        <source>Please insert the security key into the USB port</source>
        <translation>請將安全金鑰插入埠</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="162"/>
        <source>Enter security key password</source>
        <translation>輸入安全金鑰密碼</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="169"/>
        <location filename="../securitykeysetdlg.cpp" line="924"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="211"/>
        <source>The security key password has been set, please keep it properly. To unbind the security key, click Unbind.
</source>
        <translation>安全金鑰密碼已設置，請妥善保管。 如需解綁安全密鑰請點擊“解綁”。</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="221"/>
        <location filename="../securitykeysetdlg.cpp" line="222"/>
        <location filename="../securitykeysetdlg.cpp" line="225"/>
        <location filename="../securitykeysetdlg.cpp" line="226"/>
        <source>Unbind</source>
        <translation>解除綁定</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="248"/>
        <source>Bind Security Key</source>
        <translation>綁定安全金鑰</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="270"/>
        <source>You will bind your security key so that you can log in to the system as &apos;%1&apos;. If you need to bind, please click &apos;OK&apos;.
</source>
        <translation>將綁定你的安全金鑰以便以「%1」 身份登錄系統。 如需綁定請點擊「確定」。</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="287"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="291"/>
        <location filename="../securitykeysetdlg.cpp" line="806"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../securitykeysetdlg.cpp" line="792"/>
        <source>Security key unbinding failed!</source>
        <translation>安全秘鑰綁定失敗</translation>
    </message>
</context>
</TS>
