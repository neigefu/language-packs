<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Peony::PrintPicturesPlugin</name>
    <message>
        <location filename="../print-pictures-plugin.cpp" line="79"/>
        <source>Print</source>
        <translation>图片打印</translation>
    </message>
    <message>
        <location filename="../print-pictures-plugin.h" line="46"/>
        <source>Peony-Qt Printing Extension</source>
        <translation>打印扩展</translation>
    </message>
    <message>
        <location filename="../print-pictures-plugin.h" line="47"/>
        <source>Printing Extension</source>
        <translation>打印扩展</translation>
    </message>
    <message>
        <source>Printing Extension.</source>
        <translation type="vanished">打印扩展</translation>
    </message>
</context>
</TS>
