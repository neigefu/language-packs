<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Peony::PrintPicturesPlugin</name>
    <message>
        <location filename="../print-pictures-plugin.cpp" line="79"/>
        <source>Print</source>
        <translation>圖片列印</translation>
    </message>
    <message>
        <location filename="../print-pictures-plugin.h" line="46"/>
        <source>Peony-Qt Printing Extension</source>
        <translation>列印擴展</translation>
    </message>
    <message>
        <location filename="../print-pictures-plugin.h" line="47"/>
        <source>Printing Extension</source>
        <translation>列印擴展</translation>
    </message>
    <message>
        <source>Printing Extension.</source>
        <translation type="vanished">列印擴展</translation>
    </message>
</context>
</TS>
