<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::PrintPicturesPlugin</name>
    <message>
        <location filename="../print-pictures-plugin.cpp" line="79"/>
        <source>Print</source>
        <translation>པར་རིས་གཏག་པར་</translation>
    </message>
    <message>
        <location filename="../print-pictures-plugin.h" line="46"/>
        <source>Peony-Qt Printing Extension</source>
        <translation>བསྐྱར་དཔར་རྒྱ་སྐྱེད་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../print-pictures-plugin.h" line="47"/>
        <source>Printing Extension</source>
        <translation>བསྐྱར་དཔར་རྒྱ་སྐྱེད་གཏོང་དགོས།</translation>
    </message>
</context>
</TS>
