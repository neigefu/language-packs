<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ChatMsg</name>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="139"/>
        <source>Resend</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="140"/>
        <source>Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="141"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="142"/>
        <source>Open Directory</source>
        <translation>ᠭᠠᠷᠠᠭ ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="143"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="970"/>
        <source>Save As</source>
        <translation>ᠦᠭᠡᠷ᠎ᠡ ᠭᠠᠵᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="144"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="145"/>
        <source>Clear All</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭᠰᠠᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="218"/>
        <source>File</source>
        <translation>ᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="227"/>
        <source>Folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="236"/>
        <source>Screen Shot</source>
        <translation>ᠵᠢᠷᠤᠭᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="245"/>
        <source>History Message</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="262"/>
        <source>Send</source>
        <translation>ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="507"/>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠢᠯᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ IP ᠴᠦᠷᠬᠡᠯᠡᠭᠰᠡᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="629"/>
        <source>Can not write file</source>
        <translation>ᠹᠠᠢᠯ ᠵᠢ ᠪᠢᠴᠢᠵᠦ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="826"/>
        <source>No such file or directory!</source>
        <translation>ᠲᠤᠰ ᠹᠡᠢᠯ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠭᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="990"/>
        <source>Delete the currently selected message?</source>
        <translation>ᠤᠳᠤ ᠶ᠋ᠢᠨ ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="992"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1021"/>
        <source>No</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1072"/>
        <source>folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="993"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1022"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1019"/>
        <source>Clear all current messages?</source>
        <translation>ᠤᠳᠤᠬᠢ ᠪᠦᠬᠦ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1042"/>
        <source>Send Files</source>
        <translation>ᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1069"/>
        <source>Send Folders</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="273"/>
        <source>Delete</source>
        <translation>ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">ᠪᠦᠬᠦᠨ ᠢ ᠠᠷᠢᠯᠭᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="174"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="177"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="297"/>
        <source>All</source>
        <translation>ᠪᠦᠭᠦᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="182"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="184"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="298"/>
        <source>File</source>
        <translation>ᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="188"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="193"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="299"/>
        <source>Image/Video</source>
        <translation>ᠵᠢᠷᠤᠭ\ᠸᠢᠳᠢᠣ᠋</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="198"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="200"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="300"/>
        <source>Link</source>
        <translation>ᠬᠤᠯᠪᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="205"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="206"/>
        <source>canael</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="211"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="216"/>
        <source>sure</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠢ᠋ᠶ᠋ᠡᠨ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="127"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="234"/>
        <source>Batch delete</source>
        <translation>ᠪᠥᠭᠡᠮ&#x202f;ᠢ᠋ᠶ᠋ᠡᠷ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="53"/>
        <source>Chat content</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠦ᠌ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="128"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="236"/>
        <source>Clear all messages</source>
        <translation>ᠪᠦᠬᠦ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="149"/>
        <source>Chat Content</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠦ᠌ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="223"/>
        <source>more</source>
        <translation>ᠨᠡᠩ ᠠᠷᠪᠢᠨ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="274"/>
        <source>Choose Delete</source>
        <translation>ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠪᠦᠭᠡᠮ ᠢ᠋ᠶ᠋ᠡᠷ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="275"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="276"/>
        <source>Open Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="648"/>
        <source>No such file or directory!</source>
        <translation>ᠲᠤᠰ ᠹᠡᠢᠯ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠭᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="707"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="943"/>
        <source>Delete the currently selected message?</source>
        <translation>ᠤᠳᠤ ᠶ᠋ᠢᠨ ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="709"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="748"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="945"/>
        <source>No</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="710"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="749"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="946"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="746"/>
        <source>Clear all current messages?</source>
        <translation>ᠤᠳᠤᠬᠢ ᠪᠦᠬᠦ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Search</source>
        <translation type="vanished">ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/controller/control.cpp" line="286"/>
        <source>Anonymous</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="154"/>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="181"/>
        <source>Messages</source>
        <translation>ᠪᠢᠴᠢᠭ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="190"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="220"/>
        <source>Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="229"/>
        <source>IP Address</source>
        <translation>IP ᠬᠠᠶᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="239"/>
        <source>Nickname</source>
        <translation>ᠵᠡᠭᠦᠯᠲᠡ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="253"/>
        <source>edit</source>
        <translation>ᠨᠠᠢ᠌ᠷᠠᠭᠤᠯᠤᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="339"/>
        <source>Add</source>
        <translation>ᠵᠡᠭᠦᠯᠳᠡ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="98"/>
        <source>Start Chat</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠦᠷᠨᠢᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="99"/>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="156"/>
        <source>Set to Top</source>
        <translation>ᠲᠡᠷᠢᠭᠦᠨ ᠳ᠋ᠦ᠍ ᠪᠤᠯᠠᠭᠠᠨ ᠲᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="100"/>
        <source>Change Nickname</source>
        <translation>ᠨᠠᠢᠵᠠ ᠶ᠋ᠢᠨ ᠵᠡᠭᠦᠯᠳᠡ ᠵᠢ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="101"/>
        <source>View Info</source>
        <translation>ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="102"/>
        <source>Delete Friend</source>
        <translation>ᠨᠠᠢᠵᠠ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="159"/>
        <source>Cancel the Top</source>
        <translation>ᠲᠡᠷᠢᠭᠦᠨ ᠳ᠋ᠦ᠍ ᠪᠠᠢᠯᠭᠠᠬᠤ ᠪᠠᠨ ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <location filename="../../src/view/kyview.cpp" line="135"/>
        <source>Messages</source>
        <translation>ᠪᠢᠴᠢᠭ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <source>Search</source>
        <translation type="vanished">ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="156"/>
        <source>Modify Name</source>
        <translation>ᠨᠡᠷ᠎ᠡ ᠪᠠᠨ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="165"/>
        <source>Open Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="159"/>
        <source>Set Username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠲᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="160"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="211"/>
        <source>Please enter username</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠶ᠋ᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="163"/>
        <source>Change nickname</source>
        <translation>ᠵᠡᠭᠦᠯᠳᠡ ᠪᠡᠨ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="164"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="213"/>
        <source>Please enter friend nickname</source>
        <translation>ᠨᠠᠢᠵᠠ ᠶ᠋ᠢᠨ ᠵᠡᠭᠦᠯᠳᠡ ᠪᠡᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="167"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="172"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="179"/>
        <source>Skip</source>
        <translation>ᠦᠰᠦᠷᠴᠤ᠌ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="216"/>
        <source>The length of user name is less than 20 words</source>
        <translation>the length of user name is less than 20 words</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="218"/>
        <source>Please do not enter special characters</source>
        <translation>ᠪᠢᠳᠡᠬᠡᠢ ᠤᠨᠴᠠᠭᠠᠢ ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠲᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../../src/main.cpp" line="138"/>
        <source>Messages</source>
        <translation>ᠪᠢᠴᠢᠭ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/search_msg_delegate.cpp" line="45"/>
        <source> relevant chat records</source>
        <translation> ᠵᠤᠷᠪᠤᠰ ᠬᠤᠯᠪᠤᠭᠳᠠᠬᠤ ᠶᠠᠷᠢᠯᠴᠠᠭᠰᠠᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="123"/>
        <source>Start Chat</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠦᠷᠨᠢᠭᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="124"/>
        <source>Set to Top</source>
        <translation>ᠲᠡᠷᠢᠭᠦᠨ ᠳ᠋ᠦ᠍ ᠪᠤᠯᠠᠭᠠᠨ ᠲᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="125"/>
        <source>Change Nickname</source>
        <translation>ᠨᠠᠢᠵᠠ ᠶ᠋ᠢᠨ ᠵᠡᠭᠦᠯᠳᠡ ᠵᠢ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="126"/>
        <source>View Info</source>
        <translation>ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="127"/>
        <source>Delete Friend</source>
        <translation>ᠨᠠᠢᠵᠠ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="165"/>
        <source>Friend</source>
        <translation>ᠨᠠᠢᠵᠠ</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="169"/>
        <source>Chat Record</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠦ᠌ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="143"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠦ᠋ᠨ ᠪᠠᠭ᠎ᠠ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="144"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="147"/>
        <source>Messages</source>
        <translation>ᠪᠢᠴᠢᠭ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="103"/>
        <source>File Save Directory</source>
        <translation>ᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠰᠠᠨ ᠭᠠᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="116"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="236"/>
        <source>Change Directory</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="124"/>
        <source>Clear All Chat Messages</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠦ᠌ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="132"/>
        <source>Clear the Cache</source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Modified successfully</source>
        <translation type="vanished">ᠵᠠᠰᠠᠪᠤᠷᠢ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠳ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="255"/>
        <source>Modified Successfully</source>
        <translation>ᠵᠠᠰᠠᠭᠠᠳ ᠳᠡᠢᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="296"/>
        <source>Clear all messages？</source>
        <translation>ᠪᠦᠭᠦᠳᠡ ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ ᠦ᠌ ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="299"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="326"/>
        <source>No</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="298"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="327"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="242"/>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠠᠮ ᠨᠢ ᠵᠦᠪᠬᠡᠨ ᠭᠠᠷᠴᠠᠭ ᠳᠤᠤᠷᠠᠬᠢ ᠨᠢᠭᠡᠨ ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠪᠤᠰᠤ ᠭᠠᠷᠴᠠᠭ!</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="310"/>
        <source>Cleared</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠴᠡᠪᠡᠷᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="324"/>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>ᠵᠢᠷᠤᠭ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠡᠰᠡᠬᠦ/ ᠸᠢᠳᠢᠤ᠋/ ᠲᠸᠺᠰᠲ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="340"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="364"/>
        <source>Clean Up Complete</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠦ ᠪᠠᠷᠠᠪᠠ</translation>
    </message>
    <message>
        <source>Clean up complete</source>
        <translation>ᠢᠯᠭᠠᠷᠠᠭᠤᠯᠤᠨ ᠪᠡᠶᠡᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="391"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="398"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="406"/>
        <source>Please do not save the file in this directory</source>
        <translation>ᠪᠢᠳᠡᠬᠡᠢ ᠹᠠᠢᠯ ᠢ᠋ ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ ᠲᠤ᠌ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="155"/>
        <source>Ignore</source>
        <translation>ᠬᠠᠰᠤᠬᠤ ᠪᠠᠨ ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="122"/>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="166"/>
        <source>Messages</source>
        <translation>ᠪᠢᠴᠢᠭ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">ᠲᠣᠪᠶᠣᠭ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="44"/>
        <source>Options</source>
        <translation>ᠲᠥᠰᠦᠯ ᠢ ᠰᠣᠩᠭᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="58"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="141"/>
        <source>Settings</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="60"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="139"/>
        <source>Theme</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="62"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="137"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="64"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="135"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="66"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="143"/>
        <source>Quit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="74"/>
        <source>Follow the Theme</source>
        <translation>ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ ᠢ᠋ ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="78"/>
        <source>Light Theme</source>
        <translation>ᠬᠦᠶᠦᠬᠡᠨ ᠦᠩᠭᠡ ᠲᠠᠢ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="82"/>
        <source>Dark Theme</source>
        <translation>ᠭᠦᠨ ᠦᠩᠭᠡ ᠵᠢᠨ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="194"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠄</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="195"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠪᠢᠴᠢᠭ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠨᠢ ᠬᠡᠰᠡᠭ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠤᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤᠤ ᠦᠰᠦᠭ ᠪᠢᠴᠢᠭ ᠤ᠋ᠨ ᠶᠠᠷᠢᠯᠴᠠᠭ᠎ᠠ ᠵᠢᠴᠢ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠴᠢᠳᠠᠮᠵᠢ ᠵᠢ ᠬᠠᠩᠭᠠᠨ᠎ᠠ᠃ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ ᠲᠠᠢ ᠪᠤᠯ ᠤᠯᠠᠨ ᠬᠦᠮᠦᠨᠲᠡᠢ ᠬᠠᠮᠳᠤ ᠪᠡᠷ ᠰᠤᠯᠢᠯᠴᠠᠬᠤ᠂ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠳᠠᠷᠬᠠᠭᠠᠬᠤ ᠵᠢ ᠵᠡᠷᠬᠡ ᠪᠡᠷ ᠶᠠᠪᠤᠭᠳᠠᠭᠤᠯᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠨ᠎ᠡ.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠪᠠ ᠳᠡᠮᠵᠢᠯᠬᠡ᠎ᠶ᠋ᠢᠨ ᠪᠦᠯᠬᠦᠮ᠄</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.h" line="68"/>
        <source>Messages</source>
        <translation>ᠪᠢᠴᠢᠭ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
</TS>
