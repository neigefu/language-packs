<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ChatMsg</name>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="137"/>
        <source>Resend</source>
        <translation>重新发送</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="138"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="139"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="140"/>
        <source>Open Directory</source>
        <translation>打开目录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="141"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="954"/>
        <source>Save As</source>
        <translation>另存为</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="142"/>
        <source>Delete</source>
        <translation>删除记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="143"/>
        <source>Clear All</source>
        <translation>清空聊天记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="216"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="225"/>
        <source>Folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="234"/>
        <source>Screen Shot</source>
        <translation>截图</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="243"/>
        <source>History Message</source>
        <translation>历史记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="260"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="498"/>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>消息发送失败，请检查IP是否连接成功！</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="611"/>
        <source>Can not write file</source>
        <translation>无法写入文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="808"/>
        <source>No such file or directory!</source>
        <translation>此文件或文件夹不存在</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="974"/>
        <source>Delete the currently selected message?</source>
        <translation>是否删除当前所选记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="977"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1005"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1054"/>
        <source>folder</source>
        <translation>文件夹</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="976"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1004"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1002"/>
        <source>Clear all current messages?</source>
        <translation>是否清空当前所有记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1024"/>
        <source>Send Files</source>
        <translation>发送文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1051"/>
        <source>Send Folders</source>
        <translation>发送文件夹</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="289"/>
        <source>Delete</source>
        <translation>删除记录</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空聊天记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="175"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="187"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="317"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="369"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="477"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="198"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="205"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="210"/>
        <source>Image/Video</source>
        <translation>图片/视频</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="218"/>
        <source>Link</source>
        <translation>链接</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="224"/>
        <source>canael</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="229"/>
        <source>sure</source>
        <translation>删除所选</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="239"/>
        <source>DeleteMenu</source>
        <translation>删除菜单</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="131"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="250"/>
        <source>Batch delete</source>
        <translation>批量删除</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="47"/>
        <source>Chat content</source>
        <translation>聊天内容</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="132"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="252"/>
        <source>Clear all messages</source>
        <translation>清空全部消息</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="158"/>
        <source>Chat Content</source>
        <translation>聊天内容</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="290"/>
        <source>Choose Delete</source>
        <translation>批量删除记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="291"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="292"/>
        <source>Open Directory</source>
        <translation>打开目录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="696"/>
        <source>No such file or directory!</source>
        <translation>此文件或文件夹不存在</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="757"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="967"/>
        <source>Delete the currently selected message?</source>
        <translation>是否删除当前所选记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="760"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="798"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="970"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="759"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="797"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="969"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="795"/>
        <source>Clear all current messages?</source>
        <translation>是否清空当前所有记录？</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/controller/control.cpp" line="284"/>
        <source>Anonymous</source>
        <translation>匿名</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="150"/>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="177"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="186"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="215"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="224"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="234"/>
        <source>Nickname</source>
        <translation>备注名</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="332"/>
        <source>Add</source>
        <translation>添加备注</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="96"/>
        <source>Start Chat</source>
        <translation>发起聊天</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="97"/>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="150"/>
        <source>Set to Top</source>
        <translation>设为置顶</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="98"/>
        <source>Change Nickname</source>
        <translation>修改好友备注</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="99"/>
        <source>View Info</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="100"/>
        <source>Delete Friend</source>
        <translation>删除好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="153"/>
        <source>Cancel the Top</source>
        <translation>取消置顶</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <location filename="../../src/view/kyview.cpp" line="133"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="174"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="187"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="262"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="349"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="378"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="160"/>
        <source>Modify Name</source>
        <translation>修改名字</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="169"/>
        <source>Open Directory</source>
        <translation>打开目录</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="157"/>
        <source>Set Username</source>
        <translation>设置用户名</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="158"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="209"/>
        <source>Please enter username</source>
        <translation>请输入用户名</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="161"/>
        <source>Change nickname</source>
        <translation>修改备注</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="162"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="211"/>
        <source>Please enter friend nickname</source>
        <translation>请输入好友备注</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="165"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="170"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="177"/>
        <source>Skip</source>
        <translation>跳过</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="214"/>
        <source>The length of user name is less than 20 words</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="216"/>
        <source>Please do not enter special characters</source>
        <translation>请勿输入特殊字符作为用户名</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/search_msg_delegate.cpp" line="44"/>
        <source> relevant chat records</source>
        <translation> 条相关聊天记录</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="122"/>
        <source>Start Chat</source>
        <translation>发起聊天</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="123"/>
        <source>Set to Top</source>
        <translation>设为置顶</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="124"/>
        <source>Change Nickname</source>
        <translation>修改好友备注</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="125"/>
        <source>View Info</source>
        <translation>查看资料</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="126"/>
        <source>Delete Friend</source>
        <translation>删除好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="164"/>
        <source>Friend</source>
        <translation>好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="168"/>
        <source>Chat Record</source>
        <translation>聊天记录</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="142"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="143"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="146"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="103"/>
        <source>File Save Directory</source>
        <translation>文件保存目录</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="116"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="232"/>
        <source>Change Directory</source>
        <translation>更改目录</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="123"/>
        <source>Clear All Chat Messages</source>
        <translation>清空聊天记录</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="130"/>
        <source>Clear the Cache</source>
        <translation>清理缓存</translation>
    </message>
    <message>
        <source>Modified successfully</source>
        <translation type="vanished">修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="251"/>
        <source>Modified Successfully</source>
        <translation>修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="292"/>
        <source>Clear all messages？</source>
        <translation>是否清空所有聊天记录？</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="295"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="322"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="294"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="323"/>
        <source>Yes</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="238"/>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>保存路径只能是家目录下的一个非隐藏目录!</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="306"/>
        <source>Cleared</source>
        <translation>已清空</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="320"/>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>是否清理图片/视频/文档等缓存信息？</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="336"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="360"/>
        <source>Clean Up Complete</source>
        <translation>清理完成</translation>
    </message>
    <message>
        <source>Clean up complete</source>
        <translation type="vanished">清理完成</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="387"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="394"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="402"/>
        <source>Please do not save the file in this directory</source>
        <translation>请不要将文件保存在此目录中</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="157"/>
        <source>Ignore</source>
        <translation>忽略消息</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="124"/>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="168"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="53"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="67"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="148"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="69"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="146"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="71"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="144"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="73"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="142"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="81"/>
        <source>Follow the Theme</source>
        <translation>跟随主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="85"/>
        <source>Light Theme</source>
        <translation>浅色主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="89"/>
        <source>Dark Theme</source>
        <translation>深色主题</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="301"/>
        <source>Version: </source>
        <translation>版本号： </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="306"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>麒麟传书提供局域网内的文字聊天以及文件传输功能，不需要搭建服务器，支持多人同时交互，收发并行。</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="411"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="431"/>
        <source>Service &amp; Support: </source>
        <translation>服务与支持团队： </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.h" line="65"/>
        <source>Messages</source>
        <translation>传书</translation>
    </message>
</context>
</TS>
