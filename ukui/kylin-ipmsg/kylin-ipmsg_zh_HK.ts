<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>ChatMsg</name>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="139"/>
        <source>Resend</source>
        <translation>重新發送</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="140"/>
        <source>Copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="141"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="142"/>
        <source>Open Directory</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="143"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="970"/>
        <source>Save As</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="144"/>
        <source>Delete</source>
        <translation>刪除記錄</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="145"/>
        <source>Clear All</source>
        <translation>清空聊天記錄</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="218"/>
        <source>File</source>
        <translation>檔</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="227"/>
        <source>Folder</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="236"/>
        <source>Screen Shot</source>
        <translation>截圖</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="245"/>
        <source>History Message</source>
        <translation>歷史記錄</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="262"/>
        <source>Send</source>
        <translation>發送</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="507"/>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>消息發送失敗，請檢查IP是否連接成功！</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="629"/>
        <source>Can not write file</source>
        <translation>無法寫入檔案</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="826"/>
        <source>No such file or directory!</source>
        <translation>此檔或資料夾不存在</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="990"/>
        <source>Delete the currently selected message?</source>
        <translation>是否刪除目前所選取記錄？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="992"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1021"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1072"/>
        <source>folder</source>
        <translation>資料夾</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="993"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1022"/>
        <source>Yes</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1019"/>
        <source>Clear all current messages?</source>
        <translation>是否清空當前所有記錄？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1042"/>
        <source>Send Files</source>
        <translation>發送檔</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1069"/>
        <source>Send Folders</source>
        <translation>發送資料夾</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="273"/>
        <source>Delete</source>
        <translation>刪除記錄</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空聊天记录</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="174"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="177"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="297"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="182"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="184"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="298"/>
        <source>File</source>
        <translation>檔</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="188"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="193"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="299"/>
        <source>Image/Video</source>
        <translation>圖片/視頻</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="198"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="200"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="300"/>
        <source>Link</source>
        <translation>連結</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="205"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="206"/>
        <source>canael</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="211"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="216"/>
        <source>sure</source>
        <translation>刪除選取選取</translation>
    </message>
    <message>
        <source>DeleteMenu</source>
        <translation type="vanished">刪除功能表</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="127"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="234"/>
        <source>Batch delete</source>
        <translation>批量刪除</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="53"/>
        <source>Chat content</source>
        <translation>聊天內容</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="128"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="236"/>
        <source>Clear all messages</source>
        <translation>清空全部消息</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="149"/>
        <source>Chat Content</source>
        <translation>聊天內容</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="223"/>
        <source>more</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="274"/>
        <source>Choose Delete</source>
        <translation>批量刪除記錄</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="275"/>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="276"/>
        <source>Open Directory</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="648"/>
        <source>No such file or directory!</source>
        <translation>此檔或資料夾不存在</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="707"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="943"/>
        <source>Delete the currently selected message?</source>
        <translation>是否刪除目前所選取記錄？</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="709"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="748"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="945"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="710"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="749"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="946"/>
        <source>Yes</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="746"/>
        <source>Clear all current messages?</source>
        <translation>是否清空當前所有記錄？</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/controller/control.cpp" line="286"/>
        <source>Anonymous</source>
        <translation>匿名</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="154"/>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="181"/>
        <source>Messages</source>
        <translation>傳書</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="190"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="220"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="229"/>
        <source>IP Address</source>
        <translation>IP位址</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="239"/>
        <source>Nickname</source>
        <translation>備註名</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="253"/>
        <source>edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="339"/>
        <source>Add</source>
        <translation>添加備註</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="98"/>
        <source>Start Chat</source>
        <translation>發起聊天</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="99"/>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="156"/>
        <source>Set to Top</source>
        <translation>設為置頂</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="100"/>
        <source>Change Nickname</source>
        <translation>修改好友備註</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="101"/>
        <source>View Info</source>
        <translation>查看資料</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="102"/>
        <source>Delete Friend</source>
        <translation>刪除好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="159"/>
        <source>Cancel the Top</source>
        <translation>取消置頂</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <location filename="../../src/view/kyview.cpp" line="135"/>
        <source>Messages</source>
        <translation>傳書</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="156"/>
        <source>Modify Name</source>
        <translation>修改名字</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="165"/>
        <source>Open Directory</source>
        <translation>打開目錄</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="159"/>
        <source>Set Username</source>
        <translation>設置使用者名</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="160"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="211"/>
        <source>Please enter username</source>
        <translation>請輸入使用者名</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="163"/>
        <source>Change nickname</source>
        <translation>修改備註</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="164"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="213"/>
        <source>Please enter friend nickname</source>
        <translation>請輸入好友備註</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="167"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="172"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="179"/>
        <source>Skip</source>
        <translation>跳過</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="216"/>
        <source>The length of user name is less than 20 words</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="218"/>
        <source>Please do not enter special characters</source>
        <translation>請勿輸入特殊字元作為使用者名</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../../src/main.cpp" line="138"/>
        <source>Messages</source>
        <translation>傳書</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/search_msg_delegate.cpp" line="45"/>
        <source> relevant chat records</source>
        <translation> 條相關聊天記錄</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="123"/>
        <source>Start Chat</source>
        <translation>發起聊天</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="124"/>
        <source>Set to Top</source>
        <translation>設為置頂</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="125"/>
        <source>Change Nickname</source>
        <translation>修改好友備註</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="126"/>
        <source>View Info</source>
        <translation>查看資料</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="127"/>
        <source>Delete Friend</source>
        <translation>刪除好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="165"/>
        <source>Friend</source>
        <translation>好友</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="169"/>
        <source>Chat Record</source>
        <translation>聊天記錄</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="143"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="144"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="147"/>
        <source>Messages</source>
        <translation>傳書</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="103"/>
        <source>File Save Directory</source>
        <translation>檔案保存目錄</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="116"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="236"/>
        <source>Change Directory</source>
        <translation>更改目錄</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="124"/>
        <source>Clear All Chat Messages</source>
        <translation>清空聊天記錄</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="132"/>
        <source>Clear the Cache</source>
        <translation>清理緩存</translation>
    </message>
    <message>
        <source>Modified successfully</source>
        <translation type="vanished">修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="255"/>
        <source>Modified Successfully</source>
        <translation>修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="296"/>
        <source>Clear all messages？</source>
        <translation>是否清空所有聊天記錄？</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="299"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="326"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="298"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="327"/>
        <source>Yes</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="242"/>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>保存路徑只能是家目錄下的一個非隱藏目錄！</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="310"/>
        <source>Cleared</source>
        <translation>已清空</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="324"/>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>是否清理圖片/視頻/文檔等緩存資訊？</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="340"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="364"/>
        <source>Clean Up Complete</source>
        <translation>清理完成</translation>
    </message>
    <message>
        <source>Clean up complete</source>
        <translation type="vanished">清理完成</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="391"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="398"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="406"/>
        <source>Please do not save the file in this directory</source>
        <translation>請不要將檔案儲存在此目錄中</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="155"/>
        <source>Ignore</source>
        <translation>忽略消息</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="122"/>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="166"/>
        <source>Messages</source>
        <translation>傳書</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">功能表</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="44"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="58"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="141"/>
        <source>Settings</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="60"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="139"/>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="62"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="137"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="64"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="135"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="66"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="143"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="74"/>
        <source>Follow the Theme</source>
        <translation>跟隨主題</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="78"/>
        <source>Light Theme</source>
        <translation>淺色主題</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="82"/>
        <source>Dark Theme</source>
        <translation>深色主題</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="194"/>
        <source>Version: </source>
        <translation>版本號： </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="195"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>麒麟傳書提供局域網內的文字聊天以及檔傳輸功能，不需要搭建伺服器，支援多人同時交互，收發並行。</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服務與支持團隊： </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.h" line="68"/>
        <source>Messages</source>
        <translation>傳書</translation>
    </message>
</context>
</TS>
