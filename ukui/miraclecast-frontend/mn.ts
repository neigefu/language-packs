<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Dialog</name>
    <message>
        <location filename="dialog.ui" line="14"/>
        <source>cast</source>
        <translatorcomment>投屏</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="dialog.ui" line="39"/>
        <location filename="dialog.cpp" line="15"/>
        <source>allow</source>
        <translatorcomment>允许</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="dialog.ui" line="52"/>
        <source>XX request cast</source>
        <translation>xx request cast</translation>
    </message>
    <message>
        <location filename="dialog.ui" line="71"/>
        <source>refuse</source>
        <translatorcomment>拒绝</translatorcomment>
        <translation>ᠳᠡᠪᠴᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="dialog.ui" line="84"/>
        <source>12345678</source>
        <translation>12345678</translation>
    </message>
    <message>
        <location filename="dialog.cpp" line="35"/>
        <location filename="dialog.cpp" line="73"/>
        <source>ok</source>
        <translatorcomment>确认</translatorcomment>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="src/widget.ui" line="26"/>
        <source>Widget</source>
        <translation>Widget</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="67"/>
        <source>请求投屏</source>
        <translation>The 10000-10</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="102"/>
        <source>无操作10S后自动退出</source>
        <translation>Auto-10S Auto-10S Auto-10S</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="143"/>
        <source>禁止</source>
        <translation>10. The</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="166"/>
        <source>允许</source>
        <translation>you can also sy</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="230"/>
        <source>请输入如下PIN码：********</source>
        <translation>1.2.1.1.1.1.1.1.1.1</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="265"/>
        <source>配对中</source>
        <translation>In the 2006-20</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="288"/>
        <source>请直接重新连接，或重新开关投屏功能后再次连接</source>
        <translation>if you are using a 32-in-10-1000 or 1000-1000-1000-100</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="326"/>
        <location filename="src/widget.ui" line="439"/>
        <location filename="src/widget.ui" line="544"/>
        <source>退出</source>
        <translation>The 200</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="378"/>
        <source>多媒体协商中/成功/失败</source>
        <translation>In the 2006-2007 2007, the 20</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="401"/>
        <source>播放器准备启动</source>
        <translation>The 3D 1000-200</translation>
    </message>
    <message>
        <location filename="src/widget.ui" line="503"/>
        <source>投屏设备已断开</source>
        <translation>the device is not in the system.</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="113"/>
        <source>request cast.</source>
        <oldsource>request cast</oldsource>
        <translatorcomment>请求投屏。</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠬᠤ.</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="196"/>
        <source>The device is disconnected！</source>
        <translatorcomment>投屏设备已断开！</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠰᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="224"/>
        <source>request cast.Please enter the pin code on mobile</source>
        <translatorcomment>请求投屏。请在手机端输入以下PIN码</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠪᠠ. ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠪᠡᠨ ᠳᠠᠷᠠᠭᠠᠬᠢPIN ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="291"/>
        <source>Pairing failed, please reconnect...</source>
        <translatorcomment>配对失败,请重新连接...</translatorcomment>
        <translation>ᠬᠤᠤᠰᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠴᠦᠷᠬᠡᠯᠡᠬᠡᠷᠡᠢt...</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="305"/>
        <source>The environment is initializing, please wait</source>
        <translatorcomment>投屏环境初始化中，请等待</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="309"/>
        <source>request cast.Connecting......</source>
        <translatorcomment>请求投屏。连接中......</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠪᠠ᠂ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ......</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="320"/>
        <source>request cast.Multimedia connect failed</source>
        <translatorcomment>请求投屏。多媒体连接失败</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ᠣᠯᠠᠨ ᠮᠧᠳ᠋ᠢᠶ᠎ᠠ ᠳ᠋ᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="src/widget.cpp" line="326"/>
        <source>request cast.Multimedia connect succeeded</source>
        <translatorcomment>请求投屏。多媒体连接成功</translatorcomment>
        <translation>ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠭᠠᠬᠤ ᠪᠡᠷ ᠭᠤᠶᠤᠴᠢᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ᠣᠯᠠᠨ ᠮᠧᠳ᠋ᠢᠶ᠎ᠠ ᠳ᠋ᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>widget2</name>
    <message>
        <location filename="widget2.ui" line="14"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
    <message>
        <location filename="widget2.ui" line="26"/>
        <source>PPO A5请求投屏.。。</source>
        <translation>the PPO A5 is 100%.</translation>
    </message>
    <message>
        <location filename="widget2.ui" line="39"/>
        <source>允许</source>
        <translation>you can also sy</translation>
    </message>
    <message>
        <location filename="widget2.ui" line="52"/>
        <source>拒绝</source>
        <translation>if you are using</translation>
    </message>
</context>
</TS>
