<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BaseStyle</name>
    <message>
        <source>Services and Supports</source>
        <translation type="vanished">服务与支持</translation>
    </message>
</context>
<context>
    <name>ContactPage</name>
    <message>
        <location filename="../src/contact_page.cpp" line="49"/>
        <source>Contact Us</source>
        <translation>ᠪᠢᠳᠡᠨ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="61"/>
        <source>Support Team</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ</translation>
    </message>
    <message>
        <source>KylinOS Support Team</source>
        <translation type="vanished">服务与支持团队</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="79"/>
        <source>Tel</source>
        <translation>ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="95"/>
        <source>E-mail</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="111"/>
        <source>Kylin WeChat Official Account</source>
        <translation>ᠤᠯᠠᠨ ᠨᠡᠢᠳᠡᠮ ᠵᠢᠨ ᠳᠠᠪᠴᠠᠩ ᠴᠢ ᠯᠢᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ</translation>
    </message>
</context>
<context>
    <name>ContactPageStyle</name>
    <message>
        <location filename="../src/contact_page_style.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="72"/>
        <location filename="../src/contact_page_style.cpp" line="26"/>
        <source>Contact Us</source>
        <translation>ᠪᠢᠳᠡᠨ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="242"/>
        <location filename="../src/contact_page_style.cpp" line="37"/>
        <source>Support Team</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="269"/>
        <source>KylinOS Support Team</source>
        <translation>ᠴᠢ ᠯᠢᠨOS Support Team</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="337"/>
        <location filename="../src/contact_page_style.cpp" line="53"/>
        <source>Tel</source>
        <translation>ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="364"/>
        <source>400-089-1870</source>
        <translation>400-089-1870</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="432"/>
        <location filename="../src/contact_page_style.cpp" line="69"/>
        <source>E-mail</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="459"/>
        <source>support@kylinos.cn</source>
        <translation>support@kylinos.cn</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="586"/>
        <source>Kylin WeChat Official Account</source>
        <translation>ᠤᠯᠠᠨ ᠨᠡᠢᠳᠡᠮ ᠵᠢᠨ ᠳᠠᠪᠴᠠᠩ ᠴᠢ ᠯᠢᠨ ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ</translation>
    </message>
</context>
<context>
    <name>CustomNaviButton</name>
    <message>
        <location filename="../src/customNaviButton.cpp" line="40"/>
        <source>Intro</source>
        <translatorcomment>软件介绍</translatorcomment>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="45"/>
        <source>Message</source>
        <translatorcomment>留言咨询</translatorcomment>
        <translation>ᠦᠬᠡ ᠦᠯᠡᠳᠡᠬᠡᠵᠤ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="50"/>
        <source>Contact</source>
        <translatorcomment>联系我们</translatorcomment>
        <translation>ᠪᠢᠳᠡᠨ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="55"/>
        <source>Guidance</source>
        <translatorcomment>自助支持</translatorcomment>
        <translation>ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="60"/>
        <source>Customer</source>
        <translatorcomment>在线客服</translatorcomment>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠤᠴᠢᠳ ᠲᠤ᠌ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>DIYSupportPage</name>
    <message>
        <location filename="../src/diysupport_page.cpp" line="92"/>
        <source>Click </source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="99"/>
        <source>to know more about support</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="111"/>
        <source>，to KylinOS Official Web</source>
        <translation>ᠴᠢ ᠯᠢᠨOS ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠦᠰᠦᠷᠴᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="116"/>
        <source>Users can solve problems through the ways below.</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳᠤᠤᠷᠠᠬᠢ ᠠᠷᠭ᠎ᠠ ᠪᠡᠷ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="132"/>
        <source>To KylinOS Official Web</source>
        <translation>to KylinOS Official Web</translation>
    </message>
    <message>
        <source>Go to the KylinOS Official Web</source>
        <translation type="vanished">前往麒麟官网</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="139"/>
        <source>Access the Web, find the answers of the 
normal problems.</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ
ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠮᠡᠷᠬᠡᠵᠢᠯᠪᠤᠯᠤᠨ ᠳᠤᠰᠠᠯᠠᠯᠴᠠᠭ᠎ᠠ ᠡᠷᠢᠬᠦ.</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="156"/>
        <source>Guidance</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="162"/>
        <source>Check Manuals to solve the problem.</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ ᠵᠢ ᠯᠠᠪᠯᠠᠬᠤ᠂ ᠳᠡᠮᠵᠢᠯᠬᠡ ᠪᠠ ᠳᠤᠰᠠᠯᠠᠯᠴᠠᠭ᠎ᠠ ᠡᠷᠢᠬᠦ.</translation>
    </message>
</context>
<context>
    <name>FileItemInit</name>
    <message>
        <location filename="../src/file_item_init.cpp" line="41"/>
        <source>Del</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>GuidancePageStyle</name>
    <message>
        <location filename="../src/guidance_page_style.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="160"/>
        <location filename="../src/guidance_page_style.cpp" line="37"/>
        <source>Click </source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="167"/>
        <location filename="../src/guidance_page_style.cpp" line="45"/>
        <source>to know more about support</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="174"/>
        <location filename="../src/guidance_page_style.cpp" line="51"/>
        <source>，to KylinOS Official Web</source>
        <translation>ᠴᠢ ᠯᠢᠨOS ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠦᠰᠦᠷᠴᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="248"/>
        <location filename="../src/guidance_page_style.cpp" line="54"/>
        <source>You can get your answers through the ways below</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠠᠷᠭ᠎ᠠ ᠪᠡᠷ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠳᠡᠮᠵᠢᠯᠬᠡ ᠪᠤᠯᠤᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="446"/>
        <location filename="../src/guidance_page_style.cpp" line="63"/>
        <source>To KylinOS Official Web</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠴᠢᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="484"/>
        <location filename="../src/guidance_page_style.cpp" line="67"/>
        <source>Access the Web, find the answers.</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠨ ᠮᠡᠷᠬᠡᠵᠢᠯ ᠪᠠ ᠳᠤᠰᠠᠯᠠᠯᠴᠠᠭ᠎ᠠ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠡᠷᠢᠬᠦ.</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="643"/>
        <location filename="../src/guidance_page_style.cpp" line="78"/>
        <source>Guidance</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="681"/>
        <location filename="../src/guidance_page_style.cpp" line="82"/>
        <source>Check Manuals to solve the problem.</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ ᠵᠢ ᠯᠠᠪᠯᠠᠬᠤ᠂ ᠳᠡᠮᠵᠢᠯᠬᠡ ᠪᠠ ᠳᠤᠰᠠᠯᠠᠯᠴᠠᠭ᠎ᠠ ᠡᠷᠢᠬᠦ.</translation>
    </message>
    <message>
        <source>Official WeChat</source>
        <translation type="obsolete">企业微信</translation>
    </message>
</context>
<context>
    <name>IntroPageStyle</name>
    <message>
        <location filename="../src/intro_page_style.ui" line="32"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="160"/>
        <location filename="../src/intro_page_style.cpp" line="28"/>
        <source>Click </source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="167"/>
        <location filename="../src/intro_page_style.cpp" line="36"/>
        <source>to know more about support</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="174"/>
        <location filename="../src/intro_page_style.cpp" line="42"/>
        <source>，to KylinOS Official Web</source>
        <translation>ᠴᠢ ᠯᠢᠨOS ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠦᠰᠦᠷᠴᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="248"/>
        <location filename="../src/intro_page_style.cpp" line="45"/>
        <source>You can get your answers through the ways below</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠠᠷᠭ᠎ᠠ ᠪᠡᠷ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠳᠡᠮᠵᠢᠯᠬᠡ ᠪᠤᠯᠤᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="442"/>
        <location filename="../src/intro_page_style.cpp" line="54"/>
        <source>Official WeChat</source>
        <translation>ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ ᠤ᠋ᠨ ᠸᠢᠴᠠᠲ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="451"/>
        <location filename="../src/intro_page_style.cpp" line="58"/>
        <source>Add our Team&apos;s WeChat</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="458"/>
        <location filename="../src/intro_page_style.cpp" line="63"/>
        <source>Get an online response.</source>
        <translation>ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ ᠤ᠋ᠨ ᠸᠢᠴᠠᠲ ᠤ᠋ᠨ ᠨᠤᠮᠸᠷ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠬᠡᠷ᠎ᠡ ᠠᠪᠴᠠᠯᠳᠠᠬᠤ ᠵᠢ ᠡᠷᠢᠬᠦ.</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="610"/>
        <location filename="../src/intro_page_style.cpp" line="74"/>
        <source>Online</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠬᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="619"/>
        <location filename="../src/intro_page_style.cpp" line="79"/>
        <source>Leave message</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠪᠤᠯᠤᠨ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠢ᠋ ᠡᠷᠢᠨ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="626"/>
        <location filename="../src/intro_page_style.cpp" line="83"/>
        <source>to get support.</source>
        <translation>To get support.</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="760"/>
        <location filename="../src/intro_page_style.cpp" line="94"/>
        <source>Mail</source>
        <translation>ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="769"/>
        <location filename="../src/intro_page_style.cpp" line="98"/>
        <source>Send mail to</source>
        <translation>ᠢᠮᠸᠯ ᠢ᠋ ᠠᠯᠪᠠᠨ ᠤ᠋ ᠢᠮᠸᠯ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠬᠡᠬᠦ᠂ ᠡᠷᠢᠨ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="776"/>
        <location filename="../src/intro_page_style.cpp" line="103"/>
        <source>get support.</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ.</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="976"/>
        <location filename="../src/intro_page_style.cpp" line="114"/>
        <source>Telephone</source>
        <translation>ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ ᠪᠡᠷ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="985"/>
        <location filename="../src/intro_page_style.cpp" line="118"/>
        <source>5*8 hours hotline.</source>
        <translation>5*8 ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ ᠵᠢᠨ ᠳᠦᠪ ᠤ᠋ᠨ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠡᠮᠵᠢᠯᠬᠡ᠂ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ.</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1132"/>
        <location filename="../src/intro_page_style.cpp" line="134"/>
        <source>Guidance</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1141"/>
        <location filename="../src/intro_page_style.cpp" line="138"/>
        <source>Check Manuals to solve</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠮᠵᠢ ᠵᠢᠨ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠠᠢᠬᠤ ᠪᠤᠶᠤ ᠡᠷᠢᠵᠤ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1148"/>
        <location filename="../src/intro_page_style.cpp" line="143"/>
        <source>the problem.</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ.</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <location filename="../src/localInfo.cpp" line="148"/>
        <source>【显卡名称】：%1</source>
        <translation>【 ᠢᠯᠡ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ】：%1</translation>
    </message>
    <message>
        <location filename="../src/localInfo.cpp" line="156"/>
        <source>【显卡驱动名称】：%1</source>
        <translation>【 ᠢᠯᠡ ᠺᠠᠷᠲ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ】：%1</translation>
    </message>
    <message>
        <location filename="../src/localInfo.cpp" line="163"/>
        <source>【显卡数量】：%1 个</source>
        <translation>【 ᠢᠯᠡ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ】：%1 个</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../src/main_page.cpp" line="84"/>
        <source>Click </source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠬᠤ </translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="90"/>
        <source>to know more about support</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="102"/>
        <source>，to KylinOS Official Web</source>
        <translation>ᠴᠢ ᠯᠢᠨOS ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠢ᠋ ᠳᠡᠮᠵᠢᠬᠦ ᠬᠠᠭᠤᠳᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠦᠰᠦᠷᠴᠤ ᠤᠷᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="107"/>
        <source>You can get your answers through the ways below</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠠᠷᠭ᠎ᠠ ᠪᠡᠷ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠳᠡᠮᠵᠢᠯᠬᠡ ᠪᠤᠯᠤᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="121"/>
        <source>Official WeChat</source>
        <translation>ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ ᠤ᠋ᠨ ᠸᠢᠴᠠᠲ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="128"/>
        <source>Add our Team&apos;s WeChat
 Get an online response.</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠵᠢᠨ ᠪᠦᠯᠬᠦᠮ ᠢ᠋ ᠨᠡᠮᠡᠬᠦ
ᠠᠵᠤ ᠠᠬᠤᠢᠯᠠᠯ ᠤ᠋ᠨ ᠸᠢᠴᠠᠲ᠂ ᠤᠳᠠᠰᠤᠨ ᠳᠡᠬᠡᠷᠡᠬᠢ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠢ᠋ ᠡᠷᠢᠨ ᠬᠠᠢᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="164"/>
        <source>Online</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠬᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="171"/>
        <source>Leave message
 to get support.</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ
ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠢ᠋ ᠡᠷᠢᠨ ᠬᠠᠢᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="208"/>
        <source>Mail</source>
        <translation>ᠯᠠᠪᠯᠠᠭ᠎ᠠ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="215"/>
        <source>Send mail to
get support.</source>
        <translation>ᠢᠮᠸᠯ ᠢ᠋ ᠠᠯᠪᠠᠨ ᠤ᠋ ᠢᠮᠸᠯ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠬᠡᠬᠦ᠂ ᠡᠷᠢᠨ ᠬᠠᠢᠬᠤ
 ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ.</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="251"/>
        <source>Telephone</source>
        <translation>ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ ᠪᠡᠷ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="258"/>
        <source>5*8 hours hotline.</source>
        <translation>5*8 ᠴᠠᠭ ᠤ᠋ᠨ ᠳᠠᠭᠤᠳᠠᠯᠭ᠎ᠠ ᠵᠢᠨ ᠳᠦᠪ ᠤ᠋ᠨ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠡᠮᠵᠢᠯᠬᠡ᠂ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ.</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="294"/>
        <source>Guidance</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="301"/>
        <source>Check Manuals to solve
the problem.</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ ᠭᠠᠷ ᠤ᠋ᠨ ᠠᠪᠤᠯᠭ᠎ᠠ ᠵᠢ ᠯᠠᠪᠯᠠᠬᠤ
ᠳᠡᠮᠵᠢᠯᠬᠡ ᠪᠠ ᠳᠤᠰᠠᠯᠠᠯᠴᠠᠭ᠎ᠠ ᠡᠷᠢᠬᠦ.</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="64"/>
        <location filename="../src/mainwindow.cpp" line="72"/>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>Services and Supports</source>
        <translatorcomment>服务与支持</translatorcomment>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="398"/>
        <location filename="../src/mainwindow.cpp" line="487"/>
        <source>Intro</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠳᠠᠨᠢᠯᠴᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <location filename="../src/mainwindow.cpp" line="490"/>
        <source>Message</source>
        <translation>ᠦᠬᠡ ᠦᠯᠡᠳᠡᠬᠡᠵᠤ ᠯᠠᠪᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="400"/>
        <location filename="../src/mainwindow.cpp" line="493"/>
        <source>Contact</source>
        <translation>ᠪᠢᠳᠡᠨ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="401"/>
        <location filename="../src/mainwindow.cpp" line="496"/>
        <source>Guidance</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠬᠡᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="402"/>
        <location filename="../src/mainwindow.cpp" line="499"/>
        <source>Customer</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠵᠤᠴᠢᠳ ᠲᠤ᠌ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="38"/>
        <source>MainWindow</source>
        <translation>mainWindow</translation>
    </message>
</context>
<context>
    <name>MessagePageStyle</name>
    <message>
        <location filename="../src/messagePageStyle.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="232"/>
        <location filename="../src/messagePageStyle.ui" line="468"/>
        <location filename="../src/messagePageStyle.ui" line="1332"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="273"/>
        <location filename="../src/messagePageStyle.ui" line="509"/>
        <location filename="../src/messagePageStyle.ui" line="686"/>
        <location filename="../src/messagePageStyle.ui" line="806"/>
        <location filename="../src/messagePageStyle.ui" line="888"/>
        <location filename="../src/messagePageStyle.ui" line="913"/>
        <location filename="../src/messagePageStyle.ui" line="1158"/>
        <location filename="../src/messagePageStyle.ui" line="1373"/>
        <location filename="../src/messagePageStyle.ui" line="1512"/>
        <location filename="../src/messagePageStyle.ui" line="1632"/>
        <location filename="../src/messagePageStyle.ui" line="1823"/>
        <source>TextLabel</source>
        <translation>textLabel</translation>
    </message>
    <message>
        <source>    Please describe your problem</source>
        <translation type="vanished">    请详细描述要咨询的问题</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="574"/>
        <source>Please describe your problem</source>
        <translation>ᠯᠠᠪᠯᠠᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠨ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="830"/>
        <location filename="../src/messagePageStyle.ui" line="2030"/>
        <location filename="../src/messagePageStyle.ui" line="2071"/>
        <source>PushButton</source>
        <translation>pushButton</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1195"/>
        <location filename="../src/messagePageStyle.cpp" line="430"/>
        <source>Yes</source>
        <translation>ᠳᠡᠢᠮᠤ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1214"/>
        <location filename="../src/messagePageStyle.cpp" line="172"/>
        <source>No</source>
        <translation>ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1968"/>
        <location filename="../src/messagePageStyle.ui" line="1989"/>
        <source>CheckBox</source>
        <translation>checkBox</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="79"/>
        <source>Submitting...</source>
        <translation>ᠶᠠᠭ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="84"/>
        <source>Submit Successed!</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="93"/>
        <source>Question*</source>
        <translation>ᠯᠠᠪᠯᠠᠬᠤ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="97"/>
        <location filename="../src/messagePageStyle.cpp" line="588"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="98"/>
        <source>Suggestion</source>
        <translation>ᠰᠠᠨᠠᠯ ᠵᠦᠪᠯᠡᠯᠬᠡ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="99"/>
        <source>Bussiness</source>
        <translation>ᠠᠷᠠᠯᠵᠢᠶᠠᠨ ᠤ᠋ ᠬᠠᠮᠳᠤᠷᠠᠯᠴᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="100"/>
        <source>Others</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <source>Content*</source>
        <translation type="vanished">咨询内容*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="114"/>
        <source>Word Limit</source>
        <translation>ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠡᠭᠡᠳᠦ ᠬᠢᠵᠠᠭᠠᠷ ᠲᠤ᠌ ᠬᠦᠷᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="134"/>
        <source>Upload</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠲᠤᠨᠤᠭ ᠢ᠋ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="140"/>
        <source>Browse</source>
        <translation>ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="145"/>
        <source>All attachmens size should smaller than 10MB and numbers less than 5</source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠳᠠᠭᠠᠯᠳᠠ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ 10MB ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠬᠦ ᠦᠬᠡᠢ᠂ ᠳᠠᠭᠠᠯᠳᠠ ᠲᠤᠨᠤᠭ ᠤ᠋ᠨ ᠲᠤᠭ᠎ᠠ 5 ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠬᠦ ᠦᠬᠡᠢ ᠪᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="146"/>
        <source>Support format:.gif *.jpg *.png *.pptx *.wps *.xlsx *.pdf *.txt *.docx</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠵᠠᠭᠪᠤᠷ:.gif *.jpg *.png *.pptx *.wps *.xlsx *.pdf *.txt *.docx</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="165"/>
        <source>UploadLog</source>
        <translation>ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Mail*</source>
        <translation type="vanished">邮件*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="194"/>
        <source>Leave your E-mail to get response</source>
        <translation>ᠪᠢᠳᠡ ᠢᠮᠸᠯ ᠵᠢᠡᠷ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠢ᠋ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="200"/>
        <source>Incorrect E-mail address.</source>
        <translation>ᠢᠮᠸᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠵᠦᠪᠬᠡᠨ ᠪᠤᠰᠤ.</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="220"/>
        <source>Tel</source>
        <translation>ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ</translation>
    </message>
    <message>
        <source>Leave your Tel to get response.</source>
        <translation type="vanished">用于反馈处理进度和结果的联系电话</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="231"/>
        <source>Name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <source>What should I call you.</source>
        <translation type="vanished">联系人姓名</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="254"/>
        <source>Os_rel</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠰᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="254"/>
        <location filename="../src/messagePageStyle.cpp" line="263"/>
        <location filename="../src/messagePageStyle.cpp" line="270"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="263"/>
        <source>Deskenv</source>
        <translation>ᠰᠢᠷᠡᠬᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠤᠷᠴᠢᠨ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="270"/>
        <source>Lang</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠦᠭᠡ ᠬᠡᠯᠡ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="328"/>
        <source>Upload my </source>
        <translation>ᠮᠢᠨᠤᠬᠢ ᠵᠢ ᠤᠯᠬᠤ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠬᠦ </translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="333"/>
        <source>System Info</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="340"/>
        <source>Commit</source>
        <translation>ᠳᠤᠱᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="345"/>
        <source>Reset</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="429"/>
        <source>Tips:</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="428"/>
        <source>You Can Submit 1 time per 30s!</source>
        <translation>30 ᠰᠸᠺᠦᠨ᠋ᠲ ᠪᠦᠷᠢ ᠳ᠋ᠤ᠌ ᠵᠦᠪᠬᠡᠨ ᠨᠢᠭᠡ ᠤᠳᠠᠭ᠎ᠠ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="105"/>
        <source>Content</source>
        <translation>ᠯᠠᠪᠯᠠᠬᠤ ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="187"/>
        <source>Mail</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="832"/>
        <source>select file</source>
        <translation>ᠹᠠᠢᠯ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MyPushButton</name>
    <message>
        <source>Intro</source>
        <translation type="vanished">软件介绍</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">留言咨询</translation>
    </message>
    <message>
        <source>Contact</source>
        <translation type="vanished">联系我们</translation>
    </message>
    <message>
        <source>Guidance</source>
        <translation type="vanished">自助服务</translation>
    </message>
    <message>
        <source>Customer</source>
        <translation type="vanished">在线客服</translation>
    </message>
</context>
<context>
    <name>NavigateBar</name>
    <message>
        <location filename="../src/navigatebar.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Services and Supports</source>
        <translatorcomment>服务与支持</translatorcomment>
        <translation type="vanished">服务与支持</translation>
    </message>
</context>
<context>
    <name>SubmitBase</name>
    <message>
        <location filename="../src/submitBase.cpp" line="93"/>
        <source>Server Connect Overtime!</source>
        <translation>ᠢᠮᠸᠯ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="94"/>
        <source>与邮箱服务器链接超时</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠲᠠᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="97"/>
        <source>FeedBack Overtime!</source>
        <translation>smtp ᠪᠤᠴᠠᠭᠠᠭᠰᠠᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="98"/>
        <source>smtp返回信息超时</source>
        <translation>smtp ᠪᠤᠴᠠᠭᠠᠭᠰᠠᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠴᠠᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="101"/>
        <source>Server Unready!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠡᠷ ᠪᠡᠯᠡᠨ ᠪᠤᠯᠤᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="102"/>
        <source>mail服务器未准备好</source>
        <translation>mailᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠡᠷ ᠪᠡᠯᠡᠨ ᠪᠤᠯᠤᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="105"/>
        <source>Login Failed!</source>
        <translation>smtp ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="106"/>
        <source>邮箱登录失败</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="109"/>
        <source>Mail Set Failed!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="110"/>
        <source>邮箱用户设置失败</source>
        <translation>ᠢᠮᠸᠯ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="113"/>
        <source>Mail Send Failed!</source>
        <translation>ᠦᠯᠡᠳᠡᠭᠡᠭᠰᠡᠨ ᠦᠭᠡ ᠵᠢ ᠢᠯᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="114"/>
        <source>邮件发送失败</source>
        <translation>ᠢᠮᠸᠯ ᠢ᠋ ᠢᠯᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="117"/>
        <source>Mail Quit Failed!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠡᠴᠡ ᠭᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="118"/>
        <source>邮件退出失败</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠡᠴᠡ ᠭᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="121"/>
        <source>Local Info Unready!</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠤᠯᠵᠤ ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="122"/>
        <source>本机信息未获取完毕</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠤᠯᠵᠤ ᠳᠠᠭᠤᠰᠤᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="125"/>
        <source>Unknown Error!</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="126"/>
        <source>未知错误</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>T::menuModule</name>
    <message>
        <location filename="../src/menumodule.h" line="83"/>
        <source>Services and Supports</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="95"/>
        <source>kylinservicesupport</source>
        <translation>Kylinservicesupport</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="99"/>
        <source>show kylin-service-support test</source>
        <translation>Show kylin-service-support test</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule.cpp" line="57"/>
        <source>standard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="59"/>
        <source>scientific</source>
        <translation>Scientific</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="61"/>
        <source>exchange rate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="67"/>
        <source>Theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="69"/>
        <location filename="../src/menumodule.cpp" line="94"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="71"/>
        <location filename="../src/menumodule.cpp" line="91"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="73"/>
        <location filename="../src/menumodule.cpp" line="88"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="131"/>
        <source>Service and Support</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="131"/>
        <source>version：1.0.24</source>
        <translation>ᠬᠡᠪᠯᠡᠯ：1.0.24</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="189"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠎ᠦ᠋ᠨ ᠳ᠋ᠤᠭᠠᠷ: </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="200"/>
        <location filename="../src/menumodule.cpp" line="207"/>
        <location filename="../src/menumodule.cpp" line="247"/>
        <location filename="../src/menumodule.cpp" line="254"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ: </translation>
    </message>
    <message>
        <location filename="../src/menumodule.h" line="83"/>
        <source>Services and Supports</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
</context>
</TS>
