<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>BaseStyle</name>
    <message>
        <source>Services and Supports</source>
        <translation type="vanished">服务与支持</translation>
    </message>
</context>
<context>
    <name>ContactPage</name>
    <message>
        <location filename="../src/contact_page.cpp" line="49"/>
        <source>Contact Us</source>
        <translation>聯繫我們</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="61"/>
        <source>Support Team</source>
        <translation>支持團隊</translation>
    </message>
    <message>
        <source>KylinOS Support Team</source>
        <translation type="vanished">服务与支持团队</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="79"/>
        <source>Tel</source>
        <translation>電話</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="95"/>
        <source>E-mail</source>
        <translation>電子郵件</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="111"/>
        <source>Kylin WeChat Official Account</source>
        <translation>麒麟微信公眾號</translation>
    </message>
</context>
<context>
    <name>ContactPageStyle</name>
    <message>
        <location filename="../src/contact_page_style.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="72"/>
        <location filename="../src/contact_page_style.cpp" line="26"/>
        <source>Contact Us</source>
        <translation>聯繫我們</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="242"/>
        <location filename="../src/contact_page_style.cpp" line="37"/>
        <source>Support Team</source>
        <translation>支持團隊</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="269"/>
        <source>KylinOS Support Team</source>
        <translation>麒麟支持團隊</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="337"/>
        <location filename="../src/contact_page_style.cpp" line="53"/>
        <source>Tel</source>
        <translation>電話</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="364"/>
        <source>400-089-1870</source>
        <translation>400-089-1870</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="432"/>
        <location filename="../src/contact_page_style.cpp" line="69"/>
        <source>E-mail</source>
        <translation>電子郵件</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="459"/>
        <source>support@kylinos.cn</source>
        <translation>support@kylinos.cn</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="586"/>
        <source>Kylin WeChat Official Account</source>
        <translation>麒麟微信公眾號</translation>
    </message>
</context>
<context>
    <name>CustomNaviButton</name>
    <message>
        <location filename="../src/customNaviButton.cpp" line="40"/>
        <source>Intro</source>
        <translatorcomment>软件介绍</translatorcomment>
        <translation>介紹</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="45"/>
        <source>Message</source>
        <translatorcomment>留言咨询</translatorcomment>
        <translation>消息</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="50"/>
        <source>Contact</source>
        <translatorcomment>联系我们</translatorcomment>
        <translation>聯繫</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="55"/>
        <source>Guidance</source>
        <translatorcomment>自助支持</translatorcomment>
        <translation>指導</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="60"/>
        <source>Customer</source>
        <translatorcomment>在线客服</translatorcomment>
        <translation>客戶</translation>
    </message>
</context>
<context>
    <name>DIYSupportPage</name>
    <message>
        <location filename="../src/diysupport_page.cpp" line="92"/>
        <source>Click </source>
        <translation>點擊 </translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="99"/>
        <source>to know more about support</source>
        <translation>瞭解有關支援的更多資訊</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="111"/>
        <source>，to KylinOS Official Web</source>
        <translation>，到麒麟操作系統官方網站</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="116"/>
        <source>Users can solve problems through the ways below.</source>
        <translation>用戶可以通過以下方式解決問題。</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="132"/>
        <source>To KylinOS Official Web</source>
        <translation></translation>
    </message>
    <message>
        <source>Go to the KylinOS Official Web</source>
        <translation type="vanished">前往麒麟官网</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="139"/>
        <source>Access the Web, find the answers of the 
normal problems.</source>
        <translation>訪問網路，找到答案
正常問題。</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="156"/>
        <source>Guidance</source>
        <translation>指導</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="162"/>
        <source>Check Manuals to solve the problem.</source>
        <translation>檢查手冊以解決問題。</translation>
    </message>
</context>
<context>
    <name>FileItemInit</name>
    <message>
        <location filename="../src/file_item_init.cpp" line="41"/>
        <source>Del</source>
        <translation>戴爾</translation>
    </message>
</context>
<context>
    <name>GuidancePageStyle</name>
    <message>
        <location filename="../src/guidance_page_style.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="160"/>
        <location filename="../src/guidance_page_style.cpp" line="37"/>
        <source>Click </source>
        <translation>點擊 </translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="167"/>
        <location filename="../src/guidance_page_style.cpp" line="45"/>
        <source>to know more about support</source>
        <translation>瞭解有關支援的更多資訊</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="174"/>
        <location filename="../src/guidance_page_style.cpp" line="51"/>
        <source>，to KylinOS Official Web</source>
        <translation>，到麒麟操作系統官方網站</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="248"/>
        <location filename="../src/guidance_page_style.cpp" line="54"/>
        <source>You can get your answers through the ways below</source>
        <translation>您可以通過以下方式獲得答案</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="446"/>
        <location filename="../src/guidance_page_style.cpp" line="63"/>
        <source>To KylinOS Official Web</source>
        <translation>前往麒麟操作系統官方網站</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="484"/>
        <location filename="../src/guidance_page_style.cpp" line="67"/>
        <source>Access the Web, find the answers.</source>
        <translation>訪問網路，找到答案。</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="643"/>
        <location filename="../src/guidance_page_style.cpp" line="78"/>
        <source>Guidance</source>
        <translation>指導</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="681"/>
        <location filename="../src/guidance_page_style.cpp" line="82"/>
        <source>Check Manuals to solve the problem.</source>
        <translation>檢查手冊以解決問題。</translation>
    </message>
    <message>
        <source>Official WeChat</source>
        <translation type="obsolete">企业微信</translation>
    </message>
</context>
<context>
    <name>IntroPageStyle</name>
    <message>
        <location filename="../src/intro_page_style.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="160"/>
        <location filename="../src/intro_page_style.cpp" line="28"/>
        <source>Click </source>
        <translation>點擊 </translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="167"/>
        <location filename="../src/intro_page_style.cpp" line="36"/>
        <source>to know more about support</source>
        <translation>瞭解有關支援的更多資訊</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="174"/>
        <location filename="../src/intro_page_style.cpp" line="42"/>
        <source>，to KylinOS Official Web</source>
        <translation>，到麒麟操作系統官方網站</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="248"/>
        <location filename="../src/intro_page_style.cpp" line="45"/>
        <source>You can get your answers through the ways below</source>
        <translation>您可以通過以下方式獲得答案</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="442"/>
        <location filename="../src/intro_page_style.cpp" line="54"/>
        <source>Official WeChat</source>
        <translation>官方微信</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="451"/>
        <location filename="../src/intro_page_style.cpp" line="58"/>
        <source>Add our Team&apos;s WeChat</source>
        <translation>添加我們團隊的微信</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="458"/>
        <location filename="../src/intro_page_style.cpp" line="63"/>
        <source>Get an online response.</source>
        <translation>獲取在線回復。</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="610"/>
        <location filename="../src/intro_page_style.cpp" line="74"/>
        <source>Online</source>
        <translation>在線</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="619"/>
        <location filename="../src/intro_page_style.cpp" line="79"/>
        <source>Leave message</source>
        <translation>留言</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="626"/>
        <location filename="../src/intro_page_style.cpp" line="83"/>
        <source>to get support.</source>
        <translation>以獲得支援。</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="760"/>
        <location filename="../src/intro_page_style.cpp" line="94"/>
        <source>Mail</source>
        <translation>郵件</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="769"/>
        <location filename="../src/intro_page_style.cpp" line="98"/>
        <source>Send mail to</source>
        <translation>發送郵件至</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="776"/>
        <location filename="../src/intro_page_style.cpp" line="103"/>
        <source>get support.</source>
        <translation>獲取支援。</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="976"/>
        <location filename="../src/intro_page_style.cpp" line="114"/>
        <source>Telephone</source>
        <translation>電話</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="985"/>
        <location filename="../src/intro_page_style.cpp" line="118"/>
        <source>5*8 hours hotline.</source>
        <translation>5*8小時熱線。</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1132"/>
        <location filename="../src/intro_page_style.cpp" line="134"/>
        <source>Guidance</source>
        <translation>指導</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1141"/>
        <location filename="../src/intro_page_style.cpp" line="138"/>
        <source>Check Manuals to solve</source>
        <translation>查看手冊以解決</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1148"/>
        <location filename="../src/intro_page_style.cpp" line="143"/>
        <source>the problem.</source>
        <translation>問題。</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <location filename="../src/localInfo.cpp" line="148"/>
        <source>【显卡名称】：%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/localInfo.cpp" line="156"/>
        <source>【显卡驱动名称】：%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/localInfo.cpp" line="163"/>
        <source>【显卡数量】：%1 个</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../src/main_page.cpp" line="84"/>
        <source>Click </source>
        <translation>點擊 </translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="90"/>
        <source>to know more about support</source>
        <translation>瞭解有關支援的更多資訊</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="102"/>
        <source>，to KylinOS Official Web</source>
        <translation>，到麒麟操作系統官方網站</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="107"/>
        <source>You can get your answers through the ways below</source>
        <translation>您可以通過以下方式獲得答案</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="121"/>
        <source>Official WeChat</source>
        <translation>官方微信</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="128"/>
        <source>Add our Team&apos;s WeChat
 Get an online response.</source>
        <translation>添加我們團隊的微信
 獲取在線回復。</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="164"/>
        <source>Online</source>
        <translation>在線</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="171"/>
        <source>Leave message
 to get support.</source>
        <translation>留言
 以獲得支援。</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="208"/>
        <source>Mail</source>
        <translation>郵件</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="215"/>
        <source>Send mail to
get support.</source>
        <translation>發送郵件至
獲取支援。</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="251"/>
        <source>Telephone</source>
        <translation>電話</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="258"/>
        <source>5*8 hours hotline.</source>
        <translation>5*8小時熱線。</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="294"/>
        <source>Guidance</source>
        <translation>指導</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="301"/>
        <source>Check Manuals to solve
the problem.</source>
        <translation>查看手冊以解決
問題。</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="64"/>
        <location filename="../src/mainwindow.cpp" line="72"/>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>Services and Supports</source>
        <translatorcomment>服务与支持</translatorcomment>
        <translation>服務與支援</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="398"/>
        <location filename="../src/mainwindow.cpp" line="487"/>
        <source>Intro</source>
        <translation>介紹</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <location filename="../src/mainwindow.cpp" line="490"/>
        <source>Message</source>
        <translation>消息</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="400"/>
        <location filename="../src/mainwindow.cpp" line="493"/>
        <source>Contact</source>
        <translation>聯繫</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="401"/>
        <location filename="../src/mainwindow.cpp" line="496"/>
        <source>Guidance</source>
        <translation>指導</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="402"/>
        <location filename="../src/mainwindow.cpp" line="499"/>
        <source>Customer</source>
        <translation>客戶</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="38"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MessagePageStyle</name>
    <message>
        <location filename="../src/messagePageStyle.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="232"/>
        <location filename="../src/messagePageStyle.ui" line="468"/>
        <location filename="../src/messagePageStyle.ui" line="1332"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="273"/>
        <location filename="../src/messagePageStyle.ui" line="509"/>
        <location filename="../src/messagePageStyle.ui" line="686"/>
        <location filename="../src/messagePageStyle.ui" line="806"/>
        <location filename="../src/messagePageStyle.ui" line="888"/>
        <location filename="../src/messagePageStyle.ui" line="913"/>
        <location filename="../src/messagePageStyle.ui" line="1158"/>
        <location filename="../src/messagePageStyle.ui" line="1373"/>
        <location filename="../src/messagePageStyle.ui" line="1512"/>
        <location filename="../src/messagePageStyle.ui" line="1632"/>
        <location filename="../src/messagePageStyle.ui" line="1823"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <source>    Please describe your problem</source>
        <translation type="vanished">    请详细描述要咨询的问题</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="574"/>
        <source>Please describe your problem</source>
        <translation>請描述您的問題</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="830"/>
        <location filename="../src/messagePageStyle.ui" line="2030"/>
        <location filename="../src/messagePageStyle.ui" line="2071"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1195"/>
        <location filename="../src/messagePageStyle.cpp" line="430"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1214"/>
        <location filename="../src/messagePageStyle.cpp" line="172"/>
        <source>No</source>
        <translation>不</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1968"/>
        <location filename="../src/messagePageStyle.ui" line="1989"/>
        <source>CheckBox</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="79"/>
        <source>Submitting...</source>
        <translation>提交。。。</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="84"/>
        <source>Submit Successed!</source>
        <translation>提交成功！</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="93"/>
        <source>Question*</source>
        <translation>問題*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="97"/>
        <location filename="../src/messagePageStyle.cpp" line="588"/>
        <source>System</source>
        <translation>系統</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="98"/>
        <source>Suggestion</source>
        <translation>建議</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="99"/>
        <source>Bussiness</source>
        <translation>業務性</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="100"/>
        <source>Others</source>
        <translation>別人</translation>
    </message>
    <message>
        <source>Content*</source>
        <translation type="vanished">咨询内容*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="114"/>
        <source>Word Limit</source>
        <translation>字數限制</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="134"/>
        <source>Upload</source>
        <translation>上傳</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="140"/>
        <source>Browse</source>
        <translation>流覽</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="145"/>
        <source>All attachmens size should smaller than 10MB and numbers less than 5</source>
        <translation>所有附件大小應小於 10MB，數位應小於 5</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="146"/>
        <source>Support format:.gif *.jpg *.png *.pptx *.wps *.xlsx *.pdf *.txt *.docx</source>
        <translation>支援格式：.gif *.jpg *.png *.pptx *.wps *.xlsx *.pdf *.txt *.docx</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="165"/>
        <source>UploadLog</source>
        <translation>上傳日誌</translation>
    </message>
    <message>
        <source>Mail*</source>
        <translation type="vanished">邮件*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="194"/>
        <source>Leave your E-mail to get response</source>
        <translation>留下您的電子郵件以獲得回復</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="200"/>
        <source>Incorrect E-mail address.</source>
        <translation>電子郵件地址不正確。</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="220"/>
        <source>Tel</source>
        <translation>電話</translation>
    </message>
    <message>
        <source>Leave your Tel to get response.</source>
        <translation type="vanished">用于反馈处理进度和结果的联系电话</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="231"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <source>What should I call you.</source>
        <translation type="vanished">联系人姓名</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="254"/>
        <source>Os_rel</source>
        <translation>Os_rel</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="254"/>
        <location filename="../src/messagePageStyle.cpp" line="263"/>
        <location filename="../src/messagePageStyle.cpp" line="270"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="263"/>
        <source>Deskenv</source>
        <translation>德肯夫</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="270"/>
        <source>Lang</source>
        <translation>朗</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="328"/>
        <source>Upload my </source>
        <translation>上傳我的 </translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="333"/>
        <source>System Info</source>
        <translation>系統資訊</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="340"/>
        <source>Commit</source>
        <translation>犯</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="345"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="429"/>
        <source>Tips:</source>
        <translation>技巧：</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="428"/>
        <source>You Can Submit 1 time per 30s!</source>
        <translation>每 30 秒可以提交 1 次！</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="105"/>
        <source>Content</source>
        <translation>內容</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="187"/>
        <source>Mail</source>
        <translation>郵件</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="832"/>
        <source>select file</source>
        <translation>選擇檔案</translation>
    </message>
</context>
<context>
    <name>MyPushButton</name>
    <message>
        <source>Intro</source>
        <translation type="vanished">软件介绍</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">留言咨询</translation>
    </message>
    <message>
        <source>Contact</source>
        <translation type="vanished">联系我们</translation>
    </message>
    <message>
        <source>Guidance</source>
        <translation type="vanished">自助服务</translation>
    </message>
    <message>
        <source>Customer</source>
        <translation type="vanished">在线客服</translation>
    </message>
</context>
<context>
    <name>NavigateBar</name>
    <message>
        <location filename="../src/navigatebar.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Services and Supports</source>
        <translatorcomment>服务与支持</translatorcomment>
        <translation type="vanished">服务与支持</translation>
    </message>
</context>
<context>
    <name>SubmitBase</name>
    <message>
        <location filename="../src/submitBase.cpp" line="93"/>
        <source>Server Connect Overtime!</source>
        <translation>伺服器連接加班！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="94"/>
        <source>与邮箱服务器链接超时</source>
        <translation>與郵箱伺服器連結超時</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="97"/>
        <source>FeedBack Overtime!</source>
        <translation>反饋加班！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="98"/>
        <source>smtp返回信息超时</source>
        <translation>smtp返回資訊超時</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="101"/>
        <source>Server Unready!</source>
        <translation>伺服器未準備好！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="102"/>
        <source>mail服务器未准备好</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="105"/>
        <source>Login Failed!</source>
        <translation>登錄失敗！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="106"/>
        <source>邮箱登录失败</source>
        <translation>郵箱登錄失敗</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="109"/>
        <source>Mail Set Failed!</source>
        <translation>郵件設定失敗！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="110"/>
        <source>邮箱用户设置失败</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="113"/>
        <source>Mail Send Failed!</source>
        <translation>郵件發送失敗！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="114"/>
        <source>邮件发送失败</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="117"/>
        <source>Mail Quit Failed!</source>
        <translation>郵件退出失敗！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="118"/>
        <source>邮件退出失败</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="121"/>
        <source>Local Info Unready!</source>
        <translation>本地資訊未準備好！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="122"/>
        <source>本机信息未获取完毕</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="125"/>
        <source>Unknown Error!</source>
        <translation>未知錯誤！</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="126"/>
        <source>未知错误</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>T::menuModule</name>
    <message>
        <location filename="../src/menumodule.h" line="83"/>
        <source>Services and Supports</source>
        <translation>服務與支援</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="95"/>
        <source>kylinservicesupport</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="99"/>
        <source>show kylin-service-support test</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule.cpp" line="57"/>
        <source>standard</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="59"/>
        <source>scientific</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="61"/>
        <source>exchange rate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="67"/>
        <source>Theme</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="69"/>
        <location filename="../src/menumodule.cpp" line="94"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="71"/>
        <location filename="../src/menumodule.cpp" line="91"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="73"/>
        <location filename="../src/menumodule.cpp" line="88"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="131"/>
        <source>Service and Support</source>
        <translation>服務與支援</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="131"/>
        <source>version：1.0.24</source>
        <translation>版本：1.0.24</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="189"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="200"/>
        <location filename="../src/menumodule.cpp" line="207"/>
        <location filename="../src/menumodule.cpp" line="247"/>
        <location filename="../src/menumodule.cpp" line="254"/>
        <source>Service &amp; Support: </source>
        <translation>服務與支援： </translation>
    </message>
    <message>
        <location filename="../src/menumodule.h" line="83"/>
        <source>Services and Supports</source>
        <translation>服務與支援</translation>
    </message>
</context>
</TS>
