<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>About</name>
    <message>
        <location filename="../../src/about.cpp" line="26"/>
        <source>Notes</source>
        <translation>便簽貼</translation>
    </message>
</context>
<context>
    <name>CustomPushButtonGroup</name>
    <message>
        <location filename="../../src/custom_ui/custom_push_button_group.ui" line="14"/>
        <source>CustomPushButtonGroup</source>
        <translation>自訂按鈕組</translation>
    </message>
</context>
<context>
    <name>EditPage</name>
    <message>
        <location filename="../../src/editPage.cpp" line="98"/>
        <source>Notes</source>
        <translation>便簽貼</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="369"/>
        <source>undo</source>
        <translation>撤銷</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="370"/>
        <source>redo</source>
        <translation>恢復</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="371"/>
        <source>cut</source>
        <translation>剪切</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="372"/>
        <source>copy</source>
        <translation>複製</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="373"/>
        <source>paste</source>
        <translation>粘貼</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="374"/>
        <source>copy to newpage</source>
        <translation>將選取區域複製到新便簽</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="736"/>
        <source>Select an image</source>
        <translation>請選擇圖片</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="738"/>
        <source>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; All (*)</source>
        <translation>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; 全部 （*）</translation>
    </message>
</context>
<context>
    <name>Edit_page</name>
    <message>
        <location filename="../../src/editPage.ui" line="26"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../../src/font_button/font_property_widget.ui" line="14"/>
        <source>FontPropertyWidget</source>
        <translation>字體屬性小部件</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="15"/>
        <source>Bold</source>
        <translation>加粗</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="16"/>
        <source>Italic</source>
        <translation>斜體</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="17"/>
        <source>Underline</source>
        <translation>下劃線</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="18"/>
        <source>Strikeout</source>
        <translation>刪除線</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="19"/>
        <source>Unordered</source>
        <translation>無序列表</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="20"/>
        <source>Ordered</source>
        <translation>有序清單</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="21"/>
        <source>InsertPicture</source>
        <translation>插入圖片</translation>
    </message>
</context>
<context>
    <name>FontTwinButtonGroup</name>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="21"/>
        <source>Font Size</source>
        <translation>字型大小</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="22"/>
        <source>Font Color</source>
        <translation>字體顏色</translation>
    </message>
</context>
<context>
    <name>PaletteWidget</name>
    <message>
        <location filename="../../src/paletteWidget.ui" line="26"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/utils/utils.cpp" line="22"/>
        <source>show</source>
        <translation>顯示</translation>
    </message>
</context>
<context>
    <name>SelectColor</name>
    <message>
        <location filename="../../src/selectColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>SetFontColor</name>
    <message>
        <location filename="../../src/setFontColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>SetFontSize</name>
    <message>
        <location filename="../../src/setFontSizePage.ui" line="31"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../src/widget.ui" line="14"/>
        <source>Widget</source>
        <translation>Widget</translation>
    </message>
    <message>
        <location filename="../../src/widget.ui" line="63"/>
        <source>Note List</source>
        <translation>便簽清單</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="268"/>
        <location filename="../../src/widget.cpp" line="630"/>
        <source>Notes</source>
        <translation>便簽貼</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="612"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="613"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="614"/>
        <source>Empty Note</source>
        <translation>清空便簽本</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="616"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="650"/>
        <source>Create New Note</source>
        <translation>新建便簽</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="651"/>
        <source>Switch View</source>
        <translation>切換檢視</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1121"/>
        <source>Enter search content</source>
        <translation>請輸入搜尋內容</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1623"/>
        <location filename="../../src/widget.cpp" line="1637"/>
        <source>Delete this note</source>
        <translation>刪除該便簽</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1624"/>
        <location filename="../../src/widget.cpp" line="1640"/>
        <source>Open this note</source>
        <translation>打開該便簽</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1625"/>
        <location filename="../../src/widget.cpp" line="1643"/>
        <source>Clear note list</source>
        <translation>清空便簽清單</translation>
    </message>
</context>
<context>
    <name>emptyNotes</name>
    <message>
        <location filename="../../src/emptyNotes.ui" line="32"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="190"/>
        <source>Are you sure empty notebook?</source>
        <translation>您確定要清空便簽本嗎？</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="251"/>
        <source>No Tips</source>
        <translation>不再提示</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="305"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="328"/>
        <source>yes</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>iconViewModeDelegate</name>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="260"/>
        <source>[picture]</source>
        <translation>[圖片]</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="334"/>
        <source>Welcome to use Notes.</source>
        <translation>按兩下可編輯便簽內容</translation>
    </message>
</context>
<context>
    <name>listViewModeDelegate</name>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="308"/>
        <source>[picture]</source>
        <translation>[圖片]</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="313"/>
        <source>Welcome to use Notes.</source>
        <translation>按兩下可編輯便簽內容</translation>
    </message>
</context>
<context>
    <name>noteExitWindow</name>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="31"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="83"/>
        <source>Are you sure to exit the note book?</source>
        <translation>確認要退出便簽本嗎？</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="104"/>
        <source>Close the desktop note page at the same time</source>
        <translation>同時關閉桌面便簽頁</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="149"/>
        <source>No</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="171"/>
        <source>Yes</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>noteHead</name>
    <message>
        <location filename="../../src/noteHead.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name>noteHeadMenu</name>
    <message>
        <location filename="../../src/noteHeadMenu.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="75"/>
        <source>Palette</source>
        <translation>調色板</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="83"/>
        <source>Options</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="90"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="99"/>
        <source>Create New Note</source>
        <translation>新建便簽</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="149"/>
        <source>Open note list</source>
        <translation>打開便簽貼</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="150"/>
        <source>Always in the front</source>
        <translation>總在頂層</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="151"/>
        <source>Delete this note</source>
        <translation>刪除此便簽</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="152"/>
        <source>Share</source>
        <translation>分享</translation>
    </message>
</context>
<context>
    <name>paletteButton</name>
    <message>
        <location filename="../../src/paletteButton.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
</context>
<context>
    <name></name>
    <message>
        <location filename="../data/ukui-notebook.desktop.in.h" line="1"/>
        <source>Notes</source>
        <translation>便簽貼</translation>
    </message>
</context>
</TS>
