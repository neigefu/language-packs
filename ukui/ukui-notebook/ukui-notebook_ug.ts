<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name></name>
    <message>
        <location filename="../data/ukui-notebook.desktop.in.h" line="1"/>
        <source>Notes</source>
        <translation>ئىزاھاتلار</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">دىئالوگ</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">ھەققىدە</translation>
    </message>
    <message>
        <location filename="../../src/about.cpp" line="26"/>
        <source>Notes</source>
        <translation>ئىزاھاتلار</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation type="vanished">نەشرى: ٪1</translation>
    </message>
    <message>
        <source>Notes is a self-developed sidebar application plug-in, which provides a rich interface, convenient operation and stable functions, aiming at a friendly user experience.</source>
        <translation type="vanished">ئىزاھات ئۆزى تەتقىق قىلىپ ياسىغان يانبالدىقى ئەپ قىستۇرمىسى بولۇپ، ئۇ مول ئارايۈز، قولايلىق مەشغۇلات ۋە مۇقىم ئىقتىدار بىلەن تەمىنلەيدۇ، دوستانە ئابونتلار تەجرىبىسىنى نىشان قىلىدۇ.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">مۇلازىمەت &gt; قوللاش: </translation>
    </message>
</context>
<context>
    <name>CustomPushButtonGroup</name>
    <message>
        <location filename="../../src/custom_ui/custom_push_button_group.ui" line="14"/>
        <source>CustomPushButtonGroup</source>
        <translation>CustomPushButtonGroup</translation>
    </message>
</context>
<context>
    <name>EditPage</name>
    <message>
        <location filename="../../src/editPage.cpp" line="93"/>
        <source>Notes</source>
        <translation>ئىزاھاتلار</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="vanished">قاپ يۈرەك</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="vanished">ئىتاچى</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="vanished">ئاستى سىزىق</translation>
    </message>
    <message>
        <source>Strikeout</source>
        <translation type="vanished">ھۇجۇم قىلىش</translation>
    </message>
    <message>
        <source>Unordered</source>
        <translation type="vanished">تەشكىللەنمىگەن</translation>
    </message>
    <message>
        <source>Ordered</source>
        <translation type="vanished">بۇيرۇتما</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation type="vanished">خەت چوڭلۇقى</translation>
    </message>
    <message>
        <source>Font Color</source>
        <translation type="vanished">خەت نۇسخىسى رەڭگى</translation>
    </message>
    <message>
        <source>InsertPicture</source>
        <translation type="vanished">InsertPicture</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="362"/>
        <source>undo</source>
        <translation>ئاغدۇر</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="363"/>
        <source>redo</source>
        <translation>redo</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="364"/>
        <source>cut</source>
        <translation>كېسىش</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="365"/>
        <source>copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="366"/>
        <source>paste</source>
        <translation>چاپلاش</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="367"/>
        <source>copy to newpage</source>
        <translation>يېڭى بەتكە كۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="724"/>
        <source>Select an image</source>
        <translation>سۈرەت تاللاش</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="726"/>
        <source>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; All (*)</source>
        <translation>JPEG (*.jpg);؛ GIF (*.gif);؛ PNG (*.png);؛ BMP (*.bmp);؛ ھەممە (*)</translation>
    </message>
</context>
<context>
    <name>Edit_page</name>
    <message>
        <location filename="../../src/editPage.ui" line="26"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../../src/font_button/font_property_widget.ui" line="14"/>
        <source>FontPropertyWidget</source>
        <translation>FontPropertyWidget</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="15"/>
        <source>Bold</source>
        <translation>قاپ يۈرەك</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="16"/>
        <source>Italic</source>
        <translation>ئىتاچى</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="17"/>
        <source>Underline</source>
        <translation>ئاستى سىزىق</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="18"/>
        <source>Strikeout</source>
        <translation>ھۇجۇم قىلىش</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="19"/>
        <source>Unordered</source>
        <translation>تەشكىللەنمىگەن</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="20"/>
        <source>Ordered</source>
        <translation>بۇيرۇتما</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="21"/>
        <source>InsertPicture</source>
        <translation>InsertPicture</translation>
    </message>
</context>
<context>
    <name>FontTwinButtonGroup</name>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="21"/>
        <source>Font Size</source>
        <translation>خەت چوڭلۇقى</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="22"/>
        <source>Font Color</source>
        <translation>خەت نۇسخىسى رەڭگى</translation>
    </message>
</context>
<context>
    <name>PaletteWidget</name>
    <message>
        <location filename="../../src/paletteWidget.ui" line="26"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/utils/utils.cpp" line="22"/>
        <source>show</source>
        <translation>كۆرسەت</translation>
    </message>
</context>
<context>
    <name>SelectColor</name>
    <message>
        <location filename="../../src/selectColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
<context>
    <name>SetFontColor</name>
    <message>
        <location filename="../../src/setFontColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
<context>
    <name>SetFontSize</name>
    <message>
        <location filename="../../src/setFontSizePage.ui" line="31"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../src/widget.ui" line="14"/>
        <source>Widget</source>
        <translation>ۋىكىپىدىيە</translation>
    </message>
    <message>
        <location filename="../../src/widget.ui" line="63"/>
        <source>Note List</source>
        <translation>ئەسكەرتىش تىزىملىكى</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">يېڭى</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="283"/>
        <source>Notes</source>
        <translation>ئىزاھاتلار</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="616"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="617"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="618"/>
        <source>Empty Note</source>
        <translation>بوش ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="620"/>
        <source>Exit</source>
        <translation>چىقىش ئېغىزى</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="653"/>
        <source>Create New Note</source>
        <translation>يېڭى خاتىرە يارىتىش</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1123"/>
        <source>Enter search content</source>
        <translation>ئىزدەش مەزمۇنىنى كىرگۈزۈش</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1621"/>
        <location filename="../../src/widget.cpp" line="1635"/>
        <source>delete this note</source>
        <translation>بۇ ئەسكەرتىشنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1622"/>
        <location filename="../../src/widget.cpp" line="1638"/>
        <source>open this note</source>
        <translation>بۇ ئەسكەرتىشنى ئېچىش</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1623"/>
        <location filename="../../src/widget.cpp" line="1641"/>
        <source>clear note list</source>
        <translation>ئەسكەرتىش تىزىملىكىنى ئېنىقلا</translation>
    </message>
    <message>
        <source>Delete Selected Note</source>
        <translation type="vanished">تاللانغان ئەسكەرتىشنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="654"/>
        <source>Switch View</source>
        <translation>ۋىكليۇچاتېل كۆرۈش</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">ياپ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">كىچىكلىتىش</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">تىزىملىك</translation>
    </message>
    <message>
        <source>Welcome to use Notes.</source>
        <translation type="vanished">ئىزاھاتلارنى ئىشلىتىشىڭىزنى قارشى ئالىمىز.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">ئىزدە</translation>
    </message>
    <message>
        <source>[picture]</source>
        <translation type="vanished">[رەسىم]</translation>
    </message>
</context>
<context>
    <name>emptyNotes</name>
    <message>
        <location filename="../../src/emptyNotes.ui" line="32"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="190"/>
        <source>Are you sure empty notebook?</source>
        <translation>سىز راستىنلا قۇرۇق خاتىرە دەپتىرىنى جەزىملەشتۈرەلەمسىز؟</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="251"/>
        <source>No Tips</source>
        <translation>ئەسكەرتىش يوق</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="305"/>
        <source>cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="328"/>
        <source>yes</source>
        <translation>ھەئە</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.cpp" line="33"/>
        <source>emptyNotes</source>
        <translation>emptyNotes</translation>
    </message>
</context>
<context>
    <name>fontButton</name>
    <message>
        <source>Form</source>
        <translation type="vanished">جەدۋەل</translation>
    </message>
</context>
<context>
    <name>iconViewModeDelegate</name>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="260"/>
        <source>[picture]</source>
        <translation>[رەسىم]</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="334"/>
        <source>Welcome to use Notes.</source>
        <translation>ئىزاھاتلارنى ئىشلىتىشىڭىزنى قارشى ئالىمىز.</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="363"/>
        <source>Today  </source>
        <translation>تارىخ-بۈگۈن  </translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="372"/>
        <source>Yesterday  </source>
        <translation>تۈنۈگۈن  </translation>
    </message>
</context>
<context>
    <name>listViewModeDelegate</name>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="308"/>
        <source>[picture]</source>
        <translation>[رەسىم]</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="313"/>
        <source>Welcome to use Notes.</source>
        <translation>ئىزاھاتلارنى ئىشلىتىشىڭىزنى قارشى ئالىمىز.</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="385"/>
        <source>Today  </source>
        <translation>تارىخ-بۈگۈن  </translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="394"/>
        <source>Yesterday  </source>
        <translation>تۈنۈگۈن  </translation>
    </message>
</context>
<context>
    <name>noteExitWindow</name>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="31"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="83"/>
        <source>Are you sure to exit the note book?</source>
        <translation>سىز چوقۇم خاتىرە دەپتىرىدىن چىقامسىز؟</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="104"/>
        <source>Close the desktop note page at the same time</source>
        <translation>ئۈستەل يۈزى خاتىرە بېتىنى بىرلا ۋاقىتتا تاقاش</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="149"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="171"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
</context>
<context>
    <name>noteHead</name>
    <message>
        <location filename="../../src/noteHead.ui" line="14"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
<context>
    <name>noteHeadMenu</name>
    <message>
        <location filename="../../src/noteHeadMenu.ui" line="14"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="149"/>
        <source>Open note list</source>
        <translation>ئەسكەرتىش تىزىملىكىنى ئېچىش</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="150"/>
        <source>Always in the front</source>
        <translation>دائىم ئالدىدا</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="151"/>
        <source>Delete this note</source>
        <translation>بۇ ئەسكەرتىشنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="152"/>
        <source>Share</source>
        <translation>تەڭ بەھرىمان بولۇش</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="99"/>
        <source>Create New Note</source>
        <translation>يېڭى خاتىرە يارىتىش</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="90"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="82"/>
        <location filename="../../src/noteHeadMenu.cpp" line="86"/>
        <source>Menu</source>
        <translation>تىزىملىك</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="74"/>
        <source>Palette</source>
        <translation>پالتا</translation>
    </message>
</context>
<context>
    <name>paletteButton</name>
    <message>
        <location filename="../../src/paletteButton.ui" line="32"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
</context>
</TS>
