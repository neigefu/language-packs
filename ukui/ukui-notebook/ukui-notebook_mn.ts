<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name></name>
    <message>
        <location filename="../data/ukui-notebook.desktop.in.h" line="1"/>
        <source>Notes</source>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠨᠠᠭᠠᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Dialog</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">about</translation>
    </message>
    <message>
        <location filename="../../src/about.cpp" line="26"/>
        <source>Notes</source>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠨᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation type="vanished">version: %1</translation>
    </message>
    <message>
        <source>Notes is a self-developed sidebar application plug-in, which provides a rich interface, convenient operation and stable functions, aiming at a friendly user experience.</source>
        <translation type="vanished">notes is a self-developed sidebar application plug-in, which provides a rich interface, convenient operation and stable functions, aiming at a friendly user experience.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">service &amp; Support: </translation>
    </message>
</context>
<context>
    <name>CustomPushButtonGroup</name>
    <message>
        <location filename="../../src/custom_ui/custom_push_button_group.ui" line="14"/>
        <source>CustomPushButtonGroup</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>EditPage</name>
    <message>
        <location filename="../../src/editPage.cpp" line="98"/>
        <source>Notes</source>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠨᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="vanished">bold</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="vanished">italic</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="vanished">underline</translation>
    </message>
    <message>
        <source>Strikeout</source>
        <translation type="vanished">strikeout</translation>
    </message>
    <message>
        <source>Unordered</source>
        <translation type="vanished">unordered</translation>
    </message>
    <message>
        <source>Ordered</source>
        <translation type="vanished">ordered</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation type="vanished">font Size</translation>
    </message>
    <message>
        <source>Font Color</source>
        <translation type="vanished">font Color</translation>
    </message>
    <message>
        <source>InsertPicture</source>
        <translation type="vanished">insertPicture</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="369"/>
        <source>undo</source>
        <translation>ᠬᠠᠰᠤᠵᠤ ᠡᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="370"/>
        <source>redo</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="371"/>
        <source>cut</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="372"/>
        <source>copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="373"/>
        <source>paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="374"/>
        <source>copy to newpage</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠰᠢᠨ᠎ᠡ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="736"/>
        <source>Select an image</source>
        <translation>ᠵᠢᠷᠤᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="738"/>
        <source>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; All (*)</source>
        <translation>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; ᠪᠦᠬᠦᠢᠯᠡ (*)</translation>
    </message>
</context>
<context>
    <name>Edit_page</name>
    <message>
        <location filename="../../src/editPage.ui" line="26"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
    <message>
        <source>New</source>
        <translatorcomment>Yeni</translatorcomment>
        <translation type="vanished">Yeni</translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../../src/font_button/font_property_widget.ui" line="14"/>
        <source>FontPropertyWidget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="15"/>
        <source>Bold</source>
        <translation>ᠵᠣᠷᠢᠭᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="16"/>
        <source>Italic</source>
        <translation>ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="17"/>
        <source>Underline</source>
        <translation>ᠳᠣᠣᠷᠠᠳᠤ ᠤᠲᠠᠰᠤ ᠶᠢ ᠰᠡᠯᠢᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="18"/>
        <source>Strikeout</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠵᠧᠨ ᠵᠧᠨ ᠲᠣᠪᠴᠢᠶ᠎ᠠ ᠭᠠᠷᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="19"/>
        <source>Unordered</source>
        <translation>ᠡᠮᠬᠢ ᠵᠢᠷᠤᠮ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="20"/>
        <source>Ordered</source>
        <translation>ᠲᠤᠰᠢᠶᠠᠯ ᠥᠭ᠍</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="21"/>
        <source>InsertPicture</source>
        <translation>ᠵᠢᠷᠤᠭ ᠬᠠᠳᠬᠤᠨ ᠣᠷᠣᠪᠠ</translation>
    </message>
</context>
<context>
    <name>FontTwinButtonGroup</name>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="21"/>
        <source>Font Size</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠡᠶ᠎ᠡ ᠶᠢᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="22"/>
        <source>Font Color</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠥᠩᠭᠡ</translation>
    </message>
</context>
<context>
    <name>PaletteWidget</name>
    <message>
        <location filename="../../src/paletteWidget.ui" line="26"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/utils/utils.cpp" line="22"/>
        <source>show</source>
        <translation>ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>SelectColor</name>
    <message>
        <location filename="../../src/selectColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
</context>
<context>
    <name>SetFontColor</name>
    <message>
        <location filename="../../src/setFontColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
</context>
<context>
    <name>SetFontSize</name>
    <message>
        <location filename="../../src/setFontSizePage.ui" line="31"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../src/widget.ui" line="14"/>
        <source>Widget</source>
        <translation>widget</translation>
    </message>
    <message>
        <location filename="../../src/widget.ui" line="63"/>
        <source>Note List</source>
        <translation>ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠲᠠᠶᠢᠯᠪᠤᠷᠢᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>New</source>
        <translatorcomment>Yeni</translatorcomment>
        <translation type="vanished">new</translation>
    </message>
    <message>
        <source>Search</source>
        <translatorcomment>Ara</translatorcomment>
        <translation type="vanished">search</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="650"/>
        <source>Create New Note</source>
        <translatorcomment>Yeni Not Oluştur</translatorcomment>
        <translation>ᠰᠢᠨ᠎ᠡ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="268"/>
        <location filename="../../src/widget.cpp" line="630"/>
        <source>Notes</source>
        <translation>ᠳᠦᠬᠦᠮ ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠨᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="612"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="613"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="614"/>
        <source>Empty Note</source>
        <translatorcomment>Boş Not</translatorcomment>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠳᠡᠪᠳᠡᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="616"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1121"/>
        <source>Enter search content</source>
        <translation>ᠡᠷᠢᠬᠦ ᠠᠭᠤᠯᠭ᠎ᠠ ᠶᠢ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠣᠷᠣᠭᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1623"/>
        <location filename="../../src/widget.cpp" line="1637"/>
        <source>Delete this note</source>
        <translation>ᠲᠤᠰ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1624"/>
        <location filename="../../src/widget.cpp" line="1640"/>
        <source>Open this note</source>
        <translation>ᠡᠨᠡ ᠲᠡᠮᠳᠡᠭ᠍ᠯᠡᠯ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1625"/>
        <location filename="../../src/widget.cpp" line="1643"/>
        <source>Clear note list</source>
        <translation>ᠲᠡᠮᠳᠡᠭᠯᠡᠯ ᠦᠨ ᠬᠦᠰᠦᠨᠦᠭᠲᠦ ᠶᠢ ᠠᠷᠢᠯᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ</translation>
    </message>
    <message>
        <source>Delete Selected Note</source>
        <translatorcomment>Seçilen Notu Sil</translatorcomment>
        <translation type="vanished">delete Selected Note</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="651"/>
        <source>Switch View</source>
        <translatorcomment>Görünümü Değiştir</translatorcomment>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">close</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">minimize</translation>
    </message>
    <message>
        <source>Welcome to use Notes.</source>
        <translation type="vanished">welcome to use Notes.</translation>
    </message>
    <message>
        <source>[picture]</source>
        <translation type="vanished">[Picture]</translation>
    </message>
    <message>
        <source>Exit</source>
        <translatorcomment>Çıkış</translatorcomment>
        <translation type="vanished">ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Menu</source>
        <translatorcomment>Menü</translatorcomment>
        <translation type="vanished">menu</translation>
    </message>
</context>
<context>
    <name>emptyNotes</name>
    <message>
        <location filename="../../src/emptyNotes.ui" line="32"/>
        <source>Dialog</source>
        <translation>dialog</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="190"/>
        <source>Are you sure empty notebook?</source>
        <translation>ᠲᠠ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠳᠡᠪᠳᠡᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="251"/>
        <source>No Tips</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠰᠠᠨᠠᠭᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="305"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="328"/>
        <source>yes</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>emptyNotes</source>
        <translation type="vanished">ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠳᠡᠪᠳᠡᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>fontButton</name>
    <message>
        <source>Form</source>
        <translation type="vanished">ᠹᠣᠣᠮ</translation>
    </message>
</context>
<context>
    <name>iconViewModeDelegate</name>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="260"/>
        <source>[picture]</source>
        <translation>[ᠵᠢᠷᠤᠭ]</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="334"/>
        <source>Welcome to use Notes.</source>
        <translation>ᠬᠤᠶᠠᠷ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠨᠠᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>Today  </source>
        <translation type="vanished">ᠦᠨᠦᠳᠦᠷ  </translation>
    </message>
    <message>
        <source>Yesterday  </source>
        <translation type="vanished">ᠦᠴᠦᠭᠡᠳᠦᠷ  </translation>
    </message>
</context>
<context>
    <name>listViewModeDelegate</name>
    <message>
        <source>Today  </source>
        <translation type="vanished">ᠦᠨᠦᠳᠦᠷ  </translation>
    </message>
    <message>
        <source>Yesterday  </source>
        <translation type="vanished">ᠦᠴᠦᠭᠡᠳᠦᠷ  </translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="308"/>
        <source>[picture]</source>
        <translation>[ᠵᠢᠷᠤᠭ]</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="313"/>
        <source>Welcome to use Notes.</source>
        <translation>ᠬᠤᠶᠠᠷ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠨᠠᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
</context>
<context>
    <name>noteExitWindow</name>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="31"/>
        <source>Dialog</source>
        <translation>dialog</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="83"/>
        <source>Are you sure to exit the note book?</source>
        <translatorcomment>Not defterinden çıkmak istediğinizden emin misiniz?</translatorcomment>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠳᠡᠪᠳᠡᠷ ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="104"/>
        <source>Close the desktop note page at the same time</source>
        <translatorcomment>Masaüstü not sayfasını aynı anda kapatın</translatorcomment>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠵᠢ ᠵᠡᠷᠭᠡᠳᠡ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="149"/>
        <source>No</source>
        <translatorcomment>Hayır</translatorcomment>
        <translation>ᠦᠬᠡᠢ/ ᠪᠢᠰᠢ</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="171"/>
        <source>Yes</source>
        <translatorcomment>Evet</translatorcomment>
        <translation>ᠲᠡᠢ᠌ᠮᠦ/ ᠮᠦᠨ</translation>
    </message>
</context>
<context>
    <name>noteHead</name>
    <message>
        <location filename="../../src/noteHead.ui" line="14"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
</context>
<context>
    <name>noteHeadMenu</name>
    <message>
        <location filename="../../src/noteHeadMenu.ui" line="14"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="149"/>
        <source>Open note list</source>
        <translation>ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤᠨ ᠤ᠋ ᠨᠠᠭᠠᠯᠳᠠ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="150"/>
        <source>Always in the front</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢ ᠤᠷᠤᠢ ᠳ᠋ᠤ᠌ ᠨᠢ ᠪᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="151"/>
        <source>Delete this note</source>
        <translation>ᠲᠤᠰ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="152"/>
        <source>Share</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠯᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="99"/>
        <source>Create New Note</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠵᠠᠬᠢᠳᠠᠯ ᠤ᠋ᠨ ᠴᠠᠭᠠᠰᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="83"/>
        <source>Options</source>
        <translation>ᠰᠣᠩᠭᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="90"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="75"/>
        <source>Palette</source>
        <translation>ᠦᠩᠭᠡ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>paletteButton</name>
    <message>
        <location filename="../../src/paletteButton.ui" line="14"/>
        <source>Form</source>
        <translation>ᠹᠤᠤᠮ</translation>
    </message>
</context>
</TS>
