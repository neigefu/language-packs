<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>ImportWidget</name>
    <message>
        <location filename="../importwidget.cpp" line="54"/>
        <source>Drag and drop APK file here</source>
        <translation>將APK檔拖放到此處</translation>
    </message>
    <message>
        <location filename="../importwidget.cpp" line="64"/>
        <source>Note: installing the third party APK may run abnormally</source>
        <translation>注意：安裝第三方APK可能會運行異常</translation>
    </message>
    <message>
        <source>Note: Installing Android apps that are not available in the kylin-software-center may not work properly</source>
        <translation type="vanished">注意：安装未在软件商店上架的安卓应用，可能会运行异常</translation>
    </message>
    <message>
        <location filename="../importwidget.cpp" line="81"/>
        <source>Select APK File</source>
        <translation>選擇APK檔</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../infopage.cpp" line="43"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../infopage.cpp" line="46"/>
        <source>Run</source>
        <translation>跑</translation>
    </message>
</context>
<context>
    <name>InstallWidget</name>
    <message>
        <location filename="../installwidget.cpp" line="84"/>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="85"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation type="vanished">名字：</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Application Label: </source>
        <translation type="vanished">应用标签：</translation>
    </message>
    <message>
        <source>Application Zh Label: </source>
        <translation type="vanished">应用中文标签：</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation type="vanished">大小：</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="140"/>
        <location filename="../installwidget.cpp" line="310"/>
        <source>Note: installing the third party APK may run abnormally</source>
        <translation>注意：安裝第三方APK可能會運行異常</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="223"/>
        <source>Note: installing the third party APK may run abnormally
The CPU is FT1500A, limited by CPU performance, video App and game App are not effective.</source>
        <translation>注意：安裝第三方APK可能會運行異常
CPU為FT1500A，受CPU性能限制，視頻App和遊戲App無效。</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="227"/>
        <location filename="../installwidget.cpp" line="230"/>
        <location filename="../installwidget.cpp" line="233"/>
        <location filename="../installwidget.cpp" line="236"/>
        <source>Note: installing the third party APK may run abnormally
The graphics card is %1, limited by graphics card performance, video App and game App are not effective.</source>
        <translation>注意：安裝第三方APK可能會運行異常
顯卡為%1，受顯卡性能限制，視頻App和遊戲App無效。</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="230"/>
        <source>Zhaoxin</source>
        <translation>昭信</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="233"/>
        <source>virtual graphics card</source>
        <translation>虛擬顯卡</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="236"/>
        <source>JJM</source>
        <translation>傑傑姆</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="326"/>
        <source>Installing APK ......</source>
        <translation>安裝APK ...</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="331"/>
        <source>Copy APK failed</source>
        <translation>複製 APK 失敗</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="355"/>
        <source>Copying APK ......</source>
        <translation>複製APK ...</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="371"/>
        <source>APK does not exists</source>
        <translation>APK 不存在</translation>
    </message>
    <message>
        <source>APK Installed successfully</source>
        <translation type="vanished">APK安装成功</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="64"/>
        <location filename="../mainwindow.cpp" line="134"/>
        <source>KMRE APK Installer</source>
        <translation>KMRE APK 安裝程式</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="192"/>
        <location filename="../mainwindow.cpp" line="260"/>
        <source>KMRE environment not installed,please go to the app store to start the mobile environment</source>
        <translation>未安裝KMRE環境，請前往應用商店啟動移動環境</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="197"/>
        <source>APK package does not exist or file type is abnormal!</source>
        <translation>APK包不存在或文件類型異常！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="200"/>
        <source>Invalid APK file!</source>
        <translation>無效的APK檔！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="203"/>
        <source>Unknown error!</source>
        <translation>未知錯誤！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="208"/>
        <source>%1 install successfully</source>
        <translation>%1 安裝成功</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <source>%1 install failed</source>
        <translation>%1 安裝失敗</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <source>KMRE environment not installed</source>
        <translation>未安裝 KMRE 環境</translation>
    </message>
</context>
<context>
    <name>MetadataName</name>
    <message>
        <location filename="../installwidget.cpp" line="57"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="58"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="59"/>
        <source>Application</source>
        <translation>應用</translation>
    </message>
    <message>
        <source>Application Zh</source>
        <translation type="vanished">中文标签</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="60"/>
        <source>File size</source>
        <translation>檔大小</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../titlebar.cpp" line="137"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../titlebar.cpp" line="142"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
</TS>
