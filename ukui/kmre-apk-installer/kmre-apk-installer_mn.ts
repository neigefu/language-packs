<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ImportWidget</name>
    <message>
        <location filename="../importwidget.cpp" line="54"/>
        <source>Drag and drop APK file here</source>
        <translation>APK ᠹᠠᠢᠯ ᠢ᠋ ᠡᠨᠳᠡ ᠴᠢᠷᠴᠤ ᠠᠪᠴᠢᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../importwidget.cpp" line="64"/>
        <source>Note: installing the third party APK may run abnormally</source>
        <translation>ᠠᠩᠬᠠᠷ᠄ ᠭᠤᠷᠪᠠᠳᠠᠭᠴᠢ ᠡᠳᠡᠭᠡᠳ ᠤ᠋ᠨ APK ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠤᠭᠰᠠᠷᠬᠤ ᠨᠢ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠤᠯᠭᠠᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>Note: Installing Android apps that are not available in the kylin-software-center may not work properly</source>
        <translation type="vanished">注意：安装未在软件商店上架的安卓应用，可能会运行异常</translation>
    </message>
    <message>
        <location filename="../importwidget.cpp" line="81"/>
        <source>Select APK File</source>
        <translation>APKᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>InfoPage</name>
    <message>
        <location filename="../infopage.cpp" line="43"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../infopage.cpp" line="46"/>
        <source>Run</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠢ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>InstallWidget</name>
    <message>
        <location filename="../installwidget.cpp" line="84"/>
        <source>Install</source>
        <translation>ᠤᠭᠰᠠᠷᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="85"/>
        <source>Back</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Name: </source>
        <translation type="vanished">名字：</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本：</translation>
    </message>
    <message>
        <source>Application Label: </source>
        <translation type="vanished">应用标签：</translation>
    </message>
    <message>
        <source>Application Zh Label: </source>
        <translation type="vanished">应用中文标签：</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation type="vanished">大小：</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="140"/>
        <location filename="../installwidget.cpp" line="310"/>
        <source>Note: installing the third party APK may run abnormally</source>
        <translation>ᠠᠩᠬᠠᠷ᠄ ᠭᠤᠷᠪᠠᠳᠠᠭᠴᠢ ᠡᠳᠡᠭᠡᠳ ᠤ᠋ᠨ APK ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠤᠭᠰᠠᠷᠬᠤ ᠨᠢ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠤᠯᠭᠠᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="223"/>
        <source>Note: installing the third party APK may run abnormally
The CPU is FT1500A, limited by CPU performance, video App and game App are not effective.</source>
        <translation>ᠠᠩᠬᠠᠷ᠄ ᠭᠤᠷᠪᠠᠳᠠᠭᠴᠢ ᠡᠳᠡᠭᠡᠳ ᠤ᠋ᠨ APK ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠤᠭᠰᠠᠷᠬᠤ ᠨᠢ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠤᠯᠭᠠᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ
ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ ᠨᠢ FT1500A᠂ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠡᠴᠡ ᠪᠤᠯᠵᠤ ᠸᠢᠳᠢᠤ᠋ ᠪᠤᠯᠤᠨ ᠳᠤᠭᠯᠠᠭᠠᠮ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠨᠡᠯᠢᠶᠡᠳ ᠮᠠᠭᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="227"/>
        <location filename="../installwidget.cpp" line="230"/>
        <location filename="../installwidget.cpp" line="233"/>
        <location filename="../installwidget.cpp" line="236"/>
        <source>Note: installing the third party APK may run abnormally
The graphics card is %1, limited by graphics card performance, video App and game App are not effective.</source>
        <translation>ᠠᠩᠬᠠᠷ᠄ ᠭᠤᠷᠪᠠᠳᠠᠭᠴᠢ ᠡᠳᠡᠭᠡᠳ ᠤ᠋ᠨ APK ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠵᠢ ᠤᠭᠰᠠᠷᠬᠤ ᠨᠢ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠵᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠤᠯᠭᠠᠵᠤ ᠮᠡᠳᠡᠨ᠎ᠡ
ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠢᠯᠡᠷᠡᠬᠦᠢ ᠵᠢᠨ ᠺᠠᠷᠲ ᠨᠢ %1᠂ ᠢᠯᠡᠷᠡᠬᠦᠢ ᠵᠢᠨ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢᠨ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯ ᠡᠴᠡ ᠪᠤᠯᠵᠤ ᠸᠢᠳᠢᠤ᠋ ᠪᠤᠯᠤᠨ ᠳᠤᠭᠯᠠᠭᠠᠮ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ ᠨᠡᠯᠢᠶᠡᠳ ᠮᠠᠭᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="230"/>
        <source>Zhaoxin</source>
        <translation>ᠮᠸᠭ᠎ᠠ ᠭᠤᠤᠯ ᠢᠯᠡᠷᠡᠬᠦᠢ ᠵᠢᠨ ᠺᠠᠷᠲ ᠢ᠋ ᠬᠤᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="233"/>
        <source>virtual graphics card</source>
        <translation>ᠬᠡᠢᠰᠪᠦᠷᠢ ᠢᠯᠡᠷᠡᠬᠦᠢ ᠵᠢᠨ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="236"/>
        <source>JJM</source>
        <translation>ᠵᠢᠩ ᠵᠢᠶᠠ ᠪᠢᠴᠢᠯ ᠢᠯᠡᠷᠡᠬᠦᠢ ᠵᠢᠨ ᠺᠠᠷᠲ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="326"/>
        <source>Installing APK ......</source>
        <translation>APK ᠢ᠋/ ᠵᠢ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ …</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="331"/>
        <source>Copy APK failed</source>
        <translation>APK ᠹᠠᠢᠯ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="355"/>
        <source>Copying APK ......</source>
        <translation>APK ᠢ᠋/ ᠵᠢ ᠺᠤᠫᠢᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ…</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="371"/>
        <source>APK does not exists</source>
        <translation>APKᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ ᠣᠷᠣᠰᠢᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>APK Installed successfully</source>
        <translation type="vanished">APK安装成功</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="64"/>
        <location filename="../mainwindow.cpp" line="134"/>
        <source>KMRE APK Installer</source>
        <translation>KMRE APK ᠰᠠᠭᠤᠯᠭᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="192"/>
        <location filename="../mainwindow.cpp" line="260"/>
        <source>KMRE environment not installed,please go to the app store to start the mobile environment</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠤ᠋ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠭᠡ ᠦᠬᠡᠢ᠂ ᠰᠤᠹᠲ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠬᠦᠷ ᠲᠤ᠌ ᠬᠦᠷᠴᠤ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="197"/>
        <source>APK package does not exist or file type is abnormal!</source>
        <translation>APK ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="200"/>
        <source>Invalid APK file!</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ APK ᠹᠠᠢᠯ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="203"/>
        <source>Unknown error!</source>
        <translation>ᠮᠠᠳᠡᠬᠦ ᠦᠭᠡᠢ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="208"/>
        <source>%1 install successfully</source>
        <translation>%1 ᠤᠭᠰᠠᠷᠠᠵᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="211"/>
        <source>%1 install failed</source>
        <translation>%1 ᠤᠭᠰᠠᠷᠠᠴᠤ ᠲᠡᠢᠯᠦᠭᠰᠡᠨ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="159"/>
        <source>KMRE environment not installed</source>
        <translation>ᠴᠢ ᠯᠢᠨ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠠᠵᠢᠯᠯᠠᠭᠠᠨ ᠤ᠋ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠤᠭᠰᠠᠷᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠰᠤᠹᠲ ᠤ᠋ᠨ ᠳᠡᠯᠬᠡᠬᠦᠷ ᠲᠤ᠌ ᠬᠦᠷᠴᠤ ᠰᠢᠯᠵᠢᠮᠡᠯ ᠤᠷᠴᠢᠨ ᠢ᠋ ᠤᠭᠰᠠᠷᠠᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>MetadataName</name>
    <message>
        <location filename="../installwidget.cpp" line="57"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="58"/>
        <source>Version</source>
        <translation>ᠬᠡᠪᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="59"/>
        <source>Application</source>
        <translation>ᠱᠣᠱᠢᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source>Application Zh</source>
        <translation type="vanished">中文标签</translation>
    </message>
    <message>
        <location filename="../installwidget.cpp" line="60"/>
        <source>File size</source>
        <translation>ᠮᠠᠲ᠋ᠸᠷᠢᠶᠠᠯ ᠦ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../titlebar.cpp" line="137"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠪᠠᠭ᠎ᠠ ᠪᠣᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../titlebar.cpp" line="142"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
