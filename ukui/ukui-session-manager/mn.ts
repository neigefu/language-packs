<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>MainWindow</name>
    <message>
        <source>Form</source>
        <translation type="vanished">ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation type="vanished">ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation type="vanished">ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="vanished">ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="vanished">ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation type="vanished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../tools/main.cpp" line="293"/>
        <source>UKUI session tools, show the shutdown dialog without any arguments.</source>
        <translation>UKUI ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ ᠪᠠᠭᠠᠵᠢ ᠂ ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠫᠠᠷᠠᠮᠸᠲᠷ ᠦᠬᠡᠢ ᠬᠠᠭᠠᠭᠰᠠᠨ ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ ᠴᠤᠩᠬᠤ᠎ᠶᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="296"/>
        <source>ukui-session-tools</source>
        <translation>Ukui-session-tools</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="302"/>
        <source>Switch the user of this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢ ᠰᠤᠯᠢᠬᠤ᠄</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="305"/>
        <source>Hibernate this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠢᠴᠡᠬᠡᠯᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="308"/>
        <source>Suspend this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="311"/>
        <source>Logout this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="314"/>
        <source>Restart this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="317"/>
        <source>Shutdown this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠬᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="333"/>
        <source>system-monitor</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠢᠨᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="363"/>
        <source>Switch User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶ᠋ᠢ ᠰᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="364"/>
        <source>Hibernate</source>
        <translation>ᠢᠴᠡᠭᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="365"/>
        <source>Suspend</source>
        <translation>ᠤᠨᠲᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="obsolete">ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="obsolete">ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="366"/>
        <source>Log Out</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠬᠤ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="367"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠡᠬᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="368"/>
        <source>Shut Down</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="369"/>
        <source>Lock Screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠴᠣᠣᠵᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="370"/>
        <source>UpgradeThenRboot</source>
        <translation>ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠨ ᠰᠡᠩᠬᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="371"/>
        <source>UpgradeThenShutdown</source>
        <translation>ᠳᠡᠰ ᠳᠡᠪᠰᠢᠭᠰᠡᠨ ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠪᠠᠢ᠌ᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠨ ᠬᠠᠭᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="401"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>ᠬᠡᠳᠦ ᠬᠡᠳᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠠᠳᠠᠯᠢ ᠴᠠᠭ᠎ᠲᠤ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠲᠠᠢ ᠂ ᠲᠠ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="413"/>
        <source>cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="414"/>
        <source>confirm</source>
        <translation>ᠨᠣᠲ᠋ᠠᠯᠠᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source>(user),ukui-control-center is performing a system update or package installation.</source>
        <translation type="vanished">（ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ） ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ ᠳᠡᠰ ᠳᠡᠪᠱᠢᠬᠦ᠌ ᠪᠤᠶᠤᠰᠤᠹᠲ᠎ᠤᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ᠎ᠶᠢ ᠤᠭᠰᠠᠷᠠᠬᠤ/ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶᠢ ᠬᠦᠢᠴᠡᠳᠭᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>(user),yhkylin-backup-tools is performing a system backup or restore.</source>
        <translation type="vanished">（ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ） ᠂ᠬᠤᠪᠢᠯᠪᠤᠷᠢ᠎ᠶᠢ ᠬᠡᠪ᠎ᠲᠦ᠍ ᠨᠢ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨᠬᠠᠭᠤᠯᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠬᠡᠪ᠎ᠲᠦ᠍ ᠨᠢ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>For system security,Reboot、Shutdown、Logout and Hibernate are temporarily unavailable.</source>
        <translation type="vanished">ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠠᠶᠤᠯᠬᠦᠢ᠎ᠶᠢᠨ ᠳᠦᠯᠦᠬᠡ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌ ᠂ ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ ᠪᠤᠯᠤᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃</translation>
    </message>
    <message>
        <source>For system security,Reboot、Shutdown and Hibernate are temporarily unavailable.</source>
        <translation type="vanished">ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠠᠶᠤᠯᠬᠦᠢ᠎ᠶᠢᠨ ᠳᠦᠯᠦᠬᠡ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌᠂ᠬᠠᠭᠠᠬᠤ ᠪᠤᠯᠤᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃</translation>
    </message>
    <message>
        <location filename="../ukui-session/main.cpp" line="413"/>
        <source>UKUI Session Manager</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source> is block system</source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source> into sleep for reason </source>
        <translation type="vanished">休眠，因为</translation>
    </message>
    <message>
        <source> is block system </source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source>into sleep for reason </source>
        <translation type="vanished">睡眠，因为</translation>
    </message>
    <message>
        <source>Are you sure</source>
        <translation type="vanished">你确定</translation>
    </message>
    <message>
        <source> you want to get system into sleep?</source>
        <translation type="vanished">要让系统进入休眠吗?</translation>
    </message>
    <message>
        <source>Are you sure you want to get system into sleep?</source>
        <translation type="vanished">你确定要让系统进入睡眠吗?</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="63"/>
        <source>cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="62"/>
        <source>confirm</source>
        <translation>ᠨᠣᠲ᠋ᠠᠯᠠᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <source> is block system into reboot for reason </source>
        <translation type="vanished">阻止系统重启，因为</translation>
    </message>
    <message>
        <source> is block system into shutdown for reason </source>
        <translation type="vanished">阻止系统关机，因为</translation>
    </message>
    <message>
        <source>Are you sure you want to reboot?</source>
        <translation type="vanished">你确定要重启系统吗？</translation>
    </message>
    <message>
        <source>Are you sure you want to shutdown?</source>
        <translation type="vanished">你确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="57"/>
        <source>conform</source>
        <translation>ᠵᠢᠷᠤᠮᠯᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="61"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>ᠡᠭᠦᠨ᠎ᠦ᠌ ᠬᠠᠮᠲᠤ ᠣᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠳᠠᠩᠰᠠᠯᠠᠬᠤ ᠪᠠᠢᠳᠠᠯ᠎ᠳ᠋ᠤ᠌ ᠣᠷᠣᠰᠢᠵᠤ ᠂ ᠳᠠ ᠰᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠭᠠᠷᠬᠤ ᠤᠤ ? ︖</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="84"/>
        <source>System update or package installation in progress,this function is temporarily unavailable.</source>
        <translation>ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ ᠳᠡᠰ ᠳᠡᠪᠱᠢᠬᠦ᠌ ᠪᠤᠶᠤ ᠤᠭᠰᠠᠷᠠᠬᠤ/ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ （ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ）᠎ᠶᠢ ᠬᠦᠢᠴᠡᠳᠭᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠡᠨᠡ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="85"/>
        <source>System backup or restore in progress,this function is temporarily unavailable.</source>
        <translation>ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠵᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠬᠡᠪ᠎ᠲᠦ᠍ ᠨᠢ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠡᠨᠡ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="97"/>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1448"/>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1451"/>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠤᠨᠳᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1454"/>
        <source>The following program is running to prevent the system from logout!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠢ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1457"/>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1460"/>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠬᠠᠭᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1519"/>
        <source>Still Hibernate</source>
        <translation>ᠢᠴᠡᠭᠡᠯᠡᠭᠰᠡᠬᠡᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1521"/>
        <source>Still Suspend</source>
        <translation>ᠤᠨᠳᠠᠭᠰᠠᠭᠠᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1527"/>
        <source>Still Reboot</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠬᠡᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1529"/>
        <source>Still Shutdown</source>
        <translation>ᠬᠠᠭᠠᠭᠰᠠᠭᠠᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1547"/>
        <source>Cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="61"/>
        <source>some applications are running and they don&apos;t want you to do this.</source>
        <translation>ᠵᠠᠷᠢᠮ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠳᠡᠭᠡᠬᠦ᠌ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠡᠨᠡ ᠨᠢ ᠲᠠᠨ᠎ᠤ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠬᠦᠢᠴᠡᠳᠭᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠦᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="63"/>
        <source>Still to do!</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠭᠡᠭᠰᠡᠬᠡᠷ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="64"/>
        <source>give up</source>
        <translation>ᠳᠡᠪᠴᠢᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuismserver/ukuismserver.cpp" line="574"/>
        <source> canceled this operation</source>
        <translation> ᠡᠨᠡ ᠠᠵᠢᠯ ᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠵᠠᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuismserver/ukuismserver.cpp" line="933"/>
        <source> not respond</source>
        <translation> ᠲᠠᠭᠠᠬᠤ ᠦᠭᠡᠢ᠃</translation>
    </message>
    <message>
        <location filename="../ukuismserver/ukuismserver.cpp" line="1603"/>
        <source>ukui-session</source>
        <translation>ᠤᠯᠠᠭᠠᠨᠺᠦ ᠬᠤᠷᠠᠯ᠃</translation>
    </message>
    <message>
        <location filename="../ukuismserver/ukuismserver.cpp" line="1606"/>
        <source>Tips</source>
        <translation>ᠮᠡᠷᠭᠡᠵᠢᠯ ᠃</translation>
    </message>
</context>
</TS>
