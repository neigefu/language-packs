<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Form</source>
        <translation>類型</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Switch User</source>
        <translation>切換使用者</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Logout</source>
        <translation>註銷</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Reboot</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Shut Down</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Lock Screen</source>
        <translation>鎖屏</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../tools/main.cpp" line="283"/>
        <source>UKUI session tools, show the shutdown dialog without any arguments.</source>
        <translation>UKUI 工作階段工具，顯示不帶任何參數的關閉對話框。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="291"/>
        <source>Switch the user of this computer.</source>
        <translation>切換這台計算機的使用者。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="294"/>
        <source>Hibernate this computer.</source>
        <translation>讓這台電腦休眠。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="297"/>
        <source>Suspend this computer.</source>
        <translation>暫停這台電腦。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="300"/>
        <source>Logout this computer.</source>
        <translation>註銷這台電腦。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="303"/>
        <source>Restart this computer.</source>
        <translation>重啟這台電腦。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="306"/>
        <source>Shutdown this computer.</source>
        <translation>關閉這台電腦。</translation>
    </message>
    <message>
        <source>system-monitor</source>
        <translation type="vanished">系统监视器</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="391"/>
        <source>Switch User</source>
        <translation>切換使用者</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="392"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="393"/>
        <source>Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="394"/>
        <source>Logout</source>
        <translation>註銷</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="395"/>
        <source>Reboot</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="396"/>
        <source>Shut Down</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="397"/>
        <source>Lock Screen</source>
        <translation>鎖屏</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="423"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>同時有多個使用者處於登錄狀態，你確定要退出系統嗎？</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="435"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="436"/>
        <source>confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="503"/>
        <source>(user),ukui-control-center is performing a system update or package installation.</source>
        <translation>（使用者），系統正在進行升級或套件安裝/卸載相關操作。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="505"/>
        <source>(user),yhkylin-backup-tools is performing a system backup or restore.</source>
        <translation>（使用者），備份還原工具正在進行備份或還原。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="507"/>
        <source>For system security,Reboot、Shutdown、Logout and Hibernate are temporarily unavailable.</source>
        <translation>為了系統安全，重啟、關機、註銷和休眠功能暫時不可用。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="509"/>
        <source>For system security,Reboot、Shutdown and Hibernate are temporarily unavailable.</source>
        <translation>為了系統安全，重啟、關機和休眠功能暫時不可用。</translation>
    </message>
    <message>
        <location filename="../ukui-session/main.cpp" line="397"/>
        <source>UKUI Session Manager</source>
        <translation>UKUI 工作階段管理員</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source> is block system</source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source> into sleep for reason </source>
        <translation type="vanished">休眠，因为</translation>
    </message>
    <message>
        <source> is block system </source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source>into sleep for reason </source>
        <translation type="vanished">睡眠，因为</translation>
    </message>
    <message>
        <source>Are you sure</source>
        <translation type="vanished">你确定</translation>
    </message>
    <message>
        <source> you want to get system into sleep?</source>
        <translation type="vanished">要让系统进入休眠吗?</translation>
    </message>
    <message>
        <source>Are you sure you want to get system into sleep?</source>
        <translation type="vanished">你确定要让系统进入睡眠吗?</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="61"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="62"/>
        <source>confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <source> is block system into reboot for reason </source>
        <translation type="vanished">阻止系统重启，因为</translation>
    </message>
    <message>
        <source> is block system into shutdown for reason </source>
        <translation type="vanished">阻止系统关机，因为</translation>
    </message>
    <message>
        <source>Are you sure you want to reboot?</source>
        <translation type="vanished">你确定要重启系统吗？</translation>
    </message>
    <message>
        <source>Are you sure you want to shutdown?</source>
        <translation type="vanished">你确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="60"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>同時有多個使用者處於登錄狀態，你確定要退出系統嗎？</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="82"/>
        <source>System update or package installation in progress,this function is temporarily unavailable.</source>
        <translation>正在進行升級或安裝/卸載（相關操作），這個功能暫時不可用。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="83"/>
        <source>System backup or restore in progress,this function is temporarily unavailable.</source>
        <translation>正在進行系統備份或還原，這個功能暫時不可用。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="95"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1381"/>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>以下程式正在運行，阻止系統進入休眠！</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1384"/>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>以下程式正在運行，阻止系統進入睡眠！</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1387"/>
        <source>The following program is running to prevent the system from logout!</source>
        <translation>以下程式正在運行，阻止系統註銷！</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1390"/>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>以下程式正在運行，阻止系統重啟！</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1393"/>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>以下程式正在運行，阻止系統關機！</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1454"/>
        <source>Still Hibernate</source>
        <translation>仍然休眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1456"/>
        <source>Still Suspend</source>
        <translation>仍然睡眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1460"/>
        <source>Still Reboot</source>
        <translation>仍然重啟</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1462"/>
        <source>Still Shutdown</source>
        <translation>仍然關機</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1478"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="61"/>
        <source>some applications are running and they don&apos;t want you to do this.</source>
        <translation>一些程式正在運行，而且它不希望你繼續該操作。</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="63"/>
        <source>Still to do!</source>
        <translation>仍然執行</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="64"/>
        <source>give up</source>
        <translation>放棄</translation>
    </message>
</context>
</TS>
