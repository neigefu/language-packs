<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <source>Switch User</source>
        <translation type="vanished">切换用户</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation type="vanished">休眠</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation type="vanished">睡眠</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="vanished">重启</translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation type="vanished">关机</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">锁屏</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../tools/main.cpp" line="293"/>
        <source>UKUI session tools, show the shutdown dialog without any arguments.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="296"/>
        <source>ukui-session-tools</source>
        <translation>UKUI会话工具</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="302"/>
        <source>Switch the user of this computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="305"/>
        <source>Hibernate this computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="308"/>
        <source>Suspend this computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="311"/>
        <source>Logout this computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="314"/>
        <source>Restart this computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="317"/>
        <source>Shutdown this computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="333"/>
        <source>system-monitor</source>
        <translation>系统监视器</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="371"/>
        <source>Switch User</source>
        <translation>切换用户</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="372"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="373"/>
        <source>Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="vanished">注销</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="vanished">重启</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="374"/>
        <source>Log Out</source>
        <translation>注销</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="375"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="376"/>
        <source>Shut Down</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="377"/>
        <source>Lock Screen</source>
        <translation>锁屏</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="378"/>
        <source>UpgradeThenRboot</source>
        <translation>更新并重启</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="379"/>
        <source>UpgradeThenShutdown</source>
        <translation>更新并关机</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="392"/>
        <source>Turn off your computer, but the app stays open. When the computer is turned on, it can be restored to the state you left.</source>
        <translation>关闭电脑，但是应用会一直保持打开状态。当打开电脑时，可以恢复到你离开的状态。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="393"/>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off.</source>
        <translation>电脑保持开机状态，但耗电较少。应用会一直保持打开状态，可快速唤醒电脑并恢复到你离开的状态。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="394"/>
        <source>The current user logs out of the system, terminates the session, and returns to the login page.</source>
        <translation>当前用户从系统中注销，结束其会话并返回登录界面。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="395"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on.</source>
        <translation>关闭所有应用，关闭电脑，然后重新打开电脑。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="396"/>
        <source>Close all apps, and then shut down your computer.</source>
        <translation>关闭所有应用，然后关闭电脑。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="415"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>同时有多个用户处于登录状态，你确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="427"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="428"/>
        <source>confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>(user),ukui-control-center is performing a system update or package installation.</source>
        <translation type="vanished">（用户），系统正在进行升级或软件包安装/卸载相关操作。</translation>
    </message>
    <message>
        <source>(user),yhkylin-backup-tools is performing a system backup or restore.</source>
        <translation type="vanished">（用户），备份还原工具正在进行备份或还原。</translation>
    </message>
    <message>
        <source>For system security,Reboot、Shutdown、Logout and Hibernate are temporarily unavailable.</source>
        <translation type="vanished">为了系统安全，重启、关机、注销和休眠功能暂时不可用。</translation>
    </message>
    <message>
        <source>For system security,Reboot、Shutdown and Hibernate are temporarily unavailable.</source>
        <translation type="vanished">为了系统安全，重启、关机和休眠功能暂时不可用。</translation>
    </message>
    <message>
        <location filename="../ukui-session/main.cpp" line="425"/>
        <source>UKUI Session Manager</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source> is block system</source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source> into sleep for reason </source>
        <translation type="vanished">休眠，因为</translation>
    </message>
    <message>
        <source> is block system </source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source>into sleep for reason </source>
        <translation type="vanished">睡眠，因为</translation>
    </message>
    <message>
        <source>Are you sure</source>
        <translation type="vanished">你确定</translation>
    </message>
    <message>
        <source> you want to get system into sleep?</source>
        <translation type="vanished">要让系统进入休眠吗?</translation>
    </message>
    <message>
        <source>Are you sure you want to get system into sleep?</source>
        <translation type="vanished">你确定要让系统进入睡眠吗?</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="63"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="62"/>
        <source>confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <source> is block system into reboot for reason </source>
        <translation type="vanished">阻止系统重启，因为</translation>
    </message>
    <message>
        <source> is block system into shutdown for reason </source>
        <translation type="vanished">阻止系统关机，因为</translation>
    </message>
    <message>
        <source>Are you sure you want to reboot?</source>
        <translation type="vanished">你确定要重启系统吗？</translation>
    </message>
    <message>
        <source>Are you sure you want to shutdown?</source>
        <translation type="vanished">你确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="57"/>
        <source>ukui-session-tools</source>
        <translation>UKUI会话工具</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="61"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>同时有多个用户处于登录状态，你确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="84"/>
        <source>System update or package installation in progress,this function is temporarily unavailable.</source>
        <translation>正在进行升级或安装/卸载（相关操作），这个功能暂时不可用。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="85"/>
        <source>System backup or restore in progress,this function is temporarily unavailable.</source>
        <translation>正在进行系统备份或还原，这个功能暂时不可用。</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="97"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1476"/>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>以下程序阻止休眠，您可以点击“取消”然后关闭这些程序。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1479"/>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>以下程序阻止睡眠，您可以点击“取消”然后关闭这些程序。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1482"/>
        <source>The following program is running to prevent the system from logout!</source>
        <translation>以下程序阻止注销，您可以点击“取消”然后关闭这些程序。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1485"/>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>以下程序阻止重启，您可以点击“取消”然后关闭这些程序。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1488"/>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>以下程序阻止关机，您可以点击“取消”然后关闭这些程序。</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1547"/>
        <source>Still Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1549"/>
        <source>Still Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1555"/>
        <source>Still Reboot</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1557"/>
        <source>Still Shutdown</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1575"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="61"/>
        <source>some applications are running and they don&apos;t want you to do this.</source>
        <translation>一些程序正在运行，而且它不希望你继续该操作。</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="63"/>
        <source>Still to do!</source>
        <translation>仍然执行！</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="64"/>
        <source>give up</source>
        <translation>放弃</translation>
    </message>
    <message>
        <source> cancel shutdown</source>
        <translation type="vanished">取消了本次注销</translation>
    </message>
    <message>
        <location filename="../ukuismserver/ukuismserver.cpp" line="574"/>
        <source> canceled this operation</source>
        <translation>取消了本次操作</translation>
    </message>
    <message>
        <location filename="../ukuismserver/ukuismserver.cpp" line="934"/>
        <source> not respond</source>
        <translation>未响应</translation>
    </message>
    <message>
        <location filename="../ukui-session/sessionapplication.cpp" line="191"/>
        <location filename="../ukuismserver/ukuismserver.cpp" line="1605"/>
        <source>ukui-session</source>
        <translation>会话管理器</translation>
    </message>
    <message>
        <location filename="../ukui-session/sessionapplication.cpp" line="195"/>
        <source>The current user has logged in to the system! Currently, the system does not support more than one user to log in at the same time. Click the button below to log out.</source>
        <translation>当前用户已经在系统中登录！目前系统不支持同时登录一个以上相同用户，点击下方按钮退出。</translation>
    </message>
    <message>
        <location filename="../ukuismserver/ukuismserver.cpp" line="1608"/>
        <source>Tips</source>
        <translation>温馨提示</translation>
    </message>
    <message>
        <source>Logout Warning</source>
        <translation type="vanished">注销提醒</translation>
    </message>
</context>
</TS>
