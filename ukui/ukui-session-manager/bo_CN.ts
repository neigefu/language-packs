<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>MainWindow</name>
    <message>
        <source>Logout</source>
        <translation type="vanished">ད་ལྟ་རྩིས་ཐེམ་ནས་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="vanished">བསྐྱར་སློང་།</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation type="vanished">བརྙན་ཡོལ་ཟྭ་རྒྱག</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Hebernate</source>
        <translation type="vanished">ངལ་གསོ།</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation type="vanished">སྤྱོད་མཁན་བརྗེ་རེས།</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation type="vanished">གཉིད་པ།</translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation type="vanished">སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="vanished">རྣམ་པ།</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation type="vanished">མལ་གསོ།</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt; &lt;head/&gt; &lt;body&gt; &lt;p&gt; &lt;br/&gt; &lt;/p&gt; &lt;/body&gt; &lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <source>(user),yhkylin-backup-tools is performing a system backup or restore.</source>
        <translation type="vanished">（བཀོལ་མི།）ཆིས་ལིན་གྲབས་ཉར་སླར་གསོ་ཡོ་ཆས་ཀྱིས་གྲབས་ཉར་རམ་སླར་གསོ་བྱེད་སྒང་རེད།</translation>
    </message>
    <message>
        <source>(user),ukui-control-center is performing a system update or package installation.</source>
        <translation type="vanished">（བཀོལ་མི།）ཚོད་འཛིན་ངོས་པང་གིས་བརྒྱུད་ཁོངས་རིམ་སྤོར་རམ་མཉེན་ཆས་ནང་འཇུག་བྱེད་སྒང་རེད།</translation>
    </message>
    <message>
        <source>For system security,Reboot、Shutdown、Logout and Hibernate are temporarily unavailable.</source>
        <translation type="vanished">བརྒྱུད་ཁོངས་བདེ་འཇགས་ལ་དམིགས་ཏེ་སྒོ་བསྐྱར་འབྱེད་དང་སྒོ་གཏན་པ། ཐོ་གསུབ་པ། ངལ་གསོ་བྱེད་པ་སོགས་ཀྱི་བྱེད་ནུས་གནས་སྐབས་རིང་བཀོལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>བཀོལ་མི་མང་པོ་དུས་མཉམ་དུ་ཐོ་ཞུགས་རྣམ་པར་གནས་འདུག་པས། བརྒྱུད་ཁོངས་སྒོ་གཏན་ནམ།</translation>
    </message>
    <message>
        <source>For system security,Reboot、Shutdown and Hibernate are temporarily unavailable.</source>
        <translation type="vanished">བརྒྱུད་ཁོངས་བདེ་འཇགས་ལ་དམིགས་ཏེ་སྒོ་བསྐྱར་འབྱེད་དང་སྒོ་གཏན་པ། ཐོབ་བསུབ་པ། ངལ་གསོ་བྱེད་པ་སོགས་ཀྱི་བྱེད་ནུས་གནས་སྐབས་རིང་བཀོལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>UKUI session tools, show the shutdown dialog without any arguments.</source>
        <translation>UKUIཚོགས་འདུའི་ཡོ་བྱད་ལ་རྩོད་གླེང་ཅི་ཡང་མེད་པར་ལས་མཚམས་འཇོག་པའི་གླེང་མོལ་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>Switch the user of this computer.</source>
        <translation>རྩིས་འཁོར་འདིའི་སྤྱོད་མཁན་བརྗེ་རེས་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Hibernate this computer.</source>
        <translation>རྩིས་འཁོར་འདི་གཉིད་དུ་བཅུག</translation>
    </message>
    <message>
        <source>Suspend this computer.</source>
        <translation>རྩིས་འཁོར་འདི་མཚམས་བཞག</translation>
    </message>
    <message>
        <source>Logout this computer.</source>
        <translation>རྩིས་འཁོར་འདི་ཐོ་འགོད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Restart this computer.</source>
        <translation>རྩིས་འཁོར་འདི་བསྐྱར་དུ་འགོ་བརྩམས།</translation>
    </message>
    <message>
        <source>Shutdown this computer.</source>
        <translation>གློག་ཀླད་འདི་སྒོ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <source>Switch User</source>
        <translation>སྤྱོད་མཁན་བརྗེ་བ།</translation>
    </message>
    <message>
        <source>Hibernate</source>
        <translation>མལ་གསོ།</translation>
    </message>
    <message>
        <source>Suspend</source>
        <translation>གཉིད་འཇོག</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="vanished">དོར་བ།</translation>
    </message>
    <message>
        <source>Reboot</source>
        <translation type="vanished">བསྐྱར་དུ་འཁོར་བ།</translation>
    </message>
    <message>
        <source>Shut Down</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Lock Screen</source>
        <translation>ཟྭ་ངོས།</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>UKUI Session Manager</source>
        <translation>UKUIཚོགས་འདུའི་སྤྱི་གཉེར་བ།</translation>
    </message>
    <message>
        <source>ukui-session-tools</source>
        <translation>ukuiཚོགས་འདུའི་ཡོ་བྱད།</translation>
    </message>
    <message>
        <source>system-monitor</source>
        <translation>མ་ལག་ལྟ་ཞིབ་ཚད་ལེན</translation>
    </message>
    <message>
        <source>Log Out</source>
        <translation>ཐོ་ཁོངས་ནས་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation>བསྐྱར་དུ་འཁོར་བ།</translation>
    </message>
    <message>
        <source>UpgradeThenRboot</source>
        <translation>རིམ་པ་འཕར་བའི་མི་</translation>
    </message>
    <message>
        <source>UpgradeThenShutdown</source>
        <translation>རིམ་པ་འཕར་བའི་དུས་ཚོད།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Still to do!</source>
        <translation>ལག་བསྟར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>some applications are running and they dont want you to do this.</source>
        <translation type="vanished">བྱ་རིམ་རེ་འགའ་འཁོར་སྐྱོད་བྱེད་སྒང་ཡིན་པར་མ་ཟད། དེས་བྱ་བ་འདི་མི་བསྒྲུབ་པའི་རེ་འདོན་བྱ་བཞིག་འདུག</translation>
    </message>
    <message>
        <source>System update or package installation in progress,this function is temporarily unavailable.</source>
        <translation>རིམ་པ་འཕར་བའམ་ཡང་ན་སྒྲིག་སྦྱོར་བྱེད་བཞིན་ཡོད།(འབྲེལ་ཡོད་བཀོལ་སྤྱོད་)བྱེད་ནུས་འདི་གནས་སྐབས་སུ་བཀོལ་མི་རུང་།</translation>
    </message>
    <message>
        <source>System backup or restore in progress,this function is temporarily unavailable.</source>
        <translation>བརྒྱུད་ཁོངས་གྲབས་ཉར་རམ་སླར་གསོ་བྱེད་སྒང་ཡིན་པས། བྱེད་ནུས་འདི་གནས་སྐབས་རིང་བཀོལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>notice</source>
        <translation type="vanished">གསལ་བརྡ།</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>give up</source>
        <translation>བློས་གཏང་བ།</translation>
    </message>
    <message>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>དུས་མཚུངས་སུ་སྤྱོད་མཁན་མང་པོ་ཞིག་ཐོ་འགོད་ཀྱི་རྣམ་པར་གནས་ཡོད་པས་ཁྱོད་ཀྱིས་མ་ལག་ལས་ཕྱིར་འབུད་རྒྱུ་ཡིན་པ་ཁག་ཐེག་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>གཤམ་གྱི་བྱ་རིམ་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད་པས་མ་ལག་གཉིད་དུ་ཡུར་བར་བཀག་འགོག་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་གནས་སྐབས་མཚམས་འཇོག་པར་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from logout!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་གིས་ཐོ་འགོད་བྱེད་པར་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་བསྐྱར་དུ་འབྱུང་བར་སྔོན་འགོག་བྱེད་ཆེད་ཡིན།</translation>
    </message>
    <message>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>གཤམ་གྱི་གོ་རིམ་ནི་མ་ལག་གི་སྒོ་རྒྱག་པར་བཀག་འགོག་བྱེད་པའི་ཆེད་དུ་ཡིན།</translation>
    </message>
    <message>
        <source>Still Hibernate</source>
        <translation>སྔར་བཞིན་མལ་གསོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Still Suspend</source>
        <translation>སྔར་བཞིན་གཉིད་པ།</translation>
    </message>
    <message>
        <source>Still Reboot</source>
        <translation>སྔར་བཞིན་བསྐྱར་དུ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>Still Shutdown</source>
        <translation>སྔར་བཞིན་ལས་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>some applications are running and they don&apos;t want you to do this.</source>
        <translation>བརྒྱུད་རིམ་ཁ་ཤས་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད་པ་མ་ཟད་དེས་ཁྱོད་ཀྱིས་མུ་མཐུད་དུ་བཀོལ་སྤྱོད་བྱེད་པར་རེ་བ་མི་བྱེད།</translation>
    </message>
    <message>
        <source> canceled this operation</source>
        <translation> ཐེངས་འདིའི་བྱ་སྤྱོད་མེད་པར་བཟོས།</translation>
    </message>
    <message>
        <source> not respond</source>
        <translation> ལན་མི་འདེབས།</translation>
    </message>
    <message>
        <source>ukui-session</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་ཚོགས་འདུ།</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation>གསལ་འདེབས།</translation>
    </message>
    <message>
        <source>ukui-session-tools</source>
        <translation>ukuiཚོགས་འདུའི་ཡོ་བྱད།</translation>
    </message>
</context>
</TS>
