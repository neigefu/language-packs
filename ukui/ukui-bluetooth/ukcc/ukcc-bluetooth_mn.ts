<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BlueToothMainWindow</name>
    <message>
        <source>Bluetooth adapter is abnormal !</source>
        <translation>ᠬᠦᠬᠡ ᠰᠢᠳᠦ ᠲᠠᠭᠠᠷᠠᠭᠤᠯᠤᠭᠤᠷ ᠬᠡᠪ ᠤᠨ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <source>Turn on :</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ:</translation>
        <extra-contents_path>/bluetooth/Turn on :</extra-contents_path>
    </message>
    <message>
        <source>Show icon on taskbar</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠳᠡᠭᠡᠷ᠎ᠡ ᠵᠢᠷᠤᠭ ᠤᠨ ᠲᠡᠮᠳᠡᠭ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
        <extra-contents_path>/bluetooth/Show icon on taskbar</extra-contents_path>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation>ᠣᠶᠢᠷ᠎ᠠ ᠬᠠᠪᠢ ᠶᠢᠨ ᠬᠥᠬᠡ ᠰᠢᠳᠦᠨ ᠦ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠢᠯᠡᠷᠡᠭᠦᠯᠵᠦ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
        <extra-contents_path>/bluetooth/Discoverable by nearby Bluetooth devices</extra-contents_path>
    </message>
    <message>
        <source>My Devices</source>
        <translation>ᠮᠢᠨᠦ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation>ᠪᠤᠰᠤᠳ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ ᠳᠠᠪᠲᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <source>Bluetooth adapter not detected !</source>
        <translation>ᠬᠥᠬᠡ ᠰᠢᠳᠦᠨ ᠦ ᠲᠣᠬᠢᠷᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠶᠢ ᠪᠠᠶᠢᠴᠠᠭᠠᠨ ᠰᠢᠯᠭᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>Adapter List</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠶᠢᠨ ᠵᠢᠭ᠌ᠰᠠᠭᠠᠯᠲᠠ ᠶᠢᠨ ᠬᠦᠰᠦᠨᠦᠭ</translation>
        <extra-contents_path>/bluetooth/Adapter List</extra-contents_path>
    </message>
    <message>
        <source>Auto discover Bluetooth audio devices</source>
        <translation>ᠯᠠᠨᠶᠠ ᠠᠦ᠋ᠳᠢᠤ᠋ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/bluetooth/Auto discover Bluetooth audio devices</extra-contents_path>
    </message>
    <message>
        <source>All</source>
        <translation>ᠪᠦᠬᠦᠪᠥᠷ</translation>
    </message>
    <message>
        <source>Peripherals</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation>ᠺᠣᠮᠫᠢᠦᠢᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>Bluetooth</name>
    <message>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
</context>
<context>
    <name>BluetoothNameLabel</name>
    <message>
        <source>Can now be found as &quot;%1&quot;</source>
        <translation>ᠤᠳᠤ ᠪᠡᠷ ᠢᠯᠡᠷᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Click to change the device name</source>
        <translation>ᠳᠤᠪᠰᠢᠵᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DevRemoveDialog</name>
    <message>
        <source>After it is removed, the PIN code must be matched for the next connection.</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠤᠰᠠᠳᠬᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ᠂ ᠳᠠᠬᠢᠭᠠᠳ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ PIN ᠺᠤᠳ᠋ ᠲᠠᠢ ᠠᠪᠤᠴᠠᠯᠳᠤᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Are you sure to remove %1 ?</source>
        <translation>%1 ᠵᠢ/ ᠢ᠋ ᠯᠠᠪᠳᠠᠢ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Connection failed! Please remove it before connecting.</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠤᠰᠠᠳᠬᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠭᠠᠳ ᠴᠦᠷᠬᠡᠯᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢ ᠬᠤᠯᠪᠤᠪᠠ</translation>
    </message>
</context>
<context>
    <name>DevRenameDialog</name>
    <message>
        <source>Rename</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>The value contains 1 to 32 characters</source>
        <translation>ᠤᠷᠳᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1-32 ᠦᠰᠦᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <source>Rename device</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠬᠦᠨᠳᠦ ᠨᠡᠷᠡᠶᠢᠳᠬᠦ</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>bluetoothdevicefunc</name>
    <message>
        <source>sendFile</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠶᠠᠪᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>remove</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <source>connect</source>
        <translation>ᠵᠠᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>disconnect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>rename</source>
        <translation>ᠬᠦᠨᠳᠦ ᠨᠡᠷᠡᠶᠢᠳᠦᠨ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>bluetoothdeviceitem</name>
    <message>
        <source>unknown</source>
        <translation>ᠮᠡᠳᠡᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Connecting</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Disconnecting</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠳᠠᠰᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Not Paired</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠭᠡᠶ</translation>
    </message>
    <message>
        <source>Not Connected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>Connect fail,Please try again</source>
        <translation>ᠬᠣᠯᠪᠣᠭᠳᠠᠭᠰᠠᠨ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ ᠂ ᠳᠠᠬᠢᠨ ᠰᠢᠯᠭᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Disconnection Fail</source>
        <translation>ᠬᠣᠯᠪᠣᠯᠲᠠ ᠶᠢ ᠲᠠᠰᠤᠯᠤᠨ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
</TS>
