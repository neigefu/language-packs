<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BlueToothMain</name>
    <message>
        <source>Show icon on taskbar</source>
        <translation>ལས་འགན་སྒྲུབ་སའི་སྟེང་ནས་མཚོན་རྟགས་མངོན་པ།</translation>
        <extra-contents_path>/bluetooth/Show icon on taskbar</extra-contents_path>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation>ཉེ་འགྲམ་གྱི་སོ་སྔོན་སྒྲིག་ཆས་ཀྱིས་ཤེས་རྟོགས་བྱུང་།</translation>
        <extra-contents_path>/bluetooth/Discoverable</extra-contents_path>
    </message>
    <message>
        <source>My Devices</source>
        <translation>ངའི་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation>སྒྲིག་ཆས་གཞན་དག</translation>
        <extra-contents_path>/bluetooth/Other Devices</extra-contents_path>
    </message>
    <message>
        <source>Bluetooth adapter</source>
        <translation>སོ་སྔོན་འཚམ་སྒྲིག་ཆས་།</translation>
        <extra-contents_path>/Bluetooth/Bluetooth adapter</extra-contents_path>
    </message>
    <message>
        <source>Turn on</source>
        <translation>ཁ་ཕྱེ་བ།</translation>
        <extra-contents_path>/bluetooth/Turn on Bluetooth</extra-contents_path>
    </message>
    <message>
        <source>All</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <source>Peripherals</source>
        <translation>མཐའ་སྐོར་གྱི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <source>PC</source>
        <translation>གློག་ཀླད།</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>ཁ་པར།</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
    </message>
    <message>
        <source>Bluetooth driver abnormal</source>
        <translation>སོ་སྔོན་སྒུལ་ཤུགས་རྒྱུན་ལྡན་མིན་པ།</translation>
    </message>
    <message>
        <source>Auto discover Bluetooth audio devices</source>
        <translation>རང་འགུལ་གྱིས་སོ་སྔོན་སྒྲིལ་ཆས་རྙེད་པ།</translation>
        <extra-contents_path>/bluetooth/Automatically discover Bluetooth audio devices</extra-contents_path>
    </message>
</context>
<context>
    <name>BlueToothMainWindow</name>
    <message>
        <source>Bluetooth adapter is abnormal !</source>
        <translation type="obsolete">སོ་སྔོན་བཀོད་སྒྲིག་ཡོ་ཆས་རྒྱུན་ལྡན་མིན།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="obsolete">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Turn on :</source>
        <translation type="obsolete">སྒོ་འབྱེད།</translation>
    </message>
    <message>
        <source>Show icon on taskbar</source>
        <translation type="obsolete">འགན་བྱང་དུ་སོ་སྔོན་རིས་རྟགས་འཆར་བ།</translation>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation type="obsolete">ཉེ་འཁོར་གྱི་སོ་སྔོན་སྒྲིག་ཆས་ཀྱི་བཤེར་རུང་བ།</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="obsolete">སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="obsolete">སྒྲིག་ཆས་གཞན་དག</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">དྲ་བྱང་།</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="obsolete">གཞན་དག</translation>
    </message>
</context>
<context>
    <name>Bluetooth</name>
    <message>
        <source>Bluetooth</source>
        <translation>སོ་སྔོན།</translation>
    </message>
</context>
<context>
    <name>BluetoothNameLabel</name>
    <message>
        <source>Tip</source>
        <translation>གསལ་འདེབས།</translation>
    </message>
    <message>
        <source>Double-click to change the device name</source>
        <translation type="vanished">ཟུང་རྡེབ་བྱས་ཏེ་སྒྲིག་ཆས་ཀྱི་མིང་བཅོས་རོགས།</translation>
    </message>
    <message>
        <source>The length of the device name does not exceed %1 characters !</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་གི་རིང་ཚད་ནི་%1 ཡི་གེ་ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <source>Can now be found as &quot;%1&quot;</source>
        <translation>ད་ལྟ་&quot;%1&quot;ཞེས་པར་བརྩིས་ཆོག</translation>
    </message>
    <message>
        <source>Click to change the device name</source>
        <translation>བཟོ་བཅོས་སྒྲིག་ཆས་ཀྱི་མིང་མནན་ནས་བསྒྱུར་བ།།</translation>
    </message>
</context>
<context>
    <name>DevRemoveDialog</name>
    <message>
        <source>After it is removed, the PIN code must be matched for the next connection.</source>
        <translation>སྤོར་རྗེས།་ཐེངས་རྗེས་མའི་སྦྲེལ་མཐུད་ལ་ཨང་གྲངས་སྙོམས་སྒྲིག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Are you sure to remove %1 ?</source>
        <translation>%1མེད་པར་བཟོ་རྒྱུ་གཏན་འཁེལ་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <source>Connection failed! Please remove it before connecting.</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པ་རེད། འབྲེལ་མཐུད་མ་བྱས་གོང་ལ་དེ་མེད་པར་བཟོ་རོགས།</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>སོ་སྔོན་འབྲེལ་མཐུད།</translation>
    </message>
</context>
<context>
    <name>DevRenameDialog</name>
    <message>
        <source>Rename</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>The value contains 1 to 32 characters</source>
        <translation>ཡིག་རྟགས་དེའི་ནང་དུ་ཡི་གེ་1ནས་32བར་ཡོད།</translation>
    </message>
</context>
<context>
    <name>DeviceInfoItem</name>
    <message>
        <source>Connecting</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Disconnecting</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Connect fail</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <source>Disconnect fail</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
    <message>
        <source>send file</source>
        <translation>ཡིག་ཆ་བསྐུར་བ།</translation>
    </message>
    <message>
        <source>remove</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Not Connected</source>
        <translation>འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <source>disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source></source>
        <translation>མེད།</translation>
    </message>
</context>
</TS>
