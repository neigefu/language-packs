<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant_HK">
<context>
    <name>BlueToothMain</name>
    <message>
        <source>Show icon on taskbar</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/bluetooth/Show icon on taskbar</extra-contents_path>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/bluetooth/Discoverable</extra-contents_path>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/bluetooth/Other Devices</extra-contents_path>
    </message>
    <message>
        <source>Bluetooth adapter</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Bluetooth/Bluetooth adapter</extra-contents_path>
    </message>
    <message>
        <source>Turn on</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/bluetooth/Turn on Bluetooth</extra-contents_path>
    </message>
    <message>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Peripherals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bluetooth driver abnormal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auto discover Bluetooth audio devices</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/bluetooth/Automatically discover Bluetooth audio devices</extra-contents_path>
    </message>
</context>
<context>
    <name>BlueToothMainWindow</name>
    <message>
        <source>Bluetooth adapter is abnormal !</source>
        <translation type="obsolete">སོ་སྔོན་བཀོད་སྒྲིག་ཡོ་ཆས་རྒྱུན་ལྡན་མིན།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="obsolete">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Turn on :</source>
        <translation type="obsolete">སྒོ་འབྱེད།</translation>
    </message>
    <message>
        <source>Show icon on taskbar</source>
        <translation type="obsolete">འགན་བྱང་དུ་སོ་སྔོན་རིས་རྟགས་འཆར་བ།</translation>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation type="obsolete">ཉེ་འཁོར་གྱི་སོ་སྔོན་སྒྲིག་ཆས་ཀྱི་བཤེར་རུང་བ།</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="obsolete">སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="obsolete">སྒྲིག་ཆས་གཞན་དག</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">དྲ་བྱང་།</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="obsolete">གཞན་དག</translation>
    </message>
</context>
<context>
    <name>Bluetooth</name>
    <message>
        <source>Bluetooth</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BluetoothNameLabel</name>
    <message>
        <source>Tip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Double-click to change the device name</source>
        <translation type="vanished">ཟུང་རྡེབ་བྱས་ཏེ་སྒྲིག་ཆས་ཀྱི་མིང་བཅོས་རོགས།</translation>
    </message>
    <message>
        <source>The length of the device name does not exceed %1 characters !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can now be found as &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click to change the device name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DevRemoveDialog</name>
    <message>
        <source>After it is removed, the PIN code must be matched for the next connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure to remove %1 ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection failed! Please remove it before connecting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DevRenameDialog</name>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The value contains 1 to 32 characters</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceInfoItem</name>
    <message>
        <source>Connecting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disconnecting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connect fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disconnect fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>send file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>disconnect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
