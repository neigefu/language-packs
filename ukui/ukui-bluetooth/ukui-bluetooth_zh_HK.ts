<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ActiveConnectionWidget</name>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="36"/>
        <source>Bluetooth Connection</source>
        <translation>藍牙連接</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="44"/>
        <source>Bluetooth Connections</source>
        <translation>藍牙連接</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="50"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="59"/>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="221"/>
        <source>Found audio device &quot;</source>
        <translation>發現音訊設備“</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="59"/>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="221"/>
        <source>&quot;, connect it or not?</source>
        <translation>“，是否連接？</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="68"/>
        <source>No prompt</source>
        <translation>不再提示</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="84"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="87"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>BluetoothFileTransferWidget</name>
    <message>
        <source>Bluetooth file transfer</source>
        <translation type="vanished">藍牙檔案</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="393"/>
        <source>Bytes</source>
        <translation>位元組</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="78"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="603"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="680"/>
        <source> and </source>
        <translation> 等 </translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="34"/>
        <source>Bluetooth File</source>
        <translation>藍牙檔案</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="78"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="603"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="680"/>
        <source> files more</source>
        <translation> 個檔案</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="98"/>
        <source>Select Device</source>
        <translation>選擇設備</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="100"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="706"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="781"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="108"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="166"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="792"/>
        <source>File Transfer Fail!</source>
        <translation>檔案傳輸失敗！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="348"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="572"/>
        <source>The selected file is larger than 4GB, which is not supported transfer!</source>
        <translation>檔案超過4GB，不支持傳輸！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="709"/>
        <source>File Transmission Succeed!</source>
        <translation>檔案發送成功！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="728"/>
        <source>Transferred &apos;</source>
        <translation>檔案 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="730"/>
        <source>&apos; and </source>
        <translation>&quot; 等共 </translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="732"/>
        <source> files in all to &apos;</source>
        <translation> 個檔案已發送至 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="734"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="791"/>
        <source>&apos;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="735"/>
        <source>File Transfer Success!</source>
        <translation>檔案發送成功！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="788"/>
        <source>Transferring file &apos;</source>
        <translation>發送檔案 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="790"/>
        <source>&apos; to &apos;</source>
        <translation>&quot; 至 &quot;</translation>
    </message>
    <message>
        <source>File Transmission Failed !</source>
        <translation type="vanished">檔案發送失敗！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="320"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="348"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="557"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="572"/>
        <source>Warning</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="320"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="557"/>
        <source>The selected file is empty, please select the file again !</source>
        <translation>所選文件爲空，請重新選擇！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="457"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="849"/>
        <source>Transferring to &quot;</source>
        <translation>發送檔案至 &quot;</translation>
    </message>
    <message>
        <source>File Transmition Succeed!</source>
        <translation type="vanished">檔案發送成功！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="721"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>BluetoothSettingLabel</name>
    <message>
        <location filename="../component/bluetoothsettinglabel.cpp" line="105"/>
        <source>Bluetooth Settings</source>
        <translation>藍牙設定</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <location filename="../config/config.cpp" line="28"/>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <source>ukui-bluetooth</source>
        <translation type="vanished">藍牙</translation>
    </message>
    <message>
        <source>Bluetooth Message</source>
        <translation type="vanished">藍牙消息</translation>
    </message>
</context>
<context>
    <name>DeviceSeleterWidget</name>
    <message>
        <source>No device currently available 
 Please go to pair the device</source>
        <translation type="vanished">當前沒有可用的設備
請去配對設備</translation>
    </message>
</context>
<context>
    <name>ErrorMessageWidget</name>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="13"/>
        <location filename="../popwidget/errormessagewidget.cpp" line="79"/>
        <source>Bluetooth Message</source>
        <translation>藍牙消息</translation>
    </message>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="76"/>
        <source>Please confirm if the device &apos;</source>
        <translation>請確認設備 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="78"/>
        <source>&apos; is in the signal range or the Bluetooth is enabled.</source>
        <translation>&quot; 是否在訊號範圍內或藍牙功能是否已開啟。</translation>
    </message>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="79"/>
        <source>Connection Time Out!</source>
        <translation>連接超時！</translation>
    </message>
</context>
<context>
    <name>FileReceivingPopupWidget</name>
    <message>
        <source>Bluetooth file transfer</source>
        <translation type="vanished">藍牙檔案</translation>
    </message>
    <message>
        <source>Greater than 4GB</source>
        <translation type="vanished">該檔案大於4GB</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="81"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="89"/>
        <source>Accept</source>
        <translation>接收</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="101"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="167"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="183"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="203"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="337"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="385"/>
        <source>File from &quot;</source>
        <translation>檔案來自 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="183"/>
        <source>&quot;, waiting for receive...</source>
        <translation>&quot;，等待接收…</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="167"/>
        <source>&quot;, waiting for receive.</source>
        <translation>&quot;，等待接收。</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="32"/>
        <source>Bluetooth File</source>
        <translation>藍牙檔案</translation>
    </message>
    <message>
        <source>&quot;, is receiving... (has recieved </source>
        <translation type="vanished">&quot;，正在接收… (已接收 </translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="205"/>
        <source>&quot;, is receiving... (has received </source>
        <translation>&quot;，正在接收…（已接收 </translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="207"/>
        <source> files)</source>
        <translation> 個檔案)</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="337"/>
        <source>&quot;, is receiving...</source>
        <translation>&quot;，正在接收…</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="361"/>
        <source>File Receive Failed!</source>
        <translation>檔案接收失敗！</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="362"/>
        <source>Receive file &apos;</source>
        <translation>接收檔案 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="362"/>
        <source>&apos; from &apos;</source>
        <translation>&quot; 自 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="362"/>
        <source>&apos;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="370"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="385"/>
        <source>&quot;, received failed !</source>
        <translation>&quot;，接收失敗！</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="390"/>
        <source>File Transmission Failed !</source>
        <translation>檔案傳輸失敗！</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.h" line="81"/>
        <source>Bytes</source>
        <translation>位元組</translation>
    </message>
</context>
<context>
    <name>KyFileDialog</name>
    <message>
        <location filename="../component/kyfiledialog.cpp" line="13"/>
        <source>Bluetooth File</source>
        <translation>藍牙檔案</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation type="vanished">所選文件爲空，請重新選擇！</translation>
    </message>
</context>
<context>
    <name>PinCodeWidget</name>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="34"/>
        <source>Bluetooth Connection</source>
        <translation>藍牙連接</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="36"/>
        <source>Bluetooth pairing</source>
        <translation>藍牙設備配對</translation>
    </message>
    <message>
        <source>The pairing with the Bluetooth device “%1” is failed!</source>
        <translation type="vanished">與藍牙設備“%1”匹配失敗！</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="151"/>
        <source>If the PIN on &apos;</source>
        <translation>如</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="151"/>
        <source>&apos; is the same as this PIN. Please press &apos;Connect&apos;</source>
        <translation>&quot; 上的PIN碼與此PIN碼相同，請按 &quot;連接&quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="153"/>
        <source>If &apos;</source>
        <translation>如果</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="153"/>
        <source>&apos; the PIN on is the same as this PIN. Please press &apos;Connect&apos;.</source>
        <translation>&quot; 上的PIN碼與此PIN碼相同，請按 &quot;連接&quot;。</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="155"/>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair:</source>
        <translation>請在藍牙設備“%1”上輸入相同PIN，並按“Enter”確認：</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="59"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="62"/>
        <source>Refuse</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="70"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation type="vanished">藍牙連接</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="121"/>
        <source>Bluetooth Connect Failed</source>
        <translation>藍牙連接失敗</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="131"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Pair</source>
        <translation type="vanished">配對</translation>
    </message>
    <message>
        <source>Connect Failed!</source>
        <translation type="vanished">連接失敗！</translation>
    </message>
</context>
<context>
    <name>QDevItem</name>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation type="vanished">與藍牙設備“%1”連接成功！</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation type="vanished">藍牙設備“%1”失去連接！</translation>
    </message>
</context>
<context>
    <name>SessionDbusInterface</name>
    <message>
        <location filename="../dbus/sessiondbusinterface.cpp" line="358"/>
        <source>Bluetooth Message</source>
        <translation>藍牙消息</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../popwidget/trayicon.cpp" line="18"/>
        <source>Set Bluetooth Item</source>
        <translation>設定藍牙項</translation>
    </message>
    <message>
        <location filename="../popwidget/trayicon.cpp" line="38"/>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
</context>
<context>
    <name>TrayWidget</name>
    <message>
        <location filename="../popwidget/traywidget.cpp" line="83"/>
        <source>bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <source>Using Bluetooth mouse or keyboard, Do you want to turn off bluetooth?</source>
        <translation type="vanished">正在使用藍牙滑鼠或鍵盤，是否關閉藍牙？</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Close bluetooth</source>
        <translation type="vanished">關閉藍牙</translation>
    </message>
    <message>
        <location filename="../popwidget/traywidget.cpp" line="258"/>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <location filename="../popwidget/traywidget.cpp" line="300"/>
        <source>My Device</source>
        <translation>我的設備</translation>
    </message>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation type="vanished">與藍牙設備“%1”連接成功！</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation type="vanished">藍牙設備“%1”失去連接！</translation>
    </message>
</context>
<context>
    <name>beforeTurnOffHintWidget</name>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="26"/>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="45"/>
        <source>Turn Off</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="58"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="84"/>
        <source>Using Bluetooth mouse or keyboard, Do you want to turn off bluetooth?</source>
        <translation>正在使用藍牙滑鼠或鍵盤，是否關閉藍牙？</translation>
    </message>
</context>
</TS>
