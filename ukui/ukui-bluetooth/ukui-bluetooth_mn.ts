<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ActiveConnectionWidget</name>
    <message>
        <source>Bluetooth Connection</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢ ᠬᠤᠯᠪᠤᠪᠠ</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢ ᠬᠤᠯᠪᠤᠪᠠ</translation>
    </message>
    <message>
        <source>Found audio device &quot;</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠪᠡ&quot;</translation>
    </message>
    <message>
        <source>&quot;, connect it or not?</source>
        <translation>&quot;, ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>No prompt</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BluetoothFileTransferWidget</name>
    <message>
        <source>OK</source>
        <translation>ᠪᠠᠰᠠ ᠪᠣᠯᠣᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Transferring to &quot;</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ &quot; ᠳ᠋ᠤ᠌/ ᠲᠤ᠌ ᠬᠦᠷᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <source> and </source>
        <translation> ᠵᠡᠷᠭᠡ </translation>
    </message>
    <message>
        <source> files more</source>
        <translation> ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <source>Select Device</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation>ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠬᠤᠭᠤᠰᠤᠨ᠂ ᠳᠠᠬᠢᠵᠤ ᠰᠤᠨᠭᠭᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
    <message>
        <source>Bluetooth File</source>
        <translation>ᠯᠠᠨᠶᠠ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠠᠶᠠᠷ</translation>
    </message>
    <message>
        <source>File Transmission Failed!</source>
        <translation type="vanished">ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ !</translation>
    </message>
    <message>
        <source>File Transmission Succeed!</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠨᠢ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠵᠠᠢ !</translation>
    </message>
    <message>
        <source>Transferring file &apos;</source>
        <translation>ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ &quot;</translation>
    </message>
    <message>
        <source>&apos; to &apos;</source>
        <translation>&apos; ᠥᠪᠡᠷ ᠢ ᠪᠡᠨ &apos;</translation>
    </message>
    <message>
        <source>&apos;</source>
        <translation>&apos;</translation>
    </message>
    <message>
        <source>File Transfer Fail!</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ !</translation>
    </message>
    <message>
        <source>The selected file is larger than 4GB, which is not supported transfer!</source>
        <translation>ᠰᠣᠩᠭᠣᠭᠰᠠᠨ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠨᠢ 4GB ᠠᠴᠠ ᠶᠡᠬᠡ ᠂ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠶᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <source>&apos; and </source>
        <translation>&apos;ᠡᠶ᠎ᠡ ᠨᠠᠶᠢᠷᠠ </translation>
    </message>
    <message>
        <source> files in all to &apos;</source>
        <translation> ᠪᠦᠬᠦᠨ ᠦ ᠳᠣᠲᠣᠷᠠᠬᠢ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ&apos;</translation>
    </message>
    <message>
        <source>File Transfer Success!</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠳᠠᠮᠵᠢᠭᠤᠯᠬᠤ ᠨᠢ ᠠᠮᠵᠢᠯᠲᠠ ᠣᠯᠵᠠᠢ !</translation>
    </message>
    <message>
        <source>Transferred &apos;</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦ&apos;</translation>
    </message>
</context>
<context>
    <name>BluetoothSettingLabel</name>
    <message>
        <source>Bluetooth Settings</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
</context>
<context>
    <name>ErrorMessageWidget</name>
    <message>
        <source>Bluetooth Message</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Please confirm if the device &apos;</source>
        <translation>ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠨᠢ ᠡᠰᠡᠬᠦ ᠶᠢ ᠨᠤᠲᠠᠯᠠᠭᠠᠷᠠᠢ &apos;</translation>
    </message>
    <message>
        <source>&apos; is in the signal range or the Bluetooth is enabled.</source>
        <translation>&apos;ᠳᠣᠬᠢᠶᠠᠨ ᠤ ᠬᠡᠪᠴᠢᠶᠡᠨ ᠳᠣᠲᠣᠷ᠎ᠠ ᠪᠤᠶᠤ ᠬᠥᠬᠡ ᠰᠢᠳᠦ ᠶᠢ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠰᠢᠭᠯᠠᠵᠠᠢ.</translation>
    </message>
    <message>
        <source>Connection Time Out!</source>
        <translation>ᠵᠠᠯᠭᠠᠭᠠᠳ ᠴᠠᠭ ᠠᠴᠠ ᠬᠡᠲᠦᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ !</translation>
    </message>
</context>
<context>
    <name>FileReceivingPopupWidget</name>
    <message>
        <source>View</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Bluetooth File</source>
        <translation>ᠯᠠᠨᠶᠠ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <source>File from &quot;</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠢᠷᠡᠯᠳᠡ &quot;</translation>
    </message>
    <message>
        <source>&quot;, waiting for receive.</source>
        <translation>&quot;, ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠬᠤ ᠵᠢ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>&quot;, is receiving... (has recieved </source>
        <translation type="vanished">&quot;, ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠤ ᠪᠠᠢᠨ᠎ᠠ... ( ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠤᠪᠠ) </translation>
    </message>
    <message>
        <source> files)</source>
        <translation> ᠹᠠᠢᠯ)</translation>
    </message>
    <message>
        <source>&quot;, is receiving...</source>
        <translation>&quot;, ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>&quot;, received failed !</source>
        <translation>&quot;, ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>File Transmission Failed !</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠮᠵᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>&quot;, waiting for receive...</source>
        <translation>&quot;, ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠬᠤ ᠵᠢ ᠬᠦᠯᠢᠶᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>Greater than 4GB</source>
        <translation type="vanished">4GB ᠡᠴᠡ ᠶᠡᠬᠡ</translation>
    </message>
    <message>
        <source>File Receive Failed!</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠬᠤ ᠳᠤ ᠢᠯᠠᠭᠳᠠᠵᠠᠢ !</translation>
    </message>
    <message>
        <source>Receiving file &apos;</source>
        <translation type="vanished">ᠶᠠᠭ ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠤ ᠪᠠᠢ᠌ᠨ᠎ᠠ &apos;</translation>
    </message>
    <message>
        <source>&apos; from &apos;</source>
        <translation>&apos;ᠡᠭᠦᠨ ᠡᠴᠡ ᠠᠪᠤᠭᠠᠳ &apos;</translation>
    </message>
    <message>
        <source>&apos;</source>
        <translation>&apos;</translation>
    </message>
    <message>
        <source>Bytes</source>
        <translation>ᠦᠰᠦᠭ ᠦᠨ ᠪᠠᠶᠠᠷ</translation>
    </message>
    <message>
        <source>Receive file &apos;</source>
        <translation>ᠪᠢᠴᠢᠭ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠢ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠬᠤ&apos;</translation>
    </message>
    <message>
        <source>&quot;, is receiving... (has received </source>
        <translation>&quot;, ᠶᠠᠭ ᠬᠦᠯᠢᠶᠡᠨ ᠠᠪᠴᠤ ᠪᠠᠶᠢᠭ᠎ᠠ ᠁ ( ᠨᠢᠭᠡᠨᠲᠡ ᠬᠤᠷᠢᠶᠠᠨ ᠠᠪᠤᠭᠰᠠᠨ ᠃ </translation>
    </message>
</context>
<context>
    <name>KyFileDialog</name>
    <message>
        <source>Bluetooth File</source>
        <translation>ᠯᠠᠨᠶᠠ ᠹᠠᠢᠯ</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation type="vanished">ᠰᠤᠨᠭᠭᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠬᠤᠭᠤᠰᠤᠨ᠂ ᠳᠠᠬᠢᠵᠤ ᠰᠤᠨᠭᠭᠤᠭᠠᠷᠠᠢ !</translation>
    </message>
</context>
<context>
    <name>PinCodeWidget</name>
    <message>
        <source>Bluetooth pairing</source>
        <translation>ᠯᠠᠨᠶᠠ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠠᠪᠤᠴᠠᠯᠳᠤᠪᠠ</translation>
    </message>
    <message>
        <source>If &apos;</source>
        <translation>ᠬᠡᠷᠪᠡ&apos;</translation>
    </message>
    <message>
        <source>&apos; the PIN on is the same as this PIN. Please press &apos;Connect&apos;.</source>
        <translation>&apos; ᠳᠡᠭᠡᠷᠡᠬᠢ PIN ᠺᠤᠳ᠋ ᠡᠨᠡᠬᠦ PIN ᠺᠤᠳ᠋ ᠲᠠᠢ ᠢᠵᠢᠯ᠂&apos; ᠴᠦᠷᠬᠡᠯᠡᠬᠦ&apos; ᠬᠡᠰᠡᠨ ᠢ᠋ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <source>Bluetooth Connection</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢ ᠬᠤᠯᠪᠤᠪᠠ</translation>
    </message>
    <message>
        <source>If the PIN on &apos;</source>
        <translation>ᠬᠡᠷᠪᠡ &apos;</translation>
    </message>
    <message>
        <source>&apos; is the same as this PIN. Please press &apos;Connect&apos;</source>
        <translation>&apos; ᠳᠡᠭᠡᠷᠡᠬᠢ ᠳᠤᠭ᠎ᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠲᠠᠢ ᠢᠵᠢᠯ᠂&apos; ᠴᠦᠷᠬᠡᠯᠡᠬᠦ&apos; ᠭᠡᠰᠡᠨ ᠢ᠋ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair:</source>
        <translation>ᠯᠠᠨᠶᠠ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ &apos;%1&apos; ᠳ᠋ᠤ᠌/ ᠲᠤ᠌ ᠢᠵᠢᠯ PIN ᠤᠷᠤᠭᠤᠯᠵᠤ᠂enter ᠢ᠋/ ᠵᠢ ᠳᠠᠷᠤᠵᠤ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ:</translation>
    </message>
    <message>
        <source>Refuse</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Bluetooth Connect Failed</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢ ᠬᠤᠯᠪᠤᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>SessionDbusInterface</name>
    <message>
        <source>Bluetooth Message</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <source>Set Bluetooth Item</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢᠨ ᠳᠦᠷᠦᠯ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>TrayWidget</name>
    <message>
        <source>bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <source>My Device</source>
        <translation>ᠮᠢᠨᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Using Bluetooth mouse or keyboard, Do you want to turn off bluetooth?</source>
        <translation type="vanished">ᠶᠠᠭ ᠬᠦᠬᠡ ᠰᠢᠳᠦᠨ ᠬᠤᠯᠤᠭᠠᠨ᠎ᠠ ᠪᠤᠶᠤ ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠢᠢᠨ᠎ᠠ᠂ ᠬᠦᠬᠡ ᠰᠢᠳᠦ ᠪᠡᠨ ᠬᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Close bluetooth</source>
        <translation type="vanished">ᠬᠥᠬᠡ ᠰᠢᠳᠦ ᠶᠢ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>beforeTurnOffHintWidget</name>
    <message>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
    <message>
        <source>Turn Off</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Using Bluetooth mouse or keyboard, Do you want to turn off bluetooth?</source>
        <translation>ᠶᠠᠭ ᠬᠦᠬᠡ ᠰᠢᠳᠦᠨ ᠬᠤᠯᠤᠭᠠᠨ᠎ᠠ ᠪᠤᠶᠤ ᠳᠠᠷᠤᠭᠤᠯ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠠᠢᠢᠨ᠎ᠠ᠂ ᠬᠦᠬᠡ ᠰᠢᠳᠦ ᠪᠡᠨ ᠬᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
</context>
</TS>
