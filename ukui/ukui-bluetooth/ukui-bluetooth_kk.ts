<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>ActiveConnectionWidget</name>
    <message>
        <source>Bluetooth Connection</source>
        <translation>Bluetooth байланысы</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>Bluetooth байланыстары</translation>
    </message>
    <message>
        <source>Found audio device &quot;</source>
        <translation>Табылған дыбыстық құрылғы »</translation>
    </message>
    <message>
        <source>&quot;, connect it or not?</source>
        <translation>» деп, оны қосыңыз ба, жоқ па?</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Қосылу</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
</context>
<context>
    <name>BluetoothFileTransferWidget</name>
    <message>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <source>Successfully transmitted!</source>
        <translation type="vanished">ཡིག་ཆ་བསྐུར་བ་ལེགས་གྲུབ།</translation>
    </message>
    <message>
        <source>Transferring to &quot;</source>
        <translation>20__ жылғы «___» __________</translation>
    </message>
    <message>
        <source>Transmission failed!</source>
        <translation type="vanished">ཡིག་ཆ་བསྐུར་བ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Bluetooth file transfer</source>
        <translation>Bluetooth файлын тасымалдау</translation>
    </message>
    <message>
        <source> and </source>
        <translation> және </translation>
    </message>
    <message>
        <source> files more</source>
        <translation> Қосымша файлдар</translation>
    </message>
    <message>
        <source>Select Device</source>
        <translation>Құрылғыны таңдау</translation>
    </message>
    <message>
        <source>File Transmission Failed !</source>
        <translation>Файлды беру жаңылысы!</translation>
    </message>
    <message>
        <source>File Transmition Succeed!</source>
        <translation>Файлды тапсыру табысқа жетті!</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Ескерту</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation>Таңдалған файл бос, файлды қайтадан таңдаңыз!</translation>
    </message>
</context>
<context>
    <name>BluetoothSettingLabel</name>
    <message>
        <source>Bluetooth Settings</source>
        <translation>Bluetooth параметрлері</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <source>ukui-bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Bluetooth message</source>
        <translation type="vanished">སོ་སྔོན་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>Блютоот</translation>
    </message>
    <message>
        <source>Bluetooth Message</source>
        <translation>Bluetooth хабары</translation>
    </message>
</context>
<context>
    <name>DeviceSeleterWidget</name>
    <message>
        <source>Select equipment</source>
        <translation type="vanished">སྒྲིག་ཆས་འདེམ་པ།</translation>
    </message>
    <message>
        <source>No device currently available 
 Please go to pair the device</source>
        <translation>Қазіргі уақытта қол жетімді құрылғы жоқ 
 Құрылғыны жұптастыру үшін өтуіңізді сұраймын</translation>
    </message>
</context>
<context>
    <name>FeaturesWidget</name>
    <message>
        <source>Bluetooth Adapter Abnormal!!!</source>
        <translation type="vanished">སོ་སྔོན་འོས་སྒྲིག་ཆས་རྒྱུན་ལྡན་མིན།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <source>Power </source>
        <translation type="vanished">ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">བསུབ་པ།</translation>
    </message>
    <message>
        <source>Send files</source>
        <translation type="vanished">ཡིག་ཆ་སྐུར་བ།</translation>
    </message>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་ཆས“%1”དང་སྦྲེལ་མཐུད་ལེགས་གྲུབ།</translation>
    </message>
    <message>
        <source>Bluetooth message</source>
        <translation type="vanished">སོ་སྔོན་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <source>ukui-bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="vanished">སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Connect audio</source>
        <translation type="vanished">སྒྲ་ཟློས་འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <source>Bluetooth settings</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <source>no bluetooth adapter!</source>
        <translation type="vanished">སོ་སྔོན་འོས་སྒྲིག་ཆས་མི་འདུག</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་ཆས“%1”སྦྲེལ་མཐུད་བོར་བརླག་ཐེབས།</translation>
    </message>
    <message>
        <source>Disconnection</source>
        <translation type="vanished">འབྲེལ་མཐུད།</translation>
    </message>
</context>
<context>
    <name>FileReceivingPopupWidget</name>
    <message>
        <source>View</source>
        <translation>Көрініс</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Қабылдау</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <source>Sender canceled or transmission error</source>
        <translation type="vanished">སྐུར་ཡུལ་ནས་རྩིས་མེད་བཏང་བའམ་བསྐུར་ནོར་ཐེབས་པ།</translation>
    </message>
    <message>
        <source>Bluetooth file transfer from &quot;</source>
        <translation type="vanished">%s ནས་སོ་སྔོན་གྱིས་ཡིག་ཆ་བརྒྱུད་གཏོང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Bluetooth file transfer</source>
        <translation>Bluetooth файлын тасымалдау</translation>
    </message>
    <message>
        <source>Bluetooth File</source>
        <translation>Bluetooth файлы</translation>
    </message>
    <message>
        <source>File from &quot;</source>
        <translation>Файлдан&quot;</translation>
    </message>
    <message>
        <source>&quot;, waiting for receive.</source>
        <translation>» - деп, қабылдауды күтеді.</translation>
    </message>
    <message>
        <source>&quot;, is receiving... (has recieved </source>
        <translation>20__ ж. «___» __________ ___________________________ (тегі, аты, бар </translation>
    </message>
    <message>
        <source> files)</source>
        <translation> файлдар)</translation>
    </message>
    <message>
        <source>&quot;, is receiving...</source>
        <translation>20__ ж. «___» __________</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <source>&quot;, received failed !</source>
        <translation>» деген сөзден кейін &quot;, алынбады!</translation>
    </message>
    <message>
        <source>File Transmission Failed !</source>
        <translation>Файлды беру жаңылысы!</translation>
    </message>
    <message>
        <source>&quot;, waiting for receive...</source>
        <translation>» деп, қабылдауды күту...</translation>
    </message>
</context>
<context>
    <name>KyFileDialog</name>
    <message>
        <source>Bluetooth File</source>
        <translation>Bluetooth файлы</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <source>Warning</source>
        <translation>Ескерту</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation>Таңдалған файл бос, файлды қайтадан таңдаңыз!</translation>
    </message>
</context>
<context>
    <name>PinCodeWidget</name>
    <message>
        <source>Pair</source>
        <translation>Жұп</translation>
    </message>
    <message>
        <source>&quot; matches the number below. Please do not enter this code on any other accessories.</source>
        <translation type="vanished">སྟེང་དུ་མངོན་པའི་གྲངས་ཀ་དང་འོག་ཏུ་མངོན་པའི་གྲངས་ངོ་འཕྲོད་མི་ཐུབ། ཁྱོས་གཞན་གྱི་སྡེབ་སྒྲིག་སྟེང་ཨང་ཚབ་ནང་འཇུག་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <source>Connection error !!!</source>
        <translation type="vanished">སྦྲེལ་མཐུད་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Is it paired with:</source>
        <translation type="vanished">ཆ་སྒྲིག་བྱེད་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation type="vanished">སྡུད་ལེན།</translation>
    </message>
    <message>
        <source>Refush</source>
        <translation type="vanished">རྩིས་མེད།</translation>
    </message>
    <message>
        <source>Failed to pair with %1 !!!</source>
        <translation type="vanished"> %1དང་ཆ་སྒྲིག་བྱེད་པ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Please make sure the number displayed on &quot;</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་རོགས།</translation>
    </message>
    <message>
        <source>Please enter the following PIN code on the bluetooth device %1 and press enter to pair !</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་ཆས་%1སྟེང་གཤམ་གྱི་ཨང་PINནང་འཇུག་བྱས་རྗེས་ཕྱིར་ལོག་མཐེབ་མནོན་མནན་ནས་ཆ་སྒྲིག་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Bluetooth pairing</source>
        <translation>Bluetooth түйісуі</translation>
    </message>
    <message>
        <source>If you want to pair with this device, please make sure the numbers below are the same with the devices.</source>
        <translation type="vanished">གལ་ཏེ་ཁྱོད་ཀྱིས་སྒྲིག་ཆས་འདི་དང་ཆ་སྒྲིག་བྱེད་འདོད་ན།གཤམ་གྱི་གྲངས་ཀ་དང་སྒྲིག་ཆས་གཅིག་མཚུངས་ཡིན་པར་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>If &apos;</source>
        <translation>Егер &apos; болса</translation>
    </message>
    <message>
        <source>&apos; the PIN on is the same as this PIN. Please press &apos;Connect&apos;.</source>
        <translation>&apos; PIN коды осы PIN кодымен бірдей. &apos;Connect&apos; дегенді басуыңызды сұраймыз.</translation>
    </message>
    <message>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair !</source>
        <translation type="vanished">ཁྱོད་ཀྱིས་ཁ་དོག་སྔོན་པོའི་སྒྲིག་ཆས་སྟེང་གི་གཤམ་གྱི་PINཡི་ཚབ་རྟགས་&apos;%1&apos;ནང་འཇུག་བྱས་ནས་ཆ་སྒྲིག་གཉེན་འཚོལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Қосылу</translation>
    </message>
    <message>
        <source>Please make sure the PIN code displayed on &apos;</source>
        <translation type="vanished">ཁྱོད་ཀྱིས་PINཡི་ཚབ་རྟགས་དེ་མངོན་པར་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>&apos; matches the number below. Please press &apos;Connect&apos;.</source>
        <translation type="vanished">&apos;གཤམ་གྱི་ཨང་གྲངས་དང་མཐུན་པ་རེད། ཁྱེད་ཀྱིས་&quot;སྦྲེལ་མཐུད་&quot;བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Bluetooth Connection</source>
        <translation>Bluetooth байланысы</translation>
    </message>
    <message>
        <source>If the PIN on &apos;</source>
        <translation>Егер PIN коды &apos;&apos; болса</translation>
    </message>
    <message>
        <source>&apos; is the same as this PIN. Please press &apos;Connect&apos;</source>
        <translation>&apos; осы PIN кодымен бірдей. &apos;Connect&apos; пернелерыңызды басуыңызды сұраймын</translation>
    </message>
    <message>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair:</source>
        <translation>&apos;% 1&apos; bluetooth құрылғысына келесі PIN кодын енгізіп, жұптастыру үшін enter пернесін басыңыз:</translation>
    </message>
    <message>
        <source>Refuse</source>
        <translation>Бас тарту</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>Растау</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>Bluetooth байланыстары</translation>
    </message>
    <message>
        <source>Bluetooth Connect Failed</source>
        <translation>Bluetooth қосылымы жаңылысы</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <source>Connect Failed!</source>
        <translation>Қосылу жаңылысы!</translation>
    </message>
</context>
<context>
    <name>QDevItem</name>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation>&quot;% 1&quot; Bluetooth құрылғысымен байланыс сәтті!</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation>&quot;% 1&quot; Bluetooth құрылғысы ажыратылды!</translation>
    </message>
</context>
<context>
    <name>SwitchAction</name>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">ཁ་དོག་སྔོན་པོ།</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>Bluetooth</source>
        <translation>Блютоот</translation>
    </message>
    <message>
        <source>Set Bluetooth Item</source>
        <translation>Bluetooth элементін орнату</translation>
    </message>
</context>
<context>
    <name>TrayWidget</name>
    <message>
        <source>bluetooth</source>
        <translation>bluetooth</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>Блютоот</translation>
    </message>
    <message>
        <source>My Device</source>
        <translation>Менің құрылғым</translation>
    </message>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation>&quot;% 1&quot; Bluetooth құрылғысымен байланыс сәтті!</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation>&quot;% 1&quot; Bluetooth құрылғысы ажыратылды!</translation>
    </message>
</context>
</TS>
