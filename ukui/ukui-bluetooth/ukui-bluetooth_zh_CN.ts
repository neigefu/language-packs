<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ActiveConnectionWidget</name>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="36"/>
        <source>Bluetooth Connection</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="44"/>
        <source>Bluetooth Connections</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="50"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="59"/>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="221"/>
        <source>Found audio device &quot;</source>
        <translation>发现音频设备 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="59"/>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="221"/>
        <source>&quot;, connect it or not?</source>
        <translation>&quot; ，是否连接？</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="68"/>
        <source>No prompt</source>
        <translation>不再提示</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="84"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../popwidget/activeconnectionwidget.cpp" line="87"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>BluetoothFileTransferWidget</name>
    <message>
        <source>Bluetooth file transfer</source>
        <translation type="vanished">蓝牙文件</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="457"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="849"/>
        <source>Transferring to &quot;</source>
        <translation>发送文件至 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="78"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="603"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="680"/>
        <source> and </source>
        <translation> 等 </translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="34"/>
        <source>Bluetooth File</source>
        <translation>蓝牙文件</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="393"/>
        <source>Bytes</source>
        <translation>字节</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="78"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="603"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="680"/>
        <source> files more</source>
        <translation> 个文件</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="98"/>
        <source>Select Device</source>
        <translation>选择设备</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="100"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="706"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="781"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="108"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="348"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="572"/>
        <source>The selected file is larger than 4GB, which is not supported transfer!</source>
        <translation>文件超过 4GB，不支持传输！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="730"/>
        <source>&apos; and </source>
        <translation>&quot; 等共 </translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="732"/>
        <source> files in all to &apos;</source>
        <translation> 个文件已发送至 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="735"/>
        <source>File Transfer Success!</source>
        <translation>文件发送成功！</translation>
    </message>
    <message>
        <source>File Transmission Failed !</source>
        <translation type="vanished">文件发送失败！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="320"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="348"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="557"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="572"/>
        <source>Warning</source>
        <translation>提示</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="320"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="557"/>
        <source>The selected file is empty, please select the file again !</source>
        <translation>所选文件为空，请重新选择！</translation>
    </message>
    <message>
        <source>File Transmition Succeed!</source>
        <translation type="vanished">文件发送成功！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="721"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="709"/>
        <source>File Transmission Succeed!</source>
        <translation>文件发送成功！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="166"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="792"/>
        <source>File Transfer Fail!</source>
        <translation>文件发送失败！</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="728"/>
        <source>Transferred &apos;</source>
        <translation>文件 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="790"/>
        <source>&apos; to &apos;</source>
        <translation>&quot; 至 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="788"/>
        <source>Transferring file &apos;</source>
        <translation>发送文件 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="734"/>
        <location filename="../popwidget/bluetoothfiletransferwidget.cpp" line="791"/>
        <source>&apos;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <source>File Transmission Failed!</source>
        <translation type="vanished">文件发送失败！</translation>
    </message>
</context>
<context>
    <name>BluetoothSettingLabel</name>
    <message>
        <location filename="../component/bluetoothsettinglabel.cpp" line="105"/>
        <source>Bluetooth Settings</source>
        <translation>蓝牙设置</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <source>ukui-bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <location filename="../config/config.cpp" line="28"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <source>Bluetooth Message</source>
        <translation type="vanished">蓝牙消息</translation>
    </message>
    <message>
        <source>Bluetooth message</source>
        <translation type="vanished">蓝牙消息</translation>
    </message>
</context>
<context>
    <name>DeviceSeleterWidget</name>
    <message>
        <source>No device currently available 
 Please go to pair the device</source>
        <translation type="vanished">当前没有可用的设备
请去配对设备</translation>
    </message>
</context>
<context>
    <name>ErrorMessageWidget</name>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="13"/>
        <location filename="../popwidget/errormessagewidget.cpp" line="79"/>
        <source>Bluetooth Message</source>
        <translation>蓝牙消息</translation>
    </message>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="76"/>
        <source>Please confirm if the device &apos;</source>
        <translation>请确认设备 &quot;</translation>
    </message>
    <message>
        <source>Bluetooth File</source>
        <translation type="obsolete">蓝牙文件</translation>
    </message>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="78"/>
        <source>&apos; is in the signal range or the Bluetooth is enabled.</source>
        <translation>&quot; 是否在信号范围内或蓝牙功能是否已开启。</translation>
    </message>
    <message>
        <location filename="../popwidget/errormessagewidget.cpp" line="79"/>
        <source>Connection Time Out!</source>
        <translation>连接超时！</translation>
    </message>
</context>
<context>
    <name>FileReceivingPopupWidget</name>
    <message>
        <source>Bluetooth file transfer</source>
        <translation type="vanished">蓝牙文件传输</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="32"/>
        <source>Bluetooth File</source>
        <translation>蓝牙文件</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="167"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="183"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="203"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="337"/>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="385"/>
        <source>File from &quot;</source>
        <translation>文件来自 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="183"/>
        <source>&quot;, waiting for receive...</source>
        <translation>&quot;，等待接收…</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="167"/>
        <source>&quot;, waiting for receive.</source>
        <translation>&quot;，等待接收。</translation>
    </message>
    <message>
        <source>Greater than 4GB</source>
        <translation type="vanished">该文件大于4GB</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="81"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="89"/>
        <source>Accept</source>
        <translation>接收</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="101"/>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <source>&quot;, is receiving... (has recieved </source>
        <translation type="vanished">&quot;，正在接收… (已接收 </translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="207"/>
        <source> files)</source>
        <translation> 个文件)</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="337"/>
        <source>&quot;, is receiving...</source>
        <translation>&quot;，正在接收…</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="361"/>
        <source>File Receive Failed!</source>
        <translation>文件接收失败！</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="362"/>
        <source>&apos; from &apos;</source>
        <translation>&quot; 自 &quot;</translation>
    </message>
    <message>
        <source>Receiving file &apos;</source>
        <translation type="vanished">接收文件 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="205"/>
        <source>&quot;, is receiving... (has received </source>
        <translation>&quot;，正在接收… (已接收 </translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="362"/>
        <source>&apos;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="362"/>
        <source>Receive file &apos;</source>
        <translation>接收文件 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="370"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="385"/>
        <source>&quot;, received failed !</source>
        <translation>&quot;，接收失败！</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.cpp" line="390"/>
        <source>File Transmission Failed !</source>
        <translation>文件传输失败！</translation>
    </message>
    <message>
        <location filename="../popwidget/filereceivingpopupwidget.h" line="81"/>
        <source>Bytes</source>
        <translation>字节</translation>
    </message>
</context>
<context>
    <name>KyFileDialog</name>
    <message>
        <location filename="../component/kyfiledialog.cpp" line="13"/>
        <source>Bluetooth File</source>
        <translation>蓝牙文件</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <source>Warning</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation type="vanished">所选文件为空，请重新选择！</translation>
    </message>
</context>
<context>
    <name>PinCodeWidget</name>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="36"/>
        <source>Bluetooth pairing</source>
        <translation>蓝牙设备配对</translation>
    </message>
    <message>
        <source>The pairing with the Bluetooth device “%1” is failed!</source>
        <translation type="vanished">与蓝牙设备“%1”配对失败！</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="151"/>
        <source>&apos; is the same as this PIN. Please press &apos;Connect&apos;</source>
        <translation>”上的数字与下面相同，请点击“连接”</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="153"/>
        <source>If &apos;</source>
        <translation>如果 &quot;</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="62"/>
        <source>Refuse</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation type="vanished">蓝牙连接</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="121"/>
        <source>Bluetooth Connect Failed</source>
        <translation>蓝牙连接失败</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="131"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Connect Failed!</source>
        <translation type="vanished">连接失败！</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="153"/>
        <source>&apos; the PIN on is the same as this PIN. Please press &apos;Connect&apos;.</source>
        <translation>&quot; 上的PIN码与此PIN码相同，请按 &quot;连接&quot;。</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="34"/>
        <source>Bluetooth Connection</source>
        <translation>蓝牙连接</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="151"/>
        <source>If the PIN on &apos;</source>
        <translation>如“</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="155"/>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair:</source>
        <translation>请在蓝牙设备 &quot;%1&quot; 上输入相同PIN，并按&quot;Enter&quot;确认:</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="70"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../popwidget/pincodewidget.cpp" line="59"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <source>Pair</source>
        <translation type="vanished">配对</translation>
    </message>
</context>
<context>
    <name>QDevItem</name>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation type="vanished">与蓝牙设备“%1”连接成功！</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation type="vanished">蓝牙设备“%1”失去连接！</translation>
    </message>
</context>
<context>
    <name>SessionDbusInterface</name>
    <message>
        <location filename="../dbus/sessiondbusinterface.cpp" line="358"/>
        <source>Bluetooth Message</source>
        <translation>蓝牙消息</translation>
    </message>
</context>
<context>
    <name>SwitchAction</name>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../popwidget/trayicon.cpp" line="18"/>
        <source>Set Bluetooth Item</source>
        <translation>设置蓝牙项</translation>
    </message>
    <message>
        <location filename="../popwidget/trayicon.cpp" line="38"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
</context>
<context>
    <name>TrayWidget</name>
    <message>
        <location filename="../popwidget/traywidget.cpp" line="83"/>
        <source>bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <source>Using Bluetooth mouse or keyboard, Do you want to turn off bluetooth?</source>
        <translation type="vanished">正在使用蓝牙鼠标或键盘,是否关闭蓝牙?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Close bluetooth</source>
        <translation type="vanished">关闭蓝牙</translation>
    </message>
    <message>
        <location filename="../popwidget/traywidget.cpp" line="258"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../popwidget/traywidget.cpp" line="300"/>
        <source>My Device</source>
        <translation>我的设备</translation>
    </message>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation type="vanished">与蓝牙设备“%1”连接成功！</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation type="vanished">蓝牙设备“%1”失去连接！</translation>
    </message>
</context>
<context>
    <name>beforeTurnOffHintWidget</name>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="26"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="45"/>
        <source>Turn Off</source>
        <translation>关闭蓝牙</translation>
    </message>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="58"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../popwidget/beforeturnoffhintwidget.ui" line="84"/>
        <source>Using Bluetooth mouse or keyboard, Do you want to turn off bluetooth?</source>
        <translation>正在使用蓝牙鼠标或键盘,是否关闭蓝牙?</translation>
    </message>
</context>
</TS>
