<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>ActiveConnectionWidget</name>
    <message>
        <source>Bluetooth Connection</source>
        <translation>藍牙連接</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>藍牙連接</translation>
    </message>
    <message>
        <source>Found audio device &quot;</source>
        <translation>發現音訊設備 ”</translation>
    </message>
    <message>
        <source>&quot;, connect it or not?</source>
        <translation>“ ，是否連接？</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>BluetoothFileTransferWidget</name>
    <message>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Successfully transmitted!</source>
        <translation type="vanished">ཡིག་ཆ་བསྐུར་བ་ལེགས་གྲུབ།</translation>
    </message>
    <message>
        <source>Transferring to &quot;</source>
        <translation>發送檔案至 ”</translation>
    </message>
    <message>
        <source>Transmission failed!</source>
        <translation type="vanished">ཡིག་ཆ་བསྐུར་བ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Bluetooth file transfer</source>
        <translation>藍牙檔</translation>
    </message>
    <message>
        <source> and </source>
        <translation> 等 </translation>
    </message>
    <message>
        <source> files more</source>
        <translation> 個檔</translation>
    </message>
    <message>
        <source>Select Device</source>
        <translation>選擇設備</translation>
    </message>
    <message>
        <source>File Transmission Failed !</source>
        <translation>檔案發送失敗！</translation>
    </message>
    <message>
        <source>File Transmition Succeed!</source>
        <translation>檔案發送成功！</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation>選取檔案為空，請重新選擇！</translation>
    </message>
</context>
<context>
    <name>BluetoothSettingLabel</name>
    <message>
        <source>Bluetooth Settings</source>
        <translation>藍牙設置</translation>
    </message>
</context>
<context>
    <name>Config</name>
    <message>
        <source>ukui-bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Bluetooth message</source>
        <translation type="vanished">སོ་སྔོན་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <source>Bluetooth Message</source>
        <translation>藍牙消息</translation>
    </message>
</context>
<context>
    <name>DeviceSeleterWidget</name>
    <message>
        <source>Select equipment</source>
        <translation type="vanished">སྒྲིག་ཆས་འདེམ་པ།</translation>
    </message>
    <message>
        <source>No device currently available 
 Please go to pair the device</source>
        <translation>當前沒有可用的設備
請去配對設備</translation>
    </message>
</context>
<context>
    <name>FeaturesWidget</name>
    <message>
        <source>Bluetooth Adapter Abnormal!!!</source>
        <translation type="vanished">སོ་སྔོན་འོས་སྒྲིག་ཆས་རྒྱུན་ལྡན་མིན།</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <source>Power </source>
        <translation type="vanished">ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">བསུབ་པ།</translation>
    </message>
    <message>
        <source>Send files</source>
        <translation type="vanished">ཡིག་ཆ་སྐུར་བ།</translation>
    </message>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་ཆས“%1”དང་སྦྲེལ་མཐུད་ལེགས་གྲུབ།</translation>
    </message>
    <message>
        <source>Bluetooth message</source>
        <translation type="vanished">སོ་སྔོན་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <source>ukui-bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="vanished">སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>bluetooth</source>
        <translation type="vanished">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Connect audio</source>
        <translation type="vanished">སྒྲ་ཟློས་འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <source>Bluetooth settings</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <source>no bluetooth adapter!</source>
        <translation type="vanished">སོ་སྔོན་འོས་སྒྲིག་ཆས་མི་འདུག</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་ཆས“%1”སྦྲེལ་མཐུད་བོར་བརླག་ཐེབས།</translation>
    </message>
    <message>
        <source>Disconnection</source>
        <translation type="vanished">འབྲེལ་མཐུད།</translation>
    </message>
</context>
<context>
    <name>FileReceivingPopupWidget</name>
    <message>
        <source>View</source>
        <translation>查看</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>接收</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Sender canceled or transmission error</source>
        <translation type="vanished">སྐུར་ཡུལ་ནས་རྩིས་མེད་བཏང་བའམ་བསྐུར་ནོར་ཐེབས་པ།</translation>
    </message>
    <message>
        <source>Bluetooth file transfer from &quot;</source>
        <translation type="vanished">%s ནས་སོ་སྔོན་གྱིས་ཡིག་ཆ་བརྒྱུད་གཏོང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Bluetooth file transfer</source>
        <translation>藍牙文件傳輸</translation>
    </message>
    <message>
        <source>Bluetooth File</source>
        <translation>藍牙檔</translation>
    </message>
    <message>
        <source>File from &quot;</source>
        <translation>檔案來自 ”</translation>
    </message>
    <message>
        <source>&quot;, waiting for receive.</source>
        <translation>“，等待接收。</translation>
    </message>
    <message>
        <source>&quot;, is receiving... (has recieved </source>
        <translation>“，正在接收... （已接收 </translation>
    </message>
    <message>
        <source> files)</source>
        <translation> 個檔案 ）</translation>
    </message>
    <message>
        <source>&quot;, is receiving...</source>
        <translation>“，正在接收...</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <source>&quot;, received failed !</source>
        <translation>“，接收失敗！</translation>
    </message>
    <message>
        <source>File Transmission Failed !</source>
        <translation>檔案傳輸失敗！</translation>
    </message>
    <message>
        <source>&quot;, waiting for receive...</source>
        <translation>“，等待接收...</translation>
    </message>
</context>
<context>
    <name>KyFileDialog</name>
    <message>
        <source>Bluetooth File</source>
        <translation>藍牙檔</translation>
    </message>
</context>
<context>
    <name>MainProgram</name>
    <message>
        <source>Warning</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>The selected file is empty, please select the file again !</source>
        <translation>選取檔案為空，請重新選擇！</translation>
    </message>
</context>
<context>
    <name>PinCodeWidget</name>
    <message>
        <source>Pair</source>
        <translation>配對</translation>
    </message>
    <message>
        <source>&quot; matches the number below. Please do not enter this code on any other accessories.</source>
        <translation type="vanished">སྟེང་དུ་མངོན་པའི་གྲངས་ཀ་དང་འོག་ཏུ་མངོན་པའི་གྲངས་ངོ་འཕྲོད་མི་ཐུབ། ཁྱོས་གཞན་གྱི་སྡེབ་སྒྲིག་སྟེང་ཨང་ཚབ་ནང་འཇུག་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <source>Connection error !!!</source>
        <translation type="vanished">སྦྲེལ་མཐུད་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Is it paired with:</source>
        <translation type="vanished">ཆ་སྒྲིག་བྱེད་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation type="vanished">སྡུད་ལེན།</translation>
    </message>
    <message>
        <source>Refush</source>
        <translation type="vanished">རྩིས་མེད།</translation>
    </message>
    <message>
        <source>Failed to pair with %1 !!!</source>
        <translation type="vanished"> %1དང་ཆ་སྒྲིག་བྱེད་པ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Please make sure the number displayed on &quot;</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་རོགས།</translation>
    </message>
    <message>
        <source>Please enter the following PIN code on the bluetooth device %1 and press enter to pair !</source>
        <translation type="vanished">སོ་སྔོན་སྒྲིག་ཆས་%1སྟེང་གཤམ་གྱི་ཨང་PINནང་འཇུག་བྱས་རྗེས་ཕྱིར་ལོག་མཐེབ་མནོན་མནན་ནས་ཆ་སྒྲིག་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Bluetooth pairing</source>
        <translation>藍牙設備配對</translation>
    </message>
    <message>
        <source>If you want to pair with this device, please make sure the numbers below are the same with the devices.</source>
        <translation type="vanished">གལ་ཏེ་ཁྱོད་ཀྱིས་སྒྲིག་ཆས་འདི་དང་ཆ་སྒྲིག་བྱེད་འདོད་ན།གཤམ་གྱི་གྲངས་ཀ་དང་སྒྲིག་ཆས་གཅིག་མཚུངས་ཡིན་པར་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>If &apos;</source>
        <translation>如果 ”</translation>
    </message>
    <message>
        <source>&apos; the PIN on is the same as this PIN. Please press &apos;Connect&apos;.</source>
        <translation>“ 上的PIN碼與此PIN碼相同，請按 「連接」。</translation>
    </message>
    <message>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair !</source>
        <translation type="vanished">ཁྱོད་ཀྱིས་ཁ་དོག་སྔོན་པོའི་སྒྲིག་ཆས་སྟེང་གི་གཤམ་གྱི་PINཡི་ཚབ་རྟགས་&apos;%1&apos;ནང་འཇུག་བྱས་ནས་ཆ་སྒྲིག་གཉེན་འཚོལ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <source>Please make sure the PIN code displayed on &apos;</source>
        <translation type="vanished">ཁྱོད་ཀྱིས་PINཡི་ཚབ་རྟགས་དེ་མངོན་པར་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>&apos; matches the number below. Please press &apos;Connect&apos;.</source>
        <translation type="vanished">&apos;གཤམ་གྱི་ཨང་གྲངས་དང་མཐུན་པ་རེད། ཁྱེད་ཀྱིས་&quot;སྦྲེལ་མཐུད་&quot;བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Bluetooth Connection</source>
        <translation>藍牙連接</translation>
    </message>
    <message>
        <source>If the PIN on &apos;</source>
        <translation>如”</translation>
    </message>
    <message>
        <source>&apos; is the same as this PIN. Please press &apos;Connect&apos;</source>
        <translation>“上的數位與下面相同，請點擊”連接”</translation>
    </message>
    <message>
        <source>Please enter the following PIN code on the bluetooth device &apos;%1&apos; and press enter to pair:</source>
        <translation>請在藍牙裝置 %1 上輸入相同PIN，並按「Enter」確認：</translation>
    </message>
    <message>
        <source>Refuse</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>藍牙連接</translation>
    </message>
    <message>
        <source>Bluetooth Connect Failed</source>
        <translation>藍牙連接失敗</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Connect Failed!</source>
        <translation>連線失敗！</translation>
    </message>
</context>
<context>
    <name>QDevItem</name>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation>與藍牙設備「%1」連接成功！</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation>藍牙設備「%1」失去連接！</translation>
    </message>
</context>
<context>
    <name>SwitchAction</name>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">ཁ་དོག་སྔོན་པོ།</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <source>Set Bluetooth Item</source>
        <translation>設置藍牙項</translation>
    </message>
</context>
<context>
    <name>TrayWidget</name>
    <message>
        <source>bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>藍牙</translation>
    </message>
    <message>
        <source>My Device</source>
        <translation>我的設備</translation>
    </message>
    <message>
        <source>The connection with the Bluetooth device “%1” is successful!</source>
        <translation>與藍牙設備「%1」連接成功！</translation>
    </message>
    <message>
        <source>Bluetooth device “%1” disconnected!</source>
        <translation>藍牙設備「%1」失去連接！</translation>
    </message>
</context>
</TS>
