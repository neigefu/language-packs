<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Dialog</source>
        <translation>對話</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;kylin video&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;麒麟影音&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;version: 0.0.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;版本: 0.0.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;    Kylin video is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         麒麟影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>service and support: support@kylinos.cn</source>
        <translation>服務和支援： support@kylinos.cn</translation>
    </message>
    <message>
        <source>version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <source>service and support: </source>
        <translation>服務與支援： </translation>
    </message>
    <message>
        <source>Video Player About</source>
        <translation>視頻播放機 關於</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Syloti Nagri&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;    Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;         影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;Video Player&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;視頻播放機&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;    Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;        影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Kylin video supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。麒麟视频支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Video Player supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。影音支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Video Player supports almost all audio and video formats and has powerful decoding ability.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;影音是一款界面美观、互动性好的前置播放器。它是用Qt5开发的，使用MPV作为播放引擎。影音支持几乎所有的音频和视频格式，具有强大的解码能力。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>服務與支援： </translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;version: 3.1.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;版本： 3.1.1&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Mono CJK SC&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;Video Player is a front player with beautiful interface and good interaction. It is developed with Qt5 and uses MPV as the playback engine. Video Player supports almost all audio and video formats and has powerful decoding ability.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p， li { 空白：預包裝; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans Mono CJK SC&apos;; font-size:12pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt;&quot;&gt;視頻播放機是一款介面美觀、互動良好的前端播放機。它是用Qt5開發的，使用MPV作為播放引擎。視頻播放機支持幾乎所有的音訊和視頻格式，並具有強大的解碼能力。&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>ContralBar</name>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>2.0X</source>
        <translation>2.0 倍</translation>
    </message>
    <message>
        <source>1.5X</source>
        <translation>1.5 倍</translation>
    </message>
    <message>
        <source>1.25X</source>
        <translation>1.25 倍</translation>
    </message>
    <message>
        <source>1.0X</source>
        <translation>1.0 倍</translation>
    </message>
    <message>
        <source>0.75X</source>
        <translation>0.75 倍</translation>
    </message>
    <message>
        <source>0.5X</source>
        <translation>0.5 倍</translation>
    </message>
    <message>
        <source>Screen shot</source>
        <translation>屏幕截圖</translation>
    </message>
    <message>
        <source>Add mark</source>
        <translation>添加標記</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>下一個</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>以前</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>玩</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation>卷</translation>
    </message>
    <message>
        <source>Speed</source>
        <translation>速度</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation>工具</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation>全螢幕</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <source>Exit full screen</source>
        <translation>退出全屏</translation>
    </message>
    <message>
        <source>--:--:--/--:--:--</source>
        <translation>--:--:--/--:--:--</translation>
    </message>
</context>
<context>
    <name>GlobalSetup</name>
    <message>
        <source>Pictures</source>
        <translation type="vanished">图片</translation>
    </message>
    <message>
        <source>no</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <source>default</source>
        <translation>違約</translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <source>open file</source>
        <translation>打開檔</translation>
    </message>
    <message>
        <source>open dir</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
</context>
<context>
    <name>KRightClickMenu</name>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>Open &amp;File...</source>
        <translation>開啟並檔案...</translation>
    </message>
    <message>
        <source>&amp;URL...</source>
        <translation type="vanished">&amp;打开URL...</translation>
    </message>
    <message>
        <source>ToTop</source>
        <translation>到頂部</translation>
    </message>
    <message>
        <source>Order</source>
        <translation>次序</translation>
    </message>
    <message>
        <source>One Loop</source>
        <translation>一個迴圈</translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation>序列</translation>
    </message>
    <message>
        <source>List loop</source>
        <translation>清單迴圈</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>隨機</translation>
    </message>
    <message>
        <source>Frame</source>
        <translation>框架</translation>
    </message>
    <message>
        <source>Default frame</source>
        <translation>默認幀</translation>
    </message>
    <message>
        <source>Full frame</source>
        <translation>全畫幅</translation>
    </message>
    <message>
        <source>forward rotate</source>
        <translation>向前旋轉</translation>
    </message>
    <message>
        <source>Along rotate</source>
        <translation>沿旋轉</translation>
    </message>
    <message>
        <source>backward rotate</source>
        <translation>向後旋轉</translation>
    </message>
    <message>
        <source>Inverse rotate</source>
        <translation>反向旋轉</translation>
    </message>
    <message>
        <source>horizontal flip</source>
        <translation>水平翻轉</translation>
    </message>
    <message>
        <source>Horizontally flip</source>
        <translation>水平翻轉</translation>
    </message>
    <message>
        <source>vertical flip</source>
        <translation>垂直翻轉</translation>
    </message>
    <message>
        <source>Vertically flip</source>
        <translation>垂直翻轉</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>音訊</translation>
    </message>
    <message>
        <source>AudioTrack</source>
        <translation>音軌</translation>
    </message>
    <message>
        <source>AudioChannel</source>
        <translation>音頻頻道</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>立體聲</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>左通道</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>右通道</translation>
    </message>
    <message>
        <source>Audio set</source>
        <translation>音訊集</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>字幕</translation>
    </message>
    <message>
        <source>Choose a file</source>
        <translation type="vanished">选择一个文件</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>字幕</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>所有檔</translation>
    </message>
    <message>
        <source>Load subtitle</source>
        <translation>載入字幕</translation>
    </message>
    <message>
        <source>Subtitle select</source>
        <translation>字幕選擇</translation>
    </message>
    <message>
        <source>No subtitle</source>
        <translation>無字幕</translation>
    </message>
    <message>
        <source>Subtitle set</source>
        <translation>字幕集</translation>
    </message>
    <message>
        <source>volume up</source>
        <translation>提高音量</translation>
    </message>
    <message>
        <source>volume down</source>
        <translation>降低音量</translation>
    </message>
    <message>
        <source>forward</source>
        <translation>向前</translation>
    </message>
    <message>
        <source>backward</source>
        <translation>向後</translation>
    </message>
    <message>
        <source>setup</source>
        <translation>設置</translation>
    </message>
    <message>
        <source>More</source>
        <translation type="vanished">更多</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Follow system</source>
        <translation type="vanished">跟随系统</translation>
    </message>
    <message>
        <source>Light theme</source>
        <translation type="vanished">浅色主题</translation>
    </message>
    <message>
        <source>Black theme</source>
        <translation type="vanished">深色主题</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation type="vanished">隐私</translation>
    </message>
    <message>
        <source>Clear mark</source>
        <translation type="vanished">清理痕迹</translation>
    </message>
    <message>
        <source>No mark</source>
        <translation type="vanished">无痕播放</translation>
    </message>
    <message>
        <source>Match subtitle</source>
        <translation>匹配字幕</translation>
    </message>
    <message>
        <source>open file</source>
        <translation>打開檔</translation>
    </message>
    <message>
        <source>Open &amp;Directory...</source>
        <translation>開啟 &amp;目錄...</translation>
    </message>
    <message>
        <source>open dir</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <source>to top</source>
        <translation>到頂部</translation>
    </message>
    <message>
        <source>Show profile</source>
        <translation>顯示個人資料</translation>
    </message>
    <message>
        <source>Search subtitle</source>
        <translation>搜索字幕</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>玩</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>播放/暫停</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation type="vanished">音量+</translation>
    </message>
    <message>
        <source>Volume down</source>
        <translation type="vanished">音量-</translation>
    </message>
    <message>
        <source>Play forward</source>
        <translation type="vanished">快进</translation>
    </message>
    <message>
        <source>Play backward</source>
        <translation type="vanished">快退</translation>
    </message>
    <message>
        <source>Player set</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Media info</source>
        <translation>媒體資訊</translation>
    </message>
    <message>
        <source>4:3</source>
        <translation>4:3</translation>
    </message>
    <message>
        <source>16:9</source>
        <translation>16:9</translation>
    </message>
    <message>
        <source>restore frame</source>
        <translation>還原幀</translation>
    </message>
    <message>
        <source>Video Player Choose a file</source>
        <translation>視頻播放機 選擇一個檔</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>違約</translation>
    </message>
    <message>
        <source>sub load</source>
        <translation>子負載</translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox</name>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
</context>
<context>
    <name>ListLoopMenu</name>
    <message>
        <source>One Loop</source>
        <translation>一個迴圈</translation>
    </message>
    <message>
        <source>List loop</source>
        <translation>清單迴圈</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>隨機</translation>
    </message>
    <message>
        <source>Sequence</source>
        <translation>序列</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
    <message>
        <source>open file</source>
        <translation type="vanished">打开文件</translation>
    </message>
    <message>
        <source>open dir</source>
        <translation type="vanished">打开文件夹</translation>
    </message>
    <message>
        <source>Choose a file</source>
        <translation type="vanished">选择一个文件</translation>
    </message>
    <message>
        <source>Multimedia</source>
        <translation>多媒體</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>視頻</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>音訊</translation>
    </message>
    <message>
        <source>Playlists</source>
        <translation type="vanished">播放列表</translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="vanished">所有文件</translation>
    </message>
    <message>
        <source>Choose a directory</source>
        <translation type="vanished">选择一个文件夹</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Video Player </source>
        <translation type="vanished">影音 </translation>
    </message>
    <message>
        <source>Video Player Choose a file</source>
        <translation>視頻播放機 選擇一個檔</translation>
    </message>
    <message>
        <source>Video Player Choose a directory</source>
        <translation>視頻播放機 選擇一個目錄</translation>
    </message>
    <message>
        <source>video player</source>
        <translation>視頻播放機</translation>
    </message>
    <message>
        <source>/Video</source>
        <translation type="vanished">/视频</translation>
    </message>
</context>
<context>
    <name>MarkListItem</name>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
    <message>
        <source>File not exist!</source>
        <translation>檔不存在！</translation>
    </message>
</context>
<context>
    <name>MediaInfoDialog</name>
    <message>
        <source>Dialog</source>
        <translation>對話</translation>
    </message>
    <message>
        <source>media info</source>
        <translation>媒體資訊</translation>
    </message>
    <message>
        <source>media name:</source>
        <translation type="vanished">媒体文件:</translation>
    </message>
    <message>
        <source>file type:</source>
        <translation type="vanished">文件类型:</translation>
    </message>
    <message>
        <source>file size:</source>
        <translation type="vanished">文件大小:</translation>
    </message>
    <message>
        <source>file duration:</source>
        <translation type="vanished">文件时长:</translation>
    </message>
    <message>
        <source>file path:</source>
        <translation type="vanished">文件路径:</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>kylin video</source>
        <translation type="obsolete">影音</translation>
    </message>
    <message>
        <source>title:</source>
        <translation>標題：</translation>
    </message>
    <message>
        <source>type:</source>
        <translation>類型：</translation>
    </message>
    <message>
        <source>size:</source>
        <translation>大小：</translation>
    </message>
    <message>
        <source>duration:</source>
        <translation>期間：</translation>
    </message>
    <message>
        <source>path:</source>
        <translation>路徑：</translation>
    </message>
    <message>
        <source>video player</source>
        <translation>視頻播放機</translation>
    </message>
</context>
<context>
    <name>MiniModeShade</name>
    <message>
        <source>pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <source>play</source>
        <translation>玩</translation>
    </message>
    <message>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>normal mode</source>
        <translation>正常模式</translation>
    </message>
</context>
<context>
    <name>MpvCore</name>
    <message>
        <source>File size</source>
        <translation>檔大小</translation>
    </message>
    <message>
        <source>Date created</source>
        <translation>創建日期</translation>
    </message>
    <message>
        <source>Media length</source>
        <translation>介質長度</translation>
    </message>
    <message>
        <source>brightness : %1</source>
        <translation>亮度 ： %1</translation>
    </message>
    <message>
        <source>subtitle delay : %1s</source>
        <translation>字幕延遲 ： %1s</translation>
    </message>
    <message>
        <source>Horizontal Flip: </source>
        <translation>水平翻轉： </translation>
    </message>
    <message>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>Vertical Flip: </source>
        <translation>垂直翻轉： </translation>
    </message>
    <message>
        <source>File:</source>
        <translation>檔案：</translation>
    </message>
    <message>
        <source>Video:</source>
        <translation>視頻：</translation>
    </message>
    <message>
        <source>Resolution:</source>
        <translation>解析度：</translation>
    </message>
    <message>
        <source>Estimated FPS:</source>
        <translation type="vanished">估算 FPS:</translation>
    </message>
    <message>
        <source>Bitrate:</source>
        <translation>比特率：</translation>
    </message>
    <message>
        <source>Audio:</source>
        <translation>音訊：</translation>
    </message>
    <message>
        <source>Sample Rate:</source>
        <translation>採樣率：</translation>
    </message>
    <message>
        <source>Channels:</source>
        <translation>管道：</translation>
    </message>
    <message>
        <source>Audio/video synchronization:</source>
        <translation type="vanished">音视频同步差:</translation>
    </message>
    <message>
        <source>volume : %1</source>
        <translation>卷 ： %1</translation>
    </message>
    <message>
        <source>speed : %1x</source>
        <translation>速度 ： %1X</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>靜音</translation>
    </message>
    <message>
        <source>Cancel Mute</source>
        <translation>取消靜音</translation>
    </message>
    <message>
        <source>Playing</source>
        <translation>玩</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>暫停</translation>
    </message>
    <message>
        <source>subtitle : </source>
        <translation>字幕： </translation>
    </message>
    <message>
        <source>File</source>
        <translation>檔</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>標題</translation>
    </message>
    <message>
        <source>Video (x%0)</source>
        <translation>視訊 （x% 0）</translation>
    </message>
    <message>
        <source>Video Output</source>
        <translation>視頻輸出</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>解析度</translation>
    </message>
    <message>
        <source>FPS</source>
        <translation></translation>
    </message>
    <message>
        <source>A/V Sync</source>
        <translation type="vanished">音视频同步</translation>
    </message>
    <message>
        <source>Bitrate</source>
        <translation>比特率</translation>
    </message>
    <message>
        <source>%0 kbps</source>
        <translation></translation>
    </message>
    <message>
        <source>Audio (x%0)</source>
        <translation>音訊 （x%0）</translation>
    </message>
    <message>
        <source>Audio Output</source>
        <translation>音訊輸出</translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation>採樣率</translation>
    </message>
    <message>
        <source>Channels</source>
        <translation>管道</translation>
    </message>
    <message>
        <source>Chapters</source>
        <translation>章</translation>
    </message>
    <message>
        <source>Metadata</source>
        <translation>元數據</translation>
    </message>
    <message>
        <source>fps:</source>
        <translation>轉數快：</translation>
    </message>
    <message>
        <source>restore frame</source>
        <translation>還原幀</translation>
    </message>
    <message>
        <source>ScreenShot OK</source>
        <translation>屏幕截圖確定</translation>
    </message>
    <message>
        <source>ScreenShot Failed</source>
        <translation type="vanished">截图失败，文件夹无写权限</translation>
    </message>
    <message>
        <source>ScreenShot Failed, folder has no write permission.</source>
        <translation type="vanished">截图失败，文件夹无写入权限。</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>立體聲</translation>
    </message>
    <message>
        <source>Left Channel</source>
        <translation>左通道</translation>
    </message>
    <message>
        <source>Right Channel</source>
        <translation>右通道</translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation>圖片</translation>
    </message>
    <message>
        <source>ScreenShot Failed, folder has no write permission or folder not exit.</source>
        <translation>屏幕截圖失敗，資料夾沒有寫入許可權或資料夾未退出。</translation>
    </message>
    <message>
        <source>Add mark ok</source>
        <translation>添加標記確定</translation>
    </message>
    <message>
        <source>Add mark error</source>
        <translation>添加標記錯誤</translation>
    </message>
    <message>
        <source>default</source>
        <translation>違約</translation>
    </message>
    <message>
        <source>auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <source>Buffering...</source>
        <translation>緩衝。。。</translation>
    </message>
</context>
<context>
    <name>PlayListItem</name>
    <message>
        <source>File not exist!</source>
        <translation>檔不存在！</translation>
    </message>
</context>
<context>
    <name>PlayListItemMenu</name>
    <message>
        <source>Remove selected</source>
        <translation>刪除所選取內容</translation>
    </message>
    <message>
        <source>Remove invalid</source>
        <translation>刪除無效</translation>
    </message>
    <message>
        <source>Clear list</source>
        <translation>清除清單</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>打開資料夾</translation>
    </message>
    <message>
        <source>Sort</source>
        <translation>排序</translation>
    </message>
    <message>
        <source>Sort by name</source>
        <translation>按名稱排序</translation>
    </message>
    <message>
        <source>Sort by type</source>
        <translation>按類型排序</translation>
    </message>
</context>
<context>
    <name>PlayListWidget</name>
    <message>
        <source>Video</source>
        <translation>視頻</translation>
    </message>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>File not exist!</source>
        <translation>檔不存在！</translation>
    </message>
    <message>
        <source>Are you sure you want to clear the list?</source>
        <translation>是否確實要清除清單？</translation>
    </message>
    <message>
        <source>Add file</source>
        <translation>添加檔</translation>
    </message>
    <message>
        <source>Play order</source>
        <translation type="vanished">播放顺序</translation>
    </message>
    <message>
        <source>Clear list</source>
        <translation>清除清單</translation>
    </message>
    <message>
        <source>kylin-video-990</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Switch views</source>
        <translation type="vanished">切换视图</translation>
    </message>
    <message>
        <source>Are you sure you want to clear the list?
The file being played will be stopped.</source>
        <translation type="vanished">确定清空列表</translation>
    </message>
    <message>
        <source>The file being played will be stopped.</source>
        <translation>正在播放的檔將被停止。</translation>
    </message>
    <message>
        <source>Marks</source>
        <translation>標誌著</translation>
    </message>
    <message>
        <source>Load file error!</source>
        <translation>載入檔案錯誤！</translation>
    </message>
    <message>
        <source>Preview view</source>
        <translation>預覽檢視</translation>
    </message>
    <message>
        <source>List view</source>
        <translation>清單檢視</translation>
    </message>
    <message>
        <source>One loop</source>
        <translation>一個迴圈</translation>
    </message>
    <message>
        <source>Sequential</source>
        <translation>順序</translation>
    </message>
    <message>
        <source>List loop</source>
        <translation>清單迴圈</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>隨機</translation>
    </message>
    <message>
        <source>Please add file to list ~</source>
        <translation>請將檔案新增到清單中~</translation>
    </message>
</context>
<context>
    <name>PreviewWidget</name>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Model name</source>
        <translation>型號名稱</translation>
    </message>
</context>
<context>
    <name>SetUpDialog</name>
    <message>
        <source>Dialog</source>
        <translation>對話</translation>
    </message>
    <message>
        <source>Set up</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>System</source>
        <translation>系統</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>玩</translation>
    </message>
    <message>
        <source>ScreenShot</source>
        <translation>屏幕截圖</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>字幕</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">声音设置</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation>編 解碼 器</translation>
    </message>
    <message>
        <source>Shortcut</source>
        <translation>捷徑</translation>
    </message>
    <message>
        <source>Video Player Set up</source>
        <translation>視頻播放機設置</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation>設置</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>音訊</translation>
    </message>
</context>
<context>
    <name>SetupCodec</name>
    <message>
        <source>Demux</source>
        <translation>解複用器</translation>
    </message>
    <message>
        <source>Video decoder</source>
        <translation>視頻解碼器</translation>
    </message>
    <message>
        <source>Audio decoder</source>
        <translation>音訊解碼器</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Video output</source>
        <translation>視頻輸出</translation>
    </message>
    <message>
        <source>Decode threads</source>
        <translation>解碼線程</translation>
    </message>
    <message>
        <source>default</source>
        <translation>違約</translation>
    </message>
    <message>
        <source>no</source>
        <translation>不</translation>
    </message>
    <message>
        <source>auto</source>
        <translation>自動</translation>
    </message>
</context>
<context>
    <name>SetupPlay</name>
    <message>
        <source>Set fullscreen when open video.</source>
        <translation type="vanished">打开视频时自动全屏</translation>
    </message>
    <message>
        <source>Clear play list on exit.</source>
        <translation type="vanished">退出时清空播放列表</translation>
    </message>
    <message>
        <source>Automatically plays from where the file was last stopped.</source>
        <translation type="vanished">自动从文件上次停止的位置播放</translation>
    </message>
    <message>
        <source>Automatically find associated files to play</source>
        <translation>自動查找要播放的關聯檔</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Set fullscreen when open video</source>
        <translation>打開視頻時設置全屏</translation>
    </message>
    <message>
        <source>Clear play list on exit</source>
        <translation>退出時清除播放清單</translation>
    </message>
    <message>
        <source>Automatically plays from where the file was last stopped</source>
        <translation>從上次停止檔的位置自動播放</translation>
    </message>
</context>
<context>
    <name>SetupScreenshot</name>
    <message>
        <source>Only save to clipboard.</source>
        <translation type="vanished">仅保存到剪切板</translation>
    </message>
    <message>
        <source>Save to file.</source>
        <translation type="vanished">保存为文件</translation>
    </message>
    <message>
        <source>save path</source>
        <translation>保存路徑</translation>
    </message>
    <message>
        <source>browse</source>
        <translation>流覽</translation>
    </message>
    <message>
        <source>save type</source>
        <translation>保存類型</translation>
    </message>
    <message>
        <source>Screenshot according to the current screen size.</source>
        <translation type="vanished">按当前显示的画面尺寸截图</translation>
    </message>
    <message>
        <source>Choose a directory</source>
        <translation type="vanished">选择一个文件夹</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Only save to clipboard</source>
        <translation>僅保存到剪貼板</translation>
    </message>
    <message>
        <source>Save to file</source>
        <translation>保存到檔案</translation>
    </message>
    <message>
        <source>Screenshot according to the current screen size</source>
        <translation>根據當前螢幕尺寸截圖</translation>
    </message>
    <message>
        <source>Pictures</source>
        <translation>圖片</translation>
    </message>
    <message>
        <source>Video Player Choose a directory</source>
        <translation>視頻播放機 選擇一個目錄</translation>
    </message>
</context>
<context>
    <name>SetupShortcut</name>
    <message>
        <source>file</source>
        <translation>檔</translation>
    </message>
    <message>
        <source>play</source>
        <translation>玩</translation>
    </message>
    <message>
        <source>image</source>
        <translation>圖像</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">声音</translation>
    </message>
    <message>
        <source>subtitle</source>
        <translation type="vanished">字幕</translation>
    </message>
    <message>
        <source>other</source>
        <translation>其他</translation>
    </message>
    <message>
        <source>open file</source>
        <translation>打開檔</translation>
    </message>
    <message>
        <source>open dir</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <source>prev file</source>
        <translation>上一頁檔</translation>
    </message>
    <message>
        <source>next file</source>
        <translation>下一個檔</translation>
    </message>
    <message>
        <source>play/pause</source>
        <translation>播放/暫停</translation>
    </message>
    <message>
        <source>speed up</source>
        <translation>加速</translation>
    </message>
    <message>
        <source>speed down</source>
        <translation>減速</translation>
    </message>
    <message>
        <source>speed normal</source>
        <translation>速度正常</translation>
    </message>
    <message>
        <source>forword</source>
        <translation>福爾茲</translation>
    </message>
    <message>
        <source>backword</source>
        <translation>後記</translation>
    </message>
    <message>
        <source>forward 30s</source>
        <translation>前進30秒</translation>
    </message>
    <message>
        <source>backword 30s</source>
        <translation>後記 30 年代</translation>
    </message>
    <message>
        <source>insert bookmark</source>
        <translation>插入書籤</translation>
    </message>
    <message>
        <source>ib notes</source>
        <translation type="vanished">插入与注释书签</translation>
    </message>
    <message>
        <source>fullscreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <source>mini mode</source>
        <translation>迷你模式</translation>
    </message>
    <message>
        <source>to top</source>
        <translation>到頂部</translation>
    </message>
    <message>
        <source>screenshot</source>
        <translation>截圖</translation>
    </message>
    <message>
        <source>cut</source>
        <translation type="vanished">截取</translation>
    </message>
    <message>
        <source>light up</source>
        <translation>炤</translation>
    </message>
    <message>
        <source>light down</source>
        <translation>燈光下移</translation>
    </message>
    <message>
        <source>forward rotate</source>
        <translation>向前旋轉</translation>
    </message>
    <message>
        <source>backward rotate</source>
        <translation>向後旋轉</translation>
    </message>
    <message>
        <source>horizontal flip</source>
        <translation>水平翻轉</translation>
    </message>
    <message>
        <source>vertical flip</source>
        <translation>垂直翻轉</translation>
    </message>
    <message>
        <source>image boost</source>
        <translation type="vanished">画质增强</translation>
    </message>
    <message>
        <source>volume up</source>
        <translation>提高音量</translation>
    </message>
    <message>
        <source>volume down</source>
        <translation>降低音量</translation>
    </message>
    <message>
        <source>mute</source>
        <translation>靜音</translation>
    </message>
    <message>
        <source>audio next</source>
        <translation>音訊下一個</translation>
    </message>
    <message>
        <source>default channel</source>
        <translation>預設通道</translation>
    </message>
    <message>
        <source>left channel</source>
        <translation>左通道</translation>
    </message>
    <message>
        <source>right channel</source>
        <translation>右通道</translation>
    </message>
    <message>
        <source>sub load</source>
        <translation>子負載</translation>
    </message>
    <message>
        <source>sub earlier</source>
        <translation>子早先</translation>
    </message>
    <message>
        <source>sub later</source>
        <translation>子稍後</translation>
    </message>
    <message>
        <source>sub up</source>
        <translation>子向上</translation>
    </message>
    <message>
        <source>sub down</source>
        <translation>子向下</translation>
    </message>
    <message>
        <source>sub next</source>
        <translation>子 下一頁</translation>
    </message>
    <message>
        <source>play list</source>
        <translation>播放清單</translation>
    </message>
    <message>
        <source>setup</source>
        <translation>設置</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>audio</source>
        <translation>音訊</translation>
    </message>
    <message>
        <source>sub</source>
        <translation>子</translation>
    </message>
</context>
<context>
    <name>SetupSubtitle</name>
    <message>
        <source>Sub loading</source>
        <translation>子裝載</translation>
    </message>
    <message>
        <source>Auto loading subtitles with the same name.</source>
        <translation type="vanished">自动载入同名字幕</translation>
    </message>
    <message>
        <source>Auto loading other subtitles in the same folder</source>
        <translation>在同一資料夾中自動載入其他字幕</translation>
    </message>
    <message>
        <source>Sub Path</source>
        <translation>子路徑</translation>
    </message>
    <message>
        <source>browse</source>
        <translation>流覽</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>字形</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>家庭</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <source>Choose a directory</source>
        <translation type="vanished">选择一个文件夹</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Auto loading subtitles with the same name</source>
        <translation>自動載入同名字幕</translation>
    </message>
    <message>
        <source>Video Player Choose a directory</source>
        <translation>視頻播放機 選擇一個目錄</translation>
    </message>
</context>
<context>
    <name>SetupSystem</name>
    <message>
        <source>Window</source>
        <translation>窗</translation>
    </message>
    <message>
        <source>Minimize to system tray icon</source>
        <translation>最小化為系統托盤圖示</translation>
    </message>
    <message>
        <source>Pause video playback when minimized</source>
        <translation>最小化時暫停視頻播放</translation>
    </message>
    <message>
        <source>Multiple run</source>
        <translation>多次運行</translation>
    </message>
    <message>
        <source>Allow multiple Video Player to run simultaneously</source>
        <translation type="vanished">允许同时运行多个影音</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>After sleep/sleep/lock screen and wake up, keep playing state</source>
        <translation>睡眠/睡眠/鎖定螢幕並醒來后，繼續播放狀態</translation>
    </message>
    <message>
        <source>Allow multiple Kylin Video to run simultaneously</source>
        <translation>允許多個麒麟視頻同時運行</translation>
    </message>
</context>
<context>
    <name>SetupVolume</name>
    <message>
        <source>Sound card selection</source>
        <translation type="vanished">声卡选择</translation>
    </message>
    <message>
        <source>Volume contral</source>
        <translation>音量控制</translation>
    </message>
    <message>
        <source>Global volume</source>
        <translation>全球銷量</translation>
    </message>
    <message>
        <source>Default volume standardization</source>
        <translation>默認卷標準化</translation>
    </message>
    <message>
        <source>Form</source>
        <translation>形式</translation>
    </message>
    <message>
        <source>Output driver</source>
        <translation>輸出驅動器</translation>
    </message>
</context>
<context>
    <name>ShortCutItem</name>
    <message>
        <source>Hotkey conflict</source>
        <translation>熱鍵衝突</translation>
    </message>
</context>
<context>
    <name>ShortCutSetting</name>
    <message>
        <source>open file</source>
        <translation>打開檔</translation>
    </message>
    <message>
        <source>open dir</source>
        <translation>打開目錄</translation>
    </message>
    <message>
        <source>prev file</source>
        <translation>上一頁檔</translation>
    </message>
    <message>
        <source>next file</source>
        <translation>下一個檔</translation>
    </message>
    <message>
        <source>play/pause</source>
        <translation>播放/暫停</translation>
    </message>
    <message>
        <source>speed up</source>
        <translation>加速</translation>
    </message>
    <message>
        <source>speed down</source>
        <translation>減速</translation>
    </message>
    <message>
        <source>speed normal</source>
        <translation>速度正常</translation>
    </message>
    <message>
        <source>forword</source>
        <translation>福爾茲</translation>
    </message>
    <message>
        <source>backword</source>
        <translation>後記</translation>
    </message>
    <message>
        <source>forward 30s</source>
        <translation>前進30秒</translation>
    </message>
    <message>
        <source>backword 30s</source>
        <translation>後記 30 年代</translation>
    </message>
    <message>
        <source>insert bookmark</source>
        <translation>插入書籤</translation>
    </message>
    <message>
        <source>ib notes</source>
        <translation>興業銀行注意事項</translation>
    </message>
    <message>
        <source>fullscreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <source>mini mode</source>
        <translation>迷你模式</translation>
    </message>
    <message>
        <source>to top</source>
        <translation>到頂部</translation>
    </message>
    <message>
        <source>screenshot</source>
        <translation>截圖</translation>
    </message>
    <message>
        <source>cut</source>
        <translation>切</translation>
    </message>
    <message>
        <source>light up</source>
        <translation>炤</translation>
    </message>
    <message>
        <source>light down</source>
        <translation>燈光下移</translation>
    </message>
    <message>
        <source>forward rotate</source>
        <translation>向前旋轉</translation>
    </message>
    <message>
        <source>backward rotate</source>
        <translation>向後旋轉</translation>
    </message>
    <message>
        <source>horizontal flip</source>
        <translation>水平翻轉</translation>
    </message>
    <message>
        <source>vertical flip</source>
        <translation>垂直翻轉</translation>
    </message>
    <message>
        <source>image boost</source>
        <translation>圖像增強</translation>
    </message>
    <message>
        <source>volume up</source>
        <translation>提高音量</translation>
    </message>
    <message>
        <source>volume down</source>
        <translation>降低音量</translation>
    </message>
    <message>
        <source>mute</source>
        <translation>靜音</translation>
    </message>
    <message>
        <source>audio next</source>
        <translation>音訊下一個</translation>
    </message>
    <message>
        <source>default channel</source>
        <translation>預設通道</translation>
    </message>
    <message>
        <source>left channel</source>
        <translation>左通道</translation>
    </message>
    <message>
        <source>right channel</source>
        <translation>右通道</translation>
    </message>
    <message>
        <source>sub load</source>
        <translation>子負載</translation>
    </message>
    <message>
        <source>sub earlier</source>
        <translation>子早先</translation>
    </message>
    <message>
        <source>sub later</source>
        <translation>子稍後</translation>
    </message>
    <message>
        <source>sub up</source>
        <translation>子向上</translation>
    </message>
    <message>
        <source>sub down</source>
        <translation>子向下</translation>
    </message>
    <message>
        <source>sub next</source>
        <translation>子 下一頁</translation>
    </message>
    <message>
        <source>play list</source>
        <translation>播放清單</translation>
    </message>
    <message>
        <source>setup</source>
        <translation>設置</translation>
    </message>
    <message>
        <source>exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>help documentation</source>
        <translation>幫助文件</translation>
    </message>
</context>
<context>
    <name>SystemTrayIcon</name>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TitleMenu</name>
    <message>
        <source>Upload to cloud</source>
        <translation>上傳到雲</translation>
    </message>
    <message>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <source>Follow system</source>
        <translation>關注系統</translation>
    </message>
    <message>
        <source>Light theme</source>
        <translation>淺色主題</translation>
    </message>
    <message>
        <source>Black theme</source>
        <translation>黑色主題</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>隱私</translation>
    </message>
    <message>
        <source>Clear mark</source>
        <translation>清除標記</translation>
    </message>
    <message>
        <source>No mark</source>
        <translation>無標記</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <source>Check update</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <source>Advice and feedback</source>
        <translation>建議和反饋</translation>
    </message>
    <message>
        <source>Official website</source>
        <translation>官方網站</translation>
    </message>
    <message>
        <source>Setup</source>
        <translation>設置</translation>
    </message>
    <message>
        <source>System setup</source>
        <translation>系統設置</translation>
    </message>
    <message>
        <source>Play setup</source>
        <translation>播放設置</translation>
    </message>
    <message>
        <source>Screenshot setup</source>
        <translation>屏幕截圖設置</translation>
    </message>
    <message>
        <source>Subtitle setup</source>
        <translation>字幕設置</translation>
    </message>
    <message>
        <source>Audio setup</source>
        <translation>音訊設置</translation>
    </message>
    <message>
        <source>Decoder setup</source>
        <translation>解碼器設置</translation>
    </message>
    <message>
        <source>Shortcut setup</source>
        <translation>快捷方式設置</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>恢復</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>功能表</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>迷你模式</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>TopWindow</name>
    <message>
        <source>Video Player</source>
        <translation>視頻播放機</translation>
    </message>
</context>
<context>
    <name>WaylandDialog</name>
    <message>
        <source>Dialog</source>
        <translation>對話</translation>
    </message>
</context>
</TS>
