<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <source>ukui-sidebar is already running!</source>
        <translation>ukuii-sddbarrarstistre</translation>
    </message>
    <message>
        <source>show or hide sidebar widget</source>
        <translation>ཟུར་ངོས་ཀྱི་ལྷུ་ལག་ཆུང་ཆུང་ཞིག་མངོན་པའམ་སྦས་སྐུང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>show sidebar widget</source>
        <translation>གཞོགས་ངོས་ཀྱི་ལྷུ་ལག་ཆུང་ངུ་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>hide sidebar widget</source>
        <translation>གཞོགས་ངོས་ཀྱི་ལྷུ་ལག་ཆུང་ངུ་སྦས་སྐུང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Set up notification center</source>
        <translation>བརྡ་ཐོའི་ལྟེ་གནས་འཛུགས་དགོས།</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>ukui-sidebar</source>
        <translation>ལོགས་ངོས་ཀྱི་ར་བ།</translation>
    </message>
    <message>
        <source> Notifications</source>
        <translation> བརྡ་ཐོ།</translation>
    </message>
</context>
</TS>
