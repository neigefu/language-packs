<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <source>ukui-sidebar is already running!</source>
        <translation>ukui-sidebar 已经在运行了！</translation>
    </message>
    <message>
        <source>show or hide sidebar widget</source>
        <translation>显示或隐藏侧边栏小部件</translation>
    </message>
    <message>
        <source>show sidebar widget</source>
        <translation>显示侧边栏小部件</translation>
    </message>
    <message>
        <source>hide sidebar widget</source>
        <translation>隐藏侧边栏小部件</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>Set up notification center</source>
        <translation>设置通知中心</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>ukui-sidebar</source>
        <translation>侧边栏</translation>
    </message>
    <message>
        <source> Notifications</source>
        <translation> 条通知</translation>
    </message>
</context>
</TS>
