<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>QObject</name>
    <message>
        <source>ukui-sidebar is already running!</source>
        <translation>ukuui-sidebar ᠨᠢᠭᠡᠨᠲᠡ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠶᠢᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <source>show or hide sidebar widget</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠶᠢᠨ ᠵᠢᠵᠢᠭ ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠲᠣᠨᠣᠭ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>show sidebar widget</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠶᠢᠨ ᠬᠠᠵᠠᠭᠤ ᠳᠠᠬᠢ ᠵᠢᠵᠢᠭ ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠲᠣᠨᠣᠭ ᠢ ᠢᠯᠡᠷᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>hide sidebar widget</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠶᠢᠨ ᠵᠢᠵᠢᠭ ᠤᠭᠰᠠᠷᠠᠭ᠎ᠠ ᠲᠣᠨᠣᠭ ᠢ ᠨᠢᠭᠤᠨ ᠳᠠᠯᠳᠠᠯᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <source>Set up notification center</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ ᠦᠨ ᠲᠥᠪ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>ukui-sidebar</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠶᠢᠨ ᠬᠠᠵᠠᠭᠤ ᠳᠠᠬᠢ ᠬᠠᠰᠢᠯᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source> Notifications</source>
        <translation> ᠵᠤᠷᠪᠤᠰ ᠲᠤ ᠮᠡᠳᠡᠭᠳᠡᠬᠦ ᠃</translation>
    </message>
</context>
</TS>
