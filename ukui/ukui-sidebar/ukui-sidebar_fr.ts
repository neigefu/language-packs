<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>QObject</name>
    <message>
        <source>ukui-sidebar is already running!</source>
        <translation></translation>
    </message>
    <message>
        <source>show or hide sidebar widget</source>
        <translation></translation>
    </message>
    <message>
        <source>show sidebar widget</source>
        <translation></translation>
    </message>
    <message>
        <source>hide sidebar widget</source>
        <translation></translation>
    </message>
    <message>
        <source>Open</source>
        <translation></translation>
    </message>
    <message>
        <source>Set up notification center</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>ukui-sidebar</source>
        <translation></translation>
    </message>
    <message>
        <source> Notifications</source>
        <translation></translation>
    </message>
</context>
</TS>
