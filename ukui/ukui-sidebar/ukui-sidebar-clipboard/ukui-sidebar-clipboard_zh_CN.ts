<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Are you sure clear your clipboard history? This operation cannot be undone.</source>
        <translation>确认要清空剪贴板中的项目吗？此操作无法撤销。</translation>
    </message>
    <message>
        <source>Don&apos;t show</source>
        <translation>不再显示</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>清空剪贴板</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Fix</source>
        <translation>固定</translation>
    </message>
    <message>
        <source>EditButton</source>
        <translation>编辑</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Cancel the fixed</source>
        <translation>取消固定</translation>
    </message>
    <message>
        <source>Edit box</source>
        <translation>编辑框</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>SearchWidgetItemContent</name>
    <message>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>SidebarClipboardPlugin</name>
    <message>
        <source>No clip content</source>
        <translation>无剪贴内容</translation>
    </message>
</context>
</TS>
