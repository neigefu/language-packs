<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>QObject</name>
    <message>
        <source>Are you sure clear your clipboard history? This operation cannot be undone.</source>
        <translation></translation>
    </message>
    <message>
        <source>Don&apos;t show</source>
        <translation></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation></translation>
    </message>
    <message>
        <source>Fix</source>
        <translation></translation>
    </message>
    <message>
        <source>EditButton</source>
        <translation></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation></translation>
    </message>
    <message>
        <source>Cancel the fixed</source>
        <translation></translation>
    </message>
    <message>
        <source>Edit box</source>
        <translation></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation></translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchWidgetItemContent</name>
    <message>
        <source>Clear</source>
        <translation></translation>
    </message>
    <message>
        <source>Search</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SidebarClipboardPlugin</name>
    <message>
        <source>No clip content</source>
        <translation></translation>
    </message>
</context>
</TS>
