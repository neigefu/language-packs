<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Are you sure clear your clipboard history? This operation cannot be undone.</source>
        <translation>དྲས་སྦྱོར་པང་ལེབ་ཁྲོད་ཀྱི་རྣམ་གྲངས་གཙང་བཤེར་བྱེད་དགོས་པ་གཏན་ཁེལ་བྱས་ཡོད་དམ།བཀོལ་སྤྱོད་འདི་ཕྱིར་འཐེན་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <source>Don&apos;t show</source>
        <translation>མངོན་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>འབྲེག་སྦྱར་པང་ལེབ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Fix</source>
        <translation>གཏན་འཇགས།</translation>
    </message>
    <message>
        <source>EditButton</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>སུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <source>Cancel the fixed</source>
        <translation>གཏན་འཇགས།</translation>
    </message>
    <message>
        <source>Edit box</source>
        <translation>རྩོམ་སྒྲིག་སྒྲོམ།</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ངོས་འཛིན།</translation>
    </message>
</context>
<context>
    <name>SearchWidgetItemContent</name>
    <message>
        <source>Clear</source>
        <translation>བར་སྣང་གཙང་བཤེར།</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>བཤེར་འཚོལ།</translation>
    </message>
</context>
<context>
    <name>SidebarClipboardPlugin</name>
    <message>
        <source>No clip content</source>
        <translation>དྲས་སྦྱོར་གྱི་ནང་དོན་མེད།</translation>
    </message>
</context>
</TS>
