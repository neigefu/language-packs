<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <source>Are you sure clear your clipboard history? This operation cannot be undone.</source>
        <translation>ᠡᠰᠬᠡᠮᠡᠯ ᠬᠠᠪᠲᠠᠰᠤ᠎ᠳ᠋ᠠᠬᠢ ᠵᠦᠢᠯ᠎ᠢ᠋ ᠬᠣᠭᠣᠰᠣᠯᠠᠬᠤ ᠤᠤ ? ︖ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶ᠋ᠢ ᠬᠠᠰᠤᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Don&apos;t show</source>
        <translation>ᠳᠠᠬᠢᠨ ᠢᠯᠡᠷᠡᠭᠦᠯᠬᠦ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠮᠠᠯ ᠬᠠᠪᠲᠠᠰᠤ᠎ᠶ᠋ᠢ ᠬᠣᠭᠣᠰᠣᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠪᠣᠯᠢᠬᠤ ᠂ ᠪᠣᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Fix</source>
        <translation>ᠲᠣᠭᠲᠠᠮᠠᠯ</translation>
    </message>
    <message>
        <source>EditButton</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>ᠬᠠᠰᠤᠬᠤ᠎ᠶ᠋ᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel the fixed</source>
        <translation>ᠲᠣᠭᠲᠠᠮᠠᠯ ᠪᠤᠰᠤ ᠪᠣᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Edit box</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ ᠪᠣᠺᠰ</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>ᠨᠠᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>ᠨᠣᠲ᠋ᠠᠯᠠᠨ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>SearchWidgetItemContent</name>
    <message>
        <source>Clear</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>ᠨᠡᠩᠵᠢᠬᠦ ᠂ ᠨᠡᠩᠵᠢᠬᠦ</translation>
    </message>
</context>
<context>
    <name>SidebarClipboardPlugin</name>
    <message>
        <source>No clip content</source>
        <translation>ᠡᠰᠬᠡᠮᠡᠯ᠎ᠦ᠋ᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠦᠭᠡᠢ</translation>
    </message>
</context>
</TS>
