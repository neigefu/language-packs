<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QObject</name>
    <message>
        <source>ukui-sidebar is already running!</source>
        <translation>ukui-sidebar 已經在運行了！</translation>
    </message>
    <message>
        <source>show or hide sidebar widget</source>
        <translation>顯示或隱藏側邊欄小部件</translation>
    </message>
    <message>
        <source>show sidebar widget</source>
        <translation>顯示側邊欄小部件</translation>
    </message>
    <message>
        <source>hide sidebar widget</source>
        <translation>隱藏側邊欄小部件</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>Set up notification center</source>
        <translation>設置通知中心</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>ukui-sidebar</source>
        <translation>側邊欄</translation>
    </message>
    <message>
        <source> Notifications</source>
        <translation> 條通知</translation>
    </message>
</context>
</TS>
