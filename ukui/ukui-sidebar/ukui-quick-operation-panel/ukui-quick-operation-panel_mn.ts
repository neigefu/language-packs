<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>QObject</name>
    <message>
        <source>Administrators</source>
        <translation>ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ ᠃</translation>
    </message>
    <message>
        <source>Standard users</source>
        <translation>ᠡᠩ ᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠃</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <source>Energy</source>
        <translation>ᠡᠨᠧᠷᠭᠢ ᠠᠷᠪᠢᠯᠠᠬᠤ ᠵᠠᠭᠪᠤᠷ᠃</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>ᠬᠥᠬᠡ ᠰᠢᠳᠦ᠃</translation>
    </message>
    <message>
        <source>Flight</source>
        <translation>ᠨᠢᠰᠦᠯᠲᠡ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Projectscreen</source>
        <translation>ᠳᠡᠯᠬᠡᠴᠡ᠃</translation>
    </message>
    <message>
        <source>Night mode</source>
        <translation>ᠰᠥᠨᠢ ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>ᠵᠢᠷᠤᠭ ᠵᠢᠷᠤᠭ᠃</translation>
    </message>
    <message>
        <source>Clipboard</source>
        <translation>ᠬᠠᠶᠢᠴᠢᠯᠠᠬᠤ ᠰᠠᠮᠪᠠᠷ᠎ᠠ ᠶᠢ ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>NotiToggle</source>
        <translation>ᠦᠢᠮᠡᠭᠡᠨ ᠦ ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Autorotate</source>
        <translation>ᠠᠦᠢᠲ᠋ᠣ᠋ ᠡᠷᠭᠢᠯᠳᠦᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <source>Pad</source>
        <translation>ᠬᠠᠪᠲᠠᠭᠠᠢ ᠬᠠᠪᠲᠠᠭᠠᠢ ᠵᠠᠭᠪᠤᠷ ᠃</translation>
    </message>
    <message>
        <source>Notebook</source>
        <translation>ᠨᠠᠭᠠᠵᠤ ᠨᠠᠭᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <source>Support</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠦ ᠳᠡᠮᠵᠢᠯᠭᠡ ᠦᠵᠡᠭᠦᠯᠬᠦ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>Clock</source>
        <translation>ᠰᠡᠷᠢᠭᠦᠭᠡᠯᠲᠦ ᠴᠠᠭ ᠃</translation>
    </message>
</context>
<context>
    <name>ScrollingAreaWidgetMajor</name>
    <message>
        <source>Volume</source>
        <translation>ᠳᠠᠭᠤ ᠃</translation>
    </message>
    <message>
        <source>Bright</source>
        <translation>ᠭᠡᠷᠡᠯ ᠦᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>quickOperationWidget</name>
    <message>
        <source>Shortcut Panel</source>
        <translation>ᠲᠦᠷᠭᠡᠨ ᠣᠷᠣᠬᠤ ᠡᠭᠦᠳᠡ ᠃</translation>
    </message>
    <message>
        <source>Clipboard</source>
        <translation>ᠬᠠᠶᠢᠴᠢᠯᠠᠬᠤ ᠰᠠᠮᠪᠠᠷ᠎ᠠ ᠶᠢ ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
