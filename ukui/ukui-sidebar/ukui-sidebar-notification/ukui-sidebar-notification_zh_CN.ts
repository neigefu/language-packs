<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Fold</source>
        <translation>折叠</translation>
    </message>
    <message>
        <source>Notification Center</source>
        <translation>通知中心</translation>
    </message>
    <message>
        <source>Clean</source>
        <translation>清空</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>没有新通知</translation>
    </message>
</context>
<context>
    <name>SingleMsg</name>
    <message>
        <source>Expired</source>
        <translation>已过期</translation>
    </message>
    <message>
        <source>Now</source>
        <translation>现在</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>昨天 </translation>
    </message>
    <message>
        <source>In addition </source>
        <translation>还有 </translation>
    </message>
    <message>
        <source> notification</source>
        <translation> 则通知</translation>
    </message>
</context>
</TS>
