<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn_MN">
<context>
    <name>QObject</name>
    <message>
        <source>Fold</source>
        <translation>ᠨᠤᠭᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>Notification Center</source>
        <translation>ᠲᠥᠪ ᠲᠦ ᠮᠡᠳᠡᠭᠳᠡ ᠃</translation>
    </message>
    <message>
        <source>Clean</source>
        <translation>ᠬᠣᠭᠣᠰᠣᠨ ᠃</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ ᠃</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>SingleMsg</name>
    <message>
        <source>Expired</source>
        <translation>ᠨᠢᠭᠡᠨᠲᠡ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ ᠬᠡᠲᠦᠷᠡᠵᠡᠢ ᠃</translation>
    </message>
    <message>
        <source>Now</source>
        <translation>ᠣᠳᠣ ᠃</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>ᠥᠴᠥᠭᠡᠳᠦᠷ ᠃ </translation>
    </message>
    <message>
        <source>In addition </source>
        <translation>ᠪᠠᠰᠠ ᠪᠠᠢ᠌ᠨ᠎ᠠ ᠃ </translation>
    </message>
    <message>
        <source> notification</source>
        <translation> ᠮᠡᠳᠡᠭᠳᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
</TS>
