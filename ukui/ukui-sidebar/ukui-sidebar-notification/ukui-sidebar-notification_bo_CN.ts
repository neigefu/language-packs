<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Fold</source>
        <translation>ལྟེབ་བརྩེགས་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Notification Center</source>
        <translation>ལྟེ་གནས་ལ་བརྡ་ཐོ་གཏོང</translation>
    </message>
    <message>
        <source>Clean</source>
        <translation>དྭངས་ཤིང་གཙང་བ།</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>བཀོད་སྒྲིག་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>No new notifications</source>
        <translation>བརྡ་ཐོ་གསར་པ་མེད།</translation>
    </message>
</context>
<context>
    <name>SingleMsg</name>
    <message>
        <source>Expired</source>
        <translation>དུས་ལས་ཡོལ་ཟིན།</translation>
    </message>
    <message>
        <source>Now</source>
        <translation>ད་ལྟ་</translation>
    </message>
    <message>
        <source>Yesterday </source>
        <translation>ཁ་སང་། </translation>
    </message>
    <message>
        <source>In addition </source>
        <translation>ད་དུང་ཡོད། </translation>
    </message>
    <message>
        <source> notification</source>
        <translation> བརྡ་ཐོ་གཏོང་དགོས།</translation>
    </message>
</context>
</TS>
