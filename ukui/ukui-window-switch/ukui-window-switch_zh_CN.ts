<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en_US">
<context>
    <name>DesktopArea</name>
    <message>
        <location filename="qml/DesktopArea.qml" line="108"/>
        <source>Desktop</source>
        <translatorcomment>工作区</translatorcomment>
        <translation>工作区</translation>
    </message>
    <message>
        <location filename="qml/DesktopArea.qml" line="295"/>
        <source>New Desktop</source>
        <translatorcomment>新建工作区</translatorcomment>
        <translation>新建工作区</translation>
    </message>
</context>
<context>
    <name>TabletViewClearAllWindowButton</name>
    <message>
        <location filename="qml/TabletViewClearAllWindowButton.qml" line="43"/>
        <source>Clear</source>
        <translatorcomment>清除</translatorcomment>
        <translation>清除</translation>
    </message>
</context>
<context>
    <name>TabletViewRootWindow</name>
    <message>
        <location filename="qml/TabletViewRootWindow.qml" line="139"/>
        <source>No recent tasks</source>
        <translatorcomment>无应用开启</translatorcomment>
        <translation>无应用开启</translation>
    </message>
</context>
</TS>
