<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="34"/>
        <source>select detailed category</source>
        <translation>егжей-тегжейлі санатты таңдау</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="38"/>
        <source>System</source>
        <translation>Жүйе</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System activation</source>
        <translation>Жүйені белсендіру</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System installation</source>
        <translation>Жүйені орнату</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System crash</source>
        <translation>Жүйенің жаңылысы</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System performance</source>
        <translation>Жүйелік өнімділік</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>Control center</source>
        <translation>Басқару орталығы</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System setting</source>
        <translation>Жүйе параметрі</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System basis consulting</source>
        <translation>Жүйелік негіз консалтинг</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="49"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>Сіз кездестірген мәселелерді егжей-тегжейлі сипаттаңыз, мысалы: жүйені іске қосу мүмкін емес, тиісті параметрлер табылмады, жүйенің функциялары түсініксіз және т.б.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="61"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>Сіз кездестірген мәселелерді егжей-тегжейлі сипаттаңыз, мысалы: перифериялық қосылымның сәтсіздігі, функцияның ортақ параметрлері, перифериялық бейімделу және т.б.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="71"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>Kirin бағдарламалық жасақтамасының қателерін алу, орнату және жою сияқты мәселелерді егжей-тегжейлі сипаттаңыз.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="80"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>Мәселеңізді егжей-тегжейлі сипаттаңыз, немесе сіз өзіңіздің сұрауыңызды немесе түсініктемеңізді осы жерде толтыра аласыз.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="54"/>
        <source>Peripheral</source>
        <translation>Периферия</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral adaptation consulting</source>
        <translation>Перифериялық бейімделу бойынша консалтинг</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral driver acquisition</source>
        <translation>Перифериялық жүргізушіні сатып алу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral use and error reporting</source>
        <translation>Перифериялық пайдалану және қателер туралы есеп беру</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="66"/>
        <source>Application</source>
        <translation>Қолданба</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software installation and uninstallation</source>
        <translation>Бағдарламалық қамтамасыз етуді орнату және жою</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software use and error reporting</source>
        <translation>Бағдарламалық жасақтаманы пайдалану және қателер туралы есеп беру</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="76"/>
        <source>Other</source>
        <translation>Басқалары</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="78"/>
        <source>Opinions and suggestions</source>
        <translation>Пікірлер мен ұсыныстар</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="129"/>
        <source>Title and question details cannot be blank!</source>
        <translation>Тақырып пен сұрақ мәліметтері бос бола алмайды!</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="139"/>
        <source>Attachment size exceeds limit!</source>
        <translation>Тіркеме өлшемі шектен асып түседі!</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="62"/>
        <source>Failed to create temporary directory!</source>
        <translation>Уақытша каталог жасалмады!</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>Секіру</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>Бет</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>барлығы</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>бет</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>Секіру</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>бет</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="180"/>
        <source>System log</source>
        <translation>Жүйелік журнал</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="181"/>
        <source>Machine</source>
        <translation>Машина</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="182"/>
        <source>Hardware</source>
        <translation>Аппараттық құралдар</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="183"/>
        <source>Drive</source>
        <translation>Дискі</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="184"/>
        <source>APP list</source>
        <translation>APP тізімі</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="185"/>
        <source>Rules</source>
        <translation>Ережелер</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="186"/>
        <source>Network</source>
        <translation>Желі</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="187"/>
        <source>System</source>
        <translation>Жүйе</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>ServiceSupport</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Creation time</source>
        <translation>Жасалу уақыты</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Type</source>
        <translation>Түрі</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="64"/>
        <source>bydesign</source>
        <translation>Дизайн бойынша</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="66"/>
        <source>duplicate</source>
        <translation>&amp; Көшіру</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="68"/>
        <source>external</source>
        <translation>Сыртқы</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="70"/>
        <source>fixed</source>
        <translation>Бекітілген</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="72"/>
        <source>notrepro</source>
        <translation>Көбею мүмкін емес</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="74"/>
        <source>postponed</source>
        <translation>Кешіктіру</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="76"/>
        <source>willnotfix</source>
        <translation>Жөндеу мүмкін емес</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="86"/>
        <source>in process</source>
        <translation>Процесте</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="97"/>
        <source>verify</source>
        <translation>Тексеру</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="91"/>
        <source>completed</source>
        <translation>аяқталды</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Description</source>
        <translation>сипаттама</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Solution</source>
        <translation>Шешімі</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Progress</source>
        <translation>прогресс</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>No record</source>
        <translation>ەستەلىك</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="156"/>
        <source>There is a network problem, please try again later</source>
        <translation>Желі проблемасы бар, кейінірек қайталап көріңіз</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="157"/>
        <source>Loading, please wait</source>
        <translation>Жүктеу, күтуіңізді сұраймын</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="170"/>
        <source>retry</source>
        <translation>ретри</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="108"/>
        <source>Advanced</source>
        <translation>Қосымша</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="113"/>
        <source>Type</source>
        <translation>Түрі</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="130"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>Мәселені егжей-тегжейлі сипаттаңыз, төмендегі батырманы басу арқылы фотосуретті немесе файлды жүктей аласыз.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Remaining</source>
        <translation>Қалғандары</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>character</source>
        <translation>таңбасы</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="168"/>
        <source>Details</source>
        <translation>Егжей- тегжей</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="175"/>
        <source>ScreenShot</source>
        <translation>Скриншот</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="190"/>
        <source>Add file</source>
        <translation>Файлды қосу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="220"/>
        <source>Attachments</source>
        <translation>Тіркемелер</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="241"/>
        <source>The phone number cannot be empty</source>
        <translation>Телефон нөмірі бос бола алмайды</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="243"/>
        <source>The phone number format is incorrect</source>
        <translation>Телефон нөмірінің пішімі дұрыс емес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="248"/>
        <source>Please enter your phone number</source>
        <translation>Телефон нөмірін енгізіңіз</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="254"/>
        <source>appellation</source>
        <translation>аппеляция</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="262"/>
        <source>Contact</source>
        <translation>Контакт</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="273"/>
        <source>The mailbox format is incorrect</source>
        <translation>Пошта жәшігінің пішімі дұрыс емес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Mailbox</source>
        <translation>Пошта жәшігі</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="292"/>
        <source>Agree to take mine </source>
        <translation>Кенішті алуға келісім беру </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="302"/>
        <source>System information</source>
        <translation>Жүйелік ақпарат</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="328"/>
        <source>Submit</source>
        <translation>Жіберу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="345"/>
        <source>Details type</source>
        <translation>Мәліметтер түрі</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="360"/>
        <source>Time period</source>
        <translation>Уақыт кезеңі</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="369"/>
        <source>Job number</source>
        <translation>Жұмыс орнының нөмірі</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="404"/>
        <source>Information</source>
        <translation>Ақпарат</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="407"/>
        <source>lately</source>
        <translation>соңғы кездері</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="408"/>
        <source>days</source>
        <translation>күндер</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="420"/>
        <source>YES</source>
        <translation>ИӘ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="430"/>
        <source>NO</source>
        <translation>ЖОҚ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="452"/>
        <source>Upload log</source>
        <translation>Кері жүктеу журналы</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="458"/>
        <source>Path</source>
        <translation>Жол</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="472"/>
        <source>Export to</source>
        <translation>Экспорттау</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="548"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>5 файлдан артық емес және жалпы сыйымдылығы 10Мб-тан аспайды</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="549"/>
        <source>Supported formats: </source>
        <translation>Қолдау көрсетілген пішімдер: </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="131"/>
        <source>Up to 500 characters</source>
        <translation>500 таңбаға дейін</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="626"/>
        <source>Repeat addition</source>
        <translation>Толықтыруды қайталау</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="642"/>
        <source>Attachment size out of limit</source>
        <translation>Тіркеме өлшемі шектен тыс</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="808"/>
        <source>Add attachment</source>
        <translation>Тіркемені қосу</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="34"/>
        <source>OK</source>
        <translation>ЖАҚСЫ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="37"/>
        <source>Retry</source>
        <translation>ретри</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="79"/>
        <source>Submitted successfully</source>
        <translation>Сәтті ұсынылды</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="83"/>
        <source>Cancel successfully</source>
        <translation>Бас тарту сәті</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="87"/>
        <source>System is abnormal, contact technical support</source>
        <translation>Жүйе қалыпты емес, техникалық қолдау қызметіне хабарласыңыз</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Log and submission is packed, please go</source>
        <translation>Журнал мен жіберулер жинақталған, қалдырыңыз</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="95"/>
        <source>acquire</source>
        <translation>Алу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="93"/>
        <source>Submission failed</source>
        <translation>Жіберу жаңылысы</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>Қайта жүктеу үшін « Қайталау » түймесін басыңыз немесе бізбен тікелей байланысыңыз.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>Under submission...</source>
        <translation>Ұсыну бойынша...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="17"/>
        <source>Contact us</source>
        <translation>Бізге хабарласыңыз</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="37"/>
        <source>Telephone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="50"/>
        <source>Mail</source>
        <translation>Пошта</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="63"/>
        <source>Team</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="80"/>
        <source> to get more services</source>
        <translation> қосымша қызметтерді алу үшін</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="98"/>
        <source>Kylin technical services</source>
        <translation>Көлiк техникалық қызметтерiн көрсету</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="72"/>
        <source>Jump to</source>
        <translation>Секіру</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="73"/>
        <source> KylinOS website</source>
        <translation> KylinOS веб-сайты</translation>
    </message>
</context>
<context>
    <name>UiServiceOnline</name>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="40"/>
        <source>Loading, please wait</source>
        <translation>Жүктеу, күтуіңізді сұраймын</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="54"/>
        <source>retry</source>
        <translation>ретри</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="67"/>
        <source>There is a network problem, please try again later</source>
        <translation>Желі проблемасы бар, кейінірек қайталап көріңіз</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="83"/>
        <source>Go to your browser</source>
        <translation>браузерге</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="84"/>
        <source>Go</source>
        <translation>Мурунку</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="19"/>
        <source>Feedback</source>
        <translation>Кері байланыс</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="20"/>
        <source>Online</source>
        <translation>Желіде</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="21"/>
        <source>Self service</source>
        <translation>Өзіне-өзі қызмет көрсету</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="22"/>
        <source>History</source>
        <translation>Журнал</translation>
    </message>
</context>
</TS>
