<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="35"/>
        <source>select detailed category</source>
        <translation>请选择详细分类</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="39"/>
        <source>System</source>
        <translation>系统使用</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System activation</source>
        <translation>系统激活</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System installation</source>
        <translation>系统安装</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System crash</source>
        <translation>系统崩溃</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System performance</source>
        <translation>系统性能</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>Control center</source>
        <translation>控制面板</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System setting</source>
        <translation>系统设置</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="43"/>
        <source>System basis consulting</source>
        <translation>系统基础咨询</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="51"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>您可以在此反馈系统使用方面的问题，如：无法激活系统、找不到相关设置、不清楚系统自带功能等。请尽量详细描述您遇到的问题，您也可以点击下方按钮上传图片或文件以便我们快速解决问题。</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="64"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>您可以在此反馈外设方面的问题，如：外设无法连接、共享功能设置、外设适配等。请尽量详细描述您遇到的问题，您也可以点击下方按钮上传图片或文件以便我们快速解决问题。</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="75"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>您可以在此反馈应用软件方面的问题，如：麒麟相关软件获取、安装与卸载报错等。请尽量详细描述您遇到的问题，您也可以点击下方按钮上传图片或文件以便我们快速解决问题。</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="85"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>您可以在此反馈其他方面的问题或对我们提出宝贵意见。</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral</source>
        <translation>外设咨询</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral adaptation consulting</source>
        <translation>外设适配咨询</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral driver acquisition</source>
        <translation>外设驱动获取</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="60"/>
        <source>Peripheral use and error reporting</source>
        <translation>外设使用与报错</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="70"/>
        <source>Application</source>
        <translation>应用软件</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software installation and uninstallation</source>
        <translation>软件安装与卸载</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software use and error reporting</source>
        <translation>软件使用与报错</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="81"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="83"/>
        <source>Opinions and suggestions</source>
        <translation>意见建议</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="152"/>
        <source>Attachment size exceeds limit!</source>
        <translation>附件大小超过限制</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="70"/>
        <source>Failed to create temporary directory!</source>
        <translation>创建临时目录失败</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>跳至</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>页</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>共</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>页</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>跳至</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>页</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="252"/>
        <source>System log</source>
        <translation>系统日志</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="253"/>
        <source>Machine</source>
        <translation>整机信息</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="254"/>
        <source>Hardware</source>
        <translation>硬件参数</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="255"/>
        <source>Drive</source>
        <translation>驱动信息</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="256"/>
        <source>APP list</source>
        <translation>软件列表</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="257"/>
        <source>Rules</source>
        <translation>终端策略</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="258"/>
        <source>Network</source>
        <translation>网络状态</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="259"/>
        <source>System</source>
        <translation>系统状态</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>服务支持</translation>
    </message>
</context>
<context>
    <name>UIMainPage</name>
    <message>
        <location filename="../UI/uimainpage.cpp" line="93"/>
        <source>ServiceSupport</source>
        <translation>服务支持</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="96"/>
        <source>Multi-channel technical support services</source>
        <translation>多重途径技术支持服务，问题解决和意见反馈一键可达</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="99"/>
        <source>Feedback</source>
        <translation>问题反馈</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="102"/>
        <source>Self service</source>
        <translation>自助服务</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="105"/>
        <source>Online</source>
        <translation>在线客服</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="108"/>
        <source>History</source>
        <translation>反馈历史</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="119"/>
        <source>Jump to</source>
        <translation>点击跳转至</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="121"/>
        <source> KylinOS website</source>
        <translation>KylinOS 官网</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="128"/>
        <source> to get more services</source>
        <translation>，了解更多服务内容</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="180"/>
        <source>Creation time</source>
        <translation>反馈时间</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="181"/>
        <source>Type</source>
        <translation>问题类型</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="85"/>
        <source>bydesign</source>
        <translation>设计如此</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="87"/>
        <source>duplicate</source>
        <translation>重复问题</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="89"/>
        <source>external</source>
        <translation>外部原因</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="91"/>
        <source>fixed</source>
        <translation>已解决</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="93"/>
        <source>notrepro</source>
        <translation>无法重现</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="95"/>
        <source>postponed</source>
        <translation>延期处理</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="97"/>
        <source>willnotfix</source>
        <translation>不予解决</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="114"/>
        <source>in process</source>
        <translation>处理中</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="123"/>
        <source>verify</source>
        <translation>确认解决</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="119"/>
        <source>completed</source>
        <translation>已关闭</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="126"/>
        <source>Has the issue been resolved?</source>
        <translation>请确认该问题是否已解决？</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="129"/>
        <source>Once identified, the issue will be closed and no further action will be taken.</source>
        <translation>确定已解决后，该问题将关闭且技服人员不再继续跟进。</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="131"/>
        <source>resolved</source>
        <translation>解决</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="133"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="182"/>
        <source>Description</source>
        <translation>问题描述</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="184"/>
        <source>Solution</source>
        <translation>处理方案</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="186"/>
        <source>Progress</source>
        <translation>处理进度</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="232"/>
        <source>No record</source>
        <translation>无反馈记录</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="233"/>
        <source>There is a network problem, please try again later</source>
        <translation>与服务端连接失败，请稍后再试</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="234"/>
        <source>Loading, please wait</source>
        <translation>正在加载，请稍等</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="247"/>
        <source>retry</source>
        <translation>重试</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="137"/>
        <source>Advanced</source>
        <translation>高级模式</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="145"/>
        <source>Type</source>
        <translation>问题类型</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="163"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>请详细描述您遇到的问题，您也可以点击下方按钮上传图片或文件以便我们快速解决问题。</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="165"/>
        <source>Remaining</source>
        <translation>剩余</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="166"/>
        <source>character</source>
        <translation>个字符</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="206"/>
        <source>Details</source>
        <translation>问题描述</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="218"/>
        <source>ScreenShot</source>
        <translation>截图</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="234"/>
        <source>Add file</source>
        <translation>添加文件</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="292"/>
        <source>The phone number cannot be empty</source>
        <translation>手机号不能为空</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="294"/>
        <source>The phone number format is incorrect</source>
        <translation>手机号格式错误</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="299"/>
        <source>Please enter your phone number</source>
        <translation>请输入您的手机号</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="305"/>
        <source>appellation</source>
        <translation>称谓</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="313"/>
        <source>Contact</source>
        <translation>联系方式</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="324"/>
        <source>The mailbox format is incorrect</source>
        <translation>邮箱格式不正确</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="334"/>
        <source>Mailbox</source>
        <translation>邮箱</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="343"/>
        <source>Agree to take mine </source>
        <translation>同意获取我的</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="353"/>
        <source>System information</source>
        <translation>系统信息</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="379"/>
        <source>Submit</source>
        <translation>提交</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="396"/>
        <source>Details type</source>
        <translation>详细分类</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="419"/>
        <source>Time period</source>
        <translation>故障时段</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="428"/>
        <source>Job number</source>
        <translation>工号</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="463"/>
        <source>Information</source>
        <translation>系统信息</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="466"/>
        <source>lately</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="467"/>
        <source>days</source>
        <translation>天</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="479"/>
        <source>YES</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="489"/>
        <source>NO</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="511"/>
        <source>Upload log</source>
        <translation>添加日志</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="517"/>
        <source>Path</source>
        <translation>选择路径</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="532"/>
        <source>Export to</source>
        <translation>导出至</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="836"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>附件最多上传 5 个文件，总大小不超过 10 MB</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="837"/>
        <source>Supported formats: </source>
        <translation>支持格式：</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="164"/>
        <source>Up to 500 characters</source>
        <translation>最多输入 500 个字符</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="271"/>
        <source>Files</source>
        <translation>上传附件</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="927"/>
        <source>Repeat addition</source>
        <translation>重复添加</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="931"/>
        <source>文件名包含特殊字符</source>
        <translation>文件名包含特殊字符</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="947"/>
        <source>Attachment size out of limit</source>
        <translation>附件大小超出限制</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="1248"/>
        <source>Add attachment</source>
        <translation>添加附件</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="36"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="44"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="97"/>
        <source>Submitted successfully</source>
        <translation>您的反馈提交成功</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <source>Cancel successfully</source>
        <translation>取消成功</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="105"/>
        <source>System is abnormal, contact technical support</source>
        <translation>检测到您的系统异常，请直接联系技服人员</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>Log and submission is packed, please go</source>
        <translation>日志和提交的信息已打包,请前往</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>acquire</source>
        <translation>获取</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="111"/>
        <source>Submission failed</source>
        <translation>您的反馈提交失败</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="112"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>点击“重试”重新上传，或直接联系技服人员解决。</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="39"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="55"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="134"/>
        <source>Under submission...</source>
        <translation>提交中...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="17"/>
        <source>Contact us</source>
        <translation>联系我们</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="38"/>
        <source>Telephone</source>
        <translation>电话</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="58"/>
        <source>Mail</source>
        <translation>邮件</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="71"/>
        <source>Team</source>
        <translation>服务与支持团队</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="89"/>
        <source> to get more services</source>
        <translation>，了解更多服务内容</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="107"/>
        <source>Kylin technical services</source>
        <translation>公众号麒麟软件技术服务</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="80"/>
        <source>Jump to</source>
        <translation>点击跳转至</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="82"/>
        <source> KylinOS website</source>
        <translation>KylinOS 官网</translation>
    </message>
</context>
<context>
    <name>UiServiceOnline</name>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="41"/>
        <source>Loading, please wait</source>
        <translation>正在加载，请稍等</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="56"/>
        <source>retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="69"/>
        <source>There is a network problem, please try again later</source>
        <translation>网络出问题，请稍后再试</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="86"/>
        <source>Go to your browser</source>
        <translation>请前往浏览器查看</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="87"/>
        <source>Go</source>
        <translation>前往</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="26"/>
        <source>Feedback</source>
        <translation>问题反馈</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="28"/>
        <source>Online</source>
        <translation>在线客服</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="30"/>
        <source>Self service</source>
        <translation>自助服务</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="32"/>
        <source>History</source>
        <translation>反馈历史</translation>
    </message>
</context>
</TS>
