<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="35"/>
        <source>select detailed category</source>
        <translation>請選擇詳細分類</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="39"/>
        <source>System</source>
        <translation>系統使用</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System activation</source>
        <translation>系統啟動</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System installation</source>
        <translation>系統安裝</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System crash</source>
        <translation>系統崩潰</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System performance</source>
        <translation>系統性能</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>Control center</source>
        <translation>控制面板</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="42"/>
        <source>System setting</source>
        <translation>系統設置</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="43"/>
        <source>System basis consulting</source>
        <translation>系統基礎諮詢</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="51"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>您可以在此反饋系統使用方面的問題，如：無法激活系統、找不到相關設置、不清楚系統自帶功能等。 請盡量詳細描述您遇到的問題，您也可以點擊下方按鈕上傳圖片或檔以便我們快速解決問題。 </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="64"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>您可以在此反饋外設方面的問題，如：外設無法連接、共用功能設置、外設適配等。 請盡量詳細描述您遇到的問題，您也可以點擊下方按鈕上傳圖片或檔以便我們快速解決問題。 </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="75"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>您可以在此反饋應用軟體方面的問題，如：麒麟相關軟體獲取、安裝與卸載報錯等。 請盡量詳細描述您遇到的問題，您也可以點擊下方按鈕上傳圖片或檔以便我們快速解決問題。 </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="85"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>您可以在此反饋其他方面的問題或對我們提出寶貴意見。</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral</source>
        <translation>外設諮詢</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral adaptation consulting</source>
        <translation>外設適配諮詢</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="59"/>
        <source>Peripheral driver acquisition</source>
        <translation>外設驅動獲取</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="60"/>
        <source>Peripheral use and error reporting</source>
        <translation>外設使用與報錯</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="70"/>
        <source>Application</source>
        <translation>應用軟體</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software installation and uninstallation</source>
        <translation>軟體安裝與卸載</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="72"/>
        <source>Software use and error reporting</source>
        <translation>軟體使用與報錯</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="81"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="83"/>
        <source>Opinions and suggestions</source>
        <translation>意見建議</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="152"/>
        <source>Attachment size exceeds limit!</source>
        <translation>附件大小超過限制</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="70"/>
        <source>Failed to create temporary directory!</source>
        <translation>創建臨時目錄失敗</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>跳至</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>頁</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>共</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>頁</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>跳至</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>頁</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="252"/>
        <source>System log</source>
        <translation>系統記錄</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="253"/>
        <source>Machine</source>
        <translation>整機資訊</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="254"/>
        <source>Hardware</source>
        <translation>硬體參數</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="255"/>
        <source>Drive</source>
        <translation>驅動資訊</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="256"/>
        <source>APP list</source>
        <translation>軟體清單</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="257"/>
        <source>Rules</source>
        <translation>終端策略</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="258"/>
        <source>Network</source>
        <translation>網路狀態</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="259"/>
        <source>System</source>
        <translation>系統狀態</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>服務支援</translation>
    </message>
</context>
<context>
    <name>UIMainPage</name>
    <message>
        <location filename="../UI/uimainpage.cpp" line="93"/>
        <source>ServiceSupport</source>
        <translation>服務支援</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="96"/>
        <source>Multi-channel technical support services</source>
        <translation>多重途徑技術支援服務，問題解決和意見反饋一鍵可達</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="99"/>
        <source>Feedback</source>
        <translation>問題反饋</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="102"/>
        <source>Self service</source>
        <translation>自助服務</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="105"/>
        <source>Online</source>
        <translation>在線客服</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="108"/>
        <source>History</source>
        <translation>反饋歷史</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="119"/>
        <source>Jump to</source>
        <translation>點擊跳轉至</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="121"/>
        <source> KylinOS website</source>
        <translation> KylinOS 官網</translation>
    </message>
    <message>
        <location filename="../UI/uimainpage.cpp" line="128"/>
        <source> to get more services</source>
        <translation> ，瞭解更多服務內容</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="180"/>
        <source>Creation time</source>
        <translation>反饋時間</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="181"/>
        <source>Type</source>
        <translation>問題類型</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="85"/>
        <source>bydesign</source>
        <translation>設計如此</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="87"/>
        <source>duplicate</source>
        <translation>重複問題</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="89"/>
        <source>external</source>
        <translation>外部原因</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="91"/>
        <source>fixed</source>
        <translation>已解決</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="93"/>
        <source>notrepro</source>
        <translation>無法重現</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="95"/>
        <source>postponed</source>
        <translation>延期處理</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="97"/>
        <source>willnotfix</source>
        <translation>不予解決</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="114"/>
        <source>in process</source>
        <translation>處理中</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="123"/>
        <source>verify</source>
        <translation>確認解決</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="119"/>
        <source>completed</source>
        <translation>已關閉</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="126"/>
        <source>Has the issue been resolved?</source>
        <translation>請確認該問題是否已解決？</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="129"/>
        <source>Once identified, the issue will be closed and no further action will be taken.</source>
        <translation>確定已解決后，該問題將關閉且技服人員不再繼續跟進。</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="131"/>
        <source>resolved</source>
        <translation>解決</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="133"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="182"/>
        <source>Description</source>
        <translation>問題描述</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="184"/>
        <source>Solution</source>
        <translation>處理方案</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="186"/>
        <source>Progress</source>
        <translation>處理進度</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="232"/>
        <source>No record</source>
        <translation>無反饋記錄</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="233"/>
        <source>There is a network problem, please try again later</source>
        <translation>與服務端連接失敗，請稍後再試</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="234"/>
        <source>Loading, please wait</source>
        <translation>正在載入，請稍等</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="247"/>
        <source>retry</source>
        <translation>重試</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="137"/>
        <source>Advanced</source>
        <translation>高級模式</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="145"/>
        <source>Type</source>
        <translation>問題類型</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="163"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>請詳細描述您遇到的問題，您也可以點擊下方按鈕上傳圖片或檔以便我們快速解決問題。</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="165"/>
        <source>Remaining</source>
        <translation>剩餘</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="166"/>
        <source>character</source>
        <translation>個字元</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="206"/>
        <source>Details</source>
        <translation>問題描述</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="218"/>
        <source>ScreenShot</source>
        <translation>截圖</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="234"/>
        <source>Add file</source>
        <translation>添加檔</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="292"/>
        <source>The phone number cannot be empty</source>
        <translation>手機號不能為空</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="294"/>
        <source>The phone number format is incorrect</source>
        <translation>手機號格式錯誤</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="299"/>
        <source>Please enter your phone number</source>
        <translation>請輸入您的手機號</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="305"/>
        <source>appellation</source>
        <translation>稱謂</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="313"/>
        <source>Contact</source>
        <translation>聯繫方式</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="324"/>
        <source>The mailbox format is incorrect</source>
        <translation>郵箱格式不正確</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="334"/>
        <source>Mailbox</source>
        <translation>郵箱</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="343"/>
        <source>Agree to take mine </source>
        <translation>同意獲取我的 </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="353"/>
        <source>System information</source>
        <translation>系統資訊</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="379"/>
        <source>Submit</source>
        <translation>提交</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="396"/>
        <source>Details type</source>
        <translation>詳細分類</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="419"/>
        <source>Time period</source>
        <translation>故障時段</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="428"/>
        <source>Job number</source>
        <translation>工號</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="463"/>
        <source>Information</source>
        <translation>系統資訊</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="466"/>
        <source>lately</source>
        <translation>最近</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="467"/>
        <source>days</source>
        <translation>天</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="479"/>
        <source>YES</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="489"/>
        <source>NO</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="511"/>
        <source>Upload log</source>
        <translation>添加紀錄</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="517"/>
        <source>Path</source>
        <translation>選擇路徑</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="532"/>
        <source>Export to</source>
        <translation>匯出至</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="836"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>附件最多上傳 5 個檔，總大小不超過 10 MB</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="837"/>
        <source>Supported formats: </source>
        <translation>支援格式： </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="164"/>
        <source>Up to 500 characters</source>
        <translation>最多輸入 500 個字元</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="271"/>
        <source>Files</source>
        <translation>上傳附件</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="927"/>
        <source>Repeat addition</source>
        <translation>重複添加</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="931"/>
        <source>文件名包含特殊字符</source>
        <translation>檔名包含特殊字元</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="947"/>
        <source>Attachment size out of limit</source>
        <translation>附件大小超出限制</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="1248"/>
        <source>Add attachment</source>
        <translation>添加附件</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="36"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="44"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="97"/>
        <source>Submitted successfully</source>
        <translation>您的反饋提交成功</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="101"/>
        <source>Cancel successfully</source>
        <translation>取消成功</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="105"/>
        <source>System is abnormal, contact technical support</source>
        <translation>檢測到您的系統異常，請直接聯繫技服人員</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>Log and submission is packed, please go</source>
        <translation>日誌和提交的資訊已打包，請前往</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="106"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>acquire</source>
        <translation>獲取</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="111"/>
        <source>Submission failed</source>
        <translation>您的反饋提交失敗</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="112"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>點擊「重試」重新上傳，或直接聯繫技服人員解決。</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="39"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="55"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="134"/>
        <source>Under submission...</source>
        <translation>提交中...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="17"/>
        <source>Contact us</source>
        <translation>聯繫我們</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="38"/>
        <source>Telephone</source>
        <translation>電話</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="58"/>
        <source>Mail</source>
        <translation>郵件</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="71"/>
        <source>Team</source>
        <translation>服務與支持團隊</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="89"/>
        <source> to get more services</source>
        <translation> ，瞭解更多服務內容</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="107"/>
        <source>Kylin technical services</source>
        <translation>公眾號麒麟軟體技術服務</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="80"/>
        <source>Jump to</source>
        <translation>點擊跳轉至</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="82"/>
        <source> KylinOS website</source>
        <translation> KylinOS 官網</translation>
    </message>
</context>
<context>
    <name>UiServiceOnline</name>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="41"/>
        <source>Loading, please wait</source>
        <translation>正在載入，請稍等</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="56"/>
        <source>retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="69"/>
        <source>There is a network problem, please try again later</source>
        <translation>網路出問題，請稍後再試</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="86"/>
        <source>Go to your browser</source>
        <translation>請前往瀏覽器查看</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="87"/>
        <source>Go</source>
        <translation>前往</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="26"/>
        <source>Feedback</source>
        <translation>問題反饋</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="28"/>
        <source>Online</source>
        <translation>在線客服</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="30"/>
        <source>Self service</source>
        <translation>自助服務</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="32"/>
        <source>History</source>
        <translation>反饋歷史</translation>
    </message>
</context>
</TS>
