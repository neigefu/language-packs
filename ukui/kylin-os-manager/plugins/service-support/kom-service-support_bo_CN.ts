<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="34"/>
        <source>select detailed category</source>
        <translation>ཞིབ་ཕྲའི་རིགས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="38"/>
        <source>System</source>
        <translation>ལམ་ལུགས།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System activation</source>
        <translation>མ་ལག་གི་འགུལ་སྐྱོད།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System installation</source>
        <translation>མ་ལག་སྒྲིག་སྦྱོར་</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System crash</source>
        <translation>མ་ལག་ཐོར་ཞིག་ཏུ་</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System performance</source>
        <translation>མ་ལག་གི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>Control center</source>
        <translation>ཚོད་འཛིན་ལྟེ་གནས།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System setting</source>
        <translation>མ་ལག་སྒྲིག་གཞི།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System basis consulting</source>
        <translation>ལམ་ལུགས་ཀྱི་རྨང་གཞིའི་བློ་འདྲི།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="49"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>རོགས་།ཁྱེད་རང་ལ་འཕྲད་པའི་གནད་དོན་ཞིབ་ཏུ་བརྗོད་དང་།དཔེར་ན་།ཁ་འབྱེད་མ་ཐུབ་།མ་ལག་དང་།འབྲེལ་ཡོད་བཀོད་སྒྲིག་དང་།མ་ལག་གི་ནུས་པ་མི་གསལ་བ་སོགས་རེད་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="61"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>རོགས་།ཁྱེད་རང་ལ་འཕྲད་པའི་གནད་དོན་ཞིབ་ཏུ་བརྗོད་དང་།དཔེར་ན་།ཕྱིའི་སྦྲེལ་མཐུད་ཕམ་དང་།མཉམ་སྤྱོད་བྱེད་ལས་སྒྲིག་འགོད།ཕྱི་བཀོད་འཚམ་སྦྱོར་སོགས་བྱེད་ཀྱི་ཡོད་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="71"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>རོགས་།ཁྱེད་རང་ལ་འཕྲད་པའི་གནད་དོན་ཞིབ་ཕྲ་བརྗོད་དང་།དཔེར་ན་ཐོབ་ལེན་དང་།སྒྲིག་སྦྱོར་དང་བཤིག་འདོན་ཆི་ལིན་མཉེན་ཆས་ནོར་འཁྲུལ་རེད་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="80"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>རོགས་།ཁྱེད་རང་གི་གནད་དོན་ཞིབ་ཏུ་བརྗོད་དང་།ཡང་ན་ཁྱེད་རང་གི་འདིར་བྲིས་ཀྱང་ཆོག་ཁྱེད་རང་གི་རེ་བའམ་ཡང་ན་དཔྱད་གཏམ་།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="54"/>
        <source>Peripheral</source>
        <translation>མཐའ་སྐོར།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral adaptation consulting</source>
        <translation>མཐའ་འཁོར་གྱི་མཐུན་འཕྲོད་བློ་འདྲི།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral driver acquisition</source>
        <translation>མཐའ་སྐོར་ཁ་ལོ་བའི་ཉོ་སྒྲུབ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral use and error reporting</source>
        <translation>མཐའ་འཁོར་གྱི་བཀོལ་སྤྱོད་དང་ནོར་འཁྲུལ་གྱི་སྙན་ཞུ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="66"/>
        <source>Application</source>
        <translation>རེ་འདུན་ཞུ་ཡིག</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software installation and uninstallation</source>
        <translation>མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software use and error reporting</source>
        <translation>མཉེན་ཆས་བཀོལ་སྤྱོད་དང་ནོར་འཁྲུལ་གྱི་སྙན་ཞུ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="76"/>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="78"/>
        <source>Opinions and suggestions</source>
        <translation>བསམ་འཆར་དང་གྲོས་འགོ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="129"/>
        <source>Title and question details cannot be blank!</source>
        <translation>ཁ་བྱང་དང་གནད་དོན་ཞིབ་ཕྲ་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="139"/>
        <source>Attachment size exceeds limit!</source>
        <translation>ཟུར་གཏོགས་ཀྱི་གཞི་ཁྱོན་ཚད་ལས་བརྒལ་ཡོད།</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="62"/>
        <source>Failed to create temporary directory!</source>
        <translation>གནས་སྐབས་ཀྱི་དཀར་ཆག་གསར་སྐྲུན་བྱེད་མ་ཐུབ་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>ཁྱོན་བསྡོམས</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>ཤོག་ངོས།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="180"/>
        <source>System log</source>
        <translation>མ་ལག་གི་ཟིན་ཐོ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="181"/>
        <source>Machine</source>
        <translation>འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="182"/>
        <source>Hardware</source>
        <translation>མཁྲེགས་ཆས།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="183"/>
        <source>Drive</source>
        <translation>སྒུལ་ཤུགས་</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="184"/>
        <source>APP list</source>
        <translation>APPཡི་མིང་ཐོ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="185"/>
        <source>Rules</source>
        <translation>སྒྲིག་སྲོལ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="186"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="187"/>
        <source>System</source>
        <translation>ལམ་ལུགས།</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>ཞབས་ཞུའི་ལས་རིགས་ཀྱི་རྒྱབ་ཕྱོགས་མཁོ་འདོན</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Creation time</source>
        <translation>གསར་རྩོམ་བྱེད་པའི་དུས་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="64"/>
        <source>bydesign</source>
        <translation>ཇུས་འགོད་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="66"/>
        <source>duplicate</source>
        <translation>འདྲ་བཟོ་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="68"/>
        <source>external</source>
        <translation>ཕྱི་རོལ།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="70"/>
        <source>fixed</source>
        <translation>གཏན་འཇགས་ཅན།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="72"/>
        <source>notrepro</source>
        <translation>ནོར་ཆུ་ཕུ་རོ་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="74"/>
        <source>postponed</source>
        <translation>དུས་འགྱངས་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="76"/>
        <source>willnotfix</source>
        <translation>ཐག་མ་ཆོད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="86"/>
        <source>in process</source>
        <translation>བརྒྱུད་རིམ་ཁྲོད།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="97"/>
        <source>verify</source>
        <translation>ར་སྤྲོད་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="91"/>
        <source>completed</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Description</source>
        <translation>གསལ་བཤད་ཡི་གེ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Solution</source>
        <translation>ཐག་གཅོད་ཇུས་གཞི་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Progress</source>
        <translation>ཡར་ཐོན།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>No record</source>
        <translation>ཟིན་ཐོ་མེད་པ་</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="156"/>
        <source>There is a network problem, please try again later</source>
        <translation>དྲ་རྒྱའི་གནད་དོན་ཡོད་པས་རྗེས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="157"/>
        <source>Loading, please wait</source>
        <translation>དངོས་ཟོག་བླུགས་ནས་སྒུག་དང་།</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="170"/>
        <source>retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="108"/>
        <source>Advanced</source>
        <translation>སྔོན་ཐོན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="113"/>
        <source>Type</source>
        <translation>རིགས་དབྱིབས་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="130"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>རོགས་།ཞིབ་ཕྲའི་ངང་གནད་དོན་གསལ་བཤད་དང་།ཁྱེད་རང་གིས་གནོན་འོག་གི་མཐེབ་གནོན་སྟེང་དུ་བསྐུར་འདྲ་པར་དང་ཡིག་ཆ་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Remaining</source>
        <translation>དེ་བྱིངས་ཚང་མ་ལྷག</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>character</source>
        <translation>གཤིས་ཀ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="168"/>
        <source>Details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="175"/>
        <source>ScreenShot</source>
        <translation>བརྙན་ཤེལ་གྱི་པར་རིས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="190"/>
        <source>Add file</source>
        <translation>ཡིག་ཆ་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="220"/>
        <source>Attachments</source>
        <translation>ཟུར་གཏོགས་དངོས་པོ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="241"/>
        <source>The phone number cannot be empty</source>
        <translation>ཁ་པར་ཨང་གྲངས་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="243"/>
        <source>The phone number format is incorrect</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཀྱི་རྣམ་གཞག་ཡང་དག་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="248"/>
        <source>Please enter your phone number</source>
        <translation>ཁྱེད་ཀྱི་ཁ་པར་ཨང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="254"/>
        <source>appellation</source>
        <translation>གོང་གཏུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="262"/>
        <source>Contact</source>
        <translation>འབྲེལ་གཏུག་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="273"/>
        <source>The mailbox format is incorrect</source>
        <translation>ཡིག་སྒམ་གྱི་རྣམ་གཞག་ཡང་དག་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Mailbox</source>
        <translation>ཡིག་སྒམ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="292"/>
        <source>Agree to take mine </source>
        <translation>ངའི་ཁ་ཆད་དང་ལེན་བྱེད་པར་འཐད་ </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="302"/>
        <source>System information</source>
        <translation>མ་ལག་གི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="328"/>
        <source>Submit</source>
        <translation>གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="345"/>
        <source>Details type</source>
        <translation>ཞིབ་ཕྲའི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="360"/>
        <source>Time period</source>
        <translation>དུས་ཚོད་ཀྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="369"/>
        <source>Job number</source>
        <translation>ལས་ཀའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="404"/>
        <source>Information</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="407"/>
        <source>lately</source>
        <translation>ཉེ་ཆར།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="408"/>
        <source>days</source>
        <translation>ཉིན་མོ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="420"/>
        <source>YES</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="430"/>
        <source>NO</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="452"/>
        <source>Upload log</source>
        <translation>ཉིན་ཐོ་ཕབ་ལེན་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="458"/>
        <source>Path</source>
        <translation>འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="472"/>
        <source>Export to</source>
        <translation>ཕྱིར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="548"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>ཡིག་ཚགས་5ལས་བརྒལ་མི་ཆོག་པ་དང་། སྤྱིའི་ཤོང་ཚད་10MBལས་མི་བརྒལ་བ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="549"/>
        <source>Supported formats: </source>
        <translation>རྒྱབ་སྐྱོར་གྱི་རྣམ་གཞག་གཤམ་གསལ། </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="131"/>
        <source>Up to 500 characters</source>
        <translation>ཆེས་མང་ན་ཡི་གེ་500ཡོད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="626"/>
        <source>Repeat addition</source>
        <translation>ཡང་བསྐྱར་ཁ་སྣོན་བྱས་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="642"/>
        <source>Attachment size out of limit</source>
        <translation>ཟུར་གཏོགས་དངོས་པོའི་ཆེ་ཆུང་ཚད་ལས་བརྒལ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="808"/>
        <source>Add attachment</source>
        <translation>ཟུར་གཏོགས་དངོས་པོ་ཁ་སྣོན་</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="34"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="37"/>
        <source>Retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="79"/>
        <source>Submitted successfully</source>
        <translation>ལེགས་གྲུབ་བྱུང་བའི་སྒོ་ནས་ཕུལ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="83"/>
        <source>Cancel successfully</source>
        <translation>རྒྱལ་ཁ་མེད་པར་བཟོས།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="87"/>
        <source>System is abnormal, contact technical support</source>
        <translation>རྒྱུད་ཁོངས་རྒྱུན་འགལ་དང་།འབྲེལ་གཏུག་ལག་རྩལ་རྒྱབ་སྐྱོར་རོགས་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Log and submission is packed, please go</source>
        <translation>ཉིན་ཐོ་དང་འདོན་སྤྲོད་བྱས་ཟིན་ཐུམ་བརྒྱབ་ནས་རོགས་།ཁ་བྲལ་།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="95"/>
        <source>acquire</source>
        <translation>ཐོབ་པ་</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="93"/>
        <source>Submission failed</source>
        <translation>ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>“ཡང་བསྐྱར་ཚོད་ལེན”མནན་ནས་ཡང་བསྐྱར་བརྒྱུད་བསྐུར་བྱེད་པ།ཡང་ན་ཐད་ཀར་ང་ཚོ་དང་འབྲེལ་བ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>Under submission...</source>
        <translation>སྙན་ཞུ་ཕུལ་བའི་འོག་</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="17"/>
        <source>Contact us</source>
        <translation>ང་ཚོ་དང་འབྲེལ་གཏུག་བྱེད</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="37"/>
        <source>Telephone</source>
        <translation>ཁ་པར།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="50"/>
        <source>Mail</source>
        <translation>སྦྲག་རྫས།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="63"/>
        <source>Team</source>
        <translation>རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="80"/>
        <source> to get more services</source>
        <translation> དེ་བས་མང་བ་ཐོབ་།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="98"/>
        <source>Kylin technical services</source>
        <translation>ཅིན་ལིན་ལག་རྩལ་ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="72"/>
        <source>Jump to</source>
        <translation>མཆོང་ལྡིང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="73"/>
        <source> KylinOS website</source>
        <translation> ཅིན་ལིན་ནོ་སི་དྲ་ཚིགས།</translation>
    </message>
</context>
<context>
    <name>UiServiceOnline</name>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="40"/>
        <source>Loading, please wait</source>
        <translation>དངོས་ཟོག་བླུགས་ནས་སྒུག་དང་།</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="54"/>
        <source>retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="67"/>
        <source>There is a network problem, please try again later</source>
        <translation>དྲ་རྒྱའི་གནད་དོན་ཡོད་པས་རྗེས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="83"/>
        <source>Go to your browser</source>
        <translation>ཀོ་ཐོ·གཡོད་ཨེར་པུའུ་ལའོ་སེར།</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="84"/>
        <source>Go</source>
        <translation>ཀུའོ་</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="19"/>
        <source>Feedback</source>
        <translation>ལྡོག་འདྲེན་བསམ་འཆར།</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="20"/>
        <source>Online</source>
        <translation>དྲ་ཐོག</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="21"/>
        <source>Self service</source>
        <translation>རང་གིས་རང་ལ་ཞབས་འདེགས</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="22"/>
        <source>History</source>
        <translation>ལོ་རྒྱུས།</translation>
    </message>
</context>
</TS>
