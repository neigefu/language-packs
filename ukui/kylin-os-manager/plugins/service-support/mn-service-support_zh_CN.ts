<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="34"/>
        <source>select detailed category</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠯ ᠢ᠋ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="38"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System activation</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System installation</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤᠭᠰᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System crash</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠭᠠᠯᠵᠠᠭᠤᠷᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System performance</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠴᠢᠳᠠᠪᠬᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>Control center</source>
        <translation>ᠡᠵᠡᠮᠳᠡᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System setting</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System basis consulting</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠠᠭᠤᠷᠢ ᠯᠠᠪᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="49"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠬᠢ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠢᠳᠡᠪᠬᠢᠵᠢᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ᠂ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠡᠷᠢᠵᠤ ᠣᠯᠬᠤ ᠥᠬᠡᠢ᠂ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠤᠪᠢ ᠵᠢᠨ ᠴᠢᠳᠠᠪᠬᠢ ᠳᠤᠳᠤᠷᠬᠠᠢ ᠥᠬᠡᠢ ᠬᠡᠬᠦ ᠮᠡᠳᠦ ᠪᠡᠷ᠃ ᠲᠠ ᠳᠤᠬᠢᠶᠠᠯᠳᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠵᠤ᠂ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ. </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="61"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠬᠢ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠴᠥᠷᠬᠡᠯᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ᠂ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠴᠢᠳᠠᠪᠬᠢ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ᠂ ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠠᠪᠴᠠᠯᠳᠤᠯ ᠬᠡᠬᠦ ᠮᠡᠳᠦ ᠪᠡᠷ᠃ ᠲᠠ ᠳᠤᠬᠢᠶᠠᠯᠳᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠵᠤ᠂ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ. </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="71"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠡᠨ ᠤ᠋ ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠬᠢ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠵᠢᠱ᠌ᠢᠶᠡᠯᠡᠪᠡᠯ ᠴᠢ ᠯᠢᠨ ᠤ᠋ ᠬᠠᠮᠢᠶ᠎ᠠ ᠪᠦᠬᠦᠢ ᠰᠣᠹᠲ ᠢ᠋ ᠣᠯᠵᠠᠯᠠᠬᠤ᠂ ᠤᠭᠰᠠᠷᠬᠤ᠂ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠳ᠋ᠤ᠌ ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠬᠡᠬᠦ ᠮᠡᠳᠦ ᠪᠡᠷ᠃ ᠲᠠ ᠳᠤᠬᠢᠶᠠᠯᠳᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠵᠤ᠂ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ. </translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="80"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>ᠲᠠ ᠡᠨᠳᠡ ᠪᠤᠰᠤᠳ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠬᠤ ᠪᠤᠶᠤ ᠪᠢᠳᠡᠨ ᠳ᠋ᠤ᠌ ᠡᠷᠬᠢᠮ ᠰᠠᠨᠠᠯ ᠵᠦᠪᠯᠡᠯᠬᠡ ᠪᠡᠨ ᠦᠭᠴᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="54"/>
        <source>Peripheral</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠯᠠᠪᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral adaptation consulting</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠠᠪᠴᠠᠯᠳᠤᠯ ᠤ᠋ᠨ ᠯᠠᠪᠯᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral driver acquisition</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ᠋ ᠣᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral use and error reporting</source>
        <translation>ᠭᠠᠳᠠᠷ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠬᠢᠭᠡᠳ ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="66"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠰᠣᠹᠲ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software installation and uninstallation</source>
        <translation>ᠰᠣᠹᠲ ᠤᠭᠰᠠᠷᠬᠤ ᠬᠢᠬᠡᠳ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software use and error reporting</source>
        <translation>ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠬᠢᠬᠡᠳ ᠪᠤᠷᠤᠭᠤᠳᠠᠭᠰᠠᠨ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="76"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="78"/>
        <source>Opinions and suggestions</source>
        <translation>ᠰᠠᠨᠠᠯ ᠵᠥᠪᠯᠡᠯᠭᠡ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="129"/>
        <source>Title and question details cannot be blank!</source>
        <translation>ᠭᠠᠷᠴᠠᠭ ᠬᠢᠬᠡᠳ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠡᠢ ᠠᠭᠤᠯᠭ᠎ᠠ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="139"/>
        <source>Attachment size exceeds limit!</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠢᠵᠠᠭᠠᠷ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ!</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="62"/>
        <source>Failed to create temporary directory!</source>
        <translation>ᠲᠦᠷ ᠴᠠᠭ ᠤ᠋ᠨ ᠭᠠᠷᠴᠠᠭ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>ᠦᠰᠦᠷᠦᠭᠡᠳ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>ᠨᠡᠢᠲᠡ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>ᠦᠰᠦᠷᠦᠭᠡᠳ</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="180"/>
        <source>System log</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="181"/>
        <source>Machine</source>
        <translation>ᠪᠦᠳᠦᠨ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="182"/>
        <source>Hardware</source>
        <translation>ᠬᠠᠷᠳ᠋ᠸᠠᠢᠷ ᠤ᠋ᠨ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="183"/>
        <source>Drive</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="184"/>
        <source>APP list</source>
        <translation>ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="185"/>
        <source>Rules</source>
        <translation>ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠠᠷᠭ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="186"/>
        <source>Network</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="187"/>
        <source>System</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠪᠠᠢᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Creation time</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Type</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠲᠥᠷᠥᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="64"/>
        <source>bydesign</source>
        <translation>ᠵᠢᠷᠤᠭ ᠳᠦᠯᠦᠪᠯᠡᠬᠡ ᠡᠨᠡ ᠮᠡᠳᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="66"/>
        <source>duplicate</source>
        <translation>ᠳᠠᠪᠳᠠᠭᠳᠠᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="68"/>
        <source>external</source>
        <translation>ᠭᠠᠳᠠᠷ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠰᠢᠯᠳᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="70"/>
        <source>fixed</source>
        <translation>ᠨᠢᠭᠡᠨᠳᠡ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="72"/>
        <source>notrepro</source>
        <translation>ᠳᠠᠬᠢᠨ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="74"/>
        <source>postponed</source>
        <translation>ᠬᠤᠢᠰᠢᠯᠠᠭᠤᠯᠵᠤ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="76"/>
        <source>willnotfix</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠤ ᠥᠭᠬᠦ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="86"/>
        <source>in process</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="97"/>
        <source>verify</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="91"/>
        <source>completed</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Description</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠲᠣᠭᠠᠴᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Solution</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳᠦᠰᠦᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Progress</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠠᠬᠢᠴᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>No record</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠭᠰᠠᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="156"/>
        <source>There is a network problem, please try again later</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠭᠠᠷᠪᠠ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="157"/>
        <source>Loading, please wait</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="170"/>
        <source>retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="108"/>
        <source>Advanced</source>
        <translation>ᠥᠨᠳᠥᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="113"/>
        <source>Type</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="130"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>ᠲᠠᠨ ᠳ᠋ᠤ᠌ ᠳᠤᠯᠠᠭᠠᠷᠠᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠵᠢᠨᠨ ᠨᠠᠷᠢᠯᠢᠭ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠭᠠᠷᠠᠢ᠂ ᠲᠠ ᠪᠠᠰᠠ ᠳᠤᠤᠷᠠᠬᠢ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠵᠢᠷᠤᠭ ᠪᠤᠶᠤ ᠹᠠᠢᠯ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠵᠤ᠂ ᠪᠢᠳᠡᠨ ᠤ᠋ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠦᠵᠡᠬᠦᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Remaining</source>
        <translation>ᠦᠯᠡᠳᠡᠵᠤ ᠪᠤᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>character</source>
        <translation>ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="168"/>
        <source>Details</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠳᠠᠢᠯᠪᠤᠷᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="175"/>
        <source>ScreenShot</source>
        <translation>ᠵᠢᠷᠤᠭᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="190"/>
        <source>Add file</source>
        <translation>ᠹᠠᠢᠯ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="220"/>
        <source>Attachments</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="241"/>
        <source>The phone number cannot be empty</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠬᠣᠭᠣᠰᠣᠨ ᠪᠠᠢᠬᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="243"/>
        <source>The phone number format is incorrect</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="248"/>
        <source>Please enter your phone number</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="254"/>
        <source>appellation</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="262"/>
        <source>Contact</source>
        <translation>ᠬᠠᠷᠢᠯᠴᠠᠬᠤ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="273"/>
        <source>The mailbox format is incorrect</source>
        <translation>ᠢᠮᠸᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Mailbox</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="292"/>
        <source>Agree to take mine </source>
        <translation>ᠮᠢᠨᠦᠬᠢ ᠵᠢ ᠣᠯᠬᠤ ᠵᠢ ᠵᠥᠪᠰᠢᠶᠡᠷᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="302"/>
        <source>System information</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="328"/>
        <source>Submit</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="345"/>
        <source>Details type</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠬᠤᠪᠢᠶᠠᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="360"/>
        <source>Time period</source>
        <translation>ᠴᠠᠭ ᠬᠤᠭᠤᠴᠠᠭᠠᠨ ᠤ᠋ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="369"/>
        <source>Job number</source>
        <translation>ᠠᠵᠢᠯᠳᠠᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="404"/>
        <source>Information</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="407"/>
        <source>lately</source>
        <translation>ᠰᠠᠶᠢᠬᠠᠨ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="408"/>
        <source>days</source>
        <translation>ᠡᠳᠦᠷ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="420"/>
        <source>YES</source>
        <translation>ᠳᠡᠢᠮᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="430"/>
        <source>NO</source>
        <translation>ᠪᠢᠰᠢ / ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="452"/>
        <source>Upload log</source>
        <translation>ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠣᠷᠣᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="458"/>
        <source>Path</source>
        <translation>ᠵᠢᠮ ᠰᠣᠩᠭᠣᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="472"/>
        <source>Export to</source>
        <translation>ᠭᠠᠷᠭᠠᠭᠠᠳ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="548"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠢ᠋ ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 5 ᠹᠠᠢᠯ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ᠂ ᠨᠡᠢᠳᠡ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ 10MB ᠡᠴᠡ ᠳᠠᠪᠠᠵᠤ ᠪᠣᠯᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="549"/>
        <source>Supported formats: </source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠵᠠᠭᠪᠤᠷ: </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="131"/>
        <source>Up to 500 characters</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠣᠯᠠᠨ ᠳ᠋ᠤ᠌ ᠪᠡᠨ 500 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠣᠷᠣᠭᠤᠯᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="626"/>
        <source>Repeat addition</source>
        <translation>ᠳᠠᠪᠬᠤᠴᠠᠵᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="642"/>
        <source>Attachment size out of limit</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠤ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯᠳᠠ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="808"/>
        <source>Add attachment</source>
        <translation>ᠳᠠᠭᠠᠯᠳᠠ ᠮᠠᠲᠸᠷᠢᠶᠠᠯ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="34"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="37"/>
        <source>Retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="79"/>
        <source>Submitted successfully</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠢ᠋ ᠳᠤᠰᠢᠶᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="83"/>
        <source>Cancel successfully</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="87"/>
        <source>System is abnormal, contact technical support</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠰᠢᠰᠲ᠋ᠧᠮ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠠᠢᠬᠤ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠮᠡᠳᠡᠪᠡ᠂ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠥᠮᠦᠰ ᠲᠠᠢ ᠰᠢᠭ᠋ᠤᠳ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Log and submission is packed, please go</source>
        <translation>ᠡᠳᠦᠷ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠪᠤᠯᠤᠨ ᠳᠤᠰᠢᠶᠠᠭᠰᠠᠨ ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ ᠵᠢ ᠪᠣᠭᠣᠳᠠᠯᠯᠠᠭᠰᠠᠨ᠂ ᠣᠴᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="95"/>
        <source>acquire</source>
        <translation>ᠣᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="93"/>
        <source>Submission failed</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠢ᠋ ᠳᠤᠰᠢᠶᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>&apos; ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ&apos; ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠳᠠᠬᠢᠨ ᠣᠷᠣᠭᠤᠯᠤᠭᠠᠷᠠᠢ᠂ ᠡᠰᠡᠪᠡᠯ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡᠨ ᠤ᠋ ᠬᠥᠮᠦᠰ ᠲᠠᠢ ᠰᠢᠭ᠋ᠤᠳ ᠬᠠᠷᠢᠯᠴᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>Under submission...</source>
        <translation>ᠳᠤᠰᠢᠶᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="17"/>
        <source>Contact us</source>
        <translation>ᠪᠢᠳᠡᠨ ᠲᠠᠢ ᠬᠠᠷᠢᠯᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="37"/>
        <source>Telephone</source>
        <translation>ᠳ᠋ᠢᠶᠠᠩᠬᠤᠸᠠ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="50"/>
        <source>Mail</source>
        <translation>ᠢᠮᠸᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="63"/>
        <source>Team</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠬᠢᠬᠡᠳ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="80"/>
        <source> to get more services</source>
        <translation> ᠨᠡᠩ ᠣᠯᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡᠨ ᠤ᠋ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠤᠢᠯᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="98"/>
        <source>Kylin technical services</source>
        <translation>ᠣᠯᠠᠨ ᠨᠡᠢᠲᠡ ᠵᠢᠨ ᠳᠠᠪᠴᠠᠩ ᠴᠢ ᠯᠢᠨ ᠰᠣᠹᠲ ᠤ᠋ᠨ ᠮᠡᠷᠬᠡᠵᠢᠯ ᠤ᠋ᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="72"/>
        <source>Jump to</source>
        <translation>ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠦᠰᠦᠷᠦᠬᠡᠳ</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="73"/>
        <source> KylinOS website</source>
        <translation> KylinOS ᠠᠯᠪᠠᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>UiServiceOnline</name>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="40"/>
        <source>Loading, please wait</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="54"/>
        <source>retry</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="67"/>
        <source>There is a network problem, please try again later</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠲᠠᠢ᠂ ᠤᠳᠠᠰᠬᠢᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="83"/>
        <source>Go to your browser</source>
        <translation>ᠤᠷᠤᠭᠰᠢ ᠣᠴᠢᠵᠤ ᠦᠵᠡᠵᠤ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="84"/>
        <source>Go</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="19"/>
        <source>Feedback</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠭᠤ ᠲᠤᠰᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="20"/>
        <source>Online</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="21"/>
        <source>Self service</source>
        <translation>ᠦᠪᠡᠷᠳᠡᠭᠡᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="22"/>
        <source>History</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠤ᠋ᠨ ᠳᠡᠤᠬᠡ</translation>
    </message>
</context>
</TS>
