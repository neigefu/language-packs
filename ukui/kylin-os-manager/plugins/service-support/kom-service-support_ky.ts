<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>FeedbackManager</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="34"/>
        <source>select detailed category</source>
        <translation>егжей-тегжейлүү категорияны тандоо</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="38"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System activation</source>
        <translation>Системаны активдештирүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System installation</source>
        <translation>Системаны орнотуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System crash</source>
        <translation>Системанын кыйрашы</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="40"/>
        <source>System performance</source>
        <translation>Системалык аткаруу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>Control center</source>
        <translation>Башкаруу борбору</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System setting</source>
        <translation>Системаны орнотуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="41"/>
        <source>System basis consulting</source>
        <translation>Системалык негиздер боюнча консалтинг</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="49"/>
        <source>Please describe in detail the problem you encountered, such as: unable to activate the system, can not find the relevant Settings, not clear system features, etc. </source>
        <translation>Сураныч, мисалы, силер менен кездешкен проблемаларды майда-чүйдөсүнө чейин айтып: системасын жандандыруу мүмкүн эмес, тиешелүү орнотуулар, система милдеттери так эмес, таба албайт.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="61"/>
        <source>Please describe in detail the problems you encountered, such as: peripheral connection failure, sharing function Settings, peripheral adaptation, etc. </source>
        <translation>Сураныч, мисалы, сен туш болгон проблемаларды, майда-чүйдөсүнө чейин айтып: Жабдыктар байланыш бербөө, жалпы иш орнотуулары, жабдыктарына жана башка ылайыкташтыруу.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="71"/>
        <source>Please describe in detail the problems that you encounter, such as obtaining, installing, and uninstalling Kirin software errors. </source>
        <translation>Сураныч, мисалы, сатып алуу, орнотуу жана Кирин программалык каталарды орнотуудан сыяктуу көйгөйлөр, сен менен жолугушту майда-чүйдөсүнө чейин айтып.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="80"/>
        <source>Please describe your problem in detail, or you can also fill in your request or comment here.</source>
        <translation>Сураныч, сиздин суроону майда-чүйдөсүнө чейин айтып, же бул жерде сиздин суроо-жооп же комментарий толтура аласыз.</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="54"/>
        <source>Peripheral</source>
        <translation>Перифериялык</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral adaptation consulting</source>
        <translation>Перифериялык ылайыкташтыруу консалтинг</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="56"/>
        <source>Peripheral driver acquisition</source>
        <translation>Перифериялык драйвер сатып алуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="57"/>
        <source>Peripheral use and error reporting</source>
        <translation>Перифериялык колдонуу жана каталар жөнүндө отчет берүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="66"/>
        <source>Application</source>
        <translation>Тиркеме</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software installation and uninstallation</source>
        <translation>Программалык камсыздоону орнотуу жана орнотуу</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="68"/>
        <source>Software use and error reporting</source>
        <translation>Программалык камсыздоону колдонуу жана каталар жөнүндө отчет берүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="76"/>
        <source>Other</source>
        <translation>Башка</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="78"/>
        <source>Opinions and suggestions</source>
        <translation>Пикирлер жана сунуштар</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="129"/>
        <source>Title and question details cannot be blank!</source>
        <translation>Аталышы жана суроо майда-чүйдөсүнө чейин бош болушу мүмкүн эмес!</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/feedbackmanager.cpp" line="139"/>
        <source>Attachment size exceeds limit!</source>
        <translation>Тиркеме өлчөмү чектен ашып түшөт!</translation>
    </message>
</context>
<context>
    <name>FeedbackManagerLogic</name>
    <message>
        <location filename="../service-support-backend/src/feedbackmanagerlogic.cpp" line="62"/>
        <source>Failed to create temporary directory!</source>
        <translation>Убактылуу каталог түзө алган жок!</translation>
    </message>
</context>
<context>
    <name>GotoPageItem</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="523"/>
        <source>Jump to</source>
        <translation>Секирүү</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="525"/>
        <source>Page</source>
        <translation>Бет</translation>
    </message>
</context>
<context>
    <name>PaginationWid</name>
    <message>
        <location filename="../UI/paginationwid.cpp" line="10"/>
        <source>total</source>
        <translation>бардыгы</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="11"/>
        <source>pages</source>
        <translation>бет</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="40"/>
        <source>Jump to</source>
        <translation>Секирүү</translation>
    </message>
    <message>
        <location filename="../UI/paginationwid.cpp" line="43"/>
        <source>page</source>
        <translation>бет</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="180"/>
        <source>System log</source>
        <translation>Системага кирүү</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="181"/>
        <source>Machine</source>
        <translation>Машина</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="182"/>
        <source>Hardware</source>
        <translation>Аппараттык</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="183"/>
        <source>Drive</source>
        <translation>Диск</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="184"/>
        <source>APP list</source>
        <translation>APP тизмеси</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="185"/>
        <source>Rules</source>
        <translation>Эрежелер</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="186"/>
        <source>Network</source>
        <translation>Тармак</translation>
    </message>
    <message>
        <location filename="../service-support-backend/src/settings.cpp" line="187"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../plugin.cpp" line="18"/>
        <source>ServiceSupport</source>
        <translation>ServiceSupport</translation>
    </message>
</context>
<context>
    <name>UiHistoryFeedback</name>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Creation time</source>
        <translation>Жаратуу убактысы</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Type</source>
        <translation>Түрү</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="64"/>
        <source>bydesign</source>
        <translation>Malingana ndi kapangidwe</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="66"/>
        <source>duplicate</source>
        <translation>копиялоо</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="68"/>
        <source>external</source>
        <translation>Сырткы</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="70"/>
        <source>fixed</source>
        <translation>Негизги-белгиленген</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="72"/>
        <source>notrepro</source>
        <translation>Novo жалпы</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="74"/>
        <source>postponed</source>
        <translation>Kuwonjezeredwa</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="76"/>
        <source>willnotfix</source>
        <translation>Simungathe kukonzedwa</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="86"/>
        <source>in process</source>
        <translation>Процесстин жүрүшүндө</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="97"/>
        <source>verify</source>
        <translation>Onetsetsani</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="91"/>
        <source>completed</source>
        <translation>аяктады</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Description</source>
        <translation>сүрөттөлүшү</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Solution</source>
        <translation>Njira</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="118"/>
        <source>Progress</source>
        <translation>прогресс</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="155"/>
        <source>No record</source>
        <translation>Palibe zolemba</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="156"/>
        <source>There is a network problem, please try again later</source>
        <translation>тармак көйгөйү бар, сураныч, кийин кайра аракет</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="157"/>
        <source>Loading, please wait</source>
        <translation>жүктөө, сураныч, күтүү</translation>
    </message>
    <message>
        <location filename="../UI/uihistoryfeedback.cpp" line="170"/>
        <source>retry</source>
        <translation>ретри</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedback</name>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="108"/>
        <source>Advanced</source>
        <translation>Өнүккөн</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="113"/>
        <source>Type</source>
        <translation>Түрү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="130"/>
        <source>Please describe the problem in detail and you can upload a photo or file by clicking the button below.</source>
        <translation>Сураныч, майда-чүйдөсүнө чейин маселени түшүндүрүп, сиз сүрөттөрдү же материалдары жүктөп төмөнкү баскычты басып алат.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="132"/>
        <source>Remaining</source>
        <translation>Калган</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="133"/>
        <source>character</source>
        <translation>мүнөзү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="168"/>
        <source>Details</source>
        <translation>Майда-чүйдөсүнө</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="175"/>
        <source>ScreenShot</source>
        <translation>ScreenShot</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="190"/>
        <source>Add file</source>
        <translation>Файлды кошуу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="220"/>
        <source>Attachments</source>
        <translation>Тиркемелер</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="241"/>
        <source>The phone number cannot be empty</source>
        <translation>Телефон номери бош болушу мүмкүн эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="243"/>
        <source>The phone number format is incorrect</source>
        <translation>Телефон номеринин форматы туура эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="248"/>
        <source>Please enter your phone number</source>
        <translation>Телефон номериңизди киргизиңиз</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="254"/>
        <source>appellation</source>
        <translation>аппеляция</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="262"/>
        <source>Contact</source>
        <translation>Байланыш</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="273"/>
        <source>The mailbox format is incorrect</source>
        <translation>Почта кутучасынын форматы туура эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="283"/>
        <source>Mailbox</source>
        <translation>Почта кутучасы</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="292"/>
        <source>Agree to take mine </source>
        <translation>Мени алып кетүүгө макул болгула </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="302"/>
        <source>System information</source>
        <translation>Система тууралуу маалымат</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="328"/>
        <source>Submit</source>
        <translation>Тапшыруу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="345"/>
        <source>Details type</source>
        <translation>Маалымат түрү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="360"/>
        <source>Time period</source>
        <translation>Убакыт мезгили</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="369"/>
        <source>Job number</source>
        <translation>Жумуш номери</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="404"/>
        <source>Information</source>
        <translation>Маалымат</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="407"/>
        <source>lately</source>
        <translation>акыркы күндөрдө</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="408"/>
        <source>days</source>
        <translation>күн</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="420"/>
        <source>YES</source>
        <translation>ООБА</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="430"/>
        <source>NO</source>
        <translation>ЖОК</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="452"/>
        <source>Upload log</source>
        <translation>Жүктөө журналы</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="458"/>
        <source>Path</source>
        <translation>Жол</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="472"/>
        <source>Export to</source>
        <translation>Экспорттоо</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="548"/>
        <source>No more than 5 files and total capacity not exceed 10MB</source>
        <translation>5 файлдан ашык эмес жана жалпы кубаттуулугу 10МБ ашык эмес</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="549"/>
        <source>Supported formats: </source>
        <translation>Колдоого алынган форматтар: </translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="131"/>
        <source>Up to 500 characters</source>
        <translation>500 символго чейин</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="626"/>
        <source>Repeat addition</source>
        <translation>Кошумчаны кайталоо</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="642"/>
        <source>Attachment size out of limit</source>
        <translation>Тиркеме өлчөмү чектен чыккан</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedback.cpp" line="808"/>
        <source>Add attachment</source>
        <translation>Тиркемени кошуу</translation>
    </message>
</context>
<context>
    <name>UiProblemFeedbackDialog</name>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="34"/>
        <source>OK</source>
        <translation>МАКУЛ</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="37"/>
        <source>Retry</source>
        <translation>ретри</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="79"/>
        <source>Submitted successfully</source>
        <translation>Ийгиликтүү жөнөтүлдү</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="83"/>
        <source>Cancel successfully</source>
        <translation>ийгиликтүү жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="87"/>
        <source>System is abnormal, contact technical support</source>
        <translation>Ndondomeko yachilendo, chonde tanani ndi chithandizo chamakono</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Log and submission is packed, please go</source>
        <translation>Lembani ndi kutumiza zakonzedwa, chonde musiye</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="88"/>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="95"/>
        <source>acquire</source>
        <translation>Pezani</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="93"/>
        <source>Submission failed</source>
        <translation>Сунуштоо ишке ашпады</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="94"/>
        <source>Click &apos;Retry&apos; to upload again, or contact us directly.</source>
        <translation>&quot;Кайра аракет&quot; кайра жүктөп, же түздөн-түз бизге кайрылып.</translation>
    </message>
    <message>
        <location filename="../UI/uiproblemfeedbackdialog.cpp" line="113"/>
        <source>Under submission...</source>
        <translation>Сунуштоо боюнча...</translation>
    </message>
</context>
<context>
    <name>UiSelfService</name>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="17"/>
        <source>Contact us</source>
        <translation>Биз менен байланыш</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="37"/>
        <source>Telephone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="50"/>
        <source>Mail</source>
        <translation>Почта</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="63"/>
        <source>Team</source>
        <translation>Команда</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="80"/>
        <source> to get more services</source>
        <translation> көбүрөөк кызматтарды алуу үчүн</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="98"/>
        <source>Kylin technical services</source>
        <translation>Кайлин техникалык кызматтары</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="72"/>
        <source>Jump to</source>
        <translation>Секирүү</translation>
    </message>
    <message>
        <location filename="../UI/uiselfservice.cpp" line="73"/>
        <source> KylinOS website</source>
        <translation> КайлинОС сайты</translation>
    </message>
</context>
<context>
    <name>UiServiceOnline</name>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="40"/>
        <source>Loading, please wait</source>
        <translation>жүктөө, сураныч, күтүү</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="54"/>
        <source>retry</source>
        <translation>ретри</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="67"/>
        <source>There is a network problem, please try again later</source>
        <translation>тармак көйгөйү бар, сураныч, кийин кайра аракет</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="83"/>
        <source>Go to your browser</source>
        <translation>браузерге</translation>
    </message>
    <message>
        <location filename="../UI/uiserviceonline.cpp" line="84"/>
        <source>Go</source>
        <translation>Мурунку</translation>
    </message>
</context>
<context>
    <name>UiServiceSupport</name>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="19"/>
        <source>Feedback</source>
        <translation>Пикирлер</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="20"/>
        <source>Online</source>
        <translation>Онлайн</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="21"/>
        <source>Self service</source>
        <translation>Өзүмчүл кызмат</translation>
    </message>
    <message>
        <location filename="../UI/uiservicesupport.cpp" line="22"/>
        <source>History</source>
        <translation>Тарых</translation>
    </message>
</context>
</TS>
