<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ConfigWin</name>
    <message>
        <location filename="../appUI/src/config_win.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="264"/>
        <source>SetInner</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠭᠠᠯᠳᠠ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="429"/>
        <location filename="../appUI/src/config_win.cpp" line="61"/>
        <source>Cancel</source>
        <translation>ᠦᠭᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="464"/>
        <location filename="../appUI/src/config_win.cpp" line="62"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.cpp" line="35"/>
        <location filename="../appUI/src/config_win.cpp" line="37"/>
        <source>IntraNetConfig</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>DHCPCheck</name>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="31"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="33"/>
        <source>DHCP Config</source>
        <translation>DHCP ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="34"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="91"/>
        <source>Are DHCP config right?</source>
        <translation>DHCP ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠠᠢᠬᠤ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="62"/>
        <source>Checking DHCP config</source>
        <translation>DHCP ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠠᠢᠬᠤ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="69"/>
        <source>DHCP RUNNING RIGHT</source>
        <translation>DHCP ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="69"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="78"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="73"/>
        <source>DHCP DISTRIBUTED WRONG IP</source>
        <translation>DHCP ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠪᠤᠷᠤᠭᠤ IP ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="73"/>
        <source>ERR</source>
        <translation>ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="78"/>
        <source>DHCP IS OFF, NO CHECK</source>
        <translation>DHCP ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠥᠬᠡᠢ᠂ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>DNSCheck</name>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="29"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="32"/>
        <source>DNS Config</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="33"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="44"/>
        <source>Are DNS config right?</source>
        <translation>DNS ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ᠂ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠵᠠᠳᠠᠯᠬᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="56"/>
        <source>Checking DNS config</source>
        <translation>DNS ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ᠂ ᠬᠠᠶᠢᠭ ᠢ᠋ ᠵᠠᠳᠠᠯᠬᠤ ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠡᠰᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="56"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <source>NO DNS</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="119"/>
        <source>ERR</source>
        <translation>ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="119"/>
        <source>DNS service is working abnormally</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="122"/>
        <source>DNS service is working properly</source>
        <translation>DNS ᠦᠢᠯᠡᠴᠢᠯᠡᠭᠡ ᠬᠡᠪ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="122"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../customWidget/detailbutton.cpp" line="31"/>
        <source>detail</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>HWCheck</name>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="31"/>
        <location filename="../HWCheck/hwcheck.cpp" line="33"/>
        <source>HardWare</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠷᠳ᠋ᠸᠠᠢᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="32"/>
        <location filename="../HWCheck/hwcheck.cpp" line="94"/>
        <source>Are network card OK and cable connected?</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠤᠳᠠᠰᠤ ᠵᠢ ᠰᠠᠢᠨ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ ᠵᠢᠴᠢ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠬᠡᠪ ᠤ᠋ᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="64"/>
        <source>Checking NetWork HardWares</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠤᠳᠠᠰᠤ ᠵᠢ ᠰᠠᠢᠨ ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ ᠵᠢᠴᠢ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠬᠡᠪ ᠤ᠋ᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="64"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="70"/>
        <source>NetWork HardWares are OK,Primary Wired.</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠷᠳ᠋ᠸᠠᠢᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠬᠡᠪ ᠤ᠋ᠨ᠂ ᠤᠳᠠᠰᠤᠳᠤ ᠵᠢ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="70"/>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>NetWork HardWares are OK,Primary Wireless.</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠷᠳ᠋ᠸᠠᠢᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠬᠡᠪ ᠤ᠋ᠨ᠂ ᠤᠳᠠᠰᠤ ᠥᠬᠡᠢ ᠵᠢ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠰᠣᠩᠭᠣᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="78"/>
        <source>NetWork HardWares are OK, but no connection</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="78"/>
        <location filename="../HWCheck/hwcheck.cpp" line="81"/>
        <source>ERR</source>
        <translation>ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="81"/>
        <source>No valid net card</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠣᠯᠬᠤ ᠬᠠᠷᠳ᠋ᠸᠠᠢᠷ ᠤ᠋ᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠥᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>HostCheck</name>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="35"/>
        <location filename="../HostCheck/hostcheck.cpp" line="37"/>
        <source>Host File</source>
        <translation>Host ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="38"/>
        <location filename="../HostCheck/hostcheck.cpp" line="302"/>
        <source>Are Host File config right?</source>
        <translation>Host ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠬᠡᠪ ᠤ᠋ᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="57"/>
        <source>No host file!</source>
        <translation>Host ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="73"/>
        <location filename="../HostCheck/hostcheck.cpp" line="74"/>
        <source>Has no sperated line.</source>
        <translation>Has ᠹᠠᠢᠯ ᠳ᠋ᠤ᠌ ᠬᠣᠭᠣᠰᠣᠨ ᠵᠠᠢ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠥᠬᠡᠢ ᠮᠥᠷ ᠪᠠᠢᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="82"/>
        <location filename="../HostCheck/hostcheck.cpp" line="83"/>
        <location filename="../HostCheck/hostcheck.cpp" line="192"/>
        <location filename="../HostCheck/hostcheck.cpp" line="193"/>
        <source>Ipv4 localhost error.</source>
        <translation>Ipv4 localhost ᠪᠤᠷᠤᠭᠤ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="99"/>
        <location filename="../HostCheck/hostcheck.cpp" line="100"/>
        <location filename="../HostCheck/hostcheck.cpp" line="196"/>
        <location filename="../HostCheck/hostcheck.cpp" line="197"/>
        <source>Ipv4 localPChost error.</source>
        <translation>Ipv4 localPChost ᠪᠤᠷᠤᠭᠤ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="107"/>
        <location filename="../HostCheck/hostcheck.cpp" line="108"/>
        <source>Ipv6 localhost error.</source>
        <translation>Ipv6 localhost ᠪᠤᠷᠤᠭᠤ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="115"/>
        <location filename="../HostCheck/hostcheck.cpp" line="116"/>
        <source>Ipv6 localnet error.</source>
        <translation>Ipv6 localnet ᠪᠤᠷᠤᠭᠤ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="123"/>
        <location filename="../HostCheck/hostcheck.cpp" line="124"/>
        <source>Ipv6 mcastsprefix error.</source>
        <translation>Ipv6 mcastsprefix ᠪᠤᠷᠤᠭᠤ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="131"/>
        <location filename="../HostCheck/hostcheck.cpp" line="132"/>
        <source>Ipv6 nodes error.</source>
        <translation>Ipv6 mcastsprefix ᠪᠤᠷᠤᠭᠤ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="139"/>
        <location filename="../HostCheck/hostcheck.cpp" line="140"/>
        <source>Ipv6 routers error.</source>
        <translation>Ipv6 routers ᠪᠤᠷᠤᠭᠤ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="157"/>
        <location filename="../HostCheck/hostcheck.cpp" line="158"/>
        <source>User add illegal hosts.</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠮᠡᠭᠰᠡᠨ hosts ᠤ᠋ᠨ/ ᠵᠢᠨ ᠳᠤᠰᠬᠠᠯ ᠳᠦᠷᠢᠮᠵᠢᠯ ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠥᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="277"/>
        <source>Checking Host Files</source>
        <translation>Host ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠬᠡᠪ ᠤ᠋ᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="277"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="282"/>
        <source>Hosts Files are OK</source>
        <translation>Host ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠬᠡᠪ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="282"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="285"/>
        <location filename="../HostCheck/hostcheck.cpp" line="288"/>
        <source>ERR</source>
        <translation>ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="288"/>
        <source>The local hosts file is abnormal, please repait it</source>
        <translation>ᠲᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ hosts ᠹᠠᠢᠯ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ᠂ ᠵᠠᠰᠠᠨ ᠰᠡᠷᠬᠦᠬᠡᠬᠡᠷᠢ</translation>
    </message>
</context>
<context>
    <name>IPCheck</name>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="34"/>
        <location filename="../IPCheck/ipcheck.cpp" line="36"/>
        <source>IP Config</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ᠨ ᠴᠥᠷᠬᠡᠯᠡᠭᠡ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="35"/>
        <location filename="../IPCheck/ipcheck.cpp" line="88"/>
        <source>Are IP config right?</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ᠂ IP ᠬᠠᠶᠢᠭ ᠢ᠋ ᠣᠯᠵᠠᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="62"/>
        <source>Checking IP config</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠺᠠᠷᠲ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠢᠨᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ᠂ IP ᠬᠠᠶᠢᠭ ᠢ᠋ ᠣᠯᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="67"/>
        <source>DHCP ON</source>
        <translation>DHCP ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ᠂ ᠲᠤᠰ ᠳᠦᠷᠦᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="67"/>
        <location filename="../IPCheck/ipcheck.cpp" line="73"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="70"/>
        <source>IP CONFIG FALSE</source>
        <translation>ᠨᠸᠲ ᠪᠤᠭᠤᠮᠳᠠ ᠬᠢᠬᠡᠳ IP ᠬᠠᠶᠢᠭ ᠢᠵᠢᠯ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠬᠡᠰᠡᠭ ᠲᠤ᠌ ᠪᠠᠢᠬᠤ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="70"/>
        <source>ERR</source>
        <translation>ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="73"/>
        <source>IP CONFIG RIGHT</source>
        <translation>IP ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠴᠦᠷᠬᠡᠯᠡᠭᠡᠨ ᠤ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠬᠡᠪ ᠤ᠋ᠨ</translation>
    </message>
</context>
<context>
    <name>IPWebWidget</name>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="55"/>
        <source>IP</source>
        <translation>IP ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="57"/>
        <source>Website</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="101"/>
        <source>Format error,IP is invalid</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠵᠠᠭᠪᠤᠷ᠂ IP ᠬᠠᠶᠢᠭ ᠬᠦᠴᠦᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="114"/>
        <source>Format error,web is invalid</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠵᠠᠭᠪᠤᠷ᠂ web ᠬᠠᠶᠢᠭ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.ui" line="93"/>
        <source>Addr</source>
        <translation>ᠬᠠᠶᠢᠭ</translation>
    </message>
</context>
<context>
    <name>IncreaseWidget</name>
    <message>
        <location filename="../appUI/src/increase_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="37"/>
        <source>Detail</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠬᠦᠢ ᠪᠠᠢᠳᠠᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="92"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="96"/>
        <location filename="../customWidget/item_widget.cpp" line="108"/>
        <location filename="../customWidget/item_widget.cpp" line="116"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="100"/>
        <location filename="../customWidget/item_widget.cpp" line="112"/>
        <location filename="../customWidget/item_widget.cpp" line="120"/>
        <source>ERR</source>
        <translation>ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="104"/>
        <location filename="../customWidget/item_widget.cpp" line="124"/>
        <source>WARNING</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <location filename="../libNWDBus/src/kylin-dbus-interface.cpp" line="927"/>
        <source>Wired connection</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="32"/>
        <source>MainWindow</source>
        <translation>mainWindow</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="358"/>
        <source>PushButton</source>
        <translation>pushButton</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="426"/>
        <location filename="../appUI/src/mainwindow.cpp" line="58"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="451"/>
        <location filename="../appUI/src/mainwindow.cpp" line="62"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠨ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="454"/>
        <source>Checking...</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="671"/>
        <source>Intranet IP</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ IP</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="694"/>
        <source>Intranet Web</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="401"/>
        <location filename="../appUI/src/mainwindow.cpp" line="455"/>
        <source>Start</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="75"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="127"/>
        <source>NetCheck</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="135"/>
        <source>total 6 items</source>
        <translation>ᠨᠡᠢᠳᠡ 6 ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="489"/>
        <source>Canceling...</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ...</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="728"/>
        <source>InnerNet Check</source>
        <translation>ᠳᠣᠲᠣᠭᠠᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="729"/>
        <source>Check whether the intranet is smooth</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠰᠠᠭᠠᠳ ᠦᠭᠡᠶ ᠨᠡᠪᠲᠡᠷᠡᠬᠦ ᠡᠰᠡᠬᠦ᠂ ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠦᠷᠳᠡᠭᠡ ᠳ᠋ᠤ᠌ ᠰᠤᠷᠪᠤᠯᠵᠢᠯᠠᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="732"/>
        <source>Internet access</source>
        <translation>ᠺᠤᠫᠢᠦ᠋ᠲᠸᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠴᠤ ᠴᠢᠳᠠᠬᠤ ᠡᠰᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="733"/>
        <source>Can user browse out net?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠺᠤᠮᠫᠢᠦ᠋ᠲᠸᠷ ᠬᠡᠪ ᠤ᠋ᠨ᠂ ᠰᠢᠭ᠋ᠤᠷᠬᠠᠢ ᠪᠡᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="762"/>
        <source>checked %1 items,no issue</source>
        <translation>%1 ᠬᠠᠭᠤᠳᠠᠰᠤ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠪᠠ᠂ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢᠯᠡᠷᠡᠭᠰᠡᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="764"/>
        <source>checked %1 items,find %2 errs,%3 issues</source>
        <translation>%1 ᠬᠠᠭᠤᠳᠠᠰᠤ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ, %2 ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠨ,%3 ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="889"/>
        <source>Check interrupted, no issues found</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠳᠠᠰᠤᠯᠠᠭᠳᠠᠪᠠ᠂ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢᠯᠡᠷᠡᠭᠰᠡᠨ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="890"/>
        <source>We suggest that you conduct a complete inspection again</source>
        <translation>ᠲᠠ ᠳᠠᠬᠢᠵᠤ ᠪᠦᠷᠢᠨ ᠪᠦᠳᠦᠨ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠬᠢᠬᠦ ᠵᠢ ᠵᠦᠪᠯᠡᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="894"/>
        <source>Check interruption and found %1 issues</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠳᠠᠰᠤᠯᠠᠭᠳᠠᠪᠠ᠂ %1 ᠠᠰᠠᠭᠤᠳᠠᠯ ᠣᠯᠵᠤ ᠮᠡᠳᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="895"/>
        <source>Please repair and retest</source>
        <translation>ᠵᠠᠰᠠᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="903"/>
        <source>No problems found</source>
        <translation>ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠬᠡ ᠥᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="904"/>
        <source>Please continue to maintain and regularly check up</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠵᠢᠨ ᠵᠡᠷᠬᠡᠴᠡᠬᠡ ᠳᠤᠭᠳᠠᠮᠠᠯ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠬᠢᠵᠤ ᠪᠠᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="909"/>
        <location filename="../appUI/src/mainwindow.cpp" line="925"/>
        <source>Found %1 problem and %2 prompt problems</source>
        <translation>%1 ᠠᠰᠠᠭᠤᠳᠠᠯ᠂ %2 ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢᠯᠡᠷᠡᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="910"/>
        <location filename="../appUI/src/mainwindow.cpp" line="915"/>
        <location filename="../appUI/src/mainwindow.cpp" line="920"/>
        <location filename="../appUI/src/mainwindow.cpp" line="926"/>
        <source>Please re-detect after repair</source>
        <translation>ᠵᠠᠰᠠᠨ ᠰᠡᠷᠬᠦᠬᠡᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠵᠤ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠴᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="914"/>
        <source>Found %1 problem</source>
        <translation>%1 ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢᠯᠡᠷᠡᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="919"/>
        <source>Found %1 prompt problem</source>
        <translation>%1 ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢᠯᠡᠷᠡᠬᠦᠯᠪᠡ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="290"/>
        <location filename="../appUI/src/mainwindow.cpp" line="45"/>
        <location filename="../appUI/src/mainwindow.cpp" line="817"/>
        <source>Detect and resolve Network Faults</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠡᠮᠳᠦᠯ ᠢ᠋ ᠪᠦᠬᠦ ᠲᠠᠯ᠎ᠠ ᠪᠡᠷ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ᠂ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠴᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="249"/>
        <location filename="../appUI/src/mainwindow.cpp" line="42"/>
        <location filename="../appUI/src/mainwindow.cpp" line="818"/>
        <source>Detect Network Faults</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠡᠮᠳᠦᠯ᠂ ᠨᠢᠭᠡ ᠳᠠᠡᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="66"/>
        <location filename="../appUI/src/menumodule.cpp" line="102"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="68"/>
        <location filename="../appUI/src/menumodule.cpp" line="100"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="70"/>
        <location filename="../appUI/src/menumodule.cpp" line="105"/>
        <source>Configure</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="72"/>
        <location filename="../appUI/src/menumodule.cpp" line="98"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="135"/>
        <source>Network-check-tool is a software that can quickly detect,diagnose,and optimize networks.</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠨᠢ ᠨᠢᠭᠡᠨ ᠳᠦᠷᠬᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ᠂ ᠤᠨᠤᠰᠢᠯᠠᠬᠤ᠂ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠰᠢᠯᠢᠳᠡᠭᠵᠢᠬᠦᠯᠬᠦ ᠰᠤᠹᠲ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠶᠠᠪᠤᠴᠠ ᠳᠦᠬᠦᠮ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠰᠢᠭ᠋ᠤᠷᠬᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="134"/>
        <location filename="../appUI/src/menumodule.cpp" line="259"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ: </translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="263"/>
        <source>Network-check-tool is a software that can quickly detect, diagnose, and optimize networks. </source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠨᠢ ᠨᠢᠭᠡᠨ ᠳᠦᠷᠬᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ᠂ ᠤᠨᠤᠰᠢᠯᠠᠬᠤ᠂ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠵᠢ ᠰᠢᠯᠢᠳᠡᠭᠵᠢᠬᠦᠯᠬᠦ ᠰᠤᠹᠲ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠶᠠᠪᠤᠴᠠ ᠳᠦᠬᠦᠮ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠰᠢᠭ᠋ᠤᠷᠬᠠᠢ. </translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="275"/>
        <location filename="../appUI/src/menumodule.cpp" line="358"/>
        <location filename="../appUI/src/menumodule.cpp" line="366"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ: </translation>
    </message>
</context>
<context>
    <name>NetCheck</name>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="54"/>
        <location filename="../NetCheck/netcheck.cpp" line="55"/>
        <location filename="../NetCheck/netcheck.cpp" line="77"/>
        <location filename="../NetCheck/netcheck.cpp" line="78"/>
        <source>InnerNet Check</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="56"/>
        <location filename="../NetCheck/netcheck.cpp" line="76"/>
        <source>Can user browse inner net?</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠬᠠᠶᠢᠭ ᠬᠦᠷᠴᠤ ᠴᠢᠳᠠᠬᠤ ᠡᠰᠡᠬᠦ᠂ ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠦᠷᠳᠡᠭᠡ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="58"/>
        <location filename="../NetCheck/netcheck.cpp" line="59"/>
        <location filename="../NetCheck/netcheck.cpp" line="81"/>
        <location filename="../NetCheck/netcheck.cpp" line="82"/>
        <source>AccessNet Check</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠴᠤ ᠴᠢᠳᠠᠬᠤ ᠡᠰᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="60"/>
        <location filename="../NetCheck/netcheck.cpp" line="80"/>
        <source>Can user browse out net?</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠺᠤᠮᠫᠢᠦ᠋ᠲᠸᠷ ᠬᠡᠪ ᠤ᠋ᠨ᠂ ᠰᠢᠭ᠋ᠤᠷᠬᠠᠢ ᠪᠡᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="76"/>
        <location filename="../NetCheck/netcheck.cpp" line="80"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>NetCheckHomePage</name>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="29"/>
        <source>Check and Repair</source>
        <translation>ᠺᠤᠮᠫᠢᠦ᠋ᠲᠸᠷ ᠤ᠋ᠨ ᠬᠡᠮᠳᠦᠯ ᠢ᠋ ᠨᠢᠭᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠰᠢᠯᠭᠠᠨ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="36"/>
        <source>Detection and repair of computer problems</source>
        <translation>ᠬᠡᠮᠳᠦᠯ ᠢ᠋ ᠵᠠᠰᠠᠨ ᠰᠡᠷᠬᠦᠬᠡᠭᠰᠡᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠳᠦᠷᠬᠡᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ᠂ ᠲᠠᠨ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠡᠷᠡᠭᠦᠯ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="49"/>
        <source>NetCheck</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="66"/>
        <source>Start</source>
        <translation>ᠨᠢᠭᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="97"/>
        <source>IntraNetSet</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>NetCheckThread</name>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="311"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="314"/>
        <source>Extranet normal</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠬᠡᠪ ᠤ᠋ᠨ᠂ ᠰᠢᠭ᠋ᠤᠷᠬᠠᠢ ᠪᠡᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠪᠣᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="311"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="314"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="324"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="329"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="345"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="317"/>
        <source>Extranet abnormal</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠬᠡᠪ ᠤ᠋ᠨ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠥᠬᠡᠢ᠂ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="317"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="342"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="360"/>
        <source>ERR</source>
        <translation>ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="324"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="329"/>
        <source>Intranet normal</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ ᠨᠡᠪᠳᠡᠷᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <source>Url can be accessed</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠤᠭᠳᠠᠭᠰᠠᠨ ᠦᠷᠳᠡᠭᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠪ ᠤ᠋ᠨ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠴᠢᠳᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="342"/>
        <source>Url cannot be accessed</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠤᠭᠳᠠᠭᠰᠠᠨ ᠦᠷᠳᠡᠭᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠪ ᠤ᠋ᠨ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠴᠢᠳᠠᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <source>IP is reachable</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ IP ᠬᠠᠶᠢᠭ ᠬᠦᠷᠴᠤ ᠴᠢᠳᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="345"/>
        <source>IP is reachable，url cannot be accessed</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ IP ᠬᠠᠶᠢᠭ ᠬᠦᠷᠴᠤ ᠳᠡᠢᠯᠦᠨ᠎ᠡ᠂ ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠤᠭᠳᠠᠭᠰᠠᠨ ᠦᠷᠳᠡᠭᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠪ ᠤ᠋ᠨ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <source>IP is unreachable</source>
        <translation>ᠳᠤᠳᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ IP ᠬᠠᠶᠢᠭ ᠬᠦᠷᠴᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>IP is unreachable，url can be accessed</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ IP ᠬᠠᠶᠢᠭ ᠬᠦᠷᠴᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ᠂ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠤᠭᠳᠠᠭᠰᠠᠨ ᠦᠷᠳᠡᠭᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠪ ᠤ᠋ᠨ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="360"/>
        <source>IP is unreachable，url cannot be accessed</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ IP ᠬᠠᠶᠢᠭ ᠬᠦᠷᠴᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ᠂ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠤ᠋ ᠲᠤᠭᠳᠠᠭᠰᠠᠨ ᠦᠷᠳᠡᠭᠡᠨ ᠳ᠋ᠤ᠌ ᠬᠡᠪ ᠤ᠋ᠨ ᠠᠢᠯᠴᠢᠯᠠᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>ProxyCheck</name>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="38"/>
        <source>NetWork Proxy</source>
        <translation>ᠬᠠᠢᠭᠤᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Checking</source>
        <translation>ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="40"/>
        <source>Proxy</source>
        <translation>ᠬᠠᠢᠭᠤᠷ ᠤ᠋ᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="41"/>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Check whether the proxy is working?</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠤᠷᠤᠯᠠᠭᠴᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="71"/>
        <source>proxy disable</source>
        <translation>ᠤᠷᠤᠯᠠᠭᠴᠢ ᠵᠢ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="74"/>
        <source>auto proxy normal</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠤᠷᠤᠯᠠᠭᠴᠢ ᠬᠡᠪ ᠤ᠋ᠨ</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="77"/>
        <source>auto proxy abnormal</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠤᠷᠤᠯᠠᠭᠴᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="80"/>
        <source>manual proxy normal</source>
        <translation>ᠭᠠᠷ ᠤᠷᠤᠯᠠᠭᠴᠢ ᠬᠡᠪ ᠤ᠋ᠨ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appUI/src/netcheckentr.cpp" line="34"/>
        <source>NetworkCheck</source>
        <translation>ᠬᠡᠮᠳᠦᠯ ᠤ᠋ᠨ ᠪᠠᠢᠴᠠᠭᠠᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="30"/>
        <source>menu</source>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="38"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="51"/>
        <source>full screen</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="64"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="83"/>
        <source>Kylin NM</source>
        <translation>ᠴᠢ ᠯᠢᠨ NM</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="85"/>
        <source>kylin network applet desktop message</source>
        <translation>ᠴᠢ ᠯᠢᠨ network applet desktop message</translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="38"/>
        <source>WLAN Authentication</source>
        <translation>wlan Authentication</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="49"/>
        <source>Input WLAN Information Please</source>
        <translation>input WLAN Information Please</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="50"/>
        <source>WLAN ID：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="51"/>
        <source>WLAN Name:</source>
        <translation>wlanName:</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="52"/>
        <source>Password：</source>
        <translation>password：</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="53"/>
        <source>Cancl</source>
        <translation>cancl</translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="54"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
</TS>
