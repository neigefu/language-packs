<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>ConfigWin</name>
    <message>
        <location filename="../appUI/src/config_win.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="264"/>
        <source>SetInner</source>
        <translation>開啟內網檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="429"/>
        <location filename="../appUI/src/config_win.cpp" line="61"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.ui" line="464"/>
        <location filename="../appUI/src/config_win.cpp" line="62"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../appUI/src/config_win.cpp" line="35"/>
        <location filename="../appUI/src/config_win.cpp" line="37"/>
        <source>IntraNetSet</source>
        <translation>內網檢測設置</translation>
    </message>
</context>
<context>
    <name>DHCPCheck</name>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="31"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="33"/>
        <source>DHCP Config</source>
        <translation>DHCP 服務</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="34"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="91"/>
        <source>Are DHCP config right?</source>
        <translation>檢測 DHCP 服務是否正常工作</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="62"/>
        <source>Checking DHCP config</source>
        <translation>檢測 DHCP 服務是否正常工作</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="69"/>
        <source>DHCP RUNNING RIGHT</source>
        <translation>DHCP 服務工作正常</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="69"/>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="78"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="73"/>
        <source>DHCP DISTRIBUTED WRONG IP</source>
        <translation>DHCP分配了錯誤的IP</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="73"/>
        <source>ERR</source>
        <translation>異常</translation>
    </message>
    <message>
        <location filename="../DHCPCheck/dhcpcheck.cpp" line="78"/>
        <source>DHCP IS OFF, NO CHECK</source>
        <translation>DHCP 未開啟，不檢測</translation>
    </message>
</context>
<context>
    <name>DNSCheck</name>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="29"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="32"/>
        <source>DNS Config</source>
        <translation>DNS 服務</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="33"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="44"/>
        <source>Are DNS config right?</source>
        <translation>檢測 DNS 是否配置，位址解析服務是否正常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="56"/>
        <source>Checking DNS config</source>
        <translation>檢測 DNS 是否配置，位址解析服務是否正常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="56"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <source>NO DNS</source>
        <translation>DNS 服務工作異常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="62"/>
        <location filename="../DNSCheck/dnscheck.cpp" line="119"/>
        <source>ERR</source>
        <translation>異常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="119"/>
        <source>DNS service is working abnormally</source>
        <translation>DNS 服務工作異常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="122"/>
        <source>DNS service is working properly</source>
        <translation>DNS 服務工作正常</translation>
    </message>
    <message>
        <location filename="../DNSCheck/dnscheck.cpp" line="122"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
</context>
<context>
    <name>DetailButton</name>
    <message>
        <location filename="../customWidget/detailbutton.cpp" line="31"/>
        <source>detail</source>
        <translation>查看詳情</translation>
    </message>
</context>
<context>
    <name>HWCheck</name>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="31"/>
        <location filename="../HWCheck/hwcheck.cpp" line="33"/>
        <source>HardWare</source>
        <translation>網路硬體配置</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="32"/>
        <location filename="../HWCheck/hwcheck.cpp" line="94"/>
        <source>Are network card OK and cable connected?</source>
        <translation>檢測網線是否插好，網卡及驅動是否正常工作</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="64"/>
        <source>Checking NetWork HardWares</source>
        <translation>檢測網線是否插好，網卡及驅動是否正常工作</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="64"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="70"/>
        <source>NetWork HardWares are OK,Primary Wired.</source>
        <translation>網路硬體配置正常，首選有線</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="70"/>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="73"/>
        <source>NetWork HardWares are OK,Primary Wireless.</source>
        <translation>網路硬體配置正常，首選無線</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="78"/>
        <source>NetWork HardWares are OK, but no connection</source>
        <translation>未連接可用網路</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="78"/>
        <location filename="../HWCheck/hwcheck.cpp" line="81"/>
        <source>ERR</source>
        <translation>異常</translation>
    </message>
    <message>
        <location filename="../HWCheck/hwcheck.cpp" line="81"/>
        <source>No valid net card</source>
        <translation>無可用硬體設備</translation>
    </message>
</context>
<context>
    <name>HostCheck</name>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="35"/>
        <location filename="../HostCheck/hostcheck.cpp" line="37"/>
        <source>Host File</source>
        <translation>Host 檔</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="38"/>
        <location filename="../HostCheck/hostcheck.cpp" line="302"/>
        <source>Are Host File config right?</source>
        <translation>檢測 Host 檔案格式是否正常</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="57"/>
        <source>No host file!</source>
        <translation>沒有 Host 檔</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="73"/>
        <location filename="../HostCheck/hostcheck.cpp" line="74"/>
        <source>Has no sperated line.</source>
        <translation>Host 檔中有未加空格的行</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="82"/>
        <location filename="../HostCheck/hostcheck.cpp" line="83"/>
        <location filename="../HostCheck/hostcheck.cpp" line="192"/>
        <location filename="../HostCheck/hostcheck.cpp" line="193"/>
        <source>Ipv4 localhost error.</source>
        <translation>IPv4 Localhost 錯誤</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="99"/>
        <location filename="../HostCheck/hostcheck.cpp" line="100"/>
        <location filename="../HostCheck/hostcheck.cpp" line="196"/>
        <location filename="../HostCheck/hostcheck.cpp" line="197"/>
        <source>Ipv4 localPChost error.</source>
        <translation>IPv4 Localhost 錯誤</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="107"/>
        <location filename="../HostCheck/hostcheck.cpp" line="108"/>
        <source>Ipv6 localhost error.</source>
        <translation>IPv6 Localhost 錯誤</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="115"/>
        <location filename="../HostCheck/hostcheck.cpp" line="116"/>
        <source>Ipv6 localnet error.</source>
        <translation>IPv6 Localnet 錯誤</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="123"/>
        <location filename="../HostCheck/hostcheck.cpp" line="124"/>
        <source>Ipv6 mcastsprefix error.</source>
        <translation>IPv6 Mcastsprefix 錯誤</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="131"/>
        <location filename="../HostCheck/hostcheck.cpp" line="132"/>
        <source>Ipv6 nodes error.</source>
        <translation>IPv6 Nodes 錯誤</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="139"/>
        <location filename="../HostCheck/hostcheck.cpp" line="140"/>
        <source>Ipv6 routers error.</source>
        <translation>IPv6 Routers 錯誤</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="157"/>
        <location filename="../HostCheck/hostcheck.cpp" line="158"/>
        <source>User add illegal hosts.</source>
        <translation>使用者添加的 Hosts 映射不符合規範</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="277"/>
        <source>Checking Host Files</source>
        <translation>檢測 Host 檔案格式是否正常</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="277"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="282"/>
        <source>Hosts Files are OK</source>
        <translation>Host 檔案格式正常</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="282"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="285"/>
        <location filename="../HostCheck/hostcheck.cpp" line="288"/>
        <source>ERR</source>
        <translation>異常</translation>
    </message>
    <message>
        <location filename="../HostCheck/hostcheck.cpp" line="288"/>
        <source>The local hosts file is abnormal, please repait it</source>
        <translation>本機 Hosts 檔異常，請修復</translation>
    </message>
</context>
<context>
    <name>IPCheck</name>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="34"/>
        <location filename="../IPCheck/ipcheck.cpp" line="36"/>
        <source>IP Config</source>
        <translation>網路連接配置</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="35"/>
        <location filename="../IPCheck/ipcheck.cpp" line="88"/>
        <source>Are IP config right?</source>
        <translation>檢測網卡相關設置，是否可獲取到IP位址</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="62"/>
        <source>Checking IP config</source>
        <translation>檢測網卡相關設置，是否可獲取到IP位址</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="62"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="67"/>
        <source>DHCP ON</source>
        <translation>DHCP 開啟，此項不檢查</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="67"/>
        <location filename="../IPCheck/ipcheck.cpp" line="73"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="70"/>
        <source>IP CONFIG FALSE</source>
        <translation>閘道和IP位址不在同一網段</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="70"/>
        <source>ERR</source>
        <translation>異常</translation>
    </message>
    <message>
        <location filename="../IPCheck/ipcheck.cpp" line="73"/>
        <source>IP CONFIG RIGHT</source>
        <translation>網路連接配置正常</translation>
    </message>
</context>
<context>
    <name>IPWebWidget</name>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="55"/>
        <source>IP</source>
        <translation>IP 位址</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="57"/>
        <source>Website</source>
        <translation>網站位址</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="101"/>
        <source>Format error,IP is invalid</source>
        <translation>錯誤格式，IP 位址無效</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.cpp" line="114"/>
        <source>Format error,web is invalid</source>
        <translation>錯誤格式，Web 位址無效</translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../appUI/src/ipweb_widget.ui" line="93"/>
        <source>Addr</source>
        <translation>位址</translation>
    </message>
</context>
<context>
    <name>IncreaseWidget</name>
    <message>
        <location filename="../appUI/src/increase_widget.ui" line="32"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>ItemWidget</name>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="37"/>
        <source>Detail</source>
        <translation>查看詳情</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="95"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="99"/>
        <location filename="../customWidget/item_widget.cpp" line="111"/>
        <location filename="../customWidget/item_widget.cpp" line="119"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="103"/>
        <location filename="../customWidget/item_widget.cpp" line="115"/>
        <location filename="../customWidget/item_widget.cpp" line="123"/>
        <source>ERR</source>
        <translation>異常</translation>
    </message>
    <message>
        <location filename="../customWidget/item_widget.cpp" line="107"/>
        <location filename="../customWidget/item_widget.cpp" line="127"/>
        <source>WARNING</source>
        <translation>警告</translation>
    </message>
</context>
<context>
    <name>KylinDBus</name>
    <message>
        <location filename="../libNWDBus/src/kylin-dbus-interface.cpp" line="927"/>
        <source>Wired connection</source>
        <translation>有線連接</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="32"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="358"/>
        <source>PushButton</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="426"/>
        <location filename="../appUI/src/mainwindow.cpp" line="57"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="451"/>
        <location filename="../appUI/src/mainwindow.cpp" line="61"/>
        <source>Restart</source>
        <translation>重新檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="450"/>
        <source>Checking...</source>
        <translation>正在偵測中...</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="660"/>
        <source>Intranet IP</source>
        <translation>內網IP位址</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="683"/>
        <source>Intranet Web</source>
        <translation>內網網站位址</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="401"/>
        <location filename="../appUI/src/mainwindow.cpp" line="451"/>
        <source>Start</source>
        <translation>開始檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="74"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="125"/>
        <source>NetCheck</source>
        <translation>網路檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="131"/>
        <source>total 6 items</source>
        <translation>共6項</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="485"/>
        <source>Canceling...</source>
        <translation>取消中...</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="717"/>
        <source>InnerNet Check</source>
        <translation>內網檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="718"/>
        <source>Check whether the intranet is smooth</source>
        <translation>檢測內網IP是否可達，內網指定網站能否訪問</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="721"/>
        <source>Internet access</source>
        <translation>電腦能否上網</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="722"/>
        <source>Can user browse out net?</source>
        <translation>檢測您的電腦是否可以正常、流暢的訪問網頁</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="751"/>
        <source>checked %1 items, no issue</source>
        <translation>已偵測 %1 項，未發現問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="753"/>
        <location filename="../appUI/src/mainwindow.cpp" line="766"/>
        <source>checked %1 items, find %2 errs, %3 issues</source>
        <translation>已檢測 %1 項，發現 %2 項問題，%3 項提示問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="758"/>
        <source>checked %1 items, find %2 errs</source>
        <translation>已檢測 %1 項，發現 %2 項問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="762"/>
        <source>checked %1 items, find %2 issues</source>
        <translation>已檢測 %1 項，發現 %2 項提示問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="893"/>
        <source>Check interrupted, no issues found</source>
        <translation>檢測中斷，未發現問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="894"/>
        <source>We suggest that you conduct a complete inspection again</source>
        <translation>建議您重新進行完整檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="898"/>
        <source>Check interruption and found %1 issues</source>
        <translation>檢測中斷，發現 %1 項問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="899"/>
        <source>Please repair and retest</source>
        <translation>請修復後重新檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="907"/>
        <source>No problems found</source>
        <translation>未發現問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="908"/>
        <source>Please continue to maintain and regularly check up</source>
        <translation>請繼續保持並定時體檢</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="913"/>
        <location filename="../appUI/src/mainwindow.cpp" line="929"/>
        <source>Found %1 problem and %2 prompt problems</source>
        <translation>發現 %1 項問題，%2 項提示問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="914"/>
        <location filename="../appUI/src/mainwindow.cpp" line="919"/>
        <location filename="../appUI/src/mainwindow.cpp" line="924"/>
        <location filename="../appUI/src/mainwindow.cpp" line="930"/>
        <source>Please re-detect after repair</source>
        <translation>請修復後重新檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="918"/>
        <source>Found %1 problem</source>
        <translation>發現 %1 項問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.cpp" line="923"/>
        <source>Found %1 prompt problem</source>
        <translation>發現 %1 項提示問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="290"/>
        <location filename="../appUI/src/mainwindow.cpp" line="46"/>
        <location filename="../appUI/src/mainwindow.cpp" line="819"/>
        <source>Detect and resolve Network Faults</source>
        <translation>全面診斷網路故障，解決無法上網問題</translation>
    </message>
    <message>
        <location filename="../appUI/src/mainwindow.ui" line="249"/>
        <location filename="../appUI/src/mainwindow.cpp" line="43"/>
        <location filename="../appUI/src/mainwindow.cpp" line="820"/>
        <source>Detect Network Faults</source>
        <translation>網路故障 一鍵檢測</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="66"/>
        <location filename="../appUI/src/menumodule.cpp" line="102"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="68"/>
        <location filename="../appUI/src/menumodule.cpp" line="100"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="70"/>
        <location filename="../appUI/src/menumodule.cpp" line="105"/>
        <source>Configure</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="72"/>
        <location filename="../appUI/src/menumodule.cpp" line="98"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="135"/>
        <source>Network-check-tool is a software that can quickly detect,diagnose,and optimize networks.</source>
        <translation>網路檢測工具是一款快速檢測、診斷、優化網路的軟體，操作流程簡單便捷。</translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="134"/>
        <location filename="../appUI/src/menumodule.cpp" line="259"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="263"/>
        <source>Network-check-tool is a software that can quickly detect, diagnose, and optimize networks. </source>
        <translation>網路檢測工具是一款快速檢測、診斷、優化網路的軟體，操作流程簡單便捷。 </translation>
    </message>
    <message>
        <location filename="../appUI/src/menumodule.cpp" line="275"/>
        <location filename="../appUI/src/menumodule.cpp" line="358"/>
        <location filename="../appUI/src/menumodule.cpp" line="366"/>
        <source>Service &amp; Support: </source>
        <translation>服務與支援： </translation>
    </message>
</context>
<context>
    <name>NetCheck</name>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="54"/>
        <location filename="../NetCheck/netcheck.cpp" line="55"/>
        <location filename="../NetCheck/netcheck.cpp" line="77"/>
        <location filename="../NetCheck/netcheck.cpp" line="78"/>
        <source>InnerNet Check</source>
        <translation>內網檢測</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="56"/>
        <location filename="../NetCheck/netcheck.cpp" line="76"/>
        <source>Can user browse inner net?</source>
        <translation>檢測內網IP是否可達，內網指定網站能否訪問</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="58"/>
        <location filename="../NetCheck/netcheck.cpp" line="59"/>
        <location filename="../NetCheck/netcheck.cpp" line="81"/>
        <location filename="../NetCheck/netcheck.cpp" line="82"/>
        <source>AccessNet Check</source>
        <translation>電腦能否上網</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="60"/>
        <location filename="../NetCheck/netcheck.cpp" line="80"/>
        <source>Can user browse out net?</source>
        <translation>檢測您的電腦是否可以正常、流暢的訪問網頁</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheck.cpp" line="76"/>
        <location filename="../NetCheck/netcheck.cpp" line="80"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
</context>
<context>
    <name>NetCheckHomePage</name>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="32"/>
        <source>Check and Repair</source>
        <translation>電腦故障 一鍵排查</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="36"/>
        <source>Detection and repair of computer problems</source>
        <translation>快速檢測修復故障問題，為您的電腦健康保駕護航</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="45"/>
        <source>NetCheck</source>
        <translation>網路檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="62"/>
        <source>Start</source>
        <translation>一鍵檢測</translation>
    </message>
    <message>
        <location filename="../appUI/src/netcheckhomepage.cpp" line="93"/>
        <source>IntraNetSet</source>
        <translation>內網檢測設置</translation>
    </message>
</context>
<context>
    <name>NetCheckThread</name>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="311"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="314"/>
        <source>Extranet normal</source>
        <translation>您的電腦可以正常、流暢地訪問網頁</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="311"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="314"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="324"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="329"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="345"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>OK</source>
        <translation>正常</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="317"/>
        <source>Extranet abnormal</source>
        <translation>您的電腦無法正常訪問網頁，存在網路異常</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="317"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="342"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="360"/>
        <source>ERR</source>
        <translation>異常</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="324"/>
        <location filename="../NetCheck/netcheckthread.cpp" line="329"/>
        <source>Intranet normal</source>
        <translation>內網檢測通暢</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="332"/>
        <source>Url can be accessed</source>
        <translation>內網指定網站能正常訪問</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="342"/>
        <source>Url cannot be accessed</source>
        <translation>內網指定網站無法正常訪問</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="335"/>
        <source>IP is reachable</source>
        <translation>內網IP位址可達</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="345"/>
        <source>IP is reachable，url cannot be accessed</source>
        <translation>內網IP位址可達，內網指定網站無法正常訪問</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="350"/>
        <source>IP is unreachable</source>
        <translation>內網IP位址不可達</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="353"/>
        <source>IP is unreachable，url can be accessed</source>
        <translation>內網IP位址不可達，內網指定網站能正常訪問</translation>
    </message>
    <message>
        <location filename="../NetCheck/netcheckthread.cpp" line="360"/>
        <source>IP is unreachable，url cannot be accessed</source>
        <translation>內網IP位址不可達，內網指定網站無法正常訪問</translation>
    </message>
</context>
<context>
    <name>ProxyCheck</name>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="38"/>
        <source>NetWork Proxy</source>
        <translation>瀏覽器配置</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Checking</source>
        <translation>檢測中...</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="40"/>
        <source>Proxy</source>
        <translation>瀏覽器配置</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="41"/>
        <location filename="../ProxyCheck/proxycheck.cpp" line="66"/>
        <source>Check whether the proxy is working?</source>
        <translation>檢測系統代理配置問題</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="71"/>
        <source>proxy disable</source>
        <translation>代理未開啟</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="74"/>
        <source>auto proxy normal</source>
        <translation>自動代理正常</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="77"/>
        <source>auto proxy abnormal</source>
        <translation>自動代理異常</translation>
    </message>
    <message>
        <location filename="../ProxyCheck/proxycheck.cpp" line="80"/>
        <source>manual proxy normal</source>
        <translation>手動代理正常</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../appUI/src/netcheckentr.cpp" line="40"/>
        <source>NetworkCheck</source>
        <translation>故障檢測</translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="30"/>
        <source>menu</source>
        <translation>功能表</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="38"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="51"/>
        <source>full screen</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../appUI/src/quad_btns_title_bar.cpp" line="64"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="83"/>
        <source>Kylin NM</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/utils.cpp" line="85"/>
        <source>kylin network applet desktop message</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>WiFiConfigDialog</name>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="38"/>
        <source>WLAN Authentication</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="49"/>
        <source>Input WLAN Information Please</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="50"/>
        <source>WLAN ID：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="51"/>
        <source>WLAN Name:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="52"/>
        <source>Password：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="53"/>
        <source>Cancl</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.cpp" line="54"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libNWDBus/src/wificonfigdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
</TS>
