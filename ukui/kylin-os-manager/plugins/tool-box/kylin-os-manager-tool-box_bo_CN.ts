<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AppItem</name>
    <message>
        <location filename="../appitem.cpp" line="114"/>
        <source>coming soon</source>
        <translation type="unfinished">མགྱོགས་པོ་ཡོང་གི་ཡོད།</translation>
    </message>
    <message>
        <location filename="../appitem.cpp" line="115"/>
        <source>More tools are coming soon</source>
        <translation type="unfinished">ཡོ་བྱད་དེ་བས་མང་བ་ཞིག་མགྱོགས་མྱུར་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugininterface.cpp" line="35"/>
        <source>ToolBox</source>
        <translation type="unfinished">ཡོ་བྱད་སྒམ་ཆུང་།</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <location filename="../toolboxwidget.cpp" line="63"/>
        <source>My Tool</source>
        <translation>ངའི་ལག་ཆ།</translation>
    </message>
    <message>
        <location filename="../toolboxwidget.cpp" line="67"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>མ་ལག་སྣ་ཚོགས་ཀྱི་ཡོ་བྱད་ཀྱིས་ཁྱོད་ལ་རོགས་བྱས་ནས་རྩིས་འཁོར་དེ་བས་ལེགས་པའི་སྒོ་</translation>
    </message>
</context>
</TS>
