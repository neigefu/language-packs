<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>AppItem</name>
    <message>
        <location filename="../appitem.cpp" line="114"/>
        <source>coming soon</source>
        <translation type="unfinished">ئۇزاققا قالماي كىلىدۇ</translation>
    </message>
    <message>
        <location filename="../appitem.cpp" line="115"/>
        <source>More tools are coming soon</source>
        <translation type="unfinished">تېخىمۇ كۆپ قوراللار تېزلا كېلىدۇ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugininterface.cpp" line="35"/>
        <source>ToolBox</source>
        <translation type="unfinished">قورال ساندۇقى</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <location filename="../toolboxwidget.cpp" line="63"/>
        <source>My Tool</source>
        <translation>قورالىم</translation>
    </message>
    <message>
        <location filename="../toolboxwidget.cpp" line="67"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>ھەر خىل سىستېما قوراللىرى كومپىيۇتېرنى تېخىمۇ ياخشى ئىشلىتىشىڭىزگە ياردەم بېرىدۇ</translation>
    </message>
</context>
</TS>
