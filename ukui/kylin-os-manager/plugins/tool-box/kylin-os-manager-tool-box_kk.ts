<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>AppItem</name>
    <message>
        <location filename="../appitem.cpp" line="114"/>
        <source>coming soon</source>
        <translation type="unfinished">Жақында</translation>
    </message>
    <message>
        <location filename="../appitem.cpp" line="115"/>
        <source>More tools are coming soon</source>
        <translation type="unfinished">Жақын арада қосымша құралдар келе жатыр</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugininterface.cpp" line="35"/>
        <source>ToolBox</source>
        <translation type="unfinished">Құралдар жәшігі</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <location filename="../toolboxwidget.cpp" line="63"/>
        <source>My Tool</source>
        <translation>Менің құралым</translation>
    </message>
    <message>
        <location filename="../toolboxwidget.cpp" line="67"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>Компьютерді жақсырақ пайдалануға көмектесетін жүйе құралдарының барлық түрлері</translation>
    </message>
</context>
</TS>
