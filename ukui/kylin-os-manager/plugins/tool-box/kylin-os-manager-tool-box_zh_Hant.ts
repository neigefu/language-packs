<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AppItem</name>
    <message>
        <location filename="../appitem.cpp" line="114"/>
        <source>coming soon</source>
        <translation>即將推出</translation>
    </message>
    <message>
        <location filename="../appitem.cpp" line="115"/>
        <source>More tools are coming soon</source>
        <translation>更多工具即將推出</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../plugininterface.cpp" line="35"/>
        <source>ToolBox</source>
        <translation>工具箱</translation>
    </message>
</context>
<context>
    <name>ToolBoxWidget</name>
    <message>
        <location filename="../toolboxwidget.cpp" line="63"/>
        <source>My Tool</source>
        <translation>我的工具</translation>
    </message>
    <message>
        <location filename="../toolboxwidget.cpp" line="67"/>
        <source>All kinds of system tools to help you better use the computer</source>
        <translation>各種系統工具，説明您更好地使用計算機</translation>
    </message>
</context>
</TS>
