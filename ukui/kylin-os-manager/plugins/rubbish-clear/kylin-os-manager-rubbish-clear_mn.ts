<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="105"/>
        <location filename="../cleandetailveiw.cpp" line="110"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>ᠶᠠᠭ ᠰᠢᠷᠪᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="113"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="119"/>
        <source>Cleanup</source>
        <translation>ᠨᠢᠭᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="129"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="136"/>
        <source>Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="176"/>
        <location filename="../cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="184"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠪᠠᠭᠯᠠᠭ᠎ᠠ᠂ ᠪᠠᠭᠠᠰᠬᠠᠭᠰᠠᠨ ᠵᠢᠷᠤᠭ᠂ ᠬᠠᠢᠭᠴᠢ ᠵᠢᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠵᠡᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="195"/>
        <location filename="../cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠭᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="219"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="858"/>
        <source>Clearance completed</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠳᠠᠭᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="859"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤᠯᠠᠮ ᠴᠡᠪᠡᠷᠬᠡᠨ ᠪᠣᠯᠤᠭᠰᠠᠨ᠂ ᠦᠷᠬᠦᠯᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠳᠡᠭ ᠵᠤᠷᠰᠢᠯ ᠵᠢᠨᠨ ᠪᠠᠷᠢᠮᠳᠠᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="258"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="340"/>
        <location filename="../cleandetailveiw.cpp" line="820"/>
        <source>Computer cleanup in progress...</source>
        <translation>ᠶᠠᠭ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="464"/>
        <location filename="../cleandetailveiw.cpp" line="466"/>
        <source>Cleanable cache </source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source> items</source>
        <translation> ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="533"/>
        <source> historical use traces</source>
        <translation> ᠲᠡᠤᠬᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="592"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠠᠭᠤᠯᠭ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="642"/>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <source>Cleanable Cache</source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="654"/>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <source>Cleanable Cookie</source>
        <translation>Cookie ᠢ᠋/ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="697"/>
        <location filename="../cleandetailveiw.cpp" line="699"/>
        <source>Clear cache </source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source>Clear cookie </source>
        <translation>cookie ᠢ᠋/ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠭᠠᠷᠤᠭᠰᠠᠨ᠂ ᠳᠤᠭᠯᠠᠭᠠᠮ ᠲᠣᠭᠯᠠᠭᠰᠠᠨ᠂ ᠪᠠᠷᠠᠭ᠎ᠠ ᠬᠤᠳᠠᠯᠳᠤᠨ ᠠᠪᠤᠭᠰᠠᠨ ᠵᠡᠷᠭᠡ ᠮᠥᠷ ᠵᠢᠨᠨᠴᠡᠪᠡᠷᠯᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠤᠯᠠ ᠮᠦᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <source>Cleanable browser </source>
        <translation>ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="585"/>
        <source>system cache</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>cookie record</source>
        <translation>Cookie ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>history trace</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="598"/>
        <location filename="../cleandetailveiw.cpp" line="613"/>
        <source> item,</source>
        <translation> ᠳᠦᠷᠦᠯ,</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item</source>
        <translation> ᠵᠤᠷᠪᠤᠰ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="602"/>
        <source>Complete</source>
        <translation>ᠰᠢᠷᠪᠢᠵᠤ ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source>Clear </source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source> historical traces</source>
        <translation> ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="833"/>
        <location filename="../cleandetailveiw.cpp" line="836"/>
        <location filename="../cleandetailveiw.cpp" line="839"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <location filename="../cleandetailveiw.cpp" line="969"/>
        <source>Cleaning up......</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ......</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="952"/>
        <location filename="../cleandetailveiw.cpp" line="953"/>
        <location filename="../cleandetailveiw.cpp" line="954"/>
        <source>Cleaning up</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <location filename="../cleandetailveiw.cpp" line="959"/>
        <source>Cleaning up..</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ..</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <location filename="../cleandetailveiw.cpp" line="964"/>
        <source>Cleaning up....</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ....</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="97"/>
        <source>SystemCache</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="117"/>
        <source>HistoryTrace</source>
        <translation>ᠲᠡᠤᠬᠡᠨ ᠤᠯᠠ ᠮᠦᠷ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="77"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠪᠡᠯ ᠲᠠᠨ ᠤ᠋ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠬᠤᠷᠳᠤᠨ ᠮᠦᠷᠳᠡᠬᠡᠨ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠪᠠᠢᠵᠤ ᠴᠢᠳᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="149"/>
        <source>StartClear</source>
        <translation>ᠡᠬᠢᠯᠡᠵᠤ ᠰᠢᠷᠪᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <source>Cleanup Package Cache</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭᠠᠨ ᠤ᠋ ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="236"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠭᠰᠠᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="329"/>
        <location filename="../clearmainwidget.cpp" line="342"/>
        <source>Cleanup FireFox Cache</source>
        <translation>FireFox ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="387"/>
        <location filename="../clearmainwidget.cpp" line="400"/>
        <source>Cleanup Chromium Cache</source>
        <translation>Chromium ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="358"/>
        <location filename="../clearmainwidget.cpp" line="371"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>ᠴᠢ ᠠᠨ ᠰᠢᠨ ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="71"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>ᠺᠣᠮᠫᠢᠦ᠋ᠲ᠋ᠧᠷ ᠤ᠋ᠨ ᠬᠣᠭ ᠨᠢᠭᠡ ᠳᠠᠷᠤᠪᠴᠢ ᠪᠡᠷ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="98"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠪᠠᠭᠠᠯᠠᠭ᠎ᠠ᠂ ᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠵᠢᠷᠤᠭ ᠵᠢᠴᠢ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠵᠡᠷᠭᠡ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="107"/>
        <source>Cookies</source>
        <translation>cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <source>Clean up Internet history,cookies</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠳ᠋ᠤ᠌ ᠭᠠᠷᠤᠭᠰᠠᠨ ᠮᠥᠷ᠂cookies ᠵᠡᠷᠭᠡ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <source>Clean up system usage traces</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠣᠷᠣᠮ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <source>cleanup trash box</source>
        <translation>ᠬᠤᠷᠢᠶᠠᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="333"/>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Firefox ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="391"/>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>Chromium ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="362"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>ᠴᠢ ᠠᠨ ᠰᠢᠨ ᠬᠠᠢᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="395"/>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <source>Clean up the Chromium Internet records</source>
        <translation>清理Chromium浏览器上网记录</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="251"/>
        <source>Clean up the recently opened documents records</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤ᠋ᠨ ᠰᠡᠭᠦᠯ ᠳ᠋ᠤ᠌ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠲᠤᠺᠦᠢᠮᠸᠨ᠋ᠲ ᠤ᠋ᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Delete the command history</source>
        <translation>ᠵᠠᠷᠯᠢᠭ ᠤ᠋ᠨ ᠲᠡᠤᠬᠡᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="280"/>
        <source>Cache Items</source>
        <translation>ᠨᠠᠮᠬᠠᠳᠬᠠᠯ ᠫᠷᠤᠵᠸᠺᠲ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="295"/>
        <source>Cookies Items</source>
        <translation>Cookies ᠫᠷᠤᠵᠸᠺᠲ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="310"/>
        <source>Trace Items</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠰᠡᠨ ᠳᠡᠮᠳᠡᠭᠯᠡᠯ</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠫᠷᠤᠵᠸᠺᠲ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠫᠷᠤᠵᠸᠺᠲ:</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠫᠷᠤᠵᠸᠺᠲ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation>ᠬᠤᠭ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
</TS>
