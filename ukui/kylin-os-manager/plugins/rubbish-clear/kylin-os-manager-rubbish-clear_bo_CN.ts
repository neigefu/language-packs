<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="105"/>
        <location filename="../cleandetailveiw.cpp" line="110"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>ཡར་ཐོན་གྱི་རྩིས་འཁོར་ལ་ཞིབ་བཤེར་བྱེད་བཞིན་</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="113"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="119"/>
        <source>Cleanup</source>
        <translation>གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="129"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="136"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="176"/>
        <location filename="../cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>མ་ལག་གི་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="184"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>ཁ་གསལ་བའི་ཐུམ་སྒྲིལ་དང་། མཐེབ་བཀྱག་དང་བལྟ་ཀློག་ཆས་ཀྱི་མགྱོགས་</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="195"/>
        <location filename="../cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>ཞིབ་ཕྲའི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="219"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>བག་ལེབ་ཀོར་མོ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="858"/>
        <source>Clearance completed</source>
        <translation>གཙང་བཤེར་བྱས་ཚར་སོང་།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="859"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>རྩིས་འཁོར་ལ་གསོན་ཤུགས་ཧ་ཅང་ཆེན་པོ་ཡོད་པས་གཙང་སྦྲའི་གོམས་གཤིས་རྒྱུན་འཁྱོངས་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="258"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="340"/>
        <location filename="../cleandetailveiw.cpp" line="820"/>
        <source>Computer cleanup in progress...</source>
        <translation>རྩིས་འཁོར་གཙང་བཤེར་བྱེད་བཞིན་པའི་སྒང་རེད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="464"/>
        <location filename="../cleandetailveiw.cpp" line="466"/>
        <source>Cleanable cache </source>
        <translation>གཙང་སྦྲ་དོད་པའི་མགྱོགས་ཚད། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source> items</source>
        <translation> རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="533"/>
        <source> historical use traces</source>
        <translation> ལོ་རྒྱུས་ཀྱི་བེད་སྤྱོད་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="592"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས་དོན་གང་ཡང་མེད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="642"/>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <source>Cleanable Cache</source>
        <translation>གཙང་སྦྲ་དོད་པའི་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="654"/>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <source>Cleanable Cookie</source>
        <translation>གཙང་སྦྲ་དོད་པའི་ཀ་ར་གོ་རེ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="697"/>
        <location filename="../cleandetailveiw.cpp" line="699"/>
        <source>Clear cache </source>
        <translation>ཁ་གསལ་གཏིང་གསལ་ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source>Clear cookie </source>
        <translation>བག་ལེབ་ཀོར་མོ་གཙང་མ། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>དྲ་སྦྲེལ་དང་། རོལ་རྩེད། ཟོག་ཉོའི་ལོ་རྒྱུས་སོགས་ཁ་གསལ་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation>མ་ལག་བཀོལ་སྤྱོད་ཀྱི་རྗེས་ཤུལ་ཁ་གསལ་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <source>Cleanable browser </source>
        <translation>གཙང་སྦྲ་དོད་པའི་བལྟ་ཆས། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="585"/>
        <source>system cache</source>
        <translation>མ་ལག་གི་མགྱོགས་ཚད།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>cookie record</source>
        <translation>བག་ལེབ་ཀོར་མོའི་ཟིན་ཐོ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>history trace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="598"/>
        <location filename="../cleandetailveiw.cpp" line="613"/>
        <source> item,</source>
        <translation> རྣམ་གྲངས་དང་།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item</source>
        <translation> རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="602"/>
        <source>Complete</source>
        <translation>ཆ་ཚང་བ་ཞིག་རེད་</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source>Clear </source>
        <translation>གསལ་པོར་བཤད་ན། </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source> historical traces</source>
        <translation> ལོ་རྒྱུས་ཀྱི་རྗེས་ཤུལ།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="833"/>
        <location filename="../cleandetailveiw.cpp" line="836"/>
        <location filename="../cleandetailveiw.cpp" line="839"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <location filename="../cleandetailveiw.cpp" line="969"/>
        <source>Cleaning up......</source>
        <translation>གཙང་སྦྲ་བྱེད་པ......</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="952"/>
        <location filename="../cleandetailveiw.cpp" line="953"/>
        <location filename="../cleandetailveiw.cpp" line="954"/>
        <source>Cleaning up</source>
        <translation>གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <location filename="../cleandetailveiw.cpp" line="959"/>
        <source>Cleaning up..</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <location filename="../cleandetailveiw.cpp" line="964"/>
        <source>Cleaning up....</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="97"/>
        <source>SystemCache</source>
        <translation>མ་ལག་གི་ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="117"/>
        <source>HistoryTrace</source>
        <translation>ལོ་རྒྱུས་ཀྱི་བཤུལ་ལམ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="71"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>རྩིས་འཁོར་གྱི་གད་སྙིགས་དང་ལྡེ་མིག་གཅིག་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="77"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>དུས་བཀག་ལྟར་གཙང་སྦྲ་བྱས་ཏེ་གློག་ཀླད་ཀྱི་འོད་དང་བདེ་འཇགས་ལ་སྲུང་སྐྱོབ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="107"/>
        <source>Cookies</source>
        <translation>བག་ལེབ་ཀོར་མོ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="149"/>
        <source>StartClear</source>
        <translation>འགོ་རྩོམ་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <source>Cleanup Package Cache</source>
        <translation>གཙང་བཤེར་བྱེད་པའི་ཁུག་མའི་ནང་དུ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="236"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>གཙང་བཤེར་བྱས་པའི་མཐེ་བོང་བསྒྲེངས་པའི་མྱུར་ཚད།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="358"/>
        <location filename="../clearmainwidget.cpp" line="371"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>ཨན་ཞིན་བཤར་ཆས་གཙང་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <source>cleanup trash box</source>
        <translation>གད་སྙིགས་བླུགས་སྒམ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="362"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>ཆི་ཨན་ཞིན་བཤར་ཆས་།ནང་དུ་གྲོན་ཆུང་བྱས་པའི་ཀ་ར་གོ་རེ་གཙང་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="333"/>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>མེ་སྐྱོན་ཁྲོད་གྲོན་ཆུང་བྱས་པའི་ཀ་ར་གོ་རེ་གཙང་བཤེར་བྱས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="251"/>
        <source>Clean up the recently opened documents records</source>
        <translation>ཉེ་ཆར་ཁ་ཕྱེས་པའི་ཡིག་ཆའི་ཟིན་ཐོ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Delete the command history</source>
        <translation>བཀོད་འདོམས་ཀྱི་ལོ་རྒྱུས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="329"/>
        <location filename="../clearmainwidget.cpp" line="342"/>
        <source>Cleanup FireFox Cache</source>
        <translation>ཝ་དམར་བཤར་ཆས་གཙང་བཤེར་ལྷོད་གསོག་</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="387"/>
        <location filename="../clearmainwidget.cpp" line="400"/>
        <source>Cleanup Chromium Cache</source>
        <translation>ཀོའུ་ཀོའུ་བཤར་ཆས་གཙང་བཤེར་དལ་གསོག་།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="391"/>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>ཀོའུ་ཀོ་བཤར་ཆས་Cookiesགཙང་བཤེར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="395"/>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <source>Clean up the Chromium Internet records</source>
        <translation>ཀོའུ་ཀོའུ་བཤར་ཆས་ནང་གི་ཟིན་ཐོ་གཙང་བཤེར་།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="98"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>ཐུམ་སྒྲིལ་དང་མཐེ་བོང་གཙང་བཤེར་བྱས་ནས་གད་སྙིགས་བླུགས་སྒམ་དུ་བསྐྱར་དུ་བེད་སྤྱོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <source>Clean up Internet history,cookies</source>
        <translation>དྲ་རྒྱའི་ལོ་རྒྱུས་དང་ཀ་ར་གོ་རེ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <source>Clean up system usage traces</source>
        <translation>མ་ལག་གི་བཀོལ་སྤྱོད་རྗེས་ཤུལ་གཙང་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="280"/>
        <source>Cache Items</source>
        <translation>མྱུར་ཚད་མགྱོགས་པའི་དངོས་རྫས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="295"/>
        <source>Cookies Items</source>
        <translation>བག་ལེབ་ཀོར་མོའི་དངོས་རྫས།</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="310"/>
        <source>Trace Items</source>
        <translation>རྗེས་ཤུལ་གྱི་རྣམ་གྲངས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>གཙང་སྦྲ་དོད་པའི་རྣམ་གྲངས་བདམས་ཐོན་མ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>གཙང་སྦྲའི་རྣམ་གྲངས་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>གཙང་སྦྲ་བྱེད་དགོས་པའི་དངོས་རྫས་མེད་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation>གད་སྙིགས་གཙང་བཤེར།</translation>
    </message>
</context>
</TS>
