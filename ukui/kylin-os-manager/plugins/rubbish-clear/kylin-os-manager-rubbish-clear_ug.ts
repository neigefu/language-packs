<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="105"/>
        <location filename="../cleandetailveiw.cpp" line="110"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>كومپيۇتېرنى سىكاننېرلاش داۋامى...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="113"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="119"/>
        <source>Cleanup</source>
        <translation>تازىلاش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="129"/>
        <source>Return</source>
        <translation>قايتىش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="136"/>
        <source>Finish</source>
        <translation>تاماملاش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="176"/>
        <location filename="../cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>سىستېما cache</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="184"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>بوغچا،كىچىك نادىر ۋە تور كۆرگۈچنىڭ كاچاتلىرىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="195"/>
        <location filename="../cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>تەپسىلاتى</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="219"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>پېچىنە-پىرەنىكلەر</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="858"/>
        <source>Clearance completed</source>
        <translation>تازىلاش تاماملاندى</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="859"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>كومپىيۇتېرنىڭ تېتىكلىكى ئىنتايىن كۈچلۈك، تازىلىق ئادىتىنى داۋاملاشتۇرۇڭ</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="258"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>تارىخىي ئىزلار</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="340"/>
        <location filename="../cleandetailveiw.cpp" line="820"/>
        <source>Computer cleanup in progress...</source>
        <translation>كومپىيۇتېر تازىلاش خىزمىتى داۋاملىشىۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="464"/>
        <location filename="../cleandetailveiw.cpp" line="466"/>
        <source>Cleanable cache </source>
        <translation>تازىلىغىلى بولىدىغان كاچكا </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source> items</source>
        <translation> تۈر</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="533"/>
        <source> historical use traces</source>
        <translation> تارىخىي ئىشلىتىلىش ئىزلىرى</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="592"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>تازىلىغۇدەك ھېچنېمە يوق.</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="642"/>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <source>Cleanable Cache</source>
        <translation>تازىلاشقا بولىدىغان كاچكا</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="654"/>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <source>Cleanable Cookie</source>
        <translation>پاكىزە ئاشپەزلەر</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="697"/>
        <location filename="../cleandetailveiw.cpp" line="699"/>
        <source>Clear cache </source>
        <translation>Cache نى تازىلاش </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source>Clear cookie </source>
        <translation>پىچىنە-پىرەنىكنى تازىلاش </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>تور ئويۇنى،مال سېتىۋېلىش تارىخى قاتارلىقلارنى ئېنىقلاش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation>تازىلاش سىستېمىسى ئىشلىتىش ئىز قوغلاش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <source>Cleanable browser </source>
        <translation>پاكىزە تور كۆرگۈچ </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="585"/>
        <source>system cache</source>
        <translation>سىستېما cache</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>cookie record</source>
        <translation>cookie پىلاستىنكىسى</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>history trace</source>
        <translation>تارىخ ئىزنالىرى</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="598"/>
        <location filename="../cleandetailveiw.cpp" line="613"/>
        <source> item,</source>
        <translation> ئەزا ،</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item</source>
        <translation> تۈر</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="602"/>
        <source>Complete</source>
        <translation>تامام</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source>Clear </source>
        <translation>تازىلاش </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source> historical traces</source>
        <translation> تارىخىي ئىزلار</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="833"/>
        <location filename="../cleandetailveiw.cpp" line="836"/>
        <location filename="../cleandetailveiw.cpp" line="839"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <location filename="../cleandetailveiw.cpp" line="969"/>
        <source>Cleaning up......</source>
        <translation>تازىلى...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="952"/>
        <location filename="../cleandetailveiw.cpp" line="953"/>
        <location filename="../cleandetailveiw.cpp" line="954"/>
        <source>Cleaning up</source>
        <translation>تازىلاش</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <location filename="../cleandetailveiw.cpp" line="959"/>
        <source>Cleaning up..</source>
        <translation>تازىلاش..</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <location filename="../cleandetailveiw.cpp" line="964"/>
        <source>Cleaning up....</source>
        <translation>تازلاش....</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="97"/>
        <source>SystemCache</source>
        <translation>SystemCache</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="117"/>
        <source>HistoryTrace</source>
        <translation>HistoryTrace</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="77"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>كومپيۇتېرنىڭ يورۇقلۇقى ۋە بىخەتەرلىكىنى ساقلاش ئۈچۈن قەرەللىك تازىلاڭ</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="107"/>
        <source>Cookies</source>
        <translation>پېچىنە-پىرەنىكلەر</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="149"/>
        <source>StartClear</source>
        <translation>StartClear</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <source>Cleanup Package Cache</source>
        <translation>قاچىلاش بوغچىسىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="236"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>كىچىك نادىرلار Cache نى تازىلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="358"/>
        <location filename="../clearmainwidget.cpp" line="371"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>Qaxbrowser Cache نى تازىلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <source>cleanup trash box</source>
        <translation>ئەخلەت ساندۇقىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="362"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>Qaxbrowser دا ساقلىغان Cookies لارنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="333"/>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Firefox دا ساقلىغان Cookie لارنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="251"/>
        <source>Clean up the recently opened documents records</source>
        <translation>يېقىندا ئېچىلغان ھۆججەت خاتىرىسىنى تازىلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Delete the command history</source>
        <translation>بۇيرۇق تارىخىنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="329"/>
        <location filename="../clearmainwidget.cpp" line="342"/>
        <source>Cleanup FireFox Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="387"/>
        <location filename="../clearmainwidget.cpp" line="400"/>
        <source>Cleanup Chromium Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="391"/>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="395"/>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <source>Clean up the Chromium Internet records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="71"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>كومپىيۇتېر ئەخلەتلىرى بىر ئاچقۇچنى تازىلاڭلار</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="98"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>بوغچا، نادىرلارنى تازىلاپ، قايتا يىغىۋىتىش ساندۇقى</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <source>Clean up Internet history,cookies</source>
        <translation>تور تارىخىنى تازىلاپ، پېچىنە-پىرەنىكلەرنى</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <source>Clean up system usage traces</source>
        <translation>ئېنىقلاش سىستېمىسى ئىشلىتىش ئىز قوغلاش</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="280"/>
        <source>Cache Items</source>
        <translation>Cache تۈرلىرى</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="295"/>
        <source>Cookies Items</source>
        <translation>Cookies تۈرلىرى</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="310"/>
        <source>Trace Items</source>
        <translation>ئىز تۈرلىرى</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>تازىلىغىلى بولىدىغان بۇيۇملار تاللاپ چىقىلمىغان!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>پاكىز بۇيۇملار:</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>تازىلىماقچى بولغان بۇيۇملار يوق</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
