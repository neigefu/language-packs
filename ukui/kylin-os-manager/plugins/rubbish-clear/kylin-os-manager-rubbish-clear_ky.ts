<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="105"/>
        <location filename="../cleandetailveiw.cpp" line="110"/>
        <location filename="../cleandetailveiw.cpp" line="287"/>
        <source>Computer scan in progress...</source>
        <translation>Компьютердик сканерлеу жүрүшүндө...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="113"/>
        <source>Cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="119"/>
        <source>Cleanup</source>
        <translation>Тазалоо</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="129"/>
        <source>Return</source>
        <translation>Кайтып келүү</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="136"/>
        <source>Finish</source>
        <translation>Бүтүрүү</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="176"/>
        <location filename="../cleandetailveiw.cpp" line="284"/>
        <source>System cache</source>
        <translation>Системалык кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="184"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>Так пакети миниатюралар жана браузер кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="195"/>
        <location filename="../cleandetailveiw.cpp" line="235"/>
        <source>Details</source>
        <translation>Майда-чүйдөсүнө</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="219"/>
        <location filename="../cleandetailveiw.cpp" line="286"/>
        <source>Cookies</source>
        <translation>Кукилер</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="858"/>
        <source>Clearance completed</source>
        <translation>Тазалоо аяктады</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="859"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>Компьютер абдан энергиялуу, сураныч, тазалоо адаттарын сактоо</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="258"/>
        <location filename="../cleandetailveiw.cpp" line="285"/>
        <source>Historical trace</source>
        <translation>Тарыхый изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="340"/>
        <location filename="../cleandetailveiw.cpp" line="820"/>
        <source>Computer cleanup in progress...</source>
        <translation>Компьютердик тазалоо жүрүшүндө...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="464"/>
        <location filename="../cleandetailveiw.cpp" line="466"/>
        <source>Cleanable cache </source>
        <translation>Таза кэш </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source> items</source>
        <translation> элементтер</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="533"/>
        <source> historical use traces</source>
        <translation> тарыхый колдонуу изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="592"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>Тазалоо үчүн эч нерсе жок.</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="642"/>
        <location filename="../cleandetailveiw.cpp" line="643"/>
        <source>Cleanable Cache</source>
        <translation>Таза кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="654"/>
        <location filename="../cleandetailveiw.cpp" line="656"/>
        <source>Cleanable Cookie</source>
        <translation>Таза куки</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="697"/>
        <location filename="../cleandetailveiw.cpp" line="699"/>
        <source>Clear cache </source>
        <translation>Кэшти тазалоо </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="671"/>
        <source>Clear cookie </source>
        <translation>Ачык-айкын куки </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="227"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>Ачык интернет оюндар соода тарыхы ж.б</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="266"/>
        <source>Clear system usage traces</source>
        <translation>Жүйені пайдалануды бақылауды тазалаңыз</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="472"/>
        <source>Cleanable browser </source>
        <translation>Тазалоочу браузер </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="585"/>
        <source>system cache</source>
        <translation>система кэш</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="586"/>
        <source>cookie record</source>
        <translation>куки рекорду</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="587"/>
        <source>history trace</source>
        <translation>тарых изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="598"/>
        <location filename="../cleandetailveiw.cpp" line="613"/>
        <source> item,</source>
        <translation> элемент,</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="599"/>
        <location filename="../cleandetailveiw.cpp" line="614"/>
        <source> item</source>
        <translation> элемент</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="602"/>
        <source>Complete</source>
        <translation>Толук</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source>Clear </source>
        <translation>Тазалоо </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="738"/>
        <source> historical traces</source>
        <translation> тарыхый изи</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="833"/>
        <location filename="../cleandetailveiw.cpp" line="836"/>
        <location filename="../cleandetailveiw.cpp" line="839"/>
        <location filename="../cleandetailveiw.cpp" line="967"/>
        <location filename="../cleandetailveiw.cpp" line="968"/>
        <location filename="../cleandetailveiw.cpp" line="969"/>
        <source>Cleaning up......</source>
        <translation>Тазалоо ......</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="952"/>
        <location filename="../cleandetailveiw.cpp" line="953"/>
        <location filename="../cleandetailveiw.cpp" line="954"/>
        <source>Cleaning up</source>
        <translation>Тазалоо</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="957"/>
        <location filename="../cleandetailveiw.cpp" line="958"/>
        <location filename="../cleandetailveiw.cpp" line="959"/>
        <source>Cleaning up..</source>
        <translation>Тазалоо..</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="962"/>
        <location filename="../cleandetailveiw.cpp" line="963"/>
        <location filename="../cleandetailveiw.cpp" line="964"/>
        <source>Cleaning up....</source>
        <translation>Тазалоо....</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="97"/>
        <source>SystemCache</source>
        <translation>SystemCache</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="117"/>
        <source>HistoryTrace</source>
        <translation>HistoryTrace</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="77"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>Компьютердин жарыгын жана коопсуздугун сактоо үчүн дайыма тазалагыла</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="107"/>
        <source>Cookies</source>
        <translation>Кукилер</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="149"/>
        <source>StartClear</source>
        <translation>StartClear</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <source>Cleanup Package Cache</source>
        <translation>Тазалоо пакети Кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="236"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>Тазалоо Миниатюра Кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="237"/>
        <location filename="../clearmainwidget.cpp" line="358"/>
        <location filename="../clearmainwidget.cpp" line="371"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>Тазалоо Каксброузер Кэш</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <location filename="../clearmainwidget.cpp" line="230"/>
        <location filename="../clearmainwidget.cpp" line="238"/>
        <source>cleanup trash box</source>
        <translation>тазалоо таштанды кутуча</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="362"/>
        <location filename="../clearmainwidget.cpp" line="375"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>Касброузердеги кукилерди тазалоо</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="245"/>
        <location filename="../clearmainwidget.cpp" line="333"/>
        <location filename="../clearmainwidget.cpp" line="346"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>Отко үнөмдөлгөн кукилер тазалоо</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="251"/>
        <source>Clean up the recently opened documents records</source>
        <translation>Жакында ачылган документтердин жазууларын тазалоо</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="252"/>
        <source>Delete the command history</source>
        <translation>Команда тарыхын жоготуу</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="329"/>
        <location filename="../clearmainwidget.cpp" line="342"/>
        <source>Cleanup FireFox Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="387"/>
        <location filename="../clearmainwidget.cpp" line="400"/>
        <source>Cleanup Chromium Cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="391"/>
        <location filename="../clearmainwidget.cpp" line="404"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="395"/>
        <location filename="../clearmainwidget.cpp" line="408"/>
        <source>Clean up the Chromium Internet records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="71"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>Компьютердик таштандылар тазалоо үчүн бир ачкыч</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="98"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>Пакеттерди, миниатюраларды тазалап, бинтти кайра иштетүү</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="108"/>
        <source>Clean up Internet history,cookies</source>
        <translation>Интернет тарыхын тазалоо,кукилер</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="118"/>
        <source>Clean up system usage traces</source>
        <translation>Жүйені пайдалануды бақылауды тазалаңыз</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="280"/>
        <source>Cache Items</source>
        <translation>Кэш заттары</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="295"/>
        <source>Cookies Items</source>
        <translation>Кукилер элементтери</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="310"/>
        <source>Trace Items</source>
        <translation>Изи элементтер</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>Тазалоочу заттар тандалган жок!</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>албетте</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>Таза заттар:</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>Тазалоо үчүн эч кандай буюмдар</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="38"/>
        <source>RubbishClear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
