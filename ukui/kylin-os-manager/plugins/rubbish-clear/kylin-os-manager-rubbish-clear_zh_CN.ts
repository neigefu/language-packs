<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KylinRubbishClear::CleandetailVeiw</name>
    <message>
        <location filename="../cleandetailveiw.cpp" line="91"/>
        <location filename="../cleandetailveiw.cpp" line="95"/>
        <location filename="../cleandetailveiw.cpp" line="267"/>
        <source>Computer scan in progress...</source>
        <translation>正在扫描中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="104"/>
        <source>Cancel</source>
        <translation>取消扫描</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="110"/>
        <source>Cleanup</source>
        <translation>一键清理</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="120"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="127"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="164"/>
        <location filename="../cleandetailveiw.cpp" line="264"/>
        <source>System cache</source>
        <translation>系统缓存</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="174"/>
        <source>Clear package、thumbnails and browser cache</source>
        <translation>清理包、缩略图和浏览器缓存等</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="181"/>
        <location filename="../cleandetailveiw.cpp" line="213"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="196"/>
        <location filename="../cleandetailveiw.cpp" line="266"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="843"/>
        <source>Clearance completed</source>
        <translation>清理完成</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="844"/>
        <source>Computer is very energetic, please keep cleaning habits</source>
        <translation>电脑越来越干净啦，请保持常清理习惯</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="237"/>
        <location filename="../cleandetailveiw.cpp" line="265"/>
        <source>Historical trace</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="320"/>
        <location filename="../cleandetailveiw.cpp" line="805"/>
        <source>Computer cleanup in progress...</source>
        <translation>正在清理中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="444"/>
        <location filename="../cleandetailveiw.cpp" line="446"/>
        <source>Cleanable cache </source>
        <translation>可清理缓存 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="452"/>
        <location filename="../cleandetailveiw.cpp" line="655"/>
        <source> items</source>
        <translation> 个</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="513"/>
        <source> historical use traces</source>
        <translation> 历史使用痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="572"/>
        <source>There&apos;s nothing to clean up.</source>
        <translation>无可清理内容</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="618"/>
        <location filename="../cleandetailveiw.cpp" line="619"/>
        <source>Cleanable Cache</source>
        <translation>可清理缓存</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="628"/>
        <location filename="../cleandetailveiw.cpp" line="630"/>
        <source>Cleanable Cookie</source>
        <translation>可清理 Cookie</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="681"/>
        <location filename="../cleandetailveiw.cpp" line="683"/>
        <source>Clear cache </source>
        <translation>清理缓存 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="655"/>
        <source>Clear cookie </source>
        <translation>清理 Cookie </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="206"/>
        <source>Clear internet、games、shopping history, etc</source>
        <translation>清理上网、游戏、购物等历史</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="247"/>
        <source>Clear system usage traces</source>
        <translation>清理系统使用痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="452"/>
        <source>Cleanable browser </source>
        <translation>可清理浏览器 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="565"/>
        <source>system cache</source>
        <translation>系统缓存 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="566"/>
        <source>cookie record</source>
        <translation>Cookie 记录 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="567"/>
        <source>history trace</source>
        <translation>历史痕迹 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="578"/>
        <location filename="../cleandetailveiw.cpp" line="593"/>
        <source> item,</source>
        <translation> 项，</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="579"/>
        <location filename="../cleandetailveiw.cpp" line="594"/>
        <source> item</source>
        <translation> 条</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="582"/>
        <source>Complete</source>
        <translation>扫描完成</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="722"/>
        <source>Clear </source>
        <translation>清理 </translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="722"/>
        <source> historical traces</source>
        <translation> 历史痕迹</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="818"/>
        <location filename="../cleandetailveiw.cpp" line="821"/>
        <location filename="../cleandetailveiw.cpp" line="824"/>
        <location filename="../cleandetailveiw.cpp" line="952"/>
        <location filename="../cleandetailveiw.cpp" line="953"/>
        <location filename="../cleandetailveiw.cpp" line="954"/>
        <source>Cleaning up......</source>
        <translation>清理中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="937"/>
        <location filename="../cleandetailveiw.cpp" line="938"/>
        <location filename="../cleandetailveiw.cpp" line="939"/>
        <source>Cleaning up</source>
        <translation>清理</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="942"/>
        <location filename="../cleandetailveiw.cpp" line="943"/>
        <location filename="../cleandetailveiw.cpp" line="944"/>
        <source>Cleaning up..</source>
        <translation>清理中...</translation>
    </message>
    <message>
        <location filename="../cleandetailveiw.cpp" line="947"/>
        <location filename="../cleandetailveiw.cpp" line="948"/>
        <location filename="../cleandetailveiw.cpp" line="949"/>
        <source>Cleaning up....</source>
        <translation>清理中...</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::ClearMainWidget</name>
    <message>
        <location filename="../clearmainwidget.cpp" line="71"/>
        <source>Clean regularly to keep your computer light and safe</source>
        <translation>经常清理，让您的电脑又轻快又安全</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="81"/>
        <location filename="../clearmainwidget.cpp" line="264"/>
        <source>System cache</source>
        <translation>系统缓存</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="91"/>
        <location filename="../clearmainwidget.cpp" line="274"/>
        <source>Cookies</source>
        <translation>Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="101"/>
        <location filename="../clearmainwidget.cpp" line="284"/>
        <source>History trace</source>
        <translation>历史痕迹</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="133"/>
        <source>StartClear</source>
        <translation>开始扫描</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="210"/>
        <source>Cleanup Package Cache</source>
        <translation>清理安装包缓存</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="210"/>
        <location filename="../clearmainwidget.cpp" line="217"/>
        <location filename="../clearmainwidget.cpp" line="225"/>
        <source>Cleanup Thumbnails Cache</source>
        <translation>清理缩略图</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="210"/>
        <location filename="../clearmainwidget.cpp" line="217"/>
        <location filename="../clearmainwidget.cpp" line="226"/>
        <location filename="../clearmainwidget.cpp" line="340"/>
        <location filename="../clearmainwidget.cpp" line="353"/>
        <source>Cleanup Qaxbrowser Cache</source>
        <translation>清理奇安信浏览器</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="211"/>
        <location filename="../clearmainwidget.cpp" line="217"/>
        <location filename="../clearmainwidget.cpp" line="227"/>
        <source>Cleanup Trash Box</source>
        <translation>清理回收站</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="234"/>
        <location filename="../clearmainwidget.cpp" line="344"/>
        <location filename="../clearmainwidget.cpp" line="357"/>
        <source>Cleanup the Cookies saving in Qaxbrowser</source>
        <translation>清理奇安信浏览器 Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="234"/>
        <location filename="../clearmainwidget.cpp" line="315"/>
        <location filename="../clearmainwidget.cpp" line="328"/>
        <source>Cleanup the Cookies saving in Firefox</source>
        <translation>清理火狐浏览器 Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="241"/>
        <source>Clean up the recently opened documents records</source>
        <translation>清理最近打开的文档记录</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="242"/>
        <source>Delete the command history</source>
        <translation>清理命令行历史记录</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="311"/>
        <location filename="../clearmainwidget.cpp" line="324"/>
        <source>Cleanup FireFox Cache</source>
        <translation>清理火狐浏览器</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="369"/>
        <location filename="../clearmainwidget.cpp" line="382"/>
        <source>Cleanup Chromium Cache</source>
        <translation>清理谷歌浏览器</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="373"/>
        <location filename="../clearmainwidget.cpp" line="386"/>
        <source>Cleanup the Cookies saving in Chromium</source>
        <translation>清理谷歌浏览器 Cookies</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="377"/>
        <location filename="../clearmainwidget.cpp" line="390"/>
        <source>Clean up the Chromium Internet records</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="67"/>
        <source>Computer garbage  One key to clean up</source>
        <translation>电脑垃圾    一键清理</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="82"/>
        <source>Clean up packages, thumbnails, and recycle bin</source>
        <translation>清理包，缩略图及回收站等</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="92"/>
        <source>Clean up Internet history,cookies</source>
        <translation>清理上网历史，Cookies 等</translation>
    </message>
    <message>
        <location filename="../clearmainwidget.cpp" line="102"/>
        <source>Clean up system usage traces</source>
        <translation>清理系统使用痕迹</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="49"/>
        <source>Cleanable items not selected!</source>
        <translation>未选择可清理的项目！</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="52"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>KylinRubbishClear::SelectListWidget</name>
    <message>
        <location filename="../selectlistwidget.cpp" line="54"/>
        <location filename="../selectlistwidget.cpp" line="83"/>
        <location filename="../selectlistwidget.cpp" line="178"/>
        <source>Clean Items:</source>
        <translation>清理项目：</translation>
    </message>
    <message>
        <location filename="../selectlistwidget.cpp" line="98"/>
        <source>No items to clean</source>
        <translation>没有清理项</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../rubbishclearentr.cpp" line="44"/>
        <source>RubbishClear</source>
        <translation>垃圾清理</translation>
    </message>
</context>
</TS>
