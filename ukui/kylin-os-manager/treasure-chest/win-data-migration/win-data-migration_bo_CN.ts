<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QApplication</name>
    <message>
        <source>win data migration</source>
        <translation>winསྤོར་སྒྱུར་ལག་ཆ་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>win data migration</source>
        <translation>winསྤོར་སྒྱུར་ལག་ཆ་</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>གཏན་ཁེལ་</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>ཡང་བསྐྱར་ཚོད་ལྟ་</translation>
    </message>
    <message>
        <source>failed to migrate files</source>
        <translation>མ་ལེགས་པར་སྤོས་པའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>view</source>
        <translation>ལྟ་ཞིབ་</translation>
    </message>
    <message>
        <source>failed to read the migration log:</source>
        <translation>ཉིན་ཐོར་དཔེ་ཀློག་དང་གནས་སྤོ་མ་ཐུབ།</translation>
    </message>
    <message>
        <source>migration log</source>
        <translation>གནས་སྤོའི་ཉིན་ཐོ་</translation>
    </message>
    <message>
        <source>how to set the win</source>
        <translation>winསྣེ་ཇི་ལྟར་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <source>how to set the win (required reading)</source>
        <translation>winསྣེ་ཇི་ལྟར་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>IPཤག་གནས་</translation>
    </message>
    <message>
        <source>win work group</source>
        <translation>winསྤྱོད་མཁན་ཚོ་</translation>
    </message>
    <message>
        <source>win user name</source>
        <translation>winསྤྱོད་མཁན་གྱི་མིང་</translation>
    </message>
    <message>
        <source>win password</source>
        <translation>winགསང་ཨང་</translation>
    </message>
    <message>
        <source>establish connection</source>
        <translation>འབྲེལ་མཐུད་འཛུགས་པ་</translation>
    </message>
    <message>
        <source>successful connection</source>
        <translation>འབྲེལ་མཐུད་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <source>connecting...</source>
        <translation>ད་ལྟ་འབྲེལ་མཐུད་བྱེད་བཞིན་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <source>connection failed, please try again</source>
        <translation>འབྲེལ་མཐུད་ཕམ་དང་།ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས་།</translation>
    </message>
    <message>
        <source>select the files you want to migrate</source>
        <translation>ཡིག་ཆ་སྤོ་དགོས་།</translation>
    </message>
    <message>
        <source>file migration to</source>
        <translation>ཡིག་ཆ་སྤོར་བ།</translation>
    </message>
    <message>
        <source>start migration</source>
        <translation>སྤོ་མགོ་བརྩམས་།</translation>
    </message>
    <message>
        <source>select migration directory</source>
        <translation>གནས་སྤོའི་དཀར་ཆག་འདེམས་པ།</translation>
    </message>
    <message>
        <source>migration success</source>
        <translation>སྤོར་འགྲུབ་</translation>
    </message>
    <message>
        <source>failed to create migration logs</source>
        <translation>གནས་སྤོའི་ཉིན་ཐོ་གསར་འཛུགས་ཕམ་།</translation>
    </message>
    <message>
        <source>files failed to be migrated</source>
        <translation>ཡིག་ཆ་གཅིག་སྤོ་ཕམ་</translation>
    </message>
    <message>
        <source>no migration items are selected</source>
        <translation>མ་བདམས་པའི་གནས་སྤོའི་ཚན་པ་གང་རུང་</translation>
    </message>
    <message>
        <source>save path is invalid or unauthorized</source>
        <translation>ཉར་ཚགས་འགྲོ་ལམ་ཁྲིམས་དང་མི་མཐུན་པའམ་ཡང་ན་དབང་ཚད་མེད་པ།</translation>
    </message>
    <message>
        <source>file creation failure</source>
        <translation>ཡིག་ཆ་གསར་འཛུགས་ཕམ་པ།</translation>
    </message>
    <message>
        <source>file integrity check fails</source>
        <translation>ཡིག་ཆའི་ཆ་ཚང་རང་བཞིན་དག་བཤེར་མི་འཕྲོད་པ།</translation>
    </message>
    <message>
        <source>[loading]</source>
        <translation>[འདེགས་བསྐྱར]</translation>
    </message>
    <message>
        <source>smb disconnected abnormally</source>
        <translation>smbའབྲེལ་མཐུད་རྒྱུན་འགལ་བར་ཆད་</translation>
    </message>
    <message>
        <source>loading migration file tree</source>
        <translation>ཀློག་ལེན་སྤོར་ཡིག་ཆ་སྡོང་འགྲེམས།</translation>
    </message>
    <message>
        <source>&lt;h2&gt;win Settings &lt;/h2&gt;&lt;p&gt; Step 1: Both computers need to be in the same LAN network &lt;/p&gt;&lt;p&gt; Step 2: win computer needs to set the folder or file as a shared folder &lt;/p&gt;&lt;div&gt;1. Select the folder to share.&lt;/div&gt;&lt;div&gt;2. Right click on Properties and select the Sharing tab. Click on Advanced Sharing Settings, check Share this folder, and then click Apply.&lt;/div&gt;</source>
        <translation>&lt;h2&gt;winསྣེ་སྒྲིག་འགོད། &lt;/h2&gt;&lt;p&gt;  གོམ་པ་དང་པོ།གློག་ཀླད་གཉིས་དྲྭ་རྒྱའི་དྲ་བའི་ནང་གཅིག་དགོས། &lt;/p&gt;&lt;p&gt; གོམ་པ་གཉིས་པ།winགློག་ཀླད་ལ་ཡིག་ཁུག་གམ་ཡིག་ཆ་དགོས།མཉམ་སྤྱོད་ཡིག་ཁུག་བཟོ་དགོས།  &lt;/p&gt;&lt;div&gt;1.མཉམ་སྤྱོད་ཀྱི་ཡིག་ཁུག་འདེམ་དགོས།&lt;/div&gt;&lt;div&gt;2.ཙིག་རྟགས་གཡས་མཐེབ་མནན་ཏེ་གནོན་ངོ་བོ་དང་།མཉམ་སྤྱོད་ཤོག་ངོས་བདམས་ནས་མིང་རྟགས་འགོད་པ་དང་།མཐོ་རིམ་མཉམ་སྤྱོད་ལ་གནོན་།མཉམ་སྤྱོད་བཀོད་སྒྲིག་དང་།བཅད་མཉམ་སྤྱོད་ཡིག་ཁུག་དེ་འདེམས་མཉམ་སྤྱོད་དང་།གནོན་ཉེར་སྤྱོད་།&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Canceling file migration, please wait ……</source>
        <translation>ཡིག་ཆ་སྤོ་བཞིན་ཡོད་།ཏོག་ཙམ་སྒུག་དང་།</translation>
    </message>
</context>
</TS>
