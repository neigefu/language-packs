<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QApplication</name>
    <message>
        <source>win data migration</source>
        <translation>贏得數據遷移</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>win data migration</source>
        <translation>贏得數據遷移</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <source>failed to migrate files</source>
        <translation>遷移檔失敗</translation>
    </message>
    <message>
        <source>view</source>
        <translation>視圖</translation>
    </message>
    <message>
        <source>failed to read the migration log:</source>
        <translation>無法讀取移日誌：</translation>
    </message>
    <message>
        <source>migration log</source>
        <translation>遷移日誌</translation>
    </message>
    <message>
        <source>how to set the win</source>
        <translation>如何設定勝利</translation>
    </message>
    <message>
        <source>how to set the win (required reading)</source>
        <translation>如何設定勝利（必讀）</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>IP位址</translation>
    </message>
    <message>
        <source>win work group</source>
        <translation>贏工作組</translation>
    </message>
    <message>
        <source>win user name</source>
        <translation>贏使用者名</translation>
    </message>
    <message>
        <source>win password</source>
        <translation>贏取密碼</translation>
    </message>
    <message>
        <source>establish connection</source>
        <translation>建立連接</translation>
    </message>
    <message>
        <source>successful connection</source>
        <translation>連接成功</translation>
    </message>
    <message>
        <source>connecting...</source>
        <translation>連接。。。</translation>
    </message>
    <message>
        <source>connection failed, please try again</source>
        <translation>連接失敗，請重試</translation>
    </message>
    <message>
        <source>select the files you want to migrate</source>
        <translation>選擇要遷移的檔</translation>
    </message>
    <message>
        <source>file migration to</source>
        <translation>檔遷移到</translation>
    </message>
    <message>
        <source>start migration</source>
        <translation>開始遷移</translation>
    </message>
    <message>
        <source>select migration directory</source>
        <translation>選擇遷移目錄</translation>
    </message>
    <message>
        <source>migration success</source>
        <translation>遷移成功</translation>
    </message>
    <message>
        <source>failed to create migration logs</source>
        <translation>無法創建遷移日誌</translation>
    </message>
    <message>
        <source>files failed to be migrated</source>
        <translation>檔遷移失敗</translation>
    </message>
    <message>
        <source>no migration items are selected</source>
        <translation>未選擇遷移項</translation>
    </message>
    <message>
        <source>save path is invalid or unauthorized</source>
        <translation>保存路徑無效或未經授權</translation>
    </message>
    <message>
        <source>file creation failure</source>
        <translation>檔案創建失敗</translation>
    </message>
    <message>
        <source>file integrity check fails</source>
        <translation>檔完整性檢查失敗</translation>
    </message>
    <message>
        <source>[loading]</source>
        <translation>[載入中]</translation>
    </message>
    <message>
        <source>smb disconnected abnormally</source>
        <translation>SMB 斷開連接異常</translation>
    </message>
    <message>
        <source>loading migration file tree</source>
        <translation>載入遷移檔樹</translation>
    </message>
    <message>
        <source>&lt;h2&gt;win Settings &lt;/h2&gt;&lt;p&gt; Step 1: Both computers need to be in the same LAN network &lt;/p&gt;&lt;p&gt; Step 2: win computer needs to set the folder or file as a shared folder &lt;/p&gt;&lt;div&gt;1. Select the folder to share.&lt;/div&gt;&lt;div&gt;2. Right click on Properties and select the Sharing tab. Click on Advanced Sharing Settings, check Share this folder, and then click Apply.&lt;/div&gt;</source>
        <translation>&lt;h2&gt;win 設置 &lt;/h2&gt;&lt;p&gt; 步驟 1：兩台計算機需要位於同一 LAN 網络中 &lt;/p&gt;&lt;p&gt; 步驟 2：win 計算機需要將資料夾或檔設置為共用資料夾 &lt;/p&gt;&lt;div&gt;1。選擇要共享的資料夾。&lt;/div&gt;&lt;div&gt;2.右鍵按下屬性並選擇共用選項卡。按兩下高級共享設置“，選中”共用此資料夾“，然後按兩下”應用“。&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Canceling file migration, please wait ……</source>
        <translation>正在取消檔遷移，請稍候...…</translation>
    </message>
</context>
</TS>
