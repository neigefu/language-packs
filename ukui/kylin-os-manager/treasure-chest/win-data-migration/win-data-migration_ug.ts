<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>QApplication</name>
    <message>
        <source>win data migration</source>
        <translation>Win يۆتكەش قورالى</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>win data migration</source>
        <translation>Win يۆتكەش قورالى</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>بېكىتىش</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>قايتا سىناش</translation>
    </message>
    <message>
        <source>failed to migrate files</source>
        <translation>مۇۋەپپەقىيەتلىك يۆتكەلمىگەن ھۆججەت</translation>
    </message>
    <message>
        <source>view</source>
        <translation>تەكشۈرۈش</translation>
    </message>
    <message>
        <source>failed to read the migration log:</source>
        <translation>يۆتكەش خاتىرىسىنى ئوقۇش مەغلۇپ بولدى :</translation>
    </message>
    <message>
        <source>migration log</source>
        <translation>يۆتكەش خاتىرىسى</translation>
    </message>
    <message>
        <source>how to set the win</source>
        <translation>Win ئۇچى قانداق تەسىس قىلىش</translation>
    </message>
    <message>
        <source>how to set the win (required reading)</source>
        <translation>Win ئۇچى قانداق تەسىس قىلىنىدۇ ( زۆرۈر ئوقۇشلۇق )</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>IP ئادرېسى</translation>
    </message>
    <message>
        <source>win work group</source>
        <translation>Win ئىشلەتكۈچىلەر گۇرۇپپىسى</translation>
    </message>
    <message>
        <source>win user name</source>
        <translation>ئابونت نامى win</translation>
    </message>
    <message>
        <source>win password</source>
        <translation>Win مەخپىي نومۇرى</translation>
    </message>
    <message>
        <source>establish connection</source>
        <translation>ئۇلاش</translation>
    </message>
    <message>
        <source>successful connection</source>
        <translation>ئۇلاش مۇۋەپپەقىيەتلىك بولدى</translation>
    </message>
    <message>
        <source>connecting...</source>
        <translation>ئۇلىنىۋاتىدۇ...</translation>
    </message>
    <message>
        <source>connection failed, please try again</source>
        <translation>ئۇلاش مەغلۇپ بولدى، قايتا سىناڭ</translation>
    </message>
    <message>
        <source>select the files you want to migrate</source>
        <translation>يۆتكەش ھۆججەت تاللاش كېرەك</translation>
    </message>
    <message>
        <source>file migration to</source>
        <translation>ھۆججەت يۆتكەش</translation>
    </message>
    <message>
        <source>start migration</source>
        <translation>كۆچۈشكە باشلىدى</translation>
    </message>
    <message>
        <source>select migration directory</source>
        <translation>تاللاش يۆتكەش مۇندەرىجىسى</translation>
    </message>
    <message>
        <source>migration success</source>
        <translation>كۆچۈش تاماملاندى</translation>
    </message>
    <message>
        <source>failed to create migration logs</source>
        <translation>كۆچۈش خاتىرىسى بەرپا قىلىش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <source>files failed to be migrated</source>
        <translation>بىر ھۆججەت يۆتكەش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <source>no migration items are selected</source>
        <translation>تاللانمىغان ھەرقانداق كۆچۈش تۈر</translation>
    </message>
    <message>
        <source>save path is invalid or unauthorized</source>
        <translation>ساقلاش يولى قانۇنغا ئۇيغۇن ئەمەس ياكى ھوقۇق دائىرىسى يوق</translation>
    </message>
    <message>
        <source>file creation failure</source>
        <translation>ھۆججەت بەرپا قىلىش مەغلۇپ بولدى</translation>
    </message>
    <message>
        <source>file integrity check fails</source>
        <translation>ھۆججەت مۇكەممەللىكىنى تەكشۈرۈش ئارقىلىق ئەمەس</translation>
    </message>
    <message>
        <source>[loading]</source>
        <translation>يۈكلىنىۋاتىدۇ</translation>
    </message>
    <message>
        <source>smb disconnected abnormally</source>
        <translation>Smb ئۇلىنىشى بىنورمال ئۈزۈلۈش</translation>
    </message>
    <message>
        <source>loading migration file tree</source>
        <translation>ئوقۇش يۆتكەش ھۆججەت دەرىخى</translation>
    </message>
    <message>
        <source>&lt;h2&gt;win Settings &lt;/h2&gt;&lt;p&gt; Step 1: Both computers need to be in the same LAN network &lt;/p&gt;&lt;p&gt; Step 2: win computer needs to set the folder or file as a shared folder &lt;/p&gt;&lt;div&gt;1. Select the folder to share.&lt;/div&gt;&lt;div&gt;2. Right click on Properties and select the Sharing tab. Click on Advanced Sharing Settings, check Share this folder, and then click Apply.&lt;/div&gt;</source>
        <translation>&lt;h2&gt;Win ئۇچى تەسىس قىلىش &lt;/h2&gt;&lt;p&gt;بىرىنچى قەدەم： ئىككى كومپيۇتېر ئوخشاش بىر تار دائىرىلىك تور ئىچىدە بولۇشى كېرەك &lt;/p&gt;&lt;p&gt; ئىككىنچى قەدەم : win كومپيۇتېر ھۆججەت قىسقۇچ ياكى ھۆججەتنى ئورتاق بەھرىلىنىدىغان ھۆججەت قىسقۇچ قىلىپ تەڭشىشى كېرەك. &lt;/p&gt;&lt;div&gt;1. ئورتاق بەھىرلىنىدىغان ھۆججەت قىسقۇچنى تاللاڭ.&lt;/div&gt;&lt;div&gt;2.مائۇسنىڭ ئوڭ كۇنۇپكىسىنى بېسىپ خاسلىقى، ھەمدە ئورتاق بەھرىمەن بولىدىغان بەت ئىمزاسىنى تاللاڭ، يۇقىرى دەرىجىلىك ئورتاق بەھرىمەن بولۇش تەسىس قىلىش، بۇ ھۆججەت قىسقۇچنى تاللىسىڭىز ئورتاق بەھرىمەن بولۇش، قوللىنىشنى چېكىڭ.&lt;/div&gt;</translation>
    </message>
    <message>
        <source>Canceling file migration, please wait ……</source>
        <translation>ھۆججەت يۆتكەش ئەمەلدىن قالدۇرۇلماقتا، سەل ساقلاڭ...</translation>
    </message>
</context>
</TS>
