<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AssessWidget</name>
    <message>
        <location filename="../assesswidget.cpp" line="33"/>
        <source>Assess Grade</source>
        <translation>评估等级</translation>
    </message>
    <message>
        <location filename="../assesswidget.cpp" line="49"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>BreakConnTip</name>
    <message>
        <location filename="../breakconntip.cpp" line="33"/>
        <source>Remote connection exited</source>
        <translation>远程连接已退出</translation>
    </message>
    <message>
        <location filename="../breakconntip.cpp" line="36"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>DragFileWidget</name>
    <message>
        <location filename="../dragfilewidget.cpp" line="51"/>
        <source>Click on the “+” icon</source>
        <translation>点击“+”图标</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="52"/>
        <source>Alternatively, drag and drop files here for file transfer</source>
        <translation>或者将文件拖拽到这里，进行文件传输</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="83"/>
        <location filename="../dragfilewidget.cpp" line="139"/>
        <source>choose file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="139"/>
        <source>Sending folders is currently not supported!</source>
        <translation>选择文件 暂不支持发送文件夹！</translation>
    </message>
</context>
<context>
    <name>FilePathWidget</name>
    <message>
        <location filename="../filepathwidget.cpp" line="38"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="39"/>
        <source>File Transfer Path</source>
        <translation>文件传输路径</translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="111"/>
        <source>Change Directory</source>
        <translation>选择目录</translation>
    </message>
</context>
<context>
    <name>FileTransferProgressWidget</name>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="42"/>
        <location filename="../filetransferprogresswidget.cpp" line="107"/>
        <location filename="../filetransferprogresswidget.cpp" line="115"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="43"/>
        <location filename="../filetransferprogresswidget.cpp" line="106"/>
        <source>Transferring in progress:</source>
        <translation>正在传输中，传输进度：</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="103"/>
        <location filename="../filetransferprogresswidget.cpp" line="119"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="104"/>
        <source>Transmission completed, transmission progress: </source>
        <translation>传输完成，传输进度： </translation>
    </message>
</context>
<context>
    <name>FileTransferWidget</name>
    <message>
        <location filename="../filetransferwidget.cpp" line="77"/>
        <location filename="../filetransferwidget.cpp" line="78"/>
        <source>Control End</source>
        <translation>主控端</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="77"/>
        <location filename="../filetransferwidget.cpp" line="78"/>
        <source>Controlled End</source>
        <translation>被控端</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="215"/>
        <source>choose file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="353"/>
        <source>Failed to open compressed file. Please reselect the file.</source>
        <translation>压缩文件打开失败, 请重新选择文件。</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="360"/>
        <source>Failed to send file header information. Please check the link status.</source>
        <translation>文件头部信息发送失败, 请检查链接状态。</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="367"/>
        <source>The other party&apos;s file creation failed. Please resend the file.</source>
        <translation>对方文件创建失败, 请重新发送文件。</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="435"/>
        <source>Recv File</source>
        <translation>接收文件</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="518"/>
        <source>Failed to open file in write only mode. </source>
        <translation>以只写方式打开文件失败。 </translation>
    </message>
</context>
<context>
    <name>GlobalData</name>
    <message>
        <location filename="../globaldata.cpp" line="63"/>
        <source>Log</source>
        <translation>日志</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="64"/>
        <source>Allow Remote</source>
        <translation>远程需本机同意</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="65"/>
        <source>File Path</source>
        <translation>文件传输路径</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="62"/>
        <source>Remote Assistance Tool</source>
        <translation>远程协助工具</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="66"/>
        <source>Remote Assistance Client</source>
        <translation>远程协助客户端</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="68"/>
        <source>Local identification code :</source>
        <translation>本机识别码： </translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="69"/>
        <source>Native verification code (please provide it after trust):</source>
        <translation>本机验证码（请确保信任后提供）：</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="70"/>
        <source>Is it allowed for remote control to apply for file transfer?</source>
        <translation>远程控制端申请传输文件，是否允许？</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="71"/>
        <source>Is it allowed to apply for remote control of your computer?</source>
        <translation>远程控制端申请控制你的电脑，是否允许？</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="72"/>
        <source>Allow</source>
        <translation>允许</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="73"/>
        <source>Refuse</source>
        <translation>拒绝</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="76"/>
        <source>Send Status</source>
        <translation>发送状态</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="77"/>
        <source>Receive Status</source>
        <translation>接收状态</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="78"/>
        <source>Connection status</source>
        <translation>连接状态</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="95"/>
        <source>Operation Log</source>
        <translation>操作日志</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../mainwidget.cpp" line="558"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="486"/>
        <source>Local ID copied successfully</source>
        <translation>本机识别码复制成功</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="352"/>
        <source>Link failure, configuration issue</source>
        <translation>链接失败，配置出现问题</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="499"/>
        <source>Local verification code copied successfully</source>
        <translation>本机验证码复制成功</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="560"/>
        <source>Not connected to the server, please check if the network configuration is correct.</source>
        <translation>未连接到服务端，请检查网络配置是否正确。</translation>
    </message>
</context>
<context>
    <name>WebConnStatus</name>
    <message>
        <location filename="../webconnstatus.cpp" line="15"/>
        <source>Connected to the server</source>
        <translation>已连接到服务端</translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="19"/>
        <source>Not connected to the server</source>
        <translation>未连接到服务端</translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="23"/>
        <location filename="../webconnstatus.cpp" line="56"/>
        <source>Connecting to the server...</source>
        <translation>正在连接服务端...</translation>
    </message>
</context>
</TS>
