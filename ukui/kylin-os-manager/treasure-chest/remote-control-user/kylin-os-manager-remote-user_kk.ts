<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk_KZ">
<context>
    <name>AssessWidget</name>
    <message>
        <location filename="../assesswidget.cpp" line="33"/>
        <source>Assess Grade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assesswidget.cpp" line="48"/>
        <source>sure</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DragFileWidget</name>
    <message>
        <location filename="../dragfilewidget.cpp" line="51"/>
        <source>Click on the “+” icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="52"/>
        <source>Alternatively, drag and drop files here for file transfer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="83"/>
        <source>choose file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilePathWidget</name>
    <message>
        <location filename="../filepathwidget.cpp" line="37"/>
        <source>sure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="38"/>
        <source>File Transfer Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="109"/>
        <source>Change Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileTransferProgressWidget</name>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="40"/>
        <location filename="../filetransferprogresswidget.cpp" line="103"/>
        <location filename="../filetransferprogresswidget.cpp" line="109"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="41"/>
        <location filename="../filetransferprogresswidget.cpp" line="102"/>
        <source>Transferring in progress:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="99"/>
        <location filename="../filetransferprogresswidget.cpp" line="113"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="100"/>
        <source>Transmission completed, transmission progress: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileTransferWidget</name>
    <message>
        <location filename="../filetransferwidget.cpp" line="70"/>
        <location filename="../filetransferwidget.cpp" line="71"/>
        <source>Control End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="70"/>
        <location filename="../filetransferwidget.cpp" line="71"/>
        <source>Controlled End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="207"/>
        <source>choose file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="445"/>
        <source>Recv File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlobalData</name>
    <message>
        <location filename="../globaldata.cpp" line="57"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="58"/>
        <source>Allow Remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="59"/>
        <source>File Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="56"/>
        <source>Remote Assistance Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="60"/>
        <source>Remote Assistance Client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="62"/>
        <source>Local identification code :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="63"/>
        <source>Native verification code (please provide it after trust):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="64"/>
        <source>Is it allowed for remote control to apply for file transfer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="65"/>
        <source>Is it allowed to apply for remote control of your computer?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="66"/>
        <source>Allow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="67"/>
        <source>Refuse</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../mainwidget.cpp" line="564"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="486"/>
        <source>Local ID copied successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="504"/>
        <source>Local verification code copied successfully</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="562"/>
        <source>未连接到服务端，请检查网络配置是否正确。</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WebConnStatus</name>
    <message>
        <location filename="../webconnstatus.cpp" line="15"/>
        <source>Connected to the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="19"/>
        <source>Not connected to the server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="23"/>
        <location filename="../webconnstatus.cpp" line="56"/>
        <source>Connecting to the server...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
