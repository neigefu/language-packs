<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AssessWidget</name>
    <message>
        <location filename="../assesswidget.cpp" line="33"/>
        <source>Assess Grade</source>
        <translation>དཔྱད་དཔོག་རིམ་པ་</translation>
    </message>
    <message>
        <location filename="../assesswidget.cpp" line="48"/>
        <source>sure</source>
        <translation>ངོས་འཛིན་</translation>
    </message>
</context>
<context>
    <name>DragFileWidget</name>
    <message>
        <location filename="../dragfilewidget.cpp" line="51"/>
        <source>Click on the “+” icon</source>
        <translation>རིས་རྟགས་“+”ལ་གནོན་།</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="52"/>
        <source>Alternatively, drag and drop files here for file transfer</source>
        <translation>ཡང་ན་ཡིག་ཆ་འདྲུད་འཐེན་ནས་འདིར་ཡོང་ནས་ཡིག་ཆ་བརྒྱུད་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="83"/>
        <source>choose file</source>
        <translation>ཡིག་ཆ་འདེམས་པ་</translation>
    </message>
</context>
<context>
    <name>FilePathWidget</name>
    <message>
        <location filename="../filepathwidget.cpp" line="37"/>
        <source>sure</source>
        <translation>ངོས་འཛིན་</translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="38"/>
        <source>File Transfer Path</source>
        <translation>ཡིག་ཆ་བརྒྱུད་གཏོང་འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="109"/>
        <source>Change Directory</source>
        <translation>བདམས་པའི་དཀར་ཆག</translation>
    </message>
</context>
<context>
    <name>FileTransferProgressWidget</name>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="40"/>
        <location filename="../filetransferprogresswidget.cpp" line="103"/>
        <location filename="../filetransferprogresswidget.cpp" line="109"/>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ་</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="41"/>
        <location filename="../filetransferprogresswidget.cpp" line="102"/>
        <source>Transferring in progress:</source>
        <translation>བརྒྱུད་གཏོང་བྱེད་བཞིན་པའི་སྒང་ཡིན་ཞིང་།བརྒྱུད་གཏོང་གི་མྱུར་ཚད་ནི།</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="99"/>
        <location filename="../filetransferprogresswidget.cpp" line="113"/>
        <source>Confirm</source>
        <translation>ངོས་འཛིན་</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="100"/>
        <source>Transmission completed, transmission progress: </source>
        <translation>བརྒྱུད་གཏོང་ལེགས་འགྲུབ་དང་བརྒྱུད་གཏོང་གི་འཕེལ་རིམ།</translation>
    </message>
</context>
<context>
    <name>FileTransferWidget</name>
    <message>
        <location filename="../filetransferwidget.cpp" line="70"/>
        <location filename="../filetransferwidget.cpp" line="71"/>
        <source>Control End</source>
        <translation>ཚོད་འཛིན་གཙོ་སྣེ།</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="70"/>
        <location filename="../filetransferwidget.cpp" line="71"/>
        <source>Controlled End</source>
        <translation>ཚོད་འཛིན་སྣེ།</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="207"/>
        <source>choose file</source>
        <translation>ཡིག་ཆ་འདེམས་པ་</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="445"/>
        <source>Recv File</source>
        <translation>སྡུད་ལེན་ཡིག་ཆ།</translation>
    </message>
</context>
<context>
    <name>GlobalData</name>
    <message>
        <location filename="../globaldata.cpp" line="57"/>
        <source>Log</source>
        <translation>ཉིན་ཐོ།</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="58"/>
        <source>Allow Remote</source>
        <translation>རྒྱང་རིང་རྩིས་འཁོར་འདིའི་མོས་མཐུན་དགོས་།</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="59"/>
        <source>File Path</source>
        <translation>ཡིག་ཆ་བརྒྱུད་གཏོང་འགྲོ་ལམ།</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="56"/>
        <source>Remote Assistance Tool</source>
        <translation>རྒྱང་རིང་གཞོགས་འདེགས་ལག་ཆ་</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="60"/>
        <source>Remote Assistance Client</source>
        <translation>རྒྱང་རིང་གཞོགས་འདེགས་དུད་སྣེ་</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="62"/>
        <source>Local identification code :</source>
        <translation>ཁ་པར་འདིའི་ངོས་འཛིན་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="63"/>
        <source>Native verification code (please provide it after trust):</source>
        <translation>འཕྲུལ་ཆས་འདིའི་ར་སྤྲོད་ཨང་རྟགས།</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="64"/>
        <source>Is it allowed for remote control to apply for file transfer?</source>
        <translation>རྒྱང་རིང་ཚོད་འཛིན་སྣེ་རེ་ཞུ་བརྒྱུད་གཏོང་ཡིག་ཆ།ཆོག་གམ།</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="65"/>
        <source>Is it allowed to apply for remote control of your computer?</source>
        <translation>རྒྱང་རིང་ཚོད་འཛིན་སྣེ་རེ་ཞུ་ཁྱོད་ཀྱི་གློག་ཀླད་ཚོད་འཛིན་དང་།ཆོག་གམ།</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="66"/>
        <source>Allow</source>
        <translation>ཆོག་པ་</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="67"/>
        <source>Refuse</source>
        <translation>ཁས་མི་ལེན་པ་</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../mainwidget.cpp" line="564"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="486"/>
        <source>Local ID copied successfully</source>
        <translation>རང་སའི་རྩིས་འཁོར་ངོས་འཛིན་ཨང་འདྲ་བཟོ་ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="504"/>
        <source>Local verification code copied successfully</source>
        <translation>རང་སའི་རྩིས་འཁོར་གྱི་བཤེར་ཨང་འདྲ་བཟོ་ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="562"/>
        <source>未连接到服务端，请检查网络配置是否正确。</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WebConnStatus</name>
    <message>
        <location filename="../webconnstatus.cpp" line="15"/>
        <source>Connected to the server</source>
        <translation>འབྲེལ་མཐུད་བྱས་ཟིན་ནས་ཞབས་ཞུའི་སྣེ་།</translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="19"/>
        <source>Not connected to the server</source>
        <translation>སྦྲེལ་མཐུད་མ་བྱུང་ནས་ཞབས་ཞུའི་སྣེ་</translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="23"/>
        <location filename="../webconnstatus.cpp" line="56"/>
        <source>Connecting to the server...</source>
        <translation>ཞབས་ཞུའི་སྣེ།</translation>
    </message>
</context>
</TS>
