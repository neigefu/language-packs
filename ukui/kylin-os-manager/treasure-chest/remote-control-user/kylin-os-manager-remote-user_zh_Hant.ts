<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AssessWidget</name>
    <message>
        <location filename="../assesswidget.cpp" line="33"/>
        <source>Assess Grade</source>
        <translation>評估等級</translation>
    </message>
    <message>
        <location filename="../assesswidget.cpp" line="49"/>
        <source>sure</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>BreakConnTip</name>
    <message>
        <location filename="../breakconntip.cpp" line="33"/>
        <source>Remote connection exited</source>
        <translation>遠端連接已退出</translation>
    </message>
    <message>
        <location filename="../breakconntip.cpp" line="36"/>
        <source>sure</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>DragFileWidget</name>
    <message>
        <location filename="../dragfilewidget.cpp" line="51"/>
        <source>Click on the “+” icon</source>
        <translation>點擊“+”圖示</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="52"/>
        <source>Alternatively, drag and drop files here for file transfer</source>
        <translation>或者將檔拖拽到這裡，進行文件傳輸</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="83"/>
        <location filename="../dragfilewidget.cpp" line="139"/>
        <source>choose file</source>
        <translation>選擇檔案</translation>
    </message>
    <message>
        <location filename="../dragfilewidget.cpp" line="139"/>
        <source>Sending folders is currently not supported!</source>
        <translation>選擇檔案 暫不支援發送資料夾！</translation>
    </message>
</context>
<context>
    <name>FilePathWidget</name>
    <message>
        <location filename="../filepathwidget.cpp" line="38"/>
        <source>sure</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="39"/>
        <source>File Transfer Path</source>
        <translation>檔案傳輸路徑</translation>
    </message>
    <message>
        <location filename="../filepathwidget.cpp" line="111"/>
        <source>Change Directory</source>
        <translation>選擇目錄</translation>
    </message>
</context>
<context>
    <name>FileTransferProgressWidget</name>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="42"/>
        <location filename="../filetransferprogresswidget.cpp" line="107"/>
        <location filename="../filetransferprogresswidget.cpp" line="115"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="43"/>
        <location filename="../filetransferprogresswidget.cpp" line="106"/>
        <source>Transferring in progress:</source>
        <translation>傳輸中傳輸進度：</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="103"/>
        <location filename="../filetransferprogresswidget.cpp" line="119"/>
        <source>Confirm</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../filetransferprogresswidget.cpp" line="104"/>
        <source>Transmission completed, transmission progress: </source>
        <translation>傳輸完成，傳輸進度： </translation>
    </message>
</context>
<context>
    <name>FileTransferWidget</name>
    <message>
        <location filename="../filetransferwidget.cpp" line="77"/>
        <location filename="../filetransferwidget.cpp" line="78"/>
        <source>Control End</source>
        <translation>主控端</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="77"/>
        <location filename="../filetransferwidget.cpp" line="78"/>
        <source>Controlled End</source>
        <translation>被控端</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="215"/>
        <source>choose file</source>
        <translation>選擇檔案</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="353"/>
        <source>Failed to open compressed file. Please reselect the file.</source>
        <translation>壓縮檔案開啟失敗， 請重新選擇檔案。</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="360"/>
        <source>Failed to send file header information. Please check the link status.</source>
        <translation>檔案頭部資訊傳送失敗， 請檢查連結狀態。</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="367"/>
        <source>The other party&apos;s file creation failed. Please resend the file.</source>
        <translation>對方檔案建立失敗， 請重新送出檔案。</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="435"/>
        <source>Recv File</source>
        <translation>接收檔</translation>
    </message>
    <message>
        <location filename="../filetransferwidget.cpp" line="518"/>
        <source>Failed to open file in write only mode. </source>
        <translation>以只寫方式打開文件失敗。 </translation>
    </message>
</context>
<context>
    <name>GlobalData</name>
    <message>
        <location filename="../globaldata.cpp" line="63"/>
        <source>Log</source>
        <translation>日誌</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="64"/>
        <source>Allow Remote</source>
        <translation>遠端需本機同意</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="65"/>
        <source>File Path</source>
        <translation>檔案傳輸路徑</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="62"/>
        <source>Remote Assistance Tool</source>
        <translation>遠端協助工具</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="66"/>
        <source>Remote Assistance Client</source>
        <translation>遠端協助用戶端</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="68"/>
        <source>Local identification code :</source>
        <translation>本機識別碼：</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="69"/>
        <source>Native verification code (please provide it after trust):</source>
        <translation>本機驗證碼（請確保信任後提供）：</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="70"/>
        <source>Is it allowed for remote control to apply for file transfer?</source>
        <translation>遠端控制端申請傳輸檔，是否允許？</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="71"/>
        <source>Is it allowed to apply for remote control of your computer?</source>
        <translation>遠端控制端申請控制你的電腦，是否允許？</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="72"/>
        <source>Allow</source>
        <translation>允許</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="73"/>
        <source>Refuse</source>
        <translation>拒絕</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="76"/>
        <source>Send Status</source>
        <translation>發送狀態</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="77"/>
        <source>Receive Status</source>
        <translation>接收狀態</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="78"/>
        <source>Connection status</source>
        <translation>連接狀態</translation>
    </message>
    <message>
        <location filename="../globaldata.cpp" line="95"/>
        <source>Operation Log</source>
        <translation>操作紀錄</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../mainwidget.cpp" line="558"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="486"/>
        <source>Local ID copied successfully</source>
        <translation>本機識別碼複製成功</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="352"/>
        <source>Link failure, configuration issue</source>
        <translation>鏈接失敗，配置出現問題</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="499"/>
        <source>Local verification code copied successfully</source>
        <translation>本機驗證碼複製成功</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="560"/>
        <source>Not connected to the server, please check if the network configuration is correct.</source>
        <translation>未連接到服務端，請檢查網路配置是否正確。</translation>
    </message>
</context>
<context>
    <name>WebConnStatus</name>
    <message>
        <location filename="../webconnstatus.cpp" line="15"/>
        <source>Connected to the server</source>
        <translation>已連接到服務端</translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="19"/>
        <source>Not connected to the server</source>
        <translation>未連接到服務端</translation>
    </message>
    <message>
        <location filename="../webconnstatus.cpp" line="23"/>
        <location filename="../webconnstatus.cpp" line="56"/>
        <source>Connecting to the server...</source>
        <translation>連接服務端...</translation>
    </message>
</context>
</TS>
