<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="47"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>ཁྱོད་ཀྱིས་བདམས་པའི་ཡིག་ཆ་ནི་དཀར་ཆག་ཡིན་ནམ་ཡང་ན་ཆོག་མཆན་མ་ཐོབ་ན་གཤགས་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="50"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="45"/>
        <source>Are you sure to start crushing?</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་ཐལ་བའི་རྡུལ་དུ་བརླགས་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="51"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>ཐལ་བའི་རྡུལ་དུ་བརླགས་པའི་ཡིག་ཆ་སླར་གསོ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="58"/>
        <source>sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="66"/>
        <source>cancle</source>
        <translation>སྒྲོན་མེ།</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>kylin file crush</source>
        <translation>ཅིན་ལིན་ཧྲི་ཧྲི་སྤྱི་གཉེར་བ་</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <location filename="../shreddialog.cpp" line="35"/>
        <location filename="../shreddialog.cpp" line="38"/>
        <source>FileShredder</source>
        <translation>ཅིན་ལིན་ཧྲི་ཧྲི་སྤྱི་གཉེར་བ་</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="60"/>
        <source>No file selected to be shredded</source>
        <translation>བདམས་ཟིན་པའི་ཡིག་ཆ་གང་ཞིག་ཡིན་རུང་ཚང་མ་མེད་པར་བཟོ་དགོས།</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="159"/>
        <location filename="../shreddialog.cpp" line="204"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>ཡིག་ཆ་རྦད་དེ་གཤགས་ནས་སླར་གསོ་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="160"/>
        <source>Shred File</source>
        <translation>ཡིག་ཆ་ཧྲོབ་མ།</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="161"/>
        <source>Deselect</source>
        <translation>ས་ཆ་དེ་ནས་ཕྱིར་འབུད་བྱས།</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="162"/>
        <source>Add File</source>
        <translation>ཡིག་ཆ་ཁ་སྣོན་བྱས་</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="163"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation>དོ་སྣང་བྱ་དགོས།ཡིག་ཆ་རྡུལ་དུ་བརླགས་པའི་བརྒྱུད་རིམ་མེད་པར་གཏོང་ཐབས་མེ</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>Select file</source>
        <translation>ཡིག་ཆ་འདེམས་སྒྲུག་བྱ་</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="195"/>
        <source>All Files(*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད་(*)</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="236"/>
        <source>File Shredding ...</source>
        <translation>ཡིག་ཆ་ཧྲོབ་ལྷེབ་ ...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="259"/>
        <source>Shred successfully!</source>
        <translation>ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="273"/>
        <source>Shred failed!</source>
        <translation>ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="295"/>
        <source>Please select the file to shred!</source>
        <translation>ཁྱོད་ཀྱིས་ཡིག་ཆ་དེ་བདམས་ནས་གཤགས་རོགས།</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="300"/>
        <source>You did not select a file with permissions!</source>
        <translation>ཁྱེད་ཚོས་ཆོག་མཆན་ཐོབ་པའི་ཡིག་ཆ་ཞིག་བདམས་མེད།</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="49"/>
        <source>Shred Manager</source>
        <translation>ཧྲི་ཧྲི་སྤྱི་གཉེར་བ།</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="54"/>
        <source>Delete files makes it unable to recover</source>
        <translation>ཡིག་ཆ་བསུབ་ནས་སླར་གསོ་བྱེད་ཐབས་བྲལ།</translation>
    </message>
</context>
</TS>
