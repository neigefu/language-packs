<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QObject</name>
    <message>
        <location filename="../comboxwidget.cpp" line="39"/>
        <source>Shutdown</source>
        <translation>關機頻率</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="35"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="39"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="76"/>
        <source>never</source>
        <translation>從不</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="77"/>
        <source>Only this shutdown</source>
        <translation>僅本次關機</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="78"/>
        <source>Everyday</source>
        <translation>每天</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="79"/>
        <source>Mon</source>
        <translation>週一</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="80"/>
        <source>Tue</source>
        <translation>週二</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="81"/>
        <source>Wed</source>
        <translation>週三</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="82"/>
        <source>Thu</source>
        <translation>週四</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="83"/>
        <source>Fri</source>
        <translation>週五</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="84"/>
        <source>Sat</source>
        <translation>週六</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="85"/>
        <source>Sun</source>
        <translation>周日</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="58"/>
        <source>hours</source>
        <translation>時</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="64"/>
        <source>minutes</source>
        <translation>分</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="154"/>
        <source>minute</source>
        <translation>分鐘</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="104"/>
        <location filename="../widget.cpp" line="105"/>
        <location filename="../widget.cpp" line="128"/>
        <location filename="../widget.cpp" line="254"/>
        <source>time-shutdown</source>
        <translation>定時關機</translation>
    </message>
    <message>
        <source>more</source>
        <translation type="vanished">更多</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="108"/>
        <source>option</source>
        <translation>選項</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="109"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="110"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="116"/>
        <source>Assist</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="117"/>
        <source>About</source>
        <translation>關於</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="118"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Service &amp; Support:</source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="152"/>
        <source>next shutdown</source>
        <translation>距離最近一次關機還有</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="153"/>
        <source>hour</source>
        <translation>小時</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="567"/>
        <location filename="../widget.cpp" line="579"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>time-shutdown is already running!</source>
        <translation>定時關機已經運行</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalscroll60.cpp" line="271"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="128"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="234"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="237"/>
        <source>Restore</source>
        <translation>打開</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="240"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="578"/>
        <source>The shutdown time is shorter than the current time</source>
        <translation>關機時間小於當前時間</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="566"/>
        <source>no set shutdown</source>
        <translation>未設置關機頻率</translation>
    </message>
</context>
</TS>
