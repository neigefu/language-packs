<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <location filename="../comboxwidget.cpp" line="39"/>
        <source>Shutdown</source>
        <translation>ᠪᠠᠶᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ ᠶᠢᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="35"/>
        <source>Cancel</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠭᠡᠶ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../confirmareawidget.cpp" line="39"/>
        <source>Confirm</source>
        <translation>ᠨᠤᠲᠠᠯᠠᠨ ᠲᠣᠭᠲᠠᠭᠠᠬᠤ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="76"/>
        <source>never</source>
        <translation>ᠶᠡᠷᠦ ᠡᠴᠡ ᠪᠢᠰᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="77"/>
        <source>Only this shutdown</source>
        <translation>ᠵᠥᠪᠭᠡᠨ ᠲᠤᠰ ᠤᠳᠠᠭᠠᠨ ᠤ ᠪᠠᠢ᠌ᠭᠤᠯᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="78"/>
        <source>Everyday</source>
        <translation>ᠡᠳᠦᠷ ᠪᠦᠷᠢ</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="79"/>
        <source>Mon</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ ᠨᠢᠭᠡᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="80"/>
        <source>Tue</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ ᠬᠣᠶᠠᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="81"/>
        <source>Wed</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ ᠭᠤᠷᠪᠠᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="82"/>
        <source>Thu</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ ᠳᠥᠷᠪᠡᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="83"/>
        <source>Fri</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ ᠲᠠᠪᠤᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="84"/>
        <source>Sat</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ ᠵᠢᠷᠭᠤᠭᠠᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../dropdownmenu.h" line="85"/>
        <source>Sun</source>
        <translation>ᠭᠠᠷᠠᠭ ᠤᠨ ᠡᠳᠦᠷ ᠃</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="58"/>
        <source>hours</source>
        <translation>ᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../timeshowwidget.cpp" line="64"/>
        <source>minutes</source>
        <translation>ᠰᠠᠯᠭᠠᠶ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="154"/>
        <source>minute</source>
        <translation>ᠮᠢᠨᠦᠢᠲ᠋ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="104"/>
        <location filename="../widget.cpp" line="105"/>
        <location filename="../widget.cpp" line="128"/>
        <location filename="../widget.cpp" line="254"/>
        <source>time-shutdown</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠴᠠᠭ ᠲᠤᠨᠢ ᠬᠠᠭᠠᠬᠤ ᠬᠡᠷᠡᠭᠲᠡᠶ ᠃</translation>
    </message>
    <message>
        <source>more</source>
        <translation type="vanished">更多</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="108"/>
        <source>option</source>
        <translation>ᠰᠣᠩᠭᠣᠯᠲᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="109"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠪᠠᠭ᠎ᠠ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="110"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="116"/>
        <source>Assist</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="117"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="118"/>
        <source>Quit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Service &amp; Support:</source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="152"/>
        <source>next shutdown</source>
        <translation>ᠣᠷᠴᠢᠮ ᠤᠨ ᠨᠢᠭᠡ ᠤᠳᠠᠭᠠᠨ ᠤ ᠬᠠᠭᠠᠯᠭ᠎ᠠ ᠠᠴᠠ ᠪᠠᠰᠠ ᠪᠠᠶᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="153"/>
        <source>hour</source>
        <translation>ᠴᠠᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="567"/>
        <location filename="../widget.cpp" line="579"/>
        <source>OK</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>time-shutdown is already running!</source>
        <translation>ᠲᠣᠭᠲᠠᠭᠰᠠᠨ ᠴᠠᠭ ᠤᠨ ᠪᠠᠢ᠌ᠭᠤᠯᠭ᠎ᠠ ᠨᠢᠭᠡᠨᠲᠡ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠵᠠᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalscroll60.cpp" line="271"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.cpp" line="128"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ </translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="234"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠪᠠᠭ᠎ᠠ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="237"/>
        <source>Restore</source>
        <translation>ᠨᠡᠭᠡᠭᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="240"/>
        <source>Quit</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="578"/>
        <source>The shutdown time is shorter than the current time</source>
        <translation>ᠨᠢᠰᠬᠡᠯ ᠬᠠᠭᠠᠬᠤ ᠴᠠᠭ ᠨᠢ ᠣᠳᠣᠬᠠᠨ ᠤ ᠴᠠᠭ ᠠᠴᠠ ᠪᠠᠭ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="566"/>
        <source>no set shutdown</source>
        <translation>ᠨᠢᠰᠬᠡᠯ ᠦᠨ ᠳᠠᠪᠲᠠᠮᠵᠢ ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠭᠡᠶ ᠃</translation>
    </message>
</context>
</TS>
