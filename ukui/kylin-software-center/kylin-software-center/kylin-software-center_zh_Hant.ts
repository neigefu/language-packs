<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AdImageWidget</name>
    <message>
        <source>Download</source>
        <translation type="vanished">下载</translation>
    </message>
    <message>
        <source>After installation, you can experience the Android environment after restarting</source>
        <translation>安裝后，重啟后即可體驗安卓環境</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
</context>
<context>
    <name>AppCardWidget</name>
    <message>
        <source>install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>New features</source>
        <translation>新功能</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
</context>
<context>
    <name>AppDetailWidget</name>
    <message>
        <source>Platform introduction</source>
        <translation>平台介紹</translation>
    </message>
    <message>
        <source>Technical Features</source>
        <translation>技術特點</translation>
    </message>
    <message>
        <source>Number of Downloads</source>
        <translation>下載次數</translation>
    </message>
    <message>
        <source>times</source>
        <translation>次</translation>
    </message>
    <message>
        <source> scores</source>
        <translation> 分數</translation>
    </message>
    <message>
        <source>Full Score 5 points</source>
        <translation>滿分 5 分</translation>
    </message>
    <message>
        <source>size</source>
        <translation>大小</translation>
    </message>
    <message>
        <source>version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Brief Introduction</source>
        <translation>簡介</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>網站</translation>
    </message>
    <message>
        <source>Update Content</source>
        <translation>更新內容</translation>
    </message>
    <message>
        <source>Other information</source>
        <translation>其他資訊</translation>
    </message>
    <message>
        <source>Package name</source>
        <translation>包名稱</translation>
    </message>
    <message>
        <source>Payment type</source>
        <translation>付款類型</translation>
    </message>
    <message>
        <source>Safety inspection</source>
        <translation>安全檢查</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation>開發人員</translation>
    </message>
    <message>
        <source>Additional terms</source>
        <translation>附加條款</translation>
    </message>
    <message>
        <source>In software charge</source>
        <translation>軟體收費</translation>
    </message>
    <message>
        <source>free</source>
        <translation>自由</translation>
    </message>
    <message>
        <source>Software signature</source>
        <translation>軟體簽名</translation>
    </message>
    <message>
        <source>Signature unit:</source>
        <translation>簽章單位：</translation>
    </message>
    <message>
        <source>Manual recheck</source>
        <translation>手動複查</translation>
    </message>
    <message>
        <source>Intellectual property</source>
        <translation>智慧財產權</translation>
    </message>
    <message>
        <source>Similar Software Recommendation</source>
        <translation>類似軟體推薦</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>得分</translation>
    </message>
    <message>
        <source>User comments</source>
        <translation>用戶評論</translation>
    </message>
    <message>
        <source>comments</source>
        <translation>評論</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>您的反饋是我們做好國內系統軟體生態的動力！</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>最多輸入 200 個字元</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>提交</translation>
    </message>
    <message>
        <source>Please rate before commenting!</source>
        <translation>請在評論前評分！</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>Please install the software before commenting!</source>
        <translation>請在評論前安裝軟體！</translation>
    </message>
    <message>
        <source>You have read all the comments</source>
        <translation>您已閱讀所有評論</translation>
    </message>
    <message>
        <source> comments</source>
        <translation> 評論</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>W+</source>
        <translation>W+</translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppendCommentPopupWindow</name>
    <message>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>其他評論</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>您的反饋是我們做好國內系統軟體生態的動力！</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>最多輸入 200 個字元</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>提交</translation>
    </message>
</context>
<context>
    <name>CategoryShowWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>預設排名</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>下載排名</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>排序排名</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>更新時間排序</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>評論排名</translation>
    </message>
</context>
<context>
    <name>CategoryWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>預設排名</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>下載排名</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>排序排名</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>評論排名</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>更新時間排序</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>環境初始化，介面暫時鎖定，請稍候...</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>重試</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <source>just</source>
        <translation>只</translation>
    </message>
    <message>
        <source>delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>其他評論</translation>
    </message>
</context>
<context>
    <name>CommentWidget</name>
    <message>
        <source>just</source>
        <translation type="vanished">刚刚</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>DownloadRankWidget</name>
    <message>
        <source>Download Ranking</source>
        <translation>下載排名</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>DownloadRankingItem</name>
    <message>
        <source>Uninstall</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
</context>
<context>
    <name>Downloading</name>
    <message>
        <source>Downloading</source>
        <translation>下載</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>全部暫停</translation>
    </message>
    <message>
        <source>Open download directory</source>
        <translation>打開下載目錄</translation>
    </message>
    <message>
        <source>Empty downloaded</source>
        <translation>空下載</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>No download task</source>
        <translation>無下載任務</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplate</name>
    <message>
        <source>Selected Applications</source>
        <translation>選定的應用</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplateCard</name>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation>您的電子郵件/姓名/電話</translation>
    </message>
</context>
<context>
    <name>LandingWidget</name>
    <message>
        <source>No download task</source>
        <translation type="vanished">没有找到你要的软件</translation>
    </message>
    <message>
        <source>No software found</source>
        <translation>未找到軟體</translation>
    </message>
    <message>
        <source>go to</source>
        <translation>轉到</translation>
    </message>
    <message>
        <source> Full library </source>
        <translation> 完整庫 </translation>
    </message>
    <message>
        <source>search and try</source>
        <translation>搜索並嘗試</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID</source>
        <translation>麒麟身份證</translation>
    </message>
    <message>
        <source>Forget?</source>
        <translation>忘記？</translation>
    </message>
    <message>
        <source>Remember it</source>
        <translation>記住它</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>註冊</translation>
    </message>
    <message>
        <source>Your password</source>
        <translation>您的密碼</translation>
    </message>
    <message>
        <source>Your code</source>
        <translation>您的代碼</translation>
    </message>
    <message>
        <source>Your phone number here</source>
        <translation>您的電話號碼在這裡</translation>
    </message>
    <message>
        <source>Pass login</source>
        <translation>通行證登錄</translation>
    </message>
    <message>
        <source>Phone login</source>
        <translation>電話登錄</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>登錄</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>發送</translation>
    </message>
    <message>
        <source>Please move slider to right place</source>
        <translation>請將滑塊移動到正確的位置</translation>
    </message>
    <message>
        <source>%1s left</source>
        <translation>剩餘 %1 秒</translation>
    </message>
    <message>
        <source>User stop verify Captcha</source>
        <translation>使用者停止驗證驗證碼</translation>
    </message>
    <message>
        <source>No response data!</source>
        <translation>沒有回應數據！</translation>
    </message>
    <message>
        <source>Timeout!</source>
        <translation>超時！</translation>
    </message>
    <message>
        <source>Wrong account or password!</source>
        <translation>錯誤的帳戶或密碼！</translation>
    </message>
    <message>
        <source>No network!</source>
        <translation>沒有網路！</translation>
    </message>
    <message>
        <source>Pictrure has expired!</source>
        <translation>皮特魯爾已過期！</translation>
    </message>
    <message>
        <source>User deleted!</source>
        <translation>使用者已刪除！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation>電話號碼已在使用中！</translation>
    </message>
    <message>
        <source>Wrong phone number format!</source>
        <translation>錯誤的電話號碼格式！</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation>你已經達到了極限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation>請檢查您的電話號碼！</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation>請檢查您的代碼！</translation>
    </message>
    <message>
        <source>Send sms Limited!</source>
        <translation>發送簡訊有限！</translation>
    </message>
    <message>
        <source>Pictrure blocked!</source>
        <translation>皮特魯爾被封鎖了！</translation>
    </message>
    <message>
        <source>Illegal code!</source>
        <translation>非法代碼！</translation>
    </message>
    <message>
        <source>Phone code is expired!</source>
        <translation>電話代碼已過期！</translation>
    </message>
    <message>
        <source>Failed attemps limit reached!</source>
        <translation>已達到失敗的試探限制！</translation>
    </message>
    <message>
        <source>Please wait</source>
        <translation>請稍候</translation>
    </message>
    <message>
        <source>Parsing data failed!</source>
        <translation>解析數據失敗！</translation>
    </message>
    <message>
        <source>Server internal error!</source>
        <translation>伺服器內部錯誤！</translation>
    </message>
    <message>
        <source>Slider validate error</source>
        <translation>滑塊驗證錯誤</translation>
    </message>
    <message>
        <source>Phone code error!</source>
        <translation>電話代碼錯誤！</translation>
    </message>
    <message>
        <source>Code can not be empty!</source>
        <translation>代碼不能為空！</translation>
    </message>
    <message>
        <source>MCode can not be empty!</source>
        <translation>MCode不能為空！</translation>
    </message>
    <message>
        <source>Unsupported operation!</source>
        <translation>不支援的操作！</translation>
    </message>
    <message>
        <source>Unsupported Client Type!</source>
        <translation>不支援的客戶端類型！</translation>
    </message>
    <message>
        <source>Please check your input!</source>
        <translation>請檢查您的輸入！</translation>
    </message>
    <message>
        <source>Process failed</source>
        <translation>進程失敗</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>software store</source>
        <translation>軟體商店</translation>
    </message>
    <message>
        <source>homePage</source>
        <translation>主頁</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="vanished">办公</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">图像</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>駕駛</translation>
    </message>
    <message>
        <source>Mobile apps</source>
        <translation>移動應用</translation>
    </message>
    <message>
        <source>All classification</source>
        <translation>所有分類</translation>
    </message>
    <message>
        <source>Software management</source>
        <translation>軟體管理</translation>
    </message>
    <message>
        <source>Not logged in!</source>
        <translation>未登錄！</translation>
    </message>
    <message>
        <source>Log in now</source>
        <translation>立即登錄</translation>
    </message>
    <message>
        <source>User settings</source>
        <translation>用戶設置</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation>註銷</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>登錄</translation>
    </message>
    <message>
        <source>NetWork error</source>
        <translation>網路工作錯誤</translation>
    </message>
    <message>
        <source>Please check the network or try again later</source>
        <translation>請檢查網路或稍後重試</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <source>Comment failed</source>
        <translation>註釋失敗</translation>
    </message>
    <message>
        <source>Contains sensitive words</source>
        <translation>包含敏感詞</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>Failed to get mobile app list.</source>
        <translation type="vanished">获取移动应用列表失败</translation>
    </message>
    <message>
        <source>The obtained application list is empty.</source>
        <translation type="vanished">获取的应用程序列表为空</translation>
    </message>
    <message>
        <source>Get mobile app list...</source>
        <translation>抓取行動應用程式清單...</translation>
    </message>
    <message>
        <source>Failed to get mobile app list, please try again later.</source>
        <translation>無法取得移動應用程式清單，請稍後重試。</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>It is not suitable for the hardware combination of this machine. Please look forward to it.</source>
        <translation>不適合本機的硬體組合。敬請期待。</translation>
    </message>
    <message>
        <source>The obtained mobile app list is empty.</source>
        <translation>獲取的移動應用清單為空。</translation>
    </message>
    <message>
        <source>Please install mobile operating environment!</source>
        <translation>請安裝移動操作環境！</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>全選</translation>
    </message>
    <message>
        <source>709 graphics card optimization, please look forward to!</source>
        <translation>709顯卡優化，敬請期待！</translation>
    </message>
    <message>
        <source>Frequent operation</source>
        <translation>頻繁操作</translation>
    </message>
    <message>
        <source>Please try again later</source>
        <translation>請稍後重試</translation>
    </message>
    <message>
        <source>Multiple likes or cancellations are not allowed</source>
        <translation>不允許多次點讚或取消</translation>
    </message>
    <message>
        <source>Comment like failed</source>
        <translation>像失敗一樣的評論</translation>
    </message>
    <message>
        <source>Failed to delete user comments</source>
        <translation>無法刪除用戶評論</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation>立即更新</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>取消全選</translation>
    </message>
    <message>
        <source>No software to update</source>
        <translation>無需更新軟體</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>更新軟體</translation>
    </message>
    <message>
        <source>No uninstallable software</source>
        <translation>沒有可卸載的軟體</translation>
    </message>
    <message>
        <source>Uninstall software</source>
        <translation>卸載軟體</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>本地歷史安裝</translation>
    </message>
    <message>
        <source>No historical installation</source>
        <translation>無歷史安裝</translation>
    </message>
    <message>
        <source>Historical installation</source>
        <translation>歷史安裝</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>雲歷史記錄安裝</translation>
    </message>
    <message>
        <source>Network problem, please try again later</source>
        <translation>網路問題，請稍後重試</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>全部暫停</translation>
    </message>
    <message>
        <source>all Continue</source>
        <translation>全部繼續</translation>
    </message>
    <message>
        <source>Downloading</source>
        <translation>下載</translation>
    </message>
    <message>
        <source>Download path does not have read/write permission</source>
        <translation>下載路徑沒有讀/寫許可權</translation>
    </message>
    <message>
        <source>Please use a different download path</source>
        <translation>請使用其他下載路徑</translation>
    </message>
    <message>
        <source>set up path</source>
        <translation>設置路徑</translation>
    </message>
    <message>
        <source> is installed</source>
        <translation> 已安裝</translation>
    </message>
    <message>
        <source>Failed to install app!</source>
        <translation>安裝應用程式失敗！</translation>
    </message>
    <message>
        <source>Dependency resolution failed</source>
        <translation>依賴項解析失敗</translation>
    </message>
    <message>
        <source>Install Error</source>
        <translation>安裝錯誤</translation>
    </message>
    <message>
        <source>Insufficient space</source>
        <translation>空間不足</translation>
    </message>
    <message>
        <source>Please check the network</source>
        <translation>請檢查網路</translation>
    </message>
    <message>
        <source>Successfully installed the new version</source>
        <translation>已成功安裝新版本</translation>
    </message>
    <message>
        <source>Open the new software store</source>
        <translation>打開新的軟體商店</translation>
    </message>
    <message>
        <source>Install error</source>
        <translation>安裝錯誤</translation>
    </message>
    <message>
        <source>Please check apt</source>
        <translation type="vanished">请检查apt</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade?</source>
        <translation>確認取消升級？</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>Uninstall error</source>
        <translation>卸載錯誤</translation>
    </message>
    <message>
        <source>New version detected</source>
        <translation>檢測到新版本</translation>
    </message>
    <message>
        <source>Downloading installation package...</source>
        <translation>下載安裝套件...</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade</source>
        <translation type="vanished">确定取消升级？</translation>
    </message>
    <message>
        <source>Cancel upgrade</source>
        <translation>取消升級</translation>
    </message>
    <message>
        <source>Continue upgrading</source>
        <translation>繼續升級</translation>
    </message>
    <message>
        <source>It is currently the latest version</source>
        <translation>它是當前的最新版本</translation>
    </message>
    <message>
        <source>All results </source>
        <translation>所有結果 </translation>
    </message>
    <message>
        <source>Default ranking</source>
        <translation>預設排名</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>下載排名</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>排序排名</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>評論排名</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>更新時間排序</translation>
    </message>
    <message>
        <source>There are still tasks in progress. Are you sure you want to exit?</source>
        <translation>仍有任務正在進行中。您確定要退出嗎？</translation>
    </message>
    <message>
        <source>Exit now, you will not be able to check more applications.</source>
        <translation>現在退出，您將無法檢查更多應用程式。</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <source>apk Security verification failed</source>
        <translation>APK安全驗證失敗</translation>
    </message>
    <message>
        <source>%1</source>
        <translation></translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>關於自動更新</translation>
    </message>
    <message>
        <source>Error Code</source>
        <translation>錯誤代碼</translation>
    </message>
    <message>
        <source>Environment startup failed</source>
        <translation>環境啟動失敗</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>環境初始化，介面暫時鎖定，請稍候...</translation>
    </message>
</context>
<context>
    <name>NewFeaturesWidget</name>
    <message>
        <source>New Features</source>
        <translation>新功能</translation>
    </message>
    <message>
        <source>Release time:</source>
        <translation>發佈時間：</translation>
    </message>
    <message>
        <source>Installed version:</source>
        <translation>安裝版本：</translation>
    </message>
    <message>
        <source>Software size:</source>
        <translation>軟體大小：</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation>最新版本：</translation>
    </message>
</context>
<context>
    <name>NewProductsWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>今天上架的最新 %1 軟體</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>OtherInfomationModule</name>
    <message>
        <source>Privacy policy</source>
        <translation>隱私策略</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-software-center is already running!</source>
        <translation>麒麟軟體中心已經運行了！</translation>
    </message>
</context>
<context>
    <name>RatingWidget</name>
    <message>
        <source>scores</source>
        <translation>分數</translation>
    </message>
    <message>
        <source>full Score 5 points</source>
        <translation>滿分 5分</translation>
    </message>
    <message>
        <source>My score</source>
        <translation>我的分數</translation>
    </message>
    <message>
        <source>Scoring failed</source>
        <translation>評分失敗</translation>
    </message>
    <message>
        <source>Please install the app before scoring</source>
        <translation>請在評分前安裝應用程式</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>No score yet</source>
        <translation>暫無分數</translation>
    </message>
</context>
<context>
    <name>RemoterDaemon</name>
    <message>
        <source>Several new software products in the software store have been launched. Please check more details now!</source>
        <translation type="vanished">软件商店推出了几种新的软件产品,请立即查看更多详细信息！</translation>
    </message>
    <message>
        <source>Software Store</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>New Arrivals</source>
        <translation type="vanished">新品上架</translation>
    </message>
    <message>
        <source>View now</source>
        <translation type="vanished">立即查看</translation>
    </message>
</context>
<context>
    <name>SearchTipWidget</name>
    <message>
        <source>Daily recommendations</source>
        <translation>每日推薦</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>改變</translation>
    </message>
    <message>
        <source>Search all software...</source>
        <translation>搜尋所有軟體...</translation>
    </message>
    <message>
        <source>Software</source>
        <translation>軟體</translation>
    </message>
    <message>
        <source>Mobile Apps</source>
        <translation>移動應用</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>駕駛</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsCard</name>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsWidget</name>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
    <message>
        <source>Selected Applications</source>
        <translation>選定的應用</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <source>software store</source>
        <translation>軟體商店</translation>
    </message>
    <message>
        <source>Download directory settings</source>
        <translation>下載目錄設置</translation>
    </message>
    <message>
        <source>Save the downloaded software in the following directory:</source>
        <translation>將下載的軟體儲存在以下目錄中：</translation>
    </message>
    <message>
        <source>Restore default directory</source>
        <translation>恢復預設目錄</translation>
    </message>
    <message>
        <source>Installation package cleaning</source>
        <translation>安裝包清洗</translation>
    </message>
    <message>
        <source>After installation, the downloaded installation package will be automatically deleted</source>
        <translation>安裝后，下載的安裝包將自動刪除</translation>
    </message>
    <message>
        <source>After installation, keep the downloaded installation package</source>
        <translation>安裝后，保留下載的安裝包</translation>
    </message>
    <message>
        <source>One week after the download, the installation package will be deleted automatically</source>
        <translation>下載一周后，安裝包將自動刪除</translation>
    </message>
    <message>
        <source>Allow the installed software to generate desktop shortcuts</source>
        <translation>允許已安裝的軟體生成桌面快捷方式</translation>
    </message>
    <message>
        <source>Update settings</source>
        <translation>更新設置</translation>
    </message>
    <message>
        <source>Automatic software update</source>
        <translation>自動軟體更新</translation>
    </message>
    <message>
        <source>Automatically download and install software updates</source>
        <translation>自動下載並安裝軟體更新</translation>
    </message>
    <message>
        <source>When the software is updated, a notification is displayed on the right side of the screen</source>
        <translation type="vanished">软件有更新时，在屏幕右侧显示通知</translation>
    </message>
    <message>
        <source>When the software is updated, you will be notified</source>
        <translation type="vanished">软件有更新时，会以通知形式告诉您</translation>
    </message>
    <message>
        <source>Auto update software store</source>
        <translation>自動更新軟體商店</translation>
    </message>
    <message>
        <source>Automatically download the new version of software store and install it after you close software store</source>
        <translation>自動下載新版本的軟體存儲並在關閉軟體存儲后安裝它</translation>
    </message>
    <message>
        <source>Start the software store automatically</source>
        <translation>自動啟動軟體商店</translation>
    </message>
    <message>
        <source>After startup, the software store will start in the background</source>
        <translation>啟動後，軟體商店將在後台啟動</translation>
    </message>
    <message>
        <source>Server address settings</source>
        <translation>伺服器地址設置</translation>
    </message>
    <message>
        <source>If there are internal services, you can change the server address to obtain all services of Kirin software store and Kirin ID.</source>
        <translation>如果有內部服務，可以更改伺服器位址，獲取麒麟軟體商店和麒麟ID的所有服務。</translation>
    </message>
    <message>
        <source>Software store server address:</source>
        <translation>軟體商店伺服器位址：</translation>
    </message>
    <message>
        <source>Kylin ID server address:</source>
        <translation>麒麟ID伺服器位址：</translation>
    </message>
    <message>
        <source>Restore default settings</source>
        <translation>恢復預設設置</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>Download path cannot be empty</source>
        <translation>下載路徑不能為空</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>Download path does not exist</source>
        <translation>下載路徑不存在</translation>
    </message>
</context>
<context>
    <name>SoftwareManagement</name>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>Historical</source>
        <translation>歷史的</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>更新軟體</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>全選</translation>
    </message>
    <message>
        <source>Update all</source>
        <translation>全部更新</translation>
    </message>
    <message>
        <source>Uninstall all</source>
        <translation>全部卸載</translation>
    </message>
    <message>
        <source>Install all</source>
        <translation>全部安裝</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>取消全選</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>本地歷史安裝</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>雲歷史記錄安裝</translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>關於自動更新</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
</context>
<context>
    <name>SortButton</name>
    <message>
        <source>Default ranking</source>
        <translation>預設排名</translation>
    </message>
</context>
<context>
    <name>SortCardWidget</name>
    <message>
        <source>Down</source>
        <translation>下</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>跑</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
</context>
<context>
    <name>SpecialModelCard</name>
    <message>
        <source>more</source>
        <translation>更多</translation>
    </message>
</context>
<context>
    <name>SpecialModelWidget</name>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Search software/mobile app/driver</source>
        <translation>搜索軟體/移動應用程式/驅動程式</translation>
    </message>
    <message>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>homePageWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>今天上架的最新 %1 軟體</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>設置</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation>檢查更新</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>光</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>黑暗</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>kyliin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Software store is a graphical software management tool with rich content. It supports the recommendation of new products and popular applications, and provides one-stop software services such as software search, download, installation, update and uninstall.</source>
        <translation>軟體商店是具有豐富內容的圖形軟體管理工具。支援新產品和熱門應用的推薦，提供軟體搜索、下載、安裝、更新、卸載等一站式軟體服務。</translation>
    </message>
    <message>
        <source>close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <source>Software store</source>
        <translation>軟體商店</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本:</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>服務與支援： </translation>
    </message>
    <message>
        <source>kylin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Disclaimers:</source>
        <translation>免責 聲明：</translation>
    </message>
    <message>
        <source>Kylin Software Store Disclaimers</source>
        <translation>麒麟軟體商店免責聲明</translation>
    </message>
</context>
<context>
    <name>newproductcard</name>
    <message>
        <source>Open</source>
        <translation>打開</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安裝</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸載</translation>
    </message>
    <message>
        <source>N</source>
        <translation>N</translation>
    </message>
</context>
<context>
    <name>sideBarWidget</name>
    <message>
        <source>Software Store</source>
        <translation>軟體商店</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>登錄</translation>
    </message>
</context>
</TS>
