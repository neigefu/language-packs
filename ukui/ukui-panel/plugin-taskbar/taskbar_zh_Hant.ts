<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>UKUITaskBar</name>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1132"/>
        <source>Drop Error</source>
        <translation>掉線錯誤</translation>
    </message>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1133"/>
        <source>File/URL &apos;%1&apos; cannot be embedded into QuickLaunch for now</source>
        <translation>顯示在window. File/URL &apos;%1&apos;上，暫時不能嵌入QuickLaunch中。</translation>
    </message>
</context>
<context>
    <name>UKUITaskButton</name>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="563"/>
        <source>Unpin from taskbar</source>
        <translation>從任務列取消固定</translation>
    </message>
</context>
<context>
    <name>UKUITaskGroup</name>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="213"/>
        <source>Group</source>
        <translation>組</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="218"/>
        <source>Unpin from taskbar</source>
        <translation>從任務列取消固定</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="220"/>
        <source>Pin to taskbar</source>
        <translation>固定到任務列</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="229"/>
        <source>close</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>UKUITaskWidget</name>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="306"/>
        <source>Widget</source>
        <translation>小組件</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="309"/>
        <source>close</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="310"/>
        <source>restore</source>
        <translation>恢復</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="312"/>
        <source>maximaze</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="315"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="316"/>
        <source>above</source>
        <translation>置頂</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="317"/>
        <source>clear</source>
        <translation>取消置頂</translation>
    </message>
</context>
</TS>
