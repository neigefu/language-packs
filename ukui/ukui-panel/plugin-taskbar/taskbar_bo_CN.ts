<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>UKUITaskBar</name>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1132"/>
        <source>Drop Error</source>
        <translation>སྐུད་པ་ཤོར་ནས་ནོར་འཁྲུལ་ཤོར</translation>
    </message>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1133"/>
        <source>File/URL &apos;%1&apos; cannot be embedded into QuickLaunch for now</source>
        <translation>མངོན་པ་ནི་windoལ་མངོན་པ་ཡིན། File/URL&apos;1&apos;ཡི་སྟེང་དུ་གནས་སྐབས་སུ་Qucklannhhenhanhieststhiesthiesthancehhanchaehhanchhanhaehhaehar</translation>
    </message>
</context>
<context>
    <name>UKUITaskButton</name>
    <message>
        <source>Application</source>
        <translation type="obsolete">ཉེར་སྤྱོད་བྱ་རིམ།</translation>
    </message>
    <message>
        <source>Desktop &amp;%1</source>
        <translation type="vanished">ཅོག་ངོས་ཀྱི་རྣམ་པ།&amp;%1</translation>
    </message>
    <message>
        <source>&amp;Move</source>
        <translation type="vanished">སྤོ་བ།&amp;M</translation>
    </message>
    <message>
        <source>Resi&amp;ze</source>
        <translation type="vanished">ཆེ་ཆུང་སྒྱུར་བ།&amp;z</translation>
    </message>
    <message>
        <source>Ma&amp;ximize</source>
        <translation type="vanished">སྒེའུ་ཁུང་ཆེས་ཆེར་སྐྱེད།&amp;x</translation>
    </message>
    <message>
        <source>Maximize vertically</source>
        <translation type="vanished">སྒེའུ་ཁུང་དྲང་འཕྱང་དུ་ཆེས་ཆེར་སྐྱེད།</translation>
    </message>
    <message>
        <source>Maximize horizontally</source>
        <translation type="vanished">སྒེའུ་ཁུང་ཆུ་སྙོམས་སུ་ཆེས་ཆེར་སྐྱེད།</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">ཕྱིར་ལོག&amp;R</translation>
    </message>
    <message>
        <source>Mi&amp;nimize</source>
        <translation type="vanished">སྒེའུ་ཁུང་ཆུང་སྒྱུར།&amp;n</translation>
    </message>
    <message>
        <source>Roll down</source>
        <translation type="vanished">མར་དུ་འདེད།</translation>
    </message>
    <message>
        <source>Roll up</source>
        <translation type="vanished">ཡར་དུ་འདེད།་</translation>
    </message>
    <message>
        <source>&amp;Layer</source>
        <translation type="vanished">རིས་རིམ།&amp;L</translation>
    </message>
    <message>
        <source>Always on &amp;top</source>
        <translation type="vanished">ནམ་ཡང་རྩེ་མོར་གནས་པ།&amp;t</translation>
    </message>
    <message>
        <source>&amp;Normal</source>
        <translation type="vanished">དཀྱུས་མ།&amp;N</translation>
    </message>
    <message>
        <source>&amp;Close</source>
        <translation type="vanished">ཁ་རྒྱག་པ།&amp;C</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="563"/>
        <source>Unpin from taskbar</source>
        <translation>ལས་འགན་སྡེ་ནས་གཏན་འཇགས་འདོར་བ།</translation>
    </message>
</context>
<context>
    <name>UKUITaskGroup</name>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="213"/>
        <source>Group</source>
        <translation>ཚོ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="218"/>
        <source>Unpin from taskbar</source>
        <translation>ལས་འགན་སྡེ་ནས་གཏན་འཇགས་འདོར་བ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="220"/>
        <source>Pin to taskbar</source>
        <translation>ལས་འགན་སྡེ་རུ་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="229"/>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
</context>
<context>
    <name>UKUITaskWidget</name>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="306"/>
        <source>Widget</source>
        <translation>ཚོ་ཆུང་གི་ལྷུ་ལག</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="309"/>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="310"/>
        <source>restore</source>
        <translation>སླར་གསོ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="312"/>
        <source>maximaze</source>
        <translation>ཆེ་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="315"/>
        <source>minimize</source>
        <translation>ཆུང་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="316"/>
        <source>above</source>
        <translation>ཡན་ཆད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="317"/>
        <source>clear</source>
        <translation>གཙང་སེལ།</translation>
    </message>
</context>
</TS>
