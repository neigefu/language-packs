<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>UKUITaskBar</name>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1132"/>
        <source>Drop Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠪᠠᠨ ᠤᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1133"/>
        <source>File/URL &apos;%1&apos; cannot be embedded into QuickLaunch for now</source>
        <translation>winnoww ᠹᠢᠯᠧ /URL〉1〉 ᠳᠡᠭᠡᠷ᠎ᠡ ᠂ ᠲᠦᠷ ᠴᠠᠭ QuickLanch ᠳᠣᠲᠣᠷ᠎ᠠ ᠲᠦᠷ ᠣᠷᠣᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ ᠃</translation>
    </message>
</context>
<context>
    <name>UKUITaskButton</name>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="563"/>
        <source>Unpin from taskbar</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠠᠴᠠ ᠲᠣᠭᠲᠠᠮᠠᠯ ᠢ ᠦᠭᠡᠶᠢᠰᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>UKUITaskGroup</name>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="213"/>
        <source>Group</source>
        <translation>ᠳᠤᠭᠤᠢ᠌ᠯᠠᠩ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="218"/>
        <source>Unpin from taskbar</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠠᠴᠠ ᠲᠣᠭᠲᠠᠮᠠᠯ ᠢ ᠦᠭᠡᠶᠢᠰᠬᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="220"/>
        <source>Pin to taskbar</source>
        <translation>ᠡᠭᠦᠷᠭᠡ ᠶᠢᠨ ᠪᠤᠯᠤᠩ ᠳᠤ ᠲᠣᠭᠲᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="229"/>
        <source>close</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
</context>
<context>
    <name>UKUITaskWidget</name>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="306"/>
        <source>Widget</source>
        <translation>ᠳᠤᠭᠤᠢ᠌ᠯᠠᠩ ᠲᠣᠨᠣᠭ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="309"/>
        <source>close</source>
        <translation>ᠤᠬᠤᠷᠢᠵᠤ ᠭᠠᠷᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="310"/>
        <source>restore</source>
        <translation>ᠰᠡᠷᠭᠦᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="312"/>
        <source>maximaze</source>
        <translation>ᠬᠠᠮᠤᠭ ᠶᠡᠬᠡ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="315"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠪᠠᠭ᠎ᠠ ᠪᠣᠯᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="316"/>
        <source>above</source>
        <translation>ᠣᠷᠣᠢ ᠲᠠᠯᠪᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="317"/>
        <source>clear</source>
        <translation>ᠣᠷᠣᠢ ᠶᠢ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
</context>
</TS>
