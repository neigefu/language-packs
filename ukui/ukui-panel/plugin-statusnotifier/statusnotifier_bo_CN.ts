<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>StatusNotifierStorageArrow</name>
    <message>
        <location filename="../statusnotifier_storagearrow.cpp" line="97"/>
        <location filename="../statusnotifier_storagearrow.cpp" line="107"/>
        <location filename="../statusnotifier_storagearrow.cpp" line="127"/>
        <location filename="../statusnotifier_storagearrow.cpp" line="135"/>
        <source>Expand the hidden icon</source>
        <translation>རི་མོ་སྦས་སྐུང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../statusnotifier_storagearrow.cpp" line="101"/>
        <location filename="../statusnotifier_storagearrow.cpp" line="111"/>
        <location filename="../statusnotifier_storagearrow.cpp" line="124"/>
        <location filename="../statusnotifier_storagearrow.cpp" line="132"/>
        <source>Collapse the display icon</source>
        <translation>ལྟེབ་བརྩེགས་ཀྱིས་རི་མོ་མངོན་པ།</translation>
    </message>
</context>
</TS>
