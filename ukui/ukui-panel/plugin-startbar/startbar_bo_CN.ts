<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>StartMenuButton</name>
    <message>
        <location filename="../startmenu_button.cpp" line="446"/>
        <source>UKUI Menu</source>
        <translation>འགོ་རྩོམ་འདེམས་བྱང་།</translation>
    </message>
    <message>
        <source>User Action</source>
        <translation type="vanished">འགུལ་སྟངས་གཏན་རང་འཁེལ།</translation>
    </message>
    <message>
        <source>Suspend or Hibernate</source>
        <translation type="vanished">ངལ་གསོ།</translation>
    </message>
    <message>
        <source>Power Supply</source>
        <translation type="vanished">གློག་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="166"/>
        <source>Lock Screen</source>
        <translation>སྒྲོག་ཡོལ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="173"/>
        <source>Switch User</source>
        <translation>སྤྱོད་མཁན་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="180"/>
        <source>Log Out</source>
        <translation>ཐོ་ཁོངས་ནས་འདོར་བ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="194"/>
        <source>Hibernate</source>
        <translation>མལ་གསོ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="187"/>
        <source>Suspend</source>
        <translation>མལ་གསོ་བ་།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="111"/>
        <source>Power Manager</source>
        <translation>གློག་ཁུངས་དོ་དམ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="117"/>
        <source>About This Computer</source>
        <translation>འཕྲུལ་ཆས་འདིའི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="123"/>
        <source>Network Settings</source>
        <translation>དྲ་རྒྱའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="130"/>
        <source>System Monitor</source>
        <translation>མ་ལག་གི་ལྟ་ཞིབ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="136"/>
        <source>Control Center</source>
        <translation>རྒྱུད་རིམ་ལྡན་པའི་སྒོ་ནས་སྒྲིག་</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="142"/>
        <source>File Manager</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཡོ་བྱད།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="148"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="154"/>
        <source>Open Terminal</source>
        <translation>མཐའ་སྣེའི་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="160"/>
        <source>Switch User or Log Out</source>
        <translation>ཐོ་ཁོངས་བརྗེ་བའམ་ཡང་ན་ཐོ་ཁོངས་ནས་བསུབ་དགོས།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="162"/>
        <source>Power Options</source>
        <translation>གློག་ཁུངས་བཅས་ཀྱི་རྣམ་གྲངས་འདེམས་དགོས།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="201"/>
        <source>Restart</source>
        <translation>བསྐྱར་སློང་།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="209"/>
        <source>TimeShutdown</source>
        <translation>དུས་ཆད་སྒོ་རྒྱག་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="216"/>
        <source>Shut Down</source>
        <translation>འཁོར་ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="234"/>
        <source>Show Desktop</source>
        <translation>ཅོག་ཙེའི་ངོས་གསལ་པོར་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="241"/>
        <source>All Applications</source>
        <translation>ཚང་མ་བེད་སྤྱོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="254"/>
        <source>Upgrade and Restart</source>
        <translation>གསར་སྒྱུར་གལ་ཆེ་མཉམ་དུ་འཛིན་པ།</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="259"/>
        <source>Upgrade and Shut Down</source>
        <translation>གསར་སྒྱུར་བྱས་པར་མ་ཟད་།ཁ་བརྒྱབ་།</translation>
    </message>
</context>
<context>
    <name>TaskViewButton</name>
    <message>
        <location filename="../taskview_button.cpp" line="129"/>
        <source>Show Taskview</source>
        <translation>ལས་འགན་མཐོང་སྣེ་འཆར་བའི་གནོན་མཐེབ།</translation>
    </message>
</context>
</TS>
