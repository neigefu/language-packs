<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>StartMenuButton</name>
    <message>
        <source>UKui Menu</source>
        <translation type="vanished">开始菜单</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="446"/>
        <source>UKUI Menu</source>
        <translation>ᠡᠬᠢᠯᠡᠯᠳᠡ᠎ᠶᠢᠨ ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <source>User Action</source>
        <translation type="vanished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <source>Suspend or Hibernate</source>
        <translation type="vanished">ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌ ᠪᠤᠶᠤ ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Power Supply</source>
        <translation type="vanished">ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠬᠦᠰᠭᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="166"/>
        <source>Lock Screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="173"/>
        <source>Switch User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="180"/>
        <source>Log Out</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="194"/>
        <source>Hibernate</source>
        <translation>ᠢᠴᠡᠭᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="187"/>
        <source>Suspend</source>
        <translation>ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="111"/>
        <source>Power Manager</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ ᠶᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="117"/>
        <source>About This Computer</source>
        <translation>ᠲᠤᠰ ᠮᠠᠰᠢᠨ ᠤ ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="123"/>
        <source>Network Settings</source>
        <translation>ᠲᠣᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="130"/>
        <source>System Monitor</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮᠲᠦ ᠬᠢᠨᠠᠨ ᠠᠵᠢᠭᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠃</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="136"/>
        <source>Control Center</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠦᠨ ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="142"/>
        <source>File Manager</source>
        <translation>ᠪᠢᠴᠢᠭ᠌ ᠮᠠᠲ᠋ᠧᠷᠢᠶᠠᠯ ᠤᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠲᠠ ᠶᠢᠨ ᠪᠠᠭᠠᠵᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="148"/>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="154"/>
        <source>Open Terminal</source>
        <translation>ᠦᠵᠦᠭᠦᠷ ᠢ ᠨᠡᠭᠡᠭᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="160"/>
        <source>Switch User or Log Out</source>
        <translation>ᠳᠠᠩᠰᠠᠨ ᠡᠷᠦᠬᠡ ᠰᠣᠯᠢᠬᠤ ᠪᠤᠶᠤ ᠬᠦᠴᠦᠨ ᠦᠭᠡᠢ ᠪᠣᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="162"/>
        <source>Power Options</source>
        <translation>ᠴᠠᠬᠢᠯᠭᠠᠨ ᠡᠭᠦᠰᠭᠡᠭᠴᠢ ᠶᠢᠨ ᠰᠣᠩᠭᠣᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="201"/>
        <source>Restart</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="209"/>
        <source>TimeShutdown</source>
        <translation>ᠳᠤᠭᠳᠠᠭᠰᠠᠨ ᠴᠠᠭ᠎ᠲᠤ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="216"/>
        <source>Shut Down</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="234"/>
        <source>Show Desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="241"/>
        <source>All Applications</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="254"/>
        <source>Upgrade and Restart</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠶᠢ ᠵᠡᠷᠭᠡᠪᠡᠷ ᠳᠠᠬᠢᠨ ᠡᠭᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="259"/>
        <source>Upgrade and Shut Down</source>
        <translation>ᠰᠢᠨᠡᠳᠬᠡᠬᠦ ᠶᠢᠨ ᠬᠠᠮᠲᠤ ᠪᠠᠶᠢᠭᠤᠯᠤᠯᠭ᠎ᠠ ᠶᠢ ᠰᠢᠨᠡᠳᠬᠡᠨ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>TaskViewButton</name>
    <message>
        <location filename="../taskview_button.cpp" line="129"/>
        <source>Show Taskview</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
</TS>
