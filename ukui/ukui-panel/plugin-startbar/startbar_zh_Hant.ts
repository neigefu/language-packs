<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_HK">
<context>
    <name>StartMenuButton</name>
    <message>
        <source>UKui Menu</source>
        <translation type="vanished">开始菜单</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="446"/>
        <source>UKUI Menu</source>
        <translation>開始功能表</translation>
    </message>
    <message>
        <source>User Action</source>
        <translation type="vanished">使用者操作</translation>
    </message>
    <message>
        <source>Suspend or Hibernate</source>
        <translation type="vanished">休眠或睡眠</translation>
    </message>
    <message>
        <source>Power Supply</source>
        <translation type="vanished">電源</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="166"/>
        <source>Lock Screen</source>
        <translation>鎖定螢幕</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="173"/>
        <source>Switch User</source>
        <translation>切換使用者</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="180"/>
        <source>Log Out</source>
        <translation>註銷</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="194"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="187"/>
        <source>Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="111"/>
        <source>Power Manager</source>
        <translation>電源管理</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="117"/>
        <source>About This Computer</source>
        <translation>關於本機</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="123"/>
        <source>Network Settings</source>
        <translation>網路設置</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="130"/>
        <source>System Monitor</source>
        <translation>系統監視器</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="136"/>
        <source>Control Center</source>
        <translation>系統設置</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="142"/>
        <source>File Manager</source>
        <translation>檔管理員</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="148"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="154"/>
        <source>Open Terminal</source>
        <translation>打開終端</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="160"/>
        <source>Switch User or Log Out</source>
        <translation>切換帳戶或註銷</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="162"/>
        <source>Power Options</source>
        <translation>電源選項</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="201"/>
        <source>Restart</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="209"/>
        <source>TimeShutdown</source>
        <translation>定時關機</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="216"/>
        <source>Shut Down</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="234"/>
        <source>Show Desktop</source>
        <translation>顯示桌面</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="241"/>
        <source>All Applications</source>
        <translation>全部應用</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="254"/>
        <source>Upgrade and Restart</source>
        <translation>更新並重啟</translation>
    </message>
    <message>
        <location filename="../startmenu_button.cpp" line="259"/>
        <source>Upgrade and Shut Down</source>
        <translation>更新並關機</translation>
    </message>
</context>
<context>
    <name>TaskViewButton</name>
    <message>
        <location filename="../taskview_button.cpp" line="129"/>
        <source>Show Taskview</source>
        <translation>顯示任務檢視</translation>
    </message>
</context>
</TS>
