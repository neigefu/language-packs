<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AlwaysDisplayonPanel</name>
    <message>
        <source>Show NightMode</source>
        <translation type="vanished">显示夜间模式</translation>
    </message>
    <message>
        <location filename="../alwaysdisplayonpanel.cpp" line="36"/>
        <source>Show Taskview</source>
        <translation>ᠤᠯᠠᠨ ᠡᠬᠦᠷᠬᠡ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ ᠪᠡᠷ ᠤᠷᠤᠯᠳᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>GeneralSettings</name>
    <message>
        <location filename="../generalsettings.cpp" line="49"/>
        <source>Merge icons on the taskbar</source>
        <translation>ᠡᠬᠦᠷᠬᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠨᠡᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Always</source>
        <translation>ᠢᠮᠠᠭᠲᠠ ᠨᠡᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Never</source>
        <translation>ᠶᠡᠷᠦ ᠡᠴᠡ ᠨᠡᠢᠯᠡᠬᠦᠯᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="112"/>
        <source>Taskbar Position</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠤ᠋ᠨ ᠳᠡᠯᠭᠡᠴᠡᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="120"/>
        <source>Up</source>
        <translation>ᠤᠷᠤᠢ ᠳ᠋ᠤ᠌</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="121"/>
        <source>Left</source>
        <translation>ᠵᠡᠬᠦᠨ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="122"/>
        <source>Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="119"/>
        <source>Bottom</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ ᠲᠠᠯ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="152"/>
        <source>Panel Size</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="159"/>
        <source>Small</source>
        <translation>ᠪᠠᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="160"/>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="161"/>
        <source>Large</source>
        <translation>ᠲᠤᠮᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="219"/>
        <source>Lock Panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠢ᠋ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="275"/>
        <source>Hide Panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠢ᠋ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UkccPlugin</name>
    <message>
        <location filename="../ukccplugin.cpp" line="11"/>
        <location filename="../ukccplugin.cpp" line="101"/>
        <source>Panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="70"/>
        <source>UkccPlugin</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="72"/>
        <source>ukccplugin panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="115"/>
        <source>Always show  icon in panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠢᠮᠠᠭᠳᠠ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <source>始终显示在任务栏上的图标</source>
        <translation type="obsolete">Always show  icon in panel</translation>
    </message>
    <message>
        <source>ukccplugin test</source>
        <translation type="vanished">任务栏</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
</TS>
