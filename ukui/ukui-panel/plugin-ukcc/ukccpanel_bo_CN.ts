<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AlwaysDisplayonPanel</name>
    <message>
        <location filename="../alwaysdisplayonpanel.cpp" line="36"/>
        <source>Show Taskview</source>
        <translation>ལས་འགན་མང་པོ་མངོན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>GeneralSettings</name>
    <message>
        <location filename="../generalsettings.cpp" line="49"/>
        <source>Merge icons on the taskbar</source>
        <translation>མཉམ་སྒྲིལ་ལས་འགན་སྒྲོམ་སྟེང་གི་རི་མོ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Always</source>
        <translation>ཐོག་མཐའ་བར་གསུམ་དུ་ཟླ་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Never</source>
        <translation>ཟླ་སྒྲིལ་གཏན་ནས་མི་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="112"/>
        <source>Taskbar Position</source>
        <translation>ལས་འགན་སྒྲོམ་ནི་བརྙན་ཤེལ་སྟེང་གི་གནས་ན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="120"/>
        <source>Up</source>
        <translation>རྩེ་མོ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="121"/>
        <source>Left</source>
        <translation>གཡོན་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="122"/>
        <source>Right</source>
        <translation>གཡས་གཡོན།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="119"/>
        <source>Bottom</source>
        <translation>མཐིལ་དུ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="152"/>
        <source>Panel Size</source>
        <translation>ལས་འགན་སྒྲུབ་སའི་ཆེ་ཆུང་ལེགས་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="159"/>
        <source>Small</source>
        <translation>རིང་ཐུང་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="160"/>
        <source>Medium</source>
        <translation>རིང་ཐུང་འབྲིང་བ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="161"/>
        <source>Large</source>
        <translation>ཆེ་ཆུང་ཆེན་པོ།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="219"/>
        <source>Lock Panel</source>
        <translation>ལས་འགན་གྱི་རེའུ་མིག་གཏན་འཁེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="275"/>
        <source>Hide Panel</source>
        <translation>ལས་འགན་གསང་བཅད།</translation>
    </message>
</context>
<context>
    <name>UkccPlugin</name>
    <message>
        <location filename="../ukccplugin.cpp" line="11"/>
        <location filename="../ukccplugin.cpp" line="101"/>
        <source>Panel</source>
        <translation>ལས་འགན་སྒྲུབ་གཞི།</translation>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="70"/>
        <source>UkccPlugin</source>
        <translation>ལས་འགན་སྒྲུབ་གཞི།</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="72"/>
        <source>ukccplugin panel</source>
        <translation>ལས་འགན་སྒྲུབ་གཞི།</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="115"/>
        <source>Always show  icon in panel</source>
        <translation>ཐོག་མཐའ་བར་གསུམ་དུ་ལས་འགན་བྱང་བུའི་སྟེང་དུ་མངོན་པའི་རི་མོ།</translation>
    </message>
    <message>
        <source>始终显示在任务栏上的图标</source>
        <translation type="obsolete">Always show  icon in panel</translation>
    </message>
</context>
</TS>
