<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AlwaysDisplayonPanel</name>
    <message>
        <location filename="../alwaysdisplayonpanel.cpp" line="36"/>
        <source>Show Taskview</source>
        <translation>多任务视图</translation>
    </message>
</context>
<context>
    <name>GeneralSettings</name>
    <message>
        <location filename="../generalsettings.cpp" line="49"/>
        <source>Merge icons on the taskbar</source>
        <translation>合并任务栏上的图标</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Always</source>
        <translation>始终</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Never</source>
        <translation>从不</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="112"/>
        <source>Taskbar Position</source>
        <translation>任务栏在屏幕上的位置</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="120"/>
        <source>Up</source>
        <translation>顶部</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="121"/>
        <source>Left</source>
        <translation>左侧</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="122"/>
        <source>Right</source>
        <translation>右侧</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="118"/>
        <location filename="../generalsettings.cpp" line="119"/>
        <source>Bottom</source>
        <translation>底部</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="152"/>
        <source>Panel Size</source>
        <translation>任务栏大小</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="159"/>
        <source>Small</source>
        <translation>小</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="160"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="158"/>
        <location filename="../generalsettings.cpp" line="161"/>
        <source>Large</source>
        <translation>大</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="219"/>
        <source>Lock Panel</source>
        <translation>锁定任务栏</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="275"/>
        <source>Hide Panel</source>
        <translation>自动隐藏任务栏</translation>
    </message>
</context>
<context>
    <name>UkccPlugin</name>
    <message>
        <location filename="../ukccplugin.cpp" line="11"/>
        <location filename="../ukccplugin.cpp" line="101"/>
        <source>Panel</source>
        <translation>任务栏</translation>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="70"/>
        <source>UkccPlugin</source>
        <translation>任务栏</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="72"/>
        <source>ukccplugin panel</source>
        <translation>任务栏</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="115"/>
        <source>Always show  icon in panel</source>
        <translation>始终显示在任务栏上的图标</translation>
    </message>
    <message>
        <source>始终显示在任务栏上的图标</source>
        <translation type="obsolete">Always show  icon in panel</translation>
    </message>
    <message>
        <source>ukccplugin test</source>
        <translation type="vanished">任务栏</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
</TS>
