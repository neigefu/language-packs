<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AlwaysDisplayonPanel</name>
    <message>
        <source>Show NightMode</source>
        <translation type="vanished">显示夜间模式</translation>
    </message>
    <message>
        <location filename="../alwaysdisplayonpanel.cpp" line="36"/>
        <source>Show Taskview</source>
        <translation>多任務檢視</translation>
    </message>
</context>
<context>
    <name>GeneralSettings</name>
    <message>
        <location filename="../generalsettings.cpp" line="49"/>
        <source>Merge icons on the taskbar</source>
        <translation>合併任務列上的圖示</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Always Merge</source>
        <translation>始終合併</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="56"/>
        <source>Never merge</source>
        <translation>從不合併</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="111"/>
        <source>Taskbar Position</source>
        <translation>任務欄在螢幕上的位置</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="117"/>
        <location filename="../generalsettings.cpp" line="119"/>
        <source>Up</source>
        <translation>頂部</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="117"/>
        <location filename="../generalsettings.cpp" line="120"/>
        <source>Left</source>
        <translation>左側</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="117"/>
        <location filename="../generalsettings.cpp" line="121"/>
        <source>Right</source>
        <translation>右側</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="117"/>
        <location filename="../generalsettings.cpp" line="118"/>
        <source>Bottom</source>
        <translation>底部</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="149"/>
        <source>Panel Size</source>
        <translation>任務欄大小</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="155"/>
        <location filename="../generalsettings.cpp" line="156"/>
        <source>Small</source>
        <translation>小</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="155"/>
        <location filename="../generalsettings.cpp" line="157"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="155"/>
        <location filename="../generalsettings.cpp" line="158"/>
        <source>Large</source>
        <translation>大</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="216"/>
        <source>Lock Panel</source>
        <translation>鎖定任務列</translation>
    </message>
    <message>
        <location filename="../generalsettings.cpp" line="262"/>
        <source>Hide Panel</source>
        <translation>自動隱藏任務列</translation>
    </message>
</context>
<context>
    <name>UkccPlugin</name>
    <message>
        <location filename="../ukccplugin.cpp" line="11"/>
        <location filename="../ukccplugin.cpp" line="100"/>
        <source>Panel</source>
        <translation>任務列</translation>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="69"/>
        <source>UkccPlugin</source>
        <translation>任務列</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="71"/>
        <source>ukccplugin panel</source>
        <translation>任務列</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
    <message>
        <location filename="../ukccplugin.cpp" line="111"/>
        <source>Always show  icon in panel</source>
        <translation>始終顯示在任務列上的圖示</translation>
    </message>
    <message>
        <source>始终显示在任务栏上的图标</source>
        <translation type="obsolete">Always show  icon in panel</translation>
    </message>
    <message>
        <source>ukccplugin test</source>
        <translation type="vanished">任务栏</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
</TS>
