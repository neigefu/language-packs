<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>UKUIPanel</name>
    <message>
        <location filename="../ukuipanel.cpp" line="787"/>
        <source>Small</source>
        <translation>小尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="789"/>
        <source>Medium</source>
        <translation>中尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="791"/>
        <source>Large</source>
        <translation>大尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="796"/>
        <source>Adjustment Size</source>
        <translation>調整大小</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="829"/>
        <source>Up</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="831"/>
        <source>Bottom</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="833"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="835"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="838"/>
        <source>Adjustment Position</source>
        <translation>調整位置</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="864"/>
        <source>Hide Panel</source>
        <translation>隱藏任務列</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1176"/>
        <source>Set Panel</source>
        <translation>設置任務列</translation>
    </message>
    <message>
        <source>Set up Panel</source>
        <translation type="vanished">设置任务栏</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1184"/>
        <source>Show Taskview</source>
        <translation>顯示「任務檢視」 按鈕</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1193"/>
        <source>Show Desktop</source>
        <translation>顯示桌面</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1200"/>
        <source>Show System Monitor</source>
        <translation>系統監視器</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1209"/>
        <source>Lock This Panel</source>
        <translation>鎖定任務列</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1221"/>
        <location filename="../ukuipanel.cpp" line="1222"/>
        <source>About Kylin</source>
        <translation>關於麒麟</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="79"/>
        <source>Use alternate configuration file.</source>
        <translation>使用備用設定檔.</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="80"/>
        <source>Configuration file</source>
        <translation>配置檔</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="85"/>
        <source>ukui-panel set mode </source>
        <translation>ukui任務欄設置模式 </translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="86"/>
        <source>panel set option</source>
        <translation>任務列設置選項</translation>
    </message>
</context>
</TS>
