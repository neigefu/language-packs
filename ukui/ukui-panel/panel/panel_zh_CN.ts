<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>UKUIPanel</name>
    <message>
        <location filename="../ukuipanel.cpp" line="787"/>
        <source>Small</source>
        <translation>小尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="789"/>
        <source>Medium</source>
        <translation>中尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="791"/>
        <source>Large</source>
        <translation>大尺寸</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="796"/>
        <source>Adjustment Size</source>
        <translation>调整大小</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="829"/>
        <source>Up</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="831"/>
        <source>Bottom</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="833"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="835"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="838"/>
        <source>Adjustment Position</source>
        <translation>调整位置</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="864"/>
        <source>Hide Panel</source>
        <translation>隐藏任务栏</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1176"/>
        <source>Set Panel</source>
        <translation>设置任务栏</translation>
    </message>
    <message>
        <source>Set up Panel</source>
        <translation type="vanished">设置任务栏</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1184"/>
        <source>Show Taskview</source>
        <translation>显示“任务视图”按钮</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1193"/>
        <source>Show Desktop</source>
        <translation>显示桌面</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1200"/>
        <source>Show System Monitor</source>
        <translation>系统监视器</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1209"/>
        <source>Lock This Panel</source>
        <translation>锁定任务栏</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1221"/>
        <location filename="../ukuipanel.cpp" line="1222"/>
        <source>About Kylin</source>
        <translation>关于麒麟</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="79"/>
        <source>Use alternate configuration file.</source>
        <translation>使用备用配置文件.</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="80"/>
        <source>Configuration file</source>
        <translation>配置文件</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="85"/>
        <source>ukui-panel set mode </source>
        <translation>ukui任务栏设置模式 </translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="86"/>
        <source>panel set option</source>
        <translation>任务栏设置选项</translation>
    </message>
</context>
</TS>
