<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>UKUIPanel</name>
    <message>
        <location filename="../ukuipanel.cpp" line="837"/>
        <source>Small</source>
        <translation>ᠪᠠᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="839"/>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="841"/>
        <source>Large</source>
        <translation>ᠶᠡᠬᠡ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="846"/>
        <source>Adjustment Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ᠎ᠶᠢ ᠵᠤᠬᠢᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="882"/>
        <source>Up</source>
        <translation>ᠳᠡᠭᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="884"/>
        <source>Bottom</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="886"/>
        <source>Left</source>
        <translation>ᠵᠡᠬᠦᠨ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="888"/>
        <source>Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="891"/>
        <source>Adjustment Position</source>
        <translation>ᠪᠠᠢᠷᠢ᠎ᠶᠢ ᠵᠤᠬᠢᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="917"/>
        <source>Hide Panel</source>
        <translation>ᠡᠬᠦᠷᠬᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠬᠡ᠎ᠶᠢ ᠨᠢᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1176"/>
        <source>Set Panel</source>
        <translation>ᠫᠡᠨᠧᠯ᠎ᠢ᠋ ᠵᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Set up Panel</source>
        <translation type="vanished">设置任务栏</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1246"/>
        <source>Show Taskview</source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ᠎ᠤᠨ ᠳᠠᠷᠤᠪᠴᠢ᠎ᠶᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1263"/>
        <source>Show Nightmode</source>
        <translation>ᠰᠦᠨᠢ᠎ᠶᠢᠨ ᠵᠠᠭᠪᠤᠷ᠎ᠤᠨ ᠳᠠᠷᠤᠪᠴᠢ᠎ᠶᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1269"/>
        <source>Show Desktop</source>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1276"/>
        <source>Show System Monitor</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠬᠢᠨᠠᠨ ᠡᠵᠡᠮᠳᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1301"/>
        <source>Lock This Panel</source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠬᠡ᠎ᠶᠢ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1316"/>
        <source>About Kylin</source>
        <translation>ᠴᠢ ᠯᠢᠨ᠎ᠦ ᠲᠤᠬᠠᠢ</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="79"/>
        <source>Use alternate configuration file.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="80"/>
        <source>Configuration file</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ᠎ᠤᠨ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="85"/>
        <source>ukui-panel set mode </source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠭᠡ᠎ᠶᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠬᠡᠪ </translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="86"/>
        <source>panel set option</source>
        <translation>ᠨᠢᠭᠤᠷ ᠬᠠᠪᠳᠠᠰᠤ᠎ᠶᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ ᠰᠤᠩᠭᠤᠯᠳᠠ</translation>
    </message>
</context>
</TS>
