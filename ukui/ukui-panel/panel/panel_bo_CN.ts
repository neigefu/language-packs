<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>UKUIPanel</name>
    <message>
        <location filename="../ukuipanel.cpp" line="787"/>
        <source>Small</source>
        <translation>ཡིག་གཟུགས་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="789"/>
        <source>Medium</source>
        <translation>འབྲིང་རིམ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="791"/>
        <source>Large</source>
        <translation>ཆེ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="796"/>
        <source>Adjustment Size</source>
        <translation>ཆེ་ཆུང་སྙོམས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="829"/>
        <source>Up</source>
        <translation>སྟེང་།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="831"/>
        <source>Bottom</source>
        <translation>ཞབས་ཁུལ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="833"/>
        <source>Left</source>
        <translation>གཡོན།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="835"/>
        <source>Right</source>
        <translation>གཡས།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="838"/>
        <source>Adjustment Position</source>
        <translation>གནས་ཡུལ་ལེགས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="864"/>
        <source>Hide Panel</source>
        <translation>ངོས་པང་སྦེད་པ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1176"/>
        <source>Set Panel</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་པང་ལེབ</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1184"/>
        <source>Show Taskview</source>
        <translation>ལས་འགན་མཐོང་སྣེ་འཆར་བའི་གནོན་མཐེབ།</translation>
    </message>
    <message>
        <source>Show Nightmode</source>
        <translation type="vanished">མཚན་ལྗོངས་རྣམ་པ་འཆར་བའི་གནོན་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1193"/>
        <source>Show Desktop</source>
        <translation>ཅོག་ངོས་འཆར་བ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1200"/>
        <source>Show System Monitor</source>
        <translation>བརྒྱུད་ཁོངས་ལྟ་ཞིབ་ཆས།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1209"/>
        <source>Lock This Panel</source>
        <translation>འགན་བྱང་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1221"/>
        <location filename="../ukuipanel.cpp" line="1222"/>
        <source>About Kylin</source>
        <translation>དགུ་ཚིགས་ཆིག་ལིན་གྱི་སྐོར།</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="79"/>
        <source>Use alternate configuration file.</source>
        <translation>སྡེབ་སྒྲིག་ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="80"/>
        <source>Configuration file</source>
        <translation>སྡེབ་སྒྲིག་ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="85"/>
        <source>ukui-panel set mode </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="86"/>
        <source>panel set option</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
