<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ApplicationVolumeWidget</name>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="42"/>
        <source>Application Volume</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="32"/>
        <source>System Volume</source>
        <translation>系统音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="94"/>
        <source>Sound Settings</source>
        <translation>声音设置</translation>
    </message>
</context>
<context>
    <name>DeviceSwitchWidget</name>
    <message>
        <source>Go Into Mini Mode</source>
        <translation type="vanished">进入Mini模式</translation>
    </message>
    <message>
        <source>Output volume control</source>
        <translation type="vanished">输出音量控制</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <source>Sound preference(S)</source>
        <translation type="vanished">声音首选项</translation>
    </message>
    <message>
        <source>Device Volume</source>
        <translation type="vanished">设备音量</translation>
    </message>
    <message>
        <source>Application Volume</source>
        <translation type="vanished">应用音量</translation>
    </message>
    <message>
        <source>is using</source>
        <translation type="vanished">正在使用</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Unable to connect to the sound system, please check whether the pulseaudio service is running!</source>
        <translation type="vanished">无法连接到系统声音，请检查pulseaudio服务是否正在运行！</translation>
    </message>
    <message>
        <source>Dummy output</source>
        <translation type="vanished">伪输出</translation>
    </message>
    <message>
        <source>Speaker (Realtek Audio)</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Headphone</source>
        <translation type="vanished">模拟耳机</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2183"/>
        <source>pa_context_subscribe() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2206"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2213"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2220"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1935"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1950"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2043"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2056"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1919"/>
        <location filename="../ukmedia_volume_control.cpp" line="2117"/>
        <location filename="../ukmedia_volume_control.cpp" line="2143"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1565"/>
        <location filename="../ukmedia_volume_control.cpp" line="1590"/>
        <location filename="../ukmedia_volume_control.cpp" line="1620"/>
        <source>Card callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1654"/>
        <location filename="../ukmedia_volume_control.cpp" line="1769"/>
        <source>Sink callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1712"/>
        <location filename="../ukmedia_volume_control.cpp" line="1789"/>
        <source>Source callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="95"/>
        <source>Fatal Error: Unable to connect to PulseAudio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="256"/>
        <location filename="../ukmedia_volume_control.cpp" line="2105"/>
        <location filename="../ukmedia_volume_control.cpp" line="2192"/>
        <source>pa_context_get_server_info() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1810"/>
        <location filename="../ukmedia_volume_control.cpp" line="2336"/>
        <source>Sink input callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1829"/>
        <source>Source output callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1857"/>
        <source>Client callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1873"/>
        <location filename="../ukmedia_volume_control.cpp" line="1900"/>
        <source>Server info callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1968"/>
        <source>Failed to initialize device restore extension: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1989"/>
        <source>pa_ext_device_restore_read_sink_formats() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2007"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2026"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2069"/>
        <location filename="../ukmedia_volume_control.cpp" line="2082"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2095"/>
        <source>pa_context_get_client_info() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2199"/>
        <source>pa_context_client_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2226"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2233"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2248"/>
        <source>Connection failed, attempting reconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2267"/>
        <source>moduleInfoCb callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="2284"/>
        <source>Ukui Media Volume Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="3111"/>
        <location filename="../ukmedia_main_widget.cpp" line="3138"/>
        <location filename="../ukmedia_main_widget.cpp" line="3187"/>
        <source>pa_context_load_module() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>ukui-volume-control-applet-qt is already running!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaDeviceWidget</name>
    <message>
        <source>Input Device</source>
        <translation type="vanished">输入设备</translation>
    </message>
    <message>
        <source>Microphone</source>
        <translation type="vanished">麦克风</translation>
    </message>
    <message>
        <source>Output Device</source>
        <translation type="vanished">输出设备</translation>
    </message>
    <message>
        <source>Speaker Realtek Audio</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Input device can not be detected</source>
        <translation type="vanished">无法检测到输入设备</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="110"/>
        <source>Output volume control</source>
        <translation>输出音量控制</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="112"/>
        <location filename="../ukmedia_main_widget.cpp" line="114"/>
        <source>Sound preference(S)</source>
        <translation>设置声音项</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="179"/>
        <source>System Volume</source>
        <translation>系统音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="181"/>
        <source>App Volume</source>
        <translation>应用音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="909"/>
        <source>Current volume:</source>
        <translation>当前音量：</translation>
    </message>
</context>
<context>
    <name>UkmediaMiniMasterVolumeWidget</name>
    <message>
        <source>Speaker (Realtek Audio)</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Go Into Full Mode</source>
        <translation type="vanished">进入完整模式</translation>
    </message>
</context>
<context>
    <name>UkmediaSystemVolumeWidget</name>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="69"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="80"/>
        <source>Output</source>
        <translation>输出</translation>
    </message>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="96"/>
        <source>Sound Settings</source>
        <translation>声音设置</translation>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="106"/>
        <location filename="../ukmedia_volume_control.cpp" line="126"/>
        <location filename="../ukmedia_volume_control.cpp" line="129"/>
        <location filename="../ukmedia_volume_control.cpp" line="177"/>
        <location filename="../ukmedia_volume_control.cpp" line="181"/>
        <location filename="../ukmedia_volume_control.cpp" line="194"/>
        <location filename="../ukmedia_volume_control.cpp" line="216"/>
        <location filename="../ukmedia_volume_control.cpp" line="308"/>
        <location filename="../ukmedia_volume_control.cpp" line="312"/>
        <location filename="../ukmedia_volume_control.cpp" line="329"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="147"/>
        <source>pa_context_set_source_mute_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="151"/>
        <source>pa_context_set_source_volume_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="349"/>
        <source>pa_context_move_sink_input_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="368"/>
        <source>pa_context_move_source_output_by_name() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="389"/>
        <location filename="../ukmedia_volume_control.cpp" line="393"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="411"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="429"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="448"/>
        <location filename="../ukmedia_volume_control.cpp" line="455"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="473"/>
        <source>pa_context_set_default_source() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="489"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="504"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="517"/>
        <source>pa_context_kill_sink_input() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="546"/>
        <source> (plugged in)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="550"/>
        <location filename="../ukmedia_volume_control.cpp" line="715"/>
        <source> (unavailable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="552"/>
        <location filename="../ukmedia_volume_control.cpp" line="712"/>
        <source> (unplugged)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1069"/>
        <source>Failed to read data from stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1111"/>
        <source>Peak detect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1112"/>
        <source>Failed to create monitoring stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1127"/>
        <source>Failed to connect monitoring stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1286"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1536"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1882"/>
        <source>pa_context_get_sink_info_by_name() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1886"/>
        <source>pa_context_get_source_info_by_name() failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiMediaSetHeadsetWidget</name>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="36"/>
        <source>Sound Settings</source>
        <translation>声音设置</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="37"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="38"/>
        <source>Select Sound Device</source>
        <translation>选择输出设备</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="66"/>
        <source>Headphone</source>
        <translation>耳机</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="67"/>
        <source>Headset</source>
        <translation>耳麦</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="68"/>
        <source>Microphone</source>
        <translation>麦克风</translation>
    </message>
</context>
</TS>
