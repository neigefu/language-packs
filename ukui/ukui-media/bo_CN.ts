<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Audio</name>
    <message>
        <location filename="../audio.ui" line="26"/>
        <location filename="../audio.cpp" line="38"/>
        <source>Audio</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../audio.cpp" line="93"/>
        <source>UkccPlugin</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་ཕུའུ་ལུའུ་ཀིན།</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../audio.cpp" line="95"/>
        <source>ukccplugin test</source>
        <translation>ukccplugin ཚད་ལེན་ཚོད་ལྟ།</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
<context>
    <name>InputDevWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="409"/>
        <source>Input Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="446"/>
        <source>confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutputDevWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="110"/>
        <source>Output Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="147"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="217"/>
        <location filename="../ukmedia_volume_control.cpp" line="1742"/>
        <location filename="../ukmedia_volume_control.cpp" line="1821"/>
        <source>pa_context_get_server_info() failed</source>
        <translation>pa_context_get_server_info()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1336"/>
        <source>Card callback failure</source>
        <translation>བྱང་བུ་ཕྱིར་སློག་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1354"/>
        <location filename="../ukmedia_volume_control.cpp" line="1435"/>
        <source>Sink callback failure</source>
        <translation>ཕྱིར་ལྡོག་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1391"/>
        <location filename="../ukmedia_volume_control.cpp" line="1457"/>
        <source>Source callback failure</source>
        <translation>འབྱུང་ཁུངས་ཕྱིར་ལྡོག་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1481"/>
        <location filename="../ukmedia_volume_control.cpp" line="1975"/>
        <source>Sink input callback failure</source>
        <translation>མ་དངུལ་འཇོག་པའི་ཕྱིར་ལྡོག་ལ་ཕམ་ཉེས་བྱུང</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1500"/>
        <source>Source output callback failure</source>
        <translation>འབྱུང་ཁུངས་ཕྱིར་ལྡོག་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1545"/>
        <source>Client callback failure</source>
        <translation>མངགས་བཅོལ་བྱེད་མཁན་གྱིས་ཕྱིར་སློག་</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1561"/>
        <location filename="../ukmedia_volume_control.cpp" line="1575"/>
        <source>Server info callback failure</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ཀྱི་ཆ་འཕྲིན་ཕྱིར་ལྡོག་བྱེད་མ་ཐུབ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1610"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation>stream_restore་རིང་དུ་གཏོང་བའི་ཐོག་མའི་དུས་ཚོད་ལ་སླེབས་མ་ཐུབ་པ། %s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1627"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation>pa_ext_stream_restore_read()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1644"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation>སྒྲིག་ཆས་དོ་དམ་གྱི་དུས་འགྱངས་ལ་ཐོག་མའི་དུས་འགྱངས་བྱེད་མ་ཐུབ་པ། %s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1663"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation>pa_ext_device_manager_read()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1680"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation>pa_context_get_sink_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1693"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation>pa_context_get_source_info_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1706"/>
        <location filename="../ukmedia_volume_control.cpp" line="1719"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation>pa_context_get_sink_input_info()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1732"/>
        <source>pa_context_get_client_info() failed</source>
        <translation>pa_context_get_client_info()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1772"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation>pa_context_get_card_info_by_index()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1812"/>
        <source>pa_context_subscribe() failed</source>
        <translation>pa_context_subscribe()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1828"/>
        <source>pa_context_client_info_list() failed</source>
        <translation>pa_context_client_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1835"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation>pa_context_get_card_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1842"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation>pa_context_get_sink_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1849"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation>pa_context_get_source_info_list()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1856"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation>pa_context_get_sink_input_info_list()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1863"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation>pa_context_get_source_output_info_list()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1879"/>
        <source>Connection failed, attempting reconnect</source>
        <translation>འབྲེལ་མཐུད་བྱེད་མ་ཐུབ་པར་ཡང་བསྐྱར་འབྲེལ་མཐུད་བྱེད་རྩིས་བྱས།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1899"/>
        <source>Ukui Media Volume Control</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་སྨྱན་སྦྱོར་གྱི་བོངས་</translation>
    </message>
</context>
<context>
    <name>UkmediaAppCtrlWidget</name>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="37"/>
        <source>App Sound Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="333"/>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="349"/>
        <source>None</source>
        <translation type="unfinished">མེད་པ་</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="857"/>
        <source>System Volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaAppItemWidget</name>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="12"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="42"/>
        <source>Output Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="72"/>
        <source>Input Device</source>
        <translation type="unfinished">ནང་འཇུག་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="88"/>
        <source>Output Device</source>
        <translation type="unfinished">ཐོན་སྐྱེད་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="104"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaDevControlWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="14"/>
        <source>Sound Equipment Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="26"/>
        <source>Output Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="27"/>
        <source>Input Devices</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="57"/>
        <source>Input</source>
        <translation>མ་དངུལ་འཇོག་པ།</translation>
        <extra-contents_path>/Audio/Input</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="62"/>
        <source>Input Device</source>
        <translation>ནང་འཇུག་སྒྲིག་ཆས།</translation>
        <extra-contents_path>/Audio/Input Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="65"/>
        <source>Volume</source>
        <translation>བོངས་ཚད།</translation>
        <extra-contents_path>/Audio/Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="70"/>
        <source>Input Level</source>
        <translation>མ་དངུལ་འཇོག་པའི་ཆུ་ཚད</translation>
        <extra-contents_path>/Audio/Input Level</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="79"/>
        <source>Noise Reduction</source>
        <translation>སུན་སྒྲ་ཇེ་ཉུང་དུ་གཏོང</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="83"/>
        <source>Voice Monitor</source>
        <translation>སྒྲིག་ཆས་འདི་ལ་ཉན་ཞིབ་།</translation>
        <extra-contents_path>/audio/Voice Monitor</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="85"/>
        <source>You can hear your voice in the output device of your choice</source>
        <translation>འོན་ཀྱང་བདམས་པའི་ཕྱིར་འདོན་སྒྲིག་ཆས་ནང་ནས་རང་ཉིད་ཀྱི་སྐད་ཐོས་།</translation>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="84"/>
        <source>(None Device)</source>
        <translation>(སྒྲག་ཆས་མེད་པ་)</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="271"/>
        <location filename="../ukmedia_main_widget.cpp" line="366"/>
        <location filename="../ukmedia_main_widget.cpp" line="1077"/>
        <source>Custom</source>
        <translation>མཚན་ཉིད་རང་འཇོག་</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="1509"/>
        <location filename="../ukmedia_main_widget.cpp" line="1511"/>
        <location filename="../ukmedia_main_widget.cpp" line="1514"/>
        <location filename="../ukmedia_main_widget.cpp" line="2773"/>
        <location filename="../ukmedia_main_widget.cpp" line="2778"/>
        <location filename="../ukmedia_main_widget.cpp" line="2784"/>
        <location filename="../ukmedia_main_widget.cpp" line="2789"/>
        <location filename="../ukmedia_main_widget.cpp" line="2805"/>
        <location filename="../ukmedia_main_widget.cpp" line="2813"/>
        <location filename="../ukmedia_main_widget.cpp" line="2862"/>
        <location filename="../ukmedia_main_widget.cpp" line="2967"/>
        <location filename="../ukmedia_main_widget.cpp" line="3097"/>
        <location filename="../ukmedia_main_widget.cpp" line="3339"/>
        <source>None</source>
        <translation>མེད་པ་</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="74"/>
        <source>Output</source>
        <translation>ཐོན་ཚད།</translation>
        <extra-contents_path>/Audio/Output</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="78"/>
        <source>Output Device</source>
        <translation>ཐོན་སྐྱེད་སྒྲིག་ཆས།</translation>
        <extra-contents_path>/Audio/Output Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="81"/>
        <source>Master Volume</source>
        <translation>སློབ་དཔོན་གྱི་བམ་པོ།</translation>
        <extra-contents_path>/Audio/Master Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="86"/>
        <source>Balance</source>
        <translation>དོ་མཉམ་</translation>
        <extra-contents_path>/Audio/Balance</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="87"/>
        <source>Left</source>
        <translation>གཡོན</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="89"/>
        <source>Right</source>
        <translation>གཡས་</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="93"/>
        <source>Volume Increase</source>
        <translation>གྲངས་འབོར་འཕར་སྣོན་བྱུང་བ</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="94"/>
        <source>Volume above 100% can cause sound distortion and damage your speakers.</source>
        <translation>སྒྲ་ཚད་བརྒྱ་ཆ་༡༠༠ལས་བརྒལ་ན་སྒྲ་ནུས་བདེན་མེད་དང་ཁྱོད་ཀྱི་སྒྲ་ཕབ་ཆས་ལ་གནོད་པ་བྱེད་སྲིད།.</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="116"/>
        <source>Mono Audio</source>
        <translation>སྒྲ་ལམ་རྐྱང་པའི་སྒྲ་ཟློས་</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="117"/>
        <source>It merges the left and right channels into one channel.</source>
        <translation>གཡོན་སྒྲ་ལམ་དང་གཡས་སྒྲ་ལམ་ཞིག་ཏུ་ཟླ་སྒྲིལ་གཏོང་སྲིད་།སྒྲ་ལམ་ཞིག་ཏུ་གྱུར་།.</translation>
    </message>
</context>
<context>
    <name>UkmediaSettingsWidget</name>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="15"/>
        <source>Advanced Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="19"/>
        <source>Sound Equipment Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="21"/>
        <location filename="../ukmedia_settings_widget.cpp" line="26"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="24"/>
        <source>App Sound Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="58"/>
        <source>System Sound</source>
        <translation>མ་ལག་འཐུས་སྒོ་ཚང་བ</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="61"/>
        <source>Sound Theme</source>
        <translation>སྒྲའི་བརྗོད་བྱ་གཙོ་བོ།</translation>
        <extra-contents_path>/Audio/Sound Theme</extra-contents_path>
    </message>
    <message>
        <source>Alert Sound</source>
        <translation type="vanished">ཉེན་བརྡའི་སྒྲ་</translation>
        <extra-contents_path>/Audio/Alert Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="64"/>
        <source>Beep Switch</source>
        <translation>སྦྲང་མའི་གློག་སྒོ།</translation>
        <extra-contents_path>/Audio/Beep Switch</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="66"/>
        <source>Poweroff Music</source>
        <translation>སྒུལ་ཤུགས་རོལ་དབྱངས།</translation>
        <extra-contents_path>/Audio/Poweroff Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="68"/>
        <source>Startup Music</source>
        <translation>གསར་གཏོད་རོལ་དབྱངས།</translation>
        <extra-contents_path>/Audio/Startup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="70"/>
        <source>Wakeup Music</source>
        <translation>གཉིད་ལས་སད་པའི་རོལ་མོ།</translation>
        <extra-contents_path>/Audio/Wakeup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="74"/>
        <source>Notification Sound</source>
        <translation>བསྡུ་ལེན་བརྡ་ཐོ།</translation>
        <extra-contents_path>/Audio/Notification Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="78"/>
        <source>Volume Control Sound</source>
        <translation>སྒྲ་ཚད་སྙོམ་སྒྲིག</translation>
        <extra-contents_path>/Audio/Volume Control Sound</extra-contents_path>
    </message>
    <message>
        <source>Volume Change</source>
        <translation type="vanished">སྒྲ་གདངས་འགྱུར་ལྡོག</translation>
        <extra-contents_path>/Audio/Volume Change</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="72"/>
        <source>Logout Music</source>
        <translation>ཐོ་འགོད་རོལ་དབྱངས།</translation>
        <extra-contents_path>/Audio/Logout Music</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="67"/>
        <location filename="../ukmedia_volume_control.cpp" line="92"/>
        <location filename="../ukmedia_volume_control.cpp" line="96"/>
        <location filename="../ukmedia_volume_control.cpp" line="111"/>
        <location filename="../ukmedia_volume_control.cpp" line="164"/>
        <location filename="../ukmedia_volume_control.cpp" line="239"/>
        <location filename="../ukmedia_volume_control.cpp" line="243"/>
        <location filename="../ukmedia_volume_control.cpp" line="255"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation>pa_context_set_sink_volume_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="133"/>
        <source>pa_context_set_source_mute_by_index() failed</source>
        <translation>pa_context_set_source_mute_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <source>pa_context_set_source_volume_by_index() failed</source>
        <translation>pa_context_set_source_volume_by_index()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="272"/>
        <location filename="../ukmedia_volume_control.cpp" line="276"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation>pa_context_set_source_output_volume()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="288"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation>pa_context_set_source_output_mute()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="300"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation>pa_context_set_card_profile_by_index()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="314"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation>pa_context_set_default_sink()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="327"/>
        <source>pa_context_set_default_source() failed</source>
        <translation>pa_context_set_default_source()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="343"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation>pa_context_set_sink_port_by_name()ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="357"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation>pa_context_set_source_port_by_name()ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="386"/>
        <source> (plugged in)</source>
        <translation> (ནང་དུ་བཅུག་པ་)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="390"/>
        <location filename="../ukmedia_volume_control.cpp" line="581"/>
        <source> (unavailable)</source>
        <translation> (སྤྱོད་གོ་མི་ཆོད་པ)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="392"/>
        <location filename="../ukmedia_volume_control.cpp" line="578"/>
        <source> (unplugged)</source>
        <translation> (ཁ་ཐོར་ཡ་བྲལ་དུ་སོང་བ་)།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="742"/>
        <source>Failed to read data from stream</source>
        <translation>ཆུ་ཕྲན་ནས་གཞི་གྲངས་ཀློག་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="786"/>
        <source>Peak detect</source>
        <translation>ཡང་རྩེར་ཞིབ་དཔྱད་ཚད་ལེན</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="787"/>
        <source>Failed to create monitoring stream</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་གྱི་ཆུ་ཕྲན་གསར་སྐྲུན་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="801"/>
        <source>Failed to connect monitoring stream</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་གྱི་ཆུ་ཕྲན་སྦྲེལ་མཐུད་བྱེད་མ་ཐུབ་པ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="916"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation>དོན་རྐྱེན་ཞིག་ཏུ་བརྩིས་ནས་དོན་རྐྱེན་ཆུང་ཆུང་ཞིག་ཏུ་བརྩིས་ནས་ཐག་གཅོད་བྱས་པའི་རྐྱེན་གྱིས་ཆུ་ནང་དུ་འཛུལ་བར་སྣང་མེད་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1006"/>
        <source>pa_context_kill_source_output() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1302"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation>ཕུའུ་ལུའུ་ཧྲི་ཨོ་ཏོ་དང་འབྲེལ་བ་བཙུགས་པ་རེད། སྐུ་མཁྱེན་སྒུག་རོགས།...</translation>
    </message>
</context>
</TS>
