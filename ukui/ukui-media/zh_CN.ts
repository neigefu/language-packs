<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Audio</name>
    <message>
        <location filename="../audio.ui" line="26"/>
        <location filename="../audio.cpp" line="38"/>
        <source>Audio</source>
        <translation>声音</translation>
    </message>
    <message>
        <location filename="../audio.cpp" line="93"/>
        <source>UkccPlugin</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../audio.cpp" line="95"/>
        <source>ukccplugin test</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
<context>
    <name>InputDevWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="409"/>
        <source>Input Devices</source>
        <translation>输入设备</translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="446"/>
        <source>confirm</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>OutputDevWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="110"/>
        <source>Output Devices</source>
        <translation>输出设备</translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="147"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="217"/>
        <location filename="../ukmedia_volume_control.cpp" line="1742"/>
        <location filename="../ukmedia_volume_control.cpp" line="1821"/>
        <source>pa_context_get_server_info() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1336"/>
        <source>Card callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1354"/>
        <location filename="../ukmedia_volume_control.cpp" line="1435"/>
        <source>Sink callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1391"/>
        <location filename="../ukmedia_volume_control.cpp" line="1457"/>
        <source>Source callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1481"/>
        <location filename="../ukmedia_volume_control.cpp" line="1975"/>
        <source>Sink input callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1500"/>
        <source>Source output callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1545"/>
        <source>Client callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1561"/>
        <location filename="../ukmedia_volume_control.cpp" line="1575"/>
        <source>Server info callback failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1610"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1627"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1644"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1663"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1680"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1693"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1706"/>
        <location filename="../ukmedia_volume_control.cpp" line="1719"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1732"/>
        <source>pa_context_get_client_info() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1772"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1812"/>
        <source>pa_context_subscribe() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1828"/>
        <source>pa_context_client_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1835"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1842"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1849"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1856"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1863"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1879"/>
        <source>Connection failed, attempting reconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1899"/>
        <source>Ukui Media Volume Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaAppCtrlWidget</name>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="37"/>
        <source>App Sound Control</source>
        <translation>应用声音</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="333"/>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="349"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="857"/>
        <source>System Volume</source>
        <translation>系统</translation>
    </message>
</context>
<context>
    <name>UkmediaAppItemWidget</name>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="12"/>
        <source>Application</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="42"/>
        <source>Output Volume</source>
        <translation>输出音量</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="72"/>
        <source>Input Device</source>
        <translation>输入设备</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="88"/>
        <source>Output Device</source>
        <translation>输出设备</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="104"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
</context>
<context>
    <name>UkmediaDevControlWidget</name>
    <message>
        <source>audio device control</source>
        <translation type="vanished">声音设备管理</translation>
    </message>
    <message>
        <source>output device</source>
        <translation type="vanished">输出设备</translation>
    </message>
    <message>
        <source>input device</source>
        <translation type="vanished">输入设备</translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="14"/>
        <source>Sound Equipment Control</source>
        <translation type="unfinished">声音设备管理</translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="26"/>
        <source>Output Devices</source>
        <translation type="unfinished">输出设备</translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="27"/>
        <source>Input Devices</source>
        <translation type="unfinished">输入设备</translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="57"/>
        <source>Input</source>
        <translation>输入</translation>
        <extra-contents_path>/Audio/Input</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="62"/>
        <source>Input Device</source>
        <translation>选择输入设备</translation>
        <extra-contents_path>/Audio/Input Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="65"/>
        <source>Volume</source>
        <translation>音量</translation>
        <extra-contents_path>/Audio/Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="70"/>
        <source>Input Level</source>
        <translation>音量反馈</translation>
        <extra-contents_path>/Audio/Input Level</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="79"/>
        <source>Noise Reduction</source>
        <translation>智能降噪</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="83"/>
        <source>Voice Monitor</source>
        <translation>侦听此设备</translation>
        <extra-contents_path>/audio/Voice Monitor</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="84"/>
        <source>(None Device)</source>
        <translation type="unfinished">(无设备)</translation>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="85"/>
        <source>You can hear your voice in the output device of your choice</source>
        <translation>可在所选输出设备中听到自己的声音</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <source>Light-Seeking</source>
        <translation type="obsolete">寻光</translation>
    </message>
    <message>
        <source>HeYin</source>
        <translation type="obsolete">和印</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="271"/>
        <location filename="../ukmedia_main_widget.cpp" line="366"/>
        <location filename="../ukmedia_main_widget.cpp" line="1077"/>
        <source>Custom</source>
        <translation>自定义</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="1509"/>
        <location filename="../ukmedia_main_widget.cpp" line="1511"/>
        <location filename="../ukmedia_main_widget.cpp" line="1514"/>
        <location filename="../ukmedia_main_widget.cpp" line="2773"/>
        <location filename="../ukmedia_main_widget.cpp" line="2778"/>
        <location filename="../ukmedia_main_widget.cpp" line="2784"/>
        <location filename="../ukmedia_main_widget.cpp" line="2789"/>
        <location filename="../ukmedia_main_widget.cpp" line="2805"/>
        <location filename="../ukmedia_main_widget.cpp" line="2813"/>
        <location filename="../ukmedia_main_widget.cpp" line="2862"/>
        <location filename="../ukmedia_main_widget.cpp" line="2967"/>
        <location filename="../ukmedia_main_widget.cpp" line="3097"/>
        <location filename="../ukmedia_main_widget.cpp" line="3339"/>
        <source>None</source>
        <translation>无</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="74"/>
        <source>Output</source>
        <translation>输出</translation>
        <extra-contents_path>/Audio/Output</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="78"/>
        <source>Output Device</source>
        <translation>选择输出设备</translation>
        <extra-contents_path>/Audio/Output Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="81"/>
        <source>Master Volume</source>
        <translation>音量</translation>
        <extra-contents_path>/Audio/Master Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="86"/>
        <source>Balance</source>
        <translation>声道平衡</translation>
        <extra-contents_path>/Audio/Balance</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="87"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="89"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="93"/>
        <source>Volume Increase</source>
        <translation>音量增强</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="94"/>
        <source>Volume above 100% can cause sound distortion and damage your speakers.</source>
        <translation>音量超过100%时可能会导致音效失真并损害你的扬声器。</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="116"/>
        <source>Mono Audio</source>
        <translation>单声道音频</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="117"/>
        <source>It merges the left and right channels into one channel.</source>
        <translation>会将左声道和右声道合并成一个声道</translation>
    </message>
</context>
<context>
    <name>UkmediaSettingsWidget</name>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="15"/>
        <source>Advanced Settings</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="19"/>
        <source>Sound Equipment Control</source>
        <translation>声音设备管理</translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="21"/>
        <location filename="../ukmedia_settings_widget.cpp" line="26"/>
        <source>Details</source>
        <translation>详情</translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="24"/>
        <source>App Sound Control</source>
        <translation>应用声音</translation>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="58"/>
        <source>System Sound</source>
        <translation>系统音效</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="61"/>
        <source>Sound Theme</source>
        <translation>音效主题</translation>
        <extra-contents_path>/Audio/Sound Theme</extra-contents_path>
    </message>
    <message>
        <source>Alert Sound</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="64"/>
        <source>Beep Switch</source>
        <translation>提示音</translation>
        <extra-contents_path>/Audio/Beep Switch</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="66"/>
        <source>Poweroff Music</source>
        <translation>关机</translation>
        <extra-contents_path>/Audio/Poweroff Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="68"/>
        <source>Startup Music</source>
        <translation>开机</translation>
        <extra-contents_path>/Audio/Startup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="70"/>
        <source>Wakeup Music</source>
        <translation>唤醒</translation>
        <extra-contents_path>/Audio/Wakeup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="74"/>
        <source>Notification Sound</source>
        <translation>接收通知</translation>
        <extra-contents_path>/Audio/Notification Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="78"/>
        <source>Volume Control Sound</source>
        <translation>音量调节</translation>
        <extra-contents_path>/Audio/Volume Control Sound</extra-contents_path>
    </message>
    <message>
        <source>Volume Change</source>
        <translation type="vanished">音量调节</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="72"/>
        <source>Logout Music</source>
        <translation>注销</translation>
        <extra-contents_path>/Audio/Logout Music</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="67"/>
        <location filename="../ukmedia_volume_control.cpp" line="92"/>
        <location filename="../ukmedia_volume_control.cpp" line="96"/>
        <location filename="../ukmedia_volume_control.cpp" line="111"/>
        <location filename="../ukmedia_volume_control.cpp" line="164"/>
        <location filename="../ukmedia_volume_control.cpp" line="239"/>
        <location filename="../ukmedia_volume_control.cpp" line="243"/>
        <location filename="../ukmedia_volume_control.cpp" line="255"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="133"/>
        <source>pa_context_set_source_mute_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <source>pa_context_set_source_volume_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="272"/>
        <location filename="../ukmedia_volume_control.cpp" line="276"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="288"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="300"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="314"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="327"/>
        <source>pa_context_set_default_source() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="343"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="357"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="386"/>
        <source> (plugged in)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="390"/>
        <location filename="../ukmedia_volume_control.cpp" line="581"/>
        <source> (unavailable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="392"/>
        <location filename="../ukmedia_volume_control.cpp" line="578"/>
        <source> (unplugged)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="742"/>
        <source>Failed to read data from stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="786"/>
        <source>Peak detect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="787"/>
        <source>Failed to create monitoring stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="801"/>
        <source>Failed to connect monitoring stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="916"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1006"/>
        <source>pa_context_kill_source_output() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1302"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
