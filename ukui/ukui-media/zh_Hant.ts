<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Audio</name>
    <message>
        <location filename="../audio.ui" line="26"/>
        <location filename="../audio.cpp" line="38"/>
        <source>Audio</source>
        <translation>聲音</translation>
    </message>
    <message>
        <location filename="../audio.cpp" line="93"/>
        <source>UkccPlugin</source>
        <translation>Ukcc外掛程式</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../audio.cpp" line="95"/>
        <source>ukccplugin test</source>
        <translation>ukcc外掛程式 測試</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="217"/>
        <location filename="../ukmedia_volume_control.cpp" line="1640"/>
        <location filename="../ukmedia_volume_control.cpp" line="1719"/>
        <source>pa_context_get_server_info() failed</source>
        <translation>pa_context_get_server_info（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1228"/>
        <source>Card callback failure</source>
        <translation>卡片回調失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1246"/>
        <location filename="../ukmedia_volume_control.cpp" line="1333"/>
        <source>Sink callback failure</source>
        <translation>輸出回調失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1282"/>
        <location filename="../ukmedia_volume_control.cpp" line="1354"/>
        <source>Source callback failure</source>
        <translation>輸入回調失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1376"/>
        <location filename="../ukmedia_volume_control.cpp" line="1873"/>
        <source>Sink input callback failure</source>
        <translation>水槽輸入回調失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1395"/>
        <source>Source output callback failure</source>
        <translation>source-ouput回調失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1443"/>
        <source>Client callback failure</source>
        <translation>用戶端回調失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1459"/>
        <location filename="../ukmedia_volume_control.cpp" line="1473"/>
        <source>Server info callback failure</source>
        <translation>服務端回調錯誤</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1508"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation>初始化stream_restore擴展失敗：%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1525"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation>pa_ext_stream_restore_read（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1542"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation>初始化設備管理器擴展失敗：%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1561"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation>pa_ext_device_manager_read（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1578"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation>pa_context_get_sink_info_by_index（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1591"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation>pa_context_get_source_info_by_index（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1604"/>
        <location filename="../ukmedia_volume_control.cpp" line="1617"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation>pa_context_get_sink_input_info（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1630"/>
        <source>pa_context_get_client_info() failed</source>
        <translation>pa_context_get_client_info（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1670"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation>pa_context_get_card_info_by_index（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1710"/>
        <source>pa_context_subscribe() failed</source>
        <translation>pa_context_subscribe（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1726"/>
        <source>pa_context_client_info_list() failed</source>
        <translation>pa_context_client_info_list（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1733"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation>pa_context_get_card_info_list（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1740"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation>pa_context_get_sink_info_list（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1747"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation>pa_context_get_source_info_list（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1754"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation>pa_context_get_sink_input_info_list（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1761"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation>pa_context_get_source_output_info_list（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1777"/>
        <source>Connection failed, attempting reconnect</source>
        <translation>連接失敗，嘗試重新連接中</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1797"/>
        <source>Ukui Media Volume Control</source>
        <translation>UKUI 媒體音量控制</translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="53"/>
        <source>Input</source>
        <translation>輸入</translation>
        <extra-contents_path>/Audio/Input</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="58"/>
        <source>Input Device</source>
        <translation>選擇輸入設備</translation>
        <extra-contents_path>/Audio/Input Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="61"/>
        <source>Volume</source>
        <translation>音量</translation>
        <extra-contents_path>/Audio/Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="66"/>
        <source>Input Level</source>
        <translation>輸入反饋</translation>
        <extra-contents_path>/Audio/Input Level</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="77"/>
        <source>Noise Reduction</source>
        <translation>智慧降噪</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="2454"/>
        <location filename="../ukmedia_main_widget.cpp" line="2461"/>
        <location filename="../ukmedia_main_widget.cpp" line="2548"/>
        <location filename="../ukmedia_main_widget.cpp" line="2646"/>
        <location filename="../ukmedia_main_widget.cpp" line="2737"/>
        <location filename="../ukmedia_main_widget.cpp" line="2743"/>
        <location filename="../ukmedia_main_widget.cpp" line="2748"/>
        <location filename="../ukmedia_main_widget.cpp" line="2795"/>
        <location filename="../ukmedia_main_widget.cpp" line="2829"/>
        <location filename="../ukmedia_main_widget.cpp" line="3045"/>
        <source>None</source>
        <translation>空</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="72"/>
        <source>Output</source>
        <translation>輸出</translation>
        <extra-contents_path>/Audio/Output</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="76"/>
        <source>Output Device</source>
        <translation>選擇輸出設備</translation>
        <extra-contents_path>/Audio/Output Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="79"/>
        <source>Master Volume</source>
        <translation>音量</translation>
        <extra-contents_path>/Audio/Master Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="84"/>
        <source>Balance</source>
        <translation>聲道平衡</translation>
        <extra-contents_path>/Audio/Balance</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="85"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="87"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="90"/>
        <source>Volume Increase</source>
        <translation>音量增強</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="58"/>
        <source>System Sound</source>
        <translation>系統音效</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="61"/>
        <source>Sound Theme</source>
        <translation>音效主題</translation>
        <extra-contents_path>/Audio/Sound Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="64"/>
        <source>Alert Sound</source>
        <translation>通知</translation>
        <extra-contents_path>/Audio/Alert Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="67"/>
        <source>Beep Switch</source>
        <translation>提示音</translation>
        <extra-contents_path>/Audio/Beep Switch</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="69"/>
        <source>Poweroff Music</source>
        <translation>關機</translation>
        <extra-contents_path>/Audio/Poweroff Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="71"/>
        <source>Startup Music</source>
        <translation>開機</translation>
        <extra-contents_path>/Audio/Startup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="73"/>
        <source>Wakeup Music</source>
        <translation>喚醒</translation>
        <extra-contents_path>/Audio/Wakeup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="75"/>
        <source>Volume Change</source>
        <translation>音量調節</translation>
        <extra-contents_path>/Audio/Volume Change</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="77"/>
        <source>Logout Music</source>
        <translation>註銷</translation>
        <extra-contents_path>/Audio/Logout Music</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="67"/>
        <location filename="../ukmedia_volume_control.cpp" line="92"/>
        <location filename="../ukmedia_volume_control.cpp" line="96"/>
        <location filename="../ukmedia_volume_control.cpp" line="111"/>
        <location filename="../ukmedia_volume_control.cpp" line="164"/>
        <location filename="../ukmedia_volume_control.cpp" line="239"/>
        <location filename="../ukmedia_volume_control.cpp" line="243"/>
        <location filename="../ukmedia_volume_control.cpp" line="255"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation>pa_context_set_sink_volume_by_index（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="133"/>
        <source>pa_context_set_source_mute_by_index() failed</source>
        <translation>pa_context_set_source_mute_by_index（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <source>pa_context_set_source_volume_by_index() failed</source>
        <translation>pa_context_set_source_volume_by_index（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="272"/>
        <location filename="../ukmedia_volume_control.cpp" line="276"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation>pa_context_set_source_output_volume（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="288"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation>pa_context_set_source_output_mute（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="300"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation>pa_context_set_card_profile_by_index（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="314"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation>pa_context_set_default_sink（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="327"/>
        <location filename="../ukmedia_volume_control.cpp" line="337"/>
        <location filename="../ukmedia_volume_control.cpp" line="805"/>
        <location filename="../ukmedia_volume_control.cpp" line="923"/>
        <location filename="../ukmedia_volume_control.cpp" line="1313"/>
        <source>pa_context_set_default_source() failed</source>
        <translation>pa_context_set_default_source（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="356"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation>pa_context_set_sink_port_by_name（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="370"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation>pa_context_set_source_port_by_name（） 執行失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="399"/>
        <source> (plugged in)</source>
        <translation> （插上了）</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="403"/>
        <location filename="../ukmedia_volume_control.cpp" line="550"/>
        <source> (unavailable)</source>
        <translation> （ 不可用 ）</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="405"/>
        <location filename="../ukmedia_volume_control.cpp" line="547"/>
        <source> (unplugged)</source>
        <translation> 沒有連接</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="696"/>
        <source>Failed to read data from stream</source>
        <translation>從流中讀取數據失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="746"/>
        <source>Peak detect</source>
        <translation>峰值檢測</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="747"/>
        <source>Failed to create monitoring stream</source>
        <translation>創建監控流失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="762"/>
        <source>Failed to connect monitoring stream</source>
        <translation>連接監控流失敗</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="881"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation>忽略沉降輸入，因為它被指定為事件，因此由事件小部件處理</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1194"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation>正在建立與PulseAudio的連接。 請等待...</translation>
    </message>
</context>
</TS>
