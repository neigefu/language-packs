<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Audio</name>
    <message>
        <location filename="../audio.ui" line="26"/>
        <location filename="../audio.cpp" line="38"/>
        <source>Audio</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋</translation>
    </message>
    <message>
        <location filename="../audio.cpp" line="93"/>
        <source>UkccPlugin</source>
        <translation>Ukcc ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ</translation>
        <extra-contents_path>/UkccPlugin/UkccPlugin</extra-contents_path>
    </message>
    <message>
        <location filename="../audio.cpp" line="95"/>
        <source>ukccplugin test</source>
        <translation>Ukcc ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ ᠳᠤᠷᠰᠢᠬᠤ</translation>
        <extra-contents_path>/UkccPlugin/ukccplugin test</extra-contents_path>
    </message>
</context>
<context>
    <name>InputDevWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="409"/>
        <source>Input Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="446"/>
        <source>confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutputDevWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="110"/>
        <source>Output Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="147"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="217"/>
        <location filename="../ukmedia_volume_control.cpp" line="1742"/>
        <location filename="../ukmedia_volume_control.cpp" line="1821"/>
        <source>pa_context_get_server_info() failed</source>
        <translation>pa_context_get_server_info() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1336"/>
        <source>Card callback failure</source>
        <translation>ᠺᠠᠷᠲ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1354"/>
        <location filename="../ukmedia_volume_control.cpp" line="1435"/>
        <source>Sink callback failure</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠭᠠᠷᠭᠠᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1391"/>
        <location filename="../ukmedia_volume_control.cpp" line="1457"/>
        <source>Source callback failure</source>
        <translation>ᠡᠬᠡᠬᠦᠯᠬᠦ ᠵᠢᠨ ᠤᠷᠤᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1481"/>
        <location filename="../ukmedia_volume_control.cpp" line="1975"/>
        <source>Sink input callback failure</source>
        <translation>ᠤᠰᠤᠨ ᠬᠤᠪᠢᠯ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠤᠷᠤᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1500"/>
        <source>Source output callback failure</source>
        <translation>source-ouput ᠡᠬᠡᠬᠦᠯᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1545"/>
        <source>Client callback failure</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷᠯᠢᠭ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1561"/>
        <location filename="../ukmedia_volume_control.cpp" line="1575"/>
        <source>Server info callback failure</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠢ᠋ ᠡᠬᠡᠬᠦᠯᠬᠦ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1610"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation>stream_restore ᠵᠢ/ ᠢ᠋ ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠦᠷᠬᠡᠳᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ᠄%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1627"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation>pa_ext_stream_restore_read() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1644"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠯᠳᠠ ᠵᠢᠨ ᠪᠠᠭᠠᠵᠢ ᠵᠢ ᠠᠨᠭᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠦᠷᠬᠡᠳᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ᠄%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1663"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation>pa_ext_device_manager_read() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1680"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation>pa_context_get_sink_info_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1693"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation>pa_context_get_source_info_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1706"/>
        <location filename="../ukmedia_volume_control.cpp" line="1719"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation>pa_context_get_sink_input_info() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1732"/>
        <source>pa_context_get_client_info() failed</source>
        <translation>pa_context_get_client_info() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1772"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation>pa_context_get_card_info_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1812"/>
        <source>pa_context_subscribe() failed</source>
        <translation>pa_context_subscribe() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1828"/>
        <source>pa_context_client_info_list() failed</source>
        <translation>pa_context_client_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1835"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation>pa_context_get_card_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1842"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation>pa_context_get_sink_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1849"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation>pa_context_get_source_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1856"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation>pa_context_get_sink_input_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1863"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation>pa_context_get_source_output_info_list() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1879"/>
        <source>Connection failed, attempting reconnect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠵᠢ ᠳᠤᠷᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1899"/>
        <source>Ukui Media Volume Control</source>
        <translation>Ukui ᠵᠠᠭᠤᠴᠢᠯᠠᠭᠤᠷ ᠤ᠋ᠨ ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠡᠵᠡᠮᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>UkmediaAppCtrlWidget</name>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="37"/>
        <source>App Sound Control</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="333"/>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="349"/>
        <source>None</source>
        <translation type="unfinished">ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_device_ctrl.cpp" line="857"/>
        <source>System Volume</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaAppItemWidget</name>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="12"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="42"/>
        <source>Output Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="72"/>
        <source>Input Device</source>
        <translation type="unfinished">ᠤᠷᠤᠭᠤᠯᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="88"/>
        <source>Output Device</source>
        <translation type="unfinished">ᠭᠠᠷᠭᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../app-device-control/ukmedia_app_item_widget.cpp" line="104"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaDevControlWidget</name>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="14"/>
        <source>Sound Equipment Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="26"/>
        <source>Output Devices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../audio-device-control/ukmedia_device_control_widget.cpp" line="27"/>
        <source>Input Devices</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaInputWidget</name>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="57"/>
        <source>Input</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Audio/Input</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="62"/>
        <source>Input Device</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
        <extra-contents_path>/Audio/Input Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="65"/>
        <source>Volume</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
        <extra-contents_path>/Audio/Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="70"/>
        <source>Input Level</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯᠳᠠ</translation>
        <extra-contents_path>/Audio/Input Level</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="79"/>
        <source>Noise Reduction</source>
        <translation>ᠤᠶᠤᠯᠢᠭ ᠵᠢᠨᠷ ᠱᠤᠤᠬᠢᠶᠠᠨ ᠢ᠋ ᠪᠠᠭᠠᠰᠬᠠᠬᠤ</translation>
        <extra-contents_path>/audio/Noise</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="83"/>
        <source>Voice Monitor</source>
        <translation>ᠠᠪᠢᠶ᠎ᠠ ᠪᠠᠷ ᠠᠵᠢᠭᠯᠠᠬᠤ ᠮᠠᠰᠢᠨ ᠃</translation>
        <extra-contents_path>/audio/Voice Monitor</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="84"/>
        <source>(None Device)</source>
        <translation>( ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠦᠭᠡᠶ )</translation>
    </message>
    <message>
        <location filename="../ukmedia_input_widget.cpp" line="85"/>
        <source>You can hear your voice in the output device of your choice</source>
        <translation>ᠲᠠ ᠲᠠᠨ ᠤ ᠰᠤᠩᠭ᠋ᠤᠭᠰᠠᠨ ᠭᠠᠷᠭᠠᠬᠤ ᠲᠥᠬᠥᠭᠡᠷᠦᠮᠵᠢ ᠳᠦ ᠲᠠᠨ ᠤ ᠳᠠᠭᠤ ᠶᠢ ᠣᠯᠵᠤ ᠰᠣᠨᠣᠰᠴᠤ ᠪᠣᠯᠣᠨ᠎ᠠ᠃</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="271"/>
        <location filename="../ukmedia_main_widget.cpp" line="366"/>
        <location filename="../ukmedia_main_widget.cpp" line="1077"/>
        <source>Custom</source>
        <translation>ᠳᠠᠳᠬᠠᠯ ᠃</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="1509"/>
        <location filename="../ukmedia_main_widget.cpp" line="1511"/>
        <location filename="../ukmedia_main_widget.cpp" line="1514"/>
        <location filename="../ukmedia_main_widget.cpp" line="2773"/>
        <location filename="../ukmedia_main_widget.cpp" line="2778"/>
        <location filename="../ukmedia_main_widget.cpp" line="2784"/>
        <location filename="../ukmedia_main_widget.cpp" line="2789"/>
        <location filename="../ukmedia_main_widget.cpp" line="2805"/>
        <location filename="../ukmedia_main_widget.cpp" line="2813"/>
        <location filename="../ukmedia_main_widget.cpp" line="2862"/>
        <location filename="../ukmedia_main_widget.cpp" line="2967"/>
        <location filename="../ukmedia_main_widget.cpp" line="3097"/>
        <location filename="../ukmedia_main_widget.cpp" line="3339"/>
        <source>None</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
</context>
<context>
    <name>UkmediaOutputWidget</name>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="74"/>
        <source>Output</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Audio/Output</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="78"/>
        <source>Output Device</source>
        <translation>ᠭᠠᠷᠭᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠰᠤᠨᠭᠭᠤᠬᠤ</translation>
        <extra-contents_path>/Audio/Output Device</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="81"/>
        <source>Master Volume</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ</translation>
        <extra-contents_path>/Audio/Master Volume</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="86"/>
        <source>Balance</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠵᠠᠮ ᠢ᠋ ᠳᠡᠨᠭᠴᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/Audio/Balance</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="87"/>
        <source>Left</source>
        <translation>ᠵᠡᠬᠦᠨ</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="89"/>
        <source>Right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="93"/>
        <source>Volume Increase</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠨᠡᠮᠡᠭᠳᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="94"/>
        <source>Volume above 100% can cause sound distortion and damage your speakers.</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠨᠢ 100% ᠡᠴᠡ ᠥᠨᠳᠥᠷ ᠪᠣᠯᠵᠤ ᠳᠠᠭᠤ ᠨᠢ ᠦᠨᠡᠨ ᠢ ᠠᠯᠳᠠᠭᠰᠠᠨ ᠪᠥᠭᠡᠳ ᠳᠠᠭᠤ ᠬᠠᠲᠠᠭᠠᠭᠤᠷ ᠢ ᠡᠪᠳᠡᠨ᠎ᠡ ᠃</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="116"/>
        <source>Mono Audio</source>
        <translation>ᠳᠠᠩ ᠳᠠᠭᠤᠨ ᠢᠶᠠᠷ ᠳᠠᠭᠤᠨ ᠢᠶᠠᠷ ᠳᠠᠪᠲᠠᠮᠵᠢᠯᠠᠪᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../ukmedia_output_widget.cpp" line="117"/>
        <source>It merges the left and right channels into one channel.</source>
        <translation>ᠲᠡᠷᠡ ᠵᠡᠭᠦᠨ ᠨᠡᠪᠲᠡᠷᠡᠬᠦ ᠵᠠᠮ ᠪᠣᠯᠣᠨ ᠪᠠᠷᠠᠭᠤᠨ ᠨᠡᠪᠲᠡᠷᠡᠬᠦ ᠵᠠᠮ ᠢ ᠨᠡᠶᠢᠯᠡᠭᠦᠯᠬᠦ ᠵᠠᠮ ᠢ ᠨᠡᠶᠢᠯᠡᠭᠦᠯᠦᠨ᠎ᠡ ᠃</translation>
    </message>
</context>
<context>
    <name>UkmediaSettingsWidget</name>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="15"/>
        <source>Advanced Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="19"/>
        <source>Sound Equipment Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="21"/>
        <location filename="../ukmedia_settings_widget.cpp" line="26"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_settings_widget.cpp" line="24"/>
        <source>App Sound Control</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkmediaSoundEffectsWidget</name>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="58"/>
        <source>System Sound</source>
        <translation>ᠰᠢᠰᠲᠸᠮ ᠤ᠋ᠨ ᠳᠠᠭᠤᠨ ᠤ᠋ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="61"/>
        <source>Sound Theme</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ᠋ ᠴᠢᠨᠠᠷ ᠤ᠋ᠨ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
        <extra-contents_path>/Audio/Sound Theme</extra-contents_path>
    </message>
    <message>
        <source>Alert Sound</source>
        <translation type="vanished">ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
        <extra-contents_path>/Audio/Alert Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="64"/>
        <source>Beep Switch</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠤᠮᠵᠢ</translation>
        <extra-contents_path>/Audio/Beep Switch</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="66"/>
        <source>Poweroff Music</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Audio/Poweroff Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="68"/>
        <source>Startup Music</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Audio/Startup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="70"/>
        <source>Wakeup Music</source>
        <translation>ᠳᠠᠭᠤᠳᠠᠨ ᠰᠡᠷᠡᠬᠡᠬᠦ</translation>
        <extra-contents_path>/Audio/Wakeup Music</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="74"/>
        <source>Notification Sound</source>
        <translation>ᠳᠠᠭᠤ ᠪᠠᠨ ᠮᠡᠳᠡᠭ᠍ᠳᠡ ᠃</translation>
        <extra-contents_path>/Audio/Notification Sound</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="78"/>
        <source>Volume Control Sound</source>
        <translation>ᠳᠠᠭᠤᠨ ᠤ ᠬᠡᠮᠵᠢᠶ ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠳᠠᠭᠤ ᠃</translation>
        <extra-contents_path>/Audio/Volume Control Sound</extra-contents_path>
    </message>
    <message>
        <source>Volume Change</source>
        <translation type="vanished">ᠳᠠᠭᠤᠨ ᠤ᠋ ᠬᠡᠮᠵᠢᠶ᠎ᠡ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Audio/Volume Change</extra-contents_path>
    </message>
    <message>
        <location filename="../ukmedia_sound_effects_widget.cpp" line="72"/>
        <source>Logout Music</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
        <extra-contents_path>/Audio/Logout Music</extra-contents_path>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="67"/>
        <location filename="../ukmedia_volume_control.cpp" line="92"/>
        <location filename="../ukmedia_volume_control.cpp" line="96"/>
        <location filename="../ukmedia_volume_control.cpp" line="111"/>
        <location filename="../ukmedia_volume_control.cpp" line="164"/>
        <location filename="../ukmedia_volume_control.cpp" line="239"/>
        <location filename="../ukmedia_volume_control.cpp" line="243"/>
        <location filename="../ukmedia_volume_control.cpp" line="255"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation>pa_context_set_sink_volume_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="133"/>
        <source>pa_context_set_source_mute_by_index() failed</source>
        <translation>pa_context_set_source_mute_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <source>pa_context_set_source_volume_by_index() failed</source>
        <translation>pa_context_set_source_volume_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="272"/>
        <location filename="../ukmedia_volume_control.cpp" line="276"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation>pa_context_set_source_output_volume() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="288"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation>pa_context_set_source_output_mute() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="300"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation>pa_context_set_card_profile_by_index() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="314"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation>pa_context_set_default_sink() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="327"/>
        <source>pa_context_set_default_source() failed</source>
        <translation>pa_context_set_default_source() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="343"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation>pa_context_set_sink_port_by_name() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="357"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation>pa_context_set_source_port_by_name() ᠵᠢ/ ᠢ᠋ ᠬᠦᠢᠴᠡᠬᠡᠬᠦ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="386"/>
        <source> (plugged in)</source>
        <translation> ( ᠬᠠᠪᠴᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠠᠢᠨ᠎ᠠ)</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="390"/>
        <location filename="../ukmedia_volume_control.cpp" line="581"/>
        <source> (unavailable)</source>
        <translation> ( ᠬᠡᠷᠡᠭᠯᠡᠵᠦ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ)</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="392"/>
        <location filename="../ukmedia_volume_control.cpp" line="578"/>
        <source> (unplugged)</source>
        <translation> ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="742"/>
        <source>Failed to read data from stream</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠤᠨᠭᠰᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="786"/>
        <source>Peak detect</source>
        <translation>ᠣᠷᠭᠢᠯ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠦ᠋ᠨ ᠪᠠᠢᠴᠠᠭᠠᠯᠲᠠ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="787"/>
        <source>Failed to create monitoring stream</source>
        <translation>ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠤᠷᠤᠰᠬᠠᠯ ᠢ᠋ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="801"/>
        <source>Failed to connect monitoring stream</source>
        <translation>ᠬᠢᠨᠠᠨ ᠬᠡᠮᠵᠢᠬᠦ ᠤᠷᠤᠰᠬᠠᠯ ᠢ᠋ ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="916"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation>ᠰᠠᠭᠤᠭᠳᠠᠭᠤᠯᠵᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠵᠢ ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠨ᠎ᠠ᠂ ᠤᠴᠢᠷ ᠨᠢ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠶᠠᠪᠤᠳᠠᠯ ᠵᠢᠨᠷ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ᠂ ᠡᠢᠮᠤ ᠡᠴᠡ ᠶᠠᠪᠤᠳᠠᠯ ᠤ᠋ᠨ ᠵᠢᠵᠢᠭ ᠰᠡᠯᠪᠢᠭ ᠵᠢᠡᠷ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1006"/>
        <source>pa_context_kill_source_output() failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1302"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation>PulseAudio ᠳ᠋ᠤ᠌/ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠵᠦ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠳᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ...</translation>
    </message>
</context>
</TS>
