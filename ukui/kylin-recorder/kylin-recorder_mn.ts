<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ClipWidget</name>
    <message>
        <location filename="../src/clipwidget.cpp" line="135"/>
        <source>Audition</source>
        <translation>ᠳᠤᠷᠠᠰᠢᠵᠤ ᠰᠤᠨᠤᠰᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="137"/>
        <source>Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="140"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="143"/>
        <source>Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="329"/>
        <source>Save New</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠹᠠᠢᠯ ᠪᠤᠯᠭᠠᠵᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="330"/>
        <source>Cover Current</source>
        <translation>ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="354"/>
        <source>Select a file storage directory</source>
        <translation>ᠨᠢᠭᠡ ᠹᠠᠢᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠭᠠᠷᠴᠠᠭ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and you must drag at least one slider to start this activity!</source>
        <translation type="vanished">剪辑时间不得少于1s,且您必须拖拽至少一个滑块作为此活动开始!</translation>
    </message>
    <message>
        <source>New File:</source>
        <translation type="vanished">新文件:</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and the initial start and end positions cannot be used as the start and end positions of the clip!</source>
        <translation type="vanished">剪辑时长不得少于1s,且初始的开始和结束位置不能作为剪辑的开始和结束位置!</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="414"/>
        <source>Hint</source>
        <translation>ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">消息</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="415"/>
        <source>This will overwrite the original file path,are you sure?</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠭ᠎ᠠ ᠤᠳᠤᠬᠠᠨ ᠤ᠋ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ ᠪᠤᠯᠤᠨ᠎ᠠ᠂ ᠲᠠ ᠢᠩᠬᠢᠵᠤ ᠬᠢᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="417"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and the duration of the clip cannot be the duration of the original file!</source>
        <translation type="vanished">剪辑时长不能小于1s,剪辑时长不能与原时长相同!</translation>
    </message>
    <message>
        <source>This storage path is illegal!</source>
        <translation type="vanished">存储路径非法!</translation>
    </message>
    <message>
        <source>The file name cannot exceed 20 characters!</source>
        <translation type="vanished">文件名不得超过20个字符!</translation>
    </message>
    <message>
        <source>Clip Finished!</source>
        <translation type="vanished">剪辑完成!</translation>
    </message>
</context>
<context>
    <name>FileItem</name>
    <message>
        <location filename="../src/fileitem.cpp" line="34"/>
        <source>Play</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="39"/>
        <source>Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="44"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="126"/>
        <location filename="../src/fileitem.cpp" line="381"/>
        <location filename="../src/fileitem.cpp" line="384"/>
        <location filename="../src/fileitem.cpp" line="390"/>
        <location filename="../src/fileitem.cpp" line="464"/>
        <location filename="../src/fileitem.cpp" line="528"/>
        <location filename="../src/fileitem.cpp" line="545"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="obsolete">最小化</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="49"/>
        <source>Clip</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="54"/>
        <source>Flag</source>
        <translation>ᠳᠡᠮᠳᠡᠭ ᠳᠠᠯᠪᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="381"/>
        <source>Time is too short</source>
        <translation>ᠴᠠᠭ ᠬᠡᠳᠦ ᠤᠬᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="391"/>
        <source>Unable to parse the waveform of audio file generated by non recorder！</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠭᠰᠡᠨ ᠪᠤᠰᠤ ᠹᠠᠢᠯ ᠵᠢᠡᠷ ᠪᠦᠷᠢᠯᠳᠦᠭᠰᠡᠨ ᠠᠦ᠋ᠳᠢᠤ᠋ ᠹᠠᠢᠯ ᠢ᠋ ᠵᠠᠳᠠᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ！</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="464"/>
        <source>Playing, please stop and delete!</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠤᠭᠰᠤᠭᠠᠭᠰᠠᠨ ᠤᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠠᠰᠤᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="495"/>
        <source>Save as</source>
        <translation>ᠦᠭᠡᠷ᠎ᠡ ᠭᠠᠵᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="496"/>
        <source>Open folder position</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠤᠷᠤᠨ ᠪᠠᠢᠷᠢ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="516"/>
        <source>Select a file storage directory</source>
        <translation>ᠨᠢᠭᠡ ᠹᠠᠢᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠭᠠᠷᠴᠠᠭ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="127"/>
        <location filename="../src/fileitem.cpp" line="385"/>
        <location filename="../src/fileitem.cpp" line="529"/>
        <location filename="../src/fileitem.cpp" line="546"/>
        <source>The file path does not exist or has been deleted!</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠠᠰᠤᠭᠳᠠᠪᠠ!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <location filename="../src/mainwindow.cpp" line="1470"/>
        <source>Recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="245"/>
        <source>Set</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="246"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="251"/>
        <source>Menu</source>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="262"/>
        <source>Mini</source>
        <translation>ᠵᠢᠵᠢᠭ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="271"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="280"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="288"/>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <location filename="../src/mainwindow.cpp" line="1503"/>
        <source>Recording</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="372"/>
        <source>Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="385"/>
        <source>Flag</source>
        <translation>ᠳᠡᠮᠳᠡᠭ ᠳᠠᠯᠪᠢᠬᠤ</translation>
    </message>
    <message>
        <source>Sign</source>
        <translation type="vanished">标记</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>Left</source>
        <translation>ᠡᠮᠦᠨᠡᠬᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="395"/>
        <source>Right</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="422"/>
        <source>File List</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="790"/>
        <source>None of the Recording File</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠳ᠋ᠤ᠌ ᠰᠢᠩᠬᠡᠬᠡᠭᠰᠡᠨ ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <location filename="../src/mainwindow.cpp" line="1576"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <source>No input device detected!</source>
        <translation>ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <source>Audio is playing, please stop and record again!</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋ ᠵᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠤᠭᠰᠤᠭᠠᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠰᠢᠩᠬᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <source>There is audio playing, please stop after recording!</source>
        <translation type="vanished">音频正在播放,请停止后录音!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1192"/>
        <source>pause</source>
        <translation>ᠲᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>start</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1396"/>
        <source>Tips From Recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ ᠵᠢᠨ ᠠᠩᠬᠠᠷᠤᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1576"/>
        <source>The file is not in the recording list,cannot be opened</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ ᠵᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠹᠠᠢᠯ ᠪᠢᠰᠢ᠂ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1588"/>
        <source>Using multichannel device</source>
        <translation>ᠤᠯᠠᠨ ᠳᠠᠭᠤᠨ ᠵᠠᠮ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1590"/>
        <source>Microphone in use</source>
        <translation>ᠮᠢᠺᠷᠤᠹᠤᠨ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1598"/>
        <source>kylin-recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>MiniWindow</name>
    <message>
        <location filename="../src/miniwindow.cpp" line="96"/>
        <source>Recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="100"/>
        <source>Recording</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="105"/>
        <source>Finish</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="112"/>
        <location filename="../src/miniwindow.cpp" line="237"/>
        <source>Pause</source>
        <translation>ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="124"/>
        <source>Restore</source>
        <translation>ᠰᠡᠷᠭᠦᠭᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="132"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="211"/>
        <location filename="../src/miniwindow.cpp" line="298"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="211"/>
        <location filename="../src/miniwindow.cpp" line="298"/>
        <source>Audio is playing, please stop and record again!</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋ ᠵᠢ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠵᠤᠭᠰᠤᠭᠠᠭᠠᠳ ᠳᠠᠬᠢᠵᠤ ᠰᠢᠩᠬᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <source>There is audio playing, please stop after recording!</source>
        <translation type="vanished">音频在播放,请停止后再录音!</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="242"/>
        <source>Start</source>
        <translation>ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>MyThread</name>
    <message>
        <location filename="../src/mythread.cpp" line="170"/>
        <location filename="../src/mythread.cpp" line="174"/>
        <location filename="../src/mythread.cpp" line="177"/>
        <location filename="../src/mythread.cpp" line="185"/>
        <location filename="../src/mythread.cpp" line="189"/>
        <location filename="../src/mythread.cpp" line="192"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="170"/>
        <location filename="../src/mythread.cpp" line="185"/>
        <source>.mp3</source>
        <translation>.mp3</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="174"/>
        <location filename="../src/mythread.cpp" line="189"/>
        <source>.wav</source>
        <translation>.WAV</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="177"/>
        <location filename="../src/mythread.cpp" line="192"/>
        <source>.m4a</source>
        <translation>.m4a</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="337"/>
        <location filename="../src/mythread.cpp" line="340"/>
        <location filename="../src/mythread.cpp" line="342"/>
        <source>Recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="577"/>
        <source>Select a file storage directory</source>
        <translation>ᠨᠢᠭᠡ ᠹᠠᠢᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠭᠠᠷᠴᠠᠭ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="581"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="581"/>
        <source>Do not enter illegal file name</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠪᠢᠳᠡᠬᠡᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/recorderdatabase.cpp" line="30"/>
        <location filename="../src/recorderdatabase.cpp" line="72"/>
        <location filename="../src/recorderdatabase.cpp" line="120"/>
        <location filename="../src/recorderdatabase.cpp" line="161"/>
        <location filename="../src/recorderdatabase.cpp" line="189"/>
        <location filename="../src/recorderdatabase.cpp" line="241"/>
        <location filename="../src/recorderdatabase.cpp" line="289"/>
        <source>Database Error</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠬᠦᠮᠦᠷᠬᠡ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>Save</name>
    <message>
        <location filename="../src/save.cpp" line="134"/>
        <location filename="../src/save.cpp" line="140"/>
        <location filename="../src/save.cpp" line="151"/>
        <source>Select a file storage directory</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../src/settings.cpp" line="49"/>
        <location filename="../src/settings.cpp" line="62"/>
        <source>Settings</source>
        <translation>ᠲᠣᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="77"/>
        <source>Alter</source>
        <translation>ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="99"/>
        <source>Storage:</source>
        <translation>ᠹᠠᠢᠯ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="108"/>
        <location filename="../src/settings.cpp" line="111"/>
        <location filename="../src/settings.cpp" line="227"/>
        <location filename="../src/settings.cpp" line="243"/>
        <location filename="../src/settings.cpp" line="247"/>
        <source>Recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="124"/>
        <source>Format:</source>
        <translation>ᠹᠤᠷᠲᠯᠠᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="129"/>
        <source>mp3</source>
        <translation>mp3</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="137"/>
        <source>Source:</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ ᠵᠢᠨ ᠢᠷᠡᠯᠳᠡ:</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="143"/>
        <source>Microphone</source>
        <translation>ᠮᠢᠺᠷᠤᠹᠤᠨ</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="144"/>
        <source>System Inside</source>
        <translation>ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠳᠤᠭᠠᠳᠤ</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="222"/>
        <source>Select a file storage directory</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="267"/>
        <location filename="../src/settings.cpp" line="279"/>
        <location filename="../src/settings.cpp" line="286"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="267"/>
        <location filename="../src/settings.cpp" line="286"/>
        <source>This storage path is illegal!</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="280"/>
        <source>The file name cannot exceed 20 characters!</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ 20 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>TipWindow</name>
    <message>
        <source>Transcoding...</source>
        <translation type="vanished">解析中...</translation>
    </message>
    <message>
        <location filename="../src/tipwindow.cpp" line="19"/>
        <source>Analysis...</source>
        <translation>ᠵᠠᠳᠠᠯᠬᠤ...</translation>
    </message>
</context>
<context>
    <name>Tools</name>
    <message>
        <location filename="../src/tools.cpp" line="21"/>
        <location filename="../src/tools.cpp" line="32"/>
        <location filename="../src/tools.cpp" line="39"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="21"/>
        <location filename="../src/tools.cpp" line="39"/>
        <source>This storage path is illegal!</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠠᠷᠭ᠎ᠠ ᠵᠠᠮ ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ!</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="33"/>
        <source>The file name cannot exceed 20 characters!</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ 20 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠡᠴᠡ ᠬᠡᠳᠦᠷᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="59"/>
        <source>Recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Setting</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="31"/>
        <source>Theme</source>
        <translation>ᠭᠣᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="28"/>
        <location filename="../src/menumodule.cpp" line="128"/>
        <source>Settings</source>
        <translation>ᠪᠠᠶᠢᠷᠢᠯᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="34"/>
        <location filename="../src/menumodule.cpp" line="126"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠮᠵᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="37"/>
        <location filename="../src/menumodule.cpp" line="124"/>
        <location filename="../src/menumodule.cpp" line="199"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="40"/>
        <location filename="../src/menumodule.cpp" line="122"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="262"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ: </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="265"/>
        <location filename="../src/menumodule.cpp" line="329"/>
        <location filename="../src/menumodule.cpp" line="344"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠡ ᠬᠢᠬᠡᠳ ᠳᠡᠮᠵᠢᠯᠭᠡ: </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="280"/>
        <source>The UI is friendly and easy to operate. It supports by microphone ,playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation>ᠵᠠᠭᠠᠭ ᠭᠠᠳᠠᠷᠭᠤ ᠵᠢᠨ ᠨᠠᠢᠷᠰᠠᠭ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠳᠦᠬᠦᠮ᠂ ᠮᠢᠺᠷᠤᠹᠤᠨ ᠵᠢᠡᠷ ᠰᠢᠩᠬᠡᠬᠡᠬᠦ ᠵᠢ ᠳᠡᠮᠵᠢᠨ᠎ᠡ᠃ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠳ᠋ᠤ᠌ ᠨᠡᠪᠳᠡᠷᠡᠬᠦᠯᠬᠦ ᠪᠤᠶᠤ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠵᠤ᠂ ᠮᠢ ᠨᠢ ᠪᠤᠯᠤᠨ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪᠳᠤ ᠵᠠᠭᠪᠤᠷ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠨ᠎ᠡ</translation>
    </message>
    <message>
        <source>The UI is friendly and easy to operate. It supports MP3 audio formats by microphone, playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation type="vanished">界面友好操作简单,支持麦克风录制MP3音频格式,支持文件列表中播放和删除,支持迷你和主题模式切换</translation>
    </message>
    <message>
        <source>The UI is friendly and easy to operate. It supports MP3 and WAV audio formats by microphone, playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation type="vanished">界面友好操作简单,支持麦克风录制MP3和WAV音频格式,支持文件列表中播放和删除,支持迷你模式和主题模式切换</translation>
    </message>
    <message>
        <location filename="../src/menumodule.h" line="43"/>
        <source>Recorder</source>
        <translation>ᠰᠢᠩᠬᠡᠬᠡᠯᠳᠡ</translation>
    </message>
</context>
</TS>
