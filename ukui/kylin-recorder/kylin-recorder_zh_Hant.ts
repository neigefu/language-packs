<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>ClipWidget</name>
    <message>
        <location filename="../src/clipwidget.cpp" line="135"/>
        <source>Audition</source>
        <translation>試演</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="137"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="140"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="143"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="329"/>
        <source>Save New</source>
        <translation>保存新品</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="330"/>
        <source>Cover Current</source>
        <translation>覆蓋電流</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="354"/>
        <source>Select a file storage directory</source>
        <translation>選擇檔案儲存目錄</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and you must drag at least one slider to start this activity!</source>
        <translation type="vanished">剪辑时间不得少于1s,且您必须拖拽至少一个滑块作为此活动开始!</translation>
    </message>
    <message>
        <source>New File:</source>
        <translation type="vanished">新文件:</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and the initial start and end positions cannot be used as the start and end positions of the clip!</source>
        <translation type="vanished">剪辑时长不得少于1s,且初始的开始和结束位置不能作为剪辑的开始和结束位置!</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="414"/>
        <source>Hint</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">消息</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="415"/>
        <source>This will overwrite the original file path,are you sure?</source>
        <translation>這將覆蓋原始檔案路徑，您確定嗎？</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="417"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and the duration of the clip cannot be the duration of the original file!</source>
        <translation type="vanished">剪辑时长不能小于1s,剪辑时长不能与原时长相同!</translation>
    </message>
    <message>
        <source>This storage path is illegal!</source>
        <translation type="vanished">存储路径非法!</translation>
    </message>
    <message>
        <source>The file name cannot exceed 20 characters!</source>
        <translation type="vanished">文件名不得超过20个字符!</translation>
    </message>
    <message>
        <source>Clip Finished!</source>
        <translation type="vanished">剪辑完成!</translation>
    </message>
</context>
<context>
    <name>FileItem</name>
    <message>
        <location filename="../src/fileitem.cpp" line="34"/>
        <source>Play</source>
        <translation>玩</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="39"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="44"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="126"/>
        <location filename="../src/fileitem.cpp" line="381"/>
        <location filename="../src/fileitem.cpp" line="384"/>
        <location filename="../src/fileitem.cpp" line="390"/>
        <location filename="../src/fileitem.cpp" line="464"/>
        <location filename="../src/fileitem.cpp" line="528"/>
        <location filename="../src/fileitem.cpp" line="545"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="obsolete">最小化</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="49"/>
        <source>Clip</source>
        <translation>夾</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="54"/>
        <source>Flag</source>
        <translation>旗</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="381"/>
        <source>Time is too short</source>
        <translation>時間太短</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="391"/>
        <source>Unable to parse the waveform of audio file generated by non recorder！</source>
        <translation>無法解析非錄音機生成的音訊檔的波形！</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="464"/>
        <source>Playing, please stop and delete!</source>
        <translation>正在播放，請停止並刪除！</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="495"/>
        <source>Save as</source>
        <translation>另存為</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="496"/>
        <source>Open folder position</source>
        <translation>打開資料夾位置</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="516"/>
        <source>Select a file storage directory</source>
        <translation>選擇檔案儲存目錄</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="127"/>
        <location filename="../src/fileitem.cpp" line="385"/>
        <location filename="../src/fileitem.cpp" line="529"/>
        <location filename="../src/fileitem.cpp" line="546"/>
        <source>The file path does not exist or has been deleted!</source>
        <translation>檔案路徑不存在或已被刪除！</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <location filename="../src/mainwindow.cpp" line="1470"/>
        <source>Recorder</source>
        <translation>錄音機</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="245"/>
        <source>Set</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="246"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="251"/>
        <source>Menu</source>
        <translation>功能表</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="262"/>
        <source>Mini</source>
        <translation>迷你</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="271"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="280"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="288"/>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <location filename="../src/mainwindow.cpp" line="1503"/>
        <source>Recording</source>
        <translation>錄音</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="372"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="385"/>
        <source>Flag</source>
        <translation>旗</translation>
    </message>
    <message>
        <source>Sign</source>
        <translation type="vanished">标记</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>Left</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="395"/>
        <source>Right</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="422"/>
        <source>File List</source>
        <translation>檔案清單</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="790"/>
        <source>None of the Recording File</source>
        <translation>沒有錄製檔</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <location filename="../src/mainwindow.cpp" line="1576"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <source>No input device detected!</source>
        <translation>未檢測到輸入設備！</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <source>Audio is playing, please stop and record again!</source>
        <translation>音訊正在播放，請停止並重新錄製！</translation>
    </message>
    <message>
        <source>There is audio playing, please stop after recording!</source>
        <translation type="vanished">音频正在播放,请停止后录音!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1192"/>
        <source>pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1396"/>
        <source>Tips From Recorder</source>
        <translation>來自記錄器的提示</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1576"/>
        <source>The file is not in the recording list,cannot be opened</source>
        <translation>檔案不在錄製清單中，無法打開</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1588"/>
        <source>Using multichannel device</source>
        <translation>使用多通道設備</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1590"/>
        <source>Microphone in use</source>
        <translation>麥克風正在使用中</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1598"/>
        <source>kylin-recorder</source>
        <translation>麒麟記錄儀</translation>
    </message>
</context>
<context>
    <name>MiniWindow</name>
    <message>
        <location filename="../src/miniwindow.cpp" line="96"/>
        <source>Recorder</source>
        <translation>錄音機</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="100"/>
        <source>Recording</source>
        <translation>錄音</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="105"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="112"/>
        <location filename="../src/miniwindow.cpp" line="237"/>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="124"/>
        <source>Restore</source>
        <translation>恢復</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="132"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="211"/>
        <location filename="../src/miniwindow.cpp" line="298"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="211"/>
        <location filename="../src/miniwindow.cpp" line="298"/>
        <source>Audio is playing, please stop and record again!</source>
        <translation>音訊正在播放，請停止並重新錄製！</translation>
    </message>
    <message>
        <source>There is audio playing, please stop after recording!</source>
        <translation type="vanished">音频在播放,请停止后再录音!</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="242"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
</context>
<context>
    <name>MyThread</name>
    <message>
        <location filename="../src/mythread.cpp" line="170"/>
        <location filename="../src/mythread.cpp" line="174"/>
        <location filename="../src/mythread.cpp" line="177"/>
        <location filename="../src/mythread.cpp" line="185"/>
        <location filename="../src/mythread.cpp" line="189"/>
        <location filename="../src/mythread.cpp" line="192"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="170"/>
        <location filename="../src/mythread.cpp" line="185"/>
        <source>.mp3</source>
        <translation>。。.mp3</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="174"/>
        <location filename="../src/mythread.cpp" line="189"/>
        <source>.wav</source>
        <translation>。。.wav</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="177"/>
        <location filename="../src/mythread.cpp" line="192"/>
        <source>.m4a</source>
        <translation>。。.m4a</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="337"/>
        <location filename="../src/mythread.cpp" line="340"/>
        <location filename="../src/mythread.cpp" line="342"/>
        <source>Recorder</source>
        <translation>錄音機</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="577"/>
        <source>Select a file storage directory</source>
        <translation>選擇檔案儲存目錄</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="581"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="581"/>
        <source>Do not enter illegal file name</source>
        <translation>不要輸入非法檔名</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/recorderdatabase.cpp" line="30"/>
        <location filename="../src/recorderdatabase.cpp" line="72"/>
        <location filename="../src/recorderdatabase.cpp" line="120"/>
        <location filename="../src/recorderdatabase.cpp" line="161"/>
        <location filename="../src/recorderdatabase.cpp" line="189"/>
        <location filename="../src/recorderdatabase.cpp" line="241"/>
        <location filename="../src/recorderdatabase.cpp" line="289"/>
        <source>Database Error</source>
        <translation>資料庫錯誤</translation>
    </message>
</context>
<context>
    <name>Save</name>
    <message>
        <location filename="../src/save.cpp" line="134"/>
        <location filename="../src/save.cpp" line="140"/>
        <location filename="../src/save.cpp" line="151"/>
        <source>Select a file storage directory</source>
        <translation>選擇檔案儲存目錄</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../src/settings.cpp" line="49"/>
        <location filename="../src/settings.cpp" line="62"/>
        <source>Settings</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="77"/>
        <source>Alter</source>
        <translation>改變</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="99"/>
        <source>Storage:</source>
        <translation>儲存：</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="108"/>
        <location filename="../src/settings.cpp" line="111"/>
        <location filename="../src/settings.cpp" line="227"/>
        <location filename="../src/settings.cpp" line="243"/>
        <location filename="../src/settings.cpp" line="247"/>
        <source>Recorder</source>
        <translation>錄音機</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="124"/>
        <source>Format:</source>
        <translation>格式：</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="129"/>
        <source>mp3</source>
        <translation>。.mp3</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="137"/>
        <source>Source:</source>
        <translation>源：</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="143"/>
        <source>Microphone</source>
        <translation>麥克風</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="144"/>
        <source>System Inside</source>
        <translation>系統內部</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="222"/>
        <source>Select a file storage directory</source>
        <translation>選擇檔案儲存目錄</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="267"/>
        <location filename="../src/settings.cpp" line="279"/>
        <location filename="../src/settings.cpp" line="286"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="267"/>
        <location filename="../src/settings.cpp" line="286"/>
        <source>This storage path is illegal!</source>
        <translation>此儲存路徑是非法的！</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="280"/>
        <source>The file name cannot exceed 20 characters!</source>
        <translation>檔名不能超過 20 個字元！</translation>
    </message>
</context>
<context>
    <name>TipWindow</name>
    <message>
        <source>Transcoding...</source>
        <translation type="vanished">解析中...</translation>
    </message>
    <message>
        <location filename="../src/tipwindow.cpp" line="19"/>
        <source>Analysis...</source>
        <translation>分析。。。</translation>
    </message>
</context>
<context>
    <name>Tools</name>
    <message>
        <location filename="../src/tools.cpp" line="21"/>
        <location filename="../src/tools.cpp" line="32"/>
        <location filename="../src/tools.cpp" line="39"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="21"/>
        <location filename="../src/tools.cpp" line="39"/>
        <source>This storage path is illegal!</source>
        <translation>此儲存路徑是非法的！</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="33"/>
        <source>The file name cannot exceed 20 characters!</source>
        <translation>檔名不能超過 20 個字元！</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="59"/>
        <source>Recorder</source>
        <translation>錄音機</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Setting</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="31"/>
        <source>Theme</source>
        <translation>主題</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="28"/>
        <location filename="../src/menumodule.cpp" line="128"/>
        <source>Settings</source>
        <translation>設置</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="34"/>
        <location filename="../src/menumodule.cpp" line="126"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="37"/>
        <location filename="../src/menumodule.cpp" line="124"/>
        <location filename="../src/menumodule.cpp" line="199"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="40"/>
        <location filename="../src/menumodule.cpp" line="122"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="262"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="265"/>
        <location filename="../src/menumodule.cpp" line="329"/>
        <location filename="../src/menumodule.cpp" line="344"/>
        <source>Service &amp; Support: </source>
        <translation>服務與支援： </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="280"/>
        <source>The UI is friendly and easy to operate. It supports by microphone ,playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation>用戶介面友好且易於操作。它支援通過麥克風，在檔案清單中播放和刪除，以及在迷你模式和主題模式之間切換</translation>
    </message>
    <message>
        <source>The UI is friendly and easy to operate. It supports MP3 audio formats by microphone, playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation type="vanished">界面友好操作简单,支持麦克风录制MP3音频格式,支持文件列表中播放和删除,支持迷你和主题模式切换</translation>
    </message>
    <message>
        <source>The UI is friendly and easy to operate. It supports MP3 and WAV audio formats by microphone, playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation type="vanished">界面友好操作简单,支持麦克风录制MP3和WAV音频格式,支持文件列表中播放和删除,支持迷你模式和主题模式切换</translation>
    </message>
    <message>
        <location filename="../src/menumodule.h" line="43"/>
        <source>Recorder</source>
        <translation>錄音機</translation>
    </message>
</context>
</TS>
