<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AddGameWidget</name>
    <message>
        <location filename="../addgamewidget.cpp" line="105"/>
        <source>App supporting game keys</source>
        <translation>支援遊戲金鑰的應用</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="71"/>
        <source>Reset</source>
        <translation>重置</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="74"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="92"/>
        <source>ASTC texture (When turned on, the picture quality is clearer)</source>
        <translation>ASTC紋理（開機時畫質更清晰）</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="96"/>
        <source>Enable ASTC texture support</source>
        <translation>啟用 ASTC 紋理支援</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="151"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="151"/>
        <location filename="../addgamewidget.cpp" line="154"/>
        <source>Add successfully, restart the Android application set to take effect!</source>
        <translation>添加成功，重啟安卓應用設置即可生效！</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="154"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="168"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="222"/>
        <source>Game name</source>
        <translation>遊戲名稱</translation>
    </message>
    <message>
        <location filename="../addgamewidget.cpp" line="222"/>
        <source>Game package name</source>
        <translation>遊戲包名稱</translation>
    </message>
</context>
<context>
    <name>AppMultiplierWidget</name>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="63"/>
        <location filename="../appmultiplierwidget.cpp" line="85"/>
        <source>App Multiplier</source>
        <translation>應用倍增器</translation>
    </message>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="45"/>
        <source>KMRE is not running!</source>
        <translation>KMRE 沒有運行！</translation>
    </message>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="52"/>
        <source>App Multiplier displays apps in dual windows. Not all apps are supported.</source>
        <translation>應用程式乘數在雙視窗中顯示應用程式。並非所有應用都受支援。</translation>
    </message>
    <message>
        <location filename="../appmultiplierwidget.cpp" line="58"/>
        <source>No App is installed which supporting App Multiplier.</source>
        <translation>未安裝支援應用程式乘數的應用程式。</translation>
    </message>
</context>
<context>
    <name>CameraWidget</name>
    <message>
        <location filename="../camerawidget.cpp" line="63"/>
        <source>Camera Devices</source>
        <translation>相機設備</translation>
    </message>
    <message>
        <location filename="../camerawidget.cpp" line="41"/>
        <source>Camera device:</source>
        <translation>攝像設備：</translation>
    </message>
    <message>
        <location filename="../camerawidget.cpp" line="125"/>
        <source>No camera detected</source>
        <translation>未檢測到攝像頭</translation>
    </message>
</context>
<context>
    <name>CleanerItem</name>
    <message>
        <location filename="../cleaneritem.cpp" line="104"/>
        <source>Delete the idle Android image?</source>
        <translation>刪除閑置的安卓鏡像？</translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="98"/>
        <location filename="../cleaneritem.cpp" line="104"/>
        <source>After the image is deleted, KMRE will no longer be able to switch to the image. Are you sure you want to perform this operation?</source>
        <translation>刪除鏡像后，KMRE 將無法再切換到鏡像。是否確實要執行此操作？</translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="98"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="118"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="119"/>
        <source>No</source>
        <translation>不</translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="213"/>
        <source>Currently configured</source>
        <translation>當前配置</translation>
    </message>
    <message>
        <location filename="../cleaneritem.cpp" line="213"/>
        <source>Idle</source>
        <translation>怠</translation>
    </message>
</context>
<context>
    <name>CleanerWidget</name>
    <message>
        <location filename="../cleanerwidget.cpp" line="104"/>
        <location filename="../cleanerwidget.cpp" line="107"/>
        <source>Image cleaning</source>
        <translation>圖像清理</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="64"/>
        <source>Name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="66"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="68"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="46"/>
        <source>Delete all idle images</source>
        <translation>刪除所有空閒圖像</translation>
    </message>
    <message>
        <source>Delete all idle Android images?</source>
        <translation type="vanished">删除闲置的所有Android镜像？</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="331"/>
        <location filename="../cleanerwidget.cpp" line="339"/>
        <source>After all images are deleted, KMRE will no longer be able to switch to the image. Are you sure you want to perform this operation?</source>
        <translation>刪除所有映射后，KMRE 將無法再切換到該映像。是否確實要執行此操作？</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="331"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="339"/>
        <source>Delete all idle images?</source>
        <translation>刪除所有空閒圖像？</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="110"/>
        <source>No idle image</source>
        <translation>無空閒映像</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="349"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../cleanerwidget.cpp" line="350"/>
        <source>No</source>
        <translation>不</translation>
    </message>
</context>
<context>
    <name>DeveloperWidget</name>
    <message>
        <location filename="../developerwidget.cpp" line="13"/>
        <source>Developer Mode</source>
        <translation>開發人員模式</translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="17"/>
        <source>open developer mode</source>
        <translation>開放開發人員模式</translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="25"/>
        <source>Please connect to the KMRE environment via adb connect %1 or open the help documentation if in doubt</source>
        <translation>請通過 adb connect %1 連接到 KMRE 環境，或者如有疑問，請打開幫助文檔</translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="25"/>
        <source>Please enter the following command on the terminal to install dependency:%1</source>
        <translation>請在終端上輸入以下命令以安裝依賴項：%1</translation>
    </message>
    <message>
        <location filename="../developerwidget.cpp" line="39"/>
        <source>KMRE is not running!</source>
        <translation>KMRE 沒有運行！</translation>
    </message>
</context>
<context>
    <name>DisplayModeWidget</name>
    <message>
        <location filename="../displaymodewidget.cpp" line="83"/>
        <source>Performance</source>
        <translation>性能</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="83"/>
        <source>Compatibility</source>
        <translation>相容性</translation>
    </message>
    <message>
        <source>(Supports AMD and Intel graphics cards)</source>
        <translation>（支援AMD和英特爾顯卡）</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="84"/>
        <source>(Supports all graphics cards)</source>
        <translation>（支援所有顯卡）</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="122"/>
        <source>The modification takes effect after you restart the system</source>
        <translation>修改在重新啟動系統後生效</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="90"/>
        <source>Display</source>
        <translation>顯示</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="124"/>
        <source>Display mode</source>
        <translation>顯示模式</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="99"/>
        <source>Restart system</source>
        <translation>重新啟動系統</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="84"/>
        <source>Only AMD and Intel graphics card are supported</source>
        <translation>僅支援 AMD 和英特爾顯卡</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="141"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="141"/>
        <location filename="../displaymodewidget.cpp" line="147"/>
        <source>Are you sure you want to restart system?</source>
        <translation>您確定要重新啟動系統嗎？</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="147"/>
        <source>Restart system?</source>
        <translation>重新啟動系統？</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="157"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../displaymodewidget.cpp" line="158"/>
        <source>No</source>
        <translation>不</translation>
    </message>
</context>
<context>
    <name>DockerIpWidget</name>
    <message>
        <location filename="../dockeripwidget.cpp" line="107"/>
        <source>Default IP address of docker morn</source>
        <translation>碼頭工人的預設IP位址</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="76"/>
        <source>OK</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="80"/>
        <source>Cancle</source>
        <translation>坎克爾</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="84"/>
        <source>Restart system</source>
        <translation>重新啟動系統</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="88"/>
        <source>IP</source>
        <translation>智慧財產權</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="89"/>
        <source>subnet mask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="133"/>
        <location filename="../dockeripwidget.cpp" line="168"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="110"/>
        <source>After the IP address is changed, you need to restart the system for the change to take effect</source>
        <translation>更改IP位址後，需要重新啟動系統才能使更改生效</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="133"/>
        <location filename="../dockeripwidget.cpp" line="145"/>
        <source>Please check the IP and mask carefully, otherwise the docker environment may not start normally. Are you sure you want to modify it?</source>
        <translation>請仔細檢查IP和掩碼，否則docker環境可能無法正常啟動。您確定要修改它嗎？</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="145"/>
        <source>Are you sure to modify the docker IP address?</source>
        <translation>您確定要修改碼頭工人 IP 位址嗎？</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="168"/>
        <location filename="../dockeripwidget.cpp" line="174"/>
        <source>Are you sure you want to restart system?</source>
        <translation>您確定要重新啟動系統嗎？</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="174"/>
        <source>Restart system?</source>
        <translation>重新啟動系統？</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="163"/>
        <location filename="../dockeripwidget.cpp" line="196"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../dockeripwidget.cpp" line="164"/>
        <location filename="../dockeripwidget.cpp" line="197"/>
        <source>No</source>
        <translation>不</translation>
    </message>
</context>
<context>
    <name>GameListItem</name>
    <message>
        <location filename="../gamelistitem.cpp" line="37"/>
        <location filename="../gamelistitem.cpp" line="99"/>
        <source>Clear</source>
        <translation>清楚</translation>
    </message>
    <message>
        <location filename="../gamelistitem.cpp" line="71"/>
        <location filename="../gamelistitem.cpp" line="78"/>
        <source>Clearing...</source>
        <translation>空地。。。</translation>
    </message>
    <message>
        <location filename="../gamelistitem.cpp" line="75"/>
        <source>Confirm clear</source>
        <translation>確認清除</translation>
    </message>
</context>
<context>
    <name>GameWidget</name>
    <message>
        <location filename="../gamewidget.cpp" line="51"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="87"/>
        <source>appName</source>
        <translation>應用名稱</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="88"/>
        <source>pkgName</source>
        <translation>pkg名稱</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="148"/>
        <source>Game Setting</source>
        <translation>遊戲設定</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="152"/>
        <source>Enable ASTC texture support(When turned on, the picture quality is clearer)</source>
        <translation>啟用ASTC紋理支援（開啟時，畫質更清晰）</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="154"/>
        <source>When added to the list, the app will enable the Game button feature:</source>
        <translation>新增到清單後，應用程式將啟用遊戲按鈕功能：</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="178"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="178"/>
        <location filename="../gamewidget.cpp" line="186"/>
        <source>You will remove %1 from the white list of game buttons</source>
        <translation>您將從遊戲按鈕白清單中移除 %1</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="178"/>
        <location filename="../gamewidget.cpp" line="186"/>
        <source>Are you sure you want to continue?</source>
        <translation>您確定要繼續嗎？</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="215"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../gamewidget.cpp" line="216"/>
        <source>No</source>
        <translation>不</translation>
    </message>
</context>
<context>
    <name>GeneralSettingWidget</name>
    <message>
        <location filename="../generalsettingwidget.cpp" line="12"/>
        <source>save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="17"/>
        <source>General Setting</source>
        <translation>常規設置</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="21"/>
        <source>Limit APP Number</source>
        <translation>限制應用程式數量</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="24"/>
        <source>Application Number</source>
        <translation>申請編號</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="34"/>
        <source>Limit the number of applications running</source>
        <translation>限制正在運行的應用程式數</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="41"/>
        <source>KMRE Auto Start</source>
        <translation>KMRE 自動啟動</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="49"/>
        <source>KMRE starts automatically upon startup</source>
        <translation>KMRE 啟動時自動啟動</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="52"/>
        <source>Translation Switch</source>
        <translation>翻譯開關</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="55"/>
        <source>default</source>
        <translation>違約</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="50"/>
        <source>Camera device:</source>
        <translation>攝像設備：</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="286"/>
        <source>No camera detected</source>
        <translation>未檢測到攝像頭</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="114"/>
        <source>Docker network</source>
        <translation>碼頭工人網路</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="116"/>
        <source>Network segment</source>
        <translation>網段</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="117"/>
        <source>Subnet mask</source>
        <translation>子網掩碼</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="120"/>
        <source>save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="141"/>
        <source>Shortcut</source>
        <translation>捷徑</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="144"/>
        <source>screenshot</source>
        <translation>截圖</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="163"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="167"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="173"/>
        <source>Log</source>
        <translation>日誌</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="93"/>
        <location filename="../generalsettingwidget.cpp" line="97"/>
        <location filename="../generalsettingwidget.cpp" line="100"/>
        <source>Modify successfully, close the settings set to take effect!</source>
        <translation>修改成功，關閉設置即可生效！</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="114"/>
        <location filename="../generalsettingwidget.cpp" line="127"/>
        <location filename="../generalsettingwidget.cpp" line="138"/>
        <source>Are you sure to cancel the limited on the number of application?</source>
        <translation>您確定取消申請人數限制嗎？</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="114"/>
        <location filename="../generalsettingwidget.cpp" line="127"/>
        <location filename="../generalsettingwidget.cpp" line="138"/>
        <source>Cancel the limited and open too many windows may cause the system to stall. Are you sure you want to cancel it?</source>
        <translation>取消限制並打開太多視窗可能會導致系統停止。您確定要取消它嗎？</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="170"/>
        <source>auto</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="171"/>
        <source>manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="729"/>
        <source>Please check the IP, mask carefully, otherwise the docker environment may not start normally. Are you sure you want to modify it?</source>
        <translation>請檢查IP，仔細遮罩，否則docker環境可能無法正常啟動。您確定要修改它嗎？</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="210"/>
        <source>Are you sure to modify the docker IP address?</source>
        <translation>您確定要修改碼頭工人 IP 位址嗎？</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="171"/>
        <location filename="../generalsettingwidget.cpp" line="175"/>
        <location filename="../generalsettingwidget.cpp" line="178"/>
        <location filename="../generalsettingwidget.cpp" line="195"/>
        <location filename="../generalsettingwidget.cpp" line="199"/>
        <location filename="../generalsettingwidget.cpp" line="202"/>
        <source>Modify successfully!</source>
        <translation>修改成功！</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="93"/>
        <location filename="../generalsettingwidget.cpp" line="97"/>
        <location filename="../generalsettingwidget.cpp" line="100"/>
        <location filename="../generalsettingwidget.cpp" line="171"/>
        <location filename="../generalsettingwidget.cpp" line="175"/>
        <location filename="../generalsettingwidget.cpp" line="178"/>
        <location filename="../generalsettingwidget.cpp" line="195"/>
        <location filename="../generalsettingwidget.cpp" line="199"/>
        <location filename="../generalsettingwidget.cpp" line="202"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="101"/>
        <location filename="../generalsettingwidget.cpp" line="139"/>
        <location filename="../generalsettingwidget.cpp" line="179"/>
        <location filename="../generalsettingwidget.cpp" line="203"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="140"/>
        <source>No</source>
        <translation>不</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="207"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="487"/>
        <source>Getting...</source>
        <translation>得到...</translation>
    </message>
    <message>
        <location filename="../generalsettingwidget.cpp" line="686"/>
        <source>failed</source>
        <translation>失敗</translation>
    </message>
</context>
<context>
    <name>GlesVersionWidget</name>
    <message>
        <location filename="../glesversionwidget.cpp" line="70"/>
        <source>(Compatibility)</source>
        <translation>（相容性）</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="74"/>
        <source>Autoselect</source>
        <translation>自動選擇</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="75"/>
        <source>OpenGL ES 2.0</source>
        <translation>OpenGL ES 2.0</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="76"/>
        <source>OpenGL ES 3.0</source>
        <translation>OpenGL ES 3.0</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="77"/>
        <source>OpenGL ES 3.1</source>
        <translation>OpenGL ES 3.1</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">默认</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="128"/>
        <source>OpenGL ES API level(requires restart)</source>
        <translation>OpenGL ES API level（需要重啟）</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="100"/>
        <source>Restart system</source>
        <translation>重新啟動系統</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="72"/>
        <source>(Renderer maximum)</source>
        <translation>（渲染器最大值）</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="126"/>
        <source>The modification takes effect after you restart the system</source>
        <translation>修改在重新啟動系統後生效</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="145"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="145"/>
        <location filename="../glesversionwidget.cpp" line="151"/>
        <source>Are you sure you want to restart system?</source>
        <translation>您確定要重新啟動系統嗎？</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="151"/>
        <source>Restart system?</source>
        <translation>重新啟動系統？</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="158"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../glesversionwidget.cpp" line="159"/>
        <source>No</source>
        <translation>不</translation>
    </message>
</context>
<context>
    <name>InputItem</name>
    <message>
        <location filename="../inputitem.cpp" line="34"/>
        <source>For example: the glory of the king</source>
        <translation>例如：王的榮耀</translation>
    </message>
    <message>
        <location filename="../inputitem.cpp" line="35"/>
        <source>For example: com.tencent.tmgp.sgame</source>
        <translation>例如：com.tencent.tmgp.sgame</translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是(Y)</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否(N)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../messagebox.cpp" line="180"/>
        <location filename="../messagebox.cpp" line="183"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="180"/>
        <location filename="../messagebox.cpp" line="183"/>
        <source>Modify successfully!The modification takes effect only after the service is restarted. Do you want to restart the service?</source>
        <translation>修改成功！修改僅在服務重啟後生效。是否要重新啟動服務？</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="184"/>
        <source>restart now</source>
        <translation>立即重啟</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="185"/>
        <source>restart later</source>
        <translation>稍後重新啟動</translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox::MessageBox</name>
    <message>
        <location filename="../messagebox.cpp" line="103"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="108"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
</context>
<context>
    <name>LogWidget</name>
    <message>
        <location filename="../logwidget.cpp" line="127"/>
        <source>Log collection</source>
        <translation>日誌收集</translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="80"/>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="88"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="165"/>
        <source>Getting logs...</source>
        <translation>取得紀錄...</translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="350"/>
        <source>/sbin/iptables command does not exist!</source>
        <translation>/sbin/iptables 命令不存在！</translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="390"/>
        <source>An error occurred while getting log!</source>
        <translation>獲取日誌時出錯！</translation>
    </message>
    <message>
        <location filename="../logwidget.cpp" line="402"/>
        <source>Get log complete.</source>
        <translation>完成日誌。</translation>
    </message>
</context>
<context>
    <name>NetMaskItem</name>
    <message>
        <location filename="../netmaskitem.cpp" line="53"/>
        <source>Set the subnet mask of container docker</source>
        <translation>設置容器碼頭管理員的子網掩碼</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="97"/>
        <source>kylin-kmre-settings is already running!</source>
        <translation>麒麟-kmre-settings已經在運行了！</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="101"/>
        <source>kylin-kmre-settings</source>
        <translation>麒麟-公里設置</translation>
    </message>
</context>
<context>
    <name>PhoneInfoWidget</name>
    <message>
        <location filename="../phoneinfowidget.cpp" line="32"/>
        <source>Phone model</source>
        <translation>手機型號</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="33"/>
        <source>IMEI setting</source>
        <translation>IMEI設置</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="35"/>
        <source>preset model</source>
        <translation>預設模型</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="36"/>
        <source>custom</source>
        <translation>習慣</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="38"/>
        <source>vendor</source>
        <translation>供應商</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="39"/>
        <source>brand</source>
        <translation>品牌</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="40"/>
        <source>name</source>
        <translation>名字</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="41"/>
        <source>model</source>
        <translation>型</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="42"/>
        <source>equip</source>
        <translation>裝備</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="57"/>
        <source>random</source>
        <translation>隨機</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="60"/>
        <source>default</source>
        <translation>違約</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="56"/>
        <source>save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="64"/>
        <source>PhoneInfo Setting</source>
        <translation>電話信息設置</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="221"/>
        <source>The Settings take effect after the environment is restarted</source>
        <translation>設置在環境重新啟動後生效</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="216"/>
        <location filename="../phoneinfowidget.cpp" line="220"/>
        <location filename="../phoneinfowidget.cpp" line="223"/>
        <source>Modify successfully!Restart the environment to take effect.</source>
        <translation>修改成功！重啟環境以生效。</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="216"/>
        <location filename="../phoneinfowidget.cpp" line="220"/>
        <location filename="../phoneinfowidget.cpp" line="223"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../phoneinfowidget.cpp" line="224"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
</context>
<context>
    <name>RadioButtonItem</name>
    <message>
        <location filename="../radiobuttonitem.cpp" line="60"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="106"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="110"/>
        <location filename="../radiobuttonitem.cpp" line="113"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="106"/>
        <location filename="../radiobuttonitem.cpp" line="110"/>
        <location filename="../radiobuttonitem.cpp" line="113"/>
        <source>The modification takes effect after you restart the system</source>
        <translation>修改在重新啟動系統後生效</translation>
    </message>
    <message>
        <location filename="../radiobuttonitem.cpp" line="114"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
</context>
<context>
    <name>RemoveGameWidget</name>
    <message>
        <location filename="../removegamewidget.cpp" line="34"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../removegamewidget.cpp" line="51"/>
        <source>appName</source>
        <translation>應用名稱</translation>
    </message>
    <message>
        <location filename="../removegamewidget.cpp" line="52"/>
        <source>pkgName</source>
        <translation>pkg名稱</translation>
    </message>
</context>
<context>
    <name>SettingsFrame</name>
    <message>
        <location filename="../settingsframe.cpp" line="110"/>
        <location filename="../settingsframe.cpp" line="140"/>
        <source>KMRE-Preference</source>
        <translation>公里偏好</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="149"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="158"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="457"/>
        <source>Display</source>
        <translation>顯示</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="461"/>
        <source>Renderer</source>
        <translation>渲染</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="465"/>
        <source>Game Setting</source>
        <translation>遊戲設定</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="469"/>
        <source>Network</source>
        <translation>網路</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="473"/>
        <location filename="../settingsframe.cpp" line="512"/>
        <location filename="../settingsframe.cpp" line="527"/>
        <source>Camera</source>
        <translation>照相機</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="479"/>
        <source>Images</source>
        <translation>圖像</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="483"/>
        <source>Log</source>
        <translation>日誌</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="487"/>
        <location filename="../settingsframe.cpp" line="532"/>
        <source>AppMultiplier</source>
        <translation>應用乘數</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="493"/>
        <location filename="../settingsframe.cpp" line="537"/>
        <source>Tray</source>
        <translation>托盤</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="493"/>
        <location filename="../settingsframe.cpp" line="537"/>
        <source>PhoneInfo Setting</source>
        <translation>電話信息設置</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="310"/>
        <location filename="../settingsframe.cpp" line="313"/>
        <location filename="../settingsframe.cpp" line="318"/>
        <location filename="../settingsframe.cpp" line="321"/>
        <location filename="../settingsframe.cpp" line="327"/>
        <location filename="../settingsframe.cpp" line="330"/>
        <location filename="../settingsframe.cpp" line="366"/>
        <location filename="../settingsframe.cpp" line="369"/>
        <location filename="../settingsframe.cpp" line="374"/>
        <location filename="../settingsframe.cpp" line="377"/>
        <location filename="../settingsframe.cpp" line="383"/>
        <location filename="../settingsframe.cpp" line="386"/>
        <location filename="../settingsframe.cpp" line="493"/>
        <location filename="../settingsframe.cpp" line="537"/>
        <source>General Setting</source>
        <translation>常規設置</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="475"/>
        <source>Developer Mode</source>
        <translation>開發人員模式</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="535"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="535"/>
        <source>Current manager vesion does not support developer mode.</source>
        <translation>當前管理員不支援開發人員模式。</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="748"/>
        <source>Error</source>
        <translation>錯誤</translation>
    </message>
    <message>
        <location filename="../settingsframe.cpp" line="748"/>
        <source>Restarting the environment failed!</source>
        <translation>重新啟動環境失敗！</translation>
    </message>
</context>
<context>
    <name>TrayAppWidget</name>
    <message>
        <location filename="../trayappwidget.cpp" line="31"/>
        <source>Tray</source>
        <translation>托盤</translation>
    </message>
    <message>
        <location filename="../trayappwidget.cpp" line="33"/>
        <source>Set the application displayed in the tray area</source>
        <translation>設置托盤區域中顯示的應用程式</translation>
    </message>
    <message>
        <location filename="../trayappwidget.cpp" line="41"/>
        <source>No items!</source>
        <translation>沒有物品！</translation>
    </message>
    <message>
        <location filename="../trayappwidget.cpp" line="74"/>
        <source>KMRE is not running!</source>
        <translation>KMRE 沒有運行！</translation>
    </message>
</context>
</TS>
