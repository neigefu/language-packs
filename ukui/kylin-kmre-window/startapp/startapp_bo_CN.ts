<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Dialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">窗口</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Launch window of kmre</source>
        <translation>ཆི་ལིན་སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་སྒོ་འབྱེད་སྒེའུ་ཁུང་།</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="46"/>
        <source>Preparing Kmre Env</source>
        <translation>སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་ལ་གྲ་སྒྲིག་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="76"/>
        <source>Init Kmre Env</source>
        <translation>ཐོག་མའི་སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="143"/>
        <source>The kernel does not support Kmre</source>
        <translation>ནང་སྙིང་གིས་ཆི་ལིན་སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Virtual machine is not supported in Kmre</source>
        <translation>སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་གིས་གནས་སྐབས་སུ་རྟོག་བཟོའི་འཕྲུལ་ཆས་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="149"/>
        <source>Kmre is corrupted, please check if other Android compatible env are installed</source>
        <translation>སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་ལ་གཏོར་བརླག་ཐེབས་པས་ཨན་ཀྲོའོ་མཉམ་འདུས་ཁོར་ཡུག་གཞན་པ་སྒྲིག་སྦྱོར་བྱས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="152"/>
        <source>Please do not use root</source>
        <translation>root ་སྤྱོད་མཁན་བེད་སྤྱོད་མ་བྱེད་རོགས་།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="155"/>
        <source>The current graphics card is not supported in Kmre</source>
        <translation>སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་གིས་གནས་སྐབས་སུ་མིག་སྔའི་བྱང་བུ་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Kmre only supports Kylin OS for the time being</source>
        <translation>སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་གིས་གནས་སྐབས་སུ་ཆི་ལིན་བཀོལ་སྤྱོད་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱེད།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="161"/>
        <source>Kmre does not support the current CPU temporarily</source>
        <translation>CPUསྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་གིས་གནས་སྐབས་སུ་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="164"/>
        <source>The current graphics card cannot support more apps</source>
        <translation>མིག་སྔར་མངོན་གསལ་བྱང་བུས་སྤོ་འགུལ་བཀོལ་སྤྱོད་མང་པོ་བྱེད་པར་རྒྱབ་སྐྱོར་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="167"/>
        <source>The current CPU cannot support running more apps</source>
        <translation>མིག་སྔར་CPUདེ་ལས་མང་བའི་སྤོ་འགུལ་བཀོལ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="170"/>
        <source>The current hardware environment cannot support running more apps</source>
        <translation>མིག་སྔར་མཁྲེགས་ཆས་ཁོར་ཡུག་གིས་སྔར་ལས་མང་བའི་སྤོ་འགུལ་བཀོལ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="173"/>
        <source>Exception of dbus service interface to be accessed</source>
        <translation>dbusབཅར་འདྲི་བྱེད་དགོས་པའི་ཞབས་ཞུའི་མཐུད་སྣེ་རྒྱུན་ལྡན་མ་རེད།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="176"/>
        <source>For better experience, please close an android app and then open it</source>
        <translation>སྔར་ལས་བཟང་བའི་ཉམས་ལེན་གྱི་ཕན་འབྲས་ཀྱི་ཆེད་དུ།་ཨན་ཀྲོའོ་བཀོལ་སྤྱོད་ཞིག་གི་སྒོ་བརྒྱབ་རྗེས་ད་གཟོད་སྒོ་འབྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="179"/>
        <source>The current startup item does not support Kmre, please use default startup item</source>
        <translation>མིག་སྔར་འགུལ་སྐྱོད་རྣམ་གྲངས་ཀྱིས་སྤོ་འགུལ་འཁོར་སྐྱོད་ཁོར་ཡུག་ལ་རྒྱབ་སྐྱོར་མི་བྱེད་པས་འགུལ་སྐྱོད་རྣམ་གྲངས་བཀོལ་སྤྱོད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="363"/>
        <source>Service startup failed: %1</source>
        <translation>ཞབས་ཞུ་མགོ་བརྩམས་པ་ཕམ་སོང་། %1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="369"/>
        <source>No service found: %1</source>
        <translation>ཞབས་ཞུ་མ་རྙེད་པ། %1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="375"/>
        <source>Image configuration not found</source>
        <translation>མ་རྙེད་པའི་པར་རིས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="381"/>
        <source>Image not found</source>
        <translation>པར་རིས་ཡིག་ཆ་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="387"/>
        <source>Image loading failed</source>
        <translation>པར་རིས་ནང་འཇུག་བྱེད་པར་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="393"/>
        <source>Image mismatch</source>
        <translation>པར་རིས་ཆ་འགྲིག་མིན་པ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="634"/>
        <location filename="../main.cpp" line="662"/>
        <source>Tips</source>
        <translation>གསལ་འདེབས།</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="638"/>
        <source>The %1 package is not installed;</source>
        <translation>%1 ཐུམ་སྒྲིལ་སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="640"/>
        <source>After installing the dependency package, re-install the kylin-kmre-modules-dkms package to meet the environment required by KMRE, then restart your computer and try to experience KMRE;</source>
        <translation>གཞན་རྟེན་རང་བཞིན་གྱི་ཐུམ་སྒྲིལ་སྒྲིག་སྦྱོར་བྱས་རྗེས་ kylin-kmre-modules-dkms packages ཡང་བསྐྱར་སྒྲིག་སྦྱོར་བྱས་ཏེ་KMREལ་མཁོ་བའི་ཁོར་ཡུག་དང་མཐུན་པར་བྱ་དགོས་པ་དང་། དེ་ནས་ཁྱེད་ཀྱི་རྩིས་འཁོར་བསྐྱར་དུ་འགོ་བརྩམས་ཏེ་KMREཉམ་ལེན་བྱེད།</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="666"/>
        <source>binder not found, please confirm the environment required by KMRE.</source>
        <translation>binder་མ་རྙེད་པས་ལ་kmreམཁོ་བའི་ཁོར་ཡུག་ཆ་ཚང་ཡིན་མིན་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
</context>
</TS>
