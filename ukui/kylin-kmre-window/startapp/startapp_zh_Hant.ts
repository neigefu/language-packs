<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>Dialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">窗口</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Launch window of kmre</source>
        <translation>公里的啟動視窗</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="46"/>
        <source>Preparing Kmre Env</source>
        <translation>準備 Kmre Env</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="76"/>
        <source>Init Kmre Env</source>
        <translation>Init Kmre Env</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="143"/>
        <source>The kernel does not support Kmre</source>
        <translation>內核不支援 Kmre</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Virtual machine is not supported in Kmre</source>
        <translation>虛擬機在 Kmre 中不受支援</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="149"/>
        <source>Kmre is corrupted, please check if other Android compatible env are installed</source>
        <translation>Kmre 已損壞，請檢查是否安裝了其他與 Android 相容的環境</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="152"/>
        <source>Please do not use root</source>
        <translation>請不要使用根</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="155"/>
        <source>The current graphics card is not supported in Kmre</source>
        <translation>當前顯卡在 Kmre 中不受支援</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Kmre only supports Kylin OS for the time being</source>
        <translation>Kmre 暫時只支援麒麟操作系統</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="161"/>
        <source>Kmre does not support the current CPU temporarily</source>
        <translation>Kmre 暫時不支援當前的 CPU</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="164"/>
        <source>The current graphics card cannot support more apps</source>
        <translation>當前顯卡無法支援更多應用</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="167"/>
        <source>The current CPU cannot support running more apps</source>
        <translation>當前 CPU 無法支援運行更多應用</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="170"/>
        <source>The current hardware environment cannot support running more apps</source>
        <translation>當前硬體環境不支援運行更多應用</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="173"/>
        <source>Exception of dbus service interface to be accessed</source>
        <translation>要訪問的 dbus 服務介面異常</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="176"/>
        <source>For better experience, please close an android app and then open it</source>
        <translation>為了獲得更好的體驗，請關閉安卓應用程式，然後打開它</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="179"/>
        <source>The current startup item does not support Kmre, please use default startup item</source>
        <translation>當前啟動項不支援 Kmre，請使用預設啟動項</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="363"/>
        <source>Service startup failed: %1</source>
        <translation>服務啟動失敗： %1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="369"/>
        <source>No service found: %1</source>
        <translation>找不到服務： %1</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="375"/>
        <source>Image configuration not found</source>
        <translation>未找到映像配置</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="381"/>
        <source>Image not found</source>
        <translation>未找到圖像</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="387"/>
        <source>Image loading failed</source>
        <translation>圖像載入失敗</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="393"/>
        <source>Image mismatch</source>
        <translation>圖像不匹配</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="634"/>
        <location filename="../main.cpp" line="662"/>
        <source>Tips</source>
        <translation>技巧</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="638"/>
        <source>The %1 package is not installed;</source>
        <translation>未安裝 %1 套件;</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="640"/>
        <source>After installing the dependency package, re-install the kylin-kmre-modules-dkms package to meet the environment required by KMRE, then restart your computer and try to experience KMRE;</source>
        <translation>安裝好依賴包后，重新安裝麒麟-kmre-modules-dkms 包以滿足 KMRE 要求的環境，然後重啟電腦，嘗試體驗 KMRE;</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="666"/>
        <source>binder not found, please confirm the environment required by KMRE.</source>
        <translation>未找到活頁夾，請確認KMRE要求的環境。</translation>
    </message>
</context>
</TS>
