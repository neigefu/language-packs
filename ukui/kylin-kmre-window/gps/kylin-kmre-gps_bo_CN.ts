<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>GPSWINDOW</name>
    <message>
        <location filename="../gpswindow.cpp" line="15"/>
        <source>virtualgps</source>
        <translation>རྟོག་བཟོ་བྱས་པའི་དངོས་པོ།</translation>
    </message>
    <message>
        <location filename="../gpswindow.cpp" line="68"/>
        <location filename="../gpswindow.cpp" line="114"/>
        <source>uselocation</source>
        <translation>བཀོལ་སྤྱོད་གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="../gpswindow.cpp" line="103"/>
        <source>（经纬度：</source>
        <translation>（经纬度：</translation>
    </message>
    <message>
        <location filename="../gpswindow.cpp" line="116"/>
        <source>Unable to get the current location of the user</source>
        <translation>སྤྱོད་མཁན་གྱི་ད་ལྟའི་གནས་ཡུལ་ཐོབ་མི་ཐུབ་</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="41"/>
        <source>kylin-kmre-gps is already running!</source>
        <translation>ཅིན་ལིན་སྤྱི་ལེ་གཅིག་གི་gpsའཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>kmre_gps::CurrentLocationView</name>
    <message>
        <location filename="../currentlocationview.cpp" line="21"/>
        <source>currentlocation:
</source>
        <translation>གློག་རྒྱུན་གནས་གཏན་འཁེལ་བྱ་རྒྱུ་སྟེ།
</translation>
    </message>
    <message>
        <location filename="../currentlocationview.cpp" line="31"/>
        <location filename="../currentlocationview.cpp" line="47"/>
        <source>cancellocation</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../currentlocationview.cpp" line="42"/>
        <source>uselocation</source>
        <translation>བཀོལ་སྤྱོད་གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="../currentlocationview.cpp" line="77"/>
        <source>（经纬度：</source>
        <translation>（经纬度：</translation>
    </message>
</context>
<context>
    <name>kmre_gps::SearchBox</name>
    <message>
        <location filename="../searchbox.cpp" line="20"/>
        <source>entertext</source>
        <translation>ནང་དོན་དངོས་ལ་ལྟ་ཞིབ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>kmre_gps::SearchView</name>
    <message>
        <location filename="../searchview.cpp" line="31"/>
        <source>current-location</source>
        <translation>མིག་སྔའི་གནས་ཡུལ།</translation>
    </message>
</context>
<context>
    <name>location::CurrentLocationView</name>
    <message>
        <source>currentlocation:
</source>
        <translation type="obsolete">当前位置：
</translation>
    </message>
    <message>
        <source>cancellocation</source>
        <translation type="obsolete">取消定位</translation>
    </message>
    <message>
        <source>Beijing(defaultlocation)</source>
        <translation type="obsolete">北京市（默认位置）</translation>
    </message>
    <message>
        <source>uselocation</source>
        <translation type="obsolete">定位到此</translation>
    </message>
    <message>
        <source>(Longitude and latitude:</source>
        <translation type="vanished">（经纬度：</translation>
    </message>
</context>
<context>
    <name>location::LocationListWidget</name>
    <message>
        <source>(Longitude and latitude:</source>
        <translation type="vanished">（经纬度：</translation>
    </message>
</context>
<context>
    <name>location::SearchBox</name>
    <message>
        <source>entertext</source>
        <translation type="obsolete">请输入你想找的内容</translation>
    </message>
</context>
</TS>
