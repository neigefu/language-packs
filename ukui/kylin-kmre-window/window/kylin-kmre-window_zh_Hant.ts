<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>About</name>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="90"/>
        <location filename="../about.cpp" line="114"/>
        <source>KMRE</source>
        <translation>凱姆雷</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="99"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="121"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../about.cpp" line="128"/>
        <source>KMRE is a Android App compatible system environment created by Kylin team for Kylin OS, which is used to meet the diversified needs of users for application software. KMRE enables users to install and run Android apps in Kylin OS, such as games, wechat, QQ, stock, video apps, etc.</source>
        <translation>KMRE是麒麟團隊針對麒麟操作系統打造的Android應用相容系統環境，用於滿足使用者對應用軟體的多樣化需求。KMRE使用戶能夠在麒麟操作系統中安裝和運行Android應用程式，例如遊戲，微信，QQ，股票，視頻應用程式等。</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="135"/>
        <source>Service and Support:</source>
        <translation>服務與支援：</translation>
    </message>
    <message>
        <source>About us</source>
        <translation type="vanished">关于我们</translation>
    </message>
    <message>
        <source>Kmre is a Android App compatible system environment created by Kylin team for Kylin OS, which is used to meet the diversified needs of users for application software. Kmre enables users to install and run Android apps in Kylin OS, such as games, wechat, QQ, stock, video apps, etc.</source>
        <translation type="vanished">麒麟移动运行环境（KMRE）是麒麟团队专为“银河麒麟操作系统”打造的一款兼容安卓App运行的系统环境，用于满足用户对应用软件的多样化需求。KMRE 能够让用户在银河麒麟操作系统中安装和运行安卓App，比如游戏、微信、QQ、股票、视频类App等。</translation>
    </message>
    <message>
        <source>Copyright information</source>
        <translation type="vanished">版权信息</translation>
    </message>
    <message>
        <source>Copyright 2020-2021. kylinos.cn. All Rights Reserved.</source>
        <translation type="vanished">版权所有 麒麟软件有限公司 Copyright 2020-2021. kylinos.cn. All Rights Reserved.</translation>
    </message>
    <message>
        <source>Service &amp; Support Team</source>
        <translation type="vanished">服务与支持团队</translation>
    </message>
</context>
<context>
    <name>AddGameWidget</name>
    <message>
        <source>App supporting game keys</source>
        <translation type="vanished">应用支持游戏按键</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation type="vanished">重置</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>Add successfully, restart the Android application set to take effect!</source>
        <translation type="vanished">添加成功，重启被设置的Android应用后生效！</translation>
    </message>
    <message>
        <source>Game name</source>
        <translation type="vanished">游戏名称</translation>
    </message>
    <message>
        <source>Game package name</source>
        <translation type="vanished">游戏包名</translation>
    </message>
</context>
<context>
    <name>AutohideWidget</name>
    <message>
        <location filename="../displaymanager/autohidewidget.cpp" line="91"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../displaymanager/autohidewidget.cpp" line="99"/>
        <source>Game Key</source>
        <translation>遊戲金鑰</translation>
    </message>
    <message>
        <location filename="../displaymanager/autohidewidget.cpp" line="107"/>
        <source>UnFullscreen</source>
        <translation>取消全屏</translation>
    </message>
    <message>
        <location filename="../displaymanager/autohidewidget.cpp" line="123"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>CameraWidget</name>
    <message>
        <source>Camera Devices</source>
        <translation type="vanished">摄像头设备</translation>
    </message>
    <message>
        <source>Camera device:</source>
        <translation type="vanished">默认摄像头：</translation>
    </message>
</context>
<context>
    <name>CleanerItem</name>
    <message>
        <source>Delete the idle Android image?</source>
        <translation type="vanished">删除该闲置的Android镜像？</translation>
    </message>
    <message>
        <source>After the image is deleted, Kmre will no longer be able to switch to the image. Are you sure you want to perform this operation?</source>
        <translation type="vanished">镜像被删除后，麒麟移动运行环境将无法再切换到该镜像，你确定要执行该操作吗？</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <source>Currently configured</source>
        <translation type="vanished">当前配置的</translation>
    </message>
    <message>
        <source>Idle</source>
        <translation type="vanished">闲置的</translation>
    </message>
</context>
<context>
    <name>CleanerWidget</name>
    <message>
        <source>Image cleaning</source>
        <translation type="vanished">镜像清理</translation>
    </message>
    <message>
        <source>Delete all idle Android images</source>
        <translation type="vanished">删除闲置的所有Android镜像</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <source>Delete all idle Android images?</source>
        <translation type="vanished">删除闲置的所有Android镜像？</translation>
    </message>
    <message>
        <source>After all images are deleted, Kmre will no longer be able to switch to the image. Are you sure you want to perform this operation?</source>
        <translation type="vanished">删除闲置的所有镜像后，麒麟移动运行环境将无法再切换到被删除的镜像，你确定要执行该操作吗？</translation>
    </message>
    <message>
        <source>No idle image</source>
        <translation type="vanished">无闲置镜像</translation>
    </message>
</context>
<context>
    <name>DisplayManager</name>
    <message>
        <location filename="../displaymanager/displaymanager.cpp" line="488"/>
        <source>Screen rotation, please reset button！</source>
        <translation>螢幕旋轉，請重置按鈕！</translation>
    </message>
</context>
<context>
    <name>DisplayModeWidget</name>
    <message>
        <source>Performance</source>
        <translation type="vanished">性能模式</translation>
    </message>
    <message>
        <source>Compatibility</source>
        <translation type="vanished">兼容模式</translation>
    </message>
    <message>
        <source>Only AMD graphics card on aarch64 is supported</source>
        <translation type="vanished">该模式仅支持在ARM64上使用AMD显卡时使用</translation>
    </message>
    <message>
        <source>Supports all graphics cards</source>
        <translation type="vanished">支持所有显卡</translation>
    </message>
    <message>
        <source>Display mode(requires restart)</source>
        <translation type="vanished">显示模式（需要重启）</translation>
    </message>
    <message>
        <source>Restart system</source>
        <translation type="vanished">重启系统</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <source>Restart system?</source>
        <translation type="vanished">重启系统？</translation>
    </message>
    <message>
        <source>Are you sure you want to restart system?</source>
        <translation type="vanished">你确定要重启系统吗？</translation>
    </message>
</context>
<context>
    <name>DockerIpWidget</name>
    <message>
        <source>Default IP address of docker(requires restart)</source>
        <translation type="vanished">Docker默认IP地址（需要重启）</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Restart system</source>
        <translation type="vanished">重启系统</translation>
    </message>
    <message>
        <source>Please enter IP:</source>
        <translation type="vanished">请输入IP：</translation>
    </message>
    <message>
        <source>Please enter subnet mask:</source>
        <translation type="vanished">请输入子网掩码：</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <source>Are you sure to modify the docker IP address?</source>
        <translation type="vanished">确认修改Docker的IP地址？</translation>
    </message>
    <message>
        <source>Please check the IP and mask carefully, otherwise the docker environment may not start normally. Are you sure you want to modify it?</source>
        <translation type="vanished">请仔细检查IP和掩码，否则可能造成Docker环境无法正常启动，你确定要修改吗？</translation>
    </message>
    <message>
        <source>Restart system?</source>
        <translation type="vanished">重启系统？</translation>
    </message>
    <message>
        <source>Are you sure you want to restart system?</source>
        <translation type="vanished">你确定要重启系统吗？</translation>
    </message>
</context>
<context>
    <name>GameKeyManager</name>
    <message>
        <location filename="../gamekey/gamekeymanager.cpp" line="710"/>
        <source>Reset Game Key?</source>
        <translation>重置遊戲金鑰？</translation>
    </message>
    <message>
        <location filename="../gamekey/gamekeymanager.cpp" line="710"/>
        <source>Window rotationed! Do you want to clear current game key settings and continue ?</source>
        <translation>視窗旋轉！是否要清除目前的遊戲金鑰設定並繼續？</translation>
    </message>
    <message>
        <location filename="../gamekey/gamekeymanager.cpp" line="487"/>
        <location filename="../gamekey/gamekeymanager.cpp" line="695"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../gamekey/gamekeymanager.cpp" line="487"/>
        <source>Can&apos;t add more game key!</source>
        <translation>無法添加更多遊戲金鑰！</translation>
    </message>
    <message>
        <location filename="../gamekey/gamekeymanager.cpp" line="695"/>
        <source>Don&apos;t support game key under multi display mode! Please disable multi display mode of this app and restart!</source>
        <translation>不支援多屏模式下的遊戲鍵！請禁用此應用程式的多顯示模式並重新啟動！</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">是(Y)</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否(N)</translation>
    </message>
</context>
<context>
    <name>GameKeyStackWidget</name>
    <message>
        <location filename="../gamekey/gamekeystackwidget.cpp" line="22"/>
        <source>K&amp;&amp;M</source>
        <translation>K&amp;M</translation>
    </message>
    <message>
        <location filename="../gamekey/gamekeystackwidget.cpp" line="23"/>
        <source>JOYSTICK</source>
        <translation>操縱桿</translation>
    </message>
</context>
<context>
    <name>GameListItem</name>
    <message>
        <source>Clear</source>
        <translation type="vanished">清除</translation>
    </message>
    <message>
        <source>Confirm clear</source>
        <translation type="vanished">确认清除</translation>
    </message>
    <message>
        <source>You will remove %1 from the white list of game buttons</source>
        <translation type="vanished">你将把 %1 从游戏按键白名单中清除</translation>
    </message>
    <message>
        <source>Are you sure you want to continue?</source>
        <translation type="vanished">你确定要继续执行吗？</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <source>Clearing...</source>
        <translation type="vanished">清除中...</translation>
    </message>
</context>
<context>
    <name>GameWidget</name>
    <message>
        <source>Add</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>GlesVersionWidget</name>
    <message>
        <source>Autoselect</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <source>OpenGL ES 2.0(Compatibility)</source>
        <translation type="vanished">OpenGL ES 2.0（兼容）</translation>
    </message>
    <message>
        <source>OpenGL ES 3.0</source>
        <translation type="vanished">OpenGL ES 3.0</translation>
    </message>
    <message>
        <source>OpenGL ES 3.1(Renderer maximum)</source>
        <translation type="vanished">OpenGL ES 3.1（渲染器最大值）</translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="vanished">默认</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="obsolete">默认</translation>
    </message>
    <message>
        <source>OpenGL ES API level(requires restart)</source>
        <translation type="vanished">OpenGL ES API （需要重启）</translation>
    </message>
    <message>
        <source>Restart system</source>
        <translation type="vanished">重启系统</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <source>Restart system?</source>
        <translation type="vanished">重启系统？</translation>
    </message>
    <message>
        <source>Are you sure you want to restart system?</source>
        <translation type="vanished">你确定要重启系统吗？</translation>
    </message>
</context>
<context>
    <name>InputItem</name>
    <message>
        <source>For example: the glory of the king</source>
        <translation type="vanished">比如：王者荣耀</translation>
    </message>
    <message>
        <source>For example: com.tencent.tmgp.sgame</source>
        <translation type="vanished">比如：com.tencent.tmgp.sgame</translation>
    </message>
</context>
<context>
    <name>JoystickManager</name>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="75"/>
        <location filename="../gamekey/joystickmanager.cpp" line="250"/>
        <location filename="../gamekey/joystickmanager.cpp" line="255"/>
        <location filename="../gamekey/joystickmanager.cpp" line="612"/>
        <location filename="../gamekey/joystickmanager.cpp" line="944"/>
        <location filename="../gamekey/joystickmanager.cpp" line="960"/>
        <location filename="../gamekey/joystickmanager.cpp" line="1455"/>
        <location filename="../gamekey/joystickmanager.cpp" line="1463"/>
        <location filename="../gamekey/joystickmanager.cpp" line="1471"/>
        <location filename="../gamekey/joystickmanager.cpp" line="1487"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="76"/>
        <source>Don&apos;t support game key under multi display mode! Please disable multi display mode of this app and restart!</source>
        <translation>不支援多屏模式下的遊戲鍵！請禁用此應用程式的多顯示模式並重新啟動！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="250"/>
        <location filename="../gamekey/joystickmanager.cpp" line="255"/>
        <source>Can&apos;t add more game key!</source>
        <translation>無法添加更多遊戲金鑰！</translation>
    </message>
    <message>
        <source>Function development, please look forward to!</source>
        <translation type="vanished">功能开发中，敬请期待！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="613"/>
        <source>Do you want to save the modification?</source>
        <translation>是否要保存修改？</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="944"/>
        <location filename="../gamekey/joystickmanager.cpp" line="960"/>
        <source>The Key is existing, please input another key!</source>
        <translation>金鑰已存在，請輸入另一個金鑰！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="1065"/>
        <source>Handle connected!</source>
        <translation>手柄已連接！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="1082"/>
        <source>Handle disconnected!</source>
        <translation>手柄斷開！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="1456"/>
        <source>Please complete the key value of the direction key！</source>
        <translation>請填寫方向鍵的鍵值！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="1464"/>
        <source>Please complete the key value of the view key！</source>
        <translation>請填寫查看鍵的鍵值！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="1472"/>
        <source>Please complete the key value of the Accelate key！</source>
        <translation>請填寫 Accelate 鍵的鍵值！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="1488"/>
        <source>Please complete the key value of the CrossHair key！</source>
        <translation>請填寫十字准線鍵的鍵值！</translation>
    </message>
    <message>
        <location filename="../gamekey/joystickmanager.cpp" line="1861"/>
        <location filename="../gamekey/joystickmanager.cpp" line="1961"/>
        <source>The environment change，please reset button!</source>
        <translation>環境發生變化，請重置按鈕！</translation>
    </message>
</context>
<context>
    <name>JoystickSetWidget</name>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="47"/>
        <source>Click</source>
        <translation>點擊</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="69"/>
        <source>Combo</source>
        <translation>組合</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="91"/>
        <source>L-Joy.</source>
        <translation>L-喬伊。</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="105"/>
        <source>Click the button to create a custom game button</source>
        <translation>按兩下按鈕以建立自訂遊戲按鈕</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="113"/>
        <source>R-Joy.</source>
        <translation>R-喬伊。</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="136"/>
        <source>Skills</source>
        <translation>技能</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="191"/>
        <source>Save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="207"/>
        <source>Clear</source>
        <translation>清楚</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="211"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../gamekey/joysticksetwidget.cpp" line="212"/>
        <source>Are you sure to delete all the keys?!</source>
        <translation>你確定刪除所有金鑰嗎？！</translation>
    </message>
</context>
<context>
    <name>KeyMouseSetWidget</name>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="47"/>
        <source>Direction</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="69"/>
        <source>Click</source>
        <translation>點擊</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="91"/>
        <source>View</source>
        <translation>視圖</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="113"/>
        <source>Crosshair</source>
        <translation>十字准線</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="135"/>
        <source>Fire</source>
        <translation>火災</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="157"/>
        <source>Combo</source>
        <translation>組合</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="179"/>
        <source>Accele</source>
        <translation>加速</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="201"/>
        <source>Skills</source>
        <translation>技能</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="260"/>
        <source>Save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../gamekey/keymousesetwidget.cpp" line="276"/>
        <source>Clear</source>
        <translation>清楚</translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是(Y)</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否(N)</translation>
    </message>
</context>
<context>
    <name>KylinUI::MessageBox::CustomerMessageBox</name>
    <message>
        <location filename="../messagebox.cpp" line="114"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="119"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
</context>
<context>
    <name>LockWidget</name>
    <message>
        <location filename="../lockwidget.cpp" line="39"/>
        <source>Please enter your password</source>
        <translation>請輸入您的密碼</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="67"/>
        <source>forget</source>
        <translation>忘記</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="155"/>
        <source>Account/Password is not correct</source>
        <translation>帳戶/密碼不正確</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="158"/>
        <source>reset code</source>
        <translation>重置代碼</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="159"/>
        <source>kylin id</source>
        <translation>麒麟身份</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="160"/>
        <source>kylin code</source>
        <translation>麒麟碼</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="161"/>
        <source>new code</source>
        <translation>新代碼</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="187"/>
        <source>back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../lockwidget.cpp" line="190"/>
        <source>confirm</source>
        <translation>確認</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <source>Screenshot Shared</source>
        <translation type="vanished">截图</translation>
    </message>
    <message>
        <source>Screen recording Shared</source>
        <translation type="vanished">录屏</translation>
    </message>
    <message>
        <source>Pop up virtual keyboard</source>
        <translation type="vanished">虚拟键盘</translation>
    </message>
    <message>
        <source>Open Storage</source>
        <translation type="vanished">移动数据</translation>
    </message>
    <message>
        <source>Open Gallery</source>
        <translation type="vanished">相册</translation>
    </message>
    <message>
        <source>Open WeChat Download</source>
        <translation type="vanished">微信下载目录</translation>
    </message>
    <message>
        <source>StayTop</source>
        <translation type="vanished">置顶</translation>
    </message>
    <message>
        <source>Open MM Download</source>
        <translation type="vanished">微信下载目录</translation>
    </message>
    <message>
        <source>Open QQ Files</source>
        <translation type="vanished">QQ接收文件目录</translation>
    </message>
    <message>
        <source>ToTop</source>
        <translation type="vanished">置顶</translation>
    </message>
</context>
<context>
    <name>MouseCrossHairKey</name>
    <message>
        <location filename="../gamekey/mousecrosshairkey.cpp" line="44"/>
        <source>PointReset</source>
        <translation>點重置</translation>
    </message>
</context>
<context>
    <name>NetMaskItem</name>
    <message>
        <source>Set the subnet mask of container docker</source>
        <translation type="vanished">设置容器docker的子网掩码</translation>
    </message>
</context>
<context>
    <name>NoJoystickWidget</name>
    <message>
        <location filename="../gamekey/nojoystickwidget.cpp" line="18"/>
        <source>No handle detected</source>
        <translation>未檢測到句柄</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>default</source>
        <translation type="vanished">默认</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="141"/>
        <location filename="../messagebox.cpp" line="158"/>
        <source>Ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="176"/>
        <source>Yes</source>
        <translation>是的</translation>
    </message>
    <message>
        <location filename="../messagebox.cpp" line="177"/>
        <source>No</source>
        <translation>不</translation>
    </message>
</context>
<context>
    <name>RadioButtonItem</name>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>RecordScreenWidget</name>
    <message>
        <location filename="../displaymanager/recordscreenwidget.ui" line="135"/>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="436"/>
        <source>Record</source>
        <translation>記錄</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.ui" line="122"/>
        <source>Select window</source>
        <translation>選擇視窗</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.ui" line="148"/>
        <source>Audio</source>
        <translation>音訊</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.ui" line="161"/>
        <source>Fullscreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.ui" line="14"/>
        <location filename="../displaymanager/recordscreenwidget.ui" line="174"/>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="433"/>
        <source>Screen recording Shared</source>
        <translation>屏幕錄製共用</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="536"/>
        <source>Can&apos;t find any supported video codecs!</source>
        <translation>找不到任何支持的視頻編解碼器！</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="536"/>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="553"/>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="1105"/>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="1242"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="553"/>
        <source>Can&apos;t find any supported audio codecs! will disable audio record!</source>
        <translation>找不到任何支援的音訊編解碼器！將禁用錄音！</translation>
    </message>
    <message>
        <source>Saved video</source>
        <translation type="vanished">保存视频</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="841"/>
        <source>Record video </source>
        <translation>錄製視頻 </translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="841"/>
        <source> saved in video folder under user home path.</source>
        <translation> 保存在使用者主頁路徑下的視頻資料夾中。</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="958"/>
        <source>Encoding frame data ...</source>
        <translation>編碼幀數據...</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="1105"/>
        <source>Screen size changed, so stop and cancel recording!</source>
        <translation>螢幕尺寸已更改，因此請停止並取消錄製！</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="1242"/>
        <source>Video file will be reach the largest limited size soon! So stop recording now!</source>
        <translation>視頻檔將很快達到最大限制大小！所以現在停止錄製！</translation>
    </message>
</context>
<context>
    <name>RecordingToolPanel</name>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="52"/>
        <source>Start/Pause</source>
        <translation>開始/暫停</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="58"/>
        <source>Stop</source>
        <translation>停</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="65"/>
        <source>Share</source>
        <translation>共用</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="72"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="156"/>
        <source>Cancel recording?</source>
        <translation>取消錄製？</translation>
    </message>
    <message>
        <location filename="../displaymanager/recordscreenwidget.cpp" line="156"/>
        <source>Recording is going on! Do you want to cancel recording now?</source>
        <translation>錄製正在進行中！是否要立即取消錄製？</translation>
    </message>
</context>
<context>
    <name>RemoveGameWidget</name>
    <message>
        <source>Refresh</source>
        <translation type="vanished">刷新</translation>
    </message>
</context>
<context>
    <name>ScrollSettingWidget</name>
    <message>
        <location filename="../scrollsettingwidget.cpp" line="21"/>
        <source>Mouse sensitivity</source>
        <translation>滑鼠靈敏度</translation>
    </message>
    <message>
        <location filename="../scrollsettingwidget.cpp" line="39"/>
        <source>ok</source>
        <translation>還行</translation>
    </message>
    <message>
        <location filename="../scrollsettingwidget.cpp" line="33"/>
        <source>slow</source>
        <translation>慢</translation>
    </message>
    <message>
        <location filename="../scrollsettingwidget.cpp" line="34"/>
        <source>quick</source>
        <translation>快</translation>
    </message>
    <message>
        <location filename="../scrollsettingwidget.cpp" line="40"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>SensitivityWidget</name>
    <message>
        <location filename="../gamekey/sensitivitywidget.cpp" line="21"/>
        <source>Sensitivity</source>
        <translation>敏感性</translation>
    </message>
</context>
<context>
    <name>SettingsFrame</name>
    <message>
        <source>Preference</source>
        <translation type="vanished">配置</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">显示</translation>
    </message>
    <message>
        <source>Renderer</source>
        <translation type="vanished">渲染器</translation>
    </message>
    <message>
        <source>Game</source>
        <translation type="vanished">游戏</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation type="vanished">摄像头</translation>
    </message>
</context>
<context>
    <name>SettingsPanel</name>
    <message>
        <location filename="../gamekey/settingspanel.cpp" line="23"/>
        <source>Game key</source>
        <translation>遊戲金鑰</translation>
    </message>
    <message>
        <location filename="../gamekey/settingspanel.cpp" line="24"/>
        <location filename="../gamekey/settingspanel.cpp" line="36"/>
        <source>Click the button to create a custom game button</source>
        <translation>按兩下按鈕以建立自訂遊戲按鈕</translation>
    </message>
    <message>
        <location filename="../gamekey/settingspanel.cpp" line="35"/>
        <source>Direction key</source>
        <translation>方向鍵</translation>
    </message>
    <message>
        <location filename="../gamekey/settingspanel.cpp" line="53"/>
        <source>transparency</source>
        <translation>透明度</translation>
    </message>
    <message>
        <location filename="../gamekey/settingspanel.cpp" line="57"/>
        <source>Save</source>
        <translation>救</translation>
    </message>
    <message>
        <location filename="../gamekey/settingspanel.cpp" line="64"/>
        <source>Hide</source>
        <translation>隱藏</translation>
    </message>
    <message>
        <location filename="../gamekey/settingspanel.cpp" line="71"/>
        <source>Clear</source>
        <translation>清楚</translation>
    </message>
</context>
<context>
    <name>SingleKey</name>
    <message>
        <location filename="../gamekey/singlekey.cpp" line="246"/>
        <location filename="../gamekey/singlekey.cpp" line="253"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../gamekey/singlekey.cpp" line="246"/>
        <location filename="../gamekey/singlekey.cpp" line="253"/>
        <source>The Key is existing, please input another key!</source>
        <translation>金鑰已存在，請輸入另一個金鑰！</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="138"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="184"/>
        <source>Fullscreen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="179"/>
        <source>Menu</source>
        <translation>功能表</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="198"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="212"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="217"/>
        <source>Top</source>
        <translation>返回頁首</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="222"/>
        <source>Cancel Top</source>
        <translation>取消頂部</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="228"/>
        <source>Screenshot</source>
        <translation>截圖</translation>
    </message>
    <message>
        <location filename="../displaymanager/titlebar.cpp" line="291"/>
        <source>Hide Current Window</source>
        <translation>隱藏當前視窗</translation>
    </message>
</context>
<context>
    <name>TitleBarJoystick</name>
    <message>
        <location filename="../displaymanager/titlebarjoystick.cpp" line="33"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>TitleMenu</name>
    <message>
        <location filename="../displaymanager/menu.cpp" line="61"/>
        <source>Preference</source>
        <translation>偏好</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="68"/>
        <source>Game Key</source>
        <translation>遊戲金鑰</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="76"/>
        <source>Joystick</source>
        <translation>操縱桿</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="82"/>
        <source>Screenshot Shared</source>
        <translation>截圖共用</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="88"/>
        <source>Screen recording Shared</source>
        <translation>屏幕錄製共用</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="94"/>
        <source>Pop up virtual keyboard</source>
        <translation>彈出虛擬鍵盤</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="101"/>
        <source>Open Storage</source>
        <translation>開放式存儲</translation>
    </message>
    <message>
        <source>Open Gallery</source>
        <translation type="vanished">相册</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="147"/>
        <source>Shake</source>
        <translation>搖</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="153"/>
        <source>virtualgps</source>
        <translation>虛擬全球定位系統</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="159"/>
        <source>gravity_sensor</source>
        <translation>gravity_sensor</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="165"/>
        <source>Rotate</source>
        <translation>旋轉</translation>
    </message>
    <message>
        <source>Doc</source>
        <translation type="vanished">手机应用文件夹</translation>
    </message>
    <message>
        <source>Open MM Download</source>
        <translation type="vanished">微信下载目录</translation>
    </message>
    <message>
        <source>Open QQ Files</source>
        <translation type="vanished">QQ接收文件目录</translation>
    </message>
    <message>
        <source>StayTop</source>
        <translation type="vanished">置顶</translation>
    </message>
    <message>
        <source>ToTop</source>
        <translation type="vanished">置顶</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="129"/>
        <source>Lock screen</source>
        <translation>鎖定螢幕</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="171"/>
        <source>Scroll</source>
        <translation>滾動</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="135"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="141"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="117"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <source>User manual</source>
        <translation type="vanished">用户手册</translation>
    </message>
    <message>
        <source>Clean up temporary files of APK installation package</source>
        <translation type="vanished">清理APK安装包临时文件</translation>
    </message>
    <message>
        <location filename="../displaymanager/menu.cpp" line="123"/>
        <source>Close Kmre</source>
        <translation>關閉 公里雷</translation>
    </message>
    <message>
        <source>Clean up idle Android images</source>
        <translation type="vanished">清理闲置的Android镜像</translation>
    </message>
</context>
<context>
    <name>WarningNotice</name>
    <message>
        <location filename="../displaymanager/warningnotice.cpp" line="35"/>
        <source>Don&apos;t remind anymore</source>
        <translation>不要再提醒了</translation>
    </message>
    <message>
        <location filename="../displaymanager/warningnotice.cpp" line="95"/>
        <source>Playing with KMRE may result in a ban</source>
        <translation>玩KMRE可能會導致禁令</translation>
    </message>
    <message>
        <location filename="../displaymanager/warningnotice.cpp" line="110"/>
        <source>Payment is risky and use with caution</source>
        <translation>付款有風險，請謹慎使用</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是(Y)</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否(N)</translation>
    </message>
    <message>
        <source>This application can only be used after it is added to the white list. For the method of adding white list, see &quot;user manual - game keyboard white list setting&quot;</source>
        <translation type="vanished">该应用需要加入到白名单后才可使用，加入白名单方法见&quot;用户手册-游戏键盘白名单设置&quot;</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="825"/>
        <source>Close Kmre?</source>
        <translation>關閉公里？</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="825"/>
        <source>After closing Kmre, all mobile application windows will be forced to close. Are you sure you want to close Kmre?</source>
        <translation>關閉 Kmre 後，所有行動應用程式視窗將被強制關閉。您確定要關閉 Kmre 嗎？</translation>
    </message>
    <message>
        <source>Kmre</source>
        <translation type="vanished">麒麟移动运行环境</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="818"/>
        <source>The operation will clean up APK cache in KMRE. Please ensure that no apk is performing installation, Continue?</source>
        <translation>該操作將清理 KMRE 中的 APK 快取。請確保沒有apk正在執行安裝，繼續嗎？</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="818"/>
        <source>Clear Apk Cache?</source>
        <translation>清除 apk 快取？</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="913"/>
        <source>This application can only be used after it is added to the white list.                 For the method of adding white list, see &quot;user manual - game keyboard white list setting&quot;</source>
        <translation>此應用程式只有在添加到白名單後才能使用。                添加白名單的方法請參見“使用者手冊-遊戲鍵盤白名單設置”</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1062"/>
        <source>Open App</source>
        <translation>打開應用程式</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1072"/>
        <source>Quit App</source>
        <translation>退出應用</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1636"/>
        <source>Share failed ! (copy file failed)</source>
        <translation>共享失敗！（複製檔案失敗）</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1642"/>
        <source>Share failed! File size is exceed size limition(100MB)!</source>
        <translation>共享失敗！檔大小超過大小限制（100MB）！</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>Open Window</source>
        <translation type="vanished">打开窗口</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1065"/>
        <source>Preference</source>
        <translation>偏好</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1282"/>
        <location filename="../widget.cpp" line="1292"/>
        <source>%1 Message</source>
        <translation>%1 消息</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1283"/>
        <location filename="../widget.cpp" line="1293"/>
        <source>You have new news</source>
        <translation>您有新消息</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1636"/>
        <location filename="../widget.cpp" line="1642"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="1841"/>
        <source>Mouse lock is enabled in the window,press F2 to exit</source>
        <translation>視窗中啟用了滑鼠鎖定，按F2退出</translation>
    </message>
    <message>
        <source>Test notification</source>
        <translation type="vanished">测试通知消息</translation>
    </message>
</context>
<context>
    <name>kmre::DbusClient</name>
    <message>
        <location filename="../dbusclient.cpp" line="90"/>
        <source>_</source>
        <translation>_</translation>
    </message>
</context>
</TS>
