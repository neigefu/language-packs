<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>AgreementWindow</name>
    <message>
        <location filename="../greeter/agreementwindow.cpp" line="32"/>
        <source>I know</source>
        <translation>我已知晓</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <source>Current device: </source>
        <translation type="vanished">当前设备：</translation>
    </message>
    <message>
        <source>Identify failed, Please retry.</source>
        <translation type="vanished">识别失败，请重试</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <source>Please select the biometric device</source>
        <translation type="vanished">请选择生物设备</translation>
    </message>
    <message>
        <source>Device type:</source>
        <translation type="vanished">设备类型：</translation>
    </message>
    <message>
        <source>Device name:</source>
        <translation type="vanished">设备型号：</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charsmorewidget.cpp" line="166"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="97"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="111"/>
        <source>ABC</source>
        <translation>ABC</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="124"/>
        <source>123</source>
        <translation>123</translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <source>edit network</source>
        <translation type="vanished">编辑网络设置</translation>
    </message>
    <message>
        <source>LAN name: </source>
        <translation type="vanished">网络名称：</translation>
    </message>
    <message>
        <source>Method: </source>
        <translation type="vanished">编辑IP设置: </translation>
    </message>
    <message>
        <source>Address: </source>
        <translation type="vanished">IP地址: </translation>
    </message>
    <message>
        <source>Netmask: </source>
        <translation type="vanished">子网掩码: </translation>
    </message>
    <message>
        <source>Gateway: </source>
        <translation type="vanished">默认网关: </translation>
    </message>
    <message>
        <source>DNS 1: </source>
        <translation type="vanished">首选DNS: </translation>
    </message>
    <message>
        <source>DNS 2: </source>
        <translation type="vanished">备选DNS: </translation>
    </message>
    <message>
        <source>Edit Conn</source>
        <translation type="vanished">网络设置</translation>
    </message>
    <message>
        <source>Auto(DHCP)</source>
        <translation type="vanished">自动(DHCP)</translation>
    </message>
    <message>
        <source>Manual</source>
        <translation type="vanished">手动</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">保存</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Can not create new wired network for without wired card</source>
        <translation type="vanished">缺少有线网卡 无法新建网络</translation>
    </message>
    <message>
        <source>New network already created</source>
        <translation type="vanished">已创建新的有线网络</translation>
    </message>
    <message>
        <source>New network settings already finished</source>
        <translation>新的网络配置已经完成</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="329"/>
        <source>New settings already effective</source>
        <translation>新设置已生效</translation>
    </message>
    <message>
        <source>Edit Network</source>
        <translation type="vanished">编辑网络设置</translation>
    </message>
    <message>
        <source>Add Wired Network</source>
        <translation type="vanished">新建有线网络</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="38"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="40"/>
        <source>FingerVein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="42"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="44"/>
        <source>Face</source>
        <translation>人脸识别</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="46"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="48"/>
        <source>ukey</source>
        <translation>安全密钥</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="50"/>
        <source>QRCode</source>
        <translation>二维码</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add Hidden Wi-Fi</source>
        <translation type="vanished">加入隐藏Wi-Fi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Wi-Fi name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">添加Wifi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation>动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="101"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">添加隐藏Wi-Fi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">身份验证</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="89"/>
        <source>Allow automatic PAC pro_visioning</source>
        <translation>允许自动 PAC 配置</translation>
    </message>
    <message>
        <source>PAC file</source>
        <translation type="vanished">PAC 文件</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">内部验证</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="124"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="136"/>
        <source>Tunneled TLS</source>
        <translation type="vanished">隧道TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation>保护EAP (PEAP)</translation>
    </message>
    <message>
        <source>Anonymous</source>
        <translation type="vanished">匿名</translation>
    </message>
    <message>
        <source>Authenticated</source>
        <translation>已认证</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="145"/>
        <source>Both</source>
        <translation>两者兼用</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">添加隐藏Wi-Fi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">身份验证</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="119"/>
        <source>Tunneled TLS</source>
        <translation type="vanished">隧道TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation type="vanished">保护EAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">添加隐藏Wi-Fi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">身份认证</translation>
    </message>
    <message>
        <source>Anonymous identity</source>
        <translation type="vanished">匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">域</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA证书</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA证书密码</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">不需要CA证书</translation>
    </message>
    <message>
        <source>PEAP version</source>
        <translation type="vanished">PEAP环境</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation type="vanished">内部验证</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation>动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="132"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation type="vanished">隧道TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation>保护EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation type="vanished">选择文件</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="154"/>
        <source>Version 0</source>
        <translation>版本 0</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="155"/>
        <source>Version 1</source>
        <translation>版本 1</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">添加Wifi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">认证</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="vanished">WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation type="vanished">WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="vanished">动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="108"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="120"/>
        <source>Tunneled TLS</source>
        <translation type="vanished">隧道TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation>保护EAP (PEAP)</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">添加隐藏Wi-Fi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Authentication</source>
        <translation type="vanished">认证</translation>
    </message>
    <message>
        <source>Identity</source>
        <translation type="vanished">身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation type="vanished">域</translation>
    </message>
    <message>
        <source>CA certificate</source>
        <translation type="vanished">CA认证</translation>
    </message>
    <message>
        <source>CA certificate password</source>
        <translation type="vanished">CA认证密码</translation>
    </message>
    <message>
        <source>No CA certificate is required</source>
        <translation type="vanished">不需要CA认证</translation>
    </message>
    <message>
        <source>User certificate</source>
        <translation type="vanished">用户认证</translation>
    </message>
    <message>
        <source>User certificate password</source>
        <translation type="vanished">用户认证密码</translation>
    </message>
    <message>
        <source>User private key</source>
        <translation type="vanished">用户认证私钥</translation>
    </message>
    <message>
        <source>User key password</source>
        <translation type="vanished">用户密钥密码</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation>动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="131"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="143"/>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation>保护EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add hidden Wi-Fi</source>
        <translation type="vanished">添加Wifi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="93"/>
        <source>Authentication</source>
        <translation>认证</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="94"/>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="96"/>
        <source>CA certificate</source>
        <translation>CA 证书</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="97"/>
        <source>CA certificate password</source>
        <translation>CA 证书密码</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="98"/>
        <source>No CA certificate is required</source>
        <translation>不需要CA认证</translation>
    </message>
    <message>
        <source>Inner authentication</source>
        <translation>内部验证</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">用户名</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation>动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="129"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <source>Protected EAP (PEAP)</source>
        <translation>保护EAP (PEAP)</translation>
    </message>
    <message>
        <source>Choose from file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加无线网络</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性:</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="77"/>
        <source>Key</source>
        <translation>键</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="78"/>
        <source>WEP index</source>
        <translation>WEP 索引</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="79"/>
        <source>Authentication</source>
        <translation>认证</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="80"/>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位密钥(十六进制或ASCII码)</translation>
    </message>
    <message>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密码</translation>
    </message>
    <message>
        <source>Dynamic WEP (802.1X)</source>
        <translation>动态WEP(802.1X)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="115"/>
        <source>1(default)</source>
        <translation>1(默认)</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="121"/>
        <source>Open System</source>
        <translation>开放式系统</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="122"/>
        <source>Shared Key</source>
        <translation>共享密钥</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Add Hidden Wi-Fi</source>
        <translation type="vanished">加入隐藏Wi-Fi</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation type="vanished">连接设置:</translation>
    </message>
    <message>
        <source>Wi-Fi name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>C_reate…</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="vanished">WPA 及 WPA2 个人</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <source>Create Hotspot</source>
        <translation type="vanished">创建个人热点</translation>
    </message>
    <message>
        <source>Network name</source>
        <translation type="vanished">网络名称:</translation>
    </message>
    <message>
        <source>Wi-Fi security</source>
        <translation type="vanished">Wi-Fi 安全性:</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 个人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
</context>
<context>
    <name>FakeDialog</name>
    <message>
        <location filename="../common/fakedialog.cpp" line="63"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>GreeterWindow</name>
    <message>
        <source>Power dialog</source>
        <translation type="vanished">电源对话框</translation>
    </message>
    <message>
        <source>On-screen keyboard, providing virtual keyboard function</source>
        <translation type="vanished">屏幕键盘，提供虚拟键盘功能</translation>
    </message>
    <message>
        <source>Set the desktop environment for the selected user to log in.If the user is logged in, it will take effect after logging in again</source>
        <translation type="vanished">设置选中用户登录后的桌面环境，如果用户已经登录，则会在重新登录后生效</translation>
    </message>
    <message>
        <source>Set the language of the selected user after logging in. If the user is logged in, it will take effect after logging in again.</source>
        <translation type="vanished">设置选中用户登录后的语言，如果用户已登录，则在重新登录后生效</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="158"/>
        <source>LAN</source>
        <translation>有线网络</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="160"/>
        <source>WLAN</source>
        <translation>无线局域网</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="347"/>
        <source>VirtualKeyboard</source>
        <translation>虚拟键盘</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="349"/>
        <source>Power</source>
        <translation>电源</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="351"/>
        <source>SwitchUser</source>
        <translation>切换用户</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="1005"/>
        <source>Shut Down</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="1008"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="1011"/>
        <source>Sleep</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="1014"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="1557"/>
        <location filename="../greeter/greeterwindow.cpp" line="1970"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
</context>
<context>
    <name>GreeterWrapper</name>
    <message>
        <location filename="../greeter/greeterwrapper.cpp" line="169"/>
        <source>failed to start session.</source>
        <translation>启动会话失败</translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../greeter/iconedit.cpp" line="139"/>
        <source>OK</source>
        <translation>提交</translation>
    </message>
    <message>
        <location filename="../greeter/iconedit.cpp" line="247"/>
        <location filename="../greeter/iconedit.cpp" line="295"/>
        <source>Password: </source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../greeter/iconedit.cpp" line="249"/>
        <location filename="../greeter/iconedit.cpp" line="297"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">可用有线网络列表</translation>
    </message>
    <message>
        <source>New LAN</source>
        <translation type="vanished">新建有线网络</translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">加入其他网络</translation>
    </message>
    <message>
        <source>Hide WiFi</source>
        <translation type="vanished">加入其他网络</translation>
    </message>
    <message>
        <source>No usable network in the list</source>
        <translation type="vanished">列表暂无可连接网络</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>有线网络</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">Wifi</translation>
    </message>
    <message>
        <source>无线网络</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <source>HotSpot</source>
        <translation type="vanished">个人热点</translation>
    </message>
    <message>
        <source>FlyMode</source>
        <translation type="vanished">飞行模式</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">设置网络</translation>
    </message>
    <message>
        <source>Show KylinNM</source>
        <translation type="vanished">Show KylinNM</translation>
    </message>
    <message>
        <source>Inactivated LAN</source>
        <translation type="vanished">未激活</translation>
    </message>
    <message>
        <source>Other WLAN</source>
        <translation type="vanished">其他</translation>
    </message>
    <message>
        <source>LAN</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>WLAN</source>
        <translation type="vanished">无线局域网</translation>
    </message>
    <message>
        <source>No wireless card detected</source>
        <translation type="vanished">未检测到无线网卡</translation>
    </message>
    <message>
        <source>Activated LAN</source>
        <translation type="vanished">已激活</translation>
    </message>
    <message>
        <source>Activated WLAN</source>
        <translation type="vanished">已激活</translation>
    </message>
    <message>
        <source>Not connected</source>
        <translation type="vanished">当前未连接任何网络</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>NetOn,</source>
        <translation type="vanished">网络开启</translation>
    </message>
    <message>
        <source>No Other Wired Network Scheme</source>
        <translation type="vanished">列表中无其他有线网络</translation>
    </message>
    <message>
        <source>No Other Wireless Network Scheme</source>
        <translation type="vanished">未检测到其他无线网络</translation>
    </message>
    <message>
        <source>Wired net is disconnected</source>
        <translation type="vanished">断开有线网络</translation>
    </message>
    <message>
        <source>Wi-Fi is disconnected</source>
        <translation type="vanished">断开无线网络</translation>
    </message>
    <message>
        <source>Conn Ethernet Success</source>
        <translation type="vanished">连接有线网络成功</translation>
    </message>
    <message>
        <source>Conn Ethernet Fail</source>
        <translation type="vanished">连接有线网络失败</translation>
    </message>
    <message>
        <source>Conn Wifi Success</source>
        <translation type="vanished">连接无线网络成功</translation>
    </message>
    <message>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation>请确认Wi-Fi密码或无线设备</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.ui" line="14"/>
        <source>kylin-nm</source>
        <translation>网络工具</translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="140"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="154"/>
        <source>123</source>
        <translation>123</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="168"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="196"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="70"/>
        <location filename="../greeter/loginoptionswidget.cpp" line="93"/>
        <source>Login Options</source>
        <translation>登录选项</translation>
    </message>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="312"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="338"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="748"/>
        <source>Identify device removed!</source>
        <translation>校验设备已移除!</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>logged in</source>
        <translation type="vanished">已登录</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="858"/>
        <source>login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>Incorrect user name, please input again</source>
        <translation type="vanished">用户名不正确，请重新输入</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1269"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1070"/>
        <source>Password: </source>
        <translation>密码：</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1213"/>
        <source>User name input error!</source>
        <translation>用户名输入错误！</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1216"/>
        <location filename="../greeter/loginwindow.cpp" line="1221"/>
        <source>Authentication failure, Please try again</source>
        <translation>认证失败，请重试</translation>
    </message>
    <message>
        <source>Biometric/code scanning authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1974"/>
        <location filename="../greeter/loginwindow.cpp" line="1975"/>
        <location filename="../greeter/loginwindow.cpp" line="2052"/>
        <location filename="../greeter/loginwindow.cpp" line="2053"/>
        <source>Please try again in %1 minutes.</source>
        <translation>请%1分钟后再试</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1984"/>
        <location filename="../greeter/loginwindow.cpp" line="1985"/>
        <location filename="../greeter/loginwindow.cpp" line="2063"/>
        <location filename="../greeter/loginwindow.cpp" line="2064"/>
        <source>Please try again in %1 seconds.</source>
        <translation>请%1秒后再试</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1994"/>
        <location filename="../greeter/loginwindow.cpp" line="1995"/>
        <location filename="../greeter/loginwindow.cpp" line="2073"/>
        <location filename="../greeter/loginwindow.cpp" line="2074"/>
        <source>Account locked permanently.</source>
        <translation>账号已被永久锁定</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1139"/>
        <source>Password cannot be empty</source>
        <translation>密码不能为空</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="204"/>
        <location filename="../greeter/loginwindow.cpp" line="285"/>
        <source>Enter the ukey password</source>
        <translation>输入安全密钥密码</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="223"/>
        <location filename="../greeter/loginwindow.cpp" line="289"/>
        <source>Insert the ukey into the USB port</source>
        <translation>请将安全密钥插入USB端口</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="510"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>验证人脸识别或输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="515"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>按压指纹或输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="520"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>验证声纹或输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="525"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>验证指静脉或输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="530"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>验证虹膜或输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1074"/>
        <source>Input Password</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1555"/>
        <location filename="../greeter/loginwindow.cpp" line="1717"/>
        <location filename="../greeter/loginwindow.cpp" line="1855"/>
        <location filename="../greeter/loginwindow.cpp" line="2216"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>验证%1失败，请输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1557"/>
        <location filename="../greeter/loginwindow.cpp" line="1719"/>
        <location filename="../greeter/loginwindow.cpp" line="1859"/>
        <location filename="../greeter/loginwindow.cpp" line="1861"/>
        <location filename="../greeter/loginwindow.cpp" line="2218"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>无法验证%1，请输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1890"/>
        <source>Abnormal network</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1875"/>
        <location filename="../greeter/loginwindow.cpp" line="1879"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>验证%1失败，您还有%2次尝试机会</translation>
    </message>
    <message>
        <source>Incorrect password, please input again</source>
        <translation type="obsolete">密码错误，请重新输入</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="281"/>
        <location filename="../greeter/loginwindow.cpp" line="629"/>
        <location filename="../greeter/loginwindow.cpp" line="842"/>
        <location filename="../greeter/loginwindow.cpp" line="1260"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="535"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>使用绑定的微信扫码或输入密码解锁</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1079"/>
        <source>Username</source>
        <translation>用户名</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../display-switch/ui_mainwindow.h" line="127"/>
        <source>MainWindow</source>
        <translation>主窗口</translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="143"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="157"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <source>Automatically join the network</source>
        <translation>自动加入该网络</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">断开连接</translation>
    </message>
    <message>
        <source>Input Password...</source>
        <translation type="vanished">输入密码...</translation>
    </message>
    <message>
        <source>自动连接</source>
        <translation type="vanished">自动连接</translation>
    </message>
    <message>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="vanished">连接到隐藏 Wi-Fi 网络</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="obsolete">速率</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="vanished">无</translation>
    </message>
    <message>
        <source>WiFi Security：</source>
        <translation type="vanished">WiFi安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="559"/>
        <source>Signal：</source>
        <translation>信号强度：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="560"/>
        <source>MAC：</source>
        <translation type="vanished">物理地址：</translation>
    </message>
    <message>
        <source>Conn Wifi Failed</source>
        <translation>连接无线网络失败</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation>类型</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="vanished">断开连接</translation>
    </message>
    <message>
        <source>No Configuration</source>
        <translation type="vanished">未配置</translation>
    </message>
    <message>
        <source>IPv4：</source>
        <translation type="vanished">IPv4地址：</translation>
    </message>
    <message>
        <source>IPv6：</source>
        <translation type="vanished">IPv6地址：</translation>
    </message>
    <message>
        <source>BandWidth：</source>
        <translation type="vanished">带宽：</translation>
    </message>
    <message>
        <source>MAC：</source>
        <translation>物理地址：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation>表</translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="126"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation>关闭所有应用，关闭电脑，然后重新打开电脑。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="128"/>
        <source>Close all apps, and then shut down your computer</source>
        <translation>关闭所有应用，然后关闭电脑。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="130"/>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation>电脑保持开机状态，但耗电较少。应用会一直保持打开状态，可快速唤醒电脑并恢复到你离开的状态。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="133"/>
        <source>Turn off your computer, but the app stays open. When the computer is turned on, it can be restored to the state you left</source>
        <translation>关闭电脑，但是应用会一直保持打开状态。当打开电脑时，可以恢复到你离开的状态。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="385"/>
        <source>Switch User</source>
        <translation>切换用户</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="386"/>
        <location filename="../greeter/powerwindow.cpp" line="441"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="459"/>
        <source>Shut Down</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="478"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="499"/>
        <source>Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="387"/>
        <source>Power Off</source>
        <translation>关机</translation>
    </message>
</context>
<context>
    <name>Surewidget</name>
    <message>
        <location filename="../greeter/surewidget.ui" line="14"/>
        <source>Form</source>
        <translation>表</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="56"/>
        <location filename="../greeter/surewidget.ui" line="66"/>
        <location filename="../greeter/surewidget.ui" line="79"/>
        <source>TextLabel</source>
        <translation>文本标签</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="130"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="143"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="21"/>
        <location filename="../greeter/surewidget.cpp" line="75"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation>同时有多个用户登录系统，您确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="40"/>
        <location filename="../greeter/surewidget.cpp" line="68"/>
        <source>There&apos;s a program running, this process cannot be interrupted.</source>
        <translation>有程序正在运行，此过程不能被打断。</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="43"/>
        <source>The system will automatically %1 after the program is executed. Do not shut down the computer.</source>
        <translation>系统将在执行完安装程序后自动%1，请勿关闭计算机。</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="72"/>
        <source>Click OK and the system will automatically %1 after the program is executed.</source>
        <translation>点击确认系统将在程序执行完毕后自动%1</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="111"/>
        <source>Shut Down</source>
        <translation>关机</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="114"/>
        <source>Restart</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="117"/>
        <source>Sleep</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="120"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
</context>
<context>
    <name>UsersModel</name>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="58"/>
        <source>Guest Session</source>
        <translation>游客登录</translation>
    </message>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="48"/>
        <location filename="../greeter/usersmodel.cpp" line="50"/>
        <source>Guest</source>
        <translation>游客登录</translation>
    </message>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="76"/>
        <location filename="../greeter/usersmodel.cpp" line="78"/>
        <location filename="../greeter/usersmodel.cpp" line="85"/>
        <location filename="../greeter/usersmodel.cpp" line="100"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <source>kylin network applet desktop message</source>
        <translation type="vanished">麒麟网络小程序桌面消息</translation>
    </message>
</context>
</TS>
