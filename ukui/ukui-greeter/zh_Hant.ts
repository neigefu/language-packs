<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant" sourcelanguage="en">
<context>
    <name>AgreementWindow</name>
    <message>
        <location filename="../greeter/agreementwindow.cpp" line="32"/>
        <source>I know</source>
        <translation>我已知曉</translation>
    </message>
</context>
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="107"/>
        <source>Current device: </source>
        <translation>目前裝置： </translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="179"/>
        <source>Identify failed, Please retry.</source>
        <translation>識別失敗，請重試</translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="51"/>
        <source>Please select the biometric device</source>
        <translation>請選擇生物設備</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="56"/>
        <source>Device type:</source>
        <translation>裝置類型：</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="73"/>
        <source>Device name:</source>
        <translation>設備型號：</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="83"/>
        <source>OK</source>
        <translation>確定</translation>
    </message>
</context>
<context>
    <name>CharsMoreWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charsmorewidget.cpp" line="166"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
</context>
<context>
    <name>CharsWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="97"/>
        <source>More</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="111"/>
        <source>ABC</source>
        <translation>ABC</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/charswidget.cpp" line="124"/>
        <source>123</source>
        <translation>123</translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="37"/>
        <source>edit network</source>
        <translation>編輯網路設置</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="115"/>
        <source>LAN name: </source>
        <translation>網路名稱： </translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="116"/>
        <source>Method: </source>
        <translation>編輯IP設定： </translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="117"/>
        <source>Address: </source>
        <translation>IP 位址： </translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="118"/>
        <source>Netmask: </source>
        <translation>子網掩碼： </translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="119"/>
        <source>Gateway: </source>
        <translation>預設閘道： </translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="120"/>
        <source>DNS 1: </source>
        <translation>首選DNS： </translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="121"/>
        <source>DNS 2: </source>
        <translation>備選DNS： </translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="123"/>
        <source>Edit Conn</source>
        <translation>網路設置</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="124"/>
        <location filename="../kylin-nm/src/confform.cpp" line="126"/>
        <source>Auto(DHCP)</source>
        <translation>自動（DHCP）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="125"/>
        <location filename="../kylin-nm/src/confform.cpp" line="127"/>
        <source>Manual</source>
        <translation>手動</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="137"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="138"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="139"/>
        <source>Ok</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="253"/>
        <source>Can not create new wired network for without wired card</source>
        <translation>缺少有線網卡 無法新建網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="270"/>
        <source>New network already created</source>
        <translation>已創建新的有線網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="320"/>
        <source>New network settings already finished</source>
        <translation>新的網路配置已經完成</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="329"/>
        <source>New settings already effective</source>
        <translation>新設置已生效</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="353"/>
        <source>Edit Network</source>
        <translation>編輯網路設置</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="389"/>
        <source>Add Wired Network</source>
        <translation>新建有線網路</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="38"/>
        <source>FingerPrint</source>
        <translation>指紋</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="40"/>
        <source>FingerVein</source>
        <translation>指靜脈</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="42"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="44"/>
        <source>Face</source>
        <translation>人臉識別</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="46"/>
        <source>VoicePrint</source>
        <translation>聲紋</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="48"/>
        <source>ukey</source>
        <translation>安全金鑰</translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="50"/>
        <source>QRCode</source>
        <translation>二維碼</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="76"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>加入隱藏Wi-Fi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="77"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="78"/>
        <source>Wi-Fi name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="79"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="80"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="81"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="104"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="105"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="68"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加Wifi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="69"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="70"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="71"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="72"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="73"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="74"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="75"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="77"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="95"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="96"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="97"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="98"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="100"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="101"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="83"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏Wi-Fi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="84"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="85"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="86"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="87"/>
        <source>Authentication</source>
        <translation>身份驗證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="88"/>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="89"/>
        <source>Allow automatic PAC pro_visioning</source>
        <translation>允許自動 PAC 配置</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="90"/>
        <source>PAC file</source>
        <translation>PAC 檔</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="91"/>
        <source>Inner authentication</source>
        <translation>內部驗證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="92"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="93"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="94"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="95"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="100"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="118"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="148"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="119"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="120"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="121"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="123"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="124"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="136"/>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="137"/>
        <source>Protected EAP (PEAP)</source>
        <translation>保護EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="143"/>
        <source>Anonymous</source>
        <translation>匿名</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="144"/>
        <source>Authenticated</source>
        <translation>已認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="145"/>
        <source>Both</source>
        <translation>兩者兼用</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏Wi-Fi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="74"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="75"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="77"/>
        <source>Authentication</source>
        <translation>身份驗證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="78"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="79"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="80"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="81"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="101"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="119"/>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="120"/>
        <source>Protected EAP (PEAP)</source>
        <translation>保護EAP （PEAP）</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="91"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏Wi-Fi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="92"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="93"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="94"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="95"/>
        <source>Authentication</source>
        <translation>身份認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="96"/>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="97"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="98"/>
        <source>CA certificate</source>
        <translation>CA證書</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="99"/>
        <source>CA certificate password</source>
        <translation>CA證書密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="100"/>
        <source>No CA certificate is required</source>
        <translation>不需要CA證書</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="101"/>
        <source>PEAP version</source>
        <translation>PEAP環境</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="102"/>
        <source>Inner authentication</source>
        <translation>內部驗證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="103"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="104"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="105"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="106"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="126"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="149"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="127"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="128"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="129"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="131"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="132"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="144"/>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="145"/>
        <source>Protected EAP (PEAP)</source>
        <translation>保護EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="150"/>
        <source>Choose from file</source>
        <translation>選擇檔案</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="153"/>
        <source>Automatic</source>
        <translation>自動</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="154"/>
        <source>Version 0</source>
        <translation>版本 0</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="155"/>
        <source>Version 1</source>
        <translation>版本 1</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="74"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加Wifi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="75"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="76"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="77"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="78"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="79"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="80"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="81"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="82"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="84"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="102"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="103"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="104"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="105"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="107"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="108"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="120"/>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="121"/>
        <source>Protected EAP (PEAP)</source>
        <translation>保護EAP （PEAP）</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="90"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加隱藏Wi-Fi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="91"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="92"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="93"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="94"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="95"/>
        <source>Identity</source>
        <translation>身份</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="96"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="97"/>
        <source>CA certificate</source>
        <translation>CA認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="98"/>
        <source>CA certificate password</source>
        <translation>CA認證密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="99"/>
        <source>No CA certificate is required</source>
        <translation>不需要CA認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="100"/>
        <source>User certificate</source>
        <translation>用戶認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="101"/>
        <source>User certificate password</source>
        <translation>使用者認證密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="102"/>
        <source>User private key</source>
        <translation>使用者認證私鑰</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="103"/>
        <source>User key password</source>
        <translation>用戶金鑰密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="104"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="105"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="107"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="125"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="148"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="152"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="156"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="126"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="127"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="128"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="130"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="131"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="143"/>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="144"/>
        <source>Protected EAP (PEAP)</source>
        <translation>保護EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="149"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="153"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="157"/>
        <source>Choose from file</source>
        <translation>選擇檔案</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="89"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加Wifi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="90"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="91"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="92"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="93"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="94"/>
        <source>Anonymous identity</source>
        <translation>匿名身份</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="95"/>
        <source>Domain</source>
        <translation>域</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="96"/>
        <source>CA certificate</source>
        <translation>CA 證書</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="97"/>
        <source>CA certificate password</source>
        <translation>CA 憑證密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="98"/>
        <source>No CA certificate is required</source>
        <translation>不需要CA認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="99"/>
        <source>Inner authentication</source>
        <translation>內部驗證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="100"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="101"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="102"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="103"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="105"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="123"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="146"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="124"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="125"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="126"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="128"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="129"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="141"/>
        <source>Tunneled TLS</source>
        <translation>隧道TLS</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="142"/>
        <source>Protected EAP (PEAP)</source>
        <translation>保護EAP （PEAP）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="147"/>
        <source>Choose from file</source>
        <translation>選擇檔案</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation>添加無線網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="74"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="75"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="77"/>
        <source>Key</source>
        <translation>鍵</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="78"/>
        <source>WEP index</source>
        <translation>WEP 索引</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="79"/>
        <source>Authentication</source>
        <translation>認證</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="80"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="81"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="83"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="101"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation>WEP 40/128位金鑰（十六進位或ASCII碼）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation>WEP 128位密碼</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation>動態WEP（802.1X）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation>WPA &amp; WPA2 Enterprise</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="115"/>
        <source>1(default)</source>
        <translation>1（預設）</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="121"/>
        <source>Open System</source>
        <translation>開放式系統</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="122"/>
        <source>Shared Key</source>
        <translation>共用金鑰</translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="81"/>
        <source>Add Hidden Wi-Fi</source>
        <translation>加入隱藏Wi-Fi</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="82"/>
        <source>Connection</source>
        <translation>連線設定：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="83"/>
        <source>Wi-Fi name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="84"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="85"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="86"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="87"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="89"/>
        <source>C_reate…</source>
        <translation>新增…</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="112"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="113"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="46"/>
        <source>Create Hotspot</source>
        <translation>創建個人熱點</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="47"/>
        <source>Network name</source>
        <translation>網路名稱：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="48"/>
        <source>Wi-Fi security</source>
        <translation>Wi-Fi 安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="49"/>
        <source>Password</source>
        <translation>密碼：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="50"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="51"/>
        <source>Ok</source>
        <translation>確定</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="54"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="55"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation>WPA 及 WPA2 個人</translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.ui" line="14"/>
        <source>Dialog</source>
        <translation>對話框</translation>
    </message>
</context>
<context>
    <name>FakeDialog</name>
    <message>
        <location filename="../common/fakedialog.cpp" line="63"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>GreeterWindow</name>
    <message>
        <source>Power dialog</source>
        <translation type="vanished">电源对话框</translation>
    </message>
    <message>
        <source>On-screen keyboard, providing virtual keyboard function</source>
        <translation type="vanished">屏幕键盘，提供虚拟键盘功能</translation>
    </message>
    <message>
        <source>Set the desktop environment for the selected user to log in.If the user is logged in, it will take effect after logging in again</source>
        <translation type="vanished">设置选中用户登录后的桌面环境，如果用户已经登录，则会在重新登录后生效</translation>
    </message>
    <message>
        <source>Set the language of the selected user after logging in. If the user is logged in, it will take effect after logging in again.</source>
        <translation type="vanished">设置选中用户登录后的语言，如果用户已登录，则在重新登录后生效</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="158"/>
        <source>LAN</source>
        <translation>有線網路</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="160"/>
        <source>WLAN</source>
        <translation>無線局域網</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="327"/>
        <source>VirtualKeyboard</source>
        <translation>虛擬鍵盤</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="329"/>
        <source>Power</source>
        <translation>電源</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="331"/>
        <source>SwitchUser</source>
        <translation>切換使用者</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="965"/>
        <source>Shut Down</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="968"/>
        <source>Restart</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="971"/>
        <source>Sleep</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="974"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="1507"/>
        <location filename="../greeter/greeterwindow.cpp" line="1914"/>
        <source>Login</source>
        <translation>登錄</translation>
    </message>
</context>
<context>
    <name>GreeterWrapper</name>
    <message>
        <location filename="../greeter/greeterwrapper.cpp" line="160"/>
        <source>failed to start session.</source>
        <translation>啟動工作階段失敗</translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../greeter/iconedit.cpp" line="139"/>
        <source>OK</source>
        <translation>提交</translation>
    </message>
    <message>
        <location filename="../greeter/iconedit.cpp" line="247"/>
        <location filename="../greeter/iconedit.cpp" line="295"/>
        <source>Password: </source>
        <translation>密碼： </translation>
    </message>
    <message>
        <location filename="../greeter/iconedit.cpp" line="249"/>
        <location filename="../greeter/iconedit.cpp" line="297"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">可用有线网络列表</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="292"/>
        <source>New LAN</source>
        <translation>新建有線網路</translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">加入其他网络</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="319"/>
        <source>Hide WiFi</source>
        <translation>加入其他網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="349"/>
        <source>No usable network in the list</source>
        <translation>清單暫無可連接網路</translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>有线网络</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">Wifi</translation>
    </message>
    <message>
        <source>无线网络</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="436"/>
        <source>HotSpot</source>
        <translation>個人熱點</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="445"/>
        <source>FlyMode</source>
        <translation>飛行模式</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="597"/>
        <source>Advanced</source>
        <translation>設置網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="596"/>
        <source>Show KylinNM</source>
        <translation>Show KylinNM</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="283"/>
        <source>Inactivated LAN</source>
        <translation>未啟動</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="307"/>
        <source>Other WLAN</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="413"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="417"/>
        <source>LAN</source>
        <translation>有線網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="423"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="427"/>
        <source>WLAN</source>
        <translation>無線局域網</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1191"/>
        <source>No wireless card detected</source>
        <translation>未檢測到無線網卡</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1228"/>
        <source>Activated LAN</source>
        <translation>已啟動</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1301"/>
        <source>Activated WLAN</source>
        <translation>已啟動</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1339"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1434"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1593"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2275"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2366"/>
        <source>Not connected</source>
        <translation>當前未連接任何網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1342"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1436"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1506"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1507"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1596"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1676"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1829"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2277"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2368"/>
        <source>Disconnected</source>
        <translation>未連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1487"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1653"/>
        <source>NetOn,</source>
        <translation>網路開啟</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1532"/>
        <source>No Other Wired Network Scheme</source>
        <translation>清單中無其他有線網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1698"/>
        <source>No Other Wireless Network Scheme</source>
        <translation>未檢測到其他無線網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2184"/>
        <source>Wired net is disconnected</source>
        <translation>斷開有線網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2214"/>
        <source>Wi-Fi is disconnected</source>
        <translation>斷開無線網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2551"/>
        <source>Conn Ethernet Success</source>
        <translation>連接有線網路成功</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2571"/>
        <source>Conn Ethernet Fail</source>
        <translation>連接有線網路失敗</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2596"/>
        <source>Conn Wifi Success</source>
        <translation>連接無線網路成功</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2615"/>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation>請確認Wi-Fi密碼或無線設備</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.ui" line="14"/>
        <source>kylin-nm</source>
        <translation>網路工具</translation>
    </message>
</context>
<context>
    <name>LettersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="140"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="154"/>
        <source>123</source>
        <translation>123</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="168"/>
        <source>Ctrl</source>
        <translation>Ctrl</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/letterswidget.cpp" line="196"/>
        <source>Alt</source>
        <translation>Alt</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="70"/>
        <location filename="../greeter/loginoptionswidget.cpp" line="93"/>
        <source>Login Options</source>
        <translation>登錄選項</translation>
    </message>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="312"/>
        <source>Password</source>
        <translation>密碼</translation>
    </message>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="338"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="748"/>
        <source>Identify device removed!</source>
        <translation>檢查裝置已移除！</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>logged in</source>
        <translation type="vanished">已登录</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="852"/>
        <source>login</source>
        <translation>登錄</translation>
    </message>
    <message>
        <source>Incorrect user name, please input again</source>
        <translation type="vanished">用户名不正确，请重新输入</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1271"/>
        <source>Retry</source>
        <translation>重試</translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1064"/>
        <source>Password: </source>
        <translation>密碼： </translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1207"/>
        <source>User name input error!</source>
        <translation>使用者名稱輸入錯誤！</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1210"/>
        <location filename="../greeter/loginwindow.cpp" line="1215"/>
        <source>Authentication failure, Please try again</source>
        <translation>認證失敗，請重試</translation>
    </message>
    <message>
        <source>Biometric/code scanning authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1976"/>
        <location filename="../greeter/loginwindow.cpp" line="1977"/>
        <location filename="../greeter/loginwindow.cpp" line="2054"/>
        <location filename="../greeter/loginwindow.cpp" line="2055"/>
        <source>Please try again in %1 minutes.</source>
        <translation>請%1分鐘后再試</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1986"/>
        <location filename="../greeter/loginwindow.cpp" line="1987"/>
        <location filename="../greeter/loginwindow.cpp" line="2065"/>
        <location filename="../greeter/loginwindow.cpp" line="2066"/>
        <source>Please try again in %1 seconds.</source>
        <translation>請%1秒後再試</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1996"/>
        <location filename="../greeter/loginwindow.cpp" line="1997"/>
        <location filename="../greeter/loginwindow.cpp" line="2075"/>
        <location filename="../greeter/loginwindow.cpp" line="2076"/>
        <source>Account locked permanently.</source>
        <translation>帳號已被永久鎖定</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1133"/>
        <source>Password cannot be empty</source>
        <translation>密碼不能為空</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="204"/>
        <location filename="../greeter/loginwindow.cpp" line="285"/>
        <source>Enter the ukey password</source>
        <translation>輸入安全金鑰密碼</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="223"/>
        <location filename="../greeter/loginwindow.cpp" line="289"/>
        <source>Insert the ukey into the USB port</source>
        <translation>請將安全金鑰插入USB埠</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="510"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation>驗證人臉識別或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="515"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation>按壓指紋或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="520"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation>驗證聲紋或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="525"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation>驗證指靜脈或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="530"/>
        <source>Verify iris or enter password to unlock</source>
        <translation>驗證虹膜或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1068"/>
        <source>Input Password</source>
        <translation>輸入密碼</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1557"/>
        <location filename="../greeter/loginwindow.cpp" line="1719"/>
        <location filename="../greeter/loginwindow.cpp" line="1857"/>
        <location filename="../greeter/loginwindow.cpp" line="2218"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation>驗證%1失敗，請輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1559"/>
        <location filename="../greeter/loginwindow.cpp" line="1721"/>
        <location filename="../greeter/loginwindow.cpp" line="1861"/>
        <location filename="../greeter/loginwindow.cpp" line="1863"/>
        <location filename="../greeter/loginwindow.cpp" line="2220"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation>無法驗證%1，請輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1892"/>
        <source>Abnormal network</source>
        <translation>網路異常</translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1877"/>
        <location filename="../greeter/loginwindow.cpp" line="1881"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation>驗證%1失敗，您還有%2次嘗試機會</translation>
    </message>
    <message>
        <source>Incorrect password, please input again</source>
        <translation type="obsolete">密码错误，请重新输入</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="281"/>
        <location filename="../greeter/loginwindow.cpp" line="629"/>
        <location filename="../greeter/loginwindow.cpp" line="836"/>
        <location filename="../greeter/loginwindow.cpp" line="1262"/>
        <source>Login</source>
        <translation>登錄</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="535"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation>使用綁定的微信掃碼或輸入密碼解鎖</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1073"/>
        <source>Username</source>
        <translation>使用者名</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../display-switch/mainwindow.ui" line="14"/>
        <location filename="../display-switch/ui_mainwindow.h" line="127"/>
        <source>MainWindow</source>
        <translation>主視窗</translation>
    </message>
</context>
<context>
    <name>NumbersWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="143"/>
        <source>&amp;&amp;?!</source>
        <translation>&amp;&amp;?!</translation>
    </message>
    <message>
        <location filename="../VirtualKeyboard/src/numberswidget.cpp" line="157"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="157"/>
        <source>Automatically join the network</source>
        <translation>自動加入該網路</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="42"/>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="43"/>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="44"/>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="46"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="45"/>
        <source>Disconnect</source>
        <translation>斷開連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="47"/>
        <source>Input Password...</source>
        <translation>輸入密碼...</translation>
    </message>
    <message>
        <source>自动连接</source>
        <translation type="vanished">自动连接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="423"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation>連接到隱藏Wi-Fi網路</translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="obsolete">速率</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="556"/>
        <source>None</source>
        <translation>無</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="558"/>
        <source>WiFi Security：</source>
        <translation>WiFi安全性：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="559"/>
        <source>Signal：</source>
        <translation>信號強度：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="560"/>
        <source>MAC：</source>
        <translation>物理位址：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="837"/>
        <source>Conn Wifi Failed</source>
        <translation>連接無線網路失敗</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation>類型</translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="31"/>
        <location filename="../kylin-nm/src/onelancform.cpp" line="32"/>
        <source>Connect</source>
        <translation>連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="34"/>
        <source>Disconnect</source>
        <translation>斷開連接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="289"/>
        <location filename="../kylin-nm/src/onelancform.cpp" line="293"/>
        <source>No Configuration</source>
        <translation>未配置</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="296"/>
        <source>IPv4：</source>
        <translation>IPv4位址：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="297"/>
        <source>IPv6：</source>
        <translation>IPv6位址：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="298"/>
        <source>BandWidth：</source>
        <translation>頻寬：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="299"/>
        <source>MAC：</source>
        <translation>物理位址：</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation>表</translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="126"/>
        <source>Close all apps, turn off your computer, and then turn your computer back on</source>
        <translation>關閉所有應用，關閉電腦，然後重新打開電腦。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="128"/>
        <source>Close all apps, and then shut down your computer</source>
        <translation>關閉所有應用，然後關閉電腦。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="130"/>
        <source>The computer stays on, but consumes less power. The app stays open and can quickly wake up and revert to where you left off</source>
        <translation>電腦保持開機狀態，但耗電較少。 應用會一直保持打開狀態，可快速喚醒電腦並恢復到你離開的狀態。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="133"/>
        <source>Turn off your computer, but the app stays open. When the computer is turned on, it can be restored to the state you left</source>
        <translation>關閉電腦，但是應用會一直保持打開狀態。 當打開電腦時，可以恢復到你離開的狀態。</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="385"/>
        <source>Switch User</source>
        <translation>切換使用者</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="386"/>
        <location filename="../greeter/powerwindow.cpp" line="441"/>
        <source>Restart</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="459"/>
        <source>Shut Down</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="478"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="499"/>
        <source>Suspend</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="387"/>
        <source>Power Off</source>
        <translation>關機</translation>
    </message>
</context>
<context>
    <name>Surewidget</name>
    <message>
        <location filename="../greeter/surewidget.ui" line="14"/>
        <source>Form</source>
        <translation>表</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="56"/>
        <location filename="../greeter/surewidget.ui" line="66"/>
        <location filename="../greeter/surewidget.ui" line="79"/>
        <source>TextLabel</source>
        <translation>文本標籤</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="130"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="143"/>
        <source>Confirm</source>
        <translation>確認</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="19"/>
        <location filename="../greeter/surewidget.cpp" line="73"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation>同時有多個使用者登錄系統，您確定要退出系統嗎？</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="38"/>
        <location filename="../greeter/surewidget.cpp" line="66"/>
        <source>There&apos;s a program running, this process cannot be interrupted.</source>
        <translation>有程式正在運行，此過程不能被打斷。</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="41"/>
        <source>The system will automatically %1 after the program is executed. Do not shut down the computer.</source>
        <translation>系統將在執行完安裝程式後自動%1，請勿關閉電腦。</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="70"/>
        <source>Click OK and the system will automatically %1 after the program is executed.</source>
        <translation>點擊確認系統將在程式執行完畢後自動%1</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="109"/>
        <source>Shut Down</source>
        <translation>關機</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="112"/>
        <source>Restart</source>
        <translation>重啟</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="115"/>
        <source>Sleep</source>
        <translation>睡眠</translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="118"/>
        <source>Hibernate</source>
        <translation>休眠</translation>
    </message>
</context>
<context>
    <name>UsersModel</name>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="58"/>
        <source>Guest Session</source>
        <translation>遊客登錄</translation>
    </message>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="48"/>
        <location filename="../greeter/usersmodel.cpp" line="50"/>
        <source>Guest</source>
        <translation>遊客登錄</translation>
    </message>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="76"/>
        <location filename="../greeter/usersmodel.cpp" line="78"/>
        <location filename="../greeter/usersmodel.cpp" line="85"/>
        <source>Login</source>
        <translation>登錄</translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../kylin-nm/src/utils.cpp" line="89"/>
        <source>kylin network applet desktop message</source>
        <translation>麒麟網路小程式桌面消息</translation>
    </message>
</context>
</TS>
