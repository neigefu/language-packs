<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="14"/>
        <source>Connect to Sever</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="32"/>
        <source>Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="39"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="55"/>
        <source>Save Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="62"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="82"/>
        <source>Anonymous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="35"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DiscControl</name>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="451"/>
        <source> is busy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="490"/>
        <source>is busy!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="537"/>
        <source> not support udf at present.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="544"/>
        <source>unmount disc failed before udf format.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="587"/>
        <source>is not properly formatted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="592"/>
        <source>Can not found newfs_udf tool.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccontrol.cpp" line="623"/>
        <source>preparation failed before DVD-RW udf format.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileLabelModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="37"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="56"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="38"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="57"/>
        <source>Orange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="39"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="58"/>
        <source>Yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="40"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="59"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="41"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="60"/>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="42"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="61"/>
        <source>Purple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="43"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="62"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="vanished">无颜色</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="129"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="357"/>
        <source>Label or color is duplicated.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Format_Dialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="32"/>
        <source>rom_size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="45"/>
        <source>system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="59"/>
        <source>vfat/fat32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="64"/>
        <source>exfat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="69"/>
        <source>ntfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>vfat</source>
        <translation type="vanished">VFAT</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="74"/>
        <source>ext4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="88"/>
        <source>device_name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="114"/>
        <source>clean it total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="127"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="140"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="179"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>qmesg_notify</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Format operation has been finished successfully.</source>
        <translation type="vanished">格式化操作已成功完成。</translation>
    </message>
    <message>
        <source>Sorry, the format operation is failed!</source>
        <translation type="vanished">很遗憾，格式化操作失败了，您可以重新试下！</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="vanished">格式化此卷将清除其上的所有数据。请在格式化之前备份所有保留的数据。您想继续吗?</translation>
    </message>
    <message>
        <source>format</source>
        <translation type="vanished">格式化</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation type="vanished">开始</translation>
    </message>
    <message>
        <source>format_success</source>
        <translation type="vanished">格式化成功!</translation>
    </message>
    <message>
        <source>format_err</source>
        <translation type="vanished">格式化失败!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="141"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="154"/>
        <source>Rom size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="160"/>
        <source>Filesystem:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="170"/>
        <source>Disk name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="200"/>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="215"/>
        <source>Set password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="216"/>
        <source>Set password for volume based on LUKS (only ext4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="229"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="230"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="337"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="507"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="507"/>
        <source>Device name cannot start with a decimal point, Please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="518"/>
        <source>Enter Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="535"/>
        <source>Password too short, please retype a password more than 6 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1183"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1183"/>
        <source>Block not existed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1229"/>
        <source>Formatting. Do not close this window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="44"/>
        <source>Renaming &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="45"/>
        <source>Renaming failed, the reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="45"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="52"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="59"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="64"/>
        <source>Filename too long</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="49"/>
        <source>Copying &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="51"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="58"/>
        <source>To &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="52"/>
        <source>Copying failed, the reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="56"/>
        <source>Moving &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="59"/>
        <source>Moving failed, the reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="63"/>
        <source>File operation error:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="64"/>
        <source>The reason is: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="96"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="122"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="94"/>
        <source>Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="95"/>
        <source>Skip All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="97"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="112"/>
        <source>Please enter a new name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-dialog/kyfiledialogrename.cpp" line="123"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="382"/>
        <source>File operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="421"/>
        <source>starting ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="398"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="526"/>
        <source>cancel all file operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="399"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="527"/>
        <source>Are you sure want to cancel all file operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="401"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="529"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="402"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="530"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="616"/>
        <source>canceling ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="619"/>
        <source>sync ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OtherButton</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="717"/>
        <source>Other queue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::AdvanceSearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="55"/>
        <source>Key Words</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="58"/>
        <source>input key words...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="59"/>
        <source>Search Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="61"/>
        <source>choose search path...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="68"/>
        <source>browse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="69"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="71"/>
        <source>Choose File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="76"/>
        <source>Modify Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="78"/>
        <source>Choose Modify Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="83"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="85"/>
        <source>Choose file size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="90"/>
        <source>show hidden file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="91"/>
        <source>go back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="92"/>
        <source>hidden advance search page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="94"/>
        <source>file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="95"/>
        <source>content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="100"/>
        <source>search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="101"/>
        <source>start search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="174"/>
        <source>Select path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="193"/>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="202"/>
        <source>Operate Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="194"/>
        <source>Have no key words or search location!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="203"/>
        <source>Search file name or content at least choose one!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search content or file name at least choose one!</source>
        <translation type="vanished">搜索文件名或者内容请至少指定一个！</translation>
    </message>
    <message>
        <source>all</source>
        <translation type="vanished">全部</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation type="vanished">文件夹</translation>
    </message>
    <message>
        <source>image</source>
        <translation type="vanished">图片</translation>
    </message>
    <message>
        <source>video</source>
        <translation type="vanished">视频</translation>
    </message>
    <message>
        <source>text file</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">音频</translation>
    </message>
    <message>
        <source>others</source>
        <translation type="vanished">其它</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation type="vanished">WPS文件</translation>
    </message>
    <message>
        <source>today</source>
        <translation type="vanished">今天</translation>
    </message>
    <message>
        <source>this week</source>
        <translation type="vanished">本周</translation>
    </message>
    <message>
        <source>this month</source>
        <translation type="vanished">本月</translation>
    </message>
    <message>
        <source>this year</source>
        <translation type="vanished">今年</translation>
    </message>
    <message>
        <source>year ago</source>
        <translation type="vanished">去年</translation>
    </message>
    <message>
        <source>tiny(0-16K)</source>
        <translation type="vanished">极小(0-16K)</translation>
    </message>
    <message>
        <source>small(16k-1M)</source>
        <translation type="vanished">较小(16k-1M)</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">较大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedLocationBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/advanced-location-bar.cpp" line="180"/>
        <source>Search Content...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::AllFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="341"/>
        <source>Choose new application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="343"/>
        <source>Choose an Application to open this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="350"/>
        <source>apply now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="356"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="357"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::AudioPlayManager</name>
    <message>
        <source>Operation file Warning</source>
        <translation type="vanished">文件操作警告</translation>
    </message>
</context>
<context>
    <name>Peony::BasicPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="924"/>
        <source>Choose a custom icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="390"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">路径：</translation>
    </message>
    <message>
        <source>Overview:</source>
        <translation type="vanished">概览：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="241"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="242"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="296"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="297"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="339"/>
        <source>move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="396"/>
        <source>symbolLink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="400"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="407"/>
        <source>Include:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="411"/>
        <source>Open with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="417"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="429"/>
        <source>Select multiple files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="434"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total size:</source>
        <translation type="vanished">实际大小：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="435"/>
        <source>Space Useage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation type="vanished">yyyy年MM月dd日, HH:mm:ss</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="vanished">yyyy年MM月dd日, hh:mm:ss AP</translation>
    </message>
    <message>
        <source>Time Created:</source>
        <translation type="vanished">创建时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="177"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="470"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="476"/>
        <source>Time Modified:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="471"/>
        <source>Time Access:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="499"/>
        <source>Readonly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="500"/>
        <source>Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="521"/>
        <source>Property:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="610"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="716"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="965"/>
        <source>%1 (%2 Bytes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="753"/>
        <source>Choose a new folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="760"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="765"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="760"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="765"/>
        <source>cannot move a folder to itself !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="958"/>
        <source>%1 Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 KB (%2 Bytes)</source>
        <translation type="vanished">%1 KB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 MB (%2 Bytes)</source>
        <translation type="vanished">%1 MB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 GB (%2 Bytes)</source>
        <translation type="vanished">%1 GB (%2 字节)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="974"/>
        <source>%1 files, %2 folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1062"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1064"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1069"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1071"/>
        <source>Can&apos;t get remote file information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 files (include root files), %2 hidden</source>
        <translation type="vanished">共%1个文件（包括顶层目录），有%2个隐藏文件</translation>
    </message>
    <message>
        <source>%1 total</source>
        <translation type="vanished">共%1</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="93"/>
        <source>CPU Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="94"/>
        <source>CPU Core:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="95"/>
        <source>Memory Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="107"/>
        <source>User Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="108"/>
        <source>Desktop: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="116"/>
        <source>You should mount this volume first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="221"/>
        <source>Name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <source>File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="130"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="226"/>
        <source>Total Space: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="131"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="227"/>
        <source>Used Space: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="132"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="228"/>
        <source>Free Space: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="133"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="229"/>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="240"/>
        <source>Kylin Burner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="246"/>
        <source>Open with: 	</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="253"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="140"/>
        <source>connect to server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="165"/>
        <source>ip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="167"/>
        <source>port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="168"/>
        <source>type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="198"/>
        <source>Personal Collection server:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="211"/>
        <source>add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="212"/>
        <source>delete</source>
        <translatorcomment>连接</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="213"/>
        <source>connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="290"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="294"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="362"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="366"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="290"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="362"/>
        <source>ip input error, please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="294"/>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="366"/>
        <source>port input error, please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerLogin</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="455"/>
        <source>The login user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="465"/>
        <source>Please enter the %1&apos;s user name and password of the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="474"/>
        <source>User&apos;s identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="475"/>
        <source>guest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="476"/>
        <source>Registered users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="493"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="494"/>
        <source>password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="495"/>
        <source>Remember the password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="515"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="516"/>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::CreateLinkInternalPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="115"/>
        <source>Create Link to Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="141"/>
        <source>Create Link to...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="144"/>
        <source>Choose a Directory to Create Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Peony-Qt Create Link Extension</source>
        <translation type="vanished">创建链接</translation>
    </message>
    <message>
        <source>Create Link Menu Extension.</source>
        <translation type="vanished">创建链接.</translation>
    </message>
</context>
<context>
    <name>Peony::CreateSharedFileLinkMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="233"/>
        <source>Create Link to Desktop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::CreateTemplateOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="62"/>
        <source>NewFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="78"/>
        <source>Create file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="90"/>
        <source>NewFolder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="109"/>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="142"/>
        <source>Create file error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::CustomErrorHandler</name>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="40"/>
        <source>Is Error Handled?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="44"/>
        <source>Error not be handled correctly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DefaultOpenWithWidget</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="400"/>
        <source>No default app</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="71"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="186"/>
        <source>Select the file you want to preview...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="177"/>
        <source>Can not preview this file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not preivew this file.</source>
        <translation type="vanished">不能预览该文件</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPageFactory</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="50"/>
        <source>Default Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="53"/>
        <source>This is the Default Preview of peony-qt</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DetailsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="174"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="177"/>
        <source>File type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="193"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="207"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="214"/>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="199"/>
        <source>Create time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="204"/>
        <source>Modify time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="212"/>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="221"/>
        <source>File size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="228"/>
        <source>Width:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="231"/>
        <source>Height:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="239"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="240"/>
        <source>Owner:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="242"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="243"/>
        <source>Computer:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="288"/>
        <source>%1 (this computer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="295"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="324"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="325"/>
        <source>Can&apos;t get remote file information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="334"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="335"/>
        <source>%1 px</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="299"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="299"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView2</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="535"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="535"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView2</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewFactoryManager</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewMenu</name>
    <message>
        <source>Open in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="314"/>
        <source>Add to bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">打开“%1”(&amp;O)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开“%1”(&amp;N)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开“%1”(&amp;T)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">选用其它应用打开“%1”...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">更多应用...(&amp;M)</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">打开(&amp;O)</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">打开%1个选中文件(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">新建...(&amp;N)</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">空文件(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">文件夹(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="606"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <source>View Type...</source>
        <translation type="vanished">视图类型...</translation>
    </message>
    <message>
        <source>Sort By...</source>
        <translation type="vanished">排序类型...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="658"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="660"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="661"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">新建...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="278"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="367"/>
        <source>Open in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="287"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="376"/>
        <source>Open in New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="334"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="384"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="439"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="345"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="396"/>
        <source>Open with...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="360"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="433"/>
        <source>More applications...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="448"/>
        <source>Open %1 selected files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="496"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="590"/>
        <source>Empty File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="602"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="632"/>
        <source>View Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="652"/>
        <source>Sort By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="659"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="662"/>
        <source>Orignal Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">排序顺序...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="695"/>
        <source>Ascending Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="694"/>
        <source>Descending Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort Preferences...</source>
        <translation type="vanished">排序偏好...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="713"/>
        <source>Folder First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="722"/>
        <source>Chinese First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="731"/>
        <source>Show Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="766"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="774"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="893"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="935"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1125"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1185"/>
        <source>File:&quot;%1&quot; is not exist, did you moved or deleted it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1205"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1215"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1205"/>
        <source>Peony File Labels Menu Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">复制(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="788"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1130"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="812"/>
        <source>Delete to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="863"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="906"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="917"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="970"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1007"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1045"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1094"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="824"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="887"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1113"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="687"/>
        <source>Sort Order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="708"/>
        <source>Sort Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1184"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File:&quot;%1 is not exist, did you moved or deleted it?</source>
        <translation type="vanished">文件：&quot;%1&quot; 不存在，您是否已经移动或者删除了它？</translation>
    </message>
    <message>
        <source>File original path not exist, are you deleted or moved it?</source>
        <translation type="vanished">文件原始路径未找到，您是否已经移动或删除了它？</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">剪切(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">删除到回收站(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="827"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="838"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="846"/>
        <source>Delete forever</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="854"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">全选(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="926"/>
        <source>Reverse Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">重命名(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">刷新(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1070"/>
        <source>&amp;Clean the Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1117"/>
        <source>Delete Permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1117"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">还原(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1138"/>
        <source>Clean All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1155"/>
        <source>Open Parent Folder in New Window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewWidget</name>
    <message>
        <source>Directory View</source>
        <translation type="vanished">文件视图</translation>
    </message>
</context>
<context>
    <name>Peony::FMWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="92"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="170"/>
        <source>advanced search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="173"/>
        <source>clear record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="278"/>
        <source>Loaing... Press Esc to stop a loading.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="394"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <comment>Show|Hidden</comment>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="323"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="330"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="393"/>
        <source>Peony Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="447"/>
        <source>New Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileCopy</name>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="148"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="156"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="167"/>
        <source>Error in source or destination file path!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="193"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="220"/>
        <source>Vfat/FAT32 file systems do not support a single file that occupies more than 4 GB space!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error opening source or destination file!</source>
        <translation type="vanished">打开源文件或者目标文件出错！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="317"/>
        <source>Please check whether the device has been removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="319"/>
        <source>Write file error: There is no avaliable disk space for device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please confirm that the device controls are insufficient!</source>
        <translation type="vanished">请确认设备空间是否足够!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="373"/>
        <source>File opening failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reading and Writing files are inconsistent!</source>
        <translation type="vanished">读和写文件不一致！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="334"/>
        <source>operation cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileCopyOperation</name>
    <message>
        <source>File copy</source>
        <translation type="vanished">文件复制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="166"/>
        <source>Create folder %1 failed: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="170"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="385"/>
        <source>File copy error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="393"/>
        <source>Cannot opening file, permission denied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="395"/>
        <source>File:%1 was not found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileDeleteOperation</name>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="78"/>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="104"/>
        <source>File delete error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="142"/>
        <source>Delete file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="145"/>
        <source>Invalid Operation! Can not delete &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileEnumerator</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="563"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Did not find target path, do you move or deleted it?</source>
        <translation type="vanished">未找到目标路径，您是否已经移动或删除了它？</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="291"/>
        <source>data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileInfoJob</name>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="268"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="270"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="272"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="274"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileInformationLabel</name>
    <message>
        <source>File location:</source>
        <translation type="vanished">文件位置：</translation>
    </message>
    <message>
        <source>File size:</source>
        <translation type="vanished">文件大小：</translation>
    </message>
    <message>
        <source>Modify time:</source>
        <translation type="vanished">修改时间：</translation>
    </message>
</context>
<context>
    <name>Peony::FileItem</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="232"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="291"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="303"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="308"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="279"/>
        <source>Open Link failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="280"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="292"/>
        <source>Can not open path &quot;%1&quot;，permission denied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="302"/>
        <source>Can not find path &quot;%1&quot;，are you moved or renamed it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not find path &quot;%1&quot; .</source>
        <translation type="vanished">找不到路径: &quot;%1&quot; 。</translation>
    </message>
</context>
<context>
    <name>Peony::FileItemModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="312"/>
        <source>child(ren)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="300"/>
        <source>Symbol Link, </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="346"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="350"/>
        <source>Delete Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="355"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="357"/>
        <source>Original Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="353"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="351"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileLabelInternalMenuPlugin</name>
    <message>
        <source>Add File Label...</source>
        <translation type="vanished">添加标记...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="181"/>
        <source>Add File Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="204"/>
        <source>Delete All Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Peony File Labels Menu Extension</source>
        <translation type="vanished">文件标记</translation>
    </message>
    <message>
        <source>Tag a File with Menu.</source>
        <translation type="vanished">菜单中增加标记功能.</translation>
    </message>
</context>
<context>
    <name>Peony::FileLauchDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="47"/>
        <source>Applications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="48"/>
        <source>Choose an Application to open this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="56"/>
        <source>Set as Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="64"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="65"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileLaunchAction</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="251"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="444"/>
        <source>Execute Directly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="145"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="252"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="445"/>
        <source>Execute in Terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="148"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="256"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="449"/>
        <source>Detected launching an executable file %1, you want?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="289"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="482"/>
        <source>Open Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <source>Can not open %1, file not exist, is it deleted?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File not exist, is it deleted or moved to other path?</source>
        <translation type="vanished">文件不存在，您是否已将其删除或挪动位置？</translation>
    </message>
    <message>
        <source>Can not open %1</source>
        <translation type="vanished">不能打开%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="250"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="443"/>
        <source>By Default App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="255"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="448"/>
        <source>Launch Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="279"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="472"/>
        <source>Open Link failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="280"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="473"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="290"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="483"/>
        <source>Can not open %1, Please confirm you have the right authority.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="294"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="487"/>
        <source>Open App failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="295"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="488"/>
        <source>The linked app is changed or uninstalled, so it can not work correctly. 
Do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="305"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="498"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="306"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="498"/>
        <source>Can not get a default application for opening %1, do you want open it with text format?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="40"/>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="43"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="77"/>
        <source>Link file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Link file</source>
        <translation type="vanished">创建文件链接</translation>
    </message>
</context>
<context>
    <name>Peony::FileMoveOperation</name>
    <message>
        <source>Invalid move operation, cannot move a file itself.</source>
        <translation type="vanished">非法的移动操作，不能自移动到自身。</translation>
    </message>
    <message>
        <source>Move file</source>
        <translation type="vanished">文件移动</translation>
    </message>
    <message>
        <source>Create file</source>
        <translation type="vanished">文件创建</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="297"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="417"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="668"/>
        <source>Move file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="837"/>
        <source>Create file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="843"/>
        <source>Cannot opening file, permission denied!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="845"/>
        <source>File:%1 was not found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1135"/>
        <source>Invalid Operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1150"/>
        <source>File delete error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1152"/>
        <source>Invalid Operation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationAfterProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="362"/>
        <source>&amp;More Details</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="45"/>
        <source>File Operation Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="53"/>
        <source>unkwon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="55"/>
        <source>null</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="57"/>
        <source>Error message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="58"/>
        <source>Source File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="59"/>
        <source>Dest File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="63"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="64"/>
        <source>Ignore All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="65"/>
        <source>Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="66"/>
        <source>Overwrite All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="67"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="68"/>
        <source>Backup All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="69"/>
        <source>&amp;Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="70"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogConflict</name>
    <message>
        <source>This location already contains a file with the same name.</source>
        <translation type="vanished">目标文件夹里已经包含有同名文件</translation>
    </message>
    <message>
        <source>Please select the file to keep</source>
        <translation type="vanished">请选择要保留的文件</translation>
    </message>
    <message>
        <source>This location already contains the file,</source>
        <translation type="vanished">这里已包含此文件</translation>
    </message>
    <message>
        <source>Do you want to override it?</source>
        <translation type="vanished">你确定要覆盖它吗</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="45"/>
        <source>Replace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="54"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="72"/>
        <source>Do the same</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="95"/>
        <source>&lt;p&gt;This location already contains the file &apos;%1&apos;, Do you want to override it?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="101"/>
        <source>Unexpected error from %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Then do the same thing in a similar situation</source>
        <translation type="vanished">之后类似情况执行相同操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="63"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogNotSupported</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="300"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="292"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Do the same</source>
        <translation type="vanished">全部应用</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="337"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogWarning</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="193"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="201"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="226"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please make sure the disk is not full or not is write protected, or file is not being used.</source>
        <translation type="vanished">请确保磁盘未满或未被写保护或未被使用。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="750"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="752"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> - Symbolic Link</source>
        <translation type="vanished">-快捷方式</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationManager</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="190"/>
        <source>Warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="190"/>
        <source>&apos;%1&apos; is occupied，you cannot operate!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No, go to settings</source>
        <translation type="vanished">否，跳转到设置</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="209"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="213"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="218"/>
        <source>Do you want to put selected %1 item(s) into trash?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="220"/>
        <source>Do not show again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="328"/>
        <source>Can&apos;t delete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="329"/>
        <source>You can&apos;t delete a file whenthe file is doing another operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="406"/>
        <source>File Operation is Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="407"/>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done. If you really want to execute file operations parallelly anyway, you can change the default option &quot;Allow Parallel&quot; in option menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The system cannot hibernate or sleep</source>
        <translation type="vanished">无法进入休眠或睡眠模式</translation>
    </message>
    <message>
        <source>The file operation is in progress.                                         Ensure that the file operation is complete or canceled before hibernating or sleeping</source>
        <translation type="vanished">文件操作进行中,\
进入休眠或者睡眠之前，请先确保文件操作已完成或者取消</translation>
    </message>
    <message>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done.</source>
        <translation type="vanished">在执行该操作之前有操作未完成，它需要等待上一个操作完成后再执行。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationPreparePage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="298"/>
        <source>counting:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="299"/>
        <source>state:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="321"/>
        <source>&amp;More Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="332"/>
        <source>From:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="333"/>
        <source>To:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressWizard</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="55"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="59"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="68"/>
        <source>Preparing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="71"/>
        <source>Handling...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="74"/>
        <source>Clearing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="77"/>
        <source>Rollbacking...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="81"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="94"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="120"/>
        <source>File Operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="95"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="121"/>
        <source>A file operation is running backend...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="160"/>
        <source>%1 files, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="260"/>
        <source>%1 done, %2 total, %3 of %4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="203"/>
        <source>clearing: %1, %2 of %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="248"/>
        <source>copying...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="278"/>
        <source>Syncing...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FilePreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="218"/>
        <source>File Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="223"/>
        <source>File Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="227"/>
        <source>Time Access:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="231"/>
        <source>Time Modified:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="237"/>
        <source>Children Count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="242"/>
        <source>Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="247"/>
        <source>Image resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="251"/>
        <source>color model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="312"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">图片尺寸：</translation>
    </message>
    <message>
        <source>Image format:</source>
        <translation type="vanished">图片格式：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="343"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="344"/>
        <source>%1x%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="396"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="397"/>
        <source>%1 total, %2 hidden</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameDialog</name>
    <message>
        <source>Names automatically add serial Numbers (e.g., 1,2,3...)</source>
        <translation type="vanished">名称后自动添加序号（如:1,2,3...）</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>New file name</source>
        <translation type="vanished">文件名</translation>
    </message>
    <message>
        <source>Please enter the file name</source>
        <translation type="vanished">请输入文件名</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameOperation</name>
    <message>
        <source>Rename file</source>
        <translation type="vanished">文件重命名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="74"/>
        <source>File Rename error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="75"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="91"/>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or rsort!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者排序后将会被隐藏!</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden!</source>
        <translation type="vanished">文件%1%2%3将被隐藏!</translation>
    </message>
    <message>
        <source>Invalid file name &quot;%1&quot; </source>
        <translation type="vanished">文件名 &quot;%1&quot; 不合法</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="90"/>
        <source>File Rename warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The file &quot;%1&quot; will be hidden!</source>
        <translation type="vanished">文件 &quot;%1&quot; 将会被隐藏！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="196"/>
        <source>Rename file error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileTrashOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="68"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="91"/>
        <source>trash:///</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="71"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="94"/>
        <source>Trash file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="74"/>
        <source>Invalid Operation! Can not trash &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="102"/>
        <source>Can not trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="103"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%1&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %1 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="171"/>
        <source>Can not trash this file, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not trash %1, would you like to delete this file permanently?</source>
        <translation type="vanished">不能回收%1, 是否永久删除?</translation>
    </message>
    <message>
        <source>. Are you sure you want to permanently delete the file</source>
        <translation type="vanished">，你确定要永久删除文件吗？</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%s&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %s 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <source>Trash file</source>
        <translation type="vanished">删除文件到回收站</translation>
    </message>
</context>
<context>
    <name>Peony::FileUntrashOperation</name>
    <message>
        <source>Untrash file</source>
        <translation type="vanished">撤销删除的文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-untrash-operation.cpp" line="157"/>
        <source>Untrash file error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::GlobalSettings</name>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="76"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="372"/>
        <source>yyyy/MM/dd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="77"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="365"/>
        <source>HH:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="362"/>
        <source>AP hh:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="375"/>
        <source>yyyy-MM-dd</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Peony::LocationBar</name>
    <message>
        <source>click the blank area for edit</source>
        <translation type="vanished">点击空白区域编辑路径</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="obsolete">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="355"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">文件系统</translation>
    </message>
    <message>
        <source>&amp;Copy Directory</source>
        <translation type="vanished">拷贝路径(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="452"/>
        <source>Open In New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="456"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="450"/>
        <source>Copy Directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::MountOperation</name>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="90"/>
        <source>Operation Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="187"/>
        <source>Login failed, unknown username or password error, please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::NavigationToolBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="35"/>
        <source>Go Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="39"/>
        <source>Go Forward</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="43"/>
        <source>History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="73"/>
        <source>Clear History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="88"/>
        <source>Cd Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="94"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::NewFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="217"/>
        <source>Choose new application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="219"/>
        <source>Choose an Application to open this file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="226"/>
        <source>apply now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="232"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="233"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::OpenWithPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="93"/>
        <source>How do you want to open %1%2 files ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="98"/>
        <source>Default open with:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="117"/>
        <source>Other:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="158"/>
        <source>Choose other application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="174"/>
        <source>Go to application center</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::PathEdit</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/path-bar/path-edit.cpp" line="59"/>
        <source>Go To</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::PermissionsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>User or Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Readable</source>
        <translation type="vanished">可读</translation>
    </message>
    <message>
        <source>Writeable</source>
        <translation type="vanished">可写</translation>
    </message>
    <message>
        <source>Excuteable</source>
        <translation type="vanished">可执行</translation>
    </message>
    <message>
        <source>File: %1</source>
        <translation type="vanished">文件：%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="80"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="157"/>
        <source>Target: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Read and Write</source>
        <translation type="vanished">读写</translation>
    </message>
    <message>
        <source>Readonly</source>
        <translation type="vanished">只读</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="179"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="188"/>
        <source>Can not get the permission info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="249"/>
        <source>(Me)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="318"/>
        <source>Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="321"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="323"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="325"/>
        <source>Other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">其它用户</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="336"/>
        <source>You can not change the access of this file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="340"/>
        <source>Me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="344"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::PropertiesWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="282"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="286"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="294"/>
        <source>Selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="294"/>
        <source> %1 Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="300"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="307"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="314"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="410"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="411"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>Peony::RecentAndTrashPropertiesPage</name>
    <message>
        <source>Show confirm dialog while trashing: </source>
        <translation type="vanished">删除到回收站时弹出确认框:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="117"/>
        <source>Show confirm dialog while trashing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="140"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="146"/>
        <source>Origin Path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="177"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="188"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="197"/>
        <source>Deletion Date: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="167"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="212"/>
        <source>Size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="206"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="213"/>
        <source>Original Location: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="47"/>
        <source>Input the search key of files you would like to find.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="83"/>
        <source>Input search key...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="121"/>
        <source>advance search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="122"/>
        <source>clear record</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SearchBarContainer</name>
    <message>
        <source>Choose File Type</source>
        <translation type="vanished">选择文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="70"/>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="200"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="92"/>
        <source>all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="92"/>
        <source>file folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="92"/>
        <source>image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="93"/>
        <source>video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="93"/>
        <source>text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="93"/>
        <source>audio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="93"/>
        <source>others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="93"/>
        <source>wps file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SharedFileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="44"/>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="47"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFavoriteItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="86"/>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFileSystemItem</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">文件系统</translation>
    </message>
    <message>
        <source>data</source>
        <translation type="vanished">数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="169"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarMenu</name>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="55"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="78"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="104"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="119"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="247"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="297"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="89"/>
        <source>Delete Symbolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="135"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="143"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="291"/>
        <source>Unmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="152"/>
        <source>Eject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete Symbolic</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Unmount</source>
        <translation type="vanished">卸载(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Eject</source>
        <translation type="vanished">弹出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="198"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="215"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarModel</name>
    <message>
        <source>Shared Data</source>
        <translation type="vanished">共享数据</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-model.cpp" line="98"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarPersonalItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-personal-item.cpp" line="42"/>
        <source>Personal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarSeparatorItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-separator-item.h" line="68"/>
        <source>(No Sub Directory)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::StatusBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="94"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="100"/>
        <source>; %1 folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="95"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="102"/>
        <source>; %1 files, %2 total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="97"/>
        <source>; %1 folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="98"/>
        <source>; %1 file, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="105"/>
        <source>%1 selected</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SyncThread</name>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="44"/>
        <source>notify</source>
        <translatorcomment>温馨提示</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ToolBar</name>
    <message>
        <source>Open in new &amp;Window</source>
        <translation type="vanished">在新窗口中打开(&amp;W)</translation>
    </message>
    <message>
        <source>Open in &amp;New window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in new &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="138"/>
        <source>Sort Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="140"/>
        <source>File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="146"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="149"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="143"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="72"/>
        <source>Open in New window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="74"/>
        <source>Open in new Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="160"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="156"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="190"/>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="339"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="193"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="196"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="199"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="216"/>
        <source>Clean Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">永久删除</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">您确定要删除这些文件吗？一旦开始删除，这些文件将不可再恢复。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="221"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="270"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="273"/>
        <source>Forbid Thumbnail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="280"/>
        <source>Show Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="287"/>
        <source>Resident in Backend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="288"/>
        <source>Let the program still run after closing the last window. This will reduce the time for the next launch, but it will also consume resources in backend.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="300"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="306"/>
        <source>&amp;About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="308"/>
        <source>Peony Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="309"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
</context>
<context>
    <name>Peony::UserShareInfoManager</name>
    <message>
        <location filename="../../libpeony-qt/usershare-manager.cpp" line="90"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::VolumeManager</name>
    <message>
        <location filename="../../libpeony-qt/volume-manager.cpp" line="150"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="752"/>
        <source>starting ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="849"/>
        <source>canceling ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="851"/>
        <source>sync ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cancel file operation</source>
        <translation type="vanished">取消文件操作</translation>
    </message>
    <message>
        <source>Are you sure want to cancel the current selected file operation</source>
        <translation type="vanished">你确定要取消当前选中的文件操作</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="40"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="60"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="91"/>
        <source>Icon View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="46"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="97"/>
        <source>Show the folder children as icons.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="42"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="62"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="93"/>
        <source>List View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="48"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="99"/>
        <source>Show the folder children as rows in a list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Basic Preview Page</source>
        <translation type="vanished">基本</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="40"/>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="46"/>
        <source>Show the basic file properties, and allow you to modify the access and name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Permissions Page</source>
        <translation type="vanished">权限</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="41"/>
        <source>Permissions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="47"/>
        <source>Show and modify file&apos;s permission, owner and group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="184"/>
        <source>Can not trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="178"/>
        <source>Can not trash these files. You can delete them permanently. Are you sure doing that?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="182"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="274"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="283"/>
        <source>Do you want to empty the recycle bin and delete the files permanently? Once it has begun there is no way to restore them.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="340"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="342"/>
        <source>Delete Permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="340"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="342"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Computer Properties Page</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="41"/>
        <source>Computer Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="47"/>
        <source>Show the computer properties or items in computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trash and Recent Properties Page</source>
        <translation type="vanished">最近/回收</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="40"/>
        <source>Trash and Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="46"/>
        <source>Show the file properties or items in trash or recent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>eject device failed</source>
        <translation type="vanished">弹出设备失败</translation>
    </message>
    <message>
        <source>Please check whether the device is occupied and then eject the device again</source>
        <translation type="vanished">请检查设备是否正在使用,确认没有使用后再次弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="484"/>
        <source>Format failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="486"/>
        <source>YES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="934"/>
        <source>Formatting successful! But failed to set the device name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="948"/>
        <source>qmesg_notify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="932"/>
        <source>Format operation has been finished successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Formatting successful! Description Failed to set the device name.</source>
        <translation type="vanished">格式化成功！设备名设置失败。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="948"/>
        <source>Sorry, the format operation is failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="961"/>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="932"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="934"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="963"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="965"/>
        <source>begin format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="967"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="41"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="861"/>
        <source>File Manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default search vfs of peony</source>
        <translation type="vanished">默认文件搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1503"/>
        <source>Force unmount failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1503"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="482"/>
        <source>Error: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="142"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1506"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1556"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="136"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1535"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1538"/>
        <source>Unmount failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1523"/>
        <source>Not authorized to perform operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="131"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1535"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1538"/>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="278"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1024"/>
        <source>Failed to activate device: Incorrect passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1034"/>
        <source>The device has been mount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1226"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1259"/>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to eject %1</source>
        <translation type="vanished">无法弹出 %1</translation>
    </message>
    <message>
        <source>PeonyNotify</source>
        <translation type="vanished">文件管理器通知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="123"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1221"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1253"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1523"/>
        <source>Eject failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="261"/>
        <source>favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="294"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="299"/>
        <source>File is not existed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="307"/>
        <source>Share Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="311"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="315"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="361"/>
        <source>Operation not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="455"/>
        <source>The virtual file system does not support folder creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="527"/>
        <source>Can not create a symbolic file for vfs location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="534"/>
        <source>Symbolic Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="546"/>
        <source>Can not create symbolic file here, %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="555"/>
        <source>Can not add a file to favorite directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="613"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="621"/>
        <source>The virtual file system cannot be opened</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="442"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="470"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="485"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="571"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="589"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-register.h" line="43"/>
        <source>Default favorite vfs of peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="38"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="44"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="40"/>
        <source>Mark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="46"/>
        <source>mark this file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="40"/>
        <source>Open With</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="46"/>
        <source>open with.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="11"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="407"/>
        <source>permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="400"/>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="413"/>
        <source>file not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="300"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="317"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="333"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="526"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="157"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="179"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="201"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="208"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="224"/>
        <source>duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error when getting information for file : No target file found</source>
        <translation type="vanished">获取文件信息时出现错误：没有目标文件。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="347"/>
        <source>data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="186"/>
        <location filename="../../libpeony-qt/vfs/search-vfs-uri-parser.cpp" line="110"/>
        <source>Computer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="350"/>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="250"/>
        <source>File System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="255"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="391"/>
        <source>Failed to open file &quot;%1&quot;: insufficient permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="402"/>
        <source>File “%1” does not exist. Please check whether the file has been deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="58"/>
        <source>burn operation has been cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/disc/disccommand.cpp" line="62"/>
        <source> is busy!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UdfFormatDialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="16"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="129"/>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="154"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="29"/>
        <source>Disc Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="37"/>
        <source>Device Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="51"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="53"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="61"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="95"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="95"/>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="129"/>
        <source>Format operation has been finished successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="134"/>
        <source>Sorry, the format operation is failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="135"/>
        <source>Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="144"/>
        <source>Formatting. Do not close this window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="153"/>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="156"/>
        <source>Begin Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/udfFormatDialog.cpp" line="157"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
